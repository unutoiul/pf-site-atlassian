You can find the latest `master` playbooks [here](https://statlas.prod.atl-paas.net/pf-site-admin-ui-playbooks-master/playbooks-gitbook/)

You can also refer to the `playbooks/` folder for more information on how to contribute to this repository.
