def typescriptJob(parameters) {
  job(
    key: parameters.key,
    name: parameters.name,
    description: parameters.description
  ) {
    miscellaneousConfiguration() {
      if (java.net.InetAddress.getLocalHost().getHostName() != "deployment-bamboo") {
        pbc(enabled:'true', image:'docker.atl-paas.net/cloud-admin/pf-site-admin-ui-builder', size:'regular')
      } else {
        pbc(enabled:'true', image:'docker.atl-paas.net/sox/cloud-admin/pf-site-admin-ui-builder', size:'regular')
      }
    }
    task(type:'checkout',description:'Checkout Default Repository') {
      repository(name:'PF Site Admin UI')
    }
    task(type:'script', description: 'Yarn dependencies', scriptBody: 'yarn install')
    task(type:'script', description: parameters.name, scriptBody: 'yarn ts-node ' + parameters.filenameAndArgs)
  }
}

plan(key:'PFADMINUIMD',name:'pf-site-admin-ui Commit metadata Plan') {
  project(key:'FAB',name:'Product Fabric')
  repository(name:'PF Site Admin UI')

  trigger(type:'polling',strategy:'periodically',frequency:'180') {
    repository(name:'PF Site Admin UI')
  }

  notification(type:'Failed Builds and First Successful',recipient:'committers')

  notification(type:'Change of Build Status',recipient:'hipchat',
    apiKey:'${bamboo.atlassian.hipchat.apikey.password}',
    notify:'false',room:'Product Fabric MTV Builds')

  stage(name: 'Generate and upload metadata', description: 'Runs various jobs in parallel. Each of these jobs generates a part of commit metadata that gets uploaded to statlas') {
    typescriptJob(
      key: 'GENBB',
      name: 'Generate branch preview, commit file info, and upload metadata to buildboard',
      description: 'Builds the thing and uploads it to statlas for later use as a branch preview. It also builds and saves information about file bundles. This is the only pieces of information required for generating buildboard preview, so we do it here as well.',
      filenameAndArgs: './bin/build-metadata/tasks/metadata.ts',
    )
    // This step is consistently failing on builds we need to investigate what is happening
    // Details https://hello.atlassian.net/browse/ADMPF-817
    // typescriptJob(
    //   key: 'GENCOV',
    //   name: 'Generate coverage',
    //   description: 'Generates HTML code coverage report',
    //   filenameAndArgs: './bin/build-metadata/tasks/coverage.ts',
    // )
    typescriptJob(
      key: 'GENSB',
      name: 'Generate storybooks',
      description: 'Builds storybooks and uploads it to statlas',
      filenameAndArgs: './bin/build-metadata/tasks/storybooks.ts',
    )
    typescriptJob(
      key: 'GENPB',
      name: 'Generate playbooks',
      description: 'Generates playbooks as a GitBook',
      filenameAndArgs: './bin/build-metadata/tasks/playbooks.ts',
    )
    typescriptJob(
      key: 'RUNSC',
      name: 'Run SourceClear',
      description: 'Runs SourceClear for the project',
      filenameAndArgs: './bin/build-metadata/tasks/sourceclear.ts',
    )
    typescriptJob(
      key: 'RUNANALYZE',
      name: 'Run Webpack Analyzer',
      description: 'Runs webpack analyzer for the project',
      filenameAndArgs: './bin/build-metadata/tasks/webpack-analyzer.ts',
    )
    typescriptJob(
      key: 'RUNPREMIGRATION',
      name: 'Run pre-migration tasks',
      description: 'Runs tasks that have not yet been incorporated into the general plan',
      filenameAndArgs: './bin/build-metadata/tasks/pre-migration-task.ts --webpack-analyzer',
    )
  }

  if (java.net.InetAddress.getLocalHost().getHostName() != "deployment-bamboo") {
    // Not on depbac - monitor branch builds
    branchMonitoring(notificationStrategy: 'INHERIT') {
      createBranch(matchingPattern: '.*')
      inactiveBranchCleanup(periodInDays: '30')
      deletedBranchCleanup(periodInDays: '1')
    }
  }

  planMiscellaneous() {
    planOwnership(owner:'arattihalli')
  }
}

