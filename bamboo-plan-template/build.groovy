plan(key:'PFADMINUI',name:'pf-site-admin-ui Build and Test Plan (via Docker)') {
  project(key:'FAB',name:'Product Fabric')
  repository(name:'PF Site Admin UI')

  trigger(type:'polling',strategy:'periodically',frequency:'180') {
    repository(name:'PF Site Admin UI')
  }

  notification(type:'Failed Builds and First Successful',recipient:'committers')

  stage(name:'Test Stage',description:'Test the code to ensure that it works.') {
    job(key:'ALLTESTS',name:'Test and build',description:'Run all the tests against our code.') {
      requirement(key:'system.isolated.docker.for',condition:'exists')
      artifactDefinition(
        name:'CloudFront distribution files',
        location:'.',
        pattern:'build/dist/**/*',
        shared:'true'
      )
      artifactDefinition(
        name:'Service descriptor file',
        location:'.',
        pattern:'site-admin-ui.sd.yml',
        shared:'true'
      )
      artifactDefinition(
        name: 'Package file',
        location: '.',
        pattern: 'package.json',
        shared: 'true'
      )
      artifactDefinition(
        name:'Full build output',
        location:'.',
        pattern:'build/webpack-dist/**/*',
        shared:'true'
      )
      // This step is consistently failing on builds we need to investigate what is happening
      // Details https://hello.atlassian.net/browse/ADMPF-817
      // artifactDefinition(
      //   name:'Clover Report',
      //   location:'build/coverage',
      //   pattern:'**/*.*',
      //   shared:'true'
      // )
      artifactDefinition(
        name:'Deployment binaries',
        location:'.',
        pattern:'bin/**/*',
        shared:'true'
      )
      task(type:'checkout',description:'Checkout Default Repository') { repository(name:'PF Site Admin UI') }
      task(type:'script',description:'Yarn install',scriptBody:'yarn install')
      task(type:'script',description:'Yarn build',scriptBody:'yarn build')
      task(type:'script',description:'Yarn check-src-generation',scriptBody:'yarn check-src-generation')
      task(type:'script',description:'Lint',scriptBody:'yarn lint')
      // This step used to run with coverage too
      // Details https://hello.atlassian.net/browse/ADMPF-817
      // task(type:'script',description:'Yarn test with coverage',scriptBody:'yarn test:coverage')
      task(type:'script',description:'Yarn tests',scriptBody:'yarn test:nolint:bamboo')
      task(type:'script',description:'Yarn test for BUX',scriptBody:'yarn test:bux:bamboo')
      task(
        type: 'custom',
        createTaskKey: 'com.atlassian.bamboo.plugins.bamboo-nodejs-plugin:task.reporter.mocha',
        description: 'Parse output from bamboo reporter',
        final: 'true',
        testPattern: '*-mocha.json',
      )
      miscellaneousConfiguration() {
        if (java.net.InetAddress.getLocalHost().getHostName() != "deployment-bamboo") {
          pbc(enabled:'true',image:'docker.atl-paas.net/cloud-admin/pf-site-admin-ui-builder', size:'large')
        } else {
          pbc(enabled:'true',image:'docker.atl-paas.net/sox/cloud-admin/pf-site-admin-ui-builder', size:'large')
        }
        // This step is consistently failing on builds we need to investigate what is happening
        // Details https://hello.atlassian.net/browse/ADMPF-817
        // clover(type:'custom',path:'build/coverage/clover.xml')
      }
    }
  }

  if (java.net.InetAddress.getLocalHost().getHostName() != "deployment-bamboo") {
    // Not on depbac - monitor branch builds
    branchMonitoring(notificationStrategy: 'INHERIT') {
      createBranch(matchingPattern: '.*')
      inactiveBranchCleanup(periodInDays: '30')
      deletedBranchCleanup(periodInDays: '1')
    }
  }

  if (java.net.InetAddress.getLocalHost().getHostName() == "deployment-bamboo") {
    notification(type:'Failed Builds and First Successful',recipient:'stride',room:'Cloud Admin Notifications')
  }

  planMiscellaneous() {
    planOwnership(owner:'arattihalli')
  }

  permissions() {
    anonymous(permissions:'read')
    loggedInUser(permissions:'read,build')
  }
}
