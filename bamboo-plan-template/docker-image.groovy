plan(key:'PFSITEADMINUIBUILDDOCKERCONTAINER',name:'pf-site-admin-ui Docker build container image Plan',
  description:'Builds SOX-compliant Docker build container and publishes it to sox namespace of internal atlassian docker repo') {
  project(key:'FAB',name:'Product Fabric')
  repository(name:'PF Site Admin UI')

  trigger(type:'polling',strategy:'periodically',frequency:'180') {
    repository(name:'PF Site Admin UI')
  }
  trigger(type:'cron',description:'Daily',cronExpression:'0 0 0 ? * *') {
    repository(name:'PF Site Admin UI')
  }

  notification(type:'Failed Builds and First Successful',recipient:'committers')

  stage(name:'Docker Preparation Stage',description:'Builds and pushes docker image') {
    job(key:'DockerBuild',name:'Build and push',description:'') {
      requirement(key:'system.isolated.docker.for',condition:'exists')
      task(type:'checkout',description:'Checkout Default Repository') {
        repository(name:'PF Site Admin UI')
      }
      task(type:'script',description:'Build docker image',scriptBody:'./bin/docker/build-docker-image.sh --no-cache')
      if (java.net.InetAddress.getLocalHost().getHostName() != "deployment-bamboo") {
        task(type:'script',description:'Publish docker image to Atlassian repo',scriptBody:'./bin/docker/publish-docker-image.sh')
      } else {
        task(type:'script',description:'Publish docker image to Atlassian repo under SOX',scriptBody:'./bin/docker/publish-docker-image.sh --sox')
      }
      miscellaneousConfiguration() {
        pbc(enabled:'true',image:'docker.atl-paas.net/sox/buildeng/agent-baseagent') {
          extraContainer(name:'docker',image:'docker:stable-dind',size:'large')
        }
      }
    }
  }

  planMiscellaneous() {
    planOwnership(owner:'arattihalli')
  }

  permissions() {
    anonymous(permissions:'read')
    loggedInUser(permissions:'read,build')
  }
}
