plan(key:'PFADMINI18NPSP',name:'pf-site-admin-ui I18n Push Strings Plan') {
  project(key:'FAB',name:'Product Fabric')
  repository(name:'PF Site Admin UI')

  trigger(type:'polling',strategy:'periodically',frequency:'180') {
    repository(name:'PF Site Admin UI')
  }

  notification(type:'Failed Builds and First Successful',recipient:'committers')

  stage(name: 'Pushing strings', description: 'Pushing new strings on Admin Hub to Transifex') {
    job(key:'PUSHSTRINGS', name:'Push strings', description:'Pushing strings to Transifex') {
      miscellaneousConfiguration() {
        if (java.net.InetAddress.getLocalHost().getHostName() != "deployment-bamboo") {
          pbc(enabled:'true', image:'docker.atl-paas.net/cloud-admin/pf-site-admin-ui-builder', size:'regular')
        } else {
          pbc(enabled:'true', image:'docker.atl-paas.net/sox/cloud-admin/pf-site-admin-ui-builder', size:'regular')
        }
      }
      task(type:'checkout',description:'Checkout Default Repository') {
        repository(name:'PF Site Admin UI')
      }
      task(type:'script', description: 'Yarn dependencies', scriptBody: 'yarn install')
      task(type:'script', description: 'Push strings from src to Transifex', scriptBody: 'yarn i18n:push')
    }
  }

  planMiscellaneous() {
    planOwnership(owner:'kdavis')
  }
}

