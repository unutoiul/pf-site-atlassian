plan(key:'PFADMINI18NPTP',name:'pf-site-admin-ui I18n Push Translations Plan') {
  project(key:'FAB',name:'Product Fabric')
  repository(name:'PF Site Admin UI')

  trigger(type:'cron',description:'Daily',cronExpression:'0 0 15 * * ?') {
    repository(name:'PF Site Admin UI')
  }

  notification(type:'Failed Builds and First Successful',recipient:'committers')

  stage(name: 'Pull and Push new translations', description: 'Pushing new translations after pulling from Transifex into Bitbucket') {
    job(key:'NEWTRANSLATIONS', name:'Pull and push new translations', description:'Pull new translations from Transifex and push to Bitbucket') {
      miscellaneousConfiguration() {
        if (java.net.InetAddress.getLocalHost().getHostName() != "deployment-bamboo") {
          pbc(enabled:'true', image:'docker.atl-paas.net/cloud-admin/pf-site-admin-ui-builder', size:'regular')
        } else {
          pbc(enabled:'true', image:'docker.atl-paas.net/sox/cloud-admin/pf-site-admin-ui-builder', size:'regular')
        }
      }
      task(type:'checkout',description:'Checkout Default Repository') {
        repository(name:'PF Site Admin UI')
      }
      task(type:'script', description: 'Yarn dependencies', scriptBody: 'yarn install')
      task(type:'script', description: 'Pull all translations for other 24 languages from Transifex', scriptBody: 'yarn i18n:pull')
      task(type:'script', description: 'Push new translations from Transifex to Bitbucket', scriptBody: 'yarn i18n:git-push')
    }
  }

  planMiscellaneous() {
    planOwnership(owner:'kdavis')
  }
}

