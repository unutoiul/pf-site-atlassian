def environmentCommonConfig(parameters) {
  if (parameters.strideNotifications) {
    notification(type:'Deployment Failed',recipient:'slack',
        webhookUrl:'https://hooks.slack.com/services/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',
        channel:'#access-alerts')
  }
  task(type:'pbc', image:'docker.atl-paas.net/sox/cloud-admin/pf-site-admin-ui-builder', size:'regular')
  task(type:'cleanWorkingDirectory')
  task(type:'artifactDownload',description:'Download release contents',planKey:'FAB-PFADMINUI') {
    artifact(name:'all artifacts',localPath:'.')
  }

  task(type:'script', description:'Make all deployment binaries executable', scriptBody:'find ./bin/ -type f -iname "*.sh" -exec chmod +x {} \\;')

  if (parameters.env == "prod-east") {
    task(
      type:'script', 
      description: 'Check for Jira blocker issues',
      scriptBody:'./bin/check-for-release-blockers.sh'
    )
    task(
      type:'script', 
      description: 'Run Pollinator PDV checks',
      scriptBody:'./bin/run-pollinator-pdv-checks.sh'
    )
  }

  task(
    type:'script',
    description:'Deploy to ' + parameters.env,
    scriptBody:'./bin/deploy-in-bamboo.sh',
    environmentVariables:'MICROS_ENV=' + parameters.env
  )

  if (parameters.newRelicSourcemapsEnv) {
    task(
      type:'script',
      description:'Upload JS sourcemaps to New Relic',
      scriptBody:'./bin/upload-sourcemaps-to-nr.sh ' + parameters.newRelicSourcemapsEnv
    )
  }
}

deployment(name:'pf-site-admin-ui Deployment Plan',planKey:'FAB-PFADMINUI',
  description:'The deployment plan for pf-site-admin-ui. Deploys to micros static') {
  versioning(version:'release-2',autoIncrementNumber:'true')

  environment(name:'Domain Dev') {
    trigger(type:'afterSuccessfulPlan',description:'Automatically go to ddev')
    environmentCommonConfig(env:'ddev',ownerNotifications:true)
  }

  if (java.net.InetAddress.getLocalHost().getHostName() == "deployment-bamboo") {
    // Only allow to deploy to staging and prod from deployment bamboo
    environment(name:'Staging East',description:'Staging East') {
      trigger(type:'afterSuccessfulDeployment',environment:'Domain Dev')
      environmentCommonConfig(env:'stg-east',newRelicSourcemapsEnv:'stg',ownerNotifications:true)
    }
    environment(name:'Prod East',description:'Prod East') {
      trigger(type:'afterSuccessfulDeployment',environment:'Staging East')
      environmentCommonConfig(env:'prod-east',newRelicSourcemapsEnv:'prod',strideNotifications:true)
    }
    environment(name:'Prod West (DR passive)',description:'Prod West is a DR region that will be deployed on as-needed basis') {
      environmentCommonConfig(env:'prod-west',newRelicSourcemapsEnv:'prod',strideNotifications:true)
    }
  }
}
