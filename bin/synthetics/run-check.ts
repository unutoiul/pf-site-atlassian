// tslint:disable:no-console

import { ArgumentParser } from 'argparse';

import {
  cleanupCompiledDir,
  compileChecks,
  compiledCheckPath,
  isPollinatorInstalled,
  pollinatorError,
  runShellCmd,
  scriptsDir,
} from './utils';

const parser = new ArgumentParser({
  addHelp: true,
  description: 'Run synthetic checks with pollidev',
});

parser.addArgument(['--file'], {
  help: `Filename of the selenium webdriver test (with extension) under ${scriptsDir}.`,
  required: true,
});

(async () => {
  const args = parser.parseArgs();

  if (!isPollinatorInstalled()) {
    console.log(pollinatorError);

    return;
  }

  try {
    console.log(`Compiling...`);
    await compileChecks();

    console.log('Running...');
    const result = await runShellCmd(`cd tests/synthetics && pollidev run ${compiledCheckPath(args.file)}`);
    console.log(result);
  } catch (err) {
    console.log('Error: unable to run pollidev run', err);

    return;
  } finally {
    cleanupCompiledDir();
  }
})();
