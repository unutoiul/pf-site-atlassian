// tslint:disable:no-console

import { ArgumentParser } from 'argparse';

import {
  cleanupCompiledDir,
  compileChecks,
  isPollinatorInstalled,
  pollinatorError,
  runShellCmd,
} from './utils';

const parser = new ArgumentParser({
  addHelp: true,
  description: 'List current synthetic checks with pollidev',
});

(async () => {
  parser.parseArgs();

  if (!isPollinatorInstalled()) {
    console.log(pollinatorError);

    return;
  }

  try {
    console.log(`Compiling...`);
    await compileChecks();

    const result = await runShellCmd(`cd tests/synthetics && pollidev list -t synthetic`);
    console.log(result);
  } catch (err) {
    console.log('Error: unable to run pollidev list', err);

    return;
  } finally {
    cleanupCompiledDir();
  }
})();
