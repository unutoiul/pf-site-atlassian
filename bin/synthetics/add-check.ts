// tslint:disable:no-console

import { ArgumentParser } from 'argparse';
import * as fs from 'fs';
import * as yaml from 'js-yaml';

import {
  cleanupCompiledDir,
  compileChecks,
  compiledCheckPath,
  isPollinatorInstalled,
  pollinatorError,
  runShellCmd,
  scriptsDir,
} from './utils';

const checkNamePrefix = 'Cloud Admin - ';
const checkFile = 'pollinator-checks.yml';
const tmpScriptsDir = 'tmp-scripts';
const tmpFile = 'tmp';
const templateFile = 'bin/synthetics/template.yml';

const loadTemplate = () => {
  try {
    return yaml.safeLoad(fs.readFileSync(templateFile));
  } catch (err) {
    console.log('Unable to load template file');

    return {};
  }
};

const loadCheckFile = () => {
  try {
    return yaml.safeLoad(fs.readFileSync(`tests/synthetics/${checkFile}`));
  } catch (err) {
    console.log('Unable to load pollinator check file');

    return {};
  }
};

const parser = new ArgumentParser({
  addHelp: true,
  description: `
    Create new synthetic checks which conform to the Cloud Admin checks template.
    See the POLLINATOR_SYNTHETICS.md playbook for more details.
  `,
});

parser.addArgument(['--file'], {
  help: `Filename of the selenium webdriver test under ${scriptsDir}.`,
  required: true,
});

parser.addArgument(['--name'], {
  help: `The name of the pollinator check. Please note the name will be appended to the prefix "${checkNamePrefix}".`,
  required: true,
});

(async () => {
  const args = parser.parseArgs();

  if (!isPollinatorInstalled()) {
    console.log(pollinatorError);

    return;
  }

  if (!fs.existsSync(`tests/synthetics/${tmpScriptsDir}`)) {
    fs.mkdirSync(`tests/synthetics/${tmpScriptsDir}`);
  }

  const checkTemplate = loadTemplate();
  checkTemplate.plugin.script_path = compiledCheckPath(args.file);
  checkTemplate.name = checkNamePrefix + args.name;

  fs.appendFileSync(`tests/synthetics/${tmpFile}`, yaml.safeDump(checkTemplate));

  let result: string | null = null;
  try {
    console.log('Compiling...');
    await compileChecks();

    console.log('Creating...');
    result = await runShellCmd(
      `cd tests/synthetics && pollidev create --descriptor-file ${checkFile} --script-dir ${tmpScriptsDir} --interactive=false ${tmpFile}`,
    );
  } catch (err) {
    console.log('Error: unable to run pollidev create', err);

    return;
  } finally {
    fs.rmdirSync(`tests/synthetics/${tmpScriptsDir}`);
    fs.unlinkSync(`tests/synthetics/${tmpFile}`);
    cleanupCompiledDir();
  }

  const match = result.match(/UUID: ([\w-]+)/);
  let uuid: string | null = null;

  if (match) {
    uuid = match[1];
  } else {
    console.log('Error: unable to get check UUID');

    return;
  }

  const checkFileObj = loadCheckFile();
  checkFileObj.checks[uuid].plugin.script_path = compiledCheckPath(args.file);
  fs.writeFileSync(`tests/synthetics/${checkFile}`, yaml.safeDump(checkFileObj));

  console.log(result);
  console.log(`Ensure you check in your synthetic check script file named: ${scriptsDir}/${args.file}`);
})();
