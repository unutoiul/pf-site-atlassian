import { exec } from 'child_process';
import { sync as rimrafSync } from 'rimraf';

export const scriptsDir = 'checks';
export const pollinatorError = 'Error: Missing pollidev - install the Pollinator CLI ( https://extranet.atlassian.com/x/YxSSy )';

export const runShellCmd = async (command: string): Promise<string> => {
  return new Promise<string>((resolve, reject) => {
    exec(command, (error, stdout) => {
      error ? reject(error) : resolve(stdout);
    });
  });
};

export const isPollinatorInstalled = async (): Promise<boolean> => {
  try {
    const result = await runShellCmd('command -v pollidev');

    return !!result && result.length > 0;
  } catch (err) {
    return false;
  }
};

export const compileChecks = async () => {
  await runShellCmd(`tsc -p tests/synthetics/tsconfig.json`);
};

export const cleanupCompiledDir = () => {
  rimrafSync('tests/synthetics/checks/compiled');
};

export const compiledCheckPath = (filename: string): string => {
  return `${scriptsDir}/compiled/${filename.replace('.ts', '.js')}`;
};
