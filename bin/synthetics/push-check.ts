// tslint:disable:no-console

import { ArgumentParser } from 'argparse';

import {
  cleanupCompiledDir,
  compileChecks,
  isPollinatorInstalled,
  pollinatorError,
  runShellCmd,
} from './utils';

const parser = new ArgumentParser({
  addHelp: true,
  description: 'Push a synthetic check with pollidev',
});

parser.addArgument(['--UUID'], {
  help: `UUID of the check being pushed`,
  required: true,
});

(async () => {
  const args = parser.parseArgs();

  if (!isPollinatorInstalled()) {
    console.log(pollinatorError);

    return;
  }

  try {
    console.log(`Compiling...`);
    await compileChecks();

    console.log('Pushing...');
    const result = await runShellCmd(`cd tests/synthetics && pollidev push -y ${args.UUID}`);
    console.log(result);
  } catch (err) {
    console.log('Error: unable to run pollidev push', err);

    return;
  } finally {
    cleanupCompiledDir();
  }
})();
