import { writeFileSync } from 'fs';
import { buildSchemaFromTypeDefinitions } from 'graphql-tools';
import { printSchema } from 'graphql/utilities';
import { parse } from 'path';

import { ensureCleanDir } from './util/fs-util';

import { typeDefs } from '../src/schema/schema-defs';

const outFile = process.argv[2];

ensureCleanDir(parse(outFile).dir);
writeFileSync(outFile, printSchema(buildSchemaFromTypeDefinitions(typeDefs)));
