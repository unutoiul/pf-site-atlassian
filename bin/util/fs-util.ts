import { existsSync, mkdirSync } from 'fs';
import { join, sep } from 'path';
import { sync as rimrafSync } from 'rimraf';

export function ensureCleanDir(dir: string) {
  rimrafSync(dir);
  const dirs = dir.split(sep);
  let curPath = '';

  // ensure that the dir tree exists
  dirs.forEach(d => {
    const path = join(curPath, d);
    if (!existsSync(path)) {
      mkdirSync(path);
    }

    curPath = path;
  });
}
