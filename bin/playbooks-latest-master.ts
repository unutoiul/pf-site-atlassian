import { getGitRevision, getLastMasterRevisions } from './build-metadata/util/git';
import { playbooksLink } from './build-metadata/util/links';
import { runShellScript } from './build-metadata/util/shell';

async function openLatestMasterPlaybooks() {
  const [rev] = await getLastMasterRevisions(1, await getGitRevision());

  await runShellScript(`open "${playbooksLink(rev)}"`);
}

openLatestMasterPlaybooks();
