#!/usr/bin/env bash
#
# This script uploads sourcemap files to New Relic

API_KEY="${bamboo_pf_site_admin_ui_new_relic_admin_api_key_secret}"

if [ -z "$1" ]; then
    echo -e "Error: Environment is not specified.\n\nUsage:\n\t$0 [stg|prod]"
    exit 1
fi

if [ "$1" == "stg" ]; then
  HOST="admin.stg.atlassian.com"
  APP_ID=82887529
elif [ "$1" == "prod" ]; then
  HOST="admin.atlassian.com"
  APP_ID=82733089
else
  echo -e "Error: Unknown environment ($1).\n\nUsage:\n\t$0 [stg|prod]"
  exit 2
fi

npm install -g @newrelic/publish-sourcemap

function upload {
  MAP_FILE_PATH=$1
  FILE_PATH=${MAP_FILE_PATH%.map}
  FILE_NAME=$(basename "${FILE_PATH}")
  FILE_URL=https://${HOST}/${FILE_NAME}

  publish-sourcemap "${MAP_FILE_PATH}" "$FILE_URL"  --applicationId="${APP_ID}" --nrAdminKey="${API_KEY}"
}

CURRENT_DIR=`dirname $0`
DIST_DIR=${CURRENT_DIR}/../build/webpack-dist

# there may not be sourcemaps if we're deploying an old artifact (ADMIN-580)
shopt -s nullglob

for file in ${DIST_DIR}/*.js.map
do
    upload "$file"
done
