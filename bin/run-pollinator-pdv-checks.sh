#!/bin/bash

#
# This script is setup to execute the Atlassian Access Pollinator SLI Checks.
# [repo: https://bitbucket.org/atlassian/atlassian-access-pollinator-synthetics]
# It should be run before each prod deploy,
# the checks defined in POLLINATOR_CHECK_IDS are configured to run against staging.
#

POLLINATOR_URL_CHECK=https://pollinator.prod.atl-paas.net/api/v2/checks
POLLINATOR_URL_RUN_ONCE=https://pollinator.prod.atl-paas.net/api/v2/adhoc/execute
POLLINATOR_USER="${bamboo_access_iom_pollinator_user}"
POLLINATOR_PASSWORD="${bamboo_access_iom_pollinator_password}"
POLLINATOR_CHECK_IDS=("99ad5cdd-ba4d-42e4-9213-f46a141e5a34" "1f886f64-e60b-4c6b-a3ba-ffe45b3bc6e2" "a071203f-5ff6-474c-ab4f-38c6e2f1295d" "f23f35f5-f0eb-48ad-8217-2336501c89a8" "c7ecc534-e08e-4b48-8398-d271ad4da07a" "ba161cb5-dbfa-4245-b29b-1ed05921f47b" "90c32c1f-c81f-4e0a-b051-5a6ea2abebfd" "7c19e1de-55a3-41e7-897a-325d737ff21b" "1f4a39c5-782a-4939-b48b-9cee61965fc1")
RUN_DESCRIPTION="Pollinator PDV Test"

# Call the Run Once API to execute a check and store the result in a json file
# The execution is retried 3 times if the check failed
function run_once() {
    CHECK_ID="$@"
    CHECK_RESULT_JSON="check_${CHECK_ID}_result.json"
    for retry in 1 2 3
    do
        RESPONSE_STATUS=$(
            curl -sS \
                 -o ${CHECK_RESULT_JSON} \
                 -u "${POLLINATOR_USER}:${POLLINATOR_PASSWORD}" \
                 -d "{ \"template_check_uuid\": \"${CHECK_ID}\", \"description\": \"${RUN_DESCRIPTION}\", \"type\": \"synthetic\" }" \
                 -H "Content-Type: application/json" \
                 -w "%{http_code}" \
                 ${POLLINATOR_URL_RUN_ONCE}
        )
        if [ "$RESPONSE_STATUS" -eq 200 ]
        then
            CHECK_RESULT=$(jq '.data.result.status' ${CHECK_RESULT_JSON})
            if [ "${CHECK_RESULT}" = '"OK"' ]
            then
                break
            fi
        else
            sleep $(bc -l <<< "e(${retry}*0.5)") # exponential backoff
        fi
    done
}

# Call the Check API to retrieve and store check details in a json file
function check_info() {
    CHECK_ID="$@"
    CHECK_DETAILS_JSON="check_${CHECK_ID}_details.json"
    curl -sS \
         -o ${CHECK_DETAILS_JSON} \
         -u "${POLLINATOR_USER}:${POLLINATOR_PASSWORD}" \
         -X GET "${POLLINATOR_URL_CHECK}/${CHECK_ID}"
}

echo "Executing checks..."

# Execute all checks as background processes
CHECK_PIDS=()
for i in "${!POLLINATOR_CHECK_IDS[@]}"
do
    CHECK_ID=${POLLINATOR_CHECK_IDS[$i]}

    # Verify if check is enabled
    check_info "${CHECK_ID}"
    CHECK_DETAILS_JSON="check_${CHECK_ID}_details.json"
    CHECK_DISABLED=$(jq '.data[0].check_inactive' ${CHECK_DETAILS_JSON})
    CHECK_NAME=$(jq '.data[0].name' ${CHECK_DETAILS_JSON})

    # Execute enabled checks only
    if [ ${CHECK_DISABLED} = "false" ]
    then
        run_once ${CHECK_ID} &
        CHECK_PIDS[$i]=$!
        echo "${CHECK_ID}: status=STARTED,  name=${CHECK_NAME}, pid=${CHECK_PIDS[$i]}"
    else
        echo "${CHECK_ID}: status=IGNORED, name=${CHECK_NAME}"
    fi
done

echo "Waiting for results..."

# Wait for all processes to complete and check results
EXIT_CODE=0
for i in "${!POLLINATOR_CHECK_IDS[@]}"
do
    CHECK_ID=${POLLINATOR_CHECK_IDS[$i]}
    CHECK_PID=${CHECK_PIDS[$i]}

    # wait for process to complete
    while kill -0 ${CHECK_PID} 2> /dev/null; do sleep 1; done;

    CHECK_RESULT_JSON="check_${CHECK_ID}_result.json"
    CHECK_DETAILS_JSON="check_${CHECK_ID}_details.json"
    CHECK_DISABLED=$(jq '.data[0].check_inactive' ${CHECK_DETAILS_JSON})
    CHECK_NAME=$(jq '.data[0].name' ${CHECK_DETAILS_JSON})

    # Check results for enabled checks only
    if [ ${CHECK_DISABLED} = "false" ]
    then
        CHECK_RESULT=$(jq '.data.result.status' ${CHECK_RESULT_JSON})
        if [ "${CHECK_RESULT}" = '"OK"' ]
        then
            echo "${CHECK_ID}: result=SUCCESS,  name=${CHECK_NAME}, pid=${CHECK_PIDS[$i]}"
        else
            echo "${CHECK_ID}: result=ERROR,    name=${CHECK_NAME}, pid=${CHECK_PIDS[$i]}"
            EXIT_CODE=1
        fi
    fi
done

# Exit if at least one check returned a failure
if [ ${EXIT_CODE} -ne 0 ]
then
    exit "${EXIT_CODE}"
fi