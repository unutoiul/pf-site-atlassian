# Always allow rollback deployments
if [ "${bamboo_deploy_rollback}" = true ]; then
  echo "Proceed with the rollback deployment"
  exit 0;
fi 

# We fetch all the issue that match issue type release blocker from Cloud Admin project.
JIRA_BLOCKERS_URL='https://product-fabric.atlassian.net/rest/api/2/search?jql=filter=15793'

HTTP_RESPONSE=$(
  curl --user "${bamboo_cloud_admin_jira_user}:${bamboo_cloud_admin_jira_user_password}" \
       --header 'Accept: application/json' \
       --url $JIRA_BLOCKERS_URL \
       --silent \
       --write-out "HTTPSTATUS:%{http_code}" \
)

HTTP_BODY=$(
  echo $HTTP_RESPONSE | sed -e 's/HTTPSTATUS\:.*//g'
)

HTTP_STATUS=$(
  echo $HTTP_RESPONSE | tr -d '\n' | sed -e 's/.*HTTPSTATUS'://
)

if [ ! $HTTP_STATUS -eq 200 ]; then
  echo "Error: Could not fetch release blockers from Jira [Http Status: $HTTP_STATUS]"
  exit 1
fi

TOTAL=$(echo "$HTTP_BODY" | jq .total)
if [ ! $TOTAL -eq 0 ]; then
  echo "!!!Total of $TOTAL release blocker(s) found in Jira!!!"
  exit 1
else
  echo "No release blockers found! Proceed to deployment!"
fi 
