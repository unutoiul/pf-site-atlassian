import { readdirSync, readFileSync, writeFileSync } from 'fs';

const defsOutputFile = process.argv[2];

const requireGraphQL = name => {
  const filename = require.resolve(name);

  return readFileSync(filename, 'utf8');
};

const typeDefs = readdirSync('./src/schema/type-defs')
  .filter(filename => filename.endsWith('.graphql'))
  .map(filename => requireGraphQL(`../src/schema/type-defs/${filename}`))
  .join('\n');

writeFileSync(defsOutputFile, `/* tslint:disable */
// This file was automatically generated
export const typeDefs = \`
${typeDefs}
\`;
/* tslint:enable */
`);
