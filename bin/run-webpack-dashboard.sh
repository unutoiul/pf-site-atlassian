# Script is used to keep the webpack dashboard up-to-date
# to display it on the TV in the office.
while true; do
  git checkout master
  git reset --hard HEAD
  git pull --rebase origin master

  yarn install
  JARVIS=1 yarn run start &

  last_pid=$!
  echo "Current PID: ${last_pid}"
  sleep 3600 # 1 hour

  echo "Killing PID: ${last_pid}"
  kill $last_pid
done