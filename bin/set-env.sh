#!/usr/bin/env bash
#
# This script substitutes the __CDN_URL__ placeholder from dist/index.html with the matching micros host that serves
# the CDN assets depending on the current MICROS_ENV variable.
#
# It also substitutes the __ENV__ placeholder from dist/index.html with the current MICROS_ENV so that the runtime
# configuration in src/config knows in which environment the app is running.

set -e

if [ -z "$1" ]; then
    echo "Path to index.html file not specified. Usage: MICROS_ENV=[local|ddev|stg-east|prod-east|prod-west] $0 path/to/index.html"
    exit 5
fi

if [ -z "$MICROS_ENV" ]; then
    echo "MICROS_ENV not specified. Usage: MICROS_ENV=[local|ddev|stg-east|prod-east|prod-west] $0 path/to/index.html"
    exit 1
fi

new_relic_substitution='s/<meta new-relic-placeholder>//'
segment_atlassian_access_substitution='s/<meta segment-atlassian-access-placeholder>//'

if [ "$MICROS_ENV" == "local" ]; then
  cdn_url="http://localhost:3001"
elif [ "$MICROS_ENV" == "ddev" ]; then
  cdn_url="//admin.dev.atlassian.com"
  new_relic_substitution='s/<meta new-relic-placeholder>/`cat bin\/new-relic\/stg.html`/e'
  segment_atlassian_access_substitution='s/<meta segment-atlassian-access-placeholder>/`cat bin\/segment-atlassian-access\/stg.html`/e'
elif [ "$MICROS_ENV" == "stg-east" ]; then
  cdn_url="//admin.stg.atlassian.com"
  new_relic_substitution='s/<meta new-relic-placeholder>/`cat bin\/new-relic\/stg.html`/e'
  segment_atlassian_access_substitution='s/<meta segment-atlassian-access-placeholder>/`cat bin\/segment-atlassian-access\/stg.html`/e'
elif [ "$MICROS_ENV" == "prod-east" -o "$MICROS_ENV" == "prod-west" ]; then
  cdn_url="//admin.atlassian.com"
  new_relic_substitution='s/<meta new-relic-placeholder>/`cat bin\/new-relic\/prod.html`/e'
  segment_atlassian_access_substitution='s/<meta segment-atlassian-access-placeholder>/`cat bin\/segment-atlassian-access\/prod.html`/e'
else
  echo "Unknown MICROS_ENV: ${MICROS_ENV}"
  exit 2
fi
if [ "$PREVIEW_MODE" == "branch" ]; then
  cdn_url=""
fi

# in-place substitution without having to escape regex characters
perl -pi -e "s|__CDN_URL__|${cdn_url}|g" "$1" || exit 3
perl -pi -e "s|__ENV__|${MICROS_ENV}|g" "$1" || exit 4
perl -pi -e "$new_relic_substitution" "$1" || exit 5
perl -pi -e "$segment_atlassian_access_substitution" "$1" || exit 6
perl -pi -e "s|__BUILD_VERSION__|${bamboo_buildNumber}|g" "$1" || exit 7
