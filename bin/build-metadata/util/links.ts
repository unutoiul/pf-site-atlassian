export const siteAdminBranchPreviewLink = (gitRevision: string): string => `https://adminhub-branch-preview.stg.internal.atlassian.com/admin?sha=${gitRevision}`;
export const branchPreviewLink = (gitRevision: string): string => `https://adminhub-branch-preview.stg.internal.atlassian.com/?sha=${gitRevision}`;
export const storybooksLink = (gitRevision: string): string => `https://statlas.prod.atl-paas.net/pf-site-admin-ui-storybook-${gitRevision}/storybook/index.html`;
export const coverageLink = (gitRevision: string): string => `https://statlas.prod.atl-paas.net/pf-site-admin-ui-coverage-${gitRevision}/html/index.html`;
export const playbooksLink = (gitRevision: string): string => `https://statlas.prod.atl-paas.net/pf-site-admin-ui-playbooks-${gitRevision}/playbooks-gitbook/`;
export const sourceClearLink = (gitBranch: string): string => `https://atlassian.sourceclear.io/teams/Paaina7/projects/64759/vulnerabilities?branch=${encodeURIComponent(gitBranch)}`;
export const webpackAnalyzeLink = (gitRevision: string): string => `https://statlas.prod.atl-paas.net/pf-site-admin-ui-analyze-${gitRevision}/analyze-dist/report.html`;
