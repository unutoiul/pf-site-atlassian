import { getGitBranch, getGitRevision, getLastMasterRevisions } from './git';
import {
  branchPreviewLink,
  coverageLink,
  playbooksLink,
  siteAdminBranchPreviewLink,
  sourceClearLink,
  storybooksLink,
  webpackAnalyzeLink,
} from './links';
import { logger } from './logger';
import { FileInfo, readCommitFileInfo } from './statlas';
import { formatAsTable } from './table-formatter';

function formatFileSize(size: number): string {
  const kb = 1024;
  const mb = 1024 * kb;

  if (size > mb) {
    return `${(size / mb).toFixed(2)} M`;
  }

  if (size > kb) {
    return `${(size / kb).toFixed(2)} K`;
  }

  return `${size} b`;
}

function relativeSizePercents(partialSize: number, wholeSize: number): string {
  return `${(partialSize / wholeSize * 100).toFixed(2)}%`;
}

function sizeDiffPercents(newSize: number, oldSize: number): string {
  const absoluteChange = newSize - oldSize;
  const percentChange = Math.abs(absoluteChange / oldSize) * 100;

  const percentageDescription = percentChange > 0.01
    ? percentChange.toFixed(2)
    : ' <0.01';

  if (absoluteChange > 0) {
    return `+${percentageDescription}%`;
  } else if (absoluteChange < 0) {
    return `-${percentageDescription}%`;
  } else {
    return '--';
  }
}

function sizeDiffAbsolute(newSize: number, oldSize: number): string {
  const absoluteChange = newSize - oldSize;

  if (absoluteChange > 0) {
    return `+${formatFileSize(Math.abs(absoluteChange))}`;
  } else if (absoluteChange < 0) {
    return `-${formatFileSize(Math.abs(absoluteChange))}`;
  } else {
    return '--';
  }
}

function getFileInfoRow(fileInfo: FileInfo, previousData: FileInfo[]): string[] {
  const previousFileInfo = (previousData || []).find(f => f.file === fileInfo.file);

  return [
    fileInfo.file,
    formatFileSize(fileInfo.sizeInBytes),
    previousFileInfo ? formatFileSize(previousFileInfo.sizeInBytes) : 'unknown',
    previousFileInfo ? sizeDiffAbsolute(fileInfo.sizeInBytes, previousFileInfo.sizeInBytes) : 'n/a',
    previousFileInfo ? sizeDiffPercents(fileInfo.sizeInBytes, previousFileInfo.sizeInBytes) : 'n/a',
  ];
}

function isVendorFile(file: FileInfo) {
  return !isNaN(parseInt(file.file, 10)) || file.file === 'vendor.js' || file.file.startsWith('vendors~');
}

function getTotalSize(files: FileInfo[]) {
  return files.reduce((sizeAccumulator, file) => file.sizeInBytes + sizeAccumulator, 0);
}

function getMetricRow(metricName: string, newSize: number, oldSize: number) {
  return [
    metricName,
    formatFileSize(newSize),
    formatFileSize(oldSize),
    sizeDiffAbsolute(newSize, oldSize),
    sizeDiffPercents(newSize, oldSize),
  ];
}

function formatFileData(current: FileInfo[], previous: FileInfo[]): string {
  const currentVendorFiles = current.filter(isVendorFile);
  const otherCurrentFiles = current.filter(c => !isVendorFile(c));
  const previousVendorFiles = previous.filter(isVendorFile);
  const otherPreviousFiles = previous.filter(c => !isVendorFile(c));

  const uncachedFiles = current.filter(c => !previous.find(p => p.originalFileName === c.originalFileName));
  const cachedFiles = current.filter(c => previous.find(p => p.originalFileName === c.originalFileName));

  const table = formatAsTable([
    ['File', 'Size:', 'Size (on master):', 'Size diff:', 'Size diff (%):'],
    ...otherCurrentFiles.map(row => getFileInfoRow(row, otherPreviousFiles)),
  ]);

  const metricTable = formatAsTable([
    ['Metric', 'Size:', 'Size (on master):', 'Size diff:', 'Size diff (%):'],
    getMetricRow('All assets', getTotalSize(current), getTotalSize(previous)),
    getMetricRow('Vendor assets', getTotalSize(currentVendorFiles), getTotalSize(previousVendorFiles)),
  ]);

  const allSizeChange = sizeDiffPercents(
    getTotalSize(otherCurrentFiles) + getTotalSize(currentVendorFiles),
    getTotalSize(otherPreviousFiles) + getTotalSize(previousVendorFiles),
  );

  let warningData = '';
  if (parseFloat(allSizeChange) >= 5) {
    warningData += `⚠️ Warning! Your bundle size changed more than ${allSizeChange}. Analyze your build to ensure this was intentional.`;
  }

  return `
${warningData}
${'```'}
${table}
${'```'}
${'```'}
${metricTable}
${'```'}
${'```'}
${formatAsTable([
    ['Metric', 'Size:', '% of all:'],
    ['Cache invalidated', formatFileSize(getTotalSize(uncachedFiles)), relativeSizePercents(getTotalSize(uncachedFiles), getTotalSize(current))],
    ['Cache preserved', formatFileSize(getTotalSize(cachedFiles)), relativeSizePercents(getTotalSize(cachedFiles), getTotalSize(current))],
])}
${'```'}
`;
}

function filterFiles(data: FileInfo[] | null | undefined): FileInfo[] {
  const extensions = ['.js', '.css', '.js.gz', '.css.gz'];

  return (data || []).filter(item => extensions.some(ext => item.file.endsWith(ext)));
}

async function getMasterFileDataForRevision(revision: string): Promise<{
  revision: string | null,
  data: FileInfo[] | null,
}> {
  try {
    const data = await readCommitFileInfo(revision);

    return { revision, data };
  } catch (e) {
    logger.log(`Failed to load master data for revision ${revision}`);

    return { revision, data: null };
  }
}

async function getMasterFileData(masterRevisions: string[]): Promise<{
  revision: string | null,
  data: FileInfo[] | null,
}> {
  const data = await Promise.all(masterRevisions.map(getMasterFileDataForRevision));

  return data.reduce((prev, cur) => prev.data ? prev : cur, { revision: null, data: null });
}

export async function composeBuildBoardMessage(): Promise<string> {
  const currentGitHash = await getGitRevision();
  const currentGitBranch = await getGitBranch();
  const currentFileData = await readCommitFileInfo(currentGitHash);

  let masterRevisions: string[] | null = null;
  try {
    masterRevisions = await getLastMasterRevisions(5, currentGitHash);
  } catch (e) {
    logger.warn(`Could not get master revisions`);
    logger.warn(e);
  }
  const { revision: usedMasterRevision = null, data: masterFileData = null } = masterRevisions
    ? await getMasterFileData(masterRevisions)
    : {};

  const tableDescription = masterFileData
    ? `Below table uses data from \`master\` branch revision \`${usedMasterRevision}\``
    : masterRevisions
      ? `Could not find master data to make comparisons with (checked revisions ${masterRevisions.map(r => `\`${r}\``).join(', ')}). Are master builds working correctly?`
      : `Could not get any master revisions from bamboo :( Check the build output for hints on how to fix it`;

  return `#### Notes

**Your changes will be automatically released to production when you merge this PR.**
You can [preview your changes on the branch preview service](${branchPreviewLink(currentGitHash)}) to check before merging.

#### File sizes
${tableDescription}
${formatFileData(filterFiles(currentFileData), filterFiles(masterFileData))}

#### Useful links ✨
* 🖼 [Branch preview](${branchPreviewLink(currentGitHash)})
* 🖼 [Branch preview: site admin](${siteAdminBranchPreviewLink(currentGitHash)})
* 📕 [Storybooks](${storybooksLink(currentGitHash)})
* 📈 [Coverage](${coverageLink(currentGitHash)})
* 📖 [Playbooks](${playbooksLink(currentGitHash)})
* 🎛 [Webpack analyzer](${webpackAnalyzeLink(currentGitHash)})
* 🔒 [SourceClear](${sourceClearLink(currentGitBranch)})
`;
}
