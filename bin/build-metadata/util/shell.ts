import { exec } from 'child_process';

import { logger } from './logger';

export async function runShellScript(command: string): Promise<string> {
  return new Promise<string>((resolve, reject) => {
    logger.log(command);
    const s = exec(command, (error, stdout) => {
      if (error) {
        reject(error);

        return;
      }

      resolve(stdout);
    });

    s.stdout.on('data', logger.log);
    s.stderr.on('data', logger.error);
  });
}
