import { runShellScript } from './shell';

export async function getGitRevision(): Promise<string> {
  return (await runShellScript('git rev-parse HEAD')).trim().substr(0, 7);
}

export async function isOnMaster(): Promise<boolean> {
  const gitRev = await getGitRevision();
  const [lastMasterRev] = await getLastMasterRevisions(1, gitRev);

  return gitRev === lastMasterRev;
}

export async function getGitBranch(): Promise<string> {
  return (await runShellScript(`git rev-parse --abbrev-ref HEAD`)).trim();
}

export async function getLastMasterRevisions(count: number, forkRevision: string): Promise<string[]> {
  const forkPoint = (await runShellScript(`git merge-base refs/heads/master ${forkRevision}`)).trim();

  const longHashes = (await runShellScript(`git log -n ${count} ${forkPoint} --pretty=format:%H`)).split('\n');

  return longHashes.map(h => h.trim().substr(0, 7));
}
