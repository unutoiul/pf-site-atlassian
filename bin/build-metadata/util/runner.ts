import { logger } from '../util/logger';

export function runAsyncTask(task: () => Promise<any>) {
  task().catch(e => {
    logger.error(e);
    process.exit(1);
  });
}
