import * as fs from 'fs';
import * as https from 'https';

import { getGitRevision } from './git';
import { logger } from './logger';
import { runShellScript } from './shell';

export interface FileInfo {
  file: string;
  originalFileName: string;
  sizeInBytes: number;
}

async function uploadFile(url: string, filePath: string): Promise<void> {
  // it's just easier to do this with CURL :)
  try {
    await runShellScript(`curl -X POST -H 'authorization: Token pf-site-admin-ui' -T "${filePath}" "${url}"`);
  } catch (error) {
    logger.error(error);
  }
}

async function httpsGet(url: string): Promise<string> {
  return new Promise<string>((resolve, reject) => {
    https.get(url, (response) => {
      const { statusCode } = response;
      let error;

      if (statusCode !== 200) {
        error = new Error(`Request failed with status code ${statusCode} (getting ${url})`);
      }

      if (error) {
        // consume response data to free up memory
        response.resume();
        reject(error);

        return;
      }

      const rawData: string[] = [];
      response.on('data', (chunk) => rawData.push(chunk.toString()));
      response.on('end', () => resolve(rawData.join('')));
    });
  });
}

export async function uploadDir(dirPath: string, statlasSuffix: string, revision?: string): Promise<void> {
  const pathParts = dirPath.split('/');

  const tarContext = pathParts.slice(0, pathParts.length - 1);
  const tarDirName = pathParts[pathParts.length - 1];
  const tempFileName = `${tarDirName}-${new Date().getTime()}.tar`;

  await runShellScript(`tar -C "${tarContext.join('/')}" -zcvf "${tempFileName}" "${tarDirName}"`);

  const gitRevision = revision || await getGitRevision();
  const url = `https://statlas.prod.atl-paas.net/pf-site-admin-ui-${statlasSuffix ? statlasSuffix + '-' : ''}${gitRevision}`;

  await uploadFile(url, tempFileName);

  fs.unlinkSync(tempFileName);
}

export async function writeCommitFileInfo(data: FileInfo[]): Promise<void> {
  logger.log(`Uploading file info data to statlas...`);
  if (!fs.existsSync('build/data')) {
    fs.mkdirSync('build/data');
  }

  fs.writeFileSync('build/data/file-info.json', JSON.stringify(data), { encoding: 'UTF-8' });

  uploadDir('build/data', 'build-data');
}

export async function readCommitFileInfo(gitHash: string): Promise<FileInfo[]> {
  logger.log(`Fetching file info data from statlas; commit hash: ${gitHash}`);
  const response = await httpsGet(`https://statlas.prod.atl-paas.net/pf-site-admin-ui-build-data-${gitHash}/data/file-info.json`);
  logger.log(`File info data fetched from statlas`);

  return JSON.parse(response);
}
