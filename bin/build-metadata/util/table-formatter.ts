function padRight(value: string, padChar: string, length: number): string {
  return value + (new Array(length - value.length + 1).join(padChar));
}

function padLeft(value: string, padChar: string, length: number): string {
  return (new Array(length - value.length + 1).join(padChar)) + value;
}

function getColumns(rows: string[][]): string[][] {
  const columns: string[][] = [];
  for (const row of rows) {
    for (let j = 0; j < row.length; j++) {
      if (!columns[j]) {
        columns[j] = [];
      }

      columns[j].push(row[j]);
    }
  }

  return columns;
}

function getMaxColumnLength(column: string[]): number {
  return column.map(cell => cell.trim().length).reduce((p, c) => Math.max(p, c), -Infinity);
}

function formatCell(cell: string, alignment: 'left' | 'right', length: number, padChar: string) {
  return alignment === 'left'
    ? padRight(cell.trim(), padChar, length)
    : padLeft(cell.trim(), padChar, length);
}

function formatBoundRow(rowLength: number, cellLengths: number[], boxChars: { left: string, middle: string, right: string, padding: string }) {
  const rowStart = `${boxChars.left}${boxChars.padding}`;
  const cellJoiner = `${boxChars.padding}${boxChars.middle}${boxChars.padding}`;
  const rowEnd = `${boxChars.padding}${boxChars.right}`;

  return `${rowStart}${Array(rowLength).fill('').map((cell, i) => formatCell(cell, 'left', cellLengths[i], boxChars.padding)).join(cellJoiner)}${rowEnd}`;
}

function formatRow(row: string[], cellLengths: number[], char = ' ', alignment: Array<'left' | 'right'>) {
  return `│ ${row.map((cell, i) => formatCell(cell, alignment[i], cellLengths[i], char)).join(' │ ')} │`;
}

function getCellAlignment(heading: string[]): {
  alignment: Array<'left' | 'right'>,
  formattedHeading: string[],
} {
  return heading.map<{ alignment: 'left' | 'right', text: string }>(h => ({
    alignment: h.endsWith(':') ? 'right' : 'left',
    text: h.trim().replace(/:$/, ''),
  })).reduce((prev, cur) => ({
    alignment: [...prev.alignment, cur.alignment],
    formattedHeading: [...prev.formattedHeading, cur.text],
  }), { alignment: [], formattedHeading: [] });
}

export function formatAsTable(rows: string[][]) {
  const [heading, ...content] = rows;
  const { alignment, formattedHeading } = getCellAlignment(heading);

  const rowLength = heading.length;
  const maxCellLengths = getColumns([formattedHeading, ...content]).map(getMaxColumnLength);

  return `
${formatBoundRow(rowLength, maxCellLengths, { left: '┌', middle: '┬', right: '┐', padding: '─' })}
${formatRow(formattedHeading, maxCellLengths, ' ', alignment)}
${formatBoundRow(rowLength, maxCellLengths, { left: '├', middle: '┼', right: '┤', padding: '─' })}
${content.map(row => formatRow(row, maxCellLengths, ' ', alignment)).join('\n')}
${formatBoundRow(rowLength, maxCellLengths, { left: '└', middle: '┴', right: '┘', padding: '─' })}
`.trim();
}
