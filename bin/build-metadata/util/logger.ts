import { format } from 'util';

export interface Logger {
  log(message: any): void;
  warn(message: any): void;
  error(message: any): void;
}

function formatMessage(message: any): string | Buffer {
  if (typeof message !== 'string' && !(message instanceof Buffer)) {
    return format('%j', message);
  }

  return message;
}

class ProcessLogger implements Logger {
  public log = (message: any): void => {
    process.stdout.write(formatMessage(message));
  }
  public warn = (message: any): void => {
    this.log(message);
  }
  public error = (message: any): void => {
    process.stderr.write(formatMessage(message));
  }
}

export const logger: Logger = new ProcessLogger();
