// See /playbooks/BAMBOO_BUILDS.md (also available at https://statlas.prod.atl-paas.net/pf-site-admin-ui-playbooks-master/playbooks-gitbook/BAMBOO_BUILDS.html)
import { runAsyncTask } from '../util/runner';
import { runWebpackAnalyzerTask } from './webpack-analyzer';

if (process.argv.includes('--webpack-analyzer')) {
  runAsyncTask(runWebpackAnalyzerTask);
}
