import { isOnMaster } from '../util/git';
// import { runAsyncTask } from '../util/runner';
import { runShellScript } from '../util/shell';
import { uploadDir } from '../util/statlas';

export async function runWebpackAnalyzerTask() {
  await runShellScript('yarn build');
  await runShellScript('yarn analyze:build');

  await uploadDir('build/analyze-dist', 'analyze');
  if (isOnMaster()) {
    await uploadDir('build/analyze-dist', 'analyze', 'master');
  }
}

// runAsyncTask(runWebpackAnalyzerTask);
