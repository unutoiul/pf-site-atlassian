import * as fs from 'fs';
import * as path from 'path';

import { composeBuildBoardMessage } from '../util/build-board';
import { runAsyncTask } from '../util/runner';
import { runShellScript } from '../util/shell';
import { FileInfo, uploadDir, writeCommitFileInfo } from '../util/statlas';

function stripHash(file: string): string {
  const withoutGzExtension = file.replace('.gz', '');
  const hashEnd = withoutGzExtension.lastIndexOf('.');
  const hashStart = withoutGzExtension.lastIndexOf('.', hashEnd - 1);

  if (hashStart < 0 || hashEnd < 0) {
    // apparently the file is not hashed, so return the full filename
    return file;
  }

  return withoutGzExtension.substring(0, hashStart) + withoutGzExtension.substring(hashEnd);
}

function getFileInfo(): FileInfo[] {
  const directory = 'build/dist';
  const filePaths = fs.readdirSync(directory);

  return filePaths.map(file => ({
    file: stripHash(file),
    originalFileName: file,
    sizeInBytes: fs.lstatSync(path.join(directory, file)).size,
  })).sort((a, b) => b.sizeInBytes - a.sizeInBytes);
}

async function publishBuildBoard() {
  const filename: string = 'buildboard.md';

  fs.writeFileSync(filename, await composeBuildBoardMessage(), { encoding: 'UTF-8' });
  await runShellScript(`yarn buildboard --message "$(cat ${filename})"`);

  fs.unlinkSync(filename);
}

async function saveFileInfo() {
  await runShellScript(`gzip build/dist/*`);
  await writeCommitFileInfo(getFileInfo());
}

async function uploadCommitPreview() {
  await runShellScript(`PREVIEW_MODE=branch MICROS_ENV=stg-east ./bin/set-env.sh build/webpack-dist/index.html`);
  await uploadDir('build/webpack-dist', '');
}

export async function runPreviewTask() {
  await runShellScript(`yarn build`);

  await Promise.all([saveFileInfo(), uploadCommitPreview()]);
  await publishBuildBoard();
}

runAsyncTask(runPreviewTask);
