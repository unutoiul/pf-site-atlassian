import { runAsyncTask } from '../util/runner';
import { runShellScript } from '../util/shell';
import { uploadDir } from '../util/statlas';

// Upload coverage info to statlas
export async function runCoverageTask() {
  await runShellScript(`yarn test:coverage`);

  await uploadDir('build/coverage/html', 'coverage');
}

runAsyncTask(runCoverageTask);
