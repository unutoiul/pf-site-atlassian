import { isOnMaster } from '../util/git';
import { runAsyncTask } from '../util/runner';
import { runShellScript } from '../util/shell';
import { uploadDir } from '../util/statlas';

async function runStorybooksTask() {
  await runShellScript(`NODE_ENV=development yarn storybook:build`);

  await uploadDir('build/storybook', 'storybook');
  if (isOnMaster()) {
    await uploadDir('build/storybook', 'storybook', 'master');
  }
}

runAsyncTask(runStorybooksTask);
