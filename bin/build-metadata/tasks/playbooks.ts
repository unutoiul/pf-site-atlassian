import { isOnMaster } from '../util/git';
import { runAsyncTask } from '../util/runner';
import { runShellScript } from '../util/shell';
import { uploadDir } from '../util/statlas';

async function runPlaybooksTask() {
  await runShellScript(`yarn playbooks:install`);
  await runShellScript(`yarn playbooks:build`);

  await uploadDir('build/playbooks-gitbook', 'playbooks');
  if (isOnMaster()) {
    await uploadDir('build/playbooks-gitbook', 'playbooks', 'master');
  }
}

runAsyncTask(runPlaybooksTask);
