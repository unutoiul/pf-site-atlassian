import { runAsyncTask } from '../util/runner';
import { runShellScript } from '../util/shell';

async function runSourceClear(): Promise<void> {
  // Source Clear is picking up some Java options that are not supported which are defined in this global variable.
  // We set JAVA_TOOL_OPTIONS to "" it to make sure it doesn't happen and the script can run as usual
  runShellScript(`curl https://download.sourceclear.com/ci.sh | SRCCLR_API_TOKEN=${process.env.bamboo_SRCCLR_API_TOKEN} JAVA_TOOL_OPTIONS="" bash -s -- scan`);
}

runAsyncTask(runSourceClear);
