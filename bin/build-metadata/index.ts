import { spawn } from 'child_process';

import { logger } from './util/logger';

async function spawnAndLog(filename: string) {
  return new Promise<void>((resolve, reject) => {
    const proc = spawn('yarn', ['ts-node', `./bin/build-metadata/tasks/${filename}`]);

    proc.stdout.on('data', logger.log);
    proc.stderr.on('data', logger.error);

    proc.on('exit', (code) => {
      if (code === 0) {
        resolve();
      }

      reject();
    });
  });
}

async function doAllTasks() {
  await spawnAndLog('metadata.ts');
  await spawnAndLog('coverage.ts');
  await spawnAndLog('playbooks.ts');
  await spawnAndLog('storybooks.ts');
  await spawnAndLog('sourceclear.ts');
  await spawnAndLog('webpack-analyzer.ts');
}

doAllTasks()
  .catch(e => {
    logger.error(e);
    process.exit(1);
  });
