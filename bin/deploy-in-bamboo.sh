#!/usr/bin/env bash

set -xe

if [ -z "$MICROS_ENV" ]; then
  echo "MICROS_ENV not specified. Usage: MICROS_ENV=[ddev|stg-east|prod-east|prod-west] $0"
  exit 1
fi

cd "$(dirname "$0")/.."
export MICROS_TOKEN="${bamboo_pf_site_admin_ui_micros_token_password}"

yarn deploy
