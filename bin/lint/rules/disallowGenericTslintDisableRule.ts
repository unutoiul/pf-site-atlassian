import * as Lint from 'tslint';
import { forEachComment, getLineRanges, LineRange } from 'tsutils';
import * as ts from 'typescript';

/**
 * Custom TSlint Rule: disallow-generic-tslint-disable
 * Checks for:
 *   1. //tslint:disable comments that do not specify a rule to be disabled
 */
export class Rule extends Lint.Rules.AbstractRule {
  public apply(sourceFile: ts.SourceFile): Lint.RuleFailure[] {
    return this.applyWithFunction(sourceFile, walk, (this.getOptions().ruleArguments || [])[0] || {});
  }
}

interface JsonOptions {
  'skip-patterns': string[];
}

function walk(context: Lint.WalkContext<JsonOptions>): void {
  const disabledPatterns = (context.options['skip-patterns'] || []).map(p => new RegExp(p));
  if (disabledPatterns.some(p => p.test(context.sourceFile.fileName))) {
    return;
  }

  const disallowedDisableStatements = [
    'tslint:disable',
    'tslint:disable-line',
    'tslint:disable-next-line',
  ];
  const lines = getLineRanges(context.sourceFile);
  forEachComment(context.sourceFile, (fullText, comment) => {
    const start = comment.pos;
    const end = comment.end;
    const fullCommentText = fullText.substring(start, end);
    const commentContents = getCommentContents(fullCommentText, comment.kind).trim();

    if (disallowedDisableStatements.includes(commentContents)) {
      const violationMessagePosition = getViolationPlacement(fullText, commentContents, start, lines);
      if (violationMessagePosition < 0) {
        // sigh :( we didn't find where to place the error
        return;
      }

      const replacement = getReplacementSuggestion(commentContents, comment.kind);
      context.addFailureAt(
        violationMessagePosition, 1,
        `Generic rule disable statements are disallowed; use "${replacement}" instead`,
        new Lint.Replacement(start, end - start, replacement),
      );
    }
  });
}

function getViolationPlacement(fullText: string, commentContents: string, commentStart: number, lines: LineRange[]): number {
  switch (commentContents) {
    case 'tslint:disable': {
      return getDisableViolationPlacement(fullText, commentStart);
    }
    case 'tslint:disable-line': {
      return getDisableLineViolationPlacement(commentStart, lines);
    }
    case 'tslint:disable-next-line': {
      return getDisableNextLineViolationPlacement(commentStart, lines);
    }
    default: {
      throw new Error(`Found an unknown linting disable statement ("${commentContents}"). That's most likely a rule bug, so just fix it`);
    }
  }
}

function getDisableViolationPlacement(fullText: string, commentStart: number): number {
  if (commentStart > 0) {
    // add error to the character directly preceding the "disable" comment
    return commentStart - 1;
  }

  // someone tried to disable tslint right from the beginning; we'll try to overcome that by reporting an error at the end of file.
  // this can still fail if they write "tslint: disable" at the beginning of a file, withouth re-enabling it ever.
  // we try to find the "tslint:enable*" comment and add an error after it
  const enabledAt = fullText.indexOf('tslint:enable', commentStart);
  if (enabledAt < 0) {
    // oops, they disabled it for good, nothing we can do here
    return -1;
  }

  return enabledAt;
}

function getDisableLineViolationPlacement(commentStart: number, lines: LineRange[]): number {
  // report an error after the next line
  const nextLineIndex = lines.findIndex(l => l.pos >= commentStart);
  const afterNextLine = lines[nextLineIndex + 1];
  if (!afterNextLine) {
    // ouch, that was the last line
    // let's try previous line
    const reversedLines = [...lines].reverse();
    const prevLineIndex = reversedLines.findIndex(l => l.end < commentStart);
    const prevLine = reversedLines[prevLineIndex];
    if (prevLine) {
      return prevLine.pos;
    }

    return -1;
  }

  return afterNextLine.pos;
}

function getDisableNextLineViolationPlacement(commentStart: number, lines: LineRange[]): number {
  if (commentStart > 0) {
    // add error to the character directly preceding the "disable" comment
    return commentStart - 1;
  }

  // report an error on the line AFTER next line
  const currentLineIndex = lines.findIndex(l => l.pos >= commentStart);
  const afterNextLine = lines[currentLineIndex + 2];
  if (!afterNextLine) {
    // well, either the comment doesn't serve any purpose since it's on the last file line,
    // or there's no line after it; we can't report an error
    return -1;
  }

  return afterNextLine.pos;
}

function getReplacementSuggestion(comment: string, type: ts.CommentKind): string {
  const prefix = type === ts.SyntaxKind.SingleLineCommentTrivia ? '// ' : '/* ';
  const suffix = type === ts.SyntaxKind.SingleLineCommentTrivia ? '' : ' */';

  return `${prefix}${comment} <VIOLATED_RULE_NAME_1> <VIOLATED_RULE_NAME_2>${suffix}`;
}

function getCommentContents(text: string, type: ts.CommentKind): string {
  switch (type) {
    case ts.SyntaxKind.SingleLineCommentTrivia:
      // strip "//" part
      return text.substr(2);
    case ts.SyntaxKind.MultiLineCommentTrivia:
      // strip "/*" and "*/" parts
      return text.substr(0, text.length - 2).substr(2);
    default:
      throw new Error(`unsupported comment kind: ${type}`);
  }
}
