import * as Lint from 'tslint';
import * as ts from 'typescript';

/**
 * Rule to check that imports from common use path alias and not import them via relative paths.
 */
export class Rule extends Lint.Rules.AbstractRule {
  public apply(sourceFile: ts.SourceFile): Lint.RuleFailure[] {
    return this.applyWithWalker(new Walker(sourceFile, this.getOptions()));
  }
}

class Walker extends Lint.RuleWalker {
  protected visitImportDeclaration(node: ts.ImportDeclaration) {
    this.checkImportDeclaration(node);

    super.visitImportDeclaration(node);
  }

  private checkImportDeclaration(node: ts.ImportDeclaration) {
    if (!ts.isStringLiteral(node.moduleSpecifier)) {
      // Ignore grammar error
      return;
    }

    const source = removeQuotes(node.moduleSpecifier.text);
    if (source.includes('../common')) {
      const start = node.getStart() + node.getText().indexOf('\'') + 1;
      const end = node.getStart() + node.getText().indexOf('/common') + 7;
      const fix: Lint.Replacement = new Lint.Replacement(start, end - start, 'common');

      this.addFailureAtNode(node, `Use aliased imports for 'common', do not use relative path`, fix);
    }
  }
}

function removeQuotes(value: string): string {
  if (value.length > 1 && (value[0] === '\'' || value[0] === '"')) {
    value = value.substr(1, value.length - 2);
  }

  return value;
}
