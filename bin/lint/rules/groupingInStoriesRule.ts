import * as Lint from 'tslint';
import * as ts from 'typescript';

export class Rule extends Lint.Rules.AbstractRule {
  public apply(sourceFile: ts.SourceFile): Lint.RuleFailure[] {
    if (!sourceFile.fileName.endsWith('story.tsx')) {
      // only care about storybook files
      return [];
    }

    return this.applyWithWalker(new Walker(sourceFile, this.getOptions()));
  }
}

interface JsonOptions {
  'allow-separator-whitespace'?: boolean;
  'allow-prefixes'?: string[];
  'root-separator'?: string;
  'hierarchy-separator'?: string;
}

interface RuleOptions {
  allowSeparatorWhitespace: boolean;
  allowPrefixes: string[];
  rootSeparator: string;
  hierarchySeparator: string;
}

class Walker extends Lint.RuleWalker {
  private parsedOptions: RuleOptions;

  private static noWhitespaceMessage = (actualValue) => `Sorry, mate, we agreed to have no redundant whitespace here. Use "${actualValue.trim()}" instead of "${actualValue}"`;

  constructor(sourceFile: ts.SourceFile, options: Lint.IOptions) {
    super(sourceFile, options);

    const {
      'allow-separator-whitespace': allowSeparatorWhitespace = false,
      'allow-prefixes': allowPrefixes = [],
      'root-separator': rootSeparator = '|',
      'hierarchy-separator': hierarchySeparator = '/',
    } = options.ruleArguments[0] as JsonOptions;

    this.parsedOptions = {
      allowSeparatorWhitespace,
      allowPrefixes,
      rootSeparator,
      hierarchySeparator,
    };
  }

  protected visitCallExpression(node: ts.CallExpression) {
    if (ts.isIdentifier(node.expression) && node.expression.getText() === 'storiesOf') {
      this.processStoryOfNode(node);
    }

    super.visitCallExpression(node);
  }

  private processStoryOfNode(node: ts.CallExpression) {
    if (node.arguments.length < 1) {
      return;
    }

    const storyNameArg = node.arguments[0];
    if (storyNameArg === undefined) {
      return;
    }
    if (!isSimpleString(storyNameArg)) {
      return;
    }

    const storyName = getValue(storyNameArg);

    const [prefix, ...rest] = storyName.split(this.parsedOptions.rootSeparator);

    if (this.parsedOptions.allowSeparatorWhitespace === false) {
      const allParts = [
        prefix,
        ...rest.join(this.parsedOptions.rootSeparator).split(this.parsedOptions.hierarchySeparator),
      ];

      allParts.filter(part => part !== part.trim()).forEach(part => this.addFailureAtNode(storyNameArg, Walker.noWhitespaceMessage(part)));
    }

    if (!this.parsedOptions.allowPrefixes.includes(prefix.trim())) {
      const prefixes = this.parsedOptions.allowPrefixes.map(p => `"${p}"`).join(', ');
      this.addFailureAtNode(storyNameArg, `Expected to see one of predefined storybook prefixes (${prefixes}), but saw "${prefix}" instead`);
    }
  }
}

declare type SimpleTSString = ts.StringLiteral | ts.NoSubstitutionTemplateLiteral;
function isSimpleString(node: ts.Node | undefined): node is SimpleTSString {
  return node !== undefined && (ts.isStringLiteral(node) || ts.isNoSubstitutionTemplateLiteral(node));
}

function getValue(node: SimpleTSString): string {
  const valueWithQuotes = node.getText();

  return valueWithQuotes.substr(1, valueWithQuotes.length - 2);
}
