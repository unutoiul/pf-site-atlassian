import * as Lint from 'tslint';
import * as ts from 'typescript';

const FAILURE_STRING = "<react.fragment/>";

/**
 * Custom TSlint Rule: disallow-return-empty-react-fragment
 *
 * Checks for `return <React.Fragment />;`
 * Replaces it with `return null;`
 */
export class Rule extends Lint.Rules.AbstractRule {
  public apply(sourceFile: ts.SourceFile): Lint.RuleFailure[] {
    return this.applyWithWalker(new Walker(sourceFile, this.getOptions()));
  }
}

class Walker extends Lint.RuleWalker {
  protected visitReturnStatement(node: ts.ReturnStatement) {
    this.checkReturnStatement(node);
    super.visitReturnStatement(node);
  }

  private checkReturnStatement(node: ts.ReturnStatement) {
    if (node.getText().toLowerCase().replace(/\s/g,'').includes(FAILURE_STRING)) {
      this.addFailureAtNode(node, 'Returning an empty React Fragment has been banned. Return null instead.', this.fix(node));
    }
  }

  private fix(node: ts.ReturnStatement): Lint.Fix {
    return new Lint.Replacement(node.getStart(), node.getWidth(), 'return null;')
  }
}
