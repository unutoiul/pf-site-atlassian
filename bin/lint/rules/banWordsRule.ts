import * as Lint from 'tslint';
import * as ts from 'typescript';

interface JsonOptions {
  'words'?: string[];
}

interface RuleOptions {
  disallowedWords: string[];
}

export class Rule extends Lint.Rules.AbstractRule {
  public apply(sourceFile: ts.SourceFile): Lint.RuleFailure[] {
    return this.applyWithWalker(
      new Walker(sourceFile, this.getOptions()),
    );
  }
}

class Walker extends Lint.RuleWalker {
  private parsedOptions: RuleOptions;

  constructor(sourceFile: ts.SourceFile, options: Lint.IOptions) {
    super(sourceFile, options);

    const {
      'words': disallowedWords = [],
    } = options.ruleArguments[0] as JsonOptions;

    this.parsedOptions = {
      disallowedWords: disallowedWords.map(word => word.toLowerCase()),
    };
  }

  public visitSourceFile(node: ts.SourceFile) {
    const match = this.parsedOptions.disallowedWords.some(disallowed => {
      return node.fileName.toLowerCase().includes(disallowed);
    });

    if (match) {
      this.addFailure(this.createFailure(
        node.getStart(),
        node.getEnd(),
        'This file name has been banned. Please choose another one.',
      ));
    }

    super.visitSourceFile(node);
  }

  public visitIdentifier(node: ts.Identifier) {
    this.checkIdentifier(node);
    super.visitIdentifier(node);
  }

  public visitStringLiteral(node: ts.StringLiteral) {
    this.checkIdentifier(node);
    super.visitStringLiteral(node);
  }

  private checkIdentifier(node: ts.Identifier | ts.StringLiteral | ts.Node) {
    const match = this.parsedOptions.disallowedWords.some(disallowed => {
      return node.getText().toLowerCase().includes(disallowed);
    });

    if (!match) {
      return;
    }

    this.addFailure(this.createFailure(
      node.getStart(),
      node.getEnd(),
      `The code "${node.getText()}" contains a banned word. Please choose another one.`,
    ));
  }
}
