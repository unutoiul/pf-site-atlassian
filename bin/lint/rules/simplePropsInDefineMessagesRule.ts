import * as Lint from 'tslint';
import { collectVariableUsage, VariableInfo } from 'tsutils';
import * as ts from 'typescript';

export class Rule extends Lint.Rules.AbstractRule {
  public apply(sourceFile: ts.SourceFile): Lint.RuleFailure[] {
    return this.applyWithWalker(new Walker(sourceFile, this.getOptions()));
  }
}

class Walker extends Lint.RuleWalker {
  private _variableUsage: Map<ts.Identifier, VariableInfo> | undefined;

  protected visitCallExpression(node: ts.CallExpression) {
    if (ts.isIdentifier(node.expression) && node.expression.getText() === 'defineMessages') {
      this.processDefineMessagesNode(node);
    }

    super.visitCallExpression(node);
  }

  private get variableUsage(): Map<ts.Identifier, VariableInfo> {
    return this._variableUsage || (this._variableUsage = collectVariableUsage(this.getSourceFile()));
  }

  private processDefineMessagesNode(node: ts.CallExpression) {
    if (node.arguments.length !== 1) {
      return;
    }

    const messages = node.arguments[0];
    if (!ts.isObjectLiteralExpression(messages)) {
      return this.addFailureAtNode(messages, `"defineMessages" must be called with object literal`);
    }

    messages.properties.forEach(this.checkSingleMessageDefinition);
  }

  private checkSingleMessageDefinition = (prop: ts.ObjectLiteralElementLike): void => {
    if (!ts.isPropertyAssignment(prop)) {
      return this.addFailureAtNode(prop, `"defineMessages" must specify simple properties`);
    }

    this.checkProp(prop, 'id');
    this.checkProp(prop, 'defaultMessage');
    this.checkProp(prop, 'description', true);
  }

  private checkProp(prop: ts.PropertyAssignment, propName: string, optional: boolean = false) {
    if (!ts.isObjectLiteralExpression(prop.initializer)) {
      return this.addFailureAtNode(prop.initializer, `"defineMessages" properties should be defined as simple objects`);
    }

    const messagePropertyNode: ts.ObjectLiteralElementLike | undefined = prop.initializer.properties.find(p => p.name !== undefined && p.name.getText() === propName);
    if (!messagePropertyNode) {
      if (!optional) {
        this.addFailureAtNode(prop.initializer, `"${propName}" property must be defined for each message`);
      }

      return;
    }

    if (!ts.isPropertyAssignment(messagePropertyNode)) {
      return this.addFailureAtNode(messagePropertyNode, `"${propName}" property must be defined for each message`);
    }

    if (!isSimpleString(messagePropertyNode.initializer)) {
      return this.addFailureAtNode(messagePropertyNode.initializer, `"${propName}" property must be set to plain string`, this.findFix(messagePropertyNode.initializer));
    }

    // everything is fine
    return;
  }

  private findFix(messageIdNodeInitializer: ts.Expression): Lint.Replacement | undefined {
    if (ts.isTemplateExpression(messageIdNodeInitializer)) {
      return this.findFixForTemplateExpression(messageIdNodeInitializer);
    }
    if (ts.isBinaryExpression(messageIdNodeInitializer)) {
      return this.findFixForBinaryExpression(messageIdNodeInitializer);
    }

    return undefined;
  }

  private findFixForTemplateExpression(messageIdNodeInitializer: ts.TemplateExpression): Lint.Replacement | undefined {
    const [templateSpan, ...rest] = messageIdNodeInitializer.templateSpans;
    if (templateSpan === undefined || rest.length > 0) {
      // either no spans (should be impossible), or more than one.
      return undefined;
    }

    if (!ts.isIdentifier(templateSpan.expression)) {
      // weird
      return undefined;
    }

    const identifierValue = this.findIdentifierValue(templateSpan.expression);
    if (identifierValue === undefined) {
      return undefined;
    }

    const parent = templateSpan.parent;
    if (parent === undefined) {
      // probably nonsense
      return undefined;
    }

    const text = parent.getText();
    const interpolationStart = text.indexOf('${'); // tslint:disable-line no-invalid-template-strings
    const interpolationEnd = text.indexOf('}', interpolationStart) + 1;

    const replacementStart = parent.getStart() + interpolationStart;
    const replacementEnd = parent.getStart() + interpolationEnd;

    return new Lint.Replacement(replacementStart, replacementEnd - replacementStart, identifierValue);
  }

  private findFixForBinaryExpression(node: ts.BinaryExpression): Lint.Replacement | undefined {
    if (ts.isIdentifier(node.left)) {
      if (!isSimpleString(node.right)) {
        // TODO: hm... would need investigation
        return undefined;
      }

      const identifierValue = this.findIdentifierValue(node.left);
      if (identifierValue === undefined) {
        return undefined;
      }
      const rightOperandValue = getValue(node.right);
      const quote = getQuotes(node.right);

      return nodeReplacement(node, `${quote}${identifierValue + rightOperandValue}${quote}`);
    } else if (ts.isIdentifier(node.right)) {
      if (!isSimpleString(node.left)) {
        // TODO: hm... would need investigation
        return undefined;
      }

      const identifierValue = this.findIdentifierValue(node.right);
      if (identifierValue === undefined) {
        return undefined;
      }
      const leftOperandValue = getValue(node.left);
      const quote = getQuotes(node.left);

      return nodeReplacement(node, `${quote}${leftOperandValue + identifierValue}${quote}`);
    }

    // TODO: I don't know how we'd get here
    return undefined;
  }

  private findIdentifierValue = (identifier: ts.Identifier): string | undefined => {
    const variableInfo = this.getVariableInfo(identifier);
    if (variableInfo === undefined) {
      // TODO: identifier is probably declared elsewhere because we can't find variable info, so can't really fix the thing
      return undefined;
    }

    return getVariableValue(variableInfo);
  }

  private getVariableInfo(identifier: ts.Identifier): VariableInfo | undefined {
    for (const [id, value] of Array.from(this.variableUsage.entries())) {
      if (id.getText() === identifier.getText()) {
        return value;
      }
    }

    return undefined;
  }
}

function nodeReplacement(node: ts.Node, replacement: string): Lint.Replacement {
  const start = node.getStart();
  const end = node.getEnd();

  return new Lint.Replacement(start, end - start, replacement);
}

declare type SimpleTSString = ts.StringLiteral | ts.NoSubstitutionTemplateLiteral;
function isSimpleString(node: ts.Node | undefined): node is SimpleTSString {
  return node !== undefined && (ts.isStringLiteral(node) || ts.isNoSubstitutionTemplateLiteral(node));
}

function getValue(node: SimpleTSString): string {
  const valueWithQuotes = node.getText();

  return valueWithQuotes.substr(1, valueWithQuotes.length - 2);
}

function getQuotes(node: SimpleTSString): string {
  return node.getText()[0];
}

function getVariableValue(variableInfo: VariableInfo): string | undefined {
  const [varDeclaration, ...rest] = variableInfo.declarations.map(d => d.parent).filter(ts.isVariableDeclaration);
  if (!varDeclaration || rest.length > 0) {
    // variable is either declared elsewhere, or re-declared. It's a lot of branching, so I'm not doing that
    return undefined;
  }

  if (!isSimpleString(varDeclaration.initializer)) {
    // it won't be easy to get the value for that, so I'm not doing it
    return undefined;
  }

  return getValue(varDeclaration.initializer);
}
