
import * as Lint from 'tslint';
import {
  collectVariableUsage,
  isUnionOrIntersectionType,
} from 'tsutils';
import * as ts from 'typescript';
/**
 * Custom TSlint Rule: check-fetch-result
 * Checks for:
 *   1. 'await' used with fetch calls
 *   2. fetch result is checked using 'response.ok'
 */
export class Rule extends Lint.Rules.TypedRule {
  public static AWAIT_FETCH_RESULTS_MESSAGE = 'Use `await` with `fetch`: `const response = await fetch(...);`';
  public static ASSIGN_FETCH_RESULTS_TO_VARIABLE_MESSAGE = 'Assign "fetch" to a variable for success checks: `const response = await fetch(...);`';
  public static CHECK_STATUS_MESSAGE_FACTORY = variableName => `"fetch" result should be checked. Use "${variableName}.ok" or "${variableName}.status"`;

  public applyWithProgram(sourceFile: ts.SourceFile, program: ts.Program): Lint.RuleFailure[] {
    return this.applyWithFunction(sourceFile, walk, null, program.getTypeChecker());
  }
}

function walk(ctx: Lint.WalkContext<null>, tc: ts.TypeChecker) {
  const variableUsage = collectVariableUsage(ctx.sourceFile);

  variableUsage.forEach((value, key) => {
    if (!isResponse(tc.getTypeAtLocation(key))) {
      return;
    }

    const propertyAccess = value!.uses
      .map(u => u.location.parent!)
      .map(u => ts.isPropertyAccessExpression(u) ? u : undefined)
      .filter(u => u !== undefined);

    if (!propertyAccess.some(p => ['ok', 'status'].includes(p!.name.getText()))) {
      ctx.addFailureAtNode(key, Rule.CHECK_STATUS_MESSAGE_FACTORY(key.text));
    }
  });

  return ts.forEachChild(ctx.sourceFile, createNodeWalker(ctx));
}

function createNodeWalker(ctx: Lint.WalkContext<null>) {
  return function callback(node: ts.Node): void {
    function continueWalk(): void {
      return ts.forEachChild(node, callback);
    }

    if (!isFetchCall(node)) {
      return continueWalk();
    }

    const parent = node.parent;
    if (!parent) {
      // just an expression? Looks like it doesn't make any sense, so just skip it
      return continueWalk();
    }

    if (!ts.isAwaitExpression(parent)) {
      ctx.addFailureAtNode(node, Rule.AWAIT_FETCH_RESULTS_MESSAGE);

      return continueWalk();
    }

    const grandParent = parent.parent;
    if (!grandParent ||
      !(ts.isVariableDeclaration(grandParent) ||
        ts.isReturnStatement(grandParent) ||
        (grandParent.parent && ts.isVariableDeclaration(grandParent.parent)))) {
      ctx.addFailureAtNode(node, Rule.ASSIGN_FETCH_RESULTS_TO_VARIABLE_MESSAGE);
    }

    // actual "ok" and "status" prop checks will be verified by variable usage logic

    return continueWalk();
  };
}

function isFetchCall(node: ts.Node): node is ts.CallExpression {
  return ts.isCallExpression(node) && node.expression.getText() === 'fetch';
}

function isResponse(type: ts.Type): boolean {
  if (type && type.symbol && type.symbol.name === 'Response') {
    return true;
  }

  if (isUnionOrIntersectionType(type)) {
    return type.types.some(isResponse);
  }

  const bases = type.getBaseTypes();

  return !!bases && bases.some(isResponse);
}
