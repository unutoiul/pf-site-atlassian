const { dirname, basename, join } = require('path');

const usedNames = {};

module.exports = (story, { sanitizePath }) => {
  const storyFile = story.parameters.fileName;

  const storyName = basename(storyFile);
  const storyDir = dirname(storyFile);

  const name = join(
    storyDir.replace(/\.\/src/, '.shots'),
    storyName.split('.')[0],
    `${sanitizePath(story.story)}.png`
  );

  if (usedNames[name]) {
    console.error(name, 'is already used by another story');
    console.error('this story', story);
    console.error('old story', usedNames[name]);
    throw new Error('naming slash');
  }
  usedNames[name] = story;

  return name;
};
