#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

echo "data will be saved to"
echo "${DIR}/../../build-output/shots"

./node_modules/whales-story-shots/install.sh

rm -Rf "${DIR}/../../build/shots/"

docker run --shm-size 1G --rm \
-e MODE="story" \
-v "${DIR}/../../build/shots":/build-output \
-v "${DIR}/../../build/shots/meta":/meta-output \
whales-story-shots:latest node /whales-story-shots/shoot.js
