#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

echo "data will be saved to"
echo "${DIR}/../../build-output/shots"

docker run --shm-size 1G --rm \
-v "${DIR}/../..":/images \
-v"${DIR}/pathRule":/whales-story-shots/compare/pathRule \
-v "${DIR}/../../build/shots":/build-output \
-v "${DIR}/../../build/shots/meta":/meta-output \
whales-story-shots:latest node /whales-story-shots/compare.js
