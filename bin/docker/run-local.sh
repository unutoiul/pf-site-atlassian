#!/usr/bin/env bash

REPO_ROOT_DIR="$(cd -P "$(dirname "$0")/../..";pwd)";

docker run --rm -it \
  --entrypoint '/bin/bash' \
  -w /pf-site-admin-ui \
  -v $REPO_ROOT_DIR:/pf-site-admin-ui \
  -v ~/.npmrc:/root/.npmrc \
  pf-site-admin-ui-builder
