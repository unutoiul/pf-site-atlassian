# Use multi-stage docker build to cache yarn dependencies. This will prevent creds from being accessible by referencing the layer directly
FROM docker.atl-paas.net/sox/buildeng/agent-baseagent AS yarnCache

# Install Yarn
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get install -y yarn

RUN mkdir /tmp/project-dep-cache
RUN mkdir /tmp/yarn-cache
RUN yarn config set cache-folder /tmp/yarn-cache
COPY .npmrc /tmp/project-dep-cache/.npmrc
COPY package.json /tmp/project-dep-cache/package.json
COPY yarn.lock /tmp/project-dep-cache/yarn.lock
RUN cd /tmp/project-dep-cache && yarn install --frozen-lockfile

FROM docker.atl-paas.net/sox/buildeng/agent-baseagent

# Install Yarn
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get install -y yarn

# Google Chrome
RUN curl -sS https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN echo "deb http://dl.google.com/linux/chrome/deb/ stable main" | tee /etc/apt/sources.list.d/google-chrome.list
RUN apt-get update && apt-get install -y google-chrome-stable

# Puppeteer
## https://github.com/GoogleChrome/puppeteer/blob/master/docs/troubleshooting.md#running-puppeteer-in-docker
## See https://crbug.com/795759
RUN apt-get install -yq libgconf-2-4

# Cypress
RUN apt-get install -y xvfb libgtk2.0-0 libnotify-dev libgconf-2-4 libnss3 libxss1 libasound2

## It's a good idea to use dumb-init to help prevent zombie chrome processes.
ADD https://github.com/Yelp/dumb-init/releases/download/v1.2.0/dumb-init_1.2.0_amd64 /usr/local/bin/dumb-init
RUN chmod +x /usr/local/bin/dumb-init

# get linux utils (specifically, the "column" command)
RUN apt-get install bsdmainutils

# micros-cli
RUN curl -O https://statlas.atlassian.io/micros-stable/linux/micros.tar.gz
RUN mv micros.tar.gz micros.tar
RUN tar xvf micros.tar
RUN mv micros /bin/
RUN rm micros.tar

COPY --from=yarnCache /tmp/yarn-cache /tmp/yarn-cache
RUN yarn config set cache-folder /tmp/yarn-cache
