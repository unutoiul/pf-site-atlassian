#!/usr/bin/env bash

CURRENT_DIR=`dirname $0`
REPO_ROOT_DIR="$(cd -P "$(dirname "$0")/../..";pwd)";

cp ~/.npmrc $CURRENT_DIR/.npmrc
cp $REPO_ROOT_DIR/package.json $CURRENT_DIR/package.json
cp $REPO_ROOT_DIR/yarn.lock $CURRENT_DIR/yarn.lock
docker build -t pf-site-admin-ui-builder $@ $CURRENT_DIR
rm $CURRENT_DIR/.npmrc
rm $CURRENT_DIR/package.json
rm $CURRENT_DIR/yarn.lock
