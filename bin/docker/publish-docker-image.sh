#!/usr/bin/env bash

SOX_PREFIX=''
if [ "$1" == "--sox" ]; then
  SOX_PREFIX="sox/"
fi

docker tag pf-site-admin-ui-builder "docker.atl-paas.net/${SOX_PREFIX}cloud-admin/pf-site-admin-ui-builder"
docker push "docker.atl-paas.net/${SOX_PREFIX}cloud-admin/pf-site-admin-ui-builder"
