#!/usr/bin/env bash
fswatch -0 -o --latency=1 -e ".*" -i "\\.graphql$" src | xargs -0 -n 1 yarn graphql