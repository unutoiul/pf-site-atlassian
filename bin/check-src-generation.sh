#! /usr/bin/env bash

yarn graphql
if [ $? -ne 0 ]; then
    echo "'yarn graphql' command exited with non-zero code."
    exit 2
fi
yarn i18n
if [ $? -ne 0 ]; then
    echo "'yarn i18n' command exited with non-zero code."
    exit 3
fi
# buffer added in order to get the most recent status of git after the yarn commands
git status

if git diff-index --quiet HEAD --; then
    echo "No changes detected."
    exit 0
else
    echo ""
    echo ""
    echo "!!!ACTION REQUIRED!!!"
    echo ""
    echo "Please run yarn graphql && yarn i18n and then verify and commit changes."
    echo ""
    echo ""
    exit 1
fi
