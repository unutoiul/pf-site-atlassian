#!/usr/bin/env bash
set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

lang="en"
bux_target="${DIR}/data/strings.bux.json"
admin_target="${DIR}/data/strings.admin.json"

printf "Fetching source strings fox BUX"
curl -L --user "api:${TRANSIFEX_TOKEN}" \
  -X GET "https://www.transifex.com/api/2/project/${TRANSIFEX_PROJECT}/resource/${BUX_RESOURCE}/translation/${lang}/strings/" \
  -o "${bux_target}"

printf "Fetching source strings fox Site Admin"
curl -L --user "api:${TRANSIFEX_TOKEN}" \
  -X GET "https://www.transifex.com/api/2/project/${TRANSIFEX_PROJECT}/resource/${SITE_ADMIN_RESOURCE}/translation/${lang}/strings/" \
  -o "${admin_target}"



