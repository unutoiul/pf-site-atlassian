import * as fs from 'fs';
import { join } from 'path';
import { limited } from 'plimited';
import * as spawn from 'projector-spawn';

/* tslint:disable:no-console no-var-requires no-require-imports */

const context = require('./data/screenshots');
const stories = require(process.env.WHALES_BUILD_DIR + '/meta/shots');
const compare = require(process.env.WHALES_BUILD_DIR + '/meta/compare');

const { TRANSIFEX_TOKEN, TRANSIFEX_ORGANIZATION, TRANSIFEX_PROJECT } = process.env;

const runLimited = limited(8);

console.log('having ', context.length, ' uploaded images');

const deleteScreenshots = async (contextToDelete) => (
  Promise.all(
    contextToDelete.map(({ id, name }) => (
        runLimited(async () => {
          console.log('deleting', id, name);
          await spawn('curl',
            [
              '--user', `api:${TRANSIFEX_TOKEN}`,
              '-XDELETE',
              `https://api.transifex.com/organizations/${TRANSIFEX_ORGANIZATION}/projects/${TRANSIFEX_PROJECT}/context/screenshots/${id}/`,
            ]);
        })
      ),
    ),
  )
);

const DELETE_IMAGES = false; // USE ONLY FOR DEV!!!

const found = new Set();

if (DELETE_IMAGES) {// you may delete all the images uploaded
  deleteScreenshots(context).then(() => console.log('deleted'));
} else {
  Object
    .keys(compare)
    .forEach(async (baseImage) => {
      const destImage = compare[baseImage].dest;

      const story = stories.find(s => s.imageName === baseImage);

      const fileName = join(process.env.WHALES_COMPARE_DIR!, destImage);

      const fileKey = `${story.kind}:${story.story}`;

      const uploaded = context.find(({ name }) => name === fileKey);
      found.add(uploaded);

      await runLimited(async () => {
        try {
          const stat = fs.statSync(fileName);

          if (!uploaded) {
            console.log(`+ ${destImage}/${fileKey} is a new file`);

            const result = await spawn('curl',
              [
                '--user', `api:${TRANSIFEX_TOKEN}`,
                '-XPOST',
                `https://usermedia.transifex.com/organizations/${TRANSIFEX_ORGANIZATION}/projects/${TRANSIFEX_PROJECT}/context/screenshots/`,
                '-F',
                `screenshot=@"${fileName}"`,
              ]);
            try {
              const response = JSON.parse(result.stdout);
              await spawn('curl',
                [
                  '--user', `api:${TRANSIFEX_TOKEN}`,
                  '-XPATCH',
                  `https://usermedia.transifex.com/organizations/${TRANSIFEX_ORGANIZATION}/projects/${TRANSIFEX_PROJECT}/context/screenshots/${response.id}`,
                  '-F',
                  `name=${fileKey}`,
                ]);
              console.log(`= ${destImage} uploaded as ${response.id}`);
            } catch (e) {
              console.log(result.stdout);
              console.error(e);
            }

          } else if (+(new Date(uploaded.modified)) < (+(new Date(stat.mtime)))) {
            console.log(`* ${destImage} should be updated`);

            await spawn('curl',
              [
                '--user', `api:${TRANSIFEX_TOKEN}`,
                '-XPATCH',
                `https://usermedia.transifex.com/organizations/${TRANSIFEX_ORGANIZATION}/projects/${TRANSIFEX_PROJECT}/context/screenshots/${uploaded.id}`,
                '-F',
                `screenshot=@"${fileName}"`,
              ]);
          } else {
            console.log(`- ${destImage} is up to date`);
          }
        } catch (e) {
          console.error(e);
        }
      });
    });
  deleteScreenshots(context.filter(screen => !found.has(screen))).then(() => console.log('deleted'));
}
