import * as fs from 'fs';
import { join } from 'path';
import * as spawn from 'projector-spawn';

const { TRANSIFEX_TOKEN, TRANSIFEX_ORGANIZATION, TRANSIFEX_PROJECT } = process.env;

/* tslint:disable:no-console */

async function pull(url, result: any[] = []) {
  console.log(`fetching ${url}`);
  const { stdout } = await spawn('curl',
    [
      '--user', `api:${TRANSIFEX_TOKEN}`,
      '-i',
      '-XGET',
      url,
    ]);

  const [rawHeaders, body] = stdout.split('\r\n\r\n');
  const headers = rawHeaders.split('\r\n').reduce((acc, header) => {
    const [name, ...values] = header.split(':');
    const value = values.join(':');

    if (name === 'Link') {
      const parts = value.split(',');
      const next = parts.find(str => str.indexOf('rel="next"') >= 0);
      if (next) {
        return [...acc, next];
      }
    }

    return acc;
  }, []);

  const response: any[] = JSON.parse(body);
  result.push(...response);

  if (headers.length) {
    const link = headers[0].match(/<(.*)>/i)[1];
    await pull(link, result);
  }

  return result;
}

pull(`https://api.transifex.com/organizations/${TRANSIFEX_ORGANIZATION}/projects/${TRANSIFEX_PROJECT}/context/screenshots/`)
  .then(data => {
    fs.writeFileSync(
      join(__dirname, './data/screenshots.json'),
      JSON.stringify(data, null, '\t'),
    );
  });
