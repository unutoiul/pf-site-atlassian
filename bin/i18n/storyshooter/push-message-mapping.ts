import { exec } from 'child_process';
import { limited } from 'plimited';

/* tslint:disable:no-console no-var-requires no-require-imports */

const messageMap = require(process.env.WHALES_BUILD_DIR + '/meta/map.json');
const stories = require(process.env.WHALES_BUILD_DIR + '/meta/shots.json');
const compare = require(process.env.WHALES_BUILD_DIR + '/meta/compare.json');
const context = require('./data/screenshots');
const translationsBUX = require('./data/strings.bux');
const translationsAdmin = require('./data/strings.admin');

const {
  TRANSIFEX_TOKEN,
  TRANSIFEX_ORGANIZATION,
  TRANSIFEX_PROJECT,
  SITE_ADMIN_RESOURCE,
  BUX_RESOURCE,
} = process.env;

const translations = [
  ...translationsBUX.map(x => ({ ...x, resource_slug: BUX_RESOURCE })),
  ...translationsAdmin.map(x => ({ ...x, resource_slug: SITE_ADMIN_RESOURCE })),
];

const runLimited = limited(16);

const findSourceString = (key: string) => {
  const record = translations.find(str => str.key === key);
  if (!record) {
    console.error('used key ', key, ' does not exists in downloaded strings');

    return {};
  }

  return {
    source_string_hash: record.string_hash,
    resource_slug: record.resource_slug,
  };
};

const pExec = cmd => new Promise(resolve => (
  exec(cmd, {}, resolve as any)
));

Object
  .keys(compare)
  .forEach(async (baseImage) => {
    const story = stories.find(s => s.imageName === baseImage);
    const fileKey = `${story.kind}:${story.story}`;
    const shot = context.find(({ name }) => name === fileKey);
    const messages = messageMap[story.storyId] || [];

    if (!shot) {
      return;
    }

    await runLimited(async () => {
      const data = messages
        .map(message => ({
          coordinates: message.coordinates,
          ...findSourceString(message.key),
        }))
        .filter(({ source_string_hash }) => !!source_string_hash);

      console.log('delete', shot.id);
      await pExec(
        `curl --user api:${TRANSIFEX_TOKEN} -X DELETE https://api.transifex.com/organizations/${TRANSIFEX_ORGANIZATION}/projects/${TRANSIFEX_PROJECT}/context/screenshots/${shot.id}/mapped_strings/`,
      );
      if (!data.length) {
        return;
      }
      console.log('map', shot.id, data.length, 'messages', fileKey);
      await pExec(
        `curl --user api:${TRANSIFEX_TOKEN} -XPOST https://api.transifex.com/organizations/${TRANSIFEX_ORGANIZATION}/projects/${TRANSIFEX_PROJECT}/context/screenshots/${shot.id}/mapped_strings/  -H "Content-Type: application/json" -i -v --data '${JSON.stringify(data)}'`,
      );
    });
  });
