/* tslint:disable:no-console no-var-requires no-require-imports */

const messageMap = require(process.env.WHALES_BUILD_DIR + '/meta/map.json');
const translationsBUX = require('./data/strings.bux');
const translationsAdmin = require('./data/strings.admin');

const translations = [
  ...translationsBUX.map(({ key }) => key),
  // ...translationsAdmin.map(({ key }) => key), (too many of them)
];

const messages = {};
Object
  .keys(messageMap)
  .forEach(key =>
    messageMap[key].forEach((message: any) => messages[message.key] = true)
  );

const missed = translations.filter(key => !messages[key]);

console.log('missed', missed.length, 'of', translations.length);
console.log(missed.join('\n'));
