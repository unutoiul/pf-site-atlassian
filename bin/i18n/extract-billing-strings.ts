import getCatalog from '@lingui/cli/api/catalog';
import extract from '@lingui/cli/lingui-extract';
import { getConfig } from '@lingui/conf';
import * as fs from 'fs';

import { mapLinguiToChromeJson } from './map-strings';

const buxSourceDir = 'src/apps/billing/bux';
const config = getConfig({ cwd: buxSourceDir });
const sourceLocale = config.sourceLocale;

const buildConfig = {
  ...config,
  localeDir: 'build/bux/i18n',
  format: 'lingui',
};

const buildCatalog = getCatalog(buildConfig);
buildCatalog.addLocale(sourceLocale);
extract(buildConfig, { clean: true });

const linguiStrings = buildCatalog.read(sourceLocale);
const chromeStringsJson = mapLinguiToChromeJson(linguiStrings);

const chromeStringsSerialized = JSON.stringify(chromeStringsJson, null, 2);
fs.writeFileSync(`${buxSourceDir}/i18n/source.json`, chromeStringsSerialized);

const prodCatalog = getCatalog(config);
prodCatalog.write(sourceLocale, linguiStrings);
