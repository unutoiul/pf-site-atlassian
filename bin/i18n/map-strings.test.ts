import { expect } from 'chai';

import { mapLinguiToChromeJson, mapToChromeJson, mapToKeyValueJson } from './map-strings';

describe('Map-Strings', () => {
  const validBabelSingleFileJson =
    [
      {
        descriptors: [
          {
            defaultMessage: 'Something went wrong.',
            description: 'Description',
            id: 'default-error-title',
          },
          {
            defaultMessage: 'Please try again later.',
            description: '',
            id: 'default-error-description',
          },
        ],
        path: 'apollo-client/apollo-provider-wrapper.json',

      },
    ];

  const validChromeI18nJson = {
    'default-error-title': {
      message: 'Something went wrong.',
      description: 'Description',
    },
    'default-error-description': {
      message: 'Please try again later.',
      description: '',
    },
  };

  const validLinguiJson = {
    'default-error-title': {
      defaults: 'Something went wrong.',
      description: 'Description',
    },
    'default-error-description': {
      defaults: 'Please try again later.',
    },
  };

  const invalidBabelSingleFileJson =
    [
      {
        descriptors: [],
        path: 'apollo-client/apollo-provider-wrapper.json',

      },
    ];

  const emptyChromeI18nJson = {};

  it('should transform message strings into Chrome i18n JSON strings', () => {
    expect(mapToChromeJson(validBabelSingleFileJson)).to.deep.equal(
      {
        'default-error-title': {
          description: 'Description',
          message: 'Something went wrong.',
        },

        'default-error-description': {
          description: '',
          message: 'Please try again later.',
        },
      },
    );
  });

  it('should transform Chrome i18n JSON strings to key value JSON strings', () => {
    expect(mapToKeyValueJson(validChromeI18nJson)).to.deep.equal(
      {
        'default-error-title': 'Something went wrong.',
        'default-error-description': 'Please try again later.',
      });
  });

  it('should transform Lingui JSON strings to key value JSON strings', () => {
    expect(mapLinguiToChromeJson(validLinguiJson as any)).to.deep.equal({
      'default-error-title': {
        description: 'Description',
        message: 'Something went wrong.',
      },

      'default-error-description': {
        message: 'Please try again later.',
      },
    });
  });

  it('should throw an error when the Chrome i18n JSON strings are empty', () => {
    try {
      mapToChromeJson(invalidBabelSingleFileJson as any);
      expect.fail('expected to see an error');

    } catch (e) {
      expect(e.message).to.equal('Empty Output JSON file');
    }
  });

  it('should throw an error when the key value JSON strings are empty', () => {
    try {
      mapToKeyValueJson(emptyChromeI18nJson as any);
      expect.fail('expected to see an error');

    } catch (e) {
      expect(e.message).to.equal('Empty Output JSON file');
    }
  });

  it('should throw an error when lingui JSON strings are empty', () => {
    try {
      mapLinguiToChromeJson({});
      expect.fail('expected to see an error');

    } catch (e) {
      expect(e.message).to.equal('Empty Output JSON file');
    }
  });
});
