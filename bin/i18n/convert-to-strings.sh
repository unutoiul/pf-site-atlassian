#! /usr/bin/env bash
set -e

# Create i18n directory if it doesn't exist and adds the babel file
# ignore-exit-2 ensures that between tsx to jsx there's not exit 2 error which will stop the command
# Convert jsx to tsx which will show all the errors if the tsx/jsx isn't in the correct format
# Cleans up the temp directories made for the conversions

# Converting tsx to jsx then jsx to json
yarn ts-node bin/i18n/ignore-exit-2.ts 1> /dev/null 2> /dev/null
NODE_OPTIONS=--max_old_space_size=4096 babel "bin/i18n/temp/converted-jsx/**/*.{jsx,js}" 1> /dev/null

# Consolidating into one Chrome i18n JSON file
mkdir -p src/i18n/strings
yarn ts-node bin/i18n/consolidate-one-file.ts
echo 'Generating Chrome i18n JSON file ...'
yarn ts-node bin/i18n/format-chrome-i18n-strings.ts
echo 'Generated Chrome i18n JSON file'

# Removing intermidiate files
rm src/i18n/strings/defaultMessages.json
rm -r bin/i18n/temp

