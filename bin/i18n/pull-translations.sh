#! /usr/bin/env bash
set -ex

echo "Upgrading TX client...."

pip install --upgrade transifex-client

echo " TX client installed, configuring..."

cat > ~/.transifexrc <<EOL
[https://www.transifex.com]
hostname = https://www.transifex.com
password = $bamboo_admin_hub_transifex_api_key_password
username = api
EOL

echo " TX project set, pulling files..."
tx pull --mode reviewed --language de,ja --force
# zh,cs,da,nl,en_GB,fi,fr,hu,it,ko,nb,pl,pt_BR,pt_PT,ro,ru,es,sv,  -- all available languages for use later ADMIN-1170     th,tr,uk,vi --proposed but not impemented in any Atlassian products

echo "Finished pulling files"
