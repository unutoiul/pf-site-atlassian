#!/usr/bin/env bash
fswatch -0 --latency=1 -e ".*" -i "\\.tsx$" src | while read -d "" event 
do 
  if git diff ${event} | grep defaultMessage; then
    # kill any i18n processes that were previously started
    kill -9 $I18N_PID
    yarn i18n:admin &
    I18N_PID=$!
  fi
done
