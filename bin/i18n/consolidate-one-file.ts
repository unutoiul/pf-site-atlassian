// tslint:disable no-console
import manageTranslations from 'react-intl-translations-manager';

(manageTranslations as any)({
  messagesDirectory: 'bin/i18n/temp/converted-json/bin/i18n/temp/converted-jsx/src',
  translationsDirectory: 'src/i18n/strings/',
  languages: [],
  singleMessagesFile: true,
  overridePrinters: {
    printLanguageReport: () => {
      console.log('Generated JSON file');
    },
    printDuplicateIds: duplicateIds => {
      if (duplicateIds.length === 0) {
        console.log('No duplicate keys!');

        return;
      } else {
        console.log('Duplicate keys:');
        for (const id of duplicateIds) {
          console.log(id);
        }
        throw new Error('Duplicate keys detected');
      }
    },
  },
});
