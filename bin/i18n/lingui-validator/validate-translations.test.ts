import getCatalog from '@lingui/cli/api/catalog';
import { getConfig } from '@lingui/conf';
import { expect } from 'chai';

import { getTranslationLocales, loadSourceMessages } from './catalog-helper';
import {
  haveSameStructure,
  notIncluded,
  parseMessageForComponentsWithArgs,
  parseMessageForReactData,
  renderSimplifiedTree
} from './message-parser';

const buxSourceDir = 'src/apps/billing/bux';
const transifexProject = 'atlassian-admin-hub';
const transifexResource = 'i18n-en-strings-billing';

const transifexLink = (locale, key) =>
  'https://www.transifex.com/atlassian/' +
  `${transifexProject}/translate/#${locale}/${transifexResource}` +
  (key ? `?q=${encodeURIComponent(`key:${key}`)}` : '');

const printIcuComponent = (component) => {
  if (component.type === 'argument') {
    return `{${component.arg}}`;
  }
  // for complicated components like plurals print everything
  return `{${component.arg} - ${JSON.stringify(component)}}`;
};

describe('Translation messages', () => {
  const config = getConfig({ cwd: buxSourceDir });
  const catalog = getCatalog(config);
  const sourceMessages = loadSourceMessages(config, catalog);
  const translationKeys = Object.keys(sourceMessages);

  it('should have defined proper variable names', () => {
    const errors = translationKeys.map((key) => {
      const message = sourceMessages[key];
      const variableIssues = message.icu
        .filter(it => it.arg.match(/\d+/))
        .map(printIcuComponent)
        .join(', ');

      if (!variableIssues) {
        return;
      }

      return `
Error in message: "${key}"
with text : "${message.translation}"
Names are not defined for: ${variableIssues}
`;
    }).filter(error => !!error);

    if (errors && errors.length > 0) {
      errors.forEach(it => console.error(it));
      console.error(`

!!!ACTION REQUIRED!!!

Please fix listed errors by extracting an expression used in translation to a simple constant
e.g. from: 
<Trans id="key">You are now on {props.entitlementName} {getEditionName()}</Trans>
    
to  : 
const entitlementName = props.entitlementName
const editionName = getEditionName()
<Trans id="key">You are now on {entitlementName} {editionName}</Trans>    

After that don't forget to extract translation again \`yarn i18n\`
And revalidate translation \`yarn i18n:validate\`
`);
      expect.fail();
    }
  });

  describe('should use same ICU components as in the source language', () => {
    getTranslationLocales(config, catalog).forEach(locale => {
      it(`for "${locale}" language`, () => {
        const messages = catalog.read(locale);
        const translationKeys = Object.keys(messages)
          .filter(key => !!sourceMessages[key]);

        const errors = translationKeys.map((key) => {
          const source = sourceMessages[key];
          const translation = messages[key].translation;
          const icu = parseMessageForComponentsWithArgs(translation);
          const errorMessages: string[] = [];

          const notIncludedInTranslation = notIncluded(source.icu, icu)
            .map(printIcuComponent)
            .join(', ');

          if (notIncludedInTranslation) {
            errorMessages.push(`Variables not defined in translation: ${notIncludedInTranslation}`);
          }

          const notIncludedInSource = notIncluded(icu, source.icu)
            .map(printIcuComponent)
            .join(', ');

          if (notIncludedInSource) {
            errorMessages.push(`Variable defined in translation but not in the source: ${notIncludedInSource}`);
          }

          if (errorMessages.length === 0) {
            return;
          }

          return `
Error in translation: "${key}"
With text  : "${translation}"
and source : "${source.translation}"
${errorMessages.join('\n')}
Link to translation: ${transifexLink(locale, key)}
`;
        }).filter(error => !!error);

        if (errors && errors.length > 0) {
          errors.forEach(it => console.error(it));
          console.error(`

!!!ACTION REQUIRED!!!

There are translation that have not matching format to the source message. 
This may cause a page not rendering properly for a specified ${locale} language.

Please improve the translation in transifex if you are able to or at least leave an appropriate comment.
You can wait for a fixed translation or you can remove it from translation file if you are in rush.
`);
          expect.fail();
        }
      });
    });
  });

  describe('should use same react components as in the source language', () => {
    getTranslationLocales(config, catalog).forEach(locale => {
      it(`for "${locale}" language`, () => {
        const messages = catalog.read(locale);
        const translationKeys = Object.keys(messages)
          .filter(key => !!sourceMessages[key]);

        const errors = translationKeys.map((key) => {
          const source = sourceMessages[key];
          const translation = messages[key].translation;
          const react = parseMessageForReactData(translation);

          if (haveSameStructure(react, source.react)) {
            return;
          }

          return `
Error in translation: "${key}"
with text  : "${translation}"
with structure: ${renderSimplifiedTree(react)}
and source : ""${source.translation}"
with structure: ${renderSimplifiedTree(source.react)}
Link to translation: ${transifexLink(locale, key)}
`;
        }).filter(error => !!error);

        if (errors && errors.length > 0) {
          errors.forEach(it => console.error(it));
          console.error(`

!!!ACTION REQUIRED!!!

There are translation that have not matching format to the source message. 
This may cause a page not rendering properly for a specified ${locale} language.

Please improve the translation in transifex if you are able to or at least leave an appropriate comment.
You can wait for a fixed translation or you can remove it from translation file if you are in rush.
`);
          expect.fail();
        }
      });
    });
  });
});
