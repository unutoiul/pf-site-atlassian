import { parse } from 'messageformat-parser';

interface SimpleNode {
  key: string;
  children: Array<string | SimpleNode>;
}

const isDeepEqual = (a, b) => JSON.stringify(a) === JSON.stringify(b);

export const notIncluded = (arrayA: any[], arrayB: any[]): any[] => {
  arrayB = arrayB.slice();

  return arrayA.filter((elem) => {
    // include only elements that doesn't exists in the second array
    // can't use includes as deep equal is required
    return arrayB.every((elem2, matchIdx) => {
      if (!isDeepEqual(elem, elem2)) {
        return true;
      }

      // Remove match from superset so not counted twice if duplicate in subset.
      arrayB.splice(matchIdx, 1);

      return false;
    });
  });
};

/*
 * Parse the message and returns ICU components
 *
 * ICU stands for International Components for Unicode
 * More information on page http://site.icu-project.org/
 * It uses 'messageformat-parser', same library used by LinguiJS
 */
export const parseMessageForComponentsWithArgs = (message: string): object[] => {
  return parse(message)
    .filter(it => typeof it === 'object')
    .filter(it => !!it.arg) // only interested in components with arguments
    .map(removeTranslationsFromPlurals);
};

const removeTranslationsFromPlurals = (obj): object[] => {
  if (obj.type !== 'plural') {
    return obj;
  }

  return {
    ...obj,
    cases: obj.cases.map(it => {
      const { tokens, ...rest } = it;
      return rest;
    })
  };
};

/*
 * Parse the message and returns react components that are used in the message
 *
 * copied from @lingui/react/formatElements
 */
const formatElements = (value: string): Array<string | SimpleNode> => {
  // match <0>paired</0> and <1/> unpaired tags
  const tagRe = /<(\d+)>(.*)<\/\1>|<(\d+)\/>/;
  const nlRe = /(?:\r\n|\r|\n)/g;

  const parts = value.replace(nlRe, '').split(tagRe);

  // no inline elements, return
  if (parts.length === 1) {
    return [value];
  }

  const tree: Array<string | SimpleNode> = [];

  const before = parts.shift();
  if (before) {
    tree.push(before);
  }

  for (const [index, children, after] of getElements(parts)) {
    tree.push({
      key: index,
      children: children ? formatElements(children) : []
    });

    if (after) {
      tree.push(after);
    }
  }

  return tree;
};

const hasNoChildren = (node: SimpleNode): boolean =>
  !node.children || node.children.length === 0

export const haveSameStructure = (tree1: Array<string | SimpleNode>, tree2: Array<string | SimpleNode>): boolean => {

  const isSimpleNode = (node: string | SimpleNode): node is SimpleNode => {
    return typeof node === 'object';
  };

  const removeStringNodes = (tree: Array<string | SimpleNode>): Array<SimpleNode> => tree.filter(isSimpleNode);

  const areTreesEqual = (cleanTree1: Array<SimpleNode>, cleanTree2: Array<SimpleNode>): boolean => {
    if (cleanTree1.length !== cleanTree2.length) {
      return false;
    }

    return cleanTree1.every(node1 => {
      const node2 = cleanTree2.find(node2 => node2.key === node1.key);
      if (!node2) {
        return false;
      }
      if (hasNoChildren(node1)) {
        // if first node has children, second can not have them either
        return hasNoChildren(node2);
      }
      if (hasNoChildren(node2)) {
        return false; // first node has children, second doesn't so not equal
      }
      return haveSameStructure(node1.children, node2.children);
    });
  };

  return areTreesEqual(removeStringNodes(tree1), removeStringNodes(tree2));

};

export const renderSimplifiedTree = (tree: Array<string | SimpleNode>): string => {
  return tree.map(node => {
    if (typeof node === 'string') {
      return '...';
    }
    if (!node.children || node.children.length === 0) {
      return `<${node.key}/>`;
    }
    return `<${node.key}>${renderSimplifiedTree(node.children)}</${node.key}>`;
  }).join('');
};

export const parseMessageForReactData = formatElements;

/*
 * copied from @lingui/react/formatElements
 *
 * `getElements` - return array of element indexes and element childrens
 *
 * `parts` is array of [pairedIndex, children, unpairedIndex, textAfter, ...]
 * where:
 * - `pairedIndex` is index of paired element (undef for unpaired)
 * - `children` are children of paired element (undef for unpaired)
 * - `unpairedIndex` is index of unpaired element (undef for paired)
 * - `textAfter` is string after all elements (empty string, if there's nothing)
 *
 * `parts` length is always multiply of 4
 *
 * Returns: Array<[elementIndex, children, after]>
 */
const getElements = (parts) => {
  if (!parts.length) {
    return [];
  }

  const [paired, children, unpaired, after] = parts.slice(0, 4);

  return [[parseInt(paired || unpaired, 10), children || '', after]].concat(
    getElements(parts.slice(4, parts.length)),
  );
};
