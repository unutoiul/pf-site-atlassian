import { parseMessageForComponentsWithArgs, parseMessageForReactData } from './message-parser';

export const getTranslationLocales = (config, catalog) => {
  return catalog.getLocales().filter(locale => locale !== config.sourceLocale);
};

export const loadSourceMessages = (config, catalog) => {
  const messages = catalog.read(config.sourceLocale);

  return objectMap(messages, (message) => ({
    react: parseMessageForReactData(message.translation),
    icu: parseMessageForComponentsWithArgs(message.translation),
    ...message,
  }));
};

const objectMap = (object, mapFn) => {
  return Object.keys(object).reduce((result, key) => {
    result[key] = mapFn(object[key]);

    return result;
  }, {});
};
