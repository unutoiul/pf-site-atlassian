import { expect } from 'chai';

import { haveSameStructure, parseMessageForComponentsWithArgs, renderSimplifiedTree, parseMessageForReactData } from './message-parser';

describe('Message Parser', () => {

  describe('parseMessageForComponentsWithArgs', () => {
    it('should return info about argument and ignore text', () => {
      const icuData = parseMessageForComponentsWithArgs('Test {message}');

      expect(icuData).to.be.deep.equal([{
        arg: 'message',
        type: 'argument'
      }]);
    });

    it('should return info about plurals and ignore text', () => {
      const icuData = parseMessageForComponentsWithArgs('{diffDays, plural, one {# 日} other {# 日}} days');

      expect(icuData).to.be.deep.equal([{
        arg: 'diffDays',
        type: 'plural',
        cases: [{ key: 'one' }, { key: 'other' }],
        offset: 0
      }]);
    });
  });

  describe('renderSimplifiedTree', () => {
    const render = (message: string) => renderSimplifiedTree(parseMessageForReactData(message));

    it('should render text as dots', () => {
      const text = render('Some text');

      expect(text).to.be.equal('...')
    });

    it('should render component with children', () => {
      const text = render('Some <0>te<1>xt</1></0>.');

      expect(text).to.be.equal('...<0>...<1>...</1></0>...')
    });

    it('should empty component', () => {
      const text = render('Some <0/>text<1/>.');

      expect(text).to.be.equal('...<0/>...<1/>...')
    });

    it('should ignore not closed tags', () => {
      // would be nicer to handle this case and highlight but parser doesn't support it
      const text = render('Some <0>text<1>.');

      expect(text).to.be.equal('...')
    });
  });

  describe('haveSameStructure', () => {
    const parse = parseMessageForReactData;

    it('should be true for same number of components', () => {
      const tree1 = parse('Some <0>text<1/></0>.');
      const tree2 = parse('Other <0>data<1/></0>.');

      expect(haveSameStructure(tree1, tree2)).to.be.equal(true)
    });

    it('should be false if there is a difference in number of components', () => {
      const tree1 = parse('Some <0>text<1/></0>.');
      const tree2 = parse('Other <0>data</0>.');

      expect(haveSameStructure(tree1, tree2)).to.be.equal(false)
    });

    it('should be false if there is difference in the structure', () => {
      const tree1 = parse('Some <0>text<1/></0>.');
      const tree2 = parse('Other <0>data</0><1/>.');

      expect(haveSameStructure(tree1, tree2)).to.be.equal(false)
    });

    it('should be false if there is difference in the nesting order', () => {
      const tree1 = parse('Some <0><1>text</1></0>.');
      const tree2 = parse('Other <1><0>data</0></1>.');

      expect(haveSameStructure(tree1, tree2)).to.be.equal(false)
    });

    it('should be true if there is different component order', () => {
      const tree1 = parse('<0>Some</0> <1>text</1>.');
      const tree2 = parse('<1>Other</1> <0>data</0>.');

      expect(haveSameStructure(tree1, tree2)).to.be.equal(true)
    });

    it('should be true if there is different component order in sub trees', () => {
      const tree1 = parse('Some <0>text<1/><2/></0>.');
      const tree2 = parse('Other <0><2/>data<1/></0>.');

      expect(haveSameStructure(tree1, tree2)).to.be.equal(true)
    });
  });

});
