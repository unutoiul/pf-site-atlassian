import * as fs from 'fs';

import { DescriptorStrings, mapToChromeJson } from './map-strings';

const file = fs.readFileSync('src/i18n/strings/defaultMessages.json');
const stringFile = file.toString('utf8');
const descriptorStrings: DescriptorStrings[] = JSON.parse(stringFile);
const writeChromeI18nJson = mapToChromeJson(descriptorStrings);
const sortedStrings = Object.entries(writeChromeI18nJson).sort(([a], [b]) => a.localeCompare(b))
  .reduce((result, [id, value]) => ({ ...result, [id]: value }), {});
const writeStrings = JSON.stringify(sortedStrings, null, 2);
fs.writeFileSync('src/i18n/strings/en-chrome.json', writeStrings);
