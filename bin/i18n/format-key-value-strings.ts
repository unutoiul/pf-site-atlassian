import * as fs from 'fs';

import { mapToKeyValueJson } from './map-strings';

const translationFilePath = process.argv[2];
const chromeTranslations = JSON.parse(fs.readFileSync(translationFilePath, 'utf8'));
const keyValuesTranslations = mapToKeyValueJson(chromeTranslations);
const serializedTranslations = JSON.stringify(keyValuesTranslations, null, 2);

fs.writeFileSync(translationFilePath, serializedTranslations);
