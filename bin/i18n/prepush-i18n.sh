#!/bin/sh

if [[ $(git diff $(git for-each-ref --format='%(upstream:short)' $(git symbolic-ref -q HEAD))  --unified=0 | grep defaultMessage) || $(git diff --unified=0 | grep defaultMessage) ]]; then
  yarn i18n
  git add ./src/i18n
  git commit -m 'Extracts strings for translation'
fi