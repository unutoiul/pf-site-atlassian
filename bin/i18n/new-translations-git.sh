#! /usr/bin/env bash
set -e

if [ -n "$(git status --porcelain)" ]; then 
  echo "Translations have been updated, formatting and pushing..."

  for filename in src/i18n/translations/*.json; do
    yarn ts-node bin/i18n/format-key-value-strings.ts $filename
  done < <(git status --porcelain)
  for filename in src/apps/billing/bux/i18n/*/messages.json; do
    yarn ts-node bin/i18n/format-key-value-strings.ts $filename  done < <(git status --porcelain)
  done < <(git status --porcelain)

  BRANCH=$(date +%F)
  echo $BRANCH

  git checkout -b issue/TRANSLATIONS-"$BRANCH"
  git add src/i18n/translations/
    
  git commit -m "updating translations"
  
  git remote set-url origin git@bitbucket.org:atlassian/pf-site-admin-ui.git
  
  git push origin issue/TRANSLATIONS-"$BRANCH"
else 
  echo "There are no new translations to update";
fi