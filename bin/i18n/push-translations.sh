#! /usr/bin/env bash
set -ex

echo "Upgrading TX client...."

pip install --upgrade transifex-client

echo " TX client installed, configuring..."

cat > ~/.transifexrc <<EOL
[https://www.transifex.com]
hostname = https://www.transifex.com
password = $bamboo_admin_hub_transifex_api_key_password
username = api
EOL

echo "Pushing file"

tx push --source
