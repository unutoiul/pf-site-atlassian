export interface MessageStrings {
  defaultMessage: string;
  description: string;
  id: string;
}

export interface DescriptorStrings {
  descriptors: MessageStrings[];
  path: string;

}

export interface ChromeI18nJson {
  message: string;
  description: string;
}

export interface LinguiString {
  translation: string;
  defaults: string;
  description: string;
}

export const mapToChromeJson = (descriptorStrings: DescriptorStrings[]) => {
  const strings = {};
  for (const descriptorString of descriptorStrings) {
    const parsingStrings = descriptorString.descriptors;
    for (const parsingString of parsingStrings) {
      const { id, defaultMessage, description } = parsingString;
      strings[id] = {
        message: defaultMessage,
        description: description || '',
      } as ChromeI18nJson;
    }
  }
  if (Object.keys(strings).length === 0) {
    throw new Error('Empty Output JSON file');
  }

  return strings;
};

export const mapToKeyValueJson = (jsonFile: any) => {
  const strings = {};
  Object.keys(jsonFile).forEach((key) => {
    const chromeI18nString: ChromeI18nJson = jsonFile[key];
    if (chromeI18nString.hasOwnProperty('message')) {
      strings[key] = chromeI18nString.message;
    }
  });
  if (Object.keys(strings).length === 0) {
    if (Object.keys(jsonFile).length !== 0) {
      return jsonFile;
    }
    throw new Error('Empty Output JSON file');
  }

  return strings;
};

export const mapLinguiToChromeJson = (jsonFile: any) => {
  const strings = {};
  Object.keys(jsonFile).forEach((key) => {
    const linguiString: LinguiString = jsonFile[key];
    strings[key] = {
      message: linguiString.defaults,
      ...(linguiString.description ? { description: linguiString.description } : {}),
    } as ChromeI18nJson;
  });
  if (Object.keys(strings).length === 0) {
    throw new Error('Empty Output JSON file');
  }

  return strings;
};
