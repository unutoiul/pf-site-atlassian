#!/usr/bin/env bash
set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

export TRANSIFEX_ORGANIZATION="atlassian"
export TRANSIFEX_PROJECT="atlassian-admin-hub"
export SITE_ADMIN_RESOURCE="i18n-en-strings"
export BUX_RESOURCE="i18n-en-strings-billing"

export TRANSIFEX_TOKEN="${bamboo_admin_hub_transifex_api_key_password}"

export PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." >/dev/null && pwd )"

export WHALES_BUILD_DIR="${PROJECT_DIR}/build/shots"
export WHALES_COMPARE_DIR="${PROJECT_DIR}"

yarn ts-node "${DIR}/storyshooter/pull-screenshots.ts"
"${DIR}/storyshooter/pull-strings.sh"

echo "Uploading screenshots"
yarn ts-node "$DIR/storyshooter/push-screenshots.ts"

# have to download them back to sync data
yarn ts-node "${DIR}/storyshooter/pull-screenshots.ts"


echo "Uploading mapping"
yarn ts-node "${DIR}/storyshooter/push-message-mapping.ts"

echo "Missed strings"
yarn ts-node "${DIR}/storyshooter/missed-strings.ts"
