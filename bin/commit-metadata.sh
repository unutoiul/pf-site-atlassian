#!/usr/bin/env bash

set -xe

# Create and provide metadata to bitbucket.
# This is split apart from the "build in bamboo" script so that we can parallelize
# critical (build + tests) and non critical (metadata, storyboooks, etc.) build
# tasks

yarn install
yarn ci:build-metadata
