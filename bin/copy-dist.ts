import * as cpx from 'cpx';

// linux shell has some problems processing brackets, thus we're using the lib like that.
// If that weren't a problem, we could just "yarn cpx '\"build/webpack-dist/!(*.map|*.gz|*.json)\"' build/dist"
cpx.copySync('build/webpack-dist/!(*.map|*.gz|*.json)', 'build/dist');
