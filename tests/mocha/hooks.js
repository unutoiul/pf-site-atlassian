import * as React from 'react';
import gql from 'graphql-tag';
import fs from 'fs';
import { esm_modules, interopRequire, setAliases, resolutions } from 'require-control';

const path = require('path');
const root = path.resolve(process.cwd());
const styledComponentsPath = path.resolve(path.join(__dirname, '../../node_modules/styled-components'));

setAliases({
  'common': path.join(root, 'src/common'),
  'bux': path.join(root, 'src/apps/billing/bux'),
});

esm_modules();
interopRequire();

resolutions((request, parent) => {
  // force resolve component-query/type to component-type
  if (request === 'type') {
    return 'component-type';
  }
  if (parent.id.indexOf('yields-store') > 0) {
    if (request === 'unserialize') {
      return 'yields-unserialize';
    }
    if (request === 'each') {
      return 'component-each';
    }
  }

  if (request === 'styled-components') {
    return styledComponentsPath;
  }
});

// SVGX loader
require.extensions['.svgx'] = (svg) => {
  svg.exports = () => <span>{svg.filename.substring(svg.filename.lastIndexOf('/') + 1)}</span>;
};

// GQL loader
require.extensions['.graphql'] = (file) => {
  const data = fs.readFileSync(file.filename);
  file.exports.default = gql`${data}`;
};
