import 'babel-polyfill';
// Import globals first – https://github.com/airbnb/enzyme/issues/395
import 'jsdom';
import { configure as configureEnzyme } from 'enzyme';
import chai from 'chai';
import dirtyChai from 'dirty-chai';

/* eslint-disable import/imports-first */
import './DOMPolyfills';

// The adapter should be imported only after the DOM polyfills to ensure that react-dom has the right references setup.
import EnzymeReactAdapter from 'enzyme-adapter-react-16';

const Adapter = new EnzymeReactAdapter();

chai.use(dirtyChai);
configureEnzyme({ adapter: Adapter, disableLifecycleMethods: false });

