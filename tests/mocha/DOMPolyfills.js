import { JSDOM } from 'jsdom';
import * as uuid from 'uuid/v4';

const jsdom = new JSDOM('<!doctype html><html><body></body></html>', {
  url: "http://localhost/",
  userAgent: 'HeadlessChrome NodeJs/10',
  pretendToBeVisual: true,
  beforeParse(window) {
    if (typeof window.URL.createObjectURL === 'undefined' && typeof window.URL.revokeObjectURL === 'undefined') {
      const blobs = {};
      window.URL.createObjectURL = (object) => {
        const url = `blob:http://localhost/${uuid()}}`;
        blobs[url] = object;
        return url;
      };
      window.URL.revokeObjectURL = (url) => {
        delete blobs[url];
      };
    }
  }
});

const { window } = jsdom;

function copyProps(src, target) {
  const descriptors = {};
  Object.getOwnPropertyNames(src)
    .filter(prop => typeof target[prop] === 'undefined')
    .map(prop => {
      descriptors[prop] = Object.getOwnPropertyDescriptor(src, prop)
    });

  Object.defineProperties(target, descriptors);
}

// declare var global: any;

global.window = window;
global.self = window;
global.document = window.document;
global.navigator = window.navigator;
global.HTMLElement = window.HTMLElement;
global.requestAnimationFrame = window.requestAnimationFrame;
global.cancelAnimationFrame = window.cancelAnimationFrame;

global.location = window.location;

global.__NODE_ENV__ = 'test';

Object.defineProperty(
  window.CSSStyleDeclaration.prototype,
  'transition',
  Object.getOwnPropertyDescriptor(window.CSSStyleDeclaration.prototype, 'webkitTransition'));

global.document.execCommand = () => false;
//
require('whatwg-fetch');

copyProps(window, global);


window.getComputedStyle = () => new Proxy({}, {
  get(target, propName) {
    if (propName === 'getPropertyValue') {
      return () => '';
    }
    return '';
  }
});

