/**
 * Happy path of logging in and viewing admin.atlassian.com
 */

// tslint:disable-next-line:no-import-side-effect
import './helpers/selenium';

const { until, By } = $webdriver;

const URLS = {
  ADMIN_HUB: 'https://admin.atlassian.com',
  TEST_ORG_OVERVIEW: '/o/4ca58de1-ab8d-44aa-974d-03a906c68374/overview',
};

const login = async () => {
  try {
    await $driver.get(URLS.ADMIN_HUB);
    $check.time(`Loaded ${URLS.ADMIN_HUB}`);
  } catch (err) {
    $check.time(`Failed to load ${URLS.ADMIN_HUB}`);
    throw err;
  }

  await $driver.wait(until.elementLocated(By.id('username')));

  await $driver.findElement(By.id('username')).sendKeys($check.secrets.username);
  $check.time('Entered username');

  await $driver.findElement(By.id('login-submit')).click();
  $check.time('Clicked continue');

  const passwordEl = $driver.findElement(By.id('password'));
  await $driver.wait(until.elementIsVisible(passwordEl));
  passwordEl.sendKeys($check.secrets.password);
  $check.time('Entered password');

  await $driver.findElement(By.id('login-submit')).click();
  $check.time('Submitted login');

  await $driver.wait(until.urlIs(`${URLS.ADMIN_HUB}${URLS.TEST_ORG_OVERVIEW}`));
  $check.time('Successful login');
  $check.time('Landed on Org Overview');
};

const checkNav = async () => {
  const navHeaderLocator = By.xpath('//span[.="Admin"]');
  await $driver.wait(until.elementLocated(navHeaderLocator));
  $check.time('Found nav header');

  const navOverviewLinkLocator = By.xpath(`//a[@href="${URLS.TEST_ORG_OVERVIEW}"]`);
  await $driver.wait(until.elementLocated(navOverviewLinkLocator));
  $check.time('Found nav Overview link');
};

(async () => {
  try {
    await login();
  } catch (err) {
    $check.fail(`Unable to login, the following error was thrown: ${err.toString()}`);
  }

  try {
    await checkNav();
  } catch (err) {
    $check.fail(`Unable to load Admin Hub, the following error was thrown: ${err.toString()}`);
  }

  $check.pass('All tests passed');
})();
