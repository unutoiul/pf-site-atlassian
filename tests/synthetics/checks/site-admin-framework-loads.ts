/**
 * Happy path of logging in and viewing site admin. Ensures that
 * the nav has loaded as well.
 */

// tslint:disable-next-line:no-import-side-effect
import './helpers/selenium';

const { until, By } = $webdriver;

const URLS = {
  SITE_ADMIN: 'https://site-admin-test.atlassian.net/admin',
  SITE_ADMIN_USERS: 'https://site-admin-test.atlassian.net/admin/users',
};

const login = async () => {
  try {
    await $driver.get(URLS.SITE_ADMIN);
    $check.time(`Loaded ${URLS.SITE_ADMIN}`);
  } catch (err) {
    $check.time(`Failed to load ${URLS.SITE_ADMIN}`);
    throw err;
  }

  await $driver.wait(until.elementLocated(By.id('username')));

  await $driver.findElement(By.id('username')).sendKeys($check.secrets.username);
  $check.time('Entered username');

  await $driver.findElement(By.id('login-submit')).click();
  $check.time('Clicked continue');

  const passwordEl = $driver.findElement(By.id('password'));
  await $driver.wait(until.elementIsVisible(passwordEl));
  passwordEl.sendKeys($check.secrets.password);
  $check.time('Entered password');

  await $driver.findElement(By.id('login-submit')).click();
  $check.time('Submitted login');

  await $driver.wait(until.urlIs(URLS.SITE_ADMIN_USERS));
  $check.time('Successful login');
  $check.time('Landed on Site Admin users page');
};

const checkNav = async () => {
  const navHeaderEl = await $driver.findElement(By.xpath('//span[.="Administration"]'));
  await $driver.wait(until.elementIsVisible(navHeaderEl));
  $check.time('Found nav header');

  const navBillingLink = await $driver.findElement(By.xpath('//span[.="Billing"]'));
  await $driver.wait(until.elementIsVisible(navBillingLink));
  $check.time('Found nav Billing link');
};

(async () => {
  try {
    await login();
  } catch (err) {
    $check.fail(`Unable to login, the following error was thrown: ${err.toString()}`);
  }

  try {
    await checkNav();
  } catch (err) {
    $check.fail(`Unable to load Site Admin, the following error was thrown: ${err.toString()}`);
  }

  $check.pass('All tests passed');
})();
