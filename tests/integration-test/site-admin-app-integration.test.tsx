import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import { sandbox as sinonSandbox, SinonSandbox, spy } from 'sinon';

import AkFlag from '@atlaskit/flag';

import { FeatureFlagsClient } from '../../src/common/feature-flags';
import { apiFacade } from '../../src/schema/api';
import { EmojiPageImpl } from '../../src/site/emoji/emoji-page';

import { App } from '../../src/app';
import { util } from '../../src/utilities/admin-hub';
import {
  clickOnText,
  wait,
  waitUntil,
  waitUntilWithWrapperUpdate,
} from '../../src/utilities/testing';
import { mockScenarios } from './mock-scenarios';
import { currentSiteId } from './mock-service/services/site';

describe('Site Admin', () => {
  let sandbox: SinonSandbox;
  let launchDarklyFlags;

  let oldSetTimeout;

  beforeEach(() => {
    sandbox = sinonSandbox.create();
    // TODO: Remove this once the console error in loading the application is fixed.
    sandbox.stub(console, 'error');
    // isAdminHub is calculated once on page load, which isn't accurate in our integration test setup
    sandbox.stub(util, 'isAdminHub').returns(false);

    sandbox.stub(FeatureFlagsClient, 'getInstance').returns({
      allFlags: async () => launchDarklyFlags,
    });

    oldSetTimeout = window.setTimeout;

    // We need to catch and ignore a specific type of error that gets thrown from within apollo, for the 401 test.
    // We have to stub out setTimeout and handle that error explicitly
    // The error to stub is: GraphQL error: Fetch request failed with status 401 for get-current-user
    // https://hello.atlassian.net/browse/ADMPF-722
    window.setTimeout = (callback, ...other) => {
      function wrappedCallback(...args) {
        try {
          // tslint:disable-next-line:no-invalid-this
          return callback.apply(this, ...args);
        } catch (e) {
          if (e.message && e.message.indexOf('GraphQL error: Fetch request failed with status 401') > -1) {
            return;
          }
          throw e;
        }
      }

      return oldSetTimeout(wrappedCallback, ...other);
    };

    launchDarklyFlags = {
      'adminhub.enabled': false,
      'atlassianaccess.enabled': false,
      'groups.page.adg3': false,
      'admin.internationalization': false,
    };
  });

  afterEach(() => {
    window.setTimeout = oldSetTimeout;
    sandbox.restore();
    mockScenarios.restore();
    apiFacade.reset();
  });

  const clickOnAppSwitcher = async (app) => {
    await waitUntilWithWrapperUpdate(() => {
      return app.find('MenuIcon').length === 1;
    }, app);

    const appSwitcherIcon = app.find('MenuIcon');
    appSwitcherIcon.simulate('click');
  };

  let initialPath: string;

  beforeEach(() => {
    initialPath = window.location.pathname;
    window.history.replaceState({}, '', '/admin/users');
  });

  afterEach(() => {
    window.history.replaceState({}, '', initialPath);
  });

  it('should render apps in app switcher - upselling enabled', async () => {
    mockScenarios.default();

    const app = mount(
      <App />,
      );

    await clickOnAppSwitcher(app);
    await waitUntilWithWrapperUpdate(() => {
      return app.find('Group').length === 2;
    }, app);

    await waitUntilWithWrapperUpdate(() => {
      return app.find('Group').at(1).find('Item').length === 3;
    }, app, 300);

    const openedAppSwitcher = app.find('AppSwitcher');
    const html = openedAppSwitcher.html();
    expect(html).to.include('MyProduct1');
    expect(html).to.include('MyProduct2');
    expect(html).to.include('Confluence');
  });

  it('should render apps in app switcher - upselling disabled via admin setting: promotions disabled', async () => {
    mockScenarios.productSuggestionsDisabled();

    const app = mount(
      <App />,
      );

    await clickOnAppSwitcher(app);

    await waitUntilWithWrapperUpdate(() => {
      return app.find('Group').length === 2;
    }, app);

    await waitUntilWithWrapperUpdate(() => {
      return app.find('Group').at(1).find('Item').length === 2;
    }, app, 300);

    const openedAppSwitcher = app.find('AppSwitcher');
    const html = openedAppSwitcher.html();
    expect(html).to.include('MyProduct1');
    expect(html).to.include('MyProduct2');
  });

  it('should render Security in the nav bar', async () => {
    mockScenarios.default();

    const app = mount(
      <App />,
      );

    await waitUntilWithWrapperUpdate(() => {
      return app.find('NavigationItemGroup[title="Organizations & security"]').length;
    }, app, 500);

    const navigation = app.find('Navigation').html();
    expect(navigation).to.include('Security');
  });

  it('should render JSD in the nav bar when it is available', async () => {
    mockScenarios.default();

    const app = mount(
      <App />,
      );

    await waitUntilWithWrapperUpdate(() => {
      return app.find('NavigationLinkImpl[title="Jira Service Desk"]').length;
    }, app);

    const navigation = app.find('Navigation').html();
    expect(navigation).to.include('Jira Service Desk');

    clickOnText(
      app.find('NavigationLinkImpl[title="Jira Service Desk"]'),
      'Jira Service Desk',
      );

    const portalNav = app.find('Navigation').html();
    expect(portalNav).to.include('Portal only customers');
  });

  it('should render billing in the nav bar', async () => {
    mockScenarios.default();

    const app = mount(
      <App />,
      );

    await waitUntilWithWrapperUpdate(() => {
      return app.find('NavigationLinkImpl[title="Billing"]').length;
    }, app, 500);

    const navigation = app.find('Navigation').html();
    expect(navigation).to.include('Discover applications');
  });

  it('site access link should direct to new path when feature flag is set', async () => {
    mockScenarios.default();

    const app = mount(
      <App />,
      );

    await waitUntilWithWrapperUpdate(() => {
      return app.find('NavigationLinkImpl[title="Site access"]').length;
    }, app);

    const navigation = app.find('Navigation').html();
    expect(navigation).to.include('Site access');

    clickOnText(
      app.find('NavigationLinkImpl[title="Site access"]'),
      'Site access',
        { button: 0 },
      );

    expect(window.location.pathname).equals(`/admin/s/${currentSiteId}/signup`);
  });

  it.skip('should direct groups to a new path when feature flag is set', async () => {
    mockScenarios.default();

    launchDarklyFlags['groups.page.adg3'] = true;

    const app = mount(
      <App />,
      );

    await waitUntilWithWrapperUpdate(() => {
      return app.find('NavigationLinkImpl[title="Groups"]').length;
    }, app);

    const navigation = app.find('Navigation').html();
    expect(navigation).to.include('Groups');

    clickOnText(
      app.find('NavigationLinkImpl[title="Groups"]'),
      'Groups',
        { button: 0 },
      );

    expect(window.location.pathname).equals(`/admin/s/${currentSiteId}/groups`);
  });

  it('should render the code split emoji page', async () => {
    mockScenarios.default();

    const app = mount(
      <App />,
      );

    await waitUntilWithWrapperUpdate(() => {
      return app.find('NavigationLinkImpl[title="Emoji"]').length;
    }, app, 500);

    const navigation = app.find('Navigation').html();
    expect(navigation).to.include('Emoji');

    clickOnText(
      app.find('NavigationLinkImpl[title="Emoji"]'),
      'Emoji',
        { button: 0 },
      );

    await waitUntilWithWrapperUpdate(() => {
      return app.find(EmojiPageImpl).length;
    }, app, 1000);

    const emojiPage = app.find(EmojiPageImpl);

    expect(emojiPage.text().includes('Custom emojis')).to.equal(true);

    await waitUntilWithWrapperUpdate(() => {
      return app.find('DynamicTable').length;
    }, app, 500);

    expect(emojiPage.find('DynamicTable')).to.have.length(1);
  });

  describe('Error handling', () => {
    it('should display an error flag for apollo errors', async () => {
      mockScenarios.umNavigationIsUnavailable();

      const app = mount(
        <App />,
        );

      await waitUntilWithWrapperUpdate(() => {
        return app.find(AkFlag).length > 0;
      }, app);

      const flag = app.find(AkFlag).first();
      expect(flag.prop('id')).to.equal('nav-apps-fetch-error');
    });

    it.skip('should redirect to login if error status is 401', async () => {
      const redirectSpy = spy();

      mockScenarios.userServiceIsUnauthorized();

      mount(
        <App redirectToLogin={redirectSpy} />,
        );

      await waitUntil(() => redirectSpy.callCount > 0, 2000);
      await wait(1); // We wait in case errors are thrown from mounting the component that would cause the tests to fail
      expect(redirectSpy.called).to.equal(true);
    });
  });
});
