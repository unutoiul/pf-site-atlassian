import { expect } from 'chai';
import { mount, ReactWrapper } from 'enzyme';
import * as React from 'react';
import { sandbox as sinonSandbox, SinonSandbox } from 'sinon';

import { FeatureFlagsClient } from 'common/feature-flags';

import { App } from '../../src/app';
import { apiFacade } from '../../src/schema/api';
import { util } from '../../src/utilities/admin-hub';
import {
  clickOnText,
  waitUntilWithWrapperUpdate,
} from '../../src/utilities/testing';
import { mockScenarios } from '../../tests/integration-test/mock-scenarios';
import { currentSiteId } from '../../tests/integration-test/mock-service/services/site';

describe('Access Configuration Integration Tests', () => {
  let sandbox: SinonSandbox;
  let launchDarklyFlags;
  let initialPath: string;

  beforeEach(() => {
    sandbox = sinonSandbox.create();
    // TODO: Remove this once the console error in loading the application is fixed.
    sandbox.stub(console, 'error');
    // isAdminHub is calculated once on page load, which isn't accurate in our integration test setup
    sandbox.stub(util, 'isAdminHub').returns(false);

    sandbox.stub(FeatureFlagsClient, 'getInstance').returns({
      allFlags: async () => launchDarklyFlags,
    });

    launchDarklyFlags = {
      'adminhub.enabled': true,
      'atlassianaccess.enabled': false,
      'groups.page.adg3': false,
      'admin.internationalization': false,
    };

    initialPath = window.location.pathname;
    window.history.replaceState({}, '', '/admin/users');
  });

  afterEach(() => {
    window.history.replaceState({}, '', initialPath);
    sandbox.restore();
    mockScenarios.restore();
    apiFacade.reset();
  });

  const waitUntilAccessConfigPageAppears = async (app: ReactWrapper) => {
    await waitUntilWithWrapperUpdate(() => {
      return app.find('AccessConfigurationPageImpl').length;
    }, app, 500);
  };

  const waitUntilAccessConfigNavItemAppears = async (app: ReactWrapper) => {
    await waitUntilWithWrapperUpdate(() => {
      return app.find('NavigationLinkImpl[title="Product access"]').length;
    }, app);
  };

  const waitUntilAccessConfigTableAppears = async (app: ReactWrapper) => {
    await waitUntilWithWrapperUpdate(() => {
      return app.find('DynamicTable').length > 0;
    }, app);
  };

  const clickOnAccessConfigNavLink = (app: ReactWrapper): void => clickOnText(app.find('NavigationLinkImpl[title="Product access"]'), 'Product access', { button: 0 });
  const clickTabByIndex = (app: ReactWrapper, index: number): ReactWrapper => app.find('div[role="tab"]').at(index).simulate('click');

  const findTableByIndex = (tables: ReactWrapper, index: number): ReactWrapper => tables.at(index);
  const findRowByGroupName = (table: ReactWrapper, groupName: string): ReactWrapper => table.find('tr').filterWhere(e => e.text().includes(groupName));
  const findRows = (table: ReactWrapper): ReactWrapper => table.find('tr');
  const findTable = (app: ReactWrapper): ReactWrapper => app.find('DynamicTable');
  const findToggleByProduct = (app: ReactWrapper, productId: string): ReactWrapper => app.find(`input[name="${productId}"]`);

  const emptyStateExists = (app: ReactWrapper): boolean => !!app.find('EmptyState').length;
  const groupExists = (row: ReactWrapper): boolean => !!row.length;
  const groupIsDefault = (row: ReactWrapper): boolean => !!row.find('Lozenge').length;
  const groupIsAdmin = (row: ReactWrapper): boolean => !!row.find('InfoHover').length;
  const toggleExists = (app: ReactWrapper, productId: string): boolean => !!findToggleByProduct(app, productId).length;
  const toggleChecked = (app: ReactWrapper, productId: string): boolean => !!findToggleByProduct(app, productId).find('checked').length;

  describe('With product information', () => {
    it('Product access information should display', async () => {
      mockScenarios.default();
      const app = mount(<App />);

      await waitUntilAccessConfigNavItemAppears(app);

      clickOnAccessConfigNavLink(app);

      await waitUntilAccessConfigPageAppears(app);
      await waitUntilAccessConfigTableAppears(app);

      const tables = findTable(app);
      expect(tables.length).to.equal(2);

      const jiraSoftwareTable = findTableByIndex(tables, 0);

      expect(findRows(jiraSoftwareTable).length).to.equal(3);
      expect(toggleExists(app, 'jira-software')).to.equal(true);
      expect(toggleChecked(app, 'jira-software')).to.equal(false);

      const jiraSoftwareGroup = findRowByGroupName(jiraSoftwareTable, 'jira-software-users');
      expect(groupExists(jiraSoftwareGroup)).to.equal(true);
      expect(groupIsDefault(jiraSoftwareGroup)).to.equal(true);
      expect(groupIsAdmin(jiraSoftwareGroup)).to.equal(false);

      const administratorsJiraSoftware = findRowByGroupName(jiraSoftwareTable, 'administrators');
      expect(groupExists(administratorsJiraSoftware)).to.equal(true);
      expect(groupIsDefault(administratorsJiraSoftware)).to.equal(false);
      expect(groupIsAdmin(administratorsJiraSoftware)).to.equal(true);

      const jiraCoreTable = findTableByIndex(tables, 1);

      expect(findRows(jiraCoreTable).length).to.equal(3);
      expect(toggleExists(app, 'jira-core')).to.equal(true);
      expect(toggleChecked(app, 'jira-core')).to.equal(false);

      const jiraCoreGroup = findRowByGroupName(jiraCoreTable, 'jira-core-users');
      expect(groupExists(jiraCoreGroup)).to.equal(true);
      expect(groupIsDefault(jiraCoreGroup)).to.equal(true);
      expect(groupIsAdmin(jiraCoreGroup)).to.equal(false);

      const administratorsJiraCore = findRowByGroupName(jiraCoreTable, 'administrators');
      expect(groupExists(administratorsJiraCore)).to.equal(true);
      expect(groupIsDefault(administratorsJiraCore)).to.equal(false);
      expect(groupIsAdmin(administratorsJiraCore)).to.equal(true);
    });

    it('Administration access information should display', async () => {
      mockScenarios.default();
      const app = mount(<App />);

      await waitUntilAccessConfigNavItemAppears(app);

      clickOnAccessConfigNavLink(app);

      await waitUntilAccessConfigPageAppears(app);

      clickTabByIndex(app, 1);

      await waitUntilAccessConfigTableAppears(app);

      const tables = findTable(app);
      expect(tables.length).to.equal(1);

      const jiraAdministrationTable = findTableByIndex(tables, 0);

      expect(findRows(jiraAdministrationTable).length).to.equal(2);

      const jiraSoftwareGroup = findRowByGroupName(jiraAdministrationTable, 'administrators');
      expect(groupExists(jiraSoftwareGroup)).to.equal(true);
      expect(groupIsDefault(jiraSoftwareGroup)).to.equal(false);
      expect(groupIsAdmin(jiraSoftwareGroup)).to.equal(true);
    });
  });

  describe('Without product information', () => {
    it('Error state should display', async () => {
      mockScenarios.accessConfigFails();
      const app = mount(<App />);

      await waitUntilAccessConfigNavItemAppears(app);

      clickOnAccessConfigNavLink(app);

      await waitUntilAccessConfigPageAppears(app);

      expect(emptyStateExists(app)).to.equal(true);
    });
  });

  describe('Toggling feature flag', () => {
    it('Should redirect to new path when feature flag resolves to true', async () => {
      mockScenarios.default();
      const app = mount(<App />);

      await waitUntilAccessConfigNavItemAppears(app);

      clickOnAccessConfigNavLink(app);

      expect(window.location.pathname).equals(`/admin/s/${currentSiteId}/apps`);
    });

    it('Should redirect to old path when feature flag resolves to false', async () => {
      mockScenarios.accessConfigFeatureFlagIsSetToFalse();
      const app = mount(<App />);

      await waitUntilAccessConfigNavItemAppears(app);

      clickOnAccessConfigNavLink(app);

      expect(window.location.pathname).equals(`/admin/apps`);
    });
  });
});
