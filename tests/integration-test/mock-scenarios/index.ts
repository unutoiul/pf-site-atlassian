import { mockService } from '../mock-service/';
import { productSuggestionsScenarios } from './product-suggestions-scenarios';
import { umServiceScenarios } from './um-service-scenarios';
import { createScenario } from './utils';

export const mockScenarios = {
  restore: () => {
    mockService.restore();
  },
  default: createScenario(() => null),
  ...umServiceScenarios,
  ...productSuggestionsScenarios,
};
