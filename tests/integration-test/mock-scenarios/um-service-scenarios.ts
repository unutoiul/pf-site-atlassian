import { mockService } from '../mock-service';
import { umService } from '../mock-service/services/um';
import { userService } from '../mock-service/services/user';
import { createScenario } from './utils';

export const umServiceScenarios = {
  umNavigationIsUnavailable: createScenario(() => {
    mockService
      .when
      .serviceCalled(umService)
      .withEndpoint('getNavigation')
      .respondWith({
        status: 500,
        body: {},
      });
  }),

  userServiceIsUnauthorized: createScenario(() => {
    mockService
      .when
      .serviceCalled(userService)
      .withEndpoint('getCurrentUser')
      .respondWith({
        status: 401,
        body: {},
      });
  }),

  accessConfigFeatureFlagIsSetToFalse: createScenario(() => {
    mockService
      .when
      .serviceCalled(umService)
      .withEndpoint('getFeatureFlags')
      .respondWith({
        status: 200,
        body: {
          'identity.user_management.adg3.self.signup': false,
          'identity.user_management.adg3.access.config': false,
        },
      });
  }),

  accessConfigFails: createScenario(() => {
    mockService
      .when
      .serviceCalled(umService)
      .withEndpoint('getUseAccessConfig')
      .respondWith({
        status: 500,
      });
  }),

  featureFlagEndpointFails: createScenario(() => {
    mockService
      .when
      .serviceCalled(umService)
      .withEndpoint('getFeatureFlags')
      .respondWith({
        status: 500,
        body: {},
      });
  }),

  umFeatureFlagIsSetToFalse: createScenario(() => {
    mockService
      .when
      .serviceCalled(umService)
      .withEndpoint('getFeatureFlags')
      .respondWith({
        status: 200,
        body: {
          'identity.user_management.adg3.users': false,
        },
      });
  }),

  accessRequestsFeatureFlagIsSetToFalse: createScenario(() => {
    mockService
      .when
      .serviceCalled(umService)
      .withEndpoint('getFeatureFlags')
      .respondWith({
        status: 200,
        body: {
          'identity.user_management.adg3.self.signup': true,
          'identity.user_management.request_access.enabled': false,
        },
      });
  }),
};
