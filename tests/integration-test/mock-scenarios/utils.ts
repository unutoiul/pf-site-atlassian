
import { mockService } from '../mock-service';

export const createScenario = (scenario: () => void) => () => {
  mockService.mockAll();
  scenario();
};
