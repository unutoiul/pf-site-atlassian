import { mockService } from '../mock-service';
import { siteService } from '../mock-service/services/site';
import { createScenario } from './utils';

export const productSuggestionsScenarios = {
  productSuggestionsDisabled: createScenario(() => {
    mockService
      .when
      .serviceCalled(siteService)
      .withEndpoint('getProductSuggestionsEnabled')
      .respondWith({
        status: 200,
        headers: {
          'Content-Type': 'application/json',
        },
        body: {
          'product-suggestions-enabled': false,
        },
      });
  }),
};
