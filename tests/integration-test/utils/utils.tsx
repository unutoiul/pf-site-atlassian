import { ReactWrapper } from 'enzyme';

export const findRowByCellText = (table: ReactWrapper, groupName: string): ReactWrapper => {
  return table.find('tr').filterWhere(e => e.text().includes(groupName));
};

export const findRowsInTable = (table: ReactWrapper): ReactWrapper => {
  return table.find('tr');
};

export const findTablesInWrapper = (app: ReactWrapper): ReactWrapper => {
  return app.find('DynamicTable');
};

export const findHoverIcons = (table: ReactWrapper): ReactWrapper => {
  return table.find('div[role="tooltip"]');
};

export const findDropdownItems = (menu: ReactWrapper): ReactWrapper => {
  return menu.find('DropdownItem');
};

export const findDropdownMenu = (table: ReactWrapper): ReactWrapper => {
  return table.find('DropdownMenu');
};
