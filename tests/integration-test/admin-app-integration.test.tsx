import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import { sandbox as sinonSandbox, SinonSandbox, SinonStub } from 'sinon';

import { FeatureFlagsClient } from '../../src/common/feature-flags';

import { App } from '../../src/app';
import { OrganizationLandingPageImpl } from '../../src/organization/landing/organization-landing-page';
import { apiFacade } from '../../src/schema/api';
import { util } from '../../src/utilities/admin-hub';
import {
  waitUntilWithWrapperUpdate,
} from '../../src/utilities/testing';
import { mockScenarios } from './mock-scenarios';

describe('Admin Hub', () => {
  let sandbox: SinonSandbox;
  let launchDarklyFlags;
  let initialPath: string;
  let consoleErrorStub: SinonStub;

  beforeEach(() => {
    sandbox = sinonSandbox.create();

    sandbox.stub(FeatureFlagsClient, 'getInstance').returns({
      allFlags: async () => launchDarklyFlags,
    });
    consoleErrorStub = sandbox.stub(console, 'error');
    // isAdminHub is calculated once on page load, which isn't accurate in our integration test setup
    sandbox.stub(util, 'isAdminHub').returns(true);

    launchDarklyFlags = {
      'fabric.emoji.site.admin.app': true,
      'adminhub.enabled': false,
      'atlassianaccess.enabled': false,
    };
    initialPath = window.location.pathname;
    window.history.replaceState({}, '', '/');
  });

  afterEach(() => {
    sandbox.restore();
    mockScenarios.restore();
    apiFacade.reset();
    window.history.replaceState({}, '', initialPath);
  });

  it.skip('should redirect to the org overview with one org', async () => {
    mockScenarios.default();

    const app = mount(
      <App />,
      );

    await waitUntilWithWrapperUpdate(() => {
      return app.find(OrganizationLandingPageImpl).length > 0;
    }, app, 500);

    expect(window.location.pathname).to.equal('/o/foo/overview');
  });

  it('should not call console.error with the currentSite warning', async () => {
    mockScenarios.default();
    window.history.replaceState({}, '', '/o/foo/overview');

    const app = mount(
      <App />,
      );

    await waitUntilWithWrapperUpdate(() => {
      return app.find(OrganizationLandingPageImpl).length > 0;
    }, app, 500);

    const calledWithCurrentSiteError = consoleErrorStub.getCalls().some(call => call.args[0].indexOf('[currentSite usage]') > -1);

    expect(calledWithCurrentSiteError).to.equal(false);
  });
});
