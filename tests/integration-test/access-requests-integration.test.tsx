import { expect } from 'chai';
import { mount, ReactWrapper } from 'enzyme';
import * as React from 'react';
import { sandbox as sinonSandbox, SinonSandbox } from 'sinon';

import { FeatureFlagsClient } from '../../src/common/feature-flags';

import { App } from '../../src/app';
import { apiFacade } from '../../src/schema/api';
import { util } from '../../src/utilities/admin-hub';
import {
  clickOnText,
  waitUntilWithWrapperUpdate,
} from '../../src/utilities/testing';
import { mockScenarios } from '../../tests/integration-test/mock-scenarios';
import { currentSiteId } from '../../tests/integration-test/mock-service/services/site';

describe('Access Requests Integration Tests', () => {
  let sandbox: SinonSandbox;
  let launchDarklyFlags;
  let initialPath: string;

  beforeEach(() => {
    sandbox = sinonSandbox.create();
    // TODO: Remove this once the console error in loading the application is fixed.
    sandbox.stub(console, 'error');
    // isAdminHub is calculated once on page load, which isn't accurate in our integration test setup
    sandbox.stub(util, 'isAdminHub').returns(false);

    sandbox.stub(FeatureFlagsClient, 'getInstance').returns({
      allFlags: async () => launchDarklyFlags,
    });

    launchDarklyFlags = {
      'adminhub.enabled': true,
      'atlassianaccess.enabled': false,
      'groups.page.adg3': false,
      'admin.internationalization': false,
      'identity.user_management.request_access.enabled': false,
      'identity.user_management.invite_urls.enabled': false,
    };

    initialPath = window.location.pathname;
    window.history.replaceState({}, '', '/admin/users');
  });

  afterEach(() => {
    window.history.replaceState({}, '', initialPath);
    sandbox.restore();
    mockScenarios.restore();
    apiFacade.reset();
  });

  const waitUntilAccessRequestsNavItemAppears = async (app: ReactWrapper) => {
    await waitUntilWithWrapperUpdate(() => {
      return app.find('NavigationLinkImpl[title="Access Requests"]').length > 0;
    }, app);
  };

  const waitUntilAccessRequestsTableAppears = async (app: ReactWrapper) => {
    await waitUntilWithWrapperUpdate(() => {
      const dynamicTable = app.find('DynamicTable');

      return dynamicTable.length && !dynamicTable.prop('isLoading');
    }, app);
  };

  const clickOnAccessRequestsNavLink = (app: ReactWrapper): void => clickOnText(app.find('NavigationLinkImpl[title="Access Requests"]'), 'Access Requests', { button: 0 });

  const findRows = (table: ReactWrapper): ReactWrapper => table.find('Row');
  const findTable = (app: ReactWrapper): ReactWrapper => app.find('DynamicTable');

  describe('Access Requests page', () => {

    describe('Toggling feature flag', () => {

      it('Should not show Access Requests tab when feature flag is off', async () => {
        mockScenarios.accessRequestsFeatureFlagIsSetToFalse();
        const app = mount(<App />);

        expect(app.find('NavigationLinkImpl[title="Access Requests"]').length).equals(0);
      });

      it('Should show Access Requests tab when feature flag is on', async () => {
        mockScenarios.default();
        const app = mount(<App />);

        await waitUntilAccessRequestsNavItemAppears(app);

        expect(app.find('NavigationLinkImpl[title="Access Requests"]').length).equals(1);

        clickOnAccessRequestsNavLink(app);

        expect(window.location.pathname).equals(`/admin/s/${currentSiteId}/access-requests`);
      });

    });

    it('Access requests table should display', async () => {
      mockScenarios.default();
      const app = mount(<App />);

      await waitUntilAccessRequestsNavItemAppears(app);

      clickOnAccessRequestsNavLink(app);

      await waitUntilAccessRequestsTableAppears(app);

      const tables = findTable(app);
      expect(findRows(tables).length).to.equal(2);

    });
  });
});
