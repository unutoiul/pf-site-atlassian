import { expect } from 'chai';
import { mount, ReactWrapper } from 'enzyme';
import * as React from 'react';
import { sandbox as sinonSandbox, SinonSandbox } from 'sinon';

import { FeatureFlagsClient } from '../../src/common/feature-flags';

import { App } from '../../src/app';
import { apiFacade } from '../../src/schema/api';
import { util } from '../../src/utilities/admin-hub';
import {
  clickOnText,
  waitUntilWithWrapperUpdate,
} from '../../src/utilities/testing';
import { mockScenarios } from '../../tests/integration-test/mock-scenarios';
import { currentSiteId } from '../../tests/integration-test/mock-service/services/site';

describe('Users Integration Test', () => {
  let sandbox: SinonSandbox;
  let launchDarklyFlags;
  let initialPath: string;

  beforeEach(() => {
    sandbox = sinonSandbox.create();
    // TODO: Remove this once the console error in loading the application is fixed.
    sandbox.stub(console, 'error');
    // isAdminHub is calculated once on page load, which isn't accurate in our integration test setup
    sandbox.stub(util, 'isAdminHub').returns(false);

    sandbox.stub(FeatureFlagsClient, 'getInstance').returns({
      allFlags: async () => launchDarklyFlags,
    });

    launchDarklyFlags = {
      'adminhub.enabled': true,
      'atlassianaccess.enabled': false,
      'groups.page.adg3': false,
      'admin.internationalization': false,
    };

    initialPath = window.location.pathname;
    window.history.replaceState({}, '', '/admin/users');
  });

  afterEach(() => {
    window.history.replaceState({}, '', initialPath);
    sandbox.restore();
    mockScenarios.restore();
    apiFacade.reset();
  });

  const waitUntilUsersNavItemAppears = async (app: ReactWrapper) => {
    await waitUntilWithWrapperUpdate(() => {
      return app.find('NavigationLinkImpl[title="Users"]').length;
    }, app);
  };

  const clickOnUsersNavLink = (app: ReactWrapper): void => {
    return clickOnText(
      app.find('NavigationLinkImpl[title="Users"]'),
      'Users',
      { button: 0 },
    );
  };

  describe('Toggling feature flag on and off', () => {
    it('Should redirect to new path when feature flag resolves to true', async () => {
      mockScenarios.default();

      const app = mount(<App />);

      await waitUntilUsersNavItemAppears(app);

      clickOnUsersNavLink(app);

      expect(window.location.pathname).equals(`/admin/s/${currentSiteId}/users`);
    });

    it('Should redirect to old path when feature flag resolves to false', async () => {
      mockScenarios.default();
      mockScenarios.umFeatureFlagIsSetToFalse();
      const app = mount(<App />);

      await waitUntilUsersNavItemAppears(app);

      clickOnUsersNavLink(app);

      expect(window.location.pathname).equals(`/admin/users`);
    });
  });
});
