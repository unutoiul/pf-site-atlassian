export type HTTPMethod = 'GET' | 'POST';

export const buildOkResponse = (payload: any) => ({
  status: 200,
  headers: {
    'Content-Type': 'application/json',
  },
  body: payload,
});
