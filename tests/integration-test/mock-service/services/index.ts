import { HTTPMethod } from '../utils';

export type Service<T extends { [key: string]: ServiceEndpoint }> = {
  [K in keyof T]: ServiceEndpoint;
};

export interface ServiceEndpoint {
  method: HTTPMethod;
  url: string;
  defaultResponse: DefaultResponse;
}

export interface DefaultResponse {
  status: number;
  headers?: any;
  body?: any;
}

export function registerService<T extends { [key: string]: ServiceEndpoint }>(service: T): Service<T> {
  return service as any;
}
