import { getConfig } from 'common/config';

import { registerService } from '.';
import { csatEvalCohortFlagKey } from '../../../../src/site/feature-flags/flag-keys';
import { buildOkResponse } from '../utils';
import { currentSiteId } from './site';

const umContextResponse = {
  isAaEnabled: true,
  products: {
    'jira-servicedesk.ondemand': '',
  },
};

const serviceDeskFeatureResponse = {
  'sd.user.management': false,
};

const trustedUsersFeatureResponse = {
  'admin.trusted.users': false,
};

const csatSurveyResponse = {
  'admin.eval.csat.survey': false,
};

const applicationsResponse = [
  {
    id: '812ec203-ee00-303a-80d8-47d7c3d0ace6',
    homeUrl: 'https://localhost/secure/MyJiraHome.jspa',
    adminUrl: 'https://localhost/secure/project/ViewProjects.jspa',
    name: 'MyProduct1',
    hostType: 'product1',
  },
  {
    id: '1363002c-4b37-303f-b9e3-06734d54989a',
    homeUrl: 'https://localhost/wiki',
    adminUrl: 'https://localhost/wiki/admin/viewgeneralconfig.action',
    name: 'MyProduct2',
    hostType: 'product2',
  },
];

const umNextFlagResponse = {
  'identity.user_management.adg3.self.signup': true,
  'identity.user_management.adg3.access.config': true,
  'identity.user_management.adg3.users': true,
  'identity.user_management.user.export.adg3': true,
  'identity.user_management.request_access.enabled': true,
  'identity.user_management.invite_urls.enabled': true,
};

const useAccessConfig = [
  {
    product: {
      productName: 'Jira Software',
      canonicalProductKey: 'jira-software',
      productId: 'jira-software',
    },
    groups: [
      {
        id: 'dummy-group-id-1',
        name: 'jira-software-users',
        productPermission: ['WRITE'],
        requiresApproval: false,
        default: true,
      },
      {
        id: 'dummy-group-id-2',
        name: 'administrators',
        productPermission: ['WRITE', 'MANAGE'],
        requiresApproval: false,
        default: false,
      },
    ],
  },
  {
    product: {
      productName: 'Jira Core',
      productId: 'jira-core',
      canonicalProductKey: 'jira-core',
    },
    groups: [
      {
        id: 'dummy-group-id-3',
        name: 'jira-core-users',
        productPermission: ['WRITE'],
        requiresApproval: false,
        default: true,
      },
      {
        id: 'dummy-group-id-4',
        name: 'administrators',
        productPermission: ['WRITE', 'MANAGE'],
        requiresApproval: false,
        default: false,
      },
    ],
  },
];

const defaultProducts = {
  products: [],
};

const adminAccessConfig = [
  {
    product: {
      productName: 'Jira administration',
      productId: 'jira-admin',
      canonicalProductKey: null,
    },
    groups: [
      {
        id: 'dummy-group-id',
        name: 'administrators',
        productPermission: ['MANAGE'],
        requiresApproval: false,
        default: false,
      },
    ],
  },
];

const accessRequestsResponse = {
  total: 2,
  accessRequests: [
    {
      user: {
        id: '557057:80abfbd6-e1b5-4f23-99d9-077658050e25',
        displayName: 'Alex Sim',
        email: 'asimkin@atlassian.com',
        active: true,
        system: false,
        hasVerifiedEmail: true,
      },
      requestedProducts: [{
        productId: 'confluence',
        productName: 'Confluence',
        status: 'PENDING',
        requestedTime: '2017-07-10T14:00:00Z',
      }],
      type: 'INVITE',
      requester: {
        id: '557057:5bc5f1a2-ba3f-472f-aeef-7eb41a9fba97',
        displayName: 'Dipanjan Laha',
        email: 'dlaha@atlassian.com',
        active: true,
        system: false,
        hasVerifiedEmail: true,
      },
    },
    {
      user: {
        id: '557057:5bc5f1a2-ba3f-472f-aeef-7eb41a9fba97',
        displayName: 'Dipanjan Laha',
        email: 'dlaha@atlassian.com',
        active: true,
        system: false,
        hasVerifiedEmail: true,
      },
      requestedProducts: [{
        productId: 'confluence',
        productName: 'Confluence',
        status: 'PENDING',
        requestedTime: '2017-07-10T14:00:00Z',
      }],
      type: 'REQUEST',
      requester: {
        id: '557057:5bc5f1a2-ba3f-472f-aeef-7eb41a9fba97',
        displayName: 'Dipanjan Laha',
        email: 'dlaha@atlassian.com',
        active: true,
        system: false,
        hasVerifiedEmail: true,
      },
    },
  ],
};

const inviteUrls = {
  urls: [
    {
      url: 'https://id.atlassian.com/invite/p/jira?id=invitation-url-id-1',
      productKey: 'jira-software',
      productName: 'Jira Software',
      expiration: new Date(Date.now() + 10 * 24 * 3600 * 1000).toISOString(),
    },
  ],
};

export const umService = registerService({
  getUmContext: {
    method: 'GET',
    url: `/admin/rest/um/1/context`,
    defaultResponse: buildOkResponse(umContextResponse),
  },

  getServiceDeskFeatures: {
    method: 'GET',
    url: `/admin/rest/um/1/features?sd.user.management`,
    defaultResponse: buildOkResponse(serviceDeskFeatureResponse),
  },

  getTrustedUsersFeatures: {
    method: 'GET',
    url: `${getConfig().identityFeatureFlagUrl}?admin.trusted.users`,
    defaultResponse: buildOkResponse(trustedUsersFeatureResponse),
  },

  getCsatSurveyFeatures: {
    method: 'GET',
    url: `${getConfig().identityFeatureFlagUrl}?admin.eval.csat.survey`,
    defaultResponse: buildOkResponse(csatSurveyResponse),
  },

  getNavigation: {
    method: 'GET',
    url: `/admin/rest/um/1/apps/navigation`,
    defaultResponse: buildOkResponse(applicationsResponse),
  },

  getFeatureFlags: {
    method: 'GET',
    url: `${getConfig().umBasePath}/${currentSiteId}/feature-flags`,
    defaultResponse: buildOkResponse(umNextFlagResponse),
  },

  getUseAccessConfig: {
    method: 'GET',
    url: `${getConfig().umBasePath}/${currentSiteId}/product/access-config/use`,
    defaultResponse: buildOkResponse({ useAccessConfig }),
  },

  getAdminAccessConfig: {
    method: 'GET',
    url: `${getConfig().umBasePath}/${currentSiteId}/product/access-config/admin`,
    defaultResponse: buildOkResponse({ adminAccessConfig }),
  },

  getDefaultProducts: {
    method: 'GET',
    url: `${getConfig().umBasePath}/${currentSiteId}/product/defaults`,
    defaultResponse: buildOkResponse(defaultProducts),
  },

  getFeatures: {
    method: 'GET',
    url: `/admin/rest/um/1/features?${csatEvalCohortFlagKey}`,
    defaultResponse: buildOkResponse({
      [csatEvalCohortFlagKey]: true,
    }),
  },

  getAccessRequests: {
    method: 'GET',
    url: `${getConfig().umBasePath}/${currentSiteId}/access-requests?start-index=1&status=PENDING`,
    defaultResponse: buildOkResponse(accessRequestsResponse),
  },

  getInviteUrls: {
    method: 'GET',
    url: `${getConfig().umBasePath}/${currentSiteId}/invitation-urls`,
    defaultResponse: buildOkResponse(inviteUrls),
  },
});
