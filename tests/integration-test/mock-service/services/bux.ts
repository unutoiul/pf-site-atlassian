import { registerService } from '.';
import { buildOkResponse } from '../utils';

export const buxService = registerService({
  getMyOrganization: {
    method: 'GET',
    url: `/admin/rest/billing-ux/api/billing/organizations`,
    defaultResponse: buildOkResponse([
      {
        id: '1363002c-4b37-303f-b9e3-06734d54989a',
      },
    ]),
  },
});
