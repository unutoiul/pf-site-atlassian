import { getConfig } from 'common/config';

import { registerService } from '.';
import { groupListResponse, usersListResponse } from '../../../../mock/services/um-service/responses';
import { buildOkResponse } from '../utils';

export const currentSiteId = '38b7be26-03f7-42a9-b18b-9cdb56d204c2';

const cloudNameTransformResponse = {
  cloudId: currentSiteId,
};

const sitesResponse = {
  sites: [
    {
      id: currentSiteId,
      displayName: 'My site acme',
      avatarUrl: 'https://www.gravatar.com/avatar/00000000000000000000000000000000?d=monsterid',
    },
  ],
};

export const siteService = registerService({
  getCloudName: {
    method: 'GET',
    url: `/_edge/tenant_info`,
    defaultResponse: buildOkResponse(cloudNameTransformResponse),
  },

  getProductSuggestionsEnabled: {
    method: 'GET',
    url: `${getConfig().apiUrl}/site/${currentSiteId}/setting/xflow/product-suggestions-enabled`,
    defaultResponse: {
      status: 404,
    },
  },

  getUsers: {
    method: 'GET',
    url: `${getConfig().umBasePath}/${currentSiteId}/users?count=20&start-index=1`,
    defaultResponse: {
      status: 200,
      body: usersListResponse,
    },
  },

  getSites: {
    method: 'GET',
    url: `${getConfig().apiUrl}/site/admin/cloud`,
    defaultResponse: buildOkResponse(sitesResponse),
  },

  getGroups: {
    method: 'GET',
    url: `${getConfig().umBasePath}/${currentSiteId}/groups?count=20&start-index=1`,
    defaultResponse: {
      status: 200,
      body: groupListResponse,
    },
  },

  getGroupsAll: {
    method: 'GET',
    url: `${getConfig().umBasePath}/${currentSiteId}/groups`,
    defaultResponse: {
      status: 200,
      body: groupListResponse,
    },
  },

  signupOptions: {
    method: 'GET',
    url: `${getConfig().umBasePath}/${currentSiteId}/signup-options`,
    defaultResponse: buildOkResponse({
      signupEnabled: true,
      openInvite: false,
      domains: ['acme.com', 'acme.com.au', 'acme.co.uk'],
      notifyAdmin: true,
    }),
  },
});
