import { getConfig } from 'common/config';

import { registerService } from '.';
import { buildOkResponse } from '../utils';

const userResponse = {
  account_id: '557057:26ead946-4955-46ef-a11e-b1f9ae987de8',
  email: 'current-user@example.com',
};

export const userService = registerService({
  getCurrentUser: {
    method: 'GET',
    url: `${getConfig().apiUrl}/me`,
    defaultResponse: buildOkResponse(userResponse),
  },
});
