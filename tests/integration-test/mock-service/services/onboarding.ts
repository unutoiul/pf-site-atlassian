import { getConfig } from 'common/config';

import { registerService } from '.';
import { buildOkResponse } from '../utils';

const onboardingFlagResponse = {
  status: true,
};

export const onboardingService = registerService({
  getOnboardingFlag: {
    method: 'GET',
    url: `glob:${getConfig().apiUrl}/flag/my*`,
    defaultResponse: buildOkResponse(onboardingFlagResponse),
  },

  createOnboardingFlag: {
    method: 'POST',
    url: `glob:${getConfig().apiUrl}/flag/my*`,
    defaultResponse: buildOkResponse(onboardingFlagResponse),
  },
});
