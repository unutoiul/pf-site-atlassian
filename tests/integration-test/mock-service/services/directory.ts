import { getConfig } from 'common/config';

import { registerService } from '.';
import { buildOkResponse } from '../utils';

const directoryResponse = {
  data: {
    u1: {
      id: '557057:f625a429-348f-4333-9219-cd9096a0321f',
      fullName: 'Nano Peter',
      avatarUrl: 'https://api.adorable.io/avatars/80/nanop@outlook.com.png',
    },
    u2: {
      id: '557057:4b6a902d-98de-4c0d-b525-ec4a9f4732a1',
      fullName: 'Kid Dailey',
      avatarUrl: 'https://api.adorable.io/avatars/80/kiddailey@yahoo.com.png',
    },
    u3: {
      id: '557057:38a095fc-9870-4ba2-a33f-1fb963ec3db2',
      fullName: 'Stevie Steves',
      avatarUrl: 'https://api.adorable.io/avatars/80/steviesteves.png',
    },
    u4: {
      id: '557057:221a35b2-df52-48da-aa13-9aedb1ec9ae1',
      fullName: 'Monkey Trousers',
      avatarUrl: 'https://api.adorable.io/avatars/80/monkeytrousers.png',
    },
    u5: {
      id: '557057:bf4ac072-4f24-45d6-a182-a57bf14e8eac',
      fullName: 'Janda Miranda',
      avatarUrl: 'https://api.adorable.io/avatars/80/jandamiranda.png',
    },
    u6: {
      id: '557057:bggac072-4f24-45d6-a182-a57bf14e8eac',
      fullName: 'Sanny Gorani',
      avatarUrl: 'https://api.adorable.io/avatars/80/SannyGorani.png',
    },
  },
};

export const directoryService = registerService({
  getDirectoryAvatars: {
    method: 'GET',
    url: `glob:${getConfig().apiUrl}/directory/graphql`,
    defaultResponse: buildOkResponse(directoryResponse),
  },
  postDirectoryAvatars: {
    method: 'POST',
    url: `glob:${getConfig().apiUrl}/directory/graphql`,
    defaultResponse: buildOkResponse(directoryResponse),
  },
});
