import { getConfig } from 'common/config';

import { registerService } from '.';
import { buildOkResponse } from '../utils';

const organizationsResponse = [
  {
    id: 'foo',
    name: 'acme',
  },
];

const domainClaimsResponse = {
  domainClaims: [],
};

const productResponse = [
  'identity-manager',
];

export const organizationService = registerService({
  getMyOrganization: {
    method: 'GET',
    url: `${getConfig().orgUrl}/my`,
    defaultResponse: buildOkResponse(organizationsResponse),
  },

  getDomainClaims: {
    method: 'GET',
    url: '/admin/rest/domain-claim-service/api/getDomainClaims',
    defaultResponse: buildOkResponse(domainClaimsResponse),
  },

  getProducts: {
    method: 'GET',
    url: `${getConfig().orgUrl}/foo/product`,
    defaultResponse: buildOkResponse(productResponse),
  },
});
