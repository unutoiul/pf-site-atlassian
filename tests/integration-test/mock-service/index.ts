import * as fetchMock from 'fetch-mock';

import { Service, ServiceEndpoint } from './services';
import { buxService } from './services/bux';
import { directoryService } from './services/directory';
import { emojiService } from './services/emoji';
import { onboardingService } from './services/onboarding';
import { organizationService } from './services/organization';
import { siteService } from './services/site';
import { umService } from './services/um';
import { userService } from './services/user';

const mockAll = (): void => {
  [
    userService,
    directoryService,
    emojiService,
    onboardingService,
    organizationService,
    siteService,
    umService,
    buxService,
  ].forEach(service => {
    Object.keys(service).forEach(endpoint => {
      mockHttp(service[endpoint]);
    });
  });
};

const mockHttp = (endpoint: ServiceEndpoint, responseOverwrite = null, overwrite = false) => {
  const args = [
    endpoint.url,
    responseOverwrite || endpoint.defaultResponse,
    { overwriteRoutes: overwrite },
  ];

  if (endpoint.method === 'GET') {
    fetchMock.get(...args);
  } else if (endpoint.method === 'POST') {
    fetchMock.post(...args);
  } else {
    throw new Error(`HTTP method type: ${endpoint.method} is invalid`);
  }
};

function serviceCalled<P extends { [key: string]: ServiceEndpoint }, S extends Service<P>>(service: S) {
  return {
    respondWith: (resp: any) => {
      Object.keys(service).forEach(endpoint => {
        mockHttp(service[endpoint], resp, true);
      });
    },

    withEndpoint: <K extends keyof S>(endpoint: K) => ({
      respondWith: (resp: any) => {
        mockHttp(service[endpoint], resp, true);
      },
    }),
  };
}

export const mockService = {
  when: {
    serviceCalled,
  },
  mockAll,
  restore: () => {
    fetchMock.restore();

    return mockService;
  },
};
