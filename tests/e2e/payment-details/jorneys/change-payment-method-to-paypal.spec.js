const metadataMock = require('../../../../mock-server/bux/api/metadata-get.json');

const CHUNK_AWAIT_TIMEOUT = 5000;

module.exports = {
  beforeEach(client) {
    client.reset();
    client.maximizeWindow();
  },

  'Happy path': (client) => {
    const paymentDetails = client.page.paymentDetails();
    paymentDetails.navigate();
    paymentDetails.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    paymentDetails.click('@editPaymentDetails');
    paymentDetails.expect.section('@editPaymentMethod').to.be.visible.after(CHUNK_AWAIT_TIMEOUT);
    client.assert.urlContains('/paymentdetails/update');

    const editPaymentMethod = paymentDetails.section.editPaymentMethod;

    editPaymentMethod.expect.element('@paypalButton').to.be.visible.after(CHUNK_AWAIT_TIMEOUT);
    editPaymentMethod.click('@paypalButton');
    editPaymentMethod.expect.element('@paypalStatusEmail').text.to.equal('user@test.org');

    editPaymentMethod.click('@buttonSubmit');

    paymentDetails.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);
    paymentDetails.section.notification.assertMessage('success');
  }
};
