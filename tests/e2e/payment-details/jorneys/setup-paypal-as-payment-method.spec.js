const metadataMock = require('../../../../mock-server/bux/api/metadata-get.json');
const billingDetailsMock = require('../../../../mock-server/bux/api/billing/billing-details-get.json');

const CHUNK_AWAIT_TIMEOUT = 5000;
const ELEMENT_AWAIT_TIMEOUT = 2000;

module.exports = {
  beforeEach(client) {
    client.reset();
    client.maximizeWindow();
    client.mockBillingDetails({
      status: 200,
      method: 'GET',
      body: Object.assign({}, billingDetailsMock, {
        creditCard: {}
      })
    });
  },

  'Happy path': (client) => {
    const paymentDetails = client.page.paymentDetails();
    paymentDetails.navigate();
    paymentDetails.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    paymentDetails.click('@page'); // Gives page focus

    client.page.globalNotification().close();

    paymentDetails.expect.element('@addPaymentDetailsPrompt')
      .to.have.attribute('href').and.include('/paymentdetails/add');
    paymentDetails.click('@addPaymentDetailsPrompt');

    client.assert.urlContains('/paymentdetails/add');

    paymentDetails.expect.section('@addPaymentDetails').to.be.present;

    // should fill billing address
    const stepBillingAddress = paymentDetails.section.addPaymentDetails.section.stepBillingAddress;
    stepBillingAddress.section.addressFields.assertAddressSupport({
      addressLine1: 'Level 6',
      addressLine2: '341 George St',
      city: 'Sydney',
      postcode: '2000',
      state: 'NSW',
      country: 'AU'
    });
    stepBillingAddress.click('@nextButton');

    // should have paypal button
    const stepBillingMethod = paymentDetails.section.addPaymentDetails.section.stepBillingMethod;
    stepBillingMethod.expect.element('@paypalButton').to.be.visible.after(CHUNK_AWAIT_TIMEOUT);
    stepBillingMethod.click('@paypalButton');

    // should authorize paypal
    stepBillingMethod.expect.element('@paypalStatusEmail').text.to.equal('user@test.org');
    stepBillingMethod.click('@nextButton');

    // should have confirmation data
    const stepOverview = paymentDetails.section.addPaymentDetails.section.stepOverview;
    stepOverview.expect.element('@address')
      .text.to.equal('Potato Corp., Level 6, 341 George St, 2000, Sydney, NSW, AU');

    const paymentMethod = stepOverview.section.paymentMethod;
    paymentMethod.expect.element('@email').text.to.contain('user@test.org');

    stepOverview.expect.element('@nextButton').to.have.attribute('disabled');
    stepOverview.click('@agreement');
    stepOverview.expect.element('@nextButton').to.not.have.attribute('disabled');

    stepOverview.click('@nextButton');

    const globalNotification = client.page.globalNotification();
    globalNotification.waitForElementVisible('@notification', ELEMENT_AWAIT_TIMEOUT);
    globalNotification.expect.element('@notification')
      .to.have.attribute('data-title').equal('Billing details updated');
    globalNotification.expect.element('@notification')
      .to.have.attribute('data-type').equal('success');
  }
};
