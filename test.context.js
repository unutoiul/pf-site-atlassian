import chai from 'chai';
import dirtyChai from 'dirty-chai';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

chai.use(dirtyChai);

Enzyme.configure({ adapter: new Adapter() });

// make console errors cause errors in unit tests
['error'].forEach((type) => {
  console[type] = (...messages) => {
    const message = messages.join(', ');
    // We need to catch and ignore a specific type of error that gets thrown from within apollo, for the site admin app integration 401 test.
    // We have to ignore that console.error call explicitly, since it can happen any time after the test runs, and may happen after `console.error` stub is removed.
    // The error to stub is: GraphQL error: Fetch request failed with status 401 for get-current-user
    // https://hello.atlassian.net/browse/ADMPF-722
    if (message.indexOf('GraphQL error: Fetch request failed with status 401') > -1) {
      return;
    }
    throw new Error(
      `console.${type} was called with: "${message}"
      If this is an expected output of your test case, you should be stubbing
      console.${type} and asserting it was called with the appropriate message.
      If this is caused by invalid propTypes on a React component you will need
      to add all the required props to the test's setup.`,
    );
  };
});

window.__env__ = 'local';

const contextTest = require.context('./tests', true, /(\.|\-)test.+(js|jsx|ts|tsx)$/);
contextTest.keys().forEach(contextTest);

const context = require.context('./src', true, /(\.|\-)test.+(js|jsx|ts|tsx)$/);
context.keys().forEach(context);

const binContext = require.context('./bin', true, /(\.|\-)test.+(js|jsx|ts|tsx)$/);
binContext.keys().forEach(binContext);
