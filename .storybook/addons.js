import '@storybook/addon-knobs/register';
import '@storybook/addon-actions/register';
import '@storybook/addon-links/register';
import '@storybook/addon-options/register';
import '../src/apps/billing/bux/common/helpers/storybook/scenarios-addon/register';
import '../src/apps/billing/bux/common/helpers/storybook/localization/register';
import 'whales-story-shots/storybook/register';
import { blockStoryPannel } from 'whales-story-shots/storybook/speed-patch';

// remove storis pannel in whale mode, to speed up shooting
blockStoryPannel();
