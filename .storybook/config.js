import React from 'react';
import { Trans } from '@lingui/react';
import { configure, addDecorator } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import { setOptions } from '@storybook/addon-options';
import { ApolloProvider } from 'react-apollo';
import { FormattedMessage, FormattedHTMLMessage } from 'react-intl';
import whales from 'whales-story-shots/storybook/decorator';
import 'bux/common/helpers/storybook/DefinitionOfDone';
import { collectI18n, setupI18n, teardownI18n } from 'bux/common/helpers/storybook/collectI18n';
import { processTranslations } from "bux/common/helpers/storybook/translationOverlay";
import { WithLanguageControl } from 'bux/common/helpers/storybook/localization/LanguageControl';

import '../src/polyfills';
import en from '../src/apps/billing/bux/i18n/en/messages';
import ja from '../src/apps/billing/bux/i18n/ja/messages';

import { languages } from '../src/i18n/languages.ts';
import { messageLoader } from '../src/i18n/i18n-provider/message-loader';

import { createApolloClient } from "../src/apollo-client";

const messages = languages
  .getDeliveredLanguages()
  .filter(lng => lng !== 'en')
  .reduce((acc, language) => ({ ...acc, [language]: messageLoader.getMessages(language) }), {});

const apolloClient = createApolloClient();

addDecorator(withKnobs);
addDecorator( (story) => (
  <ApolloProvider client={apolloClient}>
    { story() }
  </ApolloProvider>
));
addDecorator(whales({
  hinted: {
    jsLingui: [Trans],
    reactIntl: [FormattedMessage, FormattedHTMLMessage]
  },
  injectedHinted: [
    collectI18n,
  ],
  setup: () => {
    setupI18n();
  },
  teardown: () => {
    teardownI18n();
  },
  onMessages: (messages, api) => {
    processTranslations(messages, api);
  }
}));
addDecorator((story, ctx) => (
  <div>
    <div style={{ backgroundColor: '#EEE', padding: '10px' }}>
      {ctx.kind} / {ctx.story} <br /><i>{ctx.parameters.fileName}</i>
    </div>
    <WithLanguageControl
      language='en'
      jsLinguiCatalogs={{ en, ja }}
      intlMessages={messages}
    >
      <div>{story()}</div>
    </WithLanguageControl>
  </div>
));

function requireAll(requireContext) {
  return requireContext.keys().map(requireContext);
}

function loadStories() {
  requireAll(require.context('../src', true, /\.story\.(jsx?|tsx?)?$/));
}

configure(loadStories, module);

setOptions({
  sortStoriesByKind: true,
  hierarchySeparator: /\//,
  hierarchyRootSeparator: /\|/,
});
