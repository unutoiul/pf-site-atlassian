import { CheckerPlugin } from 'awesome-typescript-loader';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import * as path from 'path';
import { Configuration, DefinePlugin } from 'webpack';

import { getModules } from '../webpack/module';
import { resolve } from '../webpack/resolve';

export const getConfiguration = (_, __, config: Configuration): Configuration => {
  config.module!.rules = getModules({
    env: 'story',
  }).rules;

  const request = path.resolve(path.join(__dirname, '../src/apps/billing/bux/common/helpers/storybook/storybook-request.js'));
  const scenarios = path.resolve(path.join(__dirname, '../src/apps/billing/mock-server/helpers/scenarios/scenarios-for-webpack.js'));
  const storyConfig = path.resolve(path.join(__dirname, '../src/apps/billing/bux/common/config/environments/storybook.json'));

  config.resolve = {
    ...resolve,
    alias: {
      'bux/common/helpers/request': request,
      '../helpers/scenarios/scenarios-for-node': scenarios,
      './environments/local.json': storyConfig,
      './environments/production.json': storyConfig,
      ...resolve.alias,
    },
  };

  config.plugins!.push(
    new CheckerPlugin(),
    new DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
      __NODE_ENV__: JSON.stringify(process.env.NODE_ENV),
    }),
    // Copy mock-server json files to be accessible from storybooks as static files.
    new CopyWebpackPlugin([
      { from: 'src/apps/billing/mock-server/bux/api/', to: 'static/mock/' },
      { from: 'src/apps/billing/mock-server/tns/', to: 'static/tns/' },
    ]),
  );

  return config;
};
