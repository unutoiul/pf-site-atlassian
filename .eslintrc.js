const schema = require('./build/graphql-temp/schema.json');

module.exports = {
  parser: "babel-eslint",
  rules: {
    "graphql/template-strings": ['error', {
      env: 'literal',
      schemaJson: schema,
      validators: [
        'UniqueOperationNames',
        'LoneAnonymousOperation',
        'KnownTypeNames',
        'FragmentsOnCompositeTypes',
        'VariablesAreInputTypes',
        'ScalarLeafs',
        'FieldsOnCorrectType',
        'UniqueFragmentNames',
        'PossibleFragmentSpreads',
        'NoFragmentCycles',
        'UniqueVariableNames',
        'NoUndefinedVariables',
        'NoUnusedVariables',
        // Currently the plugin does not support @client directives,
        // so we must ignore this rule altogether for now
        // See: https://github.com/apollographql/eslint-plugin-graphql/issues/99
        // 'KnownDirectives',
        'KnownArgumentNames',
        'UniqueArgumentNames',
        'ArgumentsOfCorrectType',
        'ProvidedNonNullArguments',
        'DefaultValuesOfCorrectType',
        'VariablesInAllowedPosition',
        'OverlappingFieldsCanBeMerged',
        'UniqueInputFieldNames',
      ]
    }],
  },
  plugins: ['graphql']
}