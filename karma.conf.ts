import * as path from 'path';
import * as puppeteer from 'puppeteer';
import * as merge from 'webpack-merge';

import createWebpackConfig from './webpack.config';

process.env.CHROME_BIN = puppeteer.executablePath();
const coverage = process.argv.indexOf('--coverage') >= 0;
const debug = process.argv.indexOf('--debug') >= 0;

const wc = merge(createWebpackConfig(), {
  devtool: 'inline-source-map',
  node: {
    fs: 'empty',
  },
  resolve: {
    alias: {
      expect: path.dirname(require.resolve('expect/package')),
    },
  },
  devServer: {
    hot: false,
  },
  module: {
    rules: debug ? [
      {
        test: /\.js$/,
        loader: 'source-map-loader',
      },
    ] : [],
  },
});
wc.entry = undefined;
wc.devServer = undefined;

// Work around webpack 4 compatibility issues:
// https://github.com/webpack-contrib/karma-webpack/issues/322
wc.optimization = {
  splitChunks: false,
  runtimeChunk: false,
};

module.exports = (config) => {
  config.set({
    browsers: [
      'ChromeHeadlessNoSandbox',
    ],
    customLaunchers: {
      ChromeHeadlessNoSandbox: {
        base: 'ChromeHeadless',
        flags: ['--no-sandbox'],
      },
    },
    frameworks: [
      'mocha',
    ],
    plugins: [
      'karma-bamboo-reporter',
      'karma-chrome-launcher',
      'karma-mocha',
      'karma-mocha-reporter',
      'karma-sourcemap-loader',
      'karma-webpack',
      ...(coverage ? ['karma-remap-istanbul'] : []),
    ],
    reporters: [
      'mocha',
      'bamboo',
      ...(coverage ? ['karma-remap-istanbul'] : []),
    ],
    files: [
      'test.context.js',
    ],
    preprocessors: {
      'test.context.js': ['webpack', 'sourcemap'],
    },

    client: {
      mocha: {
        timeout: debug ? 60 * 60 * 1000 : undefined,
      },
    },

    browserDisconnectTimeout: debug ? 60 * 60 * 1000 : undefined,
    browserNoActivityTimeout: debug ? 60 * 60 * 1000 : undefined,

    webpack: wc,
    webpackMiddleware: {
      noInfo: false,
      quiet: false,
      stats: 'errors-only',
    },
    mochaReporter: {
      showDiff: true,
    },
    bambooReporter: {
      filename: './build/test-reports/mocha.json',
    },
    remapIstanbulReporter: {
      reports: {
        html: './build/coverage/html',
        'text-summary': null,
        clover: './build/coverage/clover.xml',
      },
      remapOptions: {
        exclude: '/src/apps/billing/',
      },
    },
  });
};
