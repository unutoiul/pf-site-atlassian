import { Configuration } from 'webpack';

import { devServer } from './webpack/dev-server';
import { devtool } from './webpack/devtool';
import { entry } from './webpack/entry';
import { externals } from './webpack/externals';
import { prodDev } from './webpack/helpers';
import { module } from './webpack/module';
import { output } from './webpack/output';
import { plugins } from './webpack/plugins';
import { resolve } from './webpack/resolve';

export const createWebpackConfig = (): Configuration => ({
  mode: prodDev('production', 'development'),
  devtool,
  entry,
  devServer,
  module,
  output,
  plugins,
  resolve,
  externals,
  optimization: {
    noEmitOnErrors: prodDev(false, true),
    minimize: prodDev(true, false),
    runtimeChunk: {
      name: 'manifest',
    },
  },
});

// tslint:disable-next-line:no-default-export
export default createWebpackConfig;
