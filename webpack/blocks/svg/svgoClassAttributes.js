const crypto = require('crypto');

const csstree = require('css-tree');

let hash;
function uniquifyStyles(svg) {
  hash = crypto.createHash('sha256')
    .update(JSON.stringify(svg)) // Add SVG to be hashed
    .digest('hex') // Hash it
    .substr(0, 6); // Get the first 6 characters (hope for no collisions)

  traverseElements(svg);
  return svg;
}

function traverseElements(node) {
  switch (node.elem) {
    case 'style':
      padStyle(node);
      break;
    case 'linearGradient':
      padLinearGradient(node);
      break;
  }

  if (node.attr('class')) {
    padClassNames(node);
  }

  if (node.attr('fill')) {
    padFillNames(node);
  }

  if (node.content) {
    node.content.forEach(traverseElements);
  }
}

function padStyle(node) {
  const ast = csstree.parse(node.content[0].text);
  csstree.walk(ast, padClassName);
  node.content[0].text = padUrlReference(csstree.translate(ast));
}

function padClassName(node) {
  if (node.type === 'ClassSelector') {
    node.name = pad(node.name);
  }
}

function padLinearGradient(node) {
  node.attrs.id.value = pad(node.attrs.id.value);
  if (node.attrs['xlink:href']) {
    node.attrs['xlink:href'].value = pad(node.attrs['xlink:href'].value);
  }
}

function padClassNames(node) {
  node.attr('class').value = node.attr('class').value.split(' ')
    .map(pad)
    .join(' ');
}

function padFillNames(node) {
  node.attr('fill').value = node.attr('fill').value.split(' ')
    .map(padUrlReference)
    .join(' ');
}

function padUrlReference(style) {
  return style.replace(/url\(["']?([^)]*)["']?\)/g, (a, b) => { return a.replace(b, pad(b) )})
}
function pad(str) {
  return `${str}_${hash}`;
}

module.exports = {
  type: 'full',
  active: true,
  description: 'append a hash of the svg to css classnames and urls',
  fn: uniquifyStyles,
};
