import { Entry } from 'webpack';

import { ifDev } from './helpers';

export const entry: Entry = {
  preload: ['./src/public-path.ts', './src/preload/index.ts'],
  app: [
    './src/public-path.ts',
    ...ifDev('./node_modules/webpack/hot/only-dev-server.js'),
    './src/index.tsx',
  ],
};
