import * as autoprefixer from 'autoprefixer';
import * as MiniCssExtractPlugin from 'mini-css-extract-plugin';
import { resolve } from 'path';
import { Module, NewLoader } from 'webpack';

import * as svgoClassAttributes from './blocks/svg/svgoClassAttributes';
import { cacheBustingVersion, contentHashLength, envSwitch } from './helpers';

export const getModules = ({ env = process.env.NODE_ENV, coverage = false }) => {
  const cssLoaderFabric = (options, extraLoaders: NewLoader[] = []): NewLoader[] => {
    return [
      envSwitch(
        env,
        { loader: MiniCssExtractPlugin.loader },
        { loader: 'style-loader', options: { hmr: env !== 'production' } },
      ),
      { loader: 'css-loader', options },
      {
        loader: 'postcss-loader',
        options: {
          ident: 'postcss',
          plugins: [
            autoprefixer({
              browsers: [
                '>1%',
                'last 4 versions',
                'Firefox ESR',
                'not ie < 9',
              ],
            }),
          ],
        },
      },
      ...extraLoaders,
    ];
  };

  const cssLoaders = cssLoaderFabric({ importLoaders: 1, modules: false });
  const lessLoaders = cssLoaderFabric({ importLoaders: 2, modules: true }, [{
    loader: 'less-loader',
    options: {
      paths: [
        resolve(__dirname, '../src/'),
        resolve(__dirname, '../src/apps/billing'),
        resolve(__dirname, '../node_modules'),
      ],
    },
  }]);

  const svgoLoader: NewLoader = {
    loader: 'svgo-loader',
    options: {
      floatPrecision: 2,
      full: true,
      plugins: [
        { convertColors: { shorthex: false } },
        { convertPathData: true },
        { removeUselessDefs: true },
        { minifyStyles: true },
        { cleanupNumericValues: true },
        { svgoClassAttributes },
      ],
    },
  };

  const tsLoader = {
    loader: 'awesome-typescript-loader',
    options: {
      configFileName: 'tsconfig.babel.build.json',
      useCache: true,
      forceIsolatedModules: true,
      reportFiles: 'src/**/*.{ts,tsx}',
      cacheDirectory: 'build/.awcache',
      useBabel: true,
      // storybook compilation is unbearble slow.
      transpileOne: env === 'story',
    },
  };
  const tsLoaderForTests = {
    loader: 'awesome-typescript-loader',
    options: {
      configFileName: 'tsconfig.build.json',
      useCache: true,
      reportFiles: 'src/**/*.{ts,tsx}',
      cacheDirectory: 'build/.awcache-test',
    },
  };
  const babelLoader = { loader: 'babel-loader', options: { cacheDirectory: true } };
  const istanbulLoader = { loader: 'istanbul-instrumenter-loader', options: { esModules: true } };

  const svgxLoaders: NewLoader[] = [
    babelLoader,
    { loader: 'svg-react-loader' },
    svgoLoader,
  ];

  const coverageLoader: NewLoader[] = coverage ? [istanbulLoader] : [];

  return {
    rules: [
      {
        test: /\.(png|svg)$/,
        loader: 'url-loader',
        options: { limit: 1, name: `[name].[hash:${contentHashLength}]-${cacheBustingVersion}.[ext]` },
      },
      {
        test: /\.svgx$/,
        use: svgxLoaders,
      },
      {
        test: /\.css$/,
        use: cssLoaders,
      },
      {
        test: /\.less/,
        use: lessLoaders,
      },
      {
        test: /\.(graphql|gql)$/,
        exclude: /node_modules/,
        loader: 'graphql-tag/loader',
      },
      {
        test: /\.tsx?$/,
        // Do not include ts(x) files from node_modules in code coverage, as well as test files themselves
        exclude: [
          /node_modules/,
          /test\.tsx?$/,
        ],
        use: [...coverageLoader, tsLoader],
      },
      {
        test: /\.jsx?$/,
        exclude: [
          /node_modules/,
        ],
        use: [...coverageLoader, babelLoader],
      },
      {
        test: /messages\.json$/,
        exclude: /node_modules/,
        include: /\/src\/apps\/billing\/bux\/i18n/,
        loader: '@lingui/loader',
      },
      {
        // Correctly process ts(x) files from node_modules (e.g. for emoji testing) and test files, too
        test: [
          {
            and: [
              /\.tsx?$/,
              {
                or: [
                  /node_modules/,
                  /test\.tsx?$/,
                ],
              },
            ],
          },
        ],
        use: [tsLoaderForTests],
      },
    ],
    strictExportPresence: true,
  };
};

export const module: Module = getModules({
  coverage: process.argv.indexOf('--coverage') >= 0,
});
