import { CheckerPlugin } from 'awesome-typescript-loader';
import * as CaseSensitivePathsPlugin from 'case-sensitive-paths-webpack-plugin';
import * as CircularDependencyPlugin from 'circular-dependency-plugin';
import * as FaviconsWebpackPlugin from 'favicons-webpack-plugin';
import * as fs from 'fs';
import * as HardSourceWebpackPlugin from 'hard-source-webpack-plugin';
import * as HtmlWebpackPlugin from 'html-webpack-plugin';
import * as MiniCssExtractPlugin from 'mini-css-extract-plugin';
import * as webpack from 'webpack';
import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer';
import * as Jarvis from 'webpack-jarvis';

import { cacheBustingVersion, contentHashLength, ifDev, ifProd, prodDev } from './helpers';

const coverage = process.argv.indexOf('--coverage') >= 0;

export const plugins = [
  new CheckerPlugin(),
  new CircularDependencyPlugin({
    exclude: /node_modules/,
    failOnError: true,
    allowAsyncCycles: false,
    cwd: process.cwd(),
  }),
  new MiniCssExtractPlugin({
    filename: prodDev(`[name].[contenthash]-${cacheBustingVersion}.css`, '[name].css'),
  }),
  new CaseSensitivePathsPlugin(),
  new webpack.DefinePlugin({
    'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
    __NODE_ENV__: JSON.stringify(process.env.NODE_ENV),
  }),
  // Disable hard source for karma, as it breaks while watching, and has no advantage for a one-off test run
  ...(coverage || process.env.NODE_ENV === 'test' ? [] : [
    new HardSourceWebpackPlugin(),
    // disable mini-css for hard source, just to be sure.
    // CI builds are made without cache and always "ok-ish"
    new HardSourceWebpackPlugin.ExcludeModulePlugin([{
      test: /mini-css-extract-plugin/,
    }]),
  ]),
  ...ifDev(
    new webpack.HotModuleReplacementPlugin(),
  ),
  ...ifProd(
    new BundleAnalyzerPlugin({
      generateStatsFile: true,
      analyzerMode: 'disabled', // just use this plugin to generate the "stats.json" file
    }),
  ),
  ...(process.env.JARVIS ? [new Jarvis({ port: 1337 })] : []),
  // Ignore all moment.js locale files. We can add these in once
  // we support translations.
  new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
  ...ifProd(
    new HtmlWebpackPlugin({
      template: './src/index.html',
      minify: {
        removeComments: true,
        collapseWhitespace: true,
      },
    }),
  ),
  ...ifDev(
    new HtmlWebpackPlugin({
      // tslint:disable-next-line:no-require-imports
      templateContent: fs.readFileSync('./src/index.html').toString().replace('<meta new-relic-placeholder>', fs.readFileSync('./bin/new-relic/stg.html').toString()),
    }),
  ),
  new FaviconsWebpackPlugin({
    logo: './src/favicon.svg',
    prefix: `icon-[sha1:hash:hex:${contentHashLength}]-${cacheBustingVersion}`,
    persistentCache: false,
    title: 'Atlassian',
    // which icons should be generated (see https://github.com/haydenbleasel/favicons#usage)
    icons: {
      android: false,
      appleIcon: false,
      appleStartup: false,
      coast: false,
      favicons: true,
      firefox: false,
      opengraph: false,
      twitter: false,
      yandex: false,
      windows: false,
    },
  }),
];
