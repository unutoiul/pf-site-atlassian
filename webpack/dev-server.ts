export const devServer = {
  host: '0.0.0.0',
  port: 3001,
  hotOnly: true,
  proxy: require('../mock/services').config, // tslint:disable-line:no-require-imports
  disableHostCheck: true,
  historyApiFallback: true,
  headers: {
    'Access-Control-Allow-Origin': '*',
  },
};
