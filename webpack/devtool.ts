import { Options } from 'webpack';

import { prodDev } from './helpers';

export const devtool: Options.Devtool = prodDev('source-map', 'cheap-module-source-map');
