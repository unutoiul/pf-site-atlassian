import * as path from 'path';
import { Resolve } from 'webpack';

export const resolve: Resolve = {
  extensions: ['.js', '.json', '.ts', '.tsx'],
  alias: {
    // make all `@atlaskit` libs use newer styled-component version
    'styled-components': path.resolve(path.join(__dirname, '../node_modules/styled-components')),
    common: path.resolve(path.join(__dirname, '../src/common')),
    bux: path.resolve(path.join(__dirname, '../src/apps/billing/bux')),
  },
};
