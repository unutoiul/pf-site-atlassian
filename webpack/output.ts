import * as path from 'path';
import { Output } from 'webpack';

import { cacheBustingVersion, prodDev } from './helpers';

export const output: Output = {
  path: path.resolve(path.join(__dirname, '../build/webpack-dist')),
  crossOriginLoading: 'anonymous',
  publicPath: prodDev('__CDN_URL__/', '/'),
  filename: prodDev(`[name].[chunkhash]-${cacheBustingVersion}.js`, '[name].js'),
  ...prodDev({ chunkFilename: `[name].[chunkhash]-${cacheBustingVersion}.js` }, {}),
};
