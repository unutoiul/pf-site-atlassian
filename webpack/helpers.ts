export function ifProd(...prodItems) {
  if (process.env.NODE_ENV === 'production') {
    return prodItems;
  }

  return [];
}

export function ifDev(...devItems) {
  if (process.env.NODE_ENV !== 'production') {
    return devItems;
  }

  return [];
}

export function prodDev(prodValue, devValue) {
  return envSwitch(process.env.NODE_ENV, prodValue, devValue);
}

export function envSwitch(env, prodValue, devValue) {
  return env === 'production' ? prodValue : devValue;
}

export const contentHashLength = 6;
export const cacheBustingVersion = 4;
