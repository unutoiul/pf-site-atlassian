import { ExternalsElement } from 'webpack';

export const externals: ExternalsElement = {
  // These are required for enzyme@2.8.2 – webpack tries to build the test files,
  // which contain a require for these modules if React <=0.13. We're using a later
  // version so we mark these as external
  'react/addons': true,
  'react-addons-test-utils': true,
  'react/lib/ExecutionEnvironment': true,
  'react/lib/ReactContext': true,
};
