export const responses = {
  '/gateway/api/users/:memberId/can-close': {
    errors: [],
    warnings: [
      {
        code: 'HELLO',
        message: 'Thing',
        link: 'http://atlassian.com',
      },
      {
        code: 'HELLO2',
        message: 'Thing2',
      },
    ],
  },
  '/gateway/api/users/:memberId/can-deactivate': {
    errors: [],
    warnings: [
      {
        code: 'HELLO',
        message: 'Reason inserted from products',
        link: 'http://atlassian.com',
      }
    ],
  },
  '/gateway/api/users/:memberId': {
  },
  '/gateway/api/users/:memberId/disabled-states': {
  },
  '/gateway/api/users/:memberId/cancel-deletion': {
  },
};
