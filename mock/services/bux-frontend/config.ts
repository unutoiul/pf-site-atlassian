import { ConfigFunction } from '../config';

export const config: ConfigFunction = ({ proxy }) => ({
  context: [
    '/gateway/api/billing-ux',
  ],
  target: proxy ? (proxy as string) : 'http://localhost:3100',
  secure: false,
  changeOrigin: true,
});
