export const responses = {
  '/gateway/api/site/admin/cloud': {
    sites: [
      {
        id: '38b7be26-03f7-42a9-b18b-9cdb56d204c2',
        displayName: 'Tesla',
        avatarUrl: 'https://www.gravatar.com/avatar/00000000000000000000000000000000?d=monsterid',
      },
      {
        id: '38b7be26-03f7-42a9-b18b-9cdb56d204c3',
        displayName: 'Space X',
        avatarUrl: 'https://site-admin-avatar-cdn.prod.public.atl-paas.net/avatars/240/rocket.png',
      },
    ],
  },
  '/_edge/tenant_info': {
    cloudId: '38b7be26-03f7-42a9-b18b-9cdb56d204c2',
  },
};
