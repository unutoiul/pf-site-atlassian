import { ConfigFunction } from '../config';

export const config: ConfigFunction = ({ proxy }) => ({
  context: [
    '/admin/rest/billing-ux/api/billing/organizations',
    '/gateway/api/billing-ux/api/organizations',
  ],
  target: proxy ? (proxy as string) : 'http://localhost:3002',
  secure: false,
  changeOrigin: true,
});
