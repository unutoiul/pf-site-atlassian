const generateScopes = (scopeKeys: string[]) => {
  return scopeKeys.map(key => ({
    key,
    _links: { self: `/${key}` },
    description: `Description for the scope with key ${key}`,
    name: `Name for the scope with key ${key}`,
    serviceId: 'service-id',
  }));
};

export const responses = {
  '/caas/scope': ({ url, query }) => {
    const scopeKeys = [].concat(query.scopeKey);

    return {
      _links: {
        self: url,
      },
      values: generateScopes(scopeKeys),
    };
  },
};
