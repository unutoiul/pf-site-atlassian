export const responses = {
  '/gateway/api/xflow/:cloudId/activate-product': {},
  '/gateway/api/xflow/:cloudId/query-role': {
    permissionIds: ['add-products', 'invite-users'],
  },
  '/gateway/api/xflow/:cloudId/license-information': {
    hostname: 'https://team-superplasma.jira-dev.com',
    maintenanceEndDate: '2017-05-16',
    maintenanceStartDate: '2017-05-09',
    products: {
      'bonfire.jira.ondemand': { billingPeriod: 'MONTHLY', state: 'ACTIVATING' },
      'com.atlassian.confluence.plugins.confluence-questions.ondemand': {
        billingPeriod: 'MONTHLY',
        state: 'DEACTIVATED',
      },
      'confluence.ondemand': { billingPeriod: 'MONTHLY', state: 'DEACTIVATED' },
      'hipchat.cloud': { billingPeriod: 'MONTHLY', state: 'ACTIVE' },
      'jira-core.ondemand': { billingPeriod: 'MONTHLY', state: 'DEACTIVATED' },
      'jira-servicedesk.ondemand': { billingPeriod: 'MONTHLY', state: 'DEACTIVATED' },
      'jira-software.ondemand': { billingPeriod: 'MONTHLY', state: 'ACTIVE' },
      'team.calendars.confluence.ondemand': { billingPeriod: 'MONTHLY', state: 'DEACTIVATED' },
    },
  },
};
