import * as proxy from 'http-proxy-middleware';

interface ProxyOptions {
  proxy: string | boolean;
}

type ContextFunction = (path: string, res: Request) => boolean;

type Context = string[] | string | ContextFunction;

interface Config extends proxy.Config {
  context: Context;
  target: string;
  secure: boolean;
  changeOrigin: boolean;
}

type ConfigFunction = (proxy: ProxyOptions) => Partial<Config>;
