import { ConfigFunction } from '../config';

export const config: ConfigFunction = ({ proxy }) => ({
  context: ['/gateway/api/apps-platform/app/installation'],
  pathRewrite: proxy ? { '^/gateway/api/apps-platform/app/installation': '' } : {} as { [regexp: string]: string; },
  target: proxy ? (proxy as string) : 'http://localhost:3002',
  secure: false,
  changeOrigin: true,
});
