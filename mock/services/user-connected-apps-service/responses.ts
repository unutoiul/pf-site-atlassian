export const responses = {
  '/gateway/api/apps-platform/app/installation': ({ query }) => {
    if (query.start !== '0') {
      return {
        "_links": {
          "self": "https://connect.staging.public.atl-paas.net/addon/installation?start=20&limit=100"
        },
        "values": [
          {
            "_links": {
              "self": "https://connect.staging.public.atl-paas.net/addon/installation/0bb515f0-8330-431c-9544-486c6b376843",
              "definition": "https://connect.prod.public.atl-paas.net/addon/definition/98c3822f-9cc6-4bc6-87ef-4c6ce8fec290"
            },
            "id": "0bb515f0-8330-431c-9544-486c6b376843",
            "context": "ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/",
            "enabled": true,
            "installTime": "2018-07-16T08:42:48.441Z",
            "latest": true,
            "vendorType": "internal",
            "installerAaid": "655363:05b24ecf-6505-4f6e-a31a-51edb69f6af4",
            "definitionId": "98c3822f-9cc6-4bc6-87ef-4c6ce8fec290",
            "key": "test-58897efa-8a1f-468a-9b96-e975321ae867",
            "name": "TEST",
            "description": "This is a description. This is a description. This is a description. This is a descriptionThis is a description. This is a description. This is a description. This is a description",
            "baseUrl": "https://connect.prod.public.atl-paas.net",
            "origin": "dac",
            "distributionStatus": "public",
            "application": "jira",
            "scopes": [
              "read:jira-user",
              "write:jira-user",
              "play:jira-user",
            ],
            "version": "1.1.0",
            "oauthClientId": "YV7nURG9eXoCCG9vOch7ZBg05qozzZxh",
            "principalId": "5b4717c6141cd45ff06992aa",
            "oauthRedirectUrl": "https://www.getpostman.com/oauth2/callback",
            "avatarUrl": "https://avatar-cdn.stg.internal.atlassian.com/211ab15258c2a2d008c5d2b8e30910a0?by=hash",
            "grants": [
              {
                "id": "981f5c2c-0789-4321-b30d-ae191d2164b9",
                "accountId": "655363:05b24ecf-6505-4f6e-a31a-51edb69f6af4",
                "audience": "api.stg.atlassian.com",
                "oauthClientId": "YV7nURG9eXoCCG9vOch7ZBg05qozzZxh",
                "createdAt": "2018-07-16T08:42:48.446Z",
                "type": "user",
                "scopes": [
                  "read:jira-work"
                ],
                "context": "ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/"
              }
            ]
          },
          {
            "_links": {
              "self": "https://connect.staging.public.atl-paas.net/addon/installation/d2709eee-9d17-4bd0-8b17-f585281cf800",
              "definition": "https://connect.prod.public.atl-paas.net/addon/definition/0bb1f08e-fc47-4899-9327-0861947301d6"
            },
            "id": "d2709eee-9d17-4bd0-8b17-f585281cf800",
            "context": "ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/",
            "enabled": true,
            "installTime": "2018-08-07T13:23:34.622Z",
            "latest": true,
            "installerAaid": "655363:05b24ecf-6505-4f6e-a31a-51edb69f6af4",
            "definitionId": "0bb1f08e-fc47-4899-9327-0861947301d6",
            "key": "stargate-connector-ca32e7fc-6370-4369-858b-f7e74f09dc21",
            "name": "Stargate-connector",
            "description": "",
            "baseUrl": "https://connect.prod.public.atl-paas.net",
            "origin": "dac",
            "distributionStatus": "public",
            "application": "jira",
            "scopes": [
              "read:jira-work"
            ],
            "version": "19.0.0",
            "oauthClientId": "5rIajma4ZQXP12ICRxm5UBgn8mp2V7ST",
            "principalId": "5b1931691a75f051f4a72898",
            "oauthRedirectUrl": "http://localhost:3000",
            "avatarUrl": "https://avatar-cdn.stg.internal.atlassian.com/5d70aa30d882189c7b3b7bef26a328ba?by=hash",
            "grants": [
              {
                "id": "1126f2c2-844f-494f-a114-a707031190cc",
                "accountId": "655363:05b24ecf-6505-4f6e-a31a-51edb69f6af4",
                "audience": "api.stg.atlassian.com",
                "oauthClientId": "5rIajma4ZQXP12ICRxm5UBgn8mp2V7ST",
                "createdAt": "2018-08-07T13:23:34.637Z",
                "type": "user",
                "scopes": [
                  "read:jira-work"
                ],
                "context": "ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/"
              }
            ]
          },
          {
            "_links": {
              "self": "https://connect.staging.public.atl-paas.net/addon/installation/f7bbe3f9-4042-4793-9aee-40839d4b39e9",
              "definition": "https://connect.prod.public.atl-paas.net/addon/definition/318cb969-24a0-4913-85d5-df7c81b67fdf"
            },
            "id": "f7bbe3f9-4042-4793-9aee-40839d4b39e9",
            "context": "ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/",
            "enabled": true,
            "installTime": "2018-08-20T01:00:06.482Z",
            "latest": false,
            "vendorType": "internal",
            "vendorName": "vendorName",
            "installerAaid": "5b15dcb964666649ccb910b7",
            "definitionId": "318cb969-24a0-4913-85d5-df7c81b67fdf",
            "key": "francis-jira-3lo-blitz-3-fa76e881-32f2-4ff7-a795-8f2338acf02f",
            "name": "Francis Jira 3LO Blitz #3",
            "description": "",
            "baseUrl": "https://connect.prod.public.atl-paas.net",
            "origin": "dac",
            "distributionStatus": "public",
            "application": "jira",
            "scopes": [
              "read:jira-work",
              "read:jira-user"
            ],
            "version": "3.1.0",
            "oauthClientId": "p7fU8w2tDjR5RikcKFHRoMb8IK7sGMS3",
            "principalId": "5b7a116d9170df2b44b5cf19",
            "oauthRedirectUrl": "https://frangel.au.ngrok.io/callback",
            "avatarUrl": "https://avatar-cdn.stg.internal.atlassian.com/e2950e2f0a51476de75681b965c2e15d?by=hash",
            "grants": [
              {
                "id": "7517db59-bca7-4151-beb8-250682fc66b0",
                "accountId": "5b15dcb964666649ccb910b7",
                "audience": "api.stg.atlassian.com",
                "oauthClientId": "p7fU8w2tDjR5RikcKFHRoMb8IK7sGMS3",
                "createdAt": "2018-08-20T01:00:06.501Z",
                "type": "user",
                "scopes": [
                  "read:jira-work",
                  "read:jira-user"
                ],
                "context": "ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/"
              }
            ]
          },
          {
            "_links": {
              "self": "https://connect.staging.public.atl-paas.net/addon/installation/bf046fec-e951-41fa-bb92-caad93458b11",
              "definition": "https://connect.prod.public.atl-paas.net/addon/definition/937f42b3-b771-40d6-a4e2-be8f65d4dea0"
            },
            "id": "bf046fec-e951-41fa-bb92-caad93458b11",
            "context": "ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/",
            "enabled": true,
            "installTime": "2018-08-20T01:11:38.908Z",
            "latest": true,
            "vendorType": "thirdParty",
            "vendorName": "thirdPartyVendorName",
            "installerAaid": "655363:d37f46c1-191f-4f82-90af-9a517fa191e2",
            "definitionId": "937f42b3-b771-40d6-a4e2-be8f65d4dea0",
            "key": "3lo3-test-ac556c9a-29f9-446a-aec9-c05c5dbca5b7",
            "name": "3lo3 test",
            "description": "",
            "baseUrl": "https://connect.prod.public.atl-paas.net",
            "origin": "dac",
            "distributionStatus": "public",
            "application": "jira",
            "scopes": [
              "read:jira-work",
              "manage:jira-project",
              "manage:jira-configuration",
              "read:jira-user",
              "write:jira-work"
            ],
            "version": "6.0.0",
            "oauthClientId": "chwVUuOhBdZbnpNPkOwDRggxb6a3xDur",
            "principalId": "5b7a113bb8e3cb589585e47e",
            "oauthRedirectUrl": "https://8db2d7be.au.ngrok.io/callback",
            "avatarUrl": "https://avatar-cdn.stg.internal.atlassian.com/eff6095e7280c0ee8556357e701f344a?by=hash",
            "grants": [
              {
                "id": "3dbf6b2e-63c1-4a51-9083-b71958a5fa1f",
                "accountId": "655363:d37f46c1-191f-4f82-90af-9a517fa191e2",
                "audience": "api.stg.atlassian.com",
                "oauthClientId": "chwVUuOhBdZbnpNPkOwDRggxb6a3xDur",
                "createdAt": "2018-08-20T02:50:27.808Z",
                "type": "user",
                "scopes": [
                  "read:jira-work"
                ],
                "context": "ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/"
              }
            ]
          },
          {
            "_links": {
              "self": "https://connect.staging.public.atl-paas.net/addon/installation/524a550a-669c-4216-ab05-49a71182242c",
              "definition": "https://connect.prod.public.atl-paas.net/addon/definition/8dfc5c31-97cb-4445-b8fe-2d65b8f63a7e"
            },
            "id": "524a550a-669c-4216-ab05-49a71182242c",
            "context": "ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/",
            "enabled": true,
            "installTime": "2018-08-20T01:30:29.366Z",
            "latest": true,
            "vendorType": "thirdParty",
            "vendorName": "thirdPartyVendorName",
            "installerAaid": "5b15dcb964666649ccb910b7",
            "definitionId": "8dfc5c31-97cb-4445-b8fe-2d65b8f63a7e",
            "key": "francis-test-account-jira-3lo-blitz-3-5e7e57ee-5a82-4824-8932-6ef511aea20a",
            "name": "Francis test account Jira 3LO blitz #3",
            "description": "",
            "baseUrl": "https://connect.prod.public.atl-paas.net",
            "origin": "dac",
            "distributionStatus": "development",
            "application": "jira",
            "scopes": [
              "read:jira-work",
              "read:jira-user",
              "write:jira-work"
            ],
            "version": "4.1.0",
            "oauthClientId": "3m1iN0dwiOD7eXaetlzraOREmY58bGbg",
            "principalId": "5b7a178feb18d0589b9b15ce",
            "oauthRedirectUrl": "https://frangel.au.ngrok.io/callback",
            "avatarUrl": "https://avatar-cdn.stg.internal.atlassian.com/54045345c792ea4b5a62d0383154aaaa?by=hash",
            "grants": [
              {
                "id": "ab76a9a5-56d3-4e52-895f-7abe9f87dd9d",
                "accountId": "5b15dcb964666649ccb910b7",
                "audience": "api.stg.atlassian.com",
                "oauthClientId": "3m1iN0dwiOD7eXaetlzraOREmY58bGbg",
                "createdAt": "2018-08-20T01:30:29.384Z",
                "type": "user",
                "scopes": [
                  "read:jira-user"
                ],
                "context": "ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/"
              }
            ]
          },
          {
            "_links": {
              "self": "https://connect.staging.public.atl-paas.net/addon/installation/3670d13b-7524-4a29-a0b6-7f3082641675",
              "definition": "https://connect.prod.public.atl-paas.net/addon/definition/f4ee7472-cdcf-4c88-961b-e05ad4d28bcc"
            },
            "id": "3670d13b-7524-4a29-a0b6-7f3082641675",
            "context": "ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/",
            "enabled": true,
            "installTime": "2018-08-20T01:32:29.848Z",
            "latest": true,
            "vendorType": "thirdParty",
            "vendorName": "thirdPartyVendorName",
            "installerAaid": "5afcc36aa0e6e6142d1faec2",
            "definitionId": "f4ee7472-cdcf-4c88-961b-e05ad4d28bcc",
            "key": "blitz-3lo-be98fc05-e801-4723-ae04-211cb93deb37",
            "name": "Blitz-3lo",
            "description": "",
            "baseUrl": "https://connect.prod.public.atl-paas.net",
            "origin": "dac",
            "distributionStatus": "public",
            "application": "jira",
            "scopes": [
              "read:jira-work",
              "write:jira-work"
            ],
            "version": "5.0.0",
            "oauthClientId": "pEDzvUDNfBB44So5R59wbvDzyphGqNGl",
            "principalId": "5b7a1111332df52a5d94400e",
            "oauthRedirectUrl": "https://d4822831.ngrok.io/callback",
            "avatarUrl": "https://avatar-cdn.stg.internal.atlassian.com/9d9a3bcab9ebf10f318fc1a8f06874d4?by=hash",
            "grants": [
              {
                "id": "55b7123a-8055-449b-a902-20fbc767de51",
                accountId: "5afcc36aa0e6e6142d1faec2",
                "audience": "api.stg.atlassian.com",
                "oauthClientId": "pEDzvUDNfBB44So5R59wbvDzyphGqNGl",
                "createdAt": "2018-08-20T02:13:33.034Z",
                "type": "user",
                "scopes": [
                  "read:jira-work"
                ],
                context: "ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/"
              }
            ]
          },
          {
            _links: {
              "self": "https://connect.staging.public.atl-paas.net/addon/installation/429e67de-501f-4e28-8c04-4b0d92dcca5f",
              "definition": "https://connect.prod.public.atl-paas.net/addon/definition/cee58715-8fe9-4dc0-8223-e03f88897ebc"
            },
            id: "429e67de-501f-4e28-8c04-4b0d92dcca5f",
            context: "ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/",
            enabled: true,
            installTime: "2018-08-20T04:01:02.629Z",
            latest: true,
            vendorType: "thirdParty",
            vendorName: "thirdPartyVendorName",
            installerAaid: "655363:d37f46c1-191f-4f82-90af-9a517fa191e2",
            definitionId: "cee58715-8fe9-4dc0-8223-e03f88897ebc",
            key: "test-test-test-663d3a2e-73f6-4db2-958d-4f6865dbe219",
            name: "test test test",
            description: "",
            baseUrl: "https://connect.prod.public.atl-paas.net",
            origin: "dac",
            distributionStatus: "public",
            application: "jira",
            scopes: [
              "read:jira-work"
            ],
            version: "2.0.0",
            oauthClientId: "zgM6H8bfr2iFnqxdY525frnxnP5kQLEl",
            principalId: "5b7a3c2f99f5015905382723",
            oauthRedirectUrl: "https://8db2d7be.au.ngrok.io/callback",
            avatarUrl: "https://avatar-cdn.stg.internal.atlassian.com/1bd1d4563948eccf43a3da6db8ec2b9d?by=hash",
            grants: [
              {
                id: "06a21303-969d-4011-a743-b8492deb4148",
                accountId: "655363:d37f46c1-191f-4f82-90af-9a517fa191e2",
                audience: "api.stg.atlassian.com",
                oauthClientId: "zgM6H8bfr2iFnqxdY525frnxnP5kQLEl",
                createdAt: "2018-08-20T04:01:02.648Z",
                type: "user",
                scopes: [
                  "read:jira-work"
                ],
                context: "ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/"
              }
            ]
          },
          {
            _links: {
              "self": "https://connect.staging.public.atl-paas.net/addon/installation/12231e1a-c6ad-41a8-ba94-273f040f6ed8",
              "definition": "https://connect.prod.public.atl-paas.net/addon/definition/626db0d4-31a1-4aac-a929-fb6ee2184fef"
            },
            id: "12231e1a-c6ad-41a8-ba94-273f040f6ed8",
            context: "ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/",
            enabled: true,
            installTime: "2018-08-20T04:54:17.094Z",
            latest: false,
            vendorType: "internal",
            installerAaid: "655363:b97842a3-cd12-4aa3-ac54-84ba58467471",
            definitionId: "626db0d4-31a1-4aac-a929-fb6ee2184fef",
            key: "francis-3lo-12345-1c7d086a-6679-4554-bca5-9fb3cb223ee6",
            name: "Francis 3LO #12345",
            description: "",
            baseUrl: "https://connect.prod.public.atl-paas.net",
            origin: "dac",
            distributionStatus: "public",
            application: "jira",
            scopes: [
              "read:jira-work",
              "read:jira-user"
            ],
            version: "3.0.0",
            oauthClientId: "RgNHKegyJ7C6huN02x1BHJU1oW6GOBry",
            principalId: "5b7a4921eb18d0589b9b1652",
            oauthRedirectUrl: "https://frangel.au.ngrok.io/callback",
            avatarUrl: "https://avatar-cdn.stg.internal.atlassian.com/34635e296564c75e1b8444a2e8511418?by=hash"
          },
          {
            _links: {
              "self": "https://connect.staging.public.atl-paas.net/addon/installation/2295f66a-abf5-4d4c-9756-3293cdd2a466",
              "definition": "https://connect.prod.public.atl-paas.net/addon/definition/30acd211-b951-4ab8-b0df-76bdaad6fe22"
            },
            id: "2295f66a-abf5-4d4c-9756-3293cdd2a466",
            context: "ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/",
            enabled: true,
            installTime: "2018-10-10T09:42:42.799Z",
            latest: false,
            vendorType: "internal",
            installerAaid: "655363:0024c3d6-7f10-48a2-8f84-f9bddb4f0378",
            definitionId: "30acd211-b951-4ab8-b0df-76bdaad6fe22",
            key: "better-staging-221071e0-f42a-4a76-94a4-749230e9b8e1",
            name: "Better staging",
            description: "",
            baseUrl: "https://connect.prod.public.atl-paas.net",
            origin: "dac",
            distributionStatus: "public",
            application: "jira",
            scopes: [
              "read:jira-user",
              "read:jira-work"
            ],
            version: "2.1.0",
            oauthClientId: "FhCOB90joSIqu3MkhTDGWzrvUsFq29Up",
            principalId: "5bbdc1462995be2f0a42d83b",
            oauthRedirectUrl: "http://localhost:3000",
            avatarUrl: "https://avatar-cdn.stg.internal.atlassian.com/deef6f13240ec83970eebf4e2942c928?by=hash",
            grants: [
              {
                id: "a15838a3-ebe8-49af-b61d-03aa5b9e7dc6",
                accountId: "655363:0024c3d6-7f10-48a2-8f84-f9bddb4f0378",
                audience: "api.stg.atlassian.com",
                oauthClientId: "FhCOB90joSIqu3MkhTDGWzrvUsFq29Up",
                createdAt: "2018-10-10T09:42:42.819Z",
                type: "user",
                scopes: [
                  "read:jira-work"
                ],
                context: "ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/"
              }
            ]
          }
        ],
        start: 20,
        limit: 100,
        size: 29
      }
    }
    return {
      _links: {
        self: "https://connect.staging.public.atl-paas.net/addon/installation?start=0&limit=20",
        next: "https://connect.staging.public.atl-paas.net/addon/installation?start=20&limit=20"
      },
      values: [
        {
          _links: {
            self: "https://connect.staging.public.atl-paas.net/addon/installation/0a93e57c-a0e8-43a9-bf3a-acf018f45f71",
            definition: "https://connect.prod.public.atl-paas.net/addon/definition/47f84003-48b2-4883-a83f-bf912aa223a4"
          },
          id: "0a93e57c-a0e8-43a9-bf3a-acf018f45f71",
          context: "ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/",
          enabled: true,
          installTime: "2018-04-27T02:26:20.097Z",
          latest: true,
          installerAaid: "655363:9996cc93-dc75-4a1a-b012-4971c25cfcbc",
          definitionId: "47f84003-48b2-4883-a83f-bf912aa223a4",
          key: "jira-test-5eafbf41-e9a2-45d4-aced-cff347e67b4c",
          name: "Jira test",
          description: "",
          baseUrl: "https://peterb.ngrok.io",
          origin: "dac",
          distributionStatus: "public",
          application: "jira",
          scopes: [
            "participate:conversation"
          ],
          version: "1.4.0",
          oauthClientId: "9QE0Us05vY2i57Z4PVe3MylXlM4NAz5D",
          principalId: "5ae280c39fcb1f22f34d1fb5",
          oauthRedirectUrl: "https://peterb.ngrok.io/callback",
          avatarUrl: "https://avatar-cdn.stg.internal.atlassian.com/2636f9704cc8a26e3a5d2ecb34dd1908?by=hash",
          grants: [
            {
              id: "a2778f8a-3252-4459-96d6-b05a08905627",
              accountId: "655363:9996cc93-dc75-4a1a-b012-4971c25cfcbc",
              audience: "api.stg.atlassian.com",
              oauthClientId: "9QE0Us05vY2i57Z4PVe3MylXlM4NAz5D",
              createdAt: "2018-04-27T02:26:20.111Z",
              type: "user",
              scopes: [
                "read:jira-work",
                "read:jira-user"
              ],
              context: "ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/"
            }
          ]
        },
        {
          _links: {
            self: "https://connect.staging.public.atl-paas.net/addon/installation/73ab6fe2-d321-4706-a005-ee90fdaf83ad",
            definition: "https://connect.prod.public.atl-paas.net/addon/definition/77b2ff3a-8846-40a1-be77-092e89ed2a8a"
          },
          id: "73ab6fe2-d321-4706-a005-ee90fdaf83ad",
          context: "ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/",
          enabled: true,
          installTime: "2018-05-30T05:49:47.231Z",
          latest: true,
          installerAaid: "655363:d37f46c1-191f-4f82-90af-9a517fa191e2",
          definitionId: "77b2ff3a-8846-40a1-be77-092e89ed2a8a",
          key: "1234-jira-6afa81ba-42ec-427b-89ef-369170dd7d84",
          name: "1234 jira",
          description: "",
          baseUrl: "https://connect.prod.public.atl-paas.net",
          origin: "dac",
          distributionStatus: "public",
          application: "jira",
          scopes: [
            "read:jira-user"
          ],
          version: "1.7.0",
          oauthClientId: "vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3",
          principalId: "5b0e3525286de61ad456e639",
          oauthRedirectUrl: "https://224afe77.au.ngrok.io/callback",
          avatarUrl: "https://avatar-cdn.stg.internal.atlassian.com/43feb206d7fec9f5a6db0238a2785da9?by=hash",
          grants: [
            {
              id: "c466ffaf-05ba-41dd-8927-a879a5dc59a4",
              accountId: "655363:d37f46c1-191f-4f82-90af-9a517fa191e2",
              audience: "api.stg.atlassian.com",
              oauthClientId: "vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3",
              createdAt: "2018-07-11T04:51:40.822Z",
              type: "user",
              scopes: [
                "write:jira-work"
              ],
              context: "ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/"
            },
            {
              id: "c466ffaf-05ba-41dd-8927-a879a5dc59a5",
              accountId: "655362:1c0d1b38-3811-47b7-b6fe-93befec88670",
              audience: "api.stg.atlassian.com",
              oauthClientId: "vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3",
              createdAt: "2018-08-11T04:51:40.822Z",
              type: "user",
              scopes: [
                "write:jira-work",
                "read:jira-user"
              ],
              context: "ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/"
            },
            {
              id: "c466ffaf-05ba-41dd-8927-a879a5dc59a5",
              accountId: "655362:1c0d1b38-3810-47b7-b6fe-93befec88670",
              audience: "api.stg.atlassian.com",
              oauthClientId: "vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3",
              createdAt: "2018-08-11T04:51:40.822Z",
              type: "user",
              scopes: [
                "write:jira-work",
                "read:jira-user"
              ],
              context: "ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/"
            },
            {
              id: "c466ffaf-05ba-41dd-8927-a879a5dc59a5",
              accountId: "655362:1c0d1b38-3809-47b7-b6fe-93befec88670",
              audience: "api.stg.atlassian.com",
              oauthClientId: "vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3",
              createdAt: "2018-08-11T04:51:40.822Z",
              type: "user",
              scopes: [
                "write:jira-work",
                "read:jira-user"
              ],
              context: "ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/"
            },
            {
              id: "c466ffaf-05ba-41dd-8927-a879a5dc59a5",
              accountId: "655362:1c0d1b38-3808-47b7-b6fe-93befec88670",
              audience: "api.stg.atlassian.com",
              oauthClientId: "vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3",
              createdAt: "2018-08-11T04:51:40.822Z",
              type: "user",
              scopes: [
                "write:jira-work",
                "read:jira-user"
              ],
              context: "ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/"
            },
            {
              id: "c466ffaf-05ba-41dd-8927-a879a5dc59a5",
              accountId: "655362:1c0d1b38-3807-47b7-b6fe-93befec88670",
              audience: "api.stg.atlassian.com",
              oauthClientId: "vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3",
              createdAt: "2018-08-11T04:51:40.822Z",
              type: "user",
              scopes: [
                "write:jira-work",
                "read:jira-user"
              ],
              context: "ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/"
            },
            {
              id: "c466ffaf-05ba-41dd-8927-a879a5dc59a5",
              accountId: "655362:1c0d1b38-3806-47b7-b6fe-93befec88670",
              audience: "api.stg.atlassian.com",
              oauthClientId: "vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3",
              createdAt: "2018-08-11T04:51:40.822Z",
              type: "user",
              scopes: [
                "write:jira-work",
                "read:jira-user"
              ],
              context: "ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/"
            },
            {
              id: "c466ffaf-05ba-41dd-8927-a879a5dc59a5",
              accountId: "655362:1c0d1b38-3804-47b7-b6fe-93befec88670",
              audience: "api.stg.atlassian.com",
              oauthClientId: "vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3",
              createdAt: '2018-08-11T04:51:40.822Z',
              type: "user",
              scopes: [
                "write:jira-work",
                "read:jira-user"
              ],
              context: "ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/"
            },
            {
              id: "c466ffaf-05ba-41dd-8927-a879a5dc59a5",
              accountId: "655362:1c0d1b38-3803-47b7-b6fe-93befec88670",
              audience: "api.stg.atlassian.com",
              oauthClientId: "vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3",
              createdAt: "2018-08-11T04:51:40.822Z",
              type: "user",
              scopes: [
                "write:jira-work",
                "read:jira-user"
              ],
              context: "ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/"
            },
            {
              id: "c466ffaf-05ba-41dd-8927-a879a5dc59a5",
              accountId: "655362:1c0d1b38-3801-47b7-b6fe-93befec88670",
              audience: "api.stg.atlassian.com",
              oauthClientId: "vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3",
              createdAt: "2018-08-11T04:51:40.822Z",
              type: "user",
              scopes: [
                "write:jira-work",
                "read:jira-user"
              ],
              context: "ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/"
            },
            {
              id: "c466ffaf-05ba-41dd-8927-a879a5dc59a5",
              accountId: "655362:1c0d1b38-3800-47b7-b6fe-93befec88670",
              audience: "api.stg.atlassian.com",
              oauthClientId: "vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3",
              createdAt: "2018-08-11T04:51:40.822Z",
              type: "user",
              scopes: [
                "write:jira-work",
                "read:jira-user"
              ],
              context: "ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/"
            },
            {
              id: "c466ffaf-05ba-41dd-8927-a879a5dc59a5",
              accountId: "655362:1c0d1b38-3798-47b7-b6fe-93befec88670",
              audience: "api.stg.atlassian.com",
              oauthClientId: "vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3",
              createdAt: "2018-08-11T04:51:40.822Z",
              type: "user",
              scopes: [
                "write:jira-work",
                "read:jira-user"
              ],
              context: "ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/"
            },
            {
              id: "c466ffaf-05ba-41dd-8927-a879a5dc59a5",
              accountId: '655362:1c0d1b38-3797-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3796-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3795-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3794-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3793-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3792-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3791-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8926-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3789-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8926-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3788-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3786-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3785-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3784-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3783-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3781-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3781-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
{
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            },
            {
              id: 'c466ffaf-05ba-41dd-8927-a879a5dc59a5',
              accountId: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'vNiYBgVnEA7sbGPwyKWcjtvh4t8yKjP3',
              createdAt: '2018-08-11T04:51:40.822Z',
              type: 'user',
              scopes: [
                'write:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            }
          ],
        },
        {
          _links: {
            self: 'https://connect.staging.public.atl-paas.net/addon/installation/e9c4d356-b57d-4159-a357-e2454dfc5eaa',
            definition: 'https://connect.prod.public.atl-paas.net/addon/definition/8235903a-f84a-4220-8b40-6fdf24a70759'
          },
          id: 'e9c4d356-b57d-4159-a357-e2454dfc5eaa',
          context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/',
          enabled: true,
          installTime: '2018-05-30T07:07:50.475Z',
          latest: true,
          installerAaid: '655363:9996cc93-dc75-4a1a-b012-4971c25cfcbc',
          definitionId: '8235903a-f84a-4220-8b40-6fdf24a70759',
          key: 'example-app-for-jira-with-introspection-api-382dec93-1c46-4344-8ad5-1bac050f9638',
          name: 'Example app for JIRA with Introspection API',
          description: 'Sort of',
          baseUrl: 'https://connect.prod.public.atl-paas.net',
          origin: 'dac',
          distributionStatus: 'public',
          application: 'jira',
          scopes: [
            'read:jira-user'
          ],
          version: '1.1.0',
          oauthClientId: 'mBMhotPVhvimR3oV8PHcGD8dBigeA1rh',
          principalId: '5b0e49c191981f4fdde61e71',
          oauthRedirectUrl: 'https://dsorin.au.ngrok.io/callback',
          avatarUrl: 'https://avatar-cdn.stg.internal.atlassian.com/69bdacc85898b31c36152fe7704ec22a?by=hash',
          grants: [
            {
              id: 'c2ae2e0f-5416-490e-a014-c2718d469727',
              accountId: '655363:9996cc93-dc75-4a1a-b012-4971c25cfcbc',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'mBMhotPVhvimR3oV8PHcGD8dBigeA1rh',
              createdAt: '2018-05-30T07:07:50.482Z',
              type: 'user',
              scopes: [
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            }
          ],
        },
        {
          _links: {
            self: 'https://connect.staging.public.atl-paas.net/addon/installation/e3fb9498-9227-4a28-8067-75237cdb0450',
            definition: 'https://connect.prod.public.atl-paas.net/addon/definition/ad04c46e-f018-4941-897d-a18ca14dc087'
          },
          id: 'e3fb9498-9227-4a28-8067-75237cdb0450',
          context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/',
          enabled: true,
          installTime: '2018-05-31T01:45:56.805Z',
          latest: true,
          installerAaid: '655363:14393b36-d70b-4173-90c1-debe961ef9de',
          definitionId: 'ad04c46e-f018-4941-897d-a18ca14dc087',
          key: 'atlassian-cloud-for-gmail-staging-0acc3d46-29e2-4be7-a94c-20260486e49d',
          name: 'Atlassian Cloud for Gmail (staging)',
          description: '',
          baseUrl: 'https://connect.prod.public.atl-paas.net',
          origin: 'dac',
          distributionStatus: 'public',
          application: 'jira',
          scopes: [
            'read:jira-user'
          ],
          version: '1.1.0',
          oauthClientId: 'ijZzZg3u5SNpOghAOiF4nUZ8HcpuiV6m',
          principalId: '5b0f51d278cd734fe39f1c74',
          oauthRedirectUrl: 'https://timboconnect.ngrok.io/oauth_callback',
          avatarUrl: 'https://avatar-cdn.stg.internal.atlassian.com/056cd8c7a46ad0526a9b91d8a825f0d9?by=hash',
          grants: [
            {
              id: '29c9ee66-19e3-4d72-bd81-95ff7b143aa2',
              accountId: '655363:14393b36-d70b-4173-90c1-debe961ef9de',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'ijZzZg3u5SNpOghAOiF4nUZ8HcpuiV6m',
              createdAt: '2018-05-31T01:45:56.831Z',
              type: 'user',
              scopes: [
                'read:jira-work'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            }
          ],
        },
        {
          _links: {
            self: 'https://connect.staging.public.atl-paas.net/addon/installation/9eb273a4-0c4d-434b-85dd-fdb4fa94bdd0',
            definition: 'https://connect.prod.public.atl-paas.net/addon/definition/f3e4abfe-ee5f-4db7-8f6f-929e82c14bd0'
          },
          id: '9eb273a4-0c4d-434b-85dd-fdb4fa94bdd0',
          context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/',
          enabled: true,
          installTime: '2018-05-31T04:24:55.286Z',
          latest: true,
          installerAaid: '655363:9996cc93-dc75-4a1a-b012-4971c25cfcbc',
          definitionId: 'f3e4abfe-ee5f-4db7-8f6f-929e82c14bd0',
          key: 'jira-3lo-test-app-fec5d271-24c0-4c60-8460-21593e17020a',
          name: 'Jira 3LO TEST app',
          description: 'Sort of',
          baseUrl: 'https://connect.prod.public.atl-paas.net',
          origin: 'dac',
          distributionStatus: 'public',
          application: 'jira',
          scopes: [
            'read:jira-user'
          ],
          version: '1.1.0',
          oauthClientId: 'xyxbAgXu7rE2BuFS52RQR0R0O4I30hFN',
          principalId: '5b0f78704c20165700eddf62',
          oauthRedirectUrl: 'https://dsorin.au.ngrok.io/callback',
          avatarUrl: 'https://avatar-cdn.stg.internal.atlassian.com/0303319cc161a0a89a05b681542ae88f?by=hash',
          grants: [
            {
              id: '750bfe85-5634-4bc4-8f6f-2fc08050fc56',
              accountId: '655363:9996cc93-dc75-4a1a-b012-4971c25cfcbc',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'xyxbAgXu7rE2BuFS52RQR0R0O4I30hFN',
              createdAt: '2018-05-31T04:24:55.292Z',
              type: 'user',
              scopes: [
                'write:jira-work'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            }
          ],
        },
        {
          _links: {
            self: 'https://connect.staging.public.atl-paas.net/addon/installation/6dd00a07-27b8-4709-88f4-ed13db6f414e',
            definition: 'https://connect.prod.public.atl-paas.net/addon/definition/7240ff4b-2d00-44ba-9a10-adbe63ea589a'
          },
          id: '6dd00a07-27b8-4709-88f4-ed13db6f414e',
          context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/',
          enabled: true,
          installTime: '2018-06-01T00:42:35.292Z',
          latest: true,
          installerAaid: '655362:2191f688-33d9-4f02-8a11-0fc5cd5054d0',
          definitionId: '7240ff4b-2d00-44ba-9a10-adbe63ea589a',
          key: 'bmc-blitz3-dce0e478-233a-4e96-87f2-d2113e1a94e3',
          name: 'bmc-blitz3',
          description: '',
          baseUrl: 'https://connect.prod.public.atl-paas.net',
          origin: 'dac',
          distributionStatus: 'public',
          application: 'jira',
          scopes: [
            'read:jira-user'
          ],
          version: '1.1.0',
          oauthClientId: 'YR1G3K5Vd7VzgAuFUK1jMFCX5cfDbEFR',
          principalId: '5b109620d9936c6102204d50',
          oauthRedirectUrl: 'https://www.getpostman.com/oauth2/callback',
          avatarUrl: 'https://avatar-cdn.stg.internal.atlassian.com/412649eff0176aa7ccb4c9ce24cee716?by=hash',
        },
        {
          _links: {
            self: 'https://connect.staging.public.atl-paas.net/addon/installation/c377a537-01b0-4802-90c6-61a2a7b9756b',
            definition: 'https://connect.prod.public.atl-paas.net/addon/definition/b82e2c82-7ce3-4f9d-856a-ad965148507d'
          },
          id: 'c377a537-01b0-4802-90c6-61a2a7b9756b',
          context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/',
          enabled: true,
          installTime: '2018-06-01T01:04:27.790Z',
          latest: true,
          installerAaid: '655363:31c5a894-0e13-43e7-a0f3-b6fef062fab8',
          definitionId: 'b82e2c82-7ce3-4f9d-856a-ad965148507d',
          key: '3lo-test-app-95fcab57-f57a-477f-8d9e-0d5bf2f8e5a1',
          name: '3lo test app',
          description: '',
          baseUrl: 'https://connect.prod.public.atl-paas.net',
          origin: 'dac',
          distributionStatus: 'public',
          application: 'jira',
          scopes: [
            'read:jira-user'
          ],
          version: '1.1.0',
          oauthClientId: 'HV7E1NdFN7vUb48HHkYTZS0rfy2SyX2c',
          principalId: '5b109b4a73c23f253a5f7840',
          oauthRedirectUrl: 'https://jfurler.ngrok.io/callback',
          avatarUrl: 'https://avatar-cdn.stg.internal.atlassian.com/4e8998f129407700987162650fdf1cb6?by=hash',
          grants: [
            {
              id: 'a140c234-0371-4dc4-b076-5e63f00d7274',
              accountId: '655363:31c5a894-0e13-43e7-a0f3-b6fef062fab8',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'HV7E1NdFN7vUb48HHkYTZS0rfy2SyX2c',
              createdAt: '2018-06-01T01:09:02.477Z',
              type: 'user',
              scopes: [
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            }
          ],
        },
        {
          _links: {
            self: 'https://connect.staging.public.atl-paas.net/addon/installation/2681170c-9833-42e5-ac8d-1fed6d954ea9',
            definition: 'https://connect.prod.public.atl-paas.net/addon/definition/dec71944-afae-4409-af9a-dcb9e65c9414'
          },
          id: '2681170c-9833-42e5-ac8d-1fed6d954ea9',
          context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/',
          enabled: true,
          installTime: '2018-06-01T01:07:12.853Z',
          latest: true,
          installerAaid: '655363:9996cc93-dc75-4a1a-b012-4971c25cfcbc',
          definitionId: 'dec71944-afae-4409-af9a-dcb9e65c9414',
          key: 'jira-3lo-blitz-app-1-b6999f9f-8fbe-44d4-9aff-367bc2e01e7b',
          name: 'JIRA 3LO blitz app #1',
          description: '',
          baseUrl: 'https://connect.prod.public.atl-paas.net',
          origin: 'dac',
          distributionStatus: 'public',
          application: 'jira',
          scopes: [
            'read:jira-user'
          ],
          version: '1.3.0',
          oauthClientId: 'J5vQMLnvVBxJtPCd9wSSfcnCcW74Fezh',
          principalId: '5b109bf8daa2e712a6d348a6',
          oauthRedirectUrl: 'https://dsorin.au.ngrok.io/callback',
          avatarUrl: 'https://avatar-cdn.stg.internal.atlassian.com/6004a7550eaeadd9815fe55f8543ed0a?by=hash',
          grants: [
            {
              id: '030fd995-236e-463d-812b-90d1a12f878d',
              accountId: '655363:9996cc93-dc75-4a1a-b012-4971c25cfcbc',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'J5vQMLnvVBxJtPCd9wSSfcnCcW74Fezh',
              createdAt: '2018-06-01T01:07:12.883Z',
              type: 'user',
              scopes: [
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            }
          ],
        },
        {
          _links: {
            self: 'https://connect.staging.public.atl-paas.net/addon/installation/9c551149-1ee3-4fd9-83db-066f9b79c5d3',
            definition: 'https://connect.prod.public.atl-paas.net/addon/definition/08f031a8-dddd-4ed2-bed4-0205a3b7abd6'
          },
          id: '9c551149-1ee3-4fd9-83db-066f9b79c5d3',
          context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/',
          enabled: true,
          installTime: '2018-06-01T01:14:06.107Z',
          latest: false,
          installerAaid: '655362:1c0d1b38-3811-47b7-b6fe-93befec88670',
          definitionId: '08f031a8-dddd-4ed2-bed4-0205a3b7abd6',
          key: 'can-i-get-a-jira-in-here-dc99b893-3560-40aa-8ba2-48c5d9542499',
          name: 'Can I get a Jira in here?',
          description: '',
          baseUrl: 'https://connect.prod.public.atl-paas.net',
          origin: 'dac',
          distributionStatus: 'public',
          application: 'jira',
          scopes: [
            'read:jira-user'
          ],
          version: '1.1.0',
          oauthClientId: 'Y1JtC31uR1jUo2LIQdQA5ibZkEGPAevz',
          principalId: '5b109ccd914fd556f9ac1e7a',
          oauthRedirectUrl: 'https://jimi-3lo-test-app.ngrok.io/callback',
          avatarUrl: 'https://avatar-cdn.stg.internal.atlassian.com/03104c708c11ecb878302d31cc2e2bf5?by=hash',
        },
        {
          _links: {
            self: 'https://connect.staging.public.atl-paas.net/addon/installation/8e2878a9-76d7-4aa2-b78f-6723d5257cd9',
            definition: 'https://connect.prod.public.atl-paas.net/addon/definition/b0042c46-75aa-4ea3-8fa6-c05cde86b34e'
          },
          id: '8e2878a9-76d7-4aa2-b78f-6723d5257cd9',
          context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/',
          enabled: true,
          installTime: '2018-06-01T01:18:18.311Z',
          latest: true,
          installerAaid: '655363:b97842a3-cd12-4aa3-ac54-84ba58467471',
          definitionId: 'b0042c46-75aa-4ea3-8fa6-c05cde86b34e',
          key: 'francis-jira-3lo-test-app-4ba34f41-9e0b-4c0c-b77c-6c098a7f9146',
          name: 'Francis 3LO app',
          description: '',
          baseUrl: 'https://connect.prod.public.atl-paas.net',
          origin: 'dac',
          distributionStatus: 'public',
          application: 'jira',
          scopes: [
            'read:jira-user',
            'read:jira-work',
            'write:jira-work',
            'manage:jira-project',
            'manage:jira-configuration'
          ],
          version: '5.4.0',
          oauthClientId: 'GN60ergDtBoas7K0civcAaZOMk86raKV',
          principalId: '5b109dc393816854030df9c4',
          oauthRedirectUrl: 'https://frangel.au.ngrok.io/callback',
          avatarUrl: 'https://avatar-cdn.stg.internal.atlassian.com/56159670dc0ef81a09e4c078290e7e30?by=hash',
        },
        {
          _links: {
            self: 'https://connect.staging.public.atl-paas.net/addon/installation/f49700f5-b891-465a-8df6-62ff47e1afc9',
            definition: 'https://connect.prod.public.atl-paas.net/addon/definition/9fadb882-a2b1-4e20-adaa-52968989fca8'
          },
          id: 'f49700f5-b891-465a-8df6-62ff47e1afc9',
          context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/',
          enabled: true,
          installTime: '2018-06-01T01:38:30.832Z',
          latest: false,
          installerAaid: '655363:9996cc93-dc75-4a1a-b012-4971c25cfcbc',
          definitionId: '9fadb882-a2b1-4e20-adaa-52968989fca8',
          key: 'jira-3lo-test-app-4-55d780cc-559f-47eb-ba3a-c2310806d2bd',
          name: 'JIRA 3LO test app #4',
          description: '',
          baseUrl: 'https://connect.prod.public.atl-paas.net',
          origin: 'dac',
          distributionStatus: 'public',
          application: 'jira',
          scopes: [
            'read:jira-user'
          ],
          version: '1.1.0',
          oauthClientId: 'XS0xX4tTnooIpuGuTsLsft7kTnfRdzfC',
          principalId: '5b10a0a06f64432302074ecc',
          oauthRedirectUrl: 'https://dsorin.au.ngrok.io/callback',
          avatarUrl: 'https://avatar-cdn.stg.internal.atlassian.com/5106f93787f5516367160117d5e62838?by=hash',
          grants: [
            {
              id: 'e9885a88-9600-459b-ac87-49bb29345829',
              accountId: '655363:9996cc93-dc75-4a1a-b012-4971c25cfcbc',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'XS0xX4tTnooIpuGuTsLsft7kTnfRdzfC',
              createdAt: '2018-06-01T01:38:30.838Z',
              type: 'user',
              scopes: [
                'write:jira-work'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            }
          ],
        },
        {
          _links: {
            self: 'https://connect.staging.public.atl-paas.net/addon/installation/3e6bc649-3ca5-4c73-8696-868a26eb309a',
            definition: 'https://connect.prod.public.atl-paas.net/addon/definition/2f6f44ab-cd89-4444-8355-819c8243d1d7'
          },
          id: '3e6bc649-3ca5-4c73-8696-868a26eb309a',
          context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/',
          enabled: true,
          installTime: '2018-06-01T01:41:55.682Z',
          latest: true,
          installerAaid: '655362:bb8d834e-412f-494f-badb-0a8d369f4c28',
          definitionId: '2f6f44ab-cd89-4444-8355-819c8243d1d7',
          key: 'eero-jira-3lo-app-19ff5b85-4b4a-4491-bf1e-590f1cc39449',
          name: 'Eero Jira 3lo App',
          description: 'Eero Jira APp',
          baseUrl: 'https://ekaukonen.ngrok.io',
          origin: 'dac',
          distributionStatus: 'public',
          application: 'jira',
          scopes: [
            'read:jira-user',
            'read:jira-work',
            'manage:jira-project',
            'participate:conversation'
          ],
          version: '12.3.0',
          oauthClientId: 'i3LI4wslLa73P1fmI8iJD8zYfCNfU5W9',
          principalId: '5b109c5cc808774020515057',
          oauthRedirectUrl: 'https://ekaukonen.ngrok.io/callback',
          avatarUrl: 'https://avatar-cdn.stg.internal.atlassian.com/3a79ba202e533b3d9e9486f9cd1ccd7e?by=hash',
          grants: [
            {
              id: '3ab006af-2af3-4580-98b1-82cff5f36c87',
              accountId: '655362:bb8d834e-412f-494f-badb-0a8d369f4c28',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'i3LI4wslLa73P1fmI8iJD8zYfCNfU5W9',
              createdAt: '2018-06-08T06:19:08.645Z',
              type: 'user',
              scopes: [
                'manage:jira-configuration',
                'manage:jira-project',
                'write:jira-work',
                'read:jira-work',
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            }
          ],
        },
        {
          _links: {
            self: 'https://connect.staging.public.atl-paas.net/addon/installation/abf1cecc-7980-412c-a66b-6a64d72d65bd',
            definition: 'https://connect.prod.public.atl-paas.net/addon/definition/ebb41f76-15e5-49c6-9fae-6eda848350de'
          },
          id: 'abf1cecc-7980-412c-a66b-6a64d72d65bd',
          context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/',
          enabled: true,
          installTime: '2018-06-01T02:16:11.720Z',
          latest: true,
          installerAaid: '655362:2191f688-33d9-4f02-8a11-0fc5cd5054d0',
          definitionId: 'ebb41f76-15e5-49c6-9fae-6eda848350de',
          key: 'bmc-blitz5-two-jira-sites-e4ea6050-3e22-4fc9-a647-db9cbd127c26',
          name: 'BMC-blitz5 - two jira Sites',
          description: '',
          baseUrl: 'https://connect.prod.public.atl-paas.net',
          origin: 'dac',
          distributionStatus: 'public',
          application: 'jira',
          scopes: [
            'read:jira-user'
          ],
          version: '1.1.0',
          oauthClientId: 'xa6t4n6tLGYQMVHZ23244lFH0bRuSyci',
          principalId: '5b10ac1dc898a7196b5d178c',
          oauthRedirectUrl: 'https://www.getpostman.com/oauth2/callback',
          avatarUrl: 'https://avatar-cdn.stg.internal.atlassian.com/b2b3b74d28b18663b0036048d6bb35d0?by=hash',
          grants: [
            {
              id: 'd37f4ec8-a816-477f-acc7-4d8a812288d0',
              accountId: '655362:2191f688-33d9-4f02-8a11-0fc5cd5054d0',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'xa6t4n6tLGYQMVHZ23244lFH0bRuSyci',
              createdAt: '2018-06-01T02:33:31.943Z',
              type: 'user',
              scopes: [
                'manage:jira-configuration'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            }
          ],
        },
        {
          _links: {
            self: 'https://connect.staging.public.atl-paas.net/addon/installation/3b90a514-43d1-4508-9380-b691e169a1b1',
            definition: 'https://connect.prod.public.atl-paas.net/addon/definition/e789310e-b7f5-4274-8096-249678ec7d82'
          },
          id: '3b90a514-43d1-4508-9380-b691e169a1b1',
          context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/',
          enabled: true,
          installTime: '2018-06-01T05:42:16.440Z',
          latest: true,
          installerAaid: '655363:14393b36-d70b-4173-90c1-debe961ef9de',
          definitionId: 'e789310e-b7f5-4274-8096-249678ec7d82',
          key: 'atlassian-cloud-for-gmail-tpettersen-3974b1a9-0068-47e3-8e9e-485184c4549a',
          name: 'Atlassian Cloud for Gmail (tpettersen)',
          description: '',
          baseUrl: 'https://connect.prod.public.atl-paas.net',
          origin: 'dac',
          distributionStatus: 'public',
          application: 'jira',
          scopes: [
            'read:jira-user'
          ],
          version: '1.3.0',
          oauthClientId: 'IvC8OMlUvCA3HCZ8k9KWCQ7JAHLQ60E3',
          principalId: '5b10c12edcc6e72719f68b75',
          oauthRedirectUrl: 'https://script.google.com/macros/d/1Te5gLkH2bJgOxK1-MBe5bD9KLUT624FFQMWvwUxauMMt8U_dTQRt1DGE/usercallback',
          avatarUrl: 'https://avatar-cdn.stg.internal.atlassian.com/9954a7735a0acb822a1549b216703aa3?by=hash',
          grants: [
            {
              id: '60ab929d-c278-4d12-b3a8-690f7a2e4b58',
              accountId: '655363:14393b36-d70b-4173-90c1-debe961ef9de',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'IvC8OMlUvCA3HCZ8k9KWCQ7JAHLQ60E3',
              createdAt: '2018-06-01T05:42:16.475Z',
              type: 'user',
              scopes: [
                'read:jira-work'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            }
          ],
        },
        {
          _links: {
            self: 'https://connect.staging.public.atl-paas.net/addon/installation/77f23ab3-4170-4004-b851-9836bbaa2a64',
            definition: 'https://connect.prod.public.atl-paas.net/addon/definition/aabc7a92-8d04-4a75-a560-9792823e0038'
          },
          id: '77f23ab3-4170-4004-b851-9836bbaa2a64',
          context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/',
          enabled: true,
          installTime: '2018-06-12T04:44:22.761Z',
          latest: true,
          installerAaid: '655362:ab0f81b5-bf6f-41a6-b527-588145720bc7',
          definitionId: 'aabc7a92-8d04-4a75-a560-9792823e0038',
          key: 'dblack-testing-3lo-3bd33409-6d7c-489c-9e76-0a3abfd5b9dd',
          name: 'dblack-testing-3lo\\u0022"xxxstring.xxxxfffff\t&#10;',
          description: 'dblack-testing-3lo\\u0022"xxxx%0D%0Axxxxfffff',
          baseUrl: 'https://connect.prod.public.atl-paas.net',
          origin: 'dac',
          distributionStatus: 'public',
          application: 'jira',
          scopes: [
            'participate:conversation',
            'read:jira-user',
            'read:jira-work',
            'write:jira-work'
          ],
          version: '8.8.0',
          oauthClientId: 'n8LHAr2CYv7LmUMeAz1ylSx7rJbtu0FT',
          principalId: '5b1f4bb602cfea1ba641064e',
          oauthRedirectUrl: 'https://8f6f41e4.ngrok.io/callback',
          avatarUrl: 'https://avatar-cdn.stg.internal.atlassian.com/12c0db5bdfaaa17350bb9bd8d7413c77?by=hash',
          grants: [
            {
              id: 'cd47cd5b-0556-4c8c-9775-55d346d00db5',
              accountId: '655362:ab0f81b5-bf6f-41a6-b527-588145720bc7',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'n8LHAr2CYv7LmUMeAz1ylSx7rJbtu0FT',
              createdAt: '2018-06-12T04:44:22.779Z',
              type: 'user',
              scopes: [
                'read:jira-work'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            }
          ],
        },
        {
          _links: {
            self: 'https://connect.staging.public.atl-paas.net/addon/installation/89800577-2d28-4d01-bf74-92c991045393',
            definition: 'https://connect.prod.public.atl-paas.net/addon/definition/53493419-99fe-490d-aaf2-02415cc6ca12'
          },
          id: '89800577-2d28-4d01-bf74-92c991045393',
          context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/',
          enabled: true,
          installTime: '2018-06-26T03:25:16.625Z',
          latest: true,
          installerAaid: '655363:d37f46c1-191f-4f82-90af-9a517fa191e2',
          definitionId: '53493419-99fe-490d-aaf2-02415cc6ca12',
          key: 'jira-blitz-7786fa30-26b5-4e3e-962e-0e32bd9e18ae',
          name: 'Jira blitz',
          description: '',
          baseUrl: 'https://connect.prod.public.atl-paas.net',
          origin: 'dac',
          distributionStatus: 'public',
          application: 'jira',
          scopes: [
            'read:jira-user'
          ],
          version: '1.1.0',
          oauthClientId: 'K4qdyZ0uqnDaAKtGpjW8sYtc3PY3tpFG',
          principalId: '5b31b1a93107e84c4f645d7a',
          oauthRedirectUrl: 'https://d4612836.au.ngrok.io/callback',
          avatarUrl: 'https://avatar-cdn.stg.internal.atlassian.com/fc8b0e19349b66d2862757da67f4aec2?by=hash',
        },
        {
          _links: {
            self: 'https://connect.staging.public.atl-paas.net/addon/installation/9fc4b9d4-03cc-47d1-af87-f571856cf897',
            definition: 'https://connect.prod.public.atl-paas.net/addon/definition/877472bd-01e0-43e8-916f-a42c53593ebf'
          },
          id: '9fc4b9d4-03cc-47d1-af87-f571856cf897',
          context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/',
          enabled: true,
          installTime: '2018-06-26T05:20:50.687Z',
          latest: true,
          installerAaid: '655362:ab9ab4c0-9770-4c92-b972-47e1e031140b',
          definitionId: '877472bd-01e0-43e8-916f-a42c53593ebf',
          key: 'jeeera3loblitz2-4b59aa24-f31f-46c1-a09e-89e05021c87d',
          name: 'jeeera3LOBlitz2',
          description: '',
          baseUrl: 'https://connect.prod.public.atl-paas.net',
          origin: 'dac',
          distributionStatus: 'public',
          application: 'jira',
          scopes: [
            'read:jira-user'
          ],
          version: '1.1.0',
          oauthClientId: '6xO17OyAGIslMQo0fDJNRvJR83Q8Dagk',
          principalId: '5b31cbe3442d5666d9fc002d',
          oauthRedirectUrl: 'https://aholmgren.ngrok.io/callback',
          avatarUrl: 'https://avatar-cdn.stg.internal.atlassian.com/898e57663214cd0148f27407e492e4b9?by=hash',
        },
        {
          _links: {
            self: 'https://connect.staging.public.atl-paas.net/addon/installation/f90d1c30-734f-4b9b-8713-1998e276f6b2',
            definition: 'https://connect.prod.public.atl-paas.net/addon/definition/068bf60d-3669-4d49-b744-4c3a27eaa6c0'
          },
          id: 'f90d1c30-734f-4b9b-8713-1998e276f6b2',
          context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/',
          enabled: true,
          installTime: '2018-06-26T05:21:45.636Z',
          latest: true,
          installerAaid: '655363:b97842a3-cd12-4aa3-ac54-84ba58467471',
          definitionId: '068bf60d-3669-4d49-b744-4c3a27eaa6c0',
          key: 'jira-blitz-2-francis-7026db9e-fa0a-4260-8737-8a3894a600fa',
          name: 'Jira Blitz #2 Francis',
          description: '',
          baseUrl: 'https://connect.prod.public.atl-paas.net',
          origin: 'dac',
          distributionStatus: 'public',
          application: 'jira',
          scopes: [
            'read:jira-user'
          ],
          version: '1.1.0',
          oauthClientId: 'M46i5QmYt4r6Fw7PBFzJOpCFLq3ylgRb',
          principalId: '5b31cbcdfc76f66e7d598e55',
          oauthRedirectUrl: 'https://frangel.au.ngrok.io/callback',
          avatarUrl: 'https://avatar-cdn.stg.internal.atlassian.com/5aab8abc56f24b50e0148ed060fce440?by=hash',
          grants: [
            {
              id: '25fd5352-d5a9-4947-9af7-6aa0fee8354d',
              accountId: '5afcc36aa0e6e6142d1faec2',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'M46i5QmYt4r6Fw7PBFzJOpCFLq3ylgRb',
              createdAt: '2018-06-26T05:44:28.532Z',
              type: 'user',
              scopes: [
                'write:jira-work'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            }
          ],
        },
        {
          _links: {
            self: 'https://connect.staging.public.atl-paas.net/addon/installation/2d808b83-74a9-4376-8831-9a07e2c65359',
            definition: 'https://connect.prod.public.atl-paas.net/addon/definition/eefa2daf-3839-4f78-b25c-295f8a49c465'
          },
          id: '2d808b83-74a9-4376-8831-9a07e2c65359',
          context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/',
          enabled: true,
          installTime: '2018-06-26T05:51:17.939Z',
          latest: true,
          installerAaid: '655363:b97842a3-cd12-4aa3-ac54-84ba58467471',
          definitionId: 'eefa2daf-3839-4f78-b25c-295f8a49c465',
          key: 'jira-blitz-2-francis-2-c5939286-f1ae-4cf8-a71e-8f10255a3fb1',
          name: 'Jira Blitz #2 Francis 2',
          description: '',
          baseUrl: 'https://connect.prod.public.atl-paas.net',
          origin: 'dac',
          distributionStatus: 'public',
          application: 'jira',
          scopes: [
            'read:jira-user'
          ],
          version: '1.1.0',
          oauthClientId: 'A35AN8p489hg4Nh5PKbWU1I63BsmvLq9',
          principalId: '5b31d4121ee98e6e7e05f63e',
          oauthRedirectUrl: 'https://frangel.au.ngrok.io/callback',
          avatarUrl: 'https://avatar-cdn.stg.internal.atlassian.com/5378a067ef85352cd20af1bbc62ce205?by=hash',
        },
        {
          _links: {
            self: 'https://connect.staging.public.atl-paas.net/addon/installation/553573a8-a60e-406b-a938-594e677c18db',
            definition: 'https://connect.prod.public.atl-paas.net/addon/definition/6cec740e-5d79-4f92-ab64-58fe1a38bb5b'
          },
          id: '553573a8-a60e-406b-a938-594e677c18db',
          context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/',
          enabled: true,
          installTime: '2018-06-26T06:07:06.127Z',
          latest: false,
          installerAaid: '5afcc36aa0e6e6142d1faec2',
          definitionId: '6cec740e-5d79-4f92-ab64-58fe1a38bb5b',
          key: 'test-staging-26cd27f5-2a7a-462f-9597-60e880573a81',
          name: 'Test Staging',
          description: 'some looooong random description...........',
          baseUrl: 'https://connect.prod.public.atl-paas.net',
          origin: 'dac',
          distributionStatus: 'public',
          application: 'jira',
          scopes: [
            'read:jira-user'
          ],
          version: '1.4.0',
          oauthClientId: 'M30TxbeddYCs2imXXIKHWb42FNYHwCll',
          principalId: '5b31ca3e6f00c836f6505b10',
          oauthRedirectUrl: 'https://df305c10.ngrok.io/callback',
          avatarUrl: 'https://avatar-cdn.stg.internal.atlassian.com/1710760e6550e61ac51c3d23723d09a0?by=hash',
          outboundAuthContainerId: '5929e1ff-8523-4d84-8f7f-60528f053d72',
          grants: [
            {
              id: 'f705c16a-d3f9-4c98-80df-fbad0c6a2719',
              accountId: '5afcc36aa0e6e6142d1faec2',
              audience: 'api.stg.atlassian.com',
              oauthClientId: 'M30TxbeddYCs2imXXIKHWb42FNYHwCll',
              createdAt: '2018-06-26T06:07:06.148Z',
              type: 'user',
              scopes: [
                'read:jira-user'
              ],
              context: 'ari:cloud:jira:38b7be26-03f7-42a9-b18b-9cdb56d204c2:project/'
            }
          ],
        },
      ],
      start: 0,
      limit: 20,
      size: 29,
    };
  },
};
