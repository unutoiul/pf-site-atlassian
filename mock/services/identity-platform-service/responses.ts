const memberTokens = [
  {
    id: 'api-token-id-1',
    label: 'Alex\'s Api-token 1',
    createdAt: '2019-01-03T00:11:46.608Z',
    lastAccess: '2019-01-05T00:10:46.768Z',
  },
  {
    id: 'api-token-id-2',
    label: 'Alex\'s Api-token 2',
    createdAt: '2019-01-02T00:13:46.712Z',
    lastAccess: '',
  },
];

export const responses = {
  '/gateway/api/users/:memberId/manage/api-tokens': memberTokens,
};
