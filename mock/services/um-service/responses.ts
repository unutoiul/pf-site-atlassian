import { csatEvalCohortFlagKey, trustedUsersFeatureFlagKey } from '../../../src/site/feature-flags/flag-keys';

export const accessResponse = {
  useAccessConfig: [{ product: { productId: 'jira-servicedesk', canonicalProductKey: 'jira-servicedesk', productName: 'Jira Service Desk' }, groups: [{ id: 'c10ae462-c5b4-4e80-b3bc-6e09e8de35a62', name: 'atlassian-addons-admin', requiresApproval: true, productPermission: ['WRITE', 'MANAGE'], default: false }, { id: '2ef48b79-607e-467f-a189-5bd408882f5c', name: 'Cookies', requiresApproval: false, productPermission: ['WRITE'], default: false }, { id: '30cf684a-e8cc-4611-a4d0-432330b6b7a6', name: 'jira-servicedesk-users', requiresApproval: false, productPermission: ['WRITE'], default: true }, { id: '42a693cf-2b96-421a-81f4-85671f3319a4', name: 'system-administrators', requiresApproval: false, productPermission: ['WRITE', 'MANAGE'], default: false }] }, { product: { productId: 'jira-software', canonicalProductKey: 'jira-software', productName: 'Jira Software' }, groups: [{ id: 'c10ae462-c5b4-4e80-b3bc-6e09e8de35a6', name: 'atlassian-addons-admin', requiresApproval: false, productPermission: ['WRITE', 'MANAGE'], default: false }, { id: '77f0566d-78d7-478f-bfce-819e55bc7225', name: 'sample', requiresApproval: false, productPermission: ['WRITE'], default: true }, { id: '42a693cf-2b96-421a-81f4-85671f3319a4', name: 'system-administrators', requiresApproval: false, productPermission: ['WRITE', 'MANAGE'], default: false }] }, { product: { productId: 'hipchat.cloud', canonicalProductKey: 'chat', productName: 'Stride' }, groups: [{ id: 'c10ae462-c5b4-4e80-b3bc-6e09e8de35a6', name: 'atlassian-addons-admin', requiresApproval: false, productPermission: ['WRITE', 'MANAGE'], default: false }, { id: '77f0566d-78d7-478f-bfce-819e55bc7225', name: 'sample', requiresApproval: false, productPermission: ['WRITE'], default: true }, { id: '42a693cf-2b96-421a-81f4-85671f3319a4', name: 'system-administrators', requiresApproval: false, productPermission: ['WRITE', 'MANAGE'], default: false }] }, { product: { productId: 'jira-core', canonicalProductKey: 'jira-core', productName: 'Jira Core' }, groups: [{ id: 'c10ae462-c5b4-4e80-b3bc-6e09e8de35a6', name: 'atlassian-addons-admin', requiresApproval: false, productPermission: ['WRITE', 'MANAGE'], default: false }, { id: '16cdbdb5-9f1d-44d9-b1ec-8fe1696b8418', name: 'foo', requiresApproval: false, productPermission: ['WRITE'], default: false }, { id: '60722c1f-59b0-4b66-91a0-e38b072b74db', name: 'jira-core-users-testing', requiresApproval: false, productPermission: ['WRITE'], default: true }, { id: '42a693cf-2b96-421a-81f4-85671f3319a4', name: 'system-administrators', requiresApproval: false, productPermission: ['WRITE', 'MANAGE'], default: false }] }, { product: { productId: 'conf', canonicalProductKey: 'confluence', productName: 'Confluence' }, groups: [{ id: 'c10ae462-c5b4-4e80-b3bc-6e09e8de35a6', name: 'atlassian-addons-admin', requiresApproval: false, productPermission: ['WRITE', 'MANAGE'], default: false }, { id: 'd76e4102-5622-4d3d-a96a-8c319f121686', name: 'confluence-users', requiresApproval: false, productPermission: ['WRITE'], default: true }, { id: '42a693cf-2b96-421a-81f4-85671f3319a4', name: 'system-administrators', requiresApproval: false, productPermission: ['WRITE', 'MANAGE'], default: false }] }],
  adminAccessConfig: [{ product : { productId: 'hipchat.cloud', canonicalProductKey: 'chat', productName: 'Stride' }, groups: [{ id: 'c10ae462-c5b4-4e80-b3bc-6e09e8de35a6', name: 'atlassian-addons-admin', requiresApproval: false, productPermission: ['WRITE', 'MANAGE'], default: false }, { id: '2963ed7c-27b1-4618-8533-98078cf7f64a', name: 'stride-administrators', requiresApproval: false, productPermission: ['MANAGE'], default: false }, { id: '42a693cf-2b96-421a-81f4-85671f3319a4', name: 'Super Super Super Super Super Super Super Super Super Super Super Super Super Super Super Super ', requiresApproval: false, productPermission: ['WRITE', 'MANAGE'], default: false }] }, { product: { productId: 'jira-admin', canonicalProductKey: null, productName: 'Jira' }, groups: [{ id: 'c10ae462-c5b4-4e80-b3bc-6e09e8de35a6', name: 'atlassian-addons-admin', requiresApproval: false, productPermission: ['WRITE', 'MANAGE'], default: false }, { id: '187ddd76-03ab-484c-b932-beef8f223d90', name: 'jira-administrators', requiresApproval: false, productPermission: ['MANAGE'], default: false }, { id: '42a693cf-2b96-421a-81f4-85671f3319a4', name: 'system-administrators', requiresApproval: false, productPermission: ['WRITE', 'MANAGE'], default: false }] }, { product: { productId: 'conf', canonicalProductKey: 'confluence', productName: 'Confluence' }, groups: [{ id: 'c10ae462-c5b4-4e80-b3bc-6e09e8de35a6', name: 'atlassian-addons-admin', requiresApproval: false, productPermission: ['WRITE', 'MANAGE'], default: false }, { id: '86821c21-5055-4253-80b6-6659ddcbeafe', name: 'site-admins', requiresApproval: false, productPermission: ['MANAGE'], default: false }, { id: '42a693cf-2b96-421a-81f4-85671f3319a4', name: 'system-administrators', requiresApproval: false, productPermission: ['WRITE', 'MANAGE'], default: false }] }],
};

export const usersListResponse = {
  total: 7,
  users: [
    {
      id: '1',
      displayName: 'Hello - Enabled User',
      email: 'hello@atlassian.com',
      active: true,
      presence: '2017-07-10T14:00:00Z',
      activeStatus: 'ENABLED',
      system: false,
      hasVerifiedEmail: true,
    },
    {
      id: '2',
      displayName: 'Hello - Disabled User',
      email: 'hello@atlassian.com',
      active: false,
      presence: null,
      activeStatus: 'DISABLED',
      system: false,
      hasVerifiedEmail: true,
    },
    {
      id: '3',
      displayName: 'Hello - Blocked user',
      email: 'hello@atlassian.com',
      active: false,
      presence: '2017-07-10T14:00:00Z',
      activeStatus: 'BLOCKED',
      system: false,
      hasVerifiedEmail: true,
    },
    {
      id: '4',
      displayName: 'Hello - Invited user',
      email: 'hello@atlassian.com',
      active: true,
      presence: null,
      activeStatus: 'ENABLED',
      system: false,
      hasVerifiedEmail: true,
    },
    {
      id: '5',
      displayName: 'Hello - Enabled admin user',
      email: 'hello@atlassian.com',
      active: true,
      presence: '2017-07-10T14:00:00Z',
      activeStatus: 'ENABLED',
      siteAdmin: true,
      system: false,
      hasVerifiedEmail: true,
    },
    {
      id: '6',
      displayName: 'Hello - Enabled Trusted User',
      email: 'hello@atlassian.com',
      active: true,
      presence: '2017-07-10T14:00:00Z',
      activeStatus: 'ENABLED',
      system: false,
      hasVerifiedEmail: true,
      trustedUser: true,
    },
    {
      id: '557057:26ead946-4955-46ef-a11e-b1f9ae987de8',
      displayName: 'Current User',
      email: 'currentuser@atlassian.com',
      active: true,
      presence: '2017-07-10T14:00:00Z',
      isSiteAdmin: true,
      activeStatus: 'ENABLED',
      system: false,
      hasVerifiedEmail: true,
    },
  ],
};

const usersInviteResponse = {};

const scimGroupDetailResponse = {
  id: '96f3aeb2-654e-4295-acc4-54aa50f463a6',
  name: 'Externally Synced Group',
  unmodifiable: true,
  description: '',
  productPermissions: [],
  defaultForProducts: [],
  sitePrivilege: 'SITE_ADMIN',
  userTotal: 20,
  managementAccess: 'READ_ONLY',
  ownerType: 'EXT_SCIM',
};

const siteAdminGroupDetailResponse = {
  id: '0d2e6eeb-c953-4088-90b4-b3f60997df72',
  name: 'site-admins',
  unmodifiable: true,
  description: 'Grants access to all applications, their administration features and Site administration, which includes managing users and bills',
  productPermissions: [
    {
      productName: 'Confluence',
      productId: 'confluence.ondemand',
      permissions: [
        'WRITE',
        'MANAGE',
      ],
    },
    {
      productName: 'Jira Service Desk',
      productId: 'jira-servicedesk.ondemand',
      permissions: [
        'WRITE',
      ],
    },
    {
      productName: 'Jira Software',
      productId: 'jira-software.ondemand',
      permissions: [
        'WRITE',
      ],
    },
    {
      productName: 'Jira Core',
      productId: 'jira-core.ondemand',
      permissions: [
        'WRITE',
        'MANAGE',
      ],
    },
  ],
  defaultForProducts: [],
  sitePrivilege: 'SITE_ADMIN',
  userTotal: 999,
  managementAccess: 'CANNOT_DELETE',
  ownerType: null,
};

export const groupListResponse = {
  total: 8,
  groups: [
    {
      id: 'b62e53bd-1c6b-48d1-a15d-a2500b74fabb',
      name: 'administrators',
      unmodifiable: false,
      description: 'Grants access to all applications and their administration features (excluding Site administration)',
      productPermissions: [
        {
          productName: 'Confluence',
          productId: 'confluence.ondemand',
          permissions: [
            'WRITE',
            'MANAGE',
          ],
        },
        {
          productName: 'Jira Service Desk',
          productId: 'jira-servicedesk.ondemand',
          permissions: [
            'WRITE',
          ],
        },
        {
          productName: 'Jira Software',
          productId: 'jira-software.ondemand',
          permissions: [
            'WRITE',
          ],
        },
        {
          productName: 'Jira Core',
          productId: 'jira-core.ondemand',
          permissions: [
            'WRITE',
            'MANAGE',
          ],
        },
      ],
      defaultForProducts: [],
      sitePrivilege: 'NONE',
      userTotal: 10,
      managementAccess: 'ALL',
      ownerType: null,
    },
    {
      id: 'a0db723c-f47a-47fe-b3d0-67cf93cd6a11',
      name: 'confluence-users',
      unmodifiable: false,
      description: 'Grants access to Confluence',
      productPermissions: [
        {
          productName: 'Confluence',
          productId: 'confluence.ondemand',
          permissions: [
            'WRITE',
          ],
        },
      ],
      defaultForProducts: [
        {
          productName: 'Confluence',
          productId: 'confluence.ondemand',
        },
      ],
      sitePrivilege: 'NONE',
      userTotal: 5,
      managementAccess: 'ALL',
      ownerType: null,
    },
    {
      id: '6d0430d7-65cf-4c08-9a3c-16baabc2c26c',
      name: 'jira-administrators',
      unmodifiable: false,
      description: 'Grant access to the administration features of Jira',
      productPermissions: [
        {
          productName: 'Jira Core',
          productId: 'jira-core.ondemand',
          permissions: [
            'MANAGE',
          ],
        },
      ],
      defaultForProducts: [],
      sitePrivilege: 'NONE',
      userTotal: 0,
      managementAccess: 'ALL',
      ownerType: null,
    },
    {
      id: '63d142cf-1983-47e9-b6eb-e609b360d9a1',
      name: 'jira-core-users',
      unmodifiable: false,
      description: 'Grants access to Jira Core',
      productPermissions: [
        {
          productName: 'Jira Core',
          productId: 'jira-core.ondemand',
          permissions: [
            'WRITE',
          ],
        },
      ],
      defaultForProducts: [
        {
          productName: 'Jira Core',
          productId: 'jira-core.ondemand',
        },
      ],
      sitePrivilege: 'NONE',
      userTotal: 1500,
      managementAccess: 'ALL',
      ownerType: null,
    },
    {
      id: 'c0620d4e-a334-43f3-9468-a086d0709213',
      name: 'jira-servicedesk-users',
      unmodifiable: false,
      description: 'Grants access to Jira Service Desk',
      productPermissions: [
        {
          productName: 'Jira Service Desk',
          productId: 'jira-servicedesk.ondemand',
          permissions: [
            'WRITE',
          ],
        },
      ],
      defaultForProducts: [
        {
          productName: 'Jira Service Desk',
          productId: 'jira-servicedesk.ondemand',
        },
      ],
      sitePrivilege: 'NONE',
      userTotal: 295,
      managementAccess: 'ALL',
      ownerType: null,
    },
    {
      id: '90ff60fd-3508-4066-975c-d46490e50b44',
      name: 'jira-software-users',
      unmodifiable: false,
      description: 'Grants access to Jira Software',
      productPermissions: [
        {
          productName: 'Jira Software',
          productId: 'jira-software.ondemand',
          permissions: [
            'WRITE',
          ],
        },
      ],
      defaultForProducts: [
        {
          productName: 'Jira Software',
          productId: 'jira-software.ondemand',
        },
      ],
      sitePrivilege: 'NONE',
      userTotal: 1,
      managementAccess: 'ALL',
      ownerType: null,
    },
    siteAdminGroupDetailResponse,
    {
      id: '0d2e6eeb-c953-4088-90b4-b3f60997df71',
      name: 'system-admins',
      unmodifiable: true,
      description: 'Grants access to all applications, their administration features and Site administration, which includes managing users and bills',
      productPermissions: [],
      defaultForProducts: [],
      sitePrivilege: 'SYS_ADMIN',
      userTotal: 999,
      managementAccess: 'CANNOT_DELETE',
      ownerType: null,
    },
    scimGroupDetailResponse,
  ],
};

export const groupDetailResponse = {
  description: 'This is a really long description for a group that could potentially wrap over multiple lines if given the opportunity to do so.',
  id: 'some-gibberish-group-id',
  name: 'Really fancy group name',
  productPermissions: [
    {
      productName: 'Confluence',
      productId: 'confluence.ondemand',
      permissions: [
        'WRITE',
      ],
    },
    {
      productName: 'Jira Service Desk',
      productId: 'jira-servicedesk.ondemand',
      permissions: [
        'WRITE',
      ],
    },
    {
      productName: 'Jira Software',
      productId: 'jira-software.ondemand',
      permissions: [
        'WRITE',
      ],
    },
    {
      productName: 'Jira Core',
      productId: 'jira-core.ondemand',
      permissions: [
        'WRITE',
      ],
    },
  ],
  defaultForProducts: [
    {
      productName: 'Jira Core',
      productId: 'jira-core.ondemand',
    },
  ],
  sitePrivilege: 'NONE',
  unmodifiable: false,
  managementAccess: 'ALL',
  ownerType: null,
};

export const groupMembersResponse = {
  total: 10,
  users: [
    { id: '557057:80abfbd6-e1b5-4f23-99d9-077658050e25', displayName: 'Alex Simkin', active: true, email: 'asimkin@atlassian.com', system: false, hasVerifiedEmail: true },
    { id: '557057:4a24f93a-04b0-44ab-93f4-424b02d8dcb7', displayName: 'Alexander Minozhenko', active: true, email: 'aminozhenko@atlassian.com', system: false, hasVerifiedEmail: true },
    { id: '557057:57e7be63-e1cc-4a6f-b597-d61171415df6', displayName: 'Angel Bartomeu Bonillo', active: true, email: 'abartomeubonillo@atlassian.com', system: false, hasVerifiedEmail: true },
    { id: '557057:5bc5f1a2-ba3f-472f-aeef-7eb41a9fba97', displayName: 'Dipanjan Laha', active: true, email: 'dlaha@atlassian.com', system: false, hasVerifiedEmail: true },
    { id: '557057:e095c181-83c3-4f79-acfb-d947e6ebca33', displayName: 'Helen Xue', active: false, email: 'hxue@atlassian.com', system: false, hasVerifiedEmail: true },
    { id: '557057:708a28cd-b518-427a-a4c7-821d467feb3b', displayName: ' I told you all this before, when you have a swimming pool do not use chlorine, use salt water, the healing, salt water is the healing.', active: true, email: 'reallyreallyreallyreallyreallyreallyreallyreallyreallyreallyreallyreallylongemail@atlassian.com', system: true, hasVerifiedEmail: true },
    { id: '557057:ce58bfaa-23be-4ac7-bb69-ac24aca0711e', displayName: 'Rachel De Paula Cavalcanti', active: true, email: 'rdepaulacavalcanti@atlassian.com', system: false, hasVerifiedEmail: true },
    { id: '557057:b03d7440-663b-4c78-850d-5fc0016a69df', displayName: 'Saxon Bruce', active: false, email: 'sbruce@atlassian.com', system: false, hasVerifiedEmail: true },
    { id: '557058:7ee82aa9-e4f9-49a3-9673-f03ad05c7cd0', displayName: 'test test', active: true, email: 'test@example.com', system: false, hasVerifiedEmail: true },
    { id: '557057:26ead946-4955-46ef-a11e-b1f9ae987de8', displayName: 'Current User', active: true, email: 'current-user@example.com', system: false, hasVerifiedEmail: true },
  ],
};

export const responses = {
  '/gateway/api/me': {
    account_id: '557057:26ead946-4955-46ef-a11e-b1f9ae987de8',
    email: 'current-user@example.com',
  },
  '/admin/rest/domain-claim-service/api/getDomainClaims': {
    domainClaims: [],
  },
  '/admin/rest/um/1/apps/navigation': [
    {
      id: '812ec203-ee00-303a-80d8-47d7c3d0ace6',
      homeUrl: 'secure/MyJiraHome.jspa',
      adminUrl: 'secure/project/ViewProjects.jspa',
      name: 'MyProduct1',
      hostType: 'product1',
    },
    {
      id: '1363002c-4b37-303f-b9e3-06734d54989a',
      homeUrl: 'wiki',
      adminUrl: 'wiki/admin/viewgeneralconfig.action',
      name: 'MyProduct2',
      hostType: 'product2',
    },
    {
      id: 'd39f2918-9381-4b87-a938-cccdf6c31bbf',
      homeUrl: 'https://app.stride.com',
      adminUrl: 'http://app.stride.com',
      name: 'Stride',
      hostType: 'hipchat.cloud',
    },
  ],
  '/admin/rest/um/1/context': {
    isAaEnabled: true,
    products: {
      'jira-servicedesk.ondemand': '',
    },
    firstActivationDate: 327225600000,
  },
  '/api/feedback': {
    ok: true,
  },
  '/admin/rest/um/1/group/search': [
    {
      name: 'jira-software-users',
      active: true,
    },
  ],
  '/admin/rest/um/1/group/user/direct': [
    {
      name: 'lhunt',
      'display-name': 'Lachlan Hunt',
      email: 'lhunt@example.com',
      attributes: {
        attributes: [
          {
            name: 'atlassianid.openid.identity',
            values: ['https://id.atlassian.com/openid/v2/u/1'],
          },
        ],
      },
    },
    {
      name: 'awakeling',
      'display-name': 'Andrew Wakeling',
      email: 'awakeling@example.com',
      attributes: {
        attributes: [
          {
            name: 'atlassianid.openid.identity',
            values: ['https://id.atlassian.com/openid/v2/u/2'],
          },
        ],
      },
    },
    {
      name: 'ahammond',
      'display-name': 'Andrew Hammond',
      email: 'ahammond@example.com',
      attributes: {
        attributes: [
          {
            name: 'atlassianid.openid.identity',
            values: ['https://id.atlassian.com/openid/v2/u/3'],
          },
        ],
      },
    },
    {
      name: 'mtruong',
      'display-name': 'Michael Truong',
      email: 'mtruong@example.com',
      attributes: {
        attributes: [
          {
            name: 'atlassianid.openid.identity',
            values: ['https://id.atlassian.com/openid/v2/u/4'],
          },
        ],
      },
    },
    {
      name: 'gburrows',
      'display-name': 'George Burrows',
      email: 'gburrows@example.com',
      attributes: {
        attributes: [
          {
            name: 'atlassianid.openid.identity',
            values: ['https://id.atlassian.com/openid/v2/u/5'],
          },
        ],
      },
    },
  ],
  '/admin/rest/um/1/group': {
    ok: true,
  },
  '/gateway/api/adminhub/um/site/:cloudId/feature-flags': {
    'identity.user_management.adg3.self.signup': true,
    'identity.user_management.adg3.access.config': true,
    'identity.user_management.request_access.enabled': true,
    'identity.user_management.invite_urls.enabled': true,
    'identity.user_management.adg3.users': true,
    'identity.user_management.user.export.adg3': true,
    'identity.user_management.site.admin.page.migration.redirect': false,
  },
  '/gateway/api/adminhub/um/site/:cloudId/signup-options': {
    signupEnabled: true,
    openInvite: false,
    domains: ['acme.com', 'acme.com.au', 'acme.co.uk'],
    notifyAdmin: true,
  },
  '/gateway/api/adminhub/um/site/:cloudId/product/defaults': {
    products: [
      {
        productId: 'jira-servicedesk',
        canonicalProductKey: 'jira-servicedesk',
        productName: 'Jira Service Desk',
      },
      {
        productId: 'jira-software',
        canonicalProductKey: 'jira-software',
        productName: 'Jira Software',
      },
      {
        productId: 'conf',
        canonicalProductKey: 'confluence',
        productName: 'Confluence',
      },
    ],
  },
  '/gateway/api/adminhub/um/site/:cloudId/groups': groupListResponse,
  '/gateway/api/adminhub/um/site/:cloudId/users': usersListResponse,
  '/gateway/api/adminhub/um/site/:cloudId/users/invite': usersInviteResponse,
  '/gateway/api/adminhub/um/site/:cloudId/groups/0d2e6eeb-c953-4088-90b4-b3f60997df72': siteAdminGroupDetailResponse,
  '/gateway/api/adminhub/um/site/:cloudId/groups/96f3aeb2-654e-4295-acc4-54aa50f463a6': scimGroupDetailResponse,
  '/gateway/api/adminhub/um/site/:cloudId/groups/:groupId': groupDetailResponse,
  '/gateway/api/adminhub/um/site/:cloudId/groups/:groupId/members': groupMembersResponse,
  '/gateway/api/adminhub/um/site/:cloudId/groups/:groupId/users/:userId': {},
  '/gateway/api/adminhub/um/site/:cloudId/product/access-config/use': {
    useAccessConfig: accessResponse.useAccessConfig,
  },
  '/gateway/api/adminhub/um/site/:cloudId/product/access-config/admin': {
    adminAccessConfig: accessResponse.adminAccessConfig,
  },
  '/admin/rest/um/1/features': {
    [csatEvalCohortFlagKey]: true,
    [trustedUsersFeatureFlagKey]: true,
  },
  '/gateway/api/adminhub/um/site/:cloudId/products': [
    {
      productId: 'conf',
      productName: 'Confluence',
    },
    {
      productId: 'jira-servicedesk',
      productName: 'Jira Service Desk',
    },
    {
      productId: 'hipchat.cloud',
      productName: 'Stride',
    },
  ],
  '/gateway/api/adminhub/um/site/:cloudId/users/add-to-groups': {},
  '/gateway/api/adminhub/um/site/:cloudId/users/:userId': {
    id: '557058:7ee82aa9-e4f9-49a3-9673-f03ad05c7cd0',
    email: 'test@example.com',
    displayName: 'Example User',
    active: true,
    activeStatus: 'BLOCKED',
    nickname: 'testuser',
    location: '341 George St',
    companyName: 'Kudos Inc',
    department: 'Quality Assurance',
    title: 'CEO',
    timezone: 'Australia/Sydney',
    system: false,
    presence: '2017-07-10T14:00:00Z',
    hasVerifiedEmail: true,
    trustedUser: false,
  },
  '/gateway/api/adminhub/um/site/:cloudId/users/:userId/managed-status': {
    managed: false,
  },
  '/gateway/api/adminhub/um/site/:cloudId/users/:userId/groups': {},
  '/gateway/api/adminhub/um/site/:cloudId/access-requests': {
    total: 3,
    accessRequests: [{
      user: {
        id: '557057:80abfbd6-e1b5-4f23-99d9-077658050e25',
        displayName: 'Alex Sim',
        email: 'asimkin@atlassian.com',
        active: true,
        system: false,
        hasVerifiedEmail: true,
      },
      requestedProducts: [{
        productId: 'confluence',
        productName: 'Confluence',
        status: 'PENDING',
        requestedTime: '2017-07-10T14:00:00Z',
      }],
      type: 'INVITE',
      requester: {
        id: '557057:5bc5f1a2-ba3f-472f-aeef-7eb41a9fba97',
        displayName: 'Dipanjan Laha',
        email: 'dlaha@atlassian.com',
        active: true,
        system: false,
        hasVerifiedEmail: true,
      },
    },
      {
        user: {
          id: '557057:5bc5f1a2-ba3f-472f-aeef-7eb41a9fba97',
          displayName: 'Dipanjan Laha',
          email: 'dlaha@atlassian.com',
          active: true,
          system: false,
          hasVerifiedEmail: true,
        },
        requestedProducts: [{
          productId: 'jira',
          productName: 'All Jira products',
          status: 'PENDING',
          requestedTime: '2017-07-10T14:00:00Z',
          subProducts: [{
            productId: 'jira-software',
            productName: 'Jira Software',
          }, {
            productId: 'jira-ops',
            productName: 'Jira Ops',
          }],
        }],
        type: 'REQUEST',
        requester: {
          id: '557057:5bc5f1a2-ba3f-472f-aeef-7eb41a9fba97',
          displayName: 'Dipanjan Laha',
          email: 'dlaha@atlassian.com',
          active: true,
          system: false,
          hasVerifiedEmail: true,
        },
      },
      {
        user: {
          id: '557057:ce58bfaa-23be-4ac7-bb69-ac24aca0711e',
          displayName: 'Rachel De Paula Cavalcanti',
          email: 'rdepaulacavalcanti@atlassian.com',
          active: true,
          system: false,
          hasVerifiedEmail: true,
        },
        requestedProducts: [{
          productId: 'confluence',
          productName: 'Confluence',
          status: 'PENDING',
          requestedTime: '2017-07-10T14:00:00Z',
        }],
        type: 'INVITE',
        requester: {
          id: '557057:80abfbd6-e1b5-4f23-99d9-077658050e25',
          displayName: 'Alex Sim',
          email: 'asimkin@atlassian.com',
          active: true,
          system: false,
          hasVerifiedEmail: true,
        },
      },
    ],
  },
  '/gateway/api/adminhub/um/site/:cloudId/access-requests/approve': {},
  '/gateway/api/adminhub/um/site/:cloudId/access-requests/reject': {},
  '/gateway/api/adminhub/um/site/:cloudId/users/:userId/direct-groups': groupListResponse,
  '/gateway/api/adminhub/um/site/:cloudId/users/:userId/product-access': [
    {
      productId: 'jira-servicedesk',
      productName: 'Jira Service Desk',
      presence: '2017-07-10T14:00:00Z',
      accessLevel: 'ADMIN',
    },
    {
      productId: 'jira-software',
      productName: 'Jira Software',
      presence: '2017-07-10T14:00:00Z',
      accessLevel: 'ADMIN',
    },
    {
      productId: 'jira-core',
      productName: 'Jira Core',
      presence: '2017-07-10T14:00:00Z',
      accessLevel: 'ADMIN',
    },
    {
      productId: 'conf',
      productName: 'Confluence',
      presence: '2017-07-10T14:00:00Z',
      accessLevel: 'NONE',
    },
    {
      productName: 'Jira administration',
      presence: null,
      productId: 'jira-admin',
      accessLevel: 'NONE',
    },
  ],
  '/gateway/api/adminhub/um/site/:cloudId/product/:productId/access-config/use/:groupId': {},
  '/gateway/api/adminhub/um/site/:cloudId/product/:productId/access-config/admin/:groupId': {},
  '/gateway/api/adminhub/um/site/:cloudId/product/:productId/access-config/import/:groupId': {},
  '/gateway/api/adminhub/um/site/:cloudId/product/:productId/access-config/default-groups/:groupId': {},
  '/gateway/api/adminhub/um/site/:cloudId/users/grant-access': {},
  '/gateway/api/adminhub/um/site/:cloudId/users/revoke-access': {},
  '/gateway/api/adminhub/um/site/:cloudId/users/reset-password': {},
  '/gateway/api/adminhub/um/site/:cloudId/invitation-urls': {
    urls: [
      {
        url: 'https://id.atlassian.com/invite/p/jira?id=invitation-url-id-1',
        productKey: 'jira-software',
        productName: 'Jira Software',
        expiration: new Date(Date.now() + 10 * 24 * 3600 * 1000).toISOString(),
      },
      {
        url: 'https://id.atlassian.com/invite/p/jira?id=invitation-url-id-2',
        productKey: 'confluence',
        productName: 'Confluence',
        expiration: new Date(Date.now() + 15 * 24 * 3600 * 1000).toISOString(),
      },
    ],
  },
  '/gateway/api/adminhub/um/site/:cloudId/invitation-urls/:productKey/generate': {},
  '/gateway/api/adminhub/um/site/:cloudId/invitation-urls/:productKey': {},
  '/gateway/api/adminhub/um/site/:cloudId/gsync/state': {},
};
