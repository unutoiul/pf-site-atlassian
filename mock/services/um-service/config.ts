import { ConfigFunction } from '../config';

export const config: ConfigFunction = ({ proxy }) => ({
  context: [
    '/admin/me',
    '/admin/rest/um/1/apps/navigation',
    '/admin/rest/um/1/context',
    '/admin/rest/domain-claim-service/api/getDomainClaims',
    '/admin/rest/um/1/group/search',
    '/admin/rest/um/1/group/user/direct',
    '/admin/rest/um/1/group',
    '/admin/rest/um/1/features',
  ],
  pathRewrite: proxy ? { '^/admin/': '/' } : {} as { [regexp: string]: string; },
  target: proxy ? (proxy as string) : 'http://localhost:3002',
  secure: false,
  changeOrigin: true,
});
