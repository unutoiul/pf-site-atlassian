export const responses = {
  '/admin/rest/billing/api/api/instance/product-usage': {
    "usages": [
      {"productKey": "confluence.ondemand", "features": {"userCount": 28}},
      {"productKey": "hipchat.cloud", "features": {"userCount": 29}},
      {"productKey": "jira-software.ondemand", "features": {"userCount": 28}}
      ]
  },
  '/admin/rest/billing/api/instance/pricing': {
    activeProducts: [],
    activatingProducts: [],
  },
  '/admin/rest/billing/api/instance/prospective-prices': {
    deactivatedProducts: [],
  },
  '/admin/rest/billing/api/product/confluence.ondemand/activate': {
    ok: true,
  },
};
