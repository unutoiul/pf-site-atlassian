import { ConfigFunction } from '../config';

export const config: ConfigFunction = ({ proxy }) => ({
  context: [
    '/admin/rest/billing/api/api/instance/product-usage',
    '/admin/rest/billing/api/instance/pricing',
    '/admin/rest/billing/api/instance/prospective-prices',
    '/admin/rest/billing/api/product/confluence.ondemand/activate',
  ],
  target: proxy ? (proxy as string) : 'http://localhost:3002',
  secure: false,
  changeOrigin: true,
});
