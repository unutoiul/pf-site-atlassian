export const responses = {
  '/gateway/api/adminhub/external-directory/manage-directory/:directoryId': {
    status: 200,
  },
  '/gateway/api/adminhub/external-directory/manage-directory/:directoryId/synced-users': {
    countOfUsers: 3,
  },
  '/gateway/api/adminhub/external-directory/manage-directory/:directoryId/logs': {
    totalResults: 7,
    startIndex: 0,
    itemsPerPage: 50,
    Resources: [
      {
        error: 'something went wrong 1',
        created_on: '2018-08-02T18:07:30.610117Z',
      },
      {
        error: 'some other error 2',
        created_on: '2018-08-02T18:07:30.610117Z',
      },
      {
        error: 'wow, something else 3',
        created_on: '2018-08-02T18:07:30.610117Z',
      },
      {
        error: 'ouch this is bad 4',
        created_on: '2018-08-02T18:07:30.610117Z',
      },
      {
        error: 'error 5',
        created_on: '2018-08-02T18:07:30.610117Z',
      },
      {
        error: 'error 6',
        created_on: '2018-08-02T18:07:30.610117Z',
      },
      {
        error: 'error 7',
        created_on: '2018-08-02T18:07:30.610117Z',
      },
    ],
  },
  '/gateway/api/adminhub/external-directory/manage-directory/:directoryId/synced-group-memberships': {
    totalResults: 30,
    startIndex: 0,
    itemsPerPage: 20,
    Resources: [
      {
        name: 'group a',
        membershipCount: 1,
      },
      {
        name: 'group b',
        membershipCount: 2,
      },
      {
        name: 'group c',
        membershipCount: 3,
      },
      {
        name: 'group a',
        membershipCount: 4,
      },
      {
        name: 'group b',
        membershipCount: 5,
      },
      {
        name: 'group c',
        membershipCount: 6,
      },
      {
        name: 'group a',
        membershipCount: 7,
      },
      {
        name: 'group b',
        membershipCount: 8,
      },
      {
        name: 'group c',
        membershipCount: 9,
      },
      {
        name: 'group a',
        membershipCount: 10,
      },
      {
        name: 'group b',
        membershipCount: 11,
      },
      {
        name: 'group c',
        membershipCount: 12,
      },
      {
        name: 'group a',
        membershipCount: 13,
      },
      {
        name: 'group b',
        membershipCount: 14,
      },
      {
        name: 'group c',
        membershipCount: 15,
      },
      {
        name: 'group a',
        membershipCount: 16,
      },
      {
        name: 'group b',
        membershipCount: 17,
      },
      {
        name: 'group c',
        membershipCount: 18,
      },
      {
        name: 'group a',
        membershipCount: 19,
      },
      {
        name: 'group b',
        membershipCount: 20,
      },
      {
        name: 'group c',
        membershipCount: 21,
      },
      {
        name: 'group a',
        membershipCount: 22,
      },
      {
        name: 'group b',
        membershipCount: 23,
      },
      {
        name: 'group c',
        membershipCount: 24,
      },
      {
        name: 'group a',
        membershipCount: 25,
      },
      {
        name: 'group b',
        membershipCount: 26,
      },
      {
        name: 'group c',
        membershipCount: 27,
      },
      {
        name: 'group a',
        membershipCount: 28,
      },
      {
        name: 'group b',
        membershipCount: 29,
      },
      {
        name: 'group c',
        membershipCount: 30,
      },
    ],
  },
  '/gateway/api/adminhub/external-directory/manage-directory': [
    {
      id: 'directory.id',
      baseUrl: 'google.com',
      name: 'some name',
      creationDate: '2018-08-02T18:07:30.610117Z',
    },
  ],
  '/gateway/api/adminhub/external-directory/manage-directory/:directoryId/rotateToken': {
    token: 'abcd1234',
  },
};
