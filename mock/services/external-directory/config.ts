import { ConfigFunction } from '../config';

export const config: ConfigFunction = ({ proxy }) => ({
  context: [
    '/api/gateway/adminhub/external-directory/manage-directory/',
  ],
  target: proxy ? (proxy as string) : 'http://localhost:3002',
  secure: false,
  changeOrigin: true,
});
