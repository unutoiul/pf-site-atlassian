// import { customizableTarget } from '../../customizable-target';
import { ConfigFunction } from '../config';

export const config: ConfigFunction = ({ proxy }) => ({
  context: (_path, req) => {
    const isForIframePage = req.url.indexOf('?chrome=false&react=false') > -1;
    const isRestRequest = req.url.indexOf('/admin/rest/') === 0;

    return isForIframePage || isRestRequest;
  },
  target: (proxy as string),
  secure: false,
  changeOrigin: true,
});
