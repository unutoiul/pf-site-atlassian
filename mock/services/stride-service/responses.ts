const pendingExport = {
  job_id: '135b0679-e19a-41a6-9d32-b64389b261fc',
  user_id: 'u1',
  cloud_id: 'c1',
  status: 'pending',
  created_at: '2018-05-11T14:11:38',
  job_settings: {
    include_public_rooms: true,
    include_private_rooms: true,
    include_directs: false,
  },
  user_details: {
    emails: ['johnny@las.co'],
    name: 'Johnny Atlas',
  },
};

const cancelledExport = {
  job_id: '07bdebca-2f48-4f85-a0b3-53dc8996eaa6',
  user_id: 'u1',
  cloud_id: 'c1',
  status: 'cancelled',
  created_at: '2018-04-04T11:23:33',
  started_at: '2018-04-04T11:24:34',
  job_settings: {
    start_date: '2018-04-04T00:00:00',
    end_date: '2018-05-22T00:00:00',
    include_public_rooms: true,
    include_private_rooms: true,
    include_directs: false,
  },
  user_details: {
    emails: ['johnny@las.co'],
    name: 'Johnny Atlas',
  },
};

const completedExport = {
  job_id: '50b353b3-4a4a-49ff-b7d5-a58880a85bdb',
  user_id: 'u1',
  cloud_id: 'c1',
  status: 'completed',
  created_at: '2018-03-11T03:12:29',
  started_at: '2018-03-11T03:13:39',
  expiration_date: '2018-03-25T03:13:39',
  job_settings: {
    start_date: '2018-03-11T00:00:00',
    end_date: '2018-05-22T00:00:00',
    include_public_rooms: true,
    include_private_rooms: true,
    include_directs: false,
  },
  user_details: {
    emails: ['johnny@las.co'],
    name: 'Johnny Atlas',
  },
  download_location: 'https://stride-exports.staging.atl-paas.net/f587e5e2-687a-46b1-aff9-0d6b82451987/655363%3A568fec75-dcdb-4f10-9e33-6d2eb8683f11/6Q997oFC4BHAnGS/stride-f587e5e2-687a-46b1-aff9-0d6b82451987-2018-09-11_15-37-20.tar.gz.aes',
  integrations_location: 'https://stride-exports.staging.atl-paas.net/f587e5e2-687a-46b1-aff9-0d6b82451987/655363%3A568fec75-dcdb-4f10-9e33-6d2eb8683f11/6Q997oFC4BHAnGS/stride-f587e5e2-687a-46b1-aff9-0d6b82451987-2018-09-11_15-37-20-integrations.csv',
};

export const responses = {
  '/gateway/api/chat/site/:siteid/export': {
    job_count: 3,
    jobs: [
      pendingExport,
      completedExport,
      cancelledExport,
    ],
  },
  '/gateway/api/chat/site/:siteid/export/135b0679-e19a-41a6-9d32-b64389b261fc': pendingExport,
  '/gateway/api/chat/site/:siteid/export/07bdebca-2f48-4f85-a0b3-53dc8996eaa6': cancelledExport,
  '/gateway/api/chat/site/:siteid/export/:exportid': completedExport,
  '/gateway/api/chat/site/:siteid/export/:exportid/cancel': {},
};
