import { ConfigFunction } from '../config';

export const config: ConfigFunction = ({ proxy }) => ({
  context: ['/gateway/api/chat/site'],
  target: proxy ? (proxy as string) : 'http://localhost:3002',
  secure: false,
  changeOrigin: true,
  // Set the origin to something Stargate allows for CORs
  // https://hello.atlassian.net/wiki/spaces/~wselleck/pages/277518524/Why+POSTing+to+Stargate+from+site-admin+locally+doesn+t+work+and+how+to+fix+it
  headers: {
    origin: 'https://atlassian.net',
  },
});
