import { ConfigFunction } from '../config';

export const config: ConfigFunction = ({ proxy }) => ({
  context: [
    '/gateway/api/adminhub/um/site',
  ],
  logLevel: 'debug',
  pathRewrite: proxy && (proxy as string) === 'http://localhost:4075' ? { '^/gateway/api/adminhub/um/site': '/rest/web/site' } : {} as { [regexp: string]: string; },
  target: proxy ? (proxy as string) : 'http://localhost:3002',
  secure: false,
  changeOrigin: true,
});
