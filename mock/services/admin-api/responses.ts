import { rowsPerPage } from '../../../src/organization/admin-api/admin-api-constants';
import { AdminApiKey } from '../../../src/schema/schema-types';

const tokens: AdminApiKey[] = [];

for (let i = 1; i <= (rowsPerPage * 2) + 1; ++i) {
  tokens.push(
    {
      id: i.toString(),
      name: `Key ${i}`,
      createdBy: {
        id: i % 2 ? 'HappyCorgi' : 'SadCorgi',
        active: 'ENABLED',
        displayName: i % 2 ? 'Happy Corgi' : 'Sad Corgi',
        emails: [{
          primary: true,
          value: 'happy@corgi.com',
        }],
      },
      creationDate: '2018-08-02T18:07:30.610117Z',
      expirationDate: '2018-09-02T18:07:30.610117Z',
    },
  );
}

export const responses = {
  '/gateway/api/adminhub/organization/:organizationId/apiTokens':  ({ query }) => {
    if (query.scrollId === 'hijklm') {
      return {
        scrollId: null,
        tokens: tokens.slice(rowsPerPage * 2),
      };
    }

    if (query.scrollId === 'abcdefg') {
      return {
        scrollId: 'hijklm',
        tokens: tokens.slice(rowsPerPage, rowsPerPage * 2),
      };
    }

    return {
      scrollId: 'abcdefg',
      tokens: tokens.slice(0, rowsPerPage),
    };
  },
  '/gateway/api/adminhub/organization/:organizationId/apiTokens/post': {
    id: '12345',
    name: 'Happy Corgi key',
    createdBy: {
      id: '23456',
      displayName: 'Happy Corgi',
      emails: [{
        primary: true,
        value: 'happy@corgi.com',
      }],
    },
    creationDate: '2018-08-02T18:07:30.610117Z',
    expirationDate: '2018-09-02T18:07:30.610117Z',
    token: 'xyz123',
  },
  '/gateway/api/adminhub/organization/:organizationId/admin/invite': {
    status: 204,
  },
  '/gateway/api/adminhub/organization/:organizationId/apiTokens/:adminApiKeyId': true,
  '/gateway/api/adminhub/organization/domains/:domain/ownership-check': {
    claimed: true,
  },
};
