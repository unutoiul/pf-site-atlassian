export const responses = {
  '/gateway/api/adminhub/customer-directory/site/:siteId/service-desk-customer-directory': {
    serviceDeskCustomerDirectoryId: '1b5eb25f-cbc5-4639-91c0-1625e1539b19',
  },
  '/gateway/api/adminhub/customer-directory/directory/:directoryId/export': `
    username,full_name,email,active,last_login
    josh,Joshua Nelson,jonelson@atlassian.com,yes,thisisnotrealdatasoyouknow
  `,
  '/gateway/api/adminhub/customer-directory/directory/:directoryId/user/:userId': {},
};
