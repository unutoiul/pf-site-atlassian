import { MemberAccessApiResponse, MemberDetailsApiResponse, MembersApiResponse, SiteAdminsResponse } from '../../../src/schema/api';

const memberAccess: MemberAccessApiResponse = {
  sites: [
    {
      siteUrl: 'hamburger.atlassian.net',
      siteAdmin: true,
    },
    {
      siteUrl: 'hotdog.com',
      siteAdmin: false,
    },
    {
      siteUrl: 'empty.com',
      siteAdmin: false,
    },
    {
      siteUrl: 'apples.com',
      siteAdmin: true,
    },
  ],
  products: [
    {
      productName: 'Confluence',
      productKey: 'confluence.ondemand',
      siteUrl: 'acme.atlassian.net',
    },
    {
      productName: 'Confluence',
      productKey: 'confluence.ondemand',
      siteUrl: 'acme-support.atlassian.net',
    },
    {
      productName: 'Bitbucket',
      productKey: 'bitbucket.cloud',
      siteUrl: 'hamburger.atlassian.net',
    },
    {
      productName: 'Confluence',
      productKey: 'confluence.ondemand',
      siteUrl: 'hamburger.atlassian.net',
    },
    {
      productName: 'Confluence',
      productKey: 'confluence.ondemand',
      siteUrl: 'vmware.atlassian.net',
    },
    {
      productName: 'Confluence',
      productKey: 'confluence.ondemand',
      siteUrl: 'cisco.atlassian.net',
    },
    {
      productName: 'JIRA Core',
      productKey: 'jira-core.ondemand',
      siteUrl: 'hamburger.atlassian.net',
    },
    {
      productName: 'JIRA Service Desk',
      productKey: 'jira-servicedesk.ondemand',
      siteUrl: 'hamburger.atlassian.net',
    },
    {
      productName: 'Stride',
      productKey: 'hipchat.cloud',
      siteUrl: 'hamburger.atlassian.net',
    },
    {
      productName: 'JIRA Software',
      productKey: 'jira-software.ondemand',
      siteUrl: 'hamburger.atlassian.net',
    },
    {
      productName: 'Stride',
      productKey: 'hipchat.cloud',
      siteUrl: 'hotdog.com',
    },
  ],
};

export const regularMember: MemberDetailsApiResponse = {
  id: '557057:80abfbd6-e1b5-4f23-99d9-077658050e25',
  memberAccess,
  displayName: 'Alex Simkin',
  jobTitle: '',
  active: 'ENABLED',
  useMfa: false,
  emails: [
    { value: 'asimkin@atlassian.com', primary: true },
  ],
};

export const deactivatedMember: MemberDetailsApiResponse = {
  id: '557057:5bc5f1a2-ba3f-472f-aeef-7eb41a9fba97',
  memberAccess,
  displayName: 'Agent Smith',
  jobTitle: '',
  active: 'DISABLED',
  useMfa: false,
  emails: [
    { value: 'asmith@atlassian.com', primary: true },
  ],
};

export const deactivatedMemberWithDeactivatedInfo: MemberDetailsApiResponse = {
  id: '557057:cd970284-91ad-4fec-b319-a02254b00a85',
  memberAccess,
  displayName: 'I am deactivated',
  jobTitle: '',
  active: 'DISABLED',
  useMfa: false,
  emails: [
    { value: 'deactivated@atlassian.com', primary: true },
  ],
  deactivatedInfo: {
    deactivatedBy: regularMember.id,
    deactivatedOn: '2018-02-05T02:29:18.000Z',
  },
};

export const pendingDeletionMember: MemberDetailsApiResponse = {
  id: '557057:7aaa8a77-c66f-4e70-b137-45a4ccfc2545',
  memberAccess,
  displayName: 'I am pending Deletion',
  jobTitle: '',
  active: 'DISABLED',
  useMfa: false,
  emails: [
    { value: 'pendingdeletion@atlassian.com', primary: true },
  ],
  pendingInfo: {
    startedBy: regularMember.id,
    startedOn: '2019-02-05T02:29:18.433Z',
  },
};

export const scimProvisionedMember: MemberDetailsApiResponse = {
  id: '557057:4a24f93a-04b0-44ab-93f4-424b02d8dcb7',
  displayName: 'Alexander Minozhenko (SCIM)',
  jobTitle: 'Security Engineer',
  active: 'ENABLED',
  useMfa: false,
  emails: [
    {
      value: 'aminozhenko@atlassian.com',
      primary: true,
    },
  ],
  memberAccess: null,
};

export const gsyncProvisionedMember: MemberDetailsApiResponse = {
  id: '557057:57e7be63-e1cc-4a6f-b597-d61171415df6',
  displayName: 'Angel Bartomeu Bonillo (GSync)',
  jobTitle: 'Quality Engineer',
  active: 'ENABLED',
  useMfa: false,
  emails: [
    {
      value: 'abartomeubonillo@atlassian.com',
      primary: true,
    },
  ],
  memberAccess: null,
};

const baseMembers: MembersApiResponse = {
  total: 11,
  users: [
    regularMember,
    scimProvisionedMember,
    gsyncProvisionedMember,
    pendingDeletionMember,
    deactivatedMemberWithDeactivatedInfo,
    {
      id: '557057:5bc5f1a2-ba3f-472f-aeef-7eb41a9fba97',
      displayName: 'Agent Smith',
      jobTitle: 'Senior Developer',
      active: 'DISABLED',
      useMfa: false,
      emails: [
        {
          value: 'asmith@atlassian.com',
          primary: true,
        },
      ],
      memberAccess: null,
    },
    {
      id: '557057:e095c181-83c3-4f79-acfb-d947e6ebca33',
      displayName: 'Helen Xue',
      jobTitle: 'Graduate Product Manager',
      active: 'ENABLED',
      useMfa: false,
      emails: [
        {
          value: 'hxue@atlassian.com',
          primary: true,
        },
      ],
      memberAccess: null,
    },
    {
      id: '557057:708a28cd-b518-427a-a4c7-821d467feb3b',
      displayName: ' I told you all this before, when you have a swimming pool do not use chlorine, use salt water, the healing, salt water is the healing.',
      jobTitle: '',
      active: 'ENABLED',
      useMfa: false,
      emails: [
        {
          value: 'reallyreallyreallyreallyreallyreallyreallyreallyreallyreallyreallyreallylongemail@atlassian.com',
          primary: true,
        },
      ],
      memberAccess: null,
    },
    {
      id: '557057:ce58bfaa-23be-4ac7-bb69-ac24aca0711e',
      displayName: 'Rachel De Paula Cavalcanti',
      jobTitle: '',
      active: 'ENABLED',
      useMfa: false,
      emails: [
        {
          value: 'rdepaulacavalcanti@atlassian.com',
          primary: true,
        },
      ],
      memberAccess: null,
    },
    {
      id: '557057:b03d7440-663b-4c78-850d-5fc0016a69df',
      displayName: 'Saxon Bruce',
      jobTitle: 'Developer',
      active: 'ENABLED',
      useMfa: false,
      emails: [
        {
          value: 'sbruce@atlassian.com',
          primary: true,
        },
      ],
      memberAccess: null,
    },
    {
      id: '557058:7ee82aa9-e4f9-49a3-9673-f03ad05c7cd0',
      displayName: 'test test',
      jobTitle: null,
      active: 'ENABLED',
      useMfa: false,
      emails: [
        {
          value: 'test@example.com',
          primary: true,
        },
      ],
      memberAccess: null,
    },
  ],
};

const baseProducts = [
  {
    productName: 'Confluence',
    siteUrl: 'site-1',
  },
  {
    productName: 'Jira Core',
    siteUrl: 'site-1',
  },
  {
    productName: 'Jira Softwareeeeeeeeeeeeeeeeeeeeeeeeeeeeee',
    siteUrl: 'site-1',
  },
  {
    productName: 'Opsgenie',
    siteUrl: 'site-5',
  },
];

const duplicateProducts = [
  {
    productName: 'Confluence',
    siteUrl: 'site-1',
  },
  {
    productName: 'Confluence',
    siteUrl: 'site-2',
  },
  {
    productName: 'Jira Core',
    siteUrl: 'site-1',
  },
  {
    productName: 'Jira Core',
    siteUrl: 'site-2',
  },
  {
    productName: 'Jira Software',
    siteUrl: 'site-1',
  },
];

const membersAccess = [
  { products: baseProducts },
  { products: duplicateProducts },
  null,
  null,
  null,
  { errors: ['some-error'] },
  { products: baseProducts },
  { products: null },
  {
    products: [
      {
        productName: 'Sourcetree',
        siteUrl: 'site-1',
      },
    ],
  },
  { products: baseProducts },
  { products: [] },
];

const members = {
  ...baseMembers,
  users: (baseMembers.users || []).map(((user, i) => ({
    ...user,
    memberAccess: membersAccess[i],
  }))),
};

const orgId = '1363002c-4b37-303f-b9e3-06734d54989a';
const orgsResponse = [
  {
    id: orgId,
    name: 'Acme',
    migrationState: 'IM',
  },
  {
    id: '1363002c-4b37-303f-b9e3-06734d54989b',
    name: 'Acme 2',
    migrationState: 'IM',
  },
];

export const responses = {
  '/gateway/api/adminhub/organization/my': orgsResponse,
  '/gateway/api/adminhub/site-admin-orgs/my': orgsResponse,
  '/gateway/api/adminhub/organization/feature-flags': {
    'iom.enable.orgs': true,
    'iom.saml.auth.provider.toggle': true,
    'iom.saml.auth0.forced': true,
  },
  '/gateway/api/adminhub/organization/:organizationId/domainClaims/:domainName/verify/dnsAsync': {},
  '/gateway/api/adminhub/organization/:organizationId/domainClaims/:domainName/verify/httpAsync': { taskId: 'abcd1234' },
  '/gateway/api/adminhub/organization/:organizationId/domainClaims': [
    {
      organizationId: orgId,
      domain: 'acme.com',
      verified: true,
      status: 'VERIFIED',
      verificationType: 'http',
    },
    {
      organizationId: orgId,
      domain: 'acme.com.au',
      verified: true,
      status: 'VERIFIED',
      verificationType: 'dns',
    },
    {
      domain: 'acme.com.eu',
      verified: false,
      status: 'UNVERIFIED',
    },
  ],
  '/gateway/api/adminhub/organization/:organizationId/members/export': {
    success: true,
  },
  '/gateway/api/adminhub/organization/:organizationId/members/export/12345': `
    hello, test, something,\n
    item1, item2, item3,\n
  `,
  '/gateway/api/adminhub/organization/:organizationId/domainClaims/tokenRecord': {
    record: 'atlassian-domain-verification=0KINSCt+bU1GOyvGFsY15OM2DLqyrUmGRPYIMxMgdEtU+X9f+eTEhh8rxttWkeje',
  },
  '/gateway/api/adminhub/organization/:organizationId/members/total': {
    total: 9,
    enabledTotal: 5,
    disabledTotal: 4,
  },
  '/gateway/api/adminhub/organization/:organizationId/mfa/exemptUsers': [],
  '/gateway/api/adminhub/organization/:organizationId/mfa': { dateEnforced: 1234567890 },
  '/gateway/api/adminhub/organization/:organizationId/admin': members,
  '/gateway/api/adminhub/organization/:organizationId/members': members,
  [`/gateway/api/adminhub/organization/:organizationId/members/${scimProvisionedMember.id}`]: scimProvisionedMember,
  [`/gateway/api/adminhub/organization/:organizationId/members/${gsyncProvisionedMember.id}`]: gsyncProvisionedMember,
  [`/gateway/api/adminhub/organization/:organizationId/members/${deactivatedMember.id}`]: deactivatedMember,
  [`/gateway/api/adminhub/organization/:organizationId/members/${pendingDeletionMember.id}`]: pendingDeletionMember,
  [`/gateway/api/adminhub/organization/:organizationId/members/${deactivatedMemberWithDeactivatedInfo.id}`]: deactivatedMemberWithDeactivatedInfo,
  '/gateway/api/adminhub/organization/:organizationId/members/:memberId': regularMember,
  '/gateway/api/adminhub/organization/:organizationId/members/:memberId/active': {},
  '/gateway/api/adminhub/organization/:organizationId/saml': {
    issuer: 'https://id.acme.com/issuer',
    ssoUrl: 'https://id.acme.com/sso-url',
    publicCertificate: '-----BEGIN CERTIFICATE-----\nMIIC1jCCAcCgAwIBAgIBATALBgkqhkiG9w0BAQUwHjEcMAkGA1UEBhMCUlUwDwYD\nVQQDHggAVABlAHMAdDAeFw0xNjAxMzExMzAwMDBaFw0xOTAxMzExMzAwMDBaMB4x\nHDAJBgNVBAYTAlJVMA8GA1UEAx4IAFQAZQBzAHQwggEiMA0GCSqGSIb3DQEBAQUA\nA4IBDwAwggEKAoIBAQCX2l7qLa5RnBtVYMNWES3w/Tk6mWZCSDKl+/4meSljN8aF\nCNhQ6ewNo/TSU+ET/+cbyK9sRyik1dZMPitK+zzxfCy51+DM3H2SlphwaUBculrs\nIKKCbYoexrW3GWNwtkHRXW6miYqayn6yBy3+hLh1Wo1ArtukK8BBpHQq7mm3ASdG\nPSbMI88GGiqK+G8bpHq07yQdN5IrjtbcOCP+W645zPaTgrpTmVl9pDsKGUiyOLlY\n3A1zWftW/h+CYqYndMKlHW6uGci9o8yJ7MeVUDNqWUqsvVtzNLRjcdveEMc5fZnU\nlfeNpNlJssziLyNRkepg/Dgza8S9LrXQvwBpp8XFAgMBAAGjIzAhMBIGA1UdEwEB\n/wQIMAYBAf8CAQMwCwYDVR0PBAQDAgAGMAsGCSqGSIb3DQEBBQOCAQEARtAf80Qi\nf//dwSCbx5esKrtnCtw4as7DervxYN8Pwi2hyaWrS3Jz2qUO860jeGbrKz60IfIP\nrqZSSb1vSP58sZ8Gt3TThWIPFd3nMbqe4TpHwGhrY30F0kOosF89g9hqCEqwfO5D\nhWbM9tspSZE7U+/HE3iPFGJthpu7zhcXZFFN6bay3OEF/6rF8tfgmZ06M9UrhTrR\nnvxn9f7ItrQ9oDigKnykJYzEcRZYNidwsizjMnszUz4ffW4FtLAV6ShTa+GuyYKn\nanwCErKTLFSm/Zu6rs/pqCTCia+rkvqq9Jw92ohV2wq+ofAhmvFyqavBY7ayn8Am\nU9x6wusvuVzHpA==\n-----END CERTIFICATE-----',
    auth0MigrationState: 'AUTH0',
    serviceProvider: {
      entityId: 'https://id.atlassian.com/login',
      acsUrl: 'https://id.atlassian.com/login/saml/acs',
    },
  },
  '/gateway/api/adminhub/organization/:organizationId/passwordpolicy': {
    minimumStrength: 'strong',
    expiryDays: 180,
    resetPasswords: false,
  },
  '/gateway/api/adminhub/organization/:organizationId/product': [
    'identity-manager',
  ],
  '/gateway/api/adminhub/organization/:organizationId/product/identity-manager': {
    progressUri: 'http://localhost:3002/mock-cofs-api/progress',
  },
  '/gateway/api/adminhub/organization/create': {
    id: orgId,
    progressUri: 'http://localhost:3002/mock-cofs-api/progress',
  },
  '/gateway/api/adminhub/organization/sites/unlinked-sites': {
    sites: [
      {
        siteUrl: 'site-1.jira-dev.com',
        id: '4715475b-0f16-4183-b383-379908feff29',
        products: [
          {
            productKey: 'stride',
            productName: 'Stride',
            siteUrl: 'site-1.jira-dev.com',
          },
          {
            productKey: 'jira-software',
            productName: 'Jira Software',
            siteUrl: 'site-1.jira-dev.com',
          },
          {
            productKey: 'opsgenie',
            productName: 'Opsgenie',
            siteUrl: 'site-1.jira-dev.com',
          },
        ],
      },
      {
        siteUrl: 'site-2.jira-dev.com',
        id: '5715475b-0f16-4183-b383-379908feff29',
        products: [
          {
            productKey: 'confluence',
            productName: 'Confluence',
            siteUrl: 'site-2.jira-dev.com',
          },
        ],
      },
    ],
  },
  '/gateway/api/adminhub/organization/:organizationId/sites/:siteId/admins': {
    users: [
      {
        orgAdmin: true,
        id: '655362:7f95b9b4-b01b-4dca-b126-2581cca4c515',
        displayName: 'Ashwini Rattihalli',
        emails: [
          {
            value: 'arattihalli@atlassian.com',
            primary: true,
          },
        ],
      },
      {
        orgAdmin: false,
        id: '655362:7f95b9b4-b01b-4dca-b126-2581cca4c516',
        displayName: 'Ruslan Arkhipau',
        emails: [
          {
            value: 'rarkhipau@atlassian.com',
            primary: true,
          },
        ],
      },
    ],
  } as SiteAdminsResponse,
  [`/gateway/api/adminhub/organization/${orgId}/sites`]: [
    {
      id: orgId,
      siteUrl: 'pf-site-admin-ui-prod.atlassian.net',
      displayName: 'Product Fabric Site',
      avatar: 'https://site-admin-avatar-cdn.staging.public.atl-paas.net/avatars/240/jersey.png',
      products: [
        {
          productKey: 'jira-core',
          productName: 'Jira Core',
          siteUrl: 'pf-site-admin-ui-prod.atlassian.net',
        },
      ],
    },
    {
      id: '1363002c-4b37-303f-b9e3-06734d54989d',
      siteUrl: 'oh-my-word-this-is-a-long-site-name-hmmmmmm.atlassian.net',
      displayName: 'Oh my word this is a long site name 🔥',
      avatar: 'https://site-admin-avatar-cdn.staging.public.atl-paas.net/avatars/240/koala.png',
    },
    {
      id: '1363002c-4b37-303f-b9e3-06734d54989b',
      siteUrl: 'pf-site-admin-ui-prodb.atlassian.net',
      displayName: 'Test Site',
      avatar: 'https://site-admin-avatar-cdn.staging.public.atl-paas.net/avatars/240/cup.png',
      products: [
        {
          productKey: 'jira-core',
          productName: 'Jira Core',
          siteUrl: 'pf-site-admin-ui-prodb.atlassian.net',
        },
        {
          productKey: 'stride',
          productName: 'Stride',
          siteUrl: 'pf-site-admin-ui-prodb.atlassian.net',
        },
        {
          productName: 'Opsgenie',
          productKey: 'opsgenie',
          siteUrl: 'glutenfreepizza.com',
        },
      ],
    },
    {
      id: '1363002c-4b37-303f-b9e3-06734d54989c',
      siteUrl: 'pf-site-admin-ui-prodc.atlassian.net',
      displayName: 'Test Site C',
      avatar: 'https://site-admin-avatar-cdn.staging.public.atl-paas.net/avatars/240/star.png',
      products: null,
    },
    {
      id: '1363002c-4b37-303f-b9e3-06734d54989b',
      siteUrl: 'abandoned.atlassian.net',
      displayName: null,
      avatar: null,
      products: [],
    },
  ],
  '/gateway/api/adminhub/organization/:organizationId/sites': [],
  '/gateway/api/adminhub/organization/:organizationId/product-definitions': [
    {
      productKey: 'jira-core',
      productName: 'Jira Core',
    },
    {
      productKey: 'confluence',
      productName: 'Confluence',
    },
    {
      productKey: 'stride',
      productName: 'Stride',
    },
  ],
};
