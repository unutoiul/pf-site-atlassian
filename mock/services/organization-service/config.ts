import { ConfigFunction } from '../config';

export const config: ConfigFunction = ({ proxy }) => ({
  context: ['/gateway/api/adminhub/organization'],
  pathRewrite: proxy ? { '^/gateway/api/adminhub/organization': '' } : {} as { [regexp: string]: string; },
  target: proxy ? (proxy as string) : 'http://localhost:3002',
  secure: false,
  changeOrigin: true,
});
