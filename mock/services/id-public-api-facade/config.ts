import { ConfigFunction } from '../config';

export const config: ConfigFunction = () => ({
  context: ['/gateway/api/users/manage/'],
  target: 'http://localhost:3002',
  secure: false,
  changeOrigin: true,
});
