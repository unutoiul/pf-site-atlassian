import { ProfileMutabilityResponse } from '../../../src/schema/api';
import { gsyncProvisionedMember, scimProvisionedMember } from '../organization-service/responses';

export const responses = {
  [`/gateway/api/users/manage/${encodeURIComponent(scimProvisionedMember.id)}/profile`]: {
    not_editable: [
      { field: 'email', reason: 'ext.dir.scim' },
    ],
  } as ProfileMutabilityResponse,
  [`/gateway/api/users/manage/${encodeURIComponent(gsyncProvisionedMember.id)}/profile`]: {
    not_editable: [
      { field: 'email', reason: 'ext.dir.google' },
    ],
  } as ProfileMutabilityResponse,
  '/gateway/api/users/manage/:userId/profile': {
    not_editable: [],
  } as ProfileMutabilityResponse,
};
