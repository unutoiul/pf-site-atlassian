import { ConfigFunction } from '../config';

export const config: ConfigFunction = ({ proxy }) => ({
  context: [
    '/gateway/api/permissions/permitted',
  ],
  target: proxy ? (proxy as string) : 'http://localhost:3002',
  secure: false,
  changeOrigin: true,
});
