const tempCheckSiteNameAvailability = (siteName) => {
  switch (siteName) {
    case 'foo': case 'foobar':
      return {
        result: true,
        taken: true,
        isAlreadyOwner: true,
        renameLimitReached: false,
      };
    case 'hello':
      return {
        result: false,
        taken: true,
        isAlreadyOwner: false,
        renameLimitReached: false,
      };
    case 'mock-rename-limit-reached':
      return {
        result: false,
        taken: false,
        isAlreadyOwner: false,
        renameLimitReached: true,
      };
    case 'mock-invalid-misc':
      return {
        result: false,
        taken: false,
        isAlreadyOwner: false,
        renameLimitReached: false,
      };
    default:
      return {
        result: true,
        taken: false,
        isAlreadyOwner: false,
        renameLimitReached: false,
      };
  }
}

export const responses = {
  '/gateway/api/hostname/cloud/rename/status': {
    cloudName: 'foo',
    cloudNamespace: 'atlassian.gov',
    renamesUsed: 0,
    renameLimit: 5,
  },
  '/gateway/api/hostname/cloud/rename/check': (req) => {
    const { cloudName } = req.body;
    return tempCheckSiteNameAvailability(cloudName);
  },
};
