import { config as adminApiConfig, responses as adminApiResponses } from './admin-api';
import { config as billingServiceConfig, responses as billingServiceResponses } from './billing-service';
import { config as buxFrontEndConfig } from './bux-frontend';
import { config as buxServiceConfig, responses as buxServiceResponses } from './bux-service';
import { config as cadsServiceConfig, responses as cadsResponses } from './cads-service';
import { config as cnasServiceConfig, responses as cnasResponses } from './cloud-name-availability-service';
import { config as cofsServiceConfig, responses as cofsResponses } from './cloud-order-fulfillment-service';
import { config as connectServiceConfig, responses as caasResponses } from './connect-service';
import { config as emojiServiceConfig, responses as emojiServiceResponses } from './emoji-service';
import { config as externalDirectoryConfig, responses as externalDirectoryResponses } from './external-directory';
import { config as forgetAboutMeServiceConfig, responses as forgetAboutMeServiceResponses } from './forget-about-me-service';
import { config as gSuiteServiceConfig, responses as gSuiteServiceResponses } from './g-suite-service';
import { config as idPublicApiFacadeConfig, responses as idPublicApiFacadeResponses } from './id-public-api-facade';
import { config as jiraServiceConfig, responses as jiraServiceResponses } from './jira-service';
import { config as onboardingServiceConfig, responses as onboardingServiceResponses } from './onboarding-service';
import { config as organizationServiceConfig, responses as organizationServiceResponses } from './organization-service';
import { config as permissionsServiceConfig, responses as permissionsServiceResponses } from './permissions';
import { config as siteConfig, responses as siteResponses } from './site-service';
import { config as strideServiceConfig, responses as strideServiceResponses } from './stride-service';
import { config as umHipsterConfig } from './um-hipster';
import { config as umIframeConfig } from './um-iframe';
import { config as umServiceConfig, responses as umServiceResponses } from './um-service';
import {
  config as userConnectedAppsServiceConfig,
  responses as userConnectedAppsServiceResponses,
} from './user-connected-apps-service';
import { config as xflowServiceConfig, responses as xflowServiceResponses } from './xflow';
import { config as identityPlatformServiceConfig, responses as identityPlatformServiceResponses } from './identity-platform-service';


import { getArguments } from '../arguments';

export const responses = {
  ...adminApiResponses,
  ...onboardingServiceResponses,
  ...emojiServiceResponses,
  ...umServiceResponses,
  ...organizationServiceResponses,
  ...permissionsServiceResponses,
  ...buxServiceResponses,
  ...jiraServiceResponses,
  ...billingServiceResponses,
  ...siteResponses,
  ...externalDirectoryResponses,
  ...xflowServiceResponses,
  ...strideServiceResponses,
  ...userConnectedAppsServiceResponses,
  ...caasResponses,
  ...cnasResponses,
  ...cofsResponses,
  ...cadsResponses,
  ...forgetAboutMeServiceResponses,
  ...gSuiteServiceResponses,
  ...idPublicApiFacadeResponses,
  ...identityPlatformServiceResponses,
};

export const config = [
  adminApiConfig({ proxy: getArguments().proxyAdminApi }),
  onboardingServiceConfig({ proxy: getArguments().proxyOnboardingService }),
  emojiServiceConfig({ proxy: getArguments().proxyEmojiService }),
  umServiceConfig({ proxy: getArguments().proxyUmService }),
  umIframeConfig({ proxy: getArguments().proxyUmIframe }),
  umHipsterConfig({ proxy: getArguments().proxyUmHipster }),
  organizationServiceConfig({ proxy: getArguments().proxyOrganizationService }),
  permissionsServiceConfig({ proxy: getArguments().permissionsOrganizationService }),
  buxServiceConfig({ proxy: getArguments().proxyBuxService }),
  buxFrontEndConfig({ proxy: getArguments().proxyBuxFrontEnd }),
  jiraServiceConfig({ proxy: getArguments().proxyJiraService }),
  billingServiceConfig({ proxy: getArguments().proxyBillingService }),
  siteConfig({ proxy: getArguments().proxySite }),
  externalDirectoryConfig({ proxy: getArguments().proxyExternalDirectory }),
  xflowServiceConfig({ proxy: getArguments().xflowService }),
  strideServiceConfig({ proxy: getArguments().proxyStrideService }),
  userConnectedAppsServiceConfig({ proxy: getArguments().proxyUserConnectedAppsService }),
  connectServiceConfig({ proxy: getArguments().proxyCaasService }),
  cnasServiceConfig({ proxy: getArguments().proxyCnasService }),
  cofsServiceConfig({ proxy: getArguments().proxyCofsService }),
  cadsServiceConfig({ proxy: getArguments().proxyCadsService }),
  forgetAboutMeServiceConfig({ proxy: getArguments().proxyUserConnectedAppsService }),
  gSuiteServiceConfig({ proxy: getArguments().proxyGSuiteService }),
  idPublicApiFacadeConfig(null!),
  identityPlatformServiceConfig({ proxy: getArguments().proxyIdentityPlaformService }),
];
