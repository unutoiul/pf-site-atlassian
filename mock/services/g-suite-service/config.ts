import { ConfigFunction } from '../config';

export const config: ConfigFunction = ({ proxy }) => ({
  context: ['/gateway/api/adminhub/google-sync-legacy/clouds'],
  pathRewrite: proxy ? { '^/gateway/api/adminhub/google-sync-legacy/clouds': '' } : {} as { [regexp: string]: string; },
  target: proxy ? (proxy as string) : 'http://localhost:3002',
  secure: false,
  changeOrigin: true,
});
