export const responses = {
  '/gateway/api/adminhub/google-sync-legacy/clouds/:cloudId/state': {
    syncConfig: [],
    syncStartTime: null,
    nextSyncStartTime: '17 Oct 2018 4:35AM GMT',
    lastSync: {
      startTime: '17 Oct 2018 3:35AM GMT',
      failed: 0,
      errors: [],
      updated: 0,
      hasReport: true,
      status: 'successful',
      finishTime: '17 Oct 2018 3:35AM GMT',
      numOfSyncedUsers: 1,
      deleted: 0,
      created: 1,
    },
    selectedGroups: ['01hmsyys3fnt05a'],
    noSyncs: 2,
    user: 'jberry@jberry-test.teamsinspace.com',
  },
  '/gateway/api/adminhub/google-sync-legacy/clouds/:cloudId/connect': {
    connect_url: 'http://localhost:3001/gateway/api/adminhub/google-sync-legacy/clouds/38b7be26-03f7-42a9-b18b-9cdb56d204c2/callback',
  },
};
