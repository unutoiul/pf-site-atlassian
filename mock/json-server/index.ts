import chalk from 'chalk';
import * as jsonServer from 'json-server';

import { getArguments } from '../arguments';
import { responses } from '../services';

const defaultMiddleware = jsonServer.defaults({ static: 'mock/static' });
const server = jsonServer.create();

server.use(defaultMiddleware);

// Enable use of _req.body resolver functions
server.use(jsonServer.bodyParser);

// Dynamically combine the responses into routes
Object.keys(responses).forEach(path => {
  if (path === '/gateway/api/adminhub/um/site/:cloudId/groups') {
    server.post(path, (_req, res) => {
      res.jsonp(responses['/gateway/api/adminhub/um/site/:cloudId/groups/:groupId']);
    });

    server.get(path, (_req, res) => {
      res.jsonp(responses[path]);
    });
  }

  if (path.includes('/gateway/api/adminhub/organization/:organizationId/domainClaims/:domainName/verify/dnsAsync')) {
    server.post(path, (_req, res) => {
      res.status(204).jsonp();
    });
  }

  if (path.includes('/gateway/api/adminhub/organization/:organizationId/apiTokens')) {
    server.post(path, (_req, res) => {
      res.jsonp(responses['/gateway/api/adminhub/organization/:organizationId/apiTokens/post']);
    });
  }

  if (path.includes('/gateway/api/emoji/:siteid/site')) {
    server.post(path, (_req, res) => {
      res.jsonp({
        emojis: [
          {
            id: 'a6d5df55-1493-4e83-b45c-12345678' + Math.floor(Math.random() * 65536).toString(16),
            name: _req.body.name,
            fallback: _req.body.shortName,
            type: 'SITE',
            category: 'CUSTOM',
            order: -10000,
            representation: {
              imagePath: 'http://localhost:3002/images/log.jpg',
              mediaFileId: '685f270c-4aac-48b9-bd0a-8d26acf0b33e',
              height: 280,
              width: 280,
            },
            searchable: true,
            creatorUserId: responses['/gateway/api/directory/graphql'].data.u7.id,
            createdDate: new Date().toISOString(),
            shortName: _req.body.shortName,
          },
        ],
        meta: responses['/gateway/api/emoji/:siteid/site'].meta,
      });
    });
  }

  const resolver = responses[path];
  server.all(path, (_req, res) => {
    const response = (typeof resolver === 'function') ? resolver(_req) : resolver;
    res.jsonp(response);
  });
});

// So that localhost:3002/ looks pretty by itself
server.get('/db', (_req, res) => {
  res.jsonp(responses);
});
let progressCount = 0;
server.get('/mock-cofs-api/progress', (_req, res) => {
  progressCount++;
  if (progressCount % 3 === 0) {
    res.jsonp({
      completed: true,
      successful: true,
    });
  } else {
    res.jsonp({
      completed: false,
      successful: false,
    });
  }
});

// A test endpoint to return custom status codes for debugging. Useful in combination with resource override
server.all('/_debug/status/:status', (req, res) => {
  res.status(req.params.status);
  res.send();
});

let domainClaimProgress = 0;
server.get('/gateway/api/adminhub/organization/:organizationId/domainClaims/:domainName/task/:taskId/status', (_req, res) => {
  domainClaimProgress++;
  if (domainClaimProgress % 3 === 0) {
    res.jsonp({
      status: 'SUCCESS',
      taskId: 'abcd1234',
    });
  } else {
    res.jsonp({
      status: 'IN_PROGRESS',
      taskId: 'abcd1234',
    });
  }
});

const PORT = 3002;
server.listen(PORT, () => {
  // tslint:disable-next-line:no-console
  console.log(`JSON Server is running on port ${PORT}`);
});

if (getArguments().proxyUmIframe) {
  // tslint:disable-next-line:no-console
  console.log(chalk.red.bold.underline(`You are using the proxy iframe argument, which requires copying cookies across domains. See playbooks/MOCKING.md for instructions`));
}
