# pf-site-admin-ui

The new, centralized and unified site-admin UI. Based on [site-administration-ui](https://stash.atlassian.com/projects/IP/repos/site-administration-ui/browse).

See also [pf-site-admin](https://bitbucket.org/atlassian/pf-site-admin).

Specific documentation related to the repository is at `./playbooks`. 
You can checkout the web version of the playbooks at `go/adminhub-playbooks`.

## Requirements

* yarn

### Optional requirements

* [Apollo devtools](https://chrome.google.com/webstore/detail/apollo-client-developer-t/jdkknkkbebbapilgoeccciglkfbmbnfm?hl=en-US)
* [React devtools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=en)

## Getting started

For linting, run `yarn lint:build`

There are two primary ways to develop `pf-site-admin-ui` locally. You can develop against (and through) User Management (UM), or you can develop `pf-site-admin-ui` in standalone mode, mocking and proxying UM responses.


### Developing against local mocks

* See `./playbooks/LOCAL_DEVELOPMENT.md`

### Developing through User Management

#### For a local instance

* See `./playbooks/LOCAL_DEVELOPMENT.md`

### BUF Getting started
Useful tips to get up to speed with BUF development:
[http://go/learn-buf](http://go/learn-buf)

## Getting billing to work

You'll need to ensure your instance is on identity platform.

To do this, you may need to migrate your instance to id-plat.

You can use https://engservices-bamboo.internal.atlassian.com/browse/TG-UTOIDP-677 to do this

After that, follow instructions in the https://stash.atlassian.com/projects/PLS/repos/billing-ux-frontend/browse readme to deploy and link the new billing frontend. Key parts:

* You'll need to run `sessionStorage.setItem("billingUIConfigOverride", JSON.stringify({staticFeatures: {alwaysUseUnifiedBilling: true}}));` in the console (check readme for most up to date instruction)
* You'll need to be system-administrator on the instance to correctly override the registeredComponents.json file. You can give yourself this access through go/vernator (again, see the readme for more details).

## Docker build image

To try it out locally, you first have to build it:

    ./bin/docker/build-docker-image.sh

Or you can rebuild it without caching:

    ./bin/docker/build-docker-image.sh --no-cache

After that launch it locally with an interactive terminal:

    ./bin/docker/run-local.sh

Voila! Now just test whatever commands you want, e.g. `yarn test`, `yarn build`, etc.

## Dependencies for testing

Note: react-test-renderer is a dependency as we're depending on React >=15.5 and enzyme@2.8.2 . See the [enzyme readme](https://github.com/airbnb/enzyme)

## Debugging unit tests in VS Code

In order to have all the timeouts properly setup, you have to run `yarn test:debug` for this. After that you can attach to the headless Chrome via "Debug" -> "Start Debugging".

**Note**: Apparently, Karma has some problems loading the sourcemaps at initial test run, so your breakpoints might not be hit. To overcome that, put a `debugger` statement somewhere where it will be reached (the test file you're debugging seems to be perfect for that). After this `debugger` statement has been hit, the sourcemaps would have already loaded, and your further breakpoints will be reached as well. If someone manages to fix that - it'll be great!
