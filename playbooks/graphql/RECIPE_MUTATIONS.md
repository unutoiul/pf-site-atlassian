# Typing mutations

Mutations are a bit simpler to type than queries, nonetheless we'll go over a couple examples we use in our codebase.

# A basic mutation

First we'll create our placeholder component:

```tsx
interface UserCardProps {
  id: string;
}

class UserCardImpl extends React.Component<UserCardProps> {
  public render() {
    return (
      // The actual component
    );
  }
}

export const UserCard = UserCardImpl;
```

Next we'll introduce the mutation and the types required:

```tsx
// These are auto generated with `yarn graphql`
import { UserCardDeleteMutation, UserCardDeleteMutationVariables } from 'src/schema/schema-types';
import userCardMutation from './user-card.mutation.graphql';

const withMutation = graphql<
  // The InProps for the component. Another way to think of this 
  // is what is the base components public api.
  UserCardProps,
  // The mutation document which is auto generated into src/schema/schema-types. 
  // Don't type these manually.
  UserCardDeleteMutation,
  // The variables for the mutation, auto generated as well.
  UserCardDeleteMutationVariables,
>(userCardQuery);
```

Next we need to type our component by using the Apollo `ChildProps` type. `ChildProps` will provide the component with the Apollo types when accessing `this.props.data`. It takes the `InProps` and the mutation.

```tsx
import { ChildProps } from 'react-apollo';

class UserCardImpl extends React.Component<ChildProps<UserCardProps, UserCardDeleteMutation>> {
  ...
}
```

Then we connect up the component and run the mutation as usual:

```tsx
export const UserCard = withMutation(userCard);

...

public delete() {
  this.props.mutate({
    options: {
      variables: {
        id: 1,
      },
    },
  });
}
```

# A named mutation

Named mutations work in a similar way but they don't use `ChildProps` and instead you introduce a new type for the base component.

First we'll create our placeholder component:

```tsx
interface UserCardProps {
  id: string;
}

class UserCardImpl extends React.Component<UserCardProps> {
  public render() {
    return (
      // The actual component
    );
  }
}

export const UserCard = UserCardImpl;
```

Next we'll add our new mutation types which we will then pass to the base component. We'll use the provided `MutationFunc` for this:

```tsx
import { MutationFunc } from 'react-apollo';

interface NamedMutationProps {
  delete?: MutationFunc<UserCardDeleteMutation, UserCardDeleteMutationVariables>;
}
```

Now create the HOC with the named mutation:

```tsx
// These are auto generated with `yarn graphql`
import { UserCardDeleteMutation, UserCardDeleteMutationVariables } from 'src/schema/schema-types';
import userCardMutation from './user-card.mutation.graphql';

const withDeleteMutation = graphql<
  // The InProps for the component. Another way to think of this 
  // is what is the base components public api.
  UserCardProps,
  // The mutation document which is auto generated into src/schema/schema-types. 
  // Don't type these manually.
  UserCardDeleteMutation,
  // The variables for the mutation, auto generated as well.
  UserCardDeleteMutationVariables,
>(userCardQuery, { 
  // This value matches the new type key
  name: 'delete' 
});
```

Next we'll add the new mutation props to the base component and wire everything up.

```tsx
class UserCardImpl extends React.Component<UserCardProps & NamedMutationProps> {
  public delete() {
    this.props.delete({
      options: {
        variables: {
          id: 1,
        },
      },
    });
  }
}

export const UserCard = withDeleteMutation(UserCardImpl);
```

# Multiple named mutations

Follow the above named example but instead create two HOCs, each with different names. This is very similar to the multiple queries provided in "Typing queries" page.