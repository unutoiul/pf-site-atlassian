# GraphQL

This page contains some of the how-tos and gotchas around GraphQL.

## Stack

We use [`react-apollo`](https://github.com/apollographql/react-apollo) as a GraphQL client.

We also use [`apollo-client`](https://github.com/apollographql/apollo-client) as a client-side GraphQL server. Although sounds a bit weird, what this boils down to is this: our React components make queries/mutations as if they were talking to a real GraphQL server, but in fact we are resolving all the fields from the client. This will allow us to move this logic to a GraphQL server at a future point, when we have the time to do so.

## External guides

Here's a list of resources that you might find useful for GraphQL onboarding:

* [Introduction to GraphQL](https://graphql.org/learn/)
* [Introduction to Apollo Client (React)](https://www.apollographql.com/docs/react/)
* [Using Apollo with Typescript](https://www.apollographql.com/docs/react/recipes/static-typing.html)

## Recipes

These are the instructions on how to do something. It's a good starting point for onboarding onto using GraphQL in this repository.

List of recipes:
* [Adding GraphQL types from scratch](/graphql/RECIPE_ADD_NEW.md)
* [Typing queries](/graphql/RECIPE_QUERIES.md)
* [Typing mutations](/graphql/RECIPE_MUTATIONS.md)
* [Conventions](/graphql/CONVENTIONS.md)
