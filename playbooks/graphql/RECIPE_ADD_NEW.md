# Adding GraphQL types

Here's how you would add a new GraphQL type:

First you need to create a file `you-type-name-kebab-case.graphql` under `/src/schema/type-defs/`. This is where you'll be maintaining the information about the type and this type's relationships to other types. These types are also used for defining the schema in our pseudo GraphQL server.

After that you can define the type in this newly created file:

```graphql
# /src/schema/type-defs/you-type-name-kebab-case.graphql
type YourTypeNamePascalCase {
  id: ID!
  # ...
}
```

Make sure your type is accessible via an existing type. E.g. if this is a "root" type, extend `Query`:

```graphql
# /src/schema/type-defs/you-type-name-kebab-case.graphql
extend type Query {
  things: [YourTypeNamePascalCase]
}
```

Do the same for mutations:

```graphql
# /src/schema/type-defs/you-type-name-kebab-case.graphql
extend type Mutation {
  createThing(name: String!): YourTypeNamePascalCase!
}
```

When the type definition has been complete, run `yarn graphql` to have these types be added to the list of other types. This step is needed to tell our client-side GraphQL server about the new type

Now, our GraphQL server knows about the type, but it doesn't know how to fetch it - we need to create some resolvers.

Resolvers reside in `/src/schema/resolvers/` directory, and normally have their filename match the `*.graphql` file they're "resolving".

So, create a `you-type-name-kebab-case.resolver.ts` file (note the `.resolver` suffix). It should look something like this:

```ts
export const resolver = {
  YourTypeNamePascalCase: {
    someField: async () => { /* ... */ },
  },
  Query: {
    things: async () => { /* ... */ },
  },
  Mutation: {
    createThing: async () => { /* ... */ },
  },
};
```

Now that the resolver exists, we need to include it into the schema resolution config. To do that, open `/src/schema/resolvers/index.ts` and add a couple lines:

```ts
import { resolver as yourThingResolver } from './you-type-name-kebab-case.resolver'; // ADDED
/* ... */
const resolvers = [
  /* ... */
  yourThingResolver, // ADDED
  /* ... */
];
```

That's it! You can now use your newly created type in React components!
