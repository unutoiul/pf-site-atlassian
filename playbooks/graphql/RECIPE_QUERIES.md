# Typing queries

Typescript can be a bit tricky when typing graphql, so we'll go over common ways to type queries.

## A basic query

First we'll create our placeholder component:

```tsx
interface UserCardProps {
  id: string;
}

class UserCardImpl extends React.Component<UserCardProps> {
  public render() {
    if (this.props.data.loading) {
      return 'Loading...';
    }

    return (
      // The actual component
    );
  }
}

export const UserCard = UserCardImpl;
```

Next we'll introduce a query and types to pull in the data and wire up the component. Once your `.graphql` file is created, be sure to `yarn graphql` so the types are generated into `src/schema/schema-types`.

```tsx
// These are auto generated with `yarn graphql`
import { UserCardQuery, UserCardQueryVariables } from 'src/schema/schema-types';
import userCardQuery from './user-card.query.graphql';

const withQuery = graphql<
  // The InProps for the component. Another way to think of this 
  // is what is the base components public api.
  UserCardProps,
  // The query document which is auto generated into src/schema/schema-types. 
  // Don't type these manually.
  UserCardQuery,
  // The variables for the query, auto generated as well. If the query doesn't 
  // have any variables, leave this type blank.
  UserCardQueryVariables
>(userCardQuery, {
  options: (componentProps) => ({
    variables: {
      id: componentProps.id,
    },
  })
});
```

Next we need to type our component by using the Apollo `ChildProps` type. `ChildProps` will provide the component with the Apollo types when accessing `this.props.data`. It takes the `InProps` and the query.

```tsx
class UserCardImpl extends React.Component<ChildProps<UserCardProps, UserCardQuery>> {
  ...
}
```

Then we connect up the component:

```tsx
export const UserCard = withQuery(userCard);
```

You now have a fully connected and typed `<UserCard />` component.

## A named query

This one is similar to the basic query example but allows you to name the query and bypass using `ChildProps`. This is used mostly when a component has multiple queries.

First we create the base component:

```tsx
interface UserCardProps {
  id: string;
}

class UserCardImpl extends React.Component<UserCardProps> {
  public render() {
    return (
      // The actual component
    );
  }
}

export const UserCard = UserCardImpl;
```

Next we import the query and the newly generated types.

```tsx
import { UserCardQuery, UserCardQueryVariables } from 'src/schema/schema-types';
import userCardQuery from './user-card.query.graphql';
```

Then we create a new type for the named prop. This is different than using `ChildProps` since it will not exist on `this.props.data` but will now exist on `this.props.userCard`.

```tsx
interface NamedQueryProps {
  userCard?: QueryResult & Partial<UserCardQuery>;
}
```

Next we tell our component that it's going to receive this named prop as well as it's own props (UserCardProps):

```tsx
class UserCardImpl extends React.Component<UserCardProps & NamedQueryProps> {
  ...
}
```

Then we create our new HOC with the named prop:

```tsx

const withUserCardQuery = graphql<UserCardProps, UserCardQuery, UserCardQueryVariables>(UserCardQuery, {
  // Here we name the query to match the NamedQueryProps above
  name: 'userCard',
  options: (componentProps) => ({
    variables: {
      id: componentProps.id,
    },
  })
});
```

Now within the component we have access to the named property. Instead of `this.props.data` we'll use `this.props.userCard`:

```tsx
class UserCardImpl extends React.Component<UserCardProps & NamedQueryProps> {
  public render() {
    if (this.props.userCard && this.props.userCard.error) {
      return 'Error loading';
    }
  }
}

export const UserCard = withUserCardQuery(UserCardImpl);
```

## Multiple named queries

Like the named query example, we can create a component which makes multiple queries. Ideally, each component has only a single query but there are edge cases where multiple may be needed. This is not advised however, since the base component will now need to handle both queries' loading and error states individually.

First we need to create two interfaces for each of our queries:


```ts
interface UserCardQuery {
  userCard?: QueryResult & Partial<UserCardQuery>;
}

interface CurrentUserQuery {
  currentUser? QueryResult & Partial<UserCardCurrentUserQuery>;
}
```

Import the second query and newly generated types:

```ts
import { UserCardCurrentUserQuery, UserCardCurrentUserQueryVariables } from 'src/schema/schema-types';
import userCardCurrentUserQuery from './user-card-current-user.query.graphql';
```

Create a second HOC with a new named prop. Note: since this component will be wrapped by the previous query, we need to add the output of the first into the InProps of this.

```tsx
export const withCurrentUserQuery = graphql<UserCardProps & UserCardQuery, UserCardCurrentUserQuery, UserCardCurrentUserQueryVariables>(userCardCurrentUserQuery, {
  name: 'currentUser',
  options: (componentProps) => ({
    variables: {
      cloudId: componentProps.cloudId,
    },
  })
});
```

Next we wrap our component in both query HOCs.

```tsx
export const UserCard = withUserCardQuery(
  withCurrentUserQuery(
    UserCardImpl
  ),
);
```

## Query with remapped (derived) props

The `graphql()` HOC provides a fourth type which allows you to create new props to send to the base component. It looks like: `graphql<InProps, Query, QueryVariables, DerivedProps>`.

First let's create our base component and props. We'll create the standard `UserCardProps` for the component's public API, but now we'll create a new type `DerivedProps` which the component will receive from the HOC.

```tsx
interface UserCardProps {
  id: string;
}

interface DerivedProps {
  isLoading: boolean;
  error: boolean;
  someFancyValue: string;
}

class UserCardImpl extends React.Component<UserCardProps & DerivedProps> {
  public render() {
    return null;
  }
}

export const UserCard = UserCardImpl;
```

Next we'll create our HOC with the derived props, assuming we already have our query and types imported as explained in the examples above:

```tsx
const withData = graphql<UserCardProps, UserCardQuery, UserCardQueryVariables, DerivedProps>(userCardQuery, {
  props: (componentProps): DerivedProps => ({
    isLoading: !!componentProps.data && componentProps.data.loading,
    hasError: !!(componentProps.data && componentProps.data.error),
    someFancyValue: componentProps.data.currentUser.id === 12 ? 'Special User' : 'Normal User',
  }),

  options: (props) => ({
    variables: {
      id: componentProps.id,
    },
  }),
});
```

Now by wrapping our component in this derived prop HOC, we can access the computed prop `someFancyValue` directly via `this.props.someFancyValue`:

```tsx
class UserCardImpl extends React.Component<UserCardProps & DerivedProps> {
  public render() {
    if (this.props.loading) {
      return 'Loading...';
    }

    if (this.props.error) {
      return 'Something went wrong...';
    }

    return <p>{this.props.someFancyValue}</p>;
  }
}

export const UserCard = withData(UserCardImpl);
```