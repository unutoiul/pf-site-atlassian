# Conventions

Listed here are some conventions/guidelines since they're not currently enforced by the build or lint.

* Use `*.query.graphql` for query filenames
* Use `*.mutation.graphql` for mutation filenames
* Always namespace queries and mutations to the component that is making the query. The reasoning is the types are automatically generated into a single namespace, so any query/mutation name should be unique within our codebase or the types will be overwritten without warning. If a component is called `UserCard`, the query should be called `query UserCard`.