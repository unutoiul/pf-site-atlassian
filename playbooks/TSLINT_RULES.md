# What is that?

The purpose of this file is to keep track of why we had this or that tslint rules enabled/disabled.

Run `yarn tslint-ruler` to get an idea of which rules are justified and which ones are not.

## Rules that should/can be enabled

### jsx-no-lambda
Lambdas **should** be forbidden in JSX attributes due to their rendering performance impact.

### arrow-parens
We should enable that in some form for further code consistency

## Enabled rules

### jsx-use-translation-function

## Disabled rules

### no-collapsible-if
Someone had a reason for disabling it because sometimes it makes sense to nest `if` statements for readability purpose. An example would probably be nice here.

### no-unnecessary-else
Sometimes returning from within an `else` statement makes the code clearer.  An example would probably be nice here.

### jsx-no-multiline-js
If we enable this rule, the following code samples will be violations, which does not seem very reasonable.
```tsx
/* Example 1 */
<div>
  {this.items.map(item => (
    <span>{item.text}</span>
  ))}
</div>

/* Example 2 */
<div>
  {condition && (
    <SomeConditionalItem>
      {children}
    </SomeConditionalItem>
  )}
</div>
```

### max-line-length
Modern editors allow have great support for word wrapping, and pushing developers to have their code condensed to less than N characters doesn't seem to bring much value.

### align
Vertical alignment might results in a lot of unnecessary changes due to parameter renames and stuff like that, so we avoid it to keep git diffs to the point.

### object-literal-sort-keys
Having to specify properties in alphabetical order can be extremely annoying:
```ts
/* This would have 3 violations */
defineMessages({
  title: {
    id: 'title',
    defaultMessage: 'Some title', // violation - this should go before "id"
  },
  description: { // violation - this should go before "title"
    id: 'description',
    defaultMessage: 'Some description', // violation - this should go before "id"
  },
});
```
