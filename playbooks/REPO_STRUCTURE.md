# Repository Structure
```
|
├──src/
|  ├── organization/
|  │   └── member-page/
|  |       ├── index.tsx
|  |       ├── member-page.tsx
|  |       ├── member-page.test.tsx
|  |       ├── member-page.story.tsx
|  |       ├── member-page.query.graphql
|  |       └── member-page.mutation.graphql
|  ├── site/
|  │   ├── emoji-page/
|  |   |   ├── index.tsx
|  |   │   ├── emoji-page.tsx
|  |   │   ├── emoji-page.test.tsx
|  |   │   ├── emoji-page.story.tsx
|  |   │   └── emoji-page.query.graphql
|  │   └── emoji-table/
|  |       ├── index.tsx
|  |       ├── emoji-table.tsx
|  |       ├── emoji-table.test.tsx
|  |       ├── emoji-table.story.tsx
|  |       ├── emoji-table.query.graphql
|  |       └── emoji-table.mutation.graphql
|  └── dashboard/
|  │   └── organization-card/
|  |   |   ├── index.tsx
|  |   |   └── organization-card.tsx
|  |   |   └── organization-card.story.tsx
|  │   └── organization-overview/
|  |   |   ├── index.tsx
|  |   |   ├── organization-overview.tsx
|  |   |   ├── organization-overview.test.tsx
|  |   |   └── organization-overview.story.tsx
|  │   └── navigation/
|  |       ├── index.tsx
|  |       ├── navigation.tsx
|  |       ├── navigation.test.tsx
|  |       ├── navigation.story.tsx
|  |       ├── navigation.query.graphql
|  |       └── navigation.mutation.graphql
|  └── common/
|  │   └── user-picker/
|  |   |   ├── index.tsx
|  |   |   ├── user-picker.tsx
|  |   |   ├── user-picker.test.tsx
|  |   |   └── user-picker.story.tsx
|  │   └── group-picker/
|  |   |   ├── index.tsx
|  |   |   ├── group-picker.tsx
|  |   |   ├── group-picker.test.tsx
|  |   |   └── group-picker.story.tsx
|  │   └── page-layout/
|  |   |   ├── page-layout.tsx
|  |   |   ├── page-layout.test.tsx
|  |   |   ├── page-layout.story.tsx
|  |   |   ├── page-layout.query.graphql
|  |   |   ├── page-layout.mutation.graphql
|  |   |   ├── collapsed-page-layout.tsx
|  |   |   └── index.tsx
|  │   └── utilities/
|  └── app.tsx
|  └── index.html
|  └── schema/
|      └── resolvers/
|      └── api/
|      └── type-defs/
├──test/
|  ├── integration-tests/
|  └── synthetic-checks/
└── mocks/
```

Driving principle behind the repository structure is 'domain-based' structuring. We aim to have a flat directory structure. This enables easy discovery of available components and reuse.

At the top level we have `organization`, `site`, `dashboard`, `common` and the top level app component. Functionality that can be shared across organization and site but are not re-usable can be their own top-level folders, eg config, schema, store. All unit tests live besides their components. Integration tests, pollinator synthetic checks etc. live outside `src` in `test` folder.

## Organization

This contains all the components used in the organization context of the app. We try to have directory structure with minimal nesting. All the individual screen/page components are definitely their own folders. There can be some modals within pages that can be their own top level folders. We create top-level folders for components that can stand on their own.
Ex: A table within a page can be it's own top level folder. Tabs within a page can also be independent folders without nesting in the parent pages' folder. This helps in discovery of components and easy movement/reuse.

## Site

This contains all the components used in the site context of the app. Folder structure guidelines are the same as for organization.

## Dashboard

This contains all the components used in the dashboard i.e. `/` context of the app. Components are at top level without nested folders. There will be `site-card`, `organization-overview` like components at the top level.

## Common

These are common reusable components that can used across organization, site and dashboard. It will include common components for page-layouts, flags, loading-button which are used across the repository. `common` has a flat folder structure. We have one common utilities folder available here for reuse and it would be the only place for utilities
Components in `common` should not depend on components in any other folder.

## Schema

For using apollo client on the UI we have to define the types and resolvers in the client code. The schema definitions, types, resolvers all live under this folder. This is essentially our local client site graphql resolver. All the client models and apis used will be under this folder.


## Repo structure outside src

### Test

#### Integration

All our app mounted tests live in this folder.

### Synthetic checks

The pollinator checks running against production

### Mocks

This contains our mocking for the service endpoints used both in integration testing and running locally.

