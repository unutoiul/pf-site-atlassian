# Modal Dialogs

## MutationModal

A common use case for a modal dialog is to do a simple mutation. See `common/modal/mutation-modal` for more information on this component. Further documentation and examples exist in the storybook for this common component.
