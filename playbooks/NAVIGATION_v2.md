# Navigation

## About

There are multiple types of navigation:
1. Global Navigation
2. Organization Navigation
3. Site Navigation - For now look at Navigation Legacy

In this doc we will focus on adding navigation link to an Organization.

2 things need to be added for a new navigation link to work:
- [Routes](#add-routes)
- [Navigation Links](#add-navigation-links)

### Add Routes

Go to `src/chrome/routes/org-routes.tsx`, update the render method to add your new route pointing to the component it should render.

### Add Navigation Links

A navigation link can be created using `src/chrome/navigation-v2/navigation-link.tsx` component.

You can wrap this component with what is relevant for your use-case. You can add analytics wrapper, onboarding, flag, etc.

#### Top level organization navigation link
Add the new navigation link in `src/chrome/navigation-v2/organization/navigation.tsx`.

[Example](#organization-top-level-links)

#### Nested organization navigation link
The top level link for the nested navigation should be added in `src/chrome/navigation-v2/organization/navigation.tsx`.

For nested navigation there are 2 things that are required:
- Element with all the nested items, including a `Back` link
- A function, given an `orgId` and currently active `path` can return whether any of its nested links are active. This is used to initialize the state of the navigation.

[Example](#organization-nested-links)

##### Dynamic navigation links
In case you need the link to be dynamic, you can access the condition via a graphql query and render conditionally.

[Example](#dynamic-links)

## Examples

### Organization Top Level Links

```tsx
export class Navigation extends React.Component<AllProps, {}> {
  public render() {
    const navItems = getTopLevelLinks();

    return (
      <AkNavigation>
        <AkContainerNavigationNested stack={navItems} />
      </AkNavigation>
    );
  }

  private getTopLevelLinks = () => {
    const { match: { params: { orgId } }, intl: { formatMessage } } = this.props;

    return [
      (
        <div>
          <NavigationLink path={`/o/${orgId}/overview`} title={formatMessage(messages.overview)} />
          <MyCustomNavigationLink path={`/o/${orgId}/members`} title={formatMessage(messages.admins)} />
          {/* You can add more top level navigation links here. */}
        </div>
      ),
    ];
  };
}
```

### Organization Nested Links

```tsx
const enum NavigationState {
  TopLevel,
  Billing,
  // ***Add new Navigation State***
}

export class Navigation extends React.Component<AllProps, {activeNavigation: NavigationState}> {
  constructor(props) {
    super(props);
    const { match: { params: { orgId } }, location: { pathname } } = props;

    if (isBillingLinkActive(orgId, pathname)) {
      this.state = {
        activeNavigation: NavigationState.Billing,
      };
    // ***Add new nested nav condition here***
    } else {
      this.state = {
        activeNavigation: NavigationState.TopLevel,
      };
    }
  }

  public render() {
    const getNavItems = {
      [NavigationState.TopLevel]: this.getTopLevelLinks,
      [NavigationState.Billing]: this.getBillingLinks,
      // ***Add nav items corresponding to the new nested nav***
    };

    const navItems = getNavItems[this.state.activeNavigation]();

    return (
      <AkNavigation>
        <AkContainerNavigationNested stack={navItems} />
      </AkNavigation>
    );
  }

  private getTopLevelLinks = () => {
    const { match: { params: { orgId } }, intl: { formatMessage } } = this.props;

    return [
      (
        <div>
          <NavigationLink path={`/o/${orgId}/overview`} title={formatMessage(messages.overview)} />
          <NavigationLink
            iconRight={<AkArrowRightIcon label=""/>}
            onClick={this.setActiveNavigation(NavigationState.Billing)}
            title={formatMessage(messages.billing)}
          />
          {/* ***Add navigation link similar to the one above and set onClick handler to point to your new Navigation State*** */}
        </div>
      ),
    ];
  };

  private getBillingLinks = () => {
    const { match: { params: { orgId } }, intl: { formatMessage } } = this.props;

    return [
      [],
      billingLinks(orgId, formatMessage, this.setActiveNavigation(NavigationState.TopLevel)),
    ];
  };

  // Create a nav items fetcher similar to `getBillingLinks`. Yes, we need the first item to be empty to tell `AkContainerNestedNavigation` component that this is a nested item.

  private setActiveNavigation = (activeNavigation: NavigationState) => () => {
    this.setState({ activeNavigation });
  };
}

// We need two more details for the above to work

// This will be used to create the nav items
export const newNestedLinks = (orgId: string, formatMessage, onClick: () => void): JSX.Element => (
  <div>
    {/* Always have a back button in nested nav */}
    <BackNavigationLink onClick={onClick} />
    <NavigationLink path={nestedLink1(orgId)} title={formatMessage(messages.myNestedLink1)} />
  </div>
);

// This will be used to determine the initial state of the nav
export const isNestedLinkActive = (orgId: string, pathname: string): boolean => {
  // Based on the orgId and currently active pathname, determine if any of your nested links are active.

  return isMyNestedNavActive;
};
```

### Dynamic Links

```graphql
# navigation.query.graphql
query OrgNavigation($id: String!) {
  organization(id: $id) {
    id
    isFeatureEnabled
  }
}
```

```tsx
import query from 'navigation.query.graphql';

class NavigationImpl extends React.Component<AllProps, {}> {
  public render() {
    const navItems = getTopLevelLinks();

    return (
      <AkNavigation>
        <AkContainerNavigationNested stack={navItems} />
      </AkNavigation>
    );
  }

  private getTopLevelLinks = () => {
    const { match: { params: { orgId } }, intl: { formatMessage }, isFeatureEnabled } = this.props;

    return [
      (
        <div>
          <NavigationLink path={`/o/${orgId}/overview`} title={formatMessage(messages.overview)} />
          <MyCustomNavigationLink path={`/o/${orgId}/members`} title={formatMessage(messages.admins)} />
          {isFeatureEnabled ?
            <NavigationLink path={`/o/${orgId}/feature`} title={formatMessage(messages.feature)} /> :
            null
          }
        </div>
      ),
    ];
  };
}

const withCondition = graphql(
  query,
  {
    options: (componentProps) => ({
      variables: {
        id: componentProps.match.params.orgId,
      },
    }),
    props: ({ data }) => ({
      if (data.loading || data.error) {
        return { isFeatureEnabled: false }
      }

      return { isFeatureEnabled: data.organization.isFeatureEnabled };
    })
  },
);

export const Navigation = withCondition(injectIntl<{}>(withRouter<AllProps>(NavigationImpl)));
```

