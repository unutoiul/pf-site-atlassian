# Deprecations

## currentSite

The `currentSite` query has been deprecated, and will be removed.

Instead of using `currentSite` to determine the site, find the cloud id from the URL, and use the `site` query.

For example,

Old:

```graphql
query SiteFeatureFlags($flagKey: String!) {
  currentSite {
    id
    flag(flagKey: $flagKey) {
      id
      value
    }
  }
}
```

New:

```graphql
query SiteFeatureFlags($id: String!, $flagKey: String!) {
  site(id: $id) {
    id
    flag(flagKey: $flagKey) {
      id
      value
    }
  }
}
```

## akMath

`akMath` and it's helpers like `akMath.multiply` and `akMath.divide` have now been deprecated by AtlasKit.

Use simple operators like: "*" and "/" in it's place.

## common/modal-dialog

You can directly use the atlaskit modal dialogs with the latest API.