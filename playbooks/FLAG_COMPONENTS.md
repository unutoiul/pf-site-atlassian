# Flag Components

## About

[Flag components](https://ak-mk-2-prod.netlify.com/packages/elements/flag) are provided from AtlasKit as a way to show success/error messages. In order to use these, wrap your page component with the `withFlag()` HOC. This provides your wrapped component with the props

```tsx
showFlag(flag: FlagDescriptor);
hideFlag(flagId: string);
```

Use these methods within your component to dispatch new flags to render inside the framework.

*Note: flags may contain any JSX in the `title` and `description` props -- however the prop `id` must be unique.*

## Example

Here we're using the injected `showFlag` property to show a success and error flag.

```tsx
import { createErrorIcon, createSuccessIcon } from './common/error/error-icons';
import { withFlag, FlagProps } from './common/flag';

interface MyComponentProps {
  content?: string;
}

class MyComponentImpl extends React.Component<MyComponentProps & FlagProps, {}> {
  public componentDidMount() {
    this.showErrorFlag();
    this.showSuccessFlag();
  }

  private showErrorFlag() {
    this.props.showFlag({
      autoDismiss: true,
      icon: createErrorIcon(),
      id: 'error-flag',
      title: (
        <h1>Error</h1>
      )
      description: 'Something went wrong!',
    });
  }

  private showSuccessFlag() {
    this.props.showFlag({
      autoDismiss: false,
      icon: createSuccessIcon(),
      id: 'success-flag',
      title: (
        <h1>Success</h1>
      ),
      description: (
        <p>Click <a href="#">here</a> to see your result.</p>
      ),
    });
  }
}

export const MyComponent = withFlag(MyComponentImpl);
```
