# State Management

There's two different state management systems in this repo, one for remote state and one for local-only state. For the most part apps will only need to worry about managing remote state.

## Remote State

We use GraphQL so that the UI doesn't need to be concerned with the specifics of how to fetch data, it only needs to specify what data it requires. Fetching data is handled by resolvers in the GraphQL schema.

### Resolvers and Schema

If your app is adding another Entity type or a new relationship between Entities you'll need to update the GraphQL schema type found in `src/schema/type-defs` and schema resolver found in `src/schema/resolvers`. The schema type definitions and the set of resolvers handle fetching data. If you wanted to add a new User Entity it might look something like:

Type definition added to some file like `src/schema/type-defs/user.graphql`:
```graphql
type User {
  id: String!
  name: String!
  country: String
}

extend type Query {
  user(id: String): User
}
```
Resolver added to some file like `src/resolvers/user.resolver.ts`:
```tsx
export const resolver = {
  Query: {
    user: (_, { id }) => fetch(`/users/${id}`),
  }
}
```

After that add the resolver to combined resolver of the application found in `src/resolvers/index.ts`.
For type definitions, recommendation is to extend only Query and Mutation types. Define rest of the type definitions in one place.

[You can learn more about defining your GraphQL schema here.](http://dev.apollodata.com/tools/graphql-tools/generate-schema.html)

### GraphQL Queries in React

Connecting a React component to GraphQL state is very similar to connecting a component to Redux state.

```graphql
# my-component.query.graphql
query MyComponentQuery { 
  todos { 
    id
    text 
  } 
}
```

```tsx
import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import { TodosQuery } from '../../schema/schema-types';
import myComponentQuery from './my-component.query.graphql';

type MyComponentProps = ChildProps<null, TodosQuery>;

class MyComponentImpl extends React.Component<MyComponentProps> {
  render() {
    return <div>...</div>;
  }
}

export const MyComponent = graphql<TodosQuery>(myComponentQuery)(MyComponentImpl);
```

After writing your query you need to run `yarn graphql`. This validates your graphql queries against your schema and automatically generates the combined graphql type definitions stored in `src/schema-defs` and Typescript types stored in `src/schema-types.ts`. Whenever you change the schema or your GraphQL queries you should rerun this command (this will be made part of the watch loop in the near future).

[You can learn more about using GraphQL with React here](http://dev.apollodata.com/react/higher-order-components.html)

## Local-only State

If your React components share state that is local-only then you should manage that state using Redux. We use Typescript to ensure that actions, action creators, reducers and connected components pass around the correct data without type errors. Examples of all the pieces required to wire up a piece of redux state follow.

Action type and creator:
```tsx
export type APP_DOMAIN_CLAIMS_RECEIVE_CLAIMS = 'APP_DOMAIN_CLAIMS_RECEIVE_CLAIMS';
export const APP_DOMAIN_CLAIMS_RECEIVE_CLAIMS: APP_DOMAIN_CLAIMS_RECEIVE_CLAIMS = 'APP_DOMAIN_CLAIMS_RECEIVE_CLAIMS';

export interface ReceiveDomainClaims {
  type: APP_DOMAIN_CLAIMS_RECEIVE_CLAIMS;
  domains: Domains[];
}

export const receiveDomainClaims = (domains: Domains[]): ReceiveDomainClaims => ({
  type: APP_DOMAIN_CLAIMS_RECEIVE_CLAIMS,
  domains,
});
```

Application state and reducer:
```tsx
export interface DomainClaimState {
  isFetching: boolean;
  domains: Domains[];
  orgId: string;
  record: string;
}

const domainClaimReducer = (state: DomainClaimState = { isFetching: true, domains: [], orgId: '', record: '' }, action: Action): DomainClaimState => {
  switch (action.type) {
    case APP_DOMAIN_CLAIMS_RECEIVE_CLAIMS:
      return {
        ...state,
        isFetching: false,
        domains: action.domains,
      };
    default:
      return state;
  }
};
```

Add your reducer to the appsReducer in `src/apps/apps.reducer.ts`
```tsx
export const appsReducer = combineReducers({
  adg3Pages: adg3PagesReducer,
  domain: domainClaimReducer, //NEW
  members: membersReducer,
});
```

Add your application state to AppsState in `src/root-state.ts`
```tsx
interface AppsState {
  adg3Pages: Adg3PagesState;
  domainClaim: DomainClaimState; //NEW
  members: MembersState;
}
```

Create your `ConnectedProps` interface, `mapStateToProps` function and wire up your React component using the `connect` hire order component from `react-redux`.
```tsx
interface ConnectedProps {
  isFetching: boolean;
  dispatch?: Dispatch<any>;
  domains: Domains[];
  organizations: Organization[];
  record: string;
}

const mapStateToProps = (state: RootState): ConnectedProps => ({
  isFetching: state.apps.domainClaim.isFetching,
  organizations: [],
  domains: state.apps.domainClaim.domains,
  record: state.apps.domainClaim.record,
});

@(connect as any)(mapStateToProps)
export class DomainClaimPage extends React.Component<ConnectedProps & OwnProps, State> {
  ...
}
```

## React Context

You **should not use** React Context within apps. 
One of the things that React Context facilitates is decentralization of state. This is required only at top-level for registering components for things like routing, modal dialogs or focused tasks. In these cases  without context you would end up with a file that pulls components from 10 other files. That would have been god file is a problem, and such an approach doesn't scale very well. 


