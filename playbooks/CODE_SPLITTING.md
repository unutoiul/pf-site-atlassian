# Code Splitting

## About

Site admin framework uses the [react-loadable](https://github.com/thejameskyle/react-loadable) component to code split at the component level.

We provide a loading component called `PageLoading` from `'../../common/loading'`, which provides the default loading and error states for react-loadable, so you don't have to. Just make sure to pass it as a prop to `Loadable()`.

## Example

Below is an example using `Loadable` and `PageLoading` to split off the Emoji page.

*Note: The comment `webpackChunkName: "name"` is used to name the chunk. Make sure this makes sense semantically with what you're splitting.

```tsx
import * as React from 'react';
import Loadable from 'react-loadable';
import { PageLoading } from '../../common/loading';

export const LoadableEmojiPage = Loadable({
  // tslint:disable-next-line space-in-parens
  loader: async () => import(
    /* webpackChunkName: "emoji" */
    './emoji-page',
  ),
  loading: PageLoading,
  render({ EmojiPage }, props) {
    return <EmojiPage {...props} />;
  },
});
```