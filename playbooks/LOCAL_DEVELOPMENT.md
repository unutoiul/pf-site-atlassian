# Local Development

## Prerequisites

- [Node.js](http://nodejs.org/) >= v4 must be installed.
- [Yarn](https://yarnpkg.com/)

## Installation

- Running `yarn` in the app's root directory will install everything you need for development.

## Development Server talking to "hipster-user-management"

##### Prequisites:
- [user-management](https://stash.atlassian.com/projects/UN/repos/user-management/browse)

##### Run UM:
1. Open UM in IntelliJ. Find `InMemoryApplication` - Right click on the class, and hit "Run"
2. Wait for UM to start up. Obtain the cloudId by searching the startup logs for the `Provisioning for ` string.
3. Navigate to to [http://localhost:4075/admin](http://localhost:4075/admin) and log in locally with **admin@example.com**

##### Change config in pf-site-admin-ui:
1. Navigate to `mock/services/site-service/responses.ts`
2. Change the mocked repsonse of `/_edge/tenant_info` to be the cloudId you obtained earlier.
3. Run `pf-site-admin-ui` with the command: `PROXY_UM_HIPSTER=http://localhost:4075 PROXY_UM=http://localhost:4075 yarn start`
4. Navigate to [http://localhost:3001/admin](http://localhost:3001/admin).

## Development Server linked to User Management

- `yarn start` will run the app's development server at [http://localhost:3001](http://localhost:3001) with hot module reloading.
- Start User Management backend service to render this UI
  - Prerequisite [user-management](https://stash.atlassian.com/projects/UN/repos/user-management/browse)
  - Enable react based UI
    - Change the value of feature flag to `true`, and the base url to `localhost:3001` [here](https://stash.atlassian.com/projects/UN/repos/user-management/browse/distribution/standalone/src/main/resources/application-local.properties#95)
  - Follow the instructions in [`user-management/README.md`](https://stash.atlassian.com/projects/UN/repos/user-management/browse/README.md) to start UM locally

## Development Server isolated from User Management
- `yarn start` will run the app's development server at [http://0.0.0.0:3001](http://0.0.0.0:3001) with hot module reloading.
- Visit [http://0.0.0.0:3001/admin](http://0.0.0.0:3001/admin)

### Changing proxied and mocked responses

Visit `src/mock`. Individual contributing teams can write mock folders, with their own rules for targets. This is `localhost:3001` for mocks, served through `webpack-dev-server`, or proxied to a different service.

Currently the process for switching between mocks and proxies involves commenting in/out the `target` line. See `mock/services/organization-service` for an example.

## Running Tests

- `yarn test` will run all the tests once with linting.
- `yarn test:nolint` will run all the tests once without linting.
- `yarn test:unit` will run the unit tests through mocha (note: this has a much faster dev loop than going through karma)
- `yarn test:coverage` will run the tests and produce a coverage report in `build/coverage/`.
- `yarn test:watch` will run the tests on every change, through karma

## Building

- `yarn build` creates a production build by default.

   To create a development build, set the `NODE_ENV` environment variable to `development` while running this command.

- `yarn clean` will delete built resources.

## Running Storybook

- `yarn storybook` will load all the stories at [http://localhost:6006](http://localhost:6006)

## Running Playbooks

- Run `yarn playbooks:install` once to load everything. Unfortunately, GitBook installation process is not ideal :(
- `yarn playbooks` will load a web interface of the documentation at [http://localhost:4000](http://localhost:4000)

## Setting up background tasks in VS Code

You can setup background tasks in VS Code that can do cool stuff like regenerate code when specific files change.

### Enabling background tasks autolaunch

VS Code doesn't natively support auto launching tasks, so install the AutoLaunch (philfontaine.autolaunch) plugin and restart VS Code to start background tasks automatically when VS Code is opened. Tasks can also be started manually without installing the plugin.

### Background task configuration

Tasks are configured in `.vscode/tasks.json`

### GraphQL background task

This task watches for changes to `.graphql` files in `\src`. Changes are batched in intervals, and `yarn graphql` is executed for each batched change interval.

Task file location: `bin/graphql-watcher.sh`

### I18n background task

This task is a little more complicated. It watches changes to `.tsx` files, and when a `.tsx` is modified, a git diff of the file is grepped for `defaultMessage`. If `defaultMessage` is found, the task will run the yarn i18n task. Since this task doesn't batch changes like the GraphQL task, it saves a reference to the PID of the most recent execution of yarn i18n and simply kills it and starts again when a new change is found.

Note that this does not currently include BUX i18n.

Task file location: `bin/i18n/i18n-watcher.sh`
