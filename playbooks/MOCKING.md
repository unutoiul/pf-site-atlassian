# Mocking

Mocking can be done on a per-service basis at the moment. This will later be converted to GraphQL mocking once GraphQL coverage is high enough across the contributing apps.

By default, when running `yarn start`, all responses are mocked. They can be configured to be proxied on a per-command basis.

# Proxying

Different services can be proxied. See `mock/arguments.ts` for a list of available arguments. By default each service will be mocked, but they can be proxied locally (or remotely) by setting environment variables. For example:

```
PROXY_ORG=http://localhost:5075 yarn start
```

## Proxying a production instance with your local dev server

The app is a wrapper around old and new pages. The new pages are contained in the `src/apps` folder, but the old pages are hosted in an iframe.

This iframe that hosts old user management pages can be proxied to staging and production instances. This requires copying cookies across domains.

```
PROXY_IFRAME=https://product-fabric.atlassian.net yarn start
```

or

```
PROXY_ORG=https://id-org-manager.prod.atl-paas.net/ yarn start
```

You will need to follow the [Cookie Copy](COOKIE_COPY.md) playbook to set a *production* cookie on the domain `localhost:3001`.

If you don't do this, the iframe is redirect to login, which will break the iframe.

## Proxying everything to production

```
yarn run start:prod
```

will proxy everything to production. This assumes you are a site admin of the product-fabric instance, and have copied your cookies, as stated above. If not, you can do a similar thing with your own production site by setting the environment variables accordingly.

# Contributing service mocks and proxies

A new service that gets added can be configured to mock and proxy locally.

To add a new service:

* Add a folder in `mock/services` that exports from `index.ts` a `config` and `responses` object (see `organization-service` for an example).
* In `mock/services/index.ts`, re-export these merged config and responses objects.
* Add an argument to `mock/arguments.ts` to enable CLI set proxy targets.
