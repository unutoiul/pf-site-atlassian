# Bamboo Builds

## Adding new steps to commit metadata build

When you want to add a new step to the commit metadata build, use `/bin/build-metadata/tasks/pre-migration-task.ts` as an intermediate step.

So, you would add something like that to the `commit-metadata.groovy` file:

```groovy
// /bamboo-plan-template/commit-metadata.groovy
    typescriptJob(
      key: 'RUNYOURTHING',
      name: 'Run Your Thing',
      description: 'Runs your things that does great stuff',
      filenameAndArgs: './bin/build-metadata/tasks/your-thing.ts',
    )
```

Where `your-thing.ts` holds the task that you want to run. This task won't be run until bamboo plan template updates land (that is, after merge to `master` and a little while more). To verify that your changes won't break or significantly slow down metadata builds, run your task from within `pre-migration-task.ts` file like that:

```ts
// /bin/build-metadata/tasks/pre-migration-task.ts
import { runYourThing } from './your-thing';
import { runAsyncTask } from '../util/runner';

if (process.argv.indexOf('--your-thing-addition') < 0) {
  runAsyncTask(runYourThing);
}
```

The process argument check is there to ensure that the task isn't run twice after the plan template changes land. Perform one more modification of the `commit-metadata.groovy` file:

```groovy
// /bamboo-plan-template/commit-metadata.groovy
    typescriptJob(
      key: 'RUNPREMIGRATION',
      name: 'Run pre-migration tasks',
      description: 'Runs tasks that have not yet been incorporated into the general plan',
      filenameAndArgs: './bin/build-metadata/tasks/pre-migration-task.ts --your-thing-addition',
      //                                                                ^^^^^^^^^^^^^^^^^^^^^^
    )
```

This will ensure that your task only runs once for both when the plan changes are and aren't applied.

You would need to create a cleanup PR after that, clearing the contents of `/bin/build-metadata/tasks/pre-migration-task.ts` file, and removing the argument for `RUNPREMIGRATION` job inside `/bamboo-plan-template/commit-metadata.groovy`.
