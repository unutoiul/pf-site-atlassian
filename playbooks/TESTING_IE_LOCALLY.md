# Testing Locally in IE

In order to get IE to load Admin Hub locally you need to:

**Get IE**

* Download VirtualBox: https://www.virtualbox.org/
* Download a VM from Windows for VirtualBox: https://developer.microsoft.com/en-us/microsoft-edge/tools/vms/

**Update build configs**

* Open `.babelrc` and copy the production presets to development
* Open `/src/apps/billing/.babelrc` and copy the production presets to development

**Clear out caches**

* Remove node modules cache `rm -rf ./node_modules/.cache`
* Remove build cache `rm -rf build/.awcache`

**Final steps**

If you are in the office and not on VPN just restart Webpack and go to http://YOURHOST.office.atlassian.com:3001/admin.

Otherwise, follow these more complicated steps to configure Windows to point to Mac:

* Open Notepad as an Administrator
* Open the file `C:\windows\system32\drivers\etc\hosts` and add this line: `10.0.2.2   outer`
* Open `/webpack/output.ts` and update the `publicPath` to point to `10.0.2.2`
* Open `src/common/config/config.ts` and update `localhost` references to point to `10.0.2.2`

Now restart Webpack and go to http://10.0.2.2:3001.

### Testing in IE with staging servers

Follow the instructions for testing locally, and additionally in `src/common/config/config.ts`:

* Modify all instances of localhost to point to their stg-east equivalents
* Change `apiUrlConfig` to point to https://admin.stg.atlassian.com/gateway/api
* Change `loginUrlConfig` to point to https://id.stg.internal.atlassian.com/login
* [Disable CORS in IE 11](https://stackoverflow.com/questions/20947359/disable-same-origin-policy-internet-explorer?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa)

The first time you attempt to load Admin Hub you should be redirected to the AID login page. After doing that you should have the cookie set so return to http://10.0.2.2:3001.