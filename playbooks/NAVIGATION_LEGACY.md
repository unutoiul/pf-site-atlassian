# Navigation Legacy

## TL:DR;

This information is relevant for site specific UI navigation.
Refer to examples in the repository or to [example below](#rough-example) and do the same things.

## Intro

Various navigation items might be contributed:

* [Routes](#add-routes)
* [Navigation sections](#add-sections)
* [Drawer links](#add-drawer-links)

### Add routes

To add static routes (e.g. ones that are always accessible) open `/src/chrome/navigation/routing.tsx` and find route declarations (`export const routes: RouteProps[]`). Add your route there, adhering to `RouteProps` interface provided by `react-router`.

For adding dynamic routes whose accessibility depends on user preference/feature flag/permissions, consult [Navigation mounters](#navigation-mounters) section below.

### Add sections

To add static navigation sections (e.g. ones that are always present) open `/src/chrome/navigation/routing.tsx` and find section declarations (`export const sections: NavigationSection[]`). Add your section there, adhering to `NavigationSection` interface.

For adding dynamic sections whose presence depends on user preference/feature flag/permissions, consult [Navigation mounters](#navigation-mounters) section below.

### Add drawer links

To add static drawer links (e.g. ones that are always present) open `/src/chrome/navigation/routing.tsx` and find drawer link declarations (`export const drawerLinks: DrawerLink[]`). Add your section there, adhering to `DrawerLink` interface.

For adding dynamic drawer links whose presence depends on user preference/feature flag/permissions, consult [Navigation mounters](#navigation-mounters) section below.

## Navigation mounters

"Navigation mounters" are there to support non-trivial scenarios for dynamically adding routes/sections/links. They are basically React components with certain expectations from `chrome` side.

All mounters logic will most likely be concentrated inside lifecycle methods, namely `componentDidMount` and `componentWillUpdate`. It is adviced that a mounter returns `null` from its `render` method.

All mounters receive a number of props as indicated by `NavigationMounterProps` interface. These props contain utility callbacks for registering/unregistering dynamic routes, sections, and drawer links.

### Dynamic routes

Dynamic routes can be added/removed based on a flag. Mounter implementations can utilize `componentWillUpdate` lifecycle hook:

```tsx
componentWillUpdate(nextProps) {
  if (this.props.isFoo !== nextProps.isFoo) {
    // when foo changed, (un)register routes
    const {
      isFoo,
      registerRoutes,
      unregisterRoutes,
    } = nextProps;

    const routes: RouteRegistrationData = { /* ... */ };

    if (isFoo) {
      registerRoutes(routes);
    } else {
      unregisterRoutes(routes);
    }
  }
}
```

### Dynamic navigation sections/drawer links

Dynamic sections can be added/removed based on a flag. Mounter implementations can utilize `componentWillUpdate` lifecycle hook:

```tsx
componentWillUpdate(nextProps) {
  if (this.props.isFoo !== nextProps.isFoo) {
    const {
      isFoo,
      updateNavigationSection,
      removeNavigationSection,
    } = nextProps;

    const navSection = { id: 'foo-nav', title: messages.foo, links: [/* ... */] };

    if (isFoo) {
      updateNavigationSection(navSection);
    } else {
      removeNavigationSection(navSection);
    }
  }
}
```

Same goes for dynamic drawer links:

```tsx
componentWillUpdate(nextProps) {
  if (this.props.isFoo !== nextProps.isFoo) {
    const {
      isFoo,
      showDrawerLink,
      hideDrawerLink,
    } = nextProps;

    const drawer = { path: '/admin/foo/new-baz', title: messages.addBaz };

    if (isFoo) {
      showDrawerLink(drawer);
    } else {
      hideDrawerLink(drawer);
    }
  }
}
```

### Registering a mounter

To register your mounter open `/src/apps/routing.tsx` and find mounter declarations (`export const navMounters: NavigationMounter[]`). Then just add your mounter there.

## Rough example

```tsx
import * as React from 'react';
import { connect, Dispatch } from 'react-redux';
import { defineMessages } from 'react-intl';
import { Route } from 'react-router-dom';

import { NavigationMounterProps, RouteRegistrationData } from '../../chrome/prop-types';
import {
  fetchOrgStatus,
  OrgOverview,
  OrgMembers,
  OrgDomainRegistration,
} from './';

const messages = defineMessages({
  registerDomain: { id: 'chrome.app-org.register-domain' },
  organization: { id: 'chrome.app-org.organization' },
});

interface Props extends NavigationMounterProps {
  isOrgActive: boolean;
  dispatch: Dispatch<any>;
}

export class OrgNavMounter extends React.Component<Props, never> {
  componentDidMount() {
    const { dispatch } = this.props;
    // fetchOrgStatus action triggers a server request.
    // after the request succeeds, it sets the `state.org.isActive`
    // property to whatever status was received (this is done via epic)
    dispatch(fetchOrgStatus());
  }

  componentWillUpdate(nextProps: Props) {
    if (this.props.isOrgActive !== nextProps.isOrgActive) {
      const {
        isOrgActive,
        registerRoutes,
        unregisterRoutes,
        updateNavigationSection,
        removeNavigationSection,
        showDrawerLink,
        hideDrawerLink,
      } = nextProps;

      const orgNavSection = { id: 'org-nav', title: messages.organization, links: [/* ... */] };
      const orgDrawerLink = { path: '/admin/org/register-domain', title: messages.registerDomain };
      const orgRoutes: RouteRegistrationData = {
        switchRoutes: [
          { path: '/admin/org/overview', component: OrgOverview },
          { path: '/admin/org/members', component: OrgMembers },
        ],
        globalRoutes: [
          { path: '/admin/org/register-domain', component: OrgDomainRegistration },
        ],
      };

      if (isOrgActive) {
        registerRoutes(orgRoutes);
        updateNavigationSection(orgNavSection);
        showDrawerLink(orgDrawerLink);
      } else {
        unregisterRoutes(orgRoutes);
        removeNavigationSection(orgNavSection.id);
        hideDrawerLink(orgDrawerLink);
      }
    }
  }

  render() {
    return null;
  }
}

const mapStateToProps = state => ({
  isOrgActive: state.org.isActive,
});

export const ConnectedOrgNavMounter = connect(mapStateToProps)(OrgNavMounter);

```
