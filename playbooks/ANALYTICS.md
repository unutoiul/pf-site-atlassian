# Analytics
## Overview
Events must be structured following the new Atlassian event spec. A high-level overview of analytics and events [can be found here](https://hello.atlassian.net/wiki/spaces/I/pages/244165014/Analytics+in+new+site-admin).


## Our Client
Analytics are done through 2 main libraries. Despite the fact that you won't probably be working directly with them, it's still useful to call them out here.
* [`@atlaskit/analytics`](https://atlaskit.atlassian.com/components/analytics)
* [`@atlassiansox/analytics-web-client`](https://bitbucket.org/atlassian/analytics-web-client)

`@atlassian/analytics-web-client` is used as a client to actually propagate the information down the pipeline.

We expose an analytics client for our collaborators to use which abstracts some of the underlying analytics-web-client logic and also provides the follow data for each event by default:

```js
{
  env: getEnv(),
  product: 'admin',
  subProduct: adminHubUtil.isAdminHub() ? product.adminHub : product.siteAdmin,
  version: '1.0.0',
  locale: 'en-US',
  aaid: '...' (coming soon)
}
```

## Screen events

Screen events can be send in two ways: imperatively through analytics client (discussed later), or declaratively through `<ScreenEventSender />`.

```js
render() {
  return (
    <ScreenEventSender
      isDataLoading={this.hasLoaded()} // can be omitted if no "loading" is applicable to this screen
      event={getEventData(/* your params */)}
    >
      {this.provideRealContentHere()}
    </ScreenEventSender>
  );
}
```

## Two Ways to Use the Analytics Client
### As a class
```ts
import { AnalyticsWebClient } from 'common/analytics';
const analyticsWebClient = new AnalyticsWebClient();
```

### As an HOC
```ts
import { withAnalyticsClient, AnalyticsClientProps } from 'common/analytics'

interface YummyProps {
  // ...
}
class OutbackSteakhouseImpl extends React.Component<YummyProps & AnalyticsClientProps> {

  handleOnSubmit = (e: React.FormEvent<HTMLFormElement) => {
    // ...
    this.props.analyticsClient.sendTrackEvent({...})
  }
}

const OutbackSteakhouse = withAnalyticsClient(OutbackSteakhouseImpl);
```



### Getting Started Sending Events
Once you have determined [which event type you want to send](https://hello.atlassian.net/wiki/spaces/ANALYTICS/pages/134329599/Analytics+Lesson+1+-+Event+Types) and [an appropriate event name](https://hello.atlassian.net/wiki/spaces/ANALYTICS/pages/134329599/Analytics+Lesson+1+-+Event+Types), there are a few steps you need to take until your event data flows through the data pipeline:

1. Register your event on our [Admin Hub Analytics confluence page](https://product-fabric.atlassian.net/wiki/spaces/SA/pages/711360878/Analytic+Events+spec+in+admin+hub).

2. Wait for analyst approval

3. Register your event at [go/dataportal](https://data-portal.us-east-1.staging.public.atl-paas.net/) under the product dropdown option `Admin`

4. Create the event in `src/common/analytics/analytics-event-data.ts`

  ```ts
  export const removeDomainClickEventData = (attributes: RemoveDomainClickAttributes): UIData => ({
    action: Action.Click,
    actionSubject: ActionSubject.Button,
    actionSubjectId: 'removeDomainButton',
    source: 'domainClaimScreen',
    attributes,
  });
  ```
5. Add the link to the event you just registered in step 3 as a comment above the event

  ```ts
  // https://data-portal.us-east-1.staging.public.atl-paas.net/eventcatalog/view/1689
  export const removeDomainClickEventData = (attributes: RemoveDomainClickAttributes): UIData => ({
    action: Action.Click,
    actionSubject: ActionSubject.Button,
    actionSubjectId: 'removeDomainButton',
    source: 'domainClaimScreen',
    attributes,
  });
  ```
6. Fire the event in code using the corresponding method for the event type

  ```ts
  import { AnalyticsWebClient, removeDomainClickEventData } from 'src/common/analytics';

  analyticsClient.sendUIEvent({
    data: removeDomainClickEventData({
      verified: this.props.verified,
    }),
  });
  ```

**Sending Tenant Information**

We support sending analytics with orgId or cloudId tenant information.
```ts
import { AnalyticsWebClient, removeDomainClickEventData } from 'src/common/analytics';

analyticsClient.sendUIEvent({
  orgId: '...',
  data: removeDomainClickEventData({
    verified: this.props.verified,
  }),
});
```
```ts
import { AnalyticsWebClient, removeDomainClickEventData } from 'src/common/analytics';

analyticsClient.sendUIEvent({
  cloudId: '...',
  data: removeDomainClickEventData({
    verified: this.props.verified,
  }),
});
```

