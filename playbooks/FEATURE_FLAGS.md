# Feature Flags

## About

Site admin uses Launch Darkly for feature flag toggling. Instead of using the Launch Darkly client directly, we provide base feature flag components to promote reusability, composability and consistency.

## Feature Flag HOCs
We provide higher-order components that pass the feature flag value and loading state to the component that it wraps. We urge users of these HOCs to wrap the lowest descendent in the child tree that depends on the feature flag. By wrapping the lowest component we allow for other components to load without needing to depend on the resolution of the feature flag.

These components exist for the different feature flag contexts we have today:

#### User Level

These resolve on the user level. For example: `0d3daedb-04e6-4aa2-9226-5c1be24eefb1`.

```tsx
import { withUserFeatureFlag } from 'common/feature-flag';
```

#### Site Level

These resolve on the site level. For example: `product-fabric.atlassian.net`.

```tsx
import { withSiteFeatureFlag } from 'common/feature-flag';
```

#### Org Level

These resolve on the org level. For example: `ORG:id`.
```tsx
import { withOrgFeatureFlag } from 'common/feature-flag';
```

### Setup
```tsx
import { withUserFeatureFlag} from 'common/feature-flags'

export interface FlatWhiteFeatureFlagResult {
  flatWhiteFeatureFlag: FeatureFlag;
}

export function withFlatWhiteFeatureFlag<TOwnProps>(Component: React.ComponentType<TOwnProps & FlatWhiteFeatureFlagResult>): React.ComponentClass<TOwnProps> {
  return withUserFeatureFlag<TOwnProps, FlatWhiteFeatureFlagResult>({
    flagKey:  'feature.flat-white',
    defaultValue: false,
    name: 'flatWhiteFeatureFlag', // the name of the property on your interface. This will be type-checked, and TS intellisense will help you with setting this up
  })(Component);
}
```

### Usage
```tsx
import { withFlatWhiteFeatureFlag } from '..';

const PageImpl = ({ flatWhiteFeatureFlag }) => {
  if (flatWhiteFeatureFlag.isLoading) {
    return null;
  }
  if (flatWhiteFeatureFlag.value === true) {
    return <div>enabled</div>;
  }

  return <div>disabled</div>;
};

export const Page = withFlatWhiteFeatureFlag(PageImpl);
```

### Removing Feature Flags
The HOC pattern makes the removal of a feature flag easy–just remove the HOC and conditional logic.