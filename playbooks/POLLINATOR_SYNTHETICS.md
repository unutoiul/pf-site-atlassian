# Pollinator Synthetics

## Summary
Admin Hub synthetic checks live under `/tests/synthetics`. Existing checks can be viewed on the Pollinator app via this search https://pollinator.prod.atl-paas.net/checks?search=Cloud%20Admin%20-. All our repo tests are prefixed with "Admin Hub - ".

## Requirements

* Pollinator CLI installed locally: https://extranet.atlassian.com/display/OBSERVABILITY/How+To%3A+Pollinator+CLI

## Yarn Scripts

Yarn scripts are provided for creating, updating, and running checks. These are necessary because our checks are writtin in TypeScript which Pollinator doesn't natively support. 

Use the `-h` flag on scripts to get docs about their params.

* `yarn synthetics:add` - Creates a new check
* `yarn synthetics:list` - List out the created checks
* `yarn synthetics:run` - Run a check locally
* `yarn synthetics:push` - Pushes updates to Pollinator

## Secrets

Test secrets like username and password for login do not exist within the repo as plain text. They are stored on Pollinator via the web app so they can be encrypted.

## Creating New Tests

We'll walk through an example of adding a new check for billing.

* Create a new script file under `tests/synthetics/checks` that will hold the webdriver code for the test. Refer to `framework-loads.ts` as a working example. In this case, create  `tests/synthetics/checks/billing.ts` for this purpose.

* You can now dev on and run the check locally using `yarn synthetics:run --file billing.ts`.

* Next, run `yarn synthetics:add`. This script registers a new synthetic check with Pollinator using the default configuration. The script requires the filename of the new webdriver test (`billing.ts`) and a name for the test. For example: `yarn synthetics:add --file billing.ts --name Billing`. This will create a new check on Pollinator titled "Cloud Admin - Billing". Note the prefix in the name is handled automatically for you.

* Open the check in Pollinator from the link provided from `yarn synthetics:add`. Configure the check by putting in the required username and password as encrypted secrets. These are needed by webdriver in order to login.

* Any further local updates can be pushed to Pollinator via `yarn synthetics:push --UUID`.
