# Working with APIs

This playbook covers common coding scenarios when working with APIs.

This is the TL;DR:

* Abstract API layer as part of rest clients (one client per one real-world service)
* Create models that will utilize rest clients to transform their data into domain-level properties (it is ok for one model to utilize multiple clients underneath)
* GraphQL resolvers should use API models to make resolvers as dumb as possible.

## Conventions and code templates

A general rule of thumb is that: write all clients, models, and resolvers as if they were running on their own server. Do not reference `window`/`document` or other DOM entities, as we one day will move these resolvers, clients, and models to their own service.

### API Clients

API clients are just supposed to be a thin layer on top of service APIs.

API clients will have a couple things they are responsible for:

* HTTP requests for resources. URLs, custom headers, CORS configuration will all live in API clients.
* Caching of responses. For resources whose caching makes sense, an API client will be responsible for putting it into cache, and serving the cached response for subsequent requests. Cache invalidation is a thing, of course, and will need to be handled on a case by case basis.
* Authentication and authorization checks. There might be services that do not fully conform to 401/403 contracts, or ones that have their own way of determining auth (say, a separate endpoint or whatever else). The logic for throwing auth errors will live in API clients as well

All API clients live inside `src/schema/api/client` directory and are re-exported from `src/schema/api/client/index.ts` file along with all the types.

An API client is supposed to represent **one and only one** real-world service. API models are the ones responsible for assembling cross-service data.

All API clients **should** provide a link to the API documentation source of the service they represent.

All API clients need to be defined inside an ApiFacade (see `src/schema/api/client/api-facade.ts`).


All API clients are responsible for defining the types of data they are getting from the backend. While describing API capabilities via GraphQL types is possible, more often than not a type remapping will have to take place, see API model sections below on that. If the API shapes are defined in the API client file, we get multiple advantages: a) it's easier to check the function signature against the API documentation source, and b) it's easier to refactor GraphQL types.

#### API Client Template

```js
import { getConfig } from 'common/config';
import { RequestHelper } from './request-helper';

interface ApiOperationOptions { /* ... */ }
interface ApiOperationResult { /* ... */ }

/**
 * This client represents service X.
 * The documentation for service X can be found at https://server/swagger.json
 */
export class ApiClient {
  constructor(private requestHelper: RequestHelper) {
  }

  public async apiOperation(id: string, options: ApiOperationOptions): Promise<ApiOperationResult> {
    return this.requestHelper.postJson<ApiOperationResult>({
      url: `${getConfig().serviceUrl}/entity/${id}/operation`,
      requestId: 'unique-request-id',
      body: options,
    });
  }
}
```

You can learn more about `RequestHelper` from the in-code documentation.

### API Models

This is a place where APIs turn into more meaningful pieces of data. Consider a `UserModel`. Some user data might be coming from `user-management` service, other pieces - from `directory` service, some more stuff from `atlassian-account` service, the last bits we might be getting from `user-preferences-service`, and so on.

The only responsibility of API models is to serve as a domain-level abstraction over API clients.

So, instead of doing

```js
const user = await userManagementRestClient.getUserById(id);
const userLastActive = await userActivityRestClient.getUserLastActive(id);
const userSummary = { first: user.firstName, last: user.lastName, active: userLastActive };
```

you will do

```js
const user = User.create(id);
const userSummary = await user.getSummary();
```

That's it! The only purpose of models is to provide a domain-level interface over data so that consumers don't have to worry about where the data is coming from.

All API models are defined in `src/schema/api/model` and re-exported from `src/schema/api/model/index.ts`.

An API model is supposed to represent a domain-level entity that can be operated on.

An API model should have a constructor that accepts all the API **clients** that it depends on. This makes the API model unit-testable. Model creation should happen via a `static init()` method, which would take the API clients from `apiFacade`.
While for some models it is acceptable to provide `id` param into the static `init` method, for others it might not be possible. For example, if models accept bulk operations (like getting the list of models, or performing operations on a list of models), then providing an `id` into `init` method would result in redundant null checks. For such cases, it is advised to provide model `id` into every method explicitly. It is also possible to name the method `create` instead of `init`.

All API model methods **must** have the `@handleErrors()` decorator. This decorator is mainly responsible for reporting errors to our frontend monitoring tools, but it also handles `HTTP 401` responses for auth redirects, as well as translating various HTTP codes to their own error instances (see `src/common/error/`).

**All** errors that get thrown from within an API model method must be descendants of the `RequestError` class. For *expected* API errors that are solely used for user notifications, do not forget to set the `ignore` flag to `true`. For example, if the user submits invalid input parameters - that's not something we want to be alerted on, so an `ignore` flag is a must.

Since API models are the layer between REST and GraphQL, they are also responsible for re-shaping the data. Examples of that include:
* API defines a boolean property as `"disabled"`. A front-end convention is to name booleans like that using `"is"` prefix, so the property should be named `"isDisabled", so data re-shape is required.
* API returns an array of items that can be null. A dev decides to make GraphQL property non-nullable, thus data re-shape is required.

#### API Model Template

```js
import { GraphQLType } from '../../schema-types';
import { apiFacade, ApiClientX, ApiClientY, GetBarParams, ApiType } from '../client';
import { handleErrors } from './utils/handle-errors.decorator';

const reshapeData = (apiResponse: ApiType): GraphQLType => {
  /* ... */
};

export class Model {
  constructor(
    private apiClientX: ApiClientX,
    private apiClientY: ApiClientY,
  ) {
  }

  @handleErrors()
  public async getFoo(id: string) {
    return this.apiClientX.getFoo(id);
  }

  @handleErrors()
  public async getBar(id: string, params: GetBarParams) {
    return this.apiClientY.getBar(id, params);
  }

  @handleErrors()
  public async doStuff(id: string, param: string) {
    try {
      await this.apiClientX.checkPrerequisitesForDoingStuff(id);
    } catch (e) {
      if (e instanceof RequestError) {
        if (e.message === 'No can do') {
          throw new SpecificRequestError('could not do stuff with model');
        }
      }
    }

    const result = await this.apiClientY.doActualStuff();

    return reshapeData(result);
  }

  public static init() {
    return new Model(
      apiFacade.apiClientX,
      apiFacade.apiClientY,
    );
  }
}
```

### GraphQL Resolvers

With the concepts of API clients and models in place, the resolvers will look extremely simple, like this:

```js
export const resolver = {
  CurrentUser: {
    isSiteAdmin: async (): Promise<boolean> => {
      return CurrentUser.create().isSiteAdmin();
    },
  },
  Query: {
    currentUser: async (): Promise<CurrentUserInfo> => {
      return CurrentUser.create().getInfoOrThrow();
    },
  },
}
```

Resolvers live in `src/schema/resolvers` directory and are assembled together in `src/schema/resolvers/index.ts` file.

Resolvers should be simply a one-line calls that delegate all the heavy-lifting to API models.

Resolvers **can** serve as additional type guards between GraphQL types and whatever gets returned from the API models.

#### GraphQL Resolver template

```js
import { Model as GraphqlModel, UpdateModelMutationVariables } from '../../schema/schema-types';
import { Model as ApiModel } from '../api';

export const resolver = {
  Model: {
    someField: async ({ id }): Promise<GraphqlModel['someField']> => ApiModel.init().getSomeField(id),
  },
  Query: {
    model: async (_, { id }): Promise<Partial<GraphqlModel>> => ApiModel.init().getModel(id),
  },
  Mutation: {
    updateModel: async ({ id }, args: UpdateModelMutationVariables): Promise<Partial<GraphqlModel>> => ApiModel.init().updateModel(id, args),
  },
};
```
