# Cookie copying

## About

There are some cases where cookies need to be copied across domains for local testing. A primary example of this is the branch preview functionality, where a link like this:

```
https://adminhub-branch-preview.stg.internal.atlassian.com/?sha=728cc7a
```

is not logged in, and cannot be logged in. To fix this, one solution is to copy cookies across domains, from a logged in site to the site that needs these cookies.

## Steps to copy cookies

### Which cookie?

Staging sites will need a staging cookie, and prod sites will need a prod cookie. They are called `cloud.session.token.stg` and `cloud.session.token` respectively.

The branch preview funcitonality above requires the `cloud.session.token.stg` cookie, as it is a staging site. 

### To copy the staging cookie

1. Go to https://id.stg.internal.atlassian.com/
1. Open the devtools, and navigate to Application -> Cookies
1. Copy the cookie value for `cloud.session.token.stg`

![Screenshot of cookie copying stg](https://statlas.prod.atl-paas.net/cookie-copy/cookie-copy/id-token-stg.png)

### To copy the prod cookie

1. Go to https://id.atlassian.com/
1. Open the devtools, and navigate to Application -> Cookies
1. Copy the cookie value for `cloud.session.token`

![Screenshot of cookie copying prod](https://statlas.prod.atl-paas.net/cookie-copy/cookie-copy/id-token-prod.png)

**Note** that this cookie represents your session, so don't share it.


### Copy the cookie to the target domain

You will need a browser extension to copy a cookie across on Chrome. One popular one for this is [Edit this cookie](https://chrome.google.com/webstore/detail/editthiscookie/fngmhnnpilhplaeedifhccceomclgfbg?hl=en).

Install this extension, and navigate to your target domain (eg https://adminhub-branch-preview.stg.internal.atlassian.com/?sha=728cc7a)

Open the extension, and create the cookie (either cloud.session.token.stg for staging or cloud.session.token for prod) that you had before.

![Screenshot of edit this cookie](https://statlas.prod.atl-paas.net/cookie-copy/cookie-copy/edit-this-cookie.png)
