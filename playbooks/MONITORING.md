# Monitoring

To report an error, simply use `analyticsClient`:

**NOTE: This is a different client than the analytics client listed in ANALYTICS.md.** This is due to legacy reasons and we are working to migrate from this.

```ts
import { analyticsClient } from '/src/common/analytics';

doStuff = () => {
  if (!this.isInvariantSatisfied()) {
    analyticsClient.onError(`A very critical invariant wasn't satisfied; ${additionalInfo}`);
  }

  try {
    this.doOperationThatCanFailUnexpectedly();
  } catch (e) {
    analyticsClient.onError(e);
  }
}
```

Errors happen, and sometimes they happen unexpectedly. The thinking behind what kind of errors you should report consists of only one part:

> If the error is reported often enough for the team to be pinged by PagerDuty - is there anything that can be done with it?

The lists of things that can be done includes, but is not limited to:
1. Deployment rollback
2. Bugfix 
3. Disabling a feature 

So, if you know that there will be no action that can get rid of an error - do not report it.

A bad example would be to report user input validation error:

```ts
// DO NOT DO THIS!!!
try {
  const userData = JSON.parse(this.input.value);
} catch (e) {
  analyticsClient.onError(e);
}
```
