# Storybooks

Storybooks are our visual tests and component documentation. One Storybook can be as big as an entire page while another is just a single component. When creating Storybooks, please follow the given guidelines below.

## Guidelines

* Name your Storybook file with the suffix `.story.tsx` and place it next to your source file. For example, `banner.tsx` as the source of the component and `banner.story.tsx` for the story.
* Storybooks are organized into sections which mirror the repo's folder structure. A Storybook for a component within `src/common` will be under the **Common** section within the nav on Storybooks. To achieve this, use the `|` to delineate sections:

```tsx
storiesOf('Common|Banner', module)
  .add() // Your story logic here

storiesOf('Organization|Member Page', module)
  .add()
```

Use forward slashes to further define subsections if needed:

```tsx
// Here we render the subsection Header underneath Banner
storiesOf('Common|Banner/Header', module)
  .add() // Your story logic here
```

Use **Upper Camel Case** for the story name and regular casing for the `.add()`
```tsx
storiesOf('Common|Banner/Header', module)
  .add('With default props', () => ...) // Your story logic here
```

Thanks for helping keep things uniform and neat within Storybooks ✌.