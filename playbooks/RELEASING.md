# Releasing

Changes to the UI are deployed automatically to staging, and then automatically to production.

PDV tests run on staging and production to verify these changes are correct after deployment to each environment.

Branch previews are a good way of verifying your changes work before this automated release happens.

## Branch Previews

With branch previews, you are able to see the state of admin.stg.atlassian.com and site.jira-dev.com/admin, privately on your branch.

This is useful for QA before merging.

You can see this on a pull request:

![branch preview](https://statlas.prod.atl-paas.net/screenshot-preview/preview.jpg)

Note that both site admin and admin.atlassian.com are available on this branch preview service. Visit `https://adminhub-branch-preview.stg.internal.atlassian.com/admin?sha=<YOUR-SHA-HERE>` to use site admin on the branch preview service.


## Manually Deploying to ddev

We have pollinator PDV checks running on staging and production, so manual deployments should only be done to ddev.

To deploy to ddev, first create a build:

`yarn build`

Then deploy:

`MICROS_ENV=ddev yarn deploy`

You can then see your changes on admin.dev.atlassian.com.

## Release Blockers

Release blockers are a mechanism for halting the deployments to production. A release blocker is a jira ticket in a certain project that prevents the releases from taking place.

[This filter](https://product-fabric.atlassian.net/issues/?filter=15793) is used to check whether any release blocking tickets exist.

If you are not sure what you are doing, message @disturbed in the Access | Engineering room to have this done for you. In an emergency (customers are being affected), create a HOT ticket and our disturbed rotation will handle it.

Note that after a release blocker is removed, another deployment to prod is not triggered. If you want your changes to be deployed immediately, this can be done by messaging @disturbed in the Access | Engineering room, or just wait for another pull request to be merged and your change deployed.