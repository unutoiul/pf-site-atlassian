# Integration Testing

We want to keep testing as localized to the frontend as possible, without service dependencies. For that we are heavy on unit tests and component level integration tests.

## What should these tests cover?

Integration tests should cover the UI interactions and expected new UI and state change. 
We use [enzyme](https://github.com/airbnb/enzyme/tree/master/docs) to assert and manipulate the output and mock-scenarios to handle mocking app HTTP requests.

### Example
Once the app is loaded, when I click on the app-switcher, it should show different apps that are available.

```tsx
import { mockService } from '...'

describe('App', () => {
  afterEach(() => {
    mockScenarios.restore();
  });

  it('should render apps in app switcher', async () => {
  // Mock the default behavior for all services
  mockScenario.default();

  // Mount app with state provider
  const app = mount(
    <AdminApp />,
  );

  // Find the app-switcher icon
  const appSwitcherIcon = app.find('MenuIcon');
  // Click on it
  appSwitcherIcon.simulate('click');

  // Wait until the dropdown is loaded
  await waitUntil(() => app.find('Group').length === 2);
  await waitUntil(() => app.find('Group').at(1).find('Item').length === 2);

  // Ensure that you see apps in the app-switcher
  const openedAppSwitcher = app.find('AppSwitcher');
  const html = openedAppSwitcher.at(1).html();
  expect(html).to.include('MyProduct1');
  expect(html).to.include('MyProduct2');
  });
})

```

What we don't do for the above test is to actually make a service request to fetch apps, instead we mock the response. With these tests we will cover the state cycle of the app, wire-up of the connected components and the behavior thereof.

## What is the Best Way to Write Integration Tests?
Site Admin talks to many different backend services. In order to make integration testing with many services easier, we created the mock-scenarios library for mocking service requests. mock-scenarios enable developers to define custom service behavior in order to test how Site Admin handles certain scenarios like failed service responses and other non-default behavior.

## Getting Started with Mock Scenarios
All scenarios can be found under `integration-test/mock-scenarios`. Scenarios wrap Site Admin HTTP fetch request mocks using fetch-mock.

mock-scenario works by mocking all registered service requests with their default behavior and then allowing the scenario creator to overwrite specific service responses.

```tsx
// your-scenarios.js
import { mockService } from '...';

export const yourScenarios = {
  // createScenario() is a helper function that registers your scenario by mocking all
  // registered service endpoints with their default responses
  // and then overriding what is contained in the callback function provided

  // mocking all of the responses of a service
  yourServiceUnauthorized: createScenario(() => {
    mockService
      .when
      .serviceCalled(yourService)
      .respondWith({
        status: 401,
        body: {}
      });
  }),

  // mocking just one endpoint of a service
  // (all other endpoints will respond with default responses)
  getUserIsUnavailable: createScenario(() => {
    mockService
      .when
      .serviceCalled(yourService)
      .withEndpoint('getUser')
      .respondWith({
        status: 500,
        body: null
      });
  });
}
```

And then in your integration test:
```tsx
import { mockScenarios } from '...';

describe('YourApp', () => {
  afterEach(() => {
    // removes all fetch mocks
    mockScenarios.restore();
  });

  // each test will have a scenario
  it('should test for something...', () => {
    // the scenario we wrote above
    mockScenarios.getUserIsUnavailable();

    // all registered service calls in mockScenarios
    // will now be mocked with their defined behavior
    // when App is mounted
    const app = mount(
      <App />,
    );

    // do some assertions...
  });

  it('should test that everything is ok...', () => {
    // the default scenario mocks all endpoints using default behavior
    mockScenarios.default();

    // all registered service calls in mockScenarios
    // will now be mocked with their defined behavior
    // when App is mounted
    const app = mount(
      <App />,
    );

    // do some assertions...
  });
});
```

## Mock Scenario Suggestions

#### Where to put scenarios
While it's possible to write scenarios inline within an integration test, we strongly suggest writing scenarios inside of `integration-test/mock-scenarios` where they can be reused in future disjoint integration tests.


#### What to name scenarios
When it comes to naming scenarios, we recommend explicit scenario names that describes the app behavior that the scenario is establishing. For example if your scenario overrides the default behavior of service A by overwriting the response of every service A endpoint with a 500 status code, a good scenario name could be `serviceAIsUnavailableScenario`. If multiple services are overridden with a 500 status code, for example service A and service B, a good scenario name would be the conjunction of the two: `serviceAandServiceBAreUnavailableScenario`.


#### Scenarios in tests
Each scenario is meant to encapsulate the app behavior needed for an integration test. Each test should be restricted to only one scenario. If behavior from multiple scenarios are needed in a test, then a new scenario should be written that incorporates the behavior of mutliple scenarios rather than including multiple scenarios in a test.
```tsx
// bad
it('is some test...', () => {
  mockScenarios.someScenarioA();
  mockScenarios.someOtherScenarioB();

  // rest of test...
});
```
This can have unexpected behavior considering that mock-scenario allows for endpoints to be overwritten without warning.

```tsx
// good
it('is some test...', () => {
  mockScenarios.scenarioAandB();

  // rest of test...
})
```

