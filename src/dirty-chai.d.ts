// tslint:disable no-namespace callable-types
declare namespace Chai {
  interface Assertion {
    (message?: string): Assertion;
  }
}
