// import { RoutineActionCreator } from "redux-saga-routines";
declare module 'redux-saga-routines' {
  import { Dispatch } from 'redux';

  export interface RoutineTypes {
    TRIGGER: string;
    REQUEST: string;
    SUCCESS: string;
    FAILURE: string;
    FULFILL: string;
  }

  export type RoutineActionCreator<T> = (payload?: T) => any;
  interface Routine {
    trigger: RoutineActionCreator<any>;
    request: RoutineActionCreator<any>;
    success: RoutineActionCreator<any>;
    failure: RoutineActionCreator<any>;
    fulfill: RoutineActionCreator<any>;
  }

  export function createRoutine(typePrefix: string): RoutineTypes & Routine;

  export function promisifyRoutine(R: Routine): (payload: any, dispatch: Dispatch<any>) => Promise<any>;

  export function routinePromiseWatcherSaga(): Generator;

  export function bindPromiseCreators(promiseCreators: any, dispatch: any): any;
}
