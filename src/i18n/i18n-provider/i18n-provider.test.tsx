import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { IntlProvider } from 'react-intl';
import { sandbox as sinonSandbox, SinonSandbox } from 'sinon';

import AkPage from '@atlaskit/page';

import { ScreenEventSender } from 'common/analytics';

import { languages } from '../languages';
import { I18nProviderImpl } from './i18n-provider';
import { messageLoader } from './message-loader';

describe('I18nProvider', () => {
  let sandbox: SinonSandbox;

  const createWrapper = (isInLoadingState, i18n?: 'German' | 'Japanese' | 'none' | 'all') =>
    shallow(
      <I18nProviderImpl
        i18nJapaneseFeatureFlag={{ isLoading: isInLoadingState, value: i18n === 'Japanese' || i18n === 'all' }}
        i18nGermanFeatureFlag={{ isLoading: isInLoadingState, value: i18n === 'German' || i18n === 'all' }}
      >
        <div />
      </I18nProviderImpl>,
  );

  beforeEach(() => {
    sandbox = sinonSandbox.create();
    sandbox.stub(languages, 'getDeliveredLanguages').returns(['en', 'de', 'ja']);

  });

  afterEach(() => {
    sandbox.restore();
  });

  it('should set locale to de and print messages in German when German feature flag is on and language is German', () => {
    // tslint:disable no-require-imports
    sandbox.stub(navigator, 'language').value('de');
    sandbox.stub(messageLoader, 'getMessages').returns(require('./german-test.json'));
    const wrapper = createWrapper(false, 'German');
    const expectedMessages = {
      'groups.page.table.header.members': 'Mitglieder',
      'chrome.feedback.description': 'Erzählen Sie uns von Ihrer Erfahrung mit der Administration, damit wir sie noch besser machen können.',
      'user-management.site-settings.access-configuration.groups.page.sidebar.title': 'Was Sie wissen sollten',
    };
    const props = wrapper.find(IntlProvider).props();

    expect(props.locale).to.equal('de');
    expect(props.messages).to.deep.equal(expectedMessages);
  });

  it('should set locale to ja and print messages in Japanese when Japanese feature flag is on and language is Japanese', () => {
    // tslint:disable no-require-imports
    sandbox.stub(navigator, 'language').value('ja');
    sandbox.stub(messageLoader, 'getMessages').returns(require('./japanese-test.json'));
    const wrapper = createWrapper(false, 'Japanese');
    const expectedMessages = {
      'groups.page.table.header.members': 'メンバー',
      'chrome.feedback.description': 'Administration に関するご意見ご感想をお聞かせください。製品の改善に活用させていただきます。',
      'user-management.site-settings.access-configuration.groups.page.sidebar.title': '留意点',
    };
    const props = wrapper.find(IntlProvider).props();

    expect(props.locale).to.equal('ja');
    expect(props.messages).to.deep.equal(expectedMessages);
  });

  it('should set locale to en when navigator language is Japanese and Japanese feature flag is diabled and German feature flag is enabled', () => {
    sandbox.stub(navigator, 'language').value('ja');
    const wrapper = createWrapper(false, 'German');
    const props = wrapper.find(IntlProvider).props();

    expect(props.locale).to.equal('en');
    expect(props.messages).to.be.undefined();
  });

  it('should get correct props to IntlProvider for English language', () => {
    sandbox.stub(navigator, 'language').value('en');
    const wrapper = createWrapper(false, 'none');

    const props = wrapper.find(IntlProvider).props();

    expect(props.locale).to.equal('en');
    expect(props.messages).to.be.undefined();
  });

  it('should not send browser language to analytics until data has loaded', () => {
    sandbox.stub(navigator, 'language').value('en');
    sandbox.stub(messageLoader, 'getMessages').returns(require('./german-test.json'));
    const wrapper = createWrapper(true, 'German');

    expect(wrapper.find(AkPage).exists()).to.equal(true);
    wrapper.setProps({
      i18nGermanFeatureFlag: {
        isLoading: false,
        value: true,
      },
      i18nJapaneseFeatureFlag: {
        isLoading: false,
        value: true,
      },
    });
    expect(wrapper.find(ScreenEventSender).prop('isDataLoading')).to.equal(false);
    expect(wrapper.find(ScreenEventSender).prop('event')).to.deep.equal({
      data: {
        name: 'i18nProvider',
        attributes: { language: 'en', isI18nEnabled: true },
      },
    });
  });
});
