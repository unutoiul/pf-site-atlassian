import * as React from 'react';
import { addLocaleData, IntlProvider } from 'react-intl';
import * as de from 'react-intl/locale-data/de';
import * as ja from 'react-intl/locale-data/ja';

import AkPage from '@atlaskit/page';

import { i18nScreenEvent, ScreenEventSender, ScreenEventSenderProps } from 'common/analytics';
import { FeatureFlagI18nGerman, FeatureFlagI18nJapanese, locale, withI18nGermanFeatureFlag, withI18nJapaneseFeatureFlag } from 'common/intl';

import { SkeletonNavigation } from '../../navigation/skeleton-navigation';
import { LinguiProvider } from '../jsLingui/linguiProvider';
import { isCurrentLangEnabled, languages } from '../languages';
import { messageLoader } from './message-loader';

addLocaleData(de);
addLocaleData(ja);

export class I18nProviderImpl extends React.Component<FeatureFlagI18nGerman & FeatureFlagI18nJapanese> {

  private browserLanguage = navigator.language;
  private localeOfLanguage = locale.languageToLocale(this.browserLanguage);

  public render() {
    if (this.props.i18nGermanFeatureFlag.isLoading ||
      this.props.i18nJapaneseFeatureFlag.isLoading) {
      return (
        <AkPage
          navigation={
            <SkeletonNavigation />
          }
        />
      );
    }

    const languageProps = this.getIntlProviderProps();

    return (
      <ScreenEventSender {...this.getScreenEventProps()}>
        <IntlProvider {...languageProps}>
          <LinguiProvider language={languageProps.locale}>
            {this.props.children}
          </LinguiProvider>
        </IntlProvider>
      </ScreenEventSender>
    );
  }

  private getScreenEventProps(): ScreenEventSenderProps {
    const { isLoading: isGermanLoading, value: isGermanEnabled } = this.props.i18nGermanFeatureFlag;
    const { isLoading: isJapaneseLoading, value: isJapaneseEnabled } = this.props.i18nJapaneseFeatureFlag;

    return {
      isDataLoading: isGermanLoading || isJapaneseLoading,
      event: i18nScreenEvent({
        language: navigator.language,
        isI18nEnabled: isGermanEnabled || isJapaneseEnabled,
      }),
    };
  }

  private getIntlProviderProps() {
    const language = this.getLanguage();

    if (language === 'en') {
      return { locale: 'en' };
    }

    return {
      locale: this.localeOfLanguage,
      messages: messageLoader.getMessages(this.getLanguage()),
      key: language,
    };
  }
  private getLanguage = () => {
    const defaultLanguage = 'en';
    const { i18nGermanFeatureFlag, i18nJapaneseFeatureFlag } = this.props;
    const i18nFlags: Array<{ value: boolean, isLoading: boolean, locale: string }> = [
      { ...i18nGermanFeatureFlag, locale: 'de' },
      { ...i18nJapaneseFeatureFlag, locale: 'ja' },
    ];

    if (!isCurrentLangEnabled(i18nFlags)) {
      return defaultLanguage;
    }

    const deliveredLanguages = languages.getDeliveredLanguages();
    if (deliveredLanguages.includes(this.browserLanguage)) {
      return this.browserLanguage;
    } else if (deliveredLanguages.includes(this.localeOfLanguage)) {
      return this.localeOfLanguage;
    } else if (deliveredLanguages.includes(languages.getReplacementLanguages(this.browserLanguage))) {
      return languages.getReplacementLanguages(this.browserLanguage);
    }

    return defaultLanguage;
  };
}

export const I18nProvider: React.ComponentClass = withI18nGermanFeatureFlag(withI18nJapaneseFeatureFlag(I18nProviderImpl));
