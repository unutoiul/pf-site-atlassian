// tslint:disable no-require-imports
import akEmojiSupportedLanguages from '@atlaskit/emoji/dist/es5/i18n/languages';

export const messageLoader = {
  getMessages(language: string) {
    const emojiLanguage = !!akEmojiSupportedLanguages[language] ? language : 'en';

    return {
      ...require(`../translations/${language}.json`),
      ...require(`@atlaskit/emoji/dist/es5/i18n/${emojiLanguage}`).default,
    };
  },
};
