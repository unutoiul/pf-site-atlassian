import { storiesOf } from '@storybook/react';
import * as React from 'react';

import { I18nProviderImpl } from './i18n-provider';

storiesOf('Common|I18n', module)
  .add('Loading', () => {
    return (
      <I18nProviderImpl i18nJapaneseFeatureFlag={{ isLoading: true, value: true }} i18nGermanFeatureFlag={{ isLoading: true, value: true }} />
    );
  });
