import { I18nProvider } from '@lingui/react';
import * as React from 'react';

import memoizeOne from 'memoize-one';

import { linguiBase } from './linguiBaseLanguage';

interface Props {
  language: string;
  baseLanguage?: string;
}

interface Hash {
  [key: string]: string;
}

interface Language {
  messages: Hash;
  languageData?: any;
}

interface Catalog {
  [key: string]: Language;
}

interface State {
  catalog: Catalog;
}

type injectLanguageCallback = (key: string, data: Language) => void;

export interface LanguageProvider {
  language: string;
  injectLanguage: injectLanguageCallback;
}

export const LinguiContext = React.createContext<LanguageProvider>({
  language: 'en',
  injectLanguage() {
    // do nothing
  },
});

const injectLinguiStrings = (catalog: Catalog, baseLanguage: string, locale: string, lang: Language) => {
  const line = catalog[locale] || { messages: {} };
  const base = catalog[baseLanguage] || { messages: {} };

  return {
    ...catalog,
    [locale]: {
      ...linguiBase,
      ...line,
      ...lang,
      messages: {
        ...base.messages,
        ...line.messages,
        ...lang.messages,
      },
    },
  };
};

export class LinguiProvider extends React.Component<Props, State> {
  public state = {
    catalog: {
      en: linguiBase,
    },
  };

  private getContextValue = memoizeOne((language: string, injectLanguage: injectLanguageCallback) => ({
    language,
    injectLanguage,
  }));

  public render() {
    const { language, children, baseLanguage = 'en' } = this.props;
    const { catalog } = this.state;

    return (
      <I18nProvider
        language={catalog[language] ? language : baseLanguage}
        catalogs={catalog}
      >
        <LinguiContext.Provider value={this.getContextValue(language, this.setLanguage)}>
          {children}
        </LinguiContext.Provider>
      </I18nProvider>
    );
  }

  private setLanguage = (key: string, data: Language) => {
    this.setState(({ catalog }) => ({
      catalog: injectLinguiStrings(catalog, this.props.baseLanguage || 'en', key, data),
    }));
  };
}
