// tslint:disable max-classes-per-file
import { DateFormat, Trans } from '@lingui/react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';

import { LanguageProvider, LinguiContext, LinguiProvider } from './linguiProvider';

describe('jsLingui provider', () => {
  it('should work without languages provided', () => {
    const wrapper = mount(
      <div>
        <LinguiProvider language="en">
          <DateFormat
            value={'2020-12-30'}
            format={{
              year: '2-digit',
              month: 'short',
              day: 'numeric',
              timeZone: 'UTC',
            }}
          />
        </LinguiProvider>
      </div>,
    );
    expect(wrapper.html()).to.contain('Dec 30, 20');
  });

  it('message-injection', () => {
    const testFailed = 'test-failed';
    const stillMissing = 'stillMissing';

    class LanguageInjector extends React.PureComponent<LanguageProvider> {
      public componentDidMount() {
        this.props.injectLanguage('en', { messages: { test: 'working properly' } });
      }

      public render() {
        return null;
      }
    }

    const wrapper = mount(
      <div>
        <LinguiProvider language="en">
          <Trans id="test">{testFailed}</Trans>
          <LinguiContext.Consumer>
            {context => <LanguageInjector {...context} />}
          </LinguiContext.Consumer>
          <Trans id="missing">{stillMissing}</Trans>
        </LinguiProvider>
      </div>,
    );
    expect(wrapper.html()).to.contain('working properly');
    expect(wrapper.html()).to.contain('missing');
  });

  it('language failback', () => {
    const testFailed = 'test-failed';

    class LanguageInjector extends React.PureComponent<LanguageProvider> {
      public componentDidMount() {
        this.props.injectLanguage('en', { messages: { test: 'working properly' } });
      }

      public render() {
        return null;
      }
    }

    const wrapper = mount(
      <div>
        <LinguiProvider language="de">
          <Trans id="test">{testFailed}</Trans>
          <LinguiContext.Consumer>
            {context => <LanguageInjector {...context} />}
          </LinguiContext.Consumer>
        </LinguiProvider>
      </div>,
    );
    expect(wrapper.html()).to.contain('working properly');
  });

  it('message failback', () => {
    const testFailed = 'test-failed';

    class LanguageInjector extends React.PureComponent<LanguageProvider> {
      public componentDidMount() {
        this.props.injectLanguage('en', { messages: { test1: 'working' } });
        this.props.injectLanguage('de', { messages: { test2: 'properly' } });
      }

      public render() {
        return null;
      }
    }

    const wrapper = mount(
      <div>
        <LinguiProvider language="de">
          <Trans id="test1">{testFailed}</Trans> <Trans id="test2">{testFailed}</Trans>
          <LinguiContext.Consumer>
            {context => <LanguageInjector {...context} />}
          </LinguiContext.Consumer>
        </LinguiProvider>
      </div>,
    );
    expect(wrapper.html()).to.contain('working properly');
  });
});
