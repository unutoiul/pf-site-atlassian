import { locale } from 'common/intl';

export const languages = {
  getDeliveredLanguages: () => (
    ['de', 'en', 'ja']
  ),
  getReplacementLanguages: (language: string) => {
    if (language === 'en-AU' || language === 'en-NZ') {
      return 'en-GB';
    }

    return language;
  },
};

export const isCurrentLangEnabled = (i18nFlags) => {
  const browserLanguage = navigator.language;
  const localeOfLanguage = locale.languageToLocale(browserLanguage);

  return i18nFlags.some(flag => flag.value && localeOfLanguage === flag.locale && !flag.isLoading);
};
