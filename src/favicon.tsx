import * as React from 'react';

export interface FaviconProps {
  path: string;
}

export class Favicon extends React.Component<FaviconProps, never> {
  public componentWillMount() {
    if (!this.props.path) {
      return;
    }

    const link = document.createElement('link');
    link.rel = 'shortcut icon';
    link.href = this.props.path;
    document.head.appendChild(link);
  }

  public render() {
    return null;
  }
}
