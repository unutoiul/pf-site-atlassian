import { getConfig } from 'common/config';

import { util as adminHubUtil } from '../utilities/admin-hub';
import { preserveRedirectStatus, redirectToLoginOrSignupOrNoop } from '../utilities/auth';
import { util as redirectUtil } from '../utilities/redirect';

const config = getConfig();

interface Options {
  checkAuthorization?: boolean;
  sendCredentials?: boolean;
}

function checkAuthentication(
  endpoint: string,
  {
    checkAuthorization = false,
    sendCredentials = false,
  }: Options,
) {
  const xhr = new XMLHttpRequest();
  const doneState = 4;

  xhr.onreadystatechange = () => {
    if (xhr.readyState !== doneState) {
      return;
    }
    if (xhr.status === 401) {
      redirectToLoginOrSignupOrNoop();

      return;
    }
    if (checkAuthorization && xhr.status === 403) {
      redirectUtil.redirect('/admin?react=false');
    }
  };
  const isAsync = true;
  xhr.withCredentials = sendCredentials;
  xhr.open('GET', endpoint, isAsync);
  xhr.send();
}

function checkAuthForAllServices() {
  if (!adminHubUtil.isAdminHub()) {
    checkAuthentication('/admin/rest/um/1/apps/navigation', { checkAuthorization: true });
  }
  checkAuthentication(`${config.apiUrl}/me`, { sendCredentials: true });
}

preserveRedirectStatus();
checkAuthForAllServices();
