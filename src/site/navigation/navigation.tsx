import { Location } from 'history';
import * as React from 'react';
import { graphql } from 'react-apollo';
import { connect, DispatchProp } from 'react-redux';

import {
  default as AkNavigation,
  presetThemes as akPresetThemes,
} from '@atlaskit/navigation';

import {
  ScreenEventSender,
  siteAdminSidebarViewEventData,
} from 'common/analytics';
import { HelpMenu } from 'common/help';
import {
  NavigationDrawer,
  openDrawer,
  RouterLink,
} from 'common/navigation';
import { UserMenu } from 'common/user';

import { NavigationHeader } from './navigation-header';
import { NavigationLinks } from './navigation-links';

import { RootState } from '../../store';
import { AppSwitcher } from '../app-switcher/app-switcher';
import { NavigationGlobalCreateIcon } from './navigation-global-create-icon';
import { NavigationGlobalLogo } from './navigation-global-logo';

import { NavigationV1Query } from '../../schema/schema-types';
import { NavigationDrawers } from './navigation-drawers';
import { NavigationState } from './navigation-sections.reducer';
import navigationV1Query from './navigation.query.graphql';

interface OwnProps {
  location: Location;
  withoutDrawers?: boolean;
}

interface DerivedProps {
  siteId?: string;
}

interface PropsFromState {
  openDrawer?: NavigationDrawer;
  navigationSections: NavigationState;

  openCreateDrawer(): any;
}

export type NavigationProps = OwnProps & PropsFromState & DispatchProp & DerivedProps;

const createNavHeader = () => <NavigationHeader />;

export const NavigationImpl: React.SFC<NavigationProps> = ({ siteId, withoutDrawers, openCreateDrawer, navigationSections }) => (
  <ScreenEventSender isDataLoading={!siteId} event={{ cloudId: siteId, data: siteAdminSidebarViewEventData() }}>
    <AkNavigation
      containerTheme={akPresetThemes.settings}
      containerHeaderComponent={createNavHeader}
      globalTheme={akPresetThemes.settings}
      globalPrimaryIcon={
        <NavigationGlobalLogo />
      }
      linkComponent={RouterLink}
      isCollapsible={false}
      isResizeable={false}
      globalCreateIcon={
        <NavigationGlobalCreateIcon />
      }
      globalSecondaryActions={[
        <AppSwitcher key="app.switcher" />,
        <HelpMenu key="help.menu" />,
        <UserMenu key="user.menu" />,
      ]}
      drawers={withoutDrawers ? [] : [<NavigationDrawers key="drawers" />]}
      onCreateDrawerOpen={openCreateDrawer}
    >
      <NavigationLinks
        navigationSections={navigationSections}
      />
    </AkNavigation>
  </ScreenEventSender>
);

const withSiteId = graphql<OwnProps, NavigationV1Query, {}, DerivedProps>(navigationV1Query, {
  props: ({ data }): DerivedProps => ({
    siteId: data && data.currentSite && data.currentSite.id,
  }),
});

const withConnect = connect(
  (state: RootState) => ({
    navigationSections: state.chrome.navigationSections || [],
  }), {
    openCreateDrawer: () => openDrawer(NavigationDrawer.Create),
  });

const WithSiteId = withSiteId(NavigationImpl);

export const Navigation = withConnect(WithSiteId);

export {
  NavigationDrawers,
};
