import { NavItem, SectionUpdateParams } from 'common/navigation';

import {
  Action,
  NavigationAction,
} from '../../store/action-types';
import { sections } from './sections';

export type NavigationState = NavItem[];

interface ParentedNavItem {
  item: NavItem;
  parent: NavItem | undefined;
}

class SectionsUpdater {
  constructor(
    private state: NavigationState,
  ) {
  }

  public disableSection(sectionId: string): NavigationState | false {
    const flattenedSections = SectionsUpdater.getFlattenedSections(this.state, undefined);
    const flattenedItem = flattenedSections.find(s => s.item.id === sectionId);

    if (!flattenedItem) {
      // no section with provided id exists
      return false;
    }

    const { parent, item: itemToRemove } = flattenedItem;

    if (itemToRemove == null) {
      return false;
    }

    if (parent == null) {
      // the section to be disabled is a top one
      return disableById(this.state, sectionId);
    }

    const updatedParent = {
      ...parent,
      links: disableById(parent.links, sectionId),
    };

    return this.state.map(item => item.id === updatedParent.id ? updatedParent : item);
  }

  public updateSection(section: SectionUpdateParams): NavigationState | false {
    const sectionToBeUpdated = removeAlreadyPresentLinks(this.state, section);

    const updatedSection = this.updateSectionAndItsParents(sectionToBeUpdated);
    if (updatedSection === false) {
      return this.state;
    }

    return SectionsUpdater.replaceOneAndSort(this.state, updatedSection);
  }

  private updateSectionAndItsParents(section: SectionUpdateParams): NavItem | false {
    const isNavChangeRequested = section.path || (section.links && section.links.length > 0);
    const isMetadataChangeRequested = section.title !== undefined;
    if (!isNavChangeRequested && !isMetadataChangeRequested) {
      return false;
    }

    const flattenedSections = SectionsUpdater.getFlattenedSections(this.state, undefined);

    const parentedItem = flattenedSections.find(s => s.item.id === section.id) || ({} as ParentedNavItem);
    let parent = parentedItem.parent;

    const existingSection = parentedItem.item;
    if (!isNavChangeRequested && !existingSection) {
      // Since no navigation updates are happening, this is an update of metadata (e.g. title).
      // Updating metadata for a section that doesn't yet exist in state makes no sense.
      return false;
    }

    const navigationParametersUnchanged = existingSection && existingSection.path === section.path && areLinksEqual(existingSection.links, section.links);
    if (navigationParametersUnchanged && !isMetadataChangeRequested) {
      // Navigation updates were requested, but they would not change anything (the current state is already up-to-date).
      // Metadata (e.g. title) is not updating either.
      return existingSection;
    }

    let topMostUpdatedSection = SectionsUpdater.mergeSections(parentedItem.item, section);
    while (parent) {
      topMostUpdatedSection = {
        ...parent,
        links: SectionsUpdater.replaceOneAndSort(parent.links, topMostUpdatedSection),
      };
      parent = flattenedSections.find(s => s.item.id === parent!.id)!.parent;
    }

    return topMostUpdatedSection;
  }

  private static replaceOneAndSort(items: NavItem[] | undefined, toReplace: NavItem): NavItem[] {
    const updatedSections: NavItem[] = [...(items || []).filter(s => s.id !== toReplace.id), toReplace];

    updatedSections.sort((a, b) => a.priority! - b.priority!);

    return updatedSections;
  }

  private static mergeSections(existingSection: NavItem, section: SectionUpdateParams): NavItem {
    const links = SectionsUpdater.joinLinks(existingSection && existingSection.links, section.links);
    if (links) {
      links.forEach((l, index) => l.priority = (l.priority || (index + 1) * 10));
      links.sort((a, b) => a.priority! - b.priority!);
    }

    /**
     * Only take the parameters that a) actually changed and b) we allow to update.
     * By doing this we:
     *  I) ensure that null/undefined values do not override the real ones
     * II) don't allow consumers to update things like priorities or icons
     */
    const updates: Partial<SectionUpdateParams> = {};
    if (section.path) {
      updates.path = section.path;
    }
    if (section.title) {
      updates.title = section.title;
    }

    return {
      ...existingSection,
      ...updates,
      links,
    };
  }

  private static joinLinks(a: NavItem[] | undefined, b: NavItem[] | undefined): NavItem[] | undefined {
    if (!a && !b) {
      return undefined;
    }

    return [
      ...(a || []),
      ...(b || []),
    ];
  }

  private static getFlattenedSections(items: NavItem[] | undefined, parent: NavItem | undefined): ParentedNavItem[] {
    if (!items) {
      return [];
    }

    return items.reduce<ParentedNavItem[]>(
      (prev, cur) => prev.concat({ item: cur, parent }, this.getFlattenedSections(cur.links, cur)),
      [],
    );
  }
}

function disableById(items: NavItem[] | undefined, itemId: string): NavItem[] {
  return (items || []).map(item => item.id === itemId ? { ...item, disabled: true } : item);
}

function areLinksEqual(a: NavItem[] | undefined, b: NavItem[] | undefined): boolean {
  if (!a && !b) {
    // both do not have any links
    return true;
  }
  if (!a || !b) {
    // one of them does not have links, while the other has
    return false;
  }

  if (a.length !== b.length) {
    return false;
  }

  for (const item of a) {
    if (!b.find(other => other.path === item.path)) {
      return false;
    }
  }

  return true;
}

function isLinkAlreadyAccountedFor(state: NavItem[] | undefined, path: string | undefined): boolean {
  if (!path || !state) {
    return false;
  }

  if (state.some(l => l.path === path)) {
    return true;
  }

  return state.some(l => isLinkAlreadyAccountedFor(l.links, path));
}

function removeAlreadyPresentLinks(state: NavigationState, section: SectionUpdateParams): SectionUpdateParams {
  if (!section.links) {
    return section;
  }

  return {
    ...section,
    links: section.links.filter(l => !isLinkAlreadyAccountedFor(state, l.path)),
  };
}

export const navigationSections = (
  state: NavigationState = sections,
  action: Action,
) => {
  switch (action.type) {
    case NavigationAction.CHROME_NAVIGATION_SECTION_UPDATE: {
      const result = new SectionsUpdater(state).updateSection(action.section);

      return result || state;
    }
    case NavigationAction.CHROME_NAVIGATION_SECTION_REMOVE: {
      const result = new SectionsUpdater(state).disableSection(action.sectionId);

      return result || state;
    }
    default:
      return state;
  }
};
