// tslint:disable prefer-function-over-method
import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';

import {
  AkContainerNavigationNested,
  AkNavigationItemGroup,
} from '@atlaskit/navigation';

import { NavItem } from 'common/navigation';

import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import { NavigationLinksImpl } from './navigation-links';
import { sections as predefinedSections } from './sections';

interface VisibleItemDescriptor {
  type: 'section' | 'link' | 'heading' | 'back' | 'feedback';
  path?: string;
  text?: string;
  selected?: boolean;
  _elemRef: JSX.Element;
}

const section1Links = [
  { path: '/section-1/', title: { id: 'link', defaultMessage: 'Section 1 parent link' } },
  { path: '/section-1/link-1', title: { id: 'link11', defaultMessage: 'Section 1 Link 1' } },
  { path: '/section-1/link-2', title: { id: 'link12', defaultMessage: 'Section 1 Link 2' } },
];

const section2Links = [
  { path: '/section-2/link-1', title: { id: 'link21', defaultMessage: 'Section 2 Link 1' } },
  { path: '/section-2/link-2', title: { id: 'link22', defaultMessage: 'Section 2 Link 2' } },
];

class TestObject {
  private wrapper!: ShallowWrapper<any, any>;

  constructor(
    private items: Array<Partial<NavItem>>,
    private ignoreFeedbackSection: boolean = true,
  ) {
  }

  public render(currentUrl: string = ''): this {
    this.wrapper = shallow(
      <NavigationLinksImpl
        intl={createMockIntlProp()}
        navigationSections={this.items}
        location={{ pathname: `${new Date().getTime()}` }}
        {...{} as any}
      />,
      createMockIntlContext(),
    );
    this.wrapper.setProps({ location: { pathname: currentUrl } });
    this.wrapper.update();

    return this;
  }

  public get backButton(): VisibleItemDescriptor | null {
    return this.visibleItems.find(item => item.type === 'back') || null;
  }

  public openSectionWithTitle(text: string) {
    const section = this.visibleItems.find(item => item.type === 'section' && item.text === text);

    section!._elemRef.props.onClick();
    this.wrapper.update();
  }

  public goBack() {
    this.backButton!._elemRef.props.onClick();
    this.wrapper.update();
  }

  public navigateTo(url: string) {
    this.wrapper.setProps({ location: { pathname: url } });
    this.wrapper.update();
  }

  public expectActiveItemToBe(item: Partial<VisibleItemDescriptor> | null) {
    if (item === null) {
      expect(item).to.equal(null);
    } else {
      this.expectItemsToMatch(this.activeItem, item);
    }
  }

  public expectItemsToBe(items: Array<Partial<VisibleItemDescriptor>>) {
    const compareWith = this.visibleItems;

    expect(compareWith.length).to.equal(items.length, `Different count of items;\nexpected: ${items.map(this.stringify)}\nactual: ${compareWith.map(this.stringify)} \n`);
    for (const item of items) {
      const itemToCompare = compareWith.shift();

      this.expectItemsToMatch(itemToCompare!, item);
    }
  }

  private stringify = (item: Partial<VisibleItemDescriptor>) => {
    const { _elemRef, ...rest } = item;

    return JSON.stringify(rest);
  };

  private expectItemsToMatch(actual: VisibleItemDescriptor, expected: Partial<VisibleItemDescriptor>) {
    Object.entries(expected).forEach(([key, value]) => {
      expect(actual).to.have.property(key, value, `Objects did not match;\nexpected: ${this.stringify(expected)}\nactual: ${this.stringify(actual)}\n`);
    });
  }

  private get visibleItems(): VisibleItemDescriptor[] {
    const stack = this.wrapper.find<{ stack: JSX.Element[][] }>(AkContainerNavigationNested).props().stack;
    const lastItem = stack[stack.length - 1];

    return lastItem
      .map(this.collectItems)
      .reduce((prev, curr) => prev.concat(curr), [])
      .map(this.describeElement)
      .filter(this.notIgnored.bind(this));
  }

  private collectItems = (element: JSX.Element): JSX.Element[] => {
    if (element.type !== AkNavigationItemGroup) {
      return [element];
    }

    return [
      element,
      ...element.props.children,
    ];
  };

  private notIgnored(item: VisibleItemDescriptor) {
    if (!this.ignoreFeedbackSection) {
      return true;
    }

    return item.type !== 'feedback';
  }

  private describeElement = (element: JSX.Element): VisibleItemDescriptor => {
    if (element.key === 'feedback') {
      return {
        type: 'feedback',
        _elemRef: element,
      };
    }
    if (element.key === 'back') {
      return {
        text: element.props.text,
        type: 'back',
        _elemRef: element,
      };
    }

    if (element.type === AkNavigationItemGroup) {
      return {
        text: element.props.title,
        type: 'heading',
        _elemRef: element,
      };
    }

    if (element.props.iconRight) {
      return {
        text: element.props.title,
        type: 'section',
        selected: element.props.isSelected,
        _elemRef: element,
      };
    }

    return {
      path: element.props.path,
      text: element.props.title,
      type: 'link',
      selected: element.props.isSelected,
      _elemRef: element,
    };
  };

  private get activeItem(): VisibleItemDescriptor {
    return this.visibleItems.find(item => item.selected === true)!;
  }
}

describe('NavigationLinks', () => {
  describe('rendering', () => {
    it('should display a link for every item with path', () => {
      const obj = new TestObject([
        { path: '/1', title: { id: 'link1', defaultMessage: 'Link 1' } },
        { path: '/2', title: { id: 'link2', defaultMessage: 'Link 2' } },
      ]).render();

      obj.expectItemsToBe([
        { path: '/1' },
        { path: '/2' },
      ]);
    });
    it('should not render items that do not have links or paths', () => {
      const obj = new TestObject([
        { id: '1', title: { id: '1', defaultMessage: 'Something' } },
        { flat: true, title: { id: '2', defaultMessage: 'Something else 1' } },
        { links: [], title: { id: '3', defaultMessage: 'test' } },
      ]).render();

      obj.expectItemsToBe([
        { text: 'test' },
      ]);
    });
    it('should render non-flat top level sections as "dive-in" triggers', () => {
      const obj = new TestObject([
        {
          title: { id: 'sec1', defaultMessage: 'section1' },
          links: [{ title: { id: 'link1', defaultMessage: 'link1' }, path: '/1' }],
        },
        {
          title: { id: 'sec2', defaultMessage: 'section2' },
          links: [{ title: { id: 'link2', defaultMessage: 'link2' }, path: '/2' }],
        },
      ]).render();

      obj.expectItemsToBe([
        { text: 'section1', type: 'section' },
        { text: 'section2', type: 'section' },
      ]);
    });
    it('should render flat top level sections with their links flattened', () => {
      const obj = new TestObject([
        {
          title: { id: 'sec1', defaultMessage: 'section1' },
          flat: true,
          links: [{ title: { id: 'link1', defaultMessage: 'link1' }, path: '/1' }],
        },
        {
          title: { id: 'sec2', defaultMessage: 'section2' },
          flat: true,
          links: [{ title: { id: 'link1', defaultMessage: 'link1' }, path: '/2' }],
        },
      ]).render();

      obj.expectItemsToBe([
        { text: 'section1', type: 'heading' },
        { path: '/1', type: 'link' },
        { text: 'section2', type: 'heading' },
        { path: '/2', type: 'link' },
      ]);
    });
    it('should render flat top level sections with their subsections as "dive-in" triggers', () => {
      const obj = new TestObject([
        {
          title: { id: 'sec1', defaultMessage: 'section1' },
          flat: true,
          links: [{
            title: { id: 'sub1', defaultMessage: 'sub section 1' },
            id: 'sub-section-1',
            links: [{ title: { id: 'link1', defaultMessage: 'link 1' }, path: '/1' }],
          }],
        },
        {
          title: { id: 'sec2', defaultMessage: 'section2' },
          flat: true,
          links: [{
            title: { id: 'sub2', defaultMessage: 'sub section 2' },
            id: 'sub-section-2',
            links: [{ title: { id: 'link2', defaultMessage: 'link 2' }, path: '/2' }],
          }],
        },
      ]).render();

      obj.expectItemsToBe([
        { text: 'section1', type: 'heading' },
        { text: 'sub section 1', type: 'section' },
        { text: 'section2', type: 'heading' },
        { text: 'sub section 2', type: 'section' },
      ]);
    });
    it('should not render disabled items', () => {
      let counter = 0;
      const someLink = (): NavItem => ({
        title: { id: 'link1', defaultMessage: `Link #${++counter}` },
        path: `/link/${counter++}`,
      });
      const obj = new TestObject([
        {
          title: { id: 'top1', defaultMessage: 'Top Level Flat Section Disabled' },
          flat: true,
          disabled: true,
          links: [
            { title: { id: 'sub1', defaultMessage: 'sub section 1' }, id: 'sub-section-1', links: [someLink()] },
          ],
        },
        {
          title: { id: 'top2', defaultMessage: 'Top Level Flat Section' },
          flat: true,
          disabled: false,
          links: [
            {
              title: { id: 'sub-section-disabled', defaultMessage: 'Sub Section Disabled' },
              disabled: true,
              id: 'sub-section-disabled',
              links: [someLink()],
            },
            {
              title: { id: 'sub-section-enabled', defaultMessage: 'Sub Section' },
              disabled: false,
              id: 'sub-section-enabled',
              links: [someLink()],
            },
            {
              title: { id: 'disabled', defaultMessage: 'Sub Link Disabled' },
              path: '/sub/link/disabled',
              disabled: true,
            },
            { title: { id: 'sub2', defaultMessage: 'Sub Link' }, path: '/sub/link/enabled', disabled: false },
          ],
        },
        { title: { id: 'top3', defaultMessage: 'Top Level Section Disabled' }, links: [someLink()], disabled: true },
        { title: { id: 'top4', defaultMessage: 'Top Level Section' }, links: [someLink()], disabled: false },
        {
          title: { id: 'top5', defaultMessage: 'Top Level Link Disabled' },
          path: '/top-level-link-disabled',
          disabled: true,
        },
        { title: { id: 'top6', defaultMessage: 'Top Level Link' }, path: '/top-level-link', disabled: false },
      ]).render();

      obj.expectItemsToBe([
        { text: 'Top Level Flat Section', type: 'heading' },
        { text: 'Sub Section', type: 'section' },
        { text: 'Sub Link', type: 'link' },
        { text: 'Top Level Section', type: 'section' },
        { text: 'Top Level Link', type: 'link' },
      ]);
    });
  });

  describe('landing', () => {
    describe('no matching link', () => {
      // TODO: Implement a "fake" org section that will be shown in "loading" state if user lands on "/admin/o/*" page
      // it('should land inside "Organization / <LOADING...>" section for "/admin/o/*" links', () => {
      //   assert.fail();
      // });
      it('should default to displaying root navigation section', () => {
        const obj = new TestObject(predefinedSections).render('/admin/non-existent-route');

        obj.expectActiveItemToBe(null);
        expect(obj.backButton).to.equal(null);
      });
    });

    describe('matching link', () => {
      it('should be able to land inside a section', () => {
        const obj = new TestObject([
          {
            id: 'section',
            title: { id: '1', defaultMessage: 'Section' },
            links: [
              { path: '/test-1', title: { id: 'link1', defaultMessage: 'Link 1' } },
              { path: '/test-2', title: { id: 'link2', defaultMessage: 'Link 2' } },
            ],
          },
        ]).render('/test-2');

        obj.expectItemsToBe([
          { type: 'back' },
          { type: 'link', text: 'Link 1' },
          { type: 'link', text: 'Link 2', selected: true },
        ]);
      });
      it('should be able to land inside a section when current path has query params', () => {
        const obj = new TestObject([
          {
            id: 'section',
            title: { id: '1', defaultMessage: 'Section' },
            links: [
              { path: '/root/', title: { id: 'link1', defaultMessage: 'Link 1' } },
              { path: '/root/path1', title: { id: 'link2', defaultMessage: 'Link 2' } },
              { path: '/root/path1/path2', title: { id: 'link3', defaultMessage: 'Link 3' } },
              { path: '/root/path2', title: { id: 'link4', defaultMessage: 'Link 4' } },
            ],
          },
        ]).render('/root/path1?foo=bar');

        obj.expectItemsToBe([
          { type: 'back' },
          { type: 'link', text: 'Link 1' },
          { type: 'link', text: 'Link 2', selected: true },
          { type: 'link', text: 'Link 3' },
          { type: 'link', text: 'Link 4' },
        ]);
      });
      it('should be able to land inside site or IdM billing correctly', () => {
        const billingSections = [
          {
            id: 'subscriptions',
            title: { id: 'subscriptions', defaultMessage: 'Subscriptions & Billing' },
            links: [
              {
                title: { id: 'pricing', defaultMessage: 'Billing' },
                id: 'pricing',
                links: [
                  {
                    title: { id: 'overview', defaultMessage: 'Overview' },
                    id: 'billing-overview',
                    path: '/admin/billing/overview',
                  },
                ],
              },
              {
                id: 'identity-manager',
                title: { id: 'idm', defaultMessage: 'Identity Manager' },
                links: [
                  {
                    title: { id: 'idm-overview', defaultMessage: 'Overview' },
                    id: 'billing-overview',
                    path: '/admin/billing/overview?org=foo',
                  },
                ],
              },
            ],
          },
        ];
        const whenOnSiteBillingPage = new TestObject(billingSections).render('/admin/billing/overview');
        const whenOnIdmBillingPage = new TestObject(billingSections).render('/admin/billing/overview?org=foo');

        whenOnSiteBillingPage.expectItemsToBe([
          { type: 'back' },
          { path: '/admin/billing/overview', text: 'Overview', type: 'link', selected: true },
        ]);
        whenOnIdmBillingPage.expectItemsToBe([
          { type: 'back' },
          { path: '/admin/billing/overview?org=foo', text: 'Overview', type: 'link', selected: true },
        ]);
      });
      it('should be able to land inside a section which is a child of another (flat) section', () => {
        const obj = new TestObject([
          {
            id: 'flat-section',
            title: { id: 'flat', defaultMessage: 'Flat Section' },
            flat: true,
            links: [
              {
                id: 'section',
                title: { id: '1', defaultMessage: 'Section' },
                links: [{ path: '/test-1', title: { id: 'link1', defaultMessage: 'Link 1' } }, {
                  path: '/test-2',
                  title: { id: 'link2', defaultMessage: 'Link 2' },
                }],
              },
            ],
          },
        ]).render('/test-2');

        obj.expectActiveItemToBe({ path: '/test-2', text: 'Link 2' });
      });
      it('should be able to land on root referencing a page that is a direct child of flat section', () => {
        const obj = new TestObject([
          {
            id: 'flat-section',
            title: { id: 'flat', defaultMessage: 'Flat Section' },
            flat: true,
            links: [
              {
                id: 'section',
                title: { id: '1', defaultMessage: 'Section' },
                links: [{ path: '/test-1', title: { id: 'link1', defaultMessage: 'Link 1' } }, {
                  path: '/test-2',
                  title: { id: 'link2', defaultMessage: 'Link 2' },
                }],
              },
              { title: { id: 'simpleLink', defaultMessage: 'Simple Link' }, path: '/test-3' },
            ],
          },
        ]).render('/test-3');

        obj.expectActiveItemToBe({ path: '/test-3', text: 'Simple Link' });
      });
      it('should be able to land on root referencing a page that is one of the top pages', () => {
        const obj = new TestObject([
          {
            id: 'flat-section',
            title: { id: 'flat', defaultMessage: 'Flat Section' },
            flat: true,
            links: [
              {
                id: 'section',
                title: { id: '1', defaultMessage: 'Section' },
                links: [{ path: '/test-1', title: { id: 'link1', defaultMessage: 'Link 1' } }, {
                  path: '/test-2',
                  title: { id: 'link2', defaultMessage: 'Link 2' },
                }],
              },
              { title: { id: 'simpleLink', defaultMessage: 'Simple Link' }, path: '/test-3' },
            ],
          },
          { title: { id: 'simple-top-link', defaultMessage: 'Simple Link On Top' }, path: '/test-4' },
        ]).render('/test-4');

        obj.expectActiveItemToBe({ path: '/test-4', text: 'Simple Link On Top' });
      });
    });
  });

  describe('interactions', () => {
    it('should be able to open a section', () => {
      const obj = new TestObject([
        {
          id: 'section',
          title: { id: '1', defaultMessage: 'Section' },
          links: [{ path: '/test-1', title: { id: 'link1', defaultMessage: 'Link 1' } }, {
            path: '/test-2',
            title: { id: 'link2', defaultMessage: 'Link 2' },
          }],
        },
        { title: { id: 'simpleLink', defaultMessage: 'Simple Link' }, path: '/test-3' },
      ]).render();

      obj.openSectionWithTitle('Section');

      obj.expectItemsToBe([
        { type: 'back' },
        { type: 'link', text: 'Link 1' },
        { type: 'link', text: 'Link 2' },
      ]);
    });
    it('should navigate back to root when inside a section', () => {
      const obj = new TestObject([
        {
          id: 'section',
          title: { id: '1', defaultMessage: 'Section' },
          links: [{ path: '/test-1', title: { id: 'link1', defaultMessage: 'Link 1' } }, {
            path: '/test-2',
            title: { id: 'link2', defaultMessage: 'Link 2' },
          }],
        },
        { title: { id: 'simpleLink', defaultMessage: 'Simple Link' }, path: '/test-3' },
      ]).render('/test-2');

      obj.goBack();

      obj.expectItemsToBe([
        { type: 'section', text: 'Section' },
        { type: 'link', text: 'Simple Link' },
      ]);
    });
    it('should be able to open a section which is a child of another (flat) section', () => {
      const obj = new TestObject([
        {
          id: 'flat-section',
          title: { id: 'flat', defaultMessage: 'Flat Section' },
          flat: true,
          links: [
            {
              id: 'section',
              title: { id: '1', defaultMessage: 'Section' },
              links: [{ path: '/test-1', title: { id: 'link1', defaultMessage: 'Link 1' } }, {
                path: '/test-2',
                title: { id: 'link2', defaultMessage: 'Link 2' },
              }],
            },
          ],
        },
        { title: { id: 'simpleLink', defaultMessage: 'Simple Link' }, path: '/test-3' },
      ]).render();

      obj.openSectionWithTitle('Section');

      obj.expectItemsToBe([
        { type: 'back' },
        { type: 'link', text: 'Link 1' },
        { type: 'link', text: 'Link 2' },
      ]);
    });
    it('should navigate back to root when inside a section which is a child of another (flat) section', () => {
      const obj = new TestObject([
        {
          id: 'flat-section',
          title: { id: 'flat', defaultMessage: 'Flat Section' },
          flat: true,
          links: [
            {
              id: 'section',
              title: { id: '1', defaultMessage: 'Section' },
              links: [{ path: '/test-1', title: { id: 'link1', defaultMessage: 'Link 1' } }, {
                path: '/test-2',
                title: { id: 'link2', defaultMessage: 'Link 2' },
              }],
            },
          ],
        },
        { title: { id: 'simpleLink', defaultMessage: 'Simple Link' }, path: '/test-3' },
      ]).render('/test-1');

      obj.goBack();

      obj.expectItemsToBe([
        { type: 'heading', text: 'Flat Section' },
        { type: 'section', text: 'Section' },
        { type: 'link', text: 'Simple Link' },
      ]);
    });
    it('should see root when on a page that is a direct child of flat section', () => {
      const obj = new TestObject([
        {
          id: 'flat-section',
          title: { id: 'flat', defaultMessage: 'Flat Section' },
          flat: true,
          links: [
            {
              id: 'section',
              title: { id: '1', defaultMessage: 'Section' },
              links: [{ path: '/test-1', title: { id: 'link1', defaultMessage: 'Link 1' } }, {
                path: '/test-2',
                title: { id: 'link2', defaultMessage: 'Link 2' },
              }],
            },
            { title: { id: 'inner link', defaultMessage: 'Simple Link inside Flat section' }, path: '/test-5' },
          ],
        },
        { title: { id: 'simpleLink', defaultMessage: 'Simple Link' }, path: '/test-3' },
      ]).render('/test-5');

      obj.expectItemsToBe([
        { type: 'heading', text: 'Flat Section' },
        { type: 'section', text: 'Section' },
        { type: 'link', text: 'Simple Link inside Flat section' },
        { type: 'link', text: 'Simple Link' },
      ]);
    });
    it('should see root when on one of the top pages', () => {
      const obj = new TestObject([
        {
          id: 'flat-section',
          title: { id: 'flat', defaultMessage: 'Flat Section' },
          flat: true,
          links: [
            {
              id: 'section',
              title: { id: '1', defaultMessage: 'Section' },
              links: [{ path: '/test-1', title: { id: 'link1', defaultMessage: 'Link 1' } }, {
                path: '/test-2',
                title: { id: 'link2', defaultMessage: 'Link 2' },
              }],
            },
          ],
        },
        { title: { id: 'simpleLink', defaultMessage: 'Simple Link' }, path: '/test-3' },
      ]).render('/test-3');

      obj.expectItemsToBe([
        { type: 'heading', text: 'Flat Section' },
        { type: 'section', text: 'Section' },
        { type: 'link', text: 'Simple Link' },
      ]);
    });
  });

  describe('interactions from outside', () => {
    describe('location changes', () => {
      it('should be able to bring the user inside a section', () => {
        const obj = new TestObject([
          { id: 'section-1', title: { id: '1', defaultMessage: 'Section 1' }, links: section1Links },
          { id: 'section-2', title: { id: '2', defaultMessage: 'Section 2' }, links: section2Links },
        ]).render('/section-1/link-1');

        obj.navigateTo('/section-2/link-2');

        obj.expectItemsToBe([
          { type: 'back' },
          { type: 'link', text: 'Section 2 Link 1' },
          { type: 'link', text: 'Section 2 Link 2' },
        ]);
        obj.expectActiveItemToBe({ type: 'link', text: 'Section 2 Link 2' });
      });
      it('should be able to bring the user inside a section which is a child of another (flat) section', () => {
        const obj = new TestObject([
          { id: 'section-1', title: { id: '1', defaultMessage: 'Section 1' }, links: section1Links },
          {
            id: 'flat-section',
            title: { id: 'flat', defaultMessage: 'Flat Section' },
            flat: true,
            links: [
              {
                id: 'section-2',
                title: { id: '2', defaultMessage: 'Section 2' },
                links: section2Links,
              },
            ],
          },
        ]).render('/section-1/link-1');

        obj.navigateTo('/section-2/link-2');

        obj.expectItemsToBe([
          { type: 'back' },
          { type: 'link', text: 'Section 2 Link 1' },
          { type: 'link', text: 'Section 2 Link 2' },
        ]);
        obj.expectActiveItemToBe({ type: 'link', text: 'Section 2 Link 2' });
      });
      it('should be able to bring the user to the root when on a page that is a direct child of flat section', () => {
        const obj = new TestObject([
          { id: 'section-1', title: { id: '1', defaultMessage: 'Section 1' }, links: section1Links },
          {
            id: 'flat-section',
            title: { id: 'flat', defaultMessage: 'Flat Section' },
            flat: true,
            links: [
              {
                id: 'section-2',
                title: { id: '2', defaultMessage: 'Section 2' },
                links: section2Links,
              },
              {
                path: '/simple-link',
                title: { id: 'simpleLink', defaultMessage: 'Simple Link' },
              },
            ],
          },
        ]).render('/section-1/link-1');

        obj.navigateTo('/simple-link');

        obj.expectItemsToBe([
          { type: 'section', text: 'Section 1' },
          { type: 'heading', text: 'Flat Section' },
          { type: 'section', text: 'Section 2' },
          { type: 'link', text: 'Simple Link', selected: true },
        ]);
      });
      it('should be able to bring the user to the root when on one of the top pages', () => {
        const obj = new TestObject([
          { id: 'section-1', title: { id: '1', defaultMessage: 'Section 1' }, links: section1Links },
          { id: 'section-2', title: { id: '2', defaultMessage: 'Section 2' }, links: section2Links },
          { path: '/simple-link', title: { id: 'simpleLink', defaultMessage: 'Simple Link' } },
        ]).render('/section-1/link-1');

        obj.navigateTo('/simple-link');

        obj.expectItemsToBe([
          { type: 'section', text: 'Section 1' },
          { type: 'section', text: 'Section 2' },
          { type: 'link', text: 'Simple Link' },
        ]);
        obj.expectActiveItemToBe({ type: 'link', text: 'Simple Link' });
      });
    });
  });
});
