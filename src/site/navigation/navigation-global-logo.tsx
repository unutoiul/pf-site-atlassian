import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';

import { AtlassianIcon as AkAtlassianIcon } from '@atlaskit/logo';
import { AkGlobalItem } from '@atlaskit/navigation';

import { withClickAnalytics } from 'common/analytics';
import { NavigationTooltip } from 'common/navigation-tooltip';

const messages = defineMessages({
  logoAltText: {
    id: 'chrome.site.navigation.global-logo-label',
    description: 'Alt text for the logo',
    defaultMessage: 'Atlassian logo',
  },
  globalItemButtonTooltip: {
    id: 'chrome.site.navigation.global-logo.tooltip',
    defaultMessage: 'Admin',
  },
});

const AkGlobalItemWithAnalytics = withClickAnalytics<{ size: string; onClick(e: MouseEvent): void; }>()(AkGlobalItem);

interface NavigationGlobalLogoProps {
  onClick<T>(e: React.MouseEvent<T>): void;
}

class NavigationGlobalLogoImpl extends React.Component<RouteComponentProps<any> & InjectedIntlProps & NavigationGlobalLogoProps> {
  public render() {
    const { formatMessage } = this.props.intl;
    const alt = formatMessage(messages.logoAltText);

    return (
      <NavigationTooltip message={formatMessage(messages.globalItemButtonTooltip)}>
        <AkGlobalItemWithAnalytics
          size="medium"
          onClick={this.onClick}
          analyticsData={{
            action: 'click',
            actionSubject: 'sidebarGlobalButton',
            actionSubjectId: 'home',
            subproduct: 'chrome',
          }}
        >
          <AkAtlassianIcon label={alt} size="large" />
        </AkGlobalItemWithAnalytics>
      </NavigationTooltip>
    );
  }

  private onClick = (e) => {
    this.props.history.push('/admin');
    if (this.props.onClick) {
      this.props.onClick(e);
    }
  }
}

export const NavigationGlobalLogo = withRouter<{}>(injectIntl(NavigationGlobalLogoImpl));
