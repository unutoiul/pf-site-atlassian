import { Location } from 'history';
import { extract, parse } from 'query-string';
import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router-dom';

import AkArrowRightIcon from '@atlaskit/icon/glyph/arrow-right';
import {
  AkContainerNavigationNested,
  AkNavigationItemGroup,
} from '@atlaskit/navigation';

import { AnalyticsData } from 'common/analytics';
import { ErrorBoundary } from 'common/error-boundary';
import { NavigationLink, NavigationLinkIconGlyph, NavItem } from 'common/navigation';

import { FeedbackNavigationGroup } from 'common/feedback';

const messages = defineMessages({
  back: {
    id: 'chrome.nav.back',
    defaultMessage: 'Back',
  },
});

interface OwnProps {
  navigationSections?: NavItem[];
}

export type NavigationLinksProps = OwnProps & RouteComponentProps<any>;

interface NavigationLinksState {
  stack: NavItem[][];
}

function isHighlighted(currentLocation: Location, item: NavItem): false | number {

  const currentPath = currentLocation.pathname + currentLocation.search;

  if (currentPath.startsWith('/admin/billing') && !currentPath.startsWith('/admin/billing/addapplication')) {
    // IdM billing pages use the same urls as site billing pages, but with a query param for now
    const currentParams = parse(extract(currentPath));
    if (item.path) {
      const itemParams = parse(extract(item.path));
      if ((currentParams.org || itemParams.org) && !(currentParams.org && itemParams.org)) {
        return false;
      }
    }

    // some billing links can be loaded later than others (in case BUX is enabled).
    // If that's the case we consider "Billing" section active.
    if (item.id === 'pricing' && !currentParams.org) {
      // lowest priority
      return 1;
    }
  }
  if (item.path) {
    return currentPath.indexOf(item.path) === 0 ? item.path.length : false;
  }
  if (item.links && item.links.length > 0) {
    return (
      item.links
        .map(l => isHighlighted(currentLocation, l))
        .filter(Boolean)
        .sort()
        .reverse() || [false]
    )[0];
  }

  // nav item has neither path nor children - it can't possibly be "active"
  return false;
}

const getBestMatchingLink = (links: NavItem[], location: Location) => {
  const best = links
    .map(item => ({ item, priority: isHighlighted(location, item) }))
    .filter(({ priority }) => !!priority)
    .sort()
    .reverse()[0];

  return best ? best.item : undefined;
};

function analyticsData(item: { analyticsSubjectId: string, id?: string, path?: string, analyticsMeta?: object } | NavItem): AnalyticsData {
  return {
    action: 'click',
    actionSubject: 'sidebarContainerButton',
    actionSubjectId: item.analyticsSubjectId || (item.id || item.path)!,
    attributes: item.analyticsMeta,
    subproduct: 'chrome',
  };
}

export class NavigationLinksImpl extends React.PureComponent<NavigationLinksProps & InjectedIntlProps, NavigationLinksState> {
  public static defaultProps: Partial<OwnProps> = {
    navigationSections: [],
  };

  public state: Readonly<NavigationLinksState> = {
    stack: [this.props.navigationSections || []],
  };

  public componentWillReceiveProps(nextProps: NavigationLinksProps) {
    const locationChanged = this.props.location.pathname !== nextProps.location.pathname
      || this.props.location.search !== nextProps.location.search;
    const sectionsChanged = nextProps.navigationSections !== this.props.navigationSections;

    if (!sectionsChanged && !locationChanged) {
      return;
    }

    let current = nextProps.navigationSections;
    if (!current) {
      return;
    }

    const breadcrumbs = [current];

    while (current) {
      const next = getBestMatchingLink(current, nextProps.location);
      current = next && next.links;
      if (current && next && !next.flat) {
        breadcrumbs.push(current);
      }
    }

    this.setState({ stack: breadcrumbs });
  }

  public render() {
    return (
      <ErrorBoundary>
        <AkContainerNavigationNested stack={this.renderStack()} />
      </ErrorBoundary>
    );
  }

  private backButton = () => (
    <NavigationLink
      icon={NavigationLinkIconGlyph.ArrowLeft}
      onClick={this.stackPop}
      analyticsData={analyticsData({ analyticsSubjectId: 'back' })}
      title={this.props.intl.formatMessage(messages.back)}
      key="back"
    />
  );

  private stackPush = (newPage?: NavItem[]) => {
    if (!newPage) {
      return;
    }
    const stack = [...this.state.stack, newPage];
    this.setState({ stack });
  };

  private stackPop = () => {
    if (this.state.stack.length <= 1) {
      return;
    }

    const stack = this.state.stack.slice(0, this.state.stack.length - 1);

    this.setState({ stack });
  };

  private renderItem(item: NavItem, isSelected): React.ReactNode {
    if (item.disabled) {
      return null;
    }
    if (!item.path && !item.links) {
      return null;
    }
    const title = this.props.intl.formatMessage(item.title);

    if (item.links) {
      if (item.flat) {
        const selectedLink = getBestMatchingLink(item.links, this.props.location);

        return (
          <AkNavigationItemGroup key={item.id} title={title}>
            {item.links.map(link => this.renderItem(link, selectedLink === link)).filter(link => link != null)}
          </AkNavigationItemGroup>
        );
      }

      /* tslint:disable:react-this-binding-issue */
      return (
        <NavigationLink
          key={item.id}
          id={item.id}
          icon={item.icon}
          iconRight={<AkArrowRightIcon label={''} />}
          title={title}
          onClick={() => this.stackPush(item.links)}
          lozenge={item.lozenge}
          analyticsData={analyticsData(item)}
          isExternal={!!item.isExternal}
          isSelected={isSelected}
        />
      );
      /* tslint:enable */
    }

    return (
      <NavigationLink
        key={item.path}
        id={item.id}
        path={item.path}
        icon={item.icon}
        title={title}
        lozenge={item.lozenge}
        analyticsData={analyticsData(item)}
        isExternal={!!item.isExternal}
        isSelected={isSelected}
      />
    );
  }

  private renderStack() {
    return this.state.stack.map((page, index) => {
      const selectedItem = getBestMatchingLink(page, this.props.location);

      return [
        ...(index > 0 ? [this.backButton()] : []),
        ...page.map(item => this.renderItem(item, selectedItem === item)).filter(element => element != null),
        ...(index === 0 ? [<FeedbackNavigationGroup key="feedback" />] : []),
      ];
    });
  }
}

export const NavigationLinks = withRouter(injectIntl(NavigationLinksImpl));
