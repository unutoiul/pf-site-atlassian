import { DrawerLink } from 'common/navigation';

import {
  Action,
  NavigationAction,
} from '../../store/action-types';

import { drawerLinks as knownDrawerLinks } from './routing';

export type DrawerLinkState = ReadonlyArray<DrawerLink>;

export const navigationDrawerLinksReducer = (
  state: DrawerLinkState = knownDrawerLinks,
  action: Action,
) => {
  switch (action.type) {
    case NavigationAction.CHROME_DRAWER_LINK_SHOW:
      return [
        ...state.filter(link => link.path !== action.drawerLink.path),
        action.drawerLink,
      ];
    case NavigationAction.CHROME_DRAWER_LINK_HIDE:
      return state.filter(link => link.path !== action.drawerLink.path);
    default:
      return state;
  }
};
