import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import AkArrowLeftIcon from '@atlaskit/icon/glyph/arrow-left';
import { AkCreateDrawer } from '@atlaskit/navigation';

import { DrawerLink, NavigationLink } from 'common/navigation';

import { NavigationGlobalLogo } from './navigation-global-logo';
import { NavigationHeader } from './navigation-header';

const messages = defineMessages({
  backIconLabel: {
    id: 'chrome.navigation-drawer.back-icon-label',
    description: 'Alt text for the back icon',
    defaultMessage: 'Back icon',
  },
});

export interface NavigationCreateDrawerProps {
  isOpen?: boolean;
  links: ReadonlyArray<DrawerLink>;
  onCloseDrawer?(): void;
}

export class NavigationCreateDrawerImpl extends React.Component<NavigationCreateDrawerProps & InjectedIntlProps> {
  public static defaultProps = {
    isOpen: false,
    onCloseDrawer: () => null,
  };

  public render() {
    return (
      <AkCreateDrawer
        backIcon={<AkArrowLeftIcon label={this.props.intl.formatMessage(messages.backIconLabel)} />}
        header={<NavigationHeader />}
        isOpen={this.props.isOpen}
        onBackButton={this.props.onCloseDrawer}
        primaryIcon={<NavigationGlobalLogo />}
      >
        {this.props.links.map(link => (
          <NavigationLink
            analyticsData={{ action: 'click', actionSubject: 'createDrawerButton', actionSubjectId: link.path, subproduct: 'chrome' }}
            key={link.path}
            path={link.path}
            onClick={this.props.onCloseDrawer}
            title={this.props.intl.formatMessage(link.title)}
          />))}
        {this.props.children}
      </AkCreateDrawer>
    );
  }
}

export const NavigationCreateDrawer = injectIntl<NavigationCreateDrawerProps>(NavigationCreateDrawerImpl);
