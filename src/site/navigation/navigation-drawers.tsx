import * as React from 'react';
import { graphql } from 'react-apollo';
import { connect } from 'react-redux';

import { closeDrawer, DrawerLink, NavigationDrawer, NavItem } from 'common/navigation';

import { BUXDrawer } from '../../apps/billing/adapters/Drawer';
import { UserExportDrawer } from '../../site/user-management/users/user-export-drawer/user-export-drawer';
import { UserInviteDrawer } from '../../site/user-management/users/user-invite-drawer/user-invite-drawer';
import { RootState } from '../../store';
import { NavigationCreateDrawer } from './navigation-create-drawer';

import { NavigationV1Query } from '../../schema/schema-types';
import { GSuiteWizard } from '../user-management/g-suite/wizard/g-suite-wizard';
import navigationV1Query from './navigation.query.graphql';

interface NavigationDrawersProps {
  drawerLinks: ReadonlyArray<DrawerLink>;
  openedDrawer?: NavigationDrawer;
  navigationSections: NavItem[];
  siteId?: string;

  closeDrawerAction(): void;
}

interface DerivedProps {
  siteId?: string;
}

export const NavigationDrawersImp: React.SFC<NavigationDrawersProps> = ({ siteId, openedDrawer, drawerLinks, closeDrawerAction }) => (
  <React.Fragment>
    <NavigationCreateDrawer
      key="create"
      isOpen={openedDrawer === NavigationDrawer.Create}
      links={drawerLinks}
      onCloseDrawer={closeDrawerAction}
    />
    <UserInviteDrawer
      key="userInvite"
      isOpen={openedDrawer === NavigationDrawer.UserInvite}
      onClose={closeDrawerAction}
      cloudId={siteId}
    />
    <UserExportDrawer
      key="userExport"
      isOpen={openedDrawer === NavigationDrawer.UserExport}
      onClose={closeDrawerAction}
      cloudId={siteId}
    />
    <GSuiteWizard
      key="gSuiteWizard"
      cloudId={siteId}
      onClose={closeDrawerAction}
      isDrawerOpen={openedDrawer === NavigationDrawer.GSuiteWizard}
    />
    <BUXDrawer
      key="bux"
    />
  </React.Fragment>
);

const mapStateToProps = (state: RootState) => ({
  openedDrawer: state.chrome.navigationDrawers.openDrawer,
  navigationSections: state.chrome.navigationSections,
  drawerLinks: state.chrome.drawerLinks,
});

const withSiteId = graphql<{}, NavigationV1Query, {}, DerivedProps>(navigationV1Query, {
  props: ({ data }) => ({
    siteId: data && data.currentSite && data.currentSite.id,
  }),
});

export const NavigationDrawers = connect(
  mapStateToProps,
  { closeDrawerAction: () => closeDrawer(false) },
)(
  withSiteId(NavigationDrawersImp),
);
