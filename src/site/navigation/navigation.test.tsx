import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import { ScreenEventSender } from 'common/analytics';

import { NavigationImpl, NavigationProps } from './navigation';

describe('Navigation', () => {
  it('should send a screen event when data loads', () => {
    const wrapper = shallow<NavigationProps>((
      <NavigationImpl
        {...{} as any}
      />
    ));

    expect(wrapper.find(ScreenEventSender).prop('isDataLoading')).to.equal(true);
    wrapper.setProps({ siteId: 'dummy-id-1' });
    expect(wrapper.find(ScreenEventSender).prop('isDataLoading')).to.equal(false);
  });
});
