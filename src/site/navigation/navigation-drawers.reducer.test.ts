import { expect } from 'chai';

import { closeDrawer, NavigationDrawer, openDrawer } from 'common/navigation';

import { navigationDrawers } from './navigation-drawers.reducer';

describe('NavigationDrawers Reducer', () => {
  it('returns the correct default state', () => {
    const updatedState = navigationDrawers({}, { type: 'foo' } as any);

    expect(updatedState).to.deep.equal({});
  });

  it('handles openDrawer', () => {
    const state = {};

    const updatedState = navigationDrawers(state, openDrawer(NavigationDrawer.Create));

    expect(updatedState).to.deep.equal({ openDrawer: NavigationDrawer.Create });
  });

  it('handles closeDrawer', () => {
    const state = { openDrawer: NavigationDrawer.Create };

    const updatedState = navigationDrawers(state, closeDrawer());

    expect(updatedState).to.deep.equal({ openDrawer: undefined });
  });
});
