import { defineMessages } from 'react-intl';
import { RouteProps } from 'react-router-dom';

import { DrawerLink } from 'common/navigation';

import { PickLoader } from 'common/loading';

import { BillingNavMounter } from '../../apps/billing/billing-nav-mounter';
import { MovedOrgOnboardingPage } from '../../organization/onboarding/moved-org-onboarding-page';
import { OrganizationNavMounter } from '../../organization/organizations';
import { EmojiPage } from '../emoji';
import { AppNavMounter } from '../nav-mounter/app-nav-mounter';
import { JsdNavMounter } from '../nav-mounter/jsd-nav-mounter';
import { StrideExportDetailPage } from '../stride/stride-export-detail-page';
import { StridePage } from '../stride/stride-page';
import { UserConnectedAppsPage } from '../user-connected-apps';
import { UserConnectedAppsDetailsPage } from '../user-connected-apps/user-connected-app-details';
import { AccessConfigurationPage } from '../user-management/access-config/access-config-page';
import { AccessRequestsPage } from '../user-management/access-requests/access-requests-page';
import { GSuitePage } from '../user-management/g-suite/g-suite-page';
import { GroupDetailPage } from '../user-management/groups/group-detail-page';
import { GroupsPage } from '../user-management/groups/groups-page';
import { JsdPage } from '../user-management/jsd/jsd-page';
import { SiteAccessPage } from '../user-management/site-access/site-access-page';
import { SiteUrlPage } from '../user-management/site-url/site-url-page';
import { UserManagementMigrationMounter } from '../user-management/user-management-migration-mounter';
import { UserDetailsPage } from '../user-management/users/user-details/user-details-page/user-details-page';
import { UsersPage } from '../user-management/users/user-list/users-page';

const loadBillingPages = async () => import(
  /* webpackChunkName: "billing-pages" */
  '../../apps/billing',
);

const BillHistoryPage = PickLoader(
  loadBillingPages,
  ({ BillHistory }) => BillHistory,
);

const BillEstimatePage = PickLoader(
  loadBillingPages,
  ({ BillEstimate }) => BillEstimate,
);

const BillOverviewPage = PickLoader(
  loadBillingPages,
  ({ BillOverview }) => BillOverview,
);

const ManageSubscriptionsPage = PickLoader(
  loadBillingPages,
  ({ ManageSubscriptions }) => ManageSubscriptions,
);

const PaymentDetailPage = PickLoader(
  loadBillingPages,
  ({ PaymentDetail }) => PaymentDetail,
);

const DiscoverNewProductsPage = PickLoader(
  loadBillingPages,
  ({ DiscoverNewProducts }) => DiscoverNewProducts,
);

const messages = defineMessages({
  createGroup: {
    id: 'chrome.navigation-create-drawer.create-group',
    description: 'Text in the create drawer for creating a group',
    defaultMessage: 'Create a group',
  },
  inviteUser: {
    id: 'chrome.navigation-create-drawer.invite-users',
    description: 'Text in the create drawer for inviting a user',
    defaultMessage: 'Invite a user',
  },
});

export const routes: RouteProps[] = [
  {
    path: '/admin/s/:cloudId/emoji',
    component: EmojiPage,
  },
  {
    path: '/admin/s/:cloudId/apps',
    component: AccessConfigurationPage,
  },
  {
    path: '/admin/s/:cloudId/signup',
    component: SiteAccessPage,
  },
  {
    path: '/admin/s/:cloudId/gsuite',
    component: GSuitePage,
  },
  {
    path: '/admin/s/:cloudId/user-connected-apps',
    component: UserConnectedAppsPage,
    exact: true,
  },
  {
    path: '/admin/s/:cloudId/user-connected-apps/:appId',
    component: UserConnectedAppsDetailsPage,
    exact: true,
  },
  {
    path: '/admin/s/:cloudId/site-url',
    component: SiteUrlPage,
  },
  {
    path: '/admin/s/:cloudId/groups',
    component: GroupsPage,
    exact: true,
  },
  {
    path: '/admin/s/:cloudId/users',
    component: UsersPage,
    exact: true,
  },
  {
    path: '/admin/s/:cloudId/jira-service-desk/portal-only-customers',
    component: JsdPage,
    exact: true,
  },
  {
    path: '/admin/s/:cloudId/users/:userId',
    component: UserDetailsPage,
  },
  {
    path: '/admin/s/:cloudId/access-requests',
    component: AccessRequestsPage,
  },
  {
    path: '/admin/s/:cloudId/billing',
    component: BillOverviewPage,
    exact: true,
  },
  {
    path: '/admin/s/:cloudId/billing/overview',
    component: BillOverviewPage,
  },
  {
    path: '/admin/s/:cloudId/billing/estimate',
    component: BillEstimatePage,
  },
  {
    path: '/admin/s/:cloudId/billing/paymentdetails',
    component: PaymentDetailPage,
  },
  {
    path: '/admin/s/:cloudId/billing/history',
    component: BillHistoryPage,
  },
  {
    path: '/admin/s/:cloudId/billing/applications',
    component: ManageSubscriptionsPage,
  },
  {
    path: '/admin/s/:cloudId/billing/addapplication',
    component: DiscoverNewProductsPage,
  },
  {
    path: '/admin/s/:cloudId/groups/:groupId',
    component: GroupDetailPage,
  },
  {
    path: '/admin/s/:cloudId/stride',
    component: StridePage,
    exact: true,
  },
  {
    path: '/admin/s/:cloudId/stride/:exportId',
    component: StrideExportDetailPage,
  },
  {
    path: '/admin/security/moved',
    component: MovedOrgOnboardingPage,
  },
];

export const drawerLinks: DrawerLink[] = [
  {
    path: '/admin/users/invite',
    title: messages.inviteUser,
  },
  {
    path: '/admin/groups/add',
    title: messages.createGroup,
  },
];

interface NavigationMounter {
  id: string;
  mounter: React.ComponentClass<any>;
}

export const navMounters: NavigationMounter[] = [
  {
    id: 'organization-nav',
    mounter: OrganizationNavMounter,
  },
  {
    id: 'jsd-nav',
    mounter: JsdNavMounter,
  },
  {
    id: 'billing-nav',
    mounter: BillingNavMounter,
  },
  {
    id: 'app-nav',
    mounter: AppNavMounter,
  },
  {
    id: 'um-nav',
    mounter: UserManagementMigrationMounter,
  },
];
