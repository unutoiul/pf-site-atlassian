import { NavigationDrawer } from 'common/navigation';

import {
  Action,
  NavigationAction,
} from '../../store/action-types';

export interface NavigationDrawersState {
  readonly openDrawer?: NavigationDrawer;
}

export const navigationDrawers = (
  state: NavigationDrawersState = {},
  action: Action,
) => {
  switch (action.type) {
    case NavigationAction.CHROME_DRAWER_OPEN:
      if (action.modifyHistory) {
        window.history.pushState(action, 'openDrawer');
      }

      return {
        ...state,
        openDrawer: action.drawer,
      };
    case NavigationAction.CHROME_DRAWER_CLOSE:
      if (action.modifyHistory) {
        window.history.pushState(action, 'closeDrawer');
      }

      return {
        ...state,
        openDrawer: undefined,
      };
    default:
      return state;
  }
};
