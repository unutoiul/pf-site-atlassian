import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import AkCreateIcon, { Props as AkCreateIconProps } from '@atlaskit/icon/glyph/add';

import { withClickAnalytics } from 'common/analytics';
import { NavigationTooltip } from 'common/navigation-tooltip';

const CreateIconWithAnalytics = withClickAnalytics<AkCreateIconProps>()(AkCreateIcon);

const messages = defineMessages({
  globalButtonTooltip: {
    id: 'chrome.navigation.global-create-icon.tooltip',
    defaultMessage: 'Create users and groups',
  },
});

export class NavigationGlobalCreateIconImpl extends React.Component<InjectedIntlProps> {
  public render() {
    const { formatMessage } = this.props.intl;
    const message = formatMessage(messages.globalButtonTooltip);

    return (
      <NavigationTooltip message={message}>
        <CreateIconWithAnalytics
          analyticsData={
            {
              action: 'click',
              actionSubject: 'sidebarGlobalButton',
              actionSubjectId: 'create',
              subproduct: 'chrome',
            }}
          label={message}
        />
      </NavigationTooltip>
    );
  }

}

export const NavigationGlobalCreateIcon = injectIntl<{}>(NavigationGlobalCreateIconImpl);
