import { expect } from 'chai';

import { NavigationLinkIconGlyph, SectionUpdateParams } from 'common/navigation';

import { NavigationAction } from '../../store/action-types';
import { navigationSections, NavigationState } from './navigation-sections.reducer';

const sectionTitle1 = { id: 'sec1', defaultMessage: 'Section 1' };
const sectionTitle2 = { id: 'sec2', defaultMessage: 'Section 2' };
const otherTitle = { id: '2', defaultMessage: 'Other Title' };
const otherLinkTitle = { id: 'other-link', defaultMessage: 'Other Link Title' };
const linkTitle1 = { id: 'link1', defaultMessage: 'Link 1' };
const linkTitle2 = { id: 'link2', defaultMessage: 'Link 2' };
const newLinkTitle1 = { id: 'new-link1', defaultMessage: 'New Link 1' };
const newLinkTitle2 = { id: 'new-link2', defaultMessage: 'New Link 2' };
const newLinkTitle3 = { id: 'new-link3', defaultMessage: 'New Link 3' };
const flatSectionTitle1 = { id: 'flat1', defaultMessage: 'Flat Section 1' };
const subSectionTitle1 = { id: 'sub1', defaultMessage: 'Sub Section 1' };
const subLinkTitle1 = { id: 'sub-link1', defaultMessage: 'Sub Link 1' };
const subLinkTitle2 = { id: 'sub-link2', defaultMessage: 'Sub Link 2' };
const subLinkTitle3 = { id: 'sub-link3', defaultMessage: 'Sub Link 3' };
const subLinkTitle4 = { id: 'sub-link4', defaultMessage: 'Sub Link 4' };
const someLinkTitle = { id: 'some-link', defaultMessage: 'Some Link 1' };
const updatedLinkTitle1 = { id: 'updated-link', defaultMessage: 'Link 1 (updated title)' };

describe('navigationSections reducer', () => {
  describe('section update', () => {
    const updateSection = (state: NavigationState, section: SectionUpdateParams | { id: string }) => {
      return navigationSections(state, { type: NavigationAction.CHROME_NAVIGATION_SECTION_UPDATE, section } as any);
    };

    it('should update the section title if paths are different', () => {
      const oldState = [
        {
          id: 'section',
          title: sectionTitle1,
          path: '/path',
        },
      ];
      const newState = updateSection(
        oldState,
        { id: 'section', title: otherTitle, path: '/other/path' },
      );

      expect(newState[0].title.defaultMessage).to.equal('Other Title');
    });

    it('should not update the section if paths are same and no title was provided', () => {
      const oldState = [
        {
          id: 'section',
          title: sectionTitle1,
          path: '/path',
        },
      ];
      const newState = updateSection(
        oldState,
        { id: 'section', path: '/path' },
      );

      expect(newState[0]).to.deep.equal(oldState[0]);
    });

    it('should update the section title when it was specified even if paths are same', () => {
      const oldState = [
        {
          id: 'section',
          title: sectionTitle1,
          path: '/path',
        },
      ];
      const newState = updateSection(
        oldState,
        { id: 'section', title: otherTitle, path: '/path' },
      );

      expect(newState[0].title.defaultMessage).to.equal('Other Title');
    });

    it('should not update the section if links point to the same location', () => {
      const oldState = [
        {
          id: 'section',
          title: sectionTitle1,
          links: [{ path: '/link/1', title: linkTitle1 }],
        },
      ];
      const newState = updateSection(
        oldState,
        { id: 'section', links: [{ path: '/link/1', title: updatedLinkTitle1 }] },
      );

      expect(newState[0]).to.equal(oldState[0]);
    });

    it('should update the section title when it was specified even if links point to the same location', () => {
      const oldState = [
        {
          id: 'section',
          title: sectionTitle1,
          links: [{ path: '/link/1', title: linkTitle1 }],
        },
      ];
      const newState = updateSection(
        oldState,
        { id: 'section', title: otherTitle, links: [{ path: '/link/1', title: updatedLinkTitle1 }] },
      );

      expect(newState[0].title.defaultMessage).to.equal('Other Title');
    });

    it('should update the link title when it was specified', () => {
      const oldState = [
        {
          id: 'section',
          title: sectionTitle1,
          links: [{ id: 'link-id', path: '/link/1', title: linkTitle1 }],
        },
      ];
      const newState = updateSection(
        oldState,
        { id: 'link-id', title: otherLinkTitle },
      );

      expect(newState[0].links![0].title.defaultMessage).to.equal('Other Link Title');
    });

    it('should not add the section with no path or links', () => {
      const newState = updateSection([
      ], { id: 'section', title: sectionTitle1, links: undefined, path: undefined });

      expect(newState.length).to.equal(0);
    });

    it('should add the section with path', () => {
      const newState = updateSection([
      ], { id: 'section', title: sectionTitle1, path: '/section/path' });

      expect(newState[0].title.defaultMessage).to.equal('Section 1');
    });

    it('should add the section with links', () => {
      const newState = updateSection([
      ], { id: 'section', title: sectionTitle1, links: [{ id: 'link', path: '/path', title: linkTitle1 }] });

      expect(newState[0].title.defaultMessage).to.equal('Section 1');
    });

    it('should not add a section with links that already exist in state', () => {
      const newState = updateSection([
        { id: 'section-1', title: sectionTitle1, links: [{ id: 'link-1', path: '/path/1', title: linkTitle1 }] },
        { id: 'link-2', title: linkTitle2, path: '/path/2' },
      ], { id: 'section-2', title: sectionTitle2, links: [{ id: 'new-link-1', path: '/path/1', title: newLinkTitle1 }, { id: 'new-link-2', path: '/path/2', title: newLinkTitle2 }] });

      expect(newState.length).to.equal(2);
    });

    it('should only add section links that did not already exist in state', () => {
      const newState = updateSection([
        { id: 'section-1', title: sectionTitle1, links: [{ id: 'link-1', title: linkTitle1, path: '/path/1' }] },
        { id: 'link-2', title: linkTitle2, path: '/path/2' },
      ],
        {
          id: 'section', title: sectionTitle1, links: [
            { id: 'new-link-1', title: newLinkTitle1, path: '/path/1' },
            { id: 'new-link-2', title: newLinkTitle2, path: '/path/2' },
            { id: 'new-link-3', title: newLinkTitle3, path: '/path/3' },
          ],
        });

      expect(newState.length).to.equal(3);
      expect(newState[2].links!.length).to.equal(1);
      expect(newState[2].links![0].path).to.equal('/path/3');
    });

    it('should preserve original title upon section update', () => {
      const newState = updateSection([
        {
          id: 'section-1',
          title: sectionTitle1,
          links: [
            { id: 'link-1', title: linkTitle1, path: '/path/1' },
          ],
        },
      ],
        {
          id: 'section-1', links: [
            { id: 'link-2', title: linkTitle2, path: '/path/2' },
          ],
        });

      expect(newState.length).to.equal(1);
      expect(newState[0].links!.length).to.equal(2);
      expect(newState[0].title.defaultMessage).to.equal('Section 1');
    });

    it('should preserve original path upon section update', () => {
      const newState = updateSection([
        {
          id: 'section-1',
          title: sectionTitle1,
          path: '/path/',
          links: [
            { id: 'link-1', title: linkTitle1, path: '/path/1' },
          ],
        },
      ],
        {
          id: 'section-1', links: [
            { id: 'link-2', title: linkTitle2, path: '/path/2' },
          ],
        });

      expect(newState.length).to.equal(1);
      expect(newState[0].links!.length).to.equal(2);
      expect(newState[0].path).to.equal('/path/');
    });

    it('should update nested section if it acquired links', () => {
      const newState = updateSection([
        {
          id: 'flat-section',
          title: flatSectionTitle1,
          flat: true,
          links: [
            { id: 'link-1', title: linkTitle1, path: '/path/1' },
            { id: 'sub-section-1', title: subSectionTitle1, links: undefined },
          ],
        },
      ],
        {
          id: 'sub-section-1', links: [
            { id: 'new-link-1', title: newLinkTitle1, path: '/new/path/1' },
          ],
        });

      expect(newState.length).to.equal(1);
      expect(newState[0].links!.length).to.equal(2);
      const updatedSection = newState[0].links![1];
      expect(updatedSection.links).to.not.equal(null);
      expect(updatedSection.links!.length).to.equal(1);
      expect(updatedSection.links![0].path).to.equal('/new/path/1');
    });

    it('should update nested section if it acquired path', () => {
      const newState = updateSection([
        {
          id: 'flat-section',
          title: flatSectionTitle1,
          flat: true,
          links: [
            { id: 'link-1', title: linkTitle1, path: '/path/1' },
            { id: 'sub-section-1', title: subSectionTitle1, links: undefined },
          ],
        },
      ],
        {
          id: 'sub-section-1', title: subSectionTitle1, path: '/sub/section/path',
        });

      expect(newState.length).to.equal(1);
      expect(newState[0].links!.length).to.equal(2);
      const updatedSection = newState[0].links![1];
      expect(updatedSection.path).to.equal('/sub/section/path');
    });

    it('should have nested section placed at the right position after update (based on priority)', () => {
      const newState = updateSection([
        {
          id: 'flat-section',
          title: flatSectionTitle1,
          flat: true,
          links: [
            { id: 'link-1', title: linkTitle1, path: '/path/1', priority: 10 },
            {
              id: 'sub-section-1',
              title: subSectionTitle1,
              links: [
                { id: 'sub-link-1', title: subLinkTitle1, path: '/sub/link/path/1' },
              ],
              priority: 20,
            },
            { id: 'link-2', title: linkTitle2, path: '/path/2', priority: 30 },
          ],
        },
      ],
        {
          id: 'sub-section-1',
          title: subSectionTitle1,
          links: [
            { id: 'sub-link-1', title: subLinkTitle1, path: '/sub/link/path/1' },
            { id: 'sub-link-2', title: subLinkTitle2, path: '/sub/link/path/2' },
          ],
        },
      );

      expect(newState.length).to.equal(1);
      expect(newState[0].links!.length).to.equal(3);
      expect(newState[0].links![1].id).to.equal('sub-section-1');
      const updatedSection = newState[0].links![1];
      expect(updatedSection.links).to.not.equal(null);
      expect(updatedSection.links!.length).to.equal(2);
      expect(updatedSection.links![0].path).to.equal('/sub/link/path/1');
      expect(updatedSection.links![1].path).to.equal('/sub/link/path/2');
    });

    it('should update nested section if it got a link that did not exist before', () => {
      const newState = updateSection([
        {
          id: 'flat-section',
          title: flatSectionTitle1,
          flat: true,
          links: [
            { id: 'link-1', title: linkTitle1, path: '/path/1' },
            {
              id: 'sub-section-1',
              title: subSectionTitle1,
              links: [
                { id: 'sub-link-1', title: subLinkTitle1, path: '/sub/link/path/1' },
              ],
            },
          ],
        },
      ],
        {
          id: 'sub-section-1',
          title: subSectionTitle1,
          links: [
            { id: 'sub-link-1', title: subLinkTitle1, path: '/sub/link/path/1' },
            { id: 'sub-link-2', title: subLinkTitle2, path: '/sub/link/path/2' },
          ],
        },
      );

      expect(newState.length).to.equal(1);
      expect(newState[0].links!.length).to.equal(2);
      const updatedSection = newState[0].links![1];
      expect(updatedSection.links).to.not.equal(null);
      expect(updatedSection.links!.length).to.equal(2);
      expect(updatedSection.links![0].path).to.equal('/sub/link/path/1');
      expect(updatedSection.links![1].path).to.equal('/sub/link/path/2');
    });

    it('should only add links that did not exist before', () => {
      const newState = updateSection([
        {
          id: 'flat-section',
          title: flatSectionTitle1,
          flat: true,
          priority: 1,
          links: [
            { id: 'link-1', title: linkTitle1, path: '/path/1' },
            {
              id: 'sub-section-1',
              title: subSectionTitle1,
              links: [
                { id: 'sub-link-1', title: subLinkTitle1, path: '/sub/link/path/1' },
              ],
            },
          ],
        },
        { id: 'some-link', title: someLinkTitle, path: '/sub/link/path/2', priority: 2 },
      ],
        {
          id: 'sub-section-1',
          title: subSectionTitle1,
          links: [
            { id: 'sub-link-1', title: subLinkTitle1, path: '/sub/link/path/1' },
            { id: 'sub-link-2', title: subLinkTitle2, path: '/sub/link/path/2' },
            { id: 'sub-link-3', title: subLinkTitle3, path: '/sub/link/path/3' },
          ],
        },
      );

      expect(newState.length).to.equal(2);
      expect(newState[0].links!.length).to.equal(2);
      const updatedSection = newState[0].links![1];
      expect(updatedSection.links).to.not.equal(null);
      expect(updatedSection.links!.length).to.equal(2);
      expect(updatedSection.links![0].path).to.equal('/sub/link/path/1');
      expect(updatedSection.links![1].path).to.equal('/sub/link/path/3');
    });

    it('should add links that did not exist before to the right place based on their priority', () => {
      const newState = updateSection([
        {
          id: 'flat-section',
          title: flatSectionTitle1,
          flat: true,
          priority: 100,
          links: [
            { id: 'link-1', title: linkTitle1, path: '/path/1' },
            {
              id: 'sub-section-1',
              title: subSectionTitle1,
              links: [
                { id: 'sub-link-1', title: subLinkTitle1, path: '/sub/link/path/1', priority: 10 },
                { id: 'sub-link-2', title: subLinkTitle2, path: '/sub/link/path/2', priority: 20 },
              ],
            },
          ],
        },
        { id: 'some-link', title: someLinkTitle, path: '/sub/link/path/3', priority: 200 },
      ],
        {
          id: 'sub-section-1',
          title: subSectionTitle1,
          links: [
            { id: 'sub-link-1', title: subLinkTitle1, path: '/sub/link/path/1', priority: 10 },
            { id: 'sub-link-2', title: subLinkTitle2, path: '/sub/link/path/2', priority: 20 },
            { id: 'sub-link-3', title: subLinkTitle3, path: '/sub/link/path/3', priority: 30 },
            { id: 'sub-link-4', title: subLinkTitle4, path: '/sub/link/path/4', priority: 15 },
          ],
        },
      );

      expect(newState.length).to.equal(2);
      expect(newState[0].links!.length).to.equal(2);
      const updatedSection = newState[0].links![1];
      expect(updatedSection.links).to.not.equal(null);
      expect(updatedSection.links!.length).to.equal(3);
      expect(updatedSection.links![0].path).to.equal('/sub/link/path/1');
      expect(updatedSection.links![1].path).to.equal('/sub/link/path/4');
      expect(updatedSection.links![2].path).to.equal('/sub/link/path/2');
    });

    it('should not update the priority of section if section already existed', () => {
      const newState = updateSection([
        {
          id: 'section',
          title: sectionTitle1,
          priority: 10,
          links: [],
        },
      ], { id: 'section', title: otherTitle, links: [], priority: 20 } as any);

      expect(newState[0].priority).to.equal(10);
    });

    it('should keep the priority of section if section already existed and priority was not given', () => {
      const newState = updateSection([
        {
          id: 'section',
          title: sectionTitle1,
          priority: 10,
        },
      ], { id: 'section', title: otherTitle, links: [] });

      expect(newState[0].priority).to.equal(10);
    });

    it('should copy original icon state information on state transition', () => {
      const newState = updateSection([
        {
          id: 'section',
          title: sectionTitle1,
          priority: 10,
          icon: NavigationLinkIconGlyph.People,
        },
      ], { id: 'section', title: sectionTitle1, links: [] });

      expect('icon' in newState[0]).to.equal(true);
    });

    it('should not override icon state information on state transition', () => {
      const newState = updateSection([
        {
          id: 'section',
          title: sectionTitle1,
          priority: 10,
          icon: NavigationLinkIconGlyph.People,
        },
      ], { id: 'section', title: sectionTitle1, icon: NavigationLinkIconGlyph.Billing } as any);

      expect(newState[0].icon).to.equal(NavigationLinkIconGlyph.People);
    });
  });

  describe('section remove', () => {
    it('should disable the given section', () => {
      const section1 = {
        id: 'section1',
        title: { id: '1', defaultMessage: 'Section Title 1' },
      };
      const section2 = {
        id: 'section2',
        title: { id: '2', defaultMessage: 'Section Title 2' },
      };
      const state = [section1, section2];

      const updatedState = navigationSections(state, { type: NavigationAction.CHROME_NAVIGATION_SECTION_REMOVE, sectionId: 'section1' });

      expect(updatedState).to.deep.equal([{ ...section1, disabled: true }, section2]);
    });

    it('should not modify state if removal of non-existent section was requested', () => {
      const section1 = {
        id: 'section1',
        title: { id: '1', defaultMessage: 'Section Title 1' },
      };
      const section2 = {
        id: 'section2',
        title: { id: '2', defaultMessage: 'Section Title 2' },
      };
      const state = [section1, section2];

      const updatedState = navigationSections(state, { type: NavigationAction.CHROME_NAVIGATION_SECTION_REMOVE, sectionId: 'non-existent-section' });

      expect(updatedState).to.equal(state);
    });

    it('should remove the given section from the state even if it is not a top one', () => {
      const subSection1 = {
        id: 'section-1',
        title: { id: '1', defaultMessage: 'Section 1' },
        links: [{ id: 'link-1-1', title: linkTitle1, path: '/1/1' }],
      };
      const subSection2 = {
        id: 'section-2',
        title: { id: '2', defaultMessage: 'Section 2' },
        links: [{ id: 'link-2-1', title: linkTitle2, path: '/2/1' }],
      };
      const state = [
        {
          id: 'flat-section',
          title: { id: 'flat', defaultMessage: 'Flat Section' },
          links: [
            subSection1,
            subSection2,
          ],
        },
      ];

      const updatedState = navigationSections(state, { type: NavigationAction.CHROME_NAVIGATION_SECTION_REMOVE, sectionId: 'section-1' });

      expect(updatedState).to.deep.equal([{
        id: 'flat-section',
        title: { id: 'flat', defaultMessage: 'Flat Section' },
        links: [
          { ...subSection1, disabled: true },
          subSection2,
        ],
      }]);
    });
  });

  it('should be not update state for other action types', () => {
    const section1 = {
      id: 'section1',
      title: { id: '1', defaultMessage: 'Section Title 1' },
    };
    const state = [section1];

    const updatedState = navigationSections(state, { type: 'some-stub-action-type', sectionId: 'section1' } as any);

    expect(updatedState).to.equal(state);
  });

});
