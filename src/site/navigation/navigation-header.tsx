import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';

const Header = styled.div`
  font-size: 20px;
  padding-top: 16px;
  padding-bottom: 5px;
`;

export class NavigationHeader extends React.Component {
  public render() {
    return (
      <Header>
        <FormattedMessage
          id="chrome.navigation.header"
          description="Navigation header"
          defaultMessage="Administration"
        />
      </Header>
    );
  }
}
