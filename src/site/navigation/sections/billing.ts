import { defineMessages } from 'react-intl';

import { NavigationSectionId, NavItem } from 'common/navigation';

const messages = defineMessages({
  title: {
    defaultMessage: 'Subscriptions & Billing',
    description: '',
    id: 'chrome.nav-sections.billing.title',
  },
});

export const billingSection: NavItem = {
  id: NavigationSectionId.Subscriptions,
  title: messages.title,
  flat: true,
  links: [
  ],
};
