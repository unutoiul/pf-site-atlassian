import { defineMessages } from 'react-intl';

import { NavigationSectionId, NavItem } from 'common/navigation';

const messages = defineMessages({
  organizations: {
    id: 'chrome.nav-sections.organizations.title',
    defaultMessage: 'Organizations & security',
  },
});

export const organizationsSection: NavItem = {
  id: NavigationSectionId.Organizations,
  title: messages.organizations,
  links: undefined,
  flat: true,
};
