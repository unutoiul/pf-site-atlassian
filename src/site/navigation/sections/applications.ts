import { defineMessages } from 'react-intl';

import { NavigationSectionId, NavItem } from 'common/navigation';

const messages = defineMessages({
  title: {
    id: 'chrome.nav-sections.applications.title',
    description: 'Header for Applications configuration section (links to Jira/Confluence configuration pages).',
    defaultMessage: 'Application Settings',
  },
});

export const applicationsSection: NavItem = {
  id: NavigationSectionId.Applications,
  title: messages.title,
  flat: true,
  links: undefined,
};
