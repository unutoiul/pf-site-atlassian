import { defineMessages } from 'react-intl';

import { NavigationSectionId, NavItem } from 'common/navigation';

const messages = defineMessages({
  title: {
    defaultMessage: 'Site settings',
    description: 'The link in the navigation for the site settings page',
    id: 'chrome.nav-sections.site-settings.title',
  },
});

export const siteSettingsSection: NavItem = {
  title: messages.title,
  id: NavigationSectionId.SiteSettings,
  flat: true,
  links: undefined,
};
