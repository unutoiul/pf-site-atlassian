import { NavItem } from 'common/navigation';

import { applicationsSection } from './applications';
import { billingSection } from './billing';
import { organizationsSection } from './organizations';
import { siteSettingsSection } from './site-settings';
import { userManagementSection } from './user-management';

export const sections: NavItem[] = [
  { ...userManagementSection, priority: 100 },
  { ...siteSettingsSection, priority: 200 },
  { ...organizationsSection, priority: 400 },
  { ...billingSection, priority: 500 },
  { ...applicationsSection, priority: 600 },
];
