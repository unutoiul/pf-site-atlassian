import { defineMessages } from 'react-intl';

import { NavigationLinkIconGlyph, NavigationSectionId, NavItem } from 'common/navigation';

const messages = defineMessages({
  groups: {
    defaultMessage: 'Groups',
    description: '',
    id: 'chrome.nav-sections.um.groups',
  },
  users: {
    defaultMessage: 'Users',
    description: '',
    id: 'chrome.nav-sections.um.users',
  },
  title: {
    defaultMessage: 'User management',
    description: '',
    id: 'chrome.nav-sections.um.title',
  },
  serviceDesk: {
    defaultMessage: 'Jira Service Desk',
    description: '',
    id: 'chrome.nav-sections.um.service-desk',
  },
});

export const userManagementSection: NavItem = {
  title: messages.title,
  id: NavigationSectionId.UserManagement,
  flat: true,
  links: [
    {
      title: messages.serviceDesk,
      id: NavigationSectionId.JiraServiceDesk,
      priority: 40,
      icon: NavigationLinkIconGlyph.People,
      links: undefined,
    },
  ],
};
