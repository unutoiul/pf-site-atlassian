import { expect } from 'chai';
import { mount, ReactWrapper, shallow } from 'enzyme';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';
import { spy } from 'sinon';

import AkNavigation, { AkCreateDrawer } from '@atlaskit/navigation';

import { DrawerLink, NavigationLink } from 'common/navigation';

import { createApolloClient } from '../../apollo-client';
import { createStore } from '../../store';
import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import { NavigationCreateDrawerImpl } from './navigation-create-drawer';

const apolloClient = createApolloClient();

function mountWithNavigationContext(jsx: JSX.Element, initialEntries = [{ pathname: '/' }]): ReactWrapper<any, any> {
  return mount(
    <MemoryRouter initialEntries={initialEntries}>
      <Provider store={createStore()}>
        <ApolloProvider client={apolloClient}>
          <AkNavigation>
            {jsx}
          </AkNavigation>
        </ApolloProvider>
      </Provider>
    </MemoryRouter>, createMockIntlContext(),
  );
}

const testLinks: DrawerLink[] = [
  {
    path: '/test/1',
    title: { id: 'title.1', defaultMessage: 'title.1' },
  },
  {
    path: '/test/2',
    title: { id: 'title.2', defaultMessage: 'title.2' },
  },
];

describe('CreateDrawer', () => {
  describe('props', () => {
    it('should pass the isOpen prop as true to the <AkCreateDrawer>', () => {
      const wrapper = shallow(<NavigationCreateDrawerImpl links={testLinks} intl={createMockIntlProp()} />, createMockIntlContext());

      const createDrawerClosed = wrapper.find(AkCreateDrawer) as AkCreateDrawer;

      expect(createDrawerClosed.props().isOpen).to.not.equal(true);
    });

    it('should pass the isOpen prop as false to the <AkCreateDrawer>', () => {
      const wrapper = shallow(<NavigationCreateDrawerImpl links={testLinks} isOpen={true} intl={createMockIntlProp()} />, createMockIntlContext());

      const createDrawerOpen = wrapper.find(AkCreateDrawer) as AkCreateDrawer;

      expect(createDrawerOpen.props().isOpen).to.equal(true);
    });

    it('should pass correct parameters to <AkCreateDrawer>', () => {
      const onCloseCallback = () => 'stub-callback';
      const wrapper = shallow(<NavigationCreateDrawerImpl links={testLinks} onCloseDrawer={onCloseCallback} isOpen={true} intl={createMockIntlProp()} />, createMockIntlContext());

      const createDrawerOpen = wrapper.find(AkCreateDrawer) as AkCreateDrawer;

      expect(createDrawerOpen.props().onBackButton).to.equal(onCloseCallback);
    });
  });

  describe('rendering', () => {
    it('should render two <NavigationLink>s', () => {
      const wrapper = shallow(<NavigationCreateDrawerImpl links={testLinks} isOpen={true} intl={createMockIntlProp()} />, createMockIntlContext());
      const navigationLinks = wrapper.find(NavigationLink);

      expect(navigationLinks.length).to.equal(2);
    });
  });

  describe('behavior', () => {
    it('clicking the navigation items should close the drawer', () => {
      const closeDrawerSpy = spy();
      const wrapper = mountWithNavigationContext(<NavigationCreateDrawerImpl links={testLinks} isOpen={true} onCloseDrawer={closeDrawerSpy} intl={createMockIntlProp()} />);
      expect(closeDrawerSpy.callCount).to.equal(0);
      wrapper.find(NavigationLink).at(0).find('a').simulate('click');
      expect(closeDrawerSpy.callCount).to.equal(1);
      wrapper.find(NavigationLink).at(1).find('a').simulate('click');
      expect(closeDrawerSpy.callCount).to.equal(2);
    });
  });
});
