import { expect } from 'chai';

import { ChromeDrawerLinkHide, ChromeDrawerLinkShow, DrawerLink } from 'common/navigation';

import { NavigationAction } from '../../store/action-types';
import { navigationDrawerLinksReducer } from './navigation-drawer-links.reducer';

describe('NavigationDrawerLinks Reducer', () => {
  const drawerLink1: DrawerLink = { path: 'link-path-1', title: { id: 'link-title-1' } };
  const drawerLink2: DrawerLink = { path: 'link-path-2', title: { id: 'link-title-2' } };

  it('should add drawer link to state for show drawer link action', () => {
    const state = [];
    const action: ChromeDrawerLinkShow = { type: NavigationAction.CHROME_DRAWER_LINK_SHOW, drawerLink: drawerLink1 };

    const updatedState = navigationDrawerLinksReducer(state, action);

    expect(updatedState).to.deep.equal([drawerLink1]);
  });

  it('should not re-add drawer link to state for show drawer link action if it already exists', () => {
    const state = [drawerLink1];
    const action: ChromeDrawerLinkShow = { type: NavigationAction.CHROME_DRAWER_LINK_SHOW, drawerLink: drawerLink1 };

    const updatedState = navigationDrawerLinksReducer(state, action);

    expect(updatedState).to.deep.equal([drawerLink1]);
  });

  it('should remove drawer link from state for hide drawer link action', () => {
    const state = [drawerLink1, drawerLink2];
    const action: ChromeDrawerLinkHide = { type: NavigationAction.CHROME_DRAWER_LINK_HIDE, drawerLink: drawerLink1 };

    const updatedState = navigationDrawerLinksReducer(state, action);

    expect(updatedState).to.deep.equal([drawerLink2]);
  });

  it('should be a noop for any other action type', () => {
    const state = [drawerLink1];
    const action = { type: 'some-random-action-type', drawerLink: drawerLink1 };

    const updatedState = navigationDrawerLinksReducer(state, action as any);

    expect(updatedState).to.deep.equal([drawerLink1]);
  });
});
