import * as deepEqual from 'deep-equal';
import { parse, stringify } from 'query-string';
import * as React from 'react';
import { RouteComponentProps } from 'react-router';

import { analyticsClient } from 'common/analytics';

import { getRelativeURI } from '../../utilities/uri';

// tslint:disable-next-line:no-import-side-effect
import './iframe.css';

function createLocation({ pathname, search }) {
  return {
    pathname,
    query: parse(search),
  };
}

function setupTitleObservation(doc: Document, callback: (newTitle: string) => void): (() => void) | undefined {
  if (!(MutationObserver instanceof Function)) {
    return undefined;
  }
  if (!doc) {
    return undefined;
  }

  const target = doc.querySelector('head > title');
  if (!target) {
    // the frame must have not been loaded fully, so there's nothing to observe
    return undefined;
  }

  const observer = new MutationObserver((mutations: MutationRecord[]) => {
    mutations.forEach((mutation) => {
      if (mutation.target.textContent) {
        callback(mutation.target.textContent);
      }
    });
  });
  observer.observe(target, { subtree: true, characterData: true, childList: true });

  return () => observer.disconnect();
}

export interface IframePageProps extends RouteComponentProps<any> {
  isVisible: boolean;
}

export class IframePageImpl extends React.Component<IframePageProps> {
  public iframe: HTMLIFrameElement | undefined;
  public renderCalled = false;

  private titleObservationCancellation: (() => void) | undefined;

  public componentDidMount() {
    // listen for route changes in the iframe
    window.addEventListener('message', (message) => {
      if (message.origin !== window.location.origin) {
        return;
      }

      // Don't listen to iframe messages and reroute when the iframe isn't there, in case this is a new page that renders the iframe underneath it, hidden
      if (!this.props.isVisible) {
        return;
      }

      if (message.data && message.data.type === 'route-changed') {
        const { pathname, search, title } = message.data.payload;
        this.iframeLocationUpdated(pathname, search, title);
      }
    });
    // propagate "click" events to the iframe
    document.addEventListener('click', () => {
      if (!this.iframeDocument) {
        return;
      }

      try {
        if (this.iframe) {
          this.iframeDocument.dispatchEvent(new MouseEvent('click', {
            view: this.iframe.contentWindow,
            bubbles: true,
            cancelable: true,
          }));
        }
      } catch (_e) {
        // some browsers (namely IE11 and Safari) do not like the MouseEvent, so we fallback to an old, deprecated approach
        try {
          const clickEvent = this.iframeDocument.createEvent('Event');
          clickEvent.initEvent('click', true, true);
          this.iframeDocument.dispatchEvent(clickEvent);
        } catch (_e2) {
          // well, no luck for us, we know about this problem, so let's not propagate it to NR
        }
      }
    });
  }

  public componentWillReceiveProps(nextProps: IframePageProps) {
    if (this.iframe) {
      if (nextProps.isVisible) {
        this.iframe.classList.remove('hidden');
      } else {
        this.iframe.classList.add('hidden');
      }
    }

    if (this.props.location.key === nextProps.location.key) {
      // route didn't change, ignore
      return;
    }

    if (nextProps.location.state && nextProps.location.state.iframeInitiatedRouteChange) {
      // do not post this back to iframe, since location update has originated in the iframe itself
      return;
    }

    if (!nextProps.isVisible) {
      return;
    }

    // send message that we are navigating somewhere
    const url = getRelativeURI(nextProps.location).toString();

    if (this.iframe && this.iframe.contentWindow) {
      this.iframe.contentWindow.postMessage({ type: 'navigate', payload: { url, replace: true } }, window.location.origin);
    }
  }

  // make sure render() is never called again
  public shouldComponentUpdate() {
    return false;
  }

  public componentWillUnmount() {
    if (this.titleObservationCancellation) {
      this.titleObservationCancellation();
    }
  }

  public render() {
    this.checkRenderCalled();
    const { location, isVisible } = this.props;

    const src = getRelativeURI(location)
      .addSearch('chrome', 'false')
      .addSearch('react', 'false')
      .href();

    return (
      // tslint:disable-next-line:react-iframe-missing-sandbox
      <iframe
        src={src}
        // The name attribute is required for breaking the back button cache on the iframe.
        name={Date.now().toString()}
        className={`iframe ${isVisible ? '' : 'hidden'}`}
        ref={this.setIframeRef}
        onLoad={this.iframeLoaded}
      />
    );
  }

  private get iframeDocument(): Document | null | undefined {
    try {
      return this.iframe && this.iframe.contentDocument;
    } catch (error) {
      // most likely a SecurityError when we're trying to access document of a cross-origin frame
      if (error.name !== 'SecurityError') {
        analyticsClient.onError(error);
      }

      return null;
    }
  }

  private iframeLoaded = () => {
    if (this.iframeDocument) {
      this.titleObservationCancellation = setupTitleObservation(this.iframeDocument, this.updateTitle);
    }
  }

  private iframeLocationUpdated(pathname: string, search: string, title: string) {
    this.updateToMatchIframeLocation(pathname, search);
    if (this.titleObservationCancellation) {
      // we are observing title changes; no need to query the thing directly
      return;
    }

    this.updateTitle(title || this.getIframeDocumentTitle());
  }

  private getIframeDocumentTitle = (): string => {
    if (!this.iframeDocument) {
      return '';
    }

    return this.iframeDocument.title;
  }

  // Sanity check to make sure we never render() the iframe twice
  private checkRenderCalled() {
    if (this.renderCalled) {
      // tslint:disable-next-line:no-console
      console.error('Iframe is getting rendered more than once. This should never happen! Immediately investigate!');
    }

    this.renderCalled = true;
  }

  private setIframeRef = (iframe: HTMLIFrameElement) => this.iframe = iframe;

  private updateToMatchIframeLocation(pathname: string, search: string) {
    const nextLocation = createLocation({
      pathname,
      search,
    });

    // remove iframe specific query params that we don't want
    delete nextLocation.query.react;
    delete nextLocation.query.chrome;

    const currentLocation = this.props.location;
    const isQueryMatch = JSON.stringify(parse(currentLocation.search)) === JSON.stringify(nextLocation.query);
    if (currentLocation.pathname === nextLocation.pathname && isQueryMatch) {
      return;
    }

    this.props.history.replace({
      pathname: nextLocation.pathname,
      search: Object.keys(nextLocation.query).length ? '?' + stringify(nextLocation.query) : '',
      state: {
        iframeInitiatedRouteChange: true,
      },
    });
  }

  private updateTitle = (newTitle: string) => {
    if (!newTitle) {
      return;
    }

    if (document.title !== newTitle) {
      document.title = newTitle;
    }
  }
}

interface IframePageState {
  update: boolean;
  wasVisible: boolean;
}

// tslint:disable-next-line:max-classes-per-file
export class IframePage extends React.Component<IframePageProps, IframePageState> {
  public timeoutId: number | undefined;
  public state: IframePageState = {
    update: false,
    wasVisible: this.props.isVisible,
  };

  public componentWillReceiveProps(nextProps) {
    if (deepEqual(this.props, nextProps)) {
      return;
    }

    // but we should only update the iframe location if it is visible.
    // Unfortunately, the visibility and its location can change in
    // different change detection cycles.
    // We debounce prop updates here so that the actual `IframePage`
    // component receives props updates all at once
    clearTimeout(this.timeoutId!);
    this.timeoutId = window.setTimeout(() => {
      this.setState({ update: true });
    }, 10);
  }

  public componentDidUpdate(oldProps) {
    if (this.state.update) {
      this.setState({ update: false });
    }

    if (!this.state.wasVisible) {
      const wasVisible = oldProps.isVisible || this.props.isVisible;
      this.setState({ wasVisible });
    }
  }

  public shouldComponentUpdate(_nextProps, nextState: IframePageState) {
    return nextState.update;
  }

  public render() {
    return (this.props.isVisible || this.state.wasVisible)
      ? <IframePageImpl {...this.props} />
      : null;
  }
}
