import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { sandbox as sinonSandbox, SinonFakeTimers, SinonSandbox, SinonSpy, spy } from 'sinon';

import { IframePage, IframePageImpl, IframePageProps } from './iframe-page';

describe('IframePage', () => {
  let clock: SinonFakeTimers;
  let sandbox: SinonSandbox;
  let pseudoHistory: { replace: SinonSpy };
  let pseudoLocation: { pathname: | string, search: string, hash: string, key: 'testKey' | string, state: any };
  let originalDocumentTitle;

  const getDefaultProps = (props: Partial<IframePageProps> = {}) => {
    if (props.isVisible === undefined) {
      props.isVisible = true;
    }
    if (props.history === undefined) {
      props.history = pseudoHistory as any;
    }
    if (props.location === undefined) {
      props.location = pseudoLocation as any;
    }

    return props;
  };

  const createWrapper = (props: Partial<IframePageProps> = {}) => {
    return shallow<IframePage>(<IframePage {...(getDefaultProps(props) as any)} />);
  };

  beforeEach(() => {
    sandbox = sinonSandbox.create();
    clock = sandbox.useFakeTimers();
    pseudoHistory = { replace: spy() };
    pseudoLocation = { pathname: '', search: '', hash: '', key: '', state: {} };

    originalDocumentTitle = Object.getOwnPropertyDescriptor(Document.prototype, 'title');
    Object.defineProperty(Document.prototype, 'title', {
      enumerable: false,
      configurable: true,
      writable: true,
      value: document.title,
    });
  });

  afterEach(() => {
    sandbox.restore();

    if (originalDocumentTitle) {
      // Some versions of older browsers don't define this property.
      // In that case, the property we defined is good enough.
      Object.defineProperty(Document.prototype, 'title', originalDocumentTitle);
    }
  });

  describe('debounce wrapper', () => {
    it('should debounce component updates', () => {
      const wrapper = createWrapper();

      expect(
        wrapper.instance().shouldComponentUpdate({}, { update: true, wasVisible: false }),
      ).to.equal(true);
    });

    describe('componentWillReceiveProps', () => {
      it('should not update state if props are same', () => {
        const wrapper = createWrapper();
        const component = wrapper.instance();

        wrapper.setProps(component.props);

        expect(component.state.update).to.equal(false);
      });

      it('should not update state if props are same even after time passes', () => {
        const wrapper = createWrapper();
        const component = wrapper.instance();

        wrapper.setProps(component.props);
        clock.tick(20);

        expect(component.state.update).to.equal(false);
      });

      it('should wait before updating state after props changed', () => {
        const wrapper = createWrapper();

        wrapper.setProps({ location: null as any });
        expect(wrapper.find(IframePageImpl).props().location).not.to.be.equal(null);

        clock.tick(20);

        expect(wrapper.find(IframePageImpl).props().location).to.be.equal(null);
      });
    });
  });

  describe('iframe holder', () => {
    describe('"route-changed" message', () => {
      let postMessage: (message: { origin: string, data: { type: 'route-changed', payload: { pathname: string, search: string, title?: string } } }) => void;

      beforeEach(() => {
        // Normally there are no assertions in `beforeEach` hooks, but
        // the following can be viewed as setup code, which is most
        // suitable for this hook.
        const stub = sandbox.stub<Window>(window, 'addEventListener');
        createWrapper().dive();

        expect(stub.calledOnce).to.equal(true, `Expected "window.addEventListener" to have been called once, but it was called ${stub.callCount} times`);

        const eventType = stub.getCalls()[0].args[0];
        expect(eventType).to.equal('message', `Expected the component to subscribe to "message" event, but it subscribed to "${eventType}" instead`);

        postMessage = stub.getCalls()[0].args[1];
        expect(postMessage).to.be.a('function');
      });

      it('should replace history based on provided args', () => {
        postMessage({
          origin: window.location.origin,
          data: {
            type: 'route-changed',
            payload: {
              pathname: 'pathname',
              search: '',
            },
          },
        });

        expect(pseudoHistory.replace.callCount).to.equal(1);

        const newLocation = pseudoHistory.replace.getCalls()[0].args[0];
        expect(newLocation).to.not.equal(null);
        expect(newLocation.pathname).to.equal('pathname');
        expect(newLocation.search).to.equal('');
      });

      it('should remove "chrome" and "react" query parameters', () => {
        postMessage({
          origin: window.location.origin,
          data: {
            type: 'route-changed',
            payload: {
              pathname: 'pathname',
              search: '?a=1&chrome=false&react=false',
            },
          },
        });

        expect(pseudoHistory.replace.callCount).to.equal(1);

        const newLocation = pseudoHistory.replace.getCalls()[0].args[0];
        expect(newLocation).to.not.equal(null);
        expect(newLocation.search).to.equal('?a=1');
      });

      it('should update document title if a non-empty string is provided', () => {
        postMessage({
          origin: window.location.origin,
          data: {
            type: 'route-changed',
            payload: {
              pathname: '',
              search: '',
              title: 'Test Title',
            },
          },
        });

        expect(document.title).to.equal('Test Title');
      });

      it('should not update document title if an empty string is provided', () => {
        document.title = 'Some Document Title';
        postMessage({
          origin: window.location.origin,
          data: {
            type: 'route-changed',
            payload: {
              pathname: '',
              search: '',
              title: '',
            },
          },
        });

        expect(document.title).to.equal('Some Document Title');
      });

      it('should not fail on unknown messages', () => {
        expect(() => postMessage({ origin: window.location.origin, data: null } as any)).not.to.throw();
      });
    });

    it('should propagate document clicks back to the iframe', () => {
      const subscriptionSpy = sandbox.spy<Document>(document, 'addEventListener');

      const instance = createWrapper().dive().instance();

      const iframeDocumentDispatchEventSpy = sandbox.spy();
      const iframeWindow = window; // cannot mock this one, as `MouseEvent` ctor would choke on non-Window objects
      (instance as any).iframe = {
        contentDocument: {
          dispatchEvent: iframeDocumentDispatchEventSpy,
        },
        contentWindow: iframeWindow,
      };

      expect(subscriptionSpy.callCount).to.equal(1);
      const args = subscriptionSpy.getCalls()[0].args;
      expect(args[0]).to.equal('click');
      expect(args[1] instanceof Function).to.equal(true);

      expect(iframeDocumentDispatchEventSpy.callCount).to.equal(0);
      // invoke callback
      args[1]();
      expect(iframeDocumentDispatchEventSpy.callCount).to.equal(1);
      const eventArg: MouseEvent = iframeDocumentDispatchEventSpy.getCalls()[0].args[0];
      expect(eventArg instanceof MouseEvent).to.equal(true);
      expect(eventArg.type).to.equal('click');
      expect(eventArg.view).to.equal(iframeWindow);
    });

    describe('route updates', () => {
      it('should not replace history if new location matches current location', () => {
        const stub = sandbox.stub<Window>(window, 'addEventListener');
        const customLocation = {
          ...pseudoLocation,
          pathname: 'stub-path-name',
          search: '?b=2&a=1',
        };
        createWrapper({ location: customLocation }).dive();
        const postMessage = stub.getCalls()[0].args[1];

        postMessage({
          origin: window.location.origin,
          data: {
            type: 'route-changed',
            payload: {
              pathname: 'stub-path-name',
              search: '?a=1&b=2',
            },
          },
        });

        expect(pseudoHistory.replace.callCount).to.equal(0);
      });

      it('should not create iframe if not visible', () => {
        expect(() => createWrapper({ isVisible: false }).dive()).to.throw();
      });

      it('should not replace history if the iframe is not visible', () => {
        const stub = sandbox.stub<Window>(window, 'addEventListener');
        const customLocation = {
          ...pseudoLocation,
          pathname: 'stub-path-name',
        };
        const wrapper = createWrapper({ isVisible: true, location: customLocation });
        wrapper.setProps({ isVisible: false });
        clock.tick(20);
        wrapper.dive();
        const postMessage = stub.getCalls()[0].args[1];

        postMessage({
          origin: window.location.origin,
          data: {
            type: 'route-changed',
            payload: {
              pathname: 'new-path-name',
            },
          },
        });

        expect(pseudoHistory.replace.callCount).to.equal(0);
      });

      it('should replace history if new location query params do not match current location query params', () => {
        const stub = sandbox.stub<Window>(window, 'addEventListener');
        const customLocation = {
          ...pseudoLocation,
          pathname: 'stub-path-name',
          search: '?b=2',
        };
        createWrapper({ location: customLocation }).dive();
        const postMessage = stub.getCalls()[0].args[1];

        postMessage({
          origin: window.location.origin,
          data: {
            type: 'route-changed',
            payload: {
              pathname: 'stub-path-name',
              search: '?a=1&b=2',
            },
          },
        });

        expect(pseudoHistory.replace.callCount).to.equal(1);
      });
    });

    describe('componentWillReceiveProps', () => {
      let pseudoIframe: {
        classList: {
          add: SinonSpy,
          remove: SinonSpy,
        },
        contentWindow: {
          postMessage: SinonSpy,
        },
      };

      beforeEach(() => {
        pseudoIframe = {
          classList: {
            add: spy(),
            remove: spy(),
          },
          contentWindow: {
            postMessage: spy(),
          },
        };
      });

      it('should remove "hidden" CSS class when becomes visible', () => {
        pseudoLocation.key = 'someKey';
        const wrapper = shallow(<IframePageImpl {...getDefaultProps({ isVisible: false }) as any} />);

        (wrapper.instance() as any).iframe = pseudoIframe;

        wrapper.setProps({ isVisible: true });
        expect(pseudoIframe.classList.remove.callCount).to.equal(1);
        expect(pseudoIframe.classList.remove.getCalls()[0].args[0]).to.equal('hidden');
        expect(pseudoIframe.classList.add.callCount).to.equal(0);
      });

      it('should render the iframe with a unique `name` property', () => {
        const wrapper1 = createWrapper().dive();
        const wrapper1Name = wrapper1.find('iframe').prop('name');
        clock.tick(10);
        const wrapper2 = createWrapper().dive();
        const wrapper2Name = wrapper2.find('iframe').prop('name');
        expect(wrapper1Name).to.not.equal(wrapper2Name);
      });

      it('should add "hidden" CSS class when becomes invisible', () => {
        pseudoLocation.key = 'someKey';
        const wrapper = createWrapper().dive();

        (wrapper.instance() as any).iframe = pseudoIframe;

        wrapper.setProps({ isVisible: false });
        expect(pseudoIframe.classList.add.callCount).to.equal(1);
        expect(pseudoIframe.classList.add.getCalls()[0].args[0]).to.equal('hidden');
        expect(pseudoIframe.classList.remove.callCount).to.equal(0);
      });

      it('should always call postMessage with replace: true', () => {
        pseudoLocation.key = 'someKey';
        const wrapper = createWrapper().dive();

        (wrapper.instance() as any).iframe = pseudoIframe;
        wrapper.setProps({ location: { pathname: 'path', search: '?a=1&b=2', hash: '#hash', key: 'anykey' } });

        expect(pseudoIframe.contentWindow.postMessage.callCount).to.equal(1);
        const args = pseudoIframe.contentWindow.postMessage.getCalls()[0].args;
        expect(args[0].payload).to.deep.equal({ url: 'path?a=1&b=2#hash', replace: true });
      });

      it('should post a "navigate" message to iframe if it is visible and the location changes', () => {
        pseudoLocation.key = 'someKey';
        const wrapper = createWrapper().dive();

        (wrapper.instance() as any).iframe = pseudoIframe;
        wrapper.setProps({ location: { pathname: 'path', search: '?a=1&b=2', hash: '#hash', key: 'differentKey' } });

        expect(pseudoIframe.contentWindow.postMessage.callCount).to.equal(1);
        const args = pseudoIframe.contentWindow.postMessage.getCalls()[0].args;
        expect(args[0].type).to.equal('navigate');
        expect(args[0].payload).to.deep.equal({ url: 'path?a=1&b=2#hash', replace: true });
        expect(args[1]).to.equal(window.location.origin);
      });

      it('should not post a "navigate" message if location key is the same', () => {
        pseudoLocation.key = 'someKey';
        const wrapper = createWrapper().dive();

        (wrapper.instance() as any).iframe = pseudoIframe;
        wrapper.setProps({ location: { ...pseudoLocation, key: 'someKey' } });

        expect(pseudoIframe.contentWindow.postMessage.callCount).to.equal(0);
      });

      it('should not post a "navigate" message if iframe is not visible', () => {
        pseudoLocation.key = 'someKey';
        const wrapper = createWrapper().dive();

        (wrapper.instance() as any).iframe = pseudoIframe;
        wrapper.setProps({ location: { ...pseudoLocation, key: 'differentKey' }, isVisible: false });

        expect(pseudoIframe.contentWindow.postMessage.callCount).to.equal(0);
      });
    });

    it('should log an error to the console if the component is rendered more than once', () => {
      const consoleErrorStub = sandbox.stub(console, 'error');
      pseudoLocation.key = 'someKey';
      const wrapper = createWrapper().dive();

      expect(consoleErrorStub.callCount).to.equal(0);
      wrapper.instance().render();

      expect(consoleErrorStub.callCount).to.equal(1);
      expect(consoleErrorStub.getCalls()[0].args[0]).to.match(/Iframe is getting rendered more than once/);
    });
  });
});
