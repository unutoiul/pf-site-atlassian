import { FeatureFlagProps, withSiteFeatureFlag } from 'common/feature-flags';

import { trustedUsersFeatureFlagKey } from './flag-keys';

export interface TrustedUsersFeatureFlag {
  trustedUsersFeatureFlag: FeatureFlagProps;
}

export function withTrustedUsersFeatureFlag<TOwnProps>(Component: React.ComponentType<TOwnProps & TrustedUsersFeatureFlag>): React.ComponentClass<TOwnProps> {
  return withSiteFeatureFlag<TOwnProps, TrustedUsersFeatureFlag>({
    flagKey: trustedUsersFeatureFlagKey,
    defaultValue: false,
    name: 'trustedUsersFeatureFlag',
  })(Component);
}
