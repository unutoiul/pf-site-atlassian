import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';

import { MockSiteFeatureFlagApolloClient } from 'common/feature-flags/test-utils';

import { trustedUsersCommunicationFeatureFlagKey } from './flag-keys';
import { withTrustedUsersCommunicationFeatureFlag } from './with-trusted-users-communication-feature-flag';

describe('withTrustedUsersCommunicationFeatureFlag', () => {
  const TestComponent = ({ trustedUsersCommunicationFeatureFlag }) => <div>{JSON.stringify(trustedUsersCommunicationFeatureFlag)}</div>;
  const TestWrappedComponent = withTrustedUsersCommunicationFeatureFlag<{}>(TestComponent);

  const mountWrappedComponentWithTrustedUsersCommunicationFeatureFlag = () => {
    return mount(
      <MockSiteFeatureFlagApolloClient
        flagKey={trustedUsersCommunicationFeatureFlagKey}
        flagValue={true}
      >
        <TestWrappedComponent />
      </MockSiteFeatureFlagApolloClient>,
    );
  };

  it('should pass trusted users communication feature flag result to component', () => {
    const wrapper = mountWrappedComponentWithTrustedUsersCommunicationFeatureFlag();

    const testComponent = wrapper.find(TestComponent);
    expect(testComponent.props()).to.deep.equal({
      trustedUsersCommunicationFeatureFlag: {
        isLoading: false,
        value: true,
        defaultValue: false,
      },
    });
  });
});
