import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';

import { MockSiteFeatureFlagApolloClient } from 'common/feature-flags/test-utils';

import { siteAdminMigrationFeatureFlagKey } from './flag-keys';
import { withSiteAdminMigrationFeatureFlag } from './with-site-admin-migration-feature-flag';

describe('withSiteAdminMigrationFeatureFlag', () => {
  const TestComponent = ({ siteAdminMigrationFeatureFlag }) => <div>{JSON.stringify(siteAdminMigrationFeatureFlag)}</div>;
  const TestWrappedComponent = withSiteAdminMigrationFeatureFlag<{}>(TestComponent);

  const mountWrappedComponentWithSiteAdminMigrationFeatureFlag = () => {
    return mount(
      <MockSiteFeatureFlagApolloClient
        flagKey={siteAdminMigrationFeatureFlagKey}
        flagValue={true}
      >
        <TestWrappedComponent />
      </MockSiteFeatureFlagApolloClient>,
    );
  };

  it('should pass site admin migration feature flag result to component', () => {
    const wrapper = mountWrappedComponentWithSiteAdminMigrationFeatureFlag();

    const testComponent = wrapper.find(TestComponent);
    expect(testComponent.props()).to.deep.equal({
      siteAdminMigrationFeatureFlag: {
        isLoading: false,
        value: true,
        defaultValue: false,
      },
    });
  });
});
