import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';

import { MockSiteFeatureFlagApolloClient } from 'common/feature-flags/test-utils';

import { userConnectedAppsPageFeatureFlagKey } from './flag-keys';
import { withUserConnectedAppsPageFeatureFlag } from './with-user-connected-apps-page-feature-flag';

describe('withUserConnectedAppsPageFeatureFlag', () => {
  const TestComponent = ({ userConnectedAppsFeatureFlag }) => <div>{JSON.stringify(userConnectedAppsFeatureFlag)}</div>;
  const TestWrappedComponent = withUserConnectedAppsPageFeatureFlag<{}>(TestComponent);

  const mountWrappedComponentWithAppSwitcherFeatureFlag = () => {
    return mount(
      <MockSiteFeatureFlagApolloClient
        flagKey={userConnectedAppsPageFeatureFlagKey}
        flagValue={true}
      >
        <TestWrappedComponent />
      </MockSiteFeatureFlagApolloClient>,
    );
  };

  it('should pass user connected apps page feature flag result to component', () => {
    const wrapper = mountWrappedComponentWithAppSwitcherFeatureFlag();

    const testComponent = wrapper.find(TestComponent);
    expect(testComponent.props()).to.deep.equal({
      userConnectedAppsFeatureFlag: {
        isLoading: false,
        value: true,
        defaultValue: false,
      },
    });
  });
});
