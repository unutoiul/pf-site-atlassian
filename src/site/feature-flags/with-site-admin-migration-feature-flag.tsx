import { FeatureFlagProps, withSiteFeatureFlag } from 'common/feature-flags';

import { siteAdminMigrationFeatureFlagKey } from './flag-keys';

export interface SiteAdminMigrationFeatureFlag {
  siteAdminMigrationFeatureFlag: FeatureFlagProps;
}

export function withSiteAdminMigrationFeatureFlag<TOwnProps>(Component: React.ComponentType<TOwnProps & SiteAdminMigrationFeatureFlag>): React.ComponentClass<TOwnProps> {
  return withSiteFeatureFlag<TOwnProps, SiteAdminMigrationFeatureFlag>({
    flagKey: siteAdminMigrationFeatureFlagKey,
    defaultValue: false,
    name: 'siteAdminMigrationFeatureFlag',
  })(Component);
}
