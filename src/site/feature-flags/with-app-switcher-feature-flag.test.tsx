import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';

import { MockSiteFeatureFlagApolloClient } from 'common/feature-flags/test-utils';

import { appSwitcherFeatureFlagKey } from './flag-keys';
import { withAppSwitcherFeatureFlag } from './with-app-switcher-feature-flag';

describe('withAppSwitcherFeatureFlag', () => {
  const TestComponent = ({ appSwitcherFeatureFlag }) => <div>{JSON.stringify(appSwitcherFeatureFlag)}</div>;
  const TestWrappedComponent = withAppSwitcherFeatureFlag<{}>(TestComponent);

  const mountWrappedComponentWithAppSwitcherFeatureFlag = () => {
    return mount(
      <MockSiteFeatureFlagApolloClient
        flagKey={appSwitcherFeatureFlagKey}
        flagValue={true}
      >
        <TestWrappedComponent />
      </MockSiteFeatureFlagApolloClient>,
    );
  };

  it('should pass app switcher feature flag result to component', () => {
    const wrapper = mountWrappedComponentWithAppSwitcherFeatureFlag();

    const testComponent = wrapper.find(TestComponent);
    expect(testComponent.props()).to.deep.equal({
      appSwitcherFeatureFlag: {
        isLoading: false,
        value: true,
        defaultValue: false,
      },
    });
  });
});
