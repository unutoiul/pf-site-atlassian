export const appSwitcherFeatureFlagKey = 'admin.app-switcher.grid.icon';
export const csatEvalCohortFlagKey = 'admin.eval.csat.survey';
export const csatEvalCohortOnboardingKey = 'eval.csat.survey';
export const siteAdminMigrationFeatureFlagKey = 'site.admin.page.migration';
export const siteUrlPagePerSiteFeatureFlagKey = 'site-url.page';
export const siteUrlPagePerUserFeatureFlagKey = 'site-url.page.per-user';
export const trustedUsersFeatureFlagKey = 'admin.trusted.users';
export const trustedUsersCommunicationFeatureFlagKey = 'admin.trusted.users.communication';
export const userConnectedAppsPageFeatureFlagKey = 'admin.user.connected.apps.page';
