import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';

import { MockSiteFeatureFlagApolloClient } from 'common/feature-flags/test-utils';

import { trustedUsersFeatureFlagKey } from './flag-keys';
import { withTrustedUsersFeatureFlag } from './with-trusted-users-feature-flag';

describe('withTrustedUsersFeatureFlag', () => {
  const TestComponent = ({ trustedUsersFeatureFlag }) => <div>{JSON.stringify(trustedUsersFeatureFlag)}</div>;
  const TestWrappedComponent = withTrustedUsersFeatureFlag<{}>(TestComponent);

  const mountWrappedComponentWithTrustedUsersFeatureFlag = () => {
    return mount(
      <MockSiteFeatureFlagApolloClient
        flagKey={trustedUsersFeatureFlagKey}
        flagValue={true}
      >
        <TestWrappedComponent />
      </MockSiteFeatureFlagApolloClient>,
    );
  };

  it('should pass trusted users feature flag result to component', () => {
    const wrapper = mountWrappedComponentWithTrustedUsersFeatureFlag();

    const testComponent = wrapper.find(TestComponent);
    expect(testComponent.props()).to.deep.equal({
      trustedUsersFeatureFlag: {
        isLoading: false,
        value: true,
        defaultValue: false,
      },
    });
  });
});
