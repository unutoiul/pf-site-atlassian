import { FeatureFlagProps, withSiteFeatureFlag } from 'common/feature-flags';

import { userConnectedAppsPageFeatureFlagKey } from './flag-keys';

export interface UserConnectedAppsPageFeatureFlag {
  userConnectedAppsFeatureFlag: FeatureFlagProps;
}

export function withUserConnectedAppsPageFeatureFlag<TOwnProps>(
  Component: React.ComponentType<TOwnProps & UserConnectedAppsPageFeatureFlag>,
): React.ComponentClass<TOwnProps> {
  return withSiteFeatureFlag<TOwnProps, UserConnectedAppsPageFeatureFlag>({
    flagKey: userConnectedAppsPageFeatureFlagKey,
    defaultValue: false,
    name: 'userConnectedAppsFeatureFlag',
  })(Component);
}
