import { FeatureFlagProps, withSiteFeatureFlag } from 'common/feature-flags';

import { trustedUsersCommunicationFeatureFlagKey } from './flag-keys';

export interface TrustedUsersCommunicationFeatureFlag {
  trustedUsersCommunicationFeatureFlag: FeatureFlagProps;
}

export function withTrustedUsersCommunicationFeatureFlag<TOwnProps>(Component: React.ComponentType<TOwnProps & TrustedUsersCommunicationFeatureFlag>): React.ComponentClass<TOwnProps> {
  return withSiteFeatureFlag<TOwnProps, TrustedUsersCommunicationFeatureFlag>({
    flagKey: trustedUsersCommunicationFeatureFlagKey,
    defaultValue: false,
    name: 'trustedUsersCommunicationFeatureFlag',
  })(Component);
}
