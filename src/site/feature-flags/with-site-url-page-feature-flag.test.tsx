import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';

import { configureApolloClientWithSiteFeatureFlag, configureApolloClientWithUserFeatureFlag } from 'common/feature-flags/test-utils';

import { createApolloClient } from '../../apollo-client';
import { siteUrlPagePerSiteFeatureFlagKey, siteUrlPagePerUserFeatureFlagKey } from './flag-keys';
import { withUrlPageFeatureFlag } from './with-site-url-page-feature-flag';

describe('withUrlPageFeatureFlag', () => {
  const TestComponent = ({ urlPageFeatureFlag }) => <div>{JSON.stringify(urlPageFeatureFlag)}</div>;
  const TestWrappedComponent = withUrlPageFeatureFlag<{}>(TestComponent);

  const mountWithFlags = ({ isFlagEnabledForSite = false, isFlagEnabledForUser = false } = {}) => {
    let client = createApolloClient();
    client = configureApolloClientWithSiteFeatureFlag({
      client,
      flagKey: siteUrlPagePerSiteFeatureFlagKey,
      flagValue: isFlagEnabledForSite,
    });
    client = configureApolloClientWithUserFeatureFlag({
      client,
      flagKey: siteUrlPagePerUserFeatureFlagKey,
      flagValue: isFlagEnabledForUser,
    });

    return mount(
      <ApolloProvider
        client={client}
      >
        <TestWrappedComponent />
      </ApolloProvider>,
    );
  };

  it('should pass site URL page feature flag result to component', () => {
    const wrapper = mountWithFlags({ isFlagEnabledForSite: true });

    const testComponent = wrapper.find(TestComponent);
    expect(testComponent.props()).to.deep.equal({
      urlPageFeatureFlag: {
        isLoading: false,
        value: true,
      },
    });
  });

  it('should enable feature if flag is enabled for site', () => {
    const wrapper = mountWithFlags({ isFlagEnabledForSite: true, isFlagEnabledForUser: false });

    const testComponent = wrapper.find(TestComponent);
    expect(testComponent.props()).to.deep.equal({
      urlPageFeatureFlag: {
        isLoading: false,
        value: true,
      },
    });
  });

  it('should enable feature if flag is enabled for user', () => {
    const wrapper = mountWithFlags({ isFlagEnabledForSite: false, isFlagEnabledForUser: true });

    const testComponent = wrapper.find(TestComponent);
    expect(testComponent.props()).to.deep.equal({
      urlPageFeatureFlag: {
        isLoading: false,
        value: true,
      },
    });
  });

  it('should disable feature if flag is not enabled for site or user', () => {
    const wrapper = mountWithFlags({ isFlagEnabledForSite: false, isFlagEnabledForUser: false });

    const testComponent = wrapper.find(TestComponent);
    expect(testComponent.props()).to.deep.equal({
      urlPageFeatureFlag: {
        isLoading: false,
        value: false,
      },
    });
  });
});
