import { FeatureFlagProps, withSiteFeatureFlag } from 'common/feature-flags';

import { appSwitcherFeatureFlagKey } from './flag-keys';

export interface AppSwitcherFeatureFlag {
  appSwitcherFeatureFlag: FeatureFlagProps;
}

export function withAppSwitcherFeatureFlag<TOwnProps>(Component: React.ComponentType<TOwnProps & AppSwitcherFeatureFlag>): React.ComponentClass<TOwnProps> {
  return withSiteFeatureFlag<TOwnProps, AppSwitcherFeatureFlag>({
    flagKey: appSwitcherFeatureFlagKey,
    defaultValue: false,
    name: 'appSwitcherFeatureFlag',
  })(Component);
}
