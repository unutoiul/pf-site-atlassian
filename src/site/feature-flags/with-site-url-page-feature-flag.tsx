import * as React from 'react';

import { FeatureFlagProps, withSiteFeatureFlag, withUserFeatureFlag } from 'common/feature-flags';

import { siteUrlPagePerSiteFeatureFlagKey as perSiteFlagKey, siteUrlPagePerUserFeatureFlagKey as perUserFlagKey } from './flag-keys';

export interface UrlPageFeatureFlag {
  urlPageFeatureFlag: FeatureFlagProps;
}

export interface UrlPageFeatureFlagBySite {
  urlPageFeatureFlagBySite: FeatureFlagProps;
}

export interface UrlPageFeatureFlagByUser {
  urlPageFeatureFlagByUser: FeatureFlagProps;
}

type CombinerProps = any & UrlPageFeatureFlagBySite & UrlPageFeatureFlagByUser;

/**
 * Takes a component that needs a `urlPageFeatureFlagBySite` prop and returns
 * an equivalent component that gets the value from app state, therefore no
 * longer expecting it as a prop.
 *
 * @param {Object} Component Component to be wrapped
 * @returns {Object} The wrapped component
 */
function withUrlPageFeatureFlagBySite<TOwnProps>(Component: React.ComponentType<TOwnProps & UrlPageFeatureFlagBySite>): React.ComponentClass<TOwnProps> {
  return withSiteFeatureFlag<TOwnProps, UrlPageFeatureFlagBySite>({
    flagKey: perSiteFlagKey,
    defaultValue: false,
    name: 'urlPageFeatureFlagBySite',
  })(Component);
}

/**
 * Takes a component that needs a `urlPageFeatureFlagByUser` prop and returns
 * an equivalent component that gets the value from app state, therefore no
 * longer expecting it as a prop.
 *
 * @param {Object} Component Component to be wrapped
 * @returns {Object}The wrapped component
 */
function withUrlPageFeatureFlagByUser<TOwnProps>(Component: React.ComponentType<TOwnProps & UrlPageFeatureFlagByUser>): React.ComponentClass<TOwnProps> {
  return withUserFeatureFlag<TOwnProps, UrlPageFeatureFlagByUser>({
    flagKey: perUserFlagKey,
    defaultValue: false,
    name: 'urlPageFeatureFlagByUser',
  })(Component);
}

/**
 * The URL page can be enabled at per-site or per-user.
 * This HOC abstracts the logic for determining whether the feature is enabled
 * by reducing the separate site-level and user-level flag values into a
 * single `FeatureFlagProps` value and passing that into the wrapped component.
 *
 * @param {Object} Component Component to be wrapped
 * @returns {Object}The wrapped component
 */
function combineSiteAndUserFlags<TOwnProps extends object>(Component: React.ComponentType<TOwnProps & UrlPageFeatureFlag>): React.ComponentClass<TOwnProps & UrlPageFeatureFlagBySite & UrlPageFeatureFlagByUser> {
  return class extends React.Component<TOwnProps & UrlPageFeatureFlagBySite & UrlPageFeatureFlagByUser, {}> {
    public render() {
      const { urlPageFeatureFlagBySite: siteLevelFF, urlPageFeatureFlagByUser: userLevelFF, ...restProps } = this.props as CombinerProps;
      const urlPageFeatureFlag: FeatureFlagProps = {
        value: siteLevelFF.value || userLevelFF.value,
        isLoading: siteLevelFF.isLoading || userLevelFF.isLoading,
      };

      return <Component urlPageFeatureFlag={urlPageFeatureFlag} {...restProps} />;
    }
  };
}

/**
 * This composition ties all of the feature flag logic together.
 *
 * `withUrlPageFeatureFlagByUser` and `withUrlPageFeatureFlagBySite` are called
 * at the outermost layers so they are able to pass their flag values down to `combineSiteAndUserFlags`
 *
 * `combineSiteAndUserFlags` then combines the values and passes a single
 * reduced `FeatureFlagProps` prop to the provided component.
 *
 * @param {Object} Component Component to be wrapped
 * @returns {Object}The wrapped component
 */
export function withUrlPageFeatureFlag <TOwnProps>(Component: React.ComponentType<TOwnProps & UrlPageFeatureFlag>) {
  return withUrlPageFeatureFlagByUser(withUrlPageFeatureFlagBySite(combineSiteAndUserFlags(Component)));
}
