import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';

import { analyticsClient, AnalyticsClientProps, withAnalyticsClient } from 'common/analytics';
import { GenericErrorMessage } from 'common/error';
import { isGroupModifiableForUser } from 'common/group-permissions';
import {
  Group,
  GroupSelectedItem,
  GroupSuggestionItem,
  Picker,
} from 'common/picker';

import { GroupsPickerModalQuery, GroupsPickerModalQueryVariables } from '../../../../schema/schema-types';
import { debounce } from '../../../../utilities/decorators';
import groupsPickerModalQuery from './groups-picker-modal.query.graphql';

interface Props {
  isOpen: boolean;
  cloudId: string;
  title: string;
  inputLabelText: string;
  placeholderText: string;
  excludedGroupIds?: string[];
  isExposingScimGroups?: boolean;
  onDialogDismissed?(): void;
  renderFooter(getSelectedGroups: () => Group[]): React.ReactNode;
  setGroupsCount?(countGroupsAvailable: number): void;
}

interface State {
  selectedGroups: Group[];
  searchQuery: string | null;
}

export type GroupsPickerModalProps = ChildProps<Props, GroupsPickerModalQuery, GroupsPickerModalQueryVariables> & AnalyticsClientProps;
export class GroupsPickerModalImpl extends React.Component<GroupsPickerModalProps, State> {
  public readonly state: Readonly<State> = {
    selectedGroups: [],
    searchQuery: null,
  };

  public static getDerivedStateFromProps(nextProps: GroupsPickerModalProps, prevState: State): State {
    return {
      selectedGroups: nextProps.isOpen ? prevState.selectedGroups : [],
      searchQuery: nextProps.isOpen ? prevState.searchQuery : null,
    };
  }

  public componentDidUpdate(prevProps: GroupsPickerModalProps, _) {
    const { data, setGroupsCount } = this.props;
    if (!data) {
      return;
    }
    const groups = this.getGroups(this.props);
    const prevGroups = this.getGroups(prevProps);
    if (groups.length === prevGroups.length) {
      return;
    }

    if (setGroupsCount) {
      setGroupsCount(groups.length);
    }
  }

  public render() {
    const { data, isOpen, title, inputLabelText, placeholderText, renderFooter, onDialogDismissed } = this.props;

    if (!data) {
      return null;
    }

    const groups = this.getGroups(this.props);

    return (
      <Picker
        isOpen={isOpen}
        title={title}
        inputLabelText={inputLabelText}
        placeholderText={placeholderText}
        items={groups}
        loading={data.loading}
        errorMessage={data.error && <GenericErrorMessage />}
        renderItem={this.renderItem}
        renderItemPreview={this.renderItemPreview}
        onSelect={this.onSelect}
        onRemove={this.onRemove}
        onQueryChange={this.onQueryChange}
        renderFooter={renderFooter}
        onDialogDismissed={onDialogDismissed}
      />
    );
  }

  @debounce('onInputChange')
  private debouncedRefetch() {
    if (!this.props.data || !this.props.data.refetch || !this.shouldRefetchOnFilter()) {
      return;
    }

    this.props.data.refetch({
      cloudId: this.props.cloudId,
      displayName: this.state.searchQuery,
    }).catch(error => analyticsClient.onError(error));
  }

  private getGroups = (props) => {
    const { data } = props;
    const groups = data && data.groupList && data.groupList.groups ? data.groupList.groups : [];

    return this.convertToPickerGroupsWithQuery(groups);
  };

  private convertToPickerGroupsWithQuery = (groups): Group[] => {
    if (!groups) {
      return [];
    }

    const output = groups.map((group): Group => ({
      id: group.id,
      displayName: group.name,
      description: group.description,
      managementAccess: group.managementAccess,
      ownerType: group.ownerType,
      sitePrivilege: group.sitePrivilege,
      unmodifiable: group.unmodifiable,
    }));

    if (this.shouldRefetchOnFilter()) {
      return output;
    }

    return output.filter((group: Group) => {
      const { searchQuery = '' } = this.state;

      const isSystemAdmin = !!(this.props.data && this.props.data.currentUser && this.props.data.currentUser.isSystemAdmin);
      if (!isGroupModifiableForUser(group, isSystemAdmin, this.props.isExposingScimGroups || false) || this.props.excludedGroupIds && this.props.excludedGroupIds.includes(group.id)) {
        return false;
      }

      if (!searchQuery || !searchQuery.length) {
        return true;
      } else {
        return group.displayName.toLowerCase().includes(searchQuery.toLowerCase());
      }
    });
  };

  private renderItem = (item: Group, isHighlighted, isSelected, select) => {
    if (isSelected) {
      return null;
    }

    return <GroupSuggestionItem {...{ item, isHighlighted, select }} />;
  };

  private renderItemPreview = (item: Group, unselect) => {
    return <GroupSelectedItem {...{ item, unselect }} />;
  };

  private onSelect = (group: Group) => {
    this.setState(prevState => ({
      selectedGroups: [...prevState.selectedGroups, group],
    }));
  };

  private onRemove = (group: Group) => {
    this.setState(prevState => ({
      selectedGroups: prevState.selectedGroups.filter(g => g.id !== group.id),
    }));
  };

  private onQueryChange = (query) => {
    this.setState({ searchQuery: query });
    this.debouncedRefetch();
  };

  private shouldRefetchOnFilter = (): boolean => {
    if (!this.props.data || !this.props.data.groupList || !this.props.data.groupList.groups) {
      return false;
    }

    return this.props.data.groupList.groups.length < this.props.data.groupList.total;
  };
}

const withQuery = graphql<GroupsPickerModalProps, GroupsPickerModalQuery, GroupsPickerModalQueryVariables>(groupsPickerModalQuery, {
  options: ({ cloudId }) => ({
    variables: {
      cloudId,
    },
  }),
});

export const GroupsPickerModal = withAnalyticsClient(
  withQuery(
    GroupsPickerModalImpl,
  ),
);
