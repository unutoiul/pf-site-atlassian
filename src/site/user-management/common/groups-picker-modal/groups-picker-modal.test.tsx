import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import * as React from 'react';
import { DataValue } from 'react-apollo';
import { IntlProvider } from 'react-intl';
import { sandbox as sinonSandbox } from 'sinon';

import { Picker } from 'common/picker';

import { Group, GroupsPickerModalQuery, GroupsPickerModalQueryVariables, ManagementAccess, OwnerType, SitePrivilege } from '../../../../schema/schema-types';
import { GroupsPickerModalImpl, GroupsPickerModalProps } from './groups-picker-modal';

describe('GroupsPickerModalImpl', () => {
  const sandbox = sinonSandbox.create();

  let analyticsStub;

  beforeEach(() => {
    analyticsStub = {
      sendUIEvent: sandbox.spy(),
    };
  });

  const defaultProps = {
    isOpen: true,
    cloudId: 'test-id',
    analyticsClient: analyticsStub,
    title: 'Group picker',
    inputLabelText: 'Groups',
    placeholderText: 'Search',
    renderFooter: () => null,
  } as GroupsPickerModalProps;

  interface GroupOptionsInput {
    ownerType?: OwnerType;
    sitePrivilege?: SitePrivilege;
    unmodifiable?: boolean;
    managementAccess?: ManagementAccess;
  }

  const generateGroup = (id: string, options?: GroupOptionsInput): Group & { __typename } => {
    return {
      id,
      name: 'Party Corgi',
      description: 'Best Party',
      userTotal: 0,
      productPermissions: [],
      defaultForProducts: [],
      sitePrivilege: 'NONE',
      unmodifiable: false,
      managementAccess: 'ALL',
      ownerType: null,
      __typename: 'Group',
      ...options,
    };
  };

  const generateGroupList = (
    totalDefaultGroups: number,
    totalSpecialGroups: number,
    options?: GroupOptionsInput,
  ): Partial<GroupsPickerModalQuery> => {

    const groups = new Array<any>();

    for (let i = 0; i < totalDefaultGroups + totalSpecialGroups; i++) {
      i + 1 > totalDefaultGroups ? groups.push(generateGroup(i.toString(), options)) : groups.push(generateGroup(i.toString()));
    }

    return {
      groupList: {
        total: totalDefaultGroups + totalSpecialGroups,
        groups,
        __typename: 'GroupList',
      },
    };
  };

  const generateData = (totalDefaultGroups, totalSpecialGroups, options?) => {
    return {
      ...generateGroupList(totalDefaultGroups, totalSpecialGroups, options),
    } as DataValue<GroupsPickerModalQuery, GroupsPickerModalQueryVariables>;
  };

  describe('When displaying groups in a list', () => {
    it('should render an item for each group', () => {
      const wrapper = shallow((
        <GroupsPickerModalImpl
          {...defaultProps}
          data={generateData(11, 12, { sitePrivilege: 'SITE_ADMIN' })}
        />
      ));

      expect(wrapper.find(Picker).props().items.length).to.equal(23);
    });

    it('should filter out SCIM groups', () => {
      const wrapper = shallow((
        <GroupsPickerModalImpl
          {...defaultProps}
          data={generateData(1, 1, { ownerType: 'EXT_SCIM', managementAccess: 'READ_ONLY' })}
        />
      ));

      expect(wrapper.find(Picker).props().items.length).to.equal(1);
    });

    it('should filter out SYSTEM ADMIN groups', () => {
      const wrapper = shallow((
        <GroupsPickerModalImpl
          {...defaultProps}
          data={generateData(1, 1, { sitePrivilege: 'SYS_ADMIN' })}
        />
      ));

      expect(wrapper.find(Picker).props().items.length).to.equal(1);
    });

    it('should filter out SYSTEM groups', () => {
      const wrapper = shallow((
        <GroupsPickerModalImpl
          {...defaultProps}
          data={generateData(1, 1, { unmodifiable: true, managementAccess: 'NONE' })}
        />
      ));

      expect(wrapper.find(Picker).props().items.length).to.equal(1);
    });

    it('should not filter out SITE_ADMIN groups', () => {
      const wrapper = shallow((
        <GroupsPickerModalImpl
          {...defaultProps}
          data={generateData(1, 1, { sitePrivilege: 'SITE_ADMIN' })}
        />
      ));

      expect(wrapper.find(Picker).props().items.length).to.equal(2);
    });

    it('should filter out groups with ids in excludedGroupIds', () => {
      const wrapper = shallow((
        <GroupsPickerModalImpl
          {...defaultProps}
          data={generateData(3, 0)}
          excludedGroupIds={['1', '2']}
        />
      ));

      expect(wrapper.find(Picker).props().items.length).to.equal(1);
    });
  });

  it('should update the selectedGroups state when a group is selected', () => {
    const wrapper = mount((
      <IntlProvider locale="en">
        <GroupsPickerModalImpl
          {...defaultProps}
          data={{} as any}
        />
      </IntlProvider>
    ));

    const groupsPickerModalImpl = wrapper.find(GroupsPickerModalImpl);

    expect(groupsPickerModalImpl.state('selectedGroups')).to.deep.equal([]);

    wrapper.find(Picker).props().onSelect({ id: '1' });

    expect(groupsPickerModalImpl.state('selectedGroups')).to.deep.equal([{ id: '1' }]);
  });

  it('should clear the selectedGroups state when the dialog is closed', () => {
    class GroupsPickerModalImplWithIsOpen extends React.Component<{ isOpen: boolean }> {
      public render() {
        return (
          <IntlProvider locale="en">
            <GroupsPickerModalImpl
              {...defaultProps}
              isOpen={this.props.isOpen}
              data={{} as any}
            />
          </IntlProvider>
        );
      }
    }
    const wrapper = mount((
      <GroupsPickerModalImplWithIsOpen
        isOpen={true}
      />
    ));

    const groupsPickerModalImpl = wrapper.find(GroupsPickerModalImpl);

    wrapper.find(Picker).props().onSelect({ id: '1' });

    expect(groupsPickerModalImpl.state('selectedGroups')).to.deep.equal([{ id: '1' }]);

    wrapper.setProps({ isOpen: false });

    expect(groupsPickerModalImpl.state('selectedGroups')).to.deep.equal([]);
  });
});
