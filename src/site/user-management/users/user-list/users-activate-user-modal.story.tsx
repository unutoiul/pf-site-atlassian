import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';

import { createMockIntlProp } from '../../../../utilities/testing';
import { UserActivateModalProps, UsersActivateUserModalImpl } from './users-activate-user-modal';

const defaultProps = {
  isOpen: true,
  cloudId: 'DUMMY-CLOUD-ID',
  user: {
    id: '1',
    displayName: 'Flustered Corgi',
  },
  isActive: true,
  ...({} as UserActivateModalProps),
  intl: createMockIntlProp(),
  onDismiss: () => undefined,
  activateUser: () => undefined as any,
  deactivateUser: () => undefined as any,
  match: undefined as any,
  history: undefined as any,
  location: { search: { page: 1 } } as any,
};

storiesOf('Site|Users List Page/Activate User Modal', module)
  .add('Deactivate', () => (
    <IntlProvider locale="en">
      <UsersActivateUserModalImpl {...defaultProps} />
    </IntlProvider>
  ))
  .add('Activate', () => (
    <IntlProvider locale="en">
      <UsersActivateUserModalImpl {...defaultProps} isActive={false} />
    </IntlProvider>
  ));
