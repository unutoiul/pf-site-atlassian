import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { MemoryRouter } from 'react-router-dom';
import * as sinon from 'sinon';

import { Button as AnalyticsButton } from 'common/analytics';
import { PageLayout } from 'common/page-layout';

import { util } from '../../../../utilities/admin-hub';
import { createMockIntlContext, createMockIntlProp } from '../../../../utilities/testing';
import { UserListPageLayoutImpl } from './users-list-page-layout';

describe('Users list page layout', () => {
  let dispatchSpy: sinon.SinonSpy;
  let showDrawerSpy: sinon.SinonSpy;
  let historySpy;
  let isAdminHubStub: sinon.SinonStub;
  const sandbox = sinon.sandbox.create();

  beforeEach(() => {
    historySpy = {
      replace: sandbox.spy(),
    };
    dispatchSpy = sandbox.spy();
    showDrawerSpy = sandbox.spy();
    isAdminHubStub = sandbox.stub(util, 'isAdminHub');
    isAdminHubStub.returns(false);
  });
  afterEach(() => {
    sandbox.restore();
  });

  interface TestComponentParams {
    isDisabled?: boolean;
    isInviteOpen?: boolean;
    userExportADG3Migration?: boolean;
    isExportOpen?: boolean;
  }

  function createTestComponent(children: JSX.Element, { isDisabled, isInviteOpen, userExportADG3Migration, isExportOpen }: TestComponentParams = {}): ShallowWrapper {
    const match: any = {
      params: {
        cloudId: 'DUMMY-TEST-CLOUD-ID',
      },
    };

    return shallow(
      <MemoryRouter>
        <UserListPageLayoutImpl
          showDrawer={showDrawerSpy}
          isDisabled={isDisabled}
          intl={createMockIntlProp()}
          dispatch={dispatchSpy}
          history={historySpy}
          match={match}
          location={{
            search: `?invite=${isInviteOpen}&export=${isExportOpen}`,
          } as any}
          totalUserCount={40}
          userCountOnScreen={20}
          userExportADG3Migration={userExportADG3Migration}
        >
          {children}
        </UserListPageLayoutImpl>
      </MemoryRouter>,
      {
        disableLifecycleMethods: false,
        context: createMockIntlContext().context,
      },
    ).find(UserListPageLayoutImpl).dive();
  }

  it('Should render children', () => {
    // tslint:disable-next-line:jsx-use-translation-function
    const wrapper = createTestComponent(<span>Hello</span>);
    const span = wrapper.find('span');
    expect(span.length).to.equal(1);
    expect(span.text()).to.equal('Hello');
  });

  it('Should render title', () => {
    const wrapper = createTestComponent(<div />);
    const pageLayout = wrapper.find(PageLayout);
    expect(pageLayout.length).to.equal(1);
    expect(pageLayout.props().title).to.equal('Users');
  });

  it('Should render buttons', () => {
    const wrapper = createTestComponent(<div />);
    const pageLayout = wrapper.find(PageLayout);
    expect(pageLayout.length).to.equal(1);
    const actions = shallow(pageLayout.props().action || <div />);
    const buttons = actions.find(AnalyticsButton);
    expect(buttons.length).to.equal(2);
    expect(buttons.at(0).children().text()).to.equal('Invite users');
    expect(buttons.at(1).children().text()).to.equal('Export users');
  });

  it('Should have disabled buttons if explicity set', () => {
    const wrapper = createTestComponent(<div />, { isDisabled: true });
    const pageLayout = wrapper.find(PageLayout);
    expect(pageLayout.length).to.equal(1);
    const actions = shallow(pageLayout.props().action || <div />);
    const buttons = actions.find(AnalyticsButton);
    expect(buttons.length).to.equal(2);
    expect(buttons.at(0).props().isDisabled).to.equal(true);
    expect(buttons.at(1).props().isDisabled).to.equal(true);
  });

  it('Should pass user count props to the analytics buttons', () => {
    const wrapper = createTestComponent(<div />, { isDisabled: true });
    const pageLayout = wrapper.find(PageLayout);
    expect(pageLayout.length).to.equal(1);
    const actions = shallow(pageLayout.props().action || <div />);
    const buttons = actions.find(AnalyticsButton);
    expect(buttons.at(0).props().analyticsData!.actionSubjectId).to.equal('openInviteUserDrawer');
    expect(buttons.at(0).props().analyticsData!.attributes).to.deep.equal({
      userCountOnScreen: 20,
      totalUserCount: 40,
    });
    expect(buttons.at(1).props().analyticsData!.actionSubjectId).to.equal('exportUsers');
    expect(buttons.at(1).props().analyticsData!.attributes).to.deep.equal({
      userCountOnScreen: 20,
      totalUserCount: 40,
    });
  });

  it('Should mount users invite drawer if invite query parameter is true', () => {
    createTestComponent(<div />, { isInviteOpen: true });

    expect(dispatchSpy.calledWith(
      { type: 'CHROME_DRAWER_OPEN', drawer: 1, modifyHistory: true },
    )).to.equal(true);
  });

  it('Should remove invite query parameter from url', () => {
    createTestComponent(<div />, { isInviteOpen: true });

    expect(historySpy.replace.calledWithExactly(`${util.siteAdminBasePath}/s/DUMMY-TEST-CLOUD-ID/users`)).to.equal(true);
  });

  it('Should mount user export drawer if export query parameter is true', () => {
    createTestComponent(<div />, { isExportOpen: true });

    expect(dispatchSpy.calledWith(
      { type: 'CHROME_DRAWER_OPEN', drawer: 2, modifyHistory: true },
    )).to.equal(true);
  });

  it('Should remove export query parameter from url', () => {
    createTestComponent(<div />, { isExportOpen: true });

    expect(historySpy.replace.calledWithExactly(`${util.siteAdminBasePath}/s/DUMMY-TEST-CLOUD-ID/users`)).to.equal(true);
  });

  describe('Export users button', () => {
    it('Should launch old user export page if clicked and feature flag is off', () => {
      const wrapper = createTestComponent(<div />);
      const pageLayout = wrapper.find(PageLayout);
      const actions = shallow(pageLayout.props().action || <div />);
      const exportButton = actions.find(AnalyticsButton).at(1);
      expect(exportButton.prop('href')).to.not.equal(undefined);
      expect(exportButton.prop('onClick')).to.equal(undefined);
    });

    it('Should open export users drawer if clicked and feature flag is on', () => {
      const wrapper = createTestComponent(<div />, { userExportADG3Migration: true });
      const pageLayout = wrapper.find(PageLayout);
      const actions = shallow(pageLayout.props().action || <div />);
      const exportButton = actions.find(AnalyticsButton).at(1);
      exportButton.simulate('click');
      expect(exportButton.prop('href')).to.equal(undefined);
      expect(dispatchSpy.calledWith(
        { type: 'CHROME_DRAWER_OPEN', drawer: 2, modifyHistory: true },
      )).to.equal(true);
    });
  });

  describe('in admin hub', () => {
    beforeEach(() => {
      isAdminHubStub.returns(true);
    });

    it('should call showDrawer to open the invite drawer', () => {
      const wrapper = createTestComponent(<div />);
      const pageLayout = wrapper.find(PageLayout);
      const actions = shallow(pageLayout.props().action || <div />);
      const inviteButton = actions.find(AnalyticsButton).at(0);

      inviteButton.simulate('click');

      expect(showDrawerSpy.calledWith('INVITE_USER')).to.equal(true);
    });

    it('should call showDrawer to open the user export drawer', () => {
      const wrapper = createTestComponent(<div />);
      const pageLayout = wrapper.find(PageLayout);
      const actions = shallow(pageLayout.props().action || <div />);
      const exportButton = actions.find(AnalyticsButton).at(1);

      exportButton.simulate('click');

      expect(showDrawerSpy.calledWith('EXPORT_USERS')).to.equal(true);
    });
  });
});
