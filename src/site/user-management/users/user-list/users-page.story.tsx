import { storiesOf } from '@storybook/react';
import * as React from 'react';
import ApolloProvider from 'react-apollo/ApolloProvider';
import { IntlProvider } from 'react-intl';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { SiteUsersListQuery } from 'src/schema/schema-types';

import { FlagProvider } from 'common/flag';

import { createStore } from '../../../../store';

import adminProductsQuery from './user-list-filters/users-list-admin-products.query.graphql';
import useProductsQuery from './user-list-filters/users-list-use-products.query.graphql';

import { createApolloClient } from '../../../../apollo-client';
import { createMockIntlProp } from '../../../../utilities/testing';
// tslint:disable-next-line:disallow-relative-import-for-common
import groupsPickerModalQuery from '../../common/groups-picker-modal/groups-picker-modal.query.graphql';
import { UsersPage } from './users-page';
import siteUsersListQuery from './users-page.query.graphql';

const generateUsers = (limit: number): SiteUsersListQuery['siteUsersList']['users'] => {
  const users: SiteUsersListQuery['siteUsersList']['users'] = [];
  for (let i = 0; i < limit; i++) {
    users.push({
      __typename: 'User',
      id: `i + ${Math.floor(Math.random() * 100)}`,
      displayName: `Prince Corgi ${i}`,
      email: 'corgi@happyfriends.paws',
      active: i % 2 === 0,
      presence: '2017-07-10T14:00:00Z',
      activeStatus: i % 4 === 0 ? 'ENABLED' : i % 3 === 0 ? 'DISABLED' : 'BLOCKED',
      siteAdmin: i * 3 === 0,
      trustedUser: (i % 4) === 0,
      system: false,
      hasVerifiedEmail: true,
    });
  }

  return users;
};

const generateGroups = (limit = 20) => {
  const groups: any[] = [];

  for (let i = 0; i < limit; i++) {
    groups.push({
      __typename: 'Group',
      id: i + Math.floor(Math.random() * 100),
      name: `Party Corgi ${i}`,
      description: 'Crogi dancing',
    });
  }

  return groups;
};

const adminProducts = () => {
  return {
    currentSite: {
      id: 'DUMMY_CLOUD_ID',
      groupsAdminAccessConfig: [{
        product: {
          productId: 'confluence',
          productName: 'Confluence',
          __typename: 'Product',
        },
        __typename: 'Product',
      }],
      __typename: 'CurrentSite',
    },
  };
};

const useProducts = () => {
  return {
    currentSite: {
      id: 'DUMMY_CLOUD_ID',
      groupsUseAccessConfig: [{
        product: {
          productId: 'confluence',
          productName: 'Confluence',
          __typename: 'Product',
        },
        __typename: 'Product',
      }],
      __typename: 'CurrentSite',
    },
  };
};

const pageDefaults = {
  intl: createMockIntlProp(),
  history: { replace: () => null } as any,
  match: { params: { cloudId: 'DUMMY_CLOUD_ID' } } as any,
  location: window.location as any,
};

const currentSiteData = {
  currentSite: {
    __typename: 'CurrentSite',
    id: 'dummy-id',
    flags: {
      __typename: 'SiteFlags',
      userExportADG3Migration: true,
    },
  },
};

const client = createApolloClient();

function writeDefaultQuery(cloudId: string, usersData: SiteUsersListQuery, groupsData) {
  client.writeQuery({
    query: siteUsersListQuery,
    variables: {
      cloudId,
      input: {
        count: 20,
        start: 1,
      },
    },
    data: usersData,
  });

  client.writeQuery({
    query: useProductsQuery,
    data: useProducts(),
  });

  client.writeQuery({
    query: adminProductsQuery,
    data: adminProducts(),
  });

  client.writeQuery({
    query: groupsPickerModalQuery,
    variables: {
      cloudId,
    },
    data: groupsData,
  });
}

function createTestComponent(cloudId: string) {
  return (
    <IntlProvider locale="en">
      <ApolloProvider client={client}>
        <Provider store={createStore()}>
          <Router>
            <Switch location={{ pathname: `/s/${cloudId}/users`, search: '', state: undefined, hash: '' }}>
              <Route path="/s/:cloudId/users">
                <FlagProvider>
                  <UsersPage {...pageDefaults} />
                </FlagProvider>
              </Route>
            </Switch>
          </Router>
        </Provider>
      </ApolloProvider>
    </IntlProvider>
  );
}

storiesOf('Site|Users List Page', module)
  .add('With users', () => {
    const usersData: SiteUsersListQuery = {
      siteUsersList: {
        total: 60,
        users: generateUsers(20),
        __typename: 'PaginatedUsers',
      },
      ...currentSiteData,
    };

    const groupsData = {
      groupList: {
        total: 10,
        groups: generateGroups(10),
        __typename: 'PaginatedGroups',
      },
    };

    writeDefaultQuery('the-order-of-golden-retrievers', usersData, groupsData);

    return createTestComponent('the-order-of-golden-retrievers');
  }).add('With empty users, but total users are not 0', () => {
    const usersData = {
      siteUsersList: {
        total: 60,
        users: [],
        __typename: 'PaginatedUsers',
      },
      ...currentSiteData,
    };

    const groupsData = {
      groupList: {
        total: 10,
        groups: generateGroups(10),
        __typename: 'PaginatedGroups',
      },
    };

    writeDefaultQuery('alaskan-malamute-society', usersData, groupsData);

    return createTestComponent('alaskan-malamute-society');
  }).add('With empty users, and total users is 0', () => {
    const usersData = {
      siteUsersList: {
        total: 0,
        users: [],
        __typename: 'PaginatedUsers',
      },
      ...currentSiteData,
    };

    const groupsData = {
      groupList: {
        total: 10,
        groups: generateGroups(10),
        __typename: 'PaginatedGroups',
      },
    };

    writeDefaultQuery('afghan-hound-organization', usersData, groupsData);

    return createTestComponent('afghan-hound-organization');
  }).add('With empty users, and total users is less than 5', () => {
    const usersData = {
      siteUsersList: {
        total: 3,
        users: generateUsers(3),
        __typename: 'PaginatedUsers',
      },
      ...currentSiteData,
    };

    const groupsData = {
      groupList: {
        total: 10,
        groups: generateGroups(10),
        __typename: 'PaginatedGroups',
      },
    };

    writeDefaultQuery('corgi-school', usersData, groupsData);

    return createTestComponent('corgi-school');
  });
