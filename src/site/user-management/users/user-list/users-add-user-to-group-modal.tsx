import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import {
  defineMessages,
  InjectedIntlProps,
  injectIntl,
} from 'react-intl';

import AkButton, {
  ButtonGroup as AkButtonGroup,
} from '@atlaskit/button';

import { addedUserToGroupTrackEventData, addUserToGroupsCancelUIEventData, addUserToGroupsConfirmUIEventData, AnalyticsClientProps, Button as AnalyticsButton, ScreenEventSender, userListAddToGroupModalScreenEvent, withAnalyticsClient } from 'common/analytics';
import { createErrorIcon, createSuccessIcon } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';
import { getStart } from 'common/pagination';
import { Group } from 'common/picker';

import {
  AddUsersToGroupsMutation,
  AddUsersToGroupsMutationArgs,
} from '../../../../schema/schema-types';
import { GroupsPickerModal } from '../../../user-management/common/groups-picker-modal';
import { AnalyticsProps } from '../../user-management-analytics';
import { optimisticallyAddGroup } from '../user-details/user-details-group/user-details-group.query.updater';
import { DEFAULT_ROWS_PER_PAGE, STARTING_PAGE } from '../user-details/user-details-page/user-details-constants';
import { getGroupNameByRole, getRoleByGroupSitePrivilege } from '../user-details/user-details-permission/helpers/getGroupNameByRole';
import { optmisticallyAddGroupToUser } from '../user-details/user-details-permission/user-details-permission-group-query.updater';
import { optimisticallyUpdatePermissionsForSiteAdminGroup } from '../user-details/user-details-permission/user-details-permission-query.updater';
import { Permissions } from '../user-permission-section/permissions';
import addUsersToGroupsMutation from './add-users-to-groups.mutation.graphql';
const messages = defineMessages({
  title: {
    id: 'users.add.user.to.group.modal.title',
    defaultMessage: 'Add a user to groups',
    description: 'The title of the modal.',
  },
  inputLabelText: {
    id: 'users.add.user.to.group.input.label.modal.text',
    defaultMessage: 'Groups',
    description: 'What the search input text is.',
  },
  placeholderText: {
    id: 'users.add.user.to.group.placeholder.modal.text',
    defaultMessage: 'Search',
    description: 'Search input default text.',
  },
  addButton: {
    id: 'users.add.user.to.group.add.modal.button',
    defaultMessage: 'Add to groups',
    description: 'Add action for the popup modal.',
  },
  cancelButton: {
    id: 'users.add.user.to.group.cancel.modal.button',
    defaultMessage: 'Cancel',
    description: 'Cancel action for the popup modal.',
  },
  successFlagTitle: {
    id: 'users.add.user.to.group.success.flag.modal.title',
    defaultMessage: 'User added to groups',
    description: 'Title to show in the success flag when the user has been added to the group(s).',
  },
  successFlagDescription: {
    id: 'users.add.user.to.group.success.flag.modal.description',
    defaultMessage: `We've added {username} to {groups, plural,
      one {the {groupname} group}
      other {# groups}}.`,
    description: 'Description to show in the success flag when the user has been added to the group(s).',
  },
  errorFlagTitle: {
    id: 'users.add.user.to.group.error.flag.modal.title',
    defaultMessage: 'Unable to add user to groups. Try again later.',
    description: `Title to show in the error flag when the user couldn't be added to the group(s).`,
  },
});

interface Props {
  isOpen: boolean;
  cloudId: string;
  currentPage: number;
  userId: string;
  username: string;
  onDialogDismissed?(): void;
  onAddMembership?(): void;
}

interface State {
  isMutationLoading: boolean;
  countGroupsAvailable?: number;
}

export type UsersAddUserToGroupModalProps = ChildProps<Props, AddUsersToGroupsMutation, AddUsersToGroupsMutationArgs> & InjectedIntlProps & FlagProps & AnalyticsProps & AnalyticsClientProps;
export class UsersAddUserToGroupModalImpl extends React.Component<UsersAddUserToGroupModalProps, State> {
  public readonly state: Readonly<State> = {
    isMutationLoading: false,
  };

  private selectedGroups: Group[] = [];

  public render() {
    const { intl: { formatMessage }, isOpen, cloudId } = this.props;
    const isLoading = !!(this.state.isMutationLoading && this.selectedGroups.length);

    if (!isOpen) {
      return null;
    }

    return (
      <ScreenEventSender
        event={userListAddToGroupModalScreenEvent({
          userId: this.props.userId,
          countGroupsAvailable: this.state.countGroupsAvailable || 0,
        })}
        isDataLoading={isLoading || this.state.countGroupsAvailable === undefined}
      >
        <GroupsPickerModal
          isOpen={isOpen}
          cloudId={cloudId}
          title={formatMessage(messages.title)}
          inputLabelText={formatMessage(messages.inputLabelText)}
          placeholderText={formatMessage(messages.placeholderText)}
          renderFooter={this.renderFooter}
          onDialogDismissed={this.onDialogDismissed}
          setGroupsCount={this.onLoad}
        />
      </ScreenEventSender>
    );
  }

  private renderFooter = (getSelectedGroups: () => Group[]) => {
    const { intl: { formatMessage } } = this.props;
    this.selectedGroups = getSelectedGroups();
    const isLoading = !!(this.state.isMutationLoading && this.selectedGroups.length);

    return (
        <AkButtonGroup>
          <AnalyticsButton
            appearance="primary"
            onClick={this.onAddButtonClicked(this.selectedGroups)}
            isDisabled={!this.selectedGroups.length}
            isLoading={isLoading}
          >
            {formatMessage(messages.addButton)}
          </AnalyticsButton>
          <AkButton
            appearance="subtle-link"
            onClick={this.onDialogDismissed}
          >
            {formatMessage(messages.cancelButton)}
          </AkButton>
        </AkButtonGroup>
    );
  };
  private onLoad = (countGroupsAvailable) => {
    this.setState({ countGroupsAvailable });
  }

  private onAddButtonClicked = (selectedGroups: Group[]) => async () => {
    const {
      cloudId,
      intl: { formatMessage },
      mutate,
      showFlag,
      userId,
      username,
    } = this.props;

    this.sendAddUserToGroupsConfirmUIEventAnalytics(selectedGroups);

    if (!mutate) {
      return;
    }

    this.setState({ isMutationLoading: true });
    try {
      await mutate({
        variables: {
          cloudId,
          input: {
            users: [userId],
            groups: selectedGroups.map(group => group.id),
          },
        },
        update: (store) => {
          const groupsPageQueryVariables = {
            userId,
            cloudId,
            start: getStart(STARTING_PAGE, DEFAULT_ROWS_PER_PAGE),
            count: DEFAULT_ROWS_PER_PAGE,
          };
          optimisticallyAddGroup(store, groupsPageQueryVariables, selectedGroups);

          selectedGroups.forEach(group => {
            const newRole = getRoleByGroupSitePrivilege(group.sitePrivilege);
            if (newRole === Permissions.SITEADMIN) {
              optmisticallyAddGroupToUser(store, cloudId, userId, getGroupNameByRole(newRole), group.id);
              optimisticallyUpdatePermissionsForSiteAdminGroup(store, cloudId, userId, group.id);
            }
          });
        },
      });
      if (this.props.onAddMembership) {
        this.props.onAddMembership();
      }
      this.sendAddedUserToGroupTrackEventAnalytics(selectedGroups);

      showFlag({
        autoDismiss: true,
        icon: createSuccessIcon(),
        id: `users.add.user.to.group.success.flag.${Date.now()}`,
        title: formatMessage(messages.successFlagTitle),
        description: formatMessage(messages.successFlagDescription, { username, groups: selectedGroups.length, groupname: selectedGroups[0].displayName }),
      });
    } catch {
      this.props.showFlag({
        autoDismiss: true,
        icon: createErrorIcon(),
        id: `users.add.user.to.group.error.flag.${Date.now()}`,
        title: this.props.intl.formatMessage(messages.errorFlagTitle),
      });
    } finally {
      if (this.props.onDialogDismissed) {
        this.props.onDialogDismissed();
      }
      this.setState({ isMutationLoading: false });
    }
  }

  private sendAddedUserToGroupTrackEventAnalytics = (selectedGroups: Group[]) => {
    this.props.analyticsClient.sendTrackEvent({
      cloudId: this.props.cloudId,
      data: addedUserToGroupTrackEventData({
        userState: this.props.userStates,
        userId: this.props.userId,
        groupsAddedCount: selectedGroups.length,
      }),
    });
  }

  private sendAddUserToGroupsConfirmUIEventAnalytics = (selectedGroups: Group[]) => {
    this.props.analyticsClient.sendUIEvent({
      cloudId: this.props.cloudId,
      data: addUserToGroupsConfirmUIEventData({
        userState: this.props.userStates,
        userId: this.props.userId,
        groupCount: selectedGroups.length,
        groupIds: selectedGroups.map(group => group.id),
      }),
    });
  };

  private sendAddUserToGroupsCancelUIEventAnalytics = () => {
    this.props.analyticsClient.sendUIEvent({
      cloudId: this.props.cloudId,
      data: addUserToGroupsCancelUIEventData({
        userState: this.props.userStates,
        userId: this.props.userId,
      }),
    });
  };

  private onDialogDismissed = () => {
    this.sendAddUserToGroupsCancelUIEventAnalytics();
    if (this.props.onDialogDismissed) {
      this.props.onDialogDismissed();
    }
  };
}

const withMutation = graphql<UsersAddUserToGroupModalProps, AddUsersToGroupsMutation, AddUsersToGroupsMutationArgs>(addUsersToGroupsMutation);

export const UsersAddUserToGroupModal = withAnalyticsClient(
  withFlag(
    injectIntl(
      withMutation(
        UsersAddUserToGroupModalImpl,
      ),
    ),
  ),
);
