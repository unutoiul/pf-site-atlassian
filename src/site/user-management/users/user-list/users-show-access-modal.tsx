import { NetworkStatus } from 'apollo-client';
import * as React from 'react';
import { ChildProps, graphql, MutationFunc } from 'react-apollo';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import AkButton, { ButtonGroup as AkButtonGroup } from '@atlaskit/button';
import AkSpinner from '@atlaskit/spinner';
import { gridSize as akGridSize } from '@atlaskit/theme';

import { analyticsClient, AnalyticsClientProps, ScreenEventSender, userListCancelProductAccess, userListGrantAccessModalProductAccessSaved, userListProductAccessModalScreenEvent, userListSaveProductAccess, withAnalyticsClient } from 'common/analytics';
import { createErrorIcon, createSuccessIcon } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';
import { ModalDialog } from 'common/modal';
import { ProductAccess, ProductsChanged } from 'common/product-access';

import {
  ProductAccess as ProductAccessType,
  User,
  UsersShowAccessModalGrantAccessMutation,
  UsersShowAccessModalGrantAccessMutationVariables,
  UsersShowAccessModalQuery,
  UsersShowAccessModalQueryVariables,
  UsersShowAccessModalRevokeAccessMutation,
  UsersShowAccessModalRevokeAccessMutationVariables,
} from '../../../../schema/schema-types';

import { isUserInSiteAdminGroup, isUserInSystemAdminGroup } from '../site-privilege-checks';
import { getUserRole } from '../users-common/get-user-role';
import grantAccessMutation from './users-show-access-modal-grant-access.mutation.graphql';
import revokeAccessMutation from './users-show-access-modal-revoke-access.mutation.graphql';
import modalQuery from './users-show-access-modal.query.graphql';

const messages = defineMessages({
  title: {
    id: 'users.show.access.modal.title',
    defaultMessage: 'Product access',
    description: 'Grant/revoke product access modals header on the users page.',
  },
  description: {
    id: 'users.show.access.modal.description',
    defaultMessage: 'Choose the products you want {displayName} to be able to access:',
    description: 'Grant product access modals blip.',
  },
  readonlyDescriptionCurrentUser: {
    id: 'users.show.access.modal.readonly.description.current.user',
    defaultMessage: 'You can only modify your own access directly via group memberships.',
    description: `Text shown to a user who's granting or revoking product access for themselves.`,
  },
  readonlyDescriptionSiteAdmin: {
    id: 'users.show.access.modal.readonly.description.site.admin',
    defaultMessage: `Because this user is a site admin, you can only modify their access directly via group memberships.`,
    description: `Text shown to a user who's granting or revoking product access for another site admin.`,
  },
  readonlyDescriptionSystemAdmin: {
    id: 'users.show.access.modal.readonly.description.system.admin',
    defaultMessage: `Because this user is a system admin, you can only modify their access directly via group memberships.`,
    description: `Text shown to a user who's granting or revoking product access for a system admin.`,
  },
  buttonSave: {
    id: 'users.show.access.modal.button.save',
    defaultMessage: 'Save',
    description: 'Save the modal button.',
  },
  buttonCancel: {
    id: 'users.show.access.modal.button.cancel',
    defaultMessage: 'Cancel',
    description: 'Cancel and close the modal button.',
  },
  errorFlagTitle: {
    id: 'users.show.access.modal.error.flag.title',
    defaultMessage: 'Something went wrong',
    description: 'Title for the error flag.',
  },
  errorFlagDescription: {
    id: 'users.show.access.modal.error.flag.description',
    defaultMessage: 'Unable to save your product access changes. Try again later.',
    description: 'Description for the error flag.',
  },
  successFlagGrantOnlyTitle: {
    id: 'users.show.access.modal.success.flag.grant.only.title',
    defaultMessage: 'Product access granted for {displayName}',
    description: 'Success title when granting product access.',
  },
  successFlagGrantOnlyDescription: {
    id: 'users.show.access.modal.success.flag.grant.only.description',
    defaultMessage: `{displayName} now has access to {grantNumber} additional {grantNumber, plural, one {product} other {products}}.`,
    description: 'Success description when granting product access.',
  },
  successFlagRevokeOnlyTitle: {
    id: 'users.show.access.modal.success.flag.revoke.only.title',
    defaultMessage: 'Product access revoked for {displayName}',
    description: 'Success title when revoking product access.',
  },
  successFlagRevokeOnlyDescription: {
    id: 'users.show.access.modal.success.flag.revoke.only.description',
    defaultMessage: `{displayName} no longer has access to {revokeNumber} {revokeNumber, plural, one {product} other {products}}.`,
    description: 'Success description when revoking product access.',
  },
  successFlagGrantAndRevokeTitle: {
    id: 'users.show.access.modal.success.flag.grant.and.revoke.title',
    defaultMessage: 'Product access granted and revoked for {displayName}',
    description: 'Success title when granting and revoking product access.',
  },
  successFlagGrantAndRevokeDescription: {
    id: 'users.show.access.modal.success.flag.grant.and.revoke.description',
    defaultMessage: `{displayName} now has access to {grantNumber} additional {grantNumber, plural, one {product} other {products}} and no longer has access to {revokeNumber} {revokeNumber, plural, one {product} other {products}}.`,
    description: 'Success description when granting and revoking product access.',
  },
  loadingError: {
    id: 'users.show.access.modal.loading.error',
    defaultMessage: 'Product access failed to load. Please try again later.',
    description: 'Error to show in the Product access modal.',
  },
  user: {
    id: 'users.show.access.modal.user',
    defaultMessage: 'user',
    description: `Placeholder display name if one isn't present`,
  },
});

const FooterContainer = styled.div`
  text-align: right;
`;

const ProductAccessContainer = styled.div`
  margin: ${akGridSize() * 2}px 0 0 -${akGridSize}px;
`;

interface CheckboxDiff {
  readonly toRevoke: string[];
  readonly toGrant: string[];
}

interface OwnProps {
  isOpen: boolean;
  cloudId: string;
  user: User;
  onDialogDismissed(): void;
}

interface GrantAccessMutationProps {
  grantAccessToProducts?: MutationFunc<
  UsersShowAccessModalGrantAccessMutation,
  UsersShowAccessModalGrantAccessMutationVariables
  >;
}

interface RevokeAccessMutationProps {
  revokeAccessToProducts?: MutationFunc<
  UsersShowAccessModalRevokeAccessMutation,
  UsersShowAccessModalRevokeAccessMutationVariables
  >;
}

type MutationProps = GrantAccessMutationProps & RevokeAccessMutationProps;

export type Props = OwnProps & InjectedIntlProps & FlagProps;
interface State {
  areMutationsLoading: boolean;
  productChanges: CheckboxDiff;
}

export class UsersShowAccessModalImpl extends React.Component<ChildProps<Props & MutationProps, UsersShowAccessModalQuery & UsersShowAccessModalGrantAccessMutation & UsersShowAccessModalRevokeAccessMutation> & AnalyticsClientProps, State> {
  public readonly state: Readonly<State> = {
    areMutationsLoading: false,
    productChanges: {
      toGrant: [],
      toRevoke: [],
    },
  };

  public componentDidUpdate(prevProps: Props) {
    // tslint:disable-next-line:early-exit
    if (!prevProps.isOpen && this.props.isOpen && this.props.data) {
      this.props.data.refetch({
        cloudId: this.props.cloudId,
        userId: this.props.user.id,
      }).catch(e => analyticsClient.onError(e));
    }
  }

  public render() {
    const isChecked = (p) => p.accessLevel !== 'NONE';
    const isNotChecked = (p) => p.accessLevel === 'NONE';
    const productAccess = this.props.data && this.props.data.user && this.props.data.user.productAccess ? this.props.data.user.productAccess : [];

    return (
      <ModalDialog
        header={this.props.intl.formatMessage(messages.title)}
        footer={this.renderFooter()}
        isOpen={this.props.isOpen}
        onClose={this.onClose}
        width="small"
      >
        <ScreenEventSender
          event={userListProductAccessModalScreenEvent({
            userId: this.props.user.id,
            selectedProductKeys: productAccess.filter(isChecked).map(p => p.productId),
            unselectedProductKeys: productAccess.filter(isNotChecked).map(p => p.productId),
          })}
          isDataLoading={this.state.areMutationsLoading}
        >
          {this.renderBody()}
        </ScreenEventSender>
      </ModalDialog>
    );
  }

  private renderFooter = () => {
    const { formatMessage } = this.props.intl;

    const anyChanges = this.state.productChanges.toGrant.length
      || this.state.productChanges.toRevoke.length;

    return (
      <FooterContainer>
        <AkButtonGroup>
          <AkButton
            appearance="primary"
            onClick={this.onSaveClicked}
            isLoading={this.state.areMutationsLoading}
            isDisabled={this.isModalReadonly() || !anyChanges}
          >
            {formatMessage(messages.buttonSave)}
          </AkButton>
          <AkButton
            appearance="subtle-link"
            onClick={this.onClose}
          >
            {formatMessage(messages.buttonCancel)}
          </AkButton>
        </AkButtonGroup>
      </FooterContainer>
    );
  };

  private getRealProducts = (productAccess: ProductAccessType[]) => {
    // jira-admin is returned as a product, which is not a product you should be granting / revoking access to.
    return productAccess.filter(product => product.productId !== 'jira-admin');
  }

  private renderBody = () => {
    const { formatMessage } = this.props.intl;
    if (this.props.data && (this.props.data.loading || this.props.data.networkStatus === NetworkStatus.refetch)) {
      return <AkSpinner />;
    }

    if (this.props.data && this.props.data.error) {
      return <FormattedMessage {...messages.loadingError} />;
    }

    let products: ProductsChanged[] = [];

    if (this.props.data && this.props.data.user && this.props.data.user.productAccess) {
      const isChecked = (p) => p.accessLevel !== 'NONE';

      products = this.getRealProducts(this.props.data.user.productAccess)
        .map(product => {
          return {
            ...product,
            isChecked: isChecked(product),
            isInitiallyChecked: isChecked(product),
          } as ProductsChanged;
        });
    }

    return (
      <React.Fragment>
        {this.isModalReadonly() ? (
          this.isUserCurrentUser() ? <FormattedMessage {...messages.readonlyDescriptionCurrentUser} tagName="p" /> :
            this.isUserSiteAdmin() ? <FormattedMessage {...messages.readonlyDescriptionSiteAdmin} tagName="p" /> :
              this.isUserSystemAdmin() ? <FormattedMessage {...messages.readonlyDescriptionSystemAdmin} tagName="p" /> :
                null
        ) : (
            <FormattedMessage
              {...messages.description}
              tagName="p"
              values={{ displayName: this.props.user.displayName || formatMessage(messages.user) }}
            />
          )}
        <ProductAccessContainer>
          {/* We need to reset the state of this fully uncontrolled component https://reactjs.org/blog/2018/06/07/you-probably-dont-need-derived-state.html#recommendation-fully-uncontrolled-component-with-a-key **/}
          <ProductAccess
            tooltipStyle="inline"
            isReadOnly={this.isModalReadonly()}
            products={products}
            key={JSON.stringify(products)}
            onCheckboxChange={this.onCheckboxChange}
          />
        </ProductAccessContainer>
      </React.Fragment>
    );
  };

  private onCheckboxChange = (products: ProductsChanged[]) => {
    this.setState({
      productChanges: products.reduce<CheckboxDiff>((prev, current) => {
        if (current.isInitiallyChecked && !current.isChecked) {
          prev.toRevoke.push(current.productId);
        }

        if (!current.isInitiallyChecked && current.isChecked) {
          prev.toGrant.push(current.productId);
        }

        return prev;
      }, {
        toRevoke: [],
        toGrant: [],
      }),
    });
  };

  private onClose = () => {
    this.setState({
      areMutationsLoading: false,
      productChanges: {
        toGrant: [],
        toRevoke: [],
      },
    });

    this.props.analyticsClient.sendUIEvent({
      data: userListCancelProductAccess({
        userId: this.props.user.id,
        userRole: getUserRole(this.props.user),
        userStatus: this.props.user.activeStatus || '',
      }),
    });

    this.props.onDialogDismissed();
  };

  private onSaveClicked = async () => {
    const {
      grantAccessToProducts,
      revokeAccessToProducts,
      onDialogDismissed,
    } = this.props;

    const productAccessBefore = this.props.data &&
      !this.props.data.loading &&
      this.props.data.user &&
      this.getRealProducts(this.props.data.user.productAccess).filter(p => p.accessLevel !== 'NONE').map(p => p.productId) || [];

    const productAccessAfter = productAccessBefore.filter(p => this.state.productChanges.toRevoke.indexOf(p) === -1)
      .concat(this.state.productChanges.toGrant);

    this.props.analyticsClient.sendUIEvent({
      data: userListSaveProductAccess({
        userId: this.props.user.id,
        userRole: getUserRole(this.props.user),
        userStatus: this.props.user.activeStatus || '',
        productAccessBefore,
        productAccessAfter,
      }),
    });

    if (!grantAccessToProducts || !revokeAccessToProducts || !this.props.data) {
      return;
    }

    this.setState({ areMutationsLoading: true });

    let mutationError = false;

    try {
      let grantRequest;
      let revokeRequest;

      if (this.state.productChanges.toGrant.length) {
        grantRequest = grantAccessToProducts({
          variables: {
            cloudId: this.props.cloudId,
            users: [this.props.user.id],
            productIds: this.state.productChanges.toGrant,
          },
        });
      }

      if (this.state.productChanges.toRevoke.length) {
        revokeRequest = revokeAccessToProducts({
          variables: {
            cloudId: this.props.cloudId,
            users: [this.props.user.id],
            productIds: this.state.productChanges.toRevoke,
          },
        });
      }

      await grantRequest;
      await revokeRequest;
    } catch (e) {
      mutationError = true;
    } finally {
      onDialogDismissed();

      this.setState({ areMutationsLoading: false });

      if (mutationError) {
        this.showErrorFlag();
      } else {
        this.props.analyticsClient.sendTrackEvent({
          data: userListGrantAccessModalProductAccessSaved({
            productAccessAfter,
            productAccessBefore,
            userId: this.props.user.id,
            userRole: getUserRole(this.props.user),
            userStatus: this.props.user.activeStatus || '',
          }),
        });

        this.showSuccessFlag();
      }
    }
  };

  private showSuccessFlag = () => {
    const {
      intl: { formatMessage },
      showFlag,
    } = this.props;

    const { productChanges } = this.state;

    const defaultFlagProps = {
      autoDismiss: true,
      icon: createSuccessIcon(),
      id: `users.show.access.modal.success.${Date.now()}`,
    };

    const msgValues = {
      displayName: this.props.user.displayName,
      grantNumber: productChanges.toGrant.length,
      revokeNumber: productChanges.toRevoke.length,
    };

    if (productChanges.toGrant.length && !productChanges.toRevoke.length) {
      showFlag({
        ...defaultFlagProps,
        title: formatMessage(messages.successFlagGrantOnlyTitle, msgValues),
        description: formatMessage(messages.successFlagGrantOnlyDescription, msgValues),
      });
    } else if (!productChanges.toGrant.length && productChanges.toRevoke.length) {
      showFlag({
        ...defaultFlagProps,
        title: formatMessage(messages.successFlagRevokeOnlyTitle, msgValues),
        description: formatMessage(messages.successFlagRevokeOnlyDescription, msgValues),
      });
    } else if (productChanges.toGrant.length && productChanges.toRevoke.length) {
      showFlag({
        ...defaultFlagProps,
        title: formatMessage(messages.successFlagGrantAndRevokeTitle, msgValues),
        description: formatMessage(messages.successFlagGrantAndRevokeDescription, msgValues),
      });
    }
  };

  private showErrorFlag = () => {
    this.props.showFlag({
      autoDismiss: true,
      icon: createErrorIcon(),
      id: `users.show.access.modal.error.${Date.now()}`,
      title: this.props.intl.formatMessage(messages.errorFlagTitle),
      description: this.props.intl.formatMessage(messages.errorFlagDescription),
    });
  };

  private isUserSystemAdmin = (): boolean => {
    return !!(this.props.data && this.props.data.user && isUserInSystemAdminGroup(this.props.data.user));
  }

  private isUserSiteAdmin = (): boolean => {
    return !!(this.props.data && this.props.data.user && isUserInSiteAdminGroup(this.props.data.user));
  }
  private isUserCurrentUser = (): boolean => {
    return !!(
      this.props.data
      && this.props.data.currentUser
      && this.props.data.currentUser.id === this.props.user.id
    );
  }

  private isModalReadonly = (): boolean => {
    return this.isUserCurrentUser() || this.isUserSystemAdmin() || this.isUserSiteAdmin();
  };
}

const withQuery = graphql<Props, UsersShowAccessModalQuery>(modalQuery, {
  options: ({ cloudId, user }) => ({
    notifyOnNetworkStatusChange: true,
    variables: {
      cloudId,
      userId: user.id,
      start: 1,
      count: 100,
    } as UsersShowAccessModalQueryVariables,
  }),

  skip: ({ cloudId, user, isOpen }) => !cloudId || !user.id || !isOpen,
});

const withGrantAccessMutation = graphql<Props, UsersShowAccessModalGrantAccessMutation, {}, GrantAccessMutationProps>(grantAccessMutation, {
  name: 'grantAccessToProducts',
});

const withRevokeAccessMutation = graphql<Props & GrantAccessMutationProps, UsersShowAccessModalRevokeAccessMutation, {}, RevokeAccessMutationProps>(revokeAccessMutation, {
  name: 'revokeAccessToProducts',
});

export const UsersShowAccessModal = injectIntl(
  withFlag(
    withQuery(
      withGrantAccessMutation(
        withRevokeAccessMutation(
          withAnalyticsClient(
            UsersShowAccessModalImpl,
          ),
        ),
      ),
    ),
  ),
);
