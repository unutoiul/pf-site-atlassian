import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';

import { User } from '../../../../schema/schema-types';
import { createMockAnalyticsClient, createMockIntlProp } from '../../../../utilities/testing';
import { UsersShowAccessModalImpl } from './users-show-access-modal';

const defaultProps = {
  isOpen: true,
  cloudId: 'DUMMY-CLOUD-ID',
  user: {
    id: '1',
    displayName: 'Flustered Corgi',
    active: true,
    email: 'flusteredcorgi@atlassian.com',
    system: false,
  } as User,
  data: {
    loading: false,
    error: false,
    user: {
      productAccess: [
        {
          productId: 'jira-servicedesk',
          productName: 'Jira Service Desk',
          presence: '2017-07-10T14:00:00Z',
          accessLevel: 'NONE',
        },
        {
          productId: 'jira-software',
          productName: 'Jira Software',
          presence: '2017-07-10T14:00:00Z',
          accessLevel: 'NONE',
        },
        {
          productId: 'jira-core',
          productName: 'Jira Core',
          presence: '2017-07-10T14:00:00Z',
          accessLevel: 'NONE',
        },
        {
          productId: 'conf',
          productName: 'Confluence',
          presence: '2017-07-10T14:00:00Z',
          accessLevel: 'ADMIN',
        },
      ],
    },
    ...{} as any,
  },
  intl: createMockIntlProp(),
  onDialogDismissed: undefined as any,
  grantAccessToProducts: undefined as any,
  revokeAccessToProducts: undefined as any,
  showFlag: undefined as any,
  hideFlag: undefined as any,
  analyticsClient: createMockAnalyticsClient(),
};

storiesOf('Site|Users List Page/Show Access Modal', module)
  .add('Default', () => (
    <IntlProvider locale="en">
      <UsersShowAccessModalImpl {...defaultProps} />
    </IntlProvider>
  ))
  .add('While loading', () => (
    <IntlProvider locale="en">
      <UsersShowAccessModalImpl
        {...defaultProps}
        {...{ data: { loading: true, error: false } } as any}
      />
    </IntlProvider>
  ))
  .add('With loading error', () => (
    <IntlProvider locale="en">
      <UsersShowAccessModalImpl
        {...defaultProps}
        data={{
          ...defaultProps.data,
          loading: false,
          error: true,
        }}
      />
    </IntlProvider>
  ))
  .add('In readonly state', () => (
    <IntlProvider locale="en">
      <UsersShowAccessModalImpl
        {...defaultProps}
        data={{
          ...defaultProps.data,
          currentUser: {
            id: '1',
          },
        }}
      />
    </IntlProvider>
  ))
  .add('With Jira Core automatically checked by other Jira products', () => (
    <IntlProvider locale="en">
      <UsersShowAccessModalImpl
        {...defaultProps}
        data={{
          ...defaultProps.data,
          user: {
            productAccess: [
              {
                productId: 'jira-servicedesk',
                productName: 'Jira Service Desk',
                presence: '2017-07-10T14:00:00Z',
                accessLevel: 'ADMIN',
              },
              {
                productId: 'jira-software',
                productName: 'Jira Software',
                presence: '2017-07-10T14:00:00Z',
                accessLevel: 'ADMIN',
              },
              {
                productId: 'jira-core',
                productName: 'Jira Core',
                presence: '2017-07-10T14:00:00Z',
                accessLevel: 'NONE',
              },
              {
                productId: 'conf',
                productName: 'Confluence',
                presence: '2017-07-10T14:00:00Z',
                accessLevel: 'ADMIN',
              },
            ],
          },
        }}
      />
    </IntlProvider>
  ));
