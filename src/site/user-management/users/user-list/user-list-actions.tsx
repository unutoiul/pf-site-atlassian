import * as H from 'history';
import { parse } from 'querystring';
import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import {
  defineMessages,
  InjectedIntlProps,
  injectIntl,
} from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';

import { Action, Actions } from 'common/actions';
import { FlagProps, withFlag } from 'common/flag';

import { AnalyticsClientProps, userListAddToGroupsAction, userListGrantSiteAccessAction, userListResendInviteAction, userListRevokeSiteAccessAction, userListShowDetailsAction, userListUpdateProductAccessAction, withAnalyticsClient } from 'common/analytics';

import reInviteUser from './users-page-resend-invite.mutation.graphql';

import { User, UserPageReinviteUserMutation } from '../../../../schema/schema-types';
import { util } from '../../../../utilities/admin-hub';
import { AnalyticsProps } from '../../user-management-analytics';
import { getUserRole } from '../users-common/get-user-role';
import { resendInvite as resendInviteMutation } from '../users-common/user-resend-invite';
import { ActiveFilters } from './user-list-filters/users-list-active-filter';
import { UsersActivateUserModal } from './users-activate-user-modal';
import { UsersAddUserToGroupModal } from './users-add-user-to-group-modal';
import { UsersShowAccessModal } from './users-show-access-modal';

const messages = defineMessages({
  showDetails: {
    id: 'user.list.actions.show.details',
    description: 'An action that the admin can click on in the users table to show more details for the site user',
    defaultMessage: 'Show details',
  },
  deactivate: {
    id: 'user.list.actions.show.deactivate',
    description: 'An action that the admin can click on in the users table to deactivate site user',
    defaultMessage: 'Revoke site access',
  },
  addUserToGroup: {
    id: 'user.list.actions.add.user.to.group',
    description: 'An action that the admin can click on in the users table to pop a modal up to add the site user to groups',
    defaultMessage: 'Add user to group',
  },
  showAccess: {
    id: 'user.list.actions.show.access',
    description: 'An action that the admin can click on in the users table to show or modify product access for the site user',
    defaultMessage: 'Show product access',
  },
  activate: {
    id: 'user.list.actions.show.activate',
    description: 'An action that the admin can click on in the users table to activate site user',
    defaultMessage: 'Grant site access',
  },
  resendInvite: {
    id: 'user.list.actions.show.resend.invite',
    description: 'An action that the admin can click on in the users table to resend invite for the requesting user',
    defaultMessage: 'Resend invite',
  },
});

export interface UserListActionsProps {
  cloudId: string;
  userType: ActiveFilters;
  user: User;
  history: H.History;
  isCurrentUser: boolean;
  onUserUpdate?(): void;
}

interface UsersListActionsState {
  isAddUserToGroupsModalOpen: boolean;
  isShowAccessModalOpen: boolean;
  isActivateUserModalOpen: boolean;
}

export class UserListActionsImpl extends React.Component<ChildProps<UserListActionsProps & InjectedIntlProps & FlagProps & AnalyticsClientProps & AnalyticsProps & RouteComponentProps<{}>, UserPageReinviteUserMutation>, UsersListActionsState> {
  public readonly state: Readonly<UsersListActionsState> = {
    isAddUserToGroupsModalOpen: false,
    isActivateUserModalOpen: false,
    isShowAccessModalOpen: false,
  };

  public render() {
    return (
      <React.Fragment>
        {this.renderSwitch()}
        {this.renderModals()}
      </React.Fragment>
    );
  }

  public renderModals() {
    const { cloudId, user } = this.props;

    return (
      <React.Fragment>
        <UsersAddUserToGroupModal
          isOpen={this.state.isAddUserToGroupsModalOpen}
          cloudId={cloudId}
          userId={user.id}
          currentPage={1}
          username={user.displayName}
          userStates={this.props.userStates}
          onDialogDismissed={this.closeAddUserToGroupsModal}
          onAddMembership={this.props.onUserUpdate}
        />
        <UsersActivateUserModal
          isOpen={this.state.isActivateUserModalOpen}
          isActive={user.activeStatus === 'ENABLED'}
          cloudId={cloudId}
          user={user}
          onDismiss={this.closeActivateUserModal}
        />
        <UsersShowAccessModal
          isOpen={this.state.isShowAccessModalOpen}
          cloudId={cloudId}
          user={user}
          onDialogDismissed={this.closeShowAccessModal}
        />
      </React.Fragment>
    );
  }

  private renderSwitch = () => {
    const showDetails = (
      <Action actionType="custom" customMessage={messages.showDetails} onAction={this.showUserDetails} />
    );
    const deactivate = (
      <Action
        actionType="custom"
        customMessage={messages.deactivate}
        onAction={this.openActivateUserModal}
      />
    );
    const activate = (
      <Action
        actionType="custom"
        customMessage={messages.activate}
        onAction={this.openActivateUserModal}
      />
    );
    const resendInvite = (
      <Action
        actionType="custom"
        customMessage={messages.resendInvite}
        onAction={this.reinviteUser}
      />
    );
    const addUserToGroup = (
      <Action
        actionType="custom"
        customMessage={messages.addUserToGroup}
        onAction={this.openAddUserToGroupsModal}
      />
    );
    const showAccess = (
      <Action
        actionType="custom"
        customMessage={messages.showAccess}
        onAction={this.openShowAccessModal}
      />
    );

    switch (this.props.userType) {
      case 'blocked':
        return <Actions>{showDetails}</Actions>;
      case 'invited':
        return (
          <Actions>
            {resendInvite}
            {showDetails}
            {addUserToGroup}
            {showAccess}
          </Actions>
        );
      case 'inactive':
        return (
          <Actions>
            {!this.props.isCurrentUser && activate}
            {showDetails}
          </Actions>
        );
      default:
        return (
          <Actions>
            {showDetails}
            {!this.props.isCurrentUser && deactivate}
            {addUserToGroup}
            {showAccess}
          </Actions>
        );
    }
  };

  private reinviteUser = (): void => {
    this.props.analyticsClient.sendUIEvent({
      data: userListResendInviteAction(this.getActionAnalyticsData()),
    });

    resendInviteMutation({
      mutate: this.props.mutate,
      showFlag: this.props.showFlag,
      variables: {
        cloudId: this.props.cloudId,
        userId: this.props.user.id,
      },
      intl: this.props.intl,
      userStates: this.props.userStates,
      analyticsClient: this.props.analyticsClient,
      trackEventSource: 'userListScreen',
    });
  }

  private getActionAnalyticsData = () => ({
    userId: this.props.user.id,
    userRole: getUserRole(this.props.user),
    userStatus: this.props.user.activeStatus || '',
    lastSeenOnSite: this.props.user.presence || '',
    clickedViaFilterResults: Object.keys(parse(this.props.location.search)).filter(k => k !== 'page').length > 0,
  })

  private showUserDetails = () => {
    this.props.analyticsClient.sendUIEvent({
      data: userListShowDetailsAction(this.getActionAnalyticsData()),
    });
    this.props.history.push(`${util.siteAdminBasePath}/s/${this.props.cloudId}/users/${this.props.user.id}`);
  }

  private closeActivateUserModal = () => {
    this.setState({
      isActivateUserModalOpen: false,
    });
  };

  private closeShowAccessModal = () => {
    this.setState({
      isShowAccessModalOpen: false,
    });
  };

  private closeAddUserToGroupsModal = () => {
    this.setState({
      isAddUserToGroupsModalOpen: false,
    });
  };

  private openActivateUserModal = () => {
    const siteAccessActionEvent = this.props.userType === 'inactive' ? userListGrantSiteAccessAction : userListRevokeSiteAccessAction;

    this.props.analyticsClient.sendUIEvent({
      data: siteAccessActionEvent(this.getActionAnalyticsData()),
    });

    this.setState({
      isActivateUserModalOpen: true,
    });
  };

  private openShowAccessModal = () => {
    this.props.analyticsClient.sendUIEvent({
      data: userListUpdateProductAccessAction(this.getActionAnalyticsData()),
    });

    this.setState({
      isShowAccessModalOpen: true,
    });
  };

  private openAddUserToGroupsModal = () => {
    this.props.analyticsClient.sendUIEvent({
      data: userListAddToGroupsAction(this.getActionAnalyticsData()),
    });

    this.setState({
      isAddUserToGroupsModalOpen: true,
    });
  };
}

const withReInviteUser = graphql<UserListActionsProps & AnalyticsProps, UserPageReinviteUserMutation>(reInviteUser);
export const UserListActions = withReInviteUser(
  withFlag(
    injectIntl(
      withRouter(
        withAnalyticsClient(
          UserListActionsImpl,
        ),
      ),
    ),
  ),
);
