import { parse } from 'query-string';
import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import { connect, DispatchProp } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router';

import { ButtonGroup as AkButtonGroup } from '@atlaskit/button';

import { Button as AnalyticsButton, userListExportUsers, userListOpenInviteUserDrawer } from 'common/analytics';
import { ShowDrawerProps, withShowDrawer } from 'common/drawer';
import { NavigationDrawer, openDrawer } from 'common/navigation';
import { PageLayout } from 'common/page-layout';

import { util } from '../../../../utilities/admin-hub';

const messages = defineMessages({
  title: {
    id: 'users.page.layout.title',
    description: 'Page title used for the users list page',
    defaultMessage: 'Users',
  },
  actionInvite: {
    id: 'users.page.layout.action.invite',
    description: 'This button links to the invite users page, which allows admins to add more users to their site.',
    defaultMessage: 'Invite users',
  },
  actionExport: {
    id: 'users.page.layout.action.export',
    description: `This button links to the export users page, which allows admins to generate a CSV file of all of
    their site users.`,
    defaultMessage: 'Export users',
  },
});

interface OwnProps {
  children: React.ReactNode;
  isDisabled?: boolean;
  userCountOnScreen?: number;
  totalUserCount?: number;
  userExportADG3Migration?: boolean;
}
interface SiteParams {
  cloudId: string;
}

interface QueryParams {
  invite?: string;
  export?: string;
}

type UserListPageLayoutProps = OwnProps & InjectedIntlProps & RouteComponentProps<SiteParams> & DispatchProp & ShowDrawerProps;
export class UserListPageLayoutImpl extends React.Component<UserListPageLayoutProps> {
  public componentDidMount() {
    const { invite, export: exportParam }: QueryParams = parse(this.props.location.search);
    const cloudId = this.props.match.params.cloudId;

    if (invite === 'true') {
      this.props.history.replace(`${util.siteAdminBasePath}/s/${cloudId}/users`);
      this.openInviteDrawer();
    } else if (exportParam === 'true') {
      this.props.history.replace(`${util.siteAdminBasePath}/s/${cloudId}/users`);
      this.openUserExportDrawer();
    }
  }
  public render() {
    const { formatMessage } = this.props.intl;

    const shouldUseExportDrawer = util.isAdminHub() || this.props.userExportADG3Migration;

    return (
      <PageLayout
        isFullWidth={true}
        title={formatMessage(messages.title)}
        action={
          <AkButtonGroup>
            <AnalyticsButton
              appearance="primary"
              isDisabled={this.props.isDisabled}
              onClick={this.openInviteDrawer}
              analyticsData={userListOpenInviteUserDrawer({
                userCountOnScreen: this.props.userCountOnScreen,
                totalUserCount: this.props.totalUserCount,
              })}
            >
              {formatMessage(messages.actionInvite)}
            </AnalyticsButton>
            <AnalyticsButton
              isDisabled={this.props.isDisabled}
              href={!shouldUseExportDrawer ? '/admin/users/export' : undefined}
              onClick={shouldUseExportDrawer ? this.openUserExportDrawer : undefined}
              analyticsData={userListExportUsers({
                userCountOnScreen: this.props.userCountOnScreen,
                totalUserCount: this.props.totalUserCount,
              })}
            >
              {formatMessage(messages.actionExport)}
            </AnalyticsButton>
          </AkButtonGroup>
        }
      >
        {this.props.children}
      </PageLayout>
    );
  }

  public openInviteDrawer = () => {
    if (util.isAdminHub()) {
      this.props.showDrawer('INVITE_USER');
    } else {
      this.props.dispatch(openDrawer(NavigationDrawer.UserInvite));
    }
  };

  private openUserExportDrawer = () => {
    if (util.isAdminHub()) {
      this.props.showDrawer('EXPORT_USERS');
    } else {
      this.props.dispatch(openDrawer(NavigationDrawer.UserExport));
    }
  };
}

export const UserListPageLayout = withRouter(
  connect()(
    injectIntl(
      withShowDrawer(
        UserListPageLayoutImpl,
      ),
    ),
  ),
);
