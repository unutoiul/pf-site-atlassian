import { DataProxy } from 'apollo-cache';

import { SiteUsersListQuery, SiteUsersListQueryVariables } from '../../../../schema/schema-types';
import query from './users-page.query.graphql';

export function updateActiveStatus(
  store: DataProxy,
  activeUserId: string,
  shouldBeInactive: boolean,
  variables: SiteUsersListQueryVariables,
) {
  let currentData: SiteUsersListQuery;

  try {
    currentData = store.readQuery({
      query,
      variables,
    }) as SiteUsersListQuery;
  } catch (e) {
    return;
  }

  const {
    siteUsersList: { users },
  } = currentData;
  const updatedUsers = users.map(user => {
    if (user.id !== activeUserId || !user.activeStatus || user.activeStatus === 'BLOCKED') {
      return user;
    }

    return {
      ...user,
      active: !shouldBeInactive,
      activeStatus: shouldBeInactive ? 'DISABLED' : 'ENABLED',
    };
  });

  const newResult = {
    ...currentData,
    siteUsersList: {
      ...currentData.siteUsersList,
      users: updatedUsers,
    },
  } as SiteUsersListQuery;

  store.writeQuery({
    query,
    variables,
    data: newResult,
  });

  return;
}
