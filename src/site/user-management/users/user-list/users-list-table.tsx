import * as H from 'history';
import * as React from 'react';
import {
  defineMessages,
  InjectedIntlProps,
  injectIntl,
} from 'react-intl';
import styled from 'styled-components';

import {
  DynamicTableStateless as AkDynamicTableStateless,
  HeaderRow as AkHeaderRow,
  Row as AkRow,
} from '@atlaskit/dynamic-table';
import AkEmptyState from '@atlaskit/empty-state';
import AkLozenge from '@atlaskit/lozenge';
import {
  colors as AkColors,
  gridSize as akGridSize,
} from '@atlaskit/theme';

import { Account } from 'common/account';
import { Button as AnalyticsButton, ScreenEventSender, userListNoResultsScreenEvent, userListNoSearchResultsInviteUsers } from 'common/analytics';
import { searchErrorImage } from 'common/images';

import { User } from '../../../../schema/schema-types';
import { ActiveStatus } from '../../../../schema/schema-types/schema-types';
import { getUserStates } from '../../user-management-analytics';
import { UserListActions } from './user-list-actions';
import { ActiveFilters } from './user-list-filters/users-list-active-filter';
import { ROWS_PER_PAGE } from './users-list-constants';

const messages = defineMessages({
  headerUser: {
    id: 'users.list.header.user',
    description: 'Used as a table header',
    defaultMessage: 'User',
  },
  headerLastActive: {
    id: 'users.list.header.last.active',
    description: 'Used as a table header',
    defaultMessage: 'Last seen on site',
  },
  headerStatus: {
    id: 'users.list.header.status',
    description: 'Used as a table header',
    defaultMessage: 'Status',
  },
  headerActions: {
    id: 'users.list.header.action',
    description: 'Used as a table header',
    defaultMessage: 'Actions',
  },
  emptyStateTitle: {
    id: 'users.list.empty.state',
    description: 'Used as a part of a table\'s empty state when there are no users',
    defaultMessage: 'We looked everywhere but don\'t have anything to show you. Try modifying your search criteria',
  },
  emptyStateDescription: {
    id: 'users.list.empty.state.description',
    description: 'Used as a part of a table\'s empty state when there are no users',
    defaultMessage: 'If the user you\'re searching for doesn\'t exist, try inviting them.',
  },
  emptyStateButton: {
    id: 'users.list.empty.state.button',
    description: 'Used as a part of a table\'s empty state when there are no users',
    defaultMessage: 'Invite user',
  },
  admin: {
    id: 'users.list.status.admin',
    description: 'Used in a lozenge to show that this user is a site admin',
    defaultMessage: 'Site admin',
  },
  trustedUser: {
    id: 'users.list.status.trusted.user',
    description: 'Used in a lozenge to show that this user is a trusted user',
    defaultMessage: 'Trusted user',
  },
  invited: {
    id: 'users.list.status.invitation',
    description: 'Used as in a table cell to show that this user still needs to accept an invitation before joining the site',
    defaultMessage: 'Invitation pending',
  },
  noPresence: {
    id: 'users.list.presence',
    defaultMessage: 'Not seen on this site',
  },
  actionShowAccess: {
    id: 'users.list.action.show.access',
    description: 'Used in the action column to show the user\'s access',
    defaultMessage: 'Show access',
  },
  actionAddUserToGroup: {
    id: 'users.list.action.add.user.to.group',
    description: 'Used in the action column to show a modal which adds a user to groups.',
    defaultMessage: 'Add user to group',
  },
});

const statuses = defineMessages({
  blocked: {
    id: 'user.status.blocked',
    description: 'A user that is globally blocked and can\'t be enabled on a site level',
    defaultMessage: 'Account disabled',
  },
  active: {
    id: 'user.status.active',
    description: 'A user that is enabled',
    defaultMessage: 'Has site access',
  },
  inactive: {
    id: 'user.status.inactive',
    description: 'A user that is inactive',
    defaultMessage: 'No site access',
  },
  invited: {
    id: 'user.status.invited',
    description: 'A user that has been invited; they are enabled but they don\'t have presence data',
    defaultMessage: 'Invited',
  },
});

export const ButtonWrapper = styled.div`
  padding-top: ${akGridSize() * 4}px;
`;

export const SubtleCell = styled.span`
  color: ${AkColors.subtleHeading};
`;

export const AccountWrapper = styled.div`
  width: ${akGridSize() * 40}px;
`;

export const LozengeWrapper = styled.span`
  padding-right: ${akGridSize()}px;
`;

export interface UsersListTableProps {
  totalUsers: number;
  users: User[];
  isLoading: boolean;
  cloudId: string;
  currentPage: number;
  hasQuery: boolean;
  isCurrentUserSystemAdmin: boolean;
  currentUserId: string;
  history: H.History;
  userTypesSelected: string[];
  productKeysSelected: string[];
  adminTypesSelected: string[];
  displayNameQuery?: string;
  onSetPage(page: object | number): void;
  onUserUpdate?(): void;
}

export class UsersListTableImpl extends React.Component<UsersListTableProps & InjectedIntlProps> {
  public render() {
    const { intl: { formatMessage },
      displayNameQuery,
      userTypesSelected,
      productKeysSelected,
      adminTypesSelected,
    } = this.props;

    const head: AkHeaderRow = {
      cells: [
        { key: 'header.user', content: formatMessage(messages.headerUser), width: 40 },
        { key: 'header.last.active', content: formatMessage(messages.headerLastActive), width: 18 },
        { key: 'header.status', content: formatMessage(messages.headerStatus), width: 18 },
        { key: 'header.actions', content: formatMessage(messages.headerActions), width: 24, inlineStyles: { paddingLeft: '28px' } },
      ],
    };

    const emptyState: React.ReactNode = (
      <ScreenEventSender
        event={userListNoResultsScreenEvent({
          countSearchCharacters: displayNameQuery ? displayNameQuery.length : 0,
          userTypesSelected,
          productKeysSelected,
          adminTypesSelected,
        })}
        isDataLoading={this.props.isLoading}
      >
        <AkEmptyState
          size="wide"
          imageUrl={searchErrorImage}
          header={formatMessage(messages.emptyStateTitle)}
          description={
            <div>
              <span>{formatMessage(messages.emptyStateDescription)}</span>
              <ButtonWrapper>
                <AnalyticsButton
                  href="/admin/users/invite"
                  analyticsData={userListNoSearchResultsInviteUsers({
                    countSearchCharacters: (this.props.displayNameQuery || '').length,
                  })}
                >
                  {formatMessage(messages.emptyStateButton)}
                </AnalyticsButton>
              </ButtonWrapper>
            </div>
          }
        />
      </ScreenEventSender>
    );

    const rows: AkRow[] = this.props.users && this.props.users.filter(user => !user || !user.system || this.props.isCurrentUserSystemAdmin).map((user: User) => {
      if (!user) {
        return {
          key: 'empty-row',
          cells: [
            {
              key: 'account',
              content: null,
            },
            {
              key: 'status',
              content: null,
            },
            {
              key: 'presence',
              content: null,
            },
            {
              key: 'actions',
              content: null,
            },
          ],
        };
      }

      return ({
        cells: [
          {
            key: 'account.' + user.id,
            content: (
              <AccountWrapper>
                <Account
                  id={user.id}
                  isDisabled={!user.active}
                  email={user.email}
                  displayName={user.displayName}
                  lozenges={this.renderLozenges(user)}
                />
              </AccountWrapper>
            ),
          },
          {
            key: 'presence.' + user.id,
            content: (
              <SubtleCell>
                {this.calculatePresence(user.activeStatus || 'DISABLED', user.presence)}
              </SubtleCell>
            ),
          },
          {
            key: 'status.' + user.id,
            content: (
              <SubtleCell>
                {this.translateStatus(UsersListTableImpl.calculateUserType(user.activeStatus || 'DISABLED', user.active, user.presence))}
              </SubtleCell>
            ),
          },
          {
            key: 'actions.' + user.id,
            content: (
              <UserListActions
                cloudId={this.props.cloudId}
                user={user}
                userStates={getUserStates(user)}
                userType={UsersListTableImpl.calculateUserType(user.activeStatus || 'DISABLED', user.active, user.presence)}
                history={this.props.history}
                isCurrentUser={this.props.currentUserId === user.id}
                onUserUpdate={this.props.onUserUpdate}
              />
            ),
          },
        ],
      });
    });

    return (
      <AkDynamicTableStateless
        rowsPerPage={ROWS_PER_PAGE}
        head={this.props.totalUsers && rows.length ? head : undefined}
        rows={rows.length ? rows : undefined}
        isLoading={this.props.isLoading}
        onSetPage={this.onSetPage}
        page={this.props.currentPage}
        emptyView={this.props.hasQuery ? emptyState : undefined}
      />
    );
  }

  private renderLozenges({ id, siteAdmin, trustedUser }: Partial<User>): React.ReactNode[] {

    const lozenges: React.ReactNode[] = [];

    if (siteAdmin) {
      lozenges.push(
        <LozengeWrapper key={`site.admin.${id}`}>
          <AkLozenge>
            {this.props.intl.formatMessage(messages.admin)}
          </AkLozenge>
        </LozengeWrapper>,
      );
    }

    if (trustedUser) {
      lozenges.push(
        <LozengeWrapper key={`trusted.user.${id}`}>
          <AkLozenge>
            {this.props.intl.formatMessage(messages.trustedUser)}
          </AkLozenge>
        </LozengeWrapper>,
      );
    }

    return lozenges;
  }

  private translateStatus(filter: ActiveFilters) {
    switch (filter) {
      case 'blocked':
        return this.props.intl.formatMessage(statuses.blocked);
      case 'invited':
        return this.props.intl.formatMessage(statuses.invited);
      case 'active':
        return this.props.intl.formatMessage(statuses.active);
      case 'inactive':
        return this.props.intl.formatMessage(statuses.inactive);
      default:
        return this.props.intl.formatMessage(statuses.inactive);
    }
  }

  private calculatePresence(activeStatus: ActiveStatus, presence?: string | null): JSX.Element | string {
    if (presence) {
      return this.props.intl.formatDate(new Date(presence), { month: 'long', day: '2-digit', year: 'numeric' });
    }

    return <i>{this.props.intl.formatMessage(activeStatus === 'ENABLED' ? messages.invited : messages.noPresence)}</i>;
  }

  private static calculateUserType(activeStatus: ActiveStatus, active: boolean, presence?: string | null): ActiveFilters {
    if (activeStatus === 'BLOCKED') {
      return 'blocked';
    } else if (activeStatus === 'ENABLED' && presence) {
      return 'active';
    } else if (activeStatus === 'ENABLED') {
      return 'invited';
    } else if (active) {
      return 'active';
    }

    return 'inactive';
  }

  private onSetPage = (page: number): void => this.props.onSetPage({ page });
}

export const UsersListTable = injectIntl(UsersListTableImpl);
