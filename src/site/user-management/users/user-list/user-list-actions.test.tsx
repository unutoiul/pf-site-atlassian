import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import { Action } from 'common/actions';

import { User } from '../../../../schema/schema-types';
import { createMockIntlContext, createMockIntlProp, waitUntil } from '../../../../utilities/testing';
import { UserListActionsImpl } from './user-list-actions';
import { ActiveFilters } from './user-list-filters/users-list-active-filter';

const noop = () => null;

describe('Users list actions', () => {

  const sandbox = sinon.sandbox.create();

  let showFlagSpy;
  let hideFlagSpy;
  let mutateSpy;
  let sendUIEventSpy;

  beforeEach(() => {
    showFlagSpy = sandbox.spy();
    hideFlagSpy = sandbox.spy();
    mutateSpy = sandbox.stub().resolves();
    sendUIEventSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  function createTestComponent(userType: ActiveFilters, isCurrentUser: boolean, options: {
    search?: string,
  } = {}): ShallowWrapper {
    return shallow(
      <UserListActionsImpl
        cloudId="dummy-id"
        user={{
          id: '1',
          displayName: 'Content Corgi',
          active: true,
          email: 'contentcorgi@corgi.com',
          system: false,
          hasVerifiedEmail: true,
          presence: '2018-11-27T20:17:51.467Z',
          activeStatus: 'ENABLED',
        } as User}
        userType={userType}
        intl={createMockIntlProp()}
        analyticsClient={{
          sendUIEvent: sendUIEventSpy,
        } as any}
        userStates={[]}
        history={{
          push: noop,
        } as any}
        isCurrentUser={isCurrentUser}
        mutate={mutateSpy}
        showFlag={showFlagSpy}
        hideFlag={hideFlagSpy}
        location={{
          search: options.search || '',
        } as any}
        match={{} as any}
      />,
      createMockIntlContext(),
    );
  }

  it('active user type', () => {
    const wrapper = createTestComponent('active', false);
    const actions = wrapper.find(Action);
    expect(actions.length).to.equal(4);
    expect(actions.at(0).props().customMessage!.defaultMessage).to.equal('Show details');
    expect(actions.at(1).props().customMessage!.defaultMessage).to.equal('Revoke site access');
    expect(actions.at(2).props().customMessage!.defaultMessage).to.equal('Add user to group');
    expect(actions.at(3).props().customMessage!.defaultMessage).to.equal('Show product access');
  });

  it('Active user cannot deactivate self', () => {
    const wrapper = createTestComponent('active', true);
    const actions = wrapper.find(Action);
    expect(actions.length).to.equal(3);
    expect(actions.at(0).props().customMessage!.defaultMessage).to.equal('Show details');
    expect(actions.at(1).props().customMessage!.defaultMessage).to.equal('Add user to group');
    expect(actions.at(2).props().customMessage!.defaultMessage).to.equal('Show product access');
  });

  it('inactive user type', () => {
    const wrapper = createTestComponent('inactive', false);
    const actions = wrapper.find(Action);
    expect(actions.length).to.equal(2);
    expect(actions.at(0).props().customMessage!.defaultMessage).to.equal('Grant site access');
    expect(actions.at(1).props().customMessage!.defaultMessage).to.equal('Show details');
  });

  it('Inactive user cannot activate self', () => {
    const wrapper = createTestComponent('inactive', true);
    const actions = wrapper.find(Action);
    expect(actions.length).to.equal(1);
    expect(actions.at(0).props().customMessage!.defaultMessage).to.equal('Show details');
  });

  describe('For an invited user', () => {
    it('should have correct actions in the dropdown', () => {
      const wrapper = createTestComponent('invited', false);
      const actions = wrapper.find(Action);
      expect(actions.length).to.equal(4);
      expect(actions.at(0).props().customMessage!.defaultMessage).to.equal('Resend invite');
      expect(actions.at(1).props().customMessage!.defaultMessage).to.equal('Show details');
      expect(actions.at(2).props().customMessage!.defaultMessage).to.equal('Add user to group');
      expect(actions.at(3).props().customMessage!.defaultMessage).to.equal('Show product access');
    });

    it('Should trigger the correct mutation when the resend invite button is clicked', () => {
      const wrapper = createTestComponent('invited', false);
      const actions = wrapper.find(Action);
      (actions.at(0).props() as any).onAction();
      expect(mutateSpy.called).to.equal(true);
      expect(mutateSpy.calledWith({
        variables: {
          cloudId: 'dummy-id',
          userId: '1',
        },
      })).to.equal(true);
    });

    it('should display a success flag when prompting to impersonate user is successful', async () => {
      mutateSpy = sandbox.stub().resolves();
      const wrapper = createTestComponent('invited', false);
      const actions = wrapper.find(Action);
      (actions.at(0).props() as any).onAction();
      expect(mutateSpy.called).to.equal(true);
      await waitUntil(() => showFlagSpy.callCount > 0);
      expect(showFlagSpy.calledWithMatch({ title: 'Reminder sent!' })).to.equal(true);
    });

    it('Should display an error flag when impersonating user is not successful', async () => {
      mutateSpy = sandbox.stub().rejects();
      const wrapper = createTestComponent('invited', false);
      const actions = wrapper.find(Action);
      (actions.at(0).props() as any).onAction();
      expect(mutateSpy.called).to.equal(true);
      await waitUntil(() => showFlagSpy.callCount > 0);
      expect(showFlagSpy.calledWithMatch({ title: 'Something went wrong' })).to.equal(true);
    });
  });

  describe('UI analytics events', () => {
    const expectedUIAttributes = {
      clickedViaFilterResults: false,
      lastSeenOnSite: '2018-11-27T20:17:51.467Z',
      userId: '1',
      userRole: 'basic',
      userStatus: 'ENABLED',
    };

    it('should fire for reinvite user', async () => {
      const wrapper = createTestComponent('invited', false);
      const actions = wrapper.find(Action);

      (actions.at(0).props() as any).onAction();

      expect(sendUIEventSpy.callCount).to.equal(1);
      expect(sendUIEventSpy.getCalls()[0].args[0].data.actionSubjectId).to.equal('resendInvite');
      expect(sendUIEventSpy.getCalls()[0].args[0].data.attributes).to.deep.equal(expectedUIAttributes);
    });

    it('should fire for show user details', async () => {
      const wrapper = createTestComponent('invited', false);
      const actions = wrapper.find(Action);

      (actions.at(1).props() as any).onAction();

      expect(sendUIEventSpy.callCount).to.equal(1);
      expect(sendUIEventSpy.getCalls()[0].args[0].data.actionSubjectId).to.equal('showDetails');
      expect(sendUIEventSpy.getCalls()[0].args[0].data.attributes).to.deep.equal(expectedUIAttributes);
    });

    it('should fire for add user to group', async () => {
      const wrapper = createTestComponent('invited', false);
      const actions = wrapper.find(Action);

      (actions.at(2).props() as any).onAction();

      expect(sendUIEventSpy.callCount).to.equal(1);
      expect(sendUIEventSpy.getCalls()[0].args[0].data.actionSubjectId).to.equal('addToGroups');
      expect(sendUIEventSpy.getCalls()[0].args[0].data.attributes).to.deep.equal(expectedUIAttributes);
    });

    it('should fire for show product access', async () => {
      const wrapper = createTestComponent('invited', false);
      const actions = wrapper.find(Action);

      (actions.at(3).props() as any).onAction();

      expect(sendUIEventSpy.callCount).to.equal(1);
      expect(sendUIEventSpy.getCalls()[0].args[0].data.actionSubjectId).to.equal('updateProductAccess');
      expect(sendUIEventSpy.getCalls()[0].args[0].data.attributes).to.deep.equal(expectedUIAttributes);
    });

    it('should fire for grant site access', async () => {
      const wrapper = createTestComponent('inactive', false);
      const actions = wrapper.find(Action);

      (actions.at(0).props() as any).onAction();

      expect(sendUIEventSpy.callCount).to.equal(1);
      expect(sendUIEventSpy.getCalls()[0].args[0].data.actionSubjectId).to.equal('grantSiteAccess');
      expect(sendUIEventSpy.getCalls()[0].args[0].data.attributes).to.deep.equal(expectedUIAttributes);
    });

    it('should fire for revoke site access', async () => {
      const wrapper = createTestComponent('active', false);
      const actions = wrapper.find(Action);

      (actions.at(1).props() as any).onAction();

      expect(sendUIEventSpy.callCount).to.equal(1);
      expect(sendUIEventSpy.getCalls()[0].args[0].data.actionSubjectId).to.equal('revokeSiteAccess');
      expect(sendUIEventSpy.getCalls()[0].args[0].data.attributes).to.deep.equal(expectedUIAttributes);
    });
  });
});
