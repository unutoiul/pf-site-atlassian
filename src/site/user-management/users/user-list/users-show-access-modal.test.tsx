import { assert, expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import * as sinon from 'sinon';

import AkButton from '@atlaskit/button';
import { Checkbox as AkCheckbox } from '@atlaskit/checkbox';
import AkSpinner from '@atlaskit/spinner';

import { ScreenEventSender } from 'common/analytics';
import { ModalDialog } from 'common/modal';
import { ProductAccess } from 'common/product-access';

import { User, UsersShowAccessModalQuery } from '../../../../schema/schema-types';
import { createMockIntlProp, shallowUntilTarget, waitUntil } from '../../../../utilities/testing';
import { UsersShowAccessModalImpl } from './users-show-access-modal';

type ProductAccessType = UsersShowAccessModalQuery['user']['productAccess'][0];

describe('UsersShowAccessModal', () => {
  const sandbox = sinon.sandbox.create();

  let onDialogDismissedSpy: sinon.SinonSpy;
  let showFlagSpy: sinon.SinonSpy;
  let hideFlagSpy: sinon.SinonSpy;
  let grantAccessMutationStub: sinon.SinonStub;
  let revokeAccessMutationStub: sinon.SinonStub;
  let refetchStub: sinon.SinonStub;
  let sendTrackEventSpy: sinon.SinonSpy;
  let sendUIEventSpy: sinon.SinonSpy;

  beforeEach(() => {
    onDialogDismissedSpy = sinon.spy();
    showFlagSpy = sinon.spy();
    hideFlagSpy = sinon.spy();
    grantAccessMutationStub = sinon.stub().returns(Promise.resolve({}));
    revokeAccessMutationStub = sinon.stub().returns(Promise.resolve({}));
    refetchStub = sinon.stub().returns(Promise.resolve());
    sendTrackEventSpy = sinon.spy();
    sendUIEventSpy = sinon.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  const generateProductAccess = (product: ProductAccessType = {
    productId: 'jira-servicedesk',
    productName: 'Jira Service Desk',
    accessLevel: 'ADMIN',
    __typename: 'ProductAccess',
  }): ProductAccessType => {
    return product;
  };

  const generateUser = (): User => {
    return {
      id: '1',
      email: 'partycorgi@corgi.com',
      displayName: 'Party Corgi',
      active: true,
      system: false,
      hasVerifiedEmail: true,
    };
  };

  const getWrapper = (data = {} as any, user?: User) => {
    return shallow<UsersShowAccessModalImpl>((
      <UsersShowAccessModalImpl
        isOpen={false}
        cloudId="dummy-cloud-id"
        user={user ? user : generateUser()}
        onDialogDismissed={onDialogDismissedSpy}
        intl={createMockIntlProp()}
        showFlag={showFlagSpy}
        hideFlag={hideFlagSpy}
        grantAccessToProducts={grantAccessMutationStub}
        revokeAccessToProducts={revokeAccessMutationStub}
        data={{} as any}
        analyticsClient={{
          sendTrackEvent: sendTrackEventSpy,
          sendUIEvent: sendUIEventSpy,
        } as any}
      />
    ), { disableLifecycleMethods: false }).setProps({
      data: {
        ...data,
        refetch: refetchStub,
      },
    });
  };

  const getFooterWrapper = (wrapper: ShallowWrapper): ShallowWrapper => {
    const footer = wrapper.find(ModalDialog).prop('footer');

    return shallow(footer as React.ReactElement<any>);
  };

  const findMessageById = (wrapper: ShallowWrapper, id: string) => {
    return wrapper.find(FormattedMessage)
      .findWhere(m => m.prop('id') === id);
  };

  describe('initial state', () => {
    it('should show spinner when loading', () => {
      const wrapper = getWrapper({
        loading: true,
        error: false,
      });

      expect(wrapper.find(AkSpinner).exists()).to.equal(true);
    });

    it('should show an error message on error', () => {
      const wrapper = getWrapper({
        loading: false,
        error: true,
        user: {
          productAccess: [generateProductAccess()],
        },
      });

      expect(wrapper.find(AkSpinner).exists()).to.equal(false);

      expect(
        findMessageById(wrapper, 'users.show.access.modal.loading.error').exists(),
      ).to.equal(true);
    });

    it('should render the accesses if no error or loading', () => {
      const wrapper = getWrapper({
        loading: false,
        error: false,
        user: {
          productAccess: [generateProductAccess()],
        },
      });

      expect(wrapper.find(ProductAccess).prop('products')).to.have.lengthOf(1);
    });

    it('should refetch when opened', () => {
      const wrapper = getWrapper({
        loading: true,
        error: false,
      });

      wrapper.setProps({ isOpen: false });
      expect(refetchStub.called).to.equal(false);
      wrapper.setProps({ isOpen: true });
      expect(refetchStub.called).to.equal(true);
    });

    it('should ignore Jira administration as a product from the list', () => {
      const wrapper = getWrapper({
        loading: false,
        error: false,
        user: {
          productAccess: [
            generateProductAccess(),
            generateProductAccess({
              __typename: '',
              productName: 'Jira administration',
              productId: 'jira-admin',
              accessLevel: 'NONE',
            }),
          ],
        },
      });

      expect(wrapper.find(ProductAccess).prop('products')).to.have.lengthOf(1);
    });

    it('should send correct props to screen event sender', async () => {
      const wrapper = getWrapper({
        user: {
          productAccess: [
            generateProductAccess({
              productId: 'jira-servicedesk',
              productName: 'Jira Service Desk',
              accessLevel: 'NONE',
              __typename: 'ProductAccess',
            }),
            generateProductAccess({
              __typename: 'ProductAccess',
              productName: 'Jira software',
              productId: 'jira-software',
              accessLevel: 'USE',
            }),
            generateProductAccess({
              __typename: 'ProductAccess',
              productName: 'Jira core',
              productId: 'jira-core',
              accessLevel: 'ADMIN',
            }),
          ],
        },
      });

      expect(wrapper.find(ScreenEventSender).prop('event')).to.deep.equal({
        data: {
          name: 'userListProductAccessModal',
          attributes: {
            userId: '1',
            unselectedProductKeys: ['jira-servicedesk'],
            selectedProductKeys: ['jira-software', 'jira-core'],
          },
        },
      });
    });
  });

  describe('readonly state', () => {
    it('should display a readonly description when editing current user', () => {
      const wrapper = getWrapper({
        loading: false,
        error: false,
        currentUser: {
          id: '1',
        },
        user: {
          productAccess: [generateProductAccess()],
        },
      });

      expect(
        findMessageById(wrapper, 'users.show.access.modal.readonly.description.current.user').exists(),
      ).to.equal(true);
    });

    it('should display a readonly description when editing a user in the site-admin group', () => {
      const wrapper = getWrapper({
        loading: false,
        error: false,
        currentUser: {
          id: '2',
        },
        user: {
          productAccess: [generateProductAccess()],
          groups: {
            groups: [
              {
                id: 'site-admin',
                sitePrivilege: 'SITE_ADMIN',
              },
            ],
          },
        },
      });

      expect(
        findMessageById(wrapper, 'users.show.access.modal.readonly.description.site.admin').exists(),
      ).to.equal(true);
    });

    it('should display a readonly description when editing a user in the system-admin group', () => {
      const wrapper = getWrapper({
        loading: false,
        error: false,
        currentUser: {
          id: '2',
        },
        user: {
          productAccess: [generateProductAccess()],
          groups: {
            groups: [
              {
                id: 'system-admin',
                sitePrivilege: 'SYS_ADMIN',
              },
            ],
          },
        },
      });

      expect(
        findMessageById(wrapper, 'users.show.access.modal.readonly.description.system.admin').exists(),
      ).to.equal(true);
    });

    it('should render a disabled save button when editing current user', () => {
      const wrapper = getWrapper({
        loading: false,
        error: false,
        currentUser: {
          id: '1',
        },
        user: {
          productAccess: [generateProductAccess()],
        },
      });

      const footerWrapper = getFooterWrapper(wrapper);

      expect(footerWrapper.find(AkButton).first().prop('isDisabled')).to.equal(true);
    });
  });

  describe('saving', () => {
    let wrapper: ShallowWrapper;
    let JSDCheckbox: ShallowWrapper;
    let jiraSoftwareCheckbox: ShallowWrapper;

    beforeEach(() => {
      wrapper = getWrapper({
        loading: false,
        error: false,
        currentUser: {
          id: '2',
        },
        user: {
          productAccess: [
            generateProductAccess({
              productId: 'jira-servicedesk',
              productName: 'Jira Service Desk',
              accessLevel: 'NONE',
              __typename: 'ProductAccess',
            }),
            generateProductAccess({
              productId: 'jira-software',
              productName: 'Jira Software',
              accessLevel: 'ADMIN',
              __typename: 'ProductAccess',
            }),
          ],
        },
      });

      const productAccesses = wrapper.find(ProductAccess).dive();

      JSDCheckbox = productAccesses
        .findWhere(n => n.key() === 'jira-servicedesk')
        .find(AkCheckbox)
        .dive();

      jiraSoftwareCheckbox = productAccesses
        .findWhere(n => n.key() === 'jira-software')
        .find(AkCheckbox)
        .dive();
    });

    describe('success', () => {
      it('should run a mutation for both revoke and grant', async () => {
        // we have to find a real Checkbox, not to simulate event on Analytics Wrapper
        shallowUntilTarget(JSDCheckbox, 'Checkbox').simulate('change', {
          target: {
            checked: true,
            name: 'jira-servicedesk',
          },
        });

        shallowUntilTarget(jiraSoftwareCheckbox, 'Checkbox').simulate('change', {
          target: {
            checked: false,
            name: 'jira-software',
          },
        });

        expect(grantAccessMutationStub.callCount).to.equal(0);
        expect(revokeAccessMutationStub.callCount).to.equal(0);
        expect(showFlagSpy.callCount).to.equal(0);

        const footerWrapper = getFooterWrapper(wrapper);
        footerWrapper.find(AkButton).first().simulate('click');

        await waitUntil(() => grantAccessMutationStub.callCount > 0);

        expect(grantAccessMutationStub.callCount).to.equal(1);
        expect(revokeAccessMutationStub.callCount).to.equal(1);
        expect(showFlagSpy.callCount).to.equal(1);
      });

      it('should trigger a UI event on submission', async () => {
        shallowUntilTarget(JSDCheckbox, 'Checkbox').simulate('change', {
          target: {
            checked: true,
            name: 'jira-servicedesk',
          },
        });

        shallowUntilTarget(jiraSoftwareCheckbox, 'Checkbox').simulate('change', {
          target: {
            checked: false,
            name: 'jira-software',
          },
        });

        expect(sendUIEventSpy.callCount).to.equal(0);

        const footerWrapper = getFooterWrapper(wrapper);
        footerWrapper.find(AkButton).first().simulate('click');

        await waitUntil(() => sendTrackEventSpy.callCount > 0);

        expect(sendUIEventSpy.callCount).to.equal(1);
        expect(sendUIEventSpy.getCalls()[0].args[0].data.attributes).to.deep.equal({
          productAccessAfter: [
            'jira-servicedesk',
          ],
          productAccessBefore: [
            'jira-software',
          ],
          userId: '1',
          userRole: 'basic',
          userStatus: '',
        });
      });

      it('should trigger a track event on submission', async () => {
        shallowUntilTarget(JSDCheckbox, 'Checkbox').simulate('change', {
          target: {
            checked: true,
            name: 'jira-servicedesk',
          },
        });

        shallowUntilTarget(jiraSoftwareCheckbox, 'Checkbox').simulate('change', {
          target: {
            checked: false,
            name: 'jira-software',
          },
        });

        expect(sendTrackEventSpy.callCount).to.equal(0);

        const footerWrapper = getFooterWrapper(wrapper);
        footerWrapper.find(AkButton).first().simulate('click');

        await waitUntil(() => sendTrackEventSpy.callCount > 0);

        expect(sendTrackEventSpy.callCount).to.equal(1);
        expect(sendTrackEventSpy.getCalls()[0].args[0].data.attributes).to.deep.equal({
          productAccessAfter: [
            'jira-servicedesk',
          ],
          productAccessBefore: [
            'jira-software',
          ],
          userId: '1',
          userRole: 'basic',
          userStatus: '',
        });
      });

      describe('flag', () => {
        it('should show both messages when both grant and revoke', async () => {
          shallowUntilTarget(JSDCheckbox, 'Checkbox').simulate('change', {
            target: {
              checked: true,
              name: 'jira-servicedesk',
            },
          });

          shallowUntilTarget(jiraSoftwareCheckbox, 'Checkbox').simulate('change', {
            target: {
              checked: false,
              name: 'jira-software',
            },
          });

          const footerWrapper = getFooterWrapper(wrapper);
          footerWrapper.find(AkButton).first().simulate('click');

          await waitUntil(() => showFlagSpy.callCount > 0);

          expect(showFlagSpy.calledWithMatch({
            description: 'Party Corgi now has access to 1 additional product and no longer has access to 1 product.',
          })).to.equal(true);
        });

        it('should show grant only when only granting', async () => {
          shallowUntilTarget(JSDCheckbox, 'Checkbox').simulate('change', {
            target: {
              checked: true,
              name: 'jira-servicedesk',
            },
          });

          const footerWrapper = getFooterWrapper(wrapper);
          footerWrapper.find(AkButton).first().simulate('click');

          await waitUntil(() => showFlagSpy.callCount > 0);

          expect(showFlagSpy.calledWithMatch({
            description: 'Party Corgi now has access to 1 additional product.',
          })).to.equal(true);
        });

        it('should show revoke only when only revoking', async () => {
          shallowUntilTarget(jiraSoftwareCheckbox, 'Checkbox').simulate('change', {
            target: {
              checked: false,
              name: 'jira-software',
            },
          });

          const footerWrapper = getFooterWrapper(wrapper);
          footerWrapper.find(AkButton).first().simulate('click');

          await waitUntil(() => showFlagSpy.callCount > 0);

          expect(showFlagSpy.calledWithMatch({
            description: 'Party Corgi no longer has access to 1 product.',
          })).to.equal(true);
        });
      });
    });

    describe('error', () => {
      it('should show an error flag', async () => {
        const failedMutation = Promise.reject(new Error('Error'));

        grantAccessMutationStub = sandbox.stub().returns(failedMutation);

        const errorWrapper = getWrapper({
          loading: false,
          error: false,
          currentUser: {
            id: '2',
          },
          user: {
            productAccess: [
              generateProductAccess({
                productId: 'jira-servicedesk',
                productName: 'Jira Service Desk',
                accessLevel: 'NONE',
                __typename: 'ProductAccess',
              }),
            ],
          },
        });

        errorWrapper
          .find(ProductAccess)
          .dive()
          .find(AkCheckbox)
          .findWhere(n => n.prop('name') === 'jira-servicedesk')
          .simulate('change', {
            target: {
              checked: true,
              name: 'jira-servicedesk',
            },
          });

        const footerWrapper = getFooterWrapper(errorWrapper);
        footerWrapper.find(AkButton).first().simulate('click');

        await waitUntil(() => showFlagSpy.callCount > 0);

        await failedMutation
          .then(() => assert.fail())
          .catch(e => {
            expect(e instanceof Error).to.equal(true);
          });

        expect(showFlagSpy.calledWithMatch({
          title: 'Something went wrong',
        })).to.equal(true);
      });
    });
  });
});
