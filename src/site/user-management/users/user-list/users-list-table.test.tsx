import { expect } from 'chai';
import { mount, shallow, ShallowWrapper } from 'enzyme';
import * as moment from 'moment';
import * as React from 'react';
import { MockedProvider } from 'react-apollo/test-utils';
import * as sinon from 'sinon';

import {
  DynamicTableStateless as AkDynamicTableStateless,
} from '@atlaskit/dynamic-table';

import { Account } from 'common/account';
import { Button as AnalyticsButton, ScreenEventSender } from 'common/analytics';

import { User } from '../../../../schema/schema-types';
import { createMockContext, createMockIntlProp } from '../../../../utilities/testing';
import { UsersListTableImpl } from './users-list-table';

describe('Users list page layout', () => {

  const sandbox = sinon.sandbox.create();
  const onSetPageSpy = sandbox.spy();

  afterEach(() => {
    sandbox.reset();
  });

  function createShallowUsersList({ totalUsers, users, hasQuery = false, currentUserId = '1200', displayNameQuery, userTypesSelected = [], productKeysSelected = [], adminTypesSelected = [] }: { totalUsers: number, users: User[], hasQuery?: boolean, currentUserId?: string, displayNameQuery?: string, userTypesSelected?: string[], productKeysSelected?: string[], adminTypesSelected?: string[] }): ShallowWrapper {
    return shallow(
      <UsersListTableImpl
        cloudId="dummy-id"
        totalUsers={totalUsers}
        users={users}
        onSetPage={onSetPageSpy}
        isLoading={false}
        currentPage={1}
        isCurrentUserSystemAdmin={false}
        hasQuery={hasQuery}
        history={undefined as any}
        intl={createMockIntlProp()}
        currentUserId={currentUserId}
        userTypesSelected={userTypesSelected}
        productKeysSelected={productKeysSelected}
        adminTypesSelected={adminTypesSelected}
        displayNameQuery={displayNameQuery}
      />, { disableLifecycleMethods: false });
  }

  const getEmptyStateWrapper = (wrapper: ShallowWrapper): ShallowWrapper => {
    const statelessTableProps = wrapper.find(AkDynamicTableStateless).props();
    if (statelessTableProps.emptyView) {
      return shallow(
        <MockedProvider>
          {statelessTableProps.emptyView as React.ReactElement<any>}
        </MockedProvider>,
      );
    }

    return wrapper;
  };

  it('Should not render anything if the total number of users is less than 5, and there are no users', () => {
    const wrapper = createShallowUsersList({ totalUsers: 3, users: [] });
    const table = wrapper.find(AkDynamicTableStateless);
    expect(table.length).to.equal(1);
    expect(table.props().head).to.equal(undefined);
    expect(table.props().rows).to.equal(undefined);
    expect(table.props().emptyView).to.equal(undefined);
  });

  it('Should render empty state if there is a query parameter, and total number of users is less than 5', () => {
    const users: User[] = [
      { id: '1231', active: true, displayName: 'Jones', email: 'jones@acme.com', system: false, hasVerifiedEmail: true },
      { id: '1232', active: false, displayName: 'James', email: 'james@acme.com', system: false, hasVerifiedEmail: true },
    ];

    const wrapper = createShallowUsersList({ totalUsers: 2, users, hasQuery: true });
    const table = wrapper.find(AkDynamicTableStateless);
    expect(table.length).to.equal(1);
    expect(table.props().head).to.not.equal(undefined);
    expect(table.props().rows).to.not.equal(undefined);
    expect(table.props().emptyView).to.not.equal(undefined);
  });

  it('should render an invite users button in the empty state if there are no users', () => {
    const wrapper = createShallowUsersList({ totalUsers: 0, users: [], hasQuery: true });
    const emptyState = mount(wrapper.find(AkDynamicTableStateless).prop('emptyView') as React.ReactElement<any>,
      createMockContext({ apollo: true }),
    );

    expect(emptyState.length).to.equal(1);

    expect(emptyState.find(AnalyticsButton).length).to.equal(1);
  });

  it('should send a UI event when the invite users button is clicked', () => {
    const wrapper = createShallowUsersList({ totalUsers: 0, users: [], hasQuery: true, displayNameQuery: 'Mr. Corgi' });
    const emptyState = mount(wrapper.find(AkDynamicTableStateless).prop('emptyView') as React.ReactElement<any>,
      createMockContext({ apollo: true }),
    );
    expect(emptyState.length).to.equal(1);

    const inviteUsersButton = emptyState.find(AnalyticsButton);

    expect(inviteUsersButton.props().analyticsData!.actionSubjectId).to.equal('noSearchResultsInviteUsers');
    expect(inviteUsersButton.props().analyticsData!.attributes).to.deep.equal({
      countSearchCharacters: 'Mr. Corgi'.length,
    });
  });

  describe('Table with users', () => {

    it('Should contain headers', () => {
      const users: User[] = [
        { id: '1231', active: true, displayName: 'Jones', email: 'jones@acme.com', system: false, hasVerifiedEmail: true },
      ];

      const wrapper = createShallowUsersList({ totalUsers: 1, users });
      const table = wrapper.find(AkDynamicTableStateless);
      expect(table.length).to.equal(1);
      const head = table.props().head as any;

      expect(head.cells[0].content).to.equal('User');
      expect(head.cells[1].content).to.equal('Last seen on site');
      expect(head.cells[2].content).to.equal('Status');
      expect(head.cells[3].content).to.equal('Actions');
    });

    it('Account component should have enabled / disabled user state', () => {
      const users: User[] = [
        { id: '1231', active: true, displayName: 'Jones', email: 'jones@acme.com', system: false, hasVerifiedEmail: true },
        { id: '1232', active: false, displayName: 'James', email: 'james@acme.com', system: false, hasVerifiedEmail: true },
      ];

      const wrapper = createShallowUsersList({ totalUsers: 2, users });
      const table = wrapper.find(AkDynamicTableStateless);
      expect(table.length).to.equal(1);
      const rows = table.props().rows as any;
      expect(rows.length).to.equal(2);

      const first = shallow(rows[0].cells[0].content);
      expect(first.find(Account).exists()).to.equal(true);
      expect(first.find(Account).props().isDisabled).to.equal(false);

      const second = shallow(rows[1].cells[0].content);
      expect(second.find(Account).exists()).to.equal(true);
      expect(second.find(Account).props().isDisabled).to.equal(true);
    });

    it('Should have a user status row', () => {
      const users: User[] = [
        { id: '1231', active: true, displayName: 'Jones', email: 'jones@acme.com', siteAdmin: true, system: false, hasVerifiedEmail: true },
        { id: '1232', active: true, displayName: 'James', email: 'james@acme.com', system: false, hasVerifiedEmail: true },
        { id: '1232', active: true, displayName: 'James', email: 'james@acme.com', system: false, hasVerifiedEmail: true, trustedUser: true },
      ];

      const wrapper = createShallowUsersList({ totalUsers: 2, users });
      const table = wrapper.find(AkDynamicTableStateless);
      expect(table.length).to.equal(1);
      const rows = table.props().rows as any;
      expect(rows.length).to.equal(3);

      expect(shallow(rows[0].cells[0].content).find(Account).exists()).to.equal(true);
      expect(shallow(rows[0].cells[0].content).find(Account).props().lozenges).to.have.length(1);
      expect(shallow(rows[1].cells[0].content).find(Account).exists()).to.equal(true);
      expect(shallow(rows[1].cells[0].content).find(Account).props().lozenges).to.have.length(0);
      expect(shallow(rows[2].cells[0].content).find(Account).exists()).to.equal(true);
      expect(shallow(rows[2].cells[0].content).find(Account).props().lozenges).to.have.length(1);
    });

    it('Should have the status row', () => {
      const users: User[] = [
        { id: '1231', active: true, activeStatus: 'ENABLED', presence: '2017-07-10T14:00:00Z', displayName: 'Jones', email: 'jones@acme.com', system: false, hasVerifiedEmail: true },
        { id: '1232', active: true, activeStatus: 'ENABLED', presence: null, displayName: 'James', email: 'james@acme.com', system: false, hasVerifiedEmail: true },
        { id: '1234', active: false, activeStatus: 'BLOCKED', presence: null, displayName: 'James', email: 'james@acme.com', system: false, hasVerifiedEmail: true },
        { id: '1235', active: false, activeStatus: 'DISABLED', presence: null, displayName: 'James', email: 'james@acme.com', system: false, hasVerifiedEmail: true },
      ];

      const wrapper = createShallowUsersList({ totalUsers: 4, users });
      const table = wrapper.find(AkDynamicTableStateless);
      expect(table.length).to.equal(1);
      const rows = table.props().rows as any;
      expect(rows.length).to.equal(4);

      expect(rows[0].cells[2].content.props.children).to.equal('Has site access');
      expect(rows[1].cells[2].content.props.children).to.equal('Invited');
      expect(rows[2].cells[2].content.props.children).to.equal('Account disabled');
      expect(rows[3].cells[2].content.props.children).to.equal('No site access');
    });

    it('Presence data should display correctly', () => {
      const users: User[] = [
        { id: '1231', active: true, activeStatus: 'ENABLED', presence: '2017-07-10T14:00:00Z', displayName: 'Jones', email: 'jones@acme.com', system: false, hasVerifiedEmail: true },
        { id: '1232', active: true, activeStatus: 'ENABLED', presence: null, displayName: 'James', email: 'james@acme.com', system: false, hasVerifiedEmail: true },
        { id: '1233', active: false, activeStatus: 'DISABLED', presence: '2017-07-10T14:00:00Z', displayName: 'Janey', email: 'janey@acme.com', system: false, hasVerifiedEmail: true },
        { id: '1234', active: false, activeStatus: 'DISABLED', presence: null, displayName: 'Jinsa', email: 'jinsa@acme.com', system: false, hasVerifiedEmail: true },
        { id: '1235', active: false, activeStatus: 'BLOCKED', presence: '2017-07-10T14:00:00Z', displayName: 'Jinsa', email: 'jinsa@acme.com', system: false, hasVerifiedEmail: true },
        { id: '1236', active: false, activeStatus: 'BLOCKED', presence: null, displayName: 'Jinsa', email: 'jinsa@acme.com', system: false, hasVerifiedEmail: true },
      ];
      const wrapper = createShallowUsersList({ totalUsers: 6, users });
      const table = wrapper.find(AkDynamicTableStateless);
      expect(table.length).to.equal(1);
      const rows = table.props().rows as any;
      expect(rows.length).to.equal(6);

      expect(moment(new Date(rows[0].cells[1].content.props.children)).isValid()).to.equal(true);
      expect(shallow(rows[1].cells[1].content.props.children).text()).to.equal('Invitation pending');
      expect(moment(new Date(rows[2].cells[1].content.props.children)).isValid()).to.equal(true);
      expect(shallow(rows[3].cells[1].content.props.children).text()).to.contain('Not seen on this site');
      expect(moment(new Date(rows[4].cells[1].content.props.children)).isValid()).to.equal(true);
      expect(shallow(rows[5].cells[1].content.props.children).text()).to.contain('Not seen on this site');
    });
    it('Passes isCurrentUser=true for the row that has the current user in it', () => {
      const users: User[] = [
        { id: '1231', active: true, activeStatus: 'ENABLED', presence: '2017-07-10T14:00:00Z', displayName: 'Jones', email: 'jones@acme.com', system: false, hasVerifiedEmail: true },
      ];
      const wrapper = createShallowUsersList({ totalUsers: 1, users, currentUserId: '1231' });
      const table = wrapper.find(AkDynamicTableStateless);
      const rows = table.props().rows as any;
      expect(rows[0].cells[3].content.props.isCurrentUser).to.equal(true);
    });

    it('Passes isCurrentUser=false for all rows that without the current user in it', () => {
      const users: User[] = [
        { id: '1231', active: true, activeStatus: 'ENABLED', presence: '2017-07-10T14:00:00Z', displayName: 'Jones', email: 'jones@acme.com', system: false, hasVerifiedEmail: true },
        { id: '1232', active: true, activeStatus: 'ENABLED', presence: null, displayName: 'James', email: 'james@acme.com', system: false, hasVerifiedEmail: true },
        { id: '1233', active: false, activeStatus: 'DISABLED', presence: '2017-07-10T14:00:00Z', displayName: 'Janey', email: 'janey@acme.com', system: false, hasVerifiedEmail: true },
        { id: '1234', active: false, activeStatus: 'DISABLED', presence: null, displayName: 'Jinsa', email: 'jinsa@acme.com', system: false, hasVerifiedEmail: true },
        { id: '1235', active: false, activeStatus: 'BLOCKED', presence: '2017-07-10T14:00:00Z', displayName: 'Jinsa', email: 'jinsa@acme.com', system: false, hasVerifiedEmail: true },
        { id: '1236', active: false, activeStatus: 'BLOCKED', presence: null, displayName: 'Jinsa', email: 'jinsa@acme.com', system: false, hasVerifiedEmail: true },
      ];
      const wrapper = createShallowUsersList({ totalUsers: 6, users, currentUserId: '1231' });
      const table = wrapper.find(AkDynamicTableStateless);
      const rows = table.props().rows as any;
      expect(rows[0].cells[3].content.props.isCurrentUser).to.equal(true);
      expect(rows[1].cells[3].content.props.isCurrentUser).to.equal(false);
      expect(rows[2].cells[3].content.props.isCurrentUser).to.equal(false);
      expect(rows[3].cells[3].content.props.isCurrentUser).to.equal(false);
      expect(rows[4].cells[3].content.props.isCurrentUser).to.equal(false);
      expect(rows[5].cells[3].content.props.isCurrentUser).to.equal(false);
    });
  });

  it('should send correct props to screen event sender on empty table', () => {
    const wrapper = createShallowUsersList({ totalUsers: 3, users: [], displayNameQuery: 'Josh Van Arkhipau', hasQuery: true });
    const emptyState = getEmptyStateWrapper(wrapper).dive();

    expect(emptyState.find(ScreenEventSender).prop('event')).to.deep.equal({
      data: {
        name: 'userListNoResultsScreen',
        attributes: {
          adminTypesSelected: [],
          countSearchCharacters: 17,
          productKeysSelected: [],
          userTypesSelected: [],
        },
      },
    });
  });
});
