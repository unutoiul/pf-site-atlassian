import { assert, expect } from 'chai';
import { mount, shallow } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import AkButton from '@atlaskit/button';

import { addedUserToGroupTrackEventData, addUserToGroupsCancelUIEventData, addUserToGroupsConfirmUIEventData, Button as AnalyticsButton, ScreenEventSender } from 'common/analytics';

import { Group, GroupsPickerModalQuery, ManagementAccess, OwnerType, SitePrivilege } from '../../../../schema/schema-types';
import { createMockContext, createMockIntlProp } from '../../../../utilities/testing';
// tslint:disable-next-line:disallow-relative-import-for-common
import { GroupsPickerModal } from '../../common/groups-picker-modal';
import { UsersAddUserToGroupModalImpl } from './users-add-user-to-group-modal';
const noop = () => null;

const group = {
  id: 'one',
  displayName: 'one',
  description: '',
  managementAccess: 'ALL' as ManagementAccess,
  sitePrivilege: 'NONE' as SitePrivilege,
  unmodifiable: false,
  ownerType: null,
};

describe('UsersAddUserToGroupModalImpl', () => {
  const sandbox = sinon.sandbox.create();
  const analyticsStub = {
    sendUIEvent: sandbox.spy(),
    sendTrackEvent: sandbox.spy(),
  };
  const showFlagSpy = sandbox.spy();
  const dismissSpy = sandbox.spy();
  const onAddMembershipSpy = sandbox.spy();

  beforeEach(() => {
    sandbox.reset();
  });

  const defaultProps = {
    isOpen: true,
    cloudId: 'test-id',
    currentPage: 1,
    userId: 'test-id',
    username: 'test-name',
    intl: createMockIntlProp(),
    analyticsClient: analyticsStub as any,
    userStates: [],
    showFlag: showFlagSpy,
    hideFlag: noop,
    onDialogDismissed: dismissSpy,
    onAddMembership: onAddMembershipSpy,
  };

  interface GroupOptionsInput {
    ownerType?: OwnerType;
    sitePrivilege?: SitePrivilege;
    unmodifiable?: boolean;
    managementAccess?: ManagementAccess;
  }

  const generateGroup = (options?: GroupOptionsInput): Group & { __typename } => {
    return {
      id: '1234',
      name: 'Party Corgi',
      description: 'Best Party',
      userTotal: 0,
      productPermissions: [],
      defaultForProducts: [],
      sitePrivilege: 'NONE',
      unmodifiable: false,
      managementAccess: 'ALL',
      ownerType: null,
      __typename: 'Group',
      ...options,
    };
  };

  const generateGroupList = (
    totalDefaultGroups: number,
    totalSpecialGroups: number,
    options?: GroupOptionsInput,
  ): Partial<GroupsPickerModalQuery> => {

    const groups = new Array<any>();

    for (let i = 0; i < totalDefaultGroups + totalSpecialGroups; i++) {
      i + 1 > totalDefaultGroups ? groups.push(generateGroup(options)) : groups.push(generateGroup());
    }

    return {
      groupList: {
        total: totalDefaultGroups + totalSpecialGroups,
        groups,
        __typename: 'GroupList',
      },
    };
  };

  const generateData = (totalDefaultGroups, totalSpecialGroups, options?) => {
    return {
      ...generateGroupList(totalDefaultGroups, totalSpecialGroups, options),
    } as any;
  };

  describe('When performing actions that trigger analytics', () => {
    it('Should trigger the addUserToGroupsConfirmUIEvent when confirming to add groups', () => {
      const wrapper = shallow((
        <UsersAddUserToGroupModalImpl
          {...defaultProps}
          data={generateData(11, 12, { sitePrivilege: 'SITE_ADMIN' })}
        />
      ));

      expect(wrapper.find(GroupsPickerModal).exists()).equals(true);

      const footer = mount(wrapper.find(GroupsPickerModal).props().renderFooter(() => [group]) as any, createMockContext({ apollo: true }));
      const add = footer.find(AnalyticsButton);
      add.simulate('click');

      expect(analyticsStub.sendUIEvent.called).to.equal(true);
      expect(analyticsStub.sendUIEvent.getCall(0).args[0]).to.deep.equal({
        cloudId: 'test-id',
        data: addUserToGroupsConfirmUIEventData({
          userState: [],
          userId: 'test-id',
          groupIds: [group.id],
          groupCount: 1,

        }),
      });
    });

    it('Should trigger the addUserToGroupsCancelUIEvent when cancelling adding groups', () => {
      const wrapper = shallow((
        <UsersAddUserToGroupModalImpl
          {...defaultProps}
          data={generateData(11, 12, { sitePrivilege: 'SITE_ADMIN' })}
        />
      ));

      expect(wrapper.find(GroupsPickerModal).exists()).equals(true);
      wrapper.setState({ selectedGroups: ['group-id'] });
      const footer = mount(wrapper.find(GroupsPickerModal).props().renderFooter(() => []) as any, createMockContext({ apollo: true }));
      const add = footer.find(AkButton).filterWhere(btn => btn.children().text() === 'Cancel');
      expect(add.exists()).to.equal(true);
      add.simulate('click');
      expect(analyticsStub.sendUIEvent.called).to.equal(true);
      expect(analyticsStub.sendUIEvent.calledWith({
        cloudId: 'test-id',
        data: addUserToGroupsCancelUIEventData({
          userState: [],
          userId: 'test-id',

        }),
      })).to.equal(true);
    });
  });

  it('should trigger the addedUserToGroupTrackEvent, call onAddMembership, show a flag, and dismiss the dialog after adding a group', async () => {
    const promise = Promise.resolve(true);
    const mutateStub = sandbox.stub().returns(promise);
    const wrapper = shallow((
      <UsersAddUserToGroupModalImpl
        {...defaultProps}
        mutate={mutateStub}
        data={generateData(11, 12, { sitePrivilege: 'SITE_ADMIN' })}
      />
    ));

    expect(wrapper.find(GroupsPickerModal).exists()).equals(true);
    const footer = mount(wrapper.find(GroupsPickerModal).props().renderFooter(() => [group]) as any, createMockContext({ apollo: true }));
    const add = footer.find(AnalyticsButton);
    add.simulate('click');

    await promise;

    expect(mutateStub.callCount).to.equal(1);
    expect(analyticsStub.sendTrackEvent.called).to.equal(true);
    expect(analyticsStub.sendTrackEvent.calledWith({
      cloudId: 'test-id',
      data: addedUserToGroupTrackEventData({
        userState: [],
        userId: 'test-id',
        groupsAddedCount: 1,
      }),
    })).to.equal(true);
    expect(onAddMembershipSpy.callCount).to.equal(1);
    expect(showFlagSpy.calledWithMatch({ title: 'User added to groups' })).to.equal(true);
    expect(dismissSpy.callCount).to.equal(1);
  });

  it('should show a flag, and dismiss the dialog after failing to add a group', async () => {
    const failedMutation = Promise.reject(new Error('oh noes'));
    const mutateStub = sandbox.stub().returns(failedMutation);
    const wrapper = shallow((
      <UsersAddUserToGroupModalImpl
        {...defaultProps}
        mutate={mutateStub}
        data={{} as any}
      />
    ));

    expect(wrapper.find(GroupsPickerModal).exists()).equals(true);
    const footer = mount(wrapper.find(GroupsPickerModal).props().renderFooter(() => [group]) as any, createMockContext({ apollo: true }));
    const add = footer.find(AnalyticsButton);
    add.simulate('click');

    await failedMutation
      .then(() => assert.fail())
      .catch(e => {
        expect(e instanceof Error).to.equal(true);
      });

    expect(mutateStub.callCount).to.equal(1);
    expect(showFlagSpy.calledWithMatch({ title: 'Unable to add user to groups. Try again later.' })).to.equal(true);
    expect(dismissSpy.callCount).to.equal(1);
  });

  it('should send correct props to screen event sender', () => {
    const wrapper = mount(
      <UsersAddUserToGroupModalImpl
        {...defaultProps}
      />,
      createMockContext({ intl: true, apollo: true }),
    );

    const screenEventSender = wrapper.find(ScreenEventSender);

    expect(screenEventSender).to.have.lengthOf(1);
    expect(wrapper.find(ScreenEventSender).props().event).to.deep.equal({
      data: {
        name: 'userListAddToGroupModal',
        attributes: {
          userId: 'test-id',
          countGroupsAvailable: 0,
        },
      },
    });
  });
});
