import { expect } from 'chai';
import * as sinon from 'sinon';

import { ActiveStatus, SiteUsersListQueryVariables } from '../../../../schema/schema-types';
import { updateActiveStatus } from './users-page-active-status.updater';

describe('Optimistically activating and deactivating a user', () => {
  let writeQuerySpy;

  const sandbox = sinon.sandbox.create();

  beforeEach(() => {
    writeQuerySpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  function generateStore(activeStatus: ActiveStatus, active: boolean) {
    return {
      readQuery: sinon.stub().returns({
        siteUsersList: {
          users: [
            {
              __typename: 'User',
              id: 'DUMMY-USER-ID',
              displayName: `Prince Corgi`,
              email: 'corgi@happyfriends.paws',
              active,
              presence: '2017-07-10T14:00:00Z',
              admin: false,
              activeStatus,
              system: false,
            },
          ],
        },
      }),
      writeQuery: writeQuerySpy,
    } as any;
  }

  it('Should deactivate a user', async () => {
    const variables = {
      cloudId: 'DUMMY-CLOUD-ID',
      userId: 'DUMMY-USER-ID',
      input: {
        count: 20,
        start: 1,
      },
    } as SiteUsersListQueryVariables;

    updateActiveStatus(generateStore('ENABLED', true), 'DUMMY-USER-ID', true, variables);
    expect(writeQuerySpy.callCount).to.equal(1);
    expect(writeQuerySpy.args[0][0].data.siteUsersList.users[0].active).to.equal(false);
    expect(writeQuerySpy.args[0][0].data.siteUsersList.users[0].activeStatus).to.equal('DISABLED');
  });

  it('Should activate a user', async () => {
    const variables = {
      cloudId: 'DUMMY-CLOUD-ID',
      userId: 'DUMMY-USER-ID',
      input: {
        count: 20,
        start: 1,
      },
    };

    updateActiveStatus(generateStore('DISABLED', false), 'DUMMY-USER-ID', false, variables);
    expect(writeQuerySpy.callCount).to.equal(1);
    expect(writeQuerySpy.args[0][0].data.siteUsersList.users[0].active).to.equal(true);
    expect(writeQuerySpy.args[0][0].data.siteUsersList.users[0].activeStatus).to.equal('ENABLED');
  });

  it('Should do nothing if the user is blocked on a global level', () => {
    const variables = {
      cloudId: 'DUMMY-CLOUD-ID',
      userId: 'DUMMY-USER-ID',
      input: {
        count: 20,
        start: 1,
      },
    };

    updateActiveStatus(generateStore('BLOCKED', false), 'DUMMY-USER-ID', true, variables);
    expect(writeQuerySpy.args[0][0].data.siteUsersList.users[0].active).to.equal(false);
    expect(writeQuerySpy.args[0][0].data.siteUsersList.users[0].activeStatus).to.equal('BLOCKED');
  });
  it('Should do nothing if the selected user is not in the query', () => {
    const variables = {
      cloudId: 'DUMMY-CLOUD-ID',
      userId: 'OTHER-USER-ID',
      input: {
        count: 20,
        start: 1,
      },
    };

    updateActiveStatus(generateStore('DISABLED', false), 'OTHER-USER-ID', true, variables);
    expect(writeQuerySpy.args[0][0].data.siteUsersList.users[0].active).to.equal(false);
    expect(writeQuerySpy.args[0][0].data.siteUsersList.users[0].activeStatus).to.equal('DISABLED');
  });

  it('Should do nothing if reading the query results in an error', () => {
    const variables = {
      cloudId: 'DUMMY-CLOUD-ID',
      userId: 'DUMMY-USER-ID',
      input: {
        count: 20,
        start: 1,
      },
    };

    updateActiveStatus(
      { readQuery: sinon.stub().throws() } as any,
      'DUMMY-USER-ID',
      false,
      variables,
    );

    expect(writeQuerySpy.callCount).to.equal(0);
  });
});
