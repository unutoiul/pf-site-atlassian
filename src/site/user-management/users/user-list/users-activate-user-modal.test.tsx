import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';

import { ScreenEventSender } from 'common/analytics';

import { User } from '../../../../schema/schema-types/schema-types';
import { createMockContext, createMockIntlProp, noop } from '../../../../utilities/testing';
import { UsersActivateUserModalImpl } from './users-activate-user-modal';

describe('UsersActivateUserModalImpl', () => {
  const defaultProps = (isActive) => ({
    cloudId: 'DUMMY_CLOUD_ID',
    user: {
      id: 'DUMMY_USER_ID',
      displayName: 'HappyCorgi',
      email: 'happyCorgi@gmail.com',
      active: isActive,
      system: false,
    } as User,
    isActive,
    isOpen: true,
    onDismiss: noop,
    showFlag: noop,
    hideFlag: noop,
    intl: createMockIntlProp(),
    analyticsClient: noop as any,
    match: {} as any,
    location: {} as any,
    history: {} as any,
  });

  it('should send revoke access screen event when user is active', () => {
    const wrapper = mount(
      <UsersActivateUserModalImpl
        {...defaultProps(true)}
      />,
      createMockContext({ intl: true, apollo: true }),
    );

    expect(wrapper.find(ScreenEventSender).prop('event')).to.deep.equal({
      data: {
        attributes: {
          userId: 'DUMMY_USER_ID',
        },
        name: 'userListRevokeAccessModal',
      },
    });
  });

  it('should send grant access screen event when user is inactive', () => {
    const wrapper = mount(
      <UsersActivateUserModalImpl
        {...defaultProps(false)}
      />,
      createMockContext({ intl: true, apollo: true }),
    );

    expect(wrapper.find(ScreenEventSender).prop('event')).to.deep.equal({
      data: {
        attributes: {
          userId: 'DUMMY_USER_ID',
        },
        name: 'userListGrantAccessModal',
      },
    });
  });
});
