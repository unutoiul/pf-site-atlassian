import { DataProxy } from 'apollo-cache';
import { parse } from 'query-string';
import * as React from 'react';
import { graphql, MutationFunc } from 'react-apollo';
import { defineMessages, FormattedHTMLMessage, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';

import { ButtonGroup as AkButtonGroup } from '@atlaskit/button';

import { AnalyticsClientProps, Button as AnalyticsButton, ScreenEventSender, userListCancelGrantSiteAccess, userListCancelRevokeSiteAccess, userListGrantAccessModalScreenEvent, userListGrantAccessModalSiteAccessGranted, userListGrantAccessModalSiteAccessRevoked, userListGrantSiteAccess, userListRevokeAccessModalScreenEvent, userListRevokeSiteAccess, withAnalyticsClient } from 'common/analytics';
import { createErrorIcon, createSuccessIcon } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';
import { ModalDialog } from 'common/modal';
import { getStart, sanitizePage } from 'common/pagination';

import {
  ActivateUserMutation,
  ActivateUserMutationVariables,
  DeactivateUserMutation,
  DeactivateUserMutationVariables,
  SiteUsersListQueryVariables,
  User,
} from '../../../../schema/schema-types';
import { ROWS_PER_PAGE } from '../user-list/users-list-constants';
import { getUserRole } from '../users-common/get-user-role';
import activateUserMutation from './users-page-activate.mutation.graphql';
import { updateActiveStatus } from './users-page-active-status.updater';
import deactivateUserMutation from './users-page-deactivate.mutation.graphql';

const messages = defineMessages({

  activate: {
    id: 'users.list.activate.user.modal.activate',
    description: 'An action that the admin can click on in the users table to activate site user',
    defaultMessage: 'Grant site access',
  },
  deactivate: {
    id: 'users.list.activate.user.modal.deactivate',
    description: 'An action that the admin can click on in the users table to deactivate site user',
    defaultMessage: 'Revoke site access',
  },
  activateTitle: {
    id: 'users.list.activate.user.modal.title',
    description: 'Title - indicating the intention to activate a user',
    defaultMessage: 'Grant site access to this user?',
  },
  deactivateTitle: {
    id: 'users.list.deactivate.user.modal.title',
    description: 'Title - indicating the intention to deactivate a user',
    defaultMessage: 'Revoke site access for this user?',
  },
  deactivateDescription: {
    id: 'users.list.activate.user.modal.deactivate.description',
    defaultMessage: `<p>You are about to revoke site access for {userName}.</p><p>This user won't be able to use your site. You can grant them access at any time.</p>`,
  },
  activateDescription: {
    id: 'users.list.activate.user.modal.activate.description',
    defaultMessage: 'You are about to grant site access to {userName}.',
    description: 'Stating that the specified user will now be active on the site.',
  },
  cancelButton: {
    id: 'users.list.activate.user.modal.cancel',
    defaultMessage: 'Cancel',
    description: 'The button that cancels and closes the dialog.',
  },
  activateSuccessTitle: {
    id: 'users.list.activate.user.modal.success.flag.title',
    description: 'Success flag text for activating user.',
    defaultMessage: '{userName} has been granted access to this site.',
  },
  activateSuccessDescription: {
    id: 'users.list.activate.user.modal.success.flag.description',
    description: 'Success flag text for activating user.',
    defaultMessage: '{userName} has been granted access to this site, and all previous privileges have been restored.',
  },
  deactivateSuccessTitle: {
    id: 'users.list.deactivate.user.modal.success.flag.title',
    description: 'Success flag text for deactivating user.',
    defaultMessage: '{userName} has been revoked access to this site.',
  },
  deactivateSuccessDescription: {
    id: 'users.list.deactivate.user.modal.success.flag.description',
    description: 'Success flag text for activating user.',
    defaultMessage: '{userName} now has no access to this site and cannot access their applications or receive any notifications. You will not be billed for this user.',
  },
  activateErrorTitle: {
    id: 'users.list.activate.user.modal.error.flag.title',
    description: 'Error flag text for activating user.',
    defaultMessage: 'We couldn\'t grant access to this site for {userName}.',
  },
  deactivateErrorTitle: {
    id: 'users.list.deactivate.user.modal.error.flag.title',
    description: 'Error flag text for deactivating user.',
    defaultMessage: 'We couldn\'t revoke access to this site for {userName}.',
  },
  badRequestErrorToActivate: {
    id: 'users.list.activate.user.modal.user.bad.request.error',
    description: 'Message that displays when the user is not active on the site.',
    defaultMessage: 'You do not have permission to modify this user or your license count will be exceeded if the user is granted access to this site.',
  },
  badRequestErrorToDeactivate: {
    id: 'users.list.activate.user.modal.user.no.permission.error',
    description: 'Message that displays when the user is not active on the site.',
    defaultMessage: 'You do not have permission to modify this user.',
  },
  activateUnknownError: {
    id: 'users.list.activate.user.modal.user.unknown.error',
    description: 'Message that displays when the user is not active on the site.',
    defaultMessage: 'We could not grant site access to this user. Contact support or refresh the page to try again.',
  },
  deactivateUnknownError: {
    id: 'users.list.deactivate.user.modal.user.unknown.error',
    description: 'Message that displays when the user is not active on the site.',
    defaultMessage: 'We could not revoke site access for this user. Contact support or refresh the page to try again.',
  },
});

interface MutationProps {
  activateUser?: MutationFunc<ActivateUserMutation, ActivateUserMutationVariables>;
  deactivateUser?: MutationFunc<DeactivateUserMutation, DeactivateUserMutationVariables>;
}

interface OwnProps {
  cloudId: string;
  user: User;
  isActive: boolean;
  isOpen: boolean;
  onDismiss(): void;
}
interface State {
  isRemovingUser: boolean;
}
export type UserActivateModalProps = InjectedIntlProps & FlagProps & OwnProps & MutationProps & RouteComponentProps<{}> & AnalyticsClientProps;

export class UsersActivateUserModalImpl extends React.Component<UserActivateModalProps, State> {
  public readonly state: Readonly<State> = {
    isRemovingUser: false,
  };
  public render() {
    const { formatMessage } = this.props.intl;
    const { user, onDismiss, isOpen, isActive } = this.props;

    const cancelEvent = isActive ? userListCancelRevokeSiteAccess : userListCancelGrantSiteAccess;
    const submitEvent = isActive ? userListRevokeSiteAccess : userListGrantSiteAccess;

    if (!isOpen) {
      return null;
    }

    return (
      <ModalDialog
        isOpen={isOpen}
        width="small"
        onClose={onDismiss}
        header={formatMessage(isActive ? messages.deactivateTitle : messages.activateTitle)}
        footer={

            <AkButtonGroup>
              <AnalyticsButton
                isLoading={this.state.isRemovingUser}
                appearance={isActive ? 'danger' : 'primary'}
                onClick={this.updateUserStatus}
                id="submit-activate-user-modal"
                analyticsData={submitEvent({
                  lastSeenOnSite: this.props.user.presence || '',
                  userId: this.props.user.id,
                  userRole: getUserRole(this.props.user),
                  userStatus: this.props.user.activeStatus || '',
                })}
              >
                {formatMessage(isActive ? messages.deactivate : messages.activate)}
              </AnalyticsButton>
              <AnalyticsButton
                appearance="subtle-link"
                onClick={onDismiss}
                analyticsData={cancelEvent({
                  userId: this.props.user.id,
                  userRole: getUserRole(this.props.user),
                  userStatus: this.props.user.activeStatus || '',
                })}
              >
                {formatMessage(messages.cancelButton)}
              </AnalyticsButton>
            </AkButtonGroup>
        }
      >
        <ScreenEventSender
          event={isActive ? userListRevokeAccessModalScreenEvent({ userId: user.id }) : userListGrantAccessModalScreenEvent({ userId: user.id })}
        >
          {isActive ? (
            <FormattedHTMLMessage {...messages.deactivateDescription} values={{ userName: user.displayName }} />
          ) : (
            <FormattedMessage {...messages.activateDescription} values={{ userName: user.displayName }} />
          )}
        </ScreenEventSender>
      </ModalDialog>
    );
  }

  private updateUserStatus = async () => {
    const { formatMessage } = this.props.intl;
    const { isActive, showFlag, onDismiss, cloudId, location, deactivateUser, activateUser, user } = this.props;

    if (!activateUser || !deactivateUser) {
      return;
    }

    const { page, displayName, activeStatus, productUse, productAdmin, sitePrivilege } = parse(location.search);

    const queryVariables = {
      cloudId,
      input: {
        count: ROWS_PER_PAGE,
        start: getStart(sanitizePage(page), ROWS_PER_PAGE),
        activeStatus,
        displayName,
        productUse,
        productAdmin,
        sitePrivilege,
      },
    } as SiteUsersListQueryVariables;

    const mutationVariables = {
      cloudId,
      userId: user.id,
    } as ActivateUserMutationVariables | DeactivateUserMutationVariables;

    this.setState({ isRemovingUser: true });
    try {
      isActive ? await deactivateUser({
        variables: mutationVariables,
        update: (store: DataProxy) => updateActiveStatus(store, user.id, isActive, queryVariables),
      }) : await activateUser({
        variables: mutationVariables,
        update: (store: DataProxy) => updateActiveStatus(store, user.id, isActive, queryVariables),
      });

      const trackEventData = isActive ? userListGrantAccessModalSiteAccessRevoked : userListGrantAccessModalSiteAccessGranted;

      this.props.analyticsClient.sendTrackEvent({
        data: trackEventData({
          lastSeenOnSite: user.presence || '',
          productAccess: [],
          userId: user.id,
          userStatus: user.activeStatus || '',
          userRole: getUserRole(user),
        },
        ),
      });

      showFlag({
        autoDismiss: true,
        icon: createSuccessIcon(),
        id: `users.add.user.to.group.success.flag.${Date.now()}`,
        title: formatMessage(isActive ? messages.deactivateSuccessTitle : messages.activateSuccessTitle, {
          userName: user.displayName,
        }),
        description: formatMessage(isActive ? messages.deactivateSuccessDescription : messages.activateSuccessDescription, {
          userName: user.displayName,
        }),
      });
    } catch (error) {
      const isBadRequest = error.graphQLErrors && error.graphqlErrors[0] && error.graphQLErrors[0].originalError && error.graphQLErrors[0].originalError.status === 400;
      showFlag({
        autoDismiss: true,
        icon: createErrorIcon(),
        id: `users.add.user.to.group.error.flag.${Date.now()}`,
        title: formatMessage(isActive ? messages.deactivateErrorTitle : messages.activateErrorTitle, { userName: user.displayName }),
        description: this.determineErrorMessage(isActive, isBadRequest),
      });
    } finally {
      this.setState({ isRemovingUser: false });
      onDismiss();
    }
  }

  private determineErrorMessage(isActive: boolean, isBadRequest: boolean) {
    const {
      intl: { formatMessage },
    } = this.props;

    if (isBadRequest) {
      return formatMessage(isActive ? messages.badRequestErrorToDeactivate : messages.badRequestErrorToActivate);
    }

    return formatMessage(isActive ? messages.deactivateUnknownError : messages.activateUnknownError);
  }
}

const withActivateUserMutation = graphql<OwnProps, ActivateUserMutation, ActivateUserMutationVariables, MutationProps>(activateUserMutation, {
  name: 'activateUser',
});
const withDeactivateUserMutation = graphql<OwnProps, DeactivateUserMutation, DeactivateUserMutationVariables, MutationProps>(deactivateUserMutation, {
  name: 'deactivateUser',
});

export const UsersActivateUserModal = withActivateUserMutation(
  withDeactivateUserMutation(
    injectIntl(
      withFlag(
        withRouter(
          withAnalyticsClient(UsersActivateUserModalImpl),
        ),
      ),
    ),
  ),
);
