export const toArray = (parameters: undefined | string | string[]): string[] => {
  if (parameters === undefined) {
    return [];
  } else if (typeof parameters === 'string') {
    return [parameters];
  } else {
    return parameters;
  }
};

export const formatParameter = (id: string, checked: boolean, parameters: string[]): string[] => {
  let filter = toArray(parameters);

  if (!checked) {
    if (id === 'all' || filter.includes('all')) {
      filter = [];
    } else if ((id === 'active' || id === 'inactive') && (filter.includes('active') || filter.includes('inactive'))) {
      filter = [id];
    } else {
      filter.push(id);
    }
  } else {
    const index = filter.indexOf(id);
    if (index !== -1) {
      filter.splice(index, 1);
    }
  }

  return filter;
};
