import styled from 'styled-components';

import {
  colors as akColors,
  gridSize as akGridSize,
} from '@atlaskit/theme';

export const Divider = styled.hr`
  margin: ${akGridSize}px;
  border-style: solid;
  border-width: 0.7px;
  color: ${akColors.N40};
`;

export const DropdownWrapper = styled.div`
  padding: ${akGridSize}px ${akGridSize() / 2}px 0 ${akGridSize() / 2}px;
`;
