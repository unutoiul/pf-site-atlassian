import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import { MultiSelectDropdown, MultiSelectDropdownItem } from 'common/multi-select-dropdown';

import { AnalyticsClientProps, userListUsersFilter, withAnalyticsClient } from 'common/analytics';

import { formatParameter, toArray } from './users-list-utils';

const messages = defineMessages({
  all: {
    id: 'users.list.filter.all',
    description: 'Filter used to show all users in the users list table',
    defaultMessage: 'All users',
  },
  active: {
    id: 'users.list.filter.active',
    description: 'Filter used to show active users in the users list table',
    defaultMessage: 'Has site access',
  },
  inactive: {
    id: 'users.list.filter.inactive',
    description: 'Filter used to show inactive users in the users list table',
    defaultMessage: 'No site access',
  },
});

interface UserListActiveFilterProps {
  selectedFilters: ActiveFilters[];
  onSet(parameters: object): object;
}

const filters: string[] = ['active', 'inactive'];
export type ActiveFilters = 'active' | 'inactive' | 'blocked' | 'all' | 'invited';

export class UserListActiveFilterImpl extends React.Component<UserListActiveFilterProps & InjectedIntlProps & AnalyticsClientProps> {

  public componentWillMount() {
    const activeStatus = toArray(this.props.selectedFilters);
    const sanitizedActiveFilters = activeStatus.filter(filter => filters.includes(filter));

    if (!activeStatus.includes('all') && sanitizedActiveFilters.length === activeStatus.length) {
      return;
    }

    this.props.onSet({ activeStatus: activeStatus.includes('all') ? 'all' : sanitizedActiveFilters });
  }

  public render() {

    const defaultLabel = (<span>{this.props.intl.formatMessage(messages.all)}</span>);

    return (
      <MultiSelectDropdown
        parentItem={this.generateCheckbox('all')}
        allItems={filters.map(filter => this.generateCheckbox(filter as ActiveFilters))}
        placeholder={defaultLabel}
        onChanged={this.onChanged}
      />
    );
  }

  private generateCheckbox = (filter: ActiveFilters): MultiSelectDropdownItem => {
    return ({
      id: filter,
      label: this.props.intl.formatMessage(messages[filter]),
      isDisabled: filter === 'all' && this.props.selectedFilters.length === 0,
      isSelected: filter === 'all' ? this.props.selectedFilters.length === 0 : toArray(this.props.selectedFilters).includes(filter),
    });
  }

  private onChanged = (e: React.FormEvent<HTMLInputElement>): void => {
    const id: string = e.currentTarget.id;
    const checked: boolean = toArray(this.props.selectedFilters).includes(id);
    const newParameters = this.props.onSet({ activeStatus: formatParameter(id, checked, this.props.selectedFilters) });

    this.props.analyticsClient.sendUIEvent({
      data: userListUsersFilter({ filters: Object.keys(newParameters) }),
    });
  }
}

export const UserListActiveFilter = injectIntl<UserListActiveFilterProps>(
  withAnalyticsClient(UserListActiveFilterImpl),
);
