import { expect } from 'chai';

import { formatParameter, toArray } from './users-list-utils';

describe('Users list filter utils', () => {

  describe('toArray', () => {
    it('Should convert to empty list when value is undefined', () => {
      const array = toArray(undefined);
      expect(array).to.deep.equal([]);
    });

    it('Should convert to an array when value is a string', () => {
      const array = toArray('inactive');
      expect(array).to.deep.equal(['inactive']);
    });

    it('Should return the original array when value is an array', () => {
      const array = toArray(['jira-core', 'conf']);
      expect(array).to.deep.equal(['jira-core', 'conf']);
    });
  });

  describe('formatParameter', () => {
    it('when the all filter is being checked, it should remove all other filters', () => {
      const parameter = formatParameter('all', false, ['active', 'enabled']);
      expect(parameter).to.deep.equal([]);
    });

    it('can either select active or inactive, not both', () => {
      const parameter = formatParameter('inactive', false, ['active']);
      expect(parameter).to.deep.equal(['inactive']);
    });

    it('should remove any filter when being unchecked', () => {
      const parameter = formatParameter('active', true, ['active', 'enabled']);
      expect(parameter).to.deep.equal(['enabled']);
    });
  });
});
