import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import { SearchInput } from 'common/search-input';

import { waitUntil } from '../../../../../utilities/testing';
import { UserListSearchFilterImpl } from './users-list-search-filter';

describe('Users list search filter', () => {
  const sandbox = sinon.sandbox.create();
  const onSetSpy = sinon.stub().returns({ activeStatus: 'active' });
  const sendUIEventSpy = sinon.spy();

  afterEach(() => {
    sandbox.restore();
  });

  function createTestComponent({ totalUserCount = 40, userCountOnScreen = 20 }: { totalUserCount: number, userCountOnScreen: number }) {
    return shallow(
      <UserListSearchFilterImpl
        totalUserCount={totalUserCount}
        userCountOnScreen={userCountOnScreen}
        selectedDisplayName={'Corgi'}
        onSet={onSetSpy}
        analyticsClient={{
          sendUIEvent: sendUIEventSpy,
        } as any}
      />,
    );
  }

  it('should fire an analytics event on change', async () => {
    const wrapper = createTestComponent({
      totalUserCount: 40,
      userCountOnScreen: 20,
    });
    const search = wrapper.find(SearchInput);
    search.props().onChange!({
      persist: () => null,
      target: { value: 'Corgi!' },
    } as any);

    await waitUntil(() => sendUIEventSpy.callCount > 0);

    expect(sendUIEventSpy.getCalls()[0].args[0].data.actionSubjectId).to.equal('searchUser');
    expect(sendUIEventSpy.getCalls()[0].args[0].data.attributes).to.deep.equal({
      countSearchCharacters: 'Corgi!'.length,
      totalUserCount: 40,
      userCountOnScreen: 20,
    });
  });
});
