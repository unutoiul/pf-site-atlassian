import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import { MultiSelectDropdown } from 'common/multi-select-dropdown';

import { createMockIntlContext, createMockIntlProp, waitUntil } from '../../../../../utilities/testing';
import { UserListAdminProductFilterImpl } from './user-list-admin-filter';

describe('Users list admin filter', () => {
  const sandbox = sinon.sandbox.create();
  const onSetSpy = sinon.stub().returns({ activeStatus: 'active' });
  const sendUIEventSpy = sinon.spy();

  afterEach(() => {
    sandbox.restore();
  });

  function createTestComponent({ selectedProductAdminFilters, selectedSitePrivilege, data }): ShallowWrapper {
    return shallow(
      <UserListAdminProductFilterImpl
        intl={createMockIntlProp()}
        cloudId={'DUMMY-CLOUD-ID'}
        data={data}
        selectedSitePrivilegeFilters={selectedSitePrivilege}
        selectedProductAdminFilters={selectedProductAdminFilters}
        onSet={onSetSpy}
        analyticsClient={{
          sendUIEvent: sendUIEventSpy,
        } as any}
      />,
      createMockIntlContext(),
    );
  }

  it('should just be a dropdown with a label and no items', () => {

    it('When there is no data', () => {
      const wrapper = createTestComponent({ selectedProductAdminFilters: [], selectedSitePrivilege: [], data: null });
      const dropdown = wrapper.find(MultiSelectDropdown);
      expect(dropdown.exists()).to.equal(true);
      expect(dropdown.props().placeholder).to.not.equal(null);
      expect(dropdown.props().allItems).to.equal(null);
      expect(dropdown.props().parentItem).to.equal(null);
    });

    it('When it is loading', () => {
      const wrapper = createTestComponent({ selectedProductAdminFilters: [], selectedSitePrivilege: [], data: { loading: true } });
      const dropdown = wrapper.find(MultiSelectDropdown);
      expect(dropdown.exists()).to.equal(true);
      expect(dropdown.props().placeholder).to.not.equal(null);
      expect(dropdown.props().allItems).to.equal(null);
      expect(dropdown.props().parentItem).to.equal(null);
    });

    it('When there is an error', () => {
      const wrapper = createTestComponent({ selectedProductAdminFilters: [], selectedSitePrivilege: [], data: { error: true } });
      const dropdown = wrapper.find(MultiSelectDropdown);
      expect(dropdown.exists()).to.equal(true);
      expect(dropdown.props().placeholder).to.not.equal(null);
      expect(dropdown.props().allItems).to.equal(null);
      expect(dropdown.props().parentItem).to.equal(null);
    });
  });

  it('Should have checkboxes checked when there are default filters being passed in', () => {

    const wrapper = createTestComponent({
      selectedProductAdminFilters: ['conf'],
      selectedSitePrivilege: ['site-admin'],
      data: {
        loading: false,
        error: false,
        currentSite: {
          groupsAdminAccessConfig: [
            {
              product: {
                productId: 'conf',
                productName: 'Confluence',
              },
            },
            {
              product: {
                productId: 'refapp',
                productName: 'Refapp',
              },
            },
          ],
        },
      },
    });

    const dropdown = wrapper.find(MultiSelectDropdown);
    expect(dropdown.exists()).to.equal(true);
    expect(dropdown.props().parentItem).to.deep.equal(
      {
        label: 'Site admins',
        id: 'site-admin',
        isSelected: true,
      },
    );
    expect(dropdown.props().allItems).to.deep.equal([
      {
        label: 'Confluence admins',
        id: 'conf',
        isSelected: true,
      },
      {
        label: 'Refapp admins',
        id: 'refapp',
        isSelected: false,
      },
    ]);
  });

  it('Should have no checkboxes checked when there are no default filters being passed in', () => {

    const wrapper = createTestComponent({
      selectedProductAdminFilters: [],
      selectedSitePrivilege: [],
      data: {
        loading: false,
        error: false,
        currentSite: {
          groupsAdminAccessConfig: [
            {
              product: {
                productId: 'conf',
                productName: 'Confluence',
              },
            },
            {
              product: {
                productId: 'refapp',
                productName: 'Refapp',
              },
            },
          ],
        },
      },
    });

    const dropdown = wrapper.find(MultiSelectDropdown);
    expect(dropdown.exists()).to.equal(true);
    expect(dropdown.props().parentItem).to.deep.equal(
      {
        label: 'Site admins',
        id: 'site-admin',
        isSelected: false,
      },
    );
    expect(dropdown.props().allItems).to.deep.equal([
      {
        label: 'Confluence admins',
        id: 'conf',
        isSelected: false,
      },
      {
        label: 'Refapp admins',
        id: 'refapp',
        isSelected: false,
      },
    ]);
  });

  it('should fire a UI event when the MultiSelectDropdown changes', async () => {
    const wrapper = createTestComponent({
      selectedProductAdminFilters: ['conf'],
      selectedSitePrivilege: ['site-admin'],
      data: {
        loading: false,
        error: false,
        currentSite: {
          groupsAdminAccessConfig: [
            {
              product: {
                productId: 'conf',
                productName: 'Confluence',
              },
            },
            {
              product: {
                productId: 'refapp',
                productName: 'Refapp',
              },
            },
          ],
        },
      },
    });

    const dropdown = wrapper.find(MultiSelectDropdown);
    (dropdown.props() as any).onChanged({
      currentTarget: { id: 'site-admin' },
    });

    await waitUntil(() => sendUIEventSpy.callCount > 0);

    expect(sendUIEventSpy.callCount).to.equal(1);
    expect(sendUIEventSpy.getCalls()[0].args[0].data.actionSubjectId).to.equal('adminFilter');
    expect(sendUIEventSpy.getCalls()[0].args[0].data.attributes).to.deep.equal({
      filters: ['activeStatus'],
    });
  });
});
