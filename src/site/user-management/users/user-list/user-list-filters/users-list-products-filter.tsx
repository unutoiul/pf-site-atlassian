import * as React from 'react';
import { graphql } from 'react-apollo';
import { ChildProps } from 'react-apollo/types';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';

import { MultiSelectDropdown, MultiSelectDropdownItem } from 'common/multi-select-dropdown';

import { AnalyticsClientProps, userListProductFilter, withAnalyticsClient } from 'common/analytics';

import userListProductsQuery from './users-list-use-products.query.graphql';

import { UsersListProductsQuery } from '../../../../../../src/schema/schema-types';
import { formatParameter, toArray } from './users-list-utils';

interface UserListActiveFilterProps {
  cloudId: string;
  selectedFilters: string[];
  onSet(parameters: object): object;
}

const messages = defineMessages({
  all: {
    id: 'users.list.filter.product.all',
    description: 'Filter used to show all users in the users list table',
    defaultMessage: 'All products',
  },
});

export type UserListProductFilterProps = ChildProps<UserListActiveFilterProps & InjectedIntlProps & AnalyticsClientProps, UsersListProductsQuery>;

export class UserListProductFilterImpl extends React.Component<UserListProductFilterProps> {

  public componentDidMount() {
    const productUse = toArray(this.props.selectedFilters);
    this.props.onSet({ productUse: productUse.includes('all') ? 'all' : productUse });
  }

  public render() {

    const allText = (
      <FormattedMessage
        {...messages.all}
        tagName="span"
      />
    );

    if (!this.props.data || this.props.data.loading || this.props.data.error || !this.props.data.currentSite) {
      return (
        <MultiSelectDropdown
          placeholder={allText}
        />
      );
    }

    return (
      <MultiSelectDropdown
        parentItem={this.generateItem({ productId: 'all', productName: this.props.intl.formatMessage(messages.all) })}
        allItems={this.props.data.currentSite.groupsUseAccessConfig.map(access => this.generateItem(access.product))}
        placeholder={allText}
        onChanged={this.onChanged}
      />
    );
  }

  private generateItem = ({ productId, productName }): MultiSelectDropdownItem => {

    return {
      id: productId,
      label: productName,
      isDisabled: productId === 'all' && this.props.selectedFilters.length === 0,
      isSelected: productId === 'all' ? this.props.selectedFilters.length === 0 : this.props.selectedFilters.includes(productId),
    };
  }

  private onChanged = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const id: string = e.currentTarget.id;
    const checked: boolean = this.props.selectedFilters.includes(id);
    const newParameters = this.props.onSet({
      productUse: formatParameter(id, checked, this.props.selectedFilters),
    });

    this.props.analyticsClient.sendUIEvent({
      data: userListProductFilter({ filters: Object.keys(newParameters) }),
    });
  }
}

const withProducts = graphql<UserListActiveFilterProps, UsersListProductsQuery>(userListProductsQuery);

export const UserListProductFilter = withProducts(
  injectIntl(
    withAnalyticsClient(
      UserListProductFilterImpl,
    ),
  ),
);
