import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import { MultiSelectDropdown } from 'common/multi-select-dropdown';

import { createMockIntlProp, waitUntil } from '../../../../../utilities/testing';

import { UserListProductFilterImpl } from './users-list-products-filter';

describe('Users list product filter', () => {
  const sandbox = sinon.sandbox.create();
  const onSetSpy = sinon.stub().returns({ activeStatus: 'active' });
  const sendUIEventSpy = sinon.spy();

  afterEach(() => {
    sandbox.restore();
  });

  const currentSiteProps = ({ loading, error, products }): any => {
    return ({
      loading,
      error,
      currentSite: {
        groupsUseAccessConfig: products,
      },
    });
  };

  function createTestComponent({ selectedFilters, loading = false, error = false, products }) {
    return shallow(
      <UserListProductFilterImpl
        cloudId="DUMMY-CLOUD-ID"
        selectedFilters={selectedFilters}
        onSet={onSetSpy}
        intl={createMockIntlProp()}
        data={currentSiteProps({ loading, error, products })}
        analyticsClient={{
          sendUIEvent: sendUIEventSpy,
        } as any}
      />,
    );
  }

  it('With no selected filters', () => {
    const products = [
      { productId: 'jira-core', productName: 'Jira Core' },
      { productId: 'jira-software', productName: 'Jira Software' },
    ];

    const data = {
      selectedFilters: [],
      products: products.map(p => ({ product: { ...p } })),
    };

    const wrapper = createTestComponent(data);
    const multiSelectDropdown = wrapper.find(MultiSelectDropdown);
    expect(multiSelectDropdown.exists()).to.equal(true);
    const allItems = multiSelectDropdown.props().allItems as any;
    const parentItem = multiSelectDropdown.props().parentItem as any;
    expect(parentItem).to.deep.include({ isSelected: true });
    expect(allItems.length).to.equal(2);
    expect(allItems).to.deep.equal([
      {
        id: 'jira-core',
        label: 'Jira Core',
        isDisabled: false,
        isSelected: false,
      },
      {
        id: 'jira-software',
        label: 'Jira Software',
        isDisabled: false,
        isSelected: false,
      },
    ]);
  });

  it('With some selected filters', () => {
    const products = [
      { productId: 'jira-core', productName: 'Jira Core' },
      { productId: 'jira-software', productName: 'Jira Software' },
    ];

    const data = {
      selectedFilters: ['jira-core', 'jira-software'],
      products: products.map(p => ({ product: { ...p } })),
    };

    const wrapper = createTestComponent(data);
    const multiSelectDropdown = wrapper.find(MultiSelectDropdown);
    expect(multiSelectDropdown.exists()).to.equal(true);
    const allItems = multiSelectDropdown.props().allItems as any;
    expect(allItems.length).to.equal(2);
    expect(allItems).to.deep.equal([
      {
        id: 'jira-core',
        label: 'Jira Core',
        isDisabled: false,
        isSelected: true,
      },
      {
        id: 'jira-software',
        label: 'Jira Software',
        isDisabled: false,
        isSelected: true,
      },
    ]);
  });

  it('With some selected filters; but none match products that are found.', () => {
    const products = [
      { productId: 'jira-core', productName: 'Jira Core' },
      { productId: 'jira-software', productName: 'Jira Software' },
    ];

    const data = {
      selectedFilters: ['confluence'],
      products: products.map(p => ({ product: { ...p } })),
    };

    const wrapper = createTestComponent(data);
    const multiSelectDropdown = wrapper.find(MultiSelectDropdown);
    expect(multiSelectDropdown.exists()).to.equal(true);
    const allItems = multiSelectDropdown.props().allItems;
    expect(allItems!.length).to.equal(2);
    expect(allItems).to.deep.equal([
      {
        id: 'jira-core',
        label: 'Jira Core',
        isDisabled: false,
        isSelected: false,
      },
      {
        id: 'jira-software',
        label: 'Jira Software',
        isDisabled: false,
        isSelected: false,
      },
    ]);
  });

  it('should fire a UI event when the MultiSelectDropdown changes', async () => {
    const products = [
      { productId: 'jira-core', productName: 'Jira Core' },
      { productId: 'jira-software', productName: 'Jira Software' },
    ];

    const data = {
      selectedFilters: [],
      products: products.map(p => ({ product: { ...p } })),
    };

    const wrapper = createTestComponent(data);

    const dropdown = wrapper.find(MultiSelectDropdown);
    (dropdown.props() as any).onChanged({
      currentTarget: { id: 'site-admin' },
    });

    await waitUntil(() => sendUIEventSpy.callCount > 0);

    expect(sendUIEventSpy.callCount).to.equal(1);
    expect(sendUIEventSpy.getCalls()[0].args[0].data.actionSubjectId).to.equal('productFilter');
    expect(sendUIEventSpy.getCalls()[0].args[0].data.attributes).to.deep.equal({
      filters: ['activeStatus'],
    });
  });
});
