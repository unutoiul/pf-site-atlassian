import * as React from 'react';
import { graphql } from 'react-apollo';
import { ChildProps } from 'react-apollo/types';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';

import { AnalyticsClientProps, userListAdminFilter, withAnalyticsClient } from 'common/analytics';
import { MultiSelectDropdown, MultiSelectDropdownItem } from 'common/multi-select-dropdown';

import userListProductsQuery from './users-list-admin-products.query.graphql';

import { UsersListAdminProductsQuery } from '../../../../../../src/schema/schema-types';
import { formatParameter, toArray } from './users-list-utils';

interface UserListAdminFilterProps {
  cloudId: string;
  selectedProductAdminFilters: string[];
  selectedSitePrivilegeFilters: string[];
  onSet(parameters: object): object;
}

const messages = defineMessages({
  admin: {
    id: 'users.list.filter.admin.label',
    description: 'Dropdown label to describe that the user can filter by administration types',
    defaultMessage: 'Admin types',
  },
  adminLabel: {
    id: 'users.list.filter.admin',
    description: 'Admin label used for admin products',
    defaultMessage: '{productName} admins',
  },
});

export type UserListAdminProductFilterProps = ChildProps<UserListAdminFilterProps & InjectedIntlProps, UsersListAdminProductsQuery>;

export class UserListAdminProductFilterImpl extends React.Component<UserListAdminProductFilterProps & AnalyticsClientProps> {

  public componentDidMount() {
    const productAdmin = toArray(this.props.selectedProductAdminFilters);
    const sitePrivilege = toArray(this.props.selectedSitePrivilegeFilters);

    this.props.onSet({ productAdmin, sitePrivilege });
  }

  public render() {

    const adminText = (
      <FormattedMessage
        {...messages.admin}
        tagName="span"
      />
    );

    const siteAdmin: MultiSelectDropdownItem = {
      id: 'site-admin',
      label: 'Site admins',
      isSelected: this.props.selectedSitePrivilegeFilters.includes('site-admin'),
    };

    const isDisabled = !this.props.data || this.props.data.loading || this.props.data.error || !this.props.data.currentSite;

    return (
      <MultiSelectDropdown
        parentItem={isDisabled ? undefined : siteAdmin}
        allItems={isDisabled ? undefined : this.props.data!.currentSite!.groupsAdminAccessConfig.map(product => this.generateItem(product.product))}
        placeholder={adminText}
        onChanged={this.onChanged}
      />
    );
  }

  private generateItem = ({ productId, productName }): MultiSelectDropdownItem => {
    return {
      id: productId,
      label: this.props.intl.formatMessage(messages.adminLabel, { productName }),
      isSelected: this.props.selectedProductAdminFilters.includes(productId),
    };
  }

  private onChanged = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const id: string = e.currentTarget.id;
    const selectedFilterType = id === 'site-admin' ? this.props.selectedSitePrivilegeFilters : this.props.selectedProductAdminFilters;
    const checked: boolean = selectedFilterType.includes(id);

    let newParameters: object;

    // Disabled because we want to make it explicit, this is a set operation
    // tslint:disable-next-line:prefer-conditional-expression
    if (id === 'site-admin') {
      newParameters = this.props.onSet({ sitePrivilege: formatParameter(id, checked, selectedFilterType) });
    } else {
      newParameters = this.props.onSet({ productAdmin: formatParameter(id, checked, selectedFilterType) });
    }

    this.props.analyticsClient.sendUIEvent({
      data: userListAdminFilter({ filters: Object.keys(newParameters) }),
    });

  }
}

const withProducts = graphql<UserListAdminFilterProps, UsersListAdminProductsQuery>(userListProductsQuery);

export const UserListAdminProductFilter = withProducts(
  withAnalyticsClient(
    injectIntl(
      UserListAdminProductFilterImpl,
    ),
  ),
);
