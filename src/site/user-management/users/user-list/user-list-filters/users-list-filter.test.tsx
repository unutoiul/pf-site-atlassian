import { expect } from 'chai';
import { mount, ReactWrapper } from 'enzyme';
import * as React from 'react';
import { sandbox as sinonSandbox } from 'sinon';

import AkDropdown from '@atlaskit/dropdown-menu';

import { createMockIntlContext, createMockIntlProp } from '../../../../../utilities/testing';
import { UserListFilterImpl } from './users-list-filter';

describe('Users list active filter', () => {
  let sandbox;
  let onSetSpy;
  let sendUIEventSpy;

  beforeEach(() => {
    sandbox = sinonSandbox.create();
    onSetSpy = sandbox.spy();
    sendUIEventSpy = sandbox.spy();
  });

  function createTestComponent(queryParameters: string): ReactWrapper {
    return mount(
      <UserListFilterImpl
        analyticsClient={{
          sendUIEvent: sendUIEventSpy,
        } as any}
        cloudId="DUMMY-CLOUD-ID"
        queryParameters={queryParameters}
        intl={createMockIntlProp()}
        updateQueryParams={onSetSpy}
        userCountOnScreen={20}
        totalUserCount={20}
      />,
      createMockIntlContext(),
    );
  }

  it('Sanitizing query parameters on mounting', () => {
    it('Shouldn\'t sanitize query parameters if it is constructed as normal', () => {
      createTestComponent('?activeStatus=active&activeStatus=inactive');
      expect(onSetSpy.callCount).to.equal(0);
    });

    it('Should sanitize if there is an unknown activeStatus in the query parameters', () => {
      createTestComponent('?activeStatus=active&activeStatus=something');
      expect(onSetSpy.callCount).to.equal(1);
      expect(onSetSpy.args[0]).to.deep.equal([{ activeStatus: ['active'] }]);
    });

    it('Should not sanitize non-activeStatus query parameters', () => {
      createTestComponent('?activeStatus=all&someOtherFilter=dogs');
      expect(onSetSpy.callCount).to.equal(1);
      expect(onSetSpy.args[0]).to.deep.equal([{ activeStatus: ['all'], someOtherFilter: 'dog' }]);
    });
  });

  it('Clicking checkboxes', () => {

    const ACTIVE_CHECKBOX = 1;
    const INACTIVE_CHECKBOX = 2;

    function getCheckbox(queryParameters: string, checkboxNumber: number): ReactWrapper {
      const wrapper = createTestComponent(queryParameters);
      clickDropdown(wrapper);
      const checkboxes = getCheckboxes(wrapper);
      const checkbox = checkboxes.at(checkboxNumber);
      expect(onSetSpy.callCount).to.equal(0);

      return checkbox;
    }

    function clickDropdown(wrapper: ReactWrapper): void {
      wrapper.find(AkDropdown).find('button').simulate('click');
    }

    function getCheckboxes(wrapper: ReactWrapper): ReactWrapper {
      return wrapper.find('span[role="checkbox"]');
    }

    it('Should trigger the onSet function when checking an unchecked filter', () => {
      const checkbox = getCheckbox('', ACTIVE_CHECKBOX);
      checkbox.simulate('click');
      expect(onSetSpy.callCount).to.equal(1);
      expect(onSetSpy.args[0]).to.deep.equal([{ activeStatus: ['active'] }]);
    });

    it('Should not modify any existing non-activeStatus query parameters', () => {
      const checkbox = getCheckbox('?otherFilter=blep', ACTIVE_CHECKBOX);
      checkbox.simulate('click');
      expect(onSetSpy.callCount).to.equal(1);
      expect(onSetSpy.args[0]).to.deep.equal([{ activeStatus: ['active'], otherFilter: 'blep' }]);
    });

    it('Should pass in existing activeStatus query parameters', () => {
      const checkbox = getCheckbox('?activeStatus=inactive&activeStatus=active', INACTIVE_CHECKBOX);
      checkbox.simulate('click');
      expect(onSetSpy.callCount).to.equal(1);
      expect(onSetSpy.args[0]).to.deep.equal([{ activeStatus: ['active', 'inactive'] }]);
    });
  });
});
