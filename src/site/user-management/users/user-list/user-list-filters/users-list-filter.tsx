import { Search } from 'history';
import { parse } from 'query-string';
import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import { gridSize as akGridSize } from '@atlaskit/theme';

import { AnalyticsClientProps, withAnalyticsClient } from 'common/analytics';

import { UserListAdminProductFilter } from './user-list-admin-filter';
import { UserListActiveFilter } from './users-list-active-filter';
import { UserListProductFilter } from './users-list-products-filter';
import { UserListSearchFilter } from './users-list-search-filter';
import { DropdownWrapper } from './users-list.styled';

interface UserStatusFilterProps {
  userCountOnScreen: number;
  totalUserCount: number;
  cloudId: string;
  queryParameters: Search;
  updateQueryParams(parameters: object): void;
}

export const Container = styled.div`
  display: flex;
  padding-bottom: ${akGridSize() * 3}px;
`;

export const Label = styled.span`
  padding: ${akGridSize() * 1.8}px ${akGridSize() * 2}px 0 ${akGridSize() * 2}px;
`;

const messages = defineMessages({
  filterBy: {
    id: 'users.list.filter.by',
    description: 'Tagline that comes before the different filter options for the users list table (eg. active, inactive, etc)',
    defaultMessage: 'Filter by: ',
  },
});

export class UserListFilterImpl extends React.Component<UserStatusFilterProps & InjectedIntlProps & AnalyticsClientProps> {

  public render() {

    const { displayName, activeStatus, productUse, productAdmin, sitePrivilege } = parse(this.props.queryParameters);

    return (
      <Container>
        <UserListSearchFilter
          selectedDisplayName={displayName || ''}
          onSet={this.updateWithNewQueryParameters}
          userCountOnScreen={this.props.userCountOnScreen}
          totalUserCount={this.props.totalUserCount}
        />
        <Label>{this.props.intl.formatMessage(messages.filterBy)}</Label>
        <DropdownWrapper>
          <UserListActiveFilter
            selectedFilters={activeStatus || []}
            onSet={this.updateWithNewQueryParameters}
          />
        </DropdownWrapper>
        <DropdownWrapper>
          <UserListProductFilter
            cloudId={this.props.cloudId}
            selectedFilters={productUse || []}
            onSet={this.updateWithNewQueryParameters}
          />
        </DropdownWrapper>
        <DropdownWrapper>
          <UserListAdminProductFilter
            cloudId={this.props.cloudId}
            selectedSitePrivilegeFilters={sitePrivilege || []}
            selectedProductAdminFilters={productAdmin || []}
            onSet={this.updateWithNewQueryParameters}
          />
        </DropdownWrapper>
      </Container>
    );
  }

  private updateWithNewQueryParameters = (parameters: object): object => {
    const newParameters: object = {
      ...parse(this.props.queryParameters),
      ...parameters,
    };

    this.props.updateQueryParams(newParameters);

    return newParameters;
  }
}

export const UserListFilter = injectIntl<UserStatusFilterProps>(
  withAnalyticsClient(UserListFilterImpl),
);
