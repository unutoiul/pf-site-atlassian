import debounce from 'lodash.debounce';
import * as React from 'react';

import { AnalyticsClientProps, userListSearchUser, withAnalyticsClient } from 'common/analytics';
import { SearchInput } from 'common/search-input';

interface UserListActiveFilterProps {
  totalUserCount: number;
  userCountOnScreen: number;
  selectedDisplayName: string;
  onSet(parameters: object): void;
}
export class UserListSearchFilterImpl extends React.Component<UserListActiveFilterProps & AnalyticsClientProps> {

  public searchByDisplayName = debounce((displayName: string | undefined) => {
    this.props.onSet({ displayName });

    this.props.analyticsClient.sendUIEvent({
      data: userListSearchUser({
        totalUserCount: this.props.totalUserCount,
        userCountOnScreen: this.props.userCountOnScreen,
        countSearchCharacters: (displayName || '').length,
      }),
    });

  }, 500);

  public render() {
    return (
      <SearchInput
        initialValue={this.props.selectedDisplayName}
        onChange={this.onSearch}
      />
    );
  }

  private onSearch = (e: React.FormEvent<HTMLInputElement>) => {
    e.persist();
    const displayName = (e.target as HTMLInputElement).value;
    this.searchByDisplayName(displayName === '' ? undefined : displayName);
  };
}

export const UserListSearchFilter = withAnalyticsClient(UserListSearchFilterImpl);
