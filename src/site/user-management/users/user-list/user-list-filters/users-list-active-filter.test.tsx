import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import { MultiSelectDropdown } from 'common/multi-select-dropdown';

import { createMockIntlContext, createMockIntlProp, waitUntil } from '../../../../../utilities/testing';
import { ActiveFilters, UserListActiveFilterImpl } from './users-list-active-filter';

describe('Users list active filter', () => {
  const sandbox = sinon.sandbox.create();
  const onSetSpy = sinon.stub().returns({ activeStatus: 'active' });
  const sendUIEventSpy = sinon.spy();

  afterEach(() => {
    sandbox.restore();
  });

  function createTestComponent(selectedFilters: ActiveFilters[]): ShallowWrapper {
    return shallow(
      <UserListActiveFilterImpl
        intl={createMockIntlProp()}
        selectedFilters={selectedFilters}
        onSet={onSetSpy}
        analyticsClient={{
          sendUIEvent: sendUIEventSpy,
        } as any}
      />,
      createMockIntlContext(),
    );
  }

  it('With the no selected filter', () => {
    const wrapper = createTestComponent([]);
    const multiSelectDropdown = wrapper.find(MultiSelectDropdown);
    expect(multiSelectDropdown.exists()).to.equal(true);
    const allItems = multiSelectDropdown.props().allItems as any;
    const parentItem = multiSelectDropdown.props().parentItem as any;
    expect(parentItem).to.deep.include({ isSelected: true });
    expect(allItems).to.deep.equal([
      {
        id: 'active',
        label: 'Has site access',
        isDisabled: false,
        isSelected: false,
      },
      {
        id: 'inactive',
        label: 'No site access',
        isDisabled: false,
        isSelected: false,
      },
    ]);
  });

  it('With the ACTIVE selected filter', () => {
    const wrapper = createTestComponent(['active']);
    const multiSelectDropdown = wrapper.find(MultiSelectDropdown);
    expect(multiSelectDropdown.exists()).to.equal(true);
    const allItems = multiSelectDropdown.props().allItems as any;
    const parentItem = multiSelectDropdown.props().parentItem as any;
    expect(parentItem).to.deep.include({ isSelected: false });
    expect(allItems).to.deep.equal([
      {
        id: 'active',
        label: 'Has site access',
        isDisabled: false,
        isSelected: true,
      },
      {
        id: 'inactive',
        label: 'No site access',
        isDisabled: false,
        isSelected: false,
      },
    ]);
  });

  it('With the INACTIVE selected filter', () => {
    const wrapper = createTestComponent(['inactive']);
    const multiSelectDropdown = wrapper.find(MultiSelectDropdown);
    expect(multiSelectDropdown.exists()).to.equal(true);
    const allItems = multiSelectDropdown.props().allItems as any;
    const parentItem = multiSelectDropdown.props().parentItem as any;
    expect(parentItem).to.deep.include({ isSelected: false });
    expect(allItems).to.deep.equal([
      {
        id: 'active',
        label: 'Has site access',
        isDisabled: false,
        isSelected: false,
      },
      {
        id: 'inactive',
        label: 'No site access',
        isDisabled: false,
        isSelected: true,
      },
    ]);
  });

  it('should fire a UI event when the MultiSelectDropdown changes', async () => {
    const wrapper = createTestComponent([]);

    const dropdown = wrapper.find(MultiSelectDropdown);
    (dropdown.props() as any).onChanged({
      currentTarget: { id: 'site-admin' },
    });

    await waitUntil(() => sendUIEventSpy.callCount > 0);

    expect(sendUIEventSpy.callCount).to.equal(1);
    expect(sendUIEventSpy.getCalls()[0].args[0].data.actionSubjectId).to.equal('usersFilter');
    expect(sendUIEventSpy.getCalls()[0].args[0].data.attributes).to.deep.equal({
      filters: ['activeStatus'],
    });
  });
});
