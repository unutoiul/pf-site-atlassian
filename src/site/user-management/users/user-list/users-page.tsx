import { parse, stringify } from 'query-string';
import * as React from 'react';
import { ChildProps, graphql, QueryProps } from 'react-apollo';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';

import AkEmptyState from '@atlaskit/empty-state';
import AkSpinner from '@atlaskit/spinner';

import { analyticsClient, ScreenEventSender, userListScreenEvent } from 'common/analytics';
import { FlagProps, withFlag } from 'common/flag';
import { errorWindowImage } from 'common/images';
import { getStart, sanitizePage } from 'common/pagination';

import { IsCurrentAdminSystemAdminQuery, SiteUsersListQuery, User } from '../../../../../src/schema/schema-types';
import currentUserQuery from '../../groups/current-user.query.graphql';
import { UsersQuickInvite } from '../users-quick-invite/users-quick-invite';
import { UserListFilter } from './user-list-filters/users-list-filter';
import { ROWS_PER_PAGE } from './users-list-constants';
import { UserListPageLayout } from './users-list-page-layout';
import { UsersListTable } from './users-list-table';
import siteUsersListQuery from './users-page.query.graphql';

const messages = defineMessages({
  errorStateTitle: {
    id: 'users.list.error.state.title',
    description: 'Shown when there is an error trying to fetch users',
    defaultMessage: 'Uh oh! Something went wrong.',
  },
  errorStateDescription: {
    id: 'users.list.error.state.description',
    description: 'Shown when there is an error trying to fetch users',
    defaultMessage: 'We had trouble trying to fetch your users. Refresh the page to try again.',
  },
  errorDescription: {
    id: 'users.list.error.description',
    description: 'Shown when there is an error trying to fetch the next page of users',
    defaultMessage: 'We weren\'t able to get the next page of users, please refresh the page and try again.',
  },
});

interface SiteParams {
  cloudId: string;
}

interface UsersListPageState {
  hasInitiallyLoaded: boolean;
}

interface CurrentUserProps {
  currentSite?: QueryProps & Partial<IsCurrentAdminSystemAdminQuery>;
}

type UsersListPageProps = ChildProps<InjectedIntlProps & FlagProps & CurrentUserProps & RouteComponentProps<SiteParams>, SiteUsersListQuery>;

export class UsersListPageImpl extends React.Component<UsersListPageProps, UsersListPageState> {

  public state: UsersListPageState = {
    hasInitiallyLoaded: false,
  };

  public async componentDidUpdate(prevProps: UsersListPageProps, prevState: UsersListPageState) {
    if (this.props.location.search === prevProps.location.search || !this.props.data) {
      return;
    }

    if (!prevState.hasInitiallyLoaded) {
      this.setState({ hasInitiallyLoaded: true });
    }

    const { page, displayName, activeStatus, productUse, productAdmin, sitePrivilege } = parse(this.props.location.search);

    if (parse(prevProps.location.search).page === page) {
      this.updateQueryParams({ page: undefined });
    }

    await this.props.data.fetchMore({
      variables: {
        cloudId: this.props.match.params.cloudId,
        input: {
          start: getStart(sanitizePage(page), ROWS_PER_PAGE),
          count: ROWS_PER_PAGE,
          activeStatus,
          displayName,
          productUse,
          productAdmin,
          sitePrivilege,
        },
      },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        return {
          ...previousResult,
          ...fetchMoreResult,
        };
      },
    });
  }

  public render() {

    const { formatMessage } = this.props.intl;

    const defaultPageLayoutProps = {
      intl: this.props.intl,
      history: this.props.history,
      match: this.props.match,
      location: this.props.location,
    };

    const ErrorSection = (
      <AkEmptyState
        imageUrl={errorWindowImage}
        header={formatMessage(messages.errorStateTitle)}
        description={formatMessage(messages.errorStateDescription)}
        size="wide"
      />
    );

    if (this.props.data && this.props.data.loading && !this.state.hasInitiallyLoaded) {
      return (
        <UserListPageLayout {...defaultPageLayoutProps} isDisabled={true}>
          <AkSpinner />
        </UserListPageLayout>
      );
    }

    if (!this.props.data || this.props.data.error || !this.props.data.siteUsersList || !this.props.data.currentSite) {
      return (
        <UserListPageLayout {...defaultPageLayoutProps} isDisabled={true}>
          {ErrorSection}
        </UserListPageLayout>
      );
    }

    const { page, displayName, activeStatus, productUse, productAdmin, sitePrivilege } = parse(this.props.location.search);
    const noQuery = !activeStatus && !displayName && !productUse && !productAdmin && !sitePrivilege;
    const getArray = (stringOrArray: string | string[]) => typeof stringOrArray === 'string' ? [stringOrArray] : Array.isArray(stringOrArray) ? stringOrArray : [];
    const userTypesSelected = getArray(activeStatus);
    const productKeysSelected = getArray(productUse);
    const adminTypesSelected = [
      ...getArray(sitePrivilege),
      ...getArray(productAdmin),
    ];

    const usersWithPlaceholders = this.getUsersPaddedWithPlaceholder(
      getStart(sanitizePage(page), ROWS_PER_PAGE),
      this.props.data.siteUsersList.total,
      this.props.data.siteUsersList.users,
      false,
    );

    const isQuickAddShown = usersWithPlaceholders.filter(user => user && !user.system).length < 5 && noQuery && !page;

    return (
      <ScreenEventSender
        event={userListScreenEvent({
          userCountOnScreen: Math.min(this.props.data.siteUsersList.users.length, ROWS_PER_PAGE),
          totalUserCount: this.props.data.siteUsersList.total,
          quickInviteVisible: isQuickAddShown,
          userTypesSelected,
          productKeysSelected,
          adminTypesSelected,
        })}
        isDataLoading={this.props.data.loading}
      >
        <UserListPageLayout
          {...defaultPageLayoutProps}
          userCountOnScreen={this.props.data.siteUsersList.users.length}
          totalUserCount={this.props.data.siteUsersList.total}
          userExportADG3Migration={this.props.data.currentSite.flags.userExportADG3Migration}
        >
          {isQuickAddShown && <UsersQuickInvite onInvite={this.refresh} cloudId={this.props.match.params.cloudId} />}
          {!isQuickAddShown && <UserListFilter
            cloudId={this.props.match.params.cloudId}
            updateQueryParams={this.updateQueryParams}
            queryParameters={this.props.location.search}
            userCountOnScreen={this.props.data.siteUsersList.users.length}
            totalUserCount={this.props.data.siteUsersList.total}
          />}
          <UsersListTable
            isCurrentUserSystemAdmin={!!(this.props.currentSite && this.props.currentSite.currentUser && this.props.currentSite.currentUser.isSystemAdmin)}
            onSetPage={this.updateQueryParams}
            isLoading={this.props.data.loading}
            users={usersWithPlaceholders || []}
            currentPage={sanitizePage(page)}
            totalUsers={this.props.data.siteUsersList.total}
            cloudId={this.props.match.params.cloudId}
            history={this.props.history}
            displayNameQuery={displayName}
            userTypesSelected={userTypesSelected}
            productKeysSelected={productKeysSelected}
            adminTypesSelected={adminTypesSelected}
            hasQuery={!noQuery}
            currentUserId={this.props.currentSite && this.props.currentSite.currentUser && this.props.currentSite.currentUser.id || ''}
            onUserUpdate={this.refresh}
          />
        </UserListPageLayout>
      </ScreenEventSender>
    );
  }

  private refresh = async (): Promise<void> => {
    if (!this.props.data) {
      return;
    }
    try {
      await this.props.data.refetch({
        variables: {
          v: Math.random(), // HACK: component doesn't re-render unless some variables have changed, seems related to https://github.com/apollographql/react-apollo/issues/2070
        },
      });
    } catch (e) {
      analyticsClient.onError(e);
    }
  }

  private getUsersPaddedWithPlaceholder = (start: number, total: number, users: User[], isLoading: boolean) => {
    const paddedUsers: User[] = Array(total).fill(null);

    if (!isLoading) {
      paddedUsers.splice(start - 1, users.length, ...users);
    }

    return paddedUsers;
  };

  private updateQueryParams = (params: object): void => {
    const { history, location } = this.props;
    history.replace(`${location.pathname}?${stringify({ ...parse(location.search), ...params })}`);
  };
}

const withUsersList = graphql<RouteComponentProps<SiteParams>, SiteUsersListQuery>(siteUsersListQuery, {
  options: ({ match: { params: { cloudId } } }) => {

    const { page, displayName, activeStatus, productUse, productAdmin, sitePrivilege } = parse(location.search);

    return {
      variables: {
        cloudId,
        input: {
          count: ROWS_PER_PAGE,
          start: getStart(sanitizePage(page), ROWS_PER_PAGE),
          activeStatus,
          displayName,
          productUse,
          productAdmin,
          sitePrivilege,
        },
      },
    };
  },
});

const withCurrentUser = graphql<RouteComponentProps<SiteParams>, IsCurrentAdminSystemAdminQuery>(currentUserQuery, {
  name: 'currentSite',
  options: ({ match: { params: { cloudId } } }) => ({
    variables: {
      cloudId,
    },
  }),
});

export const UsersPage = withRouter<{}>(
  withCurrentUser(
    withUsersList(
      injectIntl(
        withFlag(
          UsersListPageImpl,
        ),
      ),
    ),
  ),
);
