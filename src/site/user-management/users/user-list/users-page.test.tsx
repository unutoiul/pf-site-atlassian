import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import AkEmptyState from '@atlaskit/empty-state';
import AkSpinner from '@atlaskit/spinner';

import { ScreenEventSender } from 'common/analytics';

import { createMockIntlProp, noop } from '../../../../utilities/testing';
import { UsersQuickInvite } from '../users-quick-invite/users-quick-invite';
import { UserListFilter } from './user-list-filters/users-list-filter';
import { UserListPageLayout } from './users-list-page-layout';
import { UsersListTable } from './users-list-table';
import { UsersListPageImpl } from './users-page';

describe('Users Page', () => {
  const generateUsers = (limit: number) => {
    const users: any[] = [];
    for (let i = 0; i < limit; i++) {
      users.push({
        __typename: 'User',
        id: i + Math.floor(Math.random() * 100),
        displayName: `Prince Corgi ${i}`,
        email: 'corgi@happyfriends.paws',
        active: i % 2 === 0,
        presence: '2017-07-10T14:00:00Z',
        admin: i % 5 === 0,
        activeStatus: i % 4 === 0 ? 'ENABLED' : i % 3 === 0 ? 'DISABLED' : 'BLOCKED',
        system: false,
      });
    }

    return users;
  };

  const pageDefaults = {
    intl: createMockIntlProp(),
    history: { replace: noop } as any,
    match: { params: { cloudId: 'DUMMY_ORG_ID' } } as any,
    showFlag: noop,
    hideFlag: noop,
  };

  const currentSiteDefaults = {
    currentUser: {
      isSystemAdmin: false,
    },
  };

  const locationDefaults = {
    search: '',
  };

  const currentSiteData = {
    currentSite: {
      __typename: 'CurrentSite',
      id: 'dummy-id',
      flags: {
        __typename: 'SiteFlags',
        userExportADG3Migration: true,
      },
    },
  };

  function createShallowUsersPage(data, currentSite, location) {
    return shallow(
      <UsersListPageImpl {...pageDefaults} data={{ ...currentSiteData, ...data }} location={location} currentSite={currentSite} />,
    );
  }

  describe('Loading and error state', () => {
    it('Should show loading spinner on page when loading / not initially loaded', () => {
      const wrapper = createShallowUsersPage({ loading: true }, currentSiteDefaults, locationDefaults);
      wrapper.setState({ hasInitiallyLoaded: false });
      expect(wrapper.find(AkSpinner).length).to.equal(1);
    });

    it('Should show error', () => {
      const wrapper = createShallowUsersPage({ loading: false, error: true }, currentSiteDefaults, locationDefaults);
      wrapper.setState({ hasInitiallyLoaded: false });
      expect(wrapper.find(AkEmptyState).length).to.equal(1);
    });

    it('Should have disabled buttons', () => {
      const wrapper = createShallowUsersPage({ loading: false, error: true }, currentSiteDefaults, locationDefaults);
      wrapper.setState({ hasInitiallyLoaded: false });
      expect(wrapper.find(UserListPageLayout).length).to.equal(1);
      expect(wrapper.find(UserListPageLayout).props().isDisabled).to.equal(true);
    });
  });

  describe('Quick invite', () => {
    it('Should show quick invite when there are no users', () => {
      const wrapper = createShallowUsersPage({ loading: false, error: false, siteUsersList: { total: 0, users: [] } }, currentSiteDefaults, locationDefaults);
      expect(wrapper.find(UsersQuickInvite).length).to.equal(1);
      expect(wrapper.find(UserListFilter).length).to.equal(0);
    });

    it('Should show quick invite when there are less than 5 users', () => {
      const wrapper = createShallowUsersPage({ loading: false, error: false, siteUsersList: { total: 3, users: generateUsers(3) } }, currentSiteDefaults, locationDefaults);
      expect(wrapper.find(UsersQuickInvite).length).to.equal(1);
      expect(wrapper.find(UserListFilter).length).to.equal(0);
    });

    it('Should not show quick invite when there are more than 5 users', () => {
      const wrapper = createShallowUsersPage({ loading: false, error: false, siteUsersList: { total: 6, users: generateUsers(6) } }, currentSiteDefaults, locationDefaults);
      expect(wrapper.find(UsersQuickInvite).length).to.equal(0);
      expect(wrapper.find(UserListFilter).length).to.equal(1);
    });

    it('Four users + a system user should show quick invite', () => {
      const wrapper = createShallowUsersPage({
        loading: false, error: false, siteUsersList: {
          total: 5, users: [...generateUsers(4), {
            __typename: 'User',
            id: 'system-user',
            displayName: 'system user',
            email: 'systemcorgi@happyfriends.paws',
            active: true,
            presence: '2017-07-10T14:00:00Z',
            admin: true,
            activeStatus: 'ENABLED',
            system: true,
          }],
        },
      }, currentSiteDefaults, locationDefaults);
      expect(wrapper.find(UsersQuickInvite).length).to.equal(1);
      expect(wrapper.find(UserListFilter).length).to.equal(0);
    });

    it('Should not show quick invite if page is defined in query parameters', () => {
      const TOTAL_USERS = 2;
      const data = { loading: false, error: false, siteUsersList: { total: TOTAL_USERS, users: generateUsers(TOTAL_USERS) } };
      const location = { search: '?page=3' };
      const wrapper = createShallowUsersPage(data, currentSiteDefaults, location);
      expect(wrapper.find(UsersQuickInvite).length).to.equal(0);
      expect(wrapper.find(UserListFilter).length).to.equal(1);
    });

    it('Should not show quick invite if displayName is defined in query parameters', () => {
      const TOTAL_USERS = 2;
      const data = { loading: false, error: false, siteUsersList: { total: TOTAL_USERS, users: generateUsers(TOTAL_USERS) } };
      const location = { search: '?displayName=HappyCorgi' };
      const wrapper = createShallowUsersPage(data, currentSiteDefaults, location);
      expect(wrapper.find(UsersQuickInvite).length).to.equal(0);
      expect(wrapper.find(UserListFilter).length).to.equal(1);
    });

    it('Should not show quick invite if activeStatus is defined in query parameters', () => {
      const TOTAL_USERS = 2;
      const data = { loading: false, error: false, siteUsersList: { total: TOTAL_USERS, users: generateUsers(TOTAL_USERS) } };
      const location = { search: '?activeStatus=active' };
      const wrapper = createShallowUsersPage(data, currentSiteDefaults, location);
      expect(wrapper.find(UsersQuickInvite).length).to.equal(0);
      expect(wrapper.find(UserListFilter).length).to.equal(1);
    });

    it('Should not show quick invite if productUse is defined in query parameters', () => {
      const TOTAL_USERS = 2;
      const data = { loading: false, error: false, siteUsersList: { total: TOTAL_USERS, users: generateUsers(TOTAL_USERS) } };
      const location = { search: '?productUse=yes' };
      const wrapper = createShallowUsersPage(data, currentSiteDefaults, location);
      expect(wrapper.find(UsersQuickInvite).length).to.equal(0);
      expect(wrapper.find(UserListFilter).length).to.equal(1);
    });

    it('Should not show quick invite if productAdmin is defined in query parameters', () => {
      const TOTAL_USERS = 2;
      const data = { loading: false, error: false, siteUsersList: { total: TOTAL_USERS, users: generateUsers(TOTAL_USERS) } };
      const location = { search: '?productAdmin=bobross' };
      const wrapper = createShallowUsersPage(data, currentSiteDefaults, location);
      expect(wrapper.find(UsersQuickInvite).length).to.equal(0);
      expect(wrapper.find(UserListFilter).length).to.equal(1);
    });

    it('Should not show quick invite if sitePrivilege is defined in query parameters', () => {
      const TOTAL_USERS = 2;
      const data = { loading: false, error: false, siteUsersList: { total: TOTAL_USERS, users: generateUsers(TOTAL_USERS) } };
      const location = { search: '?sitePrivilege=vip' };
      const wrapper = createShallowUsersPage(data, currentSiteDefaults, location);
      expect(wrapper.find(UsersQuickInvite).length).to.equal(0);
      expect(wrapper.find(UserListFilter).length).to.equal(1);
    });
  });

  describe('Users list table', () => {
    describe('System administrator', () => {
      it('When current site query results are missing, we assume the user is NOT a system user', () => {
        const TOTAL_USERS = 15;
        const data = { loading: false, error: false, siteUsersList: { total: TOTAL_USERS, users: generateUsers(TOTAL_USERS) } };
        const wrapper = createShallowUsersPage(data, undefined, locationDefaults);
        expect(wrapper.find(UsersListTable).length).to.equal(1);
        expect(wrapper.find(UsersListTable).props().isCurrentUserSystemAdmin).to.equal(false);
      });

      it('When current user query results are missing, we assume the user is NOT a system user', () => {
        const TOTAL_USERS = 15;
        const data = { loading: false, error: false, siteUsersList: { total: TOTAL_USERS, users: generateUsers(TOTAL_USERS) } };
        const wrapper = createShallowUsersPage(data, { currentUser: undefined }, locationDefaults);
        expect(wrapper.find(UsersListTable).length).to.equal(1);
        expect(wrapper.find(UsersListTable).props().isCurrentUserSystemAdmin).to.equal(false);
      });

      it('When current user is not a system admin, the user list table should not show system users', () => {
        const TOTAL_USERS = 15;
        const data = { loading: false, error: false, siteUsersList: { total: TOTAL_USERS, users: generateUsers(TOTAL_USERS) } };
        const wrapper = createShallowUsersPage(data, { currentUser: { isSystemAdmin: false } }, locationDefaults);
        expect(wrapper.find(UsersListTable).length).to.equal(1);
        expect(wrapper.find(UsersListTable).props().isCurrentUserSystemAdmin).to.equal(false);
      });

      it('When current user is a system admin, the user list table should show system users', () => {
        const TOTAL_USERS = 15;
        const data = { loading: false, error: false, siteUsersList: { total: TOTAL_USERS, users: generateUsers(TOTAL_USERS) } };
        const wrapper = createShallowUsersPage(data, { currentUser: { isSystemAdmin: true } }, locationDefaults);
        expect(wrapper.find(UsersListTable).length).to.equal(1);
        expect(wrapper.find(UsersListTable).props().isCurrentUserSystemAdmin).to.equal(true);
      });
    });

    describe('Current page', () => {
      it('Current page defaults to 1 if not specified as a query parameter', () => {
        const TOTAL_USERS = 15;
        const data = { loading: false, error: false, siteUsersList: { total: TOTAL_USERS, users: generateUsers(TOTAL_USERS) } };
        const location = { search: '' };
        const wrapper = createShallowUsersPage(data, currentSiteDefaults, location);
        expect(wrapper.find(UsersListTable).length).to.equal(1);
        expect(wrapper.find(UsersListTable).props().currentPage).to.equal(1);
      });

      it('Current page is obtained from window location', () => {
        const TOTAL_USERS = 15;
        const data = { loading: false, error: false, siteUsersList: { total: TOTAL_USERS, users: generateUsers(TOTAL_USERS) } };
        const location = { search: '?page=3' };
        const wrapper = createShallowUsersPage(data, currentSiteDefaults, location);
        expect(wrapper.find(UsersListTable).length).to.equal(1);
        expect(wrapper.find(UsersListTable).props().currentPage).to.equal(3);
      });
    });
    describe('Analytics', () => {
      it('should send correct props to screen event sender', () => {
        const TOTAL_USERS = 21;
        const wrapper = createShallowUsersPage({ ...pageDefaults, loading: false, error: false, siteUsersList: { total: TOTAL_USERS, users: generateUsers(TOTAL_USERS) } }, currentSiteDefaults, locationDefaults);

        expect(wrapper.find(ScreenEventSender).prop('event')).to.deep.equal({
          data: {
            attributes: {
              userCountOnScreen: 20,
              totalUserCount: TOTAL_USERS,
              quickInviteVisible: false,
              userTypesSelected: [],
              productKeysSelected: [],
              adminTypesSelected: [],
            },
            name: 'userListScreen',
          },
        });
      });
      it('should send activeStatus, adminType, userType from query params in screen event', () => {
        const TOTAL_USERS = 15;
        const location = { ...pageDefaults, search: '?activeStatus=active&productUse=jira-servicedesk&productUse=jira-software&productAdmin=jira-admin&productAdmin=confluence&sitePrivilege=site-admin' };
        const wrapper = createShallowUsersPage({ loading: false, error: false, siteUsersList: { total: TOTAL_USERS, users: generateUsers(TOTAL_USERS) } }, currentSiteDefaults, location);

        expect(wrapper.find(ScreenEventSender).prop('event')).to.deep.equal({
          data: {
            attributes: {
              userCountOnScreen: TOTAL_USERS,
              totalUserCount: TOTAL_USERS,
              quickInviteVisible: false,
              userTypesSelected: ['active'],
              productKeysSelected: ['jira-servicedesk', 'jira-software'],
              adminTypesSelected: ['site-admin', 'jira-admin', 'confluence'],
            },
            name: 'userListScreen',
          },
        });
      });
      it('should send quick invite status in screen event', () => {
        const TOTAL_USERS = 2;
        const wrapper = createShallowUsersPage({ ...pageDefaults, loading: false, error: false, siteUsersList: { total: TOTAL_USERS, users: generateUsers(TOTAL_USERS) } }, currentSiteDefaults, locationDefaults);

        expect(wrapper.find(ScreenEventSender).prop('event')).to.deep.equal({
          data: {
            attributes: {
              userCountOnScreen: TOTAL_USERS,
              totalUserCount: TOTAL_USERS,
              quickInviteVisible: true,
              userTypesSelected: [],
              productKeysSelected: [],
              adminTypesSelected: [],
            },
            name: 'userListScreen',
          },
        });
      });
    });

    it('should pass on displayName to the table from the query parameter', () => {
      const TOTAL_USERS = 15;
      const data = { loading: false, error: false, siteUsersList: { total: TOTAL_USERS, users: generateUsers(TOTAL_USERS) } };
      const location = { search: '?displayName=corgi' };
      const wrapper = createShallowUsersPage(data, currentSiteDefaults, location);
      expect(wrapper.find(UsersListTable).length).to.equal(1);
      expect(wrapper.find(UsersListTable).props().displayNameQuery).to.equal('corgi');
    });

    it('should pass on the users count to the filter component', () => {
      const data = { loading: false, error: false, siteUsersList: { total: 40, users: generateUsers(20) } };
      const location = { search: '' };
      const wrapper = createShallowUsersPage(data, currentSiteDefaults, location);

      expect(wrapper.find(UserListFilter).length).to.equal(1);
      expect(wrapper.find(UserListFilter).props().userCountOnScreen).to.equal(20);
      expect(wrapper.find(UserListFilter).props().totalUserCount).to.equal(40);
    });
  });
});
