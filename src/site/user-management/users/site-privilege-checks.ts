// Note: It's uncommon to have a meaningful property like this that isn't in the schema.
// This is a workaround because the backend cannot return us this information: https://sdog.jira-dev.com/browse/KT-1542
// Adding to the schema in a case like this is a preferred option.

interface UsersGroupsSitePrivilege {
  groups: {
    groups: Array<{
      sitePrivilege: string;
    }> | null,
  };
}

export function isUserInSiteAdminGroup(user: UsersGroupsSitePrivilege): boolean {
  // We check that the user is a site administrator through their group memberships, however, results become paginated
  // if they are a member of more than 100 groups. We assume here that their group membership's are below this number,
  // until this property is exposed in https://sdog.jira-dev.com/browse/KT-1542
  return !!(
    user &&
    user.groups &&
    user.groups.groups &&
    user.groups.groups.some(group => group.sitePrivilege === 'SITE_ADMIN')
  );
}

export function isUserInSystemAdminGroup(user: UsersGroupsSitePrivilege): boolean {
  // We check that the user is a system administrator through their group memberships, however, results become paginated
  // if they are a member of more than 100 groups. We assume here that their group membership's are below this number,
  // until this property is exposed in https://sdog.jira-dev.com/browse/KT-1542
  return !!(
    user &&
    user.groups &&
    user.groups.groups &&
    user.groups.groups.some(group => group.sitePrivilege === 'SYS_ADMIN')
  );
}
