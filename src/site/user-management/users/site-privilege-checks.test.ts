import { expect } from 'chai';

import { isUserInSiteAdminGroup, isUserInSystemAdminGroup } from './site-privilege-checks';

describe('Site privilege checks', () => {
  describe('isUseInSiteAdminGroup', () => {
    it('should return true for group SITE_ADMIN', () => {
      expect(isUserInSiteAdminGroup({
        groups: {
          groups: [
            {
              sitePrivilege: 'SITE_ADMIN',
            },
          ],
        },
      })).to.equal(true);
    });

    it('should return false for group SYS_ADMIN', () => {
      expect(isUserInSiteAdminGroup({
        groups: {
          groups: [
            {
              sitePrivilege: 'SYS_ADMIN',
            },
          ],
        },
      })).to.equal(false);
    });

    it('should return false for group OTHER_ADMIN', () => {
      expect(isUserInSiteAdminGroup({
        groups: {
          groups: [
            {
              sitePrivilege: 'OTHER_ADMIN',
            },
          ],
        },
      })).to.equal(false);
    });
  });

  describe('isUserInSystemAdminGroup', () => {
    it('should return false for group SITE_ADMIN', () => {
      expect(isUserInSystemAdminGroup({
        groups: {
          groups: [
            {
              sitePrivilege: 'SITE_ADMIN',
            },
          ],
        },
      })).to.equal(false);
    });

    it('should return true for group SYS_ADMIN', () => {
      expect(isUserInSystemAdminGroup({
        groups: {
          groups: [
            {
              sitePrivilege: 'SYS_ADMIN',
            },
          ],
        },
      })).to.equal(true);
    });

    it('should return false for group OTHER_ADMIN', () => {
      expect(isUserInSystemAdminGroup({
        groups: {
          groups: [
            {
              sitePrivilege: 'OTHER_ADMIN',
            },
          ],
        },
      })).to.equal(false);
    });
  });
});
