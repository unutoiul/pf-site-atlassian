import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import AkButton from '@atlaskit/button';
import { Checkbox as AkCheckbox } from '@atlaskit/checkbox';
import AkEmptyState from '@atlaskit/empty-state';
import OriginTracing from '@atlassiansox/origin-tracing';

import { gridSize as akGridSize } from '@atlaskit/theme';

import { teamOnboardingImage } from 'common/images';

import { createErrorIcon, createSuccessIcon } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';

import {
  AnalyticsClientProps,
  UserInviteAttributes,
  userQuickInviteSentTrackEventData,
  userQuickInviteSentUIEventData,
  withAnalyticsClient,
} from 'common/analytics';

import { InviteUsersMutation, InviteUsersMutationArgs, RolePermissionsQuery, SiteInviteUsersInput } from '../../../../schema/schema-types';
import { TrustedUsersFeatureFlag, withTrustedUsersFeatureFlag } from '../../../feature-flags';
import { Permissions } from '../user-permission-section/permissions';
import { UsersQuickInviteInputGroup } from './users-quick-invite-input-group';
import inviteUsers from './users-quick-invite.mutation.graphql';
import rolePermissionsQuery from './users-quick-invite.query.graphql';
interface OwnProps {
  cloudId: string;
  onInvite(): void;
}
type UsersQuickInviteProps = ChildProps<OwnProps & AnalyticsClientProps, InviteUsersMutation & RolePermissionsQuery> & InjectedIntlProps;

interface OwnState {
  emails: string[];
  isMutationLoading: boolean;
  permissionSelected: boolean;
}

const Form = styled.form`
  padding-bottom: ${akGridSize() * 4}px;
`;

const ButtonWrapper = styled.div`
  padding-top: ${akGridSize() * 3}px;
`;

const InviteSection = styled.div`
  max-width: ${akGridSize() * 37}px;
  margin: auto;
`;

const CheckboxWrapper = styled.div`
  padding: ${akGridSize() * 3}px 0 0 0;
`;

const ADD_PRODUCTS_PERMISSION = 'add-products';
const INVITE_USERS_PERMISSION = 'invite-users';

const messages = defineMessages({
  inviteTitle: {
    id: 'users.list.invite.title',
    description: 'Tagline to invite users when there are no users on the site',
    defaultMessage: 'There\'s a team behind every success',
  },
  inviteDescription: {
    id: 'users.list.invite.description',
    description: 'Tagline to invite users when there are no users on the site',
    defaultMessage: 'Invite your team and start creating great things together',
  },
  inviteButton: {
    id: 'users.list.invite.button',
    description: 'Button to invite users when there are no users on the site',
    defaultMessage: 'Invite team members',
  },
  inviteError: {
    id: 'users.list.invite.error',
    description: 'The body for the error message that occurs when adding multiple users to a site',
    defaultMessage: 'Something went wrong when adding users to your site.',
  },
  inviteErrorTitle: {
    id: 'users.list.invite.error.title',
    description: 'The title for the error message that occurs when adding multiple users to a site',
    defaultMessage: 'Could not add users',
  },
  invitePermissionAddProducts: {
    id: 'users.list.invite.permission.add.products.label',
    description: 'The label for the checkbox that grants permissions (add-products) to the invited users',
    defaultMessage: 'Allow these users to add and configure products.',
  },
  invitePermissionAddProductsInviteUsers: {
    id: 'users.list.invite.permission.add.products.invite.users.label',
    description: 'The label for the checkbox that grants permissions (add-products/invite-users) to the invited users',
    defaultMessage: 'Allow these users to add and configure products, and invite users.',
  },
  inviteSuccessTitle: {
    id: 'users.list.invite.success.title',
    description: 'The title for the success message that occurs when adding multiple users to a site',
    defaultMessage: 'Users invited to site',
  },
  inviteSuccess: {
    id: 'users.list.invite.success',
    description: 'The body for the error message that occurs when adding multiple users to a site',
    defaultMessage: 'The users have been invited to join your site.',
  },
});

export class UsersQuickInviteImpl extends React.Component<UsersQuickInviteProps & FlagProps & TrustedUsersFeatureFlag, OwnState> {
  public readonly state: Readonly<OwnState> = {
    emails: [],
    isMutationLoading: false,
    permissionSelected: true,
  };

  public render() {
    const { data, intl, trustedUsersFeatureFlag } = this.props;
    const { formatMessage } = intl;

    return (
      <Form onSubmit={this.onSubmit}>
        <AkEmptyState
          imageUrl={teamOnboardingImage}
          header={formatMessage(messages.inviteTitle)}
          description={formatMessage(messages.inviteDescription)}
          size="wide"
        />
        <InviteSection>
          <UsersQuickInviteInputGroup
            emails={this.state.emails}
            onChange={this.onInputGroupChange}
            disabled={this.state.isMutationLoading}
          />
          {data && data.rolePermissions && data.rolePermissions.permissionIds && data.rolePermissions.permissionIds.length > 0 && this.validatePermissionIds(data.rolePermissions.permissionIds) && trustedUsersFeatureFlag.value &&
            <CheckboxWrapper>
              <AkCheckbox
                id={'role-permissions-checkbox'}
                name={'role-permissions'}
                defaultChecked={true}
                onChange={this.onPermissionChange}
                label={formatMessage(this.getPermissionCheckboxMessage(data.rolePermissions.permissionIds))}
              />
            </CheckboxWrapper>
          }
          <ButtonWrapper>
            <AkButton
              id="user-quick-invite-submit"
              type="submit"
              appearance="primary"
              isDisabled={this.getEnteredEmails(this.state.emails).length === 0}
              isLoading={this.state.isMutationLoading}
              shouldFitContainer={true}
              onClick={this.onSubmit}
            >
              {formatMessage(messages.inviteButton)}
            </AkButton>
          </ButtonWrapper>
        </InviteSection>
      </Form>
    );
  }

  // Currently we only handle the add-products and invite-users permissions in this form.
  private validatePermissionIds = (permissionIds) => {
    return permissionIds.filter((id) => ![ADD_PRODUCTS_PERMISSION, INVITE_USERS_PERMISSION].includes(id)).length === 0;
  };

  private getPermissionCheckboxMessage = (permissionIds) => {
    let permissionCheckboxMessage;
    if (permissionIds.includes(ADD_PRODUCTS_PERMISSION) && permissionIds.includes(INVITE_USERS_PERMISSION)) {
      permissionCheckboxMessage = messages.invitePermissionAddProductsInviteUsers;
    } else if (permissionIds.includes(ADD_PRODUCTS_PERMISSION)) {
      permissionCheckboxMessage = messages.invitePermissionAddProducts;
    }

    return permissionCheckboxMessage;
  };

  private getEnteredEmails = (emails: string[]) => {
    return emails.filter(email => email && email.length);
  };

  private onInputGroupChange = (emails) => {
    this.setState({ emails });
  };

  private onPermissionChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const permissionSelected = e.currentTarget.checked;
    this.setState({
      permissionSelected,
    });
  };

  private showSuccessFlag = () => {
    const { formatMessage } = this.props.intl;

    this.props.showFlag({
      autoDismiss: true,
      description: formatMessage(messages.inviteSuccess),
      icon: createSuccessIcon(),
      id: 'users-list-invite-success',
      title: formatMessage(messages.inviteSuccessTitle),
    });
  };

  private showErrorFlag = () => {
    const { formatMessage } = this.props.intl;

    this.props.showFlag({
      autoDismiss: true,
      description: formatMessage(messages.inviteError),
      icon: createErrorIcon(),
      id: `users-list-invite-error-${Date.now()}`,
      title: formatMessage(messages.inviteErrorTitle),
    });
  };

  private onSubmit = async (event?: React.FormEvent<HTMLFormElement>) => {
    if (event) {
      event.preventDefault();
    }

    if (!this.props.mutate) {
      return;
    }

    this.setState({
      isMutationLoading: true,
    });
    try {
      const origin = new OriginTracing({ product: 'admin' });
      const role = (this.props.data
        && this.props.data.rolePermissions
        && this.props.data.rolePermissions.permissionIds
        && this.props.data.rolePermissions.permissionIds.length > 0
        && this.validatePermissionIds(this.props.data.rolePermissions.permissionIds)
        && this.props.trustedUsersFeatureFlag.value
        && this.state.permissionSelected)
        ? Permissions.TRUSTED
        : Permissions.BASIC;

      this.props.analyticsClient.sendUIEvent({
        cloudId: this.props.cloudId,
        data: userQuickInviteSentUIEventData({
          ...this.getAnalyticsAttributes(this.state.emails, role),
          ...origin.toAnalyticsAttributes({ hasGeneratedId: true }),
        }),
      });

      const inviteSuccessful = await this.props.mutate({
        variables: {
          id: this.props.cloudId,
          input: {
            emails: this.getEnteredEmails(this.state.emails),
            role,
            atlOrigin: origin.encode(),
            sendNotification: true,
          } as SiteInviteUsersInput,
        },
      });

      if (inviteSuccessful) {
        this.props.analyticsClient.sendTrackEvent({
          cloudId: this.props.cloudId,
          data: userQuickInviteSentTrackEventData({
            ...this.getAnalyticsAttributes(this.state.emails, role),
            ...origin.toAnalyticsAttributes({ hasGeneratedId: true }),
          }),
        });

        this.showSuccessFlag();

        this.setState({
          emails: [],
        });

        this.props.onInvite();
      } else {
        this.showErrorFlag();
      }
    } catch {
      this.showErrorFlag();
    } finally {
      this.setState({
        isMutationLoading: false,
      });
    }
  }

  private getAnalyticsAttributes = (emails: string[], role: Permissions): UserInviteAttributes => (
    {
      numberOfUsersInvited: emails.length,
      permissionType: role,
      isTrustedUserEnabled: this.props.trustedUsersFeatureFlag && this.props.trustedUsersFeatureFlag.value,
    }
  )
}

const withRolePermissionsQuery = graphql<OwnProps, InviteUsersMutation & RolePermissionsQuery>(rolePermissionsQuery, {
  skip: ({ cloudId }) => !cloudId,
  options: ({ cloudId }) => ({ variables: { cloudId } }),
});

const withInviteUsersMutation = graphql<OwnProps, InviteUsersMutation, InviteUsersMutationArgs>(inviteUsers);

export const UsersQuickInvite = withInviteUsersMutation(
  withRolePermissionsQuery(
    withFlag(
      injectIntl<OwnProps & FlagProps>(
        withTrustedUsersFeatureFlag(
          withAnalyticsClient(
            UsersQuickInviteImpl,
          ),
        ),
      ),
    ),
  ),
);
