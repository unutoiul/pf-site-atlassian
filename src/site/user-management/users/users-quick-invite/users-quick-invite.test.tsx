import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import AkButton from '@atlaskit/button';
import { Checkbox as AkCheckbox } from '@atlaskit/checkbox';
import OriginTracing from '@atlassiansox/origin-tracing';

import { InviteUsersMutationVariables } from '../../../../schema/schema-types';
import { createMockAnalyticsClient, createMockIntlContext, createMockIntlProp, waitUntil } from '../../../../utilities/testing';
import { Permissions } from '../user-permission-section/permissions';
import { UsersQuickInviteImpl } from './users-quick-invite';
import { UsersQuickInviteInputGroup } from './users-quick-invite-input-group';

const noop = () => null;
let analyticsClient = createMockAnalyticsClient();

describe('Users quick invite', () => {
  const originToAnalyticsAttributes = OriginTracing.prototype.toAnalyticsAttributes;
  beforeEach(() => {
    OriginTracing.prototype.toAnalyticsAttributes = sinon.stub().returns({
      originProduct: 'admin',
      originIdGenerated: 'some-origin-id',
    });
  });

  afterEach(() => {
    OriginTracing.prototype.toAnalyticsAttributes = originToAnalyticsAttributes;
  });

  function createTestShallowComponent(otherProps?): ShallowWrapper {
    return shallow(
      <UsersQuickInviteImpl
        cloudId={'dummy-id'}
        intl={createMockIntlProp()}
        showFlag={noop}
        hideFlag={noop}
        analyticsClient={analyticsClient}
        {...otherProps}
      />,
      createMockIntlContext(),
    );
  }

  it('does not call onInvite when clicking the button if there are no emails', () => {
    const onInviteSpy = sinon.spy();

    const wrapper = createTestShallowComponent({ onInvite: onInviteSpy });

    wrapper.find(AkButton).first().simulate('click');

    expect(onInviteSpy.callCount).to.equal(0);
  });

  it('calls onInvite when clicking the button if there are some emails', async () => {
    const onInviteSpy = sinon.spy();

    const wrapper = createTestShallowComponent({ onInvite: onInviteSpy, mutate: () => true as any });

    wrapper.find(UsersQuickInviteInputGroup).simulate('change', [
      'cyberdash@gmail.com',
    ]);
    wrapper.find(AkButton).first().simulate('click');
    expect(wrapper.find(AkButton).first().prop('isLoading')).to.equal(true);
    await waitUntil(() => onInviteSpy.callCount > 0);
    expect(onInviteSpy.callCount).to.equal(1);
  });

  it('does not do a mutation with undefined emails if given them by the input group', async () => {
    const mutateSpy = sinon.spy();

    const wrapper = createTestShallowComponent({ mutate: mutateSpy });

    wrapper.find(UsersQuickInviteInputGroup).simulate('change', [
      undefined,
      undefined,
      'cyberdash@gmail.com',
    ]);
    wrapper.find(AkButton).first().simulate('click');

    await waitUntil(() => mutateSpy.callCount > 0);
    expect(mutateSpy.callCount).to.equal(1);
    expect((mutateSpy.getCalls()[0].args[0].variables as InviteUsersMutationVariables).input.emails!.length).to.equal(1);
    expect((mutateSpy.getCalls()[0].args[0].variables as InviteUsersMutationVariables).input.emails![0]).to.equal('cyberdash@gmail.com');
  });

  it('shows a flag when clicking the button if there are some emails', async () => {
    const showFlagSpy = sinon.spy();

    const wrapper = createTestShallowComponent({ onInvite: noop, showFlag: showFlagSpy, mutate: () => true as any });

    wrapper.find(UsersQuickInviteInputGroup).simulate('change', [
      'cyberdash@gmail.com',
    ]);
    wrapper.find(AkButton).first().simulate('click');

    await waitUntil(() => showFlagSpy.callCount > 0);
    expect(showFlagSpy.callCount).to.equal(1);
  });

  it('shows a checkbox when the admin.trusted.users flag is enabled', async () => {
    const trustedUsersFeatureFlagOverride = {
      isLoading: false,
      value: true,
    };

    const permissionData = {
      loading: false,
      rolePermissions: {
        permissionIds: ['add-products', 'invite-users'],
      },
    };

    const wrapper = createTestShallowComponent({ data: permissionData, trustedUsersFeatureFlag: trustedUsersFeatureFlagOverride, onInvite: noop, showFlag: noop, mutate: () => true as any });

    expect(wrapper.find(AkCheckbox).exists()).to.equal(true);
  });

  it('does not show a checkbox when the admin.trusted.users flag is disabled', async () => {
    const trustedUsersFeatureFlagOverride = {
      isLoading: false,
      value: false,
    };

    const permissionData = {
      loading: false,
      rolePermissions: {
        permissionIds: ['add-products', 'invite-users'],
      },
    };

    const wrapper = createTestShallowComponent({ data: permissionData, trustedUsersFeatureFlag: trustedUsersFeatureFlagOverride, onInvite: noop, showFlag: noop, mutate: () => true as any });

    expect(wrapper.find(AkCheckbox).exists()).to.equal(false);
  });

  it('does not show a checkbox when there is an unsupported permissionId', async () => {
    const trustedUsersFeatureFlagOverride = {
      isLoading: false,
      value: true,
    };

    const permissionData = {
      loading: false,
      rolePermissions: {
        permissionIds: ['add-products', 'fake-permission'],
      },
    };

    const wrapper = createTestShallowComponent({ data: permissionData, trustedUsersFeatureFlag: trustedUsersFeatureFlagOverride, onInvite: noop, showFlag: noop, mutate: () => true as any });

    expect(wrapper.find(AkCheckbox).exists()).to.equal(false);
  });

  it('does not show a checkbox when there the list of permissionIds is empty', async () => {
    const trustedUsersFeatureFlagOverride = {
      isLoading: false,
      value: true,
    };

    const permissionData = {
      loading: false,
      rolePermissions: {
        permissionIds: [],
      },
    };

    const wrapper = createTestShallowComponent({ data: permissionData, trustedUsersFeatureFlag: trustedUsersFeatureFlagOverride, onInvite: noop, showFlag: noop, mutate: () => true as any });

    expect(wrapper.find(AkCheckbox).exists()).to.equal(false);
  });

  it('mutation passes sendNotification: true', async () => {
    const trustedUsersFeatureFlagOverride = {
      isLoading: false,
      value: false,
    };

    const permissionData = {
      loading: false,
      rolePermissions: {},
    };
    const mutateSpy = sinon.spy();

    const wrapper = createTestShallowComponent({
      data: permissionData,
      trustedUsersFeatureFlag:
        trustedUsersFeatureFlagOverride,
      onInvite: noop,
      showFlag: noop,
      mutate: mutateSpy,
    });

    wrapper.find(UsersQuickInviteInputGroup).simulate('change', [
      'cyberdash+1@gmail.com',
      'cyberdash+2@gmail.com',
    ]);
    wrapper.find(AkButton).first().simulate('click');

    expect(mutateSpy.callCount).to.equal(1);
    expect((mutateSpy.getCalls()[0].args[0].variables as InviteUsersMutationVariables).input.sendNotification!).to.equal(true);
  });

  it('mutation passes basic user role when feature flag is turned off', async () => {
    const trustedUsersFeatureFlagOverride = {
      isLoading: false,
      value: false,
    };

    const permissionData = {
      loading: false,
      rolePermissions: {},
    };
    const mutateSpy = sinon.spy();

    const wrapper = createTestShallowComponent({
      data: permissionData,
      trustedUsersFeatureFlag:
        trustedUsersFeatureFlagOverride,
      onInvite: noop,
      showFlag: noop,
      mutate: mutateSpy,
    });

    wrapper.find(UsersQuickInviteInputGroup).simulate('change', [
      'cyberdash+1@gmail.com',
      'cyberdash+2@gmail.com',
    ]);
    wrapper.find(AkButton).first().simulate('click');

    expect(wrapper.find(AkCheckbox).exists()).to.equal(false);

    expect(mutateSpy.callCount).to.equal(1);
    expect((mutateSpy.getCalls()[0].args[0].variables as InviteUsersMutationVariables).input.emails!.length).to.equal(2);
    expect((mutateSpy.getCalls()[0].args[0].variables as InviteUsersMutationVariables).input.emails![0]).to.equal('cyberdash+1@gmail.com');
    expect((mutateSpy.getCalls()[0].args[0].variables as InviteUsersMutationVariables).input.emails![1]).to.equal('cyberdash+2@gmail.com');
    expect((mutateSpy.getCalls()[0].args[0].variables as InviteUsersMutationVariables).input.role).to.equal(Permissions.BASIC);
  });

  it('mutation passes basic user role when feature flag is turned on and user does not select trusted user checkbox', async () => {
    const trustedUsersFeatureFlagOverride = {
      isLoading: false,
      value: true,
    };

    const permissionData = {
      loading: false,
      rolePermissions: {
        permissionIds: ['add-products', 'invite-users'],
      },
    };
    const mutateSpy = sinon.spy();

    const wrapper = createTestShallowComponent({
      data: permissionData,
      trustedUsersFeatureFlag:
        trustedUsersFeatureFlagOverride,
      onInvite: noop,
      showFlag: noop,
      mutate: mutateSpy,
    });

    wrapper.find(UsersQuickInviteInputGroup).simulate('change', [
      'cyberdash+1@gmail.com',
      'cyberdash+2@gmail.com',
    ]);

    const checkbox = wrapper.find(AkCheckbox).find({ id: 'role-permissions-checkbox' });
    expect(checkbox.exists()).to.equal(true);
    expect(checkbox.prop('defaultChecked')).to.equal(true);

    checkbox.simulate('change', {
      currentTarget: { checked: false },
    });

    wrapper.find(AkButton).first().simulate('click');

    expect(mutateSpy.callCount).to.equal(1);
    expect((mutateSpy.getCalls()[0].args[0].variables as InviteUsersMutationVariables).input.emails!.length).to.equal(2);
    expect((mutateSpy.getCalls()[0].args[0].variables as InviteUsersMutationVariables).input.emails![0]).to.equal('cyberdash+1@gmail.com');
    expect((mutateSpy.getCalls()[0].args[0].variables as InviteUsersMutationVariables).input.emails![1]).to.equal('cyberdash+2@gmail.com');
    expect((mutateSpy.getCalls()[0].args[0].variables as InviteUsersMutationVariables).input.role).to.equal(Permissions.BASIC);
  });

  it('mutation passes trusted user role when feature flag is turned on and user selects the trusted user checkbox', async () => {
    const trustedUsersFeatureFlagOverride = {
      isLoading: false,
      value: true,
    };

    const permissionData = {
      loading: false,
      rolePermissions: {
        permissionIds: ['add-products', 'invite-users'],
      },
    };
    const mutateSpy = sinon.spy();

    const wrapper = createTestShallowComponent({
      data: permissionData,
      trustedUsersFeatureFlag:
        trustedUsersFeatureFlagOverride,
      onInvite: noop,
      showFlag: noop,
      mutate: mutateSpy,
    });

    wrapper.find(UsersQuickInviteInputGroup).simulate('change', [
      'cyberdash+1@gmail.com',
      'cyberdash+2@gmail.com',
    ]);

    const checkbox = wrapper.find(AkCheckbox).find({ id: 'role-permissions-checkbox' });
    expect(checkbox.exists()).to.equal(true);
    expect(checkbox.prop('defaultChecked')).to.equal(true);

    wrapper.find(AkButton).first().simulate('click');

    expect(mutateSpy.callCount).to.equal(1);
    expect((mutateSpy.getCalls()[0].args[0].variables as InviteUsersMutationVariables).input.emails!.length).to.equal(2);
    expect((mutateSpy.getCalls()[0].args[0].variables as InviteUsersMutationVariables).input.emails![0]).to.equal('cyberdash+1@gmail.com');
    expect((mutateSpy.getCalls()[0].args[0].variables as InviteUsersMutationVariables).input.emails![1]).to.equal('cyberdash+2@gmail.com');
    expect((mutateSpy.getCalls()[0].args[0].variables as InviteUsersMutationVariables).input.role).to.equal(Permissions.TRUSTED);
  });

  describe('analytics', () => {
    beforeEach(() => {
      analyticsClient = createMockAnalyticsClient();
    });

    it('should send a track event and a ui event on success when feature flag is turned off', async () => {
      const trustedUsersFeatureFlagOverride = {
        isLoading: false,
        value: false,
      };

      const permissionData = {
        loading: false,
        rolePermissions: {},
      };
      const mutateStub = sinon.stub().returns(true);

      const wrapper = createTestShallowComponent({
        data: permissionData,
        trustedUsersFeatureFlag:
          trustedUsersFeatureFlagOverride,
        onInvite: noop,
        showFlag: noop,
        mutate: mutateStub,
      });

      expect(analyticsClient.sendUIEvent.callCount).to.equal(0);
      expect(analyticsClient.sendTrackEvent.callCount).to.equal(0);
      expect(analyticsClient.sendUIEvent.callCount).to.equal(0);

      wrapper.instance().setState({ emails: ['a@b.com', 'c@d.com'] });

      wrapper
        .find(AkButton)
        .find({ id: 'user-quick-invite-submit' })
        .first()
        .simulate('click');

      await waitUntil(() => mutateStub.called);
      mutateStub.returns(true);
      expect(analyticsClient.sendUIEvent.callCount).to.equal(1);

      expect(analyticsClient.sendUIEvent.lastCall.args[0]).to.deep.equal({
        cloudId: 'dummy-id',
        data: {
          action: 'clicked',
          actionSubject: 'button',
          actionSubjectId: 'inviteUsers',
          attributes: {
            numberOfUsersInvited: 2,
            permissionType: Permissions.BASIC,
            originIdGenerated: 'some-origin-id',
            originProduct: 'admin',
            isTrustedUserEnabled: false,
          },
          source: 'userListScreen',
        },
      });

      expect(analyticsClient.sendTrackEvent.callCount).to.equal(1);
      expect(analyticsClient.sendTrackEvent.lastCall.args[0]).to.deep.equal({
        cloudId: 'dummy-id',
        data: {
          action: 'invited',
          actionSubject: 'user',
          attributes: {
            numberOfUsersInvited: 2,
            permissionType: Permissions.BASIC,
            originIdGenerated: 'some-origin-id',
            originProduct: 'admin',
            isTrustedUserEnabled: false,
          },
          source: 'userListScreen',
        },
      });
    });

    it('should send a track event and a ui event for trusted user on success when feature flag is turned on', async () => {
      const trustedUsersFeatureFlagOverride = {
        isLoading: false,
        value: true,
      };

      const permissionData = {
        loading: false,
        rolePermissions: {
          permissionIds: ['add-products', 'invite-users'],
        },
      };
      const mutateStub = sinon.stub().returns(true);

      const wrapper = createTestShallowComponent({
        data: permissionData,
        trustedUsersFeatureFlag:
          trustedUsersFeatureFlagOverride,
        onInvite: noop,
        showFlag: noop,
        mutate: mutateStub,
      });

      expect(analyticsClient.sendUIEvent.callCount).to.equal(0);
      expect(analyticsClient.sendTrackEvent.callCount).to.equal(0);
      expect(analyticsClient.sendUIEvent.callCount).to.equal(0);

      wrapper.instance().setState({ emails: ['a@b.com', 'c@b.com'] });

      wrapper
        .find(AkButton)
        .find({ id: 'user-quick-invite-submit' })
        .first()
        .simulate('click');

      await waitUntil(() => mutateStub.called);

      expect(analyticsClient.sendTrackEvent.callCount).to.equal(1);
      expect(analyticsClient.sendUIEvent.callCount).to.equal(1);
      expect(analyticsClient.sendUIEvent.lastCall.args[0]).to.deep.equal({
        cloudId: 'dummy-id',
        data: {
          action: 'clicked',
          actionSubject: 'button',
          actionSubjectId: 'inviteUsers',
          attributes: {
            numberOfUsersInvited: 2,
            permissionType: Permissions.TRUSTED,
            originIdGenerated: 'some-origin-id',
            originProduct: 'admin',
            isTrustedUserEnabled: true,
          },
          source: 'userListScreen',
        },
      });

      expect(analyticsClient.sendTrackEvent.lastCall.args[0]).to.deep.equal({
        cloudId: 'dummy-id',
        data: {
          action: 'invited',
          actionSubject: 'user',
          attributes: {
            numberOfUsersInvited: 2,
            permissionType: Permissions.TRUSTED,
            originIdGenerated: 'some-origin-id',
            originProduct: 'admin',
            isTrustedUserEnabled: true,
          },
          source: 'userListScreen',
        },
      });
    });

    it('should send a track event and a ui event for basic user on success when feature flag is turned on and user unselects trusted user', async () => {
      const trustedUsersFeatureFlagOverride = {
        isLoading: false,
        value: true,
      };

      const permissionData = {
        loading: false,
        rolePermissions: {
          permissionIds: ['add-products', 'invite-users'],
        },
      };
      const mutateStub = sinon.stub().returns(true);

      const wrapper = createTestShallowComponent({
        data: permissionData,
        trustedUsersFeatureFlag:
          trustedUsersFeatureFlagOverride,
        onInvite: noop,
        showFlag: noop,
        mutate: mutateStub,
      });

      expect(analyticsClient.sendUIEvent.callCount).to.equal(0);
      expect(analyticsClient.sendTrackEvent.callCount).to.equal(0);
      expect(analyticsClient.sendUIEvent.callCount).to.equal(0);

      wrapper.instance().setState({ emails: ['a@b.com', 'c@d.com'] });

      const checkbox = wrapper.find(AkCheckbox).find({ id: 'role-permissions-checkbox' });
      expect(checkbox.exists()).to.equal(true);
      expect(checkbox.prop('defaultChecked')).to.equal(true);

      checkbox.simulate('change', {
        currentTarget: { checked: false },
      });

      wrapper
        .find(AkButton)
        .find({ id: 'user-quick-invite-submit' })
        .first()
        .simulate('click');

      await waitUntil(() => mutateStub.called);

      expect(analyticsClient.sendTrackEvent.callCount).to.equal(1);
      expect(analyticsClient.sendUIEvent.callCount).to.equal(1);
      expect(analyticsClient.sendUIEvent.lastCall.args[0]).to.deep.equal({
        cloudId: 'dummy-id',
        data: {
          action: 'clicked',
          actionSubject: 'button',
          actionSubjectId: 'inviteUsers',
          attributes: {
            numberOfUsersInvited: 2,
            permissionType: Permissions.BASIC,
            originProduct: 'admin',
            originIdGenerated: 'some-origin-id',
            isTrustedUserEnabled: true,
          },
          source: 'userListScreen',
        },
      });

      expect(analyticsClient.sendTrackEvent.lastCall.args[0]).to.deep.equal({
        cloudId: 'dummy-id',
        data: {
          action: 'invited',
          actionSubject: 'user',
          attributes: {
            numberOfUsersInvited: 2,
            permissionType: Permissions.BASIC,
            originProduct: 'admin',
            originIdGenerated: 'some-origin-id',
            isTrustedUserEnabled: true,
          },
          source: 'userListScreen',
        },
      });
    });

  });
});
