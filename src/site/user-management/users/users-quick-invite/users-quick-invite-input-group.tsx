import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import { FieldTextStateless as AkFieldTextStateless } from '@atlaskit/field-text';

import { gridSize as akGridSize } from '@atlaskit/theme';

interface OwnProps {
  disabled: boolean;
  emails: string[];
  onChange(a: string[]): void;
}
type UsersQuickInviteInputGroupProps = InjectedIntlProps & OwnProps;

const numberOfFields = 3;

const messages = defineMessages({
  emailAddress: {
    id: 'users.list.invite.email.address',
    description: 'Placeholder text for the user to invite\'s email address',
    defaultMessage: 'Email address',
  },
});

const InputWrapper = styled.div`
  padding-top: ${akGridSize() * 1}px;
`;

class UsersQuickInviteInputGroupImpl extends React.Component<UsersQuickInviteInputGroupProps> {

  public render() {
    const { formatMessage } = this.props.intl;

    const items: React.ReactNode[] = [];

    for (let i = 0; i < numberOfFields; i++) {
      items.push(
        <InputWrapper key={i}>
          <AkFieldTextStateless
            disabled={this.props.disabled}
            onChange={this.makeFieldChangeHandler(i)}
            isLabelHidden={true}
            value={this.props.emails[i] || ''}
            shouldFitContainer={true}
            placeholder={formatMessage(messages.emailAddress)}
          />
        </InputWrapper>,
      );
    }

    return items;

  }

  private makeFieldChangeHandler(fieldIndex: number) {
    return (event) => {
      const currentValues = [...this.props.emails];
      currentValues[fieldIndex] = event.target.value;
      this.props.onChange(currentValues);
    };
  }
}

export const UsersQuickInviteInputGroup = injectIntl<OwnProps>(UsersQuickInviteInputGroupImpl);
