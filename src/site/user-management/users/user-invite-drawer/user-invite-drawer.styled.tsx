import styled from 'styled-components';

import {
  colors as akColors,
  fontSize as akFontSize,
  gridSize as akGridSize,
} from '@atlaskit/theme';

export const EditDomainSettings = styled.div`
  margin: ${akGridSize() / 2}px 0 ${akGridSize()}px ${akGridSize() * 4}px;
  font-size: ${akFontSize() - 4}px;
  color: ${akColors.subtleText};
`;

export const Footer = styled.footer`
  display: flex;
  justify-content: space-between;
`;

export const FooterImage = styled.img`
  max-width: ${akGridSize() * 40}px;
  display: block;
  margin: ${akGridSize() * 4}px auto 0 auto;
`;

export const CheckboxWrapper = styled.div`
  display: flex;
  align-items: center;
  margin-top: ${akGridSize}px;
`;
