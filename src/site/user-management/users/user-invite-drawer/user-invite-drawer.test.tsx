import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import * as React from 'react';
import { IntlProvider } from 'react-intl';
import * as sinon from 'sinon';
import { UserInviteDrawerQuery } from 'src/schema/schema-types';

import AkButton from '@atlaskit/button';
import { Checkbox as AkCheckbox } from '@atlaskit/checkbox';
import AkFieldTextArea from '@atlaskit/field-text-area';
import Akform, { FormSection as AkFormSection, ValidatorMessage as AKValidatorMessage } from '@atlaskit/form';
import AkCrossIcon from '@atlaskit/icon/glyph/cross';
import AkSpinner from '@atlaskit/spinner';
import OriginTracing from '@atlassiansox/origin-tracing';

import { ScreenEventSender } from 'common/analytics';
import { AsyncInlineGroupSelect } from 'common/async-inline-group-select';
import { GenericErrorMessage } from 'common/error';
import { FlagProvider } from 'common/flag';

import {
  createMockAnalyticsClient,
  createMockContext,
  createMockIntlProp,
  waitUntil,
} from '../../../../utilities/testing';
import { Permissions } from '../user-permission-section/permissions';
import { UserPermissionItemContainer } from '../user-permission-section/user-permission-items';
import { SiteAccessSection } from './site-access-section/site-access-section';
import { MAX_NUMBER_INVITED_USERS, State as UserInviteState, UserInviteDrawerImpl } from './user-invite-drawer';
import { Footer } from './user-invite-drawer.styled';

describe('User invite drawer - Site access section', () => {
  const sandbox = sinon.sandbox.create();
  let analyticsClient = createMockAnalyticsClient();

  let onCloseSpy: sinon.SinonSpy;
  let showFlagSpy: sinon.SinonSpy;
  let hideFlagSpy: sinon.SinonSpy;
  let inviteUsersMutationSpy: sinon.SinonSpy;
  let updateSiteAccessMutationSpy: sinon.SinonSpy;

  beforeEach(() => {
    onCloseSpy = sandbox.spy();
    showFlagSpy = sandbox.spy();
    hideFlagSpy = sandbox.spy();
    inviteUsersMutationSpy = sinon.stub().returns(Promise.resolve({}));
    updateSiteAccessMutationSpy = sinon.stub().returns(Promise.resolve({}));
    analyticsClient = createMockAnalyticsClient();
  });

  afterEach(() => {
    sandbox.restore();
  });

  const currentSiteData: UserInviteDrawerQuery = {
    currentSite: {
      __typename: '',
      id: 'DUMMY_SITE_ID',
      defaultApps: [
        { productId: 'jira-servicedesk', __typename: '' },
        { productId: 'jira-software', __typename: '' },
        { productId: 'conf', __typename: '' },
      ],
      groupsUseAccessConfig: [
        {
          __typename: '',
          product: {
            __typename: '',
            productId: 'jira-software',
            productName: 'Jira Software',
          },
        },
      ],
      siteAccess: {
        __typename: '',
        domains: ['acme.com', 'acme.com.au', 'acme.co.uk'],
        signupEnabled: true,
      },
    },
  };

  const validEmailState: Partial<UserInviteState> = {
    emails: ['thomyorke@suspiria.com'],
    emailsInputValues: [{
      label: 'thomyorke@suspiria.com',
      value: 'thomyorke@suspiria.com',
    }],
  };

  const invalidEmailState: Partial<UserInviteState> = {
    emails: ['thomyorke@suspiria'],
    emailsInputValues: [{
      label: 'thomyorke@suspiria',
      value: 'thomyorke@suspiria',
    }],
  };

  const tooManyEmails: Partial<UserInviteState> = {
    emails: [...Array(26).keys()].map(index => `happyCorgi${index}@gmail.com`),
    emailsInputValues: [...Array(26).keys()].map(index => ({
      label: `happyCorgi${index}@gmail.com`,
      value: `happyCorgi${index}@gmail.com`,
    })),
  };

  interface GetWrapperProps {
    trustedUsersFeatureFlag?: any;
    data?: any;
    rolePermissions?: any;
  }

  const getWrapper = ({ trustedUsersFeatureFlag, data }: GetWrapperProps = {}) => {
    if (!trustedUsersFeatureFlag) {
      trustedUsersFeatureFlag = {
        value: false,
        isLoading: false,
      };
    }
    if (!data) {
      data = {
        loading: false,
        error: false,
        ...currentSiteData,
      };
    }

    return shallow(
      <UserInviteDrawerImpl
        cloudId="DUMMY-CLOUD-ID"
        trustedUsersFeatureFlag={trustedUsersFeatureFlag}
        intl={createMockIntlProp()}
        analyticsClient={analyticsClient}
        isOpen={true}
        onClose={onCloseSpy}
        showFlag={showFlagSpy}
        hideFlag={hideFlagSpy}
        inviteUsers={inviteUsersMutationSpy}
        updateSiteAccess={updateSiteAccessMutationSpy}
        data={data}
      />,
    );
  };

  const getMountedWrapper = ({ trustedUsersFeatureFlag, data, rolePermissions }: GetWrapperProps = {}) => {
    if (!trustedUsersFeatureFlag) {
      trustedUsersFeatureFlag = {
        value: false,
        isLoading: false,
      };
    }
    if (!data) {
      data = {
        loading: false,
        error: false,
        ...currentSiteData,
      };
    }

    if (!rolePermissions) {
      rolePermissions = {
        rolePermissions: {
          permissionIds: [],
        },
      };
    }

    const mounted = mount(
      <FlagProvider>
        <IntlProvider locale="en">
          <UserInviteDrawerImpl
            cloudId="DUMMY-CLOUD-ID"
            trustedUsersFeatureFlag={trustedUsersFeatureFlag}
            intl={createMockIntlProp()}
            analyticsClient={analyticsClient}
            isOpen={true}
            onClose={onCloseSpy}
            showFlag={showFlagSpy}
            hideFlag={hideFlagSpy}
            inviteUsers={inviteUsersMutationSpy}
            updateSiteAccess={updateSiteAccessMutationSpy}
            data={data}
          />
        </IntlProvider>
      </FlagProvider>,
      createMockContext({ intl: true, apollo: true }),
    );

    return mounted.find(UserInviteDrawerImpl).at(0);
  };

  const clickSubmit = wrapper => {
    wrapper
      .find(AkButton)
      .find({ id: 'user-invite-submit' })
      .first()
      .simulate('click');
  };

  it('should show a spinner while loading', () => {
    const wrapper = getWrapper({
      data: {
        loading: true,
        error: false,
      },
    });

    expect(wrapper.find(AkSpinner).exists()).to.equal(true);
  });

  it('should show an error if one exists', () => {
    const wrapper = getWrapper({
      data: {
        loading: false,
        error: true,
      },
    });

    expect(wrapper.find(GenericErrorMessage).exists()).to.equal(true);
  });

  it(`should render the drawer if no error and it's finished loading`, () => {
    const wrapper = getWrapper({
      data: {
        loading: false,
        error: false,
        ...currentSiteData,
      },
    });

    expect(wrapper.find(AkSpinner).exists()).to.equal(false);
    expect(wrapper.find(AkFormSection).exists()).to.equal(true);
  });

  describe('Site access', () => {
    it(`should show site access when the signup option isn't ANYONE`, () => {
      const anyoneWrapper = getWrapper({
        data: {
          loading: false,
          error: false,
          currentSite: {
            ...currentSiteData.currentSite,
            siteAccess: {
              signupEnabled: true,
              domains: [],
            },
          },
        },
      });

      expect(anyoneWrapper.find(SiteAccessSection).exists()).to.equal(false);

      const domainWrapper = getWrapper({
        data: {
          loading: false,
          error: false,
          currentSite: {
            ...currentSiteData.currentSite,
            siteAccess: {
              signupEnabled: true,
              domains: ['atlassian.com'],
            },
          },
        },
      });

      expect(domainWrapper.find(SiteAccessSection).exists()).to.equal(true);

      const nobodyWrapper = getWrapper({
        data: {
          loading: false,
          error: false,
          currentSite: {
            ...currentSiteData.currentSite,
            siteAccess: {
              signupEnabled: false,
              domains: [],
            },
          },
        },
      });

      expect(nobodyWrapper.find(SiteAccessSection).exists()).to.equal(true);
    });
  });

  describe('Form validation', () => {
    it('should not submit the form if the email input is empty', () => {
      const wrapper = getMountedWrapper();

      expect(inviteUsersMutationSpy.callCount).to.equal(0);
      expect(updateSiteAccessMutationSpy.callCount).to.equal(0);

      clickSubmit(wrapper);

      expect(inviteUsersMutationSpy.callCount).to.equal(0);
      expect(updateSiteAccessMutationSpy.callCount).to.equal(0);
    });

    it('should not submit the form if the number of emails entered is over an x amount', async () => {
      const wrapper = getMountedWrapper();

      wrapper.instance().setState(tooManyEmails);
      wrapper.instance().forceUpdate();

      expect(inviteUsersMutationSpy.callCount).to.equal(0);
      expect(updateSiteAccessMutationSpy.callCount).to.equal(0);

      clickSubmit(wrapper);
      const validator = wrapper.find(AKValidatorMessage).at(0);

      expect(validator.text()).to.contain(`A maximum of ${MAX_NUMBER_INVITED_USERS} users may be added at once`);
      expect(inviteUsersMutationSpy.callCount).to.equal(0);
    });

    it('should not submit the form if an email is improperly formatted', async () => {
      const wrapper = getMountedWrapper();

      wrapper.instance().setState(invalidEmailState);
      wrapper.instance().forceUpdate();

      expect(inviteUsersMutationSpy.callCount).to.equal(0);
      expect(updateSiteAccessMutationSpy.callCount).to.equal(0);

      clickSubmit(wrapper);
      const validator = wrapper.find(AKValidatorMessage).at(0);

      expect(validator.text()).to.contain('One or more email addresses are invalid.');
      expect(inviteUsersMutationSpy.callCount).to.equal(0);
    });
  });

  describe('Email invite', () => {
    it('should have the "send email" checkbox checked by default', () => {
      const wrapper = getWrapper();
      const checkbox = wrapper.find(AkCheckbox).find({ id: 'send-email-checkbox' });

      expect(checkbox.prop('isChecked')).to.equal(true);
      expect(checkbox.prop('defaultChecked')).to.equal(true);
    });

    it('should have the email textarea enabled when "send email" is checked', () => {
      const wrapper = getWrapper();
      const textarea = wrapper.find(AkFieldTextArea);
      const checkbox = wrapper.find(AkCheckbox).find({ id: 'send-email-checkbox' });

      expect(checkbox.prop('isChecked')).to.equal(true);
      expect(textarea.prop('disabled')).to.equal(false);
    });

    it('should have the email textarea disabled when "send email" is unchecked', () => {
      const wrapper = getWrapper();

      const checkbox = wrapper.find(AkCheckbox).find({ name: 'send-email' });

      expect(checkbox.prop('isChecked')).to.equal(true);

      checkbox.simulate('change', {
        target: { checked: false },
      });

      expect(wrapper.find(AkFieldTextArea).prop('disabled')).to.equal(true);
    });
  });

  describe('Saving', () => {
    it('should run invite users mutation on success', async () => {
      const wrapper = getMountedWrapper();

      wrapper.instance().setState(validEmailState);
      wrapper.instance().forceUpdate();

      expect(inviteUsersMutationSpy.callCount).to.equal(0);
      expect(updateSiteAccessMutationSpy.callCount).to.equal(0);

      clickSubmit(wrapper);

      await waitUntil(() => inviteUsersMutationSpy.called);

      expect(inviteUsersMutationSpy.callCount).to.equal(1);
      expect(updateSiteAccessMutationSpy.callCount).to.equal(0);
    });

    it('should not run updateSiteAccess if inviteUsers fails', async () => {
      const failedMutation = Promise.reject(new Error('Error'));

      inviteUsersMutationSpy = sandbox.stub().returns(failedMutation);

      const wrapper = getMountedWrapper();

      wrapper.instance().setState(validEmailState);
      wrapper.instance().forceUpdate();

      expect(inviteUsersMutationSpy.callCount).to.equal(0);
      expect(updateSiteAccessMutationSpy.callCount).to.equal(0);

      clickSubmit(wrapper);

      await waitUntil(() => inviteUsersMutationSpy.called);

      expect(inviteUsersMutationSpy.callCount).to.equal(1);
      expect(updateSiteAccessMutationSpy.callCount).to.equal(0);
    });

    it('should display a success flag if both mutations are good', async () => {
      const wrapper = getMountedWrapper();

      wrapper.instance().setState(validEmailState);
      wrapper.instance().forceUpdate();

      expect(showFlagSpy.callCount).to.equal(0);

      clickSubmit(wrapper);

      await waitUntil(() => inviteUsersMutationSpy.called);

      expect(showFlagSpy.getCall(0).calledWithMatch({
        title: 'You\'ve invited 1 user',
      })).to.equal(true);
    });
  });

  describe('User Permissions', () => {
    it('should hide UserPermissions by default', () => {
      const wrapper = getWrapper();
      const userPermissionDropDown = wrapper.find(UserPermissionItemContainer);
      const userGroupSelect = wrapper.find(AsyncInlineGroupSelect);

      expect(userPermissionDropDown.exists()).to.equal(false);
      expect(userGroupSelect.exists()).to.equal(true);
    });

    it('should show UserPermissions when flag is switched on', () => {
      const wrapper = getWrapper({
        trustedUsersFeatureFlag: { value: true, isLoading: false },
      });
      const userPermissionDropDown = wrapper.find(UserPermissionItemContainer);

      expect(userPermissionDropDown.exists()).to.equal(true);
    });
  });

  describe('User Invite Drawer getDerivedStateFromProps', () => {
    const defaultGivenState = {
      emails: [],
      groups: [],
      emailsInputValue: '',
      emailsInputValues: [],
      siteAccessDomains: [],
      numberOfUncheckedDomains: 0,
      productAccessProducts: [],
      isSendEmailChecked: true,
      emailText: '',
      userPermission: Permissions.BASIC,
      isMutationLoading: false,
      trustedUsersFeatureFlag: {
        isLoading: false,
        value: false,
      },
    };

    it('should return null when prop not set', () => {
      const givenProps = {};
      const givenState = { ...defaultGivenState };
      const result = UserInviteDrawerImpl.getDerivedStateFromProps(givenProps, givenState);

      expect(result).to.equal(null);
    });

    it('should default to Trusted User when feature flag set', () => {
      const givenProps = {
        trustedUsersFeatureFlag: {
          isLoading: false,
          value: true,
        },
      };
      const givenState = { ...defaultGivenState };
      const result = UserInviteDrawerImpl.getDerivedStateFromProps(givenProps, givenState);

      expect(result).to.deep.equal({
        trustedUsersFeatureFlag: {
          isLoading: false,
          value: true,
        },
        userPermission: Permissions.TRUSTED,
      });
    });

    it('should return null when feature flag is not set and matches default state', () => {
      const givenProps = {
        trustedUsersFeatureFlag: {
          isLoading: false,
          value: false,
        },
      };
      const givenState = { ...defaultGivenState };
      const result = UserInviteDrawerImpl.getDerivedStateFromProps(givenProps, givenState);

      expect(result).to.equal(null);
    });
  });

  describe('analytics', () => {
    const originToAnalyticsAttributes = OriginTracing.prototype.toAnalyticsAttributes;
    beforeEach(() => {
      OriginTracing.prototype.toAnalyticsAttributes = sinon.stub().returns({
        originProduct: 'admin',
        originIdGenerated: 'some-origin-id',
      });
    });

    afterEach(() => {
      OriginTracing.prototype.toAnalyticsAttributes = originToAnalyticsAttributes;
    });

    it(`should send a screen event on render`, () => {
      const screenEventSender = getWrapper().find(Akform).dive().find(ScreenEventSender);

      expect(screenEventSender).to.be.lengthOf(1);
      expect(screenEventSender.props().event).to.deep.equal({
        data: {
          name: 'userInviteDrawer',
        },
      });
    });

    it('should send a track event and a ui event on success', async () => {
      const wrapper = getMountedWrapper();

      expect(analyticsClient.sendUIEvent.callCount).to.equal(0);
      expect(analyticsClient.sendTrackEvent.callCount).to.equal(0);
      expect(analyticsClient.sendUIEvent.callCount).to.equal(0);

      wrapper.instance().setState(validEmailState);
      wrapper.instance().forceUpdate();

      clickSubmit(wrapper);

      await waitUntil(() => inviteUsersMutationSpy.called);

      expect(analyticsClient.sendTrackEvent.callCount).to.equal(1);
      expect(analyticsClient.sendUIEvent.callCount).to.equal(1);
      expect(analyticsClient.sendUIEvent.lastCall.args[0]).to.deep.equal({
        cloudId: 'DUMMY-CLOUD-ID',
        data: {
          action: 'clicked',
          actionSubject: 'button',
          actionSubjectId: 'inviteUsers',
          attributes: {
            numberOfDomainsAddedToDRS: 0,
            numberOfDomainsNotAddedToDRS: 1,
            numberOfGroupsAdded: 0,
            isInviteSent: true,
            isPersonalizedEmailSent: false,
            numberOfUsersInvited: 1,
            originIdGenerated: 'some-origin-id',
            originProduct: 'admin',
            productsGrantedAccess: [
              {
                'jira-software': true,
              },
            ],
            permissionType: Permissions.BASIC,
            isTrustedUserEnabled: false,
          },
          source: 'userInviteDrawer',
        },
      });

      expect(analyticsClient.sendTrackEvent.lastCall.args[0]).to.deep.equal({
        cloudId: 'DUMMY-CLOUD-ID',
        data: {
          action: 'invited',
          actionSubject: 'user',
          attributes: {
            numberOfDomainsAddedToDRS: 0,
            numberOfDomainsNotAddedToDRS: 1,
            numberOfGroupsAdded: 0,
            isInviteSent: true,
            isPersonalizedEmailSent: false,
            numberOfUsersInvited: 1,
            originIdGenerated: 'some-origin-id',
            originProduct: 'admin',
            productsGrantedAccess: [
              {
                'jira-software': true,
              },
            ],
            permissionType: Permissions.BASIC,
            isTrustedUserEnabled: false,
          },
          source: 'userInviteDrawer',
        },
      });
    });

    it('should send a track event and a ui event on success for trusted user', async () => {
      const wrapper = getMountedWrapper({
        rolePermissions: {
          permissionIds: ['add-products', 'invite-users'],
        },
        trustedUsersFeatureFlag: {
          value: true,
          isLoading: false,
        },
      });

      expect(analyticsClient.sendUIEvent.callCount).to.equal(0);
      expect(analyticsClient.sendTrackEvent.callCount).to.equal(0);
      expect(analyticsClient.sendUIEvent.callCount).to.equal(0);

      wrapper.instance().setState(validEmailState);
      wrapper.instance().forceUpdate();

      clickSubmit(wrapper);

      await waitUntil(() => inviteUsersMutationSpy.called);

      expect(analyticsClient.sendTrackEvent.callCount).to.equal(1);
      expect(analyticsClient.sendUIEvent.callCount).to.equal(1);
      expect(analyticsClient.sendUIEvent.lastCall.args[0]).to.deep.equal({
        cloudId: 'DUMMY-CLOUD-ID',
        data: {
          action: 'clicked',
          actionSubject: 'button',
          actionSubjectId: 'inviteUsers',
          attributes: {
            numberOfDomainsAddedToDRS: 0,
            numberOfDomainsNotAddedToDRS: 1,
            numberOfGroupsAdded: 0,
            isInviteSent: true,
            isPersonalizedEmailSent: false,
            numberOfUsersInvited: 1,
            originIdGenerated: 'some-origin-id',
            originProduct: 'admin',
            productsGrantedAccess: [
              {
                'jira-software': true,
              },
            ],
            permissionType: Permissions.TRUSTED,
            isTrustedUserEnabled: true,
          },
          source: 'userInviteDrawer',
        },
      });

      expect(analyticsClient.sendTrackEvent.lastCall.args[0]).to.deep.equal({
        cloudId: 'DUMMY-CLOUD-ID',
        data: {
          action: 'invited',
          actionSubject: 'user',
          attributes: {
            numberOfDomainsAddedToDRS: 0,
            numberOfDomainsNotAddedToDRS: 1,
            numberOfGroupsAdded: 0,
            isInviteSent: true,
            isPersonalizedEmailSent: false,
            numberOfUsersInvited: 1,
            originIdGenerated: 'some-origin-id',
            originProduct: 'admin',
            productsGrantedAccess: [
              {
                'jira-software': true,
              },
            ],
            permissionType: Permissions.TRUSTED,
            isTrustedUserEnabled: true,
          },
          source: 'userInviteDrawer',
        },
      });
    });

    it('should send a ui event on cancel', async () => {
      const wrapper = getMountedWrapper();
      const cancelButton = wrapper.find(Footer).find(AkButton).at(0);

      expect(analyticsClient.sendUIEvent.callCount).to.equal(0);
      expect(analyticsClient.sendTrackEvent.callCount).to.equal(0);
      expect(analyticsClient.sendUIEvent.callCount).to.equal(0);

      wrapper.instance().setState(validEmailState);
      wrapper.instance().forceUpdate();

      cancelButton.simulate('click');

      expect(analyticsClient.sendUIEvent.callCount).to.equal(1);
      expect(analyticsClient.sendUIEvent.lastCall.args[0]).to.deep.equal({
        cloudId: 'DUMMY-CLOUD-ID',
        data: {
          action: 'clicked',
          actionSubject: 'button',
          actionSubjectId: 'cancelInviteUsers',
          attributes: {
            numberOfDomainsAddedToDRS: 0,
            numberOfDomainsNotAddedToDRS: 1,
            numberOfGroupsAdded: 0,
            isInviteSent: true,
            isPersonalizedEmailSent: false,
            numberOfUsersInvited: 1,
            productsGrantedAccess: [
              {
                'jira-software': true,
              },
            ],
            permissionType: Permissions.BASIC,
            isTrustedUserEnabled: false,
          },
          source: 'userInviteDrawer',
        },
      });
    });

    it('should send a ui event on close', async () => {
      const wrapper = getMountedWrapper();
      const closeButton = wrapper.find(AkCrossIcon);

      expect(analyticsClient.sendUIEvent.callCount).to.equal(0);
      expect(analyticsClient.sendTrackEvent.callCount).to.equal(0);
      expect(analyticsClient.sendUIEvent.callCount).to.equal(0);

      wrapper.instance().setState(validEmailState);
      wrapper.instance().forceUpdate();

      closeButton.simulate('click');

      expect(analyticsClient.sendUIEvent.callCount).to.equal(1);
      expect(analyticsClient.sendUIEvent.lastCall.args[0]).to.deep.equal({
        cloudId: 'DUMMY-CLOUD-ID',
        data: {
          action: 'clicked',
          actionSubject: 'button',
          actionSubjectId: 'closeInviteUsers',
          attributes: {
            numberOfDomainsAddedToDRS: 0,
            numberOfDomainsNotAddedToDRS: 1,
            numberOfGroupsAdded: 0,
            isInviteSent: true,
            isPersonalizedEmailSent: false,
            numberOfUsersInvited: 1,
            productsGrantedAccess: [
              {
                'jira-software': true,
              },
            ],
            permissionType: Permissions.BASIC,
            isTrustedUserEnabled: false,
          },
          source: 'userInviteDrawer',
        },
      });
    });

    it('should send ui event but not send track event if inviteUsers fails', async () => {
      const failedMutation = Promise.reject(new Error('Error'));

      inviteUsersMutationSpy = sandbox.stub().returns(failedMutation);

      const wrapper = getMountedWrapper();

      wrapper.instance().setState(validEmailState);
      wrapper.instance().forceUpdate();

      expect(inviteUsersMutationSpy.callCount).to.equal(0);
      expect(updateSiteAccessMutationSpy.callCount).to.equal(0);

      clickSubmit(wrapper);

      await waitUntil(() => inviteUsersMutationSpy.called);

      expect(inviteUsersMutationSpy.callCount).to.equal(1);
      expect(analyticsClient.sendTrackEvent.callCount).to.equal(0);
      expect(analyticsClient.sendUIEvent.callCount).to.equal(1);
    });
  });
});
