import * as deepEqual from 'deep-equal';
import * as React from 'react';
import { ChildProps, graphql, MutationFunc } from 'react-apollo';
import {
  defineMessages,
  InjectedIntlProps,
  injectIntl,
} from 'react-intl';

import AkButton from '@atlaskit/button';
import {
  Checkbox as AkCheckbox,
} from '@atlaskit/checkbox';
import AkFieldTextArea from '@atlaskit/field-text-area';
import AkForm, {
  Field as AkField,
  FormFooter as AkFormFooter,
  FormHeader as AkFormHeader,
  FormSection as AkFormSection,
  Validator as AkValidator,
} from '@atlaskit/form';
import {
  Grid as AkGrid,
  GridColumn as AkGridColumn,
} from '@atlaskit/page';
import AkSelect from '@atlaskit/select';
import AkSpinner from '@atlaskit/spinner';
import OriginTracing from '@atlassiansox/origin-tracing';

import {
  AnalyticsClientProps,
  ScreenEventSender,
  UserInviteAttributes,
  userInviteCancelledUIEventData,
  userInviteChangedUserPermissionDrawerTrackEventData,
  userInviteClickedUserPermissionUIEventData,
  userInviteClosedUIEventData,
  userInviteDrawerScreenEventData,
  userInviteSentTrackEventData,
  userInviteSentUIEventData,
  withAnalyticsClient,
} from 'common/analytics';
import { AsyncInlineGroupSelect } from 'common/async-inline-group-select';
import { ContentReveal } from 'common/content-reveal';
import { createErrorIcon, createSuccessIcon, errorFlagMessages, GenericErrorMessage } from 'common/error';
import { FeatureFlagProps } from 'common/feature-flags';
import { FlagProps, withFlag } from 'common/flag';
import { FocusedTask, FocusedTaskProps } from 'common/focused-task';
import { teamOnboardingImage } from 'common/images';
import { InfoHover } from 'common/info-hover';
import { getStart } from 'common/pagination';
import { ProductAccess, ProductsChanged } from 'common/product-access';

import {
  SiteInviteUsersInput,
  UserInviteDrawerInviteMutation,
  UserInviteDrawerInviteMutationVariables,
  UserInviteDrawerQuery,
  UserInviteDrawerUpdateSiteAccessMutation,
  UserInviteDrawerUpdateSiteAccessMutationVariables,
} from '../../../../schema/schema-types';
import { TrustedUsersFeatureFlag, withTrustedUsersFeatureFlag } from '../../../feature-flags/with-trusted-users-feature-flag';
import { getSignupSettings, SignUpOptions } from '../../site-access/signup-settings';
import { ROWS_PER_PAGE } from '../user-list/users-list-constants';
import siteUsersListQuery from '../user-list/users-page.query.graphql';
import { Permissions } from '../user-permission-section/permissions';
import { UserPermissionItemContainer } from '../user-permission-section/user-permission-items';
import { SiteAccessSection } from './site-access-section/site-access-section';
import inviteUsersMutation from './user-invite-drawer-invite.mutation.graphql';
import updateSiteAccessMutation from './user-invite-drawer-update-access.mutation.graphql';
import query from './user-invite-drawer.query.graphql';
import {
  CheckboxWrapper,
  Footer,
  FooterImage,
} from './user-invite-drawer.styled';
const messages = defineMessages({
  title: {
    id: 'user.invite.drawer.title',
    defaultMessage: 'Invite users',
    description: 'Title text at the top of the page.',
  },
  description: {
    id: 'user.invite.drawer.description',
    defaultMessage: `We'll invite your new users to the site and onboard them. During signup they'll enter their full name.`,
    description: 'Description text at the top of the page.',
  },
  emailAddressesLabel: {
    id: 'user.invite.drawer.email.addresses.label',
    defaultMessage: 'Email address(es)',
    description: 'Email addresses label for the multi input.',
  },
  emailAddressesValid: {
    id: 'user.invite.drawer.email.addresses.valid',
    defaultMessage: 'Valid',
    description: `Message shown when the email is valid.`,
  },
  emailAddressesInvalid: {
    id: 'user.invite.drawer.email.addresses.invalid',
    defaultMessage: 'Enter at least one email address.',
    description: `Error shown when the user hasn't entered any email addresses.`,
  },
  emailAddressOverLimit: {
    id: 'user.invite.drawer.email.addresses.over.limit',
    defaultMessage: 'A maximum of {maxUsers} users may be added at once',
    description: `Error shown when the user has entered over 25 emails.`,
  },
  emailAddressInvalidFormat: {
    id: 'user.invite.drawer.email.address.invalid.format',
    defaultMessage: 'One or more email addresses are invalid.',
    description: 'Error shown when the user has entered an incorrect email address.',
  },
  applicationAccessLabel: {
    id: 'user.invite.drawer.application.access.label',
    defaultMessage: 'Application access',
    description: 'Application access label above the checkboxes.',
  },
  groupMembershipLabel: {
    id: 'user.invite.drawer.group.membership.label',
    defaultMessage: 'Group membership',
    description: 'Group membership label for the select dropdown to select groups.',
  },
  userPermissionLabel: {
    id: 'user.invite.drawer.user.permission.label',
    defaultMessage: 'Permission',
    description: 'User permission label for the select dropdown to select user permissions.',
  },
  groupMembershipTip: {
    id: 'user.invite.drawer.group.membership.tip',
    defaultMessage: `Add new users to a group to precisely manage their access & roles (e.g. give them access to a team's resources or make them a product admin).`,
    description: 'Group membership label for the select dropdown to select groups.',
  },
  emailPersonalizeLink: {
    id: 'user.invite.drawer.email.personalize.link',
    defaultMessage: 'Personalize email invite',
    description: 'The text that allows the email invite field to expand.',
  },
  emailInviteLabel: {
    id: 'user.invite.drawer.email.invite.label',
    defaultMessage: 'Email invite',
    description: 'Text above the custom email message field.',
  },
  emailInvitePlaceholder: {
    id: 'user.invite.drawer.email.invite.placeholder',
    defaultMessage: 'Hi, \n\nWelcome to...',
    description: 'Placeholder text inside the custom email body.',
  },
  emailCheckboxLabel: {
    id: 'user.invite.drawer.email.checkbox.label',
    defaultMessage: 'Send email',
    description: 'Should the invite send email checkbox.',
  },
  cancelButton: {
    id: 'user.invite.drawer.cancel.button',
    defaultMessage: 'Cancel',
    description: 'The cancel button at the end of the form.',
  },
  inviteButtonDefault: {
    id: 'user.invite.drawer.invite.button.default',
    defaultMessage: 'Invite user',
    description: 'Invite button at the bottom of the form for one user.',
  },
  inviteButtonMultiple: {
    id: 'user.invite.drawer.invite.button.multiple',
    defaultMessage: 'Invite {number} {number, plural, one {user} other {users}}',
    description: 'Invite button at the bottom of the form for one user.',
  },
  sendEmailTooltip: {
    id: 'user.invite.drawer.send.email.tooltip',
    defaultMessage: `If you don't send the email those users will still be added, and you'll be able to give them product access. You can send the invitation at any time from the user's profile.`,
    description: 'Tooltip when hovering over the send email checkbox',
  },
  emailAddressTooltip: {
    id: 'user.invite.drawer.email.address.tooltip',
    defaultMessage: `We can't send invitations to distribution lists.`,
    description: 'Tooltip for the email addresses.',
  },
  successFlagTitle: {
    id: 'user.invite.drawer.email.success.flag.title',
    defaultMessage: `You've invited {number} {number, plural, one {user} other {users}}`,
    description: 'Title of the success flag after creation.',
  },
  successFlagDescription: {
    id: 'user.invite.drawer.email.success.flag.description',
    defaultMessage: `We'll take care of the rest.`,
    description: 'Description of the success flag after creation.',
  },
  inviteErrorFlagDescription: {
    id: 'user.invite.drawer.email.invite.error.flag.description',
    defaultMessage: `We couldn't send your {number, plural, one {invitation} other {invitations}}. Try inviting {number, plural, one {that user} other {those users}} again.`,
    description: 'Error that displays in the flag when the invite failed to submit.',
  },
});

// To be raised back to 25. Reasoning for 5 is here: https://sdog.jira-dev.com/browse/KT-1797
export const MAX_NUMBER_INVITED_USERS = 5;

const PageLayout: React.SFC = ({ children }) => (
  <AkGrid>
    <AkGridColumn medium={2} />
    <AkGridColumn medium={8}>
      {children}
    </AkGridColumn>
    <AkGridColumn medium={2} />
  </AkGrid>
);

interface MutationProps {
  inviteUsers?: MutationFunc<UserInviteDrawerInviteMutation, UserInviteDrawerInviteMutationVariables>;
  updateSiteAccess?: MutationFunc<UserInviteDrawerUpdateSiteAccessMutation, UserInviteDrawerUpdateSiteAccessMutationVariables>;
}

interface EmailInputValue {
  label: string;
  value: string;
}

interface OwnProps {
  cloudId?: string;
  trustedUsersFeatureFlag?: FeatureFlagProps;
}

export interface State {
  emails: string[];
  groups: string[];
  emailsInputValue: string;
  emailsInputValues: EmailInputValue[];
  siteAccessDomains: string[];
  numberOfUncheckedDomains: number;
  productAccessProducts: ProductsChanged[];
  isSendEmailChecked: boolean;
  emailText: string;
  userPermission: Permissions;
  isMutationLoading: boolean;
  trustedUsersFeatureFlag?: FeatureFlagProps;
}

type Props = OwnProps & InjectedIntlProps & FocusedTaskProps & FlagProps & AnalyticsClientProps;

export class UserInviteDrawerImpl extends React.Component<
  ChildProps<Props & TrustedUsersFeatureFlag & MutationProps, UserInviteDrawerInviteMutation & UserInviteDrawerUpdateSiteAccessMutation & UserInviteDrawerQuery>,
  State
  > {
  private formRef;
  private emailCreationKeys = ['Tab', 'Enter'];

  constructor(props) {
    super(props);
    this.state = this.getInitialState();
  }

  public getInitialState = (): Readonly<State> => ({
    emails: [],
    groups: [],
    emailsInputValue: '',
    emailsInputValues: [],
    siteAccessDomains: [],
    numberOfUncheckedDomains: 0,
    productAccessProducts: [],
    isSendEmailChecked: true,
    emailText: '',
    userPermission: this.props.trustedUsersFeatureFlag && this.props.trustedUsersFeatureFlag.value ? Permissions.TRUSTED : Permissions.BASIC,
    isMutationLoading: false,
    trustedUsersFeatureFlag: {
      isLoading: false,
      value: false,
    },
  });

  public static getDerivedStateFromProps(props: OwnProps, state: State) {

    if (props.trustedUsersFeatureFlag
      && state.trustedUsersFeatureFlag
      && !deepEqual(props.trustedUsersFeatureFlag, state.trustedUsersFeatureFlag)) {
      return {
        trustedUsersFeatureFlag: props.trustedUsersFeatureFlag,
        userPermission: props.trustedUsersFeatureFlag.value ? Permissions.TRUSTED : Permissions.BASIC,
      };
    }

    return null;
  }

  public render() {
    const { intl: { formatMessage } } = this.props;
    let body: React.ReactNode;

    if (!this.props.data || this.props.data.loading) {
      body = <AkSpinner />;
    } else if (this.props.data && this.props.data.error) {
      body = <GenericErrorMessage />;
    } else {
      body = this.renderBody();
    }

    return (
      <FocusedTask isOpen={this.props.isOpen} onClose={this.onBackButtonClicked}>
        <PageLayout>
          {/* A little bit of a hack: AkForm doesn't support onBlur for the email input, so we invent one ourselves */}
          <div onBlurCapture={this.onEmailsInputBlur} style={{ display: 'flex' }}>
            <AkForm
              onSubmit={this.submitForm}
              ref={this.setFormRef}
            >
              <AkFormHeader
                title={formatMessage(messages.title)}
                description={formatMessage(messages.description)}
              />
              {body}
              {this.renderFooter()}
            </AkForm>
          </div>
        </PageLayout>
      </FocusedTask>
    );
  }

  private renderUserPermission = () => {
    const {
      trustedUsersFeatureFlag,
      intl: { formatMessage },
      cloudId,
    } = this.props;

    if (trustedUsersFeatureFlag && trustedUsersFeatureFlag.value) {
      return (
        <AkField label={formatMessage(messages.userPermissionLabel)}>
          <UserPermissionItemContainer
            cloudId={cloudId}
            onChange={this.onUserPermissionChange}
            trustedUsersFeatureFlag={trustedUsersFeatureFlag}
            permission={this.state.userPermission}
          />
        </AkField>
      );
    }

    return <div />;
  };

  private renderGroupMembership = () => {
    const { intl: { formatMessage } } = this.props;

    return (
      <AkField
        label={formatMessage(messages.groupMembershipLabel)}
        helperText={formatMessage(messages.groupMembershipTip)}
      >
        <AsyncInlineGroupSelect
          cloudId={this.props.cloudId}
          onChange={this.onGroupMembershipChange}
        />
      </AkField>
    );
  };

  private renderBody = () => {
    const { intl: { formatMessage } } = this.props;

    return (
      <AkFormSection>
        <AkField
          name="email-input"
          label={formatMessage(messages.emailAddressesLabel)}
          helperText={formatMessage(messages.emailAddressTooltip)}
          isRequired={true}
          validateOnChange={true}
          validators={[
            <AkValidator
              key="limitValidation"
              func={this.isEmailLimitValid}
              invalid={formatMessage(messages.emailAddressOverLimit, { maxUsers: MAX_NUMBER_INVITED_USERS })}
              valid={formatMessage(messages.emailAddressesValid)}
            />,
            <AkValidator
              key="emailRegexValidation"
              func={this.areEmailAddressesValid}
              invalid={formatMessage(messages.emailAddressInvalidFormat)}
              valid={''}
            />,
          ]}
        >
          <AkSelect
            id="email-input-select"
            components={{ DropdownIndicator: null }}
            inputValue={this.state.emailsInputValue}
            isClearable={true}
            isMulti={true}
            menuIsOpen={false}
            value={this.state.emailsInputValues}
            onInputChange={this.onEmailsInputChange}
            onKeyDown={this.onEmailsKeyDown}
            onChange={this.onEmailsChange}
            placeholder={``}
            isRequired={true}
            autoFocus={true}
          />
        </AkField>
        {this.renderSiteAccess()}
        {this.renderUserPermission()}
        <AkField
          name="application-access"
          label={this.props.intl.formatMessage(messages.applicationAccessLabel)}
        >
          {this.renderProductAccess()}
        </AkField>
        {this.renderGroupMembership()}
        <AkField name="personalize-email">
          <ContentReveal label={formatMessage(messages.emailPersonalizeLink)}>
            <AkFieldTextArea
              label={formatMessage(messages.emailInviteLabel)}
              placeholder={formatMessage(messages.emailInvitePlaceholder)}
              shouldFitContainer={true}
              autoFocus={false}
              minimumRows={3}
              enableResize="vertical"
              onChange={this.onEmailTextChange}
              value={this.state.emailText}
              disabled={!this.state.isSendEmailChecked}
            />
            <CheckboxWrapper>
              <AkCheckbox
                id="send-email-checkbox"
                name="send-email"
                label={formatMessage(messages.emailCheckboxLabel)}
                defaultChecked={this.state.isSendEmailChecked}
                isChecked={this.state.isSendEmailChecked}
                onChange={this.onSendEmailChange}
              />
              <InfoHover
                dialogContent={<div>{formatMessage(messages.sendEmailTooltip)}</div>}
              />
            </CheckboxWrapper>
          </ContentReveal>
        </AkField>
      </AkFormSection>
    );
  };

  private renderSiteAccess = (): React.ReactNode => {
    if (!this.props.data || !this.props.data.currentSite) {
      return undefined;
    }

    const signupSettings = getSignupSettings(
      this.props.data.currentSite.siteAccess.signupEnabled,
      this.props.data.currentSite.siteAccess.domains,
    );

    if (signupSettings === SignUpOptions.ANYONE) {
      return <div />;
    }

    return (
      <AkField>
        <SiteAccessSection
          emails={this.state.emails}
          onChange={this.onSiteAccessChange}
        />
      </AkField>
    );
  }

  private renderProductAccess = (): React.ReactNode => {
    if (!this.props.data || !this.props.data.currentSite) {
      return <div />;
    }

    const { trustedUsersFeatureFlag } = this.props;
    const basicUserPermissionSelected = this.state.userPermission === Permissions.BASIC;
    let productAccessIsReadonly = false;
    if (trustedUsersFeatureFlag && trustedUsersFeatureFlag.value && !basicUserPermissionSelected) {
      productAccessIsReadonly = true;
    }

    const key = basicUserPermissionSelected ? 'basic-user-product-access' : 'trusted-user-product-access';
    let products;

    if (basicUserPermissionSelected) {
      products = this.props.data.currentSite.groupsUseAccessConfig
        .map(({ product }) => ({
          ...product,
          isInitiallyChecked: this.props.data!.currentSite!.defaultApps
            .map(app => app.productId)
            .includes(product.productId),
        }));
    } else {
      products = this.props.data.currentSite.groupsUseAccessConfig
        .map(({ product }) => ({
          ...product,
          isInitiallyChecked: true,
        }));
    }

    return (
      <ProductAccess
        key={key}
        products={products}
        isReadOnly={productAccessIsReadonly}
        tooltipStyle="hover"
        onCheckboxChange={this.onProductAccessChange}
        onAfterMount={this.onProductAccessChange}
      />
    );
  };

  private renderFooter = (): React.ReactNode => {
    const { formatMessage } = this.props.intl;

    if (this.props.data && (this.props.data.error || this.props.data.loading)) {
      return <div />;
    }

    return (
      <AkFormFooter>

        <ScreenEventSender
          isDataLoading={!this.props.data || this.props.data.loading}
          event={{ data: userInviteDrawerScreenEventData() }}
        >
          <Footer>
            <AkButton
              appearance="subtle"
              onClick={this.onCancelClicked}
            >
              {formatMessage(messages.cancelButton)}
            </AkButton>
            <AkButton
              id="user-invite-submit"
              appearance="primary"
              type="submit"
              onClick={this.submitForm}
              isLoading={this.state.isMutationLoading}
              isDisabled={!this.state.emails.length}
            >
              {this.state.emails.length <= 0 ? (
                formatMessage(messages.inviteButtonDefault)
              ) : (
                  formatMessage(messages.inviteButtonMultiple, {
                    number: this.state.emails.length,
                  })
                )}
            </AkButton>
          </Footer>
        </ScreenEventSender>
        <FooterImage src={teamOnboardingImage} />
      </AkFormFooter>
    );
  };

  private resetState = (): void => {
    this.setState(this.getInitialState());
  };

  private setFormRef = (ref) => {
    this.formRef = ref;
  };

  private onSiteAccessChange = (domainsChecked: Array<{ name: string, isChecked: boolean }>) => {
    const siteAccessDomains = domainsChecked.filter(domain => domain.isChecked).map(domain => domain.name);
    this.setState({
      siteAccessDomains,
      numberOfUncheckedDomains: domainsChecked.length - siteAccessDomains.length,
    });
  };

  private onProductAccessChange = (products: ProductsChanged[]) => {
    this.setState({ productAccessProducts: products });
  };

  private onEmailTextChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ emailText: event.target.value });
  };

  private onSendEmailChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ isSendEmailChecked: !!event.target.checked });
  };

  private onUserPermissionChange = (userPermission: Permissions) => {
    const previousPermission = this.state.userPermission;
    this.setState({ userPermission });
    this.props.analyticsClient.sendUIEvent({
      cloudId: this.props.cloudId,
      data: userInviteClickedUserPermissionUIEventData(),
    });
    this.props.analyticsClient.sendTrackEvent({
      cloudId: this.props.cloudId,
      data: userInviteChangedUserPermissionDrawerTrackEventData({
        previousPermissionType: previousPermission,
        newPermissionType: userPermission,
      }),
    });
  };

  private getAnalyticsAttributes = (): UserInviteAttributes => (
    {
      numberOfUsersInvited: this.state.emails.length,
      numberOfDomainsAddedToDRS: this.state.siteAccessDomains.length,
      numberOfDomainsNotAddedToDRS: this.state.numberOfUncheckedDomains,
      productsGrantedAccess: this.state.productAccessProducts.map(product => ({
        [product.productId]: product.isChecked,
      })),
      numberOfGroupsAdded: this.state.groups.length,
      isPersonalizedEmailSent: this.state.emailText !== '',
      isInviteSent: this.state.isSendEmailChecked,
      permissionType: this.state.userPermission,
      isTrustedUserEnabled: this.props.trustedUsersFeatureFlag && this.props.trustedUsersFeatureFlag.value,
    }
  )

  private onCancelClicked = () => {
    this.props.analyticsClient.sendUIEvent({
      cloudId: this.props.cloudId,
      data: userInviteCancelledUIEventData({
        ...this.getAnalyticsAttributes(),
      }),
    });
    this.close();
  }

  private submitForm = async (event?: React.FormEvent<HTMLFormElement>) => {
    const {
      inviteUsers,
      updateSiteAccess,
    } = this.props;

    if (event) {
      event.preventDefault();
    }

    const validationResult = this.formRef.validate();
    if (validationResult.isInvalid) {
      return;
    }

    if (!inviteUsers || !updateSiteAccess || !this.props.cloudId) {
      return;
    }

    let inviteMutationError = false;

    this.setState({ isMutationLoading: true });

    try {
      const origin = new OriginTracing({ product: 'admin' });
      const productIds = this.state.productAccessProducts
        .filter(product => product.isChecked)
        .map(product => product.productId);

      this.props.analyticsClient.sendUIEvent({
        cloudId: this.props.cloudId,
        data: userInviteSentUIEventData({
          ...this.getAnalyticsAttributes(),
          ...origin.toAnalyticsAttributes({ hasGeneratedId: true }),
        }),
      });

      const input: SiteInviteUsersInput = {
        additionalGroups: this.state.groups,
        emails: this.state.emails,
        productIds,
        notificationText: this.state.emailText,
        sendNotification: this.state.isSendEmailChecked,
        role: this.state.userPermission,
      };

      const finalInput: SiteInviteUsersInput = input.sendNotification
        ? { ...input, atlOrigin: origin.encode() }
        : input;

      await inviteUsers({
        variables: {
          id: this.props.cloudId,
          input: finalInput,
        },
      });
      this.props.analyticsClient.sendTrackEvent({
        cloudId: this.props.cloudId,
        data: userInviteSentTrackEventData({
          ...this.getAnalyticsAttributes(),
          ...origin.toAnalyticsAttributes({ hasGeneratedId: true }),
        }),
      });
    } catch (_) {
      inviteMutationError = true;
    }

    if (!inviteMutationError && this.state.siteAccessDomains.length > 0) {
      try {
        const currentDomains: string[] = (
          this.props.data
          && this.props.data.currentSite
          && this.props.data.currentSite.siteAccess.domains
        ) || [];

        await updateSiteAccess({
          variables: {
            id: this.props.cloudId,
            input: {
              notifyAdmin: false,
              openInvite: false,
              signupEnabled: true,
              domains: [
                ...currentDomains,
                ...this.state.siteAccessDomains,
              ],
            },
          },
        });
      } catch (_) {
        // Resolver shows flag so we don't have to do anything
      }
    }

    this.setState({ isMutationLoading: false });

    if (inviteMutationError) {
      this.showInviteErrorFlag();

      return;
    }
    this.showSuccessFlag();
    this.close();
  };

  private onBackButtonClicked = () => {
    this.props.analyticsClient.sendUIEvent({
      cloudId: this.props.cloudId,
      data: userInviteClosedUIEventData((this.getAnalyticsAttributes())),
    });

    this.close();
  };

  private close = () => {
    if (this.props.onClose) {
      this.props.onClose();
    }
    this.resetState();
  }

  private showSuccessFlag = () => {
    this.props.showFlag({
      autoDismiss: true,
      icon: createSuccessIcon(),
      id: `user.invite.drawer.success.${Date.now()}`,
      title: this.props.intl.formatMessage(messages.successFlagTitle, {
        number: this.state.emails.length,
      }),
      description: this.props.intl.formatMessage(messages.successFlagDescription),
    });
  };

  private showInviteErrorFlag = () => {
    this.props.showFlag({
      autoDismiss: true,
      icon: createErrorIcon(),
      id: `user.invite.drawer.invite.error.${Date.now()}`,
      title: this.props.intl.formatMessage(errorFlagMessages.title),
      description: this.props.intl.formatMessage(messages.inviteErrorFlagDescription, {
        number: this.state.emails.length,
      }),
    });
  };

  private onEmailsInputChange = (inputValue: string) => {
    if (!inputValue.length && !this.state.emailsInputValue.length) {
      return;
    }

    this.setState({ emailsInputValue: inputValue });
  };

  private onEmailsKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (!this.state.emailsInputValue) {
      return;
    }

    if (!(this.emailCreationKeys && this.emailCreationKeys.includes(event.key))) {
      return;
    }

    this.submitEmailInput();

    event.preventDefault();
  };

  private onEmailsInputBlur = () => {
    if (this.state.emailsInputValue) {
      this.submitEmailInput();
    }
  };

  private submitEmailInput = () => {
    const isNewValueUnique = !this.state.emailsInputValues.some(value => {
      return value.value === this.state.emailsInputValue;
    });

    if (!isNewValueUnique) {
      this.setState({ emailsInputValue: '' });

      return;
    }

    this.setState(prevState => {
      const newEmails = prevState.emailsInputValue
        .split(/,| /)
        .filter(email => email.length)
        .map(email => {
          const trimmed = email.trim();

          return {
            label: trimmed,
            value: trimmed,
          } as EmailInputValue;
        });

      const emailsInputValues = [
        ...prevState.emailsInputValues,
        ...newEmails,
      ];

      return {
        emails: emailsInputValues.map(email => email.value),
        emailsInputValue: '',
        emailsInputValues,
      };
    });
  };

  private onEmailsChange = (values) => {
    this.setState({
      emails: values.map(email => email.value),
      emailsInputValues: values,
    });
  };

  private onGroupMembershipChange = (items: [{ value: string }]) => {
    this.setState({
      groups: items.map(item => item.value),
    });
  };

  private areEmailAddressesValid = (emails: EmailInputValue[]) => {
    return emails.every(email => /[^@]+@[^\.]+\..+/g.test(email.value));
  }

  private isEmailLimitValid = () => {
    return this.state.emails.length <= MAX_NUMBER_INVITED_USERS;
  };
}

const withInviteUsersMutation = graphql<Props, UserInviteDrawerInviteMutation, UserInviteDrawerInviteMutationVariables>(inviteUsersMutation, {
  name: 'inviteUsers',
  options: (props) => {
    return {
      refetchQueries: [{
        query: siteUsersListQuery,
        variables: {
          cloudId: props.cloudId,
          input: {
            count: ROWS_PER_PAGE,
            start: getStart(1, ROWS_PER_PAGE),
          },
        },
      }],
    };
  },
});

const withUpdateSiteAccessMutation = graphql<Props, UserInviteDrawerUpdateSiteAccessMutation, UserInviteDrawerUpdateSiteAccessMutationVariables>(updateSiteAccessMutation, {
  name: 'updateSiteAccess',
});

const withQuery = graphql<Props, UserInviteDrawerQuery>(query, {
  skip: ({ isOpen }) => !isOpen,
});

export const UserInviteDrawer = withFlag(
  withAnalyticsClient(
    injectIntl(
      withInviteUsersMutation(
        withUpdateSiteAccessMutation(
          withQuery(
            withTrustedUsersFeatureFlag(UserInviteDrawerImpl),
          ),
        ),
      ),
    ),
  ),
);
