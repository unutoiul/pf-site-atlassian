import * as React from 'react';
import {
  defineMessages,
  FormattedHTMLMessage,
  FormattedMessage,
} from 'react-intl';

import {
  Checkbox as AkCheckbox,
} from '@atlaskit/checkbox';

import { EditDomainSettings } from '../user-invite-drawer.styled';

const messages = defineMessages({
  domainAccess: {
    id: 'user.invite.drawer.domain.access',
    defaultMessage: 'Allow accounts on the <strong>{domain} domain</strong> to join your site',
    description: 'Message that gets displayed next to an added domain checkbox to signal that it is available for self signup.',
  },
  editDomainAccess: {
    id: 'user.invite.drawer.edit.domain.access',
    defaultMessage: 'You can edit these settings any time.',
    description: 'Displayed next to a domain checkbox letting them know it can be changed anytime.',
  },
});

interface Props {
  emails: string[];
  onChange(domainsChecked: Array<{ name: string, isChecked: boolean }>): void;
}

interface State {
  domainsChecked: string[];
}

export class SiteAccessSection extends React.Component<Props, State> {
  public readonly state: Readonly<State> = {
    domainsChecked: [],
  };

  public componentDidUpdate(prevProps: Props) {
    if (prevProps.emails.length === this.props.emails.length) {
      return;
    }

    const prevDomains = this.getUniqueDomainsFromEmails(prevProps.emails);
    const newDomains = this.getUniqueDomainsFromEmails(this.props.emails);
    const domainsToClear = prevDomains.filter(domain => !newDomains.includes(domain));

    this.setState(prevState => ({
      domainsChecked: prevState.domainsChecked.filter(domain => !domainsToClear.includes(domain)),
    }), () => {
      const domainsChecked = this.state.domainsChecked.map(domain => (
        { name: domain, isChecked: true }
      ));
      const domainsUnchecked = newDomains.filter(domain => !this.state.domainsChecked.includes(domain))
        .map(domain => (
          { name: domain, isChecked: false }
        ));

      this.props.onChange([
        ...domainsChecked,
        ...domainsUnchecked,
      ]);
    });
  }

  public render() {
    const domains = this.getUniqueDomainsFromEmails(this.props.emails);

    return (
      <React.Fragment>
        {domains.map(domain => (
          <React.Fragment key={domain}>
            <AkCheckbox
              name={domain}
              isChecked={this.state.domainsChecked.includes(domain)}
              onChange={this.onCheckboxChange}
              label={(
                <FormattedHTMLMessage
                  {...messages.domainAccess}
                  values={{ domain }}
                />
              )}
            />
          </React.Fragment>
        ))}
        {domains.length > 0 && (
          <EditDomainSettings>
            <FormattedMessage {...messages.editDomainAccess} />
          </EditDomainSettings>
        )}
      </React.Fragment>
    );
  }
  private getUniqueDomainsFromEmails = (emails: string[]): string[] => {
    if (emails.length <= 0) {
      return [];
    }

    const domains = emails
      .map(email => email.split('@')[1])
      .filter(email => email !== undefined);

    return [...new Set(domains)];
  };

  private onCheckboxChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const name = event.target.name;
    const stateCallback = () => {
      const domainsChecked = this.state.domainsChecked.map(domain => (
        { name: domain, isChecked: true }
      ));
      const domainsUnchecked = this.getUniqueDomainsFromEmails(this.props.emails)
        .filter(domain => !this.state.domainsChecked.includes(domain))
        .map(domain => (
          { name: domain, isChecked: false }
        ));

      this.props.onChange([
        ...domainsChecked,
        ...domainsUnchecked,
      ]);
    };

    if (event.target.checked) {
      this.setState(prevState => ({
        domainsChecked: [...prevState.domainsChecked, name],
      }), stateCallback);
    } else {
      this.setState(prevState => ({
        domainsChecked: prevState.domainsChecked.filter(domain => domain !== name),
      }), stateCallback);
    }
  };
}
