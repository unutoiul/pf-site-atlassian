import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import {
  Checkbox as AkCheckbox,
} from '@atlaskit/checkbox';

import { EditDomainSettings } from '../user-invite-drawer.styled';
import { SiteAccessSection } from './site-access-section';

describe('User invite drawer - Site access section', () => {
  const sandbox = sinon.sandbox.create();

  let onChangeSpy: sinon.SinonSpy;

  beforeEach(() => {
    onChangeSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  const getWrapper = ({ emails }) => {
    return shallow((
      <SiteAccessSection
        emails={emails}
        onChange={onChangeSpy}
      />
    ));
  };

  it('should render unique domains from an email list', () => {
    const wrapper = getWrapper({
      emails: [
        'a@gmail.com',
        'b@gmail.com',
        'c@gmail.com',
        'a@atlassian.com',
        'b@atlassian.com',
        'a@corgi.com',
      ],
    });

    const checkboxes = wrapper.find(AkCheckbox);
    expect(checkboxes).to.have.lengthOf(3);
  });

  it(`shouldn't render a domain if the input email isn't a string with an @ symbol`, () => {
    const wrapper = getWrapper({
      emails: [
        'a@gmail.com',
        'corgi',
        'Thom Yorke',
        '(*&%#(*()^*(%',
        '121412351983471234',
      ],
    });

    const checkboxes = wrapper.find(AkCheckbox);
    expect(checkboxes).to.have.lengthOf(1);
  });

  it('should render a domain if the input email contains an @ symbol', () => {
    const wrapper = getWrapper({
      emails: [
        'a@gmail.com',
        'b@atlassian.com',
        'foo@bar',
        'foo',
      ],
    });

    const checkboxes = wrapper.find(AkCheckbox);
    expect(checkboxes).to.have.lengthOf(3);
  });

  it(`shouldn't render any domains if there are no emails`, () => {
    const wrapper = getWrapper({
      emails: [],
    });

    const checkboxes = wrapper.find(AkCheckbox);
    expect(checkboxes).to.have.lengthOf(0);
  });

  it('should render domains as unchecked by default', () => {
    const wrapper = getWrapper({
      emails: [
        'a@gmail.com',
        'a@corgi.com',
      ],
    });

    wrapper.find(AkCheckbox)
      .every(checkbox => {
        expect(checkbox.prop('isChecked')).to.equal(false);
      });
  });

  it('should call onChange with all new and existing checked domains', () => {
    const wrapper = getWrapper({
      emails: [
        'a@gmail.com',
        'a@atlassian.com',
      ],
    });

    const firstCheckbox = wrapper.find(AkCheckbox).first();
    const secondCheckbox = wrapper.find(AkCheckbox).last();

    expect(onChangeSpy.called).to.equal(false);

    firstCheckbox.simulate('change', {
      target: {
        checked: true,
        name: 'gmail.com',
      },
    });

    expect(onChangeSpy.calledWithExactly([{ name: 'gmail.com', isChecked: true }, { name: 'atlassian.com', isChecked: false }])).to.equal(true);

    secondCheckbox.simulate('change', {
      target: {
        checked: true,
        name: 'atlassian.com',
      },
    });

    expect(onChangeSpy.calledWithExactly([{ name: 'gmail.com', isChecked: true }, { name: 'atlassian.com', isChecked: true }])).to.equal(true);
  });

  it('should call onChange with all removed and existing domains', () => {
    const wrapper = getWrapper({
      emails: [
        'a@gmail.com',
        'a@atlassian.com',
      ],
    });

    const firstCheckbox = wrapper.find(AkCheckbox).first();
    const secondCheckbox = wrapper.find(AkCheckbox).last();

    firstCheckbox.simulate('change', {
      target: {
        checked: true,
        name: 'gmail.com',
      },
    });

    secondCheckbox.simulate('change', {
      target: {
        checked: true,
        name: 'atlassian.com',
      },
    });

    expect(onChangeSpy.calledWithExactly([{ name: 'gmail.com', isChecked: true }, { name: 'atlassian.com', isChecked: true }])).to.equal(true);

    firstCheckbox.simulate('change', {
      target: {
        checked: false,
        name: 'gmail.com',
      },
    });

    expect(onChangeSpy.calledWithExactly([{ name: 'atlassian.com', isChecked: true }, { name: 'gmail.com', isChecked: false }])).to.equal(true);
  });

  it('should render only one edit domain access message for multiple domains', () => {
    const wrapper = getWrapper({
      emails: [
        'a@gmail.com',
        'a@atlassian.com',
      ],
    });

    expect(wrapper.find(EditDomainSettings)).to.have.lengthOf(1);
  });

  it(`shouldn't render an edit domain access message when no domains are present`, () => {
    const wrapper = getWrapper({
      emails: [],
    });

    expect(wrapper.find(EditDomainSettings)).to.have.lengthOf(0);
  });
});
