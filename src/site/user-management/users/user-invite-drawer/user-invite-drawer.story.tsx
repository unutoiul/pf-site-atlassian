import { storiesOf } from '@storybook/react';
import * as React from 'react';
import ApolloProvider from 'react-apollo/ApolloProvider';
import { IntlProvider } from 'react-intl';

import { createApolloClient } from '../../../../apollo-client';
import { createStorybookAnalyticsClient } from '../../../../utilities/storybooks';
import { createMockIntlProp } from '../../../../utilities/testing';
import { UserInviteDrawerImpl } from './user-invite-drawer';

const client = createApolloClient();

const defaultProps = {
  isOpen: true,
  trustedUsersFeatureFlag: {
    value: false,
    isLoading: false,
  },
  data: {
    loading: false,
    error: false,
    currentSite: {
      id: '38b7be26-03f7-42a9-b18b-9cdb56d204c2',
      groupsUseAccessConfig: [
        {
          product: {
            productId: 'jira-software',
            productName: 'Jira Software',
            __typename: 'ProductAccess',
          },
          __typename: 'GroupsAccessConfig',
        },
        {
          product: {
            productId: 'hipchat.cloud',
            productName: 'Stride',
            __typename: 'ProductAccess',
          },
          __typename: 'GroupsAccessConfig',
        },
        {
          product: {
            productId: 'jira-core',
            productName: 'Jira Core',
            __typename: 'ProductAccess',
          },
          __typename: 'GroupsAccessConfig',
        },
      ],
      defaultApps: [
        {
          productId: 'jira-servicedesk',
          __typename: 'ProductAccess',
        },
        {
          productId: 'jira-software',
          __typename: 'ProductAccess',
        },
        {
          productId: 'conf',
          __typename: 'ProductAccess',
        },
      ],
      __typename: 'CurrentSite',
    },
    ...{} as any,
  },
  intl: createMockIntlProp(),
  analyticsClient: createStorybookAnalyticsClient(),
  inviteUsers: undefined as any,
  updateSiteAccess: undefined as any,
  showFlag: undefined as any,
  hideFlag: undefined as any,
};

storiesOf('Site|Users Invite Drawer', module)
  .add('Default', () => {
    return (
      <IntlProvider locale="en">
        <ApolloProvider client={client}>
          <UserInviteDrawerImpl {...defaultProps} />
        </ApolloProvider>
      </IntlProvider>
    );
  })
  .add('With loading', () => {
    return (
      <IntlProvider locale="en">
        <ApolloProvider client={client}>
          <UserInviteDrawerImpl
            {...defaultProps}
            data={{
              ...defaultProps.data,
              loading: true,
              error: false,
            }}
          />
        </ApolloProvider>
      </IntlProvider>
    );
  })
  .add('With loading error', () => {
    return (
      <IntlProvider locale="en">
        <ApolloProvider client={client}>
          <UserInviteDrawerImpl
            {...defaultProps}
            data={{
              ...defaultProps.data,
              loading: false,
              error: true,
            }}
          />
        </ApolloProvider>
      </IntlProvider>
    );
  })
  .add('With trusted user feature flag', () => {
    const newProps = {
      ...defaultProps,
      trustedUsersFeatureFlag: {
        value: true,
        isLoading: false,
      },
    };

    return (
      <IntlProvider locale="en">
        <ApolloProvider client={client}>
          <UserInviteDrawerImpl
            {...newProps}
          />
        </ApolloProvider>
      </IntlProvider>
    );
  });
