import { defineMessages, InjectedIntl } from 'react-intl';
import { UserDetailsReinviteUserMutationVariables } from 'src/schema/schema-types';

import { NewAnalyticsClient, resentUserInvitationTrackEventData, userListScreenInviteResent } from 'common/analytics';
import { createErrorIcon, createSuccessIcon, errorFlagMessages } from 'common/error';

import { AnalyticsProps } from '../../user-management-analytics';

export const messages = defineMessages({
  reminderTitle: {
    id: 'users.details.site.resend.invite.success.flag.title',
    description: 'Title of the success flag that appears when the invite has been successfully sent',
    defaultMessage: 'Reminder sent!',
  },
  reminderDescription: {
    id: 'users.details.site.resend.invite.success.flag.description',
    description: 'Description of the success flag that appears when the invite has been successfully sent',
    defaultMessage: 'An email has been sent to the user to complete their account activation.',
  },
  errorDescription: {
    id: 'users.details.site.resend.invite.error.description',
    description: 'Description of the error flag that appears when the invite has been successfully sent',
    defaultMessage: 'The user could not be re-invited due to an unknown error. Refresh the page to try again.',
  },
});

interface ResendInviteInput {
  mutate: any;
  showFlag: any;
  variables: UserDetailsReinviteUserMutationVariables;
  intl: InjectedIntl;
  analyticsClient: NewAnalyticsClient;
  trackEventSource: 'userListScreen' | 'userDetailsScreen';
}

export const resendInvite = (input: ResendInviteInput & AnalyticsProps): void => {
  if (!input.mutate) {
    return;
  }
  const variables: UserDetailsReinviteUserMutationVariables = {
    cloudId: input.variables.cloudId,
    userId: input.variables.userId,
  };

  const trackEventData = input.trackEventSource === 'userDetailsScreen' ? resentUserInvitationTrackEventData : userListScreenInviteResent;
  input.mutate({
    variables,
  }).then(() => {
    input.showFlag({
      autoDismiss: true,
      icon: createSuccessIcon(),
      id: `user.details.re.invite.user.success.${(Date.now().valueOf())}`,
      title: input.intl.formatMessage(messages.reminderTitle),
      description: input.intl.formatMessage(messages.reminderDescription),
    });

    input.analyticsClient.sendTrackEvent({
      cloudId: input.variables.cloudId,
      data: trackEventData({
        userState: input.userStates,
        userId: input.variables.userId,
      }),
    });
  }).catch(() => {
    input.showFlag({
      autoDismiss: true,
      icon: createErrorIcon(),
      id: `user.details.re.invite.user.error.${(Date.now().valueOf())}`,
      title: input.intl.formatMessage(errorFlagMessages.title),
      description: input.intl.formatMessage(messages.errorDescription),
    });
  });
};
