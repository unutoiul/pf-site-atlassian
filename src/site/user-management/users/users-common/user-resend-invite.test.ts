import { expect } from 'chai';
import * as sinon from 'sinon';

import { createMockIntlProp, waitUntil } from '../../../../utilities/testing';
import { resendInvite } from './user-resend-invite';

const noop = () => null;

describe('userResendInvite', () => {
  it('should send a track event with userListScreen as the source when userListScreen is specified as the track event source', async () => {
    const sendTrackEventSpy = sinon.spy();

    resendInvite({
      mutate: sinon.stub().resolves(),
      showFlag: noop,
      variables: {
        cloudId: 'fake-cloud-id',
        userId: 'fake-user-id',
      },
      intl: createMockIntlProp(),
      analyticsClient: {
        sendTrackEvent: sendTrackEventSpy,
      } as any,
      trackEventSource: 'userListScreen',
      userStates: [],
    });

    await waitUntil(() => sendTrackEventSpy.callCount > 0);

    expect(sendTrackEventSpy.callCount).to.equal(1);
    expect(sendTrackEventSpy.getCalls()[0].args[0].data.attributes).to.deep.equal({
      userId: 'fake-user-id',
      userState: [],
    });
    expect(sendTrackEventSpy.getCalls()[0].args[0].data.source).to.equal('userListScreen');
  });

  it('should send a track event with userDetailsScreen as the source when userDetailsScreen is specified as the track event source', async () => {
    const sendTrackEventSpy = sinon.spy();

    resendInvite({
      mutate: sinon.stub().resolves(),
      showFlag: noop,
      variables: {
        cloudId: 'fake-cloud-id',
        userId: 'fake-user-id',
      },
      intl: createMockIntlProp(),
      analyticsClient: {
        sendTrackEvent: sendTrackEventSpy,
      } as any,
      trackEventSource: 'userDetailsScreen',
      userStates: [],
    });

    await waitUntil(() => sendTrackEventSpy.callCount > 0);

    expect(sendTrackEventSpy.callCount).to.equal(1);
    expect(sendTrackEventSpy.getCalls()[0].args[0].data.attributes).to.deep.equal({
      userId: 'fake-user-id',
      userState: [],
    });
    expect(sendTrackEventSpy.getCalls()[0].args[0].data.source).to.equal('userDetailsScreen');
  });
});
