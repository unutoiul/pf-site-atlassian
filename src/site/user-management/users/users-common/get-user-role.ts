import { User } from '../../../../schema/schema-types/schema-types';

type UserRole = 'site-admin' | 'system-admin' | 'trusted-user' | 'basic';

export function getUserRole(user: User): UserRole {
  return user.siteAdmin ? 'site-admin' :
    user.sysAdmin ? 'system-admin' :
    user.trustedUser ? 'trusted-user' :
    'basic';
}
