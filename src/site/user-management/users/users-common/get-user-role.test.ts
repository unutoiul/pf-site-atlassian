import { expect } from 'chai';

import { getUserRole } from './get-user-role';

describe('getUserRole', () => {
  it('should return site-admin if the user is a site admin', () => {
    expect(getUserRole({
      siteAdmin: true,
    } as any)).to.equal('site-admin');
  });

  it('should return system-admin if the user is a system admin', () => {
    expect(getUserRole({
      sysAdmin: true,
    } as any)).to.equal('system-admin');
  });

  it('should return trusted-user if the user is a trusted user', () => {
    expect(getUserRole({
      trustedUser: true,
    } as any)).to.equal('trusted-user');
  });

  it('should return basic if the user is not a system admin, site admin, or trusted user', () => {
    expect(getUserRole({} as any)).to.equal('basic');
  });
});
