import * as React from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';

import { OscillatingBlock } from 'common/loading';

interface OwnProps {
  displayName?: string;
  isLoading: boolean;
}

const messages = defineMessages({
  title: {
    id: 'user.details.page.title',
    description: 'Title of the user details page',
    defaultMessage: 'User Details',
  },
  titleWithoutS: {
    id: 'user.details.page.title.without.s',
    description: 'Title of the user details page with a name that ends without an s',
    defaultMessage: '{name}\'s details',
  },
  titleWithS: {
    id: 'user.details.page.title.with.s',
    description: 'Title of the user details page with a name that ends with an s',
    defaultMessage: '{name}\' details',
  },
});

export class UserDetailsHeaderImpl extends React.Component<OwnProps> {
  public render() {
    if (this.props.isLoading) {
      return <OscillatingBlock width={'300px'} heightPx={32}/>;
    }

    if (!this.props.displayName) {
      return <FormattedMessage {...messages.title} />;
    }

    return UserDetailsHeaderImpl.sanitizeTitle(this.props.displayName);
  }

  private static sanitizeTitle(name: string) {
    const message = name.endsWith('s') ? messages.titleWithS : messages.titleWithoutS;

    return <FormattedMessage {...message} values={{ name }} />;
  }
}

export const UserDetailsHeader = UserDetailsHeaderImpl;
