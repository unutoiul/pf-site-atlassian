import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';

import { PageLayout } from 'common/page-layout';

import { UserDetailsAccountSection } from '../user-details-account/user-details-account-section';
import { UserDetailsGroupSection } from '../user-details-group/user-details-group-section';
import { UserDetailsPageImpl } from './user-details-page';

import { createMockIntlProp } from '../../../../../utilities/testing';

describe('User details page layout', () => {

  function createTestShallowComponent(data?): ShallowWrapper {
    return shallow(
      <UserDetailsPageImpl
        intl={createMockIntlProp()}
        match={{ isExact: false, path: '', url: '', params: { cloudId: 'DUMMY-CLOUD-ID', userId: 'DUMMY-USER-ID' } }}
        location={null as any}
        history={null as any}
        data={data}
        userStates={[]}
      />,
    );
  }

  it('should render a group section', () => {
    const wrapper = createTestShallowComponent();
    const userDetailsGroupSection = wrapper.find(UserDetailsGroupSection);

    expect(userDetailsGroupSection.length).to.equal(1);
    expect(userDetailsGroupSection.at(0).props().cloudId).to.equal('DUMMY-CLOUD-ID');
    expect(userDetailsGroupSection.at(0).props().userId).to.equal('DUMMY-USER-ID');
  });

  it('should render a group section when in the loading state', () => {
    const wrapper = createTestShallowComponent({
      loading: true,
      user: {},
    });
    const userDetailsGroupSection = wrapper.find(UserDetailsGroupSection);

    expect(userDetailsGroupSection.length).to.equal(1);
    expect(userDetailsGroupSection.at(0).props().cloudId).to.equal('DUMMY-CLOUD-ID');
    expect(userDetailsGroupSection.at(0).props().userId).to.equal('DUMMY-USER-ID');
  });

  it('should render something when given partial data', () => {
    const wrapper = createTestShallowComponent({
      loading: false,
      user: {},
    });
    const userDetailsGroupSection = wrapper.find(UserDetailsGroupSection);

    expect(userDetailsGroupSection.length).to.equal(1);
    expect(userDetailsGroupSection.at(0).props().cloudId).to.equal('DUMMY-CLOUD-ID');
    expect(userDetailsGroupSection.at(0).props().userId).to.equal('DUMMY-USER-ID');
  });

  it('should render an account section', () => {
    const wrapper = createTestShallowComponent();
    const userDetailsAccountSection = wrapper.find<any>(UserDetailsAccountSection);

    expect(userDetailsAccountSection.length).to.equal(1);
    expect(userDetailsAccountSection.at(0).props().cloudId).to.equal('DUMMY-CLOUD-ID');
    expect(userDetailsAccountSection.at(0).props().userId).to.equal('DUMMY-USER-ID');
  });

  it('should render a page title', () => {
    const wrapper = createTestShallowComponent();
    const pageLayout = wrapper.find(PageLayout);

    expect(pageLayout.length).to.equal(1);
    const title = shallow(pageLayout.props().title as any);
    expect(title.find(FormattedMessage).exists()).to.equal(true);
  });
});
