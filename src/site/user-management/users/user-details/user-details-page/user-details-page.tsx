import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';

import { PageLayout } from 'common/page-layout';
import { PageParentLink } from 'common/page-parent-link/page-parent-link';

import { AnalyticsProps, getUserStates } from '../../../user-management-analytics';

import { UserDetailsPageQuery } from '../../../../../schema/schema-types';
import { util } from '../../../../../utilities/admin-hub';
import { isUserInSiteAdminGroup, isUserInSystemAdminGroup } from '../../site-privilege-checks';
import { Permissions } from '../../user-permission-section/permissions';
import { UserDetailsAccessSection } from '../user-details-access/user-details-access-section';
import { UserDetailsAccountSection } from '../user-details-account/user-details-account-section';
import { UserDetailsGroupSection } from '../user-details-group/user-details-group-section';
import { UserDetailsPermissionSection } from '../user-details-permission/user-details-permission-section';
import { UserDetailsActions } from './user-details-actions/user-details-actions';
import { UserDetailsHeader } from './user-details-header';
import userDetailsPageQuery from './user-details-page.query.graphql';
import { Children, InnerChildren, SectionWrapper } from './user-details.styled';

const messages = defineMessages({
  parentPage: {
    id: 'user.details.page.parent',
    description: 'Links to the users list page, which is the parent page of the user details page',
    defaultMessage: 'Users',
  },
});

interface SiteParams {
  cloudId: string;
  userId: string;
}

type UserDetailsPageProps = ChildProps<AnalyticsProps & RouteComponentProps<SiteParams> & InjectedIntlProps, UserDetailsPageQuery>;

const currentUserPermission = ({ trustedUser, siteAdmin, sysAdmin, system }: {
  trustedUser: boolean | null,
  siteAdmin: boolean | null,
  sysAdmin: boolean | null,
  system: boolean,
}) => {
  if (siteAdmin || sysAdmin || system) {
    return Permissions.SITEADMIN;
  }

  if (trustedUser) {
    return Permissions.TRUSTED;
  }

  return Permissions.BASIC;
};

export class UserDetailsPageImpl extends React.Component<UserDetailsPageProps> {
  public render() {
    const { formatMessage } = this.props.intl;
    const { cloudId, userId } = this.props.match.params;
    const { data } = this.props;
    const isUserSystem: boolean = !!(
      data &&
      !data.loading &&
      data.user &&
      data.user.userDetails &&
      data.user.userDetails.system
    );

    const isUserSystemAdmin: boolean = !!(data && data.user && isUserInSystemAdminGroup(data.user));
    const isUserSiteAdmin: boolean = !!(data && data.user && isUserInSiteAdminGroup(data.user));

    const isCurrentUser: boolean = !!(
      data &&
      !data.loading &&
      data.user &&
      data.user.userDetails &&
      data.currentUser &&
      data.user.userDetails.id === data.currentUser.id
    );

    const isUserBlocked: boolean = !!(
      data &&
      !data.loading &&
      data.user &&
      data.user.userDetails &&
      data.user.userDetails.activeStatus === 'BLOCKED'
    );

    const isUserWithAccess: boolean = !!(
      data &&
      !data.loading &&
      data.user &&
      data.user.userDetails &&
      data.user.userDetails.active
    );

    const isUserWithPresence: boolean = !!(
      data &&
      !data.loading &&
      data.user &&
      data.user.userDetails &&
      data.user.userDetails.presence
    );

    const displayName: string | undefined = data && data.loading ? '' : (
      data &&
      data.user &&
      data.user.userDetails &&
      data.user.userDetails.displayName
    );

    const userStates = getUserStates(data && data.user && data.user.userDetails);

    const isUserTrusted: boolean = !!(data && data.user && data.user.userDetails && data.user.userDetails.trustedUser);

    const userPermission = currentUserPermission({
      trustedUser: isUserTrusted,
      siteAdmin: isUserSiteAdmin,
      sysAdmin: isUserSystemAdmin,
      system: isUserSystem,
    });

    const children = (
      <Children>
        <InnerChildren>
          <SectionWrapper>
            <UserDetailsAccessSection
              cloudId={cloudId}
              userId={userId}
              isCurrentUser={isCurrentUser}
              isUserSiteAdmin={isUserSiteAdmin}
              isUserSystemAdmin={isUserSystemAdmin}
              userPermission={userPermission}
            />
          </SectionWrapper>
          <SectionWrapper>
            <UserDetailsPermissionSection
              cloudId={cloudId}
              userId={userId}
              isCurrentUser={isCurrentUser}
              userPermission={userPermission}
              isUserWithNoAccess={!isUserWithAccess}
              isUserSystem={isUserSystem}
              isUserSystemAdmin={isUserSystemAdmin}
              userStates={userStates}
            />
          </SectionWrapper>
          <SectionWrapper>
            <UserDetailsGroupSection
              cloudId={cloudId}
              userId={userId}
              isUserSystem={isUserSystem}
              isUserSystemAdmin={isUserSystemAdmin}
              userStates={userStates}
            />
          </SectionWrapper>
        </InnerChildren>
        <SectionWrapper>
          <UserDetailsAccountSection cloudId={cloudId} userId={userId} />
        </SectionWrapper>
      </Children>
    );

    return (
      <div>
        <PageLayout
          title={
            <UserDetailsHeader
              isLoading={!!(this.props.data && this.props.data.loading)}
              displayName={displayName}
            />
          }
          action={
            <UserDetailsActions
              isLoading={!!(data && data.loading)}
              isError={!!(!data || data.error)}
              isCurrentUser={isCurrentUser}
              isUserSystem={isUserSystem}
              isUserSiteAdmin={isUserSiteAdmin}
              isUserSystemAdmin={isUserSystemAdmin}
              isUserWithNoPresence={!isUserWithPresence}
              isUserWithNoAccess={!isUserWithAccess}
              isUserBlocked={isUserBlocked}
              history={this.props.history}
              cloudId={cloudId}
              userId={userId}
              displayName={displayName}
              userStates={userStates}
            />
          }
          pageParent={
            <PageParentLink
              linkText={formatMessage(messages.parentPage)}
              linkLocation={`${util.siteAdminBasePath}/s/${this.props.match.params.cloudId}/users`}
            />
          }
          isFullWidth={true}
        >
          {children}
        </PageLayout>
      </div>
    );
  }
}

const withUserSitePrivilege = graphql<UserDetailsPageProps, UserDetailsPageQuery>(userDetailsPageQuery, {
  skip: ({ match: { params: { cloudId, userId } } }) => (!cloudId || !userId),
  options: ({ match: { params: { cloudId, userId } } }) => ({ variables: { cloudId, userId, start: 1, count: 100 } }),
});
export const UserDetailsPage = withRouter(injectIntl(withUserSitePrivilege(UserDetailsPageImpl)));
