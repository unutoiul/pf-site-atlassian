import styled from 'styled-components';

import {
  colors as akColors,
  fontSize as akFontSize,
  gridSize as akGridSize,
} from '@atlaskit/theme';

export const Header = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const Description = styled.span`
  color: ${akColors.subtleHeading};
`;

export const Content = styled.div`
  padding-bottom: ${akGridSize() * 2}px;
`;

export const Cell = styled.div`
  height: ${akGridSize() * 4}px;
  display: flex;
  align-items: center;
  font-size: ${akFontSize() - 0.5}px;
`;

export const Name = styled(Cell)`
  width: ${akGridSize() * 25}px;
`;

export const Hover = styled(Cell)`
  width: ${akGridSize() * 8}px;
`;

export const Options = styled(Cell)`
  float: right;
`;

export const LozengeWrapper = styled.div`
  display: inline;
  padding-top: ${akGridSize() / 2}px;
  padding-right: ${akGridSize() * 2}px;
`;

export const ActiveStatusDialog = styled.div`
  width: ${akGridSize() * 40}px;
`;

export const Children = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
`;

export const InnerChildren = styled.div`
  display: flex;
  flex-direction: column;
`;

export const GroupsOuterDiv = styled.div`
  width: ${akGridSize() * 56}px;
`;

export const SectionWrapper = styled.div`
  padding-bottom: ${akGridSize() * 7}px;
`;

export const SectionTitle = styled.div`
  align-items: center;
  justify-content: flex-start;
  display: flex;
  padding-bottom: ${akGridSize() * 4}px;
`;

export const ModalWrapper = styled.div`
  white-space: normal;
`;
