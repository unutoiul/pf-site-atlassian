import { storiesOf } from '@storybook/react';
import * as React from 'react';
import ApolloProvider from 'react-apollo/ApolloProvider';
import { IntlProvider } from 'react-intl';
import { StaticRouter } from 'react-router';

import { getStart } from 'common/pagination';

import { createApolloClient } from '../../../../../apollo-client';
import { createMockIntlProp } from '../../../../../utilities/testing';
import userDetailsAccessSectionQuery from '../user-details-access/user-details-access.query.graphql';
import userDetailsAccountQuery from '../user-details-account/user-details-account-section.query.graphql';
import userDetailsGroupSectionQuery from '../user-details-group/user-details-group-section.query.graphql';
import { DEFAULT_ROWS_PER_PAGE, STARTING_PAGE } from './user-details-constants';
import { UserDetailsPageImpl } from './user-details-page';

const cloudId = 'DUMMY-CLOUD-ID';
const userId = 'DUMMY-USER-ID';

const defaultProps = {
  intl: createMockIntlProp(),
  match: { params: { cloudId, userId } } as any,
  location: window.location as any,
  history: { replace: () => null } as any,
  userStates: [],
};

const defaultUser = {
  __typename: 'User',
  userDetails: {
    id: userId,
    email: 'fake@email.com',
    displayName: 'Fake User',
    active: true,
    nickname: '@fakemate',
    location: '341 George St',
    companyName: 'Kudos Inc',
    department: 'Quality Assurance',
    title: 'CEO',
    timezone: 'Australia/Sydney',
    system: false,
  },
  groups: {
    total: 0,
    groups: [],
    __typename: 'PaginatedGroups',
  },
  productAccess: [],
};

const client = createApolloClient();

function createTestComponent(user = defaultUser) {

  client.writeQuery({
    query: userDetailsAccountQuery,
    variables: { cloudId, id: userId },
    data: user,
  });

  client.writeQuery({
    query: userDetailsGroupSectionQuery,
    variables: { cloudId, userId, start: getStart(STARTING_PAGE, DEFAULT_ROWS_PER_PAGE), count: DEFAULT_ROWS_PER_PAGE },
    data: user,
  });

  client.writeQuery({
    query: userDetailsAccessSectionQuery,
    variables: { cloudId, userId, start: getStart(STARTING_PAGE, DEFAULT_ROWS_PER_PAGE), count: DEFAULT_ROWS_PER_PAGE },
    data: user,
  });

  return (
    <ApolloProvider client={client}>
      <IntlProvider>
        <StaticRouter context={{}}>
          <UserDetailsPageImpl {...defaultProps} />
        </StaticRouter>
      </IntlProvider>
    </ApolloProvider>
  );
}

storiesOf('Site|User Details/Page', module)
  .add('Default', () => {
    return createTestComponent();
  });
