
import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import { ButtonHover } from 'common/button-hover/button-hover';

import { createMockAnalyticsClient, createMockIntlProp, waitUntil } from '../../../../../../utilities/testing';
import { UserDetailsActionsImpersonateUserImpl } from './user-details-actions-impersonate-user';

describe('User details page - Impersonating user', () => {

  const sandbox = sinon.sandbox.create();

  let showFlagSpy;
  let hideFlagSpy;
  let mutateSpy;
  let redirectSpy;

  beforeEach(() => {
    showFlagSpy = sandbox.spy();
    hideFlagSpy = sandbox.spy();
    mutateSpy = sandbox.stub().resolves();
    redirectSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  function createTestComponent(
    { isDisabled = false }:
    { isDisabled: boolean},
  ): ShallowWrapper {
    return shallow(
      <UserDetailsActionsImpersonateUserImpl
        intl={createMockIntlProp()}
        analyticsClient={createMockAnalyticsClient()}
        cloudId="dummy-cloud-id"
        userId="dummy-user-id"
        displayName={'Super Happy Pug'}
        userStates={[]}
        isDisabled={isDisabled}
        showFlag={showFlagSpy}
        hideFlag={hideFlagSpy}
        mutate={mutateSpy}
        redirect={redirectSpy}
      />,
    );
  }

  it('should be disabled', () => {
    const wrapper = createTestComponent({ isDisabled: true });
    const button = wrapper.find(ButtonHover);
    expect(button.exists()).to.equal(true);
    expect(button.props().isDisabled).to.equal(true);
  });

  it('should be enabled', () => {
    const wrapper = createTestComponent({ isDisabled: false });
    const button = wrapper.find(ButtonHover);
    expect(button.exists()).to.equal(true);
    expect(button.props().isDisabled).to.equal(false);
  });

  it('should call the correct mutation when the button is clicked', async () => {
    const wrapper = createTestComponent({ isDisabled: false });
    const button = wrapper.find(ButtonHover);
    expect(button.exists()).to.equal(true);
    button.simulate('click');
    expect(mutateSpy.called).to.equal(true);
    expect(mutateSpy.calledWith({
      variables: {
        cloudId: 'dummy-cloud-id',
        userId: 'dummy-user-id',
      },
    })).to.equal(true);
  });

  it('should display a success flag when prompting to reset password is successful', async () => {
    mutateSpy = sandbox.stub().resolves();
    const wrapper = createTestComponent({ isDisabled: false });
    const button = wrapper.find(ButtonHover);
    button.simulate('click');
    expect(mutateSpy.called).to.equal(true);
    await waitUntil(() => showFlagSpy.callCount > 0);
    expect(showFlagSpy.calledWithMatch({ title: 'Log in as user' })).to.equal(true);
  });

  it('Should display an error flag when impersonating user is not successful', async () => {
    mutateSpy = sandbox.stub().rejects();
    const wrapper = createTestComponent({ isDisabled: false });
    const button = wrapper.find(ButtonHover);
    button.simulate('click');
    expect(mutateSpy.called).to.equal(true);
    await waitUntil(() => showFlagSpy.callCount > 0);
    expect(showFlagSpy.calledWithMatch({ title: 'Something went wrong' })).to.equal(true);
  });
});
