
import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';

import { createMockIntlProp } from '../../../../../../utilities/testing';
import { UserDetailsActionsImpl } from './user-details-actions';
import { UserDetailsActionsImpersonateUser } from './user-details-actions-impersonate-user';
import { UserDetailsActionsPromptResetPassword } from './user-details-actions-prompt-reset-password';
import { UserDetailsActionsRemoveUser } from './user-details-actions-remove-user-modal';

describe('User details page actions', () => {

  function createTestComponent({
    isLoading = false,
    isError = false,
    isCurrentUser = false,
    isUserSystem = false,
    isUserSystemAdmin = false,
    isUserSiteAdmin = false,
    isUserBlocked = false,
    isUserWithNoPresence = false,
    isUserWithNoAccess = false,
  }): ShallowWrapper {
    return shallow(
      <UserDetailsActionsImpl
        intl={createMockIntlProp()}
        cloudId={'dummy-cloud-id'}
        userId={'dummy-user-id'}
        isLoading={isLoading}
        isError={isError}
        isCurrentUser={isCurrentUser}
        isUserSystem={isUserSystem}
        isUserSystemAdmin={isUserSystemAdmin}
        isUserSiteAdmin={isUserSiteAdmin}
        isUserBlocked={isUserBlocked}
        isUserWithNoPresence={isUserWithNoPresence}
        isUserWithNoAccess={isUserWithNoAccess}
        history={undefined as any}
        showFlag={undefined as any}
        hideFlag={undefined as any}
        userStates={[]}
      />,
    );
  }

  describe('Remove user button', () => {

    it('Should exist', () => {
      const wrapper = createTestComponent({});
      const button = wrapper.find<any>(UserDetailsActionsRemoveUser);
      expect(button.exists()).to.equal(true);
    });

    it('Should be disabled when it is loading', () => {
      const wrapper = createTestComponent({ isLoading: true });
      const button = wrapper.find<any>(UserDetailsActionsRemoveUser);
      expect(button.at(0).props().isDisabled).to.equal(true);
    });

    it('Should be disabled when it is errored', () => {
      const wrapper = createTestComponent({ isError: true });
      const button = wrapper.find<any>(UserDetailsActionsRemoveUser);
      expect(button.at(0).props().isDisabled).to.equal(true);
    });

    it('Should be disabled when user is a system user', () => {
      const wrapper = createTestComponent({ isUserSystem: true });
      const button = wrapper.find<any>(UserDetailsActionsRemoveUser);
      expect(button.at(0).props().isDisabled).to.equal(true);
    });

    it('Should be disabled when user is a system administrator', () => {
      const wrapper = createTestComponent({ isUserSystemAdmin: true });
      const button = wrapper.find<any>(UserDetailsActionsRemoveUser);
      expect(button.at(0).props().isDisabled).to.equal(true);
    });

    it('Should be disabled when user is a site administrator', () => {
      const wrapper = createTestComponent({ isUserSiteAdmin: true });
      const button = wrapper.find<any>(UserDetailsActionsRemoveUser);
      expect(button.at(0).props().isDisabled).to.equal(true);
    });

    it('Should be disabled when user is themselves', () => {
      const wrapper = createTestComponent({ isCurrentUser: true });
      const button = wrapper.find<any>(UserDetailsActionsRemoveUser);
      expect(button.at(0).props().isDisabled).to.equal(true);
    });

    it('Should be enabled', () => {
      const wrapper = createTestComponent({});
      const button = wrapper.find<any>(UserDetailsActionsRemoveUser);
      expect(button.at(0).props().isDisabled).to.equal(false);
    });

  });

  describe('Log in as user button', () => {

    it('Should exist', () => {
      const wrapper = createTestComponent({});
      const button = wrapper.find(UserDetailsActionsImpersonateUser);
      expect(button.at(0).props().isDisabled).to.equal(false);
    });

    it('Should be disabled when it is loading', () => {
      const wrapper = createTestComponent({ isLoading: true });
      const button = wrapper.find(UserDetailsActionsImpersonateUser);
      expect(button.at(0).props().isDisabled).to.equal(true);
      expect(button.at(0).props().disabledReason).to.equal(undefined);
    });

    it('Should be disabled when it is errored', () => {
      const wrapper = createTestComponent({ isError: true });
      const button = wrapper.find(UserDetailsActionsImpersonateUser);
      expect(button.at(0).props().isDisabled).to.equal(true);
      expect(button.at(0).props().disabledReason).to.equal(undefined);
    });

    it('Should be disabled when user is a system user', () => {
      const wrapper = createTestComponent({ isUserSystem: true });
      const button = wrapper.find(UserDetailsActionsImpersonateUser);
      expect(button.at(0).props().isDisabled).to.equal(true);
      expect(button.at(0).props().disabledReason).to.equal('You do not have enough privileges to log in as this user.');
    });

    it('Should be disabled when user is a system administrator', () => {
      const wrapper = createTestComponent({ isUserSystemAdmin: true });
      const button = wrapper.find(UserDetailsActionsImpersonateUser);
      expect(button.at(0).props().isDisabled).to.equal(true);
      expect(button.at(0).props().disabledReason).to.equal('You do not have enough privileges to log in as this user.');
    });

    it('Should be disabled when user is a site administrator', () => {
      const wrapper = createTestComponent({ isUserSiteAdmin: true });
      const button = wrapper.find(UserDetailsActionsImpersonateUser);
      expect(button.at(0).props().isDisabled).to.equal(true);
      expect(button.at(0).props().disabledReason).to.equal('You do not have enough privileges to log in as this user.');
    });

    it('Should be disabled when user is themselves', () => {
      const wrapper = createTestComponent({ isCurrentUser: true });
      const button = wrapper.find(UserDetailsActionsImpersonateUser);
      expect(button.at(0).props().isDisabled).to.equal(true);
      expect(button.at(0).props().disabledReason).to.equal('You are already logged in as yourself.');
    });

    it('Should be disabled when user is blocked', () => {
      const wrapper = createTestComponent({ isUserBlocked: true });
      const button = wrapper.find(UserDetailsActionsImpersonateUser);
      expect(button.at(0).props().isDisabled).to.equal(true);
      expect(button.at(0).props().disabledReason).to.equal('You cannot log in as a user that is managed elsewhere.');
    });

    it('Should be disabled when user has no presence', () => {
      const wrapper = createTestComponent({ isUserWithNoPresence: true });
      const button = wrapper.find(UserDetailsActionsImpersonateUser);
      expect(button.at(0).props().isDisabled).to.equal(true);
      expect(button.at(0).props().disabledReason).to.equal('You cannot log in as a user that has never accessed this site.');
    });

    it('Should be enabled', () => {
      const wrapper = createTestComponent({});
      const button = wrapper.find(UserDetailsActionsImpersonateUser);
      expect(button.at(0).props().isDisabled).to.equal(false);
    });
  });

  describe('Prompt reset password button', () => {

    it('Should exist', () => {
      const wrapper = createTestComponent({});
      const button = wrapper.find<any>(UserDetailsActionsPromptResetPassword);
      expect(button.at(0).props().isDisabled).to.equal(false);
    });

    it('Should be disabled when it is loading', () => {
      const wrapper = createTestComponent({ isLoading: true });
      const button = wrapper.find<any>(UserDetailsActionsPromptResetPassword);
      expect(button.at(0).props().isDisabled).to.equal(true);
      expect(button.at(0).props().disabledReason).to.equal(undefined);
    });

    it('Should be disabled when it is errored', () => {
      const wrapper = createTestComponent({ isError: true });
      const button = wrapper.find<any>(UserDetailsActionsPromptResetPassword);
      expect(button.at(0).props().isDisabled).to.equal(true);
      expect(button.at(0).props().disabledReason).to.equal(undefined);
    });

    it('Should be disabled when user is a system user', () => {
      const wrapper = createTestComponent({ isUserSystem: true });
      const button = wrapper.find<any>(UserDetailsActionsPromptResetPassword);
      expect(button.at(0).props().isDisabled).to.equal(true);
      expect(button.at(0).props().disabledReason).to.equal('You do not have enough privileges to prompt this user to reset their password.');
    });

    it('Should be disabled when user is a system administrator', () => {
      const wrapper = createTestComponent({ isUserSystemAdmin: true });
      const button = wrapper.find<any>(UserDetailsActionsPromptResetPassword);
      expect(button.at(0).props().isDisabled).to.equal(true);
      expect(button.at(0).props().disabledReason).to.equal('You do not have enough privileges to prompt this user to reset their password.');
    });

    it('Should be disabled when user is a site administrator', () => {
      const wrapper = createTestComponent({ isUserSiteAdmin: true });
      const button = wrapper.find<any>(UserDetailsActionsPromptResetPassword);
      expect(button.at(0).props().isDisabled).to.equal(true);
      expect(button.at(0).props().disabledReason).to.equal('You do not have enough privileges to prompt this user to reset their password.');
    });

    it('Should be disabled when user is themselves', () => {
      const wrapper = createTestComponent({ isCurrentUser: true });
      const button = wrapper.find<any>(UserDetailsActionsPromptResetPassword);
      expect(button.at(0).props().isDisabled).to.equal(true);
      expect(button.at(0).props().disabledReason).to.equal('You can reset your password through the Atlassian Account profile.');
    });

    it('Should be disabled when invited', () => {
      const wrapper = createTestComponent({ isUserWithNoPresence: true });
      const button = wrapper.find(UserDetailsActionsPromptResetPassword);
      expect(button.at(0).props().isDisabled).to.equal(true);
      expect(button.at(0).props().disabledReason).to.equal('This user must accept your invite to access this site before you can prompt them to reset their password.');
    });

    it('Should be enabled', () => {
      const wrapper = createTestComponent({});
      const button = wrapper.find<any>(UserDetailsActionsPromptResetPassword);
      expect(button.at(0).props().isDisabled).to.equal(false);
    });
  });
});
