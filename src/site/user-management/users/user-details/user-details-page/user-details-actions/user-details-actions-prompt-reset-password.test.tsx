import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import { ButtonHover } from 'common/button-hover/button-hover';

import { createMockAnalyticsClient, createMockIntlProp, waitUntil } from '../../../../../../utilities/testing';
import { UserDetailsActionsPromptResetPasswordImpl } from './user-details-actions-prompt-reset-password';

describe('User details page - Prompting reset password button', () => {

  const sandbox = sinon.sandbox.create();

  let showFlagSpy;
  let hideFlagSpy;
  let mutateSpy;

  beforeEach(() => {
    showFlagSpy = sandbox.spy();
    hideFlagSpy = sandbox.spy();
    mutateSpy = sandbox.stub().resolves();
  });

  afterEach(() => {
    sandbox.restore();
  });

  function createTestComponent(
    { isDisabled = false }:
    { isDisabled: boolean},
  ): ShallowWrapper {
    return shallow(
      <UserDetailsActionsPromptResetPasswordImpl
        intl={createMockIntlProp()}
        userStates={[]}
        analyticsClient={createMockAnalyticsClient()}
        cloudId="dummy-cloud-id"
        userId="dummy-user-id"
        isDisabled={isDisabled}
        showFlag={showFlagSpy}
        hideFlag={hideFlagSpy}
        mutate={mutateSpy}
      />,
    );
  }

  it('Should be disabled', () => {
    const wrapper = createTestComponent({ isDisabled: true });
    const button = wrapper.find(ButtonHover);
    expect(button.exists()).to.equal(true);
    expect(button.props().isDisabled).to.equal(true);
  });

  it('Should be enabled', () => {
    const wrapper = createTestComponent({ isDisabled: false });
    const button = wrapper.find(ButtonHover);
    expect(button.exists()).to.equal(true);
    expect(button.props().isDisabled).to.equal(false);
  });

  it('Should call the correct mutation when the button is clicked', async () => {
    const wrapper = createTestComponent({ isDisabled: false });
    const button = wrapper.find(ButtonHover);
    expect(button.exists()).to.equal(true);
    button.simulate('click');
    expect(mutateSpy.called).to.equal(true);
    expect(mutateSpy.calledWith({
      variables: {
        cloudId: 'dummy-cloud-id',
        userId: 'dummy-user-id',
      },
    })).to.equal(true);
  });

  it('Should display a success flag and disable button when prompting to reset password is successful', async () => {
    mutateSpy = sandbox.stub().resolves();
    const wrapper = createTestComponent({ isDisabled: false });
    const button = wrapper.find(ButtonHover);
    button.simulate('click');
    expect(wrapper.find(ButtonHover).props().isLoading).to.equal(true);
    expect(mutateSpy.called).to.equal(true);
    await waitUntil(() => showFlagSpy.callCount > 0);
    expect(showFlagSpy.calledWithMatch({ title: 'Done!' })).to.equal(true);
    expect(wrapper.find(ButtonHover).props().isDisabled).to.equal(true);
    expect(wrapper.find(ButtonHover).props().isLoading).to.equal(false);
  });

  it('Should display an error flag and still keep button enabled when prompting to reset password is not successful', async () => {
    mutateSpy = sandbox.stub().rejects();
    const wrapper = createTestComponent({ isDisabled: false });
    const button = wrapper.find(ButtonHover);
    button.simulate('click');
    expect(wrapper.find(ButtonHover).props().isLoading).to.equal(true);
    expect(mutateSpy.called).to.equal(true);
    await waitUntil(() => showFlagSpy.callCount > 0);
    expect(showFlagSpy.calledWithMatch({ title: 'Something went wrong' })).to.equal(true);
    expect(wrapper.find(ButtonHover).props().isDisabled).to.equal(false);
    expect(wrapper.find(ButtonHover).props().isLoading).to.equal(false);
  });
});
