import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import { AnalyticsProps } from 'src/site/user-management/user-management-analytics';

import { ButtonHover } from 'common/button-hover/button-hover';
import { createErrorIcon, createSuccessIcon, errorFlagMessages } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';

import { AnalyticsClientProps, resetPasswordUIEventData, resetUserPasswordTrackEventData, withAnalyticsClient } from 'common/analytics';

import { UserDetailsPromptResetPasswordMutation, UserDetailsPromptResetPasswordMutationVariables } from '../../../../../../schema/schema-types';
import promptResetPasswordMutation from './user-details-prompt-reset-password.mutation.graphql';

const messages = defineMessages({
  actionResetPassword: {
    id: 'user.details.page.reset.password.button.text',
    description: 'Button text which opens a modal to prompt a user to reset their password',
    defaultMessage: 'Prompt reset password',
  },
  successTitle: {
    id: 'user.details.page.reset.password.success.flag.title',
    description: 'Success flag title indicating that an email was sent to the user to reset their password',
    defaultMessage: 'Done!',
  },
  successDescription: {
    id: 'user.details.page.reset.password.success.flag.description',
    description: 'Success flag description indicating that an email was sent to the user to reset their password',
    defaultMessage: 'We\'ve sent an email to this user to change their password.',
  },
  errorDescription: {
    id: 'user.details.page.reset.password.error.flag.description',
    description: 'Error flag description indicating that an email was not sent to the user to reset their password',
    defaultMessage: `We couldn\'t prompt this user to reset their password. Refresh the page
    to try again.`,
  },
});

interface OwnProps {
  cloudId: string;
  userId: string;
  isDisabled: boolean;
  disabledReason?: React.ReactNode;
}

interface State {
  isAdminCurrentlyPrompting: boolean;
  hasAdminAlreadyPrompted: boolean;
}

export class UserDetailsActionsPromptResetPasswordImpl extends React.Component<ChildProps<AnalyticsProps & OwnProps & InjectedIntlProps & FlagProps & AnalyticsClientProps, UserDetailsPromptResetPasswordMutation>, State> {

  public readonly state: Readonly<State> = {
    isAdminCurrentlyPrompting: false,
    hasAdminAlreadyPrompted: false,
  };

  public render() {
    return (
      <ButtonHover
        isDisabled={this.props.isDisabled || this.state.hasAdminAlreadyPrompted}
        onClick={this.promptResetPassword}
        isLoading={this.state.isAdminCurrentlyPrompting}
        dialogMessage={this.props.disabledReason}
      >
        {this.props.intl.formatMessage(messages.actionResetPassword)}
      </ButtonHover>
    );
  }

  private sendResetUserPasswordTrackEventAnalytics = () => {
    this.props.analyticsClient.sendTrackEvent({
      cloudId: this.props.cloudId,
      data: resetUserPasswordTrackEventData({
        userState: this.props.userStates,
        userId: this.props.userId,
      }),
    });
  }

  private sendResetUserPasswordUIEventAnalytics = () => {
    this.props.analyticsClient.sendUIEvent({
      cloudId: this.props.cloudId,
      data: resetPasswordUIEventData({
        userState: this.props.userStates,
        userId: this.props.userId,
      }),
    });
  }

  private promptResetPassword = (): void => {

    this.sendResetUserPasswordUIEventAnalytics();

    if (!this.props.mutate) {
      return;
    }

    this.setState({ isAdminCurrentlyPrompting: true });

    const variables: UserDetailsPromptResetPasswordMutationVariables = {
      cloudId: this.props.cloudId,
      userId: this.props.userId,
    };

    this.props.mutate({
      variables,
    }).then(() => {
      this.props.showFlag({
        autoDismiss: true,
        icon: createSuccessIcon(),
        id: `prompt.reset.password.success.flag.${(Date.now().valueOf())}`,
        title: this.props.intl.formatMessage(messages.successTitle),
        description: this.props.intl.formatMessage(messages.successDescription),
      });
      this.sendResetUserPasswordTrackEventAnalytics();
      this.setState({ hasAdminAlreadyPrompted: true });
      this.setState({ isAdminCurrentlyPrompting: false });
    }).catch(() => {
      this.props.showFlag({
        autoDismiss: true,
        icon: createErrorIcon(),
        id: `prompt.reset.password.error.flag.${(Date.now().valueOf())}`,
        title: this.props.intl.formatMessage(errorFlagMessages.title),
        description: this.props.intl.formatMessage(messages.errorDescription),
      });
      this.setState({ isAdminCurrentlyPrompting: false });
    });
  }
}

const withPromptResetPasswordMutation = graphql<AnalyticsProps & OwnProps & InjectedIntlProps & FlagProps & AnalyticsClientProps, UserDetailsPromptResetPasswordMutation>(promptResetPasswordMutation);
export const UserDetailsActionsPromptResetPassword = withAnalyticsClient(
  injectIntl(
    withFlag(
      withPromptResetPasswordMutation(
        UserDetailsActionsPromptResetPasswordImpl,
      ),
    ),
  ),
);
