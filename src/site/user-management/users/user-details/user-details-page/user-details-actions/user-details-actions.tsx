import * as H from 'history';
import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import { ButtonGroup as AkButtonGroup } from '@atlaskit/button';

import { FlagProps, withFlag } from 'common/flag';

import { AnalyticsProps } from '../../../../user-management-analytics';

import { util } from '../../../../../../utilities/admin-hub';
import { UserDetailsActionsImpersonateUser } from './user-details-actions-impersonate-user';
import { UserDetailsActionsPromptResetPassword } from './user-details-actions-prompt-reset-password';
import { UserDetailsActionsRemoveUser } from './user-details-actions-remove-user-modal';

interface OwnProps {
  cloudId: string;
  userId: string;
  displayName?: string;
  isLoading: boolean;
  isError: boolean;
  isCurrentUser: boolean;
  isUserSystem: boolean;
  isUserSystemAdmin: boolean;
  isUserSiteAdmin: boolean;
  isUserBlocked: boolean;
  isUserWithNoPresence: boolean;
  isUserWithNoAccess: boolean;
  history: H.History;
}

const messages = defineMessages({
  noPrivileges: {
    id: 'user.details.page.disabled.button.text.remove.user.insufficient.privileges',
    description: 'Text which appears over a disabled button indicating that they cannot remove a user',
    defaultMessage: 'You do not have enough privileges to remove this user from the site.',
  },
  resetPasswordNoPrivileges: {
    id: 'user.details.page.disabled.button.text.reset.user.password.insufficient.privileges',
    description: 'Text which appears over a disabled button indicating that they cannot prompt to reset a user\'s password',
    defaultMessage: 'You do not have enough privileges to prompt this user to reset their password.',
  },
  resetPasswordCurrentUser: {
    id: 'user.details.page.disabled.button.text.reset.user.password.current.user',
    description: 'Text which appears over a disabled button indicating that they can reset their own password through the Aa profile.',
    defaultMessage: 'You can reset your password through the Atlassian Account profile.',
  },
  resetPasswordInvited: {
    id: 'user.details.page.disabled.button.text.reset.user.password.invited.user',
    description: 'Text which appears over a disabled button indicating that the user must accept their invite before we can prompt them to reset their password',
    defaultMessage: 'This user must accept your invite to access this site before you can prompt them to reset their password.',
  },
  impersonateNoAccess: {
    id: 'user.details.page.disabled.button.text.impersonate.user.insufficient.privileges',
    description: 'Text which appears over a disabled button indicating that they cannot impersonate a user',
    defaultMessage: 'You cannot log in as a user that does not have access to this site.',
  },
  impersonateCurrentUser: {
    id: 'user.details.page.disabled.button.text.impersonate.user.is.current',
    description: 'Text which appears over a disabled button indicating that they cannot impersonate a user because it is themselves',
    defaultMessage: 'You are already logged in as yourself.',
  },
  impersonateSiteAdmin: {
    id: 'user.details.page.disabled.button.text.impersonate.user.site.admin',
    description: 'Text which appears over a disabled button indicating that they cannot impersonate a user',
    defaultMessage: 'You do not have enough privileges to log in as this user.',
  },
  impersonateNoPresence: {
    id: 'user.details.page.disabled.button.text.impersonate.user.no.presence',
    description: 'Text which appears over a disabled button indicating that they cannot impersonate a user',
    defaultMessage: 'You cannot log in as a user that has never accessed this site.',
  },
  impersonateBlocked: {
    id: 'user.details.page.disabled.button.text.impersonate.user.blocked',
    description: 'Text which appears over a disabled button indicating that they cannot impersonate a user',
    defaultMessage: 'You cannot log in as a user that is managed elsewhere.',
  },
});

export class UserDetailsActionsImpl extends React.Component<OwnProps & AnalyticsProps & FlagProps & InjectedIntlProps> {

  public render() {
    const { isUserSystem, isUserSystemAdmin, isUserSiteAdmin, isCurrentUser, isLoading, isError, isUserBlocked, isUserWithNoAccess, isUserWithNoPresence } = this.props;

    const isDisabledForAllCases = isLoading || isError || isUserSystem || isUserSystemAdmin || isUserSiteAdmin || isCurrentUser;

    const isImpersonateUserDisabled = isDisabledForAllCases || isUserBlocked || isUserWithNoPresence || isUserWithNoAccess;
    const isPromptResetPasswordDisabled = isDisabledForAllCases || isUserBlocked || isUserWithNoPresence;
    const isRemoveUserDisabled = isDisabledForAllCases;

    return (
      <React.Fragment>
        <AkButtonGroup>
          <UserDetailsActionsPromptResetPassword
            cloudId={this.props.cloudId}
            userId={this.props.userId}
            isDisabled={isPromptResetPasswordDisabled}
            disabledReason={this.getPromptResetPasswordDisabledReason()}
            userStates={this.props.userStates}
          />
          <UserDetailsActionsImpersonateUser
            cloudId={this.props.cloudId}
            userId={this.props.userId}
            displayName={this.props.displayName}
            isDisabled={isImpersonateUserDisabled}
            redirect={this.redirectToHome}
            disabledReason={this.getImpersonateDisabledReason()}
          />
          <UserDetailsActionsRemoveUser
            cloudId={this.props.cloudId}
            userId={this.props.userId}
            isDisabled={isRemoveUserDisabled}
            disabledReason={this.getRemoveFromSiteDisabledReason()}
            redirect={this.redirectToUsersPage}
            userStates={this.props.userStates}
          />
        </AkButtonGroup>
      </React.Fragment>
    );
  }

  private getImpersonateDisabledReason = (): React.ReactNode => {

    const { formatMessage } = this.props.intl;

    if (this.props.isCurrentUser) {
      return formatMessage(messages.impersonateCurrentUser);
    } else if (this.props.isUserSystem || this.props.isUserSiteAdmin || this.props.isUserSystemAdmin) {
      return formatMessage(messages.impersonateSiteAdmin);
    } else if (this.props.isUserBlocked) {
      return formatMessage(messages.impersonateBlocked);
    } else if (this.props.isUserWithNoAccess) {
      return formatMessage(messages.impersonateNoAccess);
    } else if (this.props.isUserWithNoPresence) {
      return formatMessage(messages.impersonateNoPresence);
    }

    return undefined;
  }

  private getPromptResetPasswordDisabledReason = (): React.ReactNode => {

    const { formatMessage } = this.props.intl;

    if (this.props.isUserSiteAdmin || this.props.isUserSystem || this.props.isUserSystemAdmin || this.props.isUserBlocked) {
      return formatMessage(messages.resetPasswordNoPrivileges);
    } else if (this.props.isCurrentUser) {
      return formatMessage(messages.resetPasswordCurrentUser);
    } else if (this.props.isUserWithNoPresence) {
      return formatMessage(messages.resetPasswordInvited);
    }

    return undefined;
  }

  private getRemoveFromSiteDisabledReason = (): React.ReactNode => {

    const { formatMessage } = this.props.intl;

    if (this.props.isUserSystemAdmin || this.props.isUserSystem) {
      return formatMessage(messages.noPrivileges);
    }

    return undefined;
  }

  private redirectToHome = (): void => {
    // This needs to change to a non relative path, and gain a test, once we migrate to admin.atlassian.com
    // https://hello.atlassian.net/browse/ADMPF-868
    window.location.href = '/';
  }

  private redirectToUsersPage = (): void => {
    this.props.history.push(`${util.siteAdminBasePath}/s/${this.props.cloudId}/users`);
  }
}

export const UserDetailsActions = withFlag(injectIntl(UserDetailsActionsImpl));
