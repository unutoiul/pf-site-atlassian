
import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import AkButton from '@atlaskit/button';

import { cancelRemoveUserFromSiteUIEventData, confimRemoveUserFromSiteUIEventData, removedUserFromSiteTrackEventData, removeUserFromSiteUIEventData } from 'common/analytics';
import { ButtonHover } from 'common/button-hover/button-hover';

import { ModalDialog } from 'common/modal';

import { createMockIntlProp, waitUntil } from '../../../../../../utilities/testing';
import { UserDetailsActionsRemoveUserImpl } from './user-details-actions-remove-user-modal';

describe('User details page - Action buttons', () => {

  const sandbox = sinon.sandbox.create();

  let redirectSpy;
  let showFlagSpy;
  let hideFlagSpy;
  let mutateSpy;
  let analyticsStub;

  beforeEach(() => {
    showFlagSpy = sandbox.spy();
    hideFlagSpy = sandbox.spy();
    redirectSpy = sandbox.spy();
    analyticsStub = {
      sendTrackEvent: sandbox.spy(),
      sendUIEvent: sandbox.spy(),
    };
  });

  afterEach(() => {
    sandbox.restore();
  });

  function createTestComponent(): ShallowWrapper {
    return shallow(
      <UserDetailsActionsRemoveUserImpl
        intl={createMockIntlProp()}
        cloudId={'dummy-cloud-id'}
        userId={'dummy-user-id'}
        isDisabled={false}
        showFlag={showFlagSpy}
        hideFlag={hideFlagSpy}
        redirect={redirectSpy}
        mutate={mutateSpy}
        userStates={['grantedAccessToSite', 'invited']}
        analyticsClient={analyticsStub}
      />,
    );
  }

  describe('Remove user from site button', () => {

    it('Should open the remove user from site modal when clicked', () => {
      const wrapper = createTestComponent();
      const button = wrapper.find(ButtonHover);
      expect(wrapper.find(ModalDialog).prop('isOpen')).to.equal(false);
      button.simulate('click');
      expect(wrapper.find(ModalDialog).prop('isOpen')).to.equal(true);
    });

    it('Should fire the removeUserFromSiteUIEventData analytic event', () => {
      const wrapper = createTestComponent();
      const button = wrapper.find(ButtonHover);
      expect(analyticsStub.sendUIEvent.called).to.equal(false);
      button.simulate('click');
      expect(analyticsStub.sendUIEvent.called).to.equal(true);
      expect(analyticsStub.sendUIEvent.calledWith({
        cloudId: 'dummy-cloud-id',
        data: removeUserFromSiteUIEventData({
          userState: ['grantedAccessToSite', 'invited'],
          userId: 'dummy-user-id',
        }),
      })).to.equal(true);
    });
  });

  describe('Remove user from site modal', () => {

    describe('when cancelling the modal', () => {

      it('should fire the cancelRemoveUserFromSiteUIEventData analytic event', async () => {
        mutateSpy = sandbox.stub().resolves();
        const wrapper = createTestComponent();
        const footer = shallow(wrapper.find(ModalDialog).prop('footer') as React.ReactElement<any>);
        const buttons = footer.find(AkButton);
        const button = buttons.filterWhere(btn => btn.children().text() === 'Cancel');
        expect(mutateSpy.called).to.equal(false);
        button.simulate('click');
        expect(analyticsStub.sendUIEvent.called).to.equal(true);
        expect(analyticsStub.sendUIEvent.calledWith({
          cloudId: 'dummy-cloud-id',
          data: cancelRemoveUserFromSiteUIEventData({
            userState: ['grantedAccessToSite', 'invited'],
            userId: 'dummy-user-id',
          }),
        })).to.equal(true);
      });

    });

    describe('On successful removal', () => {

      it('Should redirect', async () => {
        mutateSpy = sandbox.stub().resolves();
        const wrapper = createTestComponent();
        const footer = shallow(wrapper.find(ModalDialog).prop('footer') as React.ReactElement<any>);
        const buttons = footer.find(AkButton);
        const button = buttons.filterWhere(btn => btn.children().text() === 'Remove user');
        expect(mutateSpy.called).to.equal(false);
        button.simulate('click');
        await waitUntil(() => redirectSpy.callCount > 0);
      });

      it('Should trigger a mutation when a button is clicked', () => {
        mutateSpy = sandbox.stub().resolves();
        const wrapper = createTestComponent();
        const footer = shallow(wrapper.find(ModalDialog).prop('footer') as React.ReactElement<any>);
        const buttons = footer.find(AkButton);
        const button = buttons.filterWhere(btn => btn.children().text() === 'Remove user');
        expect(mutateSpy.called).to.equal(false);
        button.simulate('click');
        expect(mutateSpy.called).to.equal(true);
        expect(mutateSpy.calledWith({
          variables: {
            cloudId: 'dummy-cloud-id',
            id: 'dummy-user-id',
          },
        })).to.equal(true);
      });

      it('Should fire the removedUserFromSiteTrackEventData analytic event', async () => {
        mutateSpy = sandbox.stub().resolves();
        const wrapper = createTestComponent();
        const footer = shallow(wrapper.find(ModalDialog).prop('footer') as React.ReactElement<any>);
        const buttons = footer.find(AkButton);
        const button = buttons.filterWhere(btn => btn.children().text() === 'Remove user');
        expect(analyticsStub.sendTrackEvent.called).to.equal(false);
        button.simulate('click');
        await waitUntil(() => showFlagSpy.callCount > 0);
        expect(analyticsStub.sendTrackEvent.called).to.equal(true);
        expect(analyticsStub.sendTrackEvent.calledWith({
          cloudId: 'dummy-cloud-id',
          data: removedUserFromSiteTrackEventData({
            userState: ['grantedAccessToSite', 'invited'],
            userId: 'dummy-user-id',
          }),
        })).to.equal(true);
      });

      it('Should fire the confimRemoveUserFromSiteUIEventData analytic event', async () => {
        mutateSpy = sandbox.stub().resolves();
        const wrapper = createTestComponent();
        const footer = shallow(wrapper.find(ModalDialog).prop('footer') as React.ReactElement<any>);
        const buttons = footer.find(AkButton);
        const button = buttons.filterWhere(btn => btn.children().text() === 'Remove user');
        expect(analyticsStub.sendUIEvent.called).to.equal(false);
        button.simulate('click');
        await waitUntil(() => showFlagSpy.callCount > 0);
        expect(analyticsStub.sendUIEvent.called).to.equal(true);
        expect(analyticsStub.sendUIEvent.calledWith({
          cloudId: 'dummy-cloud-id',
          data: confimRemoveUserFromSiteUIEventData({
            userState: ['grantedAccessToSite', 'invited'],
            userId: 'dummy-user-id',
          }),
        })).to.equal(true);
      });

      it('Should display a success flag when removing the user from the site is successful', async () => {
        mutateSpy = sandbox.stub().resolves();
        const wrapper = createTestComponent();
        const footer = shallow(wrapper.find(ModalDialog).prop('footer') as React.ReactElement<any>);
        const buttons = footer.find(AkButton);
        const button = buttons.filterWhere(btn => btn.children().text() === 'Remove user');
        button.simulate('click');
        expect(mutateSpy.called).to.equal(true);
        await waitUntil(() => showFlagSpy.callCount > 0);
        expect(showFlagSpy.calledWithMatch({ title: 'The user has been removed from the site' })).to.equal(true);
      });
    });

    describe('On unsuccessful removal', () => {

      it('Should not redirect', () => {
        mutateSpy = sandbox.stub().rejects();
        const wrapper = createTestComponent();
        const footer = shallow(wrapper.find(ModalDialog).prop('footer') as React.ReactElement<any>);
        const buttons = footer.find(AkButton);
        const button = buttons.filterWhere(btn => btn.children().text() === 'Remove user');
        expect(mutateSpy.called).to.equal(false);
        button.simulate('click');
        expect(redirectSpy.called).to.equal(false);
      });

      it('Should still fire the confimRemoveUserFromSiteUIEventData analytic event', async () => {
        mutateSpy = sandbox.stub().rejects();
        const wrapper = createTestComponent();
        const footer = shallow(wrapper.find(ModalDialog).prop('footer') as React.ReactElement<any>);
        const buttons = footer.find(AkButton);
        const button = buttons.filterWhere(btn => btn.children().text() === 'Remove user');
        expect(analyticsStub.sendUIEvent.called).to.equal(false);
        button.simulate('click');
        await waitUntil(() => showFlagSpy.callCount > 0);
        expect(analyticsStub.sendUIEvent.called).to.equal(true);
        expect(analyticsStub.sendUIEvent.calledWith({
          cloudId: 'dummy-cloud-id',
          data: confimRemoveUserFromSiteUIEventData({
            userState: ['grantedAccessToSite', 'invited'],
            userId: 'dummy-user-id',
          }),
        })).to.equal(true);
      });

      it('Should display an error flag when removing the user is not successful', async () => {
        mutateSpy = sandbox.stub().rejects();
        const wrapper = createTestComponent();
        const footer = shallow(wrapper.find(ModalDialog).prop('footer') as React.ReactElement<any>);
        const buttons = footer.find(AkButton);
        const button = buttons.filterWhere(btn => btn.children().text() === 'Remove user');
        button.simulate('click');
        expect(mutateSpy.called).to.equal(true);
        await waitUntil(() => showFlagSpy.callCount > 0);
        expect(showFlagSpy.calledWithMatch({ title: 'We couldn\'t remove the user from the site' })).to.equal(true);
      });

      it('Should not fire the removedUserFromSiteTrackEventData analytic', async () => {
        mutateSpy = sandbox.stub().rejects();
        const wrapper = createTestComponent();
        const footer = shallow(wrapper.find(ModalDialog).prop('footer') as React.ReactElement<any>);
        const buttons = footer.find(AkButton);
        const button = buttons.filterWhere(btn => btn.children().text() === 'Remove user');
        expect(analyticsStub.sendTrackEvent.called).to.equal(false);
        button.simulate('click');
        await waitUntil(() => showFlagSpy.callCount > 0);
        expect(analyticsStub.sendTrackEvent.called).to.equal(false);
      });

    });
  });
});
