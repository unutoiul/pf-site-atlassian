import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import AkButton, { ButtonGroup as AkButtonGroup } from '@atlaskit/button';

import { AnalyticsClientProps, ScreenEventSender, withAnalyticsClient } from 'common/analytics';
import { cancelRemoveUserFromSiteUIEventData, confimRemoveUserFromSiteUIEventData, removedUserFromSiteTrackEventData, removeUserFromSiteModalScreenEvent, removeUserFromSiteUIEventData } from 'common/analytics/analytics-event-data';
import { ButtonHover } from 'common/button-hover/button-hover';
import { createErrorIcon, createSuccessIcon } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';
import { ModalDialog } from 'common/modal';

import { RemoveUserFromSiteMutation, RemoveUserFromSiteMutationVariables } from '../../../../../../schema/schema-types';
import { AnalyticsProps } from '../../../../user-management-analytics';
import { ModalWrapper } from '../user-details.styled';
import removeUserFromSite from './user-details-actions-remove-user.mutation.graphql';

const messages = defineMessages({
  actionRemoveUser: {
    id: 'user.details.page.remove.user.from.site',
    description: 'Button text which opens a modal to remove a user from a site',
    defaultMessage: 'Remove user from site?',
  },
  cancel: {
    id: 'user.details.page.cancel.button',
    description: 'Cancel button on the remove user modal to close the modal',
    defaultMessage: 'Cancel',
  },
  actionSuccintRemoveUser: {
    id: 'user.details.page.remove.user.from.site.button',
    description: 'Final button to click to remove the user from the site',
    defaultMessage: 'Remove user',
  },
  description: {
    id: 'user.details.page.modal.description',
    description: 'Description on the modal to remove the user from the site',
    defaultMessage: `They will no longer have access; and won\'t be able to collaborate with your team. Your products will still keep all of this user\'s contributions.`,
  },
  flagSuccessTitle: {
    id: 'user.details.page.remove.user.flag.success',
    description: 'Success flag which pops up saying that the user has been removed from the site successfully',
    defaultMessage: 'The user has been removed from the site',
  },
  flagErrorTitle: {
    id: 'user.details.page.remove.user.flag.error',
    description: 'Error flag which pops up saying that the user has not been removed from the site',
    defaultMessage: 'We couldn\'t remove the user from the site',
  },
});

interface OwnProps {
  cloudId: string;
  userId: string;
  isDisabled: boolean;
  disabledReason?: React.ReactNode;
  redirect(): void;
}

interface OwnState {
  isRemoveUserDialogOpen: boolean;
}

export class UserDetailsActionsRemoveUserImpl extends React.Component<ChildProps<OwnProps & InjectedIntlProps & FlagProps & AnalyticsProps & AnalyticsClientProps, RemoveUserFromSiteMutation>, OwnState> {

  public readonly state: Readonly<OwnState> = {
    isRemoveUserDialogOpen: false,
  };

  public render() {

    const { formatMessage } = this.props.intl;

    return (
      <ScreenEventSender event={removeUserFromSiteModalScreenEvent()}>
        <ButtonHover
          isDisabled={this.props.isDisabled}
          dialogMessage={this.props.disabledReason}
          onClick={this.openRemoveUserFromSiteDialog}
        >
            {formatMessage(messages.actionSuccintRemoveUser)}
        </ButtonHover>
        <ModalDialog
          header={formatMessage(messages.actionRemoveUser)}
          isOpen={this.state.isRemoveUserDialogOpen}
          width="small"
          onClose={this.closeRemoveUserFromSiteDialog}
          footer={(
            <AkButtonGroup>
              <AkButton
                appearance="danger"
                onClick={this.removeUserFromSite}
              >
                {formatMessage(messages.actionSuccintRemoveUser)}
              </AkButton>
              <AkButton
                appearance="subtle-link"
                onClick={this.closeRemoveUserFromSiteDialog}
              >
                {formatMessage(messages.cancel)}
              </AkButton>
            </AkButtonGroup>
          )}
        >
          <ModalWrapper>
            <p>{formatMessage(messages.description)}</p>
          </ModalWrapper>
        </ModalDialog>
      </ScreenEventSender>
    );
  }

  private openRemoveUserFromSiteDialog = (): void => {
    this.sendRemoveUserFromSiteUIEventAnalytics();
    this.setState({ isRemoveUserDialogOpen: true });
  }

  private closeRemoveUserFromSiteDialog = (): void => {
    this.sendCancelRemoveUserFromSiteUIEventAnalytics();
    this.setState({ isRemoveUserDialogOpen: false });
  }

  private sendRemoveUserFromSiteUIEventAnalytics = () => {
    this.props.analyticsClient.sendUIEvent({
      cloudId: this.props.cloudId,
      data: removeUserFromSiteUIEventData({
        userState: this.props.userStates,
        userId: this.props.userId,
      }),
    });
  }

  private sendCancelRemoveUserFromSiteUIEventAnalytics = () => {
    this.props.analyticsClient.sendUIEvent({
      cloudId: this.props.cloudId,
      data: cancelRemoveUserFromSiteUIEventData({
        userState: this.props.userStates,
        userId: this.props.userId,
      }),
    });
  }

  private sendConfirmRemoveUserFromSiteUIEventAnalytics = () => {
    this.props.analyticsClient.sendUIEvent({
      cloudId: this.props.cloudId,
      data: confimRemoveUserFromSiteUIEventData({
        userState: this.props.userStates,
        userId: this.props.userId,
      }),
    });
  }

  private sendConfirmRemoveUserFromSiteTrackEventAnalytics = () => {
    this.props.analyticsClient.sendTrackEvent({
      cloudId: this.props.cloudId,
      data: removedUserFromSiteTrackEventData({
        userState: this.props.userStates,
        userId: this.props.userId,
      }),
    });
  }

  private removeUserFromSite = (): void => {

    this.sendConfirmRemoveUserFromSiteUIEventAnalytics();

    const variables: RemoveUserFromSiteMutationVariables = {
      cloudId: this.props.cloudId,
      id: this.props.userId,
    };

    this.props.mutate!({
      variables,
    }).then(() => {
      this.closeRemoveUserFromSiteDialog();
      this.props.showFlag({
        autoDismiss: true,
        icon: createSuccessIcon(),
        id: `remove.user.success.flag.${(Date.now().valueOf())}`,
        title: this.props.intl.formatMessage(messages.flagSuccessTitle),
      });
      this.props.redirect();
      this.sendConfirmRemoveUserFromSiteTrackEventAnalytics();
    }).catch(() => {
      this.closeRemoveUserFromSiteDialog();
      this.props.showFlag({
        autoDismiss: true,
        icon: createErrorIcon(),
        id: `remove.user.error.flag.${(Date.now().valueOf())}`,
        title: this.props.intl.formatMessage(messages.flagErrorTitle),
      });
    });
  }
}

const withRemoveUserFromSite = graphql<OwnProps & InjectedIntlProps & FlagProps & AnalyticsProps & AnalyticsClientProps, RemoveUserFromSiteMutation>(removeUserFromSite);
export const UserDetailsActionsRemoveUser = withAnalyticsClient(withFlag(injectIntl(withRemoveUserFromSite(UserDetailsActionsRemoveUserImpl))));
