import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import { ButtonHover } from 'common/button-hover/button-hover';
import { createErrorIcon, createSuccessIcon, errorFlagMessages } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';

import { AnalyticsClientProps, impersonatedUserTrackEventData, withAnalyticsClient } from 'common/analytics';

import { AnalyticsProps } from '../../../../user-management-analytics';

import { UserDetailsImpersonateUserMutation, UserDetailsImpersonateUserMutationVariables } from '../../../../../../schema/schema-types';
import impersonateUserMutation from './user-details-actions-impersonate-user.mutation.graphql';

const messages = defineMessages({
  actionImpersonateUser: {
    id: 'user.details.page.impersonate.button.text',
    description: 'Button text which allows an admin to impersonate a user in Jira or Confluence',
    defaultMessage: 'Log in as user',
  },
  successTitle: {
    id: 'user.details.page.impersonate.success.flag.title',
    description: 'Success flag title indicating that the admin is about to impersonate a user',
    defaultMessage: 'Log in as user',
  },
  successDescription: {
    id: 'user.details.page.impersonate.success.flag.description',
    description: 'Success flag description indicating that the admin is about to impersonate a user',
    defaultMessage: 'You are now being logged in as {displayName}...',
  },
  displayNamePlaceholder: {
    id: 'user.details.page.impersonate.success.flag.display.name.placeholder',
    description: `To be used in "You are now being logged in as the user"`,
    defaultMessage: 'the user',
  },
  errorDescription: {
    id: 'user.details.page.impersonate.error.flag.description',
    description: 'Error flag description indicating that the admin is about to impersonate a user',
    defaultMessage: `An error occurred while trying to log in as user. Refresh the page to try again.`,
  },
});

interface OwnProps {
  cloudId: string;
  userId: string;
  displayName?: string;
  isDisabled: boolean;
  disabledReason?: React.ReactNode;
  redirect(): void;
}

export class UserDetailsActionsImpersonateUserImpl extends React.Component<ChildProps<AnalyticsProps & OwnProps & InjectedIntlProps & FlagProps & AnalyticsClientProps, UserDetailsImpersonateUserMutation>> {

  public render() {
    return (
      <ButtonHover
        isDisabled={this.props.isDisabled}
        dialogMessage={this.props.disabledReason}
        onClick={this.impersonateUser}
      >
        {this.props.intl.formatMessage(messages.actionImpersonateUser)}
      </ButtonHover>
    );
  }

  private impersonateUser = (): void => {

    if (!this.props.mutate) {
      return;
    }

    const variables: UserDetailsImpersonateUserMutationVariables = {
      cloudId: this.props.cloudId,
      userId: this.props.userId,
    };

    this.props.mutate({
      variables,
    }).then(() => {
      this.props.showFlag({
        autoDismiss: true,
        icon: createSuccessIcon(),
        id: `impersonate.user.success.flag.${(Date.now().valueOf())}`,
        title: this.props.intl.formatMessage(messages.successTitle),
        description: this.props.intl.formatMessage(messages.successDescription,
          { displayName: this.props.displayName ? this.props.displayName :
            this.props.intl.formatMessage(messages.displayNamePlaceholder),
          },
        ),
      });
      this.props.analyticsClient.sendTrackEvent({
        cloudId: this.props.cloudId,
        data: impersonatedUserTrackEventData({
          userState: this.props.userStates,
          userId: this.props.userId,
        }),
      });
      this.props.redirect();
    }).catch(() => {
      this.props.showFlag({
        autoDismiss: true,
        icon: createErrorIcon(),
        id: `impersonate.user.error.flag.${(Date.now().valueOf())}`,
        title: this.props.intl.formatMessage(errorFlagMessages.title),
        description: this.props.intl.formatMessage(messages.errorDescription),
      });
    });
  }
}

const withImpersonateUserMutation = graphql<OwnProps & InjectedIntlProps & FlagProps & AnalyticsClientProps, UserDetailsImpersonateUserMutation>(impersonateUserMutation);
export const UserDetailsActionsImpersonateUser =
  injectIntl(
    withFlag(
      withAnalyticsClient(
        withImpersonateUserMutation(
          UserDetailsActionsImpersonateUserImpl,
      ),
    ),
  ),
);
