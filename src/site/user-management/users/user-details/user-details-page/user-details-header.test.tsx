import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';

import { OscillatingBlock } from 'common/loading';

import { UserDetailsHeaderImpl } from './user-details-header';

describe('User details page layout', () => {

  function createTestComponenet(
    { isLoading = false, displayName }:
    { isLoading?: boolean, displayName?: string },
  ): ShallowWrapper {
    return shallow(
      <UserDetailsHeaderImpl
        isLoading={isLoading}
        displayName={displayName}
      />,
    );
  }

  it('Should render an oscillating block when user details are loading', () => {
    const wrapper = createTestComponenet({ isLoading: true });
    expect(wrapper.find(OscillatingBlock).exists()).to.equal(true);
  });

  it('Should render the User Details text when data is missing', () => {
    const wrapper = createTestComponenet({});
    expect(wrapper.find(FormattedMessage).exists()).to.equal(true);
    expect(wrapper.find(FormattedMessage).props().defaultMessage).to.equal('User Details');
  });

  it('Should render the display name of the user; the user does not have an s at the end of their name', () => {
    const wrapper = createTestComponenet({ displayName: 'Jamie' });
    expect(wrapper.find(FormattedMessage).exists()).to.equal(true);
    expect(wrapper.find(FormattedMessage).props().defaultMessage).to.equal('{name}\'s details');
  });

  it('Should render the display name of the user; the user does have an s at the end of their name', () => {
    const wrapper = createTestComponenet({ displayName: 'James' });
    expect(wrapper.find(FormattedMessage).exists()).to.equal(true);
    expect(wrapper.find(FormattedMessage).props().defaultMessage).to.equal('{name}\' details');
  });
});
