import * as React from 'react';
import { ChildProps, graphql, MutationFunc } from 'react-apollo';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import AkEmptyState from '@atlaskit/empty-state';
import { AkProfilecard } from '@atlaskit/profilecard';
import AkSpinner from '@atlaskit/spinner';

import AkLozenge from '@atlaskit/lozenge';

import { getAvatarUrlByUserId } from 'common/avatar';
import { createErrorIcon, createSuccessIcon } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';
import { errorWindowImage } from 'common/images';

import { UserDetailsAccountSectionQuery, UserDetailsSuggestChangesMutation, UserDetailsSuggestChangesMutationVariables } from '../../../../../schema/schema-types';
import { util } from '../../../../../utilities/admin-hub';
import { Header, LozengeWrapper, SectionTitle } from '../user-details-page/user-details.styled';
import getUserQuery from './user-details-account-section.query.graphql';
import { UserDetailsSuggestChangesInput, UserDetailsSuggestChangesModal } from './user-details-suggest-changes-modal';
import suggestChangesMutation from './user-details-suggest-changes.mutation.graphql';

const messages = defineMessages({
  heading: {
    id: 'user.details.account.section.heading',
    description: 'Heading for account section of user details page',
    defaultMessage: 'Account',
  },
  errorHeader: {
    id: 'user.details.account.section.error.header',
    description: 'Error header shown when we fail to load the account details for the user',
    defaultMessage: 'Failed to load user details',
  },
  errorDescription: {
    id: 'user.details.account.section.error.description',
    description: 'Error message shown when we fail to load the account details for the user',
    defaultMessage: `We had trouble trying to retrieve this user's details, please refresh the page to try again.`,
  },
  blockedStatusLozengeValue: {
    id: 'user.details.account.account.active.status.blocked.lozenge.value',
    description: 'Shown when the user has been deactiveated externally',
    defaultMessage: 'Account disabled',
  },
  suggestChangesAction: {
    id: 'user.details.account.section.suggest.changes.action',
    description: 'Suggest changes button in the profile card',
    defaultMessage: 'Suggest changes',
  },
  suggestChangesSuccessFlag: {
    id: 'user.details.account.section.suggest.changes.success',
    description: 'Text in the pop-up flag on successful action',
    defaultMessage: 'We\'ve emailed the user with your suggested account changes',
  },
  suggestChangesFailureFlag: {
    id: 'user.details.account.section.suggest.changes.failure',
    description: 'Text in the pop-up flag on failed action',
    defaultMessage: 'Something went wrong',
  },
});

interface MutationProps {
  suggestChanges: MutationFunc<UserDetailsSuggestChangesMutation, UserDetailsSuggestChangesMutationVariables>;
}

interface UserDetailsAccountSectionOwnProps {
  cloudId: string;
  userId: string;
}

interface UserDetailsAccountSectionState {
  isSuggestChangesModalOpen: boolean;
}

type UserDetailsAccountSectionProps = ChildProps<MutationProps & UserDetailsAccountSectionOwnProps & InjectedIntlProps & FlagProps, UserDetailsAccountSectionQuery>;

const withUserDetails = graphql<UserDetailsAccountSectionOwnProps & InjectedIntlProps & FlagProps, UserDetailsAccountSectionQuery>(getUserQuery, {
  options: ({ cloudId, userId }) => {

    return {
      variables: {
        cloudId,
        isAdminHub: util.isAdminHub(),
        id: userId,
      },
    };
  },
});

export class UserDetailsAccountSectionImpl extends React.Component<UserDetailsAccountSectionProps, UserDetailsAccountSectionState> {

  public state: UserDetailsAccountSectionState = {
    isSuggestChangesModalOpen: false,
  };

  public render() {

    const { formatMessage } = this.props.intl;

    const isUserDisabledGlobally = this.props.data
      && !this.props.data.loading
      && !this.props.data.error
      && this.props.data.user
      && this.props.data.user.userDetails
      && this.props.data.user.userDetails.activeStatus === 'BLOCKED';

    const defaultHeader = (
      <div>
        <Header>
          <SectionTitle>
            <h2>
              {formatMessage(messages.heading)}
            </h2>
          </SectionTitle>
          {isUserDisabledGlobally && <LozengeWrapper><AkLozenge bold={true}>{formatMessage(messages.blockedStatusLozengeValue)}</AkLozenge></LozengeWrapper>}
        </Header>
      </div>
    );

    if (this.props.data && this.props.data.loading) {
      return (
        <div>
          {defaultHeader}
          <AkSpinner />
        </div>
      );
    }

    if (!this.props.data || this.props.data.error || !this.props.data.user) {
      const emptyStateProps = {
        header: formatMessage(messages.errorHeader),
        description: formatMessage(messages.errorDescription),
        imageUrl: errorWindowImage,
      };

      return (
        <div>
          {defaultHeader}
          <AkEmptyState {...emptyStateProps} />
        </div>
      );
    }

    return (
      <div>
        {defaultHeader}
        <AkProfilecard
          avatarUrl={getAvatarUrlByUserId(this.props.data.user.userDetails.id, 96)}
          fullName={this.props.data.user.userDetails.displayName}
          email={this.props.data.user.userDetails.email}
          meta={this.props.data.user.userDetails.title || ''}
          nickname={this.props.data.user.userDetails.nickname || ''}
          timestring={this.props.data.user.userDetails.timezone || ''}
          location={this.props.data.user.userDetails.location || ''}
          actions={this.getActions()}
        />
        <UserDetailsSuggestChangesModal
          isOpen={this.state.isSuggestChangesModalOpen}
          currentName={this.props.data.user.userDetails.displayName}
          currentEmail={this.props.data.user.userDetails.email}
          dismiss={this.suggestChangesModalDismiss}
          submit={this.onSubmit}
        />
      </div>
    );
  }

  private getActions = () => {
    if (this.hideSuggestChangesButton()) {
      return [];
    }

    const { formatMessage } = this.props.intl;

    return [{
      id: 'suggest-changes',
      label: formatMessage(messages.suggestChangesAction),
      callback: this.suggestChangesModalShow,
    }];
  };

  private onSubmit = async (input: UserDetailsSuggestChangesInput) => {
    this.setState({ isSuggestChangesModalOpen: false });

    const {
      cloudId,
      intl: { formatMessage },
      showFlag,
      suggestChanges,
      userId,
    } = this.props;

    try {
      await suggestChanges({
        variables: {
          cloudId,
          userId,
          email: input.email,
          name: input.name,
        } as UserDetailsSuggestChangesMutationVariables,
      });

      showFlag({
        autoDismiss: true,
        icon: createSuccessIcon(),
        id: `user.details.account.section.success.flag.${(Date.now().valueOf())}`,
        title: formatMessage(messages.suggestChangesSuccessFlag),
      });
    } catch (e) {
      showFlag({
        autoDismiss: true,
        icon: createErrorIcon(),
        id: `user.details.account.section.failure.flag.${(Date.now().valueOf())}`,
        title: formatMessage(messages.suggestChangesFailureFlag),
      });
    }
  };

  private suggestChangesModalDismiss = () => {
    this.setState({ isSuggestChangesModalOpen: false });
  };

  private suggestChangesModalShow = () => {
    this.setState({ isSuggestChangesModalOpen: true });
  };

  private inEvaluation = () => {
    const defaultValueOnError = false;
    const evaluationDurationDays = 30;

    if (!this.props.data
      || !this.props.data.site
      || !this.props.data.site.context
      || !this.props.data.site.context.firstActivationDate
    ) {
      return defaultValueOnError;
    }

    const firstActivationDate = Number(this.props.data.site.context.firstActivationDate);
    if (isNaN(firstActivationDate)) {
      return defaultValueOnError;
    }

    return firstActivationDate > (Date.now() - evaluationDurationDays * 24 * 60 * 60 * 1000);
  };

  private userIsManaged = () => {
    return (this.props.data
      && this.props.data.user
      && this.props.data.user.managedStatus
      && this.props.data.user.managedStatus.managed);
  };

  private hideSuggestChangesButton = () => {
    return this.inEvaluation() || this.userIsManaged();
  };

}

const withSuggestChangesMutation = graphql<MutationProps & UserDetailsAccountSectionOwnProps & InjectedIntlProps & FlagProps, UserDetailsAccountSectionQuery>(suggestChangesMutation, {
  name: 'suggestChanges',
});

export const UserDetailsAccountSection =
  injectIntl(
    withFlag(
      withUserDetails(
        withSuggestChangesMutation(UserDetailsAccountSectionImpl),
      ),
    ),
  );
