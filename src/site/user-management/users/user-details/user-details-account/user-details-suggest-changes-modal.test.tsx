import { expect } from 'chai';
import { mount, ReactWrapper, shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { IntlProvider } from 'react-intl';

import { Button } from 'common/analytics';
import { ModalDialog } from 'common/modal';

import { createMockIntlProp } from '../../../../../utilities/testing';
import { UserDetailsSuggestChangesModalImpl } from './user-details-suggest-changes-modal';

const nop = () => null;

describe('Suggest changes modal dialog', () => {

  function getUserDetailsSuggestChangesModalImpl() {
    return (
      <UserDetailsSuggestChangesModalImpl
        isOpen={true}
        currentEmail="test@example.com"
        currentName="John Doe"
        dismiss={nop}
        submit={nop}
        intl={createMockIntlProp()}
      />
    );
  }

  function createShallowWrapper(): ShallowWrapper {
    return shallow(
      getUserDetailsSuggestChangesModalImpl(),
    );
  }

  function createMountWrapper(): ReactWrapper {
    return mount(
      <IntlProvider locale="en">
        {getUserDetailsSuggestChangesModalImpl()}
      </IntlProvider>,
    );
  }

  describe('in default state ', () => {
    const wrapper = createShallowWrapper();

    it('should show a ModalDialog', () => {
      expect(wrapper.find(ModalDialog).length).to.equal(1);
      expect(wrapper.find(ModalDialog).props().isOpen).to.equal(true);
    });

    it('should display an input field for name', () => {
      const textField = wrapper.find('[label="Email address"]');
      expect(textField.length).to.equal(1);
      expect(textField.props().value).to.equal('');
      expect(textField.props().placeholder).to.equal('Currently: test@example.com');
    });

    it('should display an input field for email', () => {
      const textField = wrapper.find('[label="Name"]');
      expect(textField.length).to.equal(1);
      expect(textField.props().value).to.equal('');
      expect(textField.props().placeholder).to.equal('Currently: John Doe');
    });

    it('should not be submittable', () => {
      const primaryButton = createMountWrapper().find(Button).first();
      expect(primaryButton.length).to.equal(1);
      expect(primaryButton.props().appearance).to.equal('primary');
      expect(primaryButton.props().isDisabled).to.equal(true);
    });
  });

});
