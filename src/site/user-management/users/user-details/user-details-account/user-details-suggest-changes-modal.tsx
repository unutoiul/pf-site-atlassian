import * as React from 'react';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';

import { ButtonGroup as AkButtonGroup } from '@atlaskit/button';
import AkTextField from '@atlaskit/field-text';

import { Button } from 'common/analytics';
import { links } from 'common/link-constants';
import { ModalDialog } from 'common/modal';
import { ExternalLink } from 'common/navigation';

export const messages = defineMessages({
  title: {
    id: 'user.details.account.section.suggest.changes.modal.title',
    defaultMessage: 'Suggest changes to the user\'s details',
    description: 'The title of the modal dialog',
  },
  description: {
    id: 'user.details.account.section.suggest.changes.modal.description',
    defaultMessage: `We will email {name} with your suggested changes. From there, the user can accept or ignore the changes. {learnMoreLink}`,
    description: 'Description in the modal dialog, displayed below the title',
  },
  learnMoreLink: {
    id: 'user.details.account.section.suggest.changes.modal.learn.more.link',
    defaultMessage: `Read more`,
    description: 'Wording of the "Read more" link to confluence.atlassian.com',
  },
  labelEmail: {
    id: 'user.details.account.section.suggest.changes.modal.label.email',
    defaultMessage: 'Email address',
    description: 'The label of the input text field for the email address',
  },
  labelName: {
    id: 'user.details.account.section.suggest.changes.modal.label.name',
    defaultMessage: 'Name',
    description: 'The label of the input text field for the display name',
  },
  buttonSubmit: {
    id: 'user.details.account.section.suggest.changes.modal.button.confirm',
    defaultMessage: 'Suggest changes',
    description: 'The caption of the primary button',
  },
  buttonCancel: {
    id: 'user.details.account.section.suggest.changes.modal.button.cancel',
    defaultMessage: 'Cancel',
    description: 'The caption of the cancel button',
  },
  currently: {
    id: 'user.details.account.section.suggest.changes.modal.currently',
    defaultMessage: 'Currently: {currentValue}',
    description: 'Placeholder of the input text fields, displayed if no text has been typed',
  },
});

interface OwnProps {
  isOpen: boolean;
  currentEmail: string;
  currentName: string;
  dismiss(): void;
  submit(input: UserDetailsSuggestChangesInput): void;
}

export interface UserDetailsSuggestChangesInput {
  email: string;
  name: string;
}

type UserDetailsSuggestChangesModalState = UserDetailsSuggestChangesInput;

export class UserDetailsSuggestChangesModalImpl extends React.Component<InjectedIntlProps & OwnProps, UserDetailsSuggestChangesModalState> {
  public state: Readonly<UserDetailsSuggestChangesModalState> = {
    email: '',
    name: '',
  };

  public render() {
    const { formatMessage } = this.props.intl;

    return (
      <ModalDialog
        width="small"
        header={formatMessage(messages.title)}
        isOpen={this.props.isOpen}
        onClose={this.onCancel}
        footer={
          this.renderActionButtons()
        }
      >
        <FormattedMessage
          {...messages.description}
          values={{
            name: this.props.currentName,
            learnMoreLink: (
              <ExternalLink href={links.external.atlassianAccount} hideReferrer={false}>
                <FormattedMessage {...messages.learnMoreLink} />
              </ExternalLink>
            ),
          }}
        />

        <AkTextField
          autoFocus={true}
          onChange={this.onEmailChange}
          shouldFitContainer={true}
          label={formatMessage(messages.labelEmail)}
          value={this.state.email}
          maxLength={255}
          placeholder={formatMessage(messages.currently, { currentValue: this.props.currentEmail })}
        />
        <AkTextField
          onChange={this.onNameChange}
          shouldFitContainer={true}
          label={formatMessage(messages.labelName)}
          value={this.state.name}
          maxLength={255}
          placeholder={formatMessage(messages.currently, { currentValue: this.props.currentName })}
        />
      </ModalDialog>
    );
  }

  private renderActionButtons = () => {
    const { formatMessage } = this.props.intl;

    return (
      <AkButtonGroup>
        <Button
          onClick={this.onSubmit}
          appearance="primary"
          isDisabled={!this.canSubmitForm()}
        >
          {formatMessage(messages.buttonSubmit)}
        </Button>
        <Button
          onClick={this.onCancel}
          appearance="subtle-link"
        >
          {formatMessage(messages.buttonCancel)}
        </Button>
      </AkButtonGroup>
    );
  };

  private onSubmit = () => {
    this.props.submit(this.state);
  };

  private onCancel = () => {
    this.props.dismiss();
  };

  private onEmailChange = (e: React.SyntheticEvent<HTMLInputElement>) => {
    this.setState({
      email: e.currentTarget.value,
    });
  };

  private onNameChange = (e: React.SyntheticEvent<HTMLInputElement>) => {
    this.setState({
      name: e.currentTarget.value,
    });
  };

  private canSubmitForm() {
    const newEmail = this.state.email.trim();
    const newName = this.state.name.trim();

    return (newEmail.length > 0 && newEmail !== this.props.currentEmail)
        || (newName.length > 0 && newName !== this.props.currentName);
  }

}

export const UserDetailsSuggestChangesModal = injectIntl(UserDetailsSuggestChangesModalImpl);
