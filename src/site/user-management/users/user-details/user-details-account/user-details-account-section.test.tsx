import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';

import AkEmptyState from '@atlaskit/empty-state';
import { AkProfilecard, ProfileCardAction as AkProfileCardAction } from '@atlaskit/profilecard';
import AkSpinner from '@atlaskit/spinner';

import { createMockIntlContext, createMockIntlProp } from '../../../../../utilities/testing';
import { Header } from '../user-details-page/user-details.styled';
import { UserDetailsAccountSectionImpl } from './user-details-account-section';

describe('User details account section layout', () => {

  function createTestComponent(data: any): ShallowWrapper {
    return shallow(
      <UserDetailsAccountSectionImpl
        intl={createMockIntlProp()}
        userId="DUMMY-USER-ID"
        cloudId="DUMMY-CLOUD-ID"
        data={data}
        suggestChanges={undefined as any}
        showFlag={undefined as any}
        hideFlag={undefined as any}
      />,
      createMockIntlContext(),
    );
  }

  describe('On loading', () => {
    const data = {
      loading: true,
    };

    const wrapper = createTestComponent(data);

    it('should have a spinner', () => {
      expect(wrapper.find(AkSpinner).length).to.equal(1);
    });

    it('should show the header with partial data', () => {
      const partialDataWrapper = createTestComponent({
        loading: true,
        user: {
        },
      });
      expect(partialDataWrapper.find(Header).length).to.equal(1);
    });
  });

  describe('Error state', () => {
    const data = {
      error: 'Failed to load user info',
    };

    const wrapper = createTestComponent(data);

    it('should show the error EmptyState component', () => {
      expect(wrapper.find(AkEmptyState).length).to.equal(1);
    });
  });

  describe('On default state', () => {

    it('should show an AkProfilecard', () => {
      const data = {
        user:  {
          userDetails:  {
            id: 'user-id',
            email: 'email@domain.com',
            displayName: 'Display Name',
            active: true,
            nickname: 'Nickname',
            location: 'Sydney',
            companyName: 'Company Name',
            department: 'Department',
            title: 'Title',
            timezone: 'Timezone',
            system: false,
            presence: '2017-07-10T14:00:00Z',
            hasVerifiedEmail: true,
          },
          managedStatus: {
            managed: false,
          },
        },
        site: {
          context: {
            firstActivationDate: '0',
          },
        },
      };

      const wrapper = createTestComponent(data);

      const profileCard = wrapper.find(AkProfilecard);
      expect(profileCard.length).to.equal(1);
      expect(profileCard.at(0).props().avatarUrl).to.equal('https://avatar-cdn.atlassian.com/user-id?by=id&s=96');
      expect(profileCard.at(0).props().fullName).to.equal('Display Name');
      expect(profileCard.at(0).props().email).to.equal('email@domain.com');
      expect(profileCard.at(0).props().meta).to.equal('Title');
      expect(profileCard.at(0).props().nickname).to.equal('Nickname');
      expect(profileCard.at(0).props().timestring).to.equal('Timezone');
      expect(profileCard.at(0).props().location).to.equal('Sydney');
    });

    it('should pass default props to AkProfilecard when they\'re not found on the user', () => {
      const data = {
        user:  {
          userDetails:  {
            id: 'user-id',
            email: 'email@domain.com',
            displayName: 'Display Name',
            active: true,
            companyName: 'Company Name',
            department: 'Department',
            system: false,
            presence: '2017-07-10T14:00:00Z',
            hasVerifiedEmail: true,
          },
          managedStatus: {
            managed: false,
          },
        },
        site: {
          context: {
            firstActivationDate: '0',
          },
        },
      };

      const wrapper = createTestComponent(data);

      const profileCard = wrapper.find(AkProfilecard).at(0);
      expect(profileCard.props().meta).to.equal('');
      expect(profileCard.props().nickname).to.equal('');
      expect(profileCard.props().timestring).to.equal('');
      expect(profileCard.props().location).to.equal('');
    });

    describe('Suggest Change details button', () => {
      let basicUserDetails;
      const oneHour = 1000 * 60 * 60;
      const thirtyDays = 1000 * 60 * 60 * 24 * 30;

      const expectSuggestDetailsButtonToBeShown = (profileCard) => {
        const actions: AkProfileCardAction[] = profileCard.at(0).props().actions as AkProfileCardAction[];
        expect(actions.length).to.equal(1);
        expect(actions[0].id).to.equal('suggest-changes');
      };
      const expectSuggestDetailsButtonToBeHidden = (profileCard) => {
        const actions: AkProfileCardAction[] = profileCard.at(0).props().actions as AkProfileCardAction[];
        expect(actions).to.be.empty();
      };

      describe('given the user is not managed', () => {
        const getProfileCardForBasicUserInSite = (siteContext) => {
          basicUserDetails.site = siteContext;

          return createTestComponent(basicUserDetails).find(AkProfilecard);
        };

        beforeEach(() => {
          basicUserDetails = {
            user: {
              userDetails: {
                id: 'user-id',
                email: 'email@domain.com',
                displayName: 'Display Name',
              },
              managedStatus: {
                managed: false,
              },
            },
          };
        });
        it('should be shown if the site context is unknown', () => {
          const siteContext = {};

          const profileCard = getProfileCardForBasicUserInSite(siteContext);

          expectSuggestDetailsButtonToBeShown(profileCard);
        });

        it('should be shown if the activation date is not parseable', () => {
          const siteContext = {
            context: {
              firstActivationDate: 'activationDateNotParseableAsNumber',
            },
          };

          const profileCard = getProfileCardForBasicUserInSite(siteContext);

          expectSuggestDetailsButtonToBeShown(profileCard);
        });

        it('should be shown after 30 days trial', () => {
          const siteContext = {
            context: {
              firstActivationDate: `${Date.now() - thirtyDays}`,
            },
          };

          const profileCard = getProfileCardForBasicUserInSite(siteContext);

          expectSuggestDetailsButtonToBeShown(profileCard);
        });

        it('should be hidden during 30 days trial', () => {
          const siteContext = {
            context: {
              firstActivationDate: `${Date.now() - thirtyDays + oneHour}`,
            },
          };

          const profileCard = getProfileCardForBasicUserInSite(siteContext);

          expectSuggestDetailsButtonToBeHidden(profileCard);
        });
      });
      describe('given the 30 days trial has ended', () => {
        const getProfileCardForUserWithManagedStatus = (managedStatus) => {
          basicUserDetails.user.managedStatus = managedStatus;

          return createTestComponent(basicUserDetails).find(AkProfilecard);
        };

        beforeEach(() => {
          basicUserDetails = {
            user: {
              userDetails: {
                id: 'user-id',
                email: 'email@domain.com',
                displayName: 'Display Name',
              },
              managedStatus: {
                managed: false,
              },
            },
            site: {
              context: {
                firstActivationDate: `${Date.now() - thirtyDays}`,
              },
            },
          };
        });

        it('should be shown if there is no user data', () => {
          const undefinedManagedStatus = undefined;

          const profileCard = getProfileCardForUserWithManagedStatus(undefinedManagedStatus);

          expectSuggestDetailsButtonToBeShown(profileCard);
        });

        it('should be shown for not managed users', () => {
          const unmanagedUserStatus = { managed:  false };

          const profileCard = getProfileCardForUserWithManagedStatus(unmanagedUserStatus);

          expectSuggestDetailsButtonToBeShown(profileCard);
        });

        it('should be hidden for managed users', () => {
          const managedUsedStatus = { managed:  true };

          const profileCard = getProfileCardForUserWithManagedStatus(managedUsedStatus);

          expectSuggestDetailsButtonToBeHidden(profileCard);
        });
      });
    });
  });
});
