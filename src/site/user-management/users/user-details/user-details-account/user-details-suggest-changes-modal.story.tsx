import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';

import { UserDetailsSuggestChangesModal } from './user-details-suggest-changes-modal';

const nop = () => null;

storiesOf('Site|User Details/Account Section/Suggest changes', module)
  .add('Default state', () => {
    return (
      <IntlProvider>
        <UserDetailsSuggestChangesModal
          isOpen={true}
          currentEmail={'test@example.com'}
          currentName={'John Doe'}
          dismiss={nop}
          submit={nop}
        />
      </IntlProvider>
    );
  });
