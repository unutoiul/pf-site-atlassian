import { storiesOf } from '@storybook/react';
import merge from 'lodash.merge';
import * as React from 'react';
import ApolloProvider from 'react-apollo/ApolloProvider';
import { IntlProvider } from 'react-intl';

import { createApolloClient } from '../../../../../apollo-client';

import { UserDetailsAccountSectionQuery } from '../../../../../schema/schema-types';
import { createMockIntlProp } from '../../../../../utilities/testing';
import { UserDetailsAccountSectionImpl } from './user-details-account-section';

const DEFAULT_CLOUD_ID = '04606f9d-9d4f-4f3e-95f1-f2f35fdea577';
const DEFAULT_USER_ID = '04606f9d-9d4f-4f3e-95f1-f2f35fdea577';

const defaultProps = {
  intl: createMockIntlProp(),
  cloudId: DEFAULT_CLOUD_ID,
  userId: DEFAULT_USER_ID,
  suggestChanges: undefined as any,
  showFlag: undefined as any,
  hideFlag: undefined as any,
};

const client = createApolloClient();

const defaultUserTemplate = {
  user:  {
    userDetails:  {
      id: 'user-id',
      email: 'email@domain.com',
      displayName: 'Display Name',
      active: true,
      activeStatus: 'ENABLED',
      nickname: null,
      location: null,
      companyName: null,
      department: null,
      title: null,
      timezone: null,
      system: false,
      presence: null,
      hasVerifiedEmail: true,
    },
    groups:  {
      total: 0,
      groups:  [],
    },
    productAccess:  [],
  },
  loading: false,
};

function createTestComponent(data: UserDetailsAccountSectionQuery | {loading: boolean}) {
  return (
    <IntlProvider>
      <ApolloProvider client={client}>
        <div style={{ width: '360px', padding: '200px' }}>
          <UserDetailsAccountSectionImpl {...defaultProps} data={data as any} />
        </div>
      </ApolloProvider>
    </IntlProvider>
  );
}

storiesOf('Site|User Details/Account Section', module)
  .add('Loading', () => {
    return createTestComponent({ loading: true } as any);
  })
  .add('Error', () => {
    return createTestComponent(null as any);
  })
  .add('With only required info', () => {
    const user = defaultUserTemplate;

    return createTestComponent(user);
  })
  .add('Blocked user', () => {
    const user = merge({}, defaultUserTemplate, { user: { userDetails: { activeStatus: 'BLOCKED' } } });

    return createTestComponent(user);
  })
  .add('Default state', () => {
    const user = merge({}, defaultUserTemplate, { user: { userDetails: {
      active: true,
      nickname: 'Nickname',
      location: 'Sydney',
      companyName: 'Company Name',
      department: 'Department',
      title: 'Title',
      timezone: 'Timezone',
      system: false,
      hasVerifiedEmail: true,
      presence: '2017-07-10T14:00:00Z' } } });

    return createTestComponent(user);
  })
  .add('Within 30 days trial', () => {
    const user = merge({}, defaultUserTemplate, { site: { context: { firstActivationDate: Date.now() - 1000 * 60 * 60 } } });

    return createTestComponent(user);
  })
  .add('Managed user', () => {
    const user = merge(
      {},
      defaultUserTemplate,
      {
        site: { context: { firstActivationDate: '0' } },
        user: { managedStatus: { managed: true } },
      },
    );

    return createTestComponent(user);
  });
