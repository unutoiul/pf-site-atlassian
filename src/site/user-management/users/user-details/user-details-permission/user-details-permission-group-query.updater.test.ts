import { DataProxy } from 'apollo-cache';
import { expect } from 'chai';
import * as sinon from 'sinon';
import {
  UserGroupsQuery,
  UserGroupsQueryVariables,
} from 'src/schema/schema-types';

import { getStart } from 'common/pagination';

import { UserGroups } from './helpers/types';

import { Permissions } from '../../user-permission-section/permissions';
import { DEFAULT_ROWS_PER_PAGE, STARTING_PAGE } from '../user-details-page/user-details-constants';
import { createNewGroup } from './helpers/createNewGroup';
import { getGroupNameByRole, SITE_ADMIN_SITE_PRIVILEGE } from './helpers/getGroupNameByRole';
import { optmisticallyAddGroupToUser, optmisticallyRemoveGroupFromUser } from './user-details-permission-group-query.updater';

let writeQuerySpy;

const dummyGroupName = 'DUMMY-GROUP';
const dummyGroupId = 'DUMMY-GROUP-ID';
const dummyGroup: UserGroups = createNewGroup(dummyGroupName, dummyGroupId);

const generateStore = ({ additionalGroupsUserGroups }: {additionalGroupsUserGroups: UserGroups[]} = { additionalGroupsUserGroups: [] }): DataProxy => {

  const groups: UserGroups[] = [
    ...additionalGroupsUserGroups,
  ];

  const data: UserGroupsQuery = {
    user: {
      __typename: 'User',
      groups:  {
        __typename: 'Groups',
        total: 0,
        groups,
      },
    },
  };

  return {
    readQuery: sinon.stub().returns(data),
    writeQuery: writeQuerySpy,
    readFragment: () => null,
    writeFragment: () => null,
    writeData: () => null,
  };
};

describe('Optimistically add User to a Group', () => {
  const sandbox = sinon.sandbox.create();

  beforeEach(() => {
    writeQuerySpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('Successfully add User with no groups to a new group', () => {
    const variables: UserGroupsQueryVariables = {
      cloudId: 'DUMMY-CLOUD-ID',
      userId: 'DUMMY-USER-ID',
      start : getStart(STARTING_PAGE, DEFAULT_ROWS_PER_PAGE),
      count : DEFAULT_ROWS_PER_PAGE,
    };

    const store = generateStore();

    const newGroupName: string = 'TEST-GROUP';
    const newGroupId: string = 'TEST-GROUP-ID';

    optmisticallyAddGroupToUser(store, variables.cloudId, variables.userId, newGroupName, newGroupId);

    expect(writeQuerySpy.callCount).to.equal(1);
    expect(writeQuerySpy.args[0][0].variables).to.deep.equal(variables);
    expect(writeQuerySpy.args[0][0].data.user.groups.total).to.equal(1);
    expect(writeQuerySpy.args[0][0].data.user.groups.groups).to.deep.equal([
      { id: 'TEST-GROUP-ID',
        name: 'TEST-GROUP',
        description: 'TEST-GROUP',
        productPermissions: [{
          __typename: 'productPermissions',
          productId: '-1',
          productName: '',
          permissions: [null],
        }],
        defaultForProducts: [],
        sitePrivilege: 'NONE',
        unmodifiable: false,
        userTotal: 0,
        managementAccess: 'ALL',
        ownerType: null,
        __typename: 'Group' },
    ]);
  });

  it('Successfully add a User to another group', () => {
    const variables: UserGroupsQueryVariables = {
      cloudId: 'DUMMY-CLOUD-ID',
      userId: 'DUMMY-USER-ID',
      start : getStart(STARTING_PAGE, DEFAULT_ROWS_PER_PAGE),
      count : DEFAULT_ROWS_PER_PAGE,
    };

    const store = generateStore({ additionalGroupsUserGroups: [dummyGroup] });

    const newGroupName: string = 'TEST-GROUP';
    const newGroupId: string = 'TEST-GROUP-ID';

    optmisticallyAddGroupToUser(store, variables.cloudId, variables.userId, newGroupName, newGroupId);

    expect(writeQuerySpy.callCount).to.equal(1);
    expect(writeQuerySpy.args[0][0].variables).to.deep.equal(variables);
    expect(writeQuerySpy.args[0][0].data.user.groups.total).to.equal(2);
    expect(writeQuerySpy.args[0][0].data.user.groups.groups).to.deep.equal([
      { id: 'DUMMY-GROUP-ID',
        name: 'DUMMY-GROUP',
        description: 'DUMMY-GROUP',
        productPermissions: [{
          __typename: 'productPermissions',
          productId: '-1',
          productName: '',
          permissions: [null],
        }],
        defaultForProducts: [],
        sitePrivilege: 'NONE',
        unmodifiable: false,
        userTotal: 0,
        managementAccess: 'ALL',
        ownerType: null,
        __typename: 'Group' },
      { id: 'TEST-GROUP-ID',
        name: 'TEST-GROUP',
        description: 'TEST-GROUP',
        productPermissions: [{
          __typename: 'productPermissions',
          productId: '-1',
          productName: '',
          permissions: [null],
        }],
        defaultForProducts: [],
        sitePrivilege: 'NONE',
        unmodifiable: false,
        userTotal: 0,
        managementAccess: 'ALL',
        ownerType: null,
        __typename: 'Group' },
    ]);
  });

  it('Do not add User to a group if they are already in it', () => {
    const variables: UserGroupsQueryVariables = {
      cloudId: 'DUMMY-CLOUD-ID',
      userId: 'DUMMY-USER-ID',
      start : getStart(STARTING_PAGE, DEFAULT_ROWS_PER_PAGE),
      count : DEFAULT_ROWS_PER_PAGE,
    };

    const newGroupName: string = 'TEST-GROUP';
    const newGroupId: string = 'TEST-GROUP-ID';

    const store = generateStore({ additionalGroupsUserGroups: [dummyGroup, createNewGroup(newGroupName, newGroupId)] });

    optmisticallyAddGroupToUser(store, variables.cloudId, variables.userId, newGroupName, newGroupId);

    expect(writeQuerySpy.callCount).to.equal(0);
  });

  it('Successfully add User to the Site Administrator group', () => {
    const variables: UserGroupsQueryVariables = {
      cloudId: 'DUMMY-CLOUD-ID',
      userId: 'DUMMY-USER-ID',
      start : getStart(STARTING_PAGE, DEFAULT_ROWS_PER_PAGE),
      count : DEFAULT_ROWS_PER_PAGE,
    };

    const store = generateStore({ additionalGroupsUserGroups: [dummyGroup] });

    const newGroupName: string = getGroupNameByRole(Permissions.SITEADMIN);
    const newGroupId: string = 'SITE-ADMIN-GROUP-ID';

    optmisticallyAddGroupToUser(store, variables.cloudId, variables.userId, newGroupName, newGroupId);

    expect(writeQuerySpy.callCount).to.equal(1);
    expect(writeQuerySpy.args[0][0].variables).to.deep.equal(variables);
    expect(writeQuerySpy.args[0][0].data.user.groups.total).to.equal(2);
    expect(writeQuerySpy.args[0][0].data.user.groups.groups).to.deep.equal([
      { id: 'DUMMY-GROUP-ID',
        name: 'DUMMY-GROUP',
        description: 'DUMMY-GROUP',
        productPermissions: [{
          __typename: 'productPermissions',
          productId: '-1',
          productName: '',
          permissions: [null],
        }],
        defaultForProducts: [],
        sitePrivilege: 'NONE',
        unmodifiable: false,
        userTotal: 0,
        managementAccess: 'ALL',
        ownerType: null,
        __typename: 'Group' },
      { id: 'SITE-ADMIN-GROUP-ID',
        name: 'site-admins',
        description: 'site-admins',
        productPermissions: [{
          __typename: 'productPermissions',
          productId: '-1',
          productName: '',
          permissions: [null],
        }],
        defaultForProducts: [],
        sitePrivilege: SITE_ADMIN_SITE_PRIVILEGE,
        unmodifiable: false,
        userTotal: 0,
        managementAccess: 'ALL',
        ownerType: null,
        __typename: 'Group' },
    ]);
  });
});

describe('Optimistically remove User from a Group', () => {
  const sandbox = sinon.sandbox.create();

  beforeEach(() => {
    writeQuerySpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('Do nothing when attempting to remove user that does not belong to any group', () => {
    const variables: UserGroupsQueryVariables = {
      cloudId: 'DUMMY-CLOUD-ID',
      userId: 'DUMMY-USER-ID',
      start : getStart(STARTING_PAGE, DEFAULT_ROWS_PER_PAGE),
      count : DEFAULT_ROWS_PER_PAGE,
    };

    const newGroupName: string = 'TEST-GROUP';

    const store = generateStore();

    optmisticallyRemoveGroupFromUser(store, variables.cloudId, variables.userId, newGroupName);

    expect(writeQuerySpy.callCount).to.equal(0);
  });

  it('Do nothing when attempting to remove user from a group that they do not belong to', () => {
    const variables: UserGroupsQueryVariables = {
      cloudId: 'DUMMY-CLOUD-ID',
      userId: 'DUMMY-USER-ID',
      start : getStart(STARTING_PAGE, DEFAULT_ROWS_PER_PAGE),
      count : DEFAULT_ROWS_PER_PAGE,
    };

    const newGroupName: string = 'TEST-GROUP';

    const store = generateStore({ additionalGroupsUserGroups: [dummyGroup] });

    optmisticallyRemoveGroupFromUser(store, variables.cloudId, variables.userId, newGroupName);

    expect(writeQuerySpy.callCount).to.equal(0);
  });

  it('Successfully remove user from a group that they only belong to', () => {
    const variables: UserGroupsQueryVariables = {
      cloudId: 'DUMMY-CLOUD-ID',
      userId: 'DUMMY-USER-ID',
      start : getStart(STARTING_PAGE, DEFAULT_ROWS_PER_PAGE),
      count : DEFAULT_ROWS_PER_PAGE,
    };

    const store = generateStore({ additionalGroupsUserGroups: [dummyGroup] });

    optmisticallyRemoveGroupFromUser(store, variables.cloudId, variables.userId, dummyGroupName);

    expect(writeQuerySpy.callCount).to.equal(1);
    expect(writeQuerySpy.args[0][0].data.user.groups.total).to.equal(0);
    expect(writeQuerySpy.args[0][0].data.user.groups.groups).to.deep.equal([]);
  });

  it('Successfully remove user from a specified group amongst many they belong to', () => {
    const variables: UserGroupsQueryVariables = {
      cloudId: 'DUMMY-CLOUD-ID',
      userId: 'DUMMY-USER-ID',
      start : getStart(STARTING_PAGE, DEFAULT_ROWS_PER_PAGE),
      count : DEFAULT_ROWS_PER_PAGE,
    };

    const groupToRemove = 'FAKE-GROUP';
    const groupToRemoveId = 'FAKE-GROUP-ID';
    const store = generateStore({ additionalGroupsUserGroups: [dummyGroup, createNewGroup(groupToRemove, groupToRemoveId)] });

    optmisticallyRemoveGroupFromUser(store, variables.cloudId, variables.userId, groupToRemove);

    expect(writeQuerySpy.callCount).to.equal(1);
    expect(writeQuerySpy.args[0][0].data.user.groups.total).to.equal(1);
    expect(writeQuerySpy.args[0][0].data.user.groups.groups).to.deep.equal([dummyGroup]);
  });

});
