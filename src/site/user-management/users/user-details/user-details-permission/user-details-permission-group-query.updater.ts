import { DataProxy } from 'apollo-cache';
import {
  UserGroupsQuery,
  UserGroupsQueryVariables,
} from 'src/schema/schema-types';

import { getStart } from 'common/pagination';

import { createNewGroup } from './helpers/createNewGroup';
import { excludeGroup } from './helpers/excludeGroup';
import { filterGroup } from './helpers/filterGroup';
import { UserDetailsGroup, UserGroups } from './helpers/types';

import userDetailsGroupQuery from '../user-details-group/user-details-group-section.query.graphql';
import { DEFAULT_ROWS_PER_PAGE, STARTING_PAGE } from '../user-details-page/user-details-constants';

export const optmisticallyRemoveGroupFromUser = (proxy: DataProxy, cloudId: string, userId: string, groupName: string) => {
  const variables: UserGroupsQueryVariables = {
    cloudId,
    userId,
    start : getStart(STARTING_PAGE, DEFAULT_ROWS_PER_PAGE),
    count : DEFAULT_ROWS_PER_PAGE,
  };

  const storeData: UserGroupsQuery | null = proxy.readQuery({
    query: userDetailsGroupQuery,
    variables,
  });

  if (!(storeData && storeData.user && storeData.user.groups && storeData.user.groups.groups)) {
    return;
  }

  if (storeData.user.groups.groups.length === 0) {
    return;
  }

  const groupsAfterExclusion = excludeGroup(storeData.user.groups.groups, groupName) as UserGroups[];

  if (storeData.user.groups.groups.length === groupsAfterExclusion.length) {
    return;
  }

  storeData.user.groups.groups = groupsAfterExclusion;
  storeData.user.groups.total = Math.max(0, storeData.user.groups.groups.length);

  proxy.writeQuery({
    query: userDetailsGroupQuery,
    variables,
    data: storeData,
  });
};

export const optmisticallyAddGroupToUser = (proxy: DataProxy, cloudId: string, userId: string, groupName: string, groupId: string) => {
  if (!groupId) {
    return;
  }

  const storeData = proxy.readQuery({
    query: userDetailsGroupQuery,
    variables: {
      cloudId,
      userId,
      start: getStart(STARTING_PAGE, DEFAULT_ROWS_PER_PAGE),
      count: DEFAULT_ROWS_PER_PAGE,
    } as UserGroupsQueryVariables,
  }) as UserGroupsQuery;

  if (!(storeData && storeData.user && storeData.user.groups && storeData.user.groups.groups)) {
    return;
  }

  const groupAlreadyAdded: UserDetailsGroup[] = filterGroup(storeData.user.groups.groups, groupName);

  if (groupAlreadyAdded && groupAlreadyAdded.length > 0) {
    return;
  }

  storeData.user.groups.groups.push(createNewGroup(groupName, groupId));
  storeData.user.groups.total = Math.max(0, storeData.user.groups.groups.length);

  proxy.writeQuery({
    query: userDetailsGroupQuery,
    variables: {
      cloudId,
      userId,
      start: getStart(STARTING_PAGE, DEFAULT_ROWS_PER_PAGE),
      count: DEFAULT_ROWS_PER_PAGE,
    },
    data: storeData,
  });
};
