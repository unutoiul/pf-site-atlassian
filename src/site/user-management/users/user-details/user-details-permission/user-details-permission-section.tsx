import { DataProxy } from 'apollo-cache';
import * as React from 'react';
import { compose, graphql, MutationFunc, QueryResult } from 'react-apollo';
import { ChildProps } from 'react-apollo/types';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import {
  AddUsersToGroupsMutation,
  AddUsersToGroupsMutationVariables,
  ChangeUserRoleMutation,
  ChangeUserRoleMutationVariables,
  GroupListQuery,
  GroupListQueryVariables,
  RemoveUserFromGroupMutation,
  RemoveUserFromGroupMutationVariables,
  UserPermissionsQuery,
} from 'src/schema/schema-types';

import AkSpinner from '@atlaskit/spinner';

import { AnalyticsClientProps, changedUserPermissionTrackEventData, failedUserPermissionTrackEventData, selectedUserPermissionUIEventData, withAnalyticsClient } from 'common/analytics';

import { createErrorIcon, createSuccessIcon } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';

import { AnalyticsProps } from '../../../user-management-analytics';

import { TrustedUsersFeatureFlag, withTrustedUsersFeatureFlag } from '../../../../feature-flags/with-trusted-users-feature-flag';

import { Permissions, permissionToRole } from '../../user-permission-section/permissions';
import { labels } from '../../user-permission-section/user-permission-item-labels';
import { UserPermissionItemContainer } from '../../user-permission-section/user-permission-items';
import { GroupsOuterDiv, Header, SectionTitle } from '../user-details-page/user-details.styled';
import { getGroupNameByRole } from './helpers/getGroupNameByRole';
import addUsersToGroupsMutation from './user-details-permission-add-users-to-groups.mutation.graphql';
import changeUserRoleMutation from './user-details-permission-change-user-role.mutation.graphql';
import groupListQuery from './user-details-permission-group-list.query.graphql';
import { optmisticallyAddGroupToUser, optmisticallyRemoveGroupFromUser } from './user-details-permission-group-query.updater';
import { optimisticallyUpdateUserDetails } from './user-details-permission-query.updater';
import removeUserFromGroupMutation from './user-details-permission-remove-user-from-group.mutation.graphql';

const messages = defineMessages({
  heading: {
    id: 'users.detail.role.section.heading',
    description: 'Heading for the roles section of the user details page',
    defaultMessage: 'Roles',
  },
  error: {
    id: 'users.detail.role.section.error',
    description: 'Error message shown when we fail to load the list of roles the user can be in',
    defaultMessage: `We had trouble trying to retrieve this user's role, please refresh the page to try again.`,
  },
  successTitle: {
    id: 'users.detail.role.section.success.title',
    description: 'Success title shown when we successfully change the role',
    defaultMessage: 'Role was successfully assigned',
  },
  successDescription: {
    id: 'users.detail.role.section.success.description',
    description: 'Success message shown when we successfully change the role',
    defaultMessage: `{newRole} role was successfully assigned to the user. You can reassign user role at any time.`,
  },
  changeRoleErrorTitle: {
    id: 'users.detail.role.change.error.title',
    description: 'Title of error shown when we fail to change role assigned to the user',
    defaultMessage: 'Error changing role',
  },
  changeRoleErrorDescription: {
    id: 'users.detail.role.change.error.description',
    description: 'Error message shown when we fail to change role role assigned to the user',
    defaultMessage: 'Something went wrong while trying to change the role',
  },
});

interface UserDetailsPermissionSectionOwnProps {
  cloudId: string;
  userId: string;
  isCurrentUser: boolean;
  userPermission: Permissions;
  isUserWithNoAccess: boolean;
  isUserSystem: boolean;
  isUserSystemAdmin: boolean;
}

interface MutationProps {
  changeUserRole: MutationFunc<
    ChangeUserRoleMutation,
    ChangeUserRoleMutationVariables
  >;

  addUsersToGroups: MutationFunc<
    AddUsersToGroupsMutation,
    AddUsersToGroupsMutationVariables
  >;

  removeUserFromGroup: MutationFunc<
    RemoveUserFromGroupMutation,
    RemoveUserFromGroupMutationVariables
  >;

}

interface QueryProps {
  groupData: QueryResult & Partial<GroupListQuery>;
}

export type UserDetailsPermissionSectionProps = ChildProps<
  UserDetailsPermissionSectionOwnProps & InjectedIntlProps & TrustedUsersFeatureFlag & MutationProps & QueryProps & AnalyticsProps & AnalyticsClientProps & FlagProps,
  UserPermissionsQuery & GroupListQuery & ChangeUserRoleMutation & AddUsersToGroupsMutation & RemoveUserFromGroupMutation
>;

const disablePermissionChange = ({ isCurrentUser, isUserSystemAdmin, isUserSystem, isUserWithNoAccess }: {
  isCurrentUser: boolean,
  isUserSystemAdmin: boolean,
  isUserSystem: boolean,
  isUserWithNoAccess: boolean,
}) => {
  return isCurrentUser || isUserSystemAdmin || isUserSystem || isUserWithNoAccess;
};

interface OwnState {
  isChangingPermission: boolean;
}

export class UserDetailsPermissionSectionImpl extends React.Component<UserDetailsPermissionSectionProps, OwnState> {
  public readonly state: Readonly<OwnState> = {
    isChangingPermission: false,
  };

  public render() {
    const {
      intl: { formatMessage },
      trustedUsersFeatureFlag,
      cloudId,
      isCurrentUser,
      isUserSystemAdmin,
      isUserSystem,
      isUserWithNoAccess,
      userPermission,
      groupData,
    } = this.props;

    if (!trustedUsersFeatureFlag || !trustedUsersFeatureFlag.value) {
      return (
        <div data-test="hidden-permission-group" />
      );
    }

    const defaultHeadingContent = (
      <GroupsOuterDiv>
        <Header>
          <SectionTitle>
            <h2>
              {formatMessage(messages.heading)}
            </h2>
          </SectionTitle>
        </Header>
      </GroupsOuterDiv>
    );

    if (trustedUsersFeatureFlag.isLoading || groupData.loading) {
      return (
        <GroupsOuterDiv>
          {defaultHeadingContent}
          <AkSpinner />
        </GroupsOuterDiv>
      );
    }

    if (groupData.error || !groupData.groupList) {
      return (
        <GroupsOuterDiv>
          {defaultHeadingContent}
          {formatMessage(messages.error)}
        </GroupsOuterDiv>
      );
    }

    const LoadedHeadingContent = (
      <div>
        <Header>
          <SectionTitle>
            <h2>
              {formatMessage(messages.heading)}
            </h2>
          </SectionTitle>
        </Header>
      </div>
    );

    return (
      <GroupsOuterDiv>
        {LoadedHeadingContent}
        <div data-test="visible-permission-group">
          <UserPermissionItemContainer
            isDisabled={this.state.isChangingPermission || disablePermissionChange({ isCurrentUser, isUserSystemAdmin, isUserSystem, isUserWithNoAccess })}
            cloudId={cloudId}
            onChange={this.onUserRoleChange}
            trustedUsersFeatureFlag={trustedUsersFeatureFlag}
            permission={userPermission}
          />
        </div>
      </GroupsOuterDiv>
    );
  }

  private async changeUserRole(newRole: Permissions) {
    const { changeUserRole, cloudId, userId } = this.props;

    await changeUserRole({
      variables: {
        cloudId,
        userId,
        roleId: permissionToRole[newRole],
      },
      optimisticResponse: {
        changeUserRole: true,
      },
      update: (proxy) => {
        this.optimisticallyUpdateUser(proxy, cloudId, userId, newRole);
      },
    });
  }

  private optimisticallyUpdateUser(proxy: DataProxy, cloudId: string, userId: string, newRole: Permissions) {
    const siteAdminGroupName = getGroupNameByRole(Permissions.SITEADMIN);
    const trustedUserGroupName = getGroupNameByRole(Permissions.TRUSTED);

    optmisticallyRemoveGroupFromUser(proxy, cloudId, userId, siteAdminGroupName);
    optmisticallyRemoveGroupFromUser(proxy, cloudId, userId, trustedUserGroupName);
    switch (newRole) {
      case Permissions.TRUSTED: {
        optmisticallyAddGroupToUser(proxy, cloudId, userId, trustedUserGroupName, this.getGroupId(Permissions.TRUSTED));
        optimisticallyUpdateUserDetails(proxy, cloudId, userId, this.getGroups(), Permissions.TRUSTED);

        return;
      }
      case Permissions.SITEADMIN: {
        optmisticallyAddGroupToUser(proxy, cloudId, userId, siteAdminGroupName, this.getGroupId(Permissions.SITEADMIN));
        optimisticallyUpdateUserDetails(proxy, cloudId, userId, this.getGroups(), Permissions.SITEADMIN);

        return;
      }
      case Permissions.BASIC: {
        optimisticallyUpdateUserDetails(proxy, cloudId, userId, this.getGroups(), Permissions.BASIC);

        return;
      }
      default: {

        return;
      }
    }
  }

  private getGroups() {
    const { groupData } = this.props;

    return groupData && groupData.groupList && groupData.groupList.groups || [];
  }

  private getGroupId(role: Permissions) {

    const group = this.getGroups().find(
      ({ name }) => name === getGroupNameByRole(role),
    ) || { id: '' };

    return group.id;
  }

  private onUserRoleChange = async (newRole: Permissions) => {
    const oldRole = this.props.userPermission;

    if (newRole === oldRole) {
      return;
    }

    this.sendSelectedUserRoleUIEventData({
      previousPermissionType: oldRole,
      newPermissionType: newRole,
    });

    this.setState({ isChangingPermission: true });

    await this.changeUserRole(newRole)
      .then(() => this.onRoleChangeSuccess(oldRole, newRole))
      .catch(() => this.onRoleChangeError(oldRole, newRole));

    this.setState({ isChangingPermission: false });
  };

  private sendChangedUserRoleTrackEventData = ({ previousPermissionType, newPermissionType }: {previousPermissionType: Permissions, newPermissionType: Permissions}) => {
    this.props.analyticsClient.sendTrackEvent({
      cloudId: this.props.cloudId,
      data: changedUserPermissionTrackEventData({
        userState: this.props.userStates,
        userId: this.props.userId,
        previousPermissionType,
        newPermissionType,
      }),
    });
  };

  private sendSelectedUserRoleUIEventData = ({ previousPermissionType, newPermissionType }: {previousPermissionType: Permissions, newPermissionType: Permissions}) => {
    this.props.analyticsClient.sendUIEvent({
      cloudId: this.props.cloudId,
      data: selectedUserPermissionUIEventData({
        userState: this.props.userStates,
        userId: this.props.userId,
        previousPermissionType,
        newPermissionType,
      }),
    });
  };

  private failedChangedUserRoleTrackEventData = ({ previousPermissionType, failedPermissionType, failureReason }: {previousPermissionType: Permissions, failedPermissionType: Permissions, failureReason: string}) => {
    this.props.analyticsClient.sendTrackEvent({
      cloudId: this.props.cloudId,
      data: failedUserPermissionTrackEventData({
        userState: this.props.userStates,
        userId: this.props.userId,
        previousPermissionType,
        failedPermissionType,
        failureReason,
      }),
    });
  }

  private onRoleChangeError(oldRole: Permissions, newRole: Permissions) {
    const { showFlag, intl: { formatMessage } } = this.props;

    this.failedChangedUserRoleTrackEventData({
      previousPermissionType: oldRole,
      failedPermissionType: newRole,
      failureReason: 'Unable to grant role to the User. Please try again later.',
    });
    showFlag({
      autoDismiss: false,
      icon: createErrorIcon(),
      id: 'UserDetailsPermissionSection.changeRole.error.flag.description',
      title: formatMessage(messages.changeRoleErrorTitle),
      description: formatMessage(messages.changeRoleErrorDescription),
    });
  }

  private onRoleChangeSuccess(oldRole: Permissions, newRole: Permissions) {
    const { intl: { formatMessage } } = this.props;

    this.sendChangedUserRoleTrackEventData({
      previousPermissionType: oldRole,
      newPermissionType: newRole,
    });
    this.props.showFlag({
      autoDismiss: true,
      icon: createSuccessIcon(),
      id: `${messages.successTitle.id}.${newRole}`,
      title: formatMessage(messages.successTitle),
      description: formatMessage(messages.successDescription, { newRole: formatMessage(labels[newRole].label) }),
    });
  }
}

const withGroupListQuery = graphql<UserDetailsPermissionSectionOwnProps, GroupListQuery, GroupListQueryVariables>(groupListQuery, {
  name: 'groupData',
  skip: ({ cloudId }) => !cloudId,
  options: ({ cloudId }) => ({
    variables: {
      cloudId,
    },
  }),
});

const withChangeUserRoleMutation = graphql<UserDetailsPermissionSectionOwnProps, ChangeUserRoleMutation, ChangeUserRoleMutationVariables>(changeUserRoleMutation, {
  name: 'changeUserRole',
});

const withAddUsersToGroupsMutation = graphql<UserDetailsPermissionSectionOwnProps, AddUsersToGroupsMutation, AddUsersToGroupsMutationVariables>(addUsersToGroupsMutation, {
  name: 'addUsersToGroups',
});

const withRemoveUserFromGroupMutation = graphql<UserDetailsPermissionSectionOwnProps, RemoveUserFromGroupMutation, RemoveUserFromGroupMutationVariables>(removeUserFromGroupMutation, {
  name: 'removeUserFromGroup',
});

export const UserDetailsPermissionSection = compose(
  withFlag,
  withAnalyticsClient,
  withAddUsersToGroupsMutation,
  withRemoveUserFromGroupMutation,
  withChangeUserRoleMutation,
  withTrustedUsersFeatureFlag,
  withGroupListQuery,
  injectIntl,
)(UserDetailsPermissionSectionImpl);
