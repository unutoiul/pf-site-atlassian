import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import AkSpinner from '@atlaskit/spinner';

import { createMockAnalyticsClient, createMockIntlProp } from '../../../../../utilities/testing';
import { Permissions } from '../../user-permission-section/permissions';
import { UserPermissionItemContainer } from '../../user-permission-section/user-permission-items';
import { UserDetailsPermissionSectionImpl } from './user-details-permission-section';

const exampleGroupData = {
  groupList: {
    groups: [{
      id: 'example-id',
      name: 'site-admins',
    }],
  },
};

describe('User details permission section layout', () => {
  describe('Using mounted component', () => {
    function createMountedTestComponent({ trustedUsersFeatureFlag, userPermission = Permissions.BASIC, isUserSystemAdmin = false, isUserWithNoAccess= false, isUserSystem= false, groupData, isCurrentUser = false }) {
      return shallow(
        <UserDetailsPermissionSectionImpl
          cloudId="dummy-cloudId"
          userId="dummy-userId"
          userStates={[]}
          analyticsClient={createMockAnalyticsClient()}
          isCurrentUser={isCurrentUser}
          userPermission={userPermission}
          isUserWithNoAccess={isUserWithNoAccess}
          isUserSystem={isUserSystem}
          isUserSystemAdmin={isUserSystemAdmin}
          trustedUsersFeatureFlag={trustedUsersFeatureFlag}
          intl={createMockIntlProp()}
          groupData={groupData}
          showFlag={() => ({ })}
          hideFlag={() => ({ })}
          changeUserRole={async () => ({
            loading: false,
            networkStatus: 200,
            stale: false,
            data: {
              changeUserRole: true,
            },
          })}
          addUsersToGroups={async () => ({
            loading: false,
            networkStatus: 200,
            stale: false,
            data: {
              addUsersToGroups: true,
            },
          })}
          removeUserFromGroup={async () => ({
            loading: false,
            networkStatus: 200,
            stale: false,
            data: {
              deleteUserFromGroup: true,
            },
          })}
        />,
      );
    }

    it('Should render empty block when trusted users feature is turned off', () => {
      const wrapper = createMountedTestComponent({
        trustedUsersFeatureFlag: {
          value: false,
        },
        groupData: exampleGroupData,
      });
      expect(wrapper.contains(<div data-test="hidden-permission-group" />)).to.equal(true);
    });

    it('Should render spinner when trusted users feature is loading', () => {
      const wrapper = createMountedTestComponent({
        trustedUsersFeatureFlag: {
          isLoading: true,
          value: true,
        },
        groupData: exampleGroupData,
      });

      const permissionContainer = wrapper.find(UserPermissionItemContainer);
      expect(permissionContainer.exists()).to.equal(false);

      const spinner = wrapper.find(AkSpinner);
      expect(spinner.exists()).to.equal(true);
    });

    it('Should render the user permission section when trusted users feature is turned on and user query has resolved for Basic User', () => {
      const wrapper = createMountedTestComponent({
        trustedUsersFeatureFlag: {
          value: true,
        },
        groupData: exampleGroupData,
      });

      const permissionContainer = wrapper.find(UserPermissionItemContainer);
      expect(permissionContainer.exists()).to.equal(true);
      expect(permissionContainer.at(0).props().isDisabled).to.equal(false);
      expect(permissionContainer.at(0).props().cloudId).to.equal('dummy-cloudId');
      expect(permissionContainer.at(0).props().trustedUsersFeatureFlag.value).to.equal(true);
      expect(permissionContainer.at(0).props().permission).to.equal(Permissions.BASIC);
    });

    it('Should render the user permission section when trusted users feature is turned on and user permission has resolved for Trusted User', () => {
      const wrapper = createMountedTestComponent({
        trustedUsersFeatureFlag: {
          value: true,
        },
        userPermission: Permissions.TRUSTED,
        groupData: exampleGroupData,
      });

      const permissionContainer = wrapper.find(UserPermissionItemContainer);
      expect(permissionContainer.exists()).to.equal(true);
      expect(permissionContainer.at(0).props().isDisabled).to.equal(false);
      expect(permissionContainer.at(0).props().cloudId).to.equal('dummy-cloudId');
      expect(permissionContainer.at(0).props().trustedUsersFeatureFlag.value).to.equal(true);
      expect(permissionContainer.at(0).props().permission).to.equal(Permissions.TRUSTED);
    });

    it('Should render the user permission section when trusted users feature is turned on and user query has resolved for Site Admin', () => {
      const wrapper = createMountedTestComponent({
        trustedUsersFeatureFlag: {
          value: true,
        },
        userPermission: Permissions.SITEADMIN,
        groupData: exampleGroupData,
      });

      const permissionContainer = wrapper.find(UserPermissionItemContainer);
      expect(permissionContainer.exists()).to.equal(true);
      expect(permissionContainer.at(0).props().isDisabled).to.equal(false);
      expect(permissionContainer.at(0).props().cloudId).to.equal('dummy-cloudId');
      expect(permissionContainer.at(0).props().trustedUsersFeatureFlag.value).to.equal(true);
      expect(permissionContainer.at(0).props().permission).to.equal(Permissions.SITEADMIN);
    });

    it('Should render the user permission section when trusted users feature is turned on and user query has resolved for System Admin', () => {
      const wrapper = createMountedTestComponent({
        trustedUsersFeatureFlag: {
          value: true,
        },
        userPermission: Permissions.SITEADMIN,
        isUserSystemAdmin: true,
        groupData: exampleGroupData,
      });

      const permissionContainer = wrapper.find(UserPermissionItemContainer);
      expect(permissionContainer.exists()).to.equal(true);
      expect(permissionContainer.at(0).props().isDisabled).to.equal(true);
      expect(permissionContainer.at(0).props().cloudId).to.equal('dummy-cloudId');
      expect(permissionContainer.at(0).props().trustedUsersFeatureFlag.value).to.equal(true);
      expect(permissionContainer.at(0).props().permission).to.equal(Permissions.SITEADMIN);
    });

    it('Should render the user permission section when trusted users feature is turned on and user query has resolved for System', () => {
      const wrapper = createMountedTestComponent({
        trustedUsersFeatureFlag: {
          value: true,
        },
        isUserSystem: true,
        userPermission: Permissions.SITEADMIN,
        groupData: exampleGroupData,
      });

      const permissionContainer = wrapper.find(UserPermissionItemContainer);
      expect(permissionContainer.exists()).to.equal(true);
      expect(permissionContainer.at(0).props().isDisabled).to.equal(true);
      expect(permissionContainer.at(0).props().cloudId).to.equal('dummy-cloudId');
      expect(permissionContainer.at(0).props().trustedUsersFeatureFlag.value).to.equal(true);
      expect(permissionContainer.at(0).props().permission).to.equal(Permissions.SITEADMIN);
    });

    it('Should render the user permission section and disable dropdown for current trusted admin user', () => {
      const wrapper = createMountedTestComponent({
        trustedUsersFeatureFlag: {
          value: true,
        },
        isCurrentUser: true,
        userPermission: Permissions.TRUSTED,
        groupData: exampleGroupData,
      });

      const permissionContainer = wrapper.find(UserPermissionItemContainer);
      expect(permissionContainer.exists()).to.equal(true);
      expect(permissionContainer.at(0).props().isDisabled).to.equal(true);
      expect(permissionContainer.at(0).props().cloudId).to.equal('dummy-cloudId');
      expect(permissionContainer.at(0).props().trustedUsersFeatureFlag.value).to.equal(true);
      expect(permissionContainer.at(0).props().permission).to.equal(Permissions.TRUSTED);
    });

    it('Should render the user permission section and disable dropdown for inactive user', () => {
      const wrapper = createMountedTestComponent({
        trustedUsersFeatureFlag: {
          value: true,
        },
        isCurrentUser: false,
        isUserWithNoAccess: true,
        groupData: exampleGroupData,
      });

      const permissionContainer = wrapper.find(UserPermissionItemContainer);
      expect(permissionContainer.exists()).to.equal(true);
      expect(permissionContainer.at(0).props().isDisabled).to.equal(true);
      expect(permissionContainer.at(0).props().cloudId).to.equal('dummy-cloudId');
      expect(permissionContainer.at(0).props().trustedUsersFeatureFlag.value).to.equal(true);
      expect(permissionContainer.at(0).props().permission).to.equal(Permissions.BASIC);
    });
  });
});
