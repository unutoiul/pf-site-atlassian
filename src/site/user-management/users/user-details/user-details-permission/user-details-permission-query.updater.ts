import { DataProxy } from 'apollo-cache';
import merge from 'lodash.merge';

import { getGroupNameByRole } from './helpers/getGroupNameByRole';

import { createNewGroup } from './helpers/createNewGroup';
import { excludeGroup } from './helpers/excludeGroup';
import { hasSiteAdminGroup, hasTrustedUserGroup } from './helpers/filterGroup';
import { UserDetailsGroup } from './helpers/types';

import { UserDetailsPageQuery } from '../../../../../schema/schema-types';
import { Permissions } from '../../user-permission-section/permissions';
import userDetailsPageQuery from '../user-details-page/user-details-page.query.graphql';

const siteAdminGroupName = getGroupNameByRole(Permissions.SITEADMIN);
const trustedUserGroupName = getGroupNameByRole(Permissions.TRUSTED);

interface GroupDetails {
  name: string;
  id: string;
}

export const optimisticallyUpdateUserDetails = (proxy: DataProxy, cloudId: string, userId: string, allGroups: GroupDetails[], newPermission: string) => {
  const variables = {
    cloudId,
    userId,
    start: 1,
    count: 100,
  };

  const storeData: UserDetailsPageQuery | null = proxy.readQuery({
    query: userDetailsPageQuery,
    variables,
  });

  if (!(storeData && storeData.user && storeData.user.groups && storeData.user.groups.groups)) {
    return;
  }

  const userGroups = storeData.user.groups.groups;

  let updatedStoreData: StoreData;

  switch (newPermission) {
    case Permissions.TRUSTED: {
      updatedStoreData = handleTrustedUser(userGroups, allGroups);
      break;
    }
    case Permissions.SITEADMIN: {
      updatedStoreData = handleSiteAdmin(userGroups, allGroups);
      break;
    }
    default:
      updatedStoreData = handleBasicUser(userGroups, allGroups);
      break;
  }

  storeData.user.groups.groups = updatedStoreData.groups;
  storeData.user.groups.total = updatedStoreData.groups.length;

  updatedStoreData = merge(
    storeData,
    {
      user: {
        userDetails: {
          trustedUser: updatedStoreData.trustedUser,
          siteAdmin: updatedStoreData.siteAdmin,
        },
      },
    });

  proxy.writeQuery({
    query: userDetailsPageQuery,
    variables,
    data: updatedStoreData,
  });

  return;
};

interface StoreData {
  groups: UserDetailsGroup[];
  trustedUser?: boolean;
  siteAdmin?: boolean;
}

type HandlePermissionChange = (
  groups: UserDetailsGroup[],
  allGrouos: GroupDetails[],
) => StoreData;

const handleTrustedUser: HandlePermissionChange = (userGroups, allGroups) => {
  const filteredGroups = excludeGroup(userGroups, siteAdminGroupName);

  if (!hasTrustedUserGroup(userGroups)) {
    const trustedUserGroup = allGroups.find(({ name }) => name === trustedUserGroupName);
    if (trustedUserGroup) {
      filteredGroups.push(createNewGroup(trustedUserGroupName, trustedUserGroup.id));
    }
  }

  return {
    groups: filteredGroups,
    trustedUser: true,
    siteAdmin: false,
  };
};

const handleSiteAdmin: HandlePermissionChange = (userGroups, allGroups) => {
  const filteredGroups = excludeGroup(userGroups, trustedUserGroupName);

  if (!hasSiteAdminGroup(filteredGroups)) {
    const siteAdminGroup = allGroups.find(({ name }) => name === siteAdminGroupName);
    if (siteAdminGroup) {
      filteredGroups.push(createNewGroup(siteAdminGroupName, siteAdminGroup.id));
    }
  }

  return {
    groups: filteredGroups,
    trustedUser: false,
    siteAdmin: true,
  };
};

const handleBasicUser: HandlePermissionChange = (userGroups, _) => {
  const filteredGroups = excludeGroup(
    excludeGroup(userGroups, trustedUserGroupName),
    siteAdminGroupName,
  );

  return {
    groups: filteredGroups,
    trustedUser: false,
    siteAdmin: false,
  };
};

export const optimisticallyUpdatePermissionsForSiteAdminGroup = (proxy: DataProxy, cloudId: string, userId: string, siteAdminGroupId: string) => {
  const variables = {
    cloudId,
    userId,
    start: 1,
    count: 100,
  };

  const storeData: UserDetailsPageQuery | null = proxy.readQuery({
    query: userDetailsPageQuery,
    variables,
  });

  if (!(storeData && storeData.user && storeData.user.userDetails && storeData.user.groups && storeData.user.groups.groups)) {
    return;
  }

  const userGroups = storeData.user.groups.groups;

  if (!hasSiteAdminGroup(userGroups)) {
    userGroups.push(createNewGroup(siteAdminGroupName, siteAdminGroupId));
  }

  storeData.user.userDetails.siteAdmin = true;
  storeData.user.groups.groups = userGroups;
  storeData.user.groups.total = userGroups.length;

  proxy.writeQuery({
    query: userDetailsPageQuery,
    variables,
    data: storeData,
  });
};

export const optimisticallyUpdatePermissionsForSiteAdminGroupRemoval = (proxy: DataProxy, cloudId: string, userId: string) => {
  const variables = {
    cloudId,
    userId,
    start: 1,
    count: 100,
  };

  const storeData: UserDetailsPageQuery | null = proxy.readQuery({
    query: userDetailsPageQuery,
    variables,
  });

  if (!(storeData && storeData.user && storeData.user.userDetails && storeData.user.groups && storeData.user.groups.groups)) {
    return;
  }

  const { siteAdmin } = storeData.user.userDetails;

  if (!siteAdmin) {
    return;
  }

  const userGroups = storeData.user.groups.groups;
  const userGroupsExcludingSiteAdmin = excludeGroup(userGroups, siteAdminGroupName);

  storeData.user.groups.groups = userGroupsExcludingSiteAdmin;
  storeData.user.groups.total = userGroupsExcludingSiteAdmin.length;
  storeData.user.userDetails.siteAdmin = false;

  proxy.writeQuery({
    query: userDetailsPageQuery,
    variables,
    data: storeData,
  });
};
