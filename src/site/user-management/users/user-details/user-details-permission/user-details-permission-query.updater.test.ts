import { DataProxy } from 'apollo-cache';
import { expect } from 'chai';
import * as sinon from 'sinon';

import { getGroupNameByRole } from './helpers/getGroupNameByRole';

import { Permissions } from '../../user-permission-section/permissions';
import { createNewGroup } from './helpers/createNewGroup';
import { UserDetailsGroup } from './helpers/types';

import { optimisticallyUpdatePermissionsForSiteAdminGroupRemoval, optimisticallyUpdateUserDetails } from './user-details-permission-query.updater';

import { SitePrivilege, UserDetailsPageQuery, UserDetailsPageQueryVariables } from '../../../../../schema/schema-types';
let writeQuerySpy;

const DUMMY_TEST_USER_ID = 'DUMMY-TEST-USER-ID';

const trustedUserGroup = createNewGroup(getGroupNameByRole(Permissions.TRUSTED), 'TRUSTED-USER-GROUP-ID');
const siteAdminGroup = createNewGroup(getGroupNameByRole(Permissions.SITEADMIN), 'SITE-ADMIN-GROUP-ID');
const allGroups = [trustedUserGroup, siteAdminGroup];

const generateStore = ({
  system = false,
  trustedUser = false,
  siteAdmin = false,
  additionalGroups = [] as UserDetailsGroup[],
}): DataProxy => {
  const groups = [
    ...additionalGroups,
    {
      id: 'DUMMY-GROUP',
      description: 'Fake group description',
      sitePrivilege: 'NONE' as SitePrivilege,
      __typename: 'Group',
    },
  ];

  const storeQueryData: UserDetailsPageQuery = {
    user:  {
      __typename: 'UserDetails',
      userDetails:  {
        __typename: 'User',
        id: DUMMY_TEST_USER_ID,
        displayName: '',
        active: false,
        activeStatus: 'ENABLED',
        presence: 'string | null',
        system,
        trustedUser,
        siteAdmin,
      },
      groups:  {
        __typename: 'Groups',
        groups,
        total: groups.length,
      },
    },
    currentUser:  {
      __typename: 'User',
      id: DUMMY_TEST_USER_ID,
    },
  };

  return {
    readQuery: sinon.stub().returns(storeQueryData),
    writeQuery: writeQuerySpy,
    readFragment: () => null,
    writeFragment: () => null,
    writeData: () => null,
  };
};

const basicUserStore = () => {
  return generateStore({});
};

const trustedUserStore = () => {
  return generateStore({
    additionalGroups: [trustedUserGroup],
  });
};

const siteAdministratorStore = () => {
  return generateStore({
    additionalGroups: [siteAdminGroup],
  });
};

describe('Optimistically Update User Details when changing permissions', () => {
  const sandbox = sinon.sandbox.create();

  beforeEach(() => {
    writeQuerySpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('Successfully assign Trusted User role to a Basic User', () => {
    const variables: UserDetailsPageQueryVariables = {
      cloudId: 'DUMMY-CLOUD-ID',
      userId: 'DUMMY-TEST-USER-ID',
      start: 1,
      count: 100,
    };

    const store = basicUserStore();

    optimisticallyUpdateUserDetails(store, variables.cloudId, variables.userId, allGroups, Permissions.TRUSTED);

    expect(writeQuerySpy.callCount).to.equal(1);

    expect(writeQuerySpy.args[0][0].variables).to.deep.equal(variables);
    expect(writeQuerySpy.args[0][0].data.user.userDetails.trustedUser).to.equal(true);
    expect(writeQuerySpy.args[0][0].data.user.userDetails.siteAdmin).to.equal(false);
    expect(writeQuerySpy.args[0][0].data.user.groups.total).to.equal(2);
    expect(writeQuerySpy.args[0][0].data.user.groups.groups).to.deep.equal([
      {
        __typename: 'Group',
        id: 'DUMMY-GROUP',
        description: 'Fake group description',
        sitePrivilege: 'NONE',
      },
      trustedUserGroup,
    ]);
  });

  it('Successfully assign Basic User role to a Trusted User', () => {
    const currentUserId = 'DUMMY-TEST-TRUSTED-USER-ID';

    const variables = {
      cloudId: 'DUMMY-CLOUD-ID',
      userId: currentUserId,
      start: 1,
      count: 100,
    };

    const store = trustedUserStore();

    optimisticallyUpdateUserDetails(store, variables.cloudId, variables.userId, allGroups, Permissions.BASIC);

    expect(writeQuerySpy.callCount).to.equal(1);

    expect(writeQuerySpy.args[0][0].variables).to.deep.equal(variables);
    expect(writeQuerySpy.args[0][0].data.user.userDetails.trustedUser).to.equal(false);
    expect(writeQuerySpy.args[0][0].data.user.userDetails.siteAdmin).to.equal(false);
    expect(writeQuerySpy.args[0][0].data.user.groups.total).to.equal(1);
    expect(writeQuerySpy.args[0][0].data.user.groups.groups).to.deep.equal([
      {
        __typename: 'Group',
        id: 'DUMMY-GROUP',
        description: 'Fake group description',
        sitePrivilege: 'NONE',
      },
    ]);
  });

  it('Successfully assign Site Administrator role to a Basic User', () => {
    const currentUserId = 'DUMMY-TEST-BASIC-USER-ID';

    const variables: UserDetailsPageQueryVariables = {
      cloudId: 'DUMMY-CLOUD-ID',
      userId: currentUserId,
      start: 1,
      count: 100,
    };

    const store = basicUserStore();

    optimisticallyUpdateUserDetails(store, variables.cloudId, variables.userId, allGroups, Permissions.SITEADMIN);

    expect(writeQuerySpy.callCount).to.equal(1);

    expect(writeQuerySpy.args[0][0].variables).to.deep.equal(variables);
    expect(writeQuerySpy.args[0][0].data.user.userDetails.trustedUser).to.equal(false);
    expect(writeQuerySpy.args[0][0].data.user.userDetails.siteAdmin).to.equal(true);
    expect(writeQuerySpy.args[0][0].data.user.groups.total).to.equal(2);
    expect(writeQuerySpy.args[0][0].data.user.groups.groups).to.deep.equal([
      {
        __typename: 'Group',
        id: 'DUMMY-GROUP',
        description: 'Fake group description',
        sitePrivilege: 'NONE',
      },
      siteAdminGroup,
    ]);
  });

  it('Successfully assign Trusted User role to a Site Administrator', () => {
    const currentUserId = 'DUMMY-TEST-BASIC-USER-ID';

    const variables: UserDetailsPageQueryVariables = {
      cloudId: 'DUMMY-CLOUD-ID',
      userId: currentUserId,
      start: 1,
      count: 100,
    };

    const store = siteAdministratorStore();

    optimisticallyUpdateUserDetails(store, variables.cloudId, variables.userId, allGroups, Permissions.TRUSTED);
    expect(writeQuerySpy.callCount).to.equal(1);

    expect(writeQuerySpy.args[0][0].variables).to.deep.equal(variables);
    expect(writeQuerySpy.args[0][0].data.user.userDetails.trustedUser).to.equal(true);
    expect(writeQuerySpy.args[0][0].data.user.userDetails.siteAdmin).to.equal(false);
    expect(writeQuerySpy.args[0][0].data.user.groups.total).to.equal(2);
    expect(writeQuerySpy.args[0][0].data.user.groups.groups).to.deep.equal([
      {
        __typename: 'Group',
        id: 'DUMMY-GROUP',
        description: 'Fake group description',
        sitePrivilege: 'NONE',
      },
      trustedUserGroup,
    ]);
  });

  it('Successfully assign Site Administrator role to a Trusted User', () => {
    const currentUserId = 'DUMMY-TEST-BASIC-USER-ID';

    const variables: UserDetailsPageQueryVariables = {
      cloudId: 'DUMMY-CLOUD-ID',
      userId: currentUserId,
      start: 1,
      count: 100,
    };

    const store = trustedUserStore();

    optimisticallyUpdateUserDetails(store, variables.cloudId, variables.userId, allGroups, Permissions.SITEADMIN);
    expect(writeQuerySpy.callCount).to.equal(1);

    expect(writeQuerySpy.args[0][0].variables).to.deep.equal(variables);
    expect(writeQuerySpy.args[0][0].data.user.userDetails.trustedUser).to.equal(false);
    expect(writeQuerySpy.args[0][0].data.user.userDetails.siteAdmin).to.equal(true);
    expect(writeQuerySpy.args[0][0].data.user.groups.total).to.equal(2);
    expect(writeQuerySpy.args[0][0].data.user.groups.groups).to.deep.equal([
      {
        __typename: 'Group',
        id: 'DUMMY-GROUP',
        description: 'Fake group description',
        sitePrivilege: 'NONE',
      },
      siteAdminGroup,
    ]);
  });

  it('Successfully assign Basic User role to a Site Administrator', () => {
    const currentUserId = 'DUMMY-TEST-BASIC-USER-ID';

    const variables: UserDetailsPageQueryVariables = {
      cloudId: 'DUMMY-CLOUD-ID',
      userId: currentUserId,
      start: 1,
      count: 100,
    };

    const store = siteAdministratorStore();

    optimisticallyUpdateUserDetails(store, variables.cloudId, variables.userId, allGroups, Permissions.BASIC);
    expect(writeQuerySpy.callCount).to.equal(1);

    expect(writeQuerySpy.args[0][0].variables).to.deep.equal(variables);
    expect(writeQuerySpy.args[0][0].data.user.userDetails.trustedUser).to.equal(false);
    expect(writeQuerySpy.args[0][0].data.user.userDetails.siteAdmin).to.equal(false);
    expect(writeQuerySpy.args[0][0].data.user.groups.total).to.equal(1);
    expect(writeQuerySpy.args[0][0].data.user.groups.groups).to.deep.equal([{
      __typename: 'Group',
      id: 'DUMMY-GROUP',
      description: 'Fake group description',
      sitePrivilege: 'NONE',
    }]);
  });
});

let removeGroupWriteQuerySpy;
let readQuerySpy;
let readQueryStub;

const generateSiteAdminGroupStore = ({
  siteAdmin = false,
  trustedUser = false,
  additionalGroups = [] as UserDetailsGroup[],
  readSpy = false,
}): DataProxy => {
  const groups = [
    ...additionalGroups,
    {
      id: 'DUMMY-GROUP',
      description: 'Fake group description',
      sitePrivilege: 'NONE' as SitePrivilege,
      __typename: 'Group',
    },
  ];

  const storeQueryData: UserDetailsPageQuery = {
    user:  {
      __typename: 'UserDetails',
      userDetails:  {
        __typename: 'User',
        id: DUMMY_TEST_USER_ID,
        displayName: '',
        active: false,
        activeStatus: 'ENABLED',
        presence: 'string | null',
        system: false,
        trustedUser,
        siteAdmin,
      },
      groups:  {
        __typename: 'Groups',
        groups,
        total: groups.length,
      },
    },
    currentUser:  {
      __typename: 'User',
      id: DUMMY_TEST_USER_ID,
    },
  };

  readQueryStub = sinon.stub().returns(storeQueryData);

  return {
    readQuery: readSpy ? readQuerySpy : readQueryStub,
    writeQuery: removeGroupWriteQuerySpy,
    readFragment: () => null,
    writeFragment: () => null,
    writeData: () => null,
  };
};

describe('Optimistically remove the Site Admin role when the Site Admin group is removed for the user', () => {
  const sandbox = sinon.sandbox.create();
  const cloudId = 'DUMMY-CLOUD-ID';
  const userId = 'DUMMY-USER-ID';
  const variables: UserDetailsPageQueryVariables = {
    cloudId,
    userId,
    start: 1,
    count: 100,
  };

  beforeEach(() => {
    removeGroupWriteQuerySpy = sandbox.spy();
    readQuerySpy = sandbox.spy();
  });

  it('Does nothing when the store data does not exist ie. user Id mismatch', () => {
    const store = generateSiteAdminGroupStore({
      siteAdmin: false,
      additionalGroups: [siteAdminGroup],
      readSpy: true,
    });

    const userId2 = 'DUMMY-USER-ID-2';

    optimisticallyUpdatePermissionsForSiteAdminGroupRemoval(store, cloudId, userId2);
    expect(readQuerySpy.callCount).to.equal(1);
    expect(readQueryStub.called).to.equal(false);
    expect(removeGroupWriteQuerySpy.callCount).to.equal(0);
  });

  it('Does nothing when the role to remove is not Site Admin', () => {
    const store = generateSiteAdminGroupStore({
      siteAdmin: false,
      additionalGroups: [siteAdminGroup],
      readSpy: true,
    });

    optimisticallyUpdatePermissionsForSiteAdminGroupRemoval(store, cloudId, userId);

    expect(readQuerySpy.callCount).to.equal(1);
    expect(readQueryStub.called).to.equal(false);
    expect(removeGroupWriteQuerySpy.callCount).to.equal(0);
  });

  it('Does nothing when the current role in the Apollo store is not site admin', () => {
    const store = generateSiteAdminGroupStore({
      siteAdmin: false,
      additionalGroups: [siteAdminGroup],
      readSpy: true,
    });

    optimisticallyUpdatePermissionsForSiteAdminGroupRemoval(store, cloudId, userId);

    expect(readQuerySpy.callCount).to.equal(1);
    expect(readQueryStub.called).to.equal(false);
    expect(removeGroupWriteQuerySpy.callCount).to.equal(0);
  });

  it('Successfully removes the Site Admin permission when the group is removed and moves to Basic User', () => {
    const store = generateSiteAdminGroupStore({
      siteAdmin: true,
      trustedUser: false,
      additionalGroups: [siteAdminGroup],
    });

    optimisticallyUpdatePermissionsForSiteAdminGroupRemoval(store, cloudId, userId);

    expect(readQuerySpy.callCount).to.equal(0);
    expect(readQueryStub.called).to.equal(true);
    expect(removeGroupWriteQuerySpy.callCount).to.equal(1);
    expect(removeGroupWriteQuerySpy.args[0][0].variables).to.deep.equal(variables);
    expect(removeGroupWriteQuerySpy.args[0][0].data.user.userDetails.trustedUser).to.equal(false);
    expect(removeGroupWriteQuerySpy.args[0][0].data.user.userDetails.siteAdmin).to.equal(false);
    expect(removeGroupWriteQuerySpy.args[0][0].data.user.groups.total).to.equal(1);
    expect(removeGroupWriteQuerySpy.args[0][0].data.user.groups.groups).to.deep.equal([{
      __typename: 'Group',
      id: 'DUMMY-GROUP',
      description: 'Fake group description',
      sitePrivilege: 'NONE',
    }]);
  });

  it('Successfully removes the Site Admin permission when the group is removed and moves to Trusted User', () => {
    const store = generateSiteAdminGroupStore({
      siteAdmin: true,
      trustedUser: true,
      additionalGroups: [siteAdminGroup],
    });

    optimisticallyUpdatePermissionsForSiteAdminGroupRemoval(store, cloudId, userId);

    expect(readQuerySpy.callCount).to.equal(0);
    expect(readQueryStub.called).to.equal(true);
    expect(removeGroupWriteQuerySpy.callCount).to.equal(1);
    expect(removeGroupWriteQuerySpy.args[0][0].variables).to.deep.equal(variables);
    expect(removeGroupWriteQuerySpy.args[0][0].data.user.userDetails.trustedUser).to.equal(true);
    expect(removeGroupWriteQuerySpy.args[0][0].data.user.userDetails.siteAdmin).to.equal(false);
    expect(removeGroupWriteQuerySpy.args[0][0].data.user.groups.total).to.equal(1);
    expect(removeGroupWriteQuerySpy.args[0][0].data.user.groups.groups).to.deep.equal([{
      __typename: 'Group',
      id: 'DUMMY-GROUP',
      description: 'Fake group description',
      sitePrivilege: 'NONE',
    }]);
  });
});
