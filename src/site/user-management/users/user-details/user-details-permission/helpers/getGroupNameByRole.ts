import { SitePrivilege } from '../../../../../../schema/schema-types';
import { Permissions } from '../../../user-permission-section/permissions';
export const getGroupNameByRole = (role: Permissions): string => {
  switch (role) {
    case Permissions.TRUSTED:
      return 'administrators';
    case Permissions.SITEADMIN:
      return 'site-admins';
    default:
      throw Error('Undefined group name for the role');
  }
};

export const getRoleByGroupSitePrivilege = (sitePrivilege: SitePrivilege): Permissions | null => {
  if (sitePrivilege === 'SITE_ADMIN') {
    return Permissions.SITEADMIN;
  }

  return null;
};

export const SITE_ADMIN_SITE_PRIVILEGE = 'SITE_ADMIN';
