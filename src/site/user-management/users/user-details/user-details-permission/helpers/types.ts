import {
  ManagementAccess,
  OwnerType,
  Permission,
  SitePrivilege,
} from 'src/schema/schema-types';

export interface UserDetailsGroup {
  __typename: string;
  id: string;
  sitePrivilege: SitePrivilege;
  name?: string;
}

export interface UserGroups extends UserDetailsGroup {
  name: string;
  description: string | null;
  productPermissions: Array< {
    __typename: string,
    productId: string,
    productName: string,
    permissions: Array< Permission | null >,
  } >;
  defaultForProducts: Array< {
    __typename: string,
    productId: string,
    productName: string,
  } >;
  sitePrivilege: SitePrivilege;
  unmodifiable: boolean;
  userTotal: number;
  managementAccess: ManagementAccess;
  ownerType: OwnerType | null;
}
