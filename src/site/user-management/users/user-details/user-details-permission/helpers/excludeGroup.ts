import {
  UserDetailsGroup,
} from './types';

import {
  getGroupNameByRole,
  SITE_ADMIN_SITE_PRIVILEGE,
} from './getGroupNameByRole';

import { Permissions } from '../../../user-permission-section/permissions';

export const excludeGroup = (groups: UserDetailsGroup[], groupIdentifier: string): UserDetailsGroup[] => {
  return groups.filter(group => {
    let excluded = false;

    if (group.name) {
      excluded = (group.name === groupIdentifier);
    }

    if (!excluded && group.id) {
      excluded = (group.id === groupIdentifier);
    }

    if (!excluded && group.sitePrivilege && groupIdentifier === getGroupNameByRole(Permissions.SITEADMIN)) {
      excluded = (group.sitePrivilege === SITE_ADMIN_SITE_PRIVILEGE);
    }

    return !excluded;
  });
};
