import {
  ManagementAccess,
  SitePrivilege,
} from 'src/schema/schema-types';

import {
  UserGroups,
} from './types';

import {
  getGroupNameByRole,
  SITE_ADMIN_SITE_PRIVILEGE,
} from './getGroupNameByRole';

import { Permissions } from '../../../user-permission-section/permissions';

export const createNewGroup = (groupName: string, groupId: string): UserGroups => {
  return {
    id: groupId,
    name: groupName,
    description: groupName,
    productPermissions: [
      {
        __typename: 'productPermissions',
        productId: '-1',
        productName: '',
        permissions: [null],
      },
    ],
    defaultForProducts: [],
    sitePrivilege: groupName === getGroupNameByRole(Permissions.SITEADMIN) ? SITE_ADMIN_SITE_PRIVILEGE : 'NONE' as SitePrivilege,
    unmodifiable: false,
    userTotal: 0,
    managementAccess: 'ALL' as ManagementAccess,
    ownerType: null,
    __typename: 'Group',
  };
};
