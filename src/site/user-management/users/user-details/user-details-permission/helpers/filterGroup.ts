import {
  getGroupNameByRole,
  SITE_ADMIN_SITE_PRIVILEGE,
} from './getGroupNameByRole';

import {
  UserDetailsGroup,
} from './types';

import { Permissions } from '../../../user-permission-section/permissions';

export const filterGroup = (groups: UserDetailsGroup[], groupName: string) => {
  return groups.filter(group => {
    return (group.name === groupName)
            || (groupName === getGroupNameByRole(Permissions.SITEADMIN) && group.sitePrivilege === SITE_ADMIN_SITE_PRIVILEGE);
  });
};

export const hasTrustedUserGroup = (groups: UserDetailsGroup[]) => !!filterGroup(groups, getGroupNameByRole(Permissions.TRUSTED)).length;
export const hasSiteAdminGroup = (groups: UserDetailsGroup[]) => !!filterGroup(groups, getGroupNameByRole(Permissions.SITEADMIN)).length;
