import { expect } from 'chai';

import { Permissions } from '../../../user-permission-section/permissions';
import { filterGroup, hasSiteAdminGroup, hasTrustedUserGroup } from './filterGroup';
import {
  getGroupNameByRole,
  SITE_ADMIN_SITE_PRIVILEGE,
} from './getGroupNameByRole';
import {
  UserDetailsGroup,
} from './types';

const siteAdminGroupName = getGroupNameByRole(Permissions.SITEADMIN);
const trustedUserGroupName = getGroupNameByRole(Permissions.TRUSTED);

const siteAdminGroup: UserDetailsGroup = {
  id: siteAdminGroupName,
  name: siteAdminGroupName,
  __typename: 'Group',
  sitePrivilege: SITE_ADMIN_SITE_PRIVILEGE,
};

const anotherSiteAdminGroup: UserDetailsGroup = {
  id: 'ANOTHER-SITE-ADMIN-GROUP',
  name: 'ANOTHER-SITE-ADMIN-GROUP',
  __typename: 'Group',
  sitePrivilege: SITE_ADMIN_SITE_PRIVILEGE,
};

const trustedUserGroup: UserDetailsGroup = {
  id: trustedUserGroupName,
  name: trustedUserGroupName,
  __typename: 'Group',
  sitePrivilege: 'NONE',
};

const genericUserGroup: UserDetailsGroup = {
  id: 'TEST-GROUP',
  name: 'TEST-GROUP',
  __typename: 'Group',
  sitePrivilege: 'NONE',
};

describe('Filter the group specified from a list of groups', () => {
  it('Should return the Site Admins group list from a list of groups', () => {

    const emptyGroup = [];
    expect(filterGroup(emptyGroup, siteAdminGroupName)).to.deep.equal([]);
    expect(hasSiteAdminGroup(emptyGroup)).to.equal(false);

    const genericGroupList = [genericUserGroup];
    expect(filterGroup(genericGroupList, siteAdminGroupName)).to.deep.equal([]);
    expect(hasSiteAdminGroup(genericGroupList)).to.equal(false);

    const siteAdminGroupList = [siteAdminGroup];
    expect(filterGroup(siteAdminGroupList, siteAdminGroupName)).to.deep.equal([siteAdminGroup]);
    expect(hasSiteAdminGroup(siteAdminGroupList)).to.equal(true);

    const testListOfSiteAdminGroups = [siteAdminGroup, trustedUserGroup, genericUserGroup];
    expect(filterGroup([siteAdminGroup, trustedUserGroup, genericUserGroup], siteAdminGroupName)).to.deep.equal([siteAdminGroup]);
    expect(hasSiteAdminGroup(testListOfSiteAdminGroups)).to.equal(true);

    const testListOfMultipleSiteAdminGroups = [siteAdminGroup, anotherSiteAdminGroup, trustedUserGroup, genericUserGroup];
    expect(filterGroup(testListOfMultipleSiteAdminGroups, siteAdminGroupName)).to.deep.equal([siteAdminGroup, anotherSiteAdminGroup]);
    expect(hasSiteAdminGroup(testListOfMultipleSiteAdminGroups)).to.equal(true);
  });

  it('Should return the Trusted User admin group from a list of groups', () => {
    const emptyGroup = [];
    expect(filterGroup(emptyGroup, siteAdminGroupName)).to.deep.equal([]);
    expect(hasTrustedUserGroup(emptyGroup)).to.equal(false);

    const genericGroupList = [genericUserGroup];
    expect(filterGroup(genericGroupList, trustedUserGroupName)).to.deep.equal([]);
    expect(hasTrustedUserGroup(genericGroupList)).to.equal(false);

    const onlyOneGroupInList = [trustedUserGroup];
    expect(filterGroup(onlyOneGroupInList, trustedUserGroupName)).to.deep.equal([trustedUserGroup]);
    expect(hasTrustedUserGroup(onlyOneGroupInList)).to.equal(true);

    const testListOfUserGroup = [trustedUserGroup, genericUserGroup];
    expect(filterGroup(testListOfUserGroup, trustedUserGroupName)).to.deep.equal([trustedUserGroup]);
    expect(hasTrustedUserGroup(testListOfUserGroup)).to.equal(true);

    const anotherListOfGroups = [siteAdminGroup, trustedUserGroup, genericUserGroup];
    expect(filterGroup(anotherListOfGroups, trustedUserGroupName)).to.deep.equal([trustedUserGroup]);
    expect(hasTrustedUserGroup(anotherListOfGroups)).to.equal(true);

    const multipleInstancesOfTrustedUserGroup = [siteAdminGroup, trustedUserGroup, genericUserGroup, trustedUserGroup];
    expect(filterGroup(multipleInstancesOfTrustedUserGroup, trustedUserGroupName)).to.deep.equal([trustedUserGroup, trustedUserGroup]);
    expect(hasTrustedUserGroup(multipleInstancesOfTrustedUserGroup)).to.equal(true);
  });
});
