import { expect } from 'chai';

import { createNewGroup } from './createNewGroup';

import { Permissions } from '../../../user-permission-section/permissions';
import {
  getGroupNameByRole,
  SITE_ADMIN_SITE_PRIVILEGE,
} from './getGroupNameByRole';

const siteAdminGroupName = getGroupNameByRole(Permissions.SITEADMIN);
const trustedUserGroupName = getGroupNameByRole(Permissions.TRUSTED);
const trustedUserGroupId = 'TRUSTED-USER-GROUP-ID';
const siteAdminGroupId = 'SITE-ADMIN-GROUP-ID';
describe('Helper function that generates groups that are added to User Details for optimistic renders', () => {
  it('Should return group data for optimistically ading site-admin group to UI', () => {
    expect(createNewGroup(siteAdminGroupName, siteAdminGroupId)).to.deep.equal({
      id: siteAdminGroupId,
      name: siteAdminGroupName,
      description: siteAdminGroupName,
      productPermissions: [
        {
          __typename: 'productPermissions',
          productId: '-1',
          productName: '',
          permissions: [null],
        },
      ],
      defaultForProducts: [],
      sitePrivilege: SITE_ADMIN_SITE_PRIVILEGE,
      unmodifiable: false,
      userTotal: 0,
      managementAccess: 'ALL',
      ownerType: null,
      __typename: 'Group',
    });
  });

  it('Should return valid group data for other groups', () => {
    expect(createNewGroup(trustedUserGroupName, trustedUserGroupId)).to.deep.equal({
      id: trustedUserGroupId,
      name: trustedUserGroupName,
      description: trustedUserGroupName,
      productPermissions: [
        {
          __typename: 'productPermissions',
          productId: '-1',
          productName: '',
          permissions: [null],
        },
      ],
      defaultForProducts: [],
      sitePrivilege: 'NONE',
      unmodifiable: false,
      userTotal: 0,
      managementAccess: 'ALL',
      ownerType: null,
      __typename: 'Group',
    });
  });
});
