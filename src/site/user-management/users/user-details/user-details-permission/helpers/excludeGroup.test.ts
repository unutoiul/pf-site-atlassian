import { expect } from 'chai';

import { Permissions } from '../../../user-permission-section/permissions';
import { excludeGroup } from './excludeGroup';
import { getGroupNameByRole, SITE_ADMIN_SITE_PRIVILEGE } from './getGroupNameByRole';
import {
  UserDetailsGroup,
} from './types';

const siteAdminGroupName = getGroupNameByRole(Permissions.SITEADMIN);
const trustedUserGroupName = getGroupNameByRole(Permissions.TRUSTED);

const siteAdminGroup: UserDetailsGroup = {
  id: siteAdminGroupName,
  __typename: 'Group',
  sitePrivilege: SITE_ADMIN_SITE_PRIVILEGE,
};

const anotherSiteAdminGroup: UserDetailsGroup = {
  id: 'ANOTHER-SITE-ADMIN-GROUP',
  __typename: 'Group',
  sitePrivilege: SITE_ADMIN_SITE_PRIVILEGE,
};

const trustedUserGroup: UserDetailsGroup = {
  id: trustedUserGroupName,
  __typename: 'Group',
  sitePrivilege: 'NONE',
};

const genericUserGroup: UserDetailsGroup = {
  id: 'TEST-GROUP',
  __typename: 'Group',
  sitePrivilege: 'NONE',
};

describe('Exclude the group specified from a list of groups', () => {
  it('Should exclude Site admin group from an array of groups', () => {
    expect(excludeGroup([], siteAdminGroupName)).to.deep.equal([]);
    expect(excludeGroup([genericUserGroup], siteAdminGroupName)).to.deep.equal([genericUserGroup]);
    expect(excludeGroup([siteAdminGroup], siteAdminGroupName)).to.deep.equal([]);
    expect(excludeGroup([siteAdminGroup, trustedUserGroup, genericUserGroup], siteAdminGroupName)).to.deep.equal([trustedUserGroup, genericUserGroup]);
    expect(excludeGroup([siteAdminGroup, anotherSiteAdminGroup, trustedUserGroup, genericUserGroup], siteAdminGroupName)).to.deep.equal([trustedUserGroup, genericUserGroup]);
  });

  it('Should exclude Trusted User admin group from an array of groups', () => {
    expect(excludeGroup([trustedUserGroup], trustedUserGroupName)).to.deep.equal([]);
    expect(excludeGroup([trustedUserGroup, genericUserGroup], trustedUserGroupName)).to.deep.equal([genericUserGroup]);
    expect(excludeGroup([siteAdminGroup, trustedUserGroup, genericUserGroup], trustedUserGroupName)).to.deep.equal([siteAdminGroup, genericUserGroup]);
  });
});
