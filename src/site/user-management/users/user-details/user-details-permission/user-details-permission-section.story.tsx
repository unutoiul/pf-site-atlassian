import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';

import { createApolloClient } from '../../../../../apollo-client/apollo-client';

import { GroupListQuery } from '../../../../../../src/schema/schema-types';
import { Permissions } from '../../user-permission-section/permissions';

import { createMockAnalyticsClient, createMockIntlProp } from '../../../../../utilities/testing';
import rolePermissionsQuery from '../../users-quick-invite/users-quick-invite.query.graphql';
import { UserDetailsPermissionSectionImpl } from './user-details-permission-section';

const client = createApolloClient();
const defaultMutationProps = {
  changeUserRole: async () => ({
    loading: false,
    networkStatus: 200,
    stale: false,
    data: {
      changeUserRole: true,
    },
  }),
  addUsersToGroups: async () => ({
    loading: false,
    networkStatus: 200,
    stale: false,
    data: {
      addUsersToGroups: true,
    },
  }),
  removeUserFromGroup: async () => ({
    loading: false,
    networkStatus: 200,
    stale: false,
    data: {
      deleteUserFromGroup: true,
    },
  }),
};

const defaultProps = {
  cloudId: 'DUMMY-CLOUD-ID',
  userId: 'DUMMY-USER-ID',
  userStates: [],
  isCurrentUser: false,
  isUserSystemAdmin: false,
  isUserSystem: false,
  isUserWithNoAccess: false,
  userPermission: Permissions.BASIC,

  intl: createMockIntlProp(),
  analyticsClient: createMockAnalyticsClient(),
  showFlag: () => ({}),
  hideFlag: () => ({}),
  trustedUsersFeatureFlag: {
    value: true,
    isLoading: false,
  },
  handlePermissionChange: () => ({}),
  ...defaultMutationProps,
};

const generateGroupQuery = (): GroupListQuery => {
  return {
    groupList: {
      __typename: 'Groups',
      groups: [{
        __typename: 'Group',
        id: 'example-id',
        name: 'example-group',
      }],
    },
  };
};

function createTestComponent({ isCurrentUser = false, isUserSystem = false, isUserSystemAdmin = false, userPermission = Permissions.BASIC, loading = false, groupLoading = false, error = false }) {
  client.writeQuery({
    query: rolePermissionsQuery,
    variables: { cloudId: 'DUMMY-CLOUD-ID' },
    data: {
      rolePermissions: {
        __typename: 'Role Permissions',
        permissionIds: ['add-products', 'invite-users'],
      },
    },
  });

  let props = { ...defaultProps };

  if (loading) {
    props = { ...props, trustedUsersFeatureFlag: {
      isLoading: loading,
      value: true,
    }};
  }

  return (
    <div style={{ width: '600px', padding: '50px 0 0 50px' }}>
      <IntlProvider>
        <ApolloProvider client={client}>
          <UserDetailsPermissionSectionImpl
            {...props}
            isCurrentUser={isCurrentUser}
            isUserSystem={isUserSystem}
            isUserSystemAdmin={isUserSystemAdmin}
            userPermission={userPermission}
            groupData={{
              error,
              loading: groupLoading,
              ...generateGroupQuery(),
            } as any}
          />
        </ApolloProvider>
      </IntlProvider>
    </div>
  );
}

storiesOf('Site|User Details/User Permission Section/User Permission Section', module)
  .add('Loading Feature Flag', () => {
    return createTestComponent({ loading: true });
  })
  .add('Loading User Groups', () => {
    return createTestComponent({ groupLoading: true });
  })
  .add('Error', () => {
    return createTestComponent({ error: true });
  }).add('Current user', () => {
    return createTestComponent({ isCurrentUser: true });
  }).add('Selected user is System admin', () => {
    return createTestComponent({ isUserSystemAdmin: true });
  }).add('Selected user is System user', () => {
    return createTestComponent({ isUserSystem: true });
  }).add('Selected user is Site admin', () => {
    return createTestComponent({ userPermission: Permissions.SITEADMIN });
  }).add('Selected user is Trusted user', () => {
    return createTestComponent({ userPermission: Permissions.TRUSTED });
  }).add('Selected user is Basic user, can change permission', () => {
    return createTestComponent({ userPermission: Permissions.BASIC });
  });
