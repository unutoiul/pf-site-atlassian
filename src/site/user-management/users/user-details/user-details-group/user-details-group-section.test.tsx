import { expect } from 'chai';
import { mount, shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { MemoryRouter } from 'react-router';
import * as sinon from 'sinon';
import { Group, UserGroupsQueryVariables } from 'src/schema/schema-types';

import AkButton from '@atlaskit/button';
import AkSpinner from '@atlaskit/spinner';

import { FlagProvider } from 'common/flag';

import { getStart } from 'common/pagination';

import { addUserToGroupsUIEventData } from 'common/analytics';

import { createApolloClient } from '../../../../../apollo-client';
import { createMockIntlContext, createMockIntlProp } from '../../../../../utilities/testing';
import { UserDetailsGroupSection, UserDetailsGroupSectionImpl } from './user-details-group-section';
import userGroupsQuery from './user-details-group-section.query.graphql';
import { UserDetailsGroupTable } from './user-details-group-table';

import {
  findDropdownMenu,
  findHoverIcons,
  findRowsInTable,
  findTablesInWrapper,
} from '../../../../../../tests/integration-test/utils/utils';

describe('User details group section layout', () => {

  const sandbox = sinon.sandbox.create();

  let analyticsStub;

  beforeEach(() => {
    analyticsStub = {
      sendUIEvent: sandbox.spy(),
    };
  });

  function createTestComponent(
    { data = {}, isUserSystem = false, isUserSystemAdmin = false }:
    { data?: any, isUserSystem?: boolean, isUserSystemAdmin?: boolean},
  ): ShallowWrapper {
    return shallow(
      <UserDetailsGroupSectionImpl
        intl={createMockIntlProp()}
        analyticsClient={analyticsStub}
        userId="DUMMY-USER-ID"
        cloudId="DUMMY-CLOUD-ID"
        data={data}
        isUserSystem={isUserSystem}
        isUserSystemAdmin={isUserSystemAdmin}
        userStates={[]}
      />,
    );
  }

  describe('On loading', () => {

    const data = {
      loading: true,
    };

    const wrapper = createTestComponent({ data });

    it('Should have spinner', () => {
      expect(wrapper.find(AkSpinner).length).to.equal(1);
    });

    it('Add user to group button should be there, but disabled', () => {
      expect(wrapper.find(AkButton).length).to.equal(1);
      expect(wrapper.find(AkButton).props().isDisabled).to.equal(true);
    });
  });

  describe('On error', () => {

    const data = {
      error: true,
    };

    const wrapper = createTestComponent({ data });

    it('Add user to group should be there, but disabled', () => {
      expect(wrapper.find(AkButton).length).to.equal(1);
      expect(wrapper.find(AkButton).props().isDisabled).to.equal(true);
    });
  });

  describe('On default state', () => {

    const data = {
      loading: false,
      error: false,
      user: {
        groups: {
          groups: [
            {
              id: '04606f9d-9d4f-4f3e-95f1-f2f35fdea577',
              name: 'Cute Pugs Club',
              description: 'Something',
              defaultForProducts: [],
              productPermissions: [{
                productId: 'CONFLUENCE',
                productName: 'string',
                permissions: [
                  'MANAGE',
                ],
              }],
            },
          ],
        },
      },
    };

    it('Should have table', () => {
      const wrapper = createTestComponent({ data });
      expect(wrapper.find(UserDetailsGroupTable).length).to.equal(1);
    });

    it('Add user to group should be there and enabled', () => {
      const wrapper = createTestComponent({ data });
      expect(wrapper.find(AkButton).length).to.equal(1);
      expect(wrapper.find(AkButton).props().isDisabled).to.equal(false);
    });

    it('Add user to group is disabled for a system user', () => {
      const wrapper = createTestComponent({ data, isUserSystem: true });
      expect(wrapper.find(AkButton).length).to.equal(1);
      expect(wrapper.find(AkButton).props().isDisabled).to.equal(true);
    });

    it('Add user to group is disabled for a system admin', () => {
      const wrapper = createTestComponent({ data, isUserSystemAdmin: true });
      expect(wrapper.find(AkButton).length).to.equal(1);
      expect(wrapper.find(AkButton).props().isDisabled).to.equal(true);
    });

    it('should fire the addUserToGroups event when clicking on the "Add user to group" button', () => {
      const wrapper = createTestComponent({ data });
      expect(wrapper.find(AkButton).length).to.equal(1);
      wrapper.find(AkButton).simulate('click');
      expect(analyticsStub.sendUIEvent.called).to.equal(true);
      expect(analyticsStub.sendUIEvent.calledWith({
        cloudId: 'DUMMY-CLOUD-ID',
        data: addUserToGroupsUIEventData({
          userState: [],
          userId: 'DUMMY-USER-ID',
        }),
      })).to.equal(true);
    });
  });

  describe('Using mounted component', () => {

    const client = createApolloClient();

    function generateGroups(max: number, options?) {
      const groups: any[] = [];
      for (let i = 0; i < max; i++) {
        groups.push({
          id: i,
          name: `Cute Pugs Club ${i}`,
          userTotal: i,
          description: 'Some description',
          sitePrivilege: 'NONE',
          unmodifiable: false,
          defaultForProducts: [],
          productPermissions: [],
          managementAccess: 'ALL',
          ownerType: null,
          __typename: 'Group',
          ...options,
        } as Group);
      }

      return groups;
    }

    function writeDefaultQuery(max: number, options?) {
      client.writeQuery({
        query: userGroupsQuery,
        variables: {
          cloudId: 'cloudId',
          userId: 'userId',
          start: getStart(1, 5),
          count: 5,
        } as UserGroupsQueryVariables,
        data: {
          user: {
            groups: {
              total: '1',
              groups: generateGroups(max, options),
              __typename: 'PaginatedGroup',
            },
            userDetails: {
              id: `${Math.floor(Math.random() * 100)}`,
              active: true,
              displayName: `Prince Corgi`,
              email: 'corgi@happyfriends.paws',
              presence: '2017-07-10T14:00:00Z',
              activeStatus: 'ENABLED',
              system: false,
              hasVerifiedEmail: true,
              __typename: 'UserDetails',
            },
            __typename: 'User',
          },

        },
      });
    }

    function createMountedTestComponent() {
      return mount(
        <MemoryRouter>
          <ApolloProvider client={client}>
            <FlagProvider>
              <UserDetailsGroupSection
                cloudId="cloudId"
                userId="userId"
                isUserSystem={false}
                isUserSystemAdmin={false}
                userStates={[]}
              />
            </FlagProvider>
          </ApolloProvider>
        </MemoryRouter>,
        createMockIntlContext(),
      );
    }

    it('Should have all groups', () => {
      writeDefaultQuery(4, { description: 'Cute Pugs since 1995' });
      const wrapper = createMountedTestComponent();
      const table = findTablesInWrapper(wrapper);
      expect(table.length).to.equal(1);
      const rows = findRowsInTable(table);
      expect(rows.length).to.equal(5);
    });

    it('Should have hover icon when there is a description', () => {

      const NUMBER_OF_GROUPS = 2;

      writeDefaultQuery(NUMBER_OF_GROUPS, { description: 'Cute Pugs since 1995' });
      const wrapper = createMountedTestComponent();
      const table = findTablesInWrapper(wrapper);
      const icons = findHoverIcons(table);

      expect(icons.length).to.equal(NUMBER_OF_GROUPS);
    });

    it('Should have hover icon when it is a special access group', () => {
      const NUMBER_OF_GROUPS = 2;
      writeDefaultQuery(NUMBER_OF_GROUPS, { defaultForProducts: [{ productName: 'jira', productId: 'jira-core', __typename: 'Product' }] });
      const wrapper = createMountedTestComponent();
      const table = findTablesInWrapper(wrapper);
      const icons = findHoverIcons(table);

      expect(icons.length).to.equal(NUMBER_OF_GROUPS);
    });

    it('All groups should have a dropdown menu with items', () => {

      const NUMBER_OF_GROUPS = 2;

      writeDefaultQuery(NUMBER_OF_GROUPS, {
        description: 'Cute Pugs since 1995',
      });

      const wrapper = createMountedTestComponent();
      const table = findTablesInWrapper(wrapper);
      const dropdownItems = findDropdownMenu(table);

      expect(dropdownItems.length).to.equal(NUMBER_OF_GROUPS);
    });
  });
});
