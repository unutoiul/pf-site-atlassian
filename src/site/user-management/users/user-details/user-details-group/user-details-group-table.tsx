import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import { DynamicTableStateless as AkDynamicTableStateless, HeaderRow as AkHeaderRow } from '@atlaskit/dynamic-table';

import { GroupName } from 'common/group-name';
import { TruncatedField } from 'common/truncated-field';

import { AnalyticsProps } from '../../../user-management-analytics';

import { Group } from '../../../../../schema/schema-types';

import { DEFAULT_ROWS_PER_PAGE } from '../user-details-page/user-details-constants';
import { Hover, Name, Options } from '../user-details-page/user-details.styled';
import { UserDetailsGroupActions } from './user-details-group-actions';
import { GroupDetailHoverIcon } from './user-details-group-hover-icon';

export type AccessType = 'SYSTEM' | 'DEFAULT' | 'ADMIN' | 'USE';

const messages = defineMessages({
  error: {
    id: 'users.detail.group.section.table.error',
    description: 'Error trying to fetch groups from the backend',
    defaultMessage: `We had trouble trying to retrieve this user's groups, please refresh the page to try again.`,
  },
  emptyState: {
    id: 'users.detail.group.section.table.empty',
    description: 'No groups found',
    defaultMessage: `This user isn't currently a member of any groups.`,
  },
  headerName: {
    id: 'users.detail.group.section.table.group.name.header',
    description: 'Table header: Group name',
    defaultMessage: `Group`,
  },
  headerOptions: {
    id: 'users.detail.group.section.table.group.options.header',
    description: 'Table header: Options',
    defaultMessage: `Options`,
  },
});

interface UserDetailsGroupTableProps {
  groups: Group[];
  cloudId: string;
  userId: string;
  currentPage: number;
  isOptionDisabled: boolean;
  onSetPage(pageNumber: number): void;
}

export class UserDetailsGroupTableImpl extends React.Component<UserDetailsGroupTableProps & AnalyticsProps & InjectedIntlProps> {

  public render() {

    const { formatMessage } = this.props.intl;
    const { cloudId, userId } = this.props;

    const head: AkHeaderRow = {
      cells: [
        { key: 'header.group.name', content: formatMessage(messages.headerName) },
        { key: 'header.group.special.attributes', content: undefined },
        { key: 'header.group.options', content: formatMessage(messages.headerOptions) },
      ],
    };

    const rows = this.props.groups.map(group => {

      if (!group) {
        return {
          key: 'empty-row',
          cells: [
            {
              key: 'group.name',
              content: null,
            },
            {
              key: 'group.description',
              content: null,
            },
            {
              key: 'group.options',
              content: null,
            },
          ],
        };
      }

      const { id, name, description, sitePrivilege: groupSitePrivilege } = group;

      return ({
        key: `group.row.${id}`,
        cells: [
          {
            key: `group.name.${id}`,
            content: (
              <Name>
                <TruncatedField maxWidth={'100%'}>
                  <GroupName name={group.name} managementAccess={group.managementAccess} ownerType={group.ownerType} />
                </TruncatedField>
              </Name>
            ),
          },
          {
            key: `group.description.${id}`,
            content: (
              <Hover>
                <GroupDetailHoverIcon
                  description={description ? description : undefined}
                  accessAttributes={UserDetailsGroupTableImpl.determineAccessType(group)}
                />
              </Hover>
            ),
          },
          {
            key: `group.options.${id}`,
            content: (
              <Options>
                <UserDetailsGroupActions
                  userId={userId}
                  groupName={name}
                  groupId={id}
                  groupSitePrivilege={groupSitePrivilege}
                  cloudId={cloudId}
                  currentPage={this.props.currentPage}
                  userStates={this.props.userStates}
                  isOptionDisabled={this.props.isOptionDisabled}
                />
              </Options>
            ),
          },
        ],
      });
    });

    return (
      <div>
        {rows.length !== 0 &&
          <AkDynamicTableStateless
            rowsPerPage={DEFAULT_ROWS_PER_PAGE}
            onSetPage={this.props.onSetPage}
            head={head}
            page={this.props.currentPage}
            rows={rows}
            // The empty state applies default styles, which are inconsistent with our version of the design. We instead
            // implement a custom empty view outside of this component.
            emptyView={undefined}
          />
        }
        {rows.length === 0 && <span>{formatMessage(messages.emptyState)}</span>}
      </div>
    );
  }

  private static determineAccessType({ defaultForProducts, productPermissions }: Group): AccessType[] {
    const accessTypes: AccessType[] = [];

    if (defaultForProducts && defaultForProducts.length > 0) {
      accessTypes.push('DEFAULT');
    }

    const isAdmin = productPermissions && productPermissions.filter(
      product => product && product.permissions && product.permissions.includes('MANAGE'),
    ).length;

    const isUse = productPermissions && productPermissions.filter(
      product => product && product.permissions && product.permissions.includes('WRITE'),
    ).length;

    if (isAdmin) {
      accessTypes.push('ADMIN');
    }

    if (isUse) {
      accessTypes.push('USE');
    }

    return accessTypes;
  }
}

export const UserDetailsGroupTable = injectIntl(UserDetailsGroupTableImpl);
