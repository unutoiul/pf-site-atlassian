import { DataProxy } from 'apollo-cache';

import { Group as PickerGroup } from 'common/picker/group';

import { Group, UserGroupsQuery, UserGroupsQueryVariables } from '../../../../../schema/schema-types';
import userGroupsQuery from './user-details-group-section.query.graphql';

function readUserGroupsQuery(store: DataProxy, variables: UserGroupsQueryVariables) {
  try {
    return store.readQuery({
      query: userGroupsQuery,
      variables,
    }) as UserGroupsQuery;
  } catch (e) {
    return undefined;
  }
}

function createPartiallyFilledGroup({ id, displayName, description }): Group & { __typename: 'Group' } {
  return {
    id,
    name: displayName,
    description,
    productPermissions: [],
    defaultForProducts: [],
    sitePrivilege: displayName === 'site-admins' ? 'SITE_ADMIN' : 'NONE',
    unmodifiable: false,
    userTotal: 0,
    managementAccess: 'ALL',
    ownerType: null,
    __typename: 'Group',
  };
}

function sortGroups(a: Group, b: Group) {
  const aName = a.name.toLowerCase();
  const bName = b.name.toLowerCase();

  return aName < bName ? -1 : aName > bName ? 1 : 0;
}

export function optimisticallyRemoveGroup(store: DataProxy, variables: UserGroupsQueryVariables, groupId: string): void {

  const groupsPageResult = readUserGroupsQuery(store, variables);

  if (!groupsPageResult) {
    return;
  }

  store.writeQuery({
    query: userGroupsQuery,
    variables,
    data: {
      user: {
        groups: {
          total: Math.max(0, groupsPageResult.user.groups.total - 1),
          groups: (groupsPageResult.user.groups.groups || []).filter(group => group.id !== groupId),
          __typename: 'PaginatedGroups',
        },
        __typename: 'UserDetails',
      },
    },
  });
}

export function optimisticallyAddGroup(store: DataProxy, variables: UserGroupsQueryVariables, groups: PickerGroup[]): void {

  const groupsPageResult = readUserGroupsQuery(store, variables);

  if (!groupsPageResult) {
    return;
  }

  const currentGroups = groupsPageResult.user.groups.groups || [];
  const addedGroups = groups.map(createPartiallyFilledGroup);

  store.writeQuery({
    query: userGroupsQuery,
    variables,
    data: {
      user: {
        groups: {
          total: groupsPageResult.user.groups.total + addedGroups.length,
          groups: [...addedGroups, ...currentGroups].sort(sortGroups),
          __typename: 'PaginatedGroups',
        },
        __typename: 'UserDetails',
      },
    },
  });
}
