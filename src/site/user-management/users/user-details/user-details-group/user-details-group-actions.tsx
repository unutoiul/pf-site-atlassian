import { DataProxy } from 'apollo-cache';
import * as React from 'react';
import { graphql } from 'react-apollo';
import { ChildProps } from 'react-apollo/types';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';
import { SitePrivilege } from 'src/schema/schema-types/schema-types';

import AkButton from '@atlaskit/button';
import AkDropdownMenu, { DropdownItem as AkDropdownItem } from '@atlaskit/dropdown-menu';
import AkMoreIcon from '@atlaskit/icon/glyph/more';

import { AnalyticsClientProps, cancelRemoveUserFromGroupUIEventData, confirmRemoveUserFromGroupUIEventData, removedUserFromGroupTrackEventData, removeUserFromGroupUIEventData, viewGroupUIEventData, withAnalyticsClient } from 'common/analytics';
import { createErrorIcon, createSuccessIcon } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';
import { getStart } from 'common/pagination';

import { AnalyticsProps } from '../../../user-management-analytics';

import { RemoveUserFromGroupMutation, RemoveUserFromGroupMutationVariables } from '../../../../../schema/schema-types';
import { util } from '../../../../../utilities/admin-hub';
import { DeleteMemberDialog } from '../../../groups/delete-member-dialog';
import { Permissions } from '../../user-permission-section/permissions';
import { DEFAULT_ROWS_PER_PAGE, STARTING_PAGE } from '../user-details-page/user-details-constants';
import { getGroupNameByRole, getRoleByGroupSitePrivilege } from '../user-details-permission/helpers/getGroupNameByRole';
import { optmisticallyRemoveGroupFromUser } from '../user-details-permission/user-details-permission-group-query.updater';
import { optimisticallyUpdatePermissionsForSiteAdminGroupRemoval } from '../user-details-permission/user-details-permission-query.updater';
import { optimisticallyRemoveGroup } from './user-details-group.query.updater';
import removeUserFromGroup from './user-details-remove-user.mutation.graphql';

const messages = defineMessages({
  remove: {
    id: 'users.detail.group.section.dropdown.actions.remove',
    description: 'Heading for the groups section of the user details page',
    defaultMessage: 'Remove user from group',
  },
  view: {
    id: 'users.detail.group.section.dropdown.actions.view.group',
    description: 'Heading for the groups section of the user details page',
    defaultMessage: 'View group',
  },
  success: {
    id: 'users.detail.group.section.success.remove',
    description: 'Heading for the groups section of the user details page',
    defaultMessage: 'The user has been removed from the group',
  },
  error: {
    id: 'users.detail.group.section.error.remove',
    description: 'Heading for the groups section of the user details page',
    defaultMessage: 'We had trouble trying to remove the user from the group, please try again',
  },
  dropdownLabel: {
    id: 'users.detail.group.section.dropdown.label',
    description: 'Label for the dropdown button to perform user actions',
    defaultMessage: 'Click on this button to display user actions',
  },
});

interface UserDetailsGroupActionsOwnProps {
  cloudId: string;
  currentPage: number;
  userId: string;
  groupId: string;
  groupName: string;
  groupSitePrivilege: SitePrivilege;
  isOptionDisabled: boolean;
}

interface UserDetailsGroupSectionState {
  isRemoveUserFromGroupDialogOpen: boolean;
  isRemovingUser: boolean;
}

type UserDetailsGroupActionsProps = UserDetailsGroupActionsOwnProps & AnalyticsProps & InjectedIntlProps & FlagProps & AnalyticsClientProps;

export class UserDetailsGroupActionsImpl extends React.Component<ChildProps<UserDetailsGroupActionsProps & RouteComponentProps<{}>, RemoveUserFromGroupMutation>, UserDetailsGroupSectionState> {

  public readonly state: Readonly<UserDetailsGroupSectionState> = {
    isRemoveUserFromGroupDialogOpen: false,
    isRemovingUser: false,
  };

  public render() {

    const { formatMessage } = this.props.intl;
    const { groupName, groupId, cloudId } = this.props;

    const dropdownButton = (
      <AkButton
        appearance="subtle"
        iconAfter={<AkMoreIcon label={formatMessage(messages.dropdownLabel)} />}
      />
    );

    return (
      <React.Fragment>
        <AkDropdownMenu trigger={dropdownButton} position="bottom right">
          <AkDropdownItem onClick={this.onGroupClick} href={`${util.siteAdminBasePath}/s/${cloudId}/groups/${groupId}`}>
            {formatMessage(messages.view)}
          </AkDropdownItem>
          <AkDropdownItem isDisabled={this.props.isOptionDisabled} onClick={this.openDialog}>
            {formatMessage(messages.remove)}
          </AkDropdownItem>
        </AkDropdownMenu>
        <DeleteMemberDialog
          groupName={groupName}
          isOpen={this.state.isRemoveUserFromGroupDialogOpen}
          onDismiss={this.closeDialog}
          onRemove={this.onRemove}
          isRemovingUser={this.state.isRemovingUser}
        />
      </React.Fragment>
    );
  }

  private onGroupClick = (e: React.MouseEvent<HTMLAnchorElement>) => {
    e.preventDefault();
    this.sendViewGroupUIEventAnalytics();
    this.props.history.push(e.currentTarget.getAttribute('href')!);
  };

  private sendViewGroupUIEventAnalytics = () => {
    this.props.analyticsClient.sendUIEvent({
      cloudId: this.props.cloudId,
      data: viewGroupUIEventData({
        userState: this.props.userStates,
        userId: this.props.userId,
      }),
    });
  }

  private sendRemovedUserFromGroupTrackEventAnalytics = () => {
    this.props.analyticsClient.sendTrackEvent({
      cloudId: this.props.cloudId,
      data: removedUserFromGroupTrackEventData({
        userState: this.props.userStates,
        groupId: this.props.groupId,
        userId: this.props.userId,
      }),
    });
  }

  private sendRemoveUserFromGroupUIEventAnalytics = () => {
    this.props.analyticsClient.sendUIEvent({
      cloudId: this.props.cloudId,
      data: removeUserFromGroupUIEventData({
        userState: this.props.userStates,
        userId: this.props.userId,
      }),
    });
  }

  private sendConfirmRemoveUserFromGroupUIEventAnalytics = () => {
    this.props.analyticsClient.sendUIEvent({
      cloudId: this.props.cloudId,
      data: confirmRemoveUserFromGroupUIEventData({
        userState: this.props.userStates,
        userId: this.props.userId,
      }),
    });
  }

  private sendCancelRemoveUserFromGroupUIEventAnalytics = () => {
    this.props.analyticsClient.sendUIEvent({
      cloudId: this.props.cloudId,
      data: cancelRemoveUserFromGroupUIEventData({
        userState: this.props.userStates,
        userId: this.props.userId,
      }),
    });
  }

  private onRemove = (): void => {

    this.sendConfirmRemoveUserFromGroupUIEventAnalytics();

    const { formatMessage } = this.props.intl;
    const { cloudId, groupId, userId, mutate, showFlag } = this.props;

    if (!mutate) {
      return;
    }

    this.setState({ isRemovingUser: true });

    mutate({
      variables: {
        cloudId,
        groupId,
        userId,
      } as RemoveUserFromGroupMutationVariables,
      update: (store: DataProxy) => {
        const groupsPageQueryVariables = {
          userId,
          cloudId,
          start: getStart(STARTING_PAGE, DEFAULT_ROWS_PER_PAGE),
          count: DEFAULT_ROWS_PER_PAGE,
        };
        optimisticallyRemoveGroup(store, groupsPageQueryVariables, groupId);

        const removedRole = getRoleByGroupSitePrivilege(this.props.groupSitePrivilege);
        if (removedRole !== Permissions.SITEADMIN) {
          return;
        }
        optmisticallyRemoveGroupFromUser(store, cloudId, userId, getGroupNameByRole(removedRole));

        // Variables have to match the parent query at: '../user-details-page/user-details-page.tsx'
        optimisticallyUpdatePermissionsForSiteAdminGroupRemoval(store, cloudId, userId);
      },
    }).then(() => {
      this.sendRemovedUserFromGroupTrackEventAnalytics();
      showFlag({
        autoDismiss: true,
        icon: createSuccessIcon(),
        id: `users.add.user.to.group.success.flag.${Date.now()}`,
        title: formatMessage(messages.success),
      });
      this.closeDialog();
    }).catch(_ => {
      showFlag({
        autoDismiss: true,
        icon: createErrorIcon(),
        id: `users.add.user.to.group.error.flag.${Date.now()}`,
        title: formatMessage(messages.error),
      });
      this.closeDialog();
    });

    this.setState({
      isRemovingUser: false,
      isRemoveUserFromGroupDialogOpen: false,
    });
  }

  private openDialog = (): void => {
    this.sendRemoveUserFromGroupUIEventAnalytics();
    this.setState({ isRemoveUserFromGroupDialogOpen: true });
  }

  private closeDialog = (): void => {
    this.sendCancelRemoveUserFromGroupUIEventAnalytics();
  }
}

const withDeleteMemberMutation = graphql<UserDetailsGroupActionsProps, RemoveUserFromGroupMutation>(removeUserFromGroup);
export const UserDetailsGroupActions = withRouter(
  injectIntl(
    withFlag(
      withAnalyticsClient(
        withDeleteMemberMutation(
          UserDetailsGroupActionsImpl,
        ),
      ),
    ),
  ),
);
