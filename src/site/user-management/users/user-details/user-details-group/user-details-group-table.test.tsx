
import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import { DynamicTableStateless as AkDynamicTableStateless } from '@atlaskit/dynamic-table';

import { createMockIntlContext, createMockIntlProp } from '../../../../../utilities/testing';
import { STARTING_PAGE } from '../user-details-page/user-details-constants';
import { UserDetailsGroupTableImpl } from './user-details-group-table';

describe('User Details Group Table', () => {

  function createWrapper({ groups }) {
    return shallow(
      <UserDetailsGroupTableImpl
        currentPage={STARTING_PAGE}
        groups={groups}
        intl={createMockIntlProp()}
        cloudId={'cloud-id'}
        userId={'user-id'}
        onSetPage={undefined as any}
        isOptionDisabled={false}
        userStates={[]}
      />,
      createMockIntlContext(),
    );
  }

  describe('Empty view', () => {

    const wrapper = createWrapper({ groups: [] });

    it('Should show empty view when there are no users', () => {
      expect(wrapper.find('span').length).to.equal(1);
    });

    it('Table should not have a header', () => {
      expect(wrapper.find(AkDynamicTableStateless).length).to.equal(0);
    });
  });

  describe('With groups', () => {

    const wrapper = createWrapper({
      groups: [
        {
          id: '04606f9d-9d4f-4f3e-95f1-f2f35fdea577',
          name: 'Cute Pugs Club',
          defaultForProducts: [],
          productPermissions: [],
        },
        {
          id: '04606f9d-9d4f-4f3e-95f1-f2f35fdea578',
          name: 'Cute Pugs Club 2',
          defaultForProducts: [],
          productPermissions: [],
        },
      ],
    });

    it('Table should exist', () => {
      expect(wrapper.find(AkDynamicTableStateless).length).to.equal(1);
    });

    it('Empty view should not exist', () => {
      expect(wrapper.find('span').length).to.equal(0);
    });

    it('Renders groups', () => {
      expect(wrapper.find(AkDynamicTableStateless).length).to.equal(1);
      const row = (wrapper.find(AkDynamicTableStateless).props() as any).rows;
      expect(row.length).to.equal(2);
    });

  });

});
