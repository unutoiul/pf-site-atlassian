
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';
import StoryRouter from 'storybook-react-router';

import { createApolloClient } from '../../../../../apollo-client';
import { createMockAnalyticsClient, createMockIntlProp } from '../../../../../utilities/testing';
import { UserDetailsGroupSectionImpl } from './user-details-group-section';

const DEFAULT_CLOUD_ID = '04606f9d-9d4f-4f3e-95f1-f2f35fdea577';
const DEFAULT_USER_ID = '04606f9d-9d4f-4f3e-95f1-f2f35fdea577';

const DEFAULT_DESCRIPTION = `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
ullamco laboris nisi ut aliquip ex ea commodo consequat.`;

const defaultProps = {
  intl: createMockIntlProp(),
  analyticsClient: createMockAnalyticsClient(),
  cloudId: DEFAULT_CLOUD_ID,
  userId: DEFAULT_USER_ID,
  userStates: [],
};

const client = createApolloClient();

const Wrapper = ({ children }) => (
  <div style={{ width: '400px', padding: '200px' }}>
    <IntlProvider>
      <ApolloProvider client={client}>
        {children}
      </ApolloProvider>
    </IntlProvider>
  </div>
);

storiesOf('Site|User Details/Group Section', module)
  .addDecorator(StoryRouter())
  .add('Loading', () => {
    return (
      <Wrapper>
        <UserDetailsGroupSectionImpl
          {...defaultProps}
          data={{
            loading: true,
            error: false,
          } as any}
          isUserSystem={false}
          isUserSystemAdmin={false}
        />
      </Wrapper>
    );
  })
  .add('Error', () => {
    return (
      <Wrapper>
        <UserDetailsGroupSectionImpl
          {...defaultProps}
          data={{
            loading: false,
            error: true,
          } as any}
          isUserSystem={false}
          isUserSystemAdmin={false}
        />
      </Wrapper>
    );
  })
  .add('No groups', () => {
    return (
      <Wrapper>
        <UserDetailsGroupSectionImpl
          {...defaultProps}
          data={{
            loading: false,
            error: false,
            user: {
              groups: {
                total: 0,
                groups: [],
              },
            },
          } as any}
          isUserSystem={false}
          isUserSystemAdmin={false}
        />
      </Wrapper>
    );
  })
  .add('Default state', () => {
    return (
      <Wrapper>
        <UserDetailsGroupSectionImpl
          {...defaultProps}
          isUserSystem={false}
          isUserSystemAdmin={false}
          data={{
            loading: false,
            error: false,
            userDetails: {
              system: false,
            },
            user: {
              groups: {
                total: 0,
                groups: [
                  {
                    id: '04606f9d-9d4f-4f3e-95f1-f2f35fdea577',
                    name: 'Cute Pugs Club',
                    description: DEFAULT_DESCRIPTION,
                    defaultForProducts: [],
                    productPermissions: [{
                      productId: 'CONFLUENCE',
                      productName: 'string',
                      permissions: [
                        'MANAGE',
                      ],
                    }],
                    managementAccess: 'READ_ONLY',
                    ownerType: 'EXT_SCIM',
                  },
                  {
                    id: '6a0a3d7b-f05f-4ae3-9944-591dbcc271de',
                    name: 'Shiba Inu Petting Society',
                    description: DEFAULT_DESCRIPTION,
                    defaultForProducts: [{
                      productId: 'shiba-core',
                      productName: 'Shiba Core',
                    }],
                    productPermissions: [],
                  },
                  {
                    id: 'a7a8db88-52a9-4ca1-83b0-3b6a085ae228',
                    name: 'Border Collie Brushing Association',
                    description: DEFAULT_DESCRIPTION,
                    defaultForProducts: [{
                      productId: 'shiba-core',
                      productName: 'Shiba Core',
                    }],
                    productPermissions: [{
                      productId: 'CONFLUENCE',
                      productName: 'string',
                      permissions: [
                        'MANAGE',
                        'USE',
                      ],
                    }],
                    managementAccess: 'READ_ONLY',
                    ownerType: 'EXT_SCIM',
                  },
                  {
                    id: 'ac45489b-9c96-4cb3-8628-422d8d7d1a15',
                    description: DEFAULT_DESCRIPTION,
                    name: 'Corgi Runners Organization',
                    defaultForProducts: [],
                    productPermissions: [],
                  },
                ],
              },
            },
          } as any}
        />
      </Wrapper>
    );
  });
