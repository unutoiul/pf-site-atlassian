import * as React from 'react';
import { graphql } from 'react-apollo';
import { ChildProps } from 'react-apollo/types';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import AkButton from '@atlaskit/button';
import AkSpinner from '@atlaskit/spinner';

import { getStart } from 'common/pagination';

import { addUserToGroupsUIEventData, AnalyticsClientProps, withAnalyticsClient } from 'common/analytics';

import { AnalyticsProps } from '../../../user-management-analytics';

import { Group, UserGroupsQuery, UserGroupsQueryVariables } from '../../../../../../src/schema/schema-types';
import { UsersAddUserToGroupModal } from '../../user-list/users-add-user-to-group-modal';
import { DEFAULT_ROWS_PER_PAGE, STARTING_PAGE } from '../user-details-page/user-details-constants';
import { Content, Description, GroupsOuterDiv, Header, SectionTitle } from '../user-details-page/user-details.styled';
import userGroupsQuery from './user-details-group-section.query.graphql';
import { UserDetailsGroupTable } from './user-details-group-table';

const messages = defineMessages({
  heading: {
    id: 'users.detail.group.section.heading',
    description: 'Heading for the groups section of the user details page',
    defaultMessage: 'Groups',
  },
  button: {
    id: 'users.detail.group.section.button',
    description: 'Button text to show a modal to add users to a group',
    defaultMessage: 'Add to group',
  },
  description: {
    id: 'users.detail.group.section.description',
    description: 'Description text underneath the header to describe what this section does',
    defaultMessage: `Use groups to precisely manage a user's access & roles (e.g. give them access to a team's resources
    or make them a product admin).`,
  },
  error: {
    id: 'users.detail.group.section.error',
    description: 'Error message shown when we fail to load the list of groups the user is in',
    defaultMessage: `We had trouble trying to retrieve this user's groups, please refresh the page to try again.`,
  },
  userField: {
    id: 'users.detail.group.username.success.flag',
    description: 'Username used on add success flag',
    defaultMessage: `the user`,
  },
});

interface UserDetailsGroupSectionOwnProps {
  cloudId: string;
  userId: string;
  isUserSystem: boolean;
  isUserSystemAdmin: boolean;
}

interface UserDetailsGroupSectionState {
  currentPage: number;
  isAddUserToGroupDialogOpen: boolean;
}

type UserDetailsGroupSectionProps = ChildProps<AnalyticsProps & UserDetailsGroupSectionOwnProps & InjectedIntlProps & AnalyticsClientProps, UserGroupsQuery> ;

export class UserDetailsGroupSectionImpl extends React.Component<UserDetailsGroupSectionProps, UserDetailsGroupSectionState> {

  public readonly state: Readonly<UserDetailsGroupSectionState> = {
    currentPage: STARTING_PAGE,
    isAddUserToGroupDialogOpen: false,
  };

  public render() {

    const { formatMessage } = this.props.intl;

    const defaultHeadingContent = (
      <GroupsOuterDiv>
        <Header>
          <SectionTitle>
            <h2>
              {formatMessage(messages.heading)}
            </h2>
          </SectionTitle>
          <AkButton isDisabled={true}>
            {formatMessage(messages.button)}
          </AkButton>
        </Header>
        <Content>
          <Description>
            {formatMessage(messages.description)}
          </Description>
        </Content>
      </GroupsOuterDiv>
    );

    if (this.props.data && this.props.data.loading) {
      return (
        <GroupsOuterDiv>
          {defaultHeadingContent}
          <AkSpinner />
        </GroupsOuterDiv>
      );
    }

    if (!this.props.data || this.props.data.error || !this.props.data.user || !this.props.data.user.groups || !this.props.data.user.groups.groups) {
      return (
        <GroupsOuterDiv>
          {defaultHeadingContent}
          {formatMessage(messages.error)}
        </GroupsOuterDiv>
      );
    }

    const LoadedHeadingContent = (
      <div>
        <UsersAddUserToGroupModal
          cloudId={this.props.cloudId}
          userId={this.props.userId}
          currentPage={this.state.currentPage}
          username={formatMessage(messages.userField)}
          isOpen={this.state.isAddUserToGroupDialogOpen}
          onDialogDismissed={this.closeAddUserDialog}
          userStates={this.props.userStates}
        />
        <Header>
          <SectionTitle>
            <h2>
              {formatMessage(messages.heading)}
            </h2>
          </SectionTitle>
          <AkButton isDisabled={this.props.isUserSystem || this.props.isUserSystemAdmin} onClick={this.openAddUserDialog}>
            {formatMessage(messages.button)}
          </AkButton>
        </Header>
        <Content>
          <Description>
            {formatMessage(messages.description)}
          </Description>
        </Content>
      </div>
    );

    const groupsWithPlaceholders = this.getGroupsPaddedWithPlaceholders(
      getStart(this.state.currentPage, DEFAULT_ROWS_PER_PAGE),
      this.props.data.user.groups.total,
      this.props.data.user.groups.groups,
      false,
    );

    return (
      <GroupsOuterDiv>
        {LoadedHeadingContent}
        <UserDetailsGroupTable
          userId={this.props.userId}
          cloudId={this.props.cloudId}
          currentPage={this.state.currentPage}
          onSetPage={this.onSetPage}
          groups={groupsWithPlaceholders}
          isOptionDisabled={this.props.isUserSystem || this.props.isUserSystemAdmin}
          userStates={this.props.userStates}
        />
      </GroupsOuterDiv>
    );
  }

  private getGroupsPaddedWithPlaceholders = (start: number, total: number, groups: Group[], isLoading: boolean) => {
    const paddedGroups: Group[] = Array(total).fill(null);

    if (!isLoading) {
      paddedGroups.splice(start - 1, groups.length, ...groups);
    }

    return paddedGroups;
  };

  private openAddUserDialog = () => {
    this.sendAddUserToGroupsUIEventAnalytics();
    this.setState({ isAddUserToGroupDialogOpen: true });
  }

  private closeAddUserDialog = () => {
    this.setState({ isAddUserToGroupDialogOpen: false });
  }

  private sendAddUserToGroupsUIEventAnalytics = () => {
    this.props.analyticsClient.sendUIEvent({
      cloudId: this.props.cloudId,
      data: addUserToGroupsUIEventData({
        userState: this.props.userStates,
        userId: this.props.userId,
      }),
    });
  }

  private onSetPage = async (currentPage: number) => {

    if (!this.props.data) {
      return;
    }

    this.setState({ currentPage });

    await this.props.data.fetchMore({
      variables: {
        start: getStart(currentPage, DEFAULT_ROWS_PER_PAGE),
      },
      updateQuery: (previousResult: UserGroupsQuery, { fetchMoreResult }: { fetchMoreResult: UserGroupsQuery, variables: UserGroupsQueryVariables }): UserGroupsQuery => {
        return fetchMoreResult || previousResult;
      },
    });
  }
}

const withUserGroupsList = graphql<UserDetailsGroupSectionOwnProps & AnalyticsProps, UserGroupsQuery>(userGroupsQuery, {
  skip: ({ cloudId, userId }) => (!cloudId || !userId),
  options: ({ cloudId, userId }) => {

    return {
      variables: {
        cloudId,
        userId,
        start: getStart(STARTING_PAGE, DEFAULT_ROWS_PER_PAGE),
        count: DEFAULT_ROWS_PER_PAGE,
      } as UserGroupsQueryVariables,
    };
  },
});
export const UserDetailsGroupSection = withUserGroupsList(injectIntl(withAnalyticsClient(UserDetailsGroupSectionImpl)));
