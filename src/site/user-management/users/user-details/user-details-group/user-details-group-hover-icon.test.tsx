
import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import AkLozenge from '@atlaskit/lozenge';

import { InfoHover } from 'common/info-hover/info-hover';

import { AccessType } from './user-details-group-table';

import { createMockIntlContext, createMockIntlProp } from '../../../../../utilities/testing';
import { GroupDetailHoverIconImpl } from './user-details-group-hover-icon';

describe('Group Details Hover Icon', () => {

  function createIconWrapper(accessAttributes: AccessType[], description = 'Some odd description') {
    return shallow(
      <GroupDetailHoverIconImpl
        intl={createMockIntlProp()}
        description={description === '' ? undefined : description}
        accessAttributes={accessAttributes}
      />,
      createMockIntlContext(),
    );
  }

  it('Should not show hover if description and product access is not there', () => {
    const wrapper = createIconWrapper([], '');
    const infoHover = wrapper.find(InfoHover);
    expect(infoHover.length).to.equal(0);
  });

  it('Should show description on hover, if it is included', () => {
    const wrapper = createIconWrapper([]);
    const infoHover = wrapper.find(InfoHover);
    const hoverContent = shallow(infoHover.props().dialogContent);
    expect(hoverContent.html().includes('Some odd description')).to.equal(true);
  });

  it('Can display more than one lozenge', () => {
    const wrapper = createIconWrapper(['DEFAULT', 'USE']);
    const infoHover = wrapper.find(InfoHover);
    const hoverContent = shallow(infoHover.props().dialogContent);
    const lozenges = hoverContent.find(AkLozenge);
    expect(lozenges.length).to.equal(2);
  });

  it('Should display DEFAULT lozenge correctly', () => {
    const wrapper = createIconWrapper(['DEFAULT']);
    const infoHover = wrapper.find(InfoHover);
    const hoverContent = shallow(infoHover.props().dialogContent);
    const lozenges = hoverContent.find(AkLozenge);
    expect(lozenges.at(0).children().text().includes('Default access')).to.equal(true);
  });

  it('Should display ADMIN lozenge correctly', () => {
    const wrapper = createIconWrapper(['ADMIN']);
    const infoHover = wrapper.find(InfoHover);
    const hoverContent = shallow(infoHover.props().dialogContent);
    const lozenges = hoverContent.find(AkLozenge);
    expect(lozenges.at(0).children().text().includes('Admin access')).to.equal(true);
  });

  it('Should display USE lozenge correctly', () => {
    const wrapper = createIconWrapper(['USE']);
    const infoHover = wrapper.find(InfoHover);
    const hoverContent = shallow(infoHover.props().dialogContent);
    const lozenges = hoverContent.find(AkLozenge);
    expect(lozenges.at(0).children().text().includes('Use access')).to.equal(true);
  });
});
