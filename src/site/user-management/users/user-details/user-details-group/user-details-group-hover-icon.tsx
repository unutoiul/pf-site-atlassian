import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import AkLozenge from '@atlaskit/lozenge';

import { InfoHover } from 'common/info-hover';

import { Description, LozengeWrapper } from '../user-details-page/user-details.styled';
import { AccessType } from './user-details-group-table';

export interface GroupDetailHoverProps {
  description?: string;
  accessAttributes: AccessType[];
}

const messages = defineMessages({
  adminAccess: {
    id: 'users.detail.group.section.icon.admin.access',
    description: 'Lozenge to describe the type of special access this group has',
    defaultMessage: `Admin access`,
  },
  defaultAccess: {
    id: 'users.detail.group.section.icon.default.access',
    description: 'Lozenge to describe the type of special access this group has',
    defaultMessage: `Default access`,
  },
  useAccess: {
    id: 'users.detail.group.section.icon.use.access',
    description: 'Lozenge to describe the type of special access this group has',
    defaultMessage: `Use access`,
  },
  error: {
    id: 'users.detail.group.section.icon.error',
    description: 'Lozenge to describe an unexpected error',
    defaultMessage: `Unknown error`,
  },
});

export class GroupDetailHoverIconImpl extends React.Component<GroupDetailHoverProps & InjectedIntlProps> {

  public render() {

    const { accessAttributes, description } = this.props;

    if (accessAttributes.length === 0 && !description) {
      return null;
    }

    const dialogContent = (
      <div>
        {accessAttributes.map(accessType => {
          return (
            <LozengeWrapper key={accessType}>
              <AkLozenge>{this.convertAccessTypeToLozengeText(accessType)}</AkLozenge>
            </LozengeWrapper>
          );
        })}
        <p />
        {description && <Description>{this.props.description}</Description>}
      </div>
    );

    return (
      <InfoHover
        type="info"
        persistTime={0}
        dialogContent={dialogContent}
      />
    );
  }

  private convertAccessTypeToLozengeText(accessType: AccessType): string {

    const { formatMessage } = this.props.intl;

    switch (accessType) {
      case 'DEFAULT':
        return formatMessage(messages.defaultAccess);
      case 'ADMIN':
        return formatMessage(messages.adminAccess);
      case 'USE':
        return formatMessage(messages.useAccess);
      default:
        return formatMessage(messages.error);
    }
  }
}

export const GroupDetailHoverIcon = injectIntl(GroupDetailHoverIconImpl);
