import { expect } from 'chai';
import * as sinon from 'sinon';

import { Group as PickerGroup } from 'common/picker';

import { Group, ManagementAccess, UserGroupsQuery } from '../../../../../schema/schema-types';
import { optimisticallyAddGroup, optimisticallyRemoveGroup } from './user-details-group.query.updater';

describe('Optimistically remove and adding groups', () => {

  let writeQuerySpy;

  const sandbox = sinon.sandbox.create();

  beforeEach(() => {
    writeQuerySpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  function generateStore() {
    return {
      readQuery: sinon.stub().returns({
        user: {
          groups: {
            total: DUMMY_GROUP_DETAILS.length,
            groups: DUMMY_GROUP_DETAILS,
          },
        },
      } as UserGroupsQuery),
      writeQuery: writeQuerySpy,
    } as any;
  }

  const GROUP_TO_REMOVE_ID = 'DUMMY_GROUP_ID_1';
  const GROUP_TO_STAY_ID = 'DUMMY_GROUP_ID_2';
  const GROUP_3 = 'DUMMY_GROUP_ID_3';
  const GROUP_4 = 'DUMMY_GROUP_ID_4';
  const UNEXPECTED_GROUP_ID = 'UNEXPECTED_PARTY_GROUP';

  const PLACEHOLDER_GROUP_DATA = {
    description: 'Fake group description',
    name: 'Group name',
    productPermissions: [],
    defaultForProducts: [],
    sitePrivilege: 'NONE',
    unmodifiable: false,
    userTotal: 0,
    managementAccess: 'ALL',
    ownerType: null,
    __typename: 'Group',
  };
  const managementAccess: ManagementAccess = 'ALL';

  const DUMMY_GROUP_SIMPLE_DETAILS = [
    {
      id: GROUP_3,
      displayName: 'Group name',
      description: 'Fake group description',
      managementAccess,
      ownerType: null,
      sitePrivilege: 'NONE',
      unmodifiable: false,
    },
    {
      id: GROUP_4,
      displayName: 'Group name',
      description: 'Fake group description',
      managementAccess,
      ownerType: null,
      sitePrivilege: 'NONE',
      unmodifiable: false,
    },
  ] as PickerGroup[];

  const DUMMY_GROUP_DETAILS = [
    {
      id: GROUP_TO_REMOVE_ID,
      ...PLACEHOLDER_GROUP_DATA,
    } as Group,
    {
      id: GROUP_TO_STAY_ID,
      ...PLACEHOLDER_GROUP_DATA,
    } as Group,
  ];

  describe('Successfully removing a group', () => {

    const variables = {
      cloudId: 'DUMMY-CLOUD-ID',
      userId: 'DUMMY-USER-ID',
      start: 1,
      count: 5,
    };

    it('Total number decreases', () => {
      optimisticallyRemoveGroup(generateStore(), variables, GROUP_TO_REMOVE_ID);
      expect(writeQuerySpy.callCount).to.equal(1);
      expect(writeQuerySpy.args[0][0].data.user.groups.total).to.equal(DUMMY_GROUP_DETAILS.length - 1);
    });

    it('Group is removed, and other groups remain', () => {
      optimisticallyRemoveGroup(generateStore(), variables, GROUP_TO_REMOVE_ID);
      expect(writeQuerySpy.callCount).to.equal(1);
      expect(writeQuerySpy.args[0][0].data.user.groups.groups).to.deep.equal([{ id: GROUP_TO_STAY_ID, ...PLACEHOLDER_GROUP_DATA }]);
    });

    it('Trying to remove a group that does not exist does not affect current groups', () => {
      optimisticallyRemoveGroup(generateStore(), variables, UNEXPECTED_GROUP_ID);
      expect(writeQuerySpy.callCount).to.equal(1);
      expect(writeQuerySpy.args[0][0].data.user.groups.groups).to.deep.equal(DUMMY_GROUP_DETAILS);
    });
  });

  describe('Successfully adding a group', () => {

    const variables = {
      cloudId: 'DUMMY-CLOUD-ID',
      userId: 'DUMMY-USER-ID',
      start: 1,
      count: 5,
    };

    it('Total number increases', () => {
      optimisticallyAddGroup(generateStore(), variables, DUMMY_GROUP_SIMPLE_DETAILS);
      expect(writeQuerySpy.callCount).to.equal(1);
      expect(writeQuerySpy.args[0][0].data.user.groups.total).to.equal(DUMMY_GROUP_DETAILS.length +
        DUMMY_GROUP_SIMPLE_DETAILS.length);
    });

    it('Groups are added to the current groups', () => {
      optimisticallyAddGroup(generateStore(), variables, DUMMY_GROUP_SIMPLE_DETAILS);
      expect(writeQuerySpy.callCount).to.equal(1);
      expect(writeQuerySpy.args[0][0].data.user.groups.groups).to.deep.equal(
        [
          {
            id: GROUP_3,
            ...PLACEHOLDER_GROUP_DATA,
          },
          {
            id: GROUP_4,
            ...PLACEHOLDER_GROUP_DATA,
          },
          {
            id: GROUP_TO_REMOVE_ID,
            ...PLACEHOLDER_GROUP_DATA,
          },
          {
            id: GROUP_TO_STAY_ID,
            ...PLACEHOLDER_GROUP_DATA,
          },
        ]);
    });
  });

  describe('Unsuccessfully adding a group', () => {
    const store = {
      readQuery: sinon.stub().throws(),
      writeQuery: writeQuerySpy,
    } as any;

    const variables = {
      cloudId: 'DUMMY-CLOUD-ID',
      userId: 'DUMMY-USER-ID',
      start: 1,
      count: 5,
    };

    it('If fetching the current group fails, we don\'t try to write anything', () => {
      optimisticallyAddGroup(store, variables, DUMMY_GROUP_SIMPLE_DETAILS);
      expect(writeQuerySpy.callCount).to.equal(0);
    });
  });

  it('Unsuccessfully removing a group', () => {
    const store = {
      readQuery: sinon.stub().throws(),
      writeQuery: writeQuerySpy,
    } as any;

    const variables = {
      cloudId: 'DUMMY-CLOUD-ID',
      userId: 'DUMMY-USER-ID',
      start: 1,
      count: 5,
    };

    it('If fetching the current group fails, we don\'t try to write anything', () => {
      optimisticallyRemoveGroup(store, variables, GROUP_TO_REMOVE_ID);
      expect(writeQuerySpy.callCount).to.equal(0);
    });
  });
});
