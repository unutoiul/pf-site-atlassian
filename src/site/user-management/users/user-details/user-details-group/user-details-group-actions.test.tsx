import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import { DropdownItem as AkDropdownItem } from '@atlaskit/dropdown-menu';

import { cancelRemoveUserFromGroupUIEventData, confirmRemoveUserFromGroupUIEventData, removeUserFromGroupUIEventData, viewGroupUIEventData } from 'common/analytics';

import { util } from '../../../../../utilities/admin-hub';
import { createMockIntlProp } from '../../../../../utilities/testing';
import { DeleteMemberDialog } from '../../../groups/delete-member-dialog';
import { UserDetailsGroupActionsImpl } from './user-details-group-actions';

describe('Group actions', () => {

  const noop = () => null;

  const sandbox = sinon.sandbox.create();

  let analyticsStub;

  beforeEach(() => {
    analyticsStub = {
      sendUIEvent: sandbox.spy(),
    };
  });

  function createTestComponent({ isOptionDisabled = false }): ShallowWrapper {
    return shallow(
      <UserDetailsGroupActionsImpl
        intl={createMockIntlProp()}
        analyticsClient={analyticsStub}
        userStates={[]}
        groupId="DUMMY-GROUP-ID"
        groupName="DUMMY-GROUP-NAME"
        groupSitePrivilege="NONE"
        cloudId="DUMMY-CLOUD-ID"
        userId="DUMMY-USER-ID"
        currentPage={1}
        isOptionDisabled={isOptionDisabled}
        showFlag={noop}
        hideFlag={noop}
        history={{
          push: noop,
        } as any}
        match={{} as any}
        location={{} as any}
      />,
    );
  }

  describe('Options not disabled', () => {

    describe('When clicking on the removeUserFromGroup button', () => {

      it('Should pop open a modal dialog', () => {
        const wrapper = createTestComponent({});
        expect(wrapper.find(DeleteMemberDialog).props().isOpen).to.equal(false);
        const items = wrapper.find(AkDropdownItem);
        items.at(1).simulate('click');
        expect(wrapper.find(DeleteMemberDialog).props().isOpen).to.equal(true);
      });

      it('Should trigger the analytics', () => {
        const wrapper = createTestComponent({});
        expect(wrapper.find(DeleteMemberDialog).props().isOpen).to.equal(false);
        const items = wrapper.find(AkDropdownItem);
        items.at(1).simulate('click');
        expect(analyticsStub.sendUIEvent.called).to.equal(true);
        expect(analyticsStub.sendUIEvent.calledWith({
          cloudId: 'DUMMY-CLOUD-ID',
          data: removeUserFromGroupUIEventData({
            userState: [],
            userId: 'DUMMY-USER-ID',
          }),
        }));
      });

      describe('When clicking on buttons in the removeUserFromGroupModal', () => {

        it('When removing a user from a group, it fires the correct analytic', () => {
          const wrapper = createTestComponent({});
          const item = wrapper.find(DeleteMemberDialog);
          item.props().onRemove();
          expect(analyticsStub.sendUIEvent.called).to.equal(true);
          expect(analyticsStub.sendUIEvent.calledWith({
            cloudId: 'DUMMY-CLOUD-ID',
            data: confirmRemoveUserFromGroupUIEventData({
              userState: [],
              userId: 'DUMMY-USER-ID',
            }),
          }));
        });

        it('When cancelling removing a user from a group, it fires the correct analytic', () => {
          const wrapper = createTestComponent({});
          const item = wrapper.find(DeleteMemberDialog);
          item.props().onDismiss();
          expect(analyticsStub.sendUIEvent.called).to.equal(true);
          expect(analyticsStub.sendUIEvent.calledWith({
            cloudId: 'DUMMY-CLOUD-ID',
            data: cancelRemoveUserFromGroupUIEventData({
              userState: [],
              userId: 'DUMMY-USER-ID',
            }),
          }));
        });

      });

    });

    describe('Viewing group', () => {

      it('View group should have correct link', () => {
        const wrapper = createTestComponent({});
        const items = wrapper.find(AkDropdownItem);
        expect(items.at(0).props().href).to.equal(`${util.siteAdminBasePath}/s/DUMMY-CLOUD-ID/groups/DUMMY-GROUP-ID`);
      });

      it('Clicking on group should trigger the correct UI analytics', () => {
        const wrapper = createTestComponent({});
        const item = wrapper.find(AkDropdownItem).at(0);
        item.simulate('click', { preventDefault: noop, currentTarget: { getAttribute: (attr) => item.prop(attr) } });
        expect(analyticsStub.sendUIEvent.called).to.equal(true);
        expect(analyticsStub.sendUIEvent.calledWith({
          cloudId: 'DUMMY-CLOUD-ID',
          data: viewGroupUIEventData({
            userState: [],
            userId: 'DUMMY-USER-ID',
          }),
        }));
      });

    });

    it('Item dropdowns should have correct text', () => {
      const wrapper = createTestComponent({});
      const items = wrapper.find(AkDropdownItem);
      expect(items.at(0).children().text()).to.equal('View group');
      expect(items.at(1).children().text()).to.equal('Remove user from group');
    });
  });

  describe('Options disabled', () => {

    const wrapper = createTestComponent({ isOptionDisabled: true });

    it('View group should not be disabled', () => {
      const items = wrapper.find(AkDropdownItem);
      expect(items.at(0).props().isDisabled).to.equal(undefined);
    });

    it('Remove group should be disabled', () => {
      const items = wrapper.find(AkDropdownItem);
      expect(items.at(1).props().isDisabled).to.equal(true);
    });
  });

});
