import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';

import { createMockAnalyticsClient, createMockIntlProp } from '../../../../../utilities/testing';
import { SiteAccessImpl } from './user-details-site-access';

const noop = () => undefined;

const defaultProps = {
  userStates: [],
  analyticsClient: createMockAnalyticsClient(),
  intl: createMockIntlProp(),
  showFlag: noop,
  hideFlag: noop,
  updateUserStatus: noop,
};

storiesOf('Site|User Details/Access Section/Site Access Toggles', module)
  .add('Active', () => (
    <div style={{ width: '400px', padding: '50px 0 0 50px' }}>
     <IntlProvider>
      <SiteAccessImpl
        isCurrentUser={false}
        isSystem={false}
        isSystemAdministrator={false}
        cloudId={'cloudId'}
        userId={'userId'}
        presence={'2017-07-10T14:00:00Z'}
        active={true}
        activeStatus={'ENABLED'}
        {...defaultProps}
      />
      </IntlProvider>
    </div>
  )).add('Inactive - Userbase level', () => (
    <div style={{ width: '400px', padding: '50px 0 0 50px' }}>
     <IntlProvider>
      <SiteAccessImpl
        isCurrentUser={false}
        isSystem={false}
        isSystemAdministrator={false}
        cloudId={'cloudId'}
        userId={'userId'}
        presence={'2017-07-10T14:00:00Z'}
        active={false}
        activeStatus={'DISABLED'}
        {...defaultProps}
      />
      </IntlProvider>
    </div>
  ))
  .add('Inactive - Global level or invited', () => (
    <div style={{ width: '400px', padding: '50px 0 0 50px' }}>
     <IntlProvider>
      <SiteAccessImpl
        isCurrentUser={true}
        isSystem={false}
        isSystemAdministrator={false}
        cloudId={'cloudId'}
        userId={'userId'}
        presence={'2017-07-10T14:00:00Z'}
        active={false}
        activeStatus={'BLOCKED'}
        {...defaultProps}
      />
      </IntlProvider>
    </div>
  )).add('Invited', () => (
    <div style={{ width: '400px', padding: '50px 0 0 50px' }}>
     <IntlProvider>
      <SiteAccessImpl
        isCurrentUser={false}
        isSystem={false}
        isSystemAdministrator={false}
        cloudId={'cloudId'}
        userId={'userId'}
        active={false}
        activeStatus={'ENABLED'}
        {...defaultProps}
      />
      </IntlProvider>
    </div>
  ));
