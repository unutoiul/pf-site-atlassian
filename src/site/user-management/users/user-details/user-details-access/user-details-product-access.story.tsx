import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';

import { createMockAnalyticsClient, createMockIntlProp } from '../../../../../utilities/testing';
import { Permissions } from '../../user-permission-section/permissions';
import { ProductAccessSectionImpl } from './user-details-product-access';

storiesOf('Site|User Details/Access Section/Product Access Toggles', module)
  .add('With products', () => (
    <div style={{ width: '400px', padding: '50px 0 0 50px' }}>
      <IntlProvider>
        <ProductAccessSectionImpl
          showFlag={undefined as any}
          hideFlag={undefined as any}
          intl={createMockIntlProp()}
          analyticsClient={createMockAnalyticsClient()}
          userStates={[]}
          cloudId={'cloudId'}
          userId={'userId'}
          isDisabled={false}
          productAccess={[
            {
              productId: 'jira-core',
              productName: 'Jira Core',
              accessLevel: 'USE',
              presence: '2017-07-10T14:00:00Z',
            },
            {
              productId: 'jira-software',
              productName: 'Jira Software',
              accessLevel: 'USE',
              presence: '2017-07-10T14:00:00Z',
            },
          ]}
          userPermission={Permissions.BASIC}
        />
      </IntlProvider>
    </div>
  ));
