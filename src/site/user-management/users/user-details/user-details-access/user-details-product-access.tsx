import { ApolloQueryResult } from 'apollo-client';
import * as React from 'react';
import { ChildProps, graphql, MutationFunc } from 'react-apollo';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import {
  ProductAccess,
  RevokeProductAccessMutation,
  RevokeProductAccessMutationVariables,
  UserDetailsGrantProductAccessMutation,
  UserDetailsGrantProductAccessMutationVariables,
  UserGroupsQueryVariables,
} from 'src/schema/schema-types';

import { createErrorIcon, errorFlagMessages } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';
import { InfoHover } from 'common/info-hover';
import { getStart } from 'common/pagination';

import { AnalyticsClientProps, changedUserProductAccessTrackEventData, updatedUserProductsUIEventData, withAnalyticsClient } from 'common/analytics';

import { AccessToggle } from './user-details-access-toggle';

import { AnalyticsProps } from '../../../user-management-analytics';
import { Permissions } from '../../user-permission-section/permissions';
import userDetailsDirectGroupsQuery from '../user-details-group/user-details-group-section.query.graphql';
import { DEFAULT_ROWS_PER_PAGE, STARTING_PAGE } from '../user-details-page/user-details-constants';
import grantAccessMutation from './user-details-grant-product-access.mutation.graphql';
import revokeAccessMutation from './user-details-revoke-product-access.mutation.graphql';

export const messages = defineMessages({
  errorDescription: {
    id: 'users.details.product.access.section.error.flag.description',
    description: 'Description indicating that the admin could not grant or revoke access to a product for this user.',
    defaultMessage: `We couldn't change this user's access to this product. Refresh the page to try again.` ,
  },
  jiraCoreInfo: {
    id: 'users.details.product.access.jira-core.info',
    description: 'When the admin grants access to a Jira product, this info message displays indicated that Jira Core is included',
    defaultMessage: `{productName} is included with other products` ,
  },
});

interface ProductAccessProps {
  cloudId: string;
  userId: string;
  isDisabled: boolean;
  productAccess: ProductAccess[];
  userPermission: Permissions;
  refetch?(): Promise<ApolloQueryResult<any>>;
}

interface MutationProps {
  grantAccessToProducts?: MutationFunc<
    UserDetailsGrantProductAccessMutation,
    UserDetailsGrantProductAccessMutationVariables
  >;
  revokeAccessToProducts?: MutationFunc<
    RevokeProductAccessMutation,
    RevokeProductAccessMutationVariables
  >;
}

interface ProductAccessState {
  isLoading: boolean;
}

type ProductAccessSectionImplProps = ChildProps<MutationProps & ProductAccessProps & FlagProps & InjectedIntlProps & AnalyticsProps & AnalyticsClientProps, {}>;

export class ProductAccessSectionImpl extends React.Component<ProductAccessSectionImplProps, ProductAccessState> {

  public readonly state: Readonly<ProductAccessState> = {
    isLoading: false,
  };

  public render() {
    return this.props.productAccess.map(productAccess =>
      this.formatProductAccess(productAccess, this.hasJiraAccess(), this.hasSiteAdminAccess(), this.hasTrustedUserAccess(),
    ));
  }

  private formatProductAccess = (productAccess: ProductAccess, hasJiraAccess: boolean, hasSiteAdminAccess: boolean, hasTrustedUserAccess: boolean): React.ReactNode => {

    const hasJiraCoreAccess: boolean = productAccess.productId === 'jira-core' && hasJiraAccess;

    return (
      <AccessToggle
        onChecked={this.onChecked}
        isDisabled={this.props.isDisabled || this.state.isLoading || hasJiraCoreAccess || hasSiteAdminAccess || hasTrustedUserAccess}
        isChecked={productAccess.accessLevel === 'USE' || productAccess.accessLevel === 'ADMIN' || hasJiraCoreAccess || hasSiteAdminAccess || hasTrustedUserAccess}
        presence={productAccess.presence || undefined}
        key={productAccess.productId}
        name={productAccess.productName}
        id={productAccess.productId}
        icon={hasJiraCoreAccess ? this.jiraCoreInfoIcon(productAccess.productName) : undefined}
        accessType="product"
      />
    );
  };

  private jiraCoreInfoIcon = (productName: string): React.ReactNode => {
    return (
      <InfoHover
        type="info"
        persistTime={0}
        dialogContent={<span>{this.props.intl.formatMessage(messages.jiraCoreInfo, { productName })}</span>}
      />
    );
  }

  private hasJiraAccess = (): boolean => {
    return !!this.props.productAccess.some(productAccess =>
      productAccess.productId.includes('jira') &&
      productAccess.productId !== 'jira-core' &&
      productAccess.accessLevel === 'USE',
    );
  }

  private hasSiteAdminAccess = (): boolean => {
    return this.props.userPermission === Permissions.SITEADMIN;
  }

  private hasTrustedUserAccess = (): boolean => {
    return this.props.userPermission === Permissions.TRUSTED;
  }

  private sendChangedUserProductAccessTrackEventAnalytics = () => {
    this.props.analyticsClient.sendTrackEvent({
      cloudId: this.props.cloudId,
      data: changedUserProductAccessTrackEventData({
        userState: this.props.userStates,
        userId: this.props.userId,
      }),
    });
  }

  private sendUpdatedUserProductsUIEventAnalytics = (updateType: 'grant' | 'revoke') => {
    this.props.analyticsClient.sendUIEvent({
      cloudId: this.props.cloudId,
      data: updatedUserProductsUIEventData({
        userState: this.props.userStates,
        userId: this.props.userId,
        updateType,
      }),
    });
  }

  private onChecked = async ({ target: { checked, value } }: React.ChangeEvent<HTMLInputElement>) => {
    const { grantAccessToProducts, revokeAccessToProducts, intl: { formatMessage }, refetch } = this.props;

    if (!grantAccessToProducts || !revokeAccessToProducts || !refetch) {
      return;
    }

    this.sendUpdatedUserProductsUIEventAnalytics(checked ? 'grant' : 'revoke');

    this.setState({ isLoading: true });

    const mutateOptions = {
      variables: {
        cloudId: this.props.cloudId,
        users: [this.props.userId],
        productIds: [value],
      },
      refetchQueries: [{
        query: userDetailsDirectGroupsQuery,
        variables: {
          cloudId: this.props.cloudId,
          userId: this.props.userId,
          start: getStart(STARTING_PAGE, DEFAULT_ROWS_PER_PAGE),
          count: DEFAULT_ROWS_PER_PAGE,
        } as UserGroupsQueryVariables,
      }],
    };

    try {
      checked ? await grantAccessToProducts(mutateOptions) : await revokeAccessToProducts(mutateOptions);
      this.sendChangedUserProductAccessTrackEventAnalytics();
    } catch (_) {
      this.props.showFlag({
        autoDismiss: true,
        icon: createErrorIcon(),
        id: 'users.details.site.access.section.error.flag.description',
        title: formatMessage(errorFlagMessages.title),
        description: formatMessage(messages.errorDescription),
      });
    } finally {
      await refetch();
      this.setState({ isLoading: false });
    }
  };
}

const withGrantAccessMutation = graphql<ProductAccessProps, UserDetailsGrantProductAccessMutation, UserDetailsGrantProductAccessMutationVariables, MutationProps>(grantAccessMutation, {
  name: 'grantAccessToProducts',
});

const withRevokeAccessMutation = graphql<ProductAccessProps, RevokeProductAccessMutation, RevokeProductAccessMutationVariables, MutationProps>(revokeAccessMutation, {
  name: 'revokeAccessToProducts',
});

export const ProductAccessSection = withGrantAccessMutation(
  withRevokeAccessMutation(
    withFlag(
      injectIntl(
        withAnalyticsClient(
          ProductAccessSectionImpl,
        ),
      ),
    ),
  ),
);
