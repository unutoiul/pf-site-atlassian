import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import AkButton from '@atlaskit/button';
import AkSectionMessage from '@atlaskit/section-message';

import { reInviteUserUIEventData } from 'common/analytics';

import { ActiveStatus } from '../../../../../schema/schema-types';

import { createMockIntlProp, waitUntil } from '../../../../../utilities/testing';
import { AccessToggle } from './user-details-access-toggle';
import { SiteAccessImpl } from './user-details-site-access';

describe('Site Access', () => {

  const sandbox = sinon.sandbox.create();

  let analyticsStub;
  let showFlagSpy;
  let hideFlagSpy;
  let mutateSpy;

  beforeEach(() => {
    showFlagSpy = sandbox.spy();
    hideFlagSpy = sandbox.spy();
    mutateSpy = sandbox.stub().resolves();
    analyticsStub = {
      sendUIEvent: sandbox.spy(),
    };
  });

  afterEach(() => {
    sandbox.restore();
  });

  function createWrapper({
    active = true,
    activeStatus = 'ENABLED',
    isCurrentUser = false,
    isSystem = false,
    isSystemAdministrator = false,
    presence = '2018-09-10T21:49:02Z',
  }: {
    active?: boolean,
    activeStatus?: string,
    isCurrentUser?: boolean,
    isSystem?: boolean,
    isSystemAdministrator?: boolean,
    presence?: string | null,
  }) {
    return shallow(
      <SiteAccessImpl
        isSystem={isSystem}
        isSystemAdministrator={isSystemAdministrator}
        isCurrentUser={isCurrentUser}
        cloudId={'DUMMY-CLOUD-ID'}
        userId={'DUMMY-USER-ID'}
        active={active}
        mutate={mutateSpy}
        activeStatus={activeStatus as ActiveStatus}
        intl={createMockIntlProp()}
        analyticsClient={analyticsStub}
        userStates={[]}
        presence={presence || undefined}
        updateUserStatus={undefined as any}
        showFlag={showFlagSpy}
        hideFlag={hideFlagSpy}
      />,
    );
  }

  describe('For an invited user', () => {

    it('Should have resend invite button', () => {
      const wrapper = createWrapper({ active: true, presence: null });
      const additionalAction = shallow(wrapper.find(AccessToggle).prop('additionalAction') as any);
      expect(additionalAction.find(AkButton).length).to.equal(1);
      expect(additionalAction.find(AkButton).children().text()).to.equal('Resend invite');
    });

    it('Should trigger the correct UI analytic when the resend invite button is clicked', () => {
      const wrapper = createWrapper({ active: true, presence: null });
      const additionalAction = shallow(wrapper.find(AccessToggle).prop('additionalAction') as any);
      const button = additionalAction.find(AkButton);
      button.simulate('click');
      expect(analyticsStub.sendUIEvent.called).to.equal(true);
      expect(analyticsStub.sendUIEvent.calledWith({
        cloudId: 'DUMMY-CLOUD-ID',
        data: reInviteUserUIEventData({
          userState: [],
          userId: 'DUMMY-USER-ID',
        }),
      })).to.equal(true);
    });

    it('Should trigger the correct mutation when the resend invite button is clicked', () => {
      const wrapper = createWrapper({ active: true, presence: null });
      const additionalAction = shallow(wrapper.find(AccessToggle).prop('additionalAction') as any);
      const button = additionalAction.find(AkButton);
      button.simulate('click');
      expect(mutateSpy.called).to.equal(true);
      expect(mutateSpy.calledWith({
        variables: {
          cloudId: 'DUMMY-CLOUD-ID',
          userId: 'DUMMY-USER-ID',
        },
      })).to.equal(true);
    });

    it('should display a success flag when prompting to impersonate user is successful', async () => {
      mutateSpy = sandbox.stub().resolves();
      const wrapper = createWrapper({ active: true, presence: null });
      const additionalAction = shallow(wrapper.find(AccessToggle).prop('additionalAction') as any);
      const button = additionalAction.find(AkButton);
      button.simulate('click');
      expect(mutateSpy.called).to.equal(true);
      await waitUntil(() => showFlagSpy.callCount > 0);
      expect(showFlagSpy.calledWithMatch({ title: 'Reminder sent!' })).to.equal(true);
    });

    it('Should display an error flag when impersonating user is not successful', async () => {
      mutateSpy = sandbox.stub().rejects();
      const wrapper = createWrapper({ active: true, presence: null });
      const additionalAction = shallow(wrapper.find(AccessToggle).prop('additionalAction') as any);
      const button = additionalAction.find(AkButton);
      button.simulate('click');
      expect(mutateSpy.called).to.equal(true);
      await waitUntil(() => showFlagSpy.callCount > 0);
      expect(showFlagSpy.calledWithMatch({ title: 'Something went wrong' })).to.equal(true);
    });
  });

  describe('For an enabled user', () => {

    const wrapper = createWrapper({ active: true, activeStatus: 'ENABLED' });

    it('Has toggle', () => {
      expect(wrapper.find(AccessToggle).length).to.equal(1);
      expect(wrapper.find(AkSectionMessage).length).to.equal(0);
    });

    it('Toggle is not disabled, and checked', () => {
      expect(wrapper.find(AccessToggle).props().isDisabled).to.equal(false);
      expect(wrapper.find(AccessToggle).props().isChecked).to.equal(true);
    });
  });

  describe('For a (userbase) disabled user', () => {

    const wrapper = createWrapper({ active: false, activeStatus: 'DISABLED' });

    it('Has toggle', () => {
      expect(wrapper.find(AccessToggle).exists()).to.equal(true);
      expect(wrapper.find(AkSectionMessage).exists()).to.equal(false);
    });

    it('Toggle is not disabled, and unchecked', () => {
      expect(wrapper.find(AccessToggle).props().isDisabled).to.equal(false);
      expect(wrapper.find(AccessToggle).props().isChecked).to.equal(false);
    });
  });

  describe('For a (globally) disabled user', () => {

    const wrapper = createWrapper({ active: false, activeStatus: 'BLOCKED' });

    it('Has toggle', () => {
      expect(wrapper.find(AccessToggle).length).to.equal(1);
      expect(wrapper.find(AkSectionMessage).length).to.equal(1);
      expect(wrapper.find(AkSectionMessage).children().text().includes('disabled globally')).to.equal(true);
    });

    it('Toggle is disabled and not checked', () => {
      expect(wrapper.find(AccessToggle).props().isDisabled).to.equal(true);
      expect(wrapper.find(AccessToggle).props().isChecked).to.equal(false);
    });
  });

  describe('For a system administrator', () => {
    const wrapper = createWrapper({ isSystemAdministrator: true });

    it('Has toggle', () => {
      expect(wrapper.find(AccessToggle).length).to.equal(1);
      expect(wrapper.find(AkSectionMessage).length).to.equal(1);
      expect(wrapper.find(AkSectionMessage).children().text().includes('privileges cannot be modified')).to.equal(true);
    });

    it('Toggle is disabled and checked', () => {
      expect(wrapper.find(AccessToggle).props().isDisabled).to.equal(true);
      expect(wrapper.find(AccessToggle).props().isChecked).to.equal(true);
    });
  });

  describe('For a system user', () => {
    const wrapper = createWrapper({ isSystem: true });

    it('Has toggle', () => {
      expect(wrapper.find(AccessToggle).length).to.equal(1);
      expect(wrapper.find(AkSectionMessage).length).to.equal(1);
      expect(wrapper.find(AkSectionMessage).children().text().includes('privileges cannot be modified')).to.equal(true);
    });

    it('Toggle is disabled and checked', () => {
      expect(wrapper.find(AccessToggle).props().isDisabled).to.equal(true);
      expect(wrapper.find(AccessToggle).props().isChecked).to.equal(true);
    });
  });

  describe('For a current user', () => {
    const wrapper = createWrapper({ isCurrentUser: true });

    it('Has toggle', () => {
      expect(wrapper.find(AccessToggle).length).to.equal(1);
      expect(wrapper.find(AkSectionMessage).length).to.equal(1);
      expect(wrapper.find(AkSectionMessage).children().text().includes('via group memberships')).to.equal(true);
    });

    it('Toggle is disabled and checked', () => {
      expect(wrapper.find(AccessToggle).props().isDisabled).to.equal(true);
      expect(wrapper.find(AccessToggle).props().isChecked).to.equal(true);
    });
  });
});
