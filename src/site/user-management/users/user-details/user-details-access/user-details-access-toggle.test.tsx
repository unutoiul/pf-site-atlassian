import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import AkToggle from '@atlaskit/toggle';

import { createMockIntlProp } from '../../../../../utilities/testing';
import { AccessToggleImpl } from './user-details-access-toggle';
import { ToggleDescription } from './user-details-access.styled';

describe('Access toggle', () => {

  function createWrapper({ isChecked = false, isDisabled = false, presence = '2017-07-10T14:00:00Z', accessType = 'product' }) {
    return shallow(
      <AccessToggleImpl
        intl={createMockIntlProp()}
        id="dog-club"
        name="Golden Retrievers"
        presence={presence}
        isChecked={isChecked}
        isDisabled={isDisabled}
        onChecked={undefined as any}
        accessType={accessType as 'product' | 'site'}
        additionalAction={'Click me!'}
      />,
    );
  }

  it('Has toggle, name and description and additional action', () => {
    const wrapper = createWrapper({});
    expect(wrapper.html().includes('Golden Retrievers'));
    expect(wrapper.html().includes('Golden dogs'));
    expect(wrapper.html().includes('Click me!'));
    expect(wrapper.find(AkToggle).length).to.equal(1);
  });

  it('Access toggle checked makes toggle checked', () => {
    const wrapper = createWrapper({ isChecked: true });
    expect((wrapper.find(AkToggle) as any).props().isDefaultChecked).to.equal(true);
  });

  it('Access toggle unchecked makes toggle unchecked', () => {
    const wrapper = createWrapper({ isChecked: false });
    expect((wrapper.find(AkToggle) as any).props().isDefaultChecked).to.equal(false);
  });

  it('Access toggle disabled makes toggle disabled', () => {
    const wrapper = createWrapper({ isDisabled: false });
    expect((wrapper.find(AkToggle) as any).props().isDisabled).to.equal(false);
  });

  it('Access toggle enabled makes toggle enabled', () => {
    const wrapper = createWrapper({ isDisabled: true });
    expect((wrapper.find(AkToggle) as any).props().isDisabled).to.equal(true);
  });

  it('Access toggle with presence', () => {
    const wrapper = createWrapper({ presence: '2017-07-10T14:00:00Z', accessType: 'site' });
    const toggleDescription = wrapper.find(ToggleDescription);
    expect(toggleDescription.children().text().includes('Last seen on')).to.equal(true);
  });

  it('Access toggle without presence, access type = site', () => {
    const wrapper = createWrapper({ presence: null as any, accessType: 'site' });
    const toggleDescription = wrapper.find(ToggleDescription);
    expect(toggleDescription.children().text().includes('Never seen on this site')).to.equal(true);
  });

  it('Access toggle without presence, access type = product', () => {
    const wrapper = createWrapper({ presence: null as any, accessType: 'product' });
    const toggleDescription = wrapper.find(ToggleDescription);
    expect(toggleDescription.children().text().includes('Never used this product')).to.equal(true);
  });
});
