import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import AkLozenge from '@atlaskit/lozenge';

import { InfoHover } from 'common/info-hover';

import { SectionTitle } from '../user-details-page/user-details.styled';
import { LozengeWrapper } from './user-details-access.styled';

const messages = defineMessages({
  title: {
    id: 'users.details.access.title',
    description: 'Title of this component; denotes site access',
    defaultMessage: `Access`,
  },
  siteAdminLozenge: {
    id: 'users.access.title.site.admin.lozenge',
    description: 'Lozenge text to indicate that the user is a site admin',
    defaultMessage: `Site admin`,
  },
  invitedTitle: {
    id: 'user.details.access.hover.invited.user.title',
    description: 'Hover title indicating that the user has not accepted the invitation to join the site',
    defaultMessage: `This user hasn't accepted your invite yet`,
  },
  invitedDescription: {
    id: 'user.details.access.hover.invited.user.description',
    description: 'Hover description indicating that the user has not accepted the invitation to join the site',
    defaultMessage: `When they accept your invite, they will be able to start collaborating.`,
  },
  revokedAccessTitle: {
    id: 'user.details.access.hover.revoked.access.user.title',
    description: 'Hover title indicating that the user does not have access to the site',
    defaultMessage: 'This user has had their access to this site revoked',
  },
  revokedAccessDescription: {
    id: 'user.details.access.hover.revoked.access.user.description',
    description: 'Hover description indicating that the user has not accepted the invitation to join the site',
    defaultMessage: `To start collaborating, give them active access below.`,
  },
});

interface OwnProps {
  isInvited: boolean;
  isSystemUser: boolean;
  isSystemAdmin: boolean;
  isGloballyDisabled: boolean;
  isRevokedAccess: boolean;
  isSiteAdmin: boolean;
}

export class UserDetailsAccessTitleImpl extends React.Component<InjectedIntlProps & OwnProps> {
  public render() {

    const { formatMessage } = this.props.intl;

    const isSpecialUser = this.props.isSystemUser || this.props.isSystemAdmin || this.props.isGloballyDisabled;
    const isInfoIconShown = !isSpecialUser && (this.props.isInvited || this.props.isRevokedAccess);

    const siteAdminLozenge = (
      <LozengeWrapper>
        <AkLozenge>
          {this.props.intl.formatMessage(messages.siteAdminLozenge)}
        </AkLozenge>
      </LozengeWrapper>
    );

    const infoIcon = (
      <InfoHover
        type="info"
        persistTime={0}
        dialogContent={
          <React.Fragment>
            <h5>{formatMessage(this.props.isInvited ? messages.invitedTitle : messages.revokedAccessTitle)}</h5>
            <span>{formatMessage(this.props.isInvited ? messages.invitedDescription : messages.revokedAccessDescription)}</span>
          </React.Fragment>
        }
      />
    );

    return (
      <SectionTitle>
        <h2>{formatMessage(messages.title)}</h2>
        {this.props.isSiteAdmin && siteAdminLozenge}
        {isInfoIconShown && infoIcon}
      </SectionTitle>
    );
  }
}

export const UserDetailsAccessTitle = injectIntl(UserDetailsAccessTitleImpl);
