import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import { ProductAccess } from '../../../../../schema/schema-types';
import { createMockAnalyticsClient, createMockIntlProp } from '../../../../../utilities/testing';
import { Permissions } from '../../user-permission-section/permissions';
import userDetailsDirectGroupsQuery from '../user-details-group/user-details-group-section.query.graphql';
import { AccessProps, AccessToggle } from './user-details-access-toggle';
import { ProductAccessSectionImpl } from './user-details-product-access';

describe('Product Access Section', () => {

  const sandbox = sinon.sandbox.create();

  let showFlagSpy;
  let hideFlagSpy;

  let grantAccessToProductsSpy;
  let revokeAccessToProductsSpy;

  let refetchSpy;

  beforeEach(() => {
    showFlagSpy = sandbox.spy();
    hideFlagSpy = sandbox.spy();
    grantAccessToProductsSpy = sandbox.stub().resolves();
    revokeAccessToProductsSpy = sandbox.stub().resolves();
    refetchSpy = sandbox.spy();
  });

  function createWrapper(productAccess: ProductAccess[], isDisabled = false) {
    return shallow(
      <ProductAccessSectionImpl
        cloudId="cloud-id"
        userId="user-id"
        intl={createMockIntlProp()}
        isDisabled={isDisabled}
        productAccess={productAccess}
        grantAccessToProducts={grantAccessToProductsSpy}
        revokeAccessToProducts={revokeAccessToProductsSpy}
        showFlag={showFlagSpy}
        hideFlag={hideFlagSpy}
        refetch={refetchSpy}
        userStates={[]}
        analyticsClient={createMockAnalyticsClient()}
        userPermission={Permissions.BASIC}
      />,
    );
  }

  function simulateCheckAction(toggle: ShallowWrapper<AccessProps>, checked: boolean): void {
    toggle.props().onChecked({ target: { checked, value: 'conf' } });
  }

  it('Toggle should be checked if the access level of the product is USE', () => {
    const wrapper = createWrapper([
      {
        productId: 'productId',
        productName: 'Jira Core',
        accessLevel: 'USE',
        presence: '2017-07-10T14:00:00Z',
      },
    ]);

    expect(wrapper.find(AccessToggle).length).to.equal(1);
    expect(wrapper.find(AccessToggle).props().isChecked).to.equal(true);
  });

  it('Toggle should be unchecked if the access level of the product is not USE or ADMIN', () => {
    const wrapper = createWrapper([
      {
        productId: 'productId',
        productName: 'Jira Core',
        accessLevel: 'NONE',
        presence: '2017-07-10T14:00:00Z',
      },
    ]);

    expect(wrapper.find(AccessToggle).length).to.equal(1);
    expect(wrapper.find(AccessToggle).props().isChecked).to.equal(false);
  });

  it('Toggle should be disabled', () => {
    const wrapper = createWrapper([
      {
        productId: 'productId',
        productName: 'Jira Core',
        accessLevel: 'ADMIN',
        presence: '2017-07-10T14:00:00Z',
      },
    ], true);

    expect(wrapper.find(AccessToggle).length).to.equal(1);
    expect(wrapper.find(AccessToggle).props().isDisabled).to.equal(true);
  });

  it('Number of products should correspond to the number of toggles', () => {
    const wrapper = createWrapper([
      {
        productId: 'productId',
        productName: 'Jira Core',
        accessLevel: 'USE',
        presence: '2017-07-10T14:00:00Z',
      },
      {
        productId: 'productId',
        productName: 'Jira Core',
        accessLevel: 'USE',
        presence: '2017-07-10T14:00:00Z',
      },
    ]);

    expect(wrapper.find(AccessToggle).length).to.equal(2);
  });

  it('Jira Core should not be disabled if there is no other Jira Product', () => {
    const wrapper = createWrapper([
      {
        productId: 'jira-core',
        productName: 'Jira Core',
        accessLevel: 'USE',
        presence: '2017-07-10T14:00:00Z',
      },
    ]);

    wrapper.setState({ isLoading: false });

    expect(wrapper.find(AccessToggle).length).to.equal(1);
    expect((wrapper.find(AccessToggle).at(0) as any).props().isDisabled).to.equal(false);
    expect((wrapper.find(AccessToggle).at(0) as any).props().icon).to.equal(undefined);
  });

  it('Jira Core should be not disabled if there are Jira products, but their access is NONE', () => {
    const wrapper = createWrapper([
      {
        productId: 'jira-core',
        productName: 'Jira Core',
        accessLevel: 'USE',
        presence: '2017-07-10T14:00:00Z',
      },
      {
        productId: 'jira-software',
        productName: 'Jira Software',
        accessLevel: 'NONE',
        presence: '2017-07-10T14:00:00Z',
      },
    ]);

    expect(wrapper.find(AccessToggle).length).to.equal(2);
    expect((wrapper.find(AccessToggle).at(0) as any).props().isDisabled).to.equal(false);
    expect((wrapper.find(AccessToggle).at(1) as any).props().isDisabled).to.equal(false);
    expect((wrapper.find(AccessToggle).at(0) as any).props().icon).to.equal(undefined);
  });

  it('Jira Core should be disabled if there is another Jira product with USE access', () => {
    const wrapper = createWrapper([
      {
        productId: 'jira-core',
        productName: 'Jira Core',
        accessLevel: 'USE',
        presence: '2017-07-10T14:00:00Z',
      },
      {
        productId: 'jira-software',
        productName: 'Jira Software',
        accessLevel: 'USE',
        presence: '2017-07-10T14:00:00Z',
      },
    ]);

    expect(wrapper.find(AccessToggle).length).to.equal(2);
    expect((wrapper.find(AccessToggle).at(0) as any).props().isDisabled).to.equal(true);
    expect((wrapper.find(AccessToggle).at(0) as any).props().isChecked).to.equal(true);
    expect((wrapper.find(AccessToggle).at(1) as any).props().isDisabled).to.equal(false);
    expect((wrapper.find(AccessToggle).at(1) as any).props().isChecked).to.equal(true);
    expect((wrapper.find(AccessToggle).at(0) as any).props().icon).to.not.equal(undefined);
  });

  it('Granting access to a product', () => {
    const wrapper = createWrapper([
      {
        productId: 'conf',
        productName: 'Confluence',
        accessLevel: 'USE',
        presence: '2017-07-10T14:00:00Z',
      },
    ]);

    const accessToggle = wrapper.find(AccessToggle);
    expect(accessToggle.exists()).to.equal(true);
    simulateCheckAction(wrapper.find(AccessToggle), true);

    expect(grantAccessToProductsSpy.calledWith({
      variables: {
        cloudId: 'cloud-id',
        users: ['user-id'],
        productIds: ['conf'],
      },
      refetchQueries: [{
        query: userDetailsDirectGroupsQuery,
        variables: {
          cloudId: 'cloud-id',
          userId: 'user-id',
          start: 1,
          count: 5,
        },
      }],
    })).to.equal(true);
  });

  it('Revoking access to a product', () => {
    const wrapper = createWrapper([
      {
        productId: 'conf',
        productName: 'Confluence',
        accessLevel: 'USE',
        presence: '2017-07-10T14:00:00Z',
      },
    ]);
    const accessToggle = wrapper.find(AccessToggle);
    expect(accessToggle.exists()).to.equal(true);
    simulateCheckAction(wrapper.find(AccessToggle), false);

    expect(revokeAccessToProductsSpy.calledWith({
      variables: {
        cloudId: 'cloud-id',
        users: ['user-id'],
        productIds: ['conf'],
      },
      refetchQueries: [{
        query: userDetailsDirectGroupsQuery,
        variables: {
          cloudId: 'cloud-id',
          userId: 'user-id',
          start: 1,
          count: 5,
        },
      }],
    })).to.equal(true);
  });
});
