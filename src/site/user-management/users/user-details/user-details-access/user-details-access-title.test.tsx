import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import AkLozenge from '@atlaskit/lozenge';

import { InfoHover } from 'common/info-hover';

import { createMockIntlProp } from '../../../../../utilities/testing';
import { UserDetailsAccessTitleImpl } from './user-details-access-title';

describe('Access Title', () => {

  function createWrapper({
      isInvited = false,
      isSystemUser = false,
      isSystemAdmin = false,
      isGloballyDisabled = false,
      isRevokedAccess = false,
      isSiteAdmin = false,
  }) {
    return shallow(
      <UserDetailsAccessTitleImpl
        intl={createMockIntlProp()}
        isInvited={isInvited}
        isSystemUser={isSystemUser}
        isSystemAdmin={isSystemAdmin}
        isGloballyDisabled={isGloballyDisabled}
        isRevokedAccess={isRevokedAccess}
        isSiteAdmin={isSiteAdmin}
      />,
    );
  }

  it('When a user is a site admin; there should be a title and lozenge', () => {
    const wrapper = createWrapper({ isSiteAdmin: true });
    expect(wrapper.find('h2').exists()).to.equal(true);
    expect(wrapper.find(AkLozenge).exists()).to.equal(true);
    expect(wrapper.find(InfoHover).exists()).to.equal(false);
  });

  it('When a user is invited; there should be a title and info icon', () => {
    const wrapper = createWrapper({ isInvited: true });
    expect(wrapper.find('h2').exists()).to.equal(true);
    expect(wrapper.find(AkLozenge).exists()).to.equal(false);
    expect(wrapper.find(InfoHover).exists()).to.equal(true);
  });

  it('When a user is invited and site admin; there should be a title, info icon and lozenge', () => {
    const wrapper = createWrapper({ isInvited: true, isSiteAdmin: true });
    expect(wrapper.find('h2').exists()).to.equal(true);
    expect(wrapper.find(AkLozenge).exists()).to.equal(true);
    expect(wrapper.find(InfoHover).exists()).to.equal(true);
  });

  it('When a user is invited and disabled; there should be a title and info icon', () => {
    const wrapper = createWrapper({ isInvited: true, isRevokedAccess: true });
    expect(wrapper.find('h2').exists()).to.equal(true);
    expect(wrapper.find(AkLozenge).exists()).to.equal(false);
    expect(wrapper.find(InfoHover).exists()).to.equal(true);
  });

  it('When a user is globally disabled; there should be a title', () => {
    const wrapper = createWrapper({ isGloballyDisabled: true });
    expect(wrapper.find('h2').exists()).to.equal(true);
    expect(wrapper.find(AkLozenge).exists()).to.equal(false);
    expect(wrapper.find(InfoHover).exists()).to.equal(false);
  });
});
