import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import AkSpinner from '@atlaskit/spinner';

import { ProductAccess, User } from '../../../../../schema/schema-types';
import { createMockAnalyticsClient, createMockIntlProp } from '../../../../../utilities/testing';
import { Permissions } from '../../user-permission-section/permissions';
import { UserDetailsAccessSectionImpl } from './user-details-access-section';
import { ProductAccessSection } from './user-details-product-access';
import { SiteAccess } from './user-details-site-access';

describe('Access Section', () => {

  const defaultUserDetailsProp: User = {
    id: `${Math.floor(Math.random() * 100)}`,
    active: true,
    displayName: `Prince Corgi`,
    email: 'corgi@happyfriends.paws',
    presence: '2017-07-10T14:00:00Z',
    system: false,
    hasVerifiedEmail: true,
  };

  const defaultProductAccessProps: ProductAccess[] = [
    {
      productId: 'productId',
      presence: '2017-07-10T14:00:00Z',
      productName: 'productName',
      accessLevel: 'USE',
    },
  ];

  function createWrapper(data: any) {
    return shallow(
      <UserDetailsAccessSectionImpl
        intl={createMockIntlProp()}
        analyticsClient={createMockAnalyticsClient()}
        userStates={[]}
        activateUser={undefined as any}
        deactivateUser={undefined as any}
        data={data}
        isUserSystemAdmin={false}
        isUserSiteAdmin={false}
        isCurrentUser={false}
        showFlag={undefined as any}
        hideFlag={undefined as any}
        cloudId={'DUMMY-CLOUD-ID'}
        userId={'DUMMY-USER-ID'}
        userPermission={Permissions.BASIC}
      />,
    );
  }

  it('When loading, it should show spinner', () => {
    const wrapper = createWrapper({ loading: true });
    expect(wrapper.find(AkSpinner).length).to.equal(1);
  });

  describe('Enabled user state', () => {
    const wrapper = createWrapper({
      error: false,
      loading: false,
      user: {
        userDetails: {
          ...{ __typename: 'User', ...defaultUserDetailsProp },
          active: true,
          activeStatus: 'ENABLED',
        },
        productAccess: defaultProductAccessProps,
      },
    });

    it('Site access should display', () => {
      expect(wrapper.find(SiteAccess).length).to.equal(1);
      expect(wrapper.find(SiteAccess).props().cloudId).to.equal('DUMMY-CLOUD-ID');
      expect(wrapper.find(SiteAccess).props().userId).to.equal('DUMMY-USER-ID');
      expect(wrapper.find(SiteAccess).props().active).to.equal(true);
      expect(wrapper.find(SiteAccess).props().activeStatus).to.equal('ENABLED');
      expect(wrapper.find(SiteAccess).props().presence).to.equal('2017-07-10T14:00:00Z');
    });

    it('Product access should display', () => {
      const productAccessSection = wrapper.find(ProductAccessSection);
      expect(productAccessSection.length).to.equal(1);
      expect(productAccessSection.props().cloudId).to.equal('DUMMY-CLOUD-ID');
      expect(productAccessSection.props().userId).to.equal('DUMMY-USER-ID');
      expect(productAccessSection.props().isDisabled).to.equal(false);
      expect(productAccessSection.props().productAccess).to.deep.equal([
        {
          productId: 'productId',
          presence: '2017-07-10T14:00:00Z',
          productName: 'productName',
          accessLevel: 'USE',
        },
      ]);
    });
  });

  describe('Disabled user state', () => {
    const wrapper = createWrapper({
      error: false,
      loading: false,
      user: {
        userDetails: {
          ...{ __typename: 'User', ...defaultUserDetailsProp },
          active: false,
          activeStatus: 'DISABLED',
        },
        productAccess: defaultProductAccessProps,
      },
    });

    it('Site access should display', () => {
      expect(wrapper.find(SiteAccess).length).to.equal(1);
      expect(wrapper.find(SiteAccess).props().cloudId).to.equal('DUMMY-CLOUD-ID');
      expect(wrapper.find(SiteAccess).props().userId).to.equal('DUMMY-USER-ID');
      expect(wrapper.find(SiteAccess).props().active).to.equal(false);
      expect(wrapper.find(SiteAccess).props().activeStatus).to.equal('DISABLED');
      expect(wrapper.find(SiteAccess).props().presence).to.equal('2017-07-10T14:00:00Z');
    });

    it('Product access should display', () => {
      expect(wrapper.find(ProductAccessSection).length).to.equal(1);
      expect(wrapper.find(ProductAccessSection).props().cloudId).to.equal('DUMMY-CLOUD-ID');
      expect(wrapper.find(ProductAccessSection).props().userId).to.equal('DUMMY-USER-ID');
      expect(wrapper.find(ProductAccessSection).props().isDisabled).to.equal(true);
      expect(wrapper.find(ProductAccessSection).props().productAccess).to.deep.equal([
        {
          productId: 'productId',
          presence: '2017-07-10T14:00:00Z',
          productName: 'productName',
          accessLevel: 'USE',
        },
      ]);
    });
  });

  describe('Error states', () => {

    it('When there no data, it should show an error', () => {
      const wrapper = createWrapper({});
      expect(wrapper.find('span').length).to.equal(1);
    });

    it('When there is an error, it should show an error', () => {
      const wrapper = createWrapper({ error: true });
      expect(wrapper.find('span').length).to.equal(1);
    });

    it('When there are is no user, it should show an error', () => {
      const wrapper = createWrapper({ error: false, loading: false, user: null });
      expect(wrapper.find('span').length).to.equal(1);
    });

    it('When there are no products, it should show an error', () => {
      const wrapper = createWrapper({ error: false, loading: false, user: { productAccess: [] } });
      expect(wrapper.find('span').length).to.equal(1);
    });

    it('When there are no user details, it should show an error', () => {
      const wrapper = createWrapper({
        error: false,
        loading: false,
        user: {
          userDetails: null,
          productAccess: [
            {
              productId: 'productId',
              presence: '2017-07-10T14:00:00Z',
              productName: 'productName',
              accessLevel: 'SITE-ADMIN',
            },
          ],
        },
      });

      expect(wrapper.find('span').length).to.equal(1);
    });
  });

});
