import { expect } from 'chai';
import * as sinon from 'sinon';

import { ActiveStatus } from '../../../../../schema/schema-types';
import { updateActiveStatus } from './user-details-change-active-status.updater';

describe('Optimistically activating and deactivating a user', () => {

  let writeQuerySpy;

  const sandbox = sinon.sandbox.create();

  beforeEach(() => {
    writeQuerySpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  function generateStore(activeStatus: ActiveStatus, active: boolean) {
    return {
      readQuery: sinon.stub().returns({
        user: {
          userDetails: {
            __typename: 'User',
            id: Math.floor(Math.random() * 100),
            displayName: `Prince Corgi`,
            email: 'corgi@happyfriends.paws',
            active,
            presence: '2017-07-10T14:00:00Z',
            admin: false,
            activeStatus,
            system: false,
            hasVerifiedEmail: true,
          },
        },
      }),
      writeQuery: writeQuerySpy,
    } as any;
  }

  it('Should deactivate a user', () => {

    const variables = {
      cloudId: 'DUMMY-CLOUD-ID',
      userId: 'DUMMY-USER-ID',
      start: 1,
      count: 100,
    };

    updateActiveStatus(generateStore('ENABLED', true), true, variables);
    expect(writeQuerySpy.callCount).to.equal(1);
    expect(writeQuerySpy.args[0][0].data.user.userDetails.active).to.equal(false);
    expect(writeQuerySpy.args[0][0].data.user.userDetails.activeStatus).to.equal('DISABLED');
  });

  it('Should activate a user', () => {

    const variables = {
      cloudId: 'DUMMY-CLOUD-ID',
      userId: 'DUMMY-USER-ID',
      start: 1,
      count: 100,
    };

    updateActiveStatus(generateStore('DISABLED', false), false, variables);
    expect(writeQuerySpy.callCount).to.equal(1);
    expect(writeQuerySpy.args[0][0].data.user.userDetails.active).to.equal(true);
    expect(writeQuerySpy.args[0][0].data.user.userDetails.activeStatus).to.equal('ENABLED');
  });

  it('Should do nothing if the user is blocked on a global level', () => {

    const variables = {
      cloudId: 'DUMMY-CLOUD-ID',
      userId: 'DUMMY-USER-ID',
      start: 1,
      count: 100,
    };

    updateActiveStatus(generateStore('BLOCKED', false), false, variables);
    expect(writeQuerySpy.callCount).to.equal(0);
  });

  it('Should do nothing if reading the query results in an error', () => {

    const variables = {
      cloudId: 'DUMMY-CLOUD-ID',
      userId: 'DUMMY-USER-ID',
      start: 1,
      count: 100,
    };

    updateActiveStatus({
      readQuery: sinon.stub().throws(),
    } as any, false, variables);

    expect(writeQuerySpy.callCount).to.equal(0);
  });
});
