import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';

import { AccessToggle } from './user-details-access-toggle';

storiesOf('Site|Access Toggle', module)
  .add('Unchecked', () => (
    <div style={{ width: '400px', padding: '50x 0 0 50px' }}>
    <IntlProvider>
      <AccessToggle
        id="dog-club"
        name="Really Fluffy Golden Retrievers"
        accessType="product"
        presence={'2017-07-10T14:00:00Z'}
        isChecked={false}
        isDisabled={false}
        // tslint:disable-next-line:react-this-binding-issue
        onChecked={() => undefined}
      />
    </IntlProvider>
    </div>
  )).add('Checked', () => (
    <div style={{ width: '400px', padding: '50x 0 0 50px' }}>
      <IntlProvider>
        <AccessToggle
          id="dog-club"
          accessType="product"
          name="Really Fluffy Golden Retrievers"
          presence={'2017-07-10T14:00:00Z'}
          isChecked={true}
          isDisabled={false}
          // tslint:disable-next-line:react-this-binding-issue
          onChecked={() => undefined}
        />
      </IntlProvider>
    </div>
  )).add('Disabled', () => (
    <div style={{ width: '400px', padding: '50x 0 0 50px' }}>
     <IntlProvider>
        <AccessToggle
          id="dog-club"
          name="Really Fluffy Golden Retrievers"
          presence={'2017-07-10T14:00:00Z'}
          isChecked={true}
          isDisabled={true}
          accessType="product"
          // tslint:disable-next-line:react-this-binding-issue
          onChecked={() => undefined}
        />
      </IntlProvider>
    </div>
  )).add('With action', () => (
    <div style={{ width: '400px', padding: '50x 0 0 50px' }}>
      <IntlProvider>
        <AccessToggle
            id="dog-club"
            name="Really Fluffy Golden Retrievers"
            presence={'2017-07-10T14:00:00Z'}
            isChecked={true}
            isDisabled={false}
            // tslint:disable-next-line:react-this-binding-issue
            onChecked={() => undefined}
            accessType="product"
            // tslint:disable-next-line:jsx-use-translation-function
            additionalAction={<a href="#">Some additional action here</a>}
        />
      </IntlProvider>
    </div>
  )).add('No presence data - product type', () => (
    <div style={{ width: '400px', padding: '50x 0 0 50px' }}>
      <IntlProvider>
        <AccessToggle
          id="dog-club"
          name="Really Fluffy Golden Retrievers"
          presence={undefined}
          isChecked={true}
          isDisabled={false}
          // tslint:disable-next-line:react-this-binding-issue
          onChecked={() => undefined}
          accessType="product"
          // tslint:disable-next-line:jsx-use-translation-function
          additionalAction={<a href="#">Some additional action here</a>}
        />
      </IntlProvider>
    </div>
  )).add('No presence data - site type', () => (
    <div style={{ width: '400px', padding: '50x 0 0 50px' }}>
      <IntlProvider>
        <AccessToggle
          id="dog-club"
          name="Really Fluffy Golden Retrievers"
          presence={undefined}
          isChecked={true}
          isDisabled={false}
          // tslint:disable-next-line:react-this-binding-issue
          onChecked={() => undefined}
          accessType="site"
          // tslint:disable-next-line:jsx-use-translation-function
          additionalAction={<a href="#">Some additional action here</a>}
        />
      </IntlProvider>
    </div>
  ));
