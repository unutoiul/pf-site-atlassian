import styled from 'styled-components';

import {
  colors as akColors,
  fontSize as akFontSize,
  gridSize as akGridSize,
} from '@atlaskit/theme';

const TOGGLE_COLUMN_WIDTH = akGridSize() * 7;

export const ReInviteButton = styled.div`
  margin-left: -${akGridSize() * 1.5}px;
`;

export const ToggleComponent = styled.div`
  padding-bottom: ${akGridSize() * 2}px;
`;

export const ToggleWrapper = styled.div`
  width: ${TOGGLE_COLUMN_WIDTH}px;
  padding-top: 2px;
`;

export const ToggleHeader = styled.div`
  display: flex;
  align-items: center;
`;

export const ToggleName = styled.span`
  font-weight: 500;
  font-size: ${akFontSize() * 1.1}px;
  color: ${akColors.N500};
`;

export const ToggleSubHeader = styled.div`
  padding-left: ${TOGGLE_COLUMN_WIDTH}px;
`;

export const ToggleDescription = styled.span`
  color: ${akColors.subtleHeading};
  font-size: ${akFontSize() * 0.9}px;
  padding-top: ${akGridSize}px;
`;

export const ToggleAdditionalAction = styled.div`
  padding: ${akGridSize() * 2}px 0;
`;

export const SiteAccessSection = styled.div`
  padding-bottom: ${akGridSize() * 4}px;
`;

export const DialogWrapper = styled.div`
  width: ${akGridSize() * 55}px;
  padding-bottom: ${akGridSize() * 3}px;
`;

export const LozengeWrapper = styled.div`
  padding: ${akGridSize()}px;
`;
