import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import AkToggle from '@atlaskit/toggle';

import { ToggleAdditionalAction, ToggleComponent, ToggleDescription, ToggleHeader, ToggleName, ToggleSubHeader, ToggleWrapper } from './user-details-access.styled';

export interface AccessProps {
  id: string;
  name: string;
  accessType: 'product' | 'site';
  presence?: string;
  isChecked: boolean;
  isDisabled: boolean;
  additionalAction?: React.ReactNode;
  icon?: React.ReactNode;
  onChecked(e): void;
}

const messages = defineMessages({
  presencePrefix: {
    id: 'users.details.product.access.has.presence',
    description: 'Message indicating when user was last seen',
    defaultMessage: `Last seen on: ` ,
  },
  noPresenceProduct: {
    id: 'users.details.product.access.no.presence.product',
    description: 'No user activity found on the product',
    defaultMessage: `Never used this product` ,
  },
  noPresenceSite: {
    id: 'users.details.product.access.no.presence.site',
    description: 'No user activity found on the site',
    defaultMessage: `Never seen on this site` ,
  },
});

export class AccessToggleImpl extends React.Component<InjectedIntlProps & AccessProps> {

  public render() {

    const { additionalAction, isDisabled, isChecked, onChecked, name, presence, id, icon } = this.props;

    return (
      <ToggleComponent>
        <ToggleHeader>
          <ToggleWrapper>
            <AkToggle
              value={id}
              isDisabled={isDisabled}
              isDefaultChecked={isChecked}
              onChange={onChecked}
            />
          </ToggleWrapper>
          <ToggleName>
            {name}
          </ToggleName>
          <div>
            {icon}
          </div>
        </ToggleHeader>
        <ToggleSubHeader>
            <ToggleDescription>
              {this.formatPresence(presence)}
            </ToggleDescription>
            {additionalAction && <ToggleAdditionalAction>{additionalAction}</ToggleAdditionalAction>}
        </ToggleSubHeader>
      </ToggleComponent>
    );
  }

  private formatPresence = (presence?: string): string | React.ReactNode => {
    const { formatMessage, formatDate } = this.props.intl;
    if (presence) {
      return (
        <span>
          {formatMessage(messages.presencePrefix)}
          {formatDate(new Date(presence), { month: 'long', day: '2-digit', year: 'numeric' })}
        </span>
      );
    } else if (this.props.accessType === 'site') {
      return formatMessage(messages.noPresenceSite);
    } else {
      return formatMessage(messages.noPresenceProduct);
    }
  }
}

export const AccessToggle = injectIntl(AccessToggleImpl);
