import { DataProxy } from 'apollo-cache';

import { GetUserProductAccessQuery, GetUserProductAccessQueryVariables } from '../../../../../schema/schema-types';
import query from './user-details-access.query.graphql';

export function updateActiveStatus(store: DataProxy, isActive: boolean, variables: GetUserProductAccessQueryVariables) {

  let currentData;

  try {
    currentData = store.readQuery({
      query,
      variables,
    }) as GetUserProductAccessQuery;
  } catch (e) {
    return;
  }

  const { user } = currentData;

  if (!user || !user.userDetails || !user.userDetails.activeStatus ||
    user.userDetails.activeStatus === 'BLOCKED') {
    return;
  }

  const newResult = {
    user: {
      userDetails: {
        ... user.userDetails,
        active: !isActive,
        activeStatus: isActive ? 'DISABLED' : 'ENABLED',
        __typename: 'User',
      },
      productAccess: user.productAccess,
      __typename: 'UserDetails',
    },
  };

  store.writeQuery({
    query,
    variables,
    data: newResult,
  });

  return;
}
