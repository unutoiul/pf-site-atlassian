import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import AkButton from '@atlaskit/button';
import AkSectionMessage from '@atlaskit/section-message';

import { FlagProps, withFlag } from 'common/flag';

import { AnalyticsClientProps, reInviteUserUIEventData, withAnalyticsClient } from 'common/analytics';

import { AnalyticsProps } from '../../../user-management-analytics';

import reInviteUser from './user-details-reinvite-user.mutation.graphql';

import { ActiveStatus, UserDetailsReinviteUserMutation } from '../../../../../schema/schema-types';
import { resendInvite } from '../../users-common/user-resend-invite';
import { AccessToggle } from './user-details-access-toggle';
import { DialogWrapper, ReInviteButton, SiteAccessSection } from './user-details-access.styled';

interface SiteAccessProps {
  cloudId: string;
  userId: string;
  active: boolean;
  isSystem: boolean;
  isSystemAdministrator: boolean;
  isCurrentUser: boolean;
  activeStatus: ActiveStatus;
  presence?: string;
  updateUserStatus(): void;
}

const messages = defineMessages({
  error: {
    id: 'users.details.site.access.error',
    description: 'Error messsage which shows up on the page when there is an unknown error',
    defaultMessage: 'There was a problem trying to fetch this user\'s product access.',
  },
  toggleLabel: {
    id: 'users.details.site.access.toggle.label',
    description: 'Label on the toggle to denote active status on the site',
    defaultMessage: `Has access on site` ,
  },
  resendInvite: {
    id: 'users.details.site.resend.invite',
    description: 'User clicks this to resend the invite to a pending user',
    defaultMessage: 'Resend invite',
  },
});

const disabledMessages = defineMessages({
  globallyDisabled: {
    id: 'users.details.site.access.globally.disabled',
    description: 'Error message telling the user that the account is deactivated globally, and they cannot be reactivated',
    defaultMessage: `This user's information is managed elsewhere, and has been disabled globally.
    Please contact support if you wish to enabled this user.` ,
  },
  sysadmin: {
    id: 'users.details.site.access.sys.admin',
    description: 'Error message telling the user that they have unsufficient privileges to modify the user',
    defaultMessage: 'This user is a system administrator and their privileges cannot be modified.',
  },
  system: {
    id: 'users.details.site.access.system',
    description: 'Error message telling the user that they have unsufficient privileges to modify the user',
    defaultMessage: 'This user is a system user, and their privileges cannot be modified.',
  },
  currentUser: {
    id: 'users.details.site.access.current.user',
    description: 'Error message telling the user that they cannot modify themselves',
    defaultMessage: 'You can only modify your own access directly via group memberships.',
  },
});

export class SiteAccessImpl extends React.Component<ChildProps<InjectedIntlProps & SiteAccessProps & FlagProps & AnalyticsProps & AnalyticsClientProps, UserDetailsReinviteUserMutation>> {
  public render() {
    const { formatMessage } = this.props.intl;
    const { userId, activeStatus, active, presence, isSystem, isSystemAdministrator, isCurrentUser } = this.props;

    const specialUserDialog = (
      <DialogWrapper>
        <AkSectionMessage>
          {this.generateDisabledPanelText()}
        </AkSectionMessage>
      </DialogWrapper>
    );

    const invitedAction = (
      <ReInviteButton>
        <AkButton
          appearance="link"
          onClick={this.reinviteUser}
        >
          {formatMessage(messages.resendInvite)}
        </AkButton>
      </ReInviteButton>
    );

    const isDisabledGlobally = activeStatus === 'BLOCKED' || isSystem || isSystemAdministrator || isCurrentUser;
    const isInvited = !presence && !isSystem && !isSystemAdministrator && active;

    return (
      <SiteAccessSection>
        {isDisabledGlobally && specialUserDialog}
        <AccessToggle
          onChecked={this.props.updateUserStatus}
          isDisabled={isDisabledGlobally}
          isChecked={active}
          presence={presence || undefined}
          key={userId}
          id={userId}
          accessType="site"
          name={formatMessage(messages.toggleLabel)}
          additionalAction={isInvited && invitedAction}
        />
      </SiteAccessSection>
    );
  }

  private sendReInviteUserUIEventAnalytics = () => {
    this.props.analyticsClient.sendUIEvent({
      cloudId: this.props.cloudId,
      data: reInviteUserUIEventData({
        userState: this.props.userStates,
        userId: this.props.userId,
      }),
    });
  }

  private reinviteUser = (): void => {
    this.sendReInviteUserUIEventAnalytics();
    resendInvite({
      mutate: this.props.mutate,
      showFlag: this.props.showFlag,
      variables: {
        cloudId: this.props.cloudId,
        userId: this.props.userId,
      },
      intl: this.props.intl,
      userStates: this.props.userStates,
      analyticsClient: this.props.analyticsClient,
      trackEventSource: 'userDetailsScreen',
    });
  }

  private generateDisabledPanelText = (): string | null => {

    const { formatMessage } = this.props.intl;

    if (this.props.isSystem) {
      return formatMessage(disabledMessages.system);
    } else if (this.props.isSystemAdministrator) {
      return formatMessage(disabledMessages.sysadmin);
    } else if (this.props.activeStatus === 'BLOCKED') {
      return formatMessage(disabledMessages.globallyDisabled);
    } else if (this.props.isCurrentUser) {
      return formatMessage(disabledMessages.currentUser);
    }

    return null;
  }
}

const withReInviteUser = graphql<SiteAccessProps & AnalyticsProps, UserDetailsReinviteUserMutation>(reInviteUser);
export const SiteAccess =
  withReInviteUser(
    withAnalyticsClient(
      injectIntl(
        withFlag(
          SiteAccessImpl,
        ),
      ),
    ),
  );
