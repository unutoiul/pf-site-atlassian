import { DataProxy } from 'apollo-cache';
import * as React from 'react';
import { ChildProps, graphql, MutationFunc } from 'react-apollo';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import AkSpinner from '@atlaskit/spinner';

import { createErrorIcon, createSuccessIcon } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';

import { AnalyticsClientProps, changedUserSiteAccessTrackEventData, updateUserActiveStatusUIEventData, withAnalyticsClient } from 'common/analytics';

import { AnalyticsProps, getUserStates } from '../../../user-management-analytics';

import { ActivateUserMutation, ActivateUserMutationVariables, DeactivateUserMutation, DeactivateUserMutationVariables, GetUserProductAccessQuery, GetUserProductAccessQueryVariables } from '../../../../../schema/schema-types';
import { Permissions } from '../../user-permission-section/permissions';
import { SectionTitle } from '../user-details-page/user-details.styled';
import { UserDetailsAccessTitle } from './user-details-access-title';
import getUserProductAccessQuery from './user-details-access.query.graphql';
import activateUser from './user-details-activate.mutation.graphql';
import { updateActiveStatus } from './user-details-change-active-status.updater';
import deactivateUser from './user-details-deactivate.mutation.graphql';
import { ProductAccessSection } from './user-details-product-access';
import { SiteAccess } from './user-details-site-access';

const messages = defineMessages({
  title: {
    id: 'users.details.site.access.section.title',
    description: 'Title for the active section',
    defaultMessage: `Access` ,
  },
  error: {
    id: 'users.details.product.access.error',
    description: 'Error message from network',
    defaultMessage: `There was a problem trying to fetch this users' access.` ,
  },
  successTitle: {
    id: 'users.details.product.access.success.flag.title',
    description: 'Success flag text for activating / deactivating user',
    defaultMessage: `{user} has been {action} access to this site` ,
  },
  errorTitle: {
    id: 'users.details.product.access.error.flag.title',
    description: 'Success flag text for activating / deactivating user',
    defaultMessage: `We couldn't {action} access to this site for {user}` ,
  },
  badRequestErrorToActivate: {
    id: 'users.details.product.access.user.bad.request.error',
    description: 'Message that displays when the user is not active on the site',
    defaultMessage: `You do not have permission to modify this user; or your license count will be exceeded if the user is made active.`,
  },
  badRequestErrorToDeactivate: {
    id: 'users.details.product.access.user.no.permission.error',
    description: 'Message that displays when the user is not active on the site',
    defaultMessage: `You do not have permission to modify this user.`,
  },
  unknownError: {
    id: 'users.details.product.access.user.unknown.error',
    description: 'Message that displays when the user is not active on the site',
    defaultMessage: `We could not deactivate this user; please contact support or refresh the page to try again.`,
  },
  siteAdminLozenge: {
    id: 'users.details.product.access.site.admin.lozenge',
    description: 'Lozenge text to indicate that the user is a site admin',
    defaultMessage: `Site admin`,
  },
});

interface ActivateUserMutationProps {
  activateUser: MutationFunc<ActivateUserMutation, ActivateUserMutationVariables>;
}
interface DeactivateUserMutationProps {
  deactivateUser: MutationFunc<DeactivateUserMutation, DeactivateUserMutationVariables>;
}

type MutationProps = ActivateUserMutationProps & DeactivateUserMutationProps;

interface SiteParams {
  cloudId: string;
  userId: string;
  isCurrentUser: boolean;
  isUserSystemAdmin: boolean;
  isUserSiteAdmin: boolean;
  userPermission: Permissions;
}

type UserDetailPageApolloProps = GetUserProductAccessQuery;
type UserDetailPageOwnProps = InjectedIntlProps & FlagProps & SiteParams & MutationProps & AnalyticsProps & AnalyticsClientProps;

export class UserDetailsAccessSectionImpl extends React.Component<ChildProps<UserDetailPageOwnProps, UserDetailPageApolloProps>> {

  public render() {
    const { formatMessage } = this.props.intl;
    const { data } = this.props;

    if (data && data.loading) {
      return (
        <div>
          <SectionTitle>
            <h2>{formatMessage(messages.title)}</h2>
          </SectionTitle>
          <AkSpinner />
        </div>
      );
    }

    if (!data || data.error || !data.user || data.user.productAccess.length === 0 || !data.user.userDetails) {
      return (
        <span>
          {formatMessage(messages.error)}
        </span>
      );
    }

    return (
    <div>
      <UserDetailsAccessTitle
        isInvited={!data.user.userDetails.presence}
        isRevokedAccess={!data.user.userDetails.active}
        isGloballyDisabled={data.user.userDetails.activeStatus === 'BLOCKED'}
        isSystemUser={data.user.userDetails.system}
        isSystemAdmin={this.props.isUserSiteAdmin}
        isSiteAdmin={this.props.isUserSiteAdmin}
      />
      <SiteAccess
        isSystemAdministrator={this.props.isUserSystemAdmin}
        isSystem={data.user.userDetails.system}
        isCurrentUser={this.props.isCurrentUser}
        updateUserStatus={this.updateUserStatus}
        cloudId={this.props.cloudId}
        userId={this.props.userId}
        active={data.user.userDetails.active}
        activeStatus={data.user.userDetails.activeStatus || 'DISABLED'}
        presence={data.user.userDetails.presence || undefined}
        userStates={getUserStates(data && data.user && data.user.userDetails)}
      />
      <ProductAccessSection
        isDisabled={!data.user.userDetails.active || data.user.userDetails.system || this.props.isUserSystemAdmin || this.props.isCurrentUser}
        cloudId={this.props.cloudId}
        userId={this.props.userId}
        refetch={this.props.data!.refetch}
        productAccess={data.user.productAccess.filter(productAccess => productAccess.productId !== 'jira-admin')}
        userPermission={this.props.userPermission}
      />
    </div>
    );
  }

  private sendChangedUserSiteAccessTrackEventAnalytics = () => {
    this.props.analyticsClient.sendTrackEvent({
      cloudId: this.props.cloudId,
      data: changedUserSiteAccessTrackEventData({
        userState: this.props.userStates,
        userId: this.props.userId,
      }),
    });
  }

  private sendChangedUserSiteAccessUIEventAnalytics = (updateType?: 'grant' | 'revoke') => {
    this.props.analyticsClient.sendUIEvent({
      cloudId: this.props.cloudId,
      data: updateUserActiveStatusUIEventData({
        userState: this.props.userStates,
        userId: this.props.userId,
        updateType,
      }),
    });
  }

  private updateUserStatus = () => {
    const { formatMessage } = this.props.intl;
    const { data } = this.props;

    if (!this.props.activateUser || !data || data.error || !data.user || data.user.productAccess.length === 0) {
      return;
    }

    this.setState({ isRemovingUser: true });

    const displayName = data.user.userDetails.displayName;
    const isActive = data.user.userDetails.active;
    const changeActiveStatus: MutationFunc<ActivateUserMutation | DeactivateUserMutation, ActivateUserMutationVariables | DeactivateUserMutationVariables> =
      isActive ? this.props.deactivateUser : this.props.activateUser;

    const variables: GetUserProductAccessQueryVariables = {
      cloudId: this.props.cloudId,
      userId: this.props.userId,
    };

    this.sendChangedUserSiteAccessUIEventAnalytics(isActive ? 'revoke' : 'grant');

    changeActiveStatus({
      variables,
      update: (store: DataProxy) => updateActiveStatus(store, isActive, variables),
    }).then(() => {
      this.props.showFlag({
        autoDismiss: true,
        icon: createSuccessIcon(),
        id: `users.add.user.to.group.success.flag.${Date.now()}`,
        title: formatMessage(messages.successTitle, { action: isActive ? 'revoked' : 'given', user: displayName }),
      });
      this.sendChangedUserSiteAccessTrackEventAnalytics();
    }).catch(error => {
      const isBadRequest = error.graphQLErrors[0].originalError.status === 400;

      this.props.showFlag({
        autoDismiss: true,
        icon: createErrorIcon(),
        id: `users.add.user.to.group.error.flag.${Date.now()}`,
        title: formatMessage(messages.errorTitle, { action: isActive ? 'give' : 'revoke', user: displayName }),
        description: this.determineErrorMessage(isActive, isBadRequest),
      });
    });
  }

  private determineErrorMessage(isActive: boolean, isBadRequest: boolean) {

    const { formatMessage } = this.props.intl;

    if (isBadRequest) {
      return formatMessage(isActive ? messages.badRequestErrorToDeactivate : messages.badRequestErrorToActivate);
    }

    return formatMessage(messages.unknownError);
  }
}

const activateUserMutation = graphql<SiteParams, ActivateUserMutation, {}, ActivateUserMutationProps>(activateUser, {
  name: 'activateUser',
});
const deactivateUserMutation = graphql<SiteParams & ActivateUserMutationProps, DeactivateUserMutation, {}, DeactivateUserMutationProps>(deactivateUser, {
  name: 'deactivateUser',
});

const getUserDetails = graphql<SiteParams, GetUserProductAccessQuery>(getUserProductAccessQuery, {
  options: ({ cloudId, userId }) => ({
    variables: {
      cloudId,
      userId,
    },
  }),
  skip: ({ cloudId, userId }) => (!cloudId || !userId),
});

export const UserDetailsAccessSection = getUserDetails(
  activateUserMutation(
    deactivateUserMutation(
      injectIntl(
        withFlag(
          withAnalyticsClient(
            UserDetailsAccessSectionImpl,
          ),
        ),
      ),
    ),
  ),
);
