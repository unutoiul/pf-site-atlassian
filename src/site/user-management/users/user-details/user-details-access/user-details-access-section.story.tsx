import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';

import { createApolloClient } from '../../../../../apollo-client';
import { ActiveStatus, GetUserProductAccessQuery, SitePrivilege } from '../../../../../schema/schema-types';
import { createMockAnalyticsClient, createMockIntlProp } from '../../../../../utilities/testing';
import { Permissions } from '../../user-permission-section/permissions';
import { UserDetailsAccessSectionImpl } from './user-details-access-section';

const defaultProps = {
  cloudId: 'DUMMY-CLOUD-ID',
  userId: 'DUMMY-USER-ID',
  deactivateUser: undefined as any,
  activateUser: undefined as any,
  showFlag: undefined as any,
  hideFlag: undefined as any,
  isCurrentUser: false,
  isUserSiteAdmin: false,
  isUserSystemAdmin: false,
  intl: createMockIntlProp(),
  userStates: [],
  analyticsClient: createMockAnalyticsClient(),
  userPermission: Permissions.BASIC,
};

const generateUserQuery = (
  { active = true, activeStatus = 'ENABLED', system = false, presence = '2017-07-10T14:00:00Z' }:
  { active?: boolean, activeStatus?: ActiveStatus, system?: boolean, presence?: string | null, sitePrivilege?: SitePrivilege },
): GetUserProductAccessQuery => {
  return ({
    user: {
      __typename: 'User',
      userDetails:  {
        id: `${Math.floor(Math.random() * 100)}`,
        displayName: `Prince Corgi`,
        presence,
        system,
        active,
        activeStatus,
        __typename: 'UserDetails',
      },
      productAccess: [
        {
          productId: 'productId',
          productName: 'Jira Core',
          accessLevel: 'USE',
          presence: '2017-07-10T14:00:00Z',
          __typename: 'ProductAccess',
        },
        {
          productId: 'productId',
          productName: 'Jira Core',
          accessLevel: 'USE',
          presence: '2017-07-10T14:00:00Z',
          __typename: 'ProductAccess',
        },
      ],
    },
  });
};

const client = createApolloClient();

storiesOf('Site|User Details/Access Section/Access Section', module)
  .add('Error', () => (
    <div style={{ width: '400px', padding: '50px 0 0 50px' }}>
      <ApolloProvider client={client}>
        <IntlProvider>
          <UserDetailsAccessSectionImpl
            {... defaultProps}
          />
        </IntlProvider>
      </ApolloProvider>
    </div>
  )).add('Loading', () => (
    <div style={{ width: '400px', padding: '50px 0 0 50px' }}>
      <ApolloProvider client={client}>
        <IntlProvider>
          <UserDetailsAccessSectionImpl
            data={{ loading: true } as any}
            {... defaultProps}
          />
        </IntlProvider>
      </ApolloProvider>
    </div>
  )).add('Active user', () => {

    return (
      <div style={{ width: '400px', padding: '50px 0 0 50px' }}>
        <ApolloProvider client={client}>
          <IntlProvider>
            <UserDetailsAccessSectionImpl
              data={generateUserQuery({}) as any}
              {... defaultProps}
            />
          </IntlProvider>
        </ApolloProvider>
      </div>
    );
  }).add('Invited user', () => {

    return (
      <div style={{ width: '400px', padding: '50px 0 0 50px' }}>
        <ApolloProvider client={client}>
          <IntlProvider>
            <UserDetailsAccessSectionImpl
              data={generateUserQuery({ presence: null }) as any}
              {... defaultProps}
            />
          </IntlProvider>
        </ApolloProvider>
      </div>
    );
  }).add('Site admin user', () => {
    return (
      <div style={{ width: '400px', padding: '50px 0 0 50px' }}>
        <ApolloProvider client={client}>
          <IntlProvider>
            <UserDetailsAccessSectionImpl
              data={generateUserQuery({ sitePrivilege: 'SITE_ADMIN' }) as any}
              {... defaultProps}
            />
          </IntlProvider>
        </ApolloProvider>
      </div>
    );
  }).add('Invited site admin user', () => {
    return (
      <div style={{ width: '400px', padding: '50px 0 0 50px' }}>
        <ApolloProvider client={client}>
          <IntlProvider>
            <UserDetailsAccessSectionImpl
              data={generateUserQuery({ sitePrivilege: 'SITE_ADMIN', presence: null }) as any}
              {... defaultProps}
            />
          </IntlProvider>
        </ApolloProvider>
      </div>
    );
  }).add('Inactive user', () => {
    return (
      <div style={{ width: '400px', padding: '50px 0 0 50px' }}>
        <ApolloProvider client={client}>
          <IntlProvider>
            <UserDetailsAccessSectionImpl
              data={generateUserQuery({ active: false }) as any}
              {... defaultProps}
            />
          </IntlProvider>
        </ApolloProvider>
      </div>
    );
  }).add('Inactive user - Globally disabled', () => {
    return (
      <div style={{ width: '400px', padding: '50px 0 0 50px' }}>
        <ApolloProvider client={client}>
          <IntlProvider>
            <UserDetailsAccessSectionImpl
              data={generateUserQuery({ active: false, activeStatus: 'BLOCKED' }) as any}
              {... defaultProps}
              isUserSiteAdmin={false}
              isUserSystemAdmin={false}
            />
          </IntlProvider>
        </ApolloProvider>
      </div>
    );
  }).add('System user', () => {

    return (
      <div style={{ width: '400px', padding: '50px 0 0 50px' }}>
        <ApolloProvider client={client}>
          <IntlProvider>
            <UserDetailsAccessSectionImpl
              data={generateUserQuery({ system: true }) as any}
              {... defaultProps}
            />
          </IntlProvider>
        </ApolloProvider>
      </div>
    );
  }).add('System administrator', () => {

    return (
      <div style={{ width: '400px', padding: '50px 0 0 50px' }}>
        <ApolloProvider client={client}>
          <IntlProvider>
            <UserDetailsAccessSectionImpl
              data={generateUserQuery({ sitePrivilege: 'SYS_ADMIN' }) as any}
              {... defaultProps}
            />
          </IntlProvider>
        </ApolloProvider>
      </div>
    );
  }).add('Current user', () => {

    return (
      <div style={{ width: '400px', padding: '50px 0 0 50px' }}>
        <ApolloProvider client={client}>
          <IntlProvider>
            <UserDetailsAccessSectionImpl
              data={generateUserQuery({ sitePrivilege: 'SYS_ADMIN' }) as any}
              isCurrentUser={true}
              {... defaultProps}
            />
          </IntlProvider>
        </ApolloProvider>
      </div>
    );
  });
