import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import { SiteAdministratorPermissionDropdownItem } from './site-administrator-permission-item';
import { BasePermissionItemProps, PermissionDropDownItem } from './user-permission-item';

describe('User invite drawer - user permission section - site administrator permission', () => {
  const sandbox = sinon.sandbox.create();

  let onClickHandlerSpy: sinon.SinonSpy;

  beforeEach(() => {
    onClickHandlerSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  const testDefaultProps: BasePermissionItemProps = {
    textColor: 'blue',
    backgroundColor: 'red',
    selected: false ,
    onClickHandler: () => onClickHandlerSpy,
  };

  const getWrapper = (props: BasePermissionItemProps) => {
    return shallow(
      <SiteAdministratorPermissionDropdownItem {...props} />,
    );
  };

  it('Should render the Site administrator permission dropdown on the invite drawer', () => {
    const wrapper = getWrapper(testDefaultProps);
    const siteAdministratorItem = wrapper.find(PermissionDropDownItem);
    expect(siteAdministratorItem.exists()).to.equal(true);
    expect(siteAdministratorItem.prop('messages').label.defaultMessage).to.equal('Site administrator');
    expect(siteAdministratorItem.prop('textColor')).to.equal('blue');
    expect(siteAdministratorItem.prop('backgroundColor')).to.equal('red');
    expect(onClickHandlerSpy.called).to.equal(false);
  });
});
