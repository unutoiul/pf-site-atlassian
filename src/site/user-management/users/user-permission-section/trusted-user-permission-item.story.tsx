import { storiesOf } from '@storybook/react';
import * as React from 'react';

import { DEFAULT_BACKGROUND_COLOR, DEFAULT_FONT_COLOR, SELECTED_BACKGROUND_COLOR, SELECTED_FONT_COLOR } from './style';
import { TrustedUserPermissionDropdownItem } from './trusted-user-permission-item';
import { BasePermissionItemProps } from './user-permission-item';

const clickHandler = () => ({ a: 1 });

storiesOf('Site|User Permission Dropdowns/Trusted User', module)
  .add('Default', () => {
    const props: BasePermissionItemProps = {
      textColor: DEFAULT_FONT_COLOR,
      backgroundColor: DEFAULT_BACKGROUND_COLOR,
      selected: false ,
      onClickHandler: clickHandler,
    };

    return (
      <div style={{ width: '550px' }}>
        <TrustedUserPermissionDropdownItem {...props} />
      </div>
    );
  })
  .add('Selected', () => {
    const props: BasePermissionItemProps = {
      textColor: SELECTED_BACKGROUND_COLOR,
      backgroundColor: SELECTED_FONT_COLOR,
      selected: true ,
      onClickHandler: clickHandler,
    };

    return (
      <div style={{ width: '550px' }}>
        <TrustedUserPermissionDropdownItem {...props} />
      </div>
    );
  })
  .add('Default with can invite users', () => {
    const props: BasePermissionItemProps = {
      textColor: DEFAULT_FONT_COLOR,
      backgroundColor: DEFAULT_BACKGROUND_COLOR,
      selected: false ,
      onClickHandler: clickHandler,
    };

    return (
      <div style={{ width: '550px' }}>
        <TrustedUserPermissionDropdownItem {...props} canInviteUsers={true} />
      </div>
    );
  })
  .add('Selected with can invite users', () => {
    const props: BasePermissionItemProps = {
      textColor: SELECTED_BACKGROUND_COLOR,
      backgroundColor: SELECTED_FONT_COLOR,
      selected: true ,
      onClickHandler: clickHandler,
    };

    return (
      <div style={{ width: '550px' }}>
        <TrustedUserPermissionDropdownItem {...props} canInviteUsers={true} />
      </div>
    );
  });
