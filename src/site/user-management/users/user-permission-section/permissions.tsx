export enum Permissions {
  BASIC = 'basic',
  TRUSTED = 'trusted-user',
  SITEADMIN = 'site-admin',
}

export const permissionToRole = {
  [Permissions.BASIC]: 'site/basic',
  [Permissions.TRUSTED]: 'site/trusted-user',
  [Permissions.SITEADMIN]: 'site/admin',
};
