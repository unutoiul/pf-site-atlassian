import { colors as AkColors } from '@atlaskit/theme';

export const DEFAULT_BACKGROUND_COLOR = AkColors.N20;
export const DEFAULT_FONT_COLOR = AkColors.N500;
export const DISABLED_FONT_COLOR = AkColors.N200;
export const SELECTED_BACKGROUND_COLOR = AkColors.N600;
export const SELECTED_FONT_COLOR = AkColors.N0;
export const DISABLED_BACKGROUND_COLOR = AkColors.N30;
