import * as React from 'react';

import { trustedUserBase64Svg } from 'common/images';

import { DEFAULT_BACKGROUND_COLOR } from './style';
import { BasePermissionItemProps, PermissionDropDownItem } from './user-permission-item';

import { Permissions } from './permissions';
import { labels } from './user-permission-item-labels';

export const TrustedUserPermissionDropdownItem: React.SFC<BasePermissionItemProps> = ({
  textColor,
  backgroundColor,
  onClickHandler,
  selected,
  canInviteUsers,
  isDisabled,
}) => {

  return (
    <PermissionDropDownItem
      onClickHandler={onClickHandler}
      image={trustedUserBase64Svg}
      messages={labels[Permissions.TRUSTED]}
      textColor={textColor}
      backgroundColor={selected ? DEFAULT_BACKGROUND_COLOR : backgroundColor}
      canInviteUsers={canInviteUsers}
      isDisabled={isDisabled}
    />
  );
};
