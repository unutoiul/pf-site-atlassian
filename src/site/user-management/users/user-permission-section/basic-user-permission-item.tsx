import * as React from 'react';

import { basicUserBase64Svg } from 'common/images';

import { DEFAULT_BACKGROUND_COLOR } from './style';
import { BasePermissionItemProps, PermissionDropDownItem } from './user-permission-item';

import { Permissions } from './permissions';
import { labels } from './user-permission-item-labels';

export const BasicUserPermissionDropdownItem: React.SFC<BasePermissionItemProps> = ({
  textColor,
  backgroundColor = null,
  onClickHandler,
  selected,
  canInviteUsers,
  isDisabled,
}) => (
  <PermissionDropDownItem
    onClickHandler={onClickHandler}
    image={basicUserBase64Svg}
    textColor={textColor}
    backgroundColor={selected ? DEFAULT_BACKGROUND_COLOR : backgroundColor}
    messages={labels[Permissions.BASIC]}
    canInviteUsers={canInviteUsers}
    isDisabled={isDisabled}
  />
);
