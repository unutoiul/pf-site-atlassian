import { defineMessages } from 'react-intl';

import { Permissions } from './permissions';

export const labels = {
  [Permissions.BASIC]: defineMessages({
    label: {
      id: 'user.invite.drawer.user.permission.basic.user.label',
      defaultMessage: 'Basic',
      description: 'Basic User permission label for the select dropdown to select user permissions.',
    },
    secondaryText: {
      id: 'user.invite.drawer.user.permission.basic.user.secondarytext',
      defaultMessage: 'Can access specified products and apps. No additional permissions.',
      description: 'Basic User secondary text for the select dropdown to select user permissions.',
    },
  }),
  [Permissions.TRUSTED]: defineMessages({
    label: {
      id: 'user.invite.drawer.user.permission.trusted.user.label',
      defaultMessage: 'Trusted',
      description: 'Trusted User permission label for the select dropdown to select user permissions.',
    },
    secondaryText: {
      id: 'user.invite.drawer.user.permission.trusted.user.secondarytext',
      defaultMessage: 'Can access, configure and add products.',
      description: 'Trusted User secondary text for the select dropdown to select user permissions.',
    },
    alternativeSecondaryText: {
      id: 'user.invite.drawer.user.permission.trusted.user.alternativesecondarytext',
      defaultMessage: 'Can access, configure and add products. Can invite users.',
      description: 'Trusted User secondary text for the select dropdown to select user permissions.',
    },
  }),
  [Permissions.SITEADMIN]: defineMessages({
    label: {
      id: 'user.invite.drawer.user.permission.site.administrator.label',
      defaultMessage: 'Site administrator',
      description: 'Site administrator permission label for the select dropdown to select user permissions.',
    },
    secondaryText: {
      id: 'user.invite.drawer.user.permission.site.administrator.secondarytext',
      defaultMessage: 'Full administration permission, like you.',
      description: 'Site administrator secondary text for the select dropdown to select user permissions.',
    },
  }),
};
