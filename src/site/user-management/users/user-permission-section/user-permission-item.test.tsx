import { expect } from 'chai';
import { render } from 'enzyme';
import * as React from 'react';
import { defineMessages, IntlProvider } from 'react-intl';
import * as sinon from 'sinon';

import { createMockIntlContext, createMockIntlProp } from '../../../../utilities/testing';

import { CustomPermissionItemProps, PermissionDropDownItemImpl, secondaryText } from './user-permission-item';

describe('User invite drawer - user permission section - user permission items', () => {
  const sandbox = sinon.sandbox.create();
  const messages = defineMessages({
    label: {
      id: 'trusted.user.permission.label',
      defaultMessage: 'Trusted ',
      description: 'A permission label for the select dropdown to select user permissions.',
    },
    secondaryText: {
      id: 'trusted.user.permisson.secondarytext',
      defaultMessage: 'Can Add Products. Cannot Invite users.',
      description: 'A description for Trusted users who can add products but cannot invite',
    },
    alternativeSecondaryText: {
      id: 'trusted.user.permisson.alternativesecondarytext',
      defaultMessage: 'Can Add Products. Can Invite users.',
      description: 'A description for Trusted users who can add products and also invite',
    },
  });

  const defaultProps: CustomPermissionItemProps = {
    onClickHandler: () => ({ do: 'nothing' }),
    backgroundColor: 'red',
    image: 'test',
    textColor: 'blue',
    messages,
    canInviteUsers: false,
  };

  afterEach(() => {
    sandbox.restore();
  });

  const getWrapper = (props: CustomPermissionItemProps) => {
    return render(
      <IntlProvider locale="en">
        <PermissionDropDownItemImpl
          intl={createMockIntlProp()}
          {...props}
        />
      </IntlProvider>,
      createMockIntlContext(),
    );
  };

  it('Should render the Permission Down Item with correct message when user cannot invite user', () => {
    expect(secondaryText(defaultProps).defaultMessage).to.equal('Can Add Products. Cannot Invite users.');

    const wrapper = getWrapper(defaultProps);
    expect(wrapper.text()).to.contain('Trusted Can Add Products. Cannot Invite users.');
  });

  it('Should render the Permission Down Item with correct message when user can invite user', () => {
    const newProps = { ...defaultProps, canInviteUsers: true };

    expect(secondaryText(newProps).defaultMessage).to.equal('Can Add Products. Can Invite users.');

    const wrapper = getWrapper(newProps);
    expect(wrapper.text()).to.contain('Trusted Can Add Products. Can Invite users.');
  });
});
