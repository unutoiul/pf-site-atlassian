import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import { TrustedUserPermissionDropdownItem } from './trusted-user-permission-item';
import { BasePermissionItemProps, PermissionDropDownItem } from './user-permission-item';

describe('User invite drawer - user permission section - site administrator permission', () => {
  const sandbox = sinon.sandbox.create();

  let onClickHandlerSpy: sinon.SinonSpy;

  beforeEach(() => {
    onClickHandlerSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  const testDefaultProps: BasePermissionItemProps = {
    textColor: 'blue',
    backgroundColor: 'red',
    selected: false ,
    onClickHandler: () => onClickHandlerSpy,
  };

  const getWrapper = (props: BasePermissionItemProps) => {
    return shallow(
      <TrustedUserPermissionDropdownItem {...props} />,
    );
  };

  it('Should render the Trusted user permission dropdown on the invite drawer', () => {
    const wrapper = getWrapper(testDefaultProps);
    const trustedUserItem = wrapper.find(PermissionDropDownItem);
    expect(trustedUserItem.exists()).to.equal(true);
    expect(trustedUserItem.prop('messages').label.defaultMessage).to.equal('Trusted');
    expect(trustedUserItem.prop('textColor')).to.equal('blue');
    expect(trustedUserItem.prop('backgroundColor')).to.equal('red');
    expect(onClickHandlerSpy.called).to.equal(false);
  });
});
