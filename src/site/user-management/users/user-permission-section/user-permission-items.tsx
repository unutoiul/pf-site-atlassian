import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';

import AkDropdown, { DropdownItemGroup as AkDropdownItemGroup } from '@atlaskit/dropdown-menu';

import AkSpinner from '@atlaskit/spinner';

import { FeatureFlagProps } from 'common/feature-flags';

import { GenericError } from 'common/error';

import {
  DEFAULT_BACKGROUND_COLOR,
  DEFAULT_FONT_COLOR,
  DISABLED_BACKGROUND_COLOR,
  DISABLED_FONT_COLOR,
  SELECTED_BACKGROUND_COLOR,
  SELECTED_FONT_COLOR,
} from './style';

import { Permissions } from './permissions';

import { RolePermissionsQuery } from '../../../../schema/schema-types';
import { BasicUserPermissionDropdownItem } from './basic-user-permission-item';
import { SiteAdministratorPermissionDropdownItem } from './site-administrator-permission-item';
import { TrustedUserPermissionDropdownItem } from './trusted-user-permission-item';
import { SelectedPermissionDropdownItem } from './user-permission-item';

import rolePermissionsQuery from '../users-quick-invite/users-quick-invite.query.graphql';

interface OwnProps {
  cloudId?: string;
  trustedUsersFeatureFlag: FeatureFlagProps;
  isDisabled?: boolean;
  permission?: Permissions;
  onChange(e: any): void;
}

export type UserPermissionItems = ChildProps<OwnProps, RolePermissionsQuery>;

interface State {
  isDropdownOpen: boolean;
}

const INVITE_USERS_PERMISSION = 'invite-users';

function PermissionComponentToRender(permission) {
  switch (permission) {
    case Permissions.TRUSTED:
      return TrustedUserPermissionDropdownItem;
    case Permissions.SITEADMIN:
      return SiteAdministratorPermissionDropdownItem;
    case Permissions.BASIC:
    default:
      return BasicUserPermissionDropdownItem;
  }
}

export function canInviteUsers(props) {
  const { data, trustedUsersFeatureFlag } = props;

  return data
          && !data.error
          && !data.loading
          && data.rolePermissions
          && data.rolePermissions.permissionIds
          && data.rolePermissions.permissionIds.length > 0
          && data.rolePermissions.permissionIds.includes(INVITE_USERS_PERMISSION)
          && trustedUsersFeatureFlag
          && trustedUsersFeatureFlag.value || false;
}

export class UserPermissionItemContainerImpl extends React.Component<UserPermissionItems, State> {
  public readonly state: Readonly<State> = {
    isDropdownOpen: false,
  };

  public render() {
    const {
      data,
      isDisabled = false,
      permission = Permissions.TRUSTED,
    } = this.props;

    const { backgroundColor, textColor } = this.dropdownItemColors();

    const userCanInviteUsers = canInviteUsers(this.props);
    const selectedItem = SelectedPermissionDropdownItem(PermissionComponentToRender(permission))({
      onClickHandler: () => this.onClickHandler(permission),
      backgroundColor,
      textColor,
      isDisabled,
      canInviteUsers: userCanInviteUsers,
    });

    let component;

    if (isDisabled) {
      component = selectedItem;
    } else {
      const otherProps = { onOpenChange: this.onOpenChange.bind(this) };
      component = (
        <AkDropdown shouldFlip={false} shouldFitContainer={true} trigger={selectedItem} {...otherProps}>
          <AkDropdownItemGroup>
            <BasicUserPermissionDropdownItem
              onClickHandler={this.handleBasicPermissionClick}
            />
            <TrustedUserPermissionDropdownItem
              onClickHandler={this.handleTrustedPermissionClick}
              canInviteUsers={canInviteUsers(this.props)}
            />
            <SiteAdministratorPermissionDropdownItem
              onClickHandler={this.handleSiteadminPermissionClick}
            />
          </AkDropdownItemGroup>
        </AkDropdown>
      );
    }

    return (
      <React.Fragment>
        {data && data.loading && <AkSpinner />}
        {data && data.error && <GenericError />}
        {data && !data.loading && !data.error && component}
      </React.Fragment>
    );
  }

  private onClickHandler = (selected) => {
    if (this.props.isDisabled) {
      return;
    }

    this.props.onChange(selected);
  }

  private handleBasicPermissionClick = () => {
    this.onClickHandler(Permissions.BASIC);

    return Permissions.BASIC;
  }

  private handleTrustedPermissionClick = () => {
    this.onClickHandler(Permissions.TRUSTED);

    return Permissions.TRUSTED;
  }

  private handleSiteadminPermissionClick = () => {
    this.onClickHandler(Permissions.SITEADMIN);

    return Permissions.SITEADMIN;
  }

  private onOpenChange(): void {
    if (this.props.isDisabled) {
      return;
    }

    this.setState(prevState => ({
      isDropdownOpen: !prevState.isDropdownOpen,
    }));
  }

  private dropdownItemColors = () => {
    if (this.props.isDisabled) {
      return {
        backgroundColor: DISABLED_BACKGROUND_COLOR,
        textColor: DISABLED_FONT_COLOR,
      };
    }

    if (this.state.isDropdownOpen) {
      return {
        backgroundColor: SELECTED_BACKGROUND_COLOR,
        textColor: SELECTED_FONT_COLOR,
      };
    }

    return {
      backgroundColor: DEFAULT_BACKGROUND_COLOR,
      textColor: DEFAULT_FONT_COLOR,
    };
  }
}

const withRolePermissionsQuery = graphql<OwnProps, RolePermissionsQuery>(rolePermissionsQuery, {
  skip: ({ cloudId }) => !cloudId,
  options: ({ cloudId }) => ({ variables: { cloudId } }),
});

export const UserPermissionItemContainer = withRolePermissionsQuery(UserPermissionItemContainerImpl);
