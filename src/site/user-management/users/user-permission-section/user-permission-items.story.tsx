import { storiesOf } from '@storybook/react';
import * as React from 'react';

import { createMockIntlProp } from '../../../../utilities/testing';

import { Permissions } from './permissions';
import { DEFAULT_BACKGROUND_COLOR, DEFAULT_FONT_COLOR } from './style';
import { UserPermissionItemContainerImpl } from './user-permission-items';

const clickHandler = () => ({ a: 1 });

storiesOf('Site|User Permission Dropdowns/User Permission Items', module)
  .add('Default', () => {
    const props = {
      textColor: DEFAULT_FONT_COLOR,
      backgroundColor: DEFAULT_BACKGROUND_COLOR,
      selected: false,
      onClickHandler: clickHandler,
      intl: createMockIntlProp(),
      onChange: clickHandler,
      permission: Permissions.TRUSTED,
      trustedUsersFeatureFlag: {
        value: true,
        isLoading: false,
      },
    };

    return (
      <div style={{ width: '550px' }}>
        <UserPermissionItemContainerImpl {...props} />
      </div>
    );
  });
