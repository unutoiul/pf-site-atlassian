import * as React from 'react';
import { InjectedIntlProps, injectIntl, Messages } from 'react-intl';

import AkAvatar from '@atlaskit/avatar';
import { DropdownItem as AkDropdownItem } from '@atlaskit/dropdown-menu';
import { gridSize as akGridSize } from '@atlaskit/theme';

import { downArrowBase64Svg } from 'common/images';

import { DEFAULT_BACKGROUND_COLOR } from './style';
import { StyledAvatarImageContainer, StyledDivNoPaddingAndFlex, StyledDivPadding, StyledDivWithColor, StyledDropdownContainer, StyledSelectedDropdownContainer, StyledSpan } from './user-permission-item-styled';

export interface BasePermissionItemProps {
  textColor?: string;
  backgroundColor?: string;
  selected?: boolean;
  isDisabled?: boolean;
  canInviteUsers?: boolean;
  onClickHandler(e: any): void;
}

function CustomStyledAvatarImageComponent({ children }: { children: Node}) {
  return <StyledAvatarImageContainer>{children}</StyledAvatarImageContainer>;
}

export interface CustomPermissionItemProps extends BasePermissionItemProps {
  image: string;
  messages: Messages;
}

export const secondaryText = (props) => {
  const {
    canInviteUsers,
    messages,
  } = props;

  return canInviteUsers && messages.alternativeSecondaryText ? messages.alternativeSecondaryText : messages.secondaryText;
};

export class PermissionDropDownItemImpl extends React.Component<CustomPermissionItemProps & InjectedIntlProps> {
  public render() {
    const {
      onClickHandler,
      backgroundColor,
      image,
      textColor,
      messages,
      intl,
      isDisabled,
    } = this.props;
    const customStyle = { style: { display: 'flex', backgroundColor, height: `${akGridSize() * 8}px`, padding: 0 } };

    return (
        <AkDropdownItem
          onClick={onClickHandler}
          isDisabled={isDisabled}
          {...customStyle}
        >
          <StyledDropdownContainer className="dropdown-container" width={akGridSize() * 51.5} backgroundColor={backgroundColor}>
            <StyledDivPadding className="dropdown-div-padding">
              <AkAvatar
                size="xsmall"
                appearance="square"
                includeBorderWidth={false}
                src={image}
                borderColor={backgroundColor}
                component={CustomStyledAvatarImageComponent}
              />
            </StyledDivPadding>
            <StyledDivWithColor color={textColor}>
              <p>
                <b>{intl.formatMessage(messages.label)}</b>
              </p>
                <StyledSpan>{intl.formatMessage(secondaryText(this.props))}</StyledSpan>
            </StyledDivWithColor>
          </StyledDropdownContainer>
        </AkDropdownItem >
    );
  }
}

export const PermissionDropDownItem = injectIntl(PermissionDropDownItemImpl);

export const SelectedPermissionDropdownItem = WrappedComponent => props => (
  <StyledSelectedDropdownContainer className="dropdown-selected-item" backgroundColor={props.backgroundColor} isDisabled={props.isDisabled}>
    <WrappedComponent backgroundColor={DEFAULT_BACKGROUND_COLOR} {...props} isDisabled={props.isDisabled} />
    { !props.isDisabled && <StyledDivNoPaddingAndFlex className="dropdown-div-padding">
      <img src={downArrowBase64Svg} alt={undefined} role="presentation"/>
    </StyledDivNoPaddingAndFlex>
    }
  </StyledSelectedDropdownContainer>
);
