import * as React from 'react';
import styled from 'styled-components';

import { gridSize as akGridSize } from '@atlaskit/theme';

interface DropdownContainerProps {
  className: string;
  width?: number;
  backgroundColor?: string;
  isDisabled?: boolean;
}

const DropdownBaseContainer: React.SFC<DropdownContainerProps> = props => <div className={props.className}>{props.children}</div>;

export const StyledAvatarImageContainer = styled.div`
  display: flex;
  width: ${akGridSize() * 2}px;
  height: 100%;
`;

export const StyledDivPadding = styled(DropdownBaseContainer)`
  padding: 0 ${akGridSize}px;
`;

export const StyledDropdownContainer = styled(DropdownBaseContainer)`
  display: flex;
  padding: 0;
  border-radius: 3px;
  ${({ width }) => width && `width: ${width}px; max-width: ${width}px;`}
  ${({ backgroundColor }) => backgroundColor && `background-color: ${backgroundColor};`}
  ${({ isDisabled }) => isDisabled && `pointer-events:none; padding: 0;`}
`;

export const StyledSelectedDropdownContainer = styled(StyledDropdownContainer)`
  padding: ${akGridSize}px 0;
`;

export const StyledDivNoPaddingAndFlex = styled(DropdownBaseContainer)`
  display: flex;
  padding: 0;
`;

export const StyledDivWithColor = styled.div`
  ${({ color }) => color && `color: ${color};`}
`;

export const StyledSpan = styled.span`
  word-wrap: break-word;
  white-space: normal;
`;
