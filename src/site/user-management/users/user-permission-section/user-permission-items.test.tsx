import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import AkDropdown from '@atlaskit/dropdown-menu';
import AkSpinner from '@atlaskit/spinner';

import { GenericError } from 'common/error';

import { TrustedUserPermissionDropdownItem } from './trusted-user-permission-item';
import { canInviteUsers, UserPermissionItemContainerImpl } from './user-permission-items';

describe('User invite drawer - user permission section - user permission items', () => {
  const sandbox = sinon.sandbox.create();

  let onChangeSpy;

  beforeEach(() => {
    onChangeSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  const trustedUsersFeatureFlagOverride = {
    isLoading: false,
    value: true,
  };

  const permissionData = {
    loading: false,
    rolePermissions: {
      permissionIds: ['add-products', 'invite-users'],
    },
  };

  const testDefaultProps = {
    cloudId: 'dummyCloudId',
    onChange: () => onChangeSpy,
    data: permissionData,
    trustedUsersFeatureFlag: trustedUsersFeatureFlagOverride,
  };

  const getWrapper = (props) => {
    return shallow(
      <UserPermissionItemContainerImpl {...props} />,
    );
  };

  it('Should render the User Permission Items dropdown in the User Invite Drawer', () => {
    const wrapper = getWrapper(testDefaultProps);
    const userPermissionItemContainer = wrapper.find(AkDropdown);
    expect(userPermissionItemContainer.exists()).to.equal(true);
  });

  it('Should render the AKSpinner in the User Invite Drawer while data is loading', () => {
    const wrapper = getWrapper({ ...testDefaultProps, data: { loading: true } });
    const spinner = wrapper.find(AkSpinner);
    expect(spinner.exists()).to.equal(true);
  });

  it('Should render the common Generic Error in the User Invite Drawer while data has error', () => {
    const wrapper = getWrapper({ ...testDefaultProps, data: { error: true } });
    const genericError = wrapper.find(GenericError);
    expect(genericError.exists()).to.equal(true);
  });

  it('Should not be able to invite users on the trusted user dropdown item when data does not exist', () => {
    const testProps = { ...testDefaultProps, data: null };
    const wrapper = getWrapper(testProps);
    const spinner = wrapper.find(AkSpinner);
    const genericError = wrapper.find(GenericError);
    const trustedUserDropdownItem = wrapper.find(TrustedUserPermissionDropdownItem);
    expect(spinner.exists()).to.equal(false);
    expect(genericError.exists()).to.equal(false);
    expect(trustedUserDropdownItem.exists()).to.equal(false);
  });

  it('Should not be able to invite users on the trusted user dropdown item when permissions are undefined', () => {
    const testProps = {
      ...testDefaultProps,
      data: {
        loading: false,
      },
    };
    const wrapper = getWrapper(testProps);
    const userPermissionItemContainer = wrapper.find(AkDropdown);
    const trustedUserDropdownItem = userPermissionItemContainer.find(TrustedUserPermissionDropdownItem);
    expect(canInviteUsers(testProps)).to.be.equal(false);
    expect(trustedUserDropdownItem.props().canInviteUsers).to.equal(false);
  });

  it('Should not be able to invite users on the trusted user dropdown item when there are no permissions at all', () => {
    const testProps = {
      ...testDefaultProps,
      data: {
        loading: false,
        rolePermissions: null,
      },
    };
    const wrapper = getWrapper(testProps);
    const userPermissionItemContainer = wrapper.find(AkDropdown);
    const trustedUserDropdownItem = userPermissionItemContainer.find(TrustedUserPermissionDropdownItem);
    expect(canInviteUsers(testProps)).to.be.equal(false);
    expect(trustedUserDropdownItem.props().canInviteUsers).to.equal(false);
  });

  it('Should not be able to invite users on the trusted user dropdown item when it does not include INVITE_USERS_PERMISSION', () => {
    const testProps = {
      ...testDefaultProps,
      data: {
        loading: false,
        rolePermissions: {
          permissionIds: ['a-permission', 'another-permission'],
        },
      },
    };
    const wrapper = getWrapper(testProps);
    const userPermissionItemContainer = wrapper.find(AkDropdown);
    const trustedUserDropdownItem = userPermissionItemContainer.find(TrustedUserPermissionDropdownItem);
    expect(trustedUserDropdownItem.props().canInviteUsers).to.equal(false);
    expect(canInviteUsers(testProps)).to.be.equal(false);

  });

  it('Should able to invite users on the trusted user dropdown item when it does include INVITE_USERS_PERMISSION', () => {
    const wrapper = getWrapper(testDefaultProps);
    const trustedUserComponent = wrapper.find(TrustedUserPermissionDropdownItem);
    expect(canInviteUsers(testDefaultProps)).to.be.equal(true);
    expect(trustedUserComponent.props().canInviteUsers).to.be.equal(true);
  });
});
