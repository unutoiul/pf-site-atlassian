import * as React from 'react';

import { siteAdministratorBase64Svg } from 'common/images';

import { DEFAULT_BACKGROUND_COLOR } from './style';
import { BasePermissionItemProps, PermissionDropDownItem } from './user-permission-item';

import { Permissions } from './permissions';
import { labels } from './user-permission-item-labels';

export const SiteAdministratorPermissionDropdownItem: React.SFC<BasePermissionItemProps> = ({
  textColor,
  backgroundColor,
  onClickHandler,
  selected,
  canInviteUsers,
  isDisabled,
}) => (
  <PermissionDropDownItem
    onClickHandler={onClickHandler}
    image={siteAdministratorBase64Svg}
    messages={labels[Permissions.SITEADMIN]}
    textColor={textColor}
    backgroundColor={selected ? DEFAULT_BACKGROUND_COLOR : backgroundColor}
    canInviteUsers={canInviteUsers}
    isDisabled={isDisabled}
  />
);
