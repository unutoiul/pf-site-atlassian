import { storiesOf } from '@storybook/react';
import * as React from 'react';

import { SiteAdministratorPermissionDropdownItem } from './site-administrator-permission-item';
import { DEFAULT_BACKGROUND_COLOR, DEFAULT_FONT_COLOR, SELECTED_BACKGROUND_COLOR, SELECTED_FONT_COLOR } from './style';
import { BasePermissionItemProps } from './user-permission-item';

const clickHandler = () => ({ a: 1 });

storiesOf('Site|User Permission Dropdowns/Site administrator', module)
  .add('Default', () => {
    const props: BasePermissionItemProps = {
      textColor: DEFAULT_FONT_COLOR,
      backgroundColor: DEFAULT_BACKGROUND_COLOR,
      selected: false ,
      onClickHandler: clickHandler,
    };

    return (
      <div style={{ width: '550px' }}>
        <SiteAdministratorPermissionDropdownItem {...props} />
      </div>
    );
  })
  .add('Selected', () => {
    const props: BasePermissionItemProps = {
      textColor: SELECTED_BACKGROUND_COLOR,
      backgroundColor: SELECTED_FONT_COLOR,
      selected: true ,
      onClickHandler: clickHandler,
    };

    return (
      <div style={{ width: '550px' }}>
        <SiteAdministratorPermissionDropdownItem {...props} />
      </div>
    );
  });
