import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import { BasicUserPermissionDropdownItem } from './basic-user-permission-item';
import { BasePermissionItemProps, PermissionDropDownItem } from './user-permission-item';

describe('User invite drawer - user permission  section - basic user permission', () => {
  const sandbox = sinon.sandbox.create();

  let onClickHandlerSpy: sinon.SinonSpy;

  beforeEach(() => {
    onClickHandlerSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  const testDefaultProps: BasePermissionItemProps = {
    textColor: 'red',
    backgroundColor: 'blue',
    selected: false ,
    onClickHandler: () => onClickHandlerSpy,
  };

  const getWrapper = (props: BasePermissionItemProps) => {
    return shallow(
      <BasicUserPermissionDropdownItem {...props} />,
    );
  };

  it('Should render the Basic user permission dropdown on the invite drawer', () => {
    const wrapper = getWrapper(testDefaultProps);
    const basicUserPermissionItem = wrapper.find(PermissionDropDownItem);
    expect(basicUserPermissionItem.exists()).to.equal(true);
    expect(basicUserPermissionItem.prop('messages').label.defaultMessage).to.equal('Basic');
    expect(basicUserPermissionItem.prop('textColor')).to.equal('red');
    expect(basicUserPermissionItem.prop('backgroundColor')).to.equal('blue');
    expect(onClickHandlerSpy.called).to.equal(false);
  });
});
