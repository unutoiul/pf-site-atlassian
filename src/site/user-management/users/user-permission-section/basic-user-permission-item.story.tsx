import { storiesOf } from '@storybook/react';
import * as React from 'react';

import { createMockIntlProp } from '../../../../utilities/testing';

import { BasicUserPermissionDropdownItem } from './basic-user-permission-item';
import { DEFAULT_BACKGROUND_COLOR, DEFAULT_FONT_COLOR, SELECTED_BACKGROUND_COLOR, SELECTED_FONT_COLOR } from './style';
import { BasePermissionItemProps } from './user-permission-item';

const clickHandler = () => ({ a: 1 });

storiesOf('Site|User Permission Dropdowns/Basic User', module)
  .add('Default', () => {
    const props: BasePermissionItemProps = {
      textColor: DEFAULT_FONT_COLOR,
      backgroundColor: DEFAULT_BACKGROUND_COLOR,
      selected: false ,
      onClickHandler: clickHandler,
    };

    return (
      <div style={{ width: '550px' }}>
        <BasicUserPermissionDropdownItem {...props} />
      </div>
    );
  })
  .add('Selected', () => {
    const props = {
      textColor: SELECTED_BACKGROUND_COLOR,
      backgroundColor: SELECTED_FONT_COLOR,
      selected: true ,
      onClickHandler: clickHandler,
      intl: createMockIntlProp(),
    };

    return (
      <div style={{ width: '550px' }}>
        <BasicUserPermissionDropdownItem {...props} />
      </div>
    );
  });
