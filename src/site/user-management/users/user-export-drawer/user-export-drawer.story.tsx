import { storiesOf } from '@storybook/react';
import * as React from 'react';
import ApolloProvider from 'react-apollo/ApolloProvider';
import { IntlProvider } from 'react-intl';

import { createApolloClient } from '../../../../apollo-client';
import { createStorybookAnalyticsClient } from '../../../../utilities/storybooks';
import { createMockIntlProp } from '../../../../utilities/testing';
import { UserExportDrawerImpl } from './user-export-drawer';

const client = createApolloClient();

const defaultProps = {
  isOpen: true,
  data: {
    loading: false,
    groupList: {
      __typename: 'PaginatedGroups',
      total: 1,
      groups: [{
        __typename: 'Group',
        id: 'abcd-efgh',
        name: 'Party Corgi',
      }],
    },
    siteUsersList: {
      __typename: 'PaginatedUsers',
      total: 42,
    },
  } as any,
  intl: createMockIntlProp(),
  analyticsClient: createStorybookAnalyticsClient(),
  download: undefined as any,
  showFlag: undefined as any,
  hideFlag: undefined as any,
};

storiesOf('Site|User Export Drawer', module)
  .add('Default', () => {
    return (
      <IntlProvider locale="en">
        <ApolloProvider client={client}>
          <UserExportDrawerImpl {...defaultProps} />
        </ApolloProvider>
      </IntlProvider>
    );
  })
  .add('With loading', () => {
    return (
      <IntlProvider locale="en">
        <ApolloProvider client={client}>
          <UserExportDrawerImpl
            {...defaultProps}
            data={{
              ...defaultProps.data,
              loading: true,
            }}
          />
        </ApolloProvider>
      </IntlProvider>
    );
  })
  .add('With loading error', () => {
    return (
      <IntlProvider locale="en">
        <ApolloProvider client={client}>
          <UserExportDrawerImpl
            {...defaultProps}
            data={{
              ...defaultProps.data,
              loading: false,
              error: true,
            }}
          />
        </ApolloProvider>
      </IntlProvider>
    );
  });
