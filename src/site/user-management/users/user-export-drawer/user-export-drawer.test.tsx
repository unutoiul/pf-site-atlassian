import { ApolloError, ApolloQueryResult } from 'apollo-client';
import { assert, expect } from 'chai';
import { mount, shallow } from 'enzyme';
import * as React from 'react';
import { DataValue, OperationVariables } from 'react-apollo';
import { IntlProvider } from 'react-intl';
import * as sinon from 'sinon';

import { Checkbox as AkCheckbox } from '@atlaskit/checkbox';
import { AkRadio } from '@atlaskit/field-radio-group';
import AkCrossIcon from '@atlaskit/icon/glyph/cross';
import AkSelect from '@atlaskit/select';
import AkSpinner from '@atlaskit/spinner';

import { Button as AnalyticsButton, ScreenEventSender } from 'common/analytics';
import { GenericErrorMessage, RequestError } from 'common/error';
import { FlagProvider } from 'common/flag';

import { GenerateUserExportMutation, GenerateUserExportMutationVariables, UserExportQuery } from '../../../../schema/schema-types';
import {
  createMockAnalyticsClient,
  createMockContext,
  createMockIntlProp,
} from '../../../../utilities/testing';
import { ExportType, messages, UserExportDrawerImpl, UserExportDrawerState, UserStatus } from './user-export-drawer';

const cloudId = 'DUMMY-CLOUD-ID';

describe('User export drawer', () => {
  const sandbox = sinon.sandbox.create();
  const analyticsClient = createMockAnalyticsClient();
  const onCloseSpy = sandbox.spy();
  const showFlagSpy = sandbox.spy();
  const hideFlagSpy = sandbox.spy();
  const downloadMutationSpy = sandbox.stub();
  const blobUrl = 'blob:http://example.com/66d2b1b4-b7b5-457c-a9c0-f4aee87a9b6f';
  const successfulPromise = Promise.resolve({
    data: {
      userExport: blobUrl,
    },
  } as ApolloQueryResult<GenerateUserExportMutation>);
  const failingPromise = async () => Promise.reject(new Error('oh noes'));

  beforeEach(() => {
    downloadMutationSpy.returns(successfulPromise);
  });
  afterEach(() => {
    sandbox.reset();
    analyticsClient.reset();
  });
  after(() => {
    sandbox.restore();
  });

  const userExportQueryData: UserExportQuery = {
    groupList: {
      __typename: 'PaginatedGroups',
      total: 1,
      groups: [{
        __typename: 'Group',
        id: 'abcd-efgh',
        name: 'Party Corgi',
      }],
    },
    siteUsersList: {
      __typename: 'PaginatedUsers',
      total: 42,
    },
  };

  const defaultData: Partial<DataValue<UserExportQuery, OperationVariables>> = {
    loading: false,
    ...userExportQueryData,
  };

  interface GetWrapperProps {
    data?: Partial<DataValue<UserExportQuery, OperationVariables>>;
  }

  const getWrapper = ({ data }: GetWrapperProps = { data: defaultData }) => shallow<{}, UserExportDrawerState>(
    <UserExportDrawerImpl
      analyticsClient={analyticsClient}
      cloudId={cloudId}
      data={data as any}
      download={downloadMutationSpy}
      intl={createMockIntlProp()}
      isOpen={true}
      onClose={onCloseSpy}
      showFlag={showFlagSpy}
      hideFlag={hideFlagSpy}
    />,
  );

  const getMountedWrapper = ({ data }: GetWrapperProps = { data: defaultData }) => mount<{}, UserExportDrawerState>(
    <FlagProvider>
      <IntlProvider locale="en">
        <UserExportDrawerImpl
          analyticsClient={analyticsClient}
          cloudId={cloudId}
          data={data as any}
          download={downloadMutationSpy}
          intl={createMockIntlProp()}
          isOpen={true}
          onClose={onCloseSpy}
          showFlag={showFlagSpy}
          hideFlag={hideFlagSpy}
        />
      </IntlProvider>
    </FlagProvider>,
    createMockContext({ intl: true, apollo: true }),
  );

  const fakeEvent = { preventDefault: sandbox.spy() };
  const submitForm = (wrapper) => {
    const form = wrapper.find('form');
    const onSubmit = form.prop('onSubmit') as (e: React.FormEvent<HTMLFormElement>) => void;
    onSubmit(fakeEvent as any);
  };

  it('should show a spinner while loading', () => {
    const wrapper = getWrapper({
      data: {
        loading: true,
      },
    });

    expect(wrapper.find(AkSpinner).exists()).to.equal(true);
  });

  it('should show an error if one exists', () => {
    const wrapper = getWrapper({
      data: {
        loading: false,
        error: new ApolloError({ graphQLErrors: [{ originalError: new RequestError({}), name: '', message: '' }] }),
      },
    });

    expect(wrapper.find(GenericErrorMessage).exists()).to.equal(true);
  });

  it(`should render the drawer if no error and it's finished loading`, () => {
    const wrapper = getWrapper();

    expect(wrapper.find(AkSpinner).exists()).to.equal(false);
    expect(wrapper.find('form').exists()).to.equal(true);
  });

  it('should disable the groups picker when "all users on site" is selected', () => {
    const wrapper = getWrapper();
    wrapper.setState({ exportType: ExportType.SELECTED_GROUPS });

    expect(wrapper.find(AkSelect).prop('isDisabled')).to.equal(false);

    wrapper.setState({ exportType: ExportType.ALL });

    expect(wrapper.find(AkSelect).prop('isDisabled')).to.equal(true);
  });

  describe('form is submitted', () => {
    it('should set isExporting and call preventDefault on the submit event', () => {
      const wrapper = getWrapper();
      submitForm(wrapper);

      expect(wrapper.state('isExporting')).to.equal(true);
      expect(fakeEvent.preventDefault.called).to.equal(true);
    });

    it('should have isDisabled/isLoading props on relevant fields and buttons while isExporting', () => {
      const wrapper = getWrapper();
      wrapper.setState({ isExporting: true });

      const radios = wrapper.find(AkRadio);
      expect(radios).to.have.length(4);
      radios.forEach(radio => {
        expect(radio.prop('isDisabled'), `${radio.prop('name')} radio (value: ${radio.prop('value')}) should be disabled`).to.equal(true);
      });

      const checkboxes = wrapper.find(AkCheckbox);
      expect(checkboxes).to.have.length(2);
      checkboxes.forEach(checkbox => {
        expect(checkbox.prop('isDisabled'), `${checkbox.prop('name')} checkbox (value: ${checkbox.prop('value')}) should be disabled`).to.equal(true);
      });

      expect(wrapper.find(AkSelect).prop('isDisabled')).to.equal(true);
      expect(wrapper.find(AnalyticsButton).first().prop('isLoading')).to.equal(true);
    });

    it('should call download with the correct variables for an export with the default options selected', () => {
      const wrapper = getWrapper();
      submitForm(wrapper);

      expect(downloadMutationSpy.called).to.equal(true);
      expect(downloadMutationSpy.firstCall.args[0]).to.deep.equal({
        variables: {
          cloudId,
          input: {
            includeProductAccess: false,
            includeGroups: false,
            includeInactiveUsers: false,
            selectedGroupIds: [],
          },
        } as GenerateUserExportMutationVariables,
      });
    });

    it('should call download with the correct variables when groups are picked but the "all users on site" option is selected', () => {
      const wrapper = getWrapper();
      wrapper.setState({
        exportType: ExportType.ALL,
        groupSelectOptions: [{
          label: userExportQueryData.groupList.groups[0].name,
          value: userExportQueryData.groupList.groups[0].id,
        }],
      });

      submitForm(wrapper);

      expect(downloadMutationSpy.called).to.equal(true);
      expect(downloadMutationSpy.firstCall.args[0]).to.deep.equal({
        variables: {
          cloudId,
          input: {
            includeProductAccess: false,
            includeGroups: false,
            includeInactiveUsers: false,
            selectedGroupIds: [],
          },
        } as GenerateUserExportMutationVariables,
      });
    });

    it('should call download with the correct variables when all options have been changed', () => {
      const wrapper = getWrapper();
      wrapper.setState({
        exportType: ExportType.SELECTED_GROUPS,
        groupSelectOptions: [{
          label: userExportQueryData.groupList.groups[0].name,
          value: userExportQueryData.groupList.groups[0].id,
        }],
        userStatus: UserStatus.ALL,
        includeGroups: true,
        includeProductAccess: true,
      });

      submitForm(wrapper);

      expect(downloadMutationSpy.called).to.equal(true);
      expect(downloadMutationSpy.firstCall.args[0]).to.deep.equal({
        variables: {
          cloudId,
          input: {
            includeProductAccess: true,
            includeGroups: true,
            includeInactiveUsers: true,
            selectedGroupIds: [userExportQueryData.groupList.groups[0].id],
          },
        } as GenerateUserExportMutationVariables,
      });
    });

    describe('download mutation resolves', () => {
      it('shows a success flag, resets the state and calls onClose', async () => {
        const wrapper = getMountedWrapper();
        const drawer = wrapper.find(UserExportDrawerImpl);
        submitForm(wrapper);

        expect(drawer.state('isExporting')).to.equal(true);

        await successfulPromise;

        expect(showFlagSpy.called).to.equal(true);
        expect(drawer.state('isExporting')).to.equal(false);
        expect(onCloseSpy.called).to.equal(true);
      });

      it('shows the correct information in the success flag including an action to download again', async () => {
        const wrapper = getMountedWrapper();
        submitForm(wrapper);

        await successfulPromise;

        const showFlagArgs = showFlagSpy.firstCall.args[0];
        expect(showFlagArgs).to.include({
          title: messages.successFlagTitle.defaultMessage,
          description: messages.successFlagBody.defaultMessage,
        });
        expect(showFlagArgs.actions).to.have.length(1);
        expect(showFlagArgs.actions[0]).to.include({
          content: messages.successFlagAction.defaultMessage,
          href: blobUrl,
        });

        const downloadLinkOnClick = showFlagArgs.actions[0].onClick as (e: { currentTarget: HTMLAnchorElement }) => void;
        const a = document.createElement('a');
        downloadLinkOnClick({ currentTarget: a });
        expect(a.download).to.equal(messages.downloadFilename.defaultMessage);
      });

      it('revokes the object URL when the success flag is dismissed', async () => {
        const revokeObjectURLStub = sandbox.spy(window.URL, 'revokeObjectURL');

        const wrapper = getWrapper();
        submitForm(wrapper);

        await successfulPromise;

        const showFlagArgs = showFlagSpy.firstCall.args[0];
        showFlagArgs.onDismissed();

        expect(revokeObjectURLStub.called).to.equal(true);
        expect(revokeObjectURLStub.firstCall.args[0]).to.equal(blobUrl);
      });
    });

    describe('download mutation rejects', () => {
      it('should set isExporting to false', async () => {
        downloadMutationSpy.rejects(failingPromise());
        const wrapper = getWrapper();
        submitForm(wrapper);

        expect(wrapper.state('isExporting')).to.equal(true);

        await failingPromise().then(() => assert.fail(), e => {
          expect(e instanceof Error).to.equal(true);
        });

        expect(showFlagSpy.called).to.equal(true);
        const showFlagArgs = showFlagSpy.firstCall.args[0];
        expect(showFlagArgs).to.include({
          title: messages.failFlagTitle.defaultMessage,
          description: messages.failFlagBody.defaultMessage,
        });
        expect(wrapper.state('isExporting')).to.equal(false);
      });
    });
  });

  describe('analytics', () => {
    it('should send a screen event on render', () => {
      const screenEventSender = getWrapper().find(ScreenEventSender);

      expect(screenEventSender).to.have.length(1);
      expect(screenEventSender.props().event).to.deep.equal({
        cloudId,
        data: {
          name: 'exportUserDrawer',
          attributes: {
            usersInSiteCount: userExportQueryData.siteUsersList.total,
          },
        },
      });
    });

    it('should send a track event on download', async () => {
      const wrapper = getMountedWrapper();
      submitForm(wrapper);

      expect(analyticsClient.sendTrackEvent.callCount).to.equal(0);

      await successfulPromise;

      expect(analyticsClient.sendTrackEvent.callCount).to.equal(1);
      expect(analyticsClient.sendTrackEvent.firstCall.args[0]).to.deep.equal({
        cloudId,
        data: {
          action: 'downloaded',
          actionSubject: 'userList',
          actionSubjectId: 'downloadExportUserList',
          attributes: {
            downloadedAllUsers: true,
            selectedGroupsCount: undefined,
            userStatusSelected: 'onlyActiveUsers',
            additionalDataSelectedGroupMembership: false,
            additionalDataSelectedProductAccess: false,
            usersInSiteCount: userExportQueryData.siteUsersList.total,
          },
          source: 'exportUserDrawer',
        },
      });
    });

    it('should have ui event data on download button', () => {
      const wrapper = getWrapper();
      const submitButton = wrapper.find(AnalyticsButton).filter('[type="submit"]');

      expect(submitButton).to.have.length(1);
      expect(submitButton.prop('analyticsData')).to.deep.equal({
        action: 'clicked',
        actionSubject: 'button',
        actionSubjectId: 'downloadExportUserList',
        attributes: {
          downloadedAllUsers: true,
          selectedGroupsCount: undefined,
          userStatusSelected: 'onlyActiveUsers',
          additionalDataSelectedGroupMembership: false,
          additionalDataSelectedProductAccess: false,
        },
        source: 'exportUserDrawer',
      });
    });

    it('should have ui event data on cancel button', () => {
      const wrapper = getWrapper();
      const cancelButton = wrapper.find(AnalyticsButton).filter('[appearance="subtle-link"]');

      expect(cancelButton).to.have.length(1);
      expect(cancelButton.prop('analyticsData')).to.deep.equal({
        action: 'clicked',
        actionSubject: 'button',
        actionSubjectId: 'cancelExportUserList',
        source: 'exportUserDrawer',
      });
    });

    it('should send a ui event on close', async () => {
      const wrapper = getMountedWrapper();
      const closeButton = wrapper.find(AkCrossIcon);

      expect(analyticsClient.sendUIEvent.callCount).to.equal(0);

      closeButton.simulate('click');

      expect(analyticsClient.sendUIEvent.callCount).to.equal(1);
      expect(analyticsClient.sendUIEvent.firstCall.args[0]).to.deep.equal({
        cloudId,
        data: {
          action: 'clicked',
          actionSubject: 'button',
          actionSubjectId: 'cancelExportUserList',
          source: 'exportUserDrawer',
        },
      });
    });
  });
});
