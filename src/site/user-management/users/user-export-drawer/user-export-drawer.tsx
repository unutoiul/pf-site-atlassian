import * as React from 'react';
import { ChildProps, graphql, MutationFunc } from 'react-apollo';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import { Checkbox as AkCheckbox } from '@atlaskit/checkbox';
import { AkRadio } from '@atlaskit/field-radio-group';
import AkSelect, { AkSelectOption } from '@atlaskit/select';
import AkSpinner from '@atlaskit/spinner';
import { gridSize as akGridSize } from '@atlaskit/theme';

import { AnalyticsClientProps, Button as AnalyticsButton, cancelExportUserUIEventData, downloadExportUserTrackEventData, downloadExportUserUIEventData, exportUserViewEventData, ScreenEventSender, withAnalyticsClient } from 'common/analytics';
import { ButtonGroup } from 'common/button-group';
import { createErrorIcon, createSuccessIcon, GenericErrorMessage } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';
import { FocusedTask, FocusedTaskProps } from 'common/focused-task';

import { GenerateUserExportMutation, GenerateUserExportMutationVariables, UserExportQuery, UserExportQueryVariables } from '../../../../schema/schema-types';
import { Field, Fieldset, FieldsetLabel } from '../../site-access/shared-styles';
import userExportMutation from './user-export-drawer.mutation.graphql';
import userExportQuery from './user-export-drawer.query.graphql';

export const messages = defineMessages({
  heading: {
    id: 'users.user-export.button.heading',
    description: 'Heading of the user export screen',
    defaultMessage: 'Select users to export',
  },
  description: {
    id: 'users.user-export.button.description',
    description: 'Description of the user export screen',
    defaultMessage: 'We will export selected users to a CSV file that you will download.',
  },
  formExportTypeLegend: {
    id: 'users.user-export.form.export.type.legend',
    description: 'Legend for the Users section of the form',
    defaultMessage: 'Users',
  },
  formExportTypeAll: {
    id: 'users.user-export.form.export.type.radio.all',
    description: 'Title for the radio option for exporting all users in the site',
    defaultMessage: 'All users on the site',
  },
  formExportTypeSelected: {
    id: 'users.user-export.form.export.type.radio.selected',
    description: 'Title for the radio option for exporting users from only selected groups',
    defaultMessage: 'Users from selected groups only:',
  },
  formSelectedGroupsPlaceholder: {
    id: 'users.user-export.form.selected.groups.placeholder',
    description: 'Placeholder for the selected groups multiselect',
    defaultMessage: 'Groups',
  },
  formUserStatusLegend: {
    id: 'users.user-export.form.user.status.legend',
    description: 'Legend for the User status section of the form',
    defaultMessage: 'User status',
  },
  formUserStatusActive: {
    id: 'users.user-export.form.user.status.radio.active',
    description: 'Title for the radio option for users with only active status',
    defaultMessage: 'Only active users',
  },
  formUserStatusAll: {
    id: 'users.user-export.form.user.status.radio.all',
    description: 'Title for the radio option for users with any active status',
    defaultMessage: 'All users',
  },
  formAdditionalDataLegend: {
    id: 'users.user-export.form.additional.data.legend',
    description: 'Legend for the Additional data section of the form',
    defaultMessage: 'Additional data',
  },
  formAdditionalDataGroupMembership: {
    id: 'users.user-export.form.additional.data.checkbox.group.membership',
    description: 'Title for the checkbox for group membership',
    defaultMessage: 'Group membership',
  },
  formAdditionalDataProductAccess: {
    id: 'users.user-export.form.additional.data.checkbox.product.access',
    description: 'Title for the checkbox for product access',
    defaultMessage: 'Product access',
  },
  downloadButton: {
    id: 'users.user-export.button.download',
    description: 'Button to download the user export report',
    defaultMessage: 'Download file',
  },
  cancelButton: {
    id: 'users.user-export.button.cancel',
    description: 'Button to close the user export screen',
    defaultMessage: 'Cancel',
  },
  successFlagTitle: {
    id: 'users.user-export.flag.success.title',
    description: 'Title of the download success flag',
    defaultMessage: 'Your user list is being downloaded',
  },
  successFlagBody: {
    id: 'users.user-export.flag.success.body',
    description: 'Body of the download success flag',
    defaultMessage: 'Has your download not started?',
  },
  successFlagAction: {
    id: 'users.user-export.flag.success.action',
    description: 'Download link on the download success flag',
    defaultMessage: 'Download report again',
  },
  downloadFilename: {
    id: 'users.user-export.download.filename',
    description: 'Filename for user export CSV file',
    defaultMessage: 'export-users.csv',
  },
  failFlagTitle: {
    id: 'users.user-export.flag.fail.title',
    description: 'Title of the download failure flag',
    defaultMessage: 'We had trouble generating the user list',
  },
  failFlagBody: {
    id: 'users.user-export.flag.fail.body',
    description: 'Body of the download failure flag',
    defaultMessage: 'Please try again later.',
  },
});

export const Page = styled.div`
  margin: 0 auto;
  width: ${akGridSize() * 50}px;
`;
const SelectContainer = styled.div`
  margin: ${akGridSize() / 2}px 0 0 ${akGridSize() * 3}px;
`;
const Content = styled.div`
  margin-top: ${akGridSize() * 3}px;
`;

interface OwnProps {
  cloudId?: string;
}
interface MutationProps {
  download: MutationFunc<GenerateUserExportMutation, GenerateUserExportMutationVariables>;
}

export const enum ExportType {
  ALL,
  SELECTED_GROUPS,
}
export const enum UserStatus {
  ACTIVE,
  ALL,
}
export interface UserExportDrawerState {
  isExporting: boolean;
  exportType: ExportType;
  groupSelectOptions: AkSelectOption[];
  userStatus: UserStatus;
  includeGroups: boolean;
  includeProductAccess: boolean;
}

type UserExportDrawerProps = OwnProps & FocusedTaskProps & InjectedIntlProps & FlagProps & AnalyticsClientProps;

const initialState: Readonly<UserExportDrawerState> = {
  isExporting: false,
  exportType: ExportType.ALL,
  groupSelectOptions: [],
  userStatus: UserStatus.ACTIVE,
  includeGroups: false,
  includeProductAccess: false,
};

export class UserExportDrawerImpl extends React.Component<ChildProps<UserExportDrawerProps & MutationProps, UserExportQuery>, UserExportDrawerState> {
  public readonly state = initialState;

  public render() {
    const {
      data,
      intl: { formatMessage },
    } = this.props;

    return (
      <FocusedTask isOpen={this.props.isOpen} onClose={this.onBackButtonClicked}>
        <Page>
          <h3>{formatMessage(messages.heading)}</h3>
          <FormattedMessage {...messages.description} tagName="p" />
          <Content>
            {!data || data.loading ? <AkSpinner /> : data.error ? <GenericErrorMessage /> : this.renderForm()}
          </Content>
        </Page>
      </FocusedTask>
    );
  }

  private renderForm = (): React.ReactNode => {
    const {
      cloudId,
      data,
      intl: { formatMessage },
    } = this.props;

    if (!data || !data.groupList || !data.siteUsersList) {
      return undefined;
    }

    return (
      <ScreenEventSender event={{ cloudId, data: exportUserViewEventData(data.siteUsersList.total) }}>
        <form onSubmit={this.onSubmit}>
          <Fieldset>
            <FieldsetLabel>{formatMessage(messages.formExportTypeLegend)}</FieldsetLabel>
            <Field>
              <AkRadio
                name="exportType"
                value={ExportType.ALL.toString()}
                isSelected={this.state.exportType === ExportType.ALL}
                onChange={this.setExportType(ExportType.ALL)}
                isDisabled={this.state.isExporting}
              >
                {formatMessage(messages.formExportTypeAll)}
              </AkRadio>
            </Field>
            <Field>
              <AkRadio
                name="exportType"
                value={ExportType.SELECTED_GROUPS.toString()}
                isSelected={this.state.exportType === ExportType.SELECTED_GROUPS}
                onChange={this.setExportType(ExportType.SELECTED_GROUPS)}
                isDisabled={this.state.isExporting}
              >
                {formatMessage(messages.formExportTypeSelected)}
              </AkRadio>
              <SelectContainer>
                <AkSelect
                  options={data.groupList.groups.map(group => ({ label: group.name, value: group.id } as AkSelectOption))}
                  isClearable={true}
                  isMulti={true}
                  value={this.state.groupSelectOptions}
                  placeholder={formatMessage(messages.formSelectedGroupsPlaceholder)}
                  onChange={this.onSelectedGroupsChange}
                  isDisabled={this.state.isExporting || this.state.exportType !== ExportType.SELECTED_GROUPS}
                />
              </SelectContainer>
            </Field>
          </Fieldset>
          <Fieldset>
            <FieldsetLabel>{formatMessage(messages.formUserStatusLegend)}</FieldsetLabel>
            <Field>
              <AkRadio
                name="userStatus"
                value={UserStatus.ACTIVE.toString()}
                isSelected={this.state.userStatus === UserStatus.ACTIVE}
                onChange={this.setUserStatus(UserStatus.ACTIVE)}
                isDisabled={this.state.isExporting}
              >
                {formatMessage(messages.formUserStatusActive)}
              </AkRadio>
            </Field>
            <Field>
              <AkRadio
                name="userStatus"
                value={UserStatus.ALL.toString()}
                isSelected={this.state.userStatus === UserStatus.ALL}
                onChange={this.setUserStatus(UserStatus.ALL)}
                isDisabled={this.state.isExporting}
              >
                {formatMessage(messages.formUserStatusAll)}
              </AkRadio>
            </Field>
          </Fieldset>
          <Fieldset>
            <FieldsetLabel>{formatMessage(messages.formAdditionalDataLegend)}</FieldsetLabel>
            <Field>
              <AkCheckbox
                name="includeGroups"
                label={formatMessage(messages.formAdditionalDataGroupMembership)}
                isChecked={this.state.includeGroups}
                onChange={this.setIncludeGroups}
                isDisabled={this.state.isExporting}
              />
            </Field>
            <Field>
              <AkCheckbox
                name="includeProductAccess"
                label={formatMessage(messages.formAdditionalDataProductAccess)}
                isChecked={this.state.includeProductAccess}
                onChange={this.setProductAccess}
                isDisabled={this.state.isExporting}
              />
            </Field>
          </Fieldset>
          <ButtonGroup alignment="right">
            <AnalyticsButton
              appearance="primary"
              type="submit"
              isLoading={this.state.isExporting}
              analyticsData={downloadExportUserUIEventData({
                downloadedAllUsers: this.state.exportType === ExportType.ALL,
                selectedGroupsCount: this.state.exportType === ExportType.ALL ? undefined : this.state.groupSelectOptions.length,
                userStatusSelected: this.state.userStatus === UserStatus.ALL ? 'allUsers' : 'onlyActiveUsers',
                additionalDataSelectedGroupMembership: this.state.includeGroups,
                additionalDataSelectedProductAccess: this.state.includeProductAccess,
              })}
            >
              {formatMessage(messages.downloadButton)}
            </AnalyticsButton>
            <AnalyticsButton
              appearance="subtle-link"
              onClick={this.close}
              analyticsData={cancelExportUserUIEventData()}
            >
              {formatMessage(messages.cancelButton)}
            </AnalyticsButton>
          </ButtonGroup>
        </form>
      </ScreenEventSender>
    );
  }

  private setExportType = (exportType: ExportType) => () => this.setState({ exportType });

  private setUserStatus = (userStatus: UserStatus) => () => this.setState({ userStatus });

  private setIncludeGroups = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ includeGroups: e.currentTarget.checked });
  };

  private setProductAccess = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ includeProductAccess: e.currentTarget.checked });
  };

  private onSelectedGroupsChange = (groupSelectOptions: AkSelectOption[]) => {
    this.setState({ groupSelectOptions });
  };

  private onBackButtonClicked = () => {
    this.props.analyticsClient.sendUIEvent({
      cloudId: this.props.cloudId,
      data: cancelExportUserUIEventData(),
    });

    this.close();
  };

  private close = () => {
    this.resetState();
    if (this.props.onClose) {
      this.props.onClose();
    }
  }

  private resetState = () => this.setState(initialState);

  private addDownloadAttributeToFlagAction = ({ currentTarget: a }: React.MouseEvent<HTMLAnchorElement>) => {
    a.download = this.props.intl.formatMessage(messages.downloadFilename);
  }

  private attemptDownload(href: string) {
    const a = document.createElement('a');
    a.href = href;
    a.download = this.props.intl.formatMessage(messages.downloadFilename);
    a.dispatchEvent(new MouseEvent('click', {
      view: window,
      bubbles: true,
      cancelable: true,
    }));
  }

  private onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    this.setState({ isExporting: true });
    const {
      cloudId,
      data,
      download,
      intl: { formatMessage },
      showFlag,
    } = this.props;
    const {
      exportType,
      groupSelectOptions,
      userStatus,
      includeGroups,
      includeProductAccess,
    } = this.state;

    if (!cloudId || !data || !data.siteUsersList) {
      return;
    }

    const usersInSiteCount = data.siteUsersList.total;

    download({
      variables: {
        cloudId,
        input: {
          includeProductAccess,
          includeGroups,
          includeInactiveUsers: userStatus === UserStatus.ALL,
          selectedGroupIds: exportType === ExportType.ALL ? [] : groupSelectOptions.map(groupSelectOption => groupSelectOption.value),
        },
      },
    }).then(({ data: { userExport } }) => {
      this.attemptDownload(userExport);

      this.props.analyticsClient.sendTrackEvent({
        cloudId,
        data: downloadExportUserTrackEventData({
          downloadedAllUsers: exportType === ExportType.ALL,
          selectedGroupsCount: exportType === ExportType.ALL ? undefined : groupSelectOptions.length,
          userStatusSelected: userStatus === UserStatus.ALL ? 'allUsers' : 'onlyActiveUsers',
          usersInSiteCount,
          additionalDataSelectedGroupMembership: includeGroups,
          additionalDataSelectedProductAccess: includeProductAccess,
        }),
      });

      showFlag({
        id: `users.user-export.flag.success${Date.now()}`,
        autoDismiss: false,
        icon: createSuccessIcon(),
        title: formatMessage(messages.successFlagTitle),
        description: formatMessage(messages.successFlagBody),
        actions: [{
          content: formatMessage(messages.successFlagAction),
          href: userExport,
          onClick: this.addDownloadAttributeToFlagAction,
        }],
        onDismissed: () => window.URL.revokeObjectURL(userExport),
      });

      this.close();
    }).catch(() => {
      this.setState({ isExporting: false });

      showFlag({
        id: `users.user-export.flag.fail${Date.now()}`,
        autoDismiss: true,
        icon: createErrorIcon(),
        title: formatMessage(messages.failFlagTitle),
        description: formatMessage(messages.failFlagBody),
      });
    });
  }
}

const withUserExport = graphql<UserExportDrawerProps, UserExportQuery>(userExportQuery, {
  options: ({ cloudId }) => ({
    variables: {
      cloudId,
    } as UserExportQueryVariables,
  }),
  skip: ({ cloudId, isOpen }: UserExportDrawerProps) => !cloudId || !isOpen,
});

const withMutation = graphql<UserExportDrawerProps>(userExportMutation, {
  name: 'download',
});

export const UserExportDrawer = injectIntl(
  withAnalyticsClient(
    withFlag(
      withUserExport(
        withMutation(
          UserExportDrawerImpl,
        ),
      ),
    ),
  ),
);
