import * as React from 'react';
import { ChildProps, compose, graphql } from 'react-apollo';
import { defineMessages } from 'react-intl';
import { connect, DispatchProp } from 'react-redux';

import {
  AnalyticsClientProps,
  withAnalyticsClient,
} from 'common/analytics';
import {
  NavigationLinkIconGlyph,
  NavigationMounterProps,
  NavigationSectionId,
  NavItem,
  updateNavigationSection,
} from 'common/navigation';

import { UserManagementMigrationQuery } from '../../schema/schema-types';
import { OLD_APPS_ROUTE, OLD_GROUPS_OVERVIEW_ROUTE, OLD_GSUITE_ROUTE, OLD_SIGNUP_ROUTE, OLD_USERS_ROUTE } from '../page-redirect/page-redirect';
import { UserConnectedAppsPage } from '../user-connected-apps';
import { AccessConfigurationPage } from './access-config/access-config-page';
import { AccessRequestsPage } from './access-requests/access-requests-page';
import { GSuitePage } from './g-suite/g-suite-page';
import { GroupsPage } from './groups/groups-page';
import { SiteAccessPage } from './site-access/site-access-page';
import { SiteUrlPage } from './site-url/site-url-page';
import UserManagementMigration from './user-management-migration.query.graphql';
import { UsersPage } from './users/user-list/users-page';

export const messages = defineMessages({
  siteAccess: {
    defaultMessage: 'Site access',
    id: 'chrome.nav-sections.site-settings.site-access',
    description: 'Navigation link for the site access settings page',
  },
  siteUrl: {
    defaultMessage: 'Site URL',
    id: 'chrome.nav-sections.site-settings.site-url',
    description: 'Navigation link for the site url settings page',
  },
  appAccess: {
    defaultMessage: 'Product access',
    id: 'chrome.nav-sections.site-settings.application-access',
    description: 'Navigation link for the application access settings page',
  },
  groups: {
    defaultMessage: 'Groups',
    id: 'chrome.nav-sections.site-settings.groups',
    description: 'Navigation link for the groups settings page',
  },
  users: {
    defaultMessage: 'Users',
    id: 'chrome.nav-sections.site-settings.users',
    description: 'Navigation link for the users list page',
  },
  accessRequests: {
    defaultMessage: 'Access Requests',
    id: 'chrome.nav-sections.site-settings.access-requests',
    description: 'Navigation link for the access requests page',
  },
  gsuite: {
    defaultMessage: 'G Suite',
    id: 'chrome.nav-sections.site-settings.gsuite',
    description: 'Navigation link for the G Suite page',
  },
  emoji: {
    defaultMessage: 'Emoji',
    id: 'chrome.nav-sections.site-settings.emoji',
    description: 'Navigation link for the emoji settings page',
  },
  userConnectedApps: {
    defaultMessage: 'Connected apps',
    id: 'chrome.nav-sections.site-settings.user-connected-apps',
    description: 'Navigation link for the user connected apps page',
  },
});

interface State {
  registered: boolean;
}

type UmMigrationNavMounterProps = ChildProps<NavigationMounterProps & AnalyticsClientProps, UserManagementMigrationQuery> & DispatchProp<any>;

export class UserManagementMigrationMounterImpl extends React.Component<UmMigrationNavMounterProps, State> {
  public state: Readonly<State> = {
    registered: false,
  };

  public componentWillReceiveProps(nextProps: UmMigrationNavMounterProps) {
    const {
      data: {
        loading = false,
        currentSite = null,
      } = {},
      registerRoutes,
    } = nextProps;

    if (this.state.registered || loading || !currentSite || !currentSite.flags) {
      return;
    }

    const flags = currentSite.flags;
    const cloudId = currentSite.id;

    const {
      selfSignupADG3Migration,
      siteUrlPage,
      accessConfigADG3Migration,
      groupsADG3Migration,
      gSuiteADG3Migration,
      usersADG3Migration,
      accessRequestsPage,
      userConnectedAppsPage,
    } = flags;

    nextProps.dispatch(updateNavigationSection({
      id: NavigationSectionId.SiteSettings,
      links: this.generateSiteSettingsLinks(cloudId, { signupFlag: selfSignupADG3Migration }),
    }));

    if (siteUrlPage) {
      nextProps.dispatch(updateNavigationSection({
        id: NavigationSectionId.SiteSettings,
        links: this.generateSiteUrlLinks(cloudId),
      }));
    }

    nextProps.dispatch(updateNavigationSection({
      id: NavigationSectionId.SiteSettings,
      links: this.generateUserManagementLinks(cloudId, { appFlag: accessConfigADG3Migration }),
    }));

    nextProps.dispatch(updateNavigationSection({
      id: NavigationSectionId.SiteSettings,
      links: this.generateEmojiLinks(cloudId),
    }));

    nextProps.dispatch(updateNavigationSection({
      id: NavigationSectionId.UserManagement,
      links: this.generateGroupsLinks(cloudId, { flag: groupsADG3Migration }),
    }));

    nextProps.dispatch(updateNavigationSection({
      id: NavigationSectionId.UserManagement,
      links: this.generateUsersLinks(cloudId, { flag: usersADG3Migration }),
    }));

    nextProps.dispatch(updateNavigationSection({
      id: NavigationSectionId.SiteSettings,
      links: this.generateGSuiteLinks(cloudId, { flag: gSuiteADG3Migration }),
    }));

    if (accessRequestsPage) {
      nextProps.dispatch(updateNavigationSection({
        id: NavigationSectionId.UserManagement,
        links: this.generateAccessRequestsLinks(cloudId),
      }));
    }

    if (userConnectedAppsPage) {
      nextProps.dispatch(updateNavigationSection({
        id: NavigationSectionId.Organizations,
        links: this.generateUserConnectedAppsLinks(cloudId),
      }));
    }

    if (accessConfigADG3Migration) {
      registerRoutes({ switchRoutes: [{ path: `admin/s/${cloudId}/apps`, component: AccessConfigurationPage }] });
    }

    if (selfSignupADG3Migration) {
      registerRoutes({ switchRoutes: [{ path: `admin/s/${cloudId}/signup`, component: SiteAccessPage }] });
      registerRoutes({ switchRoutes: [{ path: `admin/s/${cloudId}/rename`, component: SiteUrlPage }] });
    }

    if (groupsADG3Migration) {
      registerRoutes({ switchRoutes: [{ path : `admin/s/${cloudId}/groups`, component: GroupsPage }] });
    }

    if (usersADG3Migration) {
      registerRoutes({ switchRoutes: [{ path : `admin/s/${cloudId}/users`, component: UsersPage }] });
    }

    if (accessRequestsPage) {
      registerRoutes({ switchRoutes: [{ path : `admin/s/${cloudId}/access-requests`, component: AccessRequestsPage }] });
    }

    if (gSuiteADG3Migration) {
      registerRoutes({ switchRoutes: [{ path : `admin/s/${cloudId}/gsuite`, component: GSuitePage }] });
    }

    if (userConnectedAppsPage) {
      registerRoutes({ switchRoutes: [{ path: `/admin/s/${cloudId}/user-connected-apps`, component: UserConnectedAppsPage }] });
    }

    this.setState({ registered: true });
  }

  public render() {
    return null;
  }

  private generateSiteUrlLinks = (cloudId: string): NavItem[] => {
    return [
      {
        analyticsSubjectId: 'site-url',
        path: `/admin/s/${cloudId}/site-url`,
        title: messages.siteUrl,
        icon: NavigationLinkIconGlyph.EditFilled,
        priority: 15,
      },
    ];
  }

  private generateSiteSettingsLinks = (cloudId: string, { signupFlag }): NavItem[] => {
    return [
      {
        analyticsSubjectId: 'self-signup',
        path: signupFlag ? `/admin/s/${cloudId}/signup` : OLD_SIGNUP_ROUTE,
        title: messages.siteAccess,
        icon: NavigationLinkIconGlyph.InviteTeam,
        priority: 10,
      },
    ];
  }

  private generateEmojiLinks = (cloudId: string): NavItem[] => {
    return [
      {
        analyticsSubjectId: 'emoji',
        path: `/admin/s/${cloudId}/emoji`,
        title: messages.emoji,
        icon: NavigationLinkIconGlyph.Emoji,
        priority: 40,
      },
    ];
  };

  private generateUserConnectedAppsLinks = (cloudId: string): NavItem[] => {
    return [
      {
        analyticsSubjectId: 'user-connected-apps',
        path: `/admin/s/${cloudId}/user-connected-apps`,
        title: messages.userConnectedApps,
        icon: NavigationLinkIconGlyph.App,
        priority: 20,
      },
    ];
  }

  private generateUserManagementLinks = (cloudId: string, { appFlag }): NavItem[] => {
    return [
      {
        analyticsSubjectId: 'app-access',
        path: appFlag ? `/admin/s/${cloudId}/apps` : OLD_APPS_ROUTE,
        title: messages.appAccess,
        icon: NavigationLinkIconGlyph.AppAccess,
        priority: 20,
      },
    ];
  }

  private generateGSuiteLinks = (cloudId: string, { flag }): NavItem[] => {
    return [
      {
        analyticsSubjectId: 'gsuite',
        path: flag ? `/admin/s/${cloudId}/gsuite` : OLD_GSUITE_ROUTE,
        title: messages.gsuite,
        icon: NavigationLinkIconGlyph.GsuiteIcon,
        priority: 30,
      },
    ];
  }

  private generateUsersLinks = (cloudId: string, { flag }): NavItem[] => {
    return [
      {
        analyticsSubjectId: 'users',
        path: flag ? `/admin/s/${cloudId}/users` : OLD_USERS_ROUTE,
        title: messages.users,
        icon: NavigationLinkIconGlyph.Person,
        priority: 10,
      },
    ];
  }

  private generateGroupsLinks = (cloudId: string, { flag }): NavItem[] => {
    return [
      {
        analyticsSubjectId: 'groups',
        path: flag ? `/admin/s/${cloudId}/groups` : OLD_GROUPS_OVERVIEW_ROUTE,
        title: messages.groups,
        icon: NavigationLinkIconGlyph.PeopleGroup,
        priority: 20,
      },
    ];
  }

  private generateAccessRequestsLinks = (cloudId: string): NavItem[] => {
    return [
      {
        analyticsSubjectId: 'access-requests',
        path: `/admin/s/${cloudId}/access-requests`,
        title: messages.accessRequests,
        icon: NavigationLinkIconGlyph.Tray,
        priority: 30,
      },
    ];
  }
}

const siteFeatureFlags = graphql<{}, UserManagementMigrationQuery>(UserManagementMigration);
export const UserManagementMigrationMounter = compose(siteFeatureFlags, connect(), withAnalyticsClient)(UserManagementMigrationMounterImpl);
