import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { sandbox as sinonSandbox, SinonSpy } from 'sinon';

import AkButton from '@atlaskit/button';
import AkSpinner from '@atlaskit/spinner';

import { PageLayout } from 'common/page-layout';

import { ScreenEventSender } from 'common/analytics';

import { util } from '../../../../src/utilities/admin-hub';
import { createMockIntlContext, createMockIntlProp } from '../../../utilities/testing';
import { InviteUrls } from './invite-urls';
import { SignUpOptions } from './signup-settings';
import { messages, SiteAccessPageImpl } from './site-access-page';

describe('SiteAccessPage', () => {
  const sandbox = sinonSandbox.create();
  let updateSpy;
  let showFlagSpy;
  let dispatchSpy;
  let refetchSpy;
  let mockAnalyticsClient: {
    sendUIEvent: SinonSpy,
    sendTrackEvent: SinonSpy,
  };

  beforeEach(() => {
    updateSpy = sandbox.spy();
    showFlagSpy = sandbox.spy();
    dispatchSpy = sandbox.spy();
    refetchSpy = sandbox.spy();
    mockAnalyticsClient = {
      sendUIEvent: sandbox.spy(),
      sendTrackEvent: sandbox.spy(),
    };
  });

  afterEach(() => {
    sandbox.restore();
  });

  function shallowSiteAccessPageWithData(data = {}) {
    return shallow(
      <SiteAccessPageImpl
        data={{ refetch: refetchSpy, ...data }}
        update={updateSpy}
        showFlag={showFlagSpy}
        dispatch={dispatchSpy}
        match={{ params: { cloudId: 'FAKE' } }}
        intl={createMockIntlProp()}
        analyticsClient={mockAnalyticsClient}
        {...{} as any}
      />,
      createMockIntlContext(),
    );
  }

  describe('before site access configuration has been fetched', () => {
    it('renders with title, description and no side', () => {
      const wrapper = shallowSiteAccessPageWithData({ loading: true });
      const pageLayout = wrapper.find(PageLayout);
      const pageLayoutProps = pageLayout.props();
      expect(pageLayoutProps.title).to.not.equal(undefined);
      expect(pageLayoutProps.description).to.not.equal(undefined);
      expect(pageLayoutProps.side).to.equal(undefined);
    });

    it('renders a spinner', () => {
      const wrapper = shallowSiteAccessPageWithData({ loading: true });
      expect(wrapper.find(AkSpinner)).to.have.length(1);
    });
  });

  describe('after site access configuration has been fetched', () => {
    it('renders with title, description and side', () => {
      const wrapper = shallowSiteAccessPageWithData({ currentSite: { siteAccess: {} } });
      const pageLayout = wrapper.find(PageLayout);
      const pageLayoutProps = pageLayout.props();
      expect(pageLayoutProps.title).to.equal(messages.title.defaultMessage);
      expect(pageLayoutProps.description).to.equal(messages.description.defaultMessage);
      expect(pageLayoutProps.side).to.not.equal(undefined);
    });

    it('renders without a spinner', () => {
      const wrapper = shallowSiteAccessPageWithData({ currentSite: { siteAccess: {} } });
      expect(wrapper.find(AkSpinner)).to.have.length(0);
    });

    it('defaults to nobody can join', () => {
      const wrapper = shallowSiteAccessPageWithData({ currentSite: { siteAccess: {} } });
      expect(wrapper.state('signupSetting')).to.equal(SignUpOptions.NOBODY);
    });

    it('shows an error message if something went wrong', () => {
      const wrapper = shallowSiteAccessPageWithData({ error: 'Something failed' });
      expect(wrapper.find('span').text()).to.equal(messages.error.defaultMessage);
    });

    it('should send screen analytics when loaded', () => {
      const wrapper = shallowSiteAccessPageWithData({ currentSite: { siteAccess: {} }, loading: false });
      expect(wrapper.find(ScreenEventSender).exists()).to.equal(true);
    });

    it('sets up analytics for onClick with attributes', () => {
      updateSpy = sandbox.stub().resolves(true);
      const wrapper = shallowSiteAccessPageWithData({ currentSite: { siteAccess: { openInvite: true } } });
      const submitButton = wrapper.find(AkButton);
      const onClickHandler = submitButton.prop('onClick')!;

      expect(onClickHandler).to.not.equal(undefined);
      onClickHandler();

      const sendUIEventSpy = mockAnalyticsClient.sendUIEvent;

      expect(sendUIEventSpy.called).to.equal(true);
      expect(sendUIEventSpy.lastCall.args[0]).to.deep.equal({
        cloudId: 'FAKE',
        data: {
          action: 'clicked',
          actionSubject: 'button',
          actionSubjectId: 'saveSelfSignupButton',
          source: 'siteAccessScreen',
        },
      });
    });
  });

  describe('#onSettingChange', () => {
    it('sets notifyAdmin to true when who can join is changed to domain restricted', () => {
      const wrapper = shallowSiteAccessPageWithData({ currentSite: { siteAccess: { notifyAdmin: false } } });
      const siteAccessPage = wrapper.instance() as any;
      siteAccessPage.onSettingChange({ currentTarget: { value: SignUpOptions.DOMAIN_RESTRICTED } });
      expect(wrapper.state('signupSetting')).to.equal(SignUpOptions.DOMAIN_RESTRICTED);
      expect(wrapper.state('notifyAdmin')).to.equal(true);
    });

    it('sets notifyAdmin to false when who can join is changed to off', () => {
      const wrapper = shallowSiteAccessPageWithData({ currentSite: { siteAccess: { notifyAdmin: true } } });
      const siteAccessPage = wrapper.instance() as any;
      siteAccessPage.onSettingChange({ currentTarget: { value: SignUpOptions.NOBODY } });
      expect(wrapper.state('signupSetting')).to.equal(SignUpOptions.NOBODY);
      expect(wrapper.state('notifyAdmin')).to.equal(false);
    });

    it(`doesn't modify notifyAdmin when who can join is changed to on`, () => {
      const wrapper = shallowSiteAccessPageWithData({ currentSite: { siteAccess: { notifyAdmin: true } } });
      const siteAccessPage = wrapper.instance() as any;
      siteAccessPage.onSettingChange({ currentTarget: { value: SignUpOptions.ANYONE } });
      expect(wrapper.state('signupSetting')).to.equal(SignUpOptions.ANYONE);
      expect(wrapper.state('notifyAdmin')).to.equal(true);

      wrapper.setState({ notifyAdmin: false });
      siteAccessPage.onSettingChange({ currentTarget: { value: SignUpOptions.ANYONE } });
      expect(wrapper.state('signupSetting')).to.equal(SignUpOptions.ANYONE);
      expect(wrapper.state('notifyAdmin')).to.equal(false);
    });
  });

  describe('#onDomainAdd', () => {
    it('case insensitively adds the supplied domains to the domains array stored in state without adding duplicates', () => {
      const wrapper = shallowSiteAccessPageWithData({ currentSite: { siteAccess: {} } });
      wrapper.setState({
        domains: ['acme.com', 'acme.com.au'],
      });
      const siteAccessPage = wrapper.instance() as SiteAccessPageImpl;
      siteAccessPage.onDomainAdd(['ACME.com.au', 'acme.co.uk']);

      expect(wrapper.state('domains')).to.deep.equal(['acme.com', 'acme.com.au', 'acme.co.uk']);
    });
  });

  describe('#onDomainRemove', () => {
    it('case insensitively removes a supplied domain from the domains array stored in state', () => {
      const wrapper = shallowSiteAccessPageWithData({ currentSite: { siteAccess: {} } });
      wrapper.setState({
        domains: ['acme.com', 'acme.com.au'],
      });
      const siteAccessPage = wrapper.instance() as SiteAccessPageImpl;
      siteAccessPage.onDomainRemove('ACME.com');

      expect(wrapper.state('domains')).to.deep.equal(['acme.com.au']);
    });
  });

  describe('#onSubmit', () => {
    const getSiteAccessPage = () => shallowSiteAccessPageWithData({ currentSite: { siteAccess: {} } });

    it('sets isSaving to true', () => {
      updateSpy = sandbox.stub().resolves(true);
      const wrapper = getSiteAccessPage();
      const siteAccessPage = wrapper.instance() as any;
      siteAccessPage.onSubmit({ preventDefault: sandbox.spy() });

      expect(wrapper.state('isSaving')).to.equal(true);
    });
    it('triggers preventDefault', () => {
      updateSpy = sandbox.stub().resolves(true);
      const wrapper = getSiteAccessPage();
      const siteAccessPage = wrapper.instance() as any;
      const preventDefaultSpy = sandbox.spy();
      siteAccessPage.onSubmit({ preventDefault: preventDefaultSpy });

      expect(preventDefaultSpy.callCount).to.equal(1);
    });
    describe('on success', () => {
      it('sets isSaving to false', async () => {
        updateSpy = sandbox.stub().resolves(true);
        const wrapper = getSiteAccessPage();
        const siteAccessPage = wrapper.instance() as any;
        await siteAccessPage.onSubmit({ preventDefault: sandbox.spy() });

        expect(wrapper.state('isSaving')).to.equal(false);
      });
      it('shows a successful flag', async () => {
        updateSpy = sandbox.stub().resolves(true);
        const wrapper = getSiteAccessPage();
        const siteAccessPage = wrapper.instance() as any;
        await siteAccessPage.onSubmit({ preventDefault: sandbox.spy() });

        expect(showFlagSpy.callCount).to.equal(1);
      });
      it('should fire a track event', async () => {
        updateSpy = sandbox.stub().resolves(true);
        const wrapper = getSiteAccessPage();
        wrapper.setState({
          openInvite: false,
          notifyAdmin: false,
          signupSetting: 'openInvite',
        });
        const siteAccessPage = wrapper.instance() as any;
        await siteAccessPage.onSubmit({ preventDefault: sandbox.spy() });

        const sendTrackEventSpy = mockAnalyticsClient.sendTrackEvent;

        expect(sendTrackEventSpy.lastCall.args[0]).to.deep.equal({
          cloudId: 'FAKE',
          data: {
            action: 'submitted',
            actionSubject: 'form',
            actionSubjectId: 'siteAccessForm',
            source: 'siteAccessScreen',
            attributes: {
              isOpenInvite: false,
              notify: false,
              restrictions: 'openInvite',
            },
          },
        });
      });
    });
    describe('on failure', () => {
      it('sets isSaving to false', async () => {
        updateSpy = sandbox.stub().rejects();
        const wrapper = getSiteAccessPage();
        const siteAccessPage = wrapper.instance() as any;
        await siteAccessPage.onSubmit({ preventDefault: sandbox.spy() });

        expect(wrapper.state('isSaving')).to.equal(false);
      });
      it('calls refetch', async () => {
        updateSpy = sandbox.stub().rejects();
        const wrapper = getSiteAccessPage();
        const siteAccessPage = wrapper.instance() as any;
        await siteAccessPage.onSubmit({ preventDefault: sandbox.spy() });

        expect(refetchSpy.callCount).to.equal(1);
      });
    });
  });

  describe('#onOpenInviteChange', () => {
    it('sets openInvite to true when open invite checkbox is checked', () => {
      const wrapper = shallowSiteAccessPageWithData({ currentSite: { siteAccess: { openInvite: false } } });
      const siteAccessPage = wrapper.instance() as any;
      siteAccessPage.onOpenInviteChange({ currentTarget: { checked: true } });
      expect(wrapper.state('openInvite')).to.equal(true);
    });
  });

  describe('#mapStateToInput', () => {
    const getSiteAccessPage = () => shallowSiteAccessPageWithData({ currentSite: { siteAccess: {} } });

    it('returns data for self sign up disabled', () => {
      const wrapper = getSiteAccessPage();
      const siteAccessPage = wrapper.instance() as SiteAccessPageImpl;
      wrapper.setState({
        domains: ['doesntdoanything.com'],
        notifyAdmin: false,
        openInvite: false,
        signupSetting: SignUpOptions.NOBODY,
      });

      expect(siteAccessPage.mapStateToInput()).to.deep.equal({
        domains: [],
        notifyAdmin: false,
        openInvite: false,
        signupEnabled: false,
      });
    });
    it('returns data for domain restricted sign up', () => {
      const wrapper = getSiteAccessPage();
      const siteAccessPage = wrapper.instance() as SiteAccessPageImpl;
      wrapper.setState({
        domains: ['foo.com', 'bar.com'],
        notifyAdmin: true,
        openInvite: false,
        signupSetting: SignUpOptions.DOMAIN_RESTRICTED,
      });

      expect(siteAccessPage.mapStateToInput()).to.deep.equal({
        domains: ['foo.com', 'bar.com'],
        notifyAdmin: true,
        openInvite: false,
        signupEnabled: true,
      });
    });
    it('returns data for anonymous sign up', () => {
      const wrapper = getSiteAccessPage();
      const siteAccessPage = wrapper.instance() as SiteAccessPageImpl;
      wrapper.setState({
        domains: ['doesntdoanything.com'],
        notifyAdmin: false,
        openInvite: false,
        signupSetting: SignUpOptions.ANYONE,
      });

      expect(siteAccessPage.mapStateToInput()).to.deep.equal({
        domains: [],
        notifyAdmin: false,
        openInvite: false,
        signupEnabled: true,
      });
    });
    it('returns data with open invitations enabled', () => {
      const wrapper = getSiteAccessPage();
      const siteAccessPage = wrapper.instance() as SiteAccessPageImpl;
      wrapper.setState({
        domains: ['doesntdoanything.com'],
        notifyAdmin: false,
        openInvite: true,
        signupSetting: SignUpOptions.ANYONE,
      });

      expect(siteAccessPage.mapStateToInput()).to.deep.equal({
        domains: [],
        notifyAdmin: false,
        openInvite: true,
        signupEnabled: true,
      });
    });
    it('returns data for no sign up if domain restricted is selected but no domains are entered', () => {
      const wrapper = getSiteAccessPage();
      const siteAccessPage = wrapper.instance() as SiteAccessPageImpl;
      wrapper.setState({
        domains: [],
        notifyAdmin: true,
        openInvite: false,
        signupSetting: SignUpOptions.DOMAIN_RESTRICTED,
      });

      expect(siteAccessPage.mapStateToInput()).to.deep.equal({
        domains: [],
        notifyAdmin: true,
        openInvite: false,
        signupEnabled: false,
      });
    });
  });

  describe('#mapDataToState', () => {
    it('returns state for self sign up disabled', () => {
      expect(SiteAccessPageImpl.mapDataToState({
        currentSite: {
          id: 'dummy-site',
          __typename: 'CurrentSite',
          siteAccess: {
            __typename: 'SiteAccess',
            domains: [],
            notifyAdmin: false,
            openInvite: false,
            signupEnabled: false,
          },
          defaultApps: [],
          inviteUrls: [],
          flags: {
            __typename: 'SiteFlags',
            inviteUrls: true,
          },
        },
      })).to.deep.equal({
        domains: [],
        notifyAdmin: false,
        openInvite: false,
        signupSetting: SignUpOptions.NOBODY,
      });
    });
    it('returns state for domain restricted sign up', () => {
      expect(SiteAccessPageImpl.mapDataToState({
        currentSite: {
          id: 'dummy-site',
          __typename: 'CurrentSite',
          siteAccess: {
            __typename: 'SiteAccess',
            domains: ['foo.com', 'bar.com'],
            notifyAdmin: true,
            openInvite: false,
            signupEnabled: true,
          },
          defaultApps: [],
          inviteUrls: [],
          flags: {
            __typename: 'SiteFlags',
            inviteUrls: true,
          },
        },
      })).to.deep.equal({
        domains: ['foo.com', 'bar.com'],
        notifyAdmin: true,
        openInvite: false,
        signupSetting: SignUpOptions.DOMAIN_RESTRICTED,
      });
    });
    it('returns state for anonymous invite', () => {
      expect(SiteAccessPageImpl.mapDataToState({
        currentSite: {
          id: 'dummy-site',
          __typename: 'CurrentSite',
          siteAccess: {
            __typename: 'SiteAccess',
            domains: [],
            notifyAdmin: false,
            openInvite: false,
            signupEnabled: true,
          },
          defaultApps: [],
          inviteUrls: [],
          flags: {
            __typename: 'SiteFlags',
            inviteUrls: true,
          },
        },
      })).to.deep.equal({
        domains: [],
        notifyAdmin: false,
        openInvite: false,
        signupSetting: SignUpOptions.ANYONE,
      });
    });
    it('returns state for open invitations enabled', () => {
      expect(SiteAccessPageImpl.mapDataToState({
        currentSite: {
          id: 'dummy-site',
          __typename: 'CurrentSite',
          siteAccess: {
            __typename: 'SiteAccess',
            domains: [],
            notifyAdmin: false,
            openInvite: true,
            signupEnabled: true,
          },
          defaultApps: [],
          inviteUrls: [],
          flags: {
            __typename: 'SiteFlags',
            inviteUrls: true,
          },
        },
      })).to.deep.equal({
        domains: [],
        notifyAdmin: false,
        openInvite: true,
        signupSetting: SignUpOptions.ANYONE,
      });
    });
  });

  describe('invite links', () => {
    it('should render', () => {
      const wrapper = shallowSiteAccessPageWithData({
        currentSite: {
          siteAccess: {},
          inviteUrls: [],
          defaultApps: [],
          flags: {
            inviteUrls: true,
          },
        },
      });
      expect(wrapper.find(InviteUrls).length).to.equal(1);
      const urls = wrapper.find(InviteUrls);
      expect(urls.prop('cloudId')).to.equal('FAKE');
      expect(urls.prop('defaultApps')).to.deep.equal([]);
      expect(urls.prop('inviteUrls')).to.deep.equal([]);
    });

    it('should disable via feature flag', () => {
      const wrapper = shallowSiteAccessPageWithData({
        currentSite: {
          siteAccess: {},
          inviteUrls: [],
          defaultApps: [],
          flags: {
            inviteUrls: false,
          },
        },
      });
      expect(wrapper.find(InviteUrls).length).to.equal(0);
    });
  });

  describe('apps link', () => {
    const getLink = () => {
      const wrapper = shallowSiteAccessPageWithData({ currentSite: { defaultApps: [], siteAccess: {} } });
      const sidebar = shallow(wrapper.find(PageLayout).prop('side'));

      return (sidebar.find(FormattedMessage).prop('values')!.appAccessLink as any).props.to;
    };

    it('should have cloud ID in href in admin hub', () => {
      sandbox.stub(util, 'isAdminHub').returns(true);
      const link = getLink();

      expect(link).to.equal('/s/FAKE/apps');
    });

    it('should not have cloud ID in site admin', () => {
      sandbox.stub(util, 'isAdminHub').returns(false);
      const link = getLink();

      expect(link).to.equal('/admin/apps');
    });
  });
});
