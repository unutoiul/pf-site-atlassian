import { ApolloQueryResult } from 'apollo-client';
import * as isEqual from 'lodash.isequal';
import * as React from 'react';
import { ChildProps, compose, graphql, MutationOpts } from 'react-apollo';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import { connect, DispatchProp } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import AkButton from '@atlaskit/button';
import { Checkbox as AkCheckboxStateless } from '@atlaskit/checkbox';
import { AkRadio } from '@atlaskit/field-radio-group';
import AkSpinner from '@atlaskit/spinner';
import { gridSize as akGridSize } from '@atlaskit/theme';

import {
  AnalyticsClientProps,
  ScreenEventSender,
  selfSignUpSaveClickEventData,
  selfSignUpTrackEventData,
  selfSignUpViewEventData,
  withAnalyticsClient,
} from 'common/analytics';
import { createSuccessIcon } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';
import { MultiTagger } from 'common/multi-tagger';
import { PageLayout, PageSidebar } from 'common/page-layout';

import { SiteAccessQuery, UpdateSiteAccessInput, UpdateSiteAccessMutation, UpdateSiteAccessMutationVariables } from '../../../schema/schema-types';
import { util } from '../../../utilities/admin-hub';
import { InviteUrl, InviteUrls } from './invite-urls';
import { Field, FieldDescription, Fieldset, FieldsetLabel } from './shared-styles';
import { getSignupSettings, SignUpOptions } from './signup-settings';
import getSiteAccess from './site-access.query.graphql';
import updateSiteAccess from './update-site-access.mutation.graphql';

export const messages = defineMessages({
  title: {
    id: 'user-management.site-settings.site-access.title',
    defaultMessage: 'Site access',
  },
  description: {
    id: 'user-management.site-settings.site-access.description',
    defaultMessage: 'Avoid having to invite new users one by one by letting your current users invite people too, or even let people join by themselves.',
  },
  error: {
    id: 'user-management.site-settings.site-access.error',
    defaultMessage: 'Site access configuration could not be retrieved.',
  },
  sidebarTitle: {
    id: 'user-management.site-settings.site-access.sidebar.title',
    defaultMessage: 'What you need to know',
  },
  sidebarPoint1WithAccessOne: {
    id: 'user-management.site-settings.site-access.sidebar.point1.with.access.one',
    defaultMessage: 'New users will have access to {product} as set in {appAccessLink}.',
  },
  sidebarPoint1WithAccessTwo: {
    id: 'user-management.site-settings.site-access.sidebar.point1.with.access.two',
    defaultMessage: 'New users will have access to {product1} and {product2} as set in {appAccessLink}.',
  },
  sidebarPoint1WithAccessThree: {
    id: 'user-management.site-settings.site-access.sidebar.point1.with.access.three',
    defaultMessage: 'New users will have access to {product1}, {product2} and {product3} as set in {appAccessLink}.',
  },
  sidebarPoint1WithAccessMany: {
    id: 'user-management.site-settings.site-access.sidebar.point1.with.access.many',
    defaultMessage: `New users will have access to {product1}, {product2}, {product3} and {numAdditionalProducts, plural,
      other {# more}
    } as set in {appAccessLink}.`,
  },
  sidebarPoint1WithoutAccess: {
    id: 'user-management.site-settings.site-access.sidebar.point1.without.access',
    defaultMessage: 'New users {highlight} as set in {appAccessLink}.',
  },
  sidebarPoint1WithoutAccessHighlight: {
    id: 'user-management.site-settings.site-access.sidebar.point1.without.access.highlight',
    defaultMessage: 'will have no product access',
  },
  sidebarPoint1Link: {
    id: 'user-management.site-settings.site-access.sidebar.point1.link',
    defaultMessage: 'default application access setting',
  },
  sidebarPoint2: {
    id: 'user-management.site-settings.site-access.sidebar.point2',
    defaultMessage: `Accounts created by self signup count towards your licence. If you run out of license seats, we'll automatically upgrade you and let you know by email.`,
  },
  invitationSettingsHeading: {
    id: 'user-management.site-settings.site-access.invitation-settings.heading',
    defaultMessage: 'Invitation settings',
  },
  invitationSettingsOpenInvite: {
    id: 'user-management.site-settings.site-access.invitation-settings.open-invite',
    defaultMessage: 'Users can invite others',
  },
  invitationSettingsOpenInviteDesc: {
    id: 'user-management.site-settings.site-access.invitation-settings.open-invite.description',
    defaultMessage: 'Invitations can be sent to any email address.',
  },
  formHeading: {
    id: 'user-management.site-settings.site-access.form.heading',
    defaultMessage: 'Who can join your site',
  },
  formNobodyCanSignUp: {
    id: 'user-management.site-settings.site-access.form.nobody',
    defaultMessage: 'Only invited users can join',
  },
  formDomainRestrictedSignUp: {
    id: 'user-management.site-settings.site-access.form.domainRestricted',
    defaultMessage: 'Anyone with one of the following email address domains can join:',
  },
  formDomainRestrictedDomainsPlaceholder: {
    id: 'user-management.site-settings.site-access.form.domainRestricted.domains.placeholder',
    defaultMessage: 'Domains',
  },
  formDomainRestrictedDomainsDescription: {
    id: 'user-management.site-settings.site-access.form.domainRestricted.domains.description',
    defaultMessage: `Separate domains with a space. We'll notify you about all new users.`,
  },
  formDomainRestrictedDomainsRemove: {
    id: 'user-management.site-settings.site-access.form.domainRestricted.domains.remove',
    defaultMessage: 'Remove domain',
  },
  formAnyoneCanSignUp: {
    id: 'user-management.site-settings.site-access.form.anyone',
    defaultMessage: 'Anyone can join',
  },
  formNotifyOnNewUserHeading: {
    id: 'user-management.site-settings.site-access.form.notify.heading',
    defaultMessage: 'Notifications about new users',
  },
  formNewUserSendEmail: {
    id: 'user-management.site-settings.site-access.form.notify.sendEmail',
    defaultMessage: 'Email site administrators when a new user gets access to the site',
  },
  formSubmit: {
    id: 'user-management.site-settings.site-access.form.submit',
    defaultMessage: 'Save changes',
  },
  successFlagTitle: {
    id: 'user-management.site-settings.site-access.successFlagTitle',
    defaultMessage: 'You updated the site access settings.',
  },
});

const DomainsField = styled.div`
  margin-top: ${akGridSize()}px;
  padding-left: ${akGridSize() * 3}px;
`;

const ListItem = styled.li`
  padding: ${akGridSize()}px;
`;

interface SiteParams {
  cloudId: string;
}
interface OwnProps {
  update(opts: MutationOpts & { variables: UpdateSiteAccessMutationVariables }): Promise<ApolloQueryResult<UpdateSiteAccessMutation>>;
}
type SiteAccessPageProps = ChildProps<OwnProps & FlagProps & InjectedIntlProps & DispatchProp<any> & RouteComponentProps<SiteParams>, SiteAccessQuery> & AnalyticsClientProps;

interface SiteAccessPageState {
  signupSetting: SignUpOptions;
  openInvite: boolean;
  notifyAdmin: boolean;
  domains: string[];
  isSaving: boolean;
}
export class SiteAccessPageImpl extends React.Component<SiteAccessPageProps, SiteAccessPageState> {
  public state: Readonly<SiteAccessPageState> = {
    signupSetting: SignUpOptions.NOBODY,
    openInvite: false,
    notifyAdmin: false,
    domains: [],
    isSaving: false,
  };

  constructor(props: SiteAccessPageProps) {
    super(props);

    if (!props.data || !props.data.currentSite || !props.data.currentSite.siteAccess) {
      return;
    }
    this.state = {
      ...SiteAccessPageImpl.mapDataToState(props.data),
      isSaving: this.state.isSaving,
    };
  }

  public componentWillReceiveProps(nextProps: SiteAccessPageProps) {
    if (!this.props.data || !nextProps.data) {
      return;
    }

    const prevSite = this.props.data.currentSite;
    const nextSite = nextProps.data.currentSite;

    if (nextSite && nextSite.siteAccess && (!prevSite || !isEqual(prevSite.siteAccess, nextSite.siteAccess) || !isEqual(prevSite.inviteUrls, nextSite.inviteUrls))) {
      this.setState(SiteAccessPageImpl.mapDataToState(nextProps.data));
    }
  }

  public onSettingChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const signupSetting = e.currentTarget.value as SignUpOptions;
    const notifyAdmin = signupSetting === SignUpOptions.NOBODY ? false : signupSetting === SignUpOptions.DOMAIN_RESTRICTED ? true : this.state.notifyAdmin;
    this.setState({
      signupSetting,
      notifyAdmin,
    });
  };

  public onOpenInviteChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const openInvite = e.currentTarget.checked;
    this.setState({
      openInvite,
    });
  };

  public onNewUserSendEmailChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const notifyAdmin = e.currentTarget.checked;
    this.setState({
      notifyAdmin,
    });
  };

  public onDomainAdd = (domain: string[]) => {
    const newDomains = domain
      .map(d => d.toLowerCase())
      .filter(d => !this.state.domains.map(sd => sd.toLowerCase()).includes(d));
    if (!newDomains.length) {
      return;
    }
    this.setState({
      domains: [...this.state.domains, ...newDomains],
    });
  }

  public onDomainRemove = (domain: string) => {
    this.setState({
      domains: this.state.domains.map(d => d.toLowerCase()).filter(d => d !== domain.toLowerCase()),
    });
  }

  public onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    const { formatMessage } = this.props.intl;

    e.preventDefault();

    this.setState({ isSaving: true });

    return this.props.update({
      variables: {
        id: this.props.match.params.cloudId,
        input: this.mapStateToInput(),
      },
      optimisticResponse: {
        updateSiteAccess: true,
      },
      refetchQueries: [{ query: getSiteAccess }],
    }).then(() => {
      this.setState({ isSaving: false });
      this.props.showFlag({
        appearance: 'normal',
        autoDismiss: true,
        icon: createSuccessIcon(),
        id: 'site-access-success',
        title: formatMessage(messages.successFlagTitle),
      });
      this.onSiteAccessFormSubmit();
    }).catch(async () => {
      this.setState({ isSaving: false });

      return this.props.data && this.props.data.refetch();
    });
  }

  public render() {
    const {
      intl: { formatMessage },
      data: {
        loading = false,
        error = null,
      } = {},
    } = this.props;

    if (loading || error) {
      return (
        <PageLayout
          title={formatMessage(messages.title)}
          description={formatMessage(messages.description)}
        >
          {error ? (
            <span>{formatMessage(messages.error)}</span>
          ) : (
            <AkSpinner />
          )}
        </PageLayout>
      );
    }

    const sidePanel = (
      <PageSidebar>
        <h4>{formatMessage(messages.sidebarTitle)}</h4>
        <ul>
          <ListItem>{this.sideBarProductAccessContent()}</ListItem>
          <ListItem>{formatMessage(messages.sidebarPoint2)}</ListItem>
        </ul>
      </PageSidebar>
    );

    const { cloudId } = this.props.match.params;

    return (
      <ScreenEventSender event={{ data: selfSignUpViewEventData(), cloudId }}>
        <PageLayout
          title={formatMessage(messages.title)}
          description={formatMessage(messages.description)}
          side={sidePanel}
        >
          <form onSubmit={this.onSubmit}>
            <Fieldset>
              <FieldsetLabel>{formatMessage(messages.invitationSettingsHeading)}</FieldsetLabel>
              <Field>
                <AkCheckboxStateless
                  label={formatMessage(messages.invitationSettingsOpenInvite)}
                  isChecked={this.state.openInvite}
                  isDisabled={this.state.isSaving}
                  onChange={this.onOpenInviteChange}
                />
                <FieldDescription>{formatMessage(messages.invitationSettingsOpenInviteDesc)}</FieldDescription>
              </Field>
            </Fieldset>

            {this.props.data && this.props.data.currentSite && this.props.data.currentSite.flags && this.props.data.currentSite.flags.inviteUrls &&
              <InviteUrls
                cloudId={this.props.match.params.cloudId}
                defaultApps={this.props.data.currentSite.defaultApps}
                inviteUrls={this.props.data.currentSite.inviteUrls}
                refetchInviteUrls={this.refetchInviteUrls}
              />
            }

            <Fieldset>
              <FieldsetLabel>{formatMessage(messages.formHeading)}</FieldsetLabel>
              <Field>
                <AkRadio
                  value={SignUpOptions.NOBODY}
                  isSelected={this.state.signupSetting === SignUpOptions.NOBODY}
                  onChange={this.onSettingChange}
                  isDisabled={this.state.isSaving}
                >
                  {formatMessage(messages.formNobodyCanSignUp)}
                </AkRadio>
              </Field>
              <Field>
                <AkRadio
                  value={SignUpOptions.DOMAIN_RESTRICTED}
                  isSelected={this.state.signupSetting === SignUpOptions.DOMAIN_RESTRICTED}
                  onChange={this.onSettingChange}
                  isDisabled={this.state.isSaving}
                >
                  {formatMessage(messages.formDomainRestrictedSignUp)}
                </AkRadio>
                <DomainsField>
                  <MultiTagger
                    shouldFitContainer={true}
                    isLabelHidden={true}
                    placeholder={formatMessage(messages.formDomainRestrictedDomainsPlaceholder)}
                    description={formatMessage(messages.formDomainRestrictedDomainsDescription)}
                    removeText={formatMessage(messages.formDomainRestrictedDomainsRemove)}
                    disabled={this.state.signupSetting !== SignUpOptions.DOMAIN_RESTRICTED || this.state.isSaving}
                    onAdd={this.onDomainAdd}
                    onRemove={this.onDomainRemove}
                    values={this.state.domains}
                  />
                </DomainsField>
              </Field>
              <Field>
                <AkRadio
                  value={SignUpOptions.ANYONE}
                  isSelected={this.state.signupSetting === SignUpOptions.ANYONE}
                  onChange={this.onSettingChange}
                  isDisabled={this.state.isSaving}
                >
                  {formatMessage(messages.formAnyoneCanSignUp)}
                </AkRadio>
              </Field>
            </Fieldset>
            <Fieldset>
              <FieldsetLabel>{formatMessage(messages.formNotifyOnNewUserHeading)}</FieldsetLabel>
              <AkCheckboxStateless
                label={formatMessage(messages.formNewUserSendEmail)}
                isChecked={this.state.notifyAdmin}
                isDisabled={this.state.signupSetting !== SignUpOptions.ANYONE || this.state.isSaving}
                onChange={this.onNewUserSendEmailChange}
              />
            </Fieldset>
            <AkButton
              appearance="primary"
              type="submit"
              isLoading={this.state.isSaving}
              onClick={this.onSaveSelfSignupButtonClick}
            >
              {formatMessage(messages.formSubmit)}
            </AkButton>
          </form>
        </PageLayout>
      </ScreenEventSender>
    );
  }

  public sideBarProductAccessContent() {
    if (!this.props.data || !this.props.data.currentSite || !this.props.data.currentSite.defaultApps) {
      return null;
    }
    const {
      intl: { formatMessage },
      data: { currentSite: { defaultApps } },
    } = this.props;

    const appsUrl = util.isAdminHub() ? `/s/${this.props.match.params.cloudId}/apps` : '/admin/apps';
    const commonValues = {
      appAccessLink: (<Link to={appsUrl}>{formatMessage(messages.sidebarPoint1Link)}</Link>),
    };

    if (defaultApps.length === 0) {
      return (
        <FormattedMessage
          {...messages.sidebarPoint1WithoutAccess}
          values={{
            ...commonValues,
            highlight: (<strong>{formatMessage(messages.sidebarPoint1WithoutAccessHighlight)}</strong>),
          }}
        />
      );
    } else if (defaultApps.length === 1) {
      return (
        <FormattedMessage
          {...messages.sidebarPoint1WithAccessOne}
          values={{
            ...commonValues,
            product: defaultApps[0].productName,
          }}
        />
      );
    } else if (defaultApps.length === 2) {
      return (
        <FormattedMessage
          {...messages.sidebarPoint1WithAccessTwo}
          values={{
            ...commonValues,
            product1: defaultApps[0].productName,
            product2: defaultApps[1].productName,
          }}
        />
      );
    } else if (defaultApps.length === 3) {
      return (
        <FormattedMessage
          {...messages.sidebarPoint1WithAccessThree}
          values={{
            ...commonValues,
            product1: defaultApps[0].productName,
            product2: defaultApps[1].productName,
            product3: defaultApps[2].productName,
          }}
        />
      );
    } else {
      return (
        <FormattedMessage
          {...messages.sidebarPoint1WithAccessMany}
          values={{
            ...commonValues,
            product1: defaultApps[0].productName,
            product2: defaultApps[1].productName,
            product3: defaultApps[2].productName,
            numAdditionalProducts: defaultApps.length - 3,
          }}
        />
      );
    }
  }

  public mapStateToInput(): UpdateSiteAccessInput {

    const isDomainRestrictedSignup = this.state.domains.length > 0 && this.state.signupSetting === SignUpOptions.DOMAIN_RESTRICTED;
    const isPublicSignup = this.state.signupSetting === SignUpOptions.ANYONE;

    return {
      domains: this.state.signupSetting === SignUpOptions.DOMAIN_RESTRICTED ? this.state.domains : [],
      notifyAdmin: this.state.notifyAdmin,
      openInvite: this.state.openInvite,
      signupEnabled: isDomainRestrictedSignup || isPublicSignup,
    };
  }

  public static mapDataToState({
    currentSite: {
      siteAccess: {
        domains = [],
        notifyAdmin = false,
        openInvite = false,
        signupEnabled = false,
      } = {},
    } = {},
  }: Partial<SiteAccessQuery>,
  ): Pick<SiteAccessPageState, 'domains' | 'notifyAdmin' | 'openInvite' | 'signupSetting'> {
    return {
      domains,
      notifyAdmin,
      openInvite,
      signupSetting: getSignupSettings(signupEnabled, domains),
    };
  }

  private onSiteAccessFormSubmit = () => {
    this.props.analyticsClient.sendTrackEvent({
      cloudId: this.props.match.params.cloudId,
      data: selfSignUpTrackEventData({
        isOpenInvite: this.state.openInvite,
        restrictions: this.state.signupSetting.toString(),
        notify: this.state.notifyAdmin,
      }),
    });
  }

  private onSaveSelfSignupButtonClick = () => {
    this.props.analyticsClient.sendUIEvent({
      cloudId: this.props.match.params.cloudId,
      data: selfSignUpSaveClickEventData(),
    });
  }

  private refetchInviteUrls = async (): Promise<InviteUrl[]> => {
    const result = await this.props.data!.refetch({
      variables: {
        v: Math.random(), // Because otherwise GraphQL won't work.
      },
    });

    return result.data.currentSite.inviteUrls;
  }
}

export const SiteAccessPage = compose(
  graphql<SiteAccessPageProps, SiteAccessQuery>(getSiteAccess),
  graphql<UpdateSiteAccessMutation, any>(updateSiteAccess, { name: 'update' }),
  connect(),
  withFlag,
  injectIntl,
  withAnalyticsClient,
  withRouter,
)(SiteAccessPageImpl);
