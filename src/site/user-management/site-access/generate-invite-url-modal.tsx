import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import AkButton, { ButtonGroup as AkButtonGroup } from '@atlaskit/button';

import { createErrorIcon } from 'common/error';
import { ModalDialog } from 'common/modal';

const messages = defineMessages({
  regenerateInviteModalTitle: {
    id: 'user-management.site-settings.site-access.regenerateInviteModalTitle',
    defaultMessage: 'Regenerate the invite link?',
    description: 'Title of the modal dialog, asking to confirm the invite link regeneration',
  },
  regenerateInviteModalDescription: {
    id: 'user-management.site-settings.site-access.regenerateInviteModalDescription',
    defaultMessage: 'When you regenerate an invite link, the previous link stops working and the new link starts working immediately.',
    description: 'Text in modal dialog, explaining what whould happen when user click on confirm button and regenerate the link',
  },
  regenerateInviteModalRegenerateButton: {
    id: 'user-management.site-settings.site-access.regenerateInviteModalRegenerateButton',
    defaultMessage: 'Regenerate link',
    description: 'Text on the button to regenerate the invite link',
  },
  regenerateInviteModalCancelButton: {
    id: 'user-management.site-settings.site-access.regenerateInviteModalCancelButton',
    defaultMessage: 'Cancel',
    description: 'Text on the cancel button on the modal dialog',
  },
});

const StyledIcon = styled.span`
  position: relative;
  top: 2px;
  margin-right: 5px;
`;

interface GenerateInvitationModalImplOwnProps {
  showModal: boolean;
  waiting: boolean;
  onGenerate(): void;
  onCancel(): void;
}

type GenerateInvitationModalImplProps = GenerateInvitationModalImplOwnProps & InjectedIntlProps;

export class GenerateInvitationModalImpl extends React.Component<GenerateInvitationModalImplProps> {
  public render() {
    const { formatMessage } = this.props.intl;

    return (
      <ModalDialog
        header={<span>
          <StyledIcon>{createErrorIcon()}</StyledIcon>
          {formatMessage(messages.regenerateInviteModalTitle)}
        </span>}
        isOpen={this.props.showModal}
        footer={(
          <AkButtonGroup>
            <AkButton appearance="warning" onClick={this.props.onGenerate} isDisabled={this.props.waiting} isLoading={this.props.waiting}>
              {formatMessage(messages.regenerateInviteModalRegenerateButton)}
            </AkButton>
            <AkButton onClick={this.props.onCancel} isDisabled={this.props.waiting} appearance="subtle">
              {formatMessage(messages.regenerateInviteModalCancelButton)}
            </AkButton>
          </AkButtonGroup>
        )}
        width="small"
        onClose={this.props.onCancel}
      >
        {formatMessage(messages.regenerateInviteModalDescription)}
      </ModalDialog>
    );
  }
}

export const GenerateInvitationModal = injectIntl(
  GenerateInvitationModalImpl,
);
