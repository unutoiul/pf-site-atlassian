import styled from 'styled-components';

import { colors as akColors, gridSize as akGridSize } from '@atlaskit/theme';

export const Fieldset = styled.fieldset`
  margin: ${akGridSize() * 3}px 0;

  &:first-child {
    margin-top: 0;
  }
`;

export const FieldsetLabel = styled.legend`
  color: ${akColors.N200};
  font-size: 12px;
  font-weight: 600;
  line-height: 1.3333333333333333;
  margin-bottom: ${akGridSize()}px;
  padding: 0;
  text-transform: uppercase;
`;

export const FieldDescription = styled.div`
  color: ${akColors.N300};
  font-size: 11px;
  margin: ${akGridSize() / 2}px 0 ${akGridSize()}px ${akGridSize() * 4}px;
`;

export const FieldDescriptionWithoutIndent = styled.div`
  color: ${akColors.N300};
  font-size: 11px;
  margin: ${akGridSize() / 2}px 0 ${akGridSize()}px;
`;

export const Field = styled.div`
  margin-top: ${akGridSize()}px;

  &:first-child {
    margin-top: 0;
  }
`;
