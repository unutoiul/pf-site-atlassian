import * as React from 'react';
import { graphql, MutationFunc } from 'react-apollo';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import { Checkbox as AkCheckboxStateless } from '@atlaskit/checkbox';
import { gridSize as akGridSize } from '@atlaskit/theme';

import { AnalyticsClientProps, inviteUrlCheckbox, inviteUrlLinkRegenerated, inviteUrlLinkToggled, withAnalyticsClient } from 'common/analytics';
import { createSuccessIcon } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';

import { DeleteInviteUrlMutation, DeleteInviteUrlMutationVariables,
  GenerateInviteUrlMutation, GenerateInviteUrlMutationVariables } from '../../../schema/schema-types';
import deleteInviteUrl from './delete-invite-url.mutation.graphql';
import generateInviteUrl from './generate-invite-url.mutation.graphql';
import { InviteUrlField } from './invite-url-field';
import { Field, FieldDescriptionWithoutIndent, Fieldset, FieldsetLabel } from './shared-styles';
import getSiteAccess from './site-access.query.graphql';
import { daysRemaining } from './util';

export const messages = defineMessages({
  inviteUrlsHeading: {
    id: 'user-management.site-settings.site-access.inviteUrlsHeading',
    defaultMessage: 'Invite URLs',
    description: 'Title of the UI section that contains invite links',
  },
  inviteUrlsDescription: {
    id: 'user-management.site-settings.site-access.inviteUrlsDescription',
    defaultMessage: 'An invite link gives any Atlassian account user instant access to your site. Users can copy the link from in-product and share it the way they like.',
    description: 'Description of the UI section that contains invite links',
  },
  inviteUrlCheckbox: {
    id: 'user-management.site-settings.site-access.inviteUrlCheckbox',
    defaultMessage: 'Allow anybody to share a link to {product}',
    description: 'Description for the checkbox to enable the invite link for the given product',
  },
  inviteUrlSuccessFlagTitle: {
    id: 'user-management.site-settings.site-access.inviteUrlSuccessFlagTitle',
    defaultMessage: 'Success!',
    description: 'Title for the success message after invite link generated',
  },
  inviteUrlGeneratedFlagMessage: {
    id: 'user-management.site-settings.site-access.inviteUrlGeneratedFlagMessage',
    defaultMessage: 'You’ve successfully regenerated your Invite URL for {product}.',
    description: 'Text for the success message after the invite link generated',
  },
  inviteUrlDeletedFlagMessage: {
    id: 'user-management.site-settings.site-access.inviteUrlDeletedFlagMessage',
    defaultMessage: 'You’ve successfully disabled your Invite URL for {product}.',
    description: 'Text for the success message after the invite link deleted',
  },
});

const InviteUrlFieldStyled = styled.div`
  margin-left: ${akGridSize() * 4}px;
`;

const Fields = styled.div`
  > * {
    margin-top: ${akGridSize()}px;
  }
`;

export interface InviteUrl {
  url: string;
  productKey: string;
  productName: string;
  expiration: string;
}

export interface ProductInfo {
  canonicalProductKey: string;
  productName: string;
}

export interface InviteUrlsImplOwnProps {
  cloudId: string;
  defaultApps: ProductInfo[];
  inviteUrls: InviteUrl[];
  refetchInviteUrls(): Promise<InviteUrl[]>;
}
export type InviteUrlsImplProps = InviteUrlsImplOwnProps & FlagProps & InjectedIntlProps & AnalyticsClientProps;

interface InviteUrlsState {
  waiting: boolean;
}

// Products with these canonical keys will be skipped.
const skipCanonicalKeys = ['chat'];

export class InviteUrlsImpl extends React.Component<InviteUrlsImplProps & GenerateMutationProps & DeleteMutationProps, InviteUrlsState> {
  public state = { waiting: false };

  public generateInviteUrl = async (
    productKey: string,
    productName: string,
    currentExpiration: string | null,
    track: (productKey: string, currentExpiration: string | null, newExpiration: string) => void,
  ) => {
    const { cloudId, intl: { formatMessage } } = this.props;

    this.setState({ waiting: true });

    try {
      // Updating
      await this.props.generateInviteUrl({
        variables: {
          id: cloudId,
          input: { productKey },
        },
      });

      // Re-fetching invite urls
      const newInviteUrls = await this.props.refetchInviteUrls();

      // Showing flag and updating state
      this.setState({ waiting: false });
      this.props.showFlag({
        autoDismiss: true,
        icon: createSuccessIcon(),
        id: 'generate-invite-url-success',
        title: formatMessage(messages.inviteUrlSuccessFlagTitle),
        description: formatMessage(messages.inviteUrlGeneratedFlagMessage, { product: productName }),
      });

      // Sending analytics
      const newInviteUrl = newInviteUrls.find((url) => url.productKey === productKey);
      if (!newInviteUrl) {
        throw new Error('updated invite url not found in response from the server!');
      }

      track(productKey, currentExpiration, newInviteUrl.expiration);
    } catch (e) {
      this.setState({ waiting: false });
    }
  }

  public generateInviteUrlFromCheckbox = async (productKey: string, productName: string, currentExpiration: string | null) => {
    return this.generateInviteUrl(productKey, productName, currentExpiration, this.trackInviteUrlLinkToggled);
  }

  public generateInviteUrlFromModal = async (productKey: string, productName: string, currentExpiration: string) => {
    return this.generateInviteUrl(productKey, productName, currentExpiration, this.trackInviteUrlLinkRegenerated);
  }

  public deleteInviteUrl = async (productKey: string, productName: string, currentExpiration: string) => {
    const { cloudId, intl: { formatMessage } } = this.props;

    this.setState({ waiting: true });

    return this.props.deleteInviteUrl({
      variables: {
        id: cloudId,
        input: { productKey },
      },
      refetchQueries: [{ query: getSiteAccess }],
    }).then(() => {
      this.setState({ waiting: false });
      this.props.showFlag({
        autoDismiss: true,
        icon: createSuccessIcon(),
        id: 'delete-invite-url-success',
        title: formatMessage(messages.inviteUrlSuccessFlagTitle),
        description: formatMessage(messages.inviteUrlDeletedFlagMessage, { product: productName }),
      });

      this.trackUrlDeletedEvent(productKey, currentExpiration);
    }).catch(async () => {
      this.setState({ waiting: false });
    });
  }

  public render() {
    const { intl: { formatMessage }, inviteUrls, defaultApps } = this.props;

    // Skipping stride, because Invite URL doesn't work with it
    const filteredApps = defaultApps.filter(({ canonicalProductKey }) => !skipCanonicalKeys.includes(canonicalProductKey));

    return (
      <Fieldset>
        <FieldsetLabel>{formatMessage(messages.inviteUrlsHeading)}</FieldsetLabel>
        <FieldDescriptionWithoutIndent>{formatMessage(messages.inviteUrlsDescription)}</FieldDescriptionWithoutIndent>

        <Fields>
          {filteredApps.map(({ canonicalProductKey, productName }) =>
            canonicalProductKey ? this.renderInviteUrl(canonicalProductKey, productName, inviteUrls.find((url) => url.productKey === canonicalProductKey)) : null,
          )}
        </Fields>
      </Fieldset>
    );
  }

  private renderInviteUrl(canonicalProductKey: string, productName: string, url: InviteUrl | undefined) {
    const { intl: { formatMessage }, cloudId } = this.props;

    if (url) {
      return (
        <Field key={canonicalProductKey}>
          <AkCheckboxStateless
            label={formatMessage(messages.inviteUrlCheckbox, { product: productName })}
            isChecked={true}
            isDisabled={this.state.waiting}
            onChange={this.uncheck(canonicalProductKey, productName, url.expiration)}
          />
          <InviteUrlFieldStyled>
            <InviteUrlField
              cloudId={cloudId}
              canonicalProductKey={canonicalProductKey}
              productName={productName}
              url={url.url}
              expiration={url.expiration}
              waiting={this.state.waiting}
              generateInviteUrl={this.generateInviteUrlFromModal}
            />
          </InviteUrlFieldStyled>
        </Field>
      );
    } else {
      return (
        <Field key={canonicalProductKey}>
          <AkCheckboxStateless
            label={formatMessage(messages.inviteUrlCheckbox, { product: productName })}
            isChecked={false}
            isDisabled={this.state.waiting}
            onChange={this.check(canonicalProductKey, productName)}
          />
        </Field>
      );
    }
  }

  private uncheck(canonicalProductKey: string, productName: string, expiration: string) {
    return async (e: React.ChangeEvent<HTMLInputElement>) => {
      e.preventDefault();

      this.switchCheckboxEvent(canonicalProductKey, expiration, true);

      return this.deleteInviteUrl(canonicalProductKey, productName, expiration);
    };
  }

  private check(canonicalProductKey: string, productName: string) {
    return async (e: React.ChangeEvent<HTMLInputElement>) => {
      e.preventDefault();

      this.switchCheckboxEvent(canonicalProductKey, undefined, false);

      return this.generateInviteUrlFromCheckbox(canonicalProductKey, productName, null);
    };
  }

  private switchCheckboxEvent(canonicalProductKey: string, expiration: string | undefined, isCurrentlyChecked: boolean) {
    const currentState = isCurrentlyChecked ? 'enabled' : 'disabled';
    const newState = isCurrentlyChecked ? 'disabled' : 'enabled';
    this.props.analyticsClient.sendUIEvent(inviteUrlCheckbox({
      cloudId: this.props.cloudId,
      productKey: canonicalProductKey,
      currentState,
      newState,
      currentExpirationDate: expiration ? expiration : undefined,
      currentDaysRemaining: expiration ? daysRemaining(expiration) : undefined,
    }));
  }

  private trackInviteUrlLinkToggled = (productKey: string, currentExpiration: string | null, newExpiration: string) => {
    if (currentExpiration) {
      this.props.analyticsClient.sendTrackEvent(inviteUrlLinkToggled(this.props.cloudId, {
        productKey,
        currentState: 'enabled',
        newState: 'enabled',
        currentExpirationDate: currentExpiration,
        currentDaysRemaining: daysRemaining(currentExpiration),
        newExpirationDate: newExpiration,
        newDaysRemaining: daysRemaining(newExpiration),
      }));
    } else {
      this.props.analyticsClient.sendTrackEvent(inviteUrlLinkToggled(this.props.cloudId, {
        productKey,
        currentState: 'disabled',
        newState: 'enabled',
        newExpirationDate: newExpiration,
        newDaysRemaining: daysRemaining(newExpiration),
      }));
    }
  }

  private trackInviteUrlLinkRegenerated = (productKey: string, currentExpiration: string | null, newExpiration: string) => {
    if (currentExpiration) {
      this.props.analyticsClient.sendTrackEvent(inviteUrlLinkRegenerated(this.props.cloudId, {
        productKey,
        currentExpirationDate: currentExpiration,
        currentDaysRemaining: daysRemaining(currentExpiration),
        newExpirationDate: newExpiration,
        newDaysRemaining: daysRemaining(newExpiration),
      }));
    } else {
      this.props.analyticsClient.sendTrackEvent(inviteUrlLinkRegenerated(this.props.cloudId, {
        productKey,
        newExpirationDate: newExpiration,
        newDaysRemaining: daysRemaining(newExpiration),
      }));
    }
  }

  private trackUrlDeletedEvent(productKey: string, currentExpiration: string) {
    this.props.analyticsClient.sendTrackEvent(inviteUrlLinkToggled(this.props.cloudId, {
      productKey,
      currentState: 'enabled',
      newState: 'disabled',
      currentExpirationDate: currentExpiration,
      currentDaysRemaining: daysRemaining(currentExpiration),
    }));
  }
}

export interface GenerateMutationProps {
  generateInviteUrl: MutationFunc<GenerateInviteUrlMutation, GenerateInviteUrlMutationVariables>;
}
const withGenerateMutation = graphql<InviteUrlsImplOwnProps, GenerateInviteUrlMutation, GenerateMutationProps>(generateInviteUrl, { name: 'generateInviteUrl' });
export interface DeleteMutationProps {
  deleteInviteUrl: MutationFunc<DeleteInviteUrlMutation, DeleteInviteUrlMutationVariables>;
}
const withDeleteMutation = graphql<InviteUrlsImplOwnProps, DeleteInviteUrlMutation, DeleteMutationProps>(deleteInviteUrl, { name: 'deleteInviteUrl' });

export const InviteUrls = withGenerateMutation(
  withDeleteMutation(
    withFlag(
      injectIntl(
        withAnalyticsClient(
          InviteUrlsImpl,
        ),
      ),
    ),
  ),
);
