import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { FormattedMessage, FormattedRelative } from 'react-intl';
import { sandbox as sinonSandbox } from 'sinon';

import AkButton from '@atlaskit/button';

import { TextCopy } from 'common/text-copy';

import { createMockAnalyticsClient, createMockIntlContext, createMockIntlProp, MockAnalyticsClient } from '../../../utilities/testing';
import { GenerateInvitationModal } from './generate-invite-url-modal';
import { InviteUrlFieldImpl, InviteUrlFieldImplOwnProps } from './invite-url-field';

describe('InviteUrlField', () => {
  const expiration = new Date(Date.now() + 10 * 24 * 3600 * 1000).toISOString();
  const sandbox = sinonSandbox.create();
  let mockAnalyticsClient: MockAnalyticsClient;

  beforeEach(() => {
    mockAnalyticsClient = createMockAnalyticsClient();
  });

  afterEach(() => {
    sandbox.restore();
  });

  function build(props: Partial<InviteUrlFieldImplOwnProps> = {}) {
    const defaults: InviteUrlFieldImplOwnProps = {
      cloudId: 'cloud-id-1',
      url: 'http://atlassian.com/invite-url-id-1',
      canonicalProductKey: 'jira',
      productName: 'Jira',
      expiration,
      waiting: false,
      generateInviteUrl: sandbox.spy(),
    };

    return shallow(
      <InviteUrlFieldImpl intl={createMockIntlProp()} {...defaults} {...props} analyticsClient={mockAnalyticsClient}/>,
      createMockIntlContext(),
    );
  }

  it('should render', () => {
    const wrapper = build();

    expect(wrapper.find(TextCopy).prop('label')).to.not.equal(undefined);
    expect(wrapper.find(AkButton).text()).to.not.equal(undefined);
    expect(
      (shallow(wrapper.find(FormattedMessage).prop('values')!.time as any).find(FormattedRelative).prop('value') as any),
    ).to.equal(expiration);
  });

  it('should display confirmation modal', () => {
    const wrapper = build();

    expect(wrapper.find(GenerateInvitationModal).prop('showModal')).to.equal(false);
    expect(wrapper.state('showModal')).to.equal(false);

    (wrapper.find(AkButton).prop('onClick') as any)({ preventDefault: sandbox.spy() });

    expect(wrapper.find(GenerateInvitationModal).prop('showModal')).to.equal(true);
    expect(wrapper.state('showModal')).to.equal(true);
    expect(mockAnalyticsClient.sendScreenEvent.called).to.equal(true);
  });

  it('should close confirmation modal', () => {
    const wrapper = build();

    (wrapper.find(AkButton).prop('onClick') as any)({ preventDefault: sandbox.spy() });
    expect(wrapper.find(GenerateInvitationModal).prop('showModal')).to.equal(true);

    ((wrapper as any).find(GenerateInvitationModal).prop('onCancel'))();
    expect(wrapper.find(GenerateInvitationModal).prop('showModal')).to.equal(false);
    expect(wrapper.state('showModal')).to.equal(false);
    expect(mockAnalyticsClient.sendUIEvent.called).to.equal(true);
  });

  it('should generate invite url', () => {
    const generateInviteUrl = sandbox.spy();
    const wrapper = build({ generateInviteUrl });

    (wrapper.find(AkButton).prop('onClick') as any)({ preventDefault: sandbox.spy() });
    (wrapper as any).find(GenerateInvitationModal).prop('onGenerate')();

    expect(generateInviteUrl.withArgs('jira').calledOnce).to.equal(true);
    expect(mockAnalyticsClient.sendUIEvent.called).to.equal(true);
  });
});
