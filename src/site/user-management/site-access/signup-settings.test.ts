import { expect } from 'chai';

import { getSignupSettings, SignUpOptions } from './signup-settings';

describe('Site access - signup settings', () => {
  describe('getSignupSettings', () => {
    it('should return DOMAIN_RESTRICTED if signup enabled and domains exist', () => {
      const result = getSignupSettings(true, ['atlassian.com']);
      expect(result).to.equal(SignUpOptions.DOMAIN_RESTRICTED);
    });

    it('should return ANYONE if signup is enabled only', () => {
      const result = getSignupSettings(true, []);
      expect(result).to.equal(SignUpOptions.ANYONE);
    });

    it(`should return NOBODY if signup isn't enabled`, () => {
      const result = getSignupSettings(false, []);
      expect(result).to.equal(SignUpOptions.NOBODY);
    });
  });
});
