export const enum SignUpOptions {
  NOBODY = 'nobody',
  DOMAIN_RESTRICTED = 'domainRestricted',
  ANYONE = 'anyone',
}

export function getSignupSettings(signupEnabled: boolean, domains: string[]) {
  if (signupEnabled && domains.length > 0) {
    return SignUpOptions.DOMAIN_RESTRICTED;
  } else if (signupEnabled) {
    return SignUpOptions.ANYONE;
  }

  return SignUpOptions.NOBODY;
}
