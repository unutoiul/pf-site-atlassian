import * as React from 'react';
import { defineMessages, FormattedMessage, FormattedRelative, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import AkButton from '@atlaskit/button';
import { colors as akColors, gridSize as akGridSize } from '@atlaskit/theme';

import {
  AnalyticsClientProps,
  cancelRegenerateInviteUrlLink,
  confirmRegenerateInviteUrlLink,
  copyInviteUrlButton,
  regenerateInviteUrlLink,
  regenerateInviteUrlModal,
  withAnalyticsClient,
} from 'common/analytics';
import { TextCopy } from 'common/text-copy';

import { GenerateInvitationModal } from './generate-invite-url-modal';
import { daysRemaining } from './util';

export const messages = defineMessages({
  inviteUrlLinkInfo: {
    id: 'user-management.site-settings.site-access.inviteUrlLinkInfo',
    defaultMessage: 'This link will be regenerated {time}.',
    description: 'Informational message about the invite link, says how soon it will be regenerated',
  },
  inviteUrlRegenerate: {
    id: 'user-management.site-settings.site-access.inviteUrlRegenerate',
    defaultMessage: 'Regenerate link now',
    description: 'Text on the button to regenerate the invite link',
  },
  inviteUrlCopyToClipboardLabel: {
    id: 'user-management.site-settings.site-access.inviteUrlCopyToClipboardLabel',
    defaultMessage: 'Invite link',
    description: 'Name of the invite link element that would be used in success message',
  },
  inviteCopyButtonLabel: {
    id: 'user-management.site-settings.site-access.inviteCopyButtonLabel',
    defaultMessage: 'Copy to clipboard',
    description: 'A button that copies text from a text field',
  },
});

const InviteUrlFieldStyles = styled.div`
  label {
    display: none;
  }
  > div > div {
    margin-top: 0;
  }
  > div > div:nth-child(2) {
    padding-left: 8px !important;
  }
`;

export const Description = styled.div`
  margin-top: ${akGridSize() / 2}px;
  color: ${akColors.N300};
  font-size: 11px;
`;

const ExpireIn = styled.span`
  font-weight: bold;
`;

const Regenerate = styled.span`
  margin-left: -4px;
  position: relative;
  top: -1px;
`;

export interface InviteUrlFieldImplOwnProps {
  url: string;
  canonicalProductKey: string;
  productName: string;
  expiration: string;
  waiting: boolean;
  cloudId: string;
  generateInviteUrl(canonicalProductKey: string, productName: string, currentExpiration: string | null): Promise<void>;
}

interface State {
  showModal: boolean;
  waitingModal: boolean;
}

export type InviteUrlFieldImplProps = InviteUrlFieldImplOwnProps & InjectedIntlProps & AnalyticsClientProps;

export class InviteUrlFieldImpl extends React.Component<InviteUrlFieldImplProps, State> {
  public state = { showModal: false, waitingModal: false };

  public render() {
    const { intl: { formatMessage }, url, waiting, expiration } = this.props;

    return (
      <div>
        <InviteUrlFieldStyles>
          <TextCopy
            label={formatMessage(messages.inviteUrlCopyToClipboardLabel)}
            value={url}
            isDisabled={waiting}
            alternativeButtonLabel={messages.inviteCopyButtonLabel}
            analyticsUIEvent={this.copyEvent()}
          />
        </InviteUrlFieldStyles>
        <Description>
          <FormattedMessage
            {...messages.inviteUrlLinkInfo}
            values={{
              time: (
                <ExpireIn>
                  <FormattedRelative value={expiration}/>
                </ExpireIn>
              ),
            }}
          />

          <Regenerate style={{ visibility: waiting ? 'hidden' : 'visible' }}>
            <AkButton appearance="link" spacing="compact" onClick={this.confirm}>{formatMessage(messages.inviteUrlRegenerate)}</AkButton>
          </Regenerate>
        </Description>

        <GenerateInvitationModal
          showModal={this.state.showModal}
          onGenerate={this.generate}
          onCancel={this.cancel}
          waiting={this.state.waitingModal}
        />
      </div>
    );
  }

  public cancel = () => {
    const { cloudId, canonicalProductKey, expiration } = this.props;
    this.props.analyticsClient.sendUIEvent(cancelRegenerateInviteUrlLink({
      cloudId,
      productKey: canonicalProductKey,
      currentExpirationDate: expiration ? expiration : undefined,
      currentDaysRemaining: expiration ? daysRemaining(expiration) : undefined,
    }));
    this.setState({ showModal: false });
  }

  public confirm = () => {
    const { cloudId, canonicalProductKey, expiration } = this.props;
    this.props.analyticsClient.sendUIEvent(regenerateInviteUrlLink({
      cloudId,
      productKey: canonicalProductKey,
      currentExpirationDate: expiration ? expiration : undefined,
      currentDaysRemaining: expiration ? daysRemaining(expiration) : undefined,
    }));
    this.props.analyticsClient.sendScreenEvent(regenerateInviteUrlModal(cloudId, canonicalProductKey, expiration, daysRemaining(expiration)));

    this.setState({ showModal: true });
  }

  public generate = async () => {
    const { cloudId, canonicalProductKey, expiration } = this.props;
    this.props.analyticsClient.sendUIEvent(confirmRegenerateInviteUrlLink({
      cloudId,
      productKey: canonicalProductKey,
      currentExpirationDate: expiration ? expiration : undefined,
      currentDaysRemaining: expiration ? daysRemaining(expiration) : undefined,
    }));

    this.setState({ waitingModal: true });

    try {
      await this.props.generateInviteUrl(this.props.canonicalProductKey, this.props.productName, this.props.expiration);
    } catch (e) {
      // Do nothing, error would be handled by GraphQL layer.
    } finally {
      this.setState({ showModal: false, waitingModal: false });
    }
  }

  private copyEvent() {
    const { cloudId, canonicalProductKey, expiration } = this.props;

    return copyInviteUrlButton({
      cloudId,
      productKey: canonicalProductKey,
      currentExpirationDate: expiration ? expiration : undefined,
      currentDaysRemaining: expiration ? daysRemaining(expiration) : undefined,
    });
  }
}

export const InviteUrlField = injectIntl(
  withAnalyticsClient(
    InviteUrlFieldImpl,
  ),
);
