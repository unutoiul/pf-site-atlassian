const dayInMs = 24 * 60 * 60 * 1000;

export function daysRemaining(expiration: string): number {
  const diffInMs = ((new Date(expiration)).getTime() - (new Date()).getTime());

  return diffInMs < 0 ? 0 : Math.ceil(diffInMs / dayInMs);
}
