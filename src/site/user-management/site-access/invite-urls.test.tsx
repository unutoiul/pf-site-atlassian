import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { sandbox as sinonSandbox, SinonSpy } from 'sinon';

import { Checkbox as AkCheckboxStateless } from '@atlaskit/checkbox';

import { createMockIntlContext, createMockIntlProp } from '../../../utilities/testing';
import { InviteUrlField } from './invite-url-field';
import { DeleteMutationProps, GenerateMutationProps, InviteUrl, InviteUrlsImpl, InviteUrlsImplProps } from './invite-urls';
import { Field, FieldDescriptionWithoutIndent, FieldsetLabel } from './shared-styles';

describe('InviteUrlField', () => {
  const expiration = new Date(Date.now() + 10 * 24 * 3600 * 1000).toISOString();
  const sandbox = sinonSandbox.create();

  const jiraInviteUrl: InviteUrl = {
    url: 'http://atlassian.com/invite-url-1',
    productKey: 'jira',
    productName: 'JIRA',
    expiration,
  };

  const confluenceInviteUrl: InviteUrl = {
    url: 'http://atlassian.com/invite-url-2',
    productKey: 'confluence',
    productName: 'Confluence',
    expiration,
  };

  let mockAnalyticsClient: {
    init: SinonSpy,
    sendUIEvent: SinonSpy,
    sendScreenEvent: SinonSpy,
    sendTrackEvent: SinonSpy,
  };

  beforeEach(() => {
    mockAnalyticsClient = {
      init: sandbox.spy(),
      sendUIEvent: sandbox.spy(),
      sendScreenEvent: sandbox.spy(),
      sendTrackEvent: sandbox.spy(),
    };
  });

  afterEach(() => {
    sandbox.restore();
  });

  function build(props: Partial<InviteUrlsImplProps & GenerateMutationProps & DeleteMutationProps> = {}) {
    const inviteUrls = [
      jiraInviteUrl,
    ];

    const defaults: InviteUrlsImplProps & GenerateMutationProps & DeleteMutationProps = {
      cloudId: 'some-cloud-id',
      defaultApps: [
        {
          canonicalProductKey: 'jira',
          productName: 'JIRA',
        },
        {
          canonicalProductKey: 'confluence',
          productName: 'Confluence',
        },
      ],
      inviteUrls,
      generateInviteUrl: sandbox.stub().resolves(true),
      deleteInviteUrl: sandbox.spy(),
      showFlag: sandbox.spy(),
      hideFlag: sandbox.spy(),
      intl: createMockIntlProp(),
      analyticsClient: mockAnalyticsClient,
      refetchInviteUrls: async () => inviteUrls,
    };

    return shallow(
      <InviteUrlsImpl {...{ ...defaults, ...props }}/>,
      createMockIntlContext(),
    );
  }

  it('should render', () => {
    const wrapper = build();

    // Should have two products
    expect(wrapper.find(FieldsetLabel).text()).to.not.equal(undefined);
    expect(wrapper.find(FieldDescriptionWithoutIndent).text()).to.not.equal(undefined);
    expect(wrapper.find(Field).length).to.equal(2);

    // Jira checkbox should be enabled
    const jira = wrapper.find(Field).at(0).dive();
    const jiraCheckbox = jira.find(AkCheckboxStateless);
    expect(jiraCheckbox.prop('label')).to.contain('JIRA');
    expect(jiraCheckbox.prop('isChecked')).to.equal(true);

    // Should have jira invite url field
    expect(jira.find(InviteUrlField).length).to.equal(1);
    const jiraField = jira.find(InviteUrlField);
    expect(jiraField.prop('canonicalProductKey')).to.equal('jira');
    expect(jiraField.prop('url')).to.equal('http://atlassian.com/invite-url-1');
    expect(jiraField.prop('expiration')).to.equal(expiration);

    // Confluence checkbox should be disabled
    const conf = wrapper.find(Field).at(1).dive();
    const confCheckbox = conf.find(AkCheckboxStateless);
    expect(confCheckbox.prop('label')).to.contain('Confluence');
    expect(confCheckbox.prop('isChecked')).to.equal(false);

    // Should not have confluence invite url field
    expect(conf.find(InviteUrlField).length).to.equal(0);
  });

  it('should not show invite url for disabled products', () => {
    const wrapper = build({
      defaultApps: [
        {
          canonicalProductKey: 'jira',
          productName: 'JIRA',
        },
        {
          canonicalProductKey: 'chat',
          productName: 'Stride',
        },
      ],
    });

    // Should have only one JIRA product
    expect(wrapper.find(Field).length).to.equal(1);

    // Should display only JIRA
    const jira = wrapper.find(Field).at(0).dive();
    const jiraCheckbox = jira.find(AkCheckboxStateless);
    expect(jiraCheckbox.prop('label')).to.contain('JIRA');
  });

  it('should generate event from modal', async () => {
    const wrapper = build({ refetchInviteUrls: async () => [jiraInviteUrl, confluenceInviteUrl] });

    const jiraSection = wrapper.find(Field).at(0).dive();
    expect(jiraSection.find(InviteUrlField).length).to.equal(1);
    const jiraField = jiraSection.find(InviteUrlField);
    await (jiraField.prop('generateInviteUrl') as any)('jira', 'JIRA', expiration);

    expect(mockAnalyticsClient.sendTrackEvent.called).to.equal(true);
  });

  describe('check and generate invite url', () => {
    it('with success', async () => {
      const showFlag = sandbox.spy();
      const generateInviteUrl = sandbox.stub().resolves(true);
      const wrapper = build({ generateInviteUrl, showFlag, refetchInviteUrls: async () => [jiraInviteUrl, confluenceInviteUrl] });

      const conf = wrapper.find(Field).at(1).dive();
      const confCheckbox = conf.find(AkCheckboxStateless);
      expect(wrapper.state('waiting')).to.equal(false);

      // Checking checkbox
      const onChangeFinished = (confCheckbox as any).prop('onChange')({ preventDefault: sandbox.spy() });

      expect(generateInviteUrl.called).to.equal(true);
      expect(generateInviteUrl.getCall(0).args[0].variables).to.deep.equal({
        id: 'some-cloud-id',
        input: { productKey: 'confluence' },
      });
      expect(wrapper.state('waiting')).to.equal(true);

      await onChangeFinished;

      expect(showFlag.called).to.equal(true);
      expect(wrapper.state('waiting')).to.equal(false);

      expect(mockAnalyticsClient.sendUIEvent.called).to.equal(true);
      expect(mockAnalyticsClient.sendTrackEvent.called).to.equal(true);
    });

    it('with error', async () => {
      const showFlag = sandbox.spy();
      const generateInviteUrl = sandbox.stub().rejects();
      const wrapper = build({ generateInviteUrl, showFlag });

      const conf = wrapper.find(Field).at(1).dive();
      const confCheckbox = conf.find(AkCheckboxStateless);
      expect(wrapper.state('waiting')).to.equal(false);

      // Checking checkbox
      const onChangeFinished = (confCheckbox as any).prop('onChange')({ preventDefault: sandbox.spy() });

      expect(generateInviteUrl.called).to.equal(true);
      expect(generateInviteUrl.getCall(0).args[0].variables).to.deep.equal({
        id: 'some-cloud-id',
        input: { productKey: 'confluence' },
      });
      expect(wrapper.state('waiting')).to.equal(true);

      await onChangeFinished;

      expect(wrapper.state('waiting')).to.equal(false);

      expect(mockAnalyticsClient.sendUIEvent.called).to.equal(true);
    });
  });

  describe('uncheck and delete invite url', () => {
    it('with success', async () => {
      const showFlag = sandbox.spy();
      const deleteInviteUrl = sandbox.stub().resolves(true);
      const wrapper = build({ deleteInviteUrl, showFlag });

      const conf = wrapper.find(Field).at(0).dive();
      const confCheckbox = conf.find(AkCheckboxStateless);
      expect(wrapper.state('waiting')).to.equal(false);

      // Checking checkbox
      const onChangeFinished = (confCheckbox as any).prop('onChange')({ preventDefault: sandbox.spy() });

      expect(deleteInviteUrl.called).to.equal(true);
      expect(deleteInviteUrl.getCall(0).args[0].variables).to.deep.equal({
        id: 'some-cloud-id',
        input: { productKey: 'jira' },
      });
      expect(wrapper.state('waiting')).to.equal(true);

      await onChangeFinished;

      expect(showFlag.called).to.equal(true);
      expect(wrapper.state('waiting')).to.equal(false);

      expect(mockAnalyticsClient.sendUIEvent.called).to.equal(true);
      expect(mockAnalyticsClient.sendTrackEvent.called).to.equal(true);
    });

    it('with error', async () => {
      const showFlag = sandbox.spy();
      const deleteInviteUrl = sandbox.stub().rejects();
      const wrapper = build({ deleteInviteUrl, showFlag });

      const conf = wrapper.find(Field).at(0).dive();
      const confCheckbox = conf.find(AkCheckboxStateless);
      expect(wrapper.state('waiting')).to.equal(false);

      // Checking checkbox
      const onChangeFinished = (confCheckbox as any).prop('onChange')({ preventDefault: sandbox.spy() });

      expect(deleteInviteUrl.called).to.equal(true);
      expect(deleteInviteUrl.getCall(0).args[0].variables).to.deep.equal({
        id: 'some-cloud-id',
        input: { productKey: 'jira' },
      });
      expect(wrapper.state('waiting')).to.equal(true);

      await onChangeFinished;

      expect(wrapper.state('waiting')).to.equal(false);

      expect(mockAnalyticsClient.sendUIEvent.called).to.equal(true);
    });
  });
});
