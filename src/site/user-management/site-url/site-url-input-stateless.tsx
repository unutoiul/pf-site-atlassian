import * as React from 'react';

import AkInput from '@atlaskit/input';

import {
  CloudNamespace,
  ConditionalMessage,
  FieldHelpText,
  FieldWrapper,
  Label,
  SuccessMessage,
  ValidationMessage,
} from './site-url-input-stateless.styled';

interface SiteUrlInputProps {
  cloudNamespace?: string;
  helpText?: string | React.ReactElement<any>;
  isDisabled?: boolean;
  isFocused?: boolean;
  isHovered?: boolean;
  isInvalid?: boolean;
  isSuccessful?: boolean;
  label?: React.ReactElement<any> | string;
  successMessage?: string | React.ReactElement<any>;
  validationMessage?: string | React.ReactElement<any>;
  value: string;
  onBlur?(): void;
  onChange?(e: React.ChangeEvent<HTMLInputElement>): void;
  onFocus?(): void;
  onMouseOver?(): void;
  onMouseOut?(): void;
}

export class SiteUrlInputStateless extends React.Component<SiteUrlInputProps> {
  public static defaultProps: Partial<SiteUrlInputProps> = {
    cloudNamespace: 'atlassian.net',
    isDisabled: false,
    isFocused: false,
    isHovered: false,
    isInvalid: false,
    isSuccessful: false,
    onFocus: () => null,
    onBlur: () => null,
    onMouseOver: () => null,
    onMouseOut: () => null,
  };

  public render() {
    const {
      cloudNamespace,
      helpText,
      isDisabled,
      isFocused,
      isHovered,
      isInvalid,
      isSuccessful,
      label,
      onBlur,
      onChange,
      onFocus,
      onMouseOut,
      onMouseOver,
      successMessage,
      validationMessage,
      value,
    } = this.props;

    return (
      <React.Fragment>
        {label && <Label>{label}</Label>}
        <FieldWrapper
          isDisabled={isDisabled!}
          isFocused={isFocused!}
          isHovered={isHovered!}
          isInvalid={isInvalid!}
          isSuccessful={isSuccessful!}
        >
          <AkInput
            onBlur={onBlur}
            onChange={onChange}
            onFocus={onFocus}
            onMouseOver={onMouseOver}
            onMouseOut={onMouseOut}
            isEditing={true}
            readOnly={isDisabled}
            value={value}
          />
          <CloudNamespace>{['.', cloudNamespace].join('')}</CloudNamespace>
        </FieldWrapper>

        {helpText && <FieldHelpText>{helpText}</FieldHelpText>}

        <ConditionalMessage isOpen={Boolean(validationMessage || successMessage)}>
          {validationMessage && <ValidationMessage>{validationMessage}</ValidationMessage>}
          {successMessage && <SuccessMessage>{successMessage}</SuccessMessage>}
        </ConditionalMessage>

      </React.Fragment>
    );
  }
}
