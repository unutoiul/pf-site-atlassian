import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';

import { SiteUrlFormImpl } from './site-url-form';

const pageDecorator = (storyFn) => (
    <IntlProvider locale="en">
      {storyFn()}
    </IntlProvider>
);

const defaultProps = {
  cloudId: 'cloud-id-123',
  defaultValue: 'foo',
  onChange: () => null,
  isValidName: true,
};

storiesOf('Site|URL Page/Site URL Form', module)
  .addDecorator(pageDecorator)
  .add('default', () => (
    <SiteUrlFormImpl {...defaultProps} />
  ))
  .add('invalid', () => (
    <SiteUrlFormImpl
      {...defaultProps}
      defaultValue="hello"
      validationMessage="Computer says no"
    />
  ))
  .add('validates', () => (
    <SiteUrlFormImpl
      {...defaultProps}
      isValidName={true}
    />
  ))
  .add('success', () => (
    <SiteUrlFormImpl
      {...defaultProps}
      defaultValue="bar"
      isSuccessful={true}
    />
  ));
