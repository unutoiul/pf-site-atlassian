import * as React from 'react';
import { FormattedMessage } from 'react-intl';

import AkButton, { ButtonGroup as AkButtonGroup } from '@atlaskit/button';
import AkCheckCircleIcon from '@atlaskit/icon/glyph/check-circle';
import AkErrorIcon from '@atlaskit/icon/glyph/error';

import { Centered } from 'common/styled';

import { reload } from '../../../utilities/window-facade';
import { FormConfirmationModal } from './form-confirmation-modal';
import { messages } from './messages';
import { ButtonsWrapper } from './site-url-form.styled';
import { SiteUrlInput } from './site-url-input';

interface SiteUrlFormProps {
  cloudId: string;
  cloudNamespace?: string;
  defaultValue?: string;
  isDisabled?: boolean;
  isSuccessful?: boolean;
  isValidName?: boolean;
  validationMessage?: string | React.ReactElement<any>;
  onChange(e: React.ChangeEvent<HTMLInputElement>): void;
}

interface State {
  isConfirmationModalOpen: boolean;
  value: string;
}

const MIN_SITE_NAME_LENGTH = 3;

// tslint:disable jsx-use-translation-function
const Space = () => <React.Fragment>&nbsp;</React.Fragment>;

const successMessage = (
  <Centered axis="vertical">
    <AkCheckCircleIcon label="" size="small" />
    <Space />
    <FormattedMessage {...messages.successfulRenameText} />
  </Centered>
);

const ValidationMessageWrapper = ({ children }) => (
  <Centered axis="vertical">
    <AkErrorIcon label="" size="small" />
    <Space />
    {children}
  </Centered>
);

export class SiteUrlFormImpl extends React.Component<SiteUrlFormProps, State> {
  public static defaultProps: Partial<SiteUrlFormProps> = {
    cloudNamespace: 'atlassian.net',
    isDisabled: false,
    defaultValue: '',
  };

  public readonly state = {
    isConfirmationModalOpen: false,
    value: this.props.defaultValue || '',
  };

  private handleSubmitSuccess = reload;

  public render() {
    const {
      cloudId,
      cloudNamespace,
      isSuccessful,
    } = this.props;
    const {
      isConfirmationModalOpen,
      value,
    } = this.state;

    return (
      <React.Fragment>
        <SiteUrlInput
          cloudNamespace={cloudNamespace}
          helpText={<FormattedMessage {...messages.renameHelpText} />}
          isInvalid={this.isInvalid()}
          isSuccessful={isSuccessful}
          isDisabled={this.isDisabled()}
          onChange={this.handleInputChange}
          label={<FormattedMessage {...messages.siteNameFieldLabel} />}
          successMessage={isSuccessful ? successMessage : ''}
          validationMessage={this.getValidationMessage()}
          value={value}
        />

        <ButtonsWrapper>
          <AkButtonGroup>
            <AkButton
              appearance="primary"
              isDisabled={this.isSubmitDisabled()}
              onClick={this.openConfirmationModal}
            >
              <FormattedMessage {...messages.updateUrl} />
            </AkButton>
            <AkButton
              appearance="subtle"
              isDisabled={this.isDiscardDisabled()}
              onClick={this.discardSiteUrlChanges}
            >
              <FormattedMessage {...messages.discardChanges} />
            </AkButton>
          </AkButtonGroup>
        </ButtonsWrapper>
        <FormConfirmationModal
          isOpen={isConfirmationModalOpen}
          cloudId={cloudId}
          cloudName={value}
          cloudNamespace={cloudNamespace!}
          onClose={this.closeConfirmationModal}
          onSuccess={this.handleSubmitSuccess}
        />
      </React.Fragment>
    );
  }

  private isNameLongEnough = () => this.state.value.length >= MIN_SITE_NAME_LENGTH;

  private isInvalid = () => (
    !!this.props.validationMessage ||
    !this.props.isValidName ||
    !this.isNameLongEnough()
  );

  private getValidationMessage = () => {
    if (!this.isInvalid()) {
      return undefined;
    }

    const { validationMessage: externalMessage } = this.props;

    if (externalMessage) {
      return (
        <ValidationMessageWrapper>
          {externalMessage}
        </ValidationMessageWrapper>
      );
    }

    if (!this.isNameLongEnough()) {
      return (
        <ValidationMessageWrapper>
          <FormattedMessage {...messages.validationInsufficientCharCount} />
        </ValidationMessageWrapper>
      );
    }

    return undefined;
  }

  private openConfirmationModal = () => this.setState({ isConfirmationModalOpen: true });

  private closeConfirmationModal = () => this.setState({ isConfirmationModalOpen: false });

  private isModified = () => this.props.defaultValue !== this.state.value;

  private isDisabled = () => this.props.isDisabled || this.state.isConfirmationModalOpen;

  private isSubmitDisabled = () => (
    this.isDisabled() ||
    this.isInvalid() ||
    !this.isModified()
  );

  private isDiscardDisabled = () => (
    this.isDisabled() ||
    !this.isModified()
  );

  private discardSiteUrlChanges = () => {
    const defaultValue = this.props.defaultValue || '';
    this.setState({ value: defaultValue });
    this.props.onChange({ target: { value: defaultValue }, ...{} as any });
  }

  private handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    e.persist();
    this.setState({
      value: e.currentTarget.value,
      isConfirmationModalOpen: false,
    });
    this.props.onChange(e);
  }
}

export const SiteUrlForm = SiteUrlFormImpl;
