import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';

import AkButton from '@atlaskit/button';

import { FormConfirmationModal } from './form-confirmation-modal';
import { messages } from './messages';
import { SiteUrlFormImpl } from './site-url-form';
import { SiteUrlInput } from './site-url-input';

const noop = () => null;

describe('Site URL Form', () => {
  const defaultProps = {
    cloudId: 'cloud-id-123',
    onChange: noop,
  };

  const shallowRender = (props = {}) => shallow(
    <SiteUrlFormImpl
      {...defaultProps}
      {...props}
    />,
  );

  it('should pass cloudNamespace, isDisabled and isSuccessful props through to SiteUrlInput', () => {
    const testProps = {
      cloudNamespace: 'test.namespace',
      isDisabled: true,
      isSuccessful: true,
    };
    const wrapper = shallowRender(testProps);
    expect(wrapper.find(SiteUrlInput).props()).to.include(testProps);
  });

  describe('success message', () => {
    it('should pass success message to SiteUrlInput if props.isSuccessful is true', () => {
      const wrapper = shallowRender({ isSuccessful: true });
      const successMessageProp = wrapper.find(SiteUrlInput).props().successMessage as React.ReactElement<any>;
      expect(shallow(successMessageProp).find(FormattedMessage).props()).to.include(messages.successfulRenameText);
    });

    it('should pass empty-string success message to SiteUrlInput if props.isSuccessful is false', () => {
      const wrapper = shallowRender({ isSuccessful: false });
      expect(wrapper.find(SiteUrlInput).props().successMessage).to.equal('');
    });
  });

  describe('validation message', () => {
    it('should pass validation message to SiteUrlInput if props.validationMessage is supplied', () => {
      const wrapper = shallowRender({
        validationMessage: 'hello',
      });
      const validationMessageProp = wrapper.find(SiteUrlInput).props().validationMessage as React.ReactElement<any>;
      expect(shallow(validationMessageProp).html()).to.contain('hello');
      expect(wrapper.find(SiteUrlInput).props().isInvalid).to.equal(true);
    });

    it('should pass empty-string validation message to SiteUrlInput if props.validationMessage is not supplied/falsey', () => {
      const wrapper = shallowRender({ validationMessage: null, defaultValue: 'longEnoughName' });
      expect(wrapper.find(SiteUrlInput).props().validationMessage).to.equal(undefined);
    });

    it('should show name too short validation message if name is too short', () => {
      const wrapper = shallowRender({ validationMessage: null, defaultValue: 'x' });
      const valMsg = wrapper.find(SiteUrlInput).props().validationMessage as React.ReactElement<any>;
      expect(shallow(valMsg).find(FormattedMessage).props()).to.include(messages.validationInsufficientCharCount);
    });

    describe('SiteUrlInput', () => {
      it('should set SiteUrlInput isInvalid prop to false when validationMessage is not supplied, name is long enough and props.isValid is true', () => {
        const wrapper = shallowRender({ defaultValue: 'longEnoughName', validationMessage: null, isValidName: true });
        expect(wrapper.find(SiteUrlInput).props().isInvalid).to.equal(false);

        wrapper.setProps({ defaultValue: 'longEnoughName', validationMessage: undefined, isValidName: true });
        expect(wrapper.find(SiteUrlInput).props().isInvalid).to.equal(false);

        wrapper.setProps({ defaultValue: 'longEnoughName', validationMessage: '', isValidName: true });
        expect(wrapper.find(SiteUrlInput).props().isInvalid).to.equal(false);
      });

      it('should set SiteUrlInput isInvalid prop to true when validation message is supplied', () => {
        const wrapper = shallowRender({ defaultValue: 'longEnoughName', validationMessage: 'external validation message' });
        expect(wrapper.find(SiteUrlInput).props().isInvalid).to.equal(true);
      });

      it('should set SiteUrlInput isInvalid prop to true when name is not long enough', () => {
        const wrapper = shallowRender({ defaultValue: 'x', validationMessage: null });
        expect(wrapper.find(SiteUrlInput).props().isInvalid).to.equal(true);
      });

      it('should be disabled if confirmation modal is open', () => {
        const wrapper = shallowRender();
        expect(wrapper.find(SiteUrlInput).props().isDisabled).to.equal(false);
        wrapper.setState({ isConfirmationModalOpen: true });
        expect(wrapper.find(SiteUrlInput).props().isDisabled).to.equal(true);
      });
    });
  });

  it('should initialize state.value to props.defaultValue', () => {
    const wrapper = shallowRender({ defaultValue: 'mcb' });
    expect((wrapper.state() as any).value).to.equal('mcb');
  });

  describe('submit button', () => {
    const findSubmitButton = (wrapper) =>
      wrapper
        .find(AkButton)
        .filterWhere(n => n.props().appearance === 'primary');

    it('should not be disabled if the entered name is validated AND it is not the default value', () => {
      const wrapper = shallowRender({
        defaultValue: 'foo',
        isValidName: true,
      });
      wrapper.setState({ value: 'bar' }).update();
      expect(findSubmitButton(wrapper).props().isDisabled).to.equal(false);
    });

    it('should be disabled if the entered name is validated but is the default value', () => {
      const wrapper = shallowRender({
        defaultValue: 'foo',
        isValidName: true,
      });
      expect(findSubmitButton(wrapper).props().isDisabled).to.equal(true);
    });

    it('should be disabled if the entered name is not validated', () => {
      const wrapper = shallowRender({
        defaultValue: 'foo',
        isValidName: false,
      });
      wrapper.setState({ value: 'bar' }).update();
      expect(findSubmitButton(wrapper).props().isDisabled).to.equal(true);
    });

    it('should be disabled if the form is disabled', () => {
      const wrapper = shallowRender({
        isDisabled: true,
        defaultValue: 'foo',
        isValidName: true,
      });
      wrapper.setState({ value: 'bar' }).update();
      expect(findSubmitButton(wrapper).props().isDisabled).to.equal(true);
    });

    it('should be disabled if the input name is not long enough', () => {
      const wrapper = shallowRender({
        defaultValue: 'x',
        isValidName: true,
      });
      expect(findSubmitButton(wrapper).props().isDisabled).to.equal(true);
    });

    it('should be disabled if the confirmation modal is open', () => {
      const wrapper = shallowRender();
      wrapper.setState({ isConfirmationModalOpen: true }).update();
      expect(findSubmitButton(wrapper).props().isDisabled).to.equal(true);
    });
  });

  describe('discard button', () => {
    const findDiscardButton = (wrapper) =>
      wrapper
        .find(AkButton)
        .filterWhere(n => n.props().appearance === 'subtle');

    it('should be disabled if the entered name has not been modified', () => {
      const wrapper = shallowRender();
      expect(findDiscardButton(wrapper).props().isDisabled).to.equal(true);
    });

    it('should not be disabled if the entered name has been modified', () => {
      const wrapper = shallowRender({ defaultValue: 'foo' });
      wrapper.setState({ value: 'bar' }).update();
      expect(findDiscardButton(wrapper).props().isDisabled).to.equal(false);
    });

    it('should be disabled if the form is disabled', () => {
      const wrapper = shallowRender({
        isDisabled: true,
        defaultValue: 'foo',
      });
      wrapper.setState({ value: 'bar' }).update();
      expect(findDiscardButton(wrapper).props().isDisabled).to.equal(true);
    });

    it('should be disabled if the confirmation modal is open', () => {
      const wrapper = shallowRender();
      wrapper.setState({ isConfirmationModalOpen: true }).update();
      expect(findDiscardButton(wrapper).props().isDisabled).to.equal(true);
    });
  });

  describe('FormConfirmationModal', () => {
    it('should pass state.isConfirmationModalOpen to modal\'s isOpen prop', () => {
      const wrapper = shallowRender();
      expect(wrapper.find(FormConfirmationModal).props().isOpen).to.equal(false);
      wrapper.setState({ isConfirmationModalOpen: true }).update();
      expect(wrapper.find(FormConfirmationModal).props().isOpen).to.equal(true);
    });
  });
});
