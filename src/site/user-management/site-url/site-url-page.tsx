import * as React from 'react';
import { graphql, MutationFunc } from 'react-apollo';
import { FormattedMessage } from 'react-intl';
import { RouteComponentProps } from 'react-router';

import AkSectionMessage from '@atlaskit/section-message';
import AkSpinner from '@atlaskit/spinner';

import { GenericError } from 'common/error';
import { PageLayout } from 'common/page-layout';

import { SiteNameAvailabilityCheckResult, SiteRenameStatus, SiteUrlPageNameCheckMutation, SiteUrlPageNameCheckMutationVariables, SiteUrlPageQuery } from '../../../schema/schema-types';
import { debounce } from '../../../utilities/decorators';
import { UrlPageFeatureFlag, withUrlPageFeatureFlag } from '../../feature-flags/with-site-url-page-feature-flag';
import { messages } from './messages';
import SiteNameAvailabilityCheck from './site-name-availability-check.mutation.graphql';
import { SiteUrlForm } from './site-url-form';
import SiteUrlPageData from './site-url-page.query.graphql';
import { SectionMessageWrapper } from './site-url-page.styled';

interface CheckNameMutationProps {
  checkSiteNameAvailability: MutationFunc<SiteUrlPageNameCheckMutation, SiteUrlPageNameCheckMutationVariables>;
}

type NonQueryProps = UrlPageFeatureFlag & RouteComponentProps<{ cloudId: string }>;

interface StatusQueryChildProps {
  siteRenameStatus: Partial<SiteRenameStatus> & {
    loading: boolean;
    error: boolean;
  };
}

type Props = NonQueryProps & CheckNameMutationProps & StatusQueryChildProps;

interface State {
  nameChecks: SiteNameAvailabilityCheckResult;
  isValidationPending: boolean;
}

export class SiteUrlPageImpl extends React.Component<Props, State> {
  public readonly state = {
    nameChecks: {
      isAlreadyOwner: true,
      taken: true,
      renameLimitReached: false,
      result: true,
    },
    isValidationPending: false,
  };

  public render() {
    const {
      siteRenameStatus: {
        loading = true,
        error = null,
      } = {},
      urlPageFeatureFlag,
    } = this.props;

    if (urlPageFeatureFlag.isLoading || !urlPageFeatureFlag.value) {
      return null;
    }

    if (loading || error) {
      return (
        <PageLayout
          title={<FormattedMessage {...messages.title} />}
          description={<FormattedMessage {...messages.description} />}
        >
          {error ? (
            <GenericError />
          ) : (
            <AkSpinner />
          )}
        </PageLayout>
      );
    }

    return this.renderPage();
  }

  private renderPage() {
    const { siteRenameStatus, match: { params: { cloudId } } } = this.props;

    const isRenameLimitReached = this.isRenameLimitReached();

    return (
      <PageLayout
        title={<FormattedMessage {...messages.title} />}
        description={<FormattedMessage {...messages.description} />}
      >
        {isRenameLimitReached && (
          <SectionMessageWrapper>
            <AkSectionMessage appearance="warning">
              <FormattedMessage {...messages.renameLimitReachedSectionMessage} tagName="p" />
              <p>
                <a href="https://support.atlassian.com/contact/#" target="_blank" rel="noopener noreferrer">
                  <FormattedMessage {...messages.contactSupport} />
                </a>
              </p>
            </AkSectionMessage>
          </SectionMessageWrapper>
        )}
        <SiteUrlForm
          cloudId={cloudId}
          cloudNamespace={siteRenameStatus.cloudNamespace}
          defaultValue={siteRenameStatus.cloudName}
          onChange={this.onFormInputChange}
          isDisabled={isRenameLimitReached}
          isValidName={this.isValidName()}
          validationMessage={this.getValidationMessage()}
        />
      </PageLayout>
    );
  }

  @debounce('onInputChange')
  private async debouncedCheckSiteNameAvailability(siteName: string) {
    const { cloudId } = this.props.match.params;
    const result = await this.props.checkSiteNameAvailability({
      variables: {
        id: cloudId,
        siteName,
      },
    });

    this.setState({
      nameChecks: result.data.checkSiteNameAvailability,
      isValidationPending: false,
    });
  }

  private onFormInputChange = async (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ isValidationPending: true });
    try {
      await this.debouncedCheckSiteNameAvailability(e.target.value);
    } catch (e) {
      this.setState({ isValidationPending: false });
    }
  }

  private isValidName = () => !this.state.isValidationPending && this.state.nameChecks.result;

  private isRenameLimitReached = () => {
    const { siteRenameStatus } = this.props;
    const isLimitReachedAtTimeOfPageLoad = Number(siteRenameStatus.renameLimit) <= Number(siteRenameStatus.renamesUsed);
    const isLimitReachedAtTimeOfNameValidation = this.state.nameChecks.renameLimitReached;

    return isLimitReachedAtTimeOfPageLoad || isLimitReachedAtTimeOfNameValidation;
  }

  private getValidationMessage = () => {
    const {
      isValidationPending,
      nameChecks: {
        isAlreadyOwner,
        renameLimitReached,
        result,
        taken,
      },
    } = this.state;

    if (isValidationPending || result || (!result && renameLimitReached)) {
      return undefined;
    }

    if (taken && !isAlreadyOwner) {
      return <FormattedMessage {...messages.validationNameTaken} />;
    }

    return <FormattedMessage {...messages.validationCatchAll} />;
  }
}

const withSiteUrlPageQuery = <P extends NonQueryProps>(Component) =>
  graphql<P, SiteUrlPageQuery, {}, StatusQueryChildProps>(
    SiteUrlPageData,
    {
      props: ({ data }) => {
        const queryData = data && data.currentSite && data.currentSite.siteRenameStatus || {};

        return {
          siteRenameStatus: {
            loading: !!data && data.loading,
            error: !!(data && data.error),
            ...queryData,
          },
        };
      },
    },
  )(Component);

const withSiteNameAvailabilityCheckMutation = <P extends NonQueryProps>(Component) =>
  graphql<P, SiteUrlPageNameCheckMutation, SiteUrlPageNameCheckMutationVariables>(SiteNameAvailabilityCheck,
    {
      name: 'checkSiteNameAvailability',
    },
  )(Component);

export const SiteUrlPage =
  withUrlPageFeatureFlag(
    withSiteNameAvailabilityCheckMutation(
      withSiteUrlPageQuery(
        SiteUrlPageImpl,
  )));
