import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';

import AkButton from '@atlaskit/button';
import AkTextField from '@atlaskit/field-text';

import { FormConfirmationModalImpl } from './form-confirmation-modal';

const pageDecorator = (storyFn) => (
    <IntlProvider locale="en">
      {storyFn()}
    </IntlProvider>
);

const storyMessages = {
  cloudId: 'cloud-id-123',
  cloudNamespace: 'atlassian.net',
  fieldLabel: 'Site name',
  openDialogButtonLabel: 'Rename!',
};

const noop = () => undefined;

const defaultProps = {
  cloudId: 'cloud-id-1234',
  cloudName: 'updog',
  cloudNamespace: 'atlassian.net',
  isOpen: true,
  mutate: () => new Promise(resolve => setTimeout(resolve, 1500)) as any,
};

class PersistanceExample extends React.Component {
  public state = {
    isDialogOpen: true,
    siteName: 'foo',
  };

  public render() {
    return (
      <React.Fragment>
        <AkTextField label={storyMessages.fieldLabel} onChange={this.onSiteNameChange} />
        <p>
          <AkButton appearance="primary" onClick={this.openDialog}>{storyMessages.openDialogButtonLabel}</AkButton>
        </p>
        <FormConfirmationModalImpl
          {...defaultProps}
          isOpen={this.state.isDialogOpen}
          cloudId={storyMessages.cloudId}
          cloudName={this.state.siteName}
          cloudNamespace={storyMessages.cloudNamespace}
          onClose={this.closeDialog}
        />
      </React.Fragment>
    );
  }

  private openDialog = () => this.setState({ isDialogOpen: true });

  private closeDialog = () => this.setState({ isDialogOpen: false });

  private onSiteNameChange = (e: React.ChangeEvent<HTMLInputElement>) => this.setState({ siteName: e.currentTarget.value });
}

storiesOf('Site|URL Page/Form Confirmation Modal', module)
  .addDecorator(pageDecorator)
  .add('default', () => (
    <FormConfirmationModalImpl
      {...defaultProps}
      onClose={noop}
    />
  ))
  .add('Modal keeps site name up to date', () => (
    <PersistanceExample />
  ));
