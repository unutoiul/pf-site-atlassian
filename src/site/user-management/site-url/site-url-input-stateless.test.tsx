import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import AkInput from '@atlaskit/input';

import { SiteUrlInputStateless } from './site-url-input-stateless';
import {
  CloudNamespace,
  FieldHelpText,
  FieldWrapper,
  Label,
  SuccessMessage,
  ValidationMessage,
} from './site-url-input-stateless.styled';

describe('Site URL Input Stateless', () => {

  const shallowRenderStatelessInput = (props = {}) => shallow(
    <SiteUrlInputStateless
      value=""
      {...props}
    />,
  );

  it('should render label if props.label is not empty', () => {
    const wrapper = shallowRenderStatelessInput({ label: 'hello' });
    expect(wrapper.find(Label).children().text()).to.equal('hello');
  });

  it('should not render label if props.label is empty', () => {
    const wrapper = shallowRenderStatelessInput({ label: null });
    expect(wrapper.find(Label).exists()).to.equal(false);
  });

  it('should pass through isDisabled, isFocused, isHovered, isInvalid and isSuccessful to FieldWrapper', () => {
    const stylingProps = {
      isDisabled: true,
      isFocused: true,
      isHovered: true,
      isInvalid: true,
      isSuccessful: true,
    };
    const wrapper = shallowRenderStatelessInput(stylingProps);

    expect(wrapper.find(FieldWrapper).props()).to.include(stylingProps);
  });

  it('should pass through onBlur, onChange, onFocus, onMouseOut, onMouseover and value to AkInput', () => {
    const interactiveProps = {
      onBlur: () => null,
      onChange: () => null,
      onFocus: () => null,
      onMouseOut: () => null,
      onMouseOver: () => null,
      value: '',
    };
    const wrapper = shallowRenderStatelessInput(interactiveProps);

    expect(wrapper.find(AkInput).props()).to.include(interactiveProps);
  });

  it('should set AkInput to readonly if props.isDisabled', () => {
    const wrapper = shallowRenderStatelessInput({ isDisabled: true });

    expect(wrapper.find(AkInput).props()).to.include({ readOnly: true });
  });

  it('should set cloud namespace', () => {
    const cloudNamespace = 'site-rename.swarm';
    const wrapper = shallowRenderStatelessInput({ cloudNamespace });
    expect(wrapper.find(CloudNamespace).children().text()).to.equal(`.${cloudNamespace}`);
  });

  describe('Help text', () => {
    it('should render if helpText is provided', () => {
      const wrapper = shallowRenderStatelessInput({ helpText: 'Welcome to Costco, I love you' });
      expect(wrapper.find(FieldHelpText).exists()).to.equal(true);
    });

    it('should not render if helpText is not provided', () => {
      const wrapper = shallowRenderStatelessInput();
      expect(wrapper.find(FieldHelpText).exists()).to.equal(false);
    });
  });

  describe('validation message', () => {
    it('should render if validationMessage prop is provided', () => {
      const wrapper = shallowRenderStatelessInput({ validationMessage: 'Text that is for validation' });
      expect(wrapper.find(ValidationMessage).exists()).to.equal(true);
    });

    it('should not render if validationMessage is not provided', () => {
      const wrapper = shallowRenderStatelessInput();
      expect(wrapper.find(ValidationMessage).exists()).to.equal(false);
    });
  });

  describe('success message', () => {
    it('should render if successMessage is provided', () => {
      const wrapper = shallowRenderStatelessInput({ successMessage: 'Nice' });
      expect(wrapper.find(SuccessMessage).exists()).to.equal(true);
    });

    it('should not render if successMessage is not provided', () => {
      const wrapper = shallowRenderStatelessInput();
      expect(wrapper.find(SuccessMessage).exists()).to.equal(false);
    });
  });
});
