import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  title: {
    id: 'site-url-page.title',
    description: 'Title of the Site URL settings page.',
    defaultMessage: 'Site URL',
  },
  description: {
    id: 'site-url-page.description',
    description: 'Description for the site URL settings page.',
    defaultMessage: 'You can update your site URL to any name that is not currently in use on another Atlassian site, for instance, to match a company or department name. Updates take effect immediately. When users access the old URL, we\'ll automatically redirect them to the new site URL.',
  },
  contactSupport: {
    id: 'site-url-page.contact-support',
    description: 'Text for a link that navigates the user to a contact page for customer support',
    defaultMessage: 'Contact support',
  },
  renameLimitReachedSectionMessage: {
    id: 'site-url-page.rename-limit-reached.section-message',
    description: 'A message that is displayed to the user when they have exhausted their self-serve site renames.',
    defaultMessage: 'The maximum number of site URL updates has been reached. Contact Atlassian support to make additional changes.',
  },
  siteNameFieldLabel: {
    id: 'site-url-page.rename.site-name-field.label',
    description: 'Label for the site name field',
    defaultMessage: 'Site URL',
  },
  renameHelpText: {
    id: 'site-url-page.rename.help-text',
    description: 'Text informing the user of how they should format their input.',
    defaultMessage: 'Use numbers or lowercase letters, and at least 6 characters',
  },
  successfulRenameText: {
    id: 'site-url-page.rename.success-text',
    description: 'Text informing the user of that their rename has been submitted successfully',
    defaultMessage: 'URL successfully updated',
  },
  updateUrl: {
    id: 'site-url-page.rename.btn.update-url',
    description: 'Text for the button that submits the site rename request.',
    defaultMessage: 'Update URL',
  },
  discardChanges: {
    id: 'site-url-page.rename.btn.discard',
    description: 'Text for the button that discards changes made by the user.',
    defaultMessage: 'Discard',
  },
  validationNameTaken: {
    id: 'site-url-page.rename.validation.name-taken',
    description: 'A field validation messages shown to the user when they enter a site name that has already been taken',
    defaultMessage: 'The site URL is already taken',
  },
  validationInsufficientCharCount: {
    id: 'site-url-page.rename.validation.insufficient-char-count',
    description: 'A field validation messages shown to the user when they enter a site name that has too few characters',
    defaultMessage: 'Your site URL doesn\'t have enough characters',
  },
  validationCatchAll: {
    id: 'site-url-page.rename.validation.catch-all',
    description: 'A generic error messages shown to the user when they enter a site name that is invalid in some way',
    defaultMessage: 'Invalid site name',
  },
  formConfirmationModalTitle: {
    id: 'site-url-page.rename.confirmation-modal.title',
    description: 'The title of the modal dialog that appears to check if the user is sure before proceeding with the rename.',
    defaultMessage: 'Update site URL',
  },
  formConfirmationModalYourSiteWillBeRenamedTo: {
    id: 'site-url-page.rename.confirmation-modal.paragraph.your-site-will-be-renamed-to',
    description: 'The a part of the body of text of the form confirmation dialog. This text is followed by the preview of what the site will be renamed to.',
    defaultMessage: 'Your site will be renamed to:',
  },
  formConfirmationModalDelayedEffectDisclaimer: {
    id: 'site-url-page.rename.confirmation-modal.paragraph.delayed-effect-disclaimer',
    description: 'Appears in the form confirmation dialog. Warns/informs that the changes may not take effect instantly.',
    defaultMessage: 'Please allow up to 5 minutes for this change to take effect across Atlassian Cloud, before attempting to submit any further changes to the site URL. If this change is not visible on this page by then please contact Atlassian Support.',
  },
  cancel: {
    id: 'site-url-page.btn.cancel',
    description: 'Text for a button that cancels an action or flow',
    defaultMessage: 'Cancel',
  },
});
