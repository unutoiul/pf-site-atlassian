import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import { FormattedMessage } from 'react-intl';
import { FormConfirmationModalMutation, FormConfirmationModalMutationVariables } from 'src/schema/schema-types';

import AkButton, { ButtonGroup as AkButtonGroup } from '@atlaskit/button';

import { ModalDialog } from 'common/modal';

import renameSiteMutation from './form-confirmation-modal.mutation.graphql';
import { messages } from './messages';

interface FormConfirmationModalProps {
  isOpen: boolean;
  cloudId: string;
  cloudName: string;
  cloudNamespace: string;
  onClose(): void;
  onSuccess?(): void;
}

type Props = ChildProps<FormConfirmationModalProps, FormConfirmationModalMutation, FormConfirmationModalMutationVariables>;

export interface FormConfirmationModalState {
  isPendingMutation: boolean;
}

export class FormConfirmationModalImpl extends React.Component<Props, FormConfirmationModalState> {
  public readonly state = {
    isPendingMutation: false,
  };

  public render() {
    const { cloudName, cloudNamespace, isOpen, onClose } = this.props;

    return (
      <ModalDialog
        header={<FormattedMessage {...messages.formConfirmationModalTitle} />}
        footer={this.renderFooter()}
        isOpen={isOpen}
        onClose={onClose}
        width="small"
      >
        <p>
          <FormattedMessage {...messages.formConfirmationModalYourSiteWillBeRenamedTo}/>
        </p>
        <p>
          <strong>
            {[cloudName, cloudNamespace].join('.')}
          </strong>
        </p>
        <p>
          <small>
            <FormattedMessage {...messages.formConfirmationModalDelayedEffectDisclaimer} />
          </small>
        </p>
      </ModalDialog>
    );
  }

  private renderFooter() {
    const { isPendingMutation } = this.state;

    return (
      <AkButtonGroup>
        <AkButton
          appearance="primary"
          onClick={this.renameSite}
          isLoading={isPendingMutation}
        >
          <FormattedMessage {...messages.updateUrl} />
        </AkButton>
        <AkButton
          appearance="subtle"
          onClick={this.props.onClose}
        >
          <FormattedMessage {...messages.cancel} />
        </AkButton>
      </AkButtonGroup>
    );
  }

  private renameSite = async () => {
    const { cloudId, cloudName, cloudNamespace, mutate, onClose, onSuccess } = this.props;

    this.setState({ isPendingMutation: true });

    try {
      await mutate!({
        variables: {
          id: cloudId,
          input: {
            cloudName,
            cloudNamespace,
          },
        },
      });
      onClose();
      if (onSuccess) {
        onSuccess();
      }
    } catch (e) {
      this.setState({ isPendingMutation: false });
    }
  }
}

const withRenameSiteMutation = graphql<FormConfirmationModalProps, FormConfirmationModalMutation, FormConfirmationModalMutationVariables>(renameSiteMutation);

export const FormConfirmationModal = withRenameSiteMutation(FormConfirmationModalImpl);
