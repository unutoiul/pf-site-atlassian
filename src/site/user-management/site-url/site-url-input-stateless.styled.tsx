import * as React from 'react';
import styled from 'styled-components';

import {
  borderRadius as AkBorderRadius,
  colors as akColors,
  gridSize as akGridSize,
} from '@atlaskit/theme';

const gridSize = akGridSize();

// Util to allow easy prop declarations for SC
const Div = <T extends object>() => (props: T & { className?: string, children?: React.ReactNode }) => <div className={props.className}>{props.children}</div>;

interface FieldWrapperProps {
  isDisabled: boolean;
  isFocused: boolean;
  isHovered: boolean;
  isInvalid: boolean;
  isSuccessful: boolean;
}

const FieldWrapperBase = Div<FieldWrapperProps>();

const getBorderColor = (props: FieldWrapperProps) => {
  if (props.isFocused && !props.isDisabled) {
    return `border-color: ${akColors.B100}`;
  }
  if (props.isInvalid) {
    return `border-color: ${akColors.R400}`;
  }
  if (props.isSuccessful) {
    return `border-color: ${akColors.G300}`;
  }
  if (props.isDisabled) {
    return `border-color: transparent`;
  }

  return `border-color: ${akColors.N40}`;
};

const getBackgroundColor = (props: FieldWrapperProps) => {
  if (props.isDisabled) {
    return `background-color: ${akColors.N20}`;
  }
  if (props.isFocused) {
    return `background-color: ${akColors.N0}`;
  }
  if (props.isHovered) {
    return `background-color: ${akColors.N30}`;
  }

  return `background-color: ${akColors.N10}`;
};

export const FieldWrapper = styled(FieldWrapperBase)`
  /*
    Border needs to be 2px according to Text field 1.1 spec
    Border + padding = 1 grid unit
  */
  border: ${gridSize * 0.25}px solid;
  padding: ${gridSize * 0.75}px;
  ${getBorderColor};
  border-radius: ${AkBorderRadius}px;
  box-sizing: border-box;
  height: ${gridSize * 5}px;
  width: ${gridSize * 40}px;
  display: flex;
  align-items: center;
  color: ${({ isDisabled }) => isDisabled ? akColors.N70 : 'inherit'};
  ${getBackgroundColor};
  cursor: ${({ isDisabled }) => isDisabled ? 'not-allowed' : 'inherit'};
  transition: background-color 0.2s ease-in-out 0s, border-color 0.2s ease-in-out 0s;
`;

export const CloudNamespace = styled.span`
  color: ${akColors.N70};
  padding-right: ${gridSize * 0.5}px;
  white-space: nowrap;
`;

export const Label = styled.span`
  display: inline-block;
  color: ${akColors.N200};
  font-size: 11px;
  line-height: 16px;
  font-weight: 600;
  margin: 0 0 ${gridSize}px 0;
`;

const ConditionalMessageBase = Div<{ isOpen: boolean}>();

export const ConditionalMessage = styled(ConditionalMessageBase)`
  height: ${({ isOpen }) => isOpen ? `${gridSize * 2}px` : 0};
  opacity: ${({ isOpen }) => isOpen ? 1 : 0};
  transition: height 0.25s ease-in-out, opacity 0.25s ease-in-out;
`;

const Small = styled.small`
  font-size: 11px;
  line-height: 16px;
  letter-spacing: -0.09px;
`;

export const FieldHelpText = styled(Small)`
  display: block;
  color: ${akColors.N200};
  margin-top: ${gridSize * 0.5}px;
`;

export const ValidationMessage = styled(Small)`
  display: block;
  color: ${akColors.R400};
  margin-top: ${gridSize * 0.5}px;
`;

export const SuccessMessage = styled(Small)`
  display: block;
  color: ${akColors.G300};
  margin-top: ${gridSize * 0.5}px;
`;
