import styled from 'styled-components';

import { gridSize as akGridSize } from '@atlaskit/theme';

const gridSize = akGridSize();

export const SectionMessageWrapper = styled.div`
  margin-bottom: ${gridSize * 2}px;
`;
