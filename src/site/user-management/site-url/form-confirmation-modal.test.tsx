import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import AkButton from '@atlaskit/button';

import { FormConfirmationModalImpl, FormConfirmationModalState } from './form-confirmation-modal';

const noop = () => null;

describe('Site Rename Form Confirmation Modal', () => {
  const defaultProps = {
    isOpen: true,
    onClose: noop,
    onSuccess: noop,
    cloudId: 'cloud-id-123',
    cloudName: 'foo',
    cloudNamespace: 'atlassian.net',
    mutate: sinon.stub().returns(Promise.resolve()),
  };

  const shallowRender = (props = {}) => shallow(
    <FormConfirmationModalImpl
      {...defaultProps}
      {...props}
    />,
  );

  const simulateClickSubmit = (wrapper) => {
    const footer = shallow((wrapper.props()).footer);
    footer
      .find(AkButton)
      .filterWhere(n => (n.props() as any).appearance === 'primary')
      .simulate('click');
  };

  afterEach(() => {
    defaultProps.mutate.reset();
  });

  it('should render props.siteUrl', () => {
    const wrapper = shallowRender();
    expect(wrapper.find('strong').text()).to.equal([defaultProps.cloudName, defaultProps.cloudNamespace].join('.'));
  });

  it('should call props.mutate with cloudId, cloudName and cloudNamespace props on submission', () => {
    const wrapper = shallowRender();
    simulateClickSubmit(wrapper);
    expect(defaultProps.mutate.callCount).to.equal(1);
    const { variables } = defaultProps.mutate.lastCall.args[0];
    expect(variables).to.deep.equal({
      id: defaultProps.cloudId,
      input: {
        cloudName: defaultProps.cloudName,
        cloudNamespace: defaultProps.cloudNamespace,
      },
    });
  });

  it('should stop button spinning on failed renames', async () => {
    const wrapper = shallowRender({ mutate: sinon.stub().rejects() });
    simulateClickSubmit(wrapper);
    await new Promise(setImmediate);
    expect((wrapper.state() as FormConfirmationModalState).isPendingMutation).to.equal(false);
  });
});
