import styled from 'styled-components';

import { gridSize as akGridSize } from '@atlaskit/theme';

const gridSize = akGridSize();

export const ButtonsWrapper = styled.div`
  margin-top: ${gridSize * 3}px;
`;
