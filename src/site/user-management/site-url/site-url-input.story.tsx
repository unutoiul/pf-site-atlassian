import { boolean as booleanKnob, text as textKnob } from '@storybook/addon-knobs';
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import styled from 'styled-components';

import AkCircleCheck from '@atlaskit/icon/glyph/check-circle';
import AkErrorIcon from '@atlaskit/icon/glyph/error';

import { Centered } from 'common/styled';

import { SiteUrlInput, SiteUrlInputProps } from './site-url-input';

const StoryWrapper = styled.div`
  margin: 20px;
`;

const DottedBorder = styled.div`
  border: 1px dashed;
`;

class ExampleInput extends React.Component<Partial<SiteUrlInputProps>, { value: string }> {
  public state = {
    value: 'foo',
  };

  public render() {
    return (
      <SiteUrlInput
        label="Site URL"
        onChange={this.onChange}
        value={this.state.value}
        {...this.props}
      />
    );
  }

  private onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ value: e.currentTarget.value });
  }
}

const storyMessages = {
  helpText: 'Use numbers or lowercase letters and at least 6 characters',
  successMessage: 'Woohoo!',
  validationMessage: 'You dun goofed, kid',
};

const successMessage = <Centered axis="vertical"><AkCircleCheck size="small" label="" />{storyMessages.successMessage}</Centered>;
const validationMessage = <Centered axis="vertical"><AkErrorIcon size="small" label="" />{storyMessages.validationMessage}</Centered>;

storiesOf('Site|URL Page/Site URL Input', module)
  .addDecorator(storyFn => <StoryWrapper>{storyFn()}</StoryWrapper>)
  .add('default', () => <ExampleInput />)
  .add('no label', () => <ExampleInput label={undefined} />)
  .add('disabled', () => <ExampleInput isDisabled={true} />)
  .add('invalid', () => <ExampleInput isInvalid={true} validationMessage={validationMessage} />)
  .add('successful', () => <ExampleInput isSuccessful={true} successMessage={successMessage} />)
  .add('customizable', () => (
    <DottedBorder>
      <ExampleInput
        successMessage={booleanKnob('Show successMessage', false) && successMessage}
        validationMessage={booleanKnob('Show validationMessage', false) && validationMessage}
        cloudNamespace={textKnob('cloudNamespace', 'atlassian.net')}
        label={textKnob('label', 'Site URL')}
        helpText={textKnob('helpText', storyMessages.helpText)}
      />
    </DottedBorder>
  ));
