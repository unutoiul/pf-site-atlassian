import * as React from 'react';

import { SiteUrlInputStateless } from './site-url-input-stateless';

export interface SiteUrlInputProps {
  cloudNamespace?: string;
  helpText?: string | React.ReactElement<any>;
  isInvalid?: boolean;
  isSuccessful?: boolean;
  isDisabled?: boolean;
  label?: React.ReactElement<any> | string;
  successMessage?: string | React.ReactElement<any>;
  validationMessage?: string | React.ReactElement<any>;
  value: string;
  onChange?(e: React.ChangeEvent<HTMLInputElement>): void;
}

interface State {
  isFocused: boolean;
  isHovered: boolean;
}

export class SiteUrlInput extends React.Component<SiteUrlInputProps, State> {
  public static defaultProps: Partial<SiteUrlInputProps> = {
    cloudNamespace: 'atlassian.net',
    isDisabled: false,
    isInvalid: false,
    isSuccessful: false,
  };

  public readonly state = {
    isFocused: false,
    isHovered: false,
  };

  public render() {
    const {
      cloudNamespace,
      helpText,
      isDisabled,
      isInvalid,
      isSuccessful,
      label,
      onChange,
      successMessage,
      validationMessage,
      value,
    } = this.props;
    const { isFocused, isHovered } = this.state;

    return (
      <SiteUrlInputStateless
        cloudNamespace={cloudNamespace}
        helpText={helpText}
        isDisabled={isDisabled}
        isFocused={isFocused}
        isHovered={isHovered}
        isInvalid={isInvalid}
        isSuccessful={isSuccessful}
        label={label}
        onBlur={this.handleBlur}
        onChange={onChange}
        onFocus={this.handleFocus}
        onMouseOut={this.handleMouseOut}
        onMouseOver={this.handleMouseOver}
        successMessage={successMessage}
        validationMessage={validationMessage}
        value={value}
      />
    );
  }

  private handleBlur = () => this.setState({ isFocused: false });

  private handleFocus = () => this.setState({ isFocused: true });

  private handleMouseOver = () => this.setState({ isHovered: true });

  private handleMouseOut = () => this.setState({ isHovered: false });
}
