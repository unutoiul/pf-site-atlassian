import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';

import { createApolloClient } from '../../../apollo-client';
import { SiteUrlPageImpl } from './site-url-page';

const createData = (dataProps = {}) => ({
  siteRenameStatus: {
    cloudName: 'foo',
    cloudNamespace: 'atlassian.gov',
    renamesUsed: 0,
    renameLimit: 5,
    loading: false,
    error: false,
    ...dataProps,
  },
});

const getProps = (props = {}) => ({
  siteUrlPageFeatureFlag: {
    isLoading: false,
    value: true,
  },
  match: {
    params: {
      cloudId: 'cloud-id-123',
    },
  },
  ...createData(),
  ...props,
});

const resolveAsValid = async () => ({
  data: {
    checkSiteNameAvailability: {
      result: true,
      taken: false,
      renameLimiteReached: false,
      isAlreadyOwnder: false,
    },
  },
});

const resolveAsTaken = async () => ({
  data: {
    checkSiteNameAvailability: {
      result: false,
      taken: true,
      renameLimiteReached: false,
      isAlreadyOwnder: false,
    },
  },
});

const client = createApolloClient();

const pageDecorator = (storyFn) => (
  <ApolloProvider client={client}>
    <IntlProvider locale="en">
      {storyFn()}
    </IntlProvider>
  </ApolloProvider>
);

storiesOf('Site|URL Page', module)
  .addDecorator(pageDecorator)
  .add('default', () => (
    <SiteUrlPageImpl
      {...getProps()}
      {...{} as any}
    />
  ))
  .add('Name resolves as valid', () => (
    <SiteUrlPageImpl
      checkSiteNameAvailability={resolveAsValid}
      {...getProps()}
      {...{} as any}
    />
  ))
  .add('Name resolves as taken', () => (
    <SiteUrlPageImpl
      checkSiteNameAvailability={resolveAsTaken}
      {...getProps()}
      {...{} as any}
    />
  ))
  .add('Rename limit reached', () => (
    <SiteUrlPageImpl
      {...getProps(createData({ renamesUsed: 5 }))}
      {...{} as any}
    />
  ))
  .add('FF isLoading (blank)', () => (
    <SiteUrlPageImpl
      {...getProps({ siteUrlPageFeatureFlag: { isLoading: true, value: true } })}
      {...{} as any}
    />
  ))
  .add('FF disabled (blank)', () => (
    <SiteUrlPageImpl
      {...getProps({ siteUrlPageFeatureFlag: { isLoading: false, value: false } })}
      {...{} as any}
    />
  ))
  .add('data loading', () => (
    <SiteUrlPageImpl
      {...getProps(createData({ loading: true }))}
      {...{} as any}
    />
  ))
  .add('data error', () => (
    <SiteUrlPageImpl
      {...getProps(createData({ error: true }))}
      {...{} as any}
    />
  ));
