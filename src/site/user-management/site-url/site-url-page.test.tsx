import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import AkSectionMessage from '@atlaskit/section-message';
import AkSpinner from '@atlaskit/spinner';

import { PageLayout } from 'common/page-layout';

import { GenericError } from 'common/error';

import { messages } from './messages';
import { SiteUrlForm } from './site-url-form';
import { SiteUrlPageImpl } from './site-url-page';

describe('SiteUrlPage', () => {
  const createData = (dataProps = {}) => ({
    cloudName: 'foo',
    cloudNamespace: 'atlassian.gov',
    renamesUsed: 0,
    renameLimit: 5,
    loading: false,
    error: false,
    ...dataProps,
  });

  const defaultProps = {
    urlPageFeatureFlag: {
      isLoading: false,
      value: true,
    },
    siteRenameStatus: createData(),
    match: {
      params: {
        cloudId: 'cloud-id-123',
      },
    },
  };

  const shallowRender = (props = {}) => shallow(
    <SiteUrlPageImpl
      {...defaultProps}
      {...props}
      {...{} as any}
    />,
  );

  describe('before feature flag has been fetched,', () => {
    it('should not render if feature flag is loading', () => {
      const wrapper = shallowRender({ urlPageFeatureFlag: { isLoading: true, value: true } });
      expect(wrapper.isEmptyRender()).to.equal(true);
    });
  });

  describe('after feature flag has been fetched,', () => {

    it('should not render if feature flag is not enabled', () => {
      expect(shallowRender({ urlPageFeatureFlag: { isLoading: false, value: false } }).isEmptyRender()).to.equal(true);
    });

    describe('before site rename status has been fetched,', () => {
      it('should render title and description into PageLayout', () => {
        const wrapper = shallowRender({ siteRenameStatus: createData({ loading: true }) });
        const pageLayout = wrapper.find(PageLayout);

        const title = pageLayout.props().title as React.ReactElement<any>;
        const description = pageLayout.props().description as React.ReactElement<any>;
        expect(title.props).to.include(messages.title);
        expect(description.props).to.include(messages.description);
      });

      it('should render a spinner inside the page layout', () => {
        const wrapper = shallowRender({ siteRenameStatus: createData({ loading: true }) });
        expect(wrapper.find(PageLayout).find(AkSpinner).exists()).to.equal(true);
      });
    });

    describe('after site rename status has been fetched,', () => {
      it('should render title and description into PageLayout', () => {
        const wrapper = shallowRender();
        const pageLayout = wrapper.find(PageLayout);

        const title = pageLayout.props().title as React.ReactElement<any>;
        const description = pageLayout.props().description as React.ReactElement<any>;
        expect(title.props).to.include(messages.title);
        expect(description.props).to.include(messages.description);
      });

      it('should render an error message inside the page layout if the fetch fails', () => {
        const wrapper = shallowRender({ siteRenameStatus: { error: true } });
        expect(wrapper.find(PageLayout).find(GenericError).exists()).to.equal(true);
      });

      describe('rendering a warning message when rename limit has been reached,', () => {
        it('should render when limit is reached at page load', () => {
          const wrapper = shallowRender({ siteRenameStatus: createData({ renamesUsed: 13, renameLimit: 13 }) });
          expect(wrapper.find(PageLayout).find(AkSectionMessage).exists()).to.equal(true);
        });

        it('should not render when limit is reached at page load', () => {
          const wrapper = shallowRender({ siteRenameStatus: createData({ renamesUsed: 0, renameLimit: 99 }) });
          expect(wrapper.find(PageLayout).find(AkSectionMessage).exists()).to.equal(false);
        });

        it('should be true if limit is reached asynchronously at name validation', () => {
          const wrapper = shallowRender({ siteRenameStatus: createData({ renamesUsed: 0, renameLimit: 99 }) });
          expect(wrapper.find(PageLayout).find(AkSectionMessage).exists()).to.equal(false);
          wrapper
            .setState({
              nameChecks: {
                ...(wrapper.state() as any).nameChecks,
                renameLimitReached: true,
              },
            })
            .update();
          expect(wrapper.find(PageLayout).find(AkSectionMessage).exists()).to.equal(true);
        });
      });

      describe('SiteUrlForm props,', () => {
        describe('isValidName,', () => {
          it('should set isValidName=true initially', () => {
            const wrapper = shallowRender();
            expect(wrapper.find(SiteUrlForm).props().isValidName).to.equal(true);
          });

          it('should set isValidName=true if state.nameChecks.result is true and a validation request is not pending', () => {
            const wrapper = shallowRender();

            wrapper.setState({
              nameChecks: { result: false },
              isValidationPending: true,
            }).update();
            expect(
              wrapper.find(SiteUrlForm).props().isValidName,
              'Previous name invalid, validation pending => isValidName={false}',
            ).to.equal(false);

            wrapper.setState({
              nameChecks: { result: false },
              isValidationPending: false,
            }).update();
            expect(
              wrapper.find(SiteUrlForm).props().isValidName,
              'Previous name invalid, validation not pending => isValidName={false}',
            ).to.equal(false);

            wrapper.setState({
              nameChecks: { result: true },
              isValidationPending: true,
            }).update();
            expect(
              wrapper.find(SiteUrlForm).props().isValidName,
              'Previous name valid, validation pending => isValidName={false}',
            ).to.equal(false);

            wrapper.setState({
              nameChecks: { result: true },
              isValidationPending: false,
            }).update();
            expect(
              wrapper.find(SiteUrlForm).props().isValidName,
              'validation not pending, name valid => isValidName={true}',
            ).to.equal(true);
          });
        });

        describe('validationMessage,', () => {
          const findValidationMessage = (wrapper) => wrapper.find(SiteUrlForm).props().validationMessage;

          it('should be undefined if remote validation is pending', () => {
            const wrapper = shallowRender();
            wrapper.setState({ isValidationPending: true });

            expect(findValidationMessage(wrapper)).to.equal(undefined);
          });

          describe('Remote validation is not pending,', () => {
            it('should be undefined if nameChecks.result is true', () => {
              const wrapper = shallowRender();
              wrapper.setState({
                isValidationPending: false,
                nameChecks: {
                  result: true,
                },
              });

              expect(findValidationMessage(wrapper)).to.equal(undefined);
            });

            describe('nameChecks.result is false,', () => {
              it('should be a "name taken" message if taken AND not isAlreadyOwner', () => {
                const wrapper = shallowRender();
                wrapper.setState({
                  isValidationPending: false,
                  nameChecks: {
                    result: false,
                    taken: true,
                    isAlreadyOwner: false,
                  },
                });

                expect(findValidationMessage(wrapper).props).to.include(messages.validationNameTaken);
              });

              it('should be undefined if nameChecks.renameLimitReached is true', () => {
                const wrapper = shallowRender();
                wrapper.setState({
                  isValidationPending: false,
                  nameChecks: {
                    result: false,
                    renameLimitReached: true,
                  },
                });

                expect(findValidationMessage(wrapper)).to.equal(undefined);
              });

              it('should be a generic invalidation message for any other result=false combination', () => {
                const wrapper = shallowRender();
                wrapper.setState({
                  isValidationPending: false,
                  nameChecks: {
                    result: false,
                    taken: false,
                    isAlreadyOwner: false,
                  },
                });

                expect(findValidationMessage(wrapper).props).to.include(messages.validationCatchAll);
              });
            });
          });
        });
      });
    });
  });
});
