import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { sandbox as sinonSandbox } from 'sinon';

import { createMockIntlContext } from '../../utilities/testing';
import { UserManagementMigrationMounter } from './user-management-migration-mounter';

import { SiteFlags } from '../../schema/schema-types';

describe('um nav mounter tests', () => {

  let dispatchSpy;
  let registerRoutesSpy;
  let sandbox;

  beforeEach(() => {
    sandbox = sinonSandbox.create();
    dispatchSpy = sandbox.spy();
    registerRoutesSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  function createTestComponent(): ShallowWrapper {
    const UserManagementMigrationMounterWithoutApollo = UserManagementMigrationMounter.WrappedComponent;

    return shallow(
      <UserManagementMigrationMounterWithoutApollo dispatch={dispatchSpy} registerRoutes={registerRoutesSpy}/>,
      createMockIntlContext(),
    );
  }

  function expectSpiesNotCalled() {
    expect(dispatchSpy.notCalled).to.equal(true);
    expect(registerRoutesSpy.notCalled).to.equal(true);
  }

  it('should exit update if all pages are already registered', () => {
    const wrapper = createTestComponent();
    expectSpiesNotCalled();
    wrapper.setState({ registered: true });
    expectSpiesNotCalled();
  });

  it('should exit update if loading graphql data', () => {
    const wrapper = createTestComponent();
    expectSpiesNotCalled();
    wrapper.setState({ registered: true });
    expectSpiesNotCalled();
  });

  function expectNavigationAndRouteUpdates(
    flags: Partial<SiteFlags>,
    spies: { dispatchCalled?: boolean, registerRoutesCalled?: boolean} = {},
  ) {
    const data = { loading: false, currentSite: { id: 'some-id', flags } };
    const wrapper = createTestComponent();
    expectSpiesNotCalled();
    wrapper.setProps({ data });

    const { dispatchCalled = true, registerRoutesCalled = true } = spies;
    expect(dispatchSpy.called).to.equal(dispatchCalled);
    expect(registerRoutesSpy.called).to.equal(registerRoutesCalled);
  }

  it('when signup feature flag is enabled, it should update the navigation section', () => {
    expectNavigationAndRouteUpdates({ selfSignupADG3Migration: true });
  });

  it('when apps feature flag is enabled, it should update the navigation section', () => {
    expectNavigationAndRouteUpdates({ accessConfigADG3Migration: true });
  });

  it('when groups feature flag is enabled, it should update the navigation section', () => {
    expectNavigationAndRouteUpdates({ groupsADG3Migration: true });
  });

  it('when users feature flag is enabled, it should update the navigation section', () => {
    expectNavigationAndRouteUpdates({ usersADG3Migration: true });
  });

  it('when G Suite feature flag is enabled, it should update the navigation section', () => {
    expectNavigationAndRouteUpdates({ gSuiteADG3Migration: true });
  });

  it('when feature flag is not enabled or false, it should update the navigation section but not register routes', () => {
    const flags = { selfSignupADG3Migration: false, accessConfigADG3Migration: false };
    expectNavigationAndRouteUpdates(flags, { registerRoutesCalled: false });
  });

  it('when users accessRequests feature flag is disabled, it should update the navigation section but not register routes', () => {
    expectNavigationAndRouteUpdates({ accessRequestsPage: false }, { registerRoutesCalled: false });
  });

  it('when users accessRequests feature flag is enabled, it should update the navigation section and register routes', () => {
    expectNavigationAndRouteUpdates({ accessRequestsPage: true });
  });
});
