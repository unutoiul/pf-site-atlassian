import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import {
  DynamicTableStateless as AkDynamicTableStateless,
  HeaderRow as AkHeaderRow,
  Row as AkRow,
} from '@atlaskit/dynamic-table';
import AkLozenge from '@atlaskit/lozenge';

import { Account } from 'common/account';
import { Action, Actions } from 'common/actions';
import { AnalyticsClientProps, removeMemberLinkClickedEventData, withAnalyticsClient } from 'common/analytics';
import { createErrorIcon, createSuccessIcon } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';
import { getPaginationMessages } from 'common/table';

import {
  DeleteMemberMutation,
  DeleteMemberMutationVariables,
  GroupMemberListQuery,
} from '../../../schema/schema-types';
import { DeleteMemberDialog } from './delete-member-dialog';
import deleteMemberMutation from './delete-member.mutation.graphql';
import { optimisticallyRemoveUser } from './group-details.query.updater';

import groupMemberList from './group-member-list.query.graphql';

const messages = defineMessages({
  lozengeNoAccess: {
    id: 'um.groups.member.list.no.site.access.user',
    description: 'Lozenge text that displays when the user does not have access to site',
    defaultMessage: 'No site access',
  },
  headingMember: {
    id: 'um.groups.member.list.heading.member',
    defaultMessage: 'User',
  },
  headingOptions: {
    id: 'um.groups.member.list.heading.options',
    defaultMessage: 'Options',
  },
  memberRemoveSuccessTitle: {
    id: 'um.groups.member.list.member.remove.success.title',
    defaultMessage: 'User removed from group',
    description: 'Title which displays in the success flag popup.',
  },
  memberRemoveSuccessDescription: {
    id: 'um.groups.member.list.member.remove.success.description',
    defaultMessage: 'We’ve removed {userName} from the {groupName} group. A user can always be added back to a group.',
    description: 'Text which displays in the success flag popup as a description.',
  },
  memberRemoveErrorTitle: {
    id: 'um.groups.member.list.member.remove.error.title',
    defaultMessage: 'Uh oh! Something went wrong.',
    description: 'Title which displays in the error flag popup.',
  },
  memberRemoveErrorDescription: {
    id: 'um.groups.member.list.member.remove.error.description',
    defaultMessage: 'There was a problem removing the user from the group. Please try again in a few minutes.',
    description: 'Text which displays in the error flag popup as a description.',
  },
  memberRemoveDisabledReasonSiteAdmin: {
    id: 'um.groups.member.list.member.remove.disabled.reason.site.admin',
    defaultMessage: 'You cannot remove yourself from this group',
    description: 'Tooltip text that displays when a user tries to remove themselves from the site-admins group.',
  },
  memberRemoveDisabledReasonScimReadonly: {
    id: 'um.groups.member.list.member.remove.disabled.reason.scim.readonly',
    defaultMessage: `You can't remove this member because it is synced from an external directory`,
    description: 'Tooltip text that displays when a user tries to remove themselves from the site-admins group.',
  },
  memberRemoveDisabledReasonSiteAdminReadonly: {
    id: 'um.groups.member.list.member.remove.disabled.reason.site.admin.readonly',
    defaultMessage: `You can't remove this member because this site is linked to an organization`,
    description: `Tooltip text that displays when users can't remove users from site-admins group when it is linked.`,
  },
});

const GroupMemberListContainer = styled.div`
  table {
    table-layout: fixed;
  }
`;

const ActionsWrapper = styled.div`
  margin-left: -20px;
`;

interface User {
  id: string;
  displayName: string;
  email: string;
  active: boolean;
  system: boolean;
}

interface DerivedProps {
  currentUser?: GroupMemberListQuery['currentUser'];
}

interface OwnProps {
  users: User[];
  rowsPerPage: number;
  page: number;
  cloudId: string;
  groupId: string;
  groupName: string;
  memberCount: number;
  shouldShowSystemUsers: boolean;
  isGroupReadonly: boolean;
  onSetPage(page: number): void;
}

interface State {
  deleteMemberDialog: {
    isOpen: boolean;
    isMutationLoading: boolean;
    user?: User;
  };
}

export type GroupMemberListProps = OwnProps & InjectedIntlProps & FlagProps;

export class GroupMemberListImpl extends React.Component<ChildProps<GroupMemberListProps, DeleteMemberMutation> & DerivedProps & AnalyticsClientProps, State> {
  public readonly state: Readonly<State> = {
    deleteMemberDialog: {
      isOpen: false,
      isMutationLoading: false,
      user: undefined,
    },
  };

  public render() {
    const {
      users,
      onSetPage,
      page,
      rowsPerPage,
    } = this.props;

    return (
      <GroupMemberListContainer>
        <AkDynamicTableStateless
          head={this.getTableHeader()}
          isLoading={!users}
          rows={users && users.filter(user => !user || !user.system || this.props.shouldShowSystemUsers).map(this.formatUser)}
          rowsPerPage={rowsPerPage}
          onSetPage={onSetPage}
          page={page}
          paginationi18n={this.getPaginationMessages()}
        />
        <DeleteMemberDialog
          isOpen={this.state.deleteMemberDialog.isOpen}
          isRemovingUser={this.state.deleteMemberDialog.isMutationLoading}
          onDismiss={this.toggleDeleteMemberDialog}
          onRemove={this.onMemberRemove}
          user={this.state.deleteMemberDialog.user}
          groupName={this.props.groupName}
        />
      </GroupMemberListContainer>
    );
  }

  public onMemberRemove = () => {
    if (!this.props.mutate || !this.state.deleteMemberDialog.user) {
      return;
    }

    const removedMemberId = this.state.deleteMemberDialog.user && this.state.deleteMemberDialog.user.id;

    this.toggleDeleteMemberLoadingState();

    this.props.mutate({
      variables: {
        cloudId: this.props.cloudId,
        groupId: this.props.groupId,
        userId: removedMemberId,
      } as DeleteMemberMutationVariables,
      update: (store) => {
        optimisticallyRemoveUser(store, this.props.cloudId, this.props.groupId, removedMemberId);
      },
    }).then(() => {
      this.toggleDeleteMemberLoadingState();
      this.toggleDeleteMemberDialog();
      this.showSuccessFlag();
    }).catch(() => {
      this.toggleDeleteMemberLoadingState();
      this.toggleDeleteMemberDialog();
      this.showErrorFlag();
    });
  };

  public toggleDeleteMemberDialog = (options?: { user: User }) => {
    this.setState(prevState => ({
      deleteMemberDialog: {
        ...prevState.deleteMemberDialog,
        isOpen: !prevState.deleteMemberDialog.isOpen,
        user: options ? options.user : undefined,
      },
    }));
  };

  // tslint:disable-next-line:prefer-function-over-method
  private getTableHeader(): AkHeaderRow {
    const { formatMessage } = this.props.intl;

    return {
      cells: [
        {
          key: 'user',
          content: formatMessage(messages.headingMember),
          width: 80,
        },
        {
          key: 'options',
          content: formatMessage(messages.headingOptions),
          width: 20,
        },
      ],
    };
  }

  private formatUser = (user: User, index: number): AkRow => {
    if (!user) {
      return {
        key: `row-${index}`,
        cells: [
          {
            key: 'account',
            content: null,
          },
          {
            key: 'actions',
            content: null,
          },
        ],
      };
    }

    const onAction = () => {
      this.toggleDeleteMemberDialog({ user });

      this.props.analyticsClient.sendUIEvent({
        cloudId: this.props.cloudId,
        data: removeMemberLinkClickedEventData({
          groupId: this.props.groupId,
          memberCount: this.props.memberCount,
        }),
      });
    };

    return {
      key: `row-${index}`,
      cells: [
        {
          key: 'account',
          content: (
            <Account
              id={user.id}
              email={user.email}
              lozenges={!user.active ? [
                <AkLozenge key={user.id}>
                  {this.props.intl.formatMessage(messages.lozengeNoAccess)}
                </AkLozenge>,
              ] : []}
              isDisabled={!user.active}
              displayName={user.displayName}
            />
          ),
        },
        {
          key: 'actions',
          content: (
            <ActionsWrapper>
              <Actions>
                <Action
                  actionType="remove"
                  onAction={onAction}
                  isDisabled={!!this.getMemberRemoveDisabledMessage(user)}
                  disabledReason={this.getMemberRemoveDisabledMessage(user)}
                />
              </Actions>
            </ActionsWrapper>
          ),
        },
      ],
    };
  }

  private toggleDeleteMemberLoadingState = () => {
    this.setState(prevState => ({
      deleteMemberDialog: {
        ...prevState.deleteMemberDialog,
        isMutationLoading: !prevState.deleteMemberDialog.isMutationLoading,
      },
    }));
  };

  private showSuccessFlag = () => {
    this.props.showFlag({
      id: `member-remove-success-${Date.now()}`,
      autoDismiss: true,
      icon: createSuccessIcon(),
      title: this.props.intl.formatMessage(messages.memberRemoveSuccessTitle),
      description: this.props.intl.formatMessage(messages.memberRemoveSuccessDescription, {
        userName: this.state.deleteMemberDialog.user ? this.state.deleteMemberDialog.user.displayName : 'the user',
        groupName: this.props.groupName,
      }),
    });
  };

  private showErrorFlag = () => {
    this.props.showFlag({
      id: `member-remove-error-${Date.now()}`,
      autoDismiss: true,
      icon: createErrorIcon(),
      title: this.props.intl.formatMessage(messages.memberRemoveErrorTitle),
      description: this.props.intl.formatMessage(messages.memberRemoveErrorDescription),
    });
  };

  private getPaginationMessages = () => {
    return getPaginationMessages(this.props.intl.formatMessage.bind(this.props.intl));
  }

  private getMemberRemoveDisabledMessage = (user: User): FormattedMessage.MessageDescriptor | undefined => {
    const currentUserId = this.props.currentUser && this.props.currentUser.id;

    if (this.props.groupName.toLowerCase() === 'site-admins' && currentUserId === user.id) {
      return messages.memberRemoveDisabledReasonSiteAdmin;
    }

    if (this.props.isGroupReadonly) {
      if (this.props.groupName.toLowerCase() === 'site-admins') {
        return messages.memberRemoveDisabledReasonSiteAdminReadonly;
      }

      return messages.memberRemoveDisabledReasonScimReadonly;
    }

    return undefined;
  };
}

const withDeleteMemberMutation = graphql<GroupMemberListProps, DeleteMemberMutation>(deleteMemberMutation);
const withCurrentUserData = graphql<GroupMemberListProps, GroupMemberListQuery, {}, DerivedProps>(groupMemberList, {
  props: ({ data }): DerivedProps => ({
    currentUser: data && data.currentUser,
  }),
});

export const GroupMemberList = withFlag(
  injectIntl(
    withDeleteMemberMutation(
      withCurrentUserData(
        withAnalyticsClient<GroupMemberListProps>(
          GroupMemberListImpl,
        ),
      ),
    ),
  ),
);
