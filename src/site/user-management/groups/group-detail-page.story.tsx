import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider, ChildProps } from 'react-apollo';
import { IntlProvider } from 'react-intl';
import { MemoryRouter } from 'react-router';

import AkPage from '@atlaskit/page';

import { FlagProvider } from 'common/flag';

import {
  groupDetailResponse,
  groupMembersResponse,
} from '../../../../mock/services/um-service/responses';
import { createApolloClient } from '../../../apollo-client';
import { GroupDetailsQuery, GroupsPageQuery } from '../../../schema/schema-types';
import { createMockAnalyticsClient, createMockIntlProp } from '../../../utilities/testing';
import { GroupDetailPageImpl as StatelessGroupDetailPage } from './group-detail-page';

const defaults = {
  history: { replace: () => null } as any,
  match: { params: { cloudId: 'DUMMY_ORG_ID', groupId: 'fake' } } as any,
  location: window.location as any,
  showFlag: () => null,
  hideFlag: () => null,
  dispatch: (_: any): any => null,
  remove: (_: any): any => null,
  update: () => null as any,
  intl: createMockIntlProp(),
};

const mockData: ChildProps<any, GroupDetailsQuery> = {
  loading: false,
  group: {
    groupDetails: groupDetailResponse,
    members: groupMembersResponse,
  },
};

const mockDataScim: ChildProps<any, GroupDetailsQuery> = {
  loading: false,
  group: {
    groupDetails: {
      ...groupDetailResponse,
      name: 'Really fancy SCIM group',
      managementAccess: 'READ_ONLY',
      ownerType: 'EXT_SCIM',
    },
    members: groupMembersResponse,
  },
};

const mockDataNoMembers: ChildProps<any, GroupDetailsQuery> = {
  loading: false,
  group: {
    groupDetails: groupDetailResponse,
    members: {
      total: 0,
      users: [],
    },
  },
};

const loadingData: ChildProps<any, GroupDetailsQuery> = {
  loading: true,
};

const errorData: ChildProps<any, GroupDetailsQuery> = {
  loading: false,
  error: true,
};

const currentAdminUser: ChildProps<any, GroupsPageQuery> = {
  currentUser: {
    isSystemAdmin: false,
  },
};

const currentSysadminUser: ChildProps<any, GroupsPageQuery> = {
  currentUser: {
    isSystemAdmin: true,
  },
};

const client = createApolloClient();

const Wrapper = ({ children }) => (
  <ApolloProvider client={client}>
    <IntlProvider locale="en">
      <MemoryRouter>
        <FlagProvider>
          <AkPage>
            {children}
          </AkPage>
        </FlagProvider>
      </MemoryRouter>
    </IntlProvider>
  </ApolloProvider>
);

storiesOf('Site|Group Detail Page', module)
  .add('While loading', () => (
    <Wrapper>
      <StatelessGroupDetailPage data={loadingData} {...defaults} analyticsClient={createMockAnalyticsClient()} />
    </Wrapper>
  ))
  .add('With an error', () => (
    <Wrapper>
      <StatelessGroupDetailPage data={errorData} {...defaults} analyticsClient={createMockAnalyticsClient()} />
    </Wrapper>
  ))
  .add('With no members', () => (
    <Wrapper>
      <StatelessGroupDetailPage data={mockDataNoMembers} {...defaults} analyticsClient={createMockAnalyticsClient()} />
    </Wrapper>
  ))
  .add('With members', () => (
    <Wrapper>
      <StatelessGroupDetailPage currentUser={currentAdminUser} data={mockData} {...defaults} analyticsClient={createMockAnalyticsClient()} />
    </Wrapper>
  ))
  .add('With members and no access', () => (
    <Wrapper>
      <StatelessGroupDetailPage
        data={{
          ...mockData,
          group: {
            ...mockData.group,
            groupDetails: {
              ...mockData.group.groupDetails,
              productPermissions: [],
              defaultForProducts: [],
            },
          },
        }}
        {...defaults}
        analyticsClient={createMockAnalyticsClient()}
      />
    </Wrapper>
  ))
  .add('Unmodifiable group - But admin is a sysadmin', () => (
    <Wrapper>
      <StatelessGroupDetailPage
        currentUser={currentSysadminUser}
        data={mockData}
        {...defaults}
        analyticsClient={createMockAnalyticsClient()}
      />
    </Wrapper>
  ))
  .add('SCIM group - with members', () => (
    <Wrapper>
      <StatelessGroupDetailPage currentUser={currentAdminUser} data={mockDataScim} {...defaults} analyticsClient={createMockAnalyticsClient()} />
    </Wrapper>
  ));
