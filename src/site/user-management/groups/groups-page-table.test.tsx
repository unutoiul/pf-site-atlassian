import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import { MemoryRouter } from 'react-router';

import {
  DynamicTableStateless as AkDynamicTableStateless,
} from '@atlaskit/dynamic-table';

import AkCheckCircleIcon from '@atlaskit/icon/glyph/check-circle';
import AkLozenge from '@atlaskit/lozenge';

import { TextReveal } from 'common/text-reveal';

import { createMockAnalyticsClient, createMockContext, createMockIntlProp } from '../../../utilities/testing';
import { Group } from './groups-page';
import { GroupsPageTableImpl, GroupsPageTableProps } from './groups-page-table';

describe('GroupsPageTable', () => {
  const getTableWrapper = (props: Partial<GroupsPageTableProps> = {}) => {
    const {
      groups = [],
      rowsPerPage = 10,
      page = 1,
      isLoading = false,
      onSetPage = () => null,
      shouldShowSystemGroups = false,
      searchValuePresent = false,
    } = props;

    return mount((
      <MemoryRouter>
        <GroupsPageTableImpl
          groups={groups}
          rowsPerPage={rowsPerPage}
          page={page}
          onSetPage={onSetPage}
          isLoading={isLoading}
          shouldShowSystemGroups={shouldShowSystemGroups}
          searchValuePresent={searchValuePresent}
          intl={createMockIntlProp()}
          match={{ params: { cloudId: 'FAKE' } } as any}
          location={{ search: '' } as any}
          history={{} as any}
          analyticsClient={createMockAnalyticsClient()}
        />
      </MemoryRouter>
    ), createMockContext({ intl: true, apollo: true }));
  };

  const getFirstRow = (tableWrapper) => tableWrapper.find('Row');

  const generateMockGroup = (props: Partial<Group> = {}): Group => {
    const {
      defaultForProducts = [],
      productPermissions = [],
      name = 'Foo',
      unmodifiable = false,
      sitePrivilege = 'NONE',
      managementAccess = 'ALL',
      ownerType = null,
    } = props;

    return {
      id: 'Foo',
      name,
      description: 'What a nice description.',
      productPermissions,
      defaultForProducts,
      sitePrivilege,
      unmodifiable,
      userTotal: 99,
      managementAccess,
      ownerType,
    };
  };

  it('should render a valid table with real groups', () => {
    const wrapper = getTableWrapper({
      groups: [
        generateMockGroup(),
        generateMockGroup(),
      ],
    });

    expect(wrapper.find('Row')).to.have.lengthOf(2);
  });

  it('should correctly pass the isLoading prop', () => {
    const wrapper = getTableWrapper({ isLoading: true });
    expect(wrapper.find(AkDynamicTableStateless).prop('isLoading')).to.equal(true);
  });

  describe('Displaying system groups logic', () => {
    it('Should not show system groups', () => {
      const groupOne = generateMockGroup({ sitePrivilege: 'SITE_ADMIN' });
      const groupTwo = generateMockGroup({ sitePrivilege: 'SITE_ADMIN' });
      const groupThree = generateMockGroup({ sitePrivilege: 'SITE_ADMIN' });
      const systemGroupOne = generateMockGroup({ sitePrivilege: 'SYS_ADMIN' });
      const systemGroupTwo = generateMockGroup({ sitePrivilege: 'NONE', unmodifiable: true });
      const wrapper = getTableWrapper({
        shouldShowSystemGroups: false,
        groups: [groupOne, groupTwo, groupThree, systemGroupOne, systemGroupTwo],
      });
      expect(wrapper.find('Row').length).to.equal(3);
    });

    it('Should show system groups', () => {
      const groupOne = generateMockGroup({ sitePrivilege: 'SITE_ADMIN' });
      const groupTwo = generateMockGroup({ sitePrivilege: 'SITE_ADMIN' });
      const groupThree = generateMockGroup({ sitePrivilege: 'SITE_ADMIN' });
      const systemGroupOne = generateMockGroup({ sitePrivilege: 'SYS_ADMIN' });
      const systemGroupTwo = generateMockGroup({ sitePrivilege: 'NONE', unmodifiable: true });
      const wrapper = getTableWrapper({
        shouldShowSystemGroups: true,
        groups: [groupOne, groupTwo, groupThree, systemGroupOne, systemGroupTwo],
      });
      expect(wrapper.find('Row').length).to.equal(5);
    });
  });

  it(`should render SCIM groups regardless of showShowSystemGroups`, () => {
    const scimGroup = generateMockGroup({
      sitePrivilege: 'NONE',
      unmodifiable: false,
      managementAccess: 'READ_ONLY',
      ownerType: 'EXT_SCIM',
    });

    const systemGroup = generateMockGroup({ sitePrivilege: 'SYS_ADMIN' });

    const showingSystemGroupsWrapper = getTableWrapper({
      shouldShowSystemGroups: true,
      groups: [scimGroup, systemGroup],
    });

    const notShowingSystemGroupsWrapper = getTableWrapper({
      shouldShowSystemGroups: false,
      groups: [scimGroup, systemGroup],
    });

    expect(showingSystemGroupsWrapper.find('Row').length).to.equal(2);
    expect(notShowingSystemGroupsWrapper.find('Row').length).to.equal(1);
  });

  describe('group name cell', () => {
    it('should render the group name', () => {
      const group = generateMockGroup();
      const wrapper = getTableWrapper({
        groups: [group],
      });
      const row = getFirstRow(wrapper);

      expect(row.text()).to.include(group.name);
    });

    it('should render the group description', () => {
      const group = generateMockGroup();
      const wrapper = getTableWrapper({
        groups: [group],
      });
      const row = getFirstRow(wrapper);

      expect(row.find(TextReveal).text()).to.include(group.description!);
    });

    it('should render the default access group lozenge if the group has default products', () => {
      const group = generateMockGroup({
        defaultForProducts: [{
          productName: 'Confluence',
          productId: 'confluence.ondemand',
        }],
      });

      const wrapper = getTableWrapper({
        groups: [group],
      });

      const row = getFirstRow(wrapper);

      expect(row.find(AkLozenge)).to.have.lengthOf(1);
    });

    it(`shouldn't render the default access group lozenge if the group doesn't have default products`, () => {
      const wrapper = getTableWrapper({
        groups: [generateMockGroup()],
      });

      const row = getFirstRow(wrapper);

      expect(row.find(AkLozenge)).to.have.lengthOf(0);
    });
  });

  describe('accessToProduct cell', () => {
    it('should render a checkmark if the group has WRITE or MANAGE or both access', () => {
      const group = generateMockGroup({
        productPermissions: [
          {
            productName: 'Confluence',
            productId: 'confluence.ondemand',
            permissions: [
              'WRITE',
              'MANAGE',
            ],
          },
        ],
      });

      const wrapper = getTableWrapper({
        groups: [group],
      });

      const row = getFirstRow(wrapper);
      expect(row.find(AkCheckCircleIcon)).to.have.lengthOf(2);
    });

    it(`shouldn't render a checkmark if the group has no access`, () => {
      const group = generateMockGroup({
        productPermissions: [],
      });

      const wrapper = getTableWrapper({
        groups: [group],
      });

      const row = getFirstRow(wrapper);
      expect(row.find(AkCheckCircleIcon)).to.have.lengthOf(0);
    });

    it('should render a checkmark for jira-administrators group if it has WRITE and MANAGE permissions', () => {
      const group = generateMockGroup({
        name: 'jira-administrators',
        productPermissions: [
          {
            productName: 'Jira Core',
            productId: 'jira-core.ondemand',
            permissions: [
              'WRITE',
              'MANAGE',
            ],
          },
        ],
      });

      const wrapper = getTableWrapper({
        groups: [group],
      });

      const row = getFirstRow(wrapper);
      expect(row.find(AkCheckCircleIcon)).to.have.lengthOf(2);
    });

    it(`shouldn't render a checkmark for jira-administrators group if it has WRITE or MANAGE permissions only`, () => {
      const writeGroup = generateMockGroup({
        name: 'jira-administrators',
        productPermissions: [
          {
            productName: 'Jira Core',
            productId: 'jira-core.ondemand',
            permissions: [
              'WRITE',
            ],
          },
        ],
      });

      const manageGroup = generateMockGroup({
        name: 'jira-administrators',
        productPermissions: [
          {
            productName: 'Jira Core',
            productId: 'jira-core.ondemand',
            permissions: [
              'MANAGE',
            ],
          },
        ],
      });

      let wrapper = getTableWrapper({
        groups: [writeGroup],
      });

      let row = getFirstRow(wrapper);
      expect(row.find(AkCheckCircleIcon)).to.have.lengthOf(0);

      wrapper = getTableWrapper({
        groups: [manageGroup],
      });

      row = getFirstRow(wrapper);
      expect(row.find(AkCheckCircleIcon)).to.have.lengthOf(1);
    });
  });

  it('should render a valid productAdministration cell', () => {
    const group = generateMockGroup({
      productPermissions: [
        {
          productName: 'Confluence',
          productId: 'confluence.ondemand',
          permissions: [
            'MANAGE',
          ],
        },
      ],
    });

    const wrapper = getTableWrapper({
      groups: [group],
    });

    const row = getFirstRow(wrapper);
    expect(row.find(AkCheckCircleIcon)).to.have.lengthOf(2);
  });
});
