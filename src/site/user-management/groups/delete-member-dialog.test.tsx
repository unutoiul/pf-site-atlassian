import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import * as sinon from 'sinon';

import AkButton from '@atlaskit/button';

import { createApolloClient } from '../../../apollo-client';

import { createMockIntlContext, createMockIntlProp } from '../../../utilities/testing';
import { DeleteMemberDialogImpl, DeleteMemberDialogProps } from './delete-member-dialog';

const apolloClient = createApolloClient();

const mountDeleteMemberDialog = (props: DeleteMemberDialogProps) => {
  return mount<DeleteMemberDialogProps, never>((
    <ApolloProvider client={apolloClient}>
      <DeleteMemberDialogImpl
        intl={createMockIntlProp()}
        {...props}
      />
    </ApolloProvider>
  ), createMockIntlContext());
};

const mockUser = {
  id: 'c302b2a3-340e-4e5b-bb01-6dda6495e596',
  displayName: 'Corgi',
  email: 'test@atlassian.com',
  active: true,
  admin: false,
};

describe('DeleteMemberDialog', () => {
  const sandbox = sinon.sandbox.create();
  let onDismissSpy: sinon.SinonSpy;
  let onRemoveSpy: sinon.SinonSpy;

  beforeEach(() => {
    onDismissSpy = sandbox.spy();
    onRemoveSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('should render an action button which calls the onRemove prop', () => {
    const wrapper = mountDeleteMemberDialog({
      isOpen: true,
      isRemovingUser: false,
      groupName: 'Group',
      user: mockUser,
      onDismiss: onDismissSpy,
      onRemove: onRemoveSpy,
    });

    const removeButton = wrapper.find(AkButton).first();
    expect(removeButton.props().id).to.equal('confirm-removing-member');
    expect(onRemoveSpy.callCount).to.equal(0);
    removeButton.simulate('click');
    expect(onRemoveSpy.callCount).to.equal(1);
  });

  it('should render a cancel button which calls the onDismiss prop', () => {
    const wrapper = mountDeleteMemberDialog({
      isOpen: true,
      isRemovingUser: false,
      groupName: 'Group',
      user: mockUser,
      onDismiss: onDismissSpy,
      onRemove: onRemoveSpy,
    });

    const cancelButton = wrapper.find(AkButton).last();

    expect(onDismissSpy.callCount).to.equal(0);
    cancelButton.simulate('click');
    expect(onDismissSpy.callCount).to.equal(1);
  });

  it('should render a loading button when in a loading state', () => {
    const wrapper = mountDeleteMemberDialog({
      isOpen: true,
      isRemovingUser: true,
      groupName: 'Group',
      user: mockUser,
      onDismiss: onDismissSpy,
      onRemove: onRemoveSpy,
    });

    const removeButton = wrapper.find(AkButton).first();
    expect(removeButton.prop('isLoading')).to.equal(true);
  });
});
