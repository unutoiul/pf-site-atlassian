import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import { Location } from 'history';
import * as React from 'react';
import { MemoryRouter } from 'react-router-dom';
import { sandbox as sinonSandbox } from 'sinon';

import AkSpinner from '@atlaskit/spinner';

import AkButton from '@atlaskit/button';

import { PageLayout } from 'common/page-layout';

import { GroupsPageQuery } from '../../../schema/schema-types';
import { createMockIntlContext, createMockIntlProp, waitUntil } from '../../../utilities/testing';
import { GroupsPageImpl, messages } from './groups-page';
import { GroupsPageTable } from './groups-page-table';

describe('GroupsPage', () => {
  const sandbox = sinonSandbox.create();
  let refetchSpy: sinon.SinonStub;
  let replaceSpy: sinon.SinonSpy;
  let refetchIfFlagProvidedSpy: sinon.SinonSpy;

  const validData: GroupsPageQuery = {
    groupList: {
      __typename: 'PaginatedGroups',
      total: 1,
      groups: [
        {
          __typename: 'Group',
          id: 'Foo',
          name: 'Foo Group',
          description: 'Foo description',
          productPermissions: [],
          defaultForProducts: [],
          sitePrivilege: 'NONE',
          unmodifiable: false,
          userTotal: 10,
          managementAccess: 'ALL',
          ownerType: null,
        },
      ],
    },
    groupListWithoutFilter: {
      __typename: 'PaginatedGroups',
      total: 1,
    },
  };

  interface WrapperProps {
    data?: any;
    currentUser?: any;
    location?: Location;
  }

  const getPageWrapper = ({ data = {}, location = window.location as any, currentUser = {} }: WrapperProps) => {
    return shallow((
      <GroupsPageImpl
        data={{
          refetch: refetchSpy,
          ...data,
        }}
        currentUser={currentUser}
        location={location}
        match={{ params: { cloudId: 'FAKE' } }}
        intl={createMockIntlProp()}
        history={{ replace: replaceSpy }}
        {...{} as any}
      />
    ), createMockIntlContext());
  };

  beforeEach(() => {
    refetchSpy = sandbox.stub().resolves({});
    replaceSpy = sandbox.spy();
    refetchIfFlagProvidedSpy = sandbox.spy(GroupsPageImpl.prototype, 'refetchIfFlagProvided');
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('should render an error if there is an error', () => {
    const wrapper = getPageWrapper({
      data: {
        error: true,
      },
    });

    expect(wrapper.find('span').text()).to.equal(messages.error.defaultMessage);
  });

  it('should render a spinner if the page is loading', () => {
    const wrapper = getPageWrapper({
      data: {
        loading: true,
      },
    });

    expect(wrapper.find(AkSpinner)).to.have.lengthOf(1);
  });

  it('should show a "Create group" button', () => {
    const wrapper = getPageWrapper({ data: validData });

    const propWrapper = mount((
      <MemoryRouter>
        {wrapper.find(PageLayout).prop('action')}
      </MemoryRouter>
    ));

    expect(propWrapper.find(AkButton).props()).to.include({
      appearance: 'primary',
      id: 'group-creation-button',
    });
  });

  it('should show the groups table', () => {
    const wrapper = getPageWrapper({ data: validData });
    expect(wrapper.find(GroupsPageTable)).to.have.lengthOf(1);
  });

  describe('Should or shouldn\'t show system groups logic', () => {

    it('Shouldn\'t show system groups if currentUser query does not exist', () => {
      const wrapper = getPageWrapper({ data: validData });
      const table = wrapper.find(GroupsPageTable);
      expect(table).to.have.lengthOf(1);
      expect(table.props().shouldShowSystemGroups).to.equal(false);
    });

    it('Shouldn\'t show system groups if currentUser query has error', () => {
      const wrapper = getPageWrapper({ data: validData, currentUser: { error: true } });
      const table = wrapper.find(GroupsPageTable);
      expect(table).to.have.lengthOf(1);
      expect(table.props().shouldShowSystemGroups).to.equal(false);
    });

    it('Shouldn\'t show system groups if currentUser does not have system admin field ', () => {
      const wrapper = getPageWrapper({ data: validData, currentUser: { currentUser: { id: 'something' } } });
      const table = wrapper.find(GroupsPageTable);
      expect(table).to.have.lengthOf(1);
      expect(table.props().shouldShowSystemGroups).to.equal(false);
    });

    it('Shouldn\'t show system groups if currentUser is not a system user', () => {
      const wrapper = getPageWrapper({ data: validData, currentUser: { currentUser: { system: false } } });
      const table = wrapper.find(GroupsPageTable);
      expect(table).to.have.lengthOf(1);
      expect(table.props().shouldShowSystemGroups).to.equal(false);
    });

    it('Should show system groups if currentUser is not a system user', () => {
      const wrapper = getPageWrapper({ data: validData, currentUser: { currentUser: { isSystemAdmin: true } } });
      const table = wrapper.find(GroupsPageTable);
      expect(table).to.have.lengthOf(1);
      expect(table.props().shouldShowSystemGroups).to.equal(true);
    });

  });

  it('should filter on the displayName', async () => {
    const stubbedLocation: Location = {
      hash: '',
      pathname: '/foo/bar',
      search: '?foo=bar',
      state: undefined,
    };

    const wrapper = getPageWrapper({
      data: {
        groupList: validData.groupList,
        loading: false,
      },
      location: stubbedLocation,
    }).instance() as GroupsPageImpl;

    wrapper.onSearch({
      persist: () => ({}),
      target: { value: 'hello' },
    } as any);

    await waitUntil(() => refetchSpy.called, 2000);
    expect(refetchSpy.args[0][0].displayName).to.deep.equal('hello');
    expect(replaceSpy.getCall(0).args[0]).to.equal(`/foo/bar?displayName=hello&foo=bar`);
  });

  it('should call refetchIfFlagProvided on init when no group list has been fetched', () => {
    getPageWrapper({ data: { loading: true } });
    expect(refetchIfFlagProvidedSpy.called).to.equal(true);
  });

  it('should call refetchIfFlagProvided on init after group list has been fetched', () => {
    getPageWrapper({ data: validData });
    expect(refetchIfFlagProvidedSpy.called).to.equal(true);
  });

  describe('refetchIfFlagProvided', () => {
    it('only calls refetch when there is existing groups data', async () => {
      const wrapper = getPageWrapper({
        data: { loading: true },
        location: {
          hash: '',
          pathname: '/foo',
          search: '?bar=baz',
          state: { refreshGroupList: true },
        },
      });

      expect(refetchSpy.called).to.equal(false);

      wrapper.setProps({
        data: {
          refetch: refetchSpy,
          ...validData,
        },
      });
      (wrapper.instance() as GroupsPageImpl).refetchIfFlagProvided();

      await waitUntil(() => refetchSpy.called, 2000);
      expect(refetchSpy.called).to.equal(true);
    });

    it('calls history.replace when refreshGroupList is true', () => {
      getPageWrapper({
        data: validData,
        location: {
          hash: '',
          pathname: '/foo',
          search: '?bar=baz',
          state: { refreshGroupList: true },
        },
      });
      expect(replaceSpy.calledWith('/foo?bar=baz', {})).to.equal(true);
    });
  });
});
