import * as deepEqual from 'deep-equal';
import { parse, stringify } from 'query-string';
import * as React from 'react';
import { ChildProps, graphql, MutationFunc, QueryResult } from 'react-apollo';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';
import styled from 'styled-components';

import AkButton, { ButtonGroup as AkButtonGroup } from '@atlaskit/button';
import AkInlineDialog from '@atlaskit/inline-dialog';
import { Grid as AkGrid, GridColumn as AkGridColumn } from '@atlaskit/page';
import AkSpinner from '@atlaskit/spinner';
import { gridSize as akGridSize } from '@atlaskit/theme';

import {
  addMembersButtonClickedEventData,
  analyticsClient,
  AnalyticsClientProps,
  deleteGroupButtonClickedEventData,
  editGroupAccessLinkClickedEventData,
  editGroupDescriptionCancelButtonEventData,
  editGroupDescriptionConfirmButtonEventData,
  groupDetailsScreenScreenEvent,
  ScreenEventSender,
  withAnalyticsClient,
} from 'common/analytics';
import { CountIndicator } from 'common/count-indicator';
import { createSuccessIcon } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';
import { GroupName } from 'common/group-name';
import { PageLayout } from 'common/page-layout';
import { PageParentLink } from 'common/page-parent-link/page-parent-link';
import { getStart, sanitizePage } from 'common/pagination';
import { makeComponentTimingHoc } from 'common/performance-metrics';
import { PageHeader as AkPageHeader } from 'common/responsive/PageHeader';

import {
  DeleteGroupMutation,
  DeleteGroupMutationVariables,
  GroupDetailsQuery,
  GroupDetailsQueryVariables,
  IsCurrentAdminSystemAdminQuery,
  IsCurrentAdminSystemAdminQueryVariables,
  UpdateGroupInput,
  UpdateGroupMutation,
  UpdateGroupMutationVariables,
} from '../../../schema/schema-types';

import { util } from '../../../utilities/admin-hub';
import { AddMembersDialog } from './add-members-dialog';
import currentUserQuery from './current-user.query.graphql';
import deleteGroupMutation from './delete-group.mutation.graphql';
import { GroupDeleteDialog } from './group-delete-dialog';
import { GroupDetailPageLocationState } from './group-detail-page.prop-types';
import { GroupDetailSidebar } from './group-detail-sidebar';
import getGroupDetails from './group-details.query.graphql';
import { GroupEditDialog } from './group-edit-dialog';
import { GroupMemberList } from './group-member-list';
import { ROWS_PER_PAGE } from './groups-constants';
import { GroupsPageLocationState } from './groups-page.prop-types';
import { optimisticallyRemoveGroup } from './groups-page.query.updater';
import updateGroupMutation from './update-group.mutation.graphql';

export const messages = defineMessages({
  error: {
    id: 'user-management.groups.group-detail.error',
    defaultMessage: `Group details could not be retrieved.`,
  },
  notFound: {
    id: 'user-management.groups.group-detail.not.found',
    defaultMessage: 'Group cannot be found',
  },
  membersTitle: {
    id: 'user-management.groups.group-detail.members.title',
    defaultMessage: 'Group members',
    description: 'Title of the section showing the total number of members in a group.',
  },
  noMembers: {
    id: 'user-management.groups.group-detail.no.members',
    defaultMessage: '{groupName} currently has no members. Add members above.',
  },
  buttonAddMembers: {
    id: 'user-management.groups.group-detail.button.add.members',
    defaultMessage: 'Add members',
  },
  buttonEditGroup: {
    id: 'user-management.groups.group-detail.button.edit.group',
    defaultMessage: 'Edit description',
  },
  buttonDeleteGroup: {
    id: 'user-management.groups.group-detail.button.delete.group',
    defaultMessage: 'Delete group',
  },
  updateSuccess: {
    id: 'user-management.groups.group-detail.update.success',
    defaultMessage: `Group '{name}' saved`,
  },
  deleteSuccess: {
    id: 'user-management.groups.group-detail.delete.success',
    defaultMessage: `Group '{name}' deleted`,
  },
  cantEditReasonSiteAdmin: {
    id: 'user-management.groups.group-detail.cant.edit.site-admin',
    defaultMessage: `You can't edit this group because it manages access to site administration.`,
  },
  cantEditReasonSysAdmin: {
    id: 'user-management.groups.group-detail.cant.edit.sys-admin',
    defaultMessage: `You can't edit this group because it is restricted to system administrators.`,
  },
  cantDeleteReasonSiteAdmin: {
    id: 'user-management.groups.group-detail.cant.delete.site-admin',
    defaultMessage: `You can't delete this group because it manages access to site administration.`,
  },
  cantEditReasonScim: {
    id: 'user-management.groups.group-detail.cant.edit.scim',
    defaultMessage: `You can't edit this group because it is synced from an external directory.`,
    description: `Tooltip message that displays over the "Edit description" button which explains why it's disabled by SCIM.`,
  },
  cantDeleteReasonSysAdmin: {
    id: 'user-management.groups.group-detail.cant.delete.sys-admin',
    defaultMessage: `You can't delete this group because it is restricted to system administrators.`,
  },
  cantDeleteReasonDefaultGroup: {
    id: 'user-management.groups.group-detail.cant.delete.default-group',
    defaultMessage: `You can't delete this group because it is a default access group.`,
  },
  cantDeleteReasonScim: {
    id: 'user-management.groups.group-detail.cant.delete.scim',
    defaultMessage: `You can't delete this group because it is synced from an external directory.`,
    description: `Tooltip message that displays over the "Delete group" button which explains why it's disabled by SCIM.`,
  },
  pageParentGroupsLink: {
    id: 'user-management.groups.group-detail.page.parent.groups.link',
    defaultMessage: `Groups`,
  },
  cantAddMembersReasonScim: {
    id: 'user-management.groups.group-detail.cant.add.members.scim',
    defaultMessage: `You can't add members to this group because it is synced from an external directory.`,
    description: `Tooltip message that displays over the "Add members" button which explains why it's disabled by SCIM.`,
  },
  cantAddMembersReasonSiteAdminReadonly: {
    id: 'user-management.groups.group-detail.cant.add.members.site.admin.readonly',
    defaultMessage: `You can't add members to this group because this site is linked to an organization.`,
    description: `Tooltip message that displays over the "Add members" button which explains why it's disabled for a site.`,
  },
  cantAddMembersSystem: {
    id: 'user-management.groups.group-detail.cant.add.members.sys.admin.readonly',
    defaultMessage: `You can't add members to this group because it is restricted to system administrators.`,
    description: `Tooltip message that displays over the "Add members" button which explains why it's disabled for a site.`,
  },
});

export const HeadingWithAction = styled.div`
margin: -${akGridSize() * 3}px 0 -${akGridSize()}px;
`;

export const DisabledActionContainer = styled.div`
  white-space: normal;

  /*
    Workaround for a Chrome bug where onMouseLeave isn't fired for disabled form elements.
    https://bugs.chromium.org/p/chromium/issues/detail?id=120132
    https://github.com/facebook/react/issues/4251)
  */
  button[disabled] {
    pointer-events: none;
  }
`;

interface SiteParams {
  cloudId: string;
  groupId: string;
}
interface CurrentUserProps {
  currentUser?: QueryResult & Partial<IsCurrentAdminSystemAdminQuery>;
}
interface GroupDeleteMutationProps {
  remove: MutationFunc<DeleteGroupMutation, DeleteGroupMutationVariables>;
  update: MutationFunc<UpdateGroupMutation, UpdateGroupMutationVariables>;
}
type GroupDetailPageProps = ChildProps<RouteComponentProps<SiteParams> & FlagProps & InjectedIntlProps & CurrentUserProps & AnalyticsClientProps, GroupDetailsQuery & UpdateGroupMutation> & GroupDeleteMutationProps;

type Member = GroupDetailsQuery['group']['members']['users'][0];
interface GroupDetailPageState {
  members: Member[] | null | undefined;
  isAddMembersDialogOpen: boolean;
  isDeleteDialogOpen: boolean;
  isEditDialogOpen: boolean;
  isDeleting: boolean;
  isSaving: boolean;
  isCantDeleteReasonShowing: boolean;
  isCantEditReasonShowing: boolean;
  isCantAddMembersReasonShowing: boolean;
}

export class GroupDetailPageImpl extends React.Component<GroupDetailPageProps, GroupDetailPageState> {
  constructor(props: GroupDetailPageProps) {
    super(props);

    const {
      history,
      location: { pathname, search, state },
    } = props;
    const { showAddMembersModal } = (state || {}) as GroupDetailPageLocationState;

    if (showAddMembersModal) {
      // Clear flag so that the dialog doesn't open on subsequent reloads or back/forward in navigation to this page
      history.replace(`${pathname}${search}`, {});
    }

    this.state = {
      members: props.data && props.data.group && props.data.group.members && props.data.group.members.users && this.getMembersPaddedWithPlaceholders(
        getStart(sanitizePage(parse(props.location.search).page), ROWS_PER_PAGE) - 1,
        props.data.group.members.total,
        props.data.group.members.users,
        props.data.loading,
      ),
      isAddMembersDialogOpen: showAddMembersModal,
      isDeleteDialogOpen: false,
      isEditDialogOpen: false,
      isDeleting: false,
      isSaving: false,
      isCantDeleteReasonShowing: false,
      isCantEditReasonShowing: false,
      isCantAddMembersReasonShowing: false,
    };
  }

  public componentWillReceiveProps(nextProps: GroupDetailPageImpl['props']) {
    if (!this.props.data || !nextProps.data) {
      return;
    }

    const currentMembers = this.props.data.group &&
      this.props.data.group.members &&
      this.props.data.group.members.users;
    const nextMembers = nextProps.data.group &&
      nextProps.data.group.members &&
      nextProps.data.group.members.users;

    if (this.props.data.loading === nextProps.data.loading && deepEqual(currentMembers, nextMembers)) {
      return;
    }
    if (nextProps.data.loading || nextProps.data.error || !nextProps.data.group || !nextProps.data.group.members) {
      return;
    }

    this.setState({
      members: this.getMembersPaddedWithPlaceholders(
        getStart(sanitizePage(parse(nextProps.location.search).page), ROWS_PER_PAGE) - 1,
        nextProps.data.group.members.total,
        nextProps.data.group.members.users || [],
        nextProps.data.loading,
      ),
    });
  }

  public onSetPage = async (page: number) => {
    const { data, location } = this.props;
    this.updateQueryParams({
      ...parse(location.search),
      page,
    });

    return data!.fetchMore({
      variables: {
        start: getStart(page, ROWS_PER_PAGE),
      },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        return {
          ...previousResult,
          ...fetchMoreResult,
        };
      },
    });
  };

  public render() {
    const {
      intl: { formatMessage },
      data: { loading = false, error = null } = {},
      location: { search },
      currentUser,
    } = this.props;

    const { page } = parse(search);
    const groupId = this.props.match.params.groupId;

    if (error || (currentUser && currentUser.error)) {
      return (<PageLayout><span>{formatMessage(messages.error)}</span></PageLayout>);
    } else if ((!this.state.members && loading) || (currentUser && currentUser.loading)) {
      return (<PageLayout><AkSpinner /></PageLayout>);
    } else if (!this.props.data || !this.props.data.group) {
      return (<PageLayout><span>{formatMessage(messages.notFound)}</span></PageLayout>);
    } else {
      const {
        groupDetails: { description, name, productPermissions, defaultForProducts, managementAccess, ownerType },
        members,
      } = this.props.data.group;

      const action = (
        <AkButtonGroup>
          {this.addMembersAction(members && members.total)}
          {this.editAction()}
          {this.deleteAction()}
        </AkButtonGroup>
      );

      return (
        <PageLayout
          title={<GroupName name={name} managementAccess={managementAccess} ownerType={ownerType} size="medium" />}
          isFullWidth={true}
          action={action}
          pageParent={
            <PageParentLink
              linkText={formatMessage(messages.pageParentGroupsLink)}
              linkLocation={this.props.location.pathname.split('/').slice(0, -1).join('/')}
            />
          }
          description={description && (
            <AkGrid>
              <AkGridColumn medium={7}>
                {description}
              </AkGridColumn>
            </AkGrid>
          )}
        >
          <ScreenEventSender
            isDataLoading={this.props.data.loading}
            event={groupDetailsScreenScreenEvent({
              groupId,
              memberCount: members.total,
            })}
          >
            <AkGrid>
              <AkGridColumn medium={7}>
                <HeadingWithAction>
                  <AkPageHeader
                    disableTitleStyles={true}
                  >
                    <CountIndicator
                      header={formatMessage(messages.membersTitle)}
                      count={(members && members.total) || 0}
                    />
                  </AkPageHeader>
                </HeadingWithAction>
                {members && members.total ? (
                  <GroupMemberList
                    users={this.state.members || []}
                    rowsPerPage={ROWS_PER_PAGE}
                    page={sanitizePage(page)}
                    cloudId={this.props.match.params.cloudId}
                    groupId={groupId}
                    memberCount={members.total}
                    groupName={name}
                    shouldShowSystemUsers={this.isSystemAdmin()}
                    onSetPage={this.onSetPage}
                    isGroupReadonly={this.isReadonly()}
                  />
                ) : (
                    <FormattedMessage
                      {...messages.noMembers}
                      values={{
                        groupName: name,
                      }}
                      tagName="p"
                    />
                  )}
              </AkGridColumn>
              <AkGridColumn medium="1" />
              <AkGridColumn medium="4">
                <GroupDetailSidebar
                  cantModifyReason={this.cantEditGroupAccessReason()}
                  productPermissions={productPermissions}
                  defaultForProducts={defaultForProducts}
                  onEditAccess={this.onEditAccess}
                />
              </AkGridColumn>
            </AkGrid>
            <GroupEditDialog
              groupId={groupId}
              memberCount={members.total}
              isOpen={this.state.isEditDialogOpen}
              dismiss={this.editDialogDismiss}
              update={this.onUpdate}
              description={description}
              name={name}
            />
            <GroupDeleteDialog
              groupId={groupId}
              memberCount={members.total}
              isLoading={this.state.isDeleting}
              isOpen={this.state.isDeleteDialogOpen}
              dismiss={this.deleteDialogDismiss}
              remove={this.onDelete}
              name={name}
            />
            <AddMembersDialog
              cloudId={this.props.match.params.cloudId}
              groupId={groupId}
              memberCount={members.total}
              isOpen={this.state.isAddMembersDialogOpen}
              dismiss={this.addMembersDialogDismiss}
              name={name}
            />
          </ScreenEventSender>
        </PageLayout>
      );
    }
  }

  public getMembersPaddedWithPlaceholders(start: number, total: number, members: Member[], isLoading: boolean) {
    const users = Array(total).fill(null);
    if (!isLoading) {
      users.splice(start, members.length, ...members);
    }

    return users;
  }

  public onDelete = () => {
    const {
      history,
      intl: { formatMessage },
      match: { params: { cloudId, groupId } },
      remove,
      showFlag,
    } = this.props;
    const { name } = this.props.data!.group!.groupDetails;

    this.setState({ isDeleting: true });

    remove({
      variables: {
        cloudId,
        id: groupId,
      },
      update: (store) => {
        optimisticallyRemoveGroup(store, cloudId, groupId);
      },
    }).then(() => {
      const groupsPageLocationState: GroupsPageLocationState = {
        refreshGroupList: false,
      };
      this.setState({
        isDeleteDialogOpen: false,
        isDeleting: false,
      });
      showFlag({
        autoDismiss: true,
        icon: createSuccessIcon(),
        id: `user-management.groups.group-detail.success.flag.${(Date.now().valueOf())}`,
        title: formatMessage(messages.deleteSuccess, {
          name,
        }),
      });
      history.push(`${util.siteAdminBasePath}/s/${cloudId}/groups`, groupsPageLocationState);
    }).catch(() => {
      this.setState({ isDeleting: false });
    });
  };

  public onUpdate = (input: UpdateGroupInput) => {
    const {
      intl: { formatMessage },
      match: { params: { cloudId, groupId } },
      update,
      showFlag,
    } = this.props;

    this.setState({
      isSaving: true,
      isEditDialogOpen: false,
    });

    // TODO react-apollo upgrade verify that this doesn't break anything
    update({
      variables: {
        cloudId,
        id: groupId,
        input,
      } as UpdateGroupMutationVariables,
      optimisticResponse: {
        updateGroup: {
          __typename: 'Group',
          id: groupId,
          name: input.name,
          description: input.description || '',
        },
      },
    }).then(({ data: { updateGroup: { name } } }) => {
      this.setState({ isSaving: false });
      showFlag({
        autoDismiss: true,
        icon: createSuccessIcon(),
        id: `user-management.groups.group-detail.success.flag.${(Date.now().valueOf())}`,
        title: formatMessage(messages.updateSuccess, {
          name,
        }),
      });
    }).catch(() => {
      if (this.props.data) {
        this.props.data.refetch().catch(error => {
          analyticsClient.onError(error);
        });
      }
      this.setState({ isSaving: false });
    });
  };

  private onEditAccess = () => {
    const { match: { params: { cloudId, groupId } } } = this.props;
    this.props.analyticsClient.sendUIEvent({
      cloudId,
      data: editGroupAccessLinkClickedEventData({
        groupId,
        memberCount: this.props.data!.group!.members.total,
      }),
    });
  }

  private editAction = (): React.ReactNode => {
    const { formatMessage } = this.props.intl;
    const cantEditReason = this.cantEditReason();

    if (!cantEditReason || this.isSystemAdmin()) {
      return <AkButton onClick={this.editDialogOpen}>{formatMessage(messages.buttonEditGroup)}</AkButton>;
    }

    return (
      <DisabledActionContainer onMouseEnter={this.cantEditReasonOpen} onMouseLeave={this.cantEditReasonClose}>
        <AkInlineDialog isOpen={this.state.isCantEditReasonShowing} content={formatMessage(cantEditReason)} placement="left">
          <AkButton isDisabled={true}>{formatMessage(messages.buttonEditGroup)}</AkButton>
        </AkInlineDialog>
      </DisabledActionContainer>
    );
  };

  private deleteAction = (): React.ReactNode => {
    const { formatMessage } = this.props.intl;
    const cantDeleteReason = this.cantDeleteReason();

    if (!cantDeleteReason || this.isSystemAdmin()) {
      return <AkButton onClick={this.deleteDialogOpen}>{formatMessage(messages.buttonDeleteGroup)}</AkButton>;
    }

    return (
      <DisabledActionContainer onMouseEnter={this.cantDeleteReasonOpen} onMouseLeave={this.cantDeleteReasonClose}>
        <AkInlineDialog isOpen={this.state.isCantDeleteReasonShowing} content={formatMessage(cantDeleteReason)} placement="left">
          <AkButton isDisabled={true}>{formatMessage(messages.buttonDeleteGroup)}</AkButton>
        </AkInlineDialog>
      </DisabledActionContainer>
    );
  };

  private addMembersAction = (membersTotal: number): React.ReactNode => {
    const { formatMessage } = this.props.intl;
    const reason = this.cantAddMembersReason();

    if (!reason) {
      return (
        <AkButton appearance={membersTotal ? 'default' : 'primary'} onClick={this.addMembersDialogOpen}>
          {formatMessage(messages.buttonAddMembers)}
        </AkButton>
      );
    }

    return (
      <DisabledActionContainer onMouseEnter={this.cantAddMembersReasonOpen} onMouseLeave={this.cantAddMembersReasonClose}>
        <AkInlineDialog isOpen={this.state.isCantAddMembersReasonShowing} content={formatMessage(reason)} placement="left">
          <AkButton isDisabled={true}>{formatMessage(messages.buttonAddMembers)}</AkButton>
        </AkInlineDialog>
      </DisabledActionContainer>
    );
  };

  private cantEditGroupAccessReason = (): FormattedMessage.MessageDescriptor | null => {
    const {
      defaultForProducts,
      sitePrivilege,
      unmodifiable,
    } = this.props.data!.group!.groupDetails;

    if (sitePrivilege === 'SITE_ADMIN') {
      return messages.cantDeleteReasonSiteAdmin;
    } else if (unmodifiable || sitePrivilege === 'SYS_ADMIN') {
      return messages.cantDeleteReasonSysAdmin;
    } else if (defaultForProducts.length) {
      return messages.cantDeleteReasonDefaultGroup;
    }

    return null;
  };

  private cantDeleteReason = (): FormattedMessage.MessageDescriptor | null => {
    const {
      defaultForProducts,
      sitePrivilege,
      unmodifiable,
      managementAccess,
      ownerType,
    } = this.props.data!.group!.groupDetails;

    if (managementAccess === 'READ_ONLY' && ownerType === 'EXT_SCIM') {
      return messages.cantDeleteReasonScim;
    } else if (sitePrivilege === 'SITE_ADMIN') {
      return messages.cantDeleteReasonSiteAdmin;
    } else if (unmodifiable || sitePrivilege === 'SYS_ADMIN') {
      return messages.cantDeleteReasonSysAdmin;
    } else if (defaultForProducts.length) {
      return messages.cantDeleteReasonDefaultGroup;
    }

    return null;
  };

  private cantEditReason = (): FormattedMessage.MessageDescriptor | null => {
    const {
      sitePrivilege,
      unmodifiable,
      managementAccess,
      ownerType,
    } = this.props.data!.group!.groupDetails;

    if (managementAccess === 'READ_ONLY' && ownerType === 'EXT_SCIM') {
      return messages.cantEditReasonScim;
    } else if (sitePrivilege === 'SITE_ADMIN') {
      return messages.cantEditReasonSiteAdmin;
    } else if (unmodifiable || sitePrivilege === 'SYS_ADMIN') {
      return messages.cantEditReasonSysAdmin;
    }

    return null;
  };

  private cantAddMembersReason = (): FormattedMessage.MessageDescriptor | null => {
    const {
      managementAccess,
      ownerType,
      unmodifiable,
      sitePrivilege,
    } = this.props.data!.group!.groupDetails;

    if (managementAccess === 'READ_ONLY' && ownerType === 'EXT_SCIM') {
      return messages.cantAddMembersReasonScim;
    }

    if (managementAccess === 'READ_ONLY' && ownerType === 'ORG') {
      return messages.cantAddMembersReasonSiteAdminReadonly;
    }

    if (unmodifiable || sitePrivilege === 'SYS_ADMIN') {
      return messages.cantAddMembersSystem;
    }

    return null;
  }

  private addMembersDialogDismiss = () =>
    this.setState({ isAddMembersDialogOpen: false });

  private addMembersDialogOpen = () => {
    const { match: { params: { cloudId, groupId } } } = this.props;
    this.setState({ isAddMembersDialogOpen: true });

    this.props.analyticsClient.sendUIEvent({
      cloudId,
      data: addMembersButtonClickedEventData({
        groupId,
        memberCount: this.props.data!.group!.members.total,
      }),
    });
  };

  private deleteDialogDismiss = () =>
    this.setState({ isDeleteDialogOpen: false });

  private deleteDialogOpen = () => {
    const { match: { params: { cloudId, groupId } } } = this.props;
    this.setState({ isDeleteDialogOpen: true });

    this.props.analyticsClient.sendUIEvent({
      cloudId,
      data: deleteGroupButtonClickedEventData({
        groupId,
        memberCount: this.props.data!.group!.members.total,
      }),
    });
  };

  private editDialogDismiss = () => {
    const { match: { params: { cloudId, groupId } } } = this.props;
    this.setState({ isEditDialogOpen: false });

    this.props.analyticsClient.sendUIEvent({
      cloudId, data: editGroupDescriptionCancelButtonEventData({
        groupId,
        memberCount: this.props.data!.group!.members.total,
      }),
    });
  };

  private editDialogOpen = () => {
    const { match: { params: { cloudId, groupId } } } = this.props;
    this.setState({ isEditDialogOpen: true });

    this.props.analyticsClient.sendUIEvent({
      cloudId, data: editGroupDescriptionConfirmButtonEventData({
        groupId,
        memberCount: this.props.data!.group!.members.total,
      }),
    });
  };

  private cantDeleteReasonOpen = () =>
    this.setState({ isCantDeleteReasonShowing: true });

  private cantDeleteReasonClose = () =>
    this.setState({ isCantDeleteReasonShowing: false });

  private cantEditReasonOpen = () =>
    this.setState({ isCantEditReasonShowing: true });

  private cantEditReasonClose = () =>
    this.setState({ isCantEditReasonShowing: false });

  private cantAddMembersReasonOpen = () => this.setState({ isCantAddMembersReasonShowing: true });

  private cantAddMembersReasonClose = () => this.setState({ isCantAddMembersReasonShowing: false });

  private updateQueryParams = (params: object) => {
    const { history, location } = this.props;
    history.replace(`${location.pathname}?${stringify(params)}`);
  };

  private isSystemAdmin = (): boolean => {
    const { currentUser } = this.props;

    return !!(currentUser && currentUser.currentUser && currentUser.currentUser.isSystemAdmin);
  };

  private isReadonly = (): boolean => {
    return !!(this.props.data &&
      this.props.data.group &&
      this.props.data.group.groupDetails.managementAccess === 'READ_ONLY');
  };
}

const withCurrentUser = graphql<RouteComponentProps<SiteParams>, IsCurrentAdminSystemAdminQuery, IsCurrentAdminSystemAdminQueryVariables>(currentUserQuery, {
  name: 'currentUser',
  options: ({ match: { params: { cloudId } } }) => ({
    variables: {
      cloudId,
    },
  }),
});

const withGroupDetailsQuery = graphql<RouteComponentProps<SiteParams>, GroupDetailsQuery, GroupDetailsQueryVariables>(getGroupDetails, {
  options: ({ location, match: { params: { cloudId, groupId } } }) => {
    const { page } = parse(location.search);

    return {
      variables: {
        cloudId,
        id: groupId,
        count: ROWS_PER_PAGE,
        start: getStart(sanitizePage(page), ROWS_PER_PAGE),
      },
    };
  },
});

const withUpdateGroupMutation = graphql<RouteComponentProps<SiteParams>, UpdateGroupMutation, UpdateGroupMutationVariables>(updateGroupMutation, {
  name: 'update',
});

const withDeleteGroupMutation = graphql<RouteComponentProps<SiteParams>, DeleteGroupMutation, DeleteGroupMutationVariables>(deleteGroupMutation, {
  name: 'remove',
});

const withComponentTiming = makeComponentTimingHoc<GroupDetailPageProps>({
  id: 'group-detail-page',
  category: 'page',
  isMeaningful: true,
  isLoaded: (props => props.data ? !props.data.loading : false),
});

export const GroupDetailPage = withRouter(
  withCurrentUser(
    withGroupDetailsQuery(
      withUpdateGroupMutation(
        withDeleteGroupMutation(
          withFlag<RouteComponentProps<SiteParams>>(
            withAnalyticsClient<GroupDetailPageProps>(
              injectIntl<GroupDetailPageProps>(
                withComponentTiming(GroupDetailPageImpl),
              ),
            ),
          ),
        ),
      ),
    ),
  ),
);
