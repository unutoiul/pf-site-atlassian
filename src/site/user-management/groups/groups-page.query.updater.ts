import { DataProxy } from 'apollo-cache';

import { getStart } from 'common/pagination';

import { ROWS_PER_PAGE } from './groups-constants';
import groupsPageQuery from './groups-page.query.graphql';

import { Group, GroupsPageQuery, GroupsPageQueryVariables } from '../../../schema/schema-types';

function readGroupsPageQuery(store: DataProxy, variables: GroupsPageQueryVariables) {
  try {
    return store.readQuery({
      query: groupsPageQuery,
      variables,
    }) as GroupsPageQuery;
  } catch (e) {
    // Due to https://github.com/apollographql/apollo-feature-requests/issues/1,
    // store.readQuery throws rather than returns null for this case.
    return undefined;
  }
}

export function optimisticallyAddGroup(store: DataProxy, cloudId: string, group: Group) {
  const variables: GroupsPageQueryVariables = {
    cloudId,
    count: ROWS_PER_PAGE,
    start: getStart(1, ROWS_PER_PAGE),
  };

  const groupsPageResult = readGroupsPageQuery(store, variables);

  if (!groupsPageResult) {
    return;
  }

  store.writeQuery({
    query: groupsPageQuery,
    variables,
    data: {
      groupList: {
        total: groupsPageResult.groupList.total + 1,
        groups: [
          { ...group, __typename: 'Group' },
          ...(groupsPageResult.groupList.groups || []),
        ],
        __typename: 'PaginatedGroups',
      },
      groupListWithoutFilter: {
        total: groupsPageResult.groupListWithoutFilter.total + 1,
        __typename: 'PaginatedGroups',
      },
    } as GroupsPageQuery,
  });
}

export function optimisticallyRemoveGroup(store: DataProxy, cloudId: string, groupId: string) {
  const variables: GroupsPageQueryVariables = {
    cloudId,
    start: getStart(1, ROWS_PER_PAGE),
    count: ROWS_PER_PAGE,
  };

  const groupsPageResult = readGroupsPageQuery(store, variables);

  if (!groupsPageResult) {
    return;
  }

  store.writeQuery({
    query: groupsPageQuery,
    variables,
    data: {
      groupList: {
        total: Math.max(0, groupsPageResult.groupList.total - 1),
        groups: (groupsPageResult.groupList.groups || []).filter(group => group.id !== groupId),
        __typename: 'PaginatedGroups',
      },
      groupListWithoutFilter: {
        total: Math.max(0, groupsPageResult.groupListWithoutFilter.total - 1),
        __typename: 'PaginatedGroups',
      },
    },
  });
}
