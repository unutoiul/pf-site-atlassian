// tslint:disable max-classes-per-file
import { assert, expect } from 'chai';
import { mount, shallow } from 'enzyme';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { sandbox as sinonSandbox } from 'sinon';

import { Checkbox as AkCheckbox } from '@atlaskit/checkbox';
import AkTextField from '@atlaskit/field-text';

import { Button } from 'common/analytics';
import { ModalDialog } from 'common/modal';

import { createMockAnalyticsClient, createMockContext, createMockIntlContext, createMockIntlProp } from '../../../utilities/testing';
import { GroupCreateDialogImpl, messages } from './group-create-dialog';

describe('GroupCreateDialog', () => {
  const cloudId = 'FAKE';
  const successfulMutation = Promise.resolve({ data: { createGroup: { id: 'foo', name: 'bar' } } });
  const failedWithDuplicateGroupError = new Error();
  (failedWithDuplicateGroupError as any).graphQLErrors = [{ originalError: { code: 'duplicateGroupName' } }];

  const sandbox = sinonSandbox.create();
  let dismissSpy: sinon.SinonSpy;
  let showFlagSpy: sinon.SinonSpy;
  let mutateMock;
  let historyPushSpy: sinon.SinonSpy;
  let mockAnalyticsClient;

  beforeEach(() => {
    dismissSpy = sandbox.spy();
    showFlagSpy = sandbox.spy();
    mutateMock = sandbox.stub().returns(successfulMutation);
    historyPushSpy = sandbox.spy();
    mockAnalyticsClient = createMockAnalyticsClient();
  });

  afterEach(() => {
    sandbox.restore();
  });

  function mountGroupCreateDialog() {
    return mount(
      <GroupCreateDialogImpl
        dismiss={dismissSpy}
        history={{ push: historyPushSpy }}
        intl={createMockIntlProp()}
        match={{ params: { cloudId } }}
        mutate={mutateMock}
        showFlag={showFlagSpy}
        analyticsClient={mockAnalyticsClient}
        isOpen={true}
        {...{} as any}
      />,
      createMockContext({ intl: true, apollo: true }),
    );
  }

  it('renders with title and description', () => {
    const modal = mountGroupCreateDialog().find(ModalDialog);
    expect(modal.prop('header')).to.equal(messages.title.defaultMessage);
    expect(modal.find(FormattedMessage).first().prop('defaultMessage')).to.equal(messages.description.defaultMessage);
  });

  it('renders with a disabled create button and a cancel buttons', () => {
    const modalFooterButtons = mountGroupCreateDialog().find(ModalDialog).find(Button);

    expect(modalFooterButtons).to.have.length(2);

    const createButton = modalFooterButtons.first();
    const createButtonProps = createButton.props();

    expect(createButton.text()).to.equal(messages.buttonCreate.defaultMessage);
    expect(createButtonProps.appearance).to.equal('primary');
    expect(createButtonProps.isDisabled).to.equal(true);
    expect(createButtonProps.id).to.equal('confirm-group-creation-button');

    const cancelButton = modalFooterButtons.last();
    const cancelButtonProps = cancelButton.props();

    expect(cancelButton.text()).to.equal(messages.buttonCancel.defaultMessage);
    expect(cancelButtonProps.appearance).to.equal('subtle-link');
  });

  it('enables the create button when a name is entered', () => {
    const wrapper = mountGroupCreateDialog();
    wrapper.setState({ name: 'test' });
    const createButton = wrapper.find(ModalDialog).find(Button).first();

    expect(createButton.prop('isDisabled')).to.equal(false);
  });

  it('disables the form fields and buttons while loading', () => {
    const wrapper = mountGroupCreateDialog();
    wrapper.setState({ isLoading: true });
    const modalFooterButtons = wrapper.find(ModalDialog).find(Button);
    const createButton = modalFooterButtons.first();
    const cancelButton = modalFooterButtons.last();

    expect(createButton.prop('isDisabled')).to.equal(true);
    expect(cancelButton.prop('isDisabled')).to.equal(true);
    wrapper.find(AkTextField).map(el => expect(el.prop('disabled')).to.equal(true));
    expect(wrapper.find(AkCheckbox).prop('isDisabled')).to.equal(true);
  });

  describe('create button click', () => {
    it('calls onSubmit', () => {
      const onSubmitSpy = sandbox.spy();
      class GroupCreateDialogImplWithSpy extends GroupCreateDialogImpl {
        public onSubmit = onSubmitSpy;
      }
      const wrapper = shallow(
        <GroupCreateDialogImplWithSpy
          dismiss={dismissSpy}
          history={{ push: historyPushSpy }}
          intl={createMockIntlProp()}
          match={{ params: { cloudId } }}
          mutate={mutateMock}
          showFlag={showFlagSpy}
          {...{} as any}
        />,
        createMockIntlContext(),
      );
      const modalFooter = shallow(wrapper.find(ModalDialog).prop('footer') as React.ReactElement<any>);
      const createButton = modalFooter.find(Button).first();

      expect(onSubmitSpy.called).to.equal(false);

      createButton.simulate('click');

      expect(onSubmitSpy.called).to.equal(true);
    });
  });

  describe('cancel button click', () => {
    it('calls onCancel', () => {
      const onCancelSpy = sandbox.spy();
      class GroupCreateDialogImplWithSpy extends GroupCreateDialogImpl {
        public onCancel = onCancelSpy;
      }
      const wrapper = shallow(
        <GroupCreateDialogImplWithSpy
          dismiss={dismissSpy}
          history={{ push: historyPushSpy }}
          intl={createMockIntlProp()}
          match={{ params: { cloudId } }}
          mutate={mutateMock}
          showFlag={showFlagSpy}
          {...{} as any}
        />,
        createMockIntlContext(),
      );
      const modalFooter = shallow(wrapper.find(ModalDialog).prop('footer') as React.ReactElement<any>);
      const cancelButton = modalFooter.find(Button).last();

      expect(onCancelSpy.called).to.equal(false);

      cancelButton.simulate('click');

      expect(onCancelSpy.called).to.equal(true);
    });
  });

  describe('onCancel', () => {
    it('calls dismiss', () => {
      const wrapper = mountGroupCreateDialog();
      (wrapper.instance() as any).onCancel();

      expect(dismissSpy.called).to.equal(true);
    });
  });

  describe('onSubmit', () => {
    function submitDialog() {
      const wrapper = mountGroupCreateDialog();
      (wrapper.instance() as any).onSubmit();

      return wrapper;
    }

    it('sets isLoading to true', () => {
      mutateMock = () => ({ then: () => ({ catch: () => ({}) }) });
      const wrapper = mountGroupCreateDialog();
      const modalFooter = shallow(wrapper.find(ModalDialog).prop('footer') as React.ReactElement<any>);
      const createButton = modalFooter.find(Button).first();

      expect(wrapper.state('isLoading')).to.equal(false);
      createButton.simulate('click');
      expect(wrapper.state('isLoading')).to.equal(true);
    });

    it('calls mutate', () => {
      submitDialog();
      expect(mutateMock.called).to.equal(true);
    });

    describe('on success', () => {
      it('calls dismiss', async () => {
        submitDialog();
        await successfulMutation;
        expect(dismissSpy.called).to.equal(true);
      });

      it('sets isLoading to false', async () => {
        const wrapper = submitDialog();
        await successfulMutation;
        expect(wrapper.state('isLoading')).to.equal(false);
      });

      it('calls showFlag', async () => {
        submitDialog();
        await successfulMutation;
        expect(showFlagSpy.called).to.equal(true);
      });

      it('calls history.push', async () => {
        submitDialog();
        await successfulMutation;
        expect(historyPushSpy.called).to.equal(true);
      });
    });

    describe('on failure', () => {
      it('sets isLoading to false', async () => {
        const failedMutation = Promise.reject(new Error('uh oh'));
        mutateMock = sandbox.stub().returns(failedMutation);
        const wrapper = submitDialog();
        await failedMutation
          .then(() => assert.fail())
          .catch(e => {
            expect(e instanceof Error).to.equal(true);
          });
        wrapper.update();
        expect(wrapper.state('isLoading')).to.equal(false);
      });
      it('shows an error flag', async () => {
        const failedMutation = Promise.reject(new Error('uh oh'));
        mutateMock = sandbox.stub().returns(failedMutation);
        submitDialog();
        await failedMutation
          .then(() => assert.fail())
          .catch(e => {
            expect(e instanceof Error).to.equal(true);
          });
        expect(showFlagSpy.called).to.equal(true);
      });
      it('shows an error flag when there are duplicate groups', async () => {
        const failedWithDuplicateGroupMutation = Promise.reject(failedWithDuplicateGroupError);
        mutateMock = sandbox.stub().returns(failedWithDuplicateGroupMutation);
        submitDialog();
        await failedWithDuplicateGroupMutation
          .then(() => assert.fail())
          .catch(e => {
            expect(e instanceof Error).to.equal(true);
          });
        expect(showFlagSpy.called).to.equal(true);
        expect(showFlagSpy.getCalls()[0].args[0].id).to.contain('user-management.groups.group-create.conflict.flag');
      });
    });
  });
});
