import * as React from 'react';
import {
  defineMessages,
  FormattedMessage,
  InjectedIntlProps,
  injectIntl,
} from 'react-intl';

import AkButton, { ButtonGroup as AkButtonGroup } from '@atlaskit/button';

import { removeUserFromGroupsModalScreenEvent, ScreenEventSender } from 'common/analytics';
import { ModalDialog } from 'common/modal';

export const messages = defineMessages({
  title: {
    id: 'user.remove.dialog.title',
    defaultMessage: 'Remove {userName}?',
    description: 'The title of the dialog to remove a user from a group.',
  },
  description: {
    id: 'user.remove.dialog.description',
    defaultMessage: 'You are about to remove {userName} from {groupName}.',
    description: 'Modal description telling the user what to do.',
  },
  buttonRemove: {
    id: 'user.remove.dialog.button.remove',
    defaultMessage: 'Remove',
    description: 'The button that confirms the removal of the user.',
  },
  buttonCancel: {
    id: 'user.remove.dialog.button.cancel',
    defaultMessage: 'Cancel',
    description: 'The button that cancels and closes the modal.',
  },
});

interface User {
  id: string;
  displayName: string;
  email: string;
  active: boolean;
}

export interface DeleteMemberDialogProps {
  isOpen: boolean;
  isRemovingUser: boolean;
  groupName: string;
  user?: User;
  onDismiss(): void;
  onRemove(): void;
}

export class DeleteMemberDialogImpl extends React.Component<DeleteMemberDialogProps & InjectedIntlProps> {
  public render() {
    const {
      intl: { formatMessage },
      isOpen,
      onDismiss,
      onRemove,
      isRemovingUser,
      user,
      groupName,
    } = this.props;

    return (
      <ModalDialog
        isOpen={isOpen}
        width="small"
        onClose={onDismiss}
        header={formatMessage(messages.title, { userName: user ? user.displayName : 'user' })}
        footer={(
          <AkButtonGroup>
            <AkButton
              appearance="danger"
              onClick={onRemove}
              isLoading={isRemovingUser}
              id="confirm-removing-member"
            >
              {formatMessage(messages.buttonRemove)}
            </AkButton>
            <AkButton
              appearance="subtle-link"
              onClick={onDismiss}
            >
              {formatMessage(messages.buttonCancel)}
            </AkButton>
          </AkButtonGroup>
        )}
      >
        <ScreenEventSender event={removeUserFromGroupsModalScreenEvent()}>
          <FormattedMessage
            {...messages.description}
            tagName="p"
            values={{
              groupName,
              userName: user ? user.displayName : 'user',
            }}
          />
        </ScreenEventSender>
      </ModalDialog>
    );
  }
}

export const DeleteMemberDialog = injectIntl(DeleteMemberDialogImpl);
