import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';

import AkPage from '@atlaskit/page';

import { createApolloClient } from '../../../apollo-client';
import { createMockAnalyticsClient, createMockIntlProp } from '../../../utilities/testing';
import { GroupCreateDialogImpl as GroupCreateDialog } from './group-create-dialog';

const dismissAction = action('dismiss');
const showFlagAction = action('showFlag');
const historyPushAction = action('history.push');

const successfulMutation = async (opts: any): Promise<any> => new Promise((resolve) => {
  const { name, description } = opts.variables.input;
  setTimeout(() => resolve({
    data: {
      createGroup: {
        id: `${Date.now().valueOf()}`,
        name,
        description,
      },
    },
  }), 1000);
});
const failedMutation = async (): Promise<any> => new Promise((_, reject) => {
  setTimeout(() => reject, 1000);
});

const defaults = {
  history: { push: historyPushAction } as any,
  match: { params: { cloudId: 'DUMMY_ORG_ID' } } as any,
  location: {} as any,
  isOpen: true,
  showFlag: showFlagAction,
  hideFlag: () => null,
  dismiss: dismissAction,
  mutate: successfulMutation,
  intl: createMockIntlProp(),
  analyticsClient: createMockAnalyticsClient(),
};

const Wrapper = ({ children }) => (
  <ApolloProvider client={createApolloClient()}>
    <IntlProvider locale="en">
      <AkPage>
        {children}
      </AkPage>
    </IntlProvider>
  </ApolloProvider>
);

storiesOf('Site|Group Create Dialog', module)
  .add('Default', () => (
    <Wrapper>
      <GroupCreateDialog {...defaults} />
    </Wrapper>
  ))
  .add('Error on create', () => (
    <Wrapper>
      <GroupCreateDialog {...defaults} mutate={failedMutation} />
    </Wrapper>
  ));
