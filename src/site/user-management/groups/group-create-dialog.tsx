import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';
import styled from 'styled-components';

import { ButtonGroup as AkButtonGroup } from '@atlaskit/button';
import { Checkbox as AkCheckbox } from '@atlaskit/checkbox';
import AkTextField from '@atlaskit/field-text';
import { gridSize as akGridSize } from '@atlaskit/theme';

import {
  AnalyticsClientProps,
  Button,
  createGroupCancelButtonClickedEventData,
  createGroupConfirmButtonClickedEventData,
  createGroupModalScreenEvent,
  createGroupTrackEventData,
  Referrer,
  ScreenEventSender,
  withAnalyticsClient,
} from 'common/analytics';
import { createErrorIcon, createSuccessIcon, errorFlagMessages } from 'common/error';
import { FieldDescription } from 'common/field-description';
import { FlagProps, withFlag } from 'common/flag';
import { ModalDialog } from 'common/modal';

import { CreateGroupMutation, CreateGroupMutationVariables, Group } from '../../../schema/schema-types';
import { util } from '../../../utilities/admin-hub';
import createGroupMutation from './create-group.mutation.graphql';
import { GroupDetailPageLocationState } from './group-detail-page.prop-types';
import { optimisticallyAddGroup as groupDetailsOptimisticallyAddGroup } from './group-details.query.updater';
import { optimisticallyAddGroup as groupsPageOptimisticallyAddGroup } from './groups-page.query.updater';

const CheckboxContainer = styled.div`
  margin: ${akGridSize() * 2}px 0 0 -${akGridSize()}px;
`;

export const messages = defineMessages({
  title: {
    id: 'user-management.groups.group-create.title',
    defaultMessage: 'Create group',
  },
  description: {
    id: 'user-management.groups.group-create.description',
    defaultMessage: `When you create the group, you'll be able to add users to it and configure its access.`,
  },
  buttonCreate: {
    id: 'user-management.groups.group-create.button.create',
    defaultMessage: 'Create group',
  },
  buttonCancel: {
    id: 'user-management.groups.group-create.button.cancel',
    defaultMessage: 'Cancel',
  },
  nameLabel: {
    id: 'user-management.groups.group-create.name.label',
    defaultMessage: `Group's name`,
  },
  nameDescription: {
    id: 'user-management.groups.group-create.name.description',
    defaultMessage: `You won't be able to change the group's name in the future.`,
  },
  descriptionLabel: {
    id: 'user-management.groups.group-create.description.label',
    defaultMessage: `Group's description`,
  },
  descriptionDescription: {
    id: 'user-management.groups.group-create.description.description',
    defaultMessage: 'A description will help you organize and manage your groups.',
  },
  addMembersNextStepLabel: {
    id: 'user-management.groups.group-create.add.members.next.step.label',
    defaultMessage: 'Add members in the next step',
  },
  createSuccess: {
    id: 'user-management.groups.group-create.create.success',
    defaultMessage: `Group '{name}' created`,
  },
  createConflictTitle: {
    id: 'user-management.groups.group-create.create.conflict.title',
    defaultMessage: 'A group with this name already exists',
    description: 'An error message that appears, saying that a user group cannot be created, as one with that name already exists',
  },
  createConflictDescription: {
    id: 'user-management.groups.group-create.create.conflict',
    defaultMessage: 'Please choose another name and try again.',
    description: 'An error message that instructs the user to select another group name, in the case that a group name already exists',
  },
  createGroupErrorTitle: {
    id: 'user-management.groups.group-create.create.error.title',
    defaultMessage: 'The group could not be created',
    description: 'An error message that appears, saying that a user group cannot be created, for an unknown reason',
  },
});

interface SiteParams {
  cloudId: string;
}
interface OwnProps {
  isOpen: boolean;
  dismiss(): void;
}

type GroupCreateDialogProps = ChildProps<OwnProps & RouteComponentProps<SiteParams> & FlagProps & InjectedIntlProps & AnalyticsClientProps, CreateGroupMutation>;

interface GroupCreateDialogState {
  addMembersNextStep: boolean;
  description: string;
  name: string;
  isLoading: boolean;
}

export class GroupCreateDialogImpl extends React.Component<GroupCreateDialogProps, GroupCreateDialogState> {
  public state: Readonly<GroupCreateDialogState> = {
    addMembersNextStep: false,
    description: '',
    name: '',
    isLoading: false,
  };

  public render() {
    const {
      intl: { formatMessage },
      isOpen,
    } = this.props;

    return (
      <ModalDialog
        header={formatMessage(messages.title)}
        footer={(
          <AkButtonGroup>
            <Referrer value="createGroupDialog">
              <Button
                appearance="primary"
                isDisabled={!this.state.name}
                isLoading={this.state.isLoading}
                onClick={this.onSubmit}
                id="confirm-group-creation-button"
              >
                {formatMessage(messages.buttonCreate)}
              </Button>
            </Referrer>
            <Button
              appearance="subtle-link"
              isDisabled={this.state.isLoading}
              onClick={this.onCancel}
            >
              {formatMessage(messages.buttonCancel)}
            </Button>
          </AkButtonGroup>
        )}
        isOpen={isOpen}
        width="small"
        onClose={this.onCancel}
      >
        <ScreenEventSender event={createGroupModalScreenEvent()}>
          <FormattedMessage {...messages.description} tagName="p" />
          <AkTextField
            onChange={this.onNameChange}
            label={formatMessage(messages.nameLabel)}
            required={true}
            shouldFitContainer={true}
            id="create-group-name"
            autoFocus={true}
            disabled={this.state.isLoading}
          />
          <FieldDescription>{formatMessage(messages.nameDescription)}</FieldDescription>
          <AkTextField
            onChange={this.onDescriptionChange}
            label={formatMessage(messages.descriptionLabel)}
            shouldFitContainer={true}
            id="create-group-description"
            disabled={this.state.isLoading}
          />
          <FieldDescription>{formatMessage(messages.descriptionDescription)}</FieldDescription>
          <CheckboxContainer>
            <AkCheckbox
              label={formatMessage(messages.addMembersNextStepLabel)}
              onChange={this.onAddMembersChange}
              id="create-group-add-members-next-step"
              isDisabled={this.state.isLoading}
            />
          </CheckboxContainer>
        </ScreenEventSender>
      </ModalDialog>
    );
  }

  public onCancel = () => {
    const {
      match: { params: { cloudId } },
    } = this.props;

    this.props.analyticsClient.sendUIEvent({ cloudId, data: createGroupCancelButtonClickedEventData() });
    this.props.dismiss();
  }

  public onSubmit = () => {
    const {
      dismiss,
      history,
      intl: { formatMessage },
      match: { params: { cloudId } },
      mutate,
      showFlag,
    } = this.props;
    const groupDetailPageLocationState: GroupDetailPageLocationState = {
      showAddMembersModal: this.state.addMembersNextStep,
    };

    this.setState({ isLoading: true });

    mutate!({
      variables: {
        cloudId,
        input: {
          name: this.state.name,
          description: this.state.description,
        },
      },
      update: (store, { data }) => {
        // UM will respond with 201 after creating a group, but then immediately after that, querying for the group details gives a 404. Trying a little bit later, we’re able to see the group details.
        // So, in order to not error in this state, we need to prevent Apollo from querying, even though that page is set up in a way to immediately query for group details (fair enough, it is the group details page).
        // We can do this by writing directly to the cache.
        const createdGroup = (data as CreateGroupMutation).createGroup;
        const newGroup: Group = {
          description: createdGroup.description,
          id: createdGroup.id,
          name: createdGroup.name,
          productPermissions: [],
          defaultForProducts: [],
          sitePrivilege: 'NONE',
          unmodifiable: false,
          userTotal: 0,
          managementAccess: 'ALL',
          ownerType: null,
        };

        groupDetailsOptimisticallyAddGroup(store, cloudId, newGroup);
        groupsPageOptimisticallyAddGroup(store, cloudId, newGroup);
      },
    }).then(({ data: { createGroup: { id, name, description } } }) => {
      dismiss();
      this.setState({ isLoading: false });

      this.props.analyticsClient.sendTrackEvent({
        cloudId, data: createGroupTrackEventData({
          groupId: id,
          description: !!description,
          memberCheckBox: this.state.addMembersNextStep,
        }),
      });

      this.props.analyticsClient.sendUIEvent({
        cloudId, data: createGroupConfirmButtonClickedEventData({
          groupId: id,
          description: !!description,
          memberCheckBox: this.state.addMembersNextStep,
        }),
      });

      showFlag({
        autoDismiss: true,
        icon: createSuccessIcon(),
        id: `user-management.groups.group-create.success.flag.${(Date.now().valueOf())}`,
        title: formatMessage(messages.createSuccess, {
          name,
        }),
      });

      history.push(`${util.siteAdminBasePath}/s/${cloudId}/groups/${id}`, groupDetailPageLocationState);
    }).catch((e) => {
      this.setState({ isLoading: false });

      if (e.graphQLErrors && e.graphQLErrors[0] && e.graphQLErrors[0].originalError && e.graphQLErrors[0].originalError.code === 'duplicateGroupName') {
        showFlag({
          autoDismiss: true,
          icon: createErrorIcon(),
          id: `user-management.groups.group-create.conflict.flag.${(Date.now().valueOf())}`,
          title: formatMessage(messages.createConflictTitle),
          description: formatMessage(messages.createConflictDescription),
        });
      } else {
        showFlag({
          autoDismiss: true,
          icon: createErrorIcon(),
          id: `user-management.groups.group-create.error.flag.${(Date.now().valueOf())}`,
          title: formatMessage(messages.createGroupErrorTitle),
          description: formatMessage(errorFlagMessages.description),
        });
      }
    });
  }

  private onNameChange = (e: React.SyntheticEvent<HTMLInputElement>) => {
    const name = e.currentTarget.value;
    this.setState({
      name,
    });
  };

  private onDescriptionChange = (e: React.SyntheticEvent<HTMLInputElement>) => {
    const description = e.currentTarget.value;
    this.setState({
      description,
    });
  };

  private onAddMembersChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      addMembersNextStep: event.target.checked,
    });
  };
}

const withCreateGroupMutation = graphql<RouteComponentProps<SiteParams> & OwnProps, CreateGroupMutation, CreateGroupMutationVariables, RouteComponentProps<SiteParams> & OwnProps>(createGroupMutation);

export const GroupCreateDialog = withRouter<OwnProps>(
  withCreateGroupMutation(
    withFlag(
      injectIntl(
        withAnalyticsClient(GroupCreateDialogImpl),
      ),
    ),
  ),
);
