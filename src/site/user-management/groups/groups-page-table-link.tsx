import * as React from 'react';
import { withRouter } from 'react-router';
import { Link, RouteComponentProps } from 'react-router-dom';
import styled from 'styled-components';

import { AnalyticsClientProps, groupNameLinkClickedEventData, Referrer, withAnalyticsClient } from 'common/analytics';
import { GroupName } from 'common/group-name';

import { Group } from '../../../schema/schema-types';

interface OwnProps {
  group: Group;
  groupCount?: number;
  hasAccessToProduct: boolean;
  isProductAdmin: boolean;
}

interface SiteParams {
  cloudId: string;
}

const Wrapper = styled(Link)`
  display: inline-block;
`;

export class GroupsPageTableLinkImpl extends React.Component<OwnProps & AnalyticsClientProps & RouteComponentProps<SiteParams>> {

  public render() {
    const { group } = this.props;

    return (
      <Referrer value="groupsTableLink">
        <Wrapper to={`./groups/${group.id}`} onClick={this.handleClick}>
          <GroupName name={group.name} ownerType={group.ownerType} managementAccess={group.managementAccess} />
        </Wrapper>
      </Referrer>
    );
  }

  private handleClick = () => {
    const {
      isProductAdmin,
      hasAccessToProduct,
      group,
      match: { params: { cloudId } },
      location,
      groupCount,
    } = this.props;

    this.props.analyticsClient.sendUIEvent({
      cloudId,
      data: groupNameLinkClickedEventData({
        groupId: group.id,
        groupCount,
        userCount: group.userTotal,
        defaultAccess: hasAccessToProduct,
        productAdmin: isProductAdmin,
        hasSearchQuery: location.search !== '',
        external: group.managementAccess === 'READ_ONLY' && group.ownerType === 'EXT_SCIM',
      }),
    });
  }
}

export const GroupsPageTableLink = withAnalyticsClient<OwnProps>(
  withRouter<OwnProps & AnalyticsClientProps>(GroupsPageTableLinkImpl),
);
