import * as React from 'react';
import {
  defineMessages,
  FormattedMessage,
  InjectedIntlProps,
  injectIntl,
} from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import styled from 'styled-components';

import {
  DynamicTableStateless as AkDynamicTableStateless,
  HeaderRow as AkHeaderRow,
  Row as AkRow,
} from '@atlaskit/dynamic-table';
import AkCheckCircleIcon from '@atlaskit/icon/glyph/check-circle';
import AkCrossCircleIcon from '@atlaskit/icon/glyph/cross-circle';
import AkLozenge from '@atlaskit/lozenge';
import {
  colors as akColors,
  gridSize as akGridSize,
} from '@atlaskit/theme';

import { EmptyTableView, getPaginationMessages } from 'common/table';
import { TextReveal } from 'common/text-reveal';

import { AnalyticsClientProps, Referrer, withAnalyticsClient } from 'common/analytics';

import { isGroupVisibleForUser } from 'common/group-permissions';

import { nonNullOrUndefined } from '../../../utilities/arrays';
import { Group } from './groups-page';
import { GroupsPageTableLink } from './groups-page-table-link';

const messages = defineMessages({
  lozengeDefault: {
    id: 'groups.page.table.lozenge',
    defaultMessage: 'Default access group',
  },
  headerGroup: {
    id: 'groups.page.table.header.group',
    defaultMessage: 'Group name',
  },
  headerMembers: {
    id: 'groups.page.table.header.members',
    defaultMessage: 'Members',
  },
  headerAccessToProduct: {
    id: 'groups.page.table.header.access.to.product',
    defaultMessage: 'Access to product',
  },
  headerProductAdministration: {
    id: 'groups.page.table.header.product.administration',
    defaultMessage: 'Product administration',
  },
  labelChecked: {
    id: 'groups.page.table.label.check',
    defaultMessage: 'Check',
  },
  labelNotChecked: {
    id: 'groups.page.table.label.not.checked',
    description: 'Indicates that the group does not have access to this product',
    defaultMessage: 'Not checked',
  },
  noGroups: {
    id: 'groups.page.no.groups',
    defaultMessage: 'No one has created a group yet. Be the first, using the button above.',
  },
  noGroupsFound: {
    id: 'groups.page.no.groups.found',
    defaultMessage: 'No groups found matching your filter.',
    description: 'Indicates in the groups page table that no groups were found when they searched for one.',
  },
});

const NameWrapper = styled.span`
  padding-right: ${akGridSize()}px;
`;

const IconWrapper = styled.div`
  display: flex;
  justify-content: center;
`;

const DescriptionWrapper = styled.span`
  color: ${akColors.subtleHeading};
`;

const Wrapper = styled.div`
  padding: ${akGridSize() * 0.75}px 0 ${akGridSize() * 0.75}px 0;
`;

export interface GroupsPageTableProps {
  groups: Group[] | null;
  groupCount?: number;
  rowsPerPage: number;
  page: number;
  isLoading: boolean;
  shouldShowSystemGroups: boolean;
  searchValuePresent: boolean;
  onSetPage(page: number): void;
}

interface SiteParams {
  cloudId: string;
}

export class GroupsPageTableImpl extends React.Component<GroupsPageTableProps & RouteComponentProps<SiteParams> & InjectedIntlProps & AnalyticsClientProps> {
  public render() {
    const {
      onSetPage,
      page,
      rowsPerPage,
      isLoading,
      searchValuePresent,
    } = this.props;

    return (
      <AkDynamicTableStateless
        head={this.getTableHeader()}
        isLoading={isLoading}
        rows={this.getRows()}
        rowsPerPage={rowsPerPage}
        onSetPage={onSetPage}
        page={page}
        paginationi18n={this.getPaginationMessages()}
        isFixedSize={true}
        emptyView={(
          <EmptyTableView>
            {searchValuePresent ? (
              <FormattedMessage {...messages.noGroupsFound} tagName="p" />
            ) : (
                <FormattedMessage {...messages.noGroups} tagName="p" />
              )}
          </EmptyTableView>
        )}
      />
    );
  }

  private getRows(): AkRow[] | undefined {
    if (!this.props.groups) {
      return undefined;
    }

    const groups = this.props.groups
      .filter(group => !group || isGroupVisibleForUser(group, this.props.shouldShowSystemGroups));

    if (!groups.length) {
      return undefined;
    }

    return groups.map(this.formatGroup);
  }

  private getTableHeader(): AkHeaderRow {
    const { formatMessage } = this.props.intl;

    return {
      cells: [
        {
          key: 'group',
          content: formatMessage(messages.headerGroup),
          width: 60,
        },
        {
          key: 'accessToProduct',
          content: formatMessage(messages.headerAccessToProduct),
          inlineStyles: {
            textAlign: 'center',
          },
          width: 20,
        },
        {
          key: 'productAdministration',
          content: formatMessage(messages.headerProductAdministration),
          width: 20,
          inlineStyles: {
            textAlign: 'center',
          },
        },
      ],
    };
  }

  private formatGroup = (group: Group, index: number): AkRow => {
    const { formatMessage } = this.props.intl;

    const checkedIcon = <AkCheckCircleIcon label={formatMessage(messages.labelChecked)} />;
    const crossedIcon = <AkCrossCircleIcon primaryColor={akColors.N90} label={formatMessage(messages.labelNotChecked)} />;

    if (!group) {
      return {
        key: `row-${index}`,
        cells: [
          {
            key: 'group',
            content: null,
          },
          {
            key: 'accessToProduct',
            content: null,
          },
          {
            key: 'productAdministration',
            content: null,
          },
        ],
      };
    }

    return {
      key: `row-${group.id}`,
      cells: [
        {
          key: 'group',
          content: (
            <Wrapper>
              <div>
                <NameWrapper>
                  <Referrer value="groupsTableLink">
                    <GroupsPageTableLink
                      group={group}
                      groupCount={this.props.groupCount}
                      hasAccessToProduct={this.hasAccessToProduct(group)}
                      isProductAdmin={this.isProductAdmin(group)}
                    />
                  </Referrer>
                </NameWrapper>
                {this.getDefaultLozenge(group)}
              </div>
              <DescriptionWrapper>
                <TextReveal>
                  {group.description || ''}
                </TextReveal>
              </DescriptionWrapper>
            </Wrapper>
          ),
        },
        {
          key: 'accessToProduct',
          content: <IconWrapper>{this.hasAccessToProduct(group) ? checkedIcon : crossedIcon}</IconWrapper>,
        },
        {
          key: 'productAdministration',
          content: <IconWrapper>{this.isProductAdmin(group) ? checkedIcon : crossedIcon}</IconWrapper>,
        },
      ],
    };
  };

  private getDefaultLozenge = (group: Group): React.ReactNode => {
    const { formatMessage } = this.props.intl;

    return group.defaultForProducts.length ? (
      <AkLozenge appearance="default" isBold={true}>
        {formatMessage(messages.lozengeDefault)}
      </AkLozenge>
    ) : null;
  };

  private isProductAdmin = (group: Group): boolean => {
    return group.productPermissions.filter(nonNullOrUndefined).some(({ permissions }) => permissions.includes('MANAGE'));
  };

  private hasAccessToProduct = (group: Group): boolean => {
    if (group.name.toLowerCase() === 'jira-administrators') {
      return group.productPermissions.filter(nonNullOrUndefined).some(({ permissions }) => {
        return permissions.includes('MANAGE') && permissions.includes('WRITE');
      });
    }

    return group.productPermissions.filter(nonNullOrUndefined).some(({ permissions }) => {
      return permissions.includes('MANAGE') || permissions.includes('WRITE');
    });
  };

  private getPaginationMessages = () => {
    return getPaginationMessages(this.props.intl.formatMessage.bind(this.props.intl));
  }
}

export const GroupsPageTable = withAnalyticsClient<GroupsPageTableProps>(
  withRouter<GroupsPageTableProps & AnalyticsClientProps>(
    injectIntl(GroupsPageTableImpl),
  ),
);
