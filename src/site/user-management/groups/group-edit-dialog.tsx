import * as React from 'react';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router-dom';

import { ButtonGroup as AkButtonGroup } from '@atlaskit/button';
import AkTextField from '@atlaskit/field-text';

import { AnalyticsClientProps, Button, editGroupDescriptionModalScreenEvent, ScreenEventSender, withAnalyticsClient } from 'common/analytics';
import { ModalDialog } from 'common/modal';

import { UpdateGroupInput } from '../../../schema/schema-types';

export const messages = defineMessages({
  title: {
    id: 'user-management.groups.group-edit.title',
    defaultMessage: `Edit group's description`,
  },
  description: {
    id: 'user-management.groups.group-edit.description',
    defaultMessage: `Edit the group's description to organize your groups better on the list. You can't edit the group's name.`,
  },
  buttonSave: {
    id: 'user-management.groups.group-edit.button.save',
    defaultMessage: 'Save changes',
  },
  buttonCancel: {
    id: 'user-management.groups.group-edit.button.cancel',
    defaultMessage: 'Cancel',
  },
  descriptionLabel: {
    id: 'user-management.groups.group-edit.description.label',
    defaultMessage: `Group's description`,
  },
});

interface OwnProps extends UpdateGroupInput {
  isOpen: boolean;
  groupId: string;
  memberCount: number;
  dismiss(): void;
  update(input: UpdateGroupInput): void;
}

interface SiteParams {
  cloudId: string;
}

type GroupEditDialogProps = OwnProps & InjectedIntlProps & RouteComponentProps<SiteParams> & AnalyticsClientProps;

type GroupEditDialogState = UpdateGroupInput;

export class GroupEditDialogImpl extends React.Component<GroupEditDialogProps, GroupEditDialogState> {
  public state: Readonly<GroupEditDialogState> = {
    description: this.props.description,
    name: this.props.name,
  };

  public render() {
    const {
      intl: { formatMessage },
      isOpen,
      description,
      groupId,
      memberCount,
    } = this.props;

    return (
      <ModalDialog
        header={formatMessage(messages.title)}
        footer={(
          <AkButtonGroup>
            <Button
              appearance="primary"
              onClick={this.onSubmit}
            >
              {formatMessage(messages.buttonSave)}
            </Button>
            <Button
              appearance="subtle-link"
              onClick={this.onCancel}
            >
              {formatMessage(messages.buttonCancel)}
            </Button>
          </AkButtonGroup>
        )}
        isOpen={isOpen}
        width="small"
        onClose={this.onCancel}
      >
        <ScreenEventSender
          event={editGroupDescriptionModalScreenEvent({
            groupId,
            memberCount,
          })}
        >
          <FormattedMessage {...messages.description} tagName="p" />
          <AkTextField
            autoFocus={true}
            onChange={this.onDescriptionChange}
            label={formatMessage(messages.descriptionLabel)}
            shouldFitContainer={true}
            id="edit-group-description"
            value={description || ''}
          />
        </ScreenEventSender>
      </ModalDialog>
    );
  }

  public onCancel = () => {
    this.props.dismiss();
  }

  public onSubmit = () => {
    this.props.update({
      description: this.state.description,
      name: this.state.name,
    });
  }

  private onDescriptionChange = (e: React.SyntheticEvent<HTMLInputElement>) => {
    const description = e.currentTarget.value;
    this.setState({
      description,
    });
  };
}

export const GroupEditDialog = withRouter<OwnProps>(injectIntl(withAnalyticsClient(GroupEditDialogImpl)));
