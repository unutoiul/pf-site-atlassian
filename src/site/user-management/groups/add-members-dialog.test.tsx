import { expect } from 'chai';
import { mount, ReactWrapper } from 'enzyme';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import * as sinon from 'sinon';

import AkButton from '@atlaskit/button';

import { Picker, PickerProps } from 'common/picker';

import { createApolloClient } from '../../../apollo-client';
import { AddMembersToGroupsMutationVariables, User } from '../../../schema/schema-types';
import { createMockAnalyticsClient, createMockIntlContext, createMockIntlProp, waitUntil } from '../../../utilities/testing';
import { AddMembersDialogImpl } from './add-members-dialog';

const noop = () => null;

function mountWithApolloProvider(node: React.ReactNode): ReactWrapper<any, any> {
  const apolloClient = createApolloClient();

  return mount(
    <ApolloProvider client={apolloClient}>
      {node}
    </ApolloProvider>,
    createMockIntlContext(),
  );
}

describe('AddMembersDialog', () => {
  it('renders a picker', () => {
    const wrapper = mountWithApolloProvider(
      <AddMembersDialogImpl
        data={{} as any}
        isOpen={true}
        name="Group"
        groupId="group-id"
        cloudId="cloud-id"
        dismiss={noop}
        showFlag={noop}
        hideFlag={noop}
        memberCount={0}
        intl={createMockIntlProp()}
        analyticsClient={createMockAnalyticsClient()}
      />,
    );
    const picker = wrapper.find(Picker);
    expect(picker.length).to.equal(1);
  });

  it('Clicking submit without adding items does not dismiss the dialog', () => {
    const dismissSpy = sinon.spy();
    const wrapper = mountWithApolloProvider(
      <AddMembersDialogImpl
        data={{} as any}
        isOpen={true}
        name="Group"
        groupId="group-id"
        cloudId="cloud-id"
        dismiss={dismissSpy}
        showFlag={noop}
        hideFlag={noop}
        memberCount={0}
        intl={createMockIntlProp()}
        analyticsClient={createMockAnalyticsClient()}
      />,
    );
    const picker = wrapper.find(Picker);
    picker.find('Button[appearance="primary"]').simulate('click');

    expect(dismissSpy.called).to.equal(false);
  });

  it('Clicking submit after adding items call dismiss, and showFlag', async () => {
    const dismissSpy = sinon.spy();
    const showFlagSpy = sinon.spy();

    const mutate = sinon.stub();
    mutate.returns(Promise.resolve('OK'));

    const wrapper = mountWithApolloProvider(
      <AddMembersDialogImpl
        data={{
          loading: false,
          siteUsersList: {
            total: 1,
            users: [{
              id: '1',
              displayName: 'josh',
              email: 'josh@bigtyper.com',
            }],
          },
        } as any}
        mutate={mutate}
        isOpen={true}
        name="Group"
        groupId="group-id"
        cloudId="cloud-id"
        dismiss={dismissSpy}
        showFlag={showFlagSpy}
        hideFlag={noop}
        memberCount={1}
        intl={createMockIntlProp()}
        analyticsClient={createMockAnalyticsClient()}
      />,
    );

    const picker = wrapper.find(Picker);

    picker.props().onSelect({
      id: '1',
    });

    wrapper.update();
    const button = wrapper.find(AkButton).first();

    await waitUntil(() => !button.props().isDisabled);
    button.simulate('click');
    await waitUntil(() => dismissSpy.called);

    expect(dismissSpy.called).to.equal(true);
    expect(showFlagSpy.called).to.equal(true);
  });

  it('Submitting the second time does not duplicate users submitted', async () => {
    const dismissSpy = sinon.spy();
    const showFlagSpy = sinon.spy();

    const mutate = sinon.stub();
    mutate.returns(Promise.resolve('OK'));

    const wrapper = mountWithApolloProvider(
      <AddMembersDialogImpl
        data={{
          loading: false,
          siteUsersList: {
            total: 2,
            users: [{
              id: '1',
              displayName: 'josh',
              email: 'josh@bigtyper.com',
            }, {
              id: '2',
              displayName: 'Corgi',
              email: 'corgi@bigtyper.com',
            }],
          },
        } as any}
        mutate={mutate}
        isOpen={true}
        name="Group"
        groupId="group-id"
        cloudId="cloud-id"
        dismiss={dismissSpy}
        showFlag={showFlagSpy}
        hideFlag={noop}
        memberCount={2}
        intl={createMockIntlProp()}
        analyticsClient={createMockAnalyticsClient()}
      />,
    );

    const picker = wrapper.find(Picker);

    picker.props().onSelect({
      id: '1',
    });
    wrapper.update();

    const button1 = wrapper.find(AkButton).first();

    await waitUntil(() => !button1.props().isDisabled);
    button1.simulate('click');
    await waitUntil(() => mutate.called);

    expect(mutate.callCount).to.equal(1);
    expect((mutate.getCalls()[0].args[0].variables as AddMembersToGroupsMutationVariables).input.users.length).to.equal(1);
    expect((mutate.getCalls()[0].args[0].variables as AddMembersToGroupsMutationVariables).input.users[0]).to.equal('1');

    picker.props().onSelect({
      id: '2',
    });
    wrapper.update();
    const button2 = wrapper.find(AkButton).first();
    await waitUntil(() => !button2.props().isDisabled);
    button2.simulate('click');
    await waitUntil(() => mutate.called);

    expect(mutate.callCount).to.equal(2);
    expect((mutate.getCalls()[1].args[0].variables as AddMembersToGroupsMutationVariables).input.users.length).to.equal(1);
    expect((mutate.getCalls()[1].args[0].variables as AddMembersToGroupsMutationVariables).input.users[0]).to.equal('2');
  });

  describe('success flag', () => {
    let showFlagSpy: sinon.SinonSpy;
    let dismissSpy: sinon.SinonSpy;
    let wrapper: ReactWrapper;
    let picker: ReactWrapper<PickerProps>;
    let mutate: sinon.SinonStub;

    const users: User[] = [
      {
        id: '1',
        displayName: 'Josh',
        email: 'josh@bigtyper.com',
        system: false,
        active: false,
        hasVerifiedEmail: true,
      },
      {
        id: '2',
        displayName: 'Jon Hopkins',
        email: 'jonhopkins@bigtyper.com',
        system: false,
        active: false,
        hasVerifiedEmail: true,
      },
      {
        id: '3',
        displayName: 'Burial',
        email: 'burial@bigtyper.com',
        system: false,
        active: false,
        hasVerifiedEmail: true,
      },
    ];

    beforeEach(() => {
      showFlagSpy = sinon.spy();
      dismissSpy = sinon.spy();
      mutate = sinon.stub();
      mutate.returns(Promise.resolve('OK'));

      wrapper = mountWithApolloProvider(
        <AddMembersDialogImpl
          data={{
            loading: false,
            siteUsersList: {
              total: 3,
              users,
            },
          } as any}
          mutate={mutate}
          isOpen={true}
          name="Admins"
          groupId="group-id"
          cloudId="cloud-id"
          dismiss={dismissSpy}
          showFlag={showFlagSpy}
          hideFlag={noop}
          memberCount={1}
          intl={createMockIntlProp()}
          analyticsClient={createMockAnalyticsClient()}
        />,
      );

      picker = wrapper.find(Picker);
    });

    it('should show the single message for 1 user added', async () => {
      picker.props().onSelect!(users[0]);

      wrapper.update();

      const button = wrapper.find(AkButton).first();

      await waitUntil(() => !button.props().isDisabled);
      button.simulate('click');
      await waitUntil(() => dismissSpy.called);

      expect(showFlagSpy.calledWithMatch({
        description: 'You added Josh to the Admins group.',
      })).to.equal(true);
    });

    it('should show the multiple message for 2 users added', async () => {
      picker.props().onSelect!(users[0]);
      picker.props().onSelect!(users[1]);

      wrapper.update();

      const button = wrapper.find(AkButton).first();

      await waitUntil(() => !button.props().isDisabled);
      button.simulate('click');
      await waitUntil(() => dismissSpy.called);

      expect(showFlagSpy.calledWithMatch({
        description: 'You added Josh and Jon Hopkins to the Admins group.',
      })).to.equal(true);
    });

    it('should show the many message for >2 users added', async () => {
      picker.props().onSelect!(users[0]);
      picker.props().onSelect!(users[1]);
      picker.props().onSelect!(users[2]);

      wrapper.update();

      const button = wrapper.find(AkButton).first();

      await waitUntil(() => !button.props().isDisabled);
      button.simulate('click');
      await waitUntil(() => dismissSpy.called);

      expect(showFlagSpy.calledWithMatch({
        description: 'You added Josh and 2 others to the Admins group.',
      })).to.equal(true);
    });
  });
});
