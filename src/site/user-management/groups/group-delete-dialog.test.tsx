// tslint:disable max-classes-per-file
import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { sandbox as sinonSandbox } from 'sinon';

import { Button } from 'common/analytics';
import { ModalDialog } from 'common/modal';

import { createMockAnalyticsClient, createMockIntlContext, createMockIntlProp } from '../../../utilities/testing';
import { GroupDeleteDialogImpl, messages } from './group-delete-dialog';

describe('GroupDeleteDialog', () => {
  const cloudId = 'FAKE';

  const sandbox = sinonSandbox.create();
  let dismissSpy: sinon.SinonSpy;
  let removeSpy: sinon.SinonSpy;

  beforeEach(() => {
    dismissSpy = sandbox.spy();
    removeSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  function shallowGroupDeleteDialog() {
    return shallow(
      <GroupDeleteDialogImpl
        analyticsClient={createMockAnalyticsClient()}
        match={{ params: { cloudId } }}
        dismiss={dismissSpy}
        intl={createMockIntlProp()}
        isOpen={true}
        remove={removeSpy}
        name="doggo"
        isDisabled={false}
        {...{} as any}
      />,
      createMockIntlContext(),
    );
  }

  it('renders with title and description', () => {
    const modal = shallowGroupDeleteDialog().find(ModalDialog);
    expect(modal.prop('header')).to.equal(`Delete "doggo"?`);
    expect(modal.find(FormattedMessage).first().prop('defaultMessage')).to.equal(messages.description.defaultMessage);
    expect(modal.find(FormattedMessage).last().prop('defaultMessage')).to.equal(messages.description2.defaultMessage);
  });

  it('renders with a delete button and a cancel button', () => {
    const modalFooterButtons = mount(shallowGroupDeleteDialog().find(ModalDialog).prop('footer') as React.ReactElement<any>).find(Button);

    expect(modalFooterButtons).to.have.length(2);

    const deleteButton = modalFooterButtons.first();
    const deleteButtonProps = deleteButton.props();

    expect(deleteButton.text()).to.equal(messages.buttonDelete.defaultMessage);
    expect(deleteButtonProps.appearance).to.equal('danger');
    expect(deleteButtonProps.id).to.equal('confirm-group-deletion-button');

    const cancelButton = modalFooterButtons.last();
    const cancelButtonProps = cancelButton.props();

    expect(cancelButton.text()).to.equal(messages.buttonCancel.defaultMessage);
    expect(cancelButtonProps.appearance).to.equal('subtle-link');
  });

  describe('delete button click', () => {
    it('calls onSubmit', () => {
      const onSubmitSpy = sandbox.spy();
      class GroupDeleteDialogImplWithSpy extends GroupDeleteDialogImpl {
        public onSubmit = onSubmitSpy;
      }
      const wrapper = shallow(
        <GroupDeleteDialogImplWithSpy
          analyticsClient={createMockAnalyticsClient()}
          match={{ params: { cloudId } }}
          dismiss={dismissSpy}
          intl={createMockIntlProp()}
          isOpen={true}
          remove={removeSpy}
          name="doggo"
          isDisabled={false}
          {...{} as any}
        />,
        createMockIntlContext(),
      );
      const modalFooter = shallow(wrapper.find(ModalDialog).prop('footer') as React.ReactElement<any>);
      const createButton = modalFooter.find(Button).first();

      createButton.simulate('click');

      expect(onSubmitSpy.called).to.equal(true);
    });
  });

  describe('cancel button click', () => {
    it('calls onCancel', () => {
      const onCancelSpy = sandbox.spy();
      class GroupDeleteDialogImplWithSpy extends GroupDeleteDialogImpl {
        public onCancel = onCancelSpy;
      }
      const wrapper = shallow(
        <GroupDeleteDialogImplWithSpy
          analyticsClient={createMockAnalyticsClient()}
          match={{ params: { cloudId } }}
          dismiss={dismissSpy}
          intl={createMockIntlProp()}
          isOpen={true}
          remove={removeSpy}
          name="doggo"
          isDisabled={false}
          {...{} as any}
        />,
        createMockIntlContext(),
      );
      const modalFooter = shallow(wrapper.find(ModalDialog).prop('footer') as React.ReactElement<any>);
      const cancelButton = modalFooter.find(Button).last();

      cancelButton.simulate('click');

      expect(onCancelSpy.called).to.equal(true);
    });
  });

  describe('onCancel', () => {
    it('calls dismiss', () => {
      const wrapper = shallowGroupDeleteDialog();
      (wrapper.instance() as any).onCancel();

      expect(dismissSpy.called).to.equal(true);
    });
  });

  describe('onSubmit', () => {
    it('calls remove', () => {
      const wrapper = shallowGroupDeleteDialog();
      (wrapper.instance() as any).onSubmit();

      expect(removeSpy.called).to.equal(true);
    });
  });
});
