import { DataProxy } from 'apollo-cache';

import { getStart } from 'common/pagination';

import { User as GroupPickerUser } from 'common/picker';

import { Group, GroupDetailsQuery, GroupDetailsQueryVariables } from '../../../schema/schema-types';
import getGroupDetails from './group-details.query.graphql';
import { ROWS_PER_PAGE } from './groups-constants';

function readGroupDetailsQuery(store: DataProxy, variables: GroupDetailsQueryVariables) {
  try {
    return store.readQuery({
      query: getGroupDetails,
      variables,
    }) as GroupDetailsQuery;
  } catch (e) {
    // Due to https://github.com/apollographql/apollo-feature-requests/issues/1,
    // store.readQuery throws rather than returns null for this case.
    return undefined;
  }
}

export function optimisticallyRemoveUser(store: DataProxy, cloudId: string, groupId: string, userId: string) {
  const variables: GroupDetailsQueryVariables = {
    cloudId,
    id: groupId,
    count: ROWS_PER_PAGE,
    start: getStart(1, ROWS_PER_PAGE),
  };

  const groupDetailsResult = readGroupDetailsQuery(store, variables);

  if (!groupDetailsResult) {
    return;
  }

  store.writeQuery({
    query: getGroupDetails,
    variables,
    data: {
      group: {
        groupDetails: {
          ...groupDetailsResult.group.groupDetails,
          __typename: 'GroupDetails',
        },
        members: {
          total: Math.max(0, groupDetailsResult.group.members.total - 1),
          users: (groupDetailsResult.group.members.users || []).filter(user => user.id !== userId),
          __typename: 'PaginatedUsers',
        },
        __typename: 'Group',
      },
    } as GroupDetailsQuery,
  });
}

export function optimisticallyAddUsers(store: DataProxy, cloudId: string, groupId: string, users: GroupPickerUser[]) {
  const variables: GroupDetailsQueryVariables = {
    cloudId,
    id: groupId,
    count: ROWS_PER_PAGE,
    start: getStart(1, ROWS_PER_PAGE),
  };

  const groupDetailsResult = readGroupDetailsQuery(store, variables);

  if (!groupDetailsResult) {
    return;
  }

  const newUsers = users
    .filter(user => groupDetailsResult.group.members.users.map(({ id }) => id).indexOf(user.id) === -1)
    .map(user => ({
      active: true,
      id: user.id,
      displayName: user.displayName,
      email: user.email,
      system: false,
      __typename: 'User',
    }));

  store.writeQuery({
    query: getGroupDetails,
    variables,
    data: {
      group: {
        groupDetails: {
          ...groupDetailsResult.group.groupDetails,
          __typename: 'GroupDetails',
        },
        members: {
          total: groupDetailsResult.group.members.total + users.length,
          users: [...newUsers, ...groupDetailsResult.group.members.users],
          __typename: 'PaginatedUsers',
        },
        __typename: 'Group',
      },
    } as GroupDetailsQuery,
  });
}

export function optimisticallyAddGroup(store: DataProxy, cloudId: string, group: Group) {
  const variables: GroupDetailsQueryVariables = {
    cloudId,
    id: group.id,
    count: ROWS_PER_PAGE,
    start: getStart(1, ROWS_PER_PAGE),
  };

  store.writeQuery({
    query: getGroupDetails,
    variables,
    data: {
      group: {
        groupDetails: {
          ...group as any,
          __typename: 'Group',
        },
        members: {
          total: 0,
          users: [],
          __typename: 'PaginatedUsers',
        },
        __typename: 'GroupDetails',
      },
    } as GroupDetailsQuery,
  });
}
