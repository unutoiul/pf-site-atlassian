import { expect } from 'chai';
import * as sinon from 'sinon';

import { User as GroupPickerUser } from 'common/picker';

import { Group, GroupDetailsQuery } from '../../../schema/schema-types';
import {
  optimisticallyAddGroup,
  optimisticallyAddUsers,
  optimisticallyRemoveUser,
} from './group-details.query.updater';

describe('Group details query updater', () => {
  let writeQuerySpy: sinon.SinonSpy;

  const sandbox = sinon.sandbox.create();

  beforeEach(() => {
    writeQuerySpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  const DUMMY_CLOUD_ID = 'DUMMY_CLOUD_ID';
  const DUMMY_GROUP_ID = 'DUMMY_GROUP_ID';

  const rng = (): string => String(Math.floor(Math.random() * 10000));

  const generateStore = (
    groupDetails: GroupDetailsQuery['group']['groupDetails'],
    members: GroupDetailsQuery['group']['members'],
  ) => {
    return {
      readQuery: sinon.stub().returns({
        group: {
          groupDetails,
          members,
        },
      } as GroupDetailsQuery),

      writeQuery: writeQuerySpy,
    } as any;
  };

  const generateGroupDetails = (): GroupDetailsQuery['group']['groupDetails'] => ({
    description: 'Corgi likes to party.',
    id: rng(),
    name: 'Party Corgi',
    productPermissions: [],
    defaultForProducts: [],
    sitePrivilege: 'SYS_ADMIN',
    unmodifiable: false,
    managementAccess: 'ALL',
    ownerType: null,
  } as any);

  const generateMembers = (): GroupDetailsQuery['group']['members'] => ({
    total: 2,
    users: [
      {
        active: false,
        id: `1${rng()}`,
        displayName: rng(),
        email: 'corgi@happypaws.co',
        system: false,
      },
      {
        active: false,
        id: `1${rng()}`,
        displayName: rng(),
        email: 'corgi@happypaws.co',
        system: false,
      },
    ],
  } as any);

  describe('optimisticallyRemoveUser', () => {
    it('should decrease the member total by 1', () => {
      const members = generateMembers();
      const store = generateStore(generateGroupDetails(), members);

      optimisticallyRemoveUser(store, DUMMY_CLOUD_ID, DUMMY_GROUP_ID, members.users[0].id);

      expect(writeQuerySpy.callCount).to.equal(1);
      expect(writeQuerySpy.getCall(0).args[0].data.group.members.total).to.equal(1);
    });

    it('should remove the user from the members list while leaving the others intact', () => {
      const members = generateMembers();
      const store = generateStore(generateGroupDetails(), members);
      const userToRemove = members.users[0].id;

      optimisticallyRemoveUser(store, DUMMY_CLOUD_ID, DUMMY_GROUP_ID, userToRemove);

      expect(writeQuerySpy.callCount).to.equal(1);
      expect(
        writeQuerySpy.getCall(0).args[0].data.group.members.users.find(user => user.id === userToRemove),
      ).to.equal(undefined);
    });
  });

  describe('optimisticallyAddUsers', () => {
    it('should increase the number of users by the number added', () => {
      const members = generateMembers();
      const store = generateStore(generateGroupDetails(), members);
      const users = [
        {
          id: `1${rng()}`,
          displayName: 'Happy lil corgi',
          email: 'corgi@happycorgi.co',
          username: 'Happy Corgi',
          actions: [],
        } as GroupPickerUser,
        {
          id: `2${rng()}`,
          displayName: 'Tiny lil corgi',
          email: 'tiny@lilcorgi.co',
          username: 'Tiny Corgi',
          actions: [],
        } as GroupPickerUser,
      ];

      optimisticallyAddUsers(store, DUMMY_CLOUD_ID, DUMMY_GROUP_ID, users);

      expect(writeQuerySpy.callCount).to.equal(1);
      expect(writeQuerySpy.getCall(0).args[0].data.group.members.total).to.equal(4);
    });

    it('should add the new users to the front of the cache array', () => {
      const members = generateMembers();
      const store = generateStore(generateGroupDetails(), members);

      const lilCorgi: GroupPickerUser = {
        id: `3${rng()}`,
        displayName: 'Happy lil corgi',
        email: 'corgi@happycorgi.co',
        username: 'Happy Corgi',
        actions: [],
      };

      const tinyCorgi: GroupPickerUser = {
        id: `4${rng()}`,
        displayName: 'Tiny lil corgi',
        email: 'tiny@lilcorgi.co',
        username: 'Tiny Corgi',
        actions: [],
      };

      optimisticallyAddUsers(store, DUMMY_CLOUD_ID, DUMMY_GROUP_ID, [lilCorgi, tinyCorgi]);

      expect(writeQuerySpy.callCount).to.equal(1);
      expect(writeQuerySpy.getCall(0).args[0].data.group.members.users[0].id).to.equal(lilCorgi.id);
      expect(writeQuerySpy.getCall(0).args[0].data.group.members.users[1].id).to.equal(tinyCorgi.id);
    });
  });

  describe('optimisticallyAddGroup', () => {
    it('should add the new group', () => {
      const store = generateStore(generateGroupDetails(), { total: 0, users: [] } as any);

      const corgiGroup: Group = {
        description: 'Corgi group description',
        id: '1',
        name: 'Corgi group',
        productPermissions: [],
        defaultForProducts: [],
        sitePrivilege: 'NONE',
        unmodifiable: false,
        userTotal: 0,
        managementAccess: 'ALL',
        ownerType: null,
      };

      optimisticallyAddGroup(store, DUMMY_CLOUD_ID, corgiGroup);

      expect(writeQuerySpy.callCount).to.equal(1);
      expect(writeQuerySpy.getCall(0).args[0].data.group.groupDetails.id).to.equal('1');
      expect(writeQuerySpy.getCall(0).args[0].data.group.groupDetails.name).to.equal('Corgi group');
    });
  });
});
