import { assert, expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import {
  DynamicTableStateless as AkDynamicTableStateless,
} from '@atlaskit/dynamic-table';

import { Action } from 'common/actions';

import { GroupMemberListQuery, User } from '../../../schema/schema-types';
import { createMockAnalyticsClient, createMockIntlContext, createMockIntlProp } from '../../../utilities/testing';
import { DeleteMemberDialog } from './delete-member-dialog';
import { GroupMemberListImpl, GroupMemberListProps } from './group-member-list';

describe('GroupMemberList', () => {
  const sandbox = sinon.sandbox.create();
  const successfulMutation = Promise.resolve({
    data: {},
  });

  const mockUsers: User[] = [
    { id: '1', active: true, displayName: 'Jones', email: 'jones@acme.com', system: false, activeStatus: 'ENABLED', hasVerifiedEmail: true },
    { id: '2', active: true, displayName: 'Jones', email: 'jones@acme.com', system: false, activeStatus: 'ENABLED', hasVerifiedEmail: true },
  ];

  let mutateMock;
  let showFlagSpy;
  let hideFlagSpy;
  let onSetPageSpy;

  beforeEach(() => {
    mutateMock = sandbox.stub().returns(successfulMutation);
    showFlagSpy = sandbox.spy();
    hideFlagSpy = sandbox.spy();
    onSetPageSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  interface ShallowProps {
    users: User[];
    groupName?: string;
    currentUser?: GroupMemberListQuery['currentUser'];
    shouldShowSystemUsers?: boolean;
    isGroupReadonly?: boolean;
  }

  const shallowGroupMemberList = ({
    users,
    shouldShowSystemUsers = false,
    groupName = 'dummy-group',
    currentUser = { id: '123' } as any,
    isGroupReadonly = false,
  }: ShallowProps) => {
    return shallow<GroupMemberListProps>((
      <GroupMemberListImpl
        showFlag={showFlagSpy}
        hideFlag={hideFlagSpy}
        mutate={mutateMock}
        cloudId="dummy-cloud-id"
        groupId="dummy-group-id"
        groupName={groupName}
        memberCount={0}
        analyticsClient={createMockAnalyticsClient()}
        currentUser={currentUser}
        users={users}
        rowsPerPage={20}
        page={1}
        onSetPage={onSetPageSpy}
        shouldShowSystemUsers={!!shouldShowSystemUsers}
        intl={createMockIntlProp()}
        isGroupReadonly={isGroupReadonly}
      />
    ), createMockIntlContext());
  };

  describe('Table with groups', () => {
    it('should contain headers', () => {
      const wrapper = shallowGroupMemberList({ users: mockUsers });
      const table = wrapper.find(AkDynamicTableStateless);

      expect(table.exists()).to.equal(true);

      const head = table.props().head as any;

      expect(head.cells[0].content).to.equal('User');
      expect(head.cells[1].content).to.equal('Options');
    });

    it('should have an action to remove the user', () => {
      const wrapper = shallowGroupMemberList({ users: mockUsers });
      const table = wrapper.find(AkDynamicTableStateless);

      expect(table.exists()).to.equal(true);

      const rows = table.props().rows as any;

      rows.map(row => {
        const cell = shallow(row.cells[1].content, createMockIntlContext());
        expect(cell.find(Action).exists()).to.equal(true);
        expect(cell.find(Action).props().actionType).to.equal('remove');
        expect(cell.find(Action).props().isDisabled).to.equal(false);
      });
    });

    it(`should disable the remove action if the group name is site-admins and it's the current user`, () => {
      const wrapper = shallowGroupMemberList({ users: mockUsers, groupName: 'site-admins', currentUser: { id: '1' } as any } as any);
      const table = wrapper.find(AkDynamicTableStateless);

      expect(table.exists()).to.equal(true);

      const currentUserRow = shallow((table.props().rows as any)[0].cells[1].content, createMockIntlContext());
      expect(currentUserRow.find(Action).props().isDisabled).to.equal(true);

      const otherUserRow = shallow((table.props().rows as any)[1].cells[1].content, createMockIntlContext());
      expect(otherUserRow.find(Action).props().isDisabled).to.equal(false);
    });

    describe('readonly groups', () => {
      it(`should disable the remove action with correct message for site-admins group`, () => {
        const wrapper = shallowGroupMemberList({ users: mockUsers, isGroupReadonly: true, groupName: 'site-admins' });
        const table = wrapper.find(AkDynamicTableStateless);

        expect(table.exists()).to.equal(true);
        table.props().rows!.map((row: any) => {
          const actionWrapper = shallow<Action>(row.cells[1].content, createMockIntlContext()).find(Action).props();
          expect(actionWrapper.isDisabled).to.equal(true);
          expect(actionWrapper.disabledReason!.defaultMessage).to.equal(`You can't remove this member because this site is linked to an organization`);
        });
      });

      it(`should disable the remove action with correct message for a non-site admin group`, () => {
        const wrapper = shallowGroupMemberList({ users: mockUsers, isGroupReadonly: true });
        const table = wrapper.find(AkDynamicTableStateless);

        expect(table.exists()).to.equal(true);
        table.props().rows!.map((row: any) => {
          const actionWrapper = shallow<Action>(row.cells[1].content, createMockIntlContext()).find(Action).props();
          expect(actionWrapper.isDisabled).to.equal(true);
          expect(actionWrapper.disabledReason!.defaultMessage).to.equal(`You can't remove this member because it is synced from an external directory`);
        });
      });
    });

    describe('Groups with system users', () => {

      const users: User[] = [
        { id: '1', active: true, displayName: 'Jones', email: 'jones@acme.com', system: false, activeStatus: 'ENABLED', hasVerifiedEmail: true },
        { id: '2', active: true, displayName: 'Jones', email: 'jones@acme.com', system: true, activeStatus: 'ENABLED', hasVerifiedEmail: true },
        { id: '3', active: true, displayName: 'Jones', email: 'jones@acme.com', system: true, activeStatus: 'ENABLED', hasVerifiedEmail: true },
      ];

      it('Should not show system users', () => {
        const wrapper = shallowGroupMemberList({ users, shouldShowSystemUsers: false });
        const table = wrapper.find(AkDynamicTableStateless);
        const tableRows = table.props().rows as any;
        expect(tableRows.length).to.equal(1);
      });

      it('Should show system users', () => {
        const wrapper = shallowGroupMemberList({ users, shouldShowSystemUsers: true });
        const table = wrapper.find(AkDynamicTableStateless);
        const tableRows = table.props().rows as any;
        expect(tableRows.length).to.equal(3);
      });
    });

    it('should open the delete member modal when remove is clicked', () => {
      const wrapper = shallowGroupMemberList({ users: mockUsers });
      const table = wrapper.find(AkDynamicTableStateless);

      expect(table.exists()).to.equal(true);

      const rows = table.props().rows as any;
      const cell = shallow(rows[0].cells[1].content, createMockIntlContext());

      cell.find(Action).props().onAction!();
      wrapper.update();
      expect(wrapper.find(DeleteMemberDialog).props().isOpen).to.equal(true);
    });

    describe('onMemberRemove', () => {
      it('should set the correct loading state', () => {
        mutateMock = () => ({ then: () => ({ catch: () => ({}) }) });

        const wrapper = shallowGroupMemberList({ users: mockUsers });
        const instance = wrapper.instance() as GroupMemberListImpl;

        instance.toggleDeleteMemberDialog({ user: mockUsers[0] });
        instance.onMemberRemove();

        expect(wrapper.state('deleteMemberDialog').isMutationLoading).to.equal(true);
      });

      describe('on success', () => {
        it('should reset the loading state', async () => {
          const wrapper = shallowGroupMemberList({ users: mockUsers });
          const instance = wrapper.instance() as GroupMemberListImpl;

          instance.toggleDeleteMemberDialog({ user: mockUsers[0] });
          instance.onMemberRemove();

          await successfulMutation;
          expect(wrapper.state('deleteMemberDialog').isMutationLoading).to.equal(false);
        });

        it('should show a flag', async () => {
          const wrapper = shallowGroupMemberList({ users: mockUsers });
          const instance = wrapper.instance() as GroupMemberListImpl;

          instance.toggleDeleteMemberDialog({ user: mockUsers[0] });
          instance.onMemberRemove();

          await successfulMutation;
          expect(showFlagSpy.called).to.equal(true);
        });
      });

      describe('on failure', () => {
        it('should reset the loading state', async () => {
          const failedMutation = Promise.reject(new Error('Something bad'));
          mutateMock = sandbox.stub().returns(failedMutation);

          const wrapper = shallowGroupMemberList({ users: mockUsers });
          const instance = wrapper.instance() as GroupMemberListImpl;

          instance.toggleDeleteMemberDialog({ user: mockUsers[0] });
          instance.onMemberRemove();

          await failedMutation
            .then(() => assert.fail())
            .catch(e => {
              expect(e instanceof Error).to.equal(true);
            });

          expect(wrapper.state('deleteMemberDialog').isMutationLoading).to.equal(false);
        });

        it('should show a flag', async () => {
          const failedMutation = Promise.reject(new Error('Something bad'));
          const wrapper = shallowGroupMemberList({ users: mockUsers });
          const instance = wrapper.instance() as GroupMemberListImpl;

          instance.toggleDeleteMemberDialog({ user: mockUsers[0] });
          instance.onMemberRemove();

          await failedMutation
            .then(() => assert.fail())
            .catch(e => {
              expect(e instanceof Error).to.equal(true);
            });

          expect(showFlagSpy.called).to.equal(true);
        });
      });
    });
  });
});
