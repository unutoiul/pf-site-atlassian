import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import { MemoryRouter } from 'react-router';
import { Link } from 'react-router-dom';
import { sandbox as sinonSandbox, SinonSandbox } from 'sinon';

import { util } from '../../../../src/utilities/admin-hub';
import { Permission } from '../../../schema/schema-types';
import { createMockIntlContext, createMockIntlProp } from '../../../utilities/testing';
import { GroupDetailSidebarImpl, GroupDetailSidebarProps, messages, ProductAccessList, ProductAccessName, ProductAccessType } from './group-detail-sidebar';

describe('GroupDetailSidebar', () => {

  let sandbox: SinonSandbox;
  sandbox = sinonSandbox.create();

  afterEach(() => {
    sandbox.restore();
  });

  function mountGroupDetailSidebarWithOptions(options: GroupDetailSidebarProps) {
    // Using mount instead of shallow because the styled components require rendering in order to make assertions about their text content
    return mount((
      <MemoryRouter>
        <GroupDetailSidebarImpl
          intl={createMockIntlProp()}
          match={{ params: { cloudId: 'cloudId' } } as any}
          location={{} as any}
          history={{} as any}
          {...options}
        />
      </MemoryRouter>
    ), createMockIntlContext());
  }

  it('renders no access message when productPermissions is empty', () => {
    const wrapper = mountGroupDetailSidebarWithOptions({ productPermissions: [], defaultForProducts: [] });
    expect(wrapper.find('p').text()).to.equal(messages.noProductAccess.defaultMessage);
  });

  it('renders the product list', () => {
    const productPermissions = [
      {
        productId: 'test1',
        productName: 'Test One',
        permissions: ['WRITE'] as Permission[],
      },
      {
        productId: 'test2',
        productName: 'Test Two',
        permissions: ['MANAGE'] as Permission[],
      },
      {
        productId: 'test3',
        productName: 'Test Three',
        permissions: [],
      },
      {
        productId: 'test4',
        productName: 'Test Four',
        permissions: ['WRITE', 'MANAGE'] as Permission[],
      },
    ];
    const defaultForProducts = [
      {
        productId: 'test1',
        productName: 'Test One',
      },
    ];
    const wrapper = mountGroupDetailSidebarWithOptions({ productPermissions, defaultForProducts });

    expect(wrapper.find(ProductAccessList)).to.have.length(productPermissions.length);

    expect(wrapper.find(ProductAccessName).first().text()).to.equal(productPermissions[0].productName);
    expect(wrapper.find(ProductAccessName).at(1).text()).to.equal(productPermissions[1].productName);
    expect(wrapper.find(ProductAccessName).at(2).text()).to.equal(productPermissions[2].productName);
    expect(wrapper.find(ProductAccessName).last().text()).to.equal(productPermissions[3].productName);

    expect(wrapper.find(ProductAccessType).first().text()).to.equal('User access (Default access group)');
    expect(wrapper.find(ProductAccessType).at(1).text()).to.equal(messages.adminAccess.defaultMessage);
    const productAccessTypeNone = wrapper.find(ProductAccessType).at(2);
    expect(productAccessTypeNone.text()).to.equal(messages.noAccess.defaultMessage);
    expect(productAccessTypeNone.hasClass('no-access')).to.equal(true);
    expect(wrapper.find(ProductAccessType).last().text()).to.equal(messages.adminAndUserAccess.defaultMessage);
  });

  describe('apps link', () => {
    it('should have cloud ID in href in admin hub', () => {
      sandbox.stub(util, 'isAdminHub').returns(true);
      const wrapper = mountGroupDetailSidebarWithOptions({ productPermissions: [], defaultForProducts: [] });
      const link = wrapper.find(Link);

      expect(link.prop('to')).to.equal('/s/cloudId/apps');

    });

    it('should not have cloud ID in site admin', () => {
      sandbox.stub(util, 'isAdminHub').returns(false);
      const wrapper = mountGroupDetailSidebarWithOptions({ productPermissions: [], defaultForProducts: [] });
      const link = wrapper.find(Link);

      expect(link.prop('to')).to.equal('/admin/apps');
    });
  });
});
