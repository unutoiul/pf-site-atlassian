import * as React from 'react';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';
import styled from 'styled-components';

import AkButton from '@atlaskit/button';
import {
  colors as akColors,
  fontSize as akFontSize,
  gridSize as akGridSize,
} from '@atlaskit/theme';

import { PageSidebar } from 'common/page-layout';

import { Permission } from '../../../schema/schema-types';
import { util } from '../../../utilities/admin-hub';

export const messages = defineMessages({
  title: {
    id: 'user-management.groups.group-detail.sidebar.title',
    defaultMessage: 'Group access',
  },
  noProductAccess: {
    id: 'user-management.groups.group-detail.sidebar.no.product.access',
    defaultMessage: 'This group currently has no product access.',
  },
  noAccess: {
    id: 'user-management.groups.group-detail.sidebar.no.access',
    defaultMessage: 'No access',
  },
  adminAccess: {
    id: 'user-management.groups.group-detail.sidebar.admin.access',
    defaultMessage: 'Admin access',
  },
  userAccess: {
    id: 'user-management.groups.group-detail.sidebar.user.access',
    defaultMessage: 'User access',
  },
  adminAndUserAccess: {
    id: 'user-management.groups.group-detail.sidebar.admin.and.user.access',
    defaultMessage: 'Admin & user access',
  },
  defaultAccess: {
    id: 'user-management.groups.group-detail.sidebar.default.access',
    defaultMessage: '{accessType} (Default access group)',
  },
  editAccess: {
    id: 'user-management.groups.group-detail.sidebar.edit.access',
    defaultMessage: `Edit group's access`,
  },
});

export const ProductAccessList = styled.dl`
  margin: ${akGridSize() * 2}px 0 0;
  padding: 0;

  & + dl {
    margin-top: ${akGridSize() * 3}px;
  }
`;
export const ProductAccessName = styled.dt`
  color: ${akColors.subtleHeading};
  font-size: ${akFontSize() - 2}px;
  font-weight: bold;
`;
export const ProductAccessType = styled.dd`
  margin: ${akGridSize}px 0 0;

  &.no-access {
    color: ${akColors.subtleText};
    font-style: italic;
  }
`;
export const EditAccessLink = styled(AkButton)`
  margin-top: ${akGridSize() * 3}px;
  padding: 0;

  span {
    margin: 0 !important;
  }
`;

interface ProductPermission {
  productName: string;
  productId: string;
  permissions: Array<Permission | null>;
}

interface DefaultForProduct {
  productName: string;
  productId: string;
}

export interface GroupDetailSidebarProps {
  productPermissions: ProductPermission[];
  defaultForProducts: DefaultForProduct[];
  cantModifyReason?: FormattedMessage.MessageDescriptor | null;
  onEditAccess?(): void;
}

export class GroupDetailSidebarImpl extends React.Component<GroupDetailSidebarProps & InjectedIntlProps & RouteComponentProps<{cloudId: string}>> {
  public render() {
    const {
      intl: { formatMessage },
      productPermissions,
    } = this.props;

    return (
      <React.Fragment>
        <PageSidebar>
          <h4>{formatMessage(messages.title)}</h4>
          {!productPermissions.length ? (
            <p>{formatMessage(messages.noProductAccess)}</p>
          ) : productPermissions.map(productPermission => (
            <ProductAccessList key={productPermission.productId}>
              <ProductAccessName>{productPermission.productName}</ProductAccessName>
              <ProductAccessType className={productPermission.permissions.length === 0 ? 'no-access' : undefined}>
                {this.getTypeText(productPermission)}
              </ProductAccessType>
            </ProductAccessList>
          ))}
          {!this.props.cantModifyReason && <Link onClick={this.props.onEditAccess} to={this.getAppsUrl()}>
            <EditAccessLink appearance="link" >
              {formatMessage(messages.editAccess)}
            </EditAccessLink>
          </Link>}
        </PageSidebar>
      </React.Fragment>
    );
  }

  private getAppsUrl = (): string => {
    if (util.isAdminHub()) {
      const { match: { params: { cloudId } } } = this.props;

      return `/s/${cloudId}/apps`;
    }

    return '/admin/apps';
  }

  private getTypeText({ productId, permissions }: ProductPermission): string {
    const {
      intl: { formatMessage },
      defaultForProducts,
    } = this.props;

    const accessType = formatMessage(GroupDetailSidebarImpl.getMessageFromPermissions(permissions));

    return defaultForProducts.map(product => product.productId).includes(productId) ? formatMessage(messages.defaultAccess, {
      accessType,
    }) : accessType;
  }

  private static getMessageFromPermissions(permissions: Array<Permission | null>): FormattedMessage.MessageDescriptor {
    const hasManage = permissions.includes('MANAGE');
    const hasWrite = permissions.includes('WRITE');

    if (hasManage && hasWrite) {
      return messages.adminAndUserAccess;
    } else if (hasManage) {
      return messages.adminAccess;
    } else if (hasWrite) {
      return messages.userAccess;
    } else {
      return messages.noAccess;
    }
  }
}

export const GroupDetailSidebar = withRouter<GroupDetailSidebarProps>(injectIntl(GroupDetailSidebarImpl));
