import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';
import { Link, MemoryRouter } from 'react-router-dom';

import { createApolloClient } from '../../../apollo-client';
import { createMockAnalyticsClient, MockAnalyticsClient } from '../../../utilities/testing';
import { GroupsPageTableLinkImpl } from './groups-page-table-link';

describe('GroupsPageTableLink', () => {
  let mockAnalyticsClient: MockAnalyticsClient;

  beforeEach(() => {
    mockAnalyticsClient = createMockAnalyticsClient();
  });

  it('renders a link with the group id', () => {
    const apolloClient = createApolloClient();
    const wrapper = mount(
      <MemoryRouter>
        <ApolloProvider client={apolloClient}>
          <IntlProvider locale="en">
            <GroupsPageTableLinkImpl
              group={{
                id: 'dd620c84-3244-4f7f-b29d-043d9f18493c',
                name: 'Foo',
                description: 'What a nice description.',
                productPermissions: [null],
                defaultForProducts: [null],
                sitePrivilege: 'NONE',
                unmodifiable: false,
                userTotal: 99,
                managementAccess: 'ALL',
                ownerType: null,
              }}
              groupCount={13}
              hasAccessToProduct={false}
              isProductAdmin={false}
              analyticsClient={mockAnalyticsClient}
              match={{ params: { cloudId: 'FAKE' } } as any}
              location={{ search: '' } as any}
              history={{} as any}
            />
          </IntlProvider>
        </ApolloProvider>
      </MemoryRouter>,
    );
    expect(wrapper.find(Link).props().to).to.contain('dd620c84-3244-4f7f-b29d-043d9f18493c');
  });

  it('clicking the link triggers a UI event', () => {
    const apolloClient = createApolloClient();

    const wrapper = mount(
      <MemoryRouter>
        <ApolloProvider client={apolloClient}>
          <IntlProvider locale="en">
            <GroupsPageTableLinkImpl
              group={{
                id: 'a78e4263-97e2-41d6-8cce-abb508e809c7',
                name: 'Foo',
                description: 'What a nice description.',
                productPermissions: [null],
                defaultForProducts: [null],
                sitePrivilege: 'NONE',
                unmodifiable: false,
                userTotal: 99,
                managementAccess: 'ALL',
                ownerType: null,
              }}
              groupCount={13}
              hasAccessToProduct={false}
              isProductAdmin={false}
              analyticsClient={mockAnalyticsClient}
              match={{ params: { cloudId: 'FAKE' } } as any}
              location={{ search: '' } as any}
              history={{} as any}
            />
          </IntlProvider>
        </ApolloProvider>
      </MemoryRouter>,
    );
    wrapper.find(Link).simulate('click');
    expect(mockAnalyticsClient.sendUIEvent.called).to.equal(true);
    const analyticsArguments = mockAnalyticsClient.sendUIEvent.getCalls()[0].args[0];
    expect(analyticsArguments.cloudId).to.equal('FAKE');
    expect(analyticsArguments.data.attributes.groupId).to.equal('a78e4263-97e2-41d6-8cce-abb508e809c7');
    expect(analyticsArguments.data.attributes.groupCount).to.equal(13);
    expect(analyticsArguments.data.attributes.userCount).to.equal(99);
    expect(analyticsArguments.data.attributes.defaultAccess).to.equal(false);
    expect(analyticsArguments.data.attributes.productAdmin).to.equal(false);
    expect(analyticsArguments.data.attributes.hasSearchQuery).to.equal(false);
    expect(analyticsArguments.data.attributes.external).to.equal(false);
  });
});
