import * as deepEqual from 'deep-equal';
import { parse, stringify } from 'query-string';
import * as React from 'react';
import { ChildProps, graphql, QueryProps } from 'react-apollo';
import {
  defineMessages,
  FormattedMessage,
  InjectedIntlProps,
  injectIntl,
} from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import styled from 'styled-components';

import AkSpinner from '@atlaskit/spinner';
import { gridSize as akGridSize } from '@atlaskit/theme';

import AkButton from '@atlaskit/button';

import {
  analyticsClient,
  AnalyticsClientProps,
  createGroupButtonClickedEventData,
  groupListLearnMoreLinkClickedEventData,
  groupListSearchClickedEventData,
  groupListSearchTypedEventData,
  groupsScreenScreenEvent,
  ScreenEventSender,
  withAnalyticsClient,
} from 'common/analytics';
import { links } from 'common/link-constants';
import { ExternalLink } from 'common/navigation';
import { PageLayout } from 'common/page-layout';
import { getStart, sanitizePage } from 'common/pagination';
import { makeComponentTimingHoc } from 'common/performance-metrics';
import { SearchInput } from 'common/search-input';

import { Group as GroupSchemaType, GroupsPageQuery, IsCurrentAdminSystemAdminQuery } from '../../../schema/schema-types';
import groupsPageQuery from './groups-page.query.graphql';

import { debounce } from '../../../utilities/decorators';
import currentUserQuery from './current-user.query.graphql';
import { GroupCreateDialog } from './group-create-dialog';
import { ROWS_PER_PAGE } from './groups-constants';
import { GroupsPageTable } from './groups-page-table';
import { GroupsPageLocationState } from './groups-page.prop-types';

export const messages = defineMessages({
  title: {
    id: 'groups.page.title',
    defaultMessage: 'Groups',
  },
  description: {
    id: 'groups.page.description',
    defaultMessage: `Manage product access and roles in bulk by adding users to groups that have the required permissions. For example, add users to the default 'administrators' group so they are able to delete Jira issues. {learnMoreLink}`,
  },
  action: {
    id: 'groups.page.action',
    defaultMessage: 'Create group',
  },
  learnMoreLink: {
    id: 'groups.page.learn.more.link',
    defaultMessage: 'Learn more',
  },
  error: {
    id: 'groups.page.error',
    defaultMessage: `We couldn't retrieve the groups for this site. Try again soon.`,
  },
});

const PageDescriptionWrapper = styled.div`
  max-width: ${akGridSize() * 65}px;
`;

const SearchWrapper = styled.div`
  display: flex;
  align-items: flex-start;
`;

export type Group = GroupSchemaType;

interface SiteParams {
  cloudId: string;
}

interface GroupsPageState {
  groups: Group[] | null;
  isCreateDialogOpen?: boolean;
}

interface CurrentUserProps {
  currentUser?: QueryProps & Partial<IsCurrentAdminSystemAdminQuery>;
}

type GroupsPageProps = ChildProps<InjectedIntlProps & RouteComponentProps<SiteParams> & CurrentUserProps, GroupsPageQuery> & AnalyticsClientProps;

export class GroupsPageImpl extends React.Component<GroupsPageProps, GroupsPageState> {
  constructor(props: GroupsPageProps) {
    super(props);

    this.refetchIfFlagProvided();

    if (!props.data || !props.data.groupList || !props.data.groupList.groups) {
      this.state = {
        groups: null,
      };

      return;
    }

    this.state = {
      groups: this.getGroupsPaddedWithPlaceholders(
        getStart(sanitizePage(parse(props.location.search).page), ROWS_PER_PAGE) - 1,
        props.data.groupList.total,
        props.data.groupList.groups,
        props.data.loading,
      ),
    };
  }

  @debounce('onInputChange')
  public searchByDisplayName(displayName: string) {
    const { match: { params: { cloudId } } } = this.props;

    this.updateQueryParams({
      ...parse(this.props.location.search),
      displayName: displayName === '' ? undefined : displayName,
      page: undefined,
    });

    if (!this.props.data) {
      return;
    }

    this.props.data.refetch({ displayName }).then(() => {
      this.props.analyticsClient.sendUIEvent({
        cloudId,
        data: groupListSearchTypedEventData({
          groupCount: this.getGroupTotalWithoutFilter(),
          keyWord: displayName,
        }),
      });
    }).catch(e => {
      analyticsClient.onError(e);
    });
  }

  public componentWillReceiveProps(nextProps: GroupsPageProps) {
    if (!this.props.data || !nextProps.data) {
      return;
    }
    const currentGroups = this.props.data.groupList && this.props.data.groupList.groups;
    const nextGroups = nextProps.data.groupList && nextProps.data.groupList.groups;

    if (this.props.data.loading === nextProps.data.loading && deepEqual(currentGroups, nextGroups)) {
      return;
    }

    if (nextProps.data.loading || nextProps.data.error || !nextProps.data.groupList || !nextProps.data.groupList.groups) {
      return;
    }

    this.setState({
      groups: this.getGroupsPaddedWithPlaceholders(
        getStart(sanitizePage(parse(nextProps.location.search).page), ROWS_PER_PAGE) - 1,
        nextProps.data.groupList.total,
        nextProps.data.groupList.groups,
        nextProps.data.loading,
      ),
    });
  }

  public render() {
    const {
      intl: { formatMessage },
      location: { search },
      data: {
        loading,
        error,
      } = { loading: false, error: null },
    } = this.props;

    const {
      page,
      displayName = '',
    } = parse(search);

    if (error) {
      return (
        <PageLayout>
          <span>{formatMessage(messages.error)}</span>
        </PageLayout>
      );
    }

    if (!this.state.groups && loading) {
      return (
        <PageLayout>
          <AkSpinner />
        </PageLayout>
      );
    }

    return (
      <PageLayout
        title={formatMessage(messages.title)}
        isFullWidth={true}
        description={(
          <PageDescriptionWrapper>
            <FormattedMessage
              {...messages.description}
              values={{
                learnMoreLink: (
                  <ExternalLink
                    onClick={this.onLearnMoreLinkClick}
                    href={links.external.manageGroups}
                    hideReferrer={false}
                  >
                    <FormattedMessage {...messages.learnMoreLink} />
                  </ExternalLink>
                ),
              }}
            />
          </PageDescriptionWrapper>
        )}
        action={(
          <AkButton
            appearance="primary"
            onClick={this.onCreateDialogOpen}
            id="group-creation-button"
          >
            {formatMessage(messages.action)}
          </AkButton>
        )}
      >
        <ScreenEventSender
          isDataLoading={loading}
          event={groupsScreenScreenEvent({ groupCount: this.getGroupTotalWithoutFilter() })}
        >
          <SearchWrapper>
            <SearchInput
              initialValue={displayName}
              onChange={this.onSearch}
              onClick={this.onSearchClick}
              size={60}
            />
          </SearchWrapper>
          <GroupsPageTable
            groups={this.state.groups}
            groupCount={this.getGroupTotalWithoutFilter()}
            rowsPerPage={ROWS_PER_PAGE}
            page={sanitizePage(page)}
            onSetPage={this.onSetPage}
            shouldShowSystemGroups={!!(this.props.currentUser && this.props.currentUser.currentUser && this.props.currentUser.currentUser.isSystemAdmin)}
            isLoading={loading}
            searchValuePresent={!!(displayName && displayName.length > 0)}
          />
          <GroupCreateDialog
            isOpen={!!this.state.isCreateDialogOpen}
            dismiss={this.onCreateDialogDismiss}
          />
        </ScreenEventSender>
      </PageLayout>
    );
  }

  public onSearch = (e: React.FormEvent<HTMLInputElement>) => {
    e.persist();
    this.searchByDisplayName((e.target as HTMLInputElement).value);
  };

  public refetchIfFlagProvided() {
    const {
      history,
      location: { pathname, search, state },
    } = this.props;
    const { refreshGroupList } = (state || {}) as GroupsPageLocationState;

    if (!refreshGroupList) {
      return;
    }

    // Only refetch if there's existing data to nuke
    if (this.props.data && this.props.data.groupList && this.props.data.groupList.groups) {
      this.props.data.refetch().catch();
    }

    // Clear flag so that unnecessary refetches don't occur
    history.replace(`${pathname}${search}`, {});
  }

  private onSearchClick = () => {
    const { match: { params: { cloudId } } } = this.props;
    this.props.analyticsClient.sendUIEvent({
      cloudId,
      data: groupListSearchClickedEventData({
        groupCount: this.getGroupTotalWithoutFilter(),
      }),
    });
  }

  private onLearnMoreLinkClick = () => {
    const { match: { params: { cloudId } } } = this.props;
    this.props.analyticsClient.sendUIEvent({
      cloudId,
      data: groupListLearnMoreLinkClickedEventData(),
    });
  };

  private onCreateDialogDismiss = () => this.setState({ isCreateDialogOpen: false });

  private getGroupTotalWithoutFilter() {
    return this.props.data && this.props.data.groupListWithoutFilter && this.props.data.groupListWithoutFilter.total;
  }

  private onCreateDialogOpen = () => {
    const { match: { params: { cloudId } } } = this.props;
    this.props.analyticsClient.sendUIEvent({
      cloudId,
      data: createGroupButtonClickedEventData({
        groupCount: this.getGroupTotalWithoutFilter(),
      }),
    });

    this.setState({ isCreateDialogOpen: true });
  };

  private onSetPage = async (page: number) => {
    const { data, location } = this.props;

    this.updateQueryParams({
      ...parse(location.search),
      page,
    });

    return data!.fetchMore({
      variables: {
        start: getStart(page, ROWS_PER_PAGE),
      },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        return {
          ...previousResult,
          ...fetchMoreResult,
        };
      },
    });
  };

  private updateQueryParams = (params: object) => {
    const { history, location } = this.props;
    history.replace(`${location.pathname}?${stringify(params)}`);
  };

  private getGroupsPaddedWithPlaceholders = (start: number, total: number, groups: Group[], isLoading: boolean) => {
    const paddedGroups: Group[] = Array(total).fill(null);

    if (!isLoading) {
      paddedGroups.splice(start, groups.length, ...groups);
    }

    return paddedGroups;
  };
}

const withCurrentUser = graphql<GroupsPageProps, any>(currentUserQuery, {
  name: 'currentUser',
  options: ({ match: { params: { cloudId } } }) => ({
    variables: {
      cloudId,
    },
  }),
});

const withGroupsData = graphql<GroupsPageProps, GroupsPageQuery>(groupsPageQuery, {
  options: ({ match: { params: { cloudId } } }) => {
    const { page, displayName } = parse(location.search);

    return {
      variables: {
        cloudId,
        count: ROWS_PER_PAGE,
        start: getStart(sanitizePage(page), ROWS_PER_PAGE),
        displayName,
      },
    };
  },
});

const withComponentTiming = makeComponentTimingHoc<GroupsPageProps>({
  id: 'group-page',
  category: 'page',
  isMeaningful: true,
  isLoaded: (props => props.data ? !props.data.loading : false),
});

export const GroupsPage = injectIntl<GroupsPageProps>(
  withRouter<GroupsPageProps>(
    withCurrentUser(
      withGroupsData(
        withAnalyticsClient<GroupsPageProps>(
          withComponentTiming(
            GroupsPageImpl,
         ),
        ),
      ),
    ),
  ),
);
