import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { sandbox as sinonSandbox } from 'sinon';

import { Button } from 'common/analytics';
import { ModalDialog } from 'common/modal';

import { createMockIntlContext, createMockIntlProp } from '../../../utilities/testing';
import { GroupEditDialogImpl, messages } from './group-edit-dialog';

describe('GroupEditDialog', () => {
  const sandbox = sinonSandbox.create();
  let dismissSpy: sinon.SinonSpy;
  let updateSpy: sinon.SinonSpy;

  beforeEach(() => {
    dismissSpy = sandbox.spy();
    updateSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  function shallowGroupEditDialog() {
    return shallow(
      <GroupEditDialogImpl
        dismiss={dismissSpy}
        intl={createMockIntlProp()}
        isOpen={true}
        update={updateSpy}
        {...{} as any}
      />,
      createMockIntlContext(),
    );
  }

  it('renders with title and description', () => {
    const modal = shallowGroupEditDialog().find(ModalDialog);
    expect(modal.prop('header')).to.equal(messages.title.defaultMessage);
    expect(modal.find(FormattedMessage).first().prop('defaultMessage')).to.equal(messages.description.defaultMessage);
  });

  it('renders with a save button and a cancel button', () => {
    const modalFooterButtons = mount(shallowGroupEditDialog().find(ModalDialog).prop('footer') as React.ReactElement<any>).find(Button);

    expect(modalFooterButtons).to.have.length(2);

    const saveButton = modalFooterButtons.first();
    const saveButtonProps = saveButton.props();

    expect(saveButton.text()).to.equal(messages.buttonSave.defaultMessage);
    expect(saveButtonProps.appearance).to.equal('primary');

    const cancelButton = modalFooterButtons.last();
    const cancelButtonProps = cancelButton.props();

    expect(cancelButton.text()).to.equal(messages.buttonCancel.defaultMessage);
    expect(cancelButtonProps.appearance).to.equal('subtle-link');
  });

  describe('create button click', () => {
    it('calls onSubmit', () => {
      const onSubmitSpy = sandbox.spy();
      class GroupEditDialogImplWithSpy extends GroupEditDialogImpl {
        public onSubmit = onSubmitSpy;
      }
      const wrapper = shallow(
        <GroupEditDialogImplWithSpy
          dismiss={dismissSpy}
          intl={createMockIntlProp()}
          isOpen={true}
          update={updateSpy}
          {...{} as any}
        />,
        createMockIntlContext(),
      );
      const modalFooter = shallow(wrapper.find(ModalDialog).prop('footer') as React.ReactElement<any>);
      const createButton = modalFooter.find(Button).first();

      createButton.simulate('click');

      expect(onSubmitSpy.called).to.equal(true);
    });
  });

  describe('cancel button click', () => {
    it('calls onCancel', () => {
      const onCancelSpy = sandbox.spy();
      // tslint:disable-next-line:max-classes-per-file
      class GroupEditDialogImplWithSpy extends GroupEditDialogImpl {
        public onCancel = onCancelSpy;
      }
      const wrapper = shallow(
        <GroupEditDialogImplWithSpy
          dismiss={dismissSpy}
          intl={createMockIntlProp()}
          isOpen={true}
          update={updateSpy}
          {...{} as any}
        />,
        createMockIntlContext(),
      );
      const modalFooter = shallow(wrapper.find(ModalDialog).prop('footer') as React.ReactElement<any>);
      const cancelButton = modalFooter.find(Button).last();

      cancelButton.simulate('click');

      expect(onCancelSpy.called).to.equal(true);
    });
  });

  describe('onCancel', () => {
    it('calls dismiss', () => {
      const wrapper = shallowGroupEditDialog();
      (wrapper.instance() as any).onCancel();

      expect(dismissSpy.called).to.equal(true);
    });
  });

  describe('onSubmit', () => {
    it('calls update', () => {
      const state = {
        description: 'Awesome description',
        name: 'group-name',
      };
      const wrapper = shallowGroupEditDialog();
      wrapper.setState(state);
      (wrapper.instance() as any).onSubmit();

      expect(updateSpy.called).to.equal(true);
      expect(updateSpy.calledWith(state)).to.equal(true);
    });
  });
});
