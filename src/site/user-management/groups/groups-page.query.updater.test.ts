import { expect } from 'chai';
import * as sinon from 'sinon';

import { Group, GroupsPageQuery } from '../../../schema/schema-types';
import {
  optimisticallyAddGroup,
  optimisticallyRemoveGroup,
} from './groups-page.query.updater';

describe('Groups page query updater', () => {
  let writeQuerySpy: sinon.SinonSpy;

  const sandbox = sinon.sandbox.create();

  beforeEach(() => {
    writeQuerySpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  const DUMMY_CLOUD_ID = 'DUMMY_CLOUD_ID';

  const generateStore = () => {
    const groups: Group[] = [
      {
        description: 'Sad corgi description.',
        id: '1',
        name: 'Sad corgi',
        productPermissions: [],
        defaultForProducts: [],
        sitePrivilege: 'SYS_ADMIN',
        unmodifiable: false,
        userTotal: 1,
        managementAccess: 'ALL',
        ownerType: null,
      },
    ];

    return {
      readQuery: sinon.stub().returns({
        groupList: {
          total: 1,
          groups,
        },
        groupListWithoutFilter: {
          total: groups.length,
        },
      } as GroupsPageQuery),

      writeQuery: writeQuerySpy,
    } as any;
  };

  describe('optimisticallyAddGroup', () => {
    it('should add the group to the list and increment the total by 1', () => {
      const store = generateStore();
      const newGroup: Group = {
        description: 'Excited corgi description.',
        id: '250',
        name: 'Excited corgi',
        productPermissions: [],
        defaultForProducts: [],
        sitePrivilege: 'SYS_ADMIN',
        unmodifiable: false,
        userTotal: 1,
        managementAccess: 'ALL',
        ownerType: null,
      };

      optimisticallyAddGroup(store, DUMMY_CLOUD_ID, newGroup);

      expect(writeQuerySpy.callCount).to.equal(1);
      expect(writeQuerySpy.getCall(0).args[0].data.groupList.groups[0].id).to.equal('250');
      expect(writeQuerySpy.getCall(0).args[0].data.groupList.groups[0].name).to.equal('Excited corgi');
      expect(writeQuerySpy.getCall(0).args[0].data.groupList.total).to.equal(2);
    });

    it('should increment the groupListWithoutFilter total by 1', () => {
      const store = generateStore();
      const newGroup: Group = {
        description: 'Stoked corgi description.',
        id: '300',
        name: 'Stoked corgi',
        productPermissions: [],
        defaultForProducts: [],
        sitePrivilege: 'SYS_ADMIN',
        unmodifiable: false,
        userTotal: 1,
        managementAccess: 'ALL',
        ownerType: null,
      };

      optimisticallyAddGroup(store, DUMMY_CLOUD_ID, newGroup);

      expect(writeQuerySpy.callCount).to.equal(1);
      expect(writeQuerySpy.getCall(0).args[0].data.groupListWithoutFilter.total).to.equal(2);
    });
  });

  describe('optimisticallyRemoveGroup', () => {
    it('should remove the group from the list and decrement the total by 1', () => {
      const store = generateStore();

      optimisticallyRemoveGroup(store, DUMMY_CLOUD_ID, '1');

      expect(writeQuerySpy.callCount).to.equal(1);
      expect(writeQuerySpy.getCall(0).args[0].data.groupList.groups.length).to.equal(0);
      expect(writeQuerySpy.getCall(0).args[0].data.groupList.total).to.equal(0);
    });

    it('should decrement the groupListWithoutFilter total by 1', () => {
      const store = generateStore();

      optimisticallyRemoveGroup(store, DUMMY_CLOUD_ID, '1');

      expect(writeQuerySpy.callCount).to.equal(1);
      expect(writeQuerySpy.getCall(0).args[0].data.groupListWithoutFilter.total).to.equal(0);
    });
  });
});
