import { assert, expect } from 'chai';
import { mount, shallow } from 'enzyme';
import { Location } from 'history';
import * as React from 'react';
import { FormattedMessage, IntlProvider } from 'react-intl';
import { sandbox as sinonSandbox } from 'sinon';
import { ThemeProvider } from 'styled-components';

import AkButton from '@atlaskit/button';
import AkInlineDialog from '@atlaskit/inline-dialog';
import { GridColumn as AkGridColumn } from '@atlaskit/page';
import AkSpinner from '@atlaskit/spinner';

import { GroupName } from 'common/group-name';
import { PageLayout } from 'common/page-layout';

import { Group, ManagementAccess, UpdateGroupInput } from '../../../schema/schema-types/schema-types';
import { createMockAnalyticsClient, createMockContext, createMockIntlProp } from '../../../utilities/testing';
import { GroupDetailPageImpl, messages } from './group-detail-page';
import { GroupMemberList } from './group-member-list';

describe('GroupDetailPage', () => {
  const successfulMutation = Promise.resolve({ data: { updateGroup: { id: 'foo', name: 'bar', description: 'baz' } } });
  const successfulRemoval = Promise.resolve();
  const mockedLocation = {
    hash: '',
    pathname: '/groups/foo',
    search: '',
    state: {},
  } as Location;

  const sandbox = sinonSandbox.create();
  let showFlagSpy;
  let refetchSpy;
  let mutateMock;
  let removeMock;
  let historyPushSpy: sinon.SinonSpy;
  let historyReplaceSpy: sinon.SinonSpy;

  beforeEach(() => {
    showFlagSpy = sandbox.spy();
    refetchSpy = sandbox.stub().returns(Promise.resolve());
    mutateMock = sandbox.stub().returns(successfulMutation);
    removeMock = sandbox.stub().returns(successfulRemoval);
    historyPushSpy = sandbox.spy();
    historyReplaceSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  function shallowGroupDetailPageWithData(data = {}, location: Location = mockedLocation) {
    return shallow(
      <ThemeProvider theme={{}}>
        <GroupDetailPageImpl
          data={{ refetch: refetchSpy, ...data } as any}
          history={{ push: historyPushSpy, replace: historyReplaceSpy } as any}
          showFlag={showFlagSpy}
          hideFlag={() => null}
          location={location}
          match={{ params: { cloudId: 'FAKE', groupId: 'foo' } } as any}
          remove={removeMock}
          update={mutateMock}
          intl={createMockIntlProp()}
          analyticsClient={createMockAnalyticsClient()}
        />
      </ThemeProvider>,
      createMockContext({ intl: true, apollo: true }),
    ).find(GroupDetailPageImpl).dive();
  }

  it('sets isAddMembersDialogOpen and calls history.replace when showAddMembersModal is true', () => {
    const wrapper = shallowGroupDetailPageWithData({ loading: true }, {
      ...mockedLocation,
      state: { showAddMembersModal: true },
    });
    expect(wrapper.state('isAddMembersDialogOpen')).to.equal(true);
    expect(historyReplaceSpy.calledWith(`${mockedLocation.pathname}${mockedLocation.search}`, {})).to.equal(true);
  });

  describe('before group details have been fetched', () => {
    it('renders with no title, no description and no side', () => {
      const wrapper = shallowGroupDetailPageWithData({ loading: true });
      const pageLayout = wrapper.find(PageLayout);
      const pageLayoutProps = pageLayout.props();
      expect(pageLayoutProps.title).to.equal(undefined);
      expect(pageLayoutProps.description).to.equal(undefined);
      expect(pageLayoutProps.side).to.equal(undefined);
    });

    it('renders a spinner', () => {
      const isLoading = { loading: true };
      const wrapper = shallowGroupDetailPageWithData(isLoading, mockedLocation);
      expect(wrapper.find(AkSpinner)).to.have.length(1);
    });
  });

  describe('after group details have been fetched', () => {
    const managementAccess: ManagementAccess = 'ALL';
    const group = {
      groupDetails: {
        id: 'foo',
        name: 'group name',
        description: 'group description',
        productPermissions: [],
        defaultForProducts: [],
        sitePrivilege: 'NONE',
        managementAccess,
      },
      members: {
        total: 0,
        users: [],
      },
    };

    it('renders with title, description, actions and side', () => {
      const wrapper = shallowGroupDetailPageWithData({ group });
      const pageLayout = wrapper.find(PageLayout);
      const pageLayoutProps = pageLayout.props();
      expect(mount(
        <IntlProvider locale="en">
          {pageLayoutProps.title}
        </IntlProvider> as any).find(GroupName).prop('name')).to.equal(group.groupDetails.name);
      expect(
        mount(<ThemeProvider theme={{}}>{pageLayoutProps.description}</ThemeProvider>)
          .find(AkGridColumn)
          .text(),
      ).to.equal(group.groupDetails.description);
      expect(mount(pageLayoutProps.action as any).find(AkButton)).to.have.length(3);
    });

    it('renders without a spinner', () => {
      const wrapper = shallowGroupDetailPageWithData({ group });
      expect(wrapper.find(AkSpinner)).to.have.length(0);
    });

    it('shows an error message if something went wrong', () => {
      const wrapper = shallowGroupDetailPageWithData({ error: 'Something failed' });
      expect(wrapper.find('span').text()).to.equal(messages.error.defaultMessage);
    });

    describe('with no group members', () => {
      const wrapper = shallowGroupDetailPageWithData({ group });

      it('shows a primary "Add members" button', () => {
        const actions = wrapper.find(PageLayout).props().action;
        expect(!!actions).to.equal(true);
        const shallowActions = shallow(actions!);
        expect(shallowActions.exists()).to.equal(true);
        const buttons = shallowActions.find(AkButton);
        const button = buttons.filterWhere(btn => btn.children().text() === 'Add members');
        expect(button.exists()).to.equal(true);
        expect(button.props().appearance).to.equal('primary');
      });

      it('shows a message indicating that there are no group members', () => {
        expect(wrapper.find(FormattedMessage).filter('[tagName="p"]').prop('defaultMessage')).to.equal(messages.noMembers.defaultMessage);
      });
    });

    describe('with group members', () => {
      const wrapper = shallowGroupDetailPageWithData({
        group: {
          ...group, members: {
            total: 2,
            users: [
              {
                active: true,
                id: '1',
                displayName: 'Rick Sanchez',
                email: 'rsanchez@c-137.com',
              },
              {
                active: true,
                id: '2',
                displayName: 'Morty Smith',
                email: 'msmith@c-137.com',
              },
            ],
          },
        },
      });

      it('shows a default "Add members" button', () => {
        const actions = wrapper.find(PageLayout).props().action;
        expect(!!actions).to.equal(true);
        const shallowActions = shallow(actions!);
        expect(shallowActions.exists()).to.equal(true);
        const buttons = shallowActions.find(AkButton);
        const button = buttons.filterWhere(btn => btn.children().text() === 'Add members');
        expect(button.exists()).to.equal(true);
        expect(button.props().appearance).to.equal('default');
      });

      it('shows a members table', () => {
        expect(wrapper.find(GroupMemberList)).to.have.length(1);
      });
    });
  });

  describe('edit button', () => {
    const group = {
      groupDetails: {
        id: 'foo',
        name: 'group name',
        description: 'group description',
        productPermissions: [],
        defaultForProducts: [],
        sitePrivilege: 'NONE',
        unmodifiable: false,
        userTotal: 0,
        managementAccess: 'ALL',
        ownerType: null,
      } as Group,
      members: {
        total: 0,
        users: [],
      },
    };

    it('is disabled if the group is unmodifiable', () => {
      const wrapper = shallowGroupDetailPageWithData({
        group: {
          ...group,
          groupDetails: {
            ...group.groupDetails,
            unmodifiable: true,
          },
        },
      });
      const pageLayoutProps = wrapper.find(PageLayout).props();
      expect(mount(pageLayoutProps.action as any).find(AkButton).at(1).prop('isDisabled')).to.equal(true);
    });

    it('is disabled if the group is a SCIM group', () => {
      const wrapper = shallowGroupDetailPageWithData({
        group: {
          ...group,
          groupDetails: {
            ...group.groupDetails,
            managementAccess: 'READ_ONLY',
            ownerType: 'EXT_SCIM',
          },
        },
      });

      const pageLayoutProps = wrapper.find(PageLayout).props();
      expect(mount(pageLayoutProps.action as any).find(AkButton).at(1).prop('isDisabled')).to.equal(true);
    });

    it('sets isEditDialogOpen to true on click', () => {
      const wrapper = shallowGroupDetailPageWithData({ group });
      const pageLayoutProps = wrapper.find(PageLayout).props();
      const editButton = mount(pageLayoutProps.action as any).find(AkButton).at(1);

      expect(wrapper.state('isEditDialogOpen')).to.equal(false);
      editButton.simulate('click');
      expect(wrapper.state('isEditDialogOpen')).to.equal(true);
    });
  });

  describe('delete button', () => {
    const group = {
      groupDetails: {
        id: 'foo',
        name: 'group name',
        description: 'group description',
        productPermissions: [],
        defaultForProducts: [],
        sitePrivilege: 'NONE',
        unmodifiable: false,
        userTotal: 0,
        managementAccess: 'ALL',
        ownerType: null,
      } as Group,
      members: {
        total: 0,
        users: [],
      },
    };

    it('is disabled if the group is a SCIM group', () => {
      const wrapper = shallowGroupDetailPageWithData({
        group: {
          ...group,
          groupDetails: {
            ...group.groupDetails,
            managementAccess: 'READ_ONLY',
            ownerType: 'EXT_SCIM',
          },
        },
      });

      const pageLayoutProps = wrapper.find(PageLayout).props();
      expect(mount(pageLayoutProps.action as any).find(AkButton).last().prop('isDisabled')).to.equal(true);
    });
  });

  describe('add members button', () => {
    const group = {
      groupDetails: {
        id: 'foo',
        name: 'group name',
        description: 'group description',
        productPermissions: [],
        defaultForProducts: [],
        sitePrivilege: 'NONE',
        unmodifiable: false,
        userTotal: 0,
        managementAccess: 'ALL',
        ownerType: null,
      } as Group,
      members: {
        total: 0,
        users: [],
      },
    };

    it('is disabled if the group is a SCIM group', () => {
      const wrapper = shallowGroupDetailPageWithData({
        group: {
          ...group,
          groupDetails: {
            ...group.groupDetails,
            managementAccess: 'READ_ONLY',
            ownerType: 'EXT_SCIM',
          },
        },
      });

      const actions = wrapper.find(PageLayout).props().action;
      expect(!!actions).to.equal(true);
      const shallowActions = shallow(actions!);
      expect(shallowActions.exists()).to.equal(true);
      const buttons = shallowActions.find(AkButton);
      const button = buttons.filterWhere(btn => btn.children().text() === 'Add members');
      expect(button.exists()).to.equal(true);
      expect(button.props().isDisabled).to.equal(true);
      const inlineDialogs = shallowActions.find(AkInlineDialog);
      const inlineDialog = inlineDialogs.filterWhere(dialog => (dialog.props() as any)
        .content === `You can't add members to this group because it is synced from an external directory.`);
      expect(inlineDialog.exists()).to.equal(true);
    });

    it('is disabled if the group is read only and owned by an ORG', () => {
      const wrapper = shallowGroupDetailPageWithData(
        {
          group: {
            ...group,
            groupDetails: {
              ...group.groupDetails,
              managementAccess: 'READ_ONLY',
              ownerType: 'ORG',
            },
          },
        },
        mockedLocation,
      );

      const actions = wrapper.find(PageLayout).props().action;
      expect(!!actions).to.equal(true);
      const shallowActions = shallow(actions!);
      expect(shallowActions.exists()).to.equal(true);
      const buttons = shallowActions.find(AkButton);
      const button = buttons.filterWhere(btn => btn.children().text() === 'Add members');
      expect(button.exists()).to.equal(true);
      expect(button.props().isDisabled).to.equal(true);
      const inlineDialogs = shallowActions.find(AkInlineDialog);
      const inlineDialog = inlineDialogs.filterWhere(dialog => (dialog.props() as any)
        .content === `You can't add members to this group because this site is linked to an organization.`);
      expect(inlineDialog.exists()).to.equal(true);
    });

    it('is disabled if the group is unmodifiable or has system administrator privileges', () => {
      const wrapper = shallowGroupDetailPageWithData(
        {
          group: {
            ...group,
            groupDetails: {
              ...group.groupDetails,
              sitePrivilege: 'SYS_ADMIN',
            },
          },
        },
        mockedLocation,
      );

      const actions = wrapper.find(PageLayout).props().action;
      expect(!!actions).to.equal(true);
      const shallowActions = shallow(actions!);
      expect(shallowActions.exists()).to.equal(true);
      const buttons = shallowActions.find(AkButton);
      const button = buttons.filterWhere(btn => btn.children().text() === 'Add members');
      expect(button.exists()).to.equal(true);
      expect(button.props().isDisabled).to.equal(true);
      const inlineDialogs = shallowActions.find(AkInlineDialog);
      const inlineDialog = inlineDialogs.filterWhere(dialog => (dialog.props() as any)
        .content === `You can't add members to this group because it is restricted to system administrators.`);
      expect(inlineDialog.exists()).to.equal(true);
    });
  });

  describe('onUpdate', () => {
    function submitDialog(input: UpdateGroupInput = { name: 'foo', description: 'bar' }) {
      const wrapper = shallowGroupDetailPageWithData({
        group: {
          groupDetails: {
            id: 'foo',
            name: 'group name',
            description: 'group description',
            productPermissions: [],
            defaultForProducts: [],
            sitePrivilege: 'NONE',
          },
          members: {
            total: 0,
            users: [],
          },
        },
      });
      (wrapper.instance() as any).onUpdate(input);

      return wrapper;
    }

    it('sets isSaving to true and isEditDialogOpen to false', () => {
      mutateMock = () => ({ then: () => ({ catch: () => ({}) }) });
      const wrapper = submitDialog();
      expect(wrapper.state('isSaving')).to.equal(true);
      expect(wrapper.state('isEditDialogOpen')).to.equal(false);
    });

    describe('on success', () => {
      it('sets isSaving to false', async () => {
        const wrapper = submitDialog();
        await successfulMutation;
        expect(wrapper.state('isSaving')).to.equal(false);
      });

      it('calls showFlag', async () => {
        submitDialog();
        await successfulMutation;
        expect(showFlagSpy.called).to.equal(true);
      });
      it('does not call refetch', async () => {
        submitDialog();
        await successfulMutation;
        expect(refetchSpy.called).to.equal(false);
      });
    });

    describe('on failure', () => {
      it('sets isSaving to false', async () => {
        const failedMutation = Promise.reject(new Error('uh oh'));
        mutateMock = sandbox.stub().returns(failedMutation);
        const wrapper = submitDialog();
        await failedMutation
          .then(() => assert.fail())
          .catch(e => {
            expect(e instanceof Error).to.equal(true);
          });
        expect(wrapper.state('isSaving')).to.equal(false);
      });
      it('calls refetch', async () => {
        const failedMutation = Promise.reject(new Error('uh oh'));
        mutateMock = sandbox.stub().returns(failedMutation);
        submitDialog();
        await failedMutation
          .then(() => assert.fail())
          .catch(e => {
            expect(e instanceof Error).to.equal(true);
          });
        expect(refetchSpy.called).to.equal(true);
      });
    });
  });

  describe('onDelete', () => {
    function submitDialog() {
      const wrapper = shallowGroupDetailPageWithData({
        group: {
          groupDetails: {
            id: 'foo',
            name: 'group name',
            description: 'group description',
            productPermissions: [],
            defaultForProducts: [],
            sitePrivilege: 'NONE',
          },
          members: {
            total: 0,
            users: [],
          },
        },
      });
      (wrapper.instance() as any).onDelete();

      return wrapper;
    }

    it('sets isDeleting to true', () => {
      removeMock = () => ({ then: () => ({ catch: () => ({}) }) });
      const wrapper = submitDialog();
      expect(wrapper.state('isDeleting')).to.equal(true);
    });

    describe('on success', () => {
      it('sets isDeleting to false and isDeleteDialogOpen to false', async () => {
        const wrapper = submitDialog();
        await successfulRemoval;
        expect(wrapper.state('isDeleting')).to.equal(false);
        expect(wrapper.state('isDeleteDialogOpen')).to.equal(false);
      });

      it('calls showFlag', async () => {
        submitDialog();
        await successfulRemoval;
        expect(showFlagSpy.called).to.equal(true);
      });

      it('calls history.push', async () => {
        submitDialog();
        await successfulRemoval;
        expect(historyPushSpy.called).to.equal(true);
      });
    });

    describe('on failure', () => {
      it('sets isDeleting to false', async () => {
        const failedMutation = Promise.reject(new Error('uh oh'));
        removeMock = sandbox.stub().returns(failedMutation);
        const wrapper = submitDialog();
        await failedMutation
          .then(() => assert.fail())
          .catch(e => {
            expect(e instanceof Error).to.equal(true);
          });
        expect(wrapper.state('isDeleting')).to.equal(false);
      });
    });
  });
});
