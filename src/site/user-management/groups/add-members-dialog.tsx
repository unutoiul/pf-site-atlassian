import * as React from 'react';
import { ChildProps, graphql, QueryProps } from 'react-apollo';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import AkButton, { ButtonGroup as AkButtonGroup } from '@atlaskit/button';

import {
  addMembersCancelButtonClickedEventData,
  addMembersConfirmButtonClickedEventData,
  addMembersToGroupModalScreenEvent,
  addMemberTrackEventData,
  analyticsClient,
  AnalyticsClientProps,
  ScreenEventSender,
  withAnalyticsClient,
} from 'common/analytics';
import { createSuccessIcon, GenericErrorMessage } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';
import { Picker, User, UserSelectedItem, UserSuggestionItem } from 'common/picker';

import currentUserQuery from './current-user.query.graphql';

import { debounce } from '../../../utilities/decorators';

import { AddMembersDialogQuery, AddMembersToGroupsMutation, AddMembersToGroupsMutationVariables, IsCurrentAdminSystemAdminQuery, IsCurrentAdminSystemAdminQueryVariables } from '../../../schema/schema-types';
import addMembersDialogMutation from './add-members-dialog.mutation.graphql';
import addMembersDialogQuery from './add-members-dialog.query.graphql';
import { optimisticallyAddUsers } from './group-details.query.updater';

export const messages = defineMessages({
  title: {
    id: 'user-management.groups.add-members.title',
    defaultMessage: 'Add members to {name}',
  },
  buttonClose: {
    id: 'user-management.groups.add-members.button.close',
    defaultMessage: 'Close',
  },
  inputLabelText: {
    id: 'user-management.groups.add-members.label.text',
    defaultMessage: 'People to add',
    description: 'What the users search input text is.',
  },
  placeholderText: {
    id: 'user-management.groups.add-members.placeholder.text',
    defaultMessage: 'Search',
    description: 'Search input default text.',
  },
  addButton: {
    id: 'users.add.user.member.to.group.add.button',
    defaultMessage: 'Add',
    description: 'Add users to a group action for the popup modal.',
  },
  cancelButton: {
    id: 'users.add.user.members.to.group.cancel.button',
    defaultMessage: 'Cancel',
    description: 'Cancel action for the add users to a group popup modal.',
  },
  successFlagTitle: {
    id: 'users.add.users.to.group.success.flag.title',
    defaultMessage: '{usersCount, plural, one {Member} other {Members}} successfully added to group',
    description: 'Title to show in the success flag when the users have been added to the group.',
  },
  successFlagDescriptionSingle: {
    id: 'users.add.users.to.group.success.flag.description.single',
    defaultMessage: 'You added {displayName} to the {groupName} group.',
    description: 'Description to show in the success flag when one user has been added to the group.',
  },
  successFlagDescriptionTwo: {
    id: 'users.add.users.to.group.success.flag.description.two',
    defaultMessage: 'You added {displayName} and {displayNameSecond} to the {groupName} group.',
    description: 'Description to show in the success flag when two users have been added to the group.',
  },
  successFlagDescriptionMany: {
    id: 'users.add.users.to.group.success.flag.description.many',
    defaultMessage: 'You added {displayName} and {usersCount} others to the {groupName} group.',
    description: 'Description to show in the success flag when more than two users have been added to the group.',
  },
});

interface OwnProps {
  isOpen: boolean;
  name: string;
  groupId: string;
  memberCount: number;
  cloudId: string;
  dismiss(): void;
}

interface OwnState {
  selectedUsers: User[];
  searchQuery: string | null;
  isMutationLoading: boolean;
}

interface CurrentUserProps {
  currentUser?: QueryProps & Partial<IsCurrentAdminSystemAdminQuery>;
}

type AddMembersDialogProps = ChildProps<OwnProps & CurrentUserProps, AddMembersDialogQuery> & InjectedIntlProps & FlagProps & AnalyticsClientProps;

export class AddMembersDialogImpl extends React.Component<AddMembersDialogProps, OwnState> {
  public readonly state: Readonly<OwnState> = {
    selectedUsers: [],
    searchQuery: null,
    isMutationLoading: false,
  };

  public render() {
    const {
      intl: { formatMessage },
      isOpen,
      name,
    } = this.props;

    if (!this.props.data) {
      return undefined;
    }

    if (!isOpen) {
      return null;
    }

    const users = this.props.data.siteUsersList && this.props.data.siteUsersList.users ? this.props.data.siteUsersList.users : [];

    return (
      <ScreenEventSender
        isDataLoading={!this.props.data || this.props.data.loading}
        event={addMembersToGroupModalScreenEvent({
          groupId: this.props.groupId,
        })}
      >
        <Picker
          isOpen={isOpen}
          title={formatMessage(messages.title, { name })}
          inputLabelText={formatMessage(messages.inputLabelText)}
          placeholderText={formatMessage(messages.placeholderText)}
          items={this.convertToPickerUsersWithQuery(users)}
          loading={this.props.data.loading}
          errorMessage={this.props.data.error && <GenericErrorMessage />}
          renderItem={this.renderItem}
          renderItemPreview={this.renderItemPreview}
          onSelect={this.onSelect}
          onRemove={this.onRemove}
          onQueryChange={this.onQueryChange}
          renderFooter={this.renderFooter}
          onDialogDismissed={this.onCancel}
        />
      </ScreenEventSender>
    );
  }

  public onCancel = () => {
    this.props.dismiss();
    this.props.analyticsClient.sendUIEvent({
      cloudId: this.props.cloudId,
      data: addMembersCancelButtonClickedEventData({
        groupId: this.props.groupId,
        memberCount: this.props.memberCount,
      }),
    });
  }

  @debounce('onInputChange')
  private debouncedRefetch() {
    if (!this.props.data || !this.props.data.refetch || !this.shouldRefetchOnFilter()) {
      return;
    }

    this.props.data.refetch({
      cloudId: this.props.cloudId,
      input: {
        displayName: this.state.searchQuery,
        start: 1,
        count: 100,
      },
    }).catch(error => analyticsClient.onError(error));
  }

  private convertToPickerUsersWithQuery = (users): User[] => {
    if (!users) {
      return [];
    }

    const isSystemAdmin = this.props.currentUser && this.props.currentUser.currentUser && this.props.currentUser.currentUser.isSystemAdmin;

    const output = users.filter(user => !user.system || isSystemAdmin).map(user => ({
      id: user.id,
      displayName: user.displayName,
      email: user.email,
    } as User));

    if (this.shouldRefetchOnFilter()) {
      return output;
    }

    return output.filter((user: User) => {
      const { searchQuery = '' } = this.state;

      if (!searchQuery || !searchQuery.length) {
        return true;
      } else {
        return user.displayName.toLowerCase().includes(searchQuery.toLowerCase());
      }
    });
  };

  private onAddButtonClicked = () => {
    if (!this.props.mutate) {
      return;
    }

    this.setState({ isMutationLoading: true });

    this.props.mutate({
      variables: {
        cloudId: this.props.cloudId,
        input: {
          users: this.state.selectedUsers.map(user => user.id),
          groups: [this.props.groupId],
        },
      } as AddMembersToGroupsMutationVariables,
      update: (store) => {
        optimisticallyAddUsers(store, this.props.cloudId, this.props.groupId, this.state.selectedUsers);
      },
    }).then(() => {
      this.props.dismiss();

      this.props.analyticsClient.sendUIEvent({
        cloudId: this.props.cloudId,
        data: addMembersConfirmButtonClickedEventData({
          groupId: this.props.groupId,
          memberCount: this.props.memberCount,
          membersAddedCount: this.state.selectedUsers.length,
        }),
      });
      this.props.analyticsClient.sendTrackEvent({
        cloudId: this.props.cloudId,
        data: addMemberTrackEventData({
          source: 'groupsScreen',
          groupId: this.props.groupId,
          memberCountAdded: this.state.selectedUsers.length,
        }),
      });
      this.showSuccessFlag();
      this.setState({
        isMutationLoading: false,
        selectedUsers: [],
      });
    }).catch(() => {
      this.props.dismiss();
      this.setState({
        isMutationLoading: false,
        selectedUsers: [],
      });
    });
  };

  private showSuccessFlag = () => {
    const { formatMessage } = this.props.intl;

    const defaultMsgProps = {
      groupName: this.props.name,
      usersCount: this.state.selectedUsers.length,
      displayName: this.state.selectedUsers[0].displayName,
    };

    const defaultFlagProps = {
      autoDismiss: true,
      icon: createSuccessIcon(),
      id: `users.add.user.to.group.success.flag.${Date.now()}`,
      title: formatMessage(messages.successFlagTitle, {
        usersCount: defaultMsgProps.usersCount,
      }),
    };

    if (defaultMsgProps.usersCount === 1) {
      this.props.showFlag({
        ...defaultFlagProps,
        description: formatMessage(messages.successFlagDescriptionSingle, defaultMsgProps),
      });
    } else if (defaultMsgProps.usersCount === 2) {
      this.props.showFlag({
        ...defaultFlagProps,
        description: formatMessage(messages.successFlagDescriptionTwo, {
          ...defaultMsgProps,
          displayNameSecond: this.state.selectedUsers[1].displayName,
        }),
      });
    } else {
      this.props.showFlag({
        ...defaultFlagProps,
        description: formatMessage(messages.successFlagDescriptionMany, {
          ...defaultMsgProps,
          usersCount: defaultMsgProps.usersCount - 1,
        }),
      });
    }
  };

  private renderItem = (item: User, isHighlighted, isSelected, select) => {
    if (isSelected) {
      return null;
    }

    return <UserSuggestionItem {...{ item, isHighlighted, select }} />;
  };

  private renderItemPreview = (item: User, unselect) => {
    return <UserSelectedItem {...{ item, unselect }} />;
  };

  private renderFooter = () => {
    const { formatMessage } = this.props.intl;

    return (
      <AkButtonGroup>
        <AkButton
          appearance="primary"
          onClick={this.onAddButtonClicked}
          isDisabled={!this.state.selectedUsers.length}
          isLoading={!!(this.state.isMutationLoading && this.state.selectedUsers.length)}
        >
          {formatMessage(messages.addButton)}
        </AkButton>
        <AkButton
          appearance="subtle-link"
          onClick={this.onCancel}
        >
          {formatMessage(messages.cancelButton)}
        </AkButton>
      </AkButtonGroup>
    );
  };

  private onSelect = (user: User) => {
    this.setState({
      selectedUsers: [...this.state.selectedUsers, user],
    });
  };

  private onRemove = (user: User) => {
    this.setState({
      selectedUsers: this.state.selectedUsers.filter(u => u.id !== user.id),
    });
  };

  private onQueryChange = (query) => {
    this.setState({ searchQuery: query });
    this.debouncedRefetch();
  };

  private shouldRefetchOnFilter = (): boolean => {
    if (!this.props.data || !this.props.data.siteUsersList || !this.props.data.siteUsersList.users) {
      return false;
    }

    return this.props.data.siteUsersList.users.length <= this.props.data.siteUsersList.total;
  };
}

const withCurrentUser = graphql<OwnProps & InjectedIntlProps & FlagProps & AnalyticsClientProps, IsCurrentAdminSystemAdminQuery, IsCurrentAdminSystemAdminQueryVariables, CurrentUserProps>(currentUserQuery, {
  name: 'currentUser',
  options: ({ cloudId }) => ({
    variables: {
      cloudId,
    },
  }),
});

const withMutation = graphql<OwnProps & InjectedIntlProps & FlagProps & AnalyticsClientProps, AddMembersToGroupsMutation>(addMembersDialogMutation);

const withQuery = graphql<OwnProps & InjectedIntlProps & FlagProps & AnalyticsClientProps, AddMembersDialogQuery>(addMembersDialogQuery, {
  options: ({ cloudId }) => ({
    variables: {
      cloudId,
      input: {
        start: 1,
        count: 100,
      },
    },
  }),
});

export const AddMembersDialog = withFlag(
  withAnalyticsClient(
    injectIntl(
      withMutation(
        withCurrentUser(
          withQuery(
            AddMembersDialogImpl,
          ),
        ),
      ),
    ),
  ),
);
