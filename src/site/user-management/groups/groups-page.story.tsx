import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider, ChildProps } from 'react-apollo';
import { IntlProvider } from 'react-intl';
import { MemoryRouter } from 'react-router-dom';

import AkPage from '@atlaskit/page';

import { FlagProvider } from 'common/flag';

import { groupListResponse } from '../../../../mock/services/um-service/responses';
import { createMockAnalyticsClient, createMockIntlProp } from '../../../utilities/testing';

import { createApolloClient } from '../../../apollo-client';
import { GroupsPageQuery } from '../../../schema/schema-types';
import { GroupsPageImpl } from './groups-page';

const client = createApolloClient();

const Wrapper = ({ children }) => (
  <ApolloProvider client={client}>
    <IntlProvider locale="en">
      <MemoryRouter>
        <FlagProvider>
          <AkPage>
            {children}
          </AkPage>
        </FlagProvider>
      </MemoryRouter>
    </IntlProvider>
  </ApolloProvider>
);

const pageDefaults = {
  intl: createMockIntlProp(),
  history: { replace: () => null } as any,
  match: { params: { cloudId: 'DUMMY_ORG_ID' } } as any,
  location: window.location as any,
  analyticsClient: createMockAnalyticsClient(),
};

const mockData: ChildProps<any, GroupsPageQuery> = {
  loading: false,
  currentUser: {
    currentUser: {
      isSystemAdmin: false,
    },
  },
  groupList: groupListResponse,
};

const systemUserData: ChildProps<any, GroupsPageQuery> = {
  loading: false,
  currentUser: {
    currentUser: {
      isSystemAdmin: true,
    },
  },
  groupList: groupListResponse,
};

const mockDataNoGroups: ChildProps<any, GroupsPageQuery> = {
  loading: false,
  groupList: {
    total: 0,
    groups: [],
  },
};

const loadingData: ChildProps<any, GroupsPageQuery> = {
  loading: true,
};

const errorData: ChildProps<any, GroupsPageQuery> = {
  loading: false,
  error: true,
};

const currentAdminUser: ChildProps<any, GroupsPageQuery> = {
  currentUser: {
    isSystemAdmin: false,
  },
};

const currentSysadminUser: ChildProps<any, GroupsPageQuery> = {
  currentUser: {
    isSystemAdmin: true,
  },
};

storiesOf('Site|Groups Page', module)
  .add('While loading', () => (
    <Wrapper>
      <GroupsPageImpl data={loadingData} {...pageDefaults} />
    </Wrapper>
  ))
  .add('With an error', () => (
    <Wrapper>
      <GroupsPageImpl data={errorData} {...pageDefaults} />
    </Wrapper>
  ))
  .add('With no groups', () => (
    <Wrapper>
      <GroupsPageImpl data={mockDataNoGroups} {...pageDefaults} />
    </Wrapper>
  ))
  .add('With groups - Current user is an ADMIN', () => (
    <Wrapper>
      <GroupsPageImpl data={mockData} currentUser={currentAdminUser} {...pageDefaults} />
    </Wrapper>
  ))
  .add('With system groups - Current user is a SYS_ADMIN', () => (
    <Wrapper>
      <GroupsPageImpl data={systemUserData} currentUser={currentSysadminUser} {...pageDefaults} />
    </Wrapper>
  ));
