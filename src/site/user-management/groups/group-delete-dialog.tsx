import * as React from 'react';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router-dom';

import { ButtonGroup as AkButtonGroup } from '@atlaskit/button';

import { AnalyticsClientProps, Button, deletedGroupTrackEventData, deleteGroupCancelButtonEventData, deleteGroupConfirmButtonEventData, deleteGroupModalScreenEvent, ScreenEventSender, withAnalyticsClient } from 'common/analytics';
import { ModalDialog } from 'common/modal';

export const messages = defineMessages({
  title: {
    id: 'user-management.groups.group-delete.title',
    defaultMessage: `Delete "{name}"?`,
  },
  description: {
    id: 'user-management.groups.group-delete.description',
    defaultMessage: `Users in this group will not be affected, and products will keep all the content related to the group. If you create a new group with the same name, this content will be associated with it.`,
  },
  description2: {
    id: 'user-management.groups.group-delete.description2',
    defaultMessage: `Deleting a group can't be undone.`,
  },
  buttonDelete: {
    id: 'user-management.groups.group-delete.button.delete',
    defaultMessage: 'Delete group',
  },
  buttonCancel: {
    id: 'user-management.groups.group-delete.button.cancel',
    defaultMessage: 'Cancel',
  },
});

interface OwnProps {
  groupId: string;
  memberCount: number;
  isLoading: boolean;
  isOpen: boolean;
  name: string;
  dismiss(): void;
  remove(): void;
}

interface SiteParams {
  cloudId: string;
}

type GroupDeleteDialogProps = OwnProps & InjectedIntlProps & AnalyticsClientProps & RouteComponentProps<SiteParams>;

export class GroupDeleteDialogImpl extends React.Component<GroupDeleteDialogProps> {
  public render() {
    const {
      intl: { formatMessage },
      isLoading,
      isOpen,
      name,
      groupId,
      memberCount,
    } = this.props;

    return (
      <ModalDialog
        header={formatMessage(messages.title, { name })}
        footer={(
          <AkButtonGroup>
            <Button
              appearance="danger"
              isLoading={isLoading}
              onClick={this.onSubmit}
              id="confirm-group-deletion-button"
            >
              {formatMessage(messages.buttonDelete)}
            </Button>
            <Button
              appearance="subtle-link"
              onClick={this.onCancel}
            >
              {formatMessage(messages.buttonCancel)}
            </Button>
          </AkButtonGroup>
        )}
        isOpen={isOpen}
        width="small"
        onClose={this.onCancel}
      >
        <ScreenEventSender
          event={deleteGroupModalScreenEvent({
            groupId,
            memberCount,
          })}
        >
          <FormattedMessage {...messages.description} tagName="p" />
          <FormattedMessage {...messages.description2} tagName="p" />
        </ScreenEventSender>
      </ModalDialog>
    );
  }

  public onCancel = () => {
    const {
      match: { params: { cloudId } },
    } = this.props;

    this.props.analyticsClient.sendUIEvent({
      cloudId, data: deleteGroupCancelButtonEventData({
        groupId: this.props.groupId,
        memberCount: this.props.memberCount,
      }),
    });

    this.props.dismiss();
  }

  public onSubmit = () => {
    const {
      match: { params: { cloudId } },
    } = this.props;

    this.props.analyticsClient.sendTrackEvent({
      cloudId, data: deletedGroupTrackEventData({
        groupId: this.props.groupId,
        memberCount: this.props.memberCount,
      }),
    });

    this.props.analyticsClient.sendUIEvent({
      cloudId, data: deleteGroupConfirmButtonEventData({
        groupId: this.props.groupId,
        memberCount: this.props.memberCount,
      }),
    });

    this.props.remove();
  }
}

export const GroupDeleteDialog = (
  withRouter<OwnProps>(
    injectIntl(
      withAnalyticsClient(
        GroupDeleteDialogImpl,
      ),
    ),
  )
);
