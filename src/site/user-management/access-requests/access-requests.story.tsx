import { storiesOf } from '@storybook/react';
import * as React from 'react';
import ApolloProvider from 'react-apollo/ApolloProvider';
import { IntlProvider } from 'react-intl';

import { FlagProvider } from 'common/flag';

import { createApolloClient } from '../../../apollo-client';
import { AccessRequest } from '../../../schema/schema-types/schema-types';
import { createMockIntlProp } from '../../../utilities/testing';
import { AccessRequestsTableImpl } from './access-requests-table';
import { DenyAccessModalImpl } from './deny-access-modal';

const client = createApolloClient();

const generateAccessRequests = (limit: number) => {
  const accessRequests: AccessRequest[] = [];
  for (let i = 0; i < limit; i++) {
    accessRequests.push({
      user: {
        id: i.toString(),
        displayName: `User ${i}`,
        email: `user${i}@example.com`,
        active: true,
        system: false,
        hasVerifiedEmail: true,
      },
      requestedProducts: [{
        productId: 'confluence',
        productName: 'Confluence',
        status: 'PENDING',
        requestedTime: '2017-07-10T14:00:00Z',
      }],
      type: i % 2 === 0 ? 'REQUEST' : 'INVITE',
      requester: {
        id: i.toString(),
        displayName: `User ${i}`,
        email: `user${i}@example.com`,
        active: true,
        system: false,
        hasVerifiedEmail: true,
      },
    });
  }

  return accessRequests;
};

const createTestComponent = (total: number, isLoading: boolean = false) => {
  const noop = async () => Promise.resolve();

  return (
    <ApolloProvider client={client}>
      <IntlProvider locale="en">
        <FlagProvider>
          <AccessRequestsTableImpl
            cloudId={'dummy-cloudId'}
            rows={generateAccessRequests(total)}
            rowsPerPage={20}
            page={1}
            onSetPage={noop}
            isLoading={isLoading}
            intl={createMockIntlProp()}
            history={{} as any}
            onDenyAccess={noop}
            onApproveAccess={noop}
          />
        </FlagProvider>
      </IntlProvider>
    </ApolloProvider>
  );
};

storiesOf('Site|Access Requests', module)
  .add('Show list', () => {
    return createTestComponent(30);
  })
  .add('Show initial loading', () => {
    return createTestComponent(0, true);
  })
  .add('Show loading state with data', () => {
    return createTestComponent(20, true);
  })
  .add('Show empty state', () => {
    return createTestComponent(0);
  })
  .add('Show deny access modal', () => {
    const noop = () => undefined;

    return (
      <ApolloProvider client={client}>
        <IntlProvider locale="en">
          <DenyAccessModalImpl
            cloudId={'dummy-cloudId'}
            accessRequest={generateAccessRequests(1)[0]}
            isOpen={true}
            intl={createMockIntlProp()}
            history={{} as any}
            onDenyAccess={noop}
            onDialogDismissed={noop}
            showFlag={noop}
            hideFlag={noop}
            denyAccess={noop as any}
            analyticsClient={{ sendUIEvent: noop } as any}
          />
        </IntlProvider>
      </ApolloProvider>
    );
  });
