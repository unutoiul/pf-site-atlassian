import { expect } from 'chai';
import { mount } from 'enzyme';
import times from 'lodash.times';
import * as moment from 'moment';
import * as React from 'react';

import {
  DynamicTableStateless as AkDynamicTableStateless,
} from '@atlaskit/dynamic-table';
import AkEmptyState from '@atlaskit/empty-state';
import AkLozenge from '@atlaskit/lozenge';
import AkTooltip from '@atlaskit/tooltip';

import { Account } from 'common/account';
import { FlagProvider } from 'common/flag';

import { AccessRequest, AccessRequestType, SubProduct } from '../../../schema/schema-types';
import { createMockContext, createMockIntlProp } from '../../../utilities/testing';
import { AccessRequestsActions } from './access-requests-actions';
import { AccessRequestsTableImpl, messages } from './access-requests-table';

describe('AccessRequestsTable', () => {
  const noop = async () => Promise.resolve();
  const mockIntl = createMockIntlProp();
  const getTableWrapper = (props) => {
    const {
      rows = [],
      rowsPerPage = 10,
      page = 1,
      isLoading = false,
      onSetPage = () => null,
    } = props;

    return mount((
      <FlagProvider>
        <AccessRequestsTableImpl
          cloudId={'dummy-cloudId'}
          rows={rows}
          rowsPerPage={rowsPerPage}
          page={page}
          onSetPage={onSetPage}
          isLoading={isLoading}
          intl={mockIntl}
          history={{} as any}
          onApproveAccess={noop}
          onDenyAccess={noop}
        />
      </FlagProvider>
    ), createMockContext({ intl: true, apollo: true }));
  };

  const getFirstRow = (tableWrapper) => tableWrapper.find('Row');

  const generateMockAccessRequest = (type?: AccessRequestType): AccessRequest => {
    return {
      user: {
        id: '557057:80abfbd6-e1b5-4f23-99d9-077658050e25',
        displayName: 'Alex Sim',
        email: 'asimkin@atlassian.com',
        active: true,
        system: false,
        hasVerifiedEmail: true,
      },
      requestedProducts: [{
        productId: 'confluence',
        productName: 'Confluence',
        status: 'PENDING',
        requestedTime: '2017-07-10T14:00:00Z',
      }],
      type: type || 'REQUEST',
      requester: {
        id: '557057:80abfbd6-e1b5-4f23-99d9-077658050e25',
        displayName: 'Alex Sim',
        email: 'asimkin@atlassian.com',
        active: true,
        system: false,
        hasVerifiedEmail: true,
      },
    };
  };

  const generateMockAccessRequestForJira = (subProducts: SubProduct[]): AccessRequest => {
    return {
      user: {
        id: '557057:80abfbd6-e1b5-4f23-99d9-077658050e25',
        displayName: 'Alex Sim',
        email: 'asimkin@atlassian.com',
        active: true,
        system: false,
        hasVerifiedEmail: true,
      },
      requestedProducts: [{
        productId: 'jira',
        productName: 'All Jira products',
        status: 'PENDING',
        requestedTime: '2017-07-10T14:00:00Z',
        subProducts,
      }],
      type: 'REQUEST',
      requester: {
        id: '557057:80abfbd6-e1b5-4f23-99d9-077658050e25',
        displayName: 'Alex Sim',
        email: 'asimkin@atlassian.com',
        active: true,
        system: false,
        hasVerifiedEmail: true,
      },
    };
  };

  const generateMockAccessRequestWithoutRequester = (): AccessRequest => {
    return {
      user: {
        id: '557057:80abfbd6-e1b5-4f23-99d9-077658050e25',
        displayName: 'Alex Sim',
        email: 'asimkin@atlassian.com',
        active: true,
        system: false,
        hasVerifiedEmail: true,
      },
      requestedProducts: [{
        productId: 'jira',
        productName: 'All Jira products',
        status: 'PENDING',
        requestedTime: '2017-07-10T14:00:00Z',
      }],
      type: 'REQUEST',
    };
  };

  const getSubProducts = (count: number): SubProduct[] => {
    const subProducts: SubProduct[] = [];
    times(count, (i: number) => {
      subProducts.push({
        productId: `product-${i + 1}`,
        productName: `Product ${i + 1}`,
      });
    });

    return subProducts;
  };

  it('should render empty state', () => {
    const wrapper = getTableWrapper({
      rows: [],
    });

    expect(wrapper.find(AkEmptyState)).to.have.lengthOf(1);
  });

  it('should render a valid access requests table', () => {
    const wrapper = getTableWrapper({
      rows: [generateMockAccessRequest()],
    });

    expect(wrapper.find('Row')).to.have.lengthOf(1);
  });

  it('should correctly pass the isLoading prop', () => {
    const wrapper = getTableWrapper({ isLoading: true });
    expect(wrapper.find(AkDynamicTableStateless).prop('isLoading')).to.equal(true);
  });

  describe('access requests table columns', () => {
    it('should render Account', () => {
      const accessRequest = generateMockAccessRequest();
      const wrapper = getTableWrapper({
        rows: [accessRequest],
      });
      const row = getFirstRow(wrapper);
      const accountProps = row.find(Account).props();
      expect(accountProps.id).to.equal(accessRequest.user.id);
      expect(accountProps.email).to.equal(accessRequest.user.email);
      expect(accountProps.displayName).to.equal(accessRequest.user.displayName);
    });

    it('should render requested access time and requester', () => {
      const accessRequest = generateMockAccessRequest();
      const { requestedProducts } = accessRequest;
      const [{ requestedTime }] = requestedProducts;
      const wrapper = getTableWrapper({
        rows: [accessRequest],
      });
      const row = getFirstRow(wrapper);
      const requesterEmail = (accessRequest.requester && accessRequest.requester.email) || '';

      expect(row.find('td').at(3).find('a').prop('href')).to.equal(`/admin/users/view?email=${encodeURIComponent(requesterEmail)}`);
      expect(row.find('td').at(3).find('div').text()).to.equal(moment(requestedTime).format('D MMM YYYY'));
    });

    it('should render row when requester is null', () => {
      const wrapper = getTableWrapper({
        rows: [generateMockAccessRequestWithoutRequester()],
      });
      const row = getFirstRow(wrapper);

      expect(row.find('td').at(3).find('a').length).to.equal(0);
    });

    it('should render request access product', () => {
      const accessRequest = generateMockAccessRequest();
      const wrapper = getTableWrapper({
        rows: [accessRequest],
      });
      const row = getFirstRow(wrapper);
      const { productName } = accessRequest.requestedProducts[0];
      expect(row.find('td').at(1).find('span').text()).to.equal(productName);
    });

    it('should render type lozenge as request', () => {
      const accessRequest = generateMockAccessRequest();
      const wrapper = getTableWrapper({
        rows: [accessRequest],
      });
      const row = getFirstRow(wrapper);

      expect(row.find(AkLozenge)).to.have.lengthOf(1);
      expect(row.find(AkLozenge).text()).to.equal(mockIntl.formatMessage(messages.typeRequest));
    });

    it('should render type lozenge as invite', () => {
      const accessRequest = generateMockAccessRequest('INVITE');
      const wrapper = getTableWrapper({
        rows: [accessRequest],
      });
      const row = getFirstRow(wrapper);

      expect(row.find(AkLozenge)).to.have.lengthOf(1);
      expect(row.find(AkLozenge).text()).to.equal(mockIntl.formatMessage(messages.typeInvite));
    });

    it('should render AccessRequestsActions component', () => {
      const accessRequest = generateMockAccessRequest();
      const wrapper = getTableWrapper({
        rows: [accessRequest],
      });
      const row = getFirstRow(wrapper);

      expect(row.find(AccessRequestsActions)).to.have.lengthOf(1);
    });

    it('should pass correct tooltip value when subProducts length is 1', () => {
      const subProducts = getSubProducts(1);
      const accessRequest = generateMockAccessRequestForJira(subProducts);
      const wrapper = getTableWrapper({
        rows: [accessRequest],
      });
      const row = getFirstRow(wrapper);

      expect(row.find(AkTooltip).prop('content')).to.equal(mockIntl.formatMessage(messages.accessRequestProductsOne, {
        product1: subProducts[0].productName,
      }));
    });

    it('should pass correct tooltip value when subProducts length is 2', () => {
      const subProducts = getSubProducts(2);
      const accessRequest = generateMockAccessRequestForJira(subProducts);
      const wrapper = getTableWrapper({
        rows: [accessRequest],
      });
      const row = getFirstRow(wrapper);

      expect(row.find(AkTooltip).prop('content')).to.equal(mockIntl.formatMessage(messages.accessRequestProductsTwo, {
        product1: subProducts[0].productName,
        product2: subProducts[1].productName,
      }));
    });

    it('should pass correct tooltip value when subProducts length is 3', () => {
      const subProducts = getSubProducts(3);
      const accessRequest = generateMockAccessRequestForJira(subProducts);
      const wrapper = getTableWrapper({
        rows: [accessRequest],
      });
      const row = getFirstRow(wrapper);

      expect(row.find(AkTooltip).prop('content')).to.equal(mockIntl.formatMessage(messages.accessRequestProductsThree, {
        product1: subProducts[0].productName,
        product2: subProducts[1].productName,
        product3: subProducts[2].productName,
      }));
    });

    it('should pass correct tooltip value when subProducts length is 4', () => {
      const subProducts = getSubProducts(4);
      const accessRequest = generateMockAccessRequestForJira(subProducts);
      const wrapper = getTableWrapper({
        rows: [accessRequest],
      });
      const row = getFirstRow(wrapper);

      expect(row.find(AkTooltip).prop('content')).to.equal(mockIntl.formatMessage(messages.accessRequestProductsFour, {
        product1: subProducts[0].productName,
        product2: subProducts[1].productName,
        product3: subProducts[2].productName,
        product4: subProducts[3].productName,
      }));
    });
  });
});
