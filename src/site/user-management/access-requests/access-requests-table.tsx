import * as H from 'history';
import * as moment from 'moment';
import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import { DynamicTableStateless as AkDynamicTableStateless } from '@atlaskit/dynamic-table';
import AkEmptyState from '@atlaskit/empty-state';
import AkLozenge from '@atlaskit/lozenge';
import AkTooltip from '@atlaskit/tooltip';

import { Account } from 'common/account';

import { overComingObstaclesImage } from 'common/images';

import { getPaginationMessages } from 'common/table';

import { AccessRequest, RequestedProduct, SubProduct } from '../../../schema/schema-types';
import { AccessRequestsActions } from './access-requests-actions';
import { RequestedDateWrapper, RequesterWrapper, TableWrapper } from './access-requests-table.styled';

export const messages = defineMessages({
  headerUser: {
    id: `user-management.access-requests.table.header.user`,
    description: 'This will be a table header. Will show the user who requested access.',
    defaultMessage: `User`,
  },
  headerRequestsAccessTo: {
    id: `user-management.access-requests.table.header.requests-access-to`,
    description: 'This will be a table header to show the product to which access was requested',
    defaultMessage: `Product`,
  },
  headerRequestMadeOn: {
    id: `user-management.access-requests.table.header.request-made-on`,
    description: 'This will be a table header to show the time when the request was made',
    defaultMessage: `Requested by`,
  },
  headerAction: {
    id: `user-management.access-requests.table.header.action`,
    description: 'This will be a table header',
    defaultMessage: `Actions`,
  },
  headerType: {
    id: `user-management.access-requests.table.header.type`,
    description: 'This will be a type header',
    defaultMessage: `Type`,
  },
  emptyStateTitle: {
    id: `user-management.access-requests.table.pending.empty-title`,
    description: 'Title shown when there is no data to be shown',
    defaultMessage: `You're on top of things`,
  },
  emptyStateDescription: {
    id: `user-management.access-requests.table.pending.empty-description`,
    description: 'Description shown when there is no data to be shown',
    defaultMessage: `You don't have any pending access requests.`,
  },
  typeRequest: {
    id: `user-management.access-requests.table.lozenge.type.request`,
    description: 'This will denote that the access request was for the same person',
    defaultMessage: `Request`,
  },
  typeInvite: {
    id: `user-management.access-requests.table.lozenge.type.invite`,
    description: 'This will denote that the access request was for a different same person',
    defaultMessage: `Invite`,
  },
  accessRequestProductsOne: {
    id: `user-management.access-requests.table.requested.products.one`,
    description: 'This will list of products access is requested',
    defaultMessage: `The user will get access to {product1}`,
  },
  accessRequestProductsTwo: {
    id: `user-management.access-requests.table.requested.products.two`,
    description: 'This will list of products access is requested',
    defaultMessage: `The user will get access to {product1} and {product2}`,
  },
  accessRequestProductsThree: {
    id: `user-management.access-requests.table.requested.products.three`,
    description: 'This will list of products access is requested',
    defaultMessage: `The user will get access to {product1}, {product2} and {product3}`,
  },
  accessRequestProductsFour: {
    id: `user-management.access-requests.table.requested.products.four`,
    description: 'This will list of products access is requested',
    defaultMessage: `The user will get access to {product1}, {product2}, {product3} and {product4}`,
  },
  accessRequestProductsMany: {
    id: `user-management.access-requests.table.requested.products.many`,
    description: 'This will list of products access is requested',
    defaultMessage: `The user will get access to {product1}, {product2}, {product3}, {product4} and {numAdditionalProducts, plural, other {# more}}`,
  },
});

export interface OwnProps {
  cloudId: string;
  isLoading: boolean;
  rows: AccessRequest[];
  rowsPerPage: number;
  page: number;
  history: H.History;
  onSetPage(page: number): void;
  onApproveAccess(accessRequest: AccessRequest): Promise<void>;
  onDenyAccess(accessRequest: AccessRequest): Promise<void>;
}

export class AccessRequestsTableImpl extends React.Component<OwnProps & InjectedIntlProps> {
  public render() {
    const { formatMessage } = this.props.intl;
    const {
      rowsPerPage,
      onSetPage,
      page,
      isLoading,
      rows,
    } = this.props;

    const head = {
      cells: [
        { key: 'header.user', content: formatMessage(messages.headerUser), width: 30 },
        { key: 'header.product', content: formatMessage(messages.headerRequestsAccessTo), width: 20 },
        { key: 'header.type', content: formatMessage(messages.headerType), width: 15 },
        { key: 'header.request-made-on', content: formatMessage(messages.headerRequestMadeOn), width: 15 },
        { key: 'header.action', content: <span>{formatMessage(messages.headerAction)}</span>, width: 20, inlineStyles: { paddingLeft: '28px' } },
      ],
    };

    const emptyView = (
      <AkEmptyState
        maxImageWidth={320}
        imageUrl={overComingObstaclesImage}
        header={formatMessage(messages.emptyStateTitle)}
        description={formatMessage(messages.emptyStateDescription)}
      />
    );

    return (
      <TableWrapper>
        <AkDynamicTableStateless
          isLoading={isLoading}
          head={head}
          rows={rows.length ? this.getRows() : undefined}
          emptyView={emptyView}
          rowsPerPage={rowsPerPage}
          onSetPage={onSetPage}
          page={page}
          paginationi18n={this.getPaginationMessages()}
        />
      </TableWrapper>
    );
  }

  private getRows = () => {
    const {
      rows,
      onApproveAccess,
      onDenyAccess,
      cloudId,
      history,
      intl: { formatMessage },
    } = this.props;

    return rows.map((accessRequest: AccessRequest, index: number) => {
      if (!accessRequest) {
        return {
          key: `row-${index}`,
          cells: [
            {
              key: 'access-requests.user',
              content: null,
            },
            {
              key: 'access-requests.product',
              content: null,
            },
            {
              key: 'access-requests.type',
              content: null,
            },
            {
              key: 'access-requests.request-made-on',
              content: null,
            },
            {
              key: 'access-requests.actions',
              content: null,
            },
          ],
        };
      }
      const { user, requestedProducts, type, requester } = accessRequest;
      const { id, displayName, email } = user;
      const [{ requestedTime }] = requestedProducts as [RequestedProduct];
      const inviteType = type === 'REQUEST' ? formatMessage(messages.typeRequest) : formatMessage(messages.typeInvite);

      const accessRequestActions = (
        <AccessRequestsActions
          cloudId={cloudId}
          onDenyAccess={onDenyAccess}
          onApproveAccess={onApproveAccess}
          accessRequest={accessRequest}
          history={history}
        />
      );

      return {
        cells: [
          { key: `access-requests.user-${id}`, content: <Account id={id} email={email} displayName={displayName}/> },
          { key: `access-requests.product-${id}`, content: this.getProductName(requestedProducts) },
          { key: `access-requests.type-${id}`, content: <AkLozenge appearance="inprogress" >{inviteType}</AkLozenge> },
          { key: `access-requests.request-made-on-${id}`, content: this.getRequestedBy(requester, requestedTime) },
          { key: `access-requests.actions-${id}`, content: accessRequestActions },
        ],
      };
    });
  };

  private getJiraProductNameList = (subProducts: SubProduct[]): string => {
    const { intl: { formatMessage } } = this.props;

    if (subProducts.length === 1) {
      return formatMessage(messages.accessRequestProductsOne, {
        product1: subProducts[0].productName,
      });
    } else if (subProducts.length === 2) {
      return formatMessage(messages.accessRequestProductsTwo, {
        product1: subProducts[0].productName,
        product2: subProducts[1].productName,
      });
    } else if (subProducts.length === 3) {
      return formatMessage(messages.accessRequestProductsThree, {
        product1: subProducts[0].productName,
        product2: subProducts[1].productName,
        product3: subProducts[2].productName,
      });
    } else if (subProducts.length === 4) {
      return formatMessage(messages.accessRequestProductsFour, {
        product1: subProducts[0].productName,
        product2: subProducts[1].productName,
        product3: subProducts[2].productName,
        product4: subProducts[3].productName,
      });
    } else {
      return formatMessage(messages.accessRequestProductsMany, {
        product1: subProducts[0].productName,
        product2: subProducts[1].productName,
        product3: subProducts[2].productName,
        product4: subProducts[3].productName,
        numAdditionalProducts: subProducts.length - 4,
      });
    }
  };

  private getProductName = (requestedProducts: RequestedProduct[]) => {
    const { intl: { formatMessage } } = this.props;
    const [{ productId, productName, subProducts }] = requestedProducts;

    return productId === 'jira' && subProducts && subProducts.length
      ? <AkTooltip content={this.getJiraProductNameList(subProducts)} position="right"><span>{productName}</span></AkTooltip>
      : (
        <AkTooltip content={formatMessage(messages.accessRequestProductsOne, { product1: productName })} position="right">
          <span>{productName}</span>
        </AkTooltip>
      );
  }

  private getPaginationMessages = () => {
    return getPaginationMessages(this.props.intl.formatMessage.bind(this.props.intl));
  }

  private getRequestedBy = (requester, requestedTime) => {

    return (
      <React.Fragment>
        {requester
          ? <RequesterWrapper href={`/admin/users/view?email=${encodeURIComponent(requester.email)}`}>{requester.displayName}</RequesterWrapper>
          : null}
        <RequestedDateWrapper>{moment(requestedTime).format('D MMM YYYY')}</RequestedDateWrapper>
      </React.Fragment>
    );
  }
}

export const AccessRequestsTable = injectIntl(AccessRequestsTableImpl);
