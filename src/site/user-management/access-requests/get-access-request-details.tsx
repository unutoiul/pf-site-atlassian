import { AccessRequestAttributes } from 'common/analytics';

import { AccessRequest } from '../../../schema/schema-types/schema-types';

export const getAccessRequestDetails = (accessRequest: AccessRequest): AccessRequestAttributes => {
  const { user, requestedProducts, requester, type } = accessRequest;
  const [{ productId, status, requestedTime, subProducts }] = requestedProducts;
  const accessRequestAttributes = {
    requestedProduct: productId,
    status,
    requestedTime,
    requesterUserId: (requester && requester.id) || user.id,
    requestedForUserId: user.id,
    type,
    hasVerifiedEmail: user.hasVerifiedEmail,
  };

  if (subProducts) {
    return {
      ...accessRequestAttributes,
      subProducts: subProducts.map(({ productId: subProductId }) => subProductId).join(),
    };
  }

  return accessRequestAttributes;
};
