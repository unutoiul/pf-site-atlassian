import { parse, stringify } from 'query-string';
import * as React from 'react';
import { ChildProps, graphql, MutationFunc } from 'react-apollo';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps } from 'react-router';
import styled from 'styled-components';

import { gridSize as akGridSize } from '@atlaskit/theme';
import OriginTracing from '@atlassiansox/origin-tracing';

import {
  accessRequestsViewData,
  AnalyticsClientProps,
  approveAccessTrackData,
  approveAccessUIData,
  withAnalyticsClient,
} from 'common/analytics';
import { createErrorIcon, createSuccessIcon, RequestError } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';
import { PageLayout } from 'common/page-layout';
import { getPage, getStart } from 'common/pagination';

import {
  AccessRequest,
  AccessRequestsQuery,
  AccessRequestStatus,
  ApproveAccessMutation,
  ApproveAccessMutationVariables,
} from '../../../schema/schema-types';
import { AccessRequestsTable } from './access-requests-table';
import AccessRequests from './access-requests.query.graphql';
import approveAccessMutation from './approve-access.mutation.graphql';
import { getAccessRequestDetails } from './get-access-request-details';

export const messages = defineMessages({
  title: {
    id: 'user-management.access-requests.title',
    description: 'The tile of the page',
    defaultMessage: 'Access Requests',
  },
  description: {
    id: 'user-management.access-requests.description',
    description: 'Description of the page',
    defaultMessage: 'When you grant users access to your products, they count towards your licence. Your users can request access for themselves or others. You can grant access to a user for the product they\'ve requested.',
  },
  error: {
    id: `user-management.access-requests.table.denied.error`,
    description: 'Error message which will be shown on the page ',
    defaultMessage: `We couldn't retrieve the access requests for this site. Try again soon.`,
  },
  approveSuccessTitle: {
    id: `user-management.access-requests.approve-access-success-title`,
    description: 'Title of the flag which is shown when access is granted',
    defaultMessage: `Access granted to {userDisplayName}`,
  },
  approveSuccessDescription: {
    id: `user-management.access-requests.approve-access-success-description`,
    description: 'Description of the flag which is shown when access is granted',
    defaultMessage: `{userDisplayName} is now able to use {productName}.`,
  },
  approveFailureTitle: {
    id: `user-management.access-requests.approve-access-failure-title`,
    description: 'Title of the flag which is shown when access request failed',
    defaultMessage: `Unable to grant access. Try again later.`,
  },
  userDetails: {
    id: `user-management.access-requests.approve-access-success-user-details`,
    description: 'Link text which navigates to user\'s details page',
    defaultMessage: `See user profile for {userDisplayName}`,
  },
  alreadyActionedTitle: {
    id: `user-management.access-requests.already-approved-access-title`,
    description: 'Shown when access request for a user is already approved/denied',
    defaultMessage: `Error - Request already actioned`,
  },
  alreadyActionedDescription: {
    id: `user-management.access-requests.already-approved-access-description`,
    description: 'Description shown when access request for a user is already approved',
    defaultMessage: `This request for {userDisplayName} has already been actioned. You can manage their access from their user profile.`,
  },
  invalidProductTitle: {
    id: `user-management.access-requests.invalid-product-title`,
    description: 'Shown when access request is approved for a product not in site',
    defaultMessage: `Error - Product not available`,
  },
  invalidProductDescription: {
    id: `user-management.access-requests.invalid-product-description`,
    description: 'Description shown when access request is approved for a product not in site',
    defaultMessage: `Sorry, we can't grant access to that product, as it is no longer available.`,
  },
});

const PageDescriptionWrapper = styled.div`
  max-width: ${akGridSize() * 75}px;
`;

interface SiteParams {
  cloudId: string;
}

interface AccessRequestsPageState {
  showTableLoading: boolean;
}

interface MutationProps {
  approveAccess: MutationFunc<ApproveAccessMutation, ApproveAccessMutationVariables>;
}

type AccessRequestsPageProps = MutationProps & InjectedIntlProps & RouteComponentProps<SiteParams> & FlagProps & AnalyticsClientProps;

const ROWS_PER_PAGE = 20;
const status: [AccessRequestStatus] = ['PENDING'];

export class AccessRequestsPageImpl extends React.Component<ChildProps<AccessRequestsPageProps, AccessRequestsQuery>, AccessRequestsPageState> {

  public readonly state: AccessRequestsPageState = {
    showTableLoading: false,
  };

  public async componentDidMount() {
    const { data } = this.props;

    this.handleOriginTracing();
    if (!data || data.loading) {
      return;
    }

    this.setState({ showTableLoading: true });
    try {
      await data.refetch();
    } finally {
      this.setState({ showTableLoading: false });
    }
  }

  public render() {
    const {
      intl: { formatMessage },
      data: { loading = false, error = null } = {},
      location,
      match,
      history,
    } = this.props;
    const cloudId = match.params.cloudId;

    if (error) {
      return (
        <PageLayout>
          <span>{formatMessage(messages.error)}</span>
        </PageLayout>
      );
    }

    const description = (
      <PageDescriptionWrapper>
        {formatMessage(messages.description)}
      </PageDescriptionWrapper>
    );

    return (
      <PageLayout
        isFullWidth={true}
        title={formatMessage(messages.title)}
        description={description}
      >
        <AccessRequestsTable
          cloudId={cloudId}
          history={history}
          isLoading={loading || this.state.showTableLoading}
          rows={this.getRows()}
          rowsPerPage={ROWS_PER_PAGE}
          onSetPage={this.gotoPage}
          page={getPage(location)}
          onApproveAccess={this.onApproveAccess}
          onDenyAccess={this.onDenyAccess}
        />
      </PageLayout>
    );
  }

  private handleOriginTracing = () => {
    const { match, location, analyticsClient, history } = this.props;
    const cloudId = match.params.cloudId;
    const origin = OriginTracing.fromUrl(location.search);

    if (origin.isEmpty()) {
      analyticsClient.sendScreenEvent({ data: accessRequestsViewData(), cloudId });

      return;
    }
    // tslint:disable-next-line:no-unused
    const { atlOrigin, ...rest } = parse(location.search);
    const atlOriginAttributes = origin.toAnalyticsAttributes();

    history.replace(`${location.pathname}?${stringify(rest)}`);
    analyticsClient.sendScreenEvent({
      data: accessRequestsViewData(atlOriginAttributes),
      cloudId,
    });
  };

  private getRows() {
    const { data, location } = this.props;

    if (!data || data.loading || !data.accessRequestList) {
      return [];
    }

    return this.getAccessRequestsPaddedWithPlaceholders(
      getStart(getPage(location), ROWS_PER_PAGE) - 1,
      data.accessRequestList.total,
      data.accessRequestList.accessRequests,
    );
  }

  private getAccessRequestsPaddedWithPlaceholders = (start: number, total: number, accessRequests: AccessRequest[]) => {
    const paddedAccessRequests: AccessRequest[] = Array(total).fill(null);
    paddedAccessRequests.splice(start, accessRequests.length, ...accessRequests);

    return paddedAccessRequests;
  };

  private updateQueryParams = (params: object) => {
    const { history, location } = this.props;
    history.push(`${location.pathname}?${stringify(params)}`);
  };

  private gotoPage = async (page: number) => {
    const { data, location } = this.props;

    this.updateQueryParams({
      ...parse(location.search),
      page,
    });

    return data && data.fetchMore({
      variables: {
        start: getStart(page, ROWS_PER_PAGE),
      },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        return {
          ...previousResult,
          ...fetchMoreResult,
        };
      },
    });
  };

  private onDenyAccess = async () => {
    const {
      data,
      showFlag,
      intl: { formatMessage },
    } = this.props;

    if (!data || !data.refetch) {
      return;
    }

    this.setState({ showTableLoading: true });

    try {
      await this.refetchPage();
    } catch (error) {
      showFlag({
        autoDismiss: true,
        icon: createErrorIcon(),
        id: `user.access-request.list.error.${Date.now()}`,
        title: formatMessage(messages.error),
      });
    } finally {
      this.setState({ showTableLoading: false });
    }
  };

  private onApproveAccess = async (accessRequest: AccessRequest) => {
    const {
      approveAccess,
      match,
      intl: { formatMessage },
    } = this.props;
    const cloudId = match.params.cloudId;
    const { user, requestedProducts } = accessRequest;
    const [requestedProduct] = requestedProducts;
    const origin = new OriginTracing({ product: 'admin' });

    this.setState({ showTableLoading: true });
    this.sendApproveUIEvent(accessRequest, origin.toAnalyticsAttributes({ hasGeneratedId: true }));

    try {
      await approveAccess({
        variables: {
          cloudId,
          input: {
            productId: requestedProduct.productId,
            userId: user.id,
            atlOrigin: origin.encode(),
          },
        },
      });

      this.showSuccessFlag(accessRequest);
      this.sendApproveTrackEvent(accessRequest, origin.toAnalyticsAttributes({ hasGeneratedId: true }));
    } catch (e) {
      if (!e.graphQLErrors.length || !e.graphQLErrors[0].originalError) {
        this.showErrorFlag(accessRequest);

        return;
      }
      const originalError = e.graphQLErrors[0].originalError as RequestError;
      let title;
      let description;

      if (originalError.status === 412) {
        title = formatMessage(messages.alreadyActionedTitle);
        description = formatMessage(messages.alreadyActionedDescription, {
          userDisplayName: user.displayName,
        });
      } else if (originalError.status === 402) {
        title = formatMessage(messages.invalidProductTitle);
        description = formatMessage(messages.invalidProductDescription);
      }

      this.showErrorFlag(accessRequest, { title, description });
    }

    await this.refetchPage();
    this.setState({ showTableLoading: false });
  };

  private isLastAccessRequest() {
    const { data } = this.props;

    return data
      && data.accessRequestList
      && data.accessRequestList.total > 1
      && data.accessRequestList.accessRequests.length === 1;
  }

  private async refetchPage() {
    const { location, data } = this.props;

    if (!data || !data.refetch) {
      return;
    }

    if (this.isLastAccessRequest()) {
      await this.gotoPage(getPage(location) - 1);
    } else {
      await data.refetch();
    }
  }

  private showSuccessFlag = ({ user, requestedProducts }: AccessRequest) => {
    const {
      showFlag,
      intl: { formatMessage },
    } = this.props;
    const [requestedProduct] = requestedProducts;

    const title = formatMessage(messages.approveSuccessTitle, {
      userDisplayName: user.displayName,
    });
    const description = formatMessage(messages.approveSuccessDescription, {
      userDisplayName: user.displayName,
      productName: requestedProduct.productName,
    });

    showFlag({
      autoDismiss: true,
      icon: createSuccessIcon(),
      id: `user.access-request.approve.success.${Date.now()}`,
      title,
      description,
      actions: [{
        content: formatMessage(messages.userDetails, {
          userDisplayName: user.displayName,
        }),
        onClick: () => this.gotoUserDetailsPage(user),
      }],
    });
  };

  private showErrorFlag = ({ user }: AccessRequest, { title = '', description = '' } = {}) => {
    const {
      showFlag,
      intl: { formatMessage },
    } = this.props;

    const defaultErrorTitle = formatMessage(messages.approveFailureTitle);
    showFlag({
      autoDismiss: true,
      icon: createErrorIcon(),
      id: `user.access-request.approve.error.${Date.now()}`,
      title: title || defaultErrorTitle,
      description,
      actions: [{
        content: formatMessage(messages.userDetails, {
          userDisplayName: user.displayName,
        }),
        onClick: () => this.gotoUserDetailsPage(user),
      }],
    });
  };

  private gotoUserDetailsPage = (user) => {
    window.location.href = `/admin/users/view?email=${encodeURIComponent(user.email)}`;
  };

  private sendApproveTrackEvent = (accessRequest, originAttributes) => {
    const { analyticsClient, match } = this.props;

    analyticsClient.sendTrackEvent({
      cloudId: match.params.cloudId,
      data: approveAccessTrackData({
        ...getAccessRequestDetails(accessRequest),
        ...originAttributes,
      }),
    });
  };

  private sendApproveUIEvent = (accessRequest, originAttributes) => {
    const { analyticsClient, match } = this.props;

    analyticsClient.sendUIEvent({
      cloudId: match.params.cloudId,
      data: approveAccessUIData({
        ...getAccessRequestDetails(accessRequest),
        ...originAttributes,
      }),
    });
  };
}

const withAccessRequestData = graphql<AccessRequestsPageProps, AccessRequestsQuery>(AccessRequests, {
  options: ({ location, match }) => {
    return {
      variables: {
        id: match.params.cloudId,
        input: {
          status,
          start: getStart(getPage(location), ROWS_PER_PAGE),
        },
      },
    };
  },
});

const withApproveAccessMutation = graphql<AccessRequestsPageProps, ApproveAccessMutation, ApproveAccessMutationVariables>(approveAccessMutation, {
  name: 'approveAccess',
});

export const AccessRequestsPage = withFlag(
  withAnalyticsClient(
    injectIntl(
      withApproveAccessMutation(
        withAccessRequestData(
          AccessRequestsPageImpl,
        ),
      ),
    ),
  ),
);
