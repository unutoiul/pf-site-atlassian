import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { sandbox as sinonSandbox } from 'sinon';

import { Action } from 'common/actions';
import { FlagProvider } from 'common/flag';

import { createApolloClient } from '../../../apollo-client';
import { AccessRequest } from '../../../schema/schema-types';
import {
  createMockAnalyticsClient,
  createMockIntlContext,
  createMockIntlProp,
} from '../../../utilities/testing';
import { AccessRequestsActionsImpl, messages } from './access-requests-actions';
import { DenyAccessModal } from './deny-access-modal';

describe('AccessRequestsActionsImpl', () => {
  const cloudId = 'dummy-cloudId';
  const accessRequest: AccessRequest = {
    user: {
      id: '557057:80abfbd6-e1b5-4f23-99d9-077658050e25',
      displayName: 'Alex Sim',
      email: 'asimkin@atlassian.com',
      active: true,
      system: false,
      hasVerifiedEmail: true,
    },
    requestedProducts: [{
      productId: 'confluence',
      productName: 'Confluence',
      status: 'PENDING',
      requestedTime: '2017-07-10T14:00:00Z',
    }],
    type: 'REQUEST',
    requester: {
      id: '557057:80abfbd6-e1b5-4f23-99d9-077658050e25',
      displayName: 'Alex Sim',
      email: 'asimkin@atlassian.com',
      active: true,
      system: false,
      hasVerifiedEmail: true,
    },
  };

  const sandbox = sinonSandbox.create();
  const client = createApolloClient();
  let onApproveAccessSpy: sinon.SinonSpy;
  let onDenyAccessSpy: sinon.SinonSpy;
  let historySpy;
  let mockAnalyticsClient;

  beforeEach(() => {
    onApproveAccessSpy = sandbox.spy();
    onDenyAccessSpy = sandbox.spy();
    mockAnalyticsClient = createMockAnalyticsClient();
    historySpy = {
      push: sandbox.spy(),
    };
  });

  afterEach(() => {
    sandbox.restore();
  });

  const getAccessRequestsActionWrapper = () => {

    return mount((
      <ApolloProvider client={client}>
        <FlagProvider>
          <AccessRequestsActionsImpl
            cloudId={cloudId}
            onDenyAccess={onDenyAccessSpy}
            onApproveAccess={onApproveAccessSpy}
            accessRequest={accessRequest}
            history={historySpy}
            analyticsClient={mockAnalyticsClient}
            intl={createMockIntlProp()}
          />
        </FlagProvider>
      </ApolloProvider>
    ), createMockIntlContext());
  };

  it('should render approve, deny and view profile actions', () => {
    const wrapper = getAccessRequestsActionWrapper();

    wrapper.find('MoreIcon').simulate('click');

    expect(wrapper.find(Action).at(0).prop('customMessage')).to.equal(messages.approveAccess);
    expect(wrapper.find(Action).at(1).prop('customMessage')).to.equal(messages.showProfile);
    expect(wrapper.find(Action).at(2).prop('customMessage')).to.equal(messages.denyAccess);

  });

  it('should call onApproveAccess when Grant Access is clicked', () => {
    const wrapper = getAccessRequestsActionWrapper();

    wrapper.find(Action).at(0).simulate('click');
    expect(onApproveAccessSpy.called).to.equal(true);
  });

  it('should open deny access modal when deny access action is clicked', () => {
    const wrapper = getAccessRequestsActionWrapper();

    wrapper.find('MoreIcon').simulate('click');
    wrapper.find(Action).at(2).simulate('click');

    expect(wrapper.find(DenyAccessModal).prop('isOpen')).to.equal(true);
  });

  it('should close modal and call onDenyAccess when onDenyConfirm is called', () => {
    const wrapper = getAccessRequestsActionWrapper();

    wrapper.find(DenyAccessModal).prop('onDenyAccess')();

    expect(wrapper.find(DenyAccessModal).prop('isOpen')).to.equal(false);
    expect(onDenyAccessSpy.called).to.equal(true);
  });

  it('should close modal when onDialogDismissed is called', () => {
    const wrapper = getAccessRequestsActionWrapper();

    wrapper.find(DenyAccessModal).prop('onDialogDismissed')();

    expect(wrapper.find(DenyAccessModal).prop('isOpen')).to.equal(false);
  });
});
