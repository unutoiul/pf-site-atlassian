import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { sandbox as sinonSandbox } from 'sinon';

import { RequestError } from 'common/error';
import { ModalDialog } from 'common/modal';

import { createApolloClient } from '../../../apollo-client';
import { AccessRequest } from '../../../schema/schema-types';
import {
  createMockAnalyticsClient,
  createMockIntlProp,
} from '../../../utilities/testing';
import { DenyAccessModalImpl, DenyModalProps, messages } from './deny-access-modal';

describe('DenyAccessModal', () => {
  // tslint:disable-next-line:no-empty
  const noop = () => {};
  const sandbox = sinonSandbox.create();
  const mockIntl = createMockIntlProp();
  let dismissSpy: sinon.SinonSpy;
  let onDenyAccessSpy: sinon.SinonSpy;
  let showFlagSpy: sinon.SinonSpy;
  let denyAccessStub;
  let mockAnalyticsClient;

  beforeEach(() => {
    dismissSpy = sandbox.spy();
    onDenyAccessSpy = sandbox.spy();
    showFlagSpy = sandbox.spy();
    denyAccessStub = sandbox.stub().resolves();
    mockAnalyticsClient = createMockAnalyticsClient();
  });

  afterEach(() => {
    sandbox.restore();
  });

  const generateMockAccessRequest = (): AccessRequest => {
    return {
      user: {
        id: '557057:80abfbd6-e1b5-4f23-99d9-077658050e25',
        displayName: 'Alex Sim',
        email: 'asimkin@atlassian.com',
        active: true,
        system: false,
        hasVerifiedEmail: true,
      },
      requestedProducts: [{
        productId: 'confluence',
        productName: 'Confluence',
        status: 'PENDING',
        requestedTime: '2017-07-10T14:00:00Z',
      }],
      type: 'REQUEST',
      requester: {
        id: '557057:80abfbd6-e1b5-4f23-99d9-077658050e25',
        displayName: 'Alex Sim',
        email: 'asimkin@atlassian.com',
        active: true,
        system: false,
        hasVerifiedEmail: true,
      },
    };
  };

  const getDenyAccessModalWrapper = (props: Partial<DenyModalProps> = {}) => {
    const {
      denyAccess = denyAccessStub,
      isOpen = true,
    } = props;

    const accessRequest = generateMockAccessRequest();
    const client = createApolloClient();

    return mount((
      <ApolloProvider client={client}>
        <DenyAccessModalImpl
          cloudId={'dummy-cloudId'}
          accessRequest={accessRequest}
          isOpen={isOpen}
          onDenyAccess={onDenyAccessSpy}
          onDialogDismissed={dismissSpy}
          intl={mockIntl}
          history={{} as any}
          showFlag={showFlagSpy}
          hideFlag={noop}
          denyAccess={denyAccess}
          analyticsClient={mockAnalyticsClient}
        />
      </ApolloProvider>
    ));
  };

  it('should not open deny access modal', () => {
    const wrapper = getDenyAccessModalWrapper({
      isOpen: false,
    });

    expect(wrapper.find(ModalDialog).prop('isOpen')).to.equal(false);
  });

  it('should display deny access modal', () => {
    const wrapper = getDenyAccessModalWrapper();

    expect(wrapper.find('Button')).to.have.lengthOf(2);
  });

  it('should call onDialogDismissed when cancel is clicked', () => {
    const wrapper = getDenyAccessModalWrapper();

    wrapper.find('Button').at(1).simulate('click');

    expect(dismissSpy.called).to.equal(true);
  });

  it('should call onDenyAccess when deny access is successful and show success flag', async () => {
    const wrapper = getDenyAccessModalWrapper();
    const { user, requestedProducts } = generateMockAccessRequest();
    const flagTitle = mockIntl.formatMessage(messages.denySuccessTitle, {
      userDisplayName: user.displayName,
    });

    wrapper.find('textarea').simulate('change', {
      target: {
        value: 'lorem ipsum...',
      },
    });
    wrapper.find('Button').at(0).simulate('click');
    wrapper.update();
    await new Promise(resolve => setTimeout(resolve, 0));

    expect(denyAccessStub.called).to.equal(true);
    expect(denyAccessStub.firstCall.args[0]).to.deep.equal({
      variables: {
        cloudId: 'dummy-cloudId',
        input: {
          productId: requestedProducts[0].productId,
          denialReason: 'lorem ipsum...',
          userId: user.id,
        },
      },
    });

    expect(showFlagSpy.called).to.equal(true);
    expect(showFlagSpy.firstCall.args[0].title).to.equal(flagTitle);
    expect(mockAnalyticsClient.sendTrackEvent.called).to.equal(true);
    expect(onDenyAccessSpy.called).to.equal(true);
  });

  it('should not call onDenyAccess when deny access failed and show error flag', async () => {
    const failureDenyAccessStub = sandbox.stub().rejects({
      graphQLErrors: [{
        originalError: new RequestError({}),
      }],
    });
    const wrapper = getDenyAccessModalWrapper({
      denyAccess: failureDenyAccessStub,
    });
    const { user, requestedProducts } = generateMockAccessRequest();

    wrapper.find('Button').at(0).simulate('click');
    wrapper.update();
    await new Promise(resolve => setTimeout(resolve, 0));

    expect(failureDenyAccessStub.called).to.equal(true);
    expect(failureDenyAccessStub.firstCall.args[0]).to.deep.equal({
      variables: {
        cloudId: 'dummy-cloudId',
        input: {
          productId: requestedProducts[0].productId,
          denialReason: '',
          userId: user.id,
        },
      },
    });

    expect(showFlagSpy.called).to.equal(true);
    expect(showFlagSpy.firstCall.args[0].title).to.equal(mockIntl.formatMessage(messages.denyFailureTitle));
    expect(onDenyAccessSpy.called).to.equal(false);
  });

  it('should not call onDenyAccess when deny access failed with a non graphql error and show generic error flag', async () => {
    const failureDenyAccessStub = sandbox.stub().rejects({
      graphQLErrors: [],
    });
    const wrapper = getDenyAccessModalWrapper({
      denyAccess: failureDenyAccessStub,
    });

    wrapper.find('Button').at(0).simulate('click');
    wrapper.update();
    await new Promise(resolve => setTimeout(resolve, 0));

    expect(showFlagSpy.called).to.equal(true);
    expect(showFlagSpy.firstCall.args[0].title).to.equal(mockIntl.formatMessage(messages.denyFailureTitle));
  });

  it('should show already denied error flag when error 412 status - precondition failed', async () => {
    const failureDenyAccessStub = sandbox.stub().rejects({
      graphQLErrors: [{ originalError: new RequestError({ status: 412 }) }],
    });
    const wrapper = getDenyAccessModalWrapper({
      denyAccess: failureDenyAccessStub,
    });
    const { user } = generateMockAccessRequest();

    wrapper.find('Button').at(0).simulate('click');
    wrapper.update();
    await new Promise(resolve => setTimeout(resolve, 0));

    expect(showFlagSpy.called).to.equal(true);
    expect(showFlagSpy.calledWith({
      title: mockIntl.formatMessage(messages.alreadyActionedTitle),
      description: mockIntl.formatMessage(messages.alreadyActionedDescription, {
        userDisplayName: user.displayName,
      }),
    }));
  });
});
