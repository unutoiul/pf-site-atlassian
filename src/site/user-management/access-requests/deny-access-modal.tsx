import * as H from 'history';
import * as React from 'react';
import { graphql, MutationFunc } from 'react-apollo';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import { ButtonGroup as AkButtonGroup } from '@atlaskit/button';
import { FieldTextAreaStateless as AkFieldTextAreaStateless } from '@atlaskit/field-text-area';
import { colors as akColors } from '@atlaskit/theme';
import OriginTracing from '@atlassiansox/origin-tracing';

import {
  AnalyticsClientProps,
  Button,
  denyAccessConfirmUIData,
  denyAccessRequestModalViewData,
  denyAccessTrackData,
  ScreenEventSender,
  withAnalyticsClient,
} from 'common/analytics';
import { createErrorIcon, createSuccessIcon, RequestError } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';
import { ModalDialog } from 'common/modal';

import { DenyAccessMutation, DenyAccessMutationArgs } from '../../../schema/schema-types';

import { AccessRequest } from '../../../schema/schema-types/schema-types';
import denyAccessMutation from './deny-access.mutation.graphql';
import { getAccessRequestDetails } from './get-access-request-details';

const RemainingCountContainer = styled.div`
  color: ${akColors.N200};
  padding-top: 5px;
`;

export const messages = defineMessages({
  denyAccess: {
    id: `user-management.access-requests.deny-access-modal.deny`,
    description: 'Shown on a button to deny permissions',
    defaultMessage: `Deny access`,
  },
  cancel: {
    id: `user-management.access-requests.deny-access-modal.cancel`,
    description: 'Shown on a button to close popup',
    defaultMessage: `Cancel`,
  },
  denyAccessDescription: {
    id: `user-management.access-requests.deny-access-modal-description`,
    description: 'Shown on deny access popup',
    defaultMessage: `You're about to deny {userDisplayName} access to {productName}, preventing them from requesting access in the future. We'll let them know they were denied access.`,
  },
  denyAccessTitle: {
    id: `user-management.access-requests.deny-access-modal-title`,
    description: 'Shown on title of deny access popup',
    defaultMessage: `Deny access`,
  },
  remainingCharacters: {
    id: `user-management.access-requests.deny-access-modal-remaining-characters`,
    description: 'Shows remaining characters user can type',
    defaultMessage: `Characters left: {remaining}`,
  },
  denialReasonTitle: {
    id: `user-management.access-requests.deny-access-modal-denial-reason-title`,
    description: 'Title for textbox where the admin will enter the reason for denial',
    defaultMessage: `Message`,
  },
  denialReasonPlaceholder: {
    id: `user-management.access-requests.deny-access-modal-denial-reason-placeholder`,
    description: 'Placeholder text to enter the reason for denial',
    defaultMessage: `Hi, we've denied your request because...`,
  },
  denySuccessTitle: {
    id: `user-management.access-requests.deny-access-modal-deny-success-title`,
    description: 'Title of the flag which is shown when access is denied',
    defaultMessage: `Access denied to {userDisplayName}`,
  },
  denySuccessDescription: {
    id: `user-management.access-requests.deny-access-modal-deny-success-description`,
    description: 'Description of the flag which is shown when access is denied',
    defaultMessage: `{userDisplayName} is not able to use {productName}.`,
  },
  denyFailureTitle: {
    id: `user-management.access-requests.deny-access-modal-deny-failure-title`,
    description: 'Title of the flag which is shown when denying access failed',
    defaultMessage: `Unable to deny access. Try again later.`,
  },
  userDetails: {
    id: `user-management.access-requests.deny-access-modal-deny-success-user-details`,
    description: 'Link text which navigates to user\'s details page',
    defaultMessage: `See user profile for {userDisplayName}`,
  },
  alreadyActionedTitle: {
    id: `user-management.access-requests.already-denied-access-title`,
    description: 'Shown when access request for a user is already denied',
    defaultMessage: `Error - Request already actioned`,
  },
  alreadyActionedDescription: {
    id: `user-management.access-requests.already-denied-access-description`,
    description: 'Shown when access request for a user is already denied',
    defaultMessage: `This request for {userDisplayName} has already been actioned. You can manage their access from their user profile.`,
  },
});

interface OwnProps {
  cloudId: string;
  isOpen: boolean;
  accessRequest: AccessRequest;
  history: H.History;
  onDenyAccess(): void;
  onDialogDismissed(): void;
}

interface State {
  denialReason: string;
  isDenyAccessLoading: boolean;
}

const MAX_DENIAL_REASON_LENGTH = 250;

interface MutationProps {
  denyAccess: MutationFunc<DenyAccessMutation, DenyAccessMutationArgs>;
}

export type DenyModalProps = OwnProps & AnalyticsClientProps & FlagProps & InjectedIntlProps & MutationProps;

export class DenyAccessModalImpl extends React.Component<DenyModalProps, State> {

  public readonly state: Readonly<State> = {
    denialReason: '',
    isDenyAccessLoading: false,
  };

  public static getDerivedStateFromProps(nextProps: DenyModalProps, prevState: State): State {
    const { isOpen } = nextProps;

    if (isOpen) {
      return prevState;
    }

    return {
      denialReason: '',
      isDenyAccessLoading: false,
    };
  }

  public render() {
    const {
      cloudId,
      isOpen,
      onDialogDismissed,
      accessRequest,
    } = this.props;
    const { user, requestedProducts } = accessRequest;
    const [{ productName }] = requestedProducts;
    const { formatMessage } = this.props.intl;
    const { denialReason, isDenyAccessLoading } = this.state;
    const footer = (
      <AkButtonGroup>
        <Button
          appearance="primary"
          onClick={this.handleDeny}
          isLoading={isDenyAccessLoading}
        >
          {formatMessage(messages.denyAccess)}
        </Button>
        <Button
          appearance="subtle-link"
          onClick={onDialogDismissed}
        >
          {formatMessage(messages.cancel)}
        </Button>
      </AkButtonGroup>
    );

    return (
      <ModalDialog
        header={formatMessage(messages.denyAccessTitle)}
        footer={footer}
        isOpen={isOpen}
        width="small"
        onClose={onDialogDismissed}
      >
        <ScreenEventSender
          event={{
            data: denyAccessRequestModalViewData(getAccessRequestDetails(accessRequest)),
            cloudId,
          }}
        >
          <div>
            {formatMessage(messages.denyAccessDescription, {
              userDisplayName: user.displayName,
              productName,
            })}
          </div>
          <AkFieldTextAreaStateless
            label={formatMessage(messages.denialReasonTitle)}
            placeholder={formatMessage(messages.denialReasonPlaceholder)}
            onChange={this.onChange}
            value={denialReason}
            shouldFitContainer={true}
            enableResize={'vertical'}
            maxLength={MAX_DENIAL_REASON_LENGTH}
          />
          <div>
            <RemainingCountContainer>
              {formatMessage(messages.remainingCharacters, { remaining: this.getRemaining() })}
            </RemainingCountContainer>
          </div>
        </ScreenEventSender>
      </ModalDialog>
    );
  }

  private getRemaining = () => {
    return MAX_DENIAL_REASON_LENGTH - this.state.denialReason.length;
  };

  private onChange = (event) => {
    const denialReason = event.target.value;
    this.setState({ denialReason });
  };

  private handleDeny = async () => {
    const {
      cloudId,
      denyAccess,
      onDenyAccess,
      accessRequest,
      intl: { formatMessage },
    } = this.props;
    const { user, requestedProducts } = accessRequest;
    const [{ productId }] = requestedProducts;
    const { denialReason } = this.state;
    const origin = new OriginTracing({ product: 'admin' });

    this.setState({ isDenyAccessLoading: true });

    this.props.analyticsClient.sendUIEvent({
      cloudId,
      data: denyAccessConfirmUIData({
        ...getAccessRequestDetails(accessRequest),
        denialReasonLength: denialReason.length,
        ...origin.toAnalyticsAttributes({ hasGeneratedId: true }),
      }),
    });

    try {
      await denyAccess({
        variables: {
          cloudId,
          input: {
            productId,
            denialReason,
            userId: user.id,
          },
        },
      });

      this.showSuccessFlag();
      onDenyAccess();

      this.props.analyticsClient.sendTrackEvent({
        cloudId,
        data: denyAccessTrackData({
          ...getAccessRequestDetails(accessRequest),
          denialReasonLength: denialReason.length,
          ...origin.toAnalyticsAttributes({ hasGeneratedId: true }),
        }),
      });
    } catch (e) {
      if (!e.graphQLErrors.length || !e.graphQLErrors[0].originalError) {
        this.showErrorFlag();

        return;
      }

      const originalError = e.graphQLErrors[0].originalError as RequestError;
      const isPreconditionFailed = originalError.status === 412;

      if (isPreconditionFailed) {
        this.showErrorFlag({
          title: formatMessage(messages.alreadyActionedTitle),
          description: formatMessage(messages.alreadyActionedDescription, { userDisplayName: user.displayName }),
        });
      } else {
        this.showErrorFlag();
      }
    } finally {
      this.setState({ isDenyAccessLoading: false });
    }
  };

  private showSuccessFlag = () => {
    const {
      showFlag,
      intl: { formatMessage },
      accessRequest: { user, requestedProducts },
    } = this.props;
    const [requestedProduct] = requestedProducts;

    const title = formatMessage(messages.denySuccessTitle, {
      userDisplayName: user.displayName,
    });
    const description = formatMessage(messages.denySuccessDescription, {
      userDisplayName: user.displayName,
      productName: requestedProduct.productName,
    });

    showFlag({
      autoDismiss: true,
      icon: createSuccessIcon(),
      id: `user.access-request.deny.success.${Date.now()}`,
      title,
      description,
      actions: [{
        content: formatMessage(messages.userDetails, {
          userDisplayName: user.displayName,
        }),
        onClick: this.gotoUserDetailsPage,
      }],
    });
  };

  private showErrorFlag = ({ title = '', description = '' } = {}) => {
    const {
      showFlag,
      intl: { formatMessage },
      accessRequest: { user },
    } = this.props;

    const defaultErrorTitle = formatMessage(messages.denyFailureTitle);

    showFlag({
      autoDismiss: true,
      icon: createErrorIcon(),
      id: `user.access-request.deny.error.${Date.now()}`,
      title: title || defaultErrorTitle,
      description,
      actions: [{
        content: formatMessage(messages.userDetails, {
          userDisplayName: user.displayName,
        }),
        onClick: this.gotoUserDetailsPage,
      }],
    });
  };

  private gotoUserDetailsPage = () => {
    const { accessRequest: { user } } = this.props;
    window.location.href = `/admin/users/view?email=${encodeURIComponent(user.email)}`;
  };
}

const withDenyAccessMutation = graphql<OwnProps, DenyAccessMutation>(denyAccessMutation, {
  name: 'denyAccess',
});

export const DenyAccessModal = withDenyAccessMutation(
  injectIntl(
    withAnalyticsClient(
      withFlag(DenyAccessModalImpl),
    ),
  ),
);
