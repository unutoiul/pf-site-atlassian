import styled from 'styled-components';

import { colors as akColors } from '@atlaskit/theme';

export const TableWrapper = styled.div`
    flex-grow: 1;
`;

export const RequestedDateWrapper = styled.div`
  color: ${akColors.subtleText};
  font-size: 0.85em;
  text-transform: capitalize;
`;

export const RequesterWrapper = styled.a`
  color: inherit;
  &:focus, &:hover, &:visited, &:link, &:active{
    color: inherit;
    outline: none;
  }
`;
