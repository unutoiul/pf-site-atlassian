import * as H from 'history';
import * as React from 'react';
import {
  defineMessages,
  InjectedIntlProps,
  injectIntl,
} from 'react-intl';

import { Action, Actions } from 'common/actions';
import {
  AnalyticsClientProps,
  denyAccessUIData,
  showProfileDropDownUIEventData,
  withAnalyticsClient,
} from 'common/analytics';

import { AccessRequest } from '../../../schema/schema-types/schema-types';
import { DenyAccessModal } from './deny-access-modal';
import { getAccessRequestDetails } from './get-access-request-details';

export const messages = defineMessages({
  showProfile: {
    id: `user-management.access-requests.table.show-profile`,
    description: 'Drop down item which will take the user to user profile page',
    defaultMessage: `Show profile`,
  },
  approveAccess: {
    id: `user-management.access-requests.approve-access`,
    description: 'Shown on a button to approve permissions',
    defaultMessage: `Grant access`,
  },
  denyAccess: {
    id: `user-management.access-requests.deny-access`,
    description: 'Shown on a button to deny permissions',
    defaultMessage: `Deny access`,
  },
});

export interface AccessRequestsActionsProps {
  cloudId: string;
  accessRequest: AccessRequest;
  history: H.History;
  onApproveAccess(accessRequest: AccessRequest): Promise<void>;
  onDenyAccess(accessRequest: AccessRequest): Promise<void>;
}

interface AccessRequestsActionsState {
  isDenyAccessModalOpen: boolean;
}

export class AccessRequestsActionsImpl extends React.Component<AccessRequestsActionsProps & InjectedIntlProps & AnalyticsClientProps, AccessRequestsActionsState> {
  public readonly state: Readonly<AccessRequestsActionsState> = {
    isDenyAccessModalOpen: false,
  };

  public render() {
    const { accessRequest, cloudId, history } = this.props;

    return (
      <React.Fragment>
        <Actions>
          <Action actionType="custom" customMessage={messages.approveAccess} onAction={this.onApprove} />
          <Action actionType="custom" customMessage={messages.showProfile} onAction={this.showProfile} />
          <Action actionType="custom" customMessage={messages.denyAccess} onAction={this.handleDenyAccess} />
        </Actions>
        <DenyAccessModal
          cloudId={cloudId}
          isOpen={this.state.isDenyAccessModalOpen}
          accessRequest={accessRequest}
          history={history}
          onDenyAccess={this.onDenyConfirm}
          onDialogDismissed={this.toggleDenyAccessModal}
        />
      </React.Fragment>
    );
  }

  private handleDenyAccess = () => {
    const { cloudId, accessRequest } = this.props;

    this.props.analyticsClient.sendUIEvent({
      cloudId,
      data: denyAccessUIData(getAccessRequestDetails(accessRequest)),
    });
    this.toggleDenyAccessModal();
  };

  private showProfile = () => {
    const { cloudId, accessRequest } = this.props;
    this.props.analyticsClient.sendUIEvent({
      cloudId,
      data: showProfileDropDownUIEventData(getAccessRequestDetails(accessRequest)),
    });
    window.location.href = `/admin/users/view?email=${encodeURIComponent(accessRequest.user.email)}`;
  };

  private onApprove = async () => {
    const { onApproveAccess, accessRequest } = this.props;

    await onApproveAccess(accessRequest);
  };

  private onDenyConfirm = async () => {
    const { onDenyAccess, accessRequest } = this.props;

    this.toggleDenyAccessModal();
    await onDenyAccess(accessRequest);
  };

  private toggleDenyAccessModal = () => {
    this.setState(prevState => ({
      isDenyAccessModalOpen: !prevState.isDenyAccessModalOpen,
    }));
  };
}

export const AccessRequestsActions = withAnalyticsClient<AccessRequestsActionsProps>(
  injectIntl(AccessRequestsActionsImpl),
);
