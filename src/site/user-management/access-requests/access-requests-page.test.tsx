import { expect } from 'chai';
import { shallow } from 'enzyme';
import { Location } from 'history';
import * as React from 'react';
import { sandbox as sinonSandbox } from 'sinon';

import OriginTracing from '@atlassiansox/origin-tracing';

import { RequestError } from 'common/error';

import { AccessRequest, AccessRequestsQuery } from '../../../schema/schema-types';
import { RequestedProduct } from '../../../schema/schema-types/schema-types';
import { createMockAnalyticsClient, createMockContext, createMockIntlProp, waitUntil } from '../../../utilities/testing';
import { AccessRequestsPageImpl, messages } from './access-requests-page';
import { AccessRequestsTable } from './access-requests-table';

describe('AccessRequestsPage', () => {
  const sandbox = sinonSandbox.create();
  let fetchMoreStub: sinon.SinonStub;
  let approveAccessMutationStub: sinon.SinonStub;
  let refetchStub: sinon.SinonStub;
  let historyPushSpy: sinon.SinonSpy;
  let historyReplaceSpy: sinon.SinonSpy;
  let showFlagSpy: sinon.SinonSpy;
  let locationStub: Location;
  const mockIntl = createMockIntlProp();
  const mockAnalyticsClient = createMockAnalyticsClient();

  const requestedProduct: RequestedProduct = {
    productId: 'confluence',
    productName: 'Confluence',
    status: 'PENDING',
    requestedTime: '2017-07-10T14:00:00Z',
  };
  const accessRequest: AccessRequest = {
    user: {
      id: '557057:80abfbd6-e1b5-4f23-99d9-077658050e25',
      displayName: 'Alex Sim',
      email: 'asimkin@atlassian.com',
      active: true,
      system: false,
      hasVerifiedEmail: true,
    },
    requestedProducts: [requestedProduct],
    type: 'REQUEST',
    requester: {
      id: '557057:80abfbd6-e1b5-4f23-99d9-077658050e25',
      displayName: 'Alex Sim',
      email: 'asimkin@atlassian.com',
      active: true,
      system: false,
      hasVerifiedEmail: true,
    },
  };
  const accessRequestsData: AccessRequestsQuery = {
    accessRequestList: {
      total: 1,
      accessRequests: [accessRequest],
    } as any,
  };
  const lastAccessRequestsData: AccessRequestsQuery = {
    accessRequestList: {
      total: 21,
      accessRequests: [accessRequest],
    } as any,
  };
  const cloudId = 'FAKE';

  interface WrapperProps {
    data?: any;
    location?: Location;
  }

  const getPageWrapper = ({ data = {}, location = window.location as any }: WrapperProps, renderWithInitialData?: boolean) => {
    const newData = {
      fetchMore: fetchMoreStub,
      refetch: refetchStub,
      ...data,
    };

    const wrapper = shallow((
      <AccessRequestsPageImpl
        data={renderWithInitialData ? newData : undefined}
        location={location}
        match={{ params: { cloudId } }}
        intl={mockIntl}
        history={{ push: historyPushSpy, replace: historyReplaceSpy }}
        approveAccess={approveAccessMutationStub}
        showFlag={showFlagSpy}
        analyticsClient={mockAnalyticsClient}
        {...{} as any}
      />
    ), {
      ...createMockContext({ intl: true, apollo: true }),
      disableLifecycleMethods: false,
    } as any);

    if (!renderWithInitialData) {
      wrapper.setProps({ data: newData });
      wrapper.update();
    }

    return wrapper;
  };

  beforeEach(() => {
    fetchMoreStub = sandbox.stub().resolves({});
    approveAccessMutationStub = sandbox.stub().resolves();
    refetchStub = sandbox.stub().resolves({});
    historyPushSpy = sandbox.spy();
    historyReplaceSpy = sandbox.spy();
    showFlagSpy = sandbox.spy();
    locationStub = {
      hash: '',
      pathname: '/access-requests',
      search: '',
      state: undefined,
    };
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('should render an error if there is an error', () => {
    const wrapper = getPageWrapper({
      data: {
        error: true,
      },
    });

    expect(wrapper.find('span').text()).to.equal(messages.error.defaultMessage);
  });

  it('should render a spinner if the page is loading', () => {
    const wrapper = getPageWrapper({
      data: {
        loading: true,
      },
    });

    expect(wrapper.find(AccessRequestsTable).prop('isLoading')).to.equal(true);
  });

  it('should show the access requests table', async () => {
    const wrapper = getPageWrapper({ data: accessRequestsData });

    expect(refetchStub.called).to.equal(false);

    await waitUntil(() => wrapper.find(AccessRequestsTable).prop('isLoading') === false);

    expect(wrapper.find(AccessRequestsTable).prop('isLoading')).to.equal(false);
    expect(wrapper.find(AccessRequestsTable).prop('rows')).to.deep.equal(accessRequestsData.accessRequestList.accessRequests);
    expect(wrapper.find(AccessRequestsTable).prop('rowsPerPage')).to.equal(20);
    expect(mockAnalyticsClient.sendScreenEvent.calledWith({
      cloudId,
      data: {
        name: 'accessRequestSiteAdminScreen',
      },
    }));
  });

  it('should call refetch when data is provided immediately', async () => {
    getPageWrapper({ data: accessRequestsData }, true);

    expect(refetchStub.called).to.equal(true);
  });

  it('should update query param and fetch next page data when onSetPage is called', () => {
    const wrapper = getPageWrapper({ data: accessRequestsData, location: locationStub });

    const onSetPageProp: (n: number) => void = wrapper.find(AccessRequestsTable).prop('onSetPage');
    onSetPageProp(2);

    const fetchMoreArgs = fetchMoreStub.getCall(0).args[0];

    expect(historyPushSpy.getCall(0).args[0]).to.equal(`/access-requests?page=2`);
    expect(fetchMoreArgs.variables).to.deep.equal({ start: 21 });
    expect(fetchMoreArgs.updateQuery({ foo: 1 }, { fetchMoreResult: { foo: 2 } })).to.deep.equal({ foo: 2 });
  });

  it('should call approveAccess mutation on onApproveAccess callback and show success flag', async () => {
    const wrapper = getPageWrapper({ data: accessRequestsData });

    const onApproveAccess = wrapper.find(AccessRequestsTable).prop('onApproveAccess');
    await onApproveAccess(accessRequest);

    const mutationArgs = approveAccessMutationStub.firstCall.args[0];
    const expectedArgs = {
      variables: {
        cloudId,
        input: {
          productId: requestedProduct.productId,
          userId: accessRequest.user.id,
        },
      },
    };
    const expectedTitle = mockIntl.formatMessage(messages.approveSuccessTitle, {
      userDisplayName: accessRequest.user.displayName,
    });
    const expectedDescription = mockIntl.formatMessage(messages.approveSuccessDescription, {
      userDisplayName: accessRequest.user.displayName,
      productName: requestedProduct.productName,
    });

    expect(approveAccessMutationStub.called).to.equal(true);
    expect(mutationArgs.variables.cloudId).to.deep.include(expectedArgs.variables.cloudId);
    expect(mutationArgs.variables.input).to.deep.include(expectedArgs.variables.input);
    expect(mutationArgs.variables.input.atlOrigin).not.to.be.undefined();
    expect(refetchStub.called).to.equal(true);
    expect(showFlagSpy.called).to.equal(true);
    expect(showFlagSpy.firstCall.args[0].title).to.equal(expectedTitle);
    expect(showFlagSpy.firstCall.args[0].description).to.equal(expectedDescription);
  });

  it('should refetch and show success flag when onDenyAccess is called', async () => {
    const wrapper = getPageWrapper({ data: accessRequestsData });

    await wrapper.find(AccessRequestsTable).prop('onDenyAccess')(accessRequest);

    expect(refetchStub.called).to.equal(true);
    expect(wrapper.find(AccessRequestsTable).prop('isLoading')).to.equal(false);
  });

  it('should refetch and show error flag when onDenyAccess is called and refetch fails', async () => {
    const failureRefetchStub = sandbox.stub().rejects({});
    const wrapper = getPageWrapper({ data: {
      ...accessRequestsData,
      refetch: failureRefetchStub,
    } });

    await wrapper.find(AccessRequestsTable).prop('onDenyAccess')(accessRequest);

    expect(failureRefetchStub.called).to.equal(true);
    expect(wrapper.find(AccessRequestsTable).prop('isLoading')).to.equal(false);
    expect(showFlagSpy.called).to.equal(true);
    expect(showFlagSpy.firstCall.args[0].title).to.deep.equal(mockIntl.formatMessage(messages.error));
  });

  it('should goto previous page if the last access request is approved and page >= 2', async () => {
    locationStub.search = '?page=2';
    const wrapper = getPageWrapper({ data: lastAccessRequestsData, location: locationStub });

    const onApproveAccess = wrapper.find(AccessRequestsTable).prop('onApproveAccess');
    await onApproveAccess(accessRequest);

    const fetchMoreArgs = fetchMoreStub.getCall(0).args[0];

    expect(historyPushSpy.getCall(0).args[0]).to.equal(`/access-requests?page=1`);
    expect(fetchMoreArgs.variables).to.deep.equal({ start: 1 });
  });

  it('should goto previous page if the last access request is rejected and page >= 2', async () => {
    locationStub.search = '?page=2';
    const wrapper = getPageWrapper({ data: lastAccessRequestsData, location: locationStub });

    const onDenyAccess = wrapper.find(AccessRequestsTable).prop('onDenyAccess');
    await onDenyAccess(accessRequest);

    const fetchMoreArgs = fetchMoreStub.getCall(0).args[0];

    expect(historyPushSpy.getCall(0).args[0]).to.equal(`/access-requests?page=1`);
    expect(fetchMoreArgs.variables).to.deep.equal({ start: 1 });
  });

  it('should show precondition fail error flag when onApproveAccess fails with 412 status - precondition failed', async () => {
    approveAccessMutationStub = sandbox.stub().rejects({
      graphQLErrors: [{ originalError: new RequestError({ status: 412 }) }],
    });
    const wrapper = getPageWrapper({ data: accessRequestsData });

    const onApproveAccess = wrapper.find(AccessRequestsTable).prop('onApproveAccess');
    await onApproveAccess(accessRequest);

    const expectedTitle = mockIntl.formatMessage(messages.alreadyActionedTitle);

    const expectedDescription = mockIntl.formatMessage(messages.alreadyActionedDescription, {
      userDisplayName: accessRequest.user.displayName,
    });

    expect(showFlagSpy.calledWith({ title: expectedTitle, description: expectedDescription }));
  });

  it('should show invalid product error flag when onApproveAccess fails with 402 status', async () => {
    approveAccessMutationStub = sandbox.stub().rejects({
      graphQLErrors: [{ originalError: new RequestError({ status: 402 }) }],
    });
    const wrapper = getPageWrapper({ data: accessRequestsData });

    const onApproveAccess = wrapper.find(AccessRequestsTable).prop('onApproveAccess');
    await onApproveAccess(accessRequest);

    const expectedTitle = mockIntl.formatMessage(messages.invalidProductTitle);
    const expectedDescription = mockIntl.formatMessage(messages.invalidProductDescription);

    expect(showFlagSpy.calledWith({ title: expectedTitle, description: expectedDescription }));
  });

  it('should show generic error flag when onApproveAccess fails without graphql errors', async () => {
    approveAccessMutationStub = sandbox.stub().rejects({
      graphQLErrors: [],
    });
    const wrapper = getPageWrapper({ data: accessRequestsData });

    const onApproveAccess = wrapper.find(AccessRequestsTable).prop('onApproveAccess');
    await onApproveAccess(accessRequest);

    const expectedTitle = mockIntl.formatMessage(messages.approveFailureTitle);

    expect(showFlagSpy.calledWith({ title: expectedTitle }));
  });

  it('should replace atlOrigin query param', async () => {
    const origin = new OriginTracing({ product: 'admin' });
    locationStub.search = `?page=2&atlOrigin=${origin.encode()}`;

    const wrapper = getPageWrapper({ data: accessRequestsData, location: locationStub });

    await waitUntil(() => wrapper.find(AccessRequestsTable).prop('isLoading') === false);
    expect(historyReplaceSpy.calledWith(`${location.pathname}?page=2`));
    expect(mockAnalyticsClient.sendScreenEvent.calledWith({
      cloudId,
      data: {
        name: 'accessRequestSiteAdminScreen',
        originIdGenerated: origin.id,
        originProduct: origin.product,
      },
    }));
  });
});
