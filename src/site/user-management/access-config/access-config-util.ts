import { Group, ProductGroupsConfig, ProductId } from './access-config-type';

// Maps an object of ProductGroupsConfig into a Map where key is ProductId, and entries an array of groups
export const formatProduct = (accessConfig: ProductGroupsConfig[]): Map<ProductId, Group[]> => {
  const allProducts = new Map<ProductId, Group[]>();
  accessConfig.forEach(config => allProducts.set(config.product.productId, config.groups));

  return allProducts;
};

export const formatImportedProduct = (useGroups: Map<ProductId, Group[]>): Map<ProductId, Group[]> => {
  const importedGroupsMap = new Map<ProductId, Group[]>();
  useGroups.forEach((value, key) => {
    const groupsRequiringApproval = value.filter(group => group.requiresApproval);
    if (groupsRequiringApproval.length) {
      importedGroupsMap.set(key, groupsRequiringApproval);
    }
  });

  return importedGroupsMap;
};

// Jira products have a special use case where there the USE productId is not the same as the ADMIN productId.
// All Jira products have a bundled administration product called "Jira administration"
export const determineAdminProductId = (productId: ProductId) => {
  return (productId.includes('jira') || productId.includes('servicedesk')) ? 'jira-admin' : productId;
};

export const shouldMakeJiraCoreDefault = (
  useProductIds: ProductId[],
  defaultProductIds: ProductId[],
  toggledProductId: ProductId,
  settingDefault: boolean,
) => {

  // Jira Core is not a product
  if (!useProductIds.includes('jira-core')) {
    return false;
  }

  // Jira Core is already default
  if (defaultProductIds.includes('jira-core')) {
    return false;
  }

  // Jira Core is being set default, no need to set it again
  if (toggledProductId.includes('jira-core')) {
    return false;
  }

  // A Jira product is not being toggled
  if (!toggledProductId.includes('jira') && !toggledProductId.includes('servicedesk')) {
    return false;
  }

  // A Jira product is not being set to default
  if (!settingDefault) {
    return false;
  }

  return true;
};

export const productIdToProductName = {
  conf: 'Confluence',
  jira: 'Jira Legacy',
  refapp: 'Refapp',
  'hipchat.cloud': 'Stride',
  'jira-core': 'Jira Core',
  'jira-software': 'Jira Software',
  'jira-admin': 'Jira Administration',
  'jira-incident-manager': 'Jira Ops',
  'jira-servicedesk': 'Jira Service Desk',
  'jira-servicedesk.ondemand': 'Jira Service Desk Legacy',
};

export function sortByName(a: { name: string }, b: { name: string }) {
  const aName = a.name.toLowerCase();
  const bName = b.name.toLowerCase();

  return aName < bName ? -1 : aName > bName ? 1 : 0;
}
