import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';

import AkPage from '@atlaskit/page';

import { createApolloClient } from '../../../apollo-client';
import { AccessConfigImportActions } from './access-config-import-actions';

const client = createApolloClient();

storiesOf('Site|Access Config/Import actions', module)
  .add('Import actions', () => (
    <ApolloProvider client={client}>
    <IntlProvider locale="en">
      <AkPage>
        <AccessConfigImportActions
          cloudId="DUMMY-CLOUD-ID"
          productId="jira-core"
          groupName="Jira Server Users"
          groupId="249082349-2093489034-239748234"
        />
      </AkPage>
    </IntlProvider>
    </ApolloProvider>
  ));
