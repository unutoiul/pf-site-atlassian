import * as React from 'react';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import { DynamicTableStateless as AkDynamicTableStateless } from '@atlaskit/dynamic-table';
import AkLozenge from '@atlaskit/lozenge';
import { gridSize as akGridSize } from '@atlaskit/theme';

import { GroupName } from 'common/group-name';
import { InfoHover } from 'common/info-hover/info-hover';
import { EmptyTableView } from 'common/table';

import { ManagementAccess, OwnerType } from '../../../schema/schema-types';
import { AccessConfigDropdown } from './access-config-dropdown';
import { ConfigType, Group, ProductId } from './access-config-type';
import { sortByName } from './access-config-util';
import { OptionsHeader } from './access-config.styled';

export const messages = defineMessages({
  headerGroup: {
    id: `user-management.site-settings.access-configuration.groups.table.header.group`,
    defaultMessage: `Group`,
  },
  headerOptions: {
    id: `user-management.site-settings.access-configuration.groups.table.header.options`,
    defaultMessage: `Options`,
  },
  lozengeDefaultAccessGroup: {
    id: `user-management.site-settings.access-configuration.groups.table.lozenge.default.group`,
    defaultMessage: `Default access group`,
  },
  actionDisabledReason: {
    id: `user-management.site-settings.access-configuration.groups.table.action.disabled.reason`,
    defaultMessage: `Feature not available yet`,
  },
  infoHoverAdministration: {
    id: `user-management.site-settings.access-configuration.groups.table.info.hover.administration`,
    defaultMessage: `Grants admin access to all {product} products.`,
  },
  tableEmptyView: {
    id: `user-management.site-settings.access-configuration.groups.table.table.empty.view`,
    defaultMessage: `No groups found with access to {product}`,
  },
});

export const ActionsContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: ${akGridSize() * 2}px 0 ${akGridSize() * 2}px;
`;

export const LozengeContainer = styled.span`
  padding: ${akGridSize}px;
`;

const NameWrapper = styled.span`
  width: ${akGridSize() * 25}px;
`;

const CellWrapper = styled.div`
  display: flex;
  align-items: center;
`;

export interface OwnProps {
  cloudId: string;
  configType: ConfigType;
}

export interface ConfigProps {
  productName: string;
  groups: Group[];
  productId: ProductId;
}

export class AccessConfigGroupTableImpl extends React.Component<OwnProps & InjectedIntlProps & ConfigProps> {

  public render(): JSX.Element {

    const { formatMessage } = this.props.intl;

    const isImportTable = this.props.configType === 'importAccess';

    const head = {
      cells: [
        { key: 'header.group', content: formatMessage(messages.headerGroup), width: isImportTable ? 40 : 60 },
        { key: 'header.administrator', content: '', width: 15 },
        { key: 'header.options', content: <OptionsHeader>{formatMessage(messages.headerOptions)}</OptionsHeader>, width: isImportTable ? 45 : 25 },
      ],
    };

    const defaultGroupsLength = this.props.groups.filter(group => group.default).length;

    const rows = this.props.groups.filter(group => this.props.configType === 'importAccess' || !group.requiresApproval).sort(sortByName).map(group => {

      const Dropdown = () => (
        <AccessConfigDropdown
          cloudId={this.props.cloudId}
          isAdmin={!!group.admin}
          productName={this.props.productName}
          isLastDefault={group.default && defaultGroupsLength <= 1}
          configType={this.props.configType}
          groupName={group.name}
          groupId={group.id}
          productId={this.props.productId}
          isDefault={group.default}
        />
      );

      const uniqueId = `${this.props.productId}.${group.id}.${this.props.configType}`;

      return ({
        cells: [
          { key: `group.name.${uniqueId}`, content: this.generateGroupNameHeader(group.name, group.default, group.managementAccess, group.ownerType) },
          { key: `group.administrator.${uniqueId}`, content: this.generateAdministratorContent(group.admin || false) },
          { key: `header.options.${uniqueId}`, content: <Dropdown /> },
        ],
      });
    });

    return (
      <AkDynamicTableStateless
        head={head}
        rows={rows.length ? rows : undefined}
        emptyView={
          <EmptyTableView>
            {formatMessage(messages.tableEmptyView, { product: this.props.productName })}
          </EmptyTableView>
        }
      />
    );
  }

  private generateAdministratorContent(isAdminGroup: boolean): React.ReactNode {
    if (!isAdminGroup) {
      return null;
    }

    return (
      <InfoHover
        persistTime={0}
        dialogContent={
          <div>
            <FormattedMessage
              {...messages.infoHoverAdministration}
              values={{ product: this.props.productName }}
            />
          </div>
        }
      />
    );
  }

  private generateGroupNameHeader(groupName: string, isDefaultGroup: boolean, managementAccess: ManagementAccess, ownerType?: OwnerType | null): React.ReactNode {

    const adminLozenge: JSX.Element = (
      <AkLozenge appearance="default">
        {this.props.intl.formatMessage(messages.lozengeDefaultAccessGroup)}
      </AkLozenge>
    );

    return (
      <CellWrapper>
        <NameWrapper>
          <GroupName name={groupName} ownerType={ownerType} managementAccess={managementAccess} />
        </NameWrapper>
        <LozengeContainer>{isDefaultGroup && adminLozenge}</LozengeContainer>
      </CellWrapper>
    );
  }
}

export const AccessConfigGroupTable = injectIntl<ConfigProps & OwnProps>(AccessConfigGroupTableImpl);
