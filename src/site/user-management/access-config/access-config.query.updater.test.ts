import { expect } from 'chai';
import * as sinon from 'sinon';

import { optimisticallyRemoveGroup, optimisticallyUpdateDefaultGroup, optimisticallyUpdateImportedGroup } from './access-config.query.updater';

describe('Updater', () => {

  const sandbox = sinon.sandbox.create();
  const writeQuerySpy = sandbox.spy();

  beforeEach(() => {
    sandbox.reset();
  });

  function generateStore(data) {
    return {
      readQuery: () => ({
        currentSite: data,
      }),
      writeQuery: writeQuerySpy,
    } as any;
  }

  it('Should make group default', () => {
    const data = {
      id: 'cloud-id',
      __typename: 'CurrentSite',
      groupsUseAccessConfig: [{
        product: {
          productId: 'jira-core',
          producName: 'jira-core',
        },
        groups: [
          {
            id: 'group-id',
            name: 'group',
            default: false,
            requiresApproval: false,
          },
        ],
      }],
      groupsAdminAccessConfig: [],
    };

    optimisticallyUpdateDefaultGroup(generateStore(data), 'jira-core', 'group-id', false);
    expect(writeQuerySpy.callCount).to.equal(1);
    expect(writeQuerySpy.args[0][0].data.currentSite.groupsUseAccessConfig[0].groups).to.deep.equal([
      {
        id: 'group-id',
        name: 'group',
        default: true,
        requiresApproval: false,
      },
    ]);
  });

  it('Should make group not default', () => {
    const data = {
      id: 'cloud-id',
      __typename: 'CurrentSite',
      groupsUseAccessConfig: [{
        product: {
          productId: 'jira-core',
          producName: 'jira-core',
        },
        groups: [
          {
            id: 'group-id',
            name: 'group',
            default: true,
            requiresApproval: false,
          },
        ],
      }],
      groupsAdminAccessConfig: [],
    };

    optimisticallyUpdateDefaultGroup(generateStore(data), 'jira-core', 'group-id', true);
    expect(writeQuerySpy.callCount).to.equal(1);
    expect(writeQuerySpy.args[0][0].data.currentSite.groupsUseAccessConfig[0].groups).to.deep.equal([
      {
        id: 'group-id',
        name: 'group',
        default: false,
        requiresApproval: false,
      },
    ]);
  });

  it('Should not change group default status if no group id is found', () => {
    const data = {
      id: 'cloud-id',
      __typename: 'CurrentSite',
      groupsUseAccessConfig: [{
        product: {
          productId: 'jira-core',
          producName: 'jira-core',
        },
        groups: [
          {
            id: 'group-id',
            name: 'group',
            default: true,
            requiresApproval: false,
          },
        ],
      }],
      groupsAdminAccessConfig: [],
    };

    optimisticallyUpdateDefaultGroup(generateStore(data), 'jira-core', 'group-id-fake', false);
    expect(writeQuerySpy.callCount).to.equal(1);
    expect(writeQuerySpy.args[0][0].data.currentSite.groupsUseAccessConfig[0].groups).to.deep.equal([
      {
        id: 'group-id',
        name: 'group',
        default: true,
        requiresApproval: false,
      },
    ]);
  });

  it('Should not change group default status if no product id is found', () => {
    const data = {
      id: 'cloud-id',
      __typename: 'CurrentSite',
      groupsUseAccessConfig: [{
        product: {
          productId: 'jira-software',
          producName: 'jira-core',
        },
        groups: [
          {
            id: 'group-id',
            name: 'group',
            default: true,
            requiresApproval: false,
          },
        ],
      }],
      groupsAdminAccessConfig: [],
    };

    optimisticallyUpdateDefaultGroup(generateStore(data), 'jira-core', 'group-id', true);
    expect(writeQuerySpy.callCount).to.equal(1);
    expect(writeQuerySpy.args[0][0].data.currentSite.groupsUseAccessConfig[0].groups).to.deep.equal([
      {
        id: 'group-id',
        name: 'group',
        default: true,
        requiresApproval: false,
      },
    ]);
  });

  it('When granting import access, will remove the group', () => {
    const data = {
      id: 'cloud-id',
      __typename: 'CurrentSite',
      groupsUseAccessConfig: [{
        product: {
          productId: 'jira-core',
          producName: 'jira-core',
        },
        groups: [
          {
            id: 'group-id',
            name: 'group',
            default: false,
            requiresApproval: true,
          },
        ],
      }, {
        product: {
          productId: 'conf',
          producName: 'confluence',
        },
        groups: [
          {
            id: 'group-id',
            name: 'group',
            default: false,
            requiresApproval: false,
          },
        ],
      }],
      groupsAdminAccessConfig: [],
    };

    optimisticallyUpdateImportedGroup(generateStore(data), 'grant', 'jira-core', 'group-id');
    expect(writeQuerySpy.callCount).to.equal(1);
    expect(writeQuerySpy.args[0][0].data.currentSite.groupsUseAccessConfig[0].groups).to.deep.equal([
      {
        id: 'group-id',
        name: 'group',
        default: false,
        requiresApproval: false,
      },
    ]);
  });

  it('When revoking import access, will remove the group', () => {
    const data = {
      id: 'cloud-id',
      __typename: 'CurrentSite',
      groupsUseAccessConfig: [{
        product: {
          productId: 'jira-core',
          producName: 'jira-core',
        },
        groups: [
          {
            id: 'group-id',
            name: 'group',
            default: false,
            requiresApproval: true,
          },
        ],
      }, {
        product: {
          productId: 'conf',
          producName: 'confluence',
        },
        groups: [
          {
            id: 'group-id',
            name: 'group',
            default: false,
            requiresApproval: false,
          },
        ],
      }],
      groupsAdminAccessConfig: [],
    };

    optimisticallyUpdateImportedGroup(generateStore(data), 'revoke', 'jira-core', 'group-id');
    expect(writeQuerySpy.callCount).to.equal(1);
    expect(writeQuerySpy.args[0][0].data.currentSite.groupsUseAccessConfig[0].groups).to.deep.equal([]);
  });

  it('Does not remove any groups if the productId is not found', () => {

    const data = {
      id: 'cloud-id',
      __typename: 'CurrentSite',
      groupsUseAccessConfig: [{
        product: {
          productId: 'jira-core',
          producName: 'jira-core',
        },
        groups: [
          {
            id: 'group-id',
            name: 'group',
            default: false,
            requiresApproval: false,
          },
        ],
      }],
      groupsAdminAccessConfig: [{
        product: {
          productId: 'jira-core',
          producName: 'jira-core',
        },
        groups: [
          {
            id: 'group-id',
            name: 'group',
            default: false,
            requiresApproval: false,
          },
        ],
      }],
    };

    optimisticallyRemoveGroup(generateStore(data), 'productAccess', 'conf', '1234');
    expect(writeQuerySpy.callCount).to.equal(1);
    expect(writeQuerySpy.args[0][0].data.currentSite.groupsUseAccessConfig[0].groups).to.deep.equal([
      {
        id: 'group-id',
        name: 'group',
        default: false,
        requiresApproval: false,
      },
    ]);
  });

  it('Does not remove any groups if the groupId is not found', () => {
    const data = {
      id: 'cloud-id',
      __typename: 'CurrentSite',
      groupsUseAccessConfig: [{
        product: {
          productId: 'jira-core',
          producName: 'jira-core',
        },
        groups: [
          {
            id: 'group-id',
            name: 'group',
            default: false,
            requiresApproval: false,
          },
        ],
      }],
      groupsAdminAccessConfig: [{
        product: {
          productId: 'jira-core',
          producName: 'jira-core',
        },
        groups: [
          {
            id: 'group-id',
            name: 'group',
            default: false,
            requiresApproval: false,
          },
        ],
      }],
    };

    optimisticallyRemoveGroup(generateStore(data), 'productAccess', 'jira-core', '1234');
    expect(writeQuerySpy.callCount).to.equal(1);
    expect(writeQuerySpy.args[0][0].data.currentSite.groupsUseAccessConfig[0].groups).to.deep.equal([
      {
        id: 'group-id',
        name: 'group',
        default: false,
        requiresApproval: false,
      },
    ]);
  });

  it('Removes the group only from the corresponding product', () => {
    const data = {
      id: 'cloud-id',
      __typename: 'CurrentSite',
      groupsUseAccessConfig: [{
        product: {
          productId: 'jira-core',
          producName: 'jira-core',
        },
        groups: [
          {
            id: 'group-id',
            name: 'group',
            default: false,
            requiresApproval: false,
          },
        ],
      }, {
        product: {
          productId: 'conf',
          producName: 'confluence',
        },
        groups: [
          {
            id: 'group-id',
            name: 'group',
            default: false,
            requiresApproval: false,
          },
        ],
      }],
      groupsAdminAccessConfig: [],
    };

    optimisticallyRemoveGroup(generateStore(data), 'productAccess', 'jira-core', 'group-id');
    expect(writeQuerySpy.callCount).to.equal(1);
    expect(writeQuerySpy.args[0][0].data.currentSite.groupsUseAccessConfig[0].groups).to.deep.equal([]);
    expect(writeQuerySpy.args[0][0].data.currentSite.groupsUseAccessConfig[1].groups).to.deep.equal([
      {
        id: 'group-id',
        name: 'group',
        default: false,
        requiresApproval: false,
      },
    ]);
  });

  it('Removes the group only from the corresponding config type', () => {
    const data = {
      id: 'cloud-id',
      __typename: 'CurrentSite',
      groupsUseAccessConfig: [{
        product: {
          productId: 'jira-core',
          producName: 'jira-core',
        },
        groups: [
          {
            id: 'group-id',
            name: 'group',
            default: false,
            requiresApproval: false,
          },
        ],
      }],
      groupsAdminAccessConfig: [{
        product: {
          productId: 'jira-core',
          producName: 'jira-core',
        },
        groups: [
          {
            id: 'group-id',
            name: 'group',
            default: false,
            requiresApproval: false,
          },
        ],
      }],
    };

    optimisticallyRemoveGroup(generateStore(data), 'productAccess', 'jira-core', 'group-id');
    expect(writeQuerySpy.callCount).to.equal(1);
    expect(writeQuerySpy.args[0][0].data.currentSite.groupsUseAccessConfig[0].groups).to.deep.equal([]);
    expect(writeQuerySpy.args[0][0].data.currentSite.groupsAdminAccessConfig[0].groups).to.deep.equal([
      {
        id: 'group-id',
        name: 'group',
        default: false,
        requiresApproval: false,
      },
    ]);
  });
});
