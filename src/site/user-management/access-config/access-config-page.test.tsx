import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { AccessConfigQuery } from 'src/schema/schema-types';

import AkSpinner from '@atlaskit/spinner';
import AkTabs from '@atlaskit/tabs';

import { GenericError } from 'common/error';
import { PageLayout } from 'common/page-layout';

import { createMockIntlProp } from '../../../utilities/testing';
import { AccessConfigImportBanner } from './access-config-import-banner';
import { AccessConfigurationPageImpl } from './access-config-page';
import { AccessConfigTab, Configs } from './access-config-tab';
import { accessConfig, adminAccessConfig } from './access-config-test-util';
import { Group, ProductId } from './access-config-type';

describe('Access Config Groups Page Test', () => {

  function createTestComponent(data): ShallowWrapper {
    return shallow(
      <AccessConfigurationPageImpl
        match={{ params: { cloudId: 'dummy' } } as any}
        location={{} as any}
        history={{} as any}
        intl={createMockIntlProp()}
        data={data}
      />,
    );
  }

  it('Should show spinner when loading data', () => {
    const wrapper = createTestComponent({ loading: true });
    const pageLayout = wrapper.find(PageLayout);
    expect(pageLayout.length).to.equal(1);
    expect(pageLayout.find(AkSpinner).length).to.equal(1);
  });

  it('should show page and content when data loaded', () => {
    const wrapper = createTestComponent({ loading: false, error: false, currentSite: {
      groupsUseAccessConfig: accessConfig, groupsAdminAccessConfig: adminAccessConfig,
    }});
    const pageLayout = wrapper.find(PageLayout);
    expect(pageLayout.length).to.equal(1);
    const tabs = wrapper.find(AkTabs);
    expect(tabs.length).to.equal(1);
  });

  it('Should show error page on graphql error', () => {
    const wrapper = createTestComponent({ loading: false, error: true });
    expect(wrapper.find(GenericError).length).to.equal(1);
  });

  it('Should show error page when site data unavailable', () => {
    const wrapper = createTestComponent({ loading: false, error: false, currentSite: null });
    expect(wrapper.find(GenericError).length).to.equal(1);
  });

  describe('With import banner', () => {

    it('With all groups requiring approval, the banner should appear', () => {

      const data: AccessConfigQuery = {
        currentSite: {
          __typename: 'CurrentSite',
          id: 'cloud-id',
          groupsAdminAccessConfig: [],
          groupsUseAccessConfig: [{
            __typename: 'GroupsUseAccessConfig',
            product: {
              __typename: 'Product',
              productId: 'conf',
              productName: 'Confluence',
            },
            groups: [{
              __typename: 'Group',
              id: 'group-id',
              name: 'Hello world',
              default: false,
              productPermission: ['MANAGE'],
              requiresApproval: true,
            }],
          }],
        },
      };

      const expectedImportMap = new Map<ProductId, Group[]>();
      expectedImportMap.set('conf', [{ id: 'group-id', managementAccess: 'NONE', admin: true, name: 'Hello world', default: false, requiresApproval: true }]);

      const wrapper = createTestComponent({ loading: false, error: false, ...data });
      const parent = wrapper.find(AccessConfigImportBanner);
      expect(parent.exists()).to.equal(true);
      expect(parent.props().useGroups).to.deep.equal(expectedImportMap);
    });

  });

  describe('Admin access config', () => {

    function getAdminConfigTab(wrapper: ShallowWrapper): ShallowWrapper<Configs> {
      return shallow(wrapper.prop('tabs')[1].content).find(AccessConfigTab);
    }

    it('With admin products existing, but with no groups having access', () => {

      const data: AccessConfigQuery = {
        currentSite: {
          __typename: 'CurrentSite',
          id: 'cloud-id',
          groupsUseAccessConfig: [],
          groupsAdminAccessConfig: [{
            __typename: 'GroupsUseAccessConfig',
            product: {
              __typename: 'Product',
              productId: 'conf',
              productName: 'Confluence',
            },
            groups: [],
          }],
        },
      };

      const expectedUseConfig = new Map();
      expectedUseConfig.set('conf', []);

      const wrapper = createTestComponent({ loading: false, error: false, ...data });
      const parent = wrapper.find(AkTabs);

      const useConfigTab = getAdminConfigTab(parent);
      expect(useConfigTab.prop('config')).to.deep.equal(expectedUseConfig);
    });

    it('With admin products existing, and with groups having access, and management access is WRITE', () => {

      const data: AccessConfigQuery = {
        currentSite: {
          __typename: 'CurrentSite',
          id: 'cloud-id',
          groupsUseAccessConfig: [],
          groupsAdminAccessConfig: [{
            __typename: 'GroupsUseAccessConfig',
            product: {
              __typename: 'Product',
              productId: 'conf',
              productName: 'Confluence',
            },
            groups: [{
              __typename: 'Group',
              id: 'group-id',
              name: 'Hello world',
              default: false,
              productPermission: ['MANAGE'],
              requiresApproval: false,
            }],
          }],
        },
      };

      const expectedConfigObject: Group[] = [{
        id: 'group-id',
        name: 'Hello world',
        default: false,
        managementAccess: 'NONE',
        admin: true,
        requiresApproval: false,
      }];

      const wrapper = createTestComponent({ loading: false, error: false, ...data });
      const parent = wrapper.find(AkTabs);

      const useConfigTab = getAdminConfigTab(parent);
      expect(Array.from(useConfigTab.prop('config').values())).to.deep.equal([expectedConfigObject]);
    });
  });

  describe('Use access config', () => {

    function getUseConfigTab(wrapper: ShallowWrapper): ShallowWrapper<Configs> {
      return shallow(wrapper.prop('tabs')[0].content).find(AccessConfigTab);
    }

    it('With use products existing, but with no groups having access', () => {

      const data: AccessConfigQuery = {
        currentSite: {
          __typename: 'CurrentSite',
          id: 'cloud-id',
          groupsUseAccessConfig: [{
            __typename: 'GroupsUseAccessConfig',
            product: {
              __typename: 'Product',
              productId: 'conf',
              productName: 'Confluence',
            },
            groups: [],
          }],
          groupsAdminAccessConfig: [],
        },
      };

      const expectedUseConfig = new Map();
      expectedUseConfig.set('conf', []);

      const wrapper = createTestComponent({ loading: false, error: false, ...data });
      const parent = wrapper.find(AkTabs);

      const useConfigTab = getUseConfigTab(parent);
      expect(useConfigTab.prop('config')).to.deep.equal(expectedUseConfig);
    });

    it('With use products existing, and with groups having access, and management access is WRITE', () => {

      const data: AccessConfigQuery = {
        currentSite: {
          __typename: 'CurrentSite',
          id: 'cloud-id',
          groupsUseAccessConfig: [{
            __typename: 'GroupsUseAccessConfig',
            product: {
              __typename: 'Product',
              productId: 'conf',
              productName: 'Confluence',
            },
            groups: [{
              __typename: 'Group',
              id: 'group-id',
              name: 'Hello world',
              default: false,
              productPermission: ['WRITE'],
              requiresApproval: false,
            }],
          }],
          groupsAdminAccessConfig: [],
        },
      };

      const expectedConfigObject: Group[] = [{
        id: 'group-id',
        name: 'Hello world',
        default: false,
        managementAccess: 'NONE',
        admin: false,
        requiresApproval: false,
      }];

      const wrapper = createTestComponent({ loading: false, error: false, ...data });
      const parent = wrapper.find(AkTabs);

      const useConfigTab = getUseConfigTab(parent);
      expect(Array.from(useConfigTab.prop('config').values())).to.deep.equal([expectedConfigObject]);
    });

    it('With use products existing, and with groups having access, and management access is MANAGE', () => {

      const data: AccessConfigQuery = {
        currentSite: {
          __typename: 'CurrentSite',
          id: 'cloud-id',
          groupsUseAccessConfig: [{
            __typename: 'GroupsUseAccessConfig',
            product: {
              __typename: 'Product',
              productId: 'conf',
              productName: 'Confluence',
            },
            groups: [{
              __typename: 'Group',
              id: 'group-id',
              name: 'Hello world',
              default: false,
              productPermission: ['MANAGE'],
              requiresApproval: false,
            }],
          }],
          groupsAdminAccessConfig: [],
        },
      };

      const expectedConfigObject: Group[] = [{
        id: 'group-id',
        name: 'Hello world',
        default: false,
        managementAccess: 'NONE',
        admin: true,
        requiresApproval: false,
      }];

      const wrapper = createTestComponent({ loading: false, error: false, ...data });
      const parent = wrapper.find(AkTabs);

      const useConfigTab = getUseConfigTab(parent);
      expect(Array.from(useConfigTab.prop('config').values())).to.deep.equal([expectedConfigObject]);
    });
  });
});
