import { assert, expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import { Button as AnalyticsButton } from 'common/analytics';

import { ManagementAccess, SitePrivilege } from '../../../schema/schema-types';
import { createMockIntlProp } from '../../../utilities/testing';
// tslint:disable-next-line:disallow-relative-import-for-common
import { GroupsPickerModal } from '../common/groups-picker-modal';
import { AccessConfigAddGroupsToProductModalImpl, AccessConfigAddGroupsToProductModalProps } from './access-config-add-groups-to-product-modal';
const noop = () => null;

const group = {
  id: 'one',
  displayName: 'one',
  description: '',
  managementAccess: 'ALL' as ManagementAccess,
  sitePrivilege: 'NONE' as SitePrivilege,
  unmodifiable: false,
  ownerType: null,
};

describe('AccessConfigAddGroupsToProductModalImpl', () => {
  const sandbox = sinon.sandbox.create();
  const analyticsStub = {
    sendUIEvent: sandbox.spy(),
    sendTrackEvent: sandbox.spy(),
  };
  const showFlagSpy = sandbox.spy();
  const dismissSpy = sandbox.spy();

  beforeEach(() => {
    sandbox.reset();
  });

  const defaultProps: AccessConfigAddGroupsToProductModalProps = {
    isOpen: true,
    cloudId: 'test-id',
    productId: 'refapp',
    productName: 'test-name',
    productType: 'USE',
    intl: createMockIntlProp(),
    analyticsClient: analyticsStub as any,
    showFlag: showFlagSpy,
    hideFlag: noop,
    onDialogDismissed: dismissSpy,
  };

  it('should have a disabled add button when no groups are selected', async () => {
    const wrapper = shallow((
      <AccessConfigAddGroupsToProductModalImpl
        {...defaultProps}
        data={{} as any}
      />
    ));

    expect(wrapper.find(GroupsPickerModal).exists()).equals(true);
    const footer = shallow(wrapper.find(GroupsPickerModal).props().renderFooter(() => []) as any);
    const add = footer.find(AnalyticsButton).at(0);

    expect(add.prop('isDisabled')).to.equal(true);
  });

  it('should have a add button with isLoading when mutating and groups are selected', async () => {
    const wrapper = shallow((
      <AccessConfigAddGroupsToProductModalImpl
        {...defaultProps}
        data={{} as any}
      />
    ));

    expect(wrapper.find(GroupsPickerModal).exists()).equals(true);

    const addInitialState = shallow(wrapper.find(GroupsPickerModal).props().renderFooter(() => []) as any).find(AnalyticsButton).at(0);
    expect(addInitialState.prop('isLoading')).to.equal(false);

    const addAfterGroupSelected = shallow(wrapper.find(GroupsPickerModal).props().renderFooter(() => [group]) as any).find(AnalyticsButton).at(0);
    expect(addAfterGroupSelected.prop('isLoading')).to.equal(false);

    wrapper.setState({ isMutationLoading: true });
    const addAfterGroupSelectedAndMutating = shallow(wrapper.find(GroupsPickerModal).props().renderFooter(() => [group]) as any).find(AnalyticsButton).at(0);
    expect(addAfterGroupSelectedAndMutating.prop('isLoading')).to.equal(true);
  });

  it('should dismiss the dialog when clicking cancel', async () => {
    const wrapper = shallow((
      <AccessConfigAddGroupsToProductModalImpl
        {...defaultProps}
        data={{} as any}
      />
    ));

    expect(wrapper.find(GroupsPickerModal).exists()).equals(true);
    const footer = shallow(wrapper.find(GroupsPickerModal).props().renderFooter(() => []) as any);
    const cancel = footer.find(AnalyticsButton).filterWhere(btn => btn.children().text() === 'Cancel');
    cancel.simulate('click');

    expect(dismissSpy.callCount).to.equal(1);
  });

  it('should show a flag, and dismiss the dialog after adding a group', async () => {
    const promise = Promise.resolve(true);
    const mutateStub = sandbox.stub().returns(promise);
    const wrapper = shallow((
      <AccessConfigAddGroupsToProductModalImpl
        {...defaultProps}
        mutate={mutateStub}
        data={{} as any}
      />
    ));

    expect(wrapper.find(GroupsPickerModal).exists()).equals(true);
    const footer = shallow(wrapper.find(GroupsPickerModal).props().renderFooter(() => [group]) as any);
    const add = footer.find(AnalyticsButton).filterWhere(btn => btn.children().text() === 'Add groups');
    add.simulate('click');

    await promise;

    expect(mutateStub.callCount).to.equal(1);
    expect(showFlagSpy.calledWithMatch({ title: 'Product access updated!' })).to.equal(true);
    expect(dismissSpy.callCount).to.equal(1);
  });

  it('should show a flag, and dismiss the dialog after failing to add a group', async () => {
    const failedMutation = Promise.reject(new Error('oh noes'));
    const mutateStub = sandbox.stub().returns(failedMutation);
    const wrapper = shallow((
      <AccessConfigAddGroupsToProductModalImpl
        {...defaultProps}
        mutate={mutateStub}
        data={{} as any}
      />
    ));

    expect(wrapper.find(GroupsPickerModal).exists()).equals(true);
    const footer = shallow(wrapper.find(GroupsPickerModal).props().renderFooter(() => [group]) as any);
    const add = footer.find(AnalyticsButton).filterWhere(btn => btn.children().text() === 'Add groups');
    add.simulate('click');

    await failedMutation
      .then(() => assert.fail())
      .catch(e => {
        expect(e instanceof Error).to.equal(true);
      });

    expect(mutateStub.callCount).to.equal(1);
    expect(showFlagSpy.calledWithMatch({ title: 'Unable to update product access. Try again later.' })).to.equal(true);
    expect(dismissSpy.callCount).to.equal(1);
  });
});
