import { expect } from 'chai';

import { Group, ProductId } from './access-config-type';
import { determineAdminProductId, formatImportedProduct, shouldMakeJiraCoreDefault } from './access-config-util';

describe('Access Config Util Test', () => {

  describe('Formatting imported products', () => {
    it('Returns an empty map if no groups are imported', () => {
      const useGroupsMap = new Map<ProductId, Group[]>();
      const regularGroup: Group = { id: '123', name: 'Regular group', requiresApproval: false, default: false, managementAccess: 'NONE' };
      useGroupsMap.set('jira-core', [regularGroup]);
      const map = formatImportedProduct(useGroupsMap);
      expect(map.size).to.equal(0);
    });

    it('Returns a map if groups are imported', () => {
      const useGroupsMap = new Map<ProductId, Group[]>();
      const importedGroup: Group = { id: '123', name: 'Regular group', requiresApproval: true, default: false, managementAccess: 'NONE' };
      useGroupsMap.set('jira-core', [importedGroup]);
      const map = formatImportedProduct(useGroupsMap);
      expect(map).to.deep.equal(useGroupsMap);
    });

    it('Returns a map excluding groups that are not imported', () => {
      const useGroupsMap = new Map<ProductId, Group[]>();
      const regularGroup: Group = { id: '123', name: 'Regular group', requiresApproval: false, default: false, managementAccess: 'NONE' };
      const importedGroup: Group = { id: '123', name: 'Imported group', requiresApproval: true, default: false, managementAccess: 'NONE' };
      useGroupsMap.set('jira-core', [regularGroup, importedGroup]);
      const expectedGroupsMap = new Map<ProductId, Group[]>();
      expectedGroupsMap.set('jira-core', [importedGroup]);
      const map = formatImportedProduct(useGroupsMap);
      expect(map).to.deep.equal(expectedGroupsMap);
    });

    it('Returns a map excluding products that have no group imported', () => {
      const useGroupsMap = new Map<ProductId, Group[]>();
      const regularGroup: Group = { id: '123', name: 'Regular group', requiresApproval: false, default: false, managementAccess: 'NONE' };
      const importedGroup: Group = { id: '123', name: 'Imported group', requiresApproval: true, default: false, managementAccess: 'NONE' };
      useGroupsMap.set('jira-core', [regularGroup]);
      useGroupsMap.set('conf', [regularGroup, importedGroup]);
      const expectedGroupsMap = new Map<ProductId, Group[]>();
      expectedGroupsMap.set('conf', [importedGroup]);
      const map = formatImportedProduct(useGroupsMap);
      expect(map).to.deep.equal(expectedGroupsMap);
    });
  });

  describe('Formatting ADMIN products', () => {
    it('If it is a Jira USE product, it\'s corresponding ADMIN product id should jira-admin', () => {
      expect(determineAdminProductId('com.atlassian.servicedesk.ondemand')).to.equal('jira-admin');
      expect(determineAdminProductId('jira-servicedesk')).to.equal('jira-admin');
      expect(determineAdminProductId('jira-core')).to.equal('jira-admin');
      expect(determineAdminProductId('jira-software')).to.equal('jira-admin');
    });

    it('If it is not a Jira USE product, it\'s corresponding ADMIN product id should be the same id as their USE product id', () => {
      expect(determineAdminProductId('conf')).to.equal('conf');
      expect(determineAdminProductId('hipchat.cloud')).to.equal('hipchat.cloud');
      expect(determineAdminProductId('refapp')).to.equal('refapp');
    });
  });

  describe('Should also make jira-core a default product', () => {

    it('should make jira-core default when a Jira product is being made default', () => {
      const useProductIds: ProductId[] = ['jira-core', 'jira-software', 'jira-servicedesk'];
      const defaultProductIds: ProductId[] = [];
      const toggledProductId: ProductId = 'jira-servicedesk';
      expect(shouldMakeJiraCoreDefault(useProductIds, defaultProductIds, toggledProductId, true)).to.equal(true);
    });

    it('should make jira-core default when a JSD product is being made default', () => {
      const useProductIds: ProductId[] = ['jira-core', 'jira-software', 'jira-servicedesk'];
      const defaultProductIds: ProductId[] = [];
      const toggledProductId: ProductId = 'jira-servicedesk';
      expect(shouldMakeJiraCoreDefault(useProductIds, defaultProductIds, toggledProductId, true)).to.equal(true);
    });

    it('should not make jira-core default again if jira-core is currently being made default', () => {
      const useProductIds: ProductId[] = ['jira-core', 'jira-software', 'jira-servicedesk'];
      const defaultProductIds: ProductId[] = [];
      const toggledProductId: ProductId = 'jira-core';
      expect(shouldMakeJiraCoreDefault(useProductIds, defaultProductIds, toggledProductId, true)).to.equal(false);
    });

    it('should not make jira-core default if jira-software is being unset as a default product', () => {
      const useProductIds: ProductId[] = ['jira-core', 'jira-software', 'jira-servicedesk'];
      const defaultProductIds: ProductId[] = [];
      const toggledProductId: ProductId = 'jira-software';
      expect(shouldMakeJiraCoreDefault(useProductIds, defaultProductIds, toggledProductId, false)).to.equal(false);
    });

    it('should not make jira-core default if it is not a use product', () => {
      const useProductIds: ProductId[] = ['jira-software', 'jira-servicedesk'];
      const defaultProductIds: ProductId[] = [];
      const toggledProductId: ProductId = 'jira-software';
      expect(shouldMakeJiraCoreDefault(useProductIds, defaultProductIds, toggledProductId, true)).to.equal(false);
    });

    it('should not make jira-core default if it is already a default product', () => {
      const useProductIds: ProductId[] = ['jira-core'];
      const defaultProductIds: ProductId[] = ['jira-core'];
      const toggledProductId: ProductId = 'jira-core';
      expect(shouldMakeJiraCoreDefault(useProductIds, defaultProductIds, toggledProductId, true)).to.equal(false);
    });

    it('should not make jira-core default if the product made default is not a jira product', () => {
      const useProductIds: ProductId[] = ['conf'];
      const defaultProductIds: ProductId[] = ['conf'];
      const toggledProductId: ProductId = 'conf';
      expect(shouldMakeJiraCoreDefault(useProductIds, defaultProductIds, toggledProductId, true)).to.equal(false);
    });

  });

});
