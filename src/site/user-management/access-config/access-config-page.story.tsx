import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';

import AkPage from '@atlaskit/page';

import { createApolloClient } from '../../../apollo-client';
import { createMockIntlProp } from '../../../utilities/testing';
import { AccessConfigurationPage } from './access-config-page';

import defaultProducts from './access-config-default-products.query.graphql';
import accessConfig from './access-config.query.graphql';

const client = createApolloClient();

const confluence = {
  __typename: 'ProductAccess',
  productId: 'conf',
  productName: 'Confluence',
};

const confluenceGroup = {
  __typename: 'GroupAccess',
  id: 'conf-users',
  name: 'conf-users',
  requiresApproval: false,
  default: false,
};

const otherConfluenceGroup = {
  __typename: 'GroupAccess',
  id: 'confluence-users',
  name: 'confluence-users',
  requiresApproval: false,
  default: true,
};

const importedConfluenceGroup = {
  __typename: 'GroupAccess',
  id: 'conf-users',
  name: 'conf-users',
  requiresApproval: true,
  default: false,
};

const emptyDefaultProductsQueryResult = {
  query: defaultProducts,
  variables: {
    id: 'floof.com',
  },
  data: {
    currentSite: {
      id: 'dummy-id',
      __typename: 'CurrentSite',
      defaultApps: [],
    },
  },
};

const withDefaultProductsQueryResult = {
  query: defaultProducts,
  variables: {
    id: 'floof.com',
  },
  data: {
    currentSite: {
      id: 'dummy-id',
      __typename: 'CurrentSite',
      defaultApps: [confluence],
    },
  },
};

const accessConfigQueryResult = {
  query: accessConfig,
  data: {
    currentSite: {
      id: 'dummy-id-1',
      __typename: 'CurrentSite',
      groupsUseAccessConfig: [{
        __typename: 'GroupsAccessConfig[]',
        product: confluence,
        groups: [confluenceGroup, otherConfluenceGroup],
      }],
      groupsAdminAccessConfig: [{
        __typename: 'GroupsAccessConfig[]',
        product: confluence,
        groups: [confluenceGroup],
      }],
    },
  },
};

const accessConfigQueryResultWithImportedGroups = {
  query: accessConfig,
  data: {
    currentSite: {
      id: 'dummy-id-1',
      __typename: 'CurrentSite',
      groupsUseAccessConfig: [{
        __typename: 'GroupsAccessConfig[]',
        product: confluence,
        groups: [importedConfluenceGroup],
      }],
      groupsAdminAccessConfig: [{
        __typename: 'GroupsAccessConfig[]',
        product: confluence,
        groups: [importedConfluenceGroup],
      }],
    },
  },
};

const emptyAccessConfigQueryResult = {
  query: accessConfig,
  data: {
    currentSite: {
      id: 'none',
      __typename: 'CurrentSite',
      groupsUseAccessConfig: [],
      groupsAdminAccessConfig: [],
    },
  },
};

const defaultProps = {
  match: {
    params: {
      cloudId: 'floof.com',
    },
  } as any,
  history: {} as any,
  location: {} as any,
  intl: createMockIntlProp(),
};

storiesOf('Site|Access Config/Page', module)
  .add('Access config page, view only', () => {
    client.writeQuery(emptyDefaultProductsQueryResult);
    client.writeQuery(accessConfigQueryResult);

    return (
      <ApolloProvider client={client}>
        <IntlProvider locale="en">
          <AkPage>
            <AccessConfigurationPage {...defaultProps} />
          </AkPage>
        </IntlProvider>
      </ApolloProvider>
    );
  }).add('Access config page, loading', () => {
    client.resetStore().catch((_) => undefined);

    return (
      <ApolloProvider client={client}>
        <IntlProvider locale="en">
          <AkPage>
            <AccessConfigurationPage {...defaultProps} />
          </AkPage>
        </IntlProvider>
      </ApolloProvider>
    );
  }).add('Access config page, with default product set', () => {
    client.writeQuery(withDefaultProductsQueryResult);
    client.writeQuery(accessConfigQueryResult);

    return (
      <ApolloProvider client={client}>
        <IntlProvider locale="en">
          <AkPage>
            <AccessConfigurationPage {...defaultProps} />
          </AkPage>
        </IntlProvider>
      </ApolloProvider>
    );
  }).add('Access config page, no config or error', () => {
    client.writeQuery(emptyAccessConfigQueryResult);

    return (
      <ApolloProvider client={client}>
        <IntlProvider locale="en">
          <AkPage>
            <AccessConfigurationPage {...defaultProps} />
          </AkPage>
        </IntlProvider>
      </ApolloProvider>
    );
  }).add('Access config page, with imported groups', () => {
    client.writeQuery(withDefaultProductsQueryResult);
    client.writeQuery(accessConfigQueryResultWithImportedGroups);

    return (
      <ApolloProvider client={client}>
        <IntlProvider locale="en">
          <AkPage>
            <AccessConfigurationPage {...defaultProps} />
          </AkPage>
        </IntlProvider>
      </ApolloProvider>
    );
  });
