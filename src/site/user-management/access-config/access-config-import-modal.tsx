import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import AkButton from '@atlaskit/button';
import { gridSize as akGridSize } from '@atlaskit/theme';

import { ModalDialog } from 'common/modal';

import { AccessConfigQuery } from '../../../schema/schema-types';
import { AccessConfigGroupTable } from './access-config-group-table';
import { Group, ProductId } from './access-config-type';
import { productIdToProductName } from './access-config-util';

export const messages = defineMessages({
  title: {
    id: 'user-management.site-settings.access-configuration.import.modal.title',
    defaultMessage: 'Review groups imported from server',
  },
  cancel: {
    id: 'user-management.site-settings.access-configuration.import.modal.cancel',
    defaultMessage: `Cancel`,
  },
  approve: {
    id: 'user-management.site-settings.access-configuration.import.modal.approve',
    defaultMessage: `Approve`,
  },
  reject: {
    id: 'user-management.site-settings.access-configuration.import.modal.reject',
    defaultMessage: `Reject`,
  },
  groupname: {
    id: 'user-management.site-settings.access-configuration.import.modal.group.name',
    defaultMessage: `Group name`,
  },
});

interface OwnProps {
  isOpen: boolean;
  cloudId: string;
  useGroups: Map<ProductId, Group[]>;
  onClose(): void;
}

export const CardWrapper = styled.div`
  padding-top: ${akGridSize() * 3}px;
  padding-bottom: ${akGridSize() * 3}px;
`;

export class AccessConfigImportModalImpl extends React.Component<InjectedIntlProps & OwnProps, AccessConfigQuery> {

  public render() {
    const { formatMessage } = this.props.intl;
    const { useGroups } = this.props;

    return (
      <ModalDialog
        width="medium"
        header={formatMessage(messages.title)}
        isOpen={this.props.isOpen}
        onClose={this.props.onClose}
        footer={
          <AkButton onClick={this.props.onClose} appearance="subtle-link">
            {formatMessage(messages.cancel)}
          </AkButton>}
      >
        {Array.from(useGroups.keys()).map(productId => this.generateAccessConfigImportCard(productId, useGroups.get(productId) as Group[]))}
      </ModalDialog>
    );
  }

  private generateAccessConfigImportCard = (productId: ProductId, groups: Group[]): JSX.Element => {
    const productName = productIdToProductName[productId];

    return (
      <CardWrapper key={productId}>
        <h3>{productName}</h3>
        <AccessConfigGroupTable
          cloudId={this.props.cloudId}
          configType="importAccess"
          productId={productId}
          productName={productName}
          groups={groups}
        />
      </CardWrapper>
    );
  }
}

export const AccessConfigImportModal = injectIntl<OwnProps>(AccessConfigImportModalImpl);
