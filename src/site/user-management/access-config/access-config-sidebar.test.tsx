import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';

import { createMockIntlProp } from '../../../utilities/testing';

import { AccessConfigSidebarImpl } from './access-config-sidebar';

function createTestWrapper({ configType, products }): ShallowWrapper {
  return shallow(
    <AccessConfigSidebarImpl
      intl={createMockIntlProp()}
      configType={configType}
      products={products}
    />,
  );
}

describe('Access Config Sidebar', () => {

  describe('For the product access config type', () => {

    const wrapper = createTestWrapper({ configType: 'productAccess', products: [] });

    it('Should contain a header', () => {
      expect(wrapper.find('h4').text()).to.contain('What you need to know');
    });

    it('Should contain three bullet points', () => {
      expect(wrapper.find('li').length).to.equal(3);
      expect(wrapper.find('li').at(0).text()).to.contain('Any user added to a group with product access will count towards your product licenses.');
    });

  });

  describe('For the admin access config type', () => {

    it('Should contain a header', () => {
      const wrapper = createTestWrapper({ configType: 'productAccess', products: [] });
      expect(wrapper.find('h4').text()).to.contain('What you need to know');
    });

    it('Should have two bullet points if it includes Jira and another product', () => {
      const wrapper = createTestWrapper({ configType: 'adminAccess', products: ['jira-admin', 'conf'] });
      expect(wrapper.find('li').length).to.equal(2);
      expect(wrapper.find('li').at(0).text()).to.contain('Groups in Jira administration');
      expect(wrapper.find('li').at(1).text()).to.contain('All groups added to other administration products');
    });

    it('Should have one bullet point if Jira is the only product', () => {
      const wrapper = createTestWrapper({ configType: 'adminAccess', products: ['jira-admin'] });
      expect(wrapper.find('li').length).to.equal(1);
      expect(wrapper.find('li').at(0).text()).to.contain('Groups in Jira administration');
    });

    it('Should have one bullet point if there are products, but none are Jira', () => {
      const wrapper = createTestWrapper({ configType: 'adminAccess', products: ['conf'] });
      expect(wrapper.find('li').length).to.equal(1);
      expect(wrapper.find('li').at(0).text()).to.contain('All groups added to other administration products');
    });
  });
});
