import { Group, ProductGroupsConfig } from './access-config-type';

export const accessConfig: ProductGroupsConfig[] = [
  {
    product: {
      productName: 'Confluence',
      productId: 'conf',
    },
    groups: [
      { id: 'f99dff40-f855-11e8-b568-0800200c9a660', name: 'Fluffy Samoyeds', default: false, admin: true, requiresApproval: false, managementAccess: 'NONE' },
    ],
  },
  {
    product: {
      productName: 'Jira',
      productId: 'jira-software',
    },
    groups: [
      { id: 'f99dff40-f855-11e8-b568-0800200c9a667', name: 'Happy Corgi Friends', default: true, admin: false, requiresApproval: false, managementAccess: 'NONE' },
      { id: 'f99dff40-f855-11e8-b568-0800200c9a668', name: 'Shiba Inu Petting Society', default: false, admin: true, requiresApproval: false, managementAccess: 'NONE' },
    ],
  },
];

export const adminAccessConfig: ProductGroupsConfig[] = [
  {
    product: {
      productName: 'Confluence',
      productId: 'conf',
    },
    groups: [
      { id: 'f99dff40-f855-11e8-b568-0800200c9a666', name: 'Fluffy Samoyeds', default: false, admin: true, requiresApproval: false, managementAccess: 'NONE' },
    ],
  },
  {
    product: {
      productName: 'Jira Administration',
      productId: 'jira-admin',
    },
    groups: [
      { id: 'f99dff40-f855-11e8-b568-0800200c9a665', name: 'Shiba Inu Petting Society', default: false, admin: true, requiresApproval: false, managementAccess: 'NONE' },
    ],
  },
];

export const defaultProducts = {
  products: [
    {
      productId: 'conf',
      productName: 'Confluence',
    },
  ],
};

export const useGroups: Group[] = [
  { id: 'f99dff40-f855-11e8-b568-0800200c9a661', name: 'Happy Corgi Friends', default: true, requiresApproval: false, managementAccess: 'NONE' },
  { id: 'f99dff40-f855-11e8-b568-0800200c9a662', name: 'Shiba Inu Petting Society', default: false, requiresApproval: false, managementAccess: 'NONE' },
];

export const adminGroups: Group[] = [
  { id: 'f99dff40-f855-11e8-b568-0800200c9a663', name: 'Excited Pugs', default: true, requiresApproval: false, managementAccess: 'NONE' },
  { id: 'f99dff40-f855-11e8-b568-0800200c9a664', name: 'Shiba Inu Petting Society', default: false, requiresApproval: false, managementAccess: 'NONE' },
];
