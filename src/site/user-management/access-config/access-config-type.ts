import { ManagementAccess, OwnerType } from '../../../schema/schema-types';
import { AccessConfig, ConfigType, ProductGroupsConfig } from './access-config-type';

export type ConfigType = 'productAccess' | 'adminAccess' | 'importAccess';

export interface Group {
  id: string;
  name: string;
  default: boolean;
  requiresApproval: boolean;
  admin?: boolean;
  ownerType?: OwnerType | null;
  managementAccess: ManagementAccess;
}

export interface Product {
  productName: string;
  productId: ProductId;
}

export interface ProductGroups {
  adminAccess: AccessConfig;
  useAccess: AccessConfig;
}

export interface ProductGroupsConfig {
  product: Product;
  groups: Group[];
}

export interface AccessConfig {
  config: ProductGroupsConfig[];
  configType: ConfigType;
}

// These ProductId definitions are what the backend uses to specify the product id
// https://stash.atlassian.com/projects/UN/repos/user-management/browse/client-common/src/main/java/com/atlassian/usermanagement/products/ProductKey.java
export type ProductId = 'conf' | 'hipchat.cloud' | 'jira-admin' | 'jira-core' | 'jira' | 'jira-servicedesk'
  | 'com.atlassian.servicedesk.ondemand' | 'jira-software' | 'refapp';
