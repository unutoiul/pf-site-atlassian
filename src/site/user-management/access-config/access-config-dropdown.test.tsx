import { expect } from 'chai';
import { mount, ReactWrapper, shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import { DropdownItem as AkDropdownItem } from 'common/analytics';

import { util } from '../../../utilities/admin-hub';
import { createMockAnalyticsClient, createMockIntlProp } from '../../../utilities/testing';
import { AccessConfigDropdownImpl } from './access-config-dropdown';

describe('Access config dropdown', () => {

  function createTestComponent({ isDefaultGroup, configType, isAdmin, isLastDefault }): ShallowWrapper {
    return shallow(
      <AccessConfigDropdownImpl
        analyticsClient={createMockAnalyticsClient()}
        groupName="Jira Software Users"
        cloudId="DUMMY-TEST-CLOUD-ID"
        groupId="DUMMY-GROUP-ID"
        productId="jira-software"
        productName="Jira Software"
        isAdmin={isAdmin}
        isLastDefault={isLastDefault}
        isDefault={isDefaultGroup}
        configType={configType}
        removeGroup={mutateStubRemoveGroup}
        showFlag={() => null}
        hideFlag={() => null}
        intl={createMockIntlProp()}
        history={{} as any}
        match={{} as any}
        location={{} as any}
      />,
    );
  }

  function getDropdownItems(wrapper: any): ShallowWrapper {
    return wrapper.find(AkDropdownItem);
  }

  function getDropdownIsDisabled(dropdown: ShallowWrapper, index: number): string {
    return dropdown.at(index).prop('isDisabled');
  }

  function getDropdownText(dropdown: ShallowWrapper, index: number): string {
    return dropdown.at(index).children().text();
  }

  function getDropdownHref(dropdown: ShallowWrapper, index: number): string {
    return dropdown.at(index).prop('href');
  }

  it('Should have an option to make a group default if it is not already', () => {
    const wrapper = createTestComponent({ isDefaultGroup: false, configType: 'productAccess', isAdmin: false, isLastDefault: false });
    const dropdownItems = getDropdownItems(wrapper);
    expect(dropdownItems).to.have.length(3);
    expect(getDropdownText(dropdownItems, 0)).to.equal('Show group\'s details');
    expect(getDropdownText(dropdownItems, 1)).to.equal('Make this group default');
    expect(getDropdownText(dropdownItems, 2)).to.equal('Remove group');
  });

  it('Should have an option to make a group not default if it is already', () => {
    const wrapper = createTestComponent({ isDefaultGroup: true, configType: 'productAccess', isAdmin: false, isLastDefault: false });
    const dropdownItems = getDropdownItems(wrapper);
    expect(dropdownItems).to.have.length(3);
    expect(getDropdownText(dropdownItems, 0)).to.equal('Show group\'s details');
    expect(getDropdownText(dropdownItems, 1)).to.equal('Don\'t make this group default');
    expect(getDropdownText(dropdownItems, 2)).to.equal('Remove group');
  });

  it('Making a group default should be disabled if the group is also an admin group', () => {
    const wrapper = createTestComponent({ isDefaultGroup: false, configType: 'productAccess', isAdmin: true, isLastDefault: false });
    const dropdownItems = getDropdownItems(wrapper);
    expect(dropdownItems).to.have.length(3);
    expect(getDropdownText(dropdownItems, 1)).to.equal('Make this group default');
    expect(getDropdownIsDisabled(dropdownItems, 1)).to.equal(true);
  });

  it('Removing a group should be disabled if the group is also a default group', () => {
    const wrapper = createTestComponent({ isDefaultGroup: true, configType: 'productAccess', isAdmin: true, isLastDefault: false });
    const dropdownItems = getDropdownItems(wrapper);
    expect(dropdownItems).to.have.length(3);
    expect(getDropdownText(dropdownItems, 2)).to.equal('Remove group');
    expect(getDropdownIsDisabled(dropdownItems, 2)).to.equal(true);
  });

  it('Making a group default should be disabled if the group is the last group', () => {
    const wrapper = createTestComponent({ isDefaultGroup: false, configType: 'productAccess', isAdmin: false, isLastDefault: true });
    const dropdownItems = getDropdownItems(wrapper);
    expect(dropdownItems).to.have.length(3);
    expect(getDropdownText(dropdownItems, 1)).to.equal('Make this group default');
    expect(getDropdownIsDisabled(dropdownItems, 1)).to.equal(true);
  });

  it('Should not have the option to make a group default or not default if the group is part of an ADMIN access product', () => {
    const wrapper = createTestComponent({ isDefaultGroup: false, configType: 'adminAccess', isAdmin: false, isLastDefault: false });
    const dropdownItems = getDropdownItems(wrapper);
    expect(dropdownItems).to.have.length(2);
    expect(getDropdownText(dropdownItems, 0)).to.equal('Show group\'s details');
    expect(getDropdownText(dropdownItems, 1)).to.equal('Remove group');
  });

  it('Should link to the correct show details URL', () => {
    const wrapper = createTestComponent({ isDefaultGroup: false, configType: 'adminAccess', isLastDefault: false, isAdmin: false });
    const dropdownItems = getDropdownItems(wrapper);
    expect(dropdownItems).to.have.length(2);
    expect(getDropdownText(dropdownItems, 0)).to.equal('Show group\'s details');
    expect(getDropdownHref(dropdownItems, 0)).to.equal(`${util.siteAdminBasePath}/s/DUMMY-TEST-CLOUD-ID/groups/DUMMY-GROUP-ID`);
  });

  let mutateStubRemoveGroup;
  let mutateStubSetDefaultGroup;

  const sandbox = sinon.sandbox.create();

  beforeEach(() => {
    mutateStubRemoveGroup = sandbox.stub().resolves({});
    mutateStubSetDefaultGroup = sandbox.stub().resolves({});
  });

  afterEach(() => {
    sandbox.reset();
  });

  function clickOnDropdownMeatballs(wrapper: ReactWrapper): void {
    wrapper.find('button').simulate('click');
  }

  function getMutationRemoveGroupVariables(argNumber: number) {
    return mutateStubRemoveGroup.args[argNumber][0].variables.input;
  }

  function getMutationSetDefaultGroupVariables(argNumber: number) {
    return mutateStubSetDefaultGroup.args[argNumber][0].variables.input;
  }

  // Mounted only to test the integration between selecting and mutating
  function createMountedTestComponent({ isDefaultGroup, configType }): ReactWrapper {
    return mount(
      <AccessConfigDropdownImpl
        analyticsClient={createMockAnalyticsClient()}
        cloudId="DUMMY-TEST-CLOUD-ID"
        groupName="Jira Software Users"
        groupId="jira-software-users"
        productId="jira-software"
        productName="Jira Software"
        isLastDefault={false}
        isAdmin={false}
        isDefault={isDefaultGroup}
        configType={configType}
        removeGroup={mutateStubRemoveGroup}
        setDefaultGroup={mutateStubSetDefaultGroup}
        showFlag={() => null}
        hideFlag={() => null}
        intl={createMockIntlProp()}
        history={{} as any}
        match={{} as any}
        location={{} as any}
      />,
    );
  }

  it('Clicking on set default group should trigger set default group mutation', () => {
    const wrapper = createMountedTestComponent({ isDefaultGroup: false, configType: 'productAccess' });
    clickOnDropdownMeatballs(wrapper);
    const dropdownItems = getDropdownItems(wrapper);
    expect(dropdownItems).to.have.length(3);
    expect(getDropdownText(dropdownItems, 0)).to.equal('Show group\'s details');
    expect(getDropdownText(dropdownItems, 1)).to.equal('Make this group default');
    expect(mutateStubSetDefaultGroup.callCount).to.equal(0);
    dropdownItems.at(1).simulate('click');
    expect(mutateStubSetDefaultGroup.callCount).to.equal(1);
    expect(getMutationSetDefaultGroupVariables(0).groupId).to.equal('jira-software-users');
    expect(getMutationSetDefaultGroupVariables(0).productId).to.equal('jira-software');
    expect(getMutationSetDefaultGroupVariables(0).isDefault).to.equal(false);
  });

  it('Clicking on remove group should trigger remove group mutation', () => {
    const wrapper = createMountedTestComponent({ isDefaultGroup: false, configType: 'adminAccess' });
    clickOnDropdownMeatballs(wrapper);
    const dropdownItems = getDropdownItems(wrapper);
    expect(dropdownItems).to.have.length(2);
    expect(getDropdownText(dropdownItems, 0)).to.equal('Show group\'s details');
    expect(getDropdownText(dropdownItems, 1)).to.equal('Remove group');
    expect(mutateStubRemoveGroup.callCount).to.equal(0);
    dropdownItems.at(1).simulate('click');
    expect(mutateStubRemoveGroup.callCount).to.equal(1);
    expect(getMutationRemoveGroupVariables(0).groupId).to.equal('jira-software-users');
    expect(getMutationRemoveGroupVariables(0).productId).to.equal('jira-software');
    expect(getMutationRemoveGroupVariables(0).productType).to.equal('ADMIN');
  });
});
