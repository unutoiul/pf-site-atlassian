import { expect } from 'chai';
import { mount, ReactWrapper, shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';

import AkSectionMessage from '@atlaskit/section-message';

import { createMockIntlContext, createMockIntlProp } from '../../../utilities/testing';
import { AccessConfigImportBannerImpl, messages } from './access-config-import-banner';
import { AccessConfigImportModal } from './access-config-import-modal';

describe('Access Configuration Import Banner', () => {

  function createShallowTestComponent(): ShallowWrapper {
    return shallow(
      <AccessConfigImportBannerImpl
        cloudId={'DUMMY-CLOUD-ID'}
        useGroups={new Map()}
        intl={createMockIntlProp()}
      />,
      createMockIntlContext(),
    );
  }

  it('Should display banner, modal should be hidden', () => {
    const wrapper = createShallowTestComponent();
    const panel = wrapper.find(AkSectionMessage);
    const modal = wrapper.find(AccessConfigImportModal);
    expect(panel.length).to.equal(1);
    expect(modal.length).to.equal(1);
    expect(modal.props().isOpen).to.equal(false);
    expect(panel.prop('title')).to.equal(messages.title.defaultMessage);
    expect(panel.children().text().includes(messages.description.defaultMessage)).to.equal(true);
  });

  function createMountedTestComponent(): ReactWrapper {
    return mount(
      <AccessConfigImportBannerImpl
        cloudId={'DUMMY-CLOUD-ID'}
        useGroups={new Map()}
        intl={createMockIntlProp()}
      />,
      createMockIntlContext(),
    );
  }

  it('Modal should open when clicked', () => {
    const wrapper = createMountedTestComponent();
    const button = wrapper.find('button');
    let modal = wrapper.find(AccessConfigImportModal);
    expect(modal.props().isOpen).to.equal(false);
    button.simulate('click');
    modal = wrapper.find(AccessConfigImportModal);
    expect(modal.props().isOpen).to.equal(true);
  });
});
