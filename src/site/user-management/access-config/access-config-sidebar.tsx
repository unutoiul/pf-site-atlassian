import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import { PageSidebar } from 'common/page-layout';

import { ConfigType, ProductId } from './access-config-type';

export const messages = defineMessages({
  sideBarTitle: {
    id: `user-management.site-settings.access-configuration.groups.page.sidebar.title`,
    description: 'Title on the sidebar of the Access Config tab',
    defaultMessage: `What you need to know`,
  },
  sideBarUseDescription1: {
    id: `user-management.site-settings.access-configuration.groups.page.sidebar.use.description.1`,
    description: 'Bullet point explaining the impact of grant product access to a group',
    defaultMessage: `Any user added to a group with product access will count towards your product licenses.`,
  },
  sideBarUseDescription2: {
    id: `user-management.site-settings.access-configuration.groups.page.sidebar.use.description.2`,
    description: 'Bullet point explaining that an administration product cannot have a default group',
    defaultMessage: `When a user is granted access to a product, they will be added to the products default access group.`,
  },
  sideBarUseDescription3: {
    id: `user-management.site-settings.access-configuration.groups.page.sidebar.use.description.3`,
    description: 'Bullet point explaining that an administration product cannot have a default group',
    defaultMessage: `Groups used in administration access cannot be set as a default access group.`,
  },
  sideBarAdminDescription1: {
    id: `user-management.site-settings.access-configuration.groups.page.sidebar.admin.description.1`,
    description: 'Bullet point explaining that Jira administration does not also grant product access',
    defaultMessage: `Groups in Jira administration don't count towards your product license, so when you want these
    groups to use Jira, you need to grant them product access.`,
  },
  sideBarAdminDescription2: {
    id: `user-management.site-settings.access-configuration.groups.page.sidebar.admin.description.2`,
    description: 'Bullet point explaining that granting access to an administration product will also grant product access',
    defaultMessage: `All groups added to other administration products have product access and count towards your product license.`,
  },
});

interface AccessConfigSidebarProps {
  configType: ConfigType;
  products?: ProductId[];
}

export class AccessConfigSidebarImpl extends React.Component<AccessConfigSidebarProps & InjectedIntlProps> {
  public render() {

    const { formatMessage } = this.props.intl;

    if (this.props.configType === 'productAccess') {
      return (
        <PageSidebar>
          <h4>{formatMessage(messages.sideBarTitle)}</h4>
          <ul>
            <li>{formatMessage(messages.sideBarUseDescription1)}</li>
            <li>{formatMessage(messages.sideBarUseDescription2)}</li>
            <li>{formatMessage(messages.sideBarUseDescription3)}</li>
          </ul>
        </PageSidebar>
      );
    }

    const hasJiraAdministration = this.props.products && this.props.products.includes('jira-admin');
    const hasOtherAdministration = this.props.products && this.props.products.filter(product => product !== 'jira-admin').length;

    return (
      <PageSidebar>
        <h4>{formatMessage(messages.sideBarTitle)}</h4>
        <ul>
          {!!hasJiraAdministration && <li>{formatMessage(messages.sideBarAdminDescription1)}</li>}
          {!!hasOtherAdministration && <li>{formatMessage(messages.sideBarAdminDescription2)}</li>}
        </ul>
      </PageSidebar>
    );
  }
}

export const AccessConfigSidebar = injectIntl(AccessConfigSidebarImpl);
