import { expect } from 'chai';
import { mount, ReactWrapper, shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import AkButton from '@atlaskit/button';

import { ApproveImportedGroupMutationArgs } from '../../../schema/schema-types';
import { createMockIntlProp } from '../../../utilities/testing';
import { AccessConfigImportActionsImpl } from './access-config-import-actions';

describe('Access configuration import options', () => {

  function createTestComponent(): ShallowWrapper {
    return shallow(
      <AccessConfigImportActionsImpl
        cloudId="import-cloud"
        productId="conf"
        groupName="Migrated Server Group"
        groupId="38b7be26-03f7-42a9-b18b-9cdb56d204c2"
        intl={createMockIntlProp()}
        showFlag={undefined as any}
        hideFlag={undefined as any}
      />,
    );
  }

  it('Should have an approve and reject button', () => {
    const wrapper = createTestComponent();
    const button = wrapper.find(AkButton);
    expect(button.length).to.equal(2);
    expect(button.at(0).children().text()).to.equal('Approve');
    expect(button.at(0).props().appearance).to.equal('link');
    expect(button.at(1).children().text()).to.equal('Reject');
    expect(button.at(1).props().appearance).to.equal('subtle-link');
  });

  let mutateApproveImportedGroup;
  let mutateRejectImportedGroup;

  const sandbox = sinon.sandbox.create();

  beforeEach(() => {
    mutateApproveImportedGroup = sandbox.stub().resolves({});
    mutateRejectImportedGroup = sandbox.stub().resolves({});
  });

  function createMountedTestComponent(): ReactWrapper {
    return mount(
      <AccessConfigImportActionsImpl
        cloudId="import-cloud"
        productId="conf"
        groupName="Migrated Server Group"
        groupId="38b7be26-03f7-42a9-b18b-9cdb56d204c2"
        intl={createMockIntlProp()}
        approveImportedGroup={mutateApproveImportedGroup}
        rejectImportedGroup={mutateRejectImportedGroup}
        showFlag={undefined as any}
        hideFlag={undefined as any}
      />,
    );
  }

  function getMutationApproveImportedGroupVariables(): ApproveImportedGroupMutationArgs {
    return mutateApproveImportedGroup.args[0][0].variables;
  }

  function getMutationRejectImportedGroupVariables(): ApproveImportedGroupMutationArgs {
    return mutateRejectImportedGroup.args[0][0].variables;
  }

  it('Clicking on the approve button should trigger the approve mutation', () => {
    const wrapper = createMountedTestComponent();
    const buttons = wrapper.find('button');
    expect(buttons.length).to.equal(2);
    const approve = buttons.at(0);
    expect(mutateApproveImportedGroup.callCount).to.equal(0);
    approve.simulate('click');
    expect(mutateApproveImportedGroup.callCount).to.equal(1);
    expect(getMutationApproveImportedGroupVariables().id).to.equal('import-cloud');
    expect(getMutationApproveImportedGroupVariables().input.groupId).to.equal('38b7be26-03f7-42a9-b18b-9cdb56d204c2');
    expect(getMutationApproveImportedGroupVariables().input.productId).to.equal('conf');
  });

  it('Clicking on the reject button should trigger the reject mutation', () => {
    const wrapper = createMountedTestComponent();
    const buttons = wrapper.find('button');
    expect(buttons.length).to.equal(2);
    const approve = buttons.at(1);
    expect(mutateRejectImportedGroup.callCount).to.equal(0);
    approve.simulate('click');
    expect(mutateRejectImportedGroup.callCount).to.equal(1);
    expect(getMutationRejectImportedGroupVariables().id).to.equal('import-cloud');
    expect(getMutationRejectImportedGroupVariables().input.groupId).to.equal('38b7be26-03f7-42a9-b18b-9cdb56d204c2');
    expect(getMutationRejectImportedGroupVariables().input.productId).to.equal('conf');
  });
});
