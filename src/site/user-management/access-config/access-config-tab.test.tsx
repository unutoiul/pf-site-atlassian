import { expect } from 'chai';
import { mount, ReactWrapper, shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { IntlProvider } from 'react-intl';
import { MemoryRouter } from 'react-router';
import * as sinon from 'sinon';

import { GenericError } from 'common/error';
import { FlagProvider } from 'common/flag';

import { createMockAnalyticsClient, createMockContext, createMockIntlProp } from '../../../utilities/testing';
import { AccessConfigGroupCard } from './access-config-groups-card';
import { AccessConfigTabImpl } from './access-config-tab';
import { defaultProducts } from './access-config-test-util';
import { ConfigType, Group, ProductId } from './access-config-type';

describe('Access Config Groups Tab Test', () => {

  const groupAdmin: Group = { id: 'f99dff4e-f855-11e8-b568-0800200c9a66', name: 'administrators', default: false, admin: true, requiresApproval: false, managementAccess: 'NONE' };
  const groupDefault: Group = { id: 'f99dff40-f855-11e8-b568-0800200c9a66', name: 'users', default: true, admin: false, requiresApproval: false, managementAccess: 'NONE' };

  let mutateStub;

  const sandbox = sinon.sandbox.create();

  beforeEach(() => {
    mutateStub = sandbox.stub().resolves({});
  });

  afterEach(() => {
    sandbox.reset();
  });

  function createTestComponent(configType: ConfigType, config: Map<ProductId, Group[]>): ShallowWrapper {
    return shallow(
      <AccessConfigTabImpl
        analyticsClient={createMockAnalyticsClient()}
        cloudId={'DUMMY-CLOUD-ID'}
        configType={configType}
        data={{ loading: false, currentSite: { defaultApps: defaultProducts.products } } as any}
        config={config}
        intl={createMockIntlProp()}
        showFlag={() => null}
        hideFlag={() => null}
      />,
    );
  }

  function getProductNameFromTable(table): string {
    return table.props.productName;
  }

  function getTables(wrapper: ShallowWrapper): ShallowWrapper {
    return wrapper.find(AccessConfigGroupCard);
  }

  function getNumberOfTables(wrapper: ShallowWrapper): number {
    return wrapper.find(AccessConfigGroupCard).length;
  }

  describe('For adminAccess tab type', () => {

    it('Should display error message when there is no access config', () => {
      const wrapper = createTestComponent('adminAccess', new Map());
      const emptyState = wrapper.find(GenericError);
      expect(emptyState.length).to.equal(1);
    });

    it('It should show products', () => {
      const products = new Map<ProductId, Group[]>();
      products.set('jira-admin', [{ id: 'f99dff40-f855-11e8-b568-0800200c9a661', name: 'jira-admins', default: false, admin: true, requiresApproval: false, managementAccess: 'NONE' }]);
      const wrapper = createTestComponent('adminAccess', products);
      const tables = getTables(wrapper);
      expect(getNumberOfTables(wrapper)).to.equal(1);

      expect(getProductNameFromTable(tables.get(0))).to.equal('Jira Administration');
    });
  });

  describe('For productAccess tab type', () => {

    it('It should show products', () => {
      const products = new Map<ProductId, Group[]>();
      products.set('jira-core', [{ id: 'f99dff40-f855-11e8-b568-0800200c9a662', name: 'jira-admins', default: false, admin: false, requiresApproval: false, managementAccess: 'NONE' }]);
      products.set('jira-software', [{ id: 'f99dff40-f855-11e8-b568-0800200c9a663', name: 'jira-admins', default: false, admin: false, requiresApproval: false, managementAccess: 'NONE' }]);
      const wrapper = createTestComponent('productAccess', products);
      const tables = getTables(wrapper);
      expect(getNumberOfTables(tables)).to.equal(2);

      expect(getProductNameFromTable(tables.get(0))).to.equal('Jira Core');
      expect(getProductNameFromTable(tables.get(1))).to.equal('Jira Software');
    });

    describe('Toggling default products behaves as expected', () => {

      function getToggles(wrapper: ReactWrapper): ReactWrapper {
        return wrapper.find('Toggle');
      }

      function getNumberOfToggles(wrapper: ReactWrapper): number {
        return getToggles(wrapper).length;
      }

      function clickToggle(toggle): void {
        toggle.find('input').simulate('change');
      }

      function getNumberOfMutations(stub): number {
        return stub.callCount;
      }

      function getMutationProductVariables(argNumber: number) {
        return mutateStub.args[argNumber][0].variables.input;
      }

      function getMutationProductId(argNumber: number) {
        return getMutationProductVariables(argNumber).productId;
      }

      function getMutationProductDefault(argNumber: number) {
        return getMutationProductVariables(argNumber).productDefault;
      }

      // This function is mounted only to test the integration between a toggle click and the mutation
      function mountTestComponent(
        config: Map<ProductId, Group[]>,
        defaultP: any,
      ): ReactWrapper {
        return mount(
          <MemoryRouter>
            <FlagProvider>
              <IntlProvider locale="en">
                <AccessConfigTabImpl
                  analyticsClient={createMockAnalyticsClient()}
                  cloudId={'DUMMY-CLOUD-ID'}
                  configType={'productAccess'}
                  data={{ loading: false, currentSite: { defaultApps: defaultP } } as any}
                  config={config}
                  mutate={mutateStub}
                  intl={createMockIntlProp()}
                  showFlag={() => null}
                  hideFlag={() => null}
                />
              </IntlProvider>
            </FlagProvider>
          </MemoryRouter>,
          createMockContext({ intl: true, apollo: true }),
        );
      }

      it('If a product is not default, clicking on the toggle should make the product default', () => {

        const product = new Map();
        product.set('conf', [groupAdmin, groupDefault]);

        const wrapper = mountTestComponent(product, []).children().at(0);
        const toggles = getToggles(wrapper);

        expect(getNumberOfToggles(wrapper)).to.equal(1);
        expect(getNumberOfMutations(mutateStub)).to.equal(0);

        clickToggle(toggles);
        expect(getNumberOfMutations(mutateStub)).to.equal(1);
        expect(getMutationProductId(0)).to.equal('conf');
        expect(getMutationProductDefault(0)).to.equal(true);
      });

      it('If a product is default, clicking on the toggle should make the product not default', () => {

        const product = new Map();
        product.set('conf', [groupAdmin, groupDefault]);

        const wrapper = mountTestComponent(product, defaultProducts.products);
        const toggles = getToggles(wrapper);

        expect(getNumberOfToggles(wrapper)).to.equal(1);
        expect(getNumberOfMutations(mutateStub)).to.equal(0);

        clickToggle(toggles);
        expect(getNumberOfMutations(mutateStub)).to.equal(1);
        expect(getMutationProductId(0)).to.equal('conf');
        expect(getMutationProductDefault(0)).to.equal(false);
      });

      it('If Jira Core and Jira Software are not default products, clicking on the Jira Software product will make Jira Core default too.', () => {

        const product = new Map();
        product.set('jira-software', [groupAdmin, groupDefault]);
        product.set('jira-core', [groupAdmin, groupDefault]);

        const defaultProduct = [{ productId: 'conf', productName: 'Confluence' }];

        const wrapper = mountTestComponent(product, defaultProduct);

        expect(getNumberOfToggles(wrapper)).to.equal(2);
        expect(getNumberOfMutations(mutateStub)).to.equal(0);

        const toggle = getToggles(wrapper).at(0);

        clickToggle(toggle);
        expect(getNumberOfMutations(mutateStub)).to.equal(2);
        expect(getMutationProductId(0)).to.equal('jira-software');
        expect(getMutationProductDefault(0)).to.equal(true);
        expect(getMutationProductId(1)).to.equal('jira-core');
        expect(getMutationProductDefault(1)).to.equal(true);
      });

      it('If Jira Software is already a default product, making it not default should not change anything about how Jira Core is default', () => {

        const product = new Map();
        product.set('jira-software', [groupAdmin, groupDefault]);
        product.set('jira-core', [groupAdmin, groupDefault]);
        const defaultProduct = [{ productId: 'jira-software', productName: 'Jira Software' }];

        const wrapper = mountTestComponent(product, defaultProduct);

        expect(getNumberOfToggles(wrapper)).to.equal(2);
        expect(getNumberOfMutations(mutateStub)).to.equal(0);

        const toggle = getToggles(wrapper).at(0);

        clickToggle(toggle);
        expect(getNumberOfMutations(mutateStub)).to.equal(1);
        expect(getMutationProductId(0)).to.equal('jira-software');
        expect(getMutationProductDefault(0)).to.equal(false);
      });

    });

  });

});
