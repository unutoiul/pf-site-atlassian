import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import AkSpinner from '@atlaskit/spinner';

import { gridSize as akGridSize } from '@atlaskit/theme';

import { createErrorIcon, GenericError } from 'common/error';
import { FlagProps, withFlag } from 'common/flag/with-flag';

import { adminProductAccessConfigurationScreenEvent, AnalyticsClientProps, importAccessConfigurationScreenEvent, newUserAccessUIEventData, productAccessConfigurationScreenEvent, ScreenEventSender, toggledNewUserAccessTrackEventData, withAnalyticsClient } from 'common/analytics';

import DefaultProducts from './access-config-default-products.query.graphql';
import accessConfigMutation from './access-config-mutation.graphql';

import { DefaultProductsQuery, SetDefaultProductsMutation } from '../../../schema/schema-types';
import { AccessConfigGroupCard } from './access-config-groups-card';
import { AccessConfigSidebar } from './access-config-sidebar';
import { ConfigType, Group, ProductId } from './access-config-type';
import { productIdToProductName, shouldMakeJiraCoreDefault } from './access-config-util';

export const GroupsTableContainer = styled.div`
  padding-top: ${akGridSize() * 3}px;
`;

export const Container = styled.div`
  padding-left: ${akGridSize() * 6}px;
`;

export const Tab = styled.div`
  display: flex;
`;

export const SidebarWrapper = styled.div`
  padding-top: ${akGridSize() * 4}px;
  padding-left: ${akGridSize() * 4}px;
`;

export const messages = defineMessages({
  errorTitle: {
    id: 'user-management.site-settings.access-configuration.groups.tab.admin.access.error.title',
    defaultMessage: 'No application access config found',
  },
  setErrorTitle: {
    id: 'user-management.site-settings.access-configuration.groups.tab.access.set.error.title',
    defaultMessage: 'Could not set the default state of this product',
  },
});

export interface Configs {
  config: Map<ProductId, Group[]>;
  configType: ConfigType;
  cloudId: string;
}

export type Props = ChildProps<Configs & InjectedIntlProps & FlagProps & AnalyticsClientProps, DefaultProductsQuery & SetDefaultProductsMutation>;

export class AccessConfigTabImpl extends React.Component<Props> {

  public render() {
    if (this.props.data && this.props.data.loading) {
      return (<AkSpinner />);
    }

    if (!this.props.data || this.props.config.size === 0 || this.props.data.error) {
      return (
        <Container>
          <GenericError header={<FormattedMessage {...messages.errorTitle} />} />
        </Container>
      );
    }

    const productIds: string[] = Array.from(this.props.config.keys());

    let event;

    if (this.props.configType === 'adminAccess') {
      event = adminProductAccessConfigurationScreenEvent;
    } else if (this.props.configType === 'productAccess') {
      event = productAccessConfigurationScreenEvent;
    } else {
      event = importAccessConfigurationScreenEvent;
    }

    return (
      <ScreenEventSender
        event={
          event({
            productKeys: productIds,
            productCount: productIds.length,
          })
        }
      >
      <Tab>
        <div>
          {Array.from(this.props.config.keys()).map(product => this.generateTable(product, this.props.config.get(product) as Group[]))}
        </div>
        <SidebarWrapper>
          <AccessConfigSidebar
            configType={this.props.configType}
            products={Array.from(this.props.config.keys())}
          />
        </SidebarWrapper>
      </Tab>
      </ScreenEventSender>
    );
  }

  private generateTable(productId: ProductId, groups: Group[]): React.ReactNode {

    if (!this.props.data || !this.props.data.currentSite) {
      return undefined;
    }

    const defaultProductIds = this.props.data.currentSite.defaultApps.map(defaultProduct => defaultProduct.productId);

    return (
      <GroupsTableContainer key={`${productId}.${this.props.configType}`}>
        <AccessConfigGroupCard
          cloudId={this.props.cloudId}
          productId={productId}
          onToggleClick={this.toggleDefaultTrigger}
          isProductDefault={defaultProductIds.includes(productId)}
          viewOnly={true}
          configType={this.props.configType}
          productName={productIdToProductName[productId]}
          groups={groups}
        />
      </GroupsTableContainer>
    );
  }

  private toggleDefaultTrigger = ({ target: { name, value } }): void => {

    if (!this.props.data || !this.props.data.currentSite) {
      return;
    }

    this.callSetDefaultMutation(name, value === 'true');

    const productIds = Array.from(this.props.config.keys());
    const defaultProductIds = this.props.data.currentSite.defaultApps.map(defaultProduct => defaultProduct.productId as ProductId);

    if (!shouldMakeJiraCoreDefault(productIds, defaultProductIds, name, value === 'true')) {
      return;
    }

    this.callSetDefaultMutation('jira-core', true);
  }

  private callSetDefaultMutation(productId: ProductId, isDefault: boolean): void {
    if (!this.props.mutate) {
      return;
    }

    this.props.analyticsClient.sendUIEvent({
      cloudId: this.props.cloudId,
      data: newUserAccessUIEventData({
        productKey: productId,
        hasAccess: !isDefault,
      }),
    });

    this.props.mutate({
      variables: { id: this.props.cloudId, input: { productId, productDefault: isDefault } },
      refetchQueries: [{ query: DefaultProducts }],
    }).then(() => {
      this.props.analyticsClient.sendTrackEvent({
        cloudId: this.props.cloudId,
        data: toggledNewUserAccessTrackEventData({
          productKey: productId,
          hasAccess: !isDefault,
        }),
      });
    }).catch(async () => {
      this.props.showFlag({
        autoDismiss: true,
        icon: createErrorIcon(),
        id: `user-management.site-settings.access-configuration.groups.tab.access`,
        title: this.props.intl.formatMessage(messages.setErrorTitle),
      });

      if (!this.props.data) {
        return;
      }
      await this.props.data.refetch();
    });
  }
}

const withMutation = graphql<Props, SetDefaultProductsMutation>(accessConfigMutation);
const withGetDefaultProducts = graphql<Props, DefaultProductsQuery>(DefaultProducts, {
  skip: ({ cloudId }) => !cloudId,
});

export const AccessConfigTab = withAnalyticsClient(withFlag(injectIntl(
  withGetDefaultProducts(
    withMutation(
      AccessConfigTabImpl,
      ),
    ),
  ),
));
