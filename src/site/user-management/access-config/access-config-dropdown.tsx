import { DataProxy } from 'apollo-cache';
import * as React from 'react';
import { ChildProps, graphql, MutationFunc } from 'react-apollo';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';

import AkButton from '@atlaskit/button';
import AkDropdownMenu from '@atlaskit/dropdown-menu';
import AkMoreIcon from '@atlaskit/icon/glyph/more';
import AkTooltip from '@atlaskit/tooltip';

import { AnalyticsClientProps, DropdownItem as AnalyticsDropdownItem, makeDefaultGroupTrackEventData, makeGroupDefaultUIEventData, removeAdminGroupTrackEventData, removeDefaultGroupTrackEventData, removeGroupDefaultUIEventData, removeGroupFromAdminProductUIEventData, removeGroupFromProductUIEventData, removeGroupTrackEventData, showAdminGroupDetailsUIEventData, showGroupDetailsUIEventData, withAnalyticsClient } from 'common/analytics';

import { createErrorIcon, createSuccessIcon } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';

import { RemoveGroupMutation, RemoveGroupMutationVariables, SetDefaultGroupMutation, SetDefaultGroupMutationVariables } from '../../../schema/schema-types';
import { util } from '../../../utilities/admin-hub';
import DefaultProducts from './access-config-default-products.query.graphql';
import { AccessConfigImportActions } from './access-config-import-actions';
import removeGroupsMutation from './access-config-remove-groups.mutation.graphql';
import setDefaultGroupsMutation from './access-config-set-default-groups.query.graphql';
import { ConfigType, ProductId } from './access-config-type';
import { optimisticallyRemoveGroup, optimisticallyUpdateDefaultGroup } from './access-config.query.updater';
import { DropdownContainer } from './access-config.styled';

export const messages = defineMessages({
  setDefaultGroup: {
    id: `user-management.site-settings.access-configuration.groups.table.set.default.group`,
    defaultMessage: `Make this group default`,
  },
  unsetDefaultGroup: {
    id: `user-management.site-settings.access-configuration.groups.table.unset.default.group`,
    defaultMessage: `Don't make this group default`,
  },
  errorDefaultTitle: {
    id: `user-management.site-settings.access-configuration.groups.table.set.default.group.error.flag`,
    defaultMessage: `Could not make this group default`,
  },
  errorRemoveTitle: {
    id: `user-management.site-settings.access-configuration.groups.table.remove.group.error.flag.remove.title`,
    defaultMessage: `Could not remove this group`,
  },
  successDefaultTitle: {
    id: `user-management.site-settings.access-configuration.groups.table.success.title`,
    description: 'Title that displays on the flag when a group has access removed from a product',
    defaultMessage: `{configType} access was removed from group`,
  },
  successDefaultDescription: {
    id: `user-management.site-settings.access-configuration.groups.table.success.description`,
    description: 'Description that displays on the flag when a group has access removed from a product',
    defaultMessage: `{productName} access was removed from {groupName}. You can add product access back to this group at any time.`,
  },
  showDetails: {
    id: `user-management.site-settings.access-configuration.groups.table.show.details`,
    defaultMessage: `Show group's details`,
  },
  removeGroup: {
    id: `user-management.site-settings.access-configuration.groups.table.remove.group`,
    defaultMessage: `Remove group`,
  },
  defaultGroupDisabled: {
    id: `user-management.site-settings.access-configuration.groups.table.default.group.action.disabled.default`,
    description: 'Tooltip information displayed indicating that the group cannot be removed because it is a default group.',
    defaultMessage: `A default group cannot be removed.`,
  },
  defaultGroupDisabledLastDefault: {
    id: `user-management.site-settings.access-configuration.groups.table.default.group.action.disabled.last.default`,
    description: 'Tooltip information displayed indicating that the group cannot be made default because it is the last default group.',
    defaultMessage: `There must be at least one default group per product.`,
  },
  defaultGroupDisabledAdmin: {
    id: `user-management.site-settings.access-configuration.groups.table.default.group.action.disabled.admin`,
    description: 'Tooltip information displayed indicating that the group cannot be made default because it is an admin group.',
    defaultMessage: `Groups used in administration access cannot be set as a default access group.`,
  },
});

export interface AccessConfigDropdownProps {
  cloudId: string;
  productId: ProductId;
  productName: string;
  groupName: string;
  groupId: string;
  isAdmin: boolean;
  isDefault: boolean;
  isLastDefault: boolean;
  configType: ConfigType;
  setDefaultGroup?: MutationFunc<SetDefaultGroupMutation, SetDefaultGroupMutationVariables>;
  removeGroup?: MutationFunc<RemoveGroupMutation, RemoveGroupMutationVariables>;
}

export class AccessConfigDropdownImpl extends React.Component<ChildProps<AnalyticsClientProps & InjectedIntlProps & AccessConfigDropdownProps & FlagProps & RouteComponentProps<{}>, SetDefaultGroupMutation>> {

  public render() {

    const { formatMessage } = this.props.intl;

    if (this.props.configType === 'importAccess') {
      return (
        <DropdownContainer>
          <AccessConfigImportActions
            groupId={this.props.groupId}
            groupName={this.props.groupName}
            cloudId={this.props.cloudId}
            productId={this.props.productId}
          />
        </DropdownContainer>
      );
    }

    const showGroupEvent = this.props.configType === 'productAccess' ? showGroupDetailsUIEventData : showAdminGroupDetailsUIEventData;
    const removeGroupEvent = this.props.configType === 'productAccess' ? removeGroupFromProductUIEventData : removeGroupFromAdminProductUIEventData;
    const defaultGroupEvent = this.props.isDefault ? removeGroupDefaultUIEventData : makeGroupDefaultUIEventData;

    return (
      <DropdownContainer>
        <AkDropdownMenu trigger={<AkButton appearance="subtle" iconAfter={<AkMoreIcon label="" />} />} position="bottom right">
          <AnalyticsDropdownItem
            href={`${util.siteAdminBasePath}/s/${this.props.cloudId}/groups/${this.props.groupId}`}
            onClick={this.onGroupClick}
            analyticsData={showGroupEvent({
              productKey: this.props.productId,
              groupId: this.props.groupId,
            })}
          >
            {formatMessage(messages.showDetails)}
          </AnalyticsDropdownItem>
          {this.props.configType === 'productAccess' &&
            <AkTooltip content={this.generateMakeDefaultGroupTooltip()}>
              <AnalyticsDropdownItem
                isDisabled={!!this.generateMakeDefaultGroupTooltip()}
                onClick={this.callSetDefaultProductMutation}
                analyticsData={defaultGroupEvent({
                  productKey: this.props.productId,
                  groupId: this.props.groupId,
                })}
              >
                {formatMessage(this.props.isDefault ? messages.unsetDefaultGroup : messages.setDefaultGroup)}
              </AnalyticsDropdownItem>
            </AkTooltip>
          }
          <AkTooltip content={this.generateRemoveGroupTooltip()}>
            <AnalyticsDropdownItem
              isDisabled={!!this.generateRemoveGroupTooltip()}
              onClick={this.callRemoveGroupProductMutation}
              analyticsData={removeGroupEvent({
                productKey: this.props.productId,
              })}
            >
              {formatMessage(messages.removeGroup)}
            </AnalyticsDropdownItem>
          </AkTooltip>
        </AkDropdownMenu>
      </DropdownContainer>
    );
  }

  private onGroupClick = (e: React.MouseEvent<HTMLAnchorElement>) => {
    e.preventDefault();
    this.props.history.push(e.currentTarget.getAttribute('href')!);
  };

  private generateRemoveGroupTooltip = (): React.ReactNode => {
    if (this.props.isDefault) {
      return this.props.intl.formatMessage(messages.defaultGroupDisabled);
    }

    return null;
  }

  private generateMakeDefaultGroupTooltip = (): React.ReactNode => {
    if (this.props.isAdmin) {
      return this.props.intl.formatMessage(messages.defaultGroupDisabledAdmin);
    } else if (this.props.isLastDefault) {
      return this.props.intl.formatMessage(messages.defaultGroupDisabledLastDefault);
    }

    return null;
  }

  private callRemoveGroupProductMutation = (): void => {
    if (!this.props.removeGroup) {
      return;
    }
    this.props.removeGroup({
      variables: {
        id: this.props.cloudId,
        input: {
          groupId: this.props.groupId,
          productId: this.props.productId,
          productType: this.props.configType === 'productAccess' ? 'USE' : 'ADMIN',
        },
      },
      refetchQueries: [{ query: DefaultProducts }],
      update: (store: DataProxy) => optimisticallyRemoveGroup(store, this.props.configType, this.props.productId, this.props.groupId),
    }).then(() => {
      const event = this.props.isAdmin ? removeGroupTrackEventData : removeAdminGroupTrackEventData;
      this.props.analyticsClient.sendTrackEvent({
        cloudId: this.props.cloudId,
        data: event({
          productKey: this.props.productId,
          groupId: this.props.groupId,
        }),
      });

      this.props.showFlag({
        autoDismiss: true,
        icon: createSuccessIcon(),
        id: `user-management.site-settings.access-configuration.groups.table.success.flag.${Date.now()}`,
        title: this.props.intl.formatMessage(messages.successDefaultTitle, { configType: this.props.configType === 'productAccess' ? 'Product' : 'Admin' }),
        description: this.props.intl.formatMessage(messages.successDefaultDescription, { productName: this.props.productName, groupName: this.props.groupName }),
      });
    }).catch(async () => {
      this.props.showFlag({
        autoDismiss: true,
        icon: createErrorIcon(),
        id: `user-management.site-settings.access-configuration.groups.table.error.flag.${Date.now()}`,
        title: this.props.intl.formatMessage(messages.errorRemoveTitle),
      });
    });
  }

  private callSetDefaultProductMutation = (): void => {
    if (!this.props.setDefaultGroup) {
      return;
    }
    this.props.setDefaultGroup({
      variables: {
        id: this.props.cloudId,
        input: {
          groupId: this.props.groupId,
          productId: this.props.productId,
          isDefault: this.props.isDefault,
        },
      },
      update: (store) => optimisticallyUpdateDefaultGroup(store, this.props.productId, this.props.groupId, this.props.isDefault),
    }).then(() => {
      const event = this.props.isDefault ? removeDefaultGroupTrackEventData : makeDefaultGroupTrackEventData;
      this.props.analyticsClient.sendTrackEvent({
        cloudId: this.props.cloudId,
        data: event({
          productKey: this.props.productId,
          groupId: this.props.groupId,
        }),
      });
    }).catch(async () => {
      this.props.showFlag({
        autoDismiss: true,
        icon: createErrorIcon(),
        id: messages.errorDefaultTitle.id,
        title: this.props.intl.formatMessage(messages.errorDefaultTitle),
      });
    });
  }

}
const withRemoveGroup = graphql<AccessConfigDropdownProps & FlagProps & InjectedIntlProps, SetDefaultGroupMutation>(removeGroupsMutation, { name: 'removeGroup' });
const withSetDefaultGroup = graphql<AccessConfigDropdownProps & FlagProps & InjectedIntlProps, SetDefaultGroupMutation>(setDefaultGroupsMutation, { name: 'setDefaultGroup' });

export const AccessConfigDropdown = withRouter(
  withFlag(
    injectIntl(
      withSetDefaultGroup(
        withRemoveGroup(
          withAnalyticsClient(
            AccessConfigDropdownImpl,
          ),
        ),
      ),
    ),
  ),
);
