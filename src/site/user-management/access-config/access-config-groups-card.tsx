import * as React from 'react';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import {
  colors as akColors,
  gridSize as akGridSize,
} from '@atlaskit/theme';
import AkToggle from '@atlaskit/toggle';

import { addGroupUIEventData, Button as AnalyticsButton } from 'common/analytics';

import { AccessConfigAddGroupsToProductModal } from './access-config-add-groups-to-product-modal';
import { AccessConfigGroupTable } from './access-config-group-table';
import { ConfigType, Group, ProductId } from './access-config-type';

export const messages = defineMessages({
  toggleLabel: {
    id: `user-management.site-settings.access-configuration.groups.table.toggle.label`,
    defaultMessage: `New users have access to this product`,
  },
  buttonAddGroup: {
    id: `user-management.site-settings.access-configuration.groups.table.button.add.group`,
    defaultMessage: `Add group`,
  },
  linkManangePermissions: {
    id: `user-management.site-settings.access-configuration.groups.table.link.manage.permissions`,
    defaultMessage: `Manage {product} global permissions`,
  },
});

export const ActionsContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: ${akGridSize() * 2}px 0 ${akGridSize() * 2}px;
`;

export const ToggleContainer = styled.div`
  display: flex;
  align-items: center;
  margin-left: -${akGridSize() * 0.75}px;
`;

export const LinkContainer = styled.div`
  padding-top: ${akGridSize() * 2}px;
  > a {
    color: ${akColors.N200};
    float: right;
    display: block;
  }
`;

export const Divider = styled.span`
  padding-right: 3px;
  font-size: 0;
`;

export interface OwnProps {
  cloudId: string;
  containerWidth?: number;
  managePermissionsLink?: string;
  viewOnly?: boolean;
  onToggleClick?(event: any): void;
}

export interface ConfigProps {
  configType: ConfigType;
  productName: string;
  groups: Group[];
  productId: ProductId;
  isProductDefault: boolean;
}

export interface State {
  isAddGroupDialogOpen: boolean;
}

export class AccessConfigGroupCardImpl extends React.Component<OwnProps & InjectedIntlProps & ConfigProps> {

  public static defaultProps: Partial<OwnProps> = {
    onToggleClick: () => null,
  };

  public readonly state: Readonly<State> = {
    isAddGroupDialogOpen: false,
  };

  public AccessConfigContainer = styled.div`
      width: ${this.props.containerWidth ? this.props.containerWidth : akGridSize() * 70}px;
  `;

  public render() {

    return (
      <this.AccessConfigContainer>
        {this.generateHeaderContent()}
        <AccessConfigGroupTable
          cloudId={this.props.cloudId}
          configType={this.props.configType}
          productName={this.props.productName}
          groups={this.props.groups}
          productId={this.props.productId}
        />
        {this.generateLink()}
        <AccessConfigAddGroupsToProductModal
          isOpen={this.state.isAddGroupDialogOpen}
          cloudId={this.props.cloudId}
          productId={this.props.productId}
          productName={this.props.productName}
          productType={this.props.configType === 'adminAccess' ? 'ADMIN' : 'USE'}
          excludedGroupIds={this.props.groups.map(group => group.id)}
          onDialogDismissed={this.closeGroupDialog}
        />
      </this.AccessConfigContainer>
    );
  }

  private generateLink(): React.ReactNode {
    if (!this.props.managePermissionsLink) {
      return null;
    }

    return (
      <LinkContainer>
        <a href={this.props.managePermissionsLink} target="_blank" rel="noopener noreferrer">
          <FormattedMessage
            {...messages.linkManangePermissions}
            values={{ product: this.props.productName }}
          />
        </a>
      </LinkContainer>
    );
  }

  private generateHeaderContent(): React.ReactNode {

    const space = '&nbsp;';

    const addGroupButton: React.ReactNode = (
      <AnalyticsButton
        onClick={this.openGroupDialog}
        analyticsData={addGroupUIEventData({
          productKey: this.props.productId,
        })}
      >
        {this.props.intl.formatMessage(messages.buttonAddGroup)}
      </AnalyticsButton>
    );

    const grantAccessToggle: React.ReactNode = (
      <ToggleContainer>
        <AkToggle checked={this.props.isProductDefault} isDefaultChecked={this.props.isProductDefault} name={this.props.productId} value={!this.props.isProductDefault} onChange={this.props.onToggleClick}/>
        <Divider>{space}</Divider>
        <span>{this.props.intl.formatMessage(messages.toggleLabel)}</span>
      </ToggleContainer>
    );

    if (this.props.configType === 'productAccess') {
      return (
        <div>
          <h3>{this.props.productName}</h3>
          <ActionsContainer>
            {grantAccessToggle}
            {addGroupButton}
          </ActionsContainer>
        </div>
      );
    }

    return (
      <ActionsContainer>
        <h3>{this.props.productName}</h3>
        {addGroupButton}
      </ActionsContainer>
    );
  }

  private openGroupDialog = () => {
    this.setState({ isAddGroupDialogOpen: true });
  };

  private closeGroupDialog = () => {
    this.setState({ isAddGroupDialogOpen: false });
  };
}

export const AccessConfigGroupCard = injectIntl<OwnProps & ConfigProps>(AccessConfigGroupCardImpl);
