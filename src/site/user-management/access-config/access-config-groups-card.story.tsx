import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';

import AkPage from '@atlaskit/page';

import { createApolloClient } from '../../../apollo-client';
import { AccessConfigGroupCard } from './access-config-groups-card';

function onToggle() {
  // tslint:disable-next-line:no-console
  console.log('Toggle has been clicked');
}

const client = createApolloClient();

storiesOf('Site|Access Config/Group Table', module)
  .add('Product access config group table', () => (
    <ApolloProvider client={client}>
      <IntlProvider locale="en">
        <AkPage>
          <AccessConfigGroupCard
            cloudId={'DUMMY-CLOUD-ID'}
            productId="jira-core"
            isProductDefault={false}
            productName="Jira Core"
            configType={'productAccess'}
            onToggleClick={onToggle}
            groups={[
              {
                id: 'f99d8a20-f855-11e8-b568-0800200c9a668',
                name: 'jira-core-admins',
                admin: true,
                default: false,
                requiresApproval: false,
                managementAccess: 'ALL',
                ownerType: null,
              },
              {
                id: 'f99db117-f855-11e8-b568-0800200c9a669',
                name: 'jira-server-admins',
                admin: false,
                default: true,
                requiresApproval: false,
                managementAccess: 'ALL',
                ownerType: null,
              },
              {
                id: 'f99dff34-f855-11e8-b568-0800200c9a660',
                name: 'some-really-really-really-really-really-really-really-long-group-name',
                admin: false,
                default: true,
                requiresApproval: false,
                managementAccess: 'ALL',
                ownerType: null,
              },
              {
                id: 'f99dff34-f855-11e8-b568-0800200c9a621',
                name: 'confluence-admins',
                admin: false,
                default: true,
                requiresApproval: false,
                managementAccess: 'READ_ONLY',
                ownerType: 'EXT_SCIM',
              },
            ]}
          />
        </AkPage>
      </IntlProvider>
    </ApolloProvider>
  ))
  .add('Empty product access config group table', () => (
    <IntlProvider locale="en">
      <AkPage>
        <AccessConfigGroupCard
          cloudId={'DUMMY-CLOUD-ID'}
          productId="jira-core"
          isProductDefault={false}
          productName="Jira Core"
          configType={'productAccess'}
          groups={[]}
        />
      </AkPage>
    </IntlProvider>
  ))
  .add('Admin access config group table', () => (
    <ApolloProvider client={client}>
      <IntlProvider locale="en">
        <AkPage>
          <AccessConfigGroupCard
            cloudId={'DUMMY-CLOUD-ID'}
            productId="jira-core"
            isProductDefault={false}
            productName="Jira Core"
            configType={'adminAccess'}
            managePermissionsLink={'#'}
            groups={[
              {
                id: 'f99dff34-f855-11e8-b568-0800200c9a661',
                name: 'jira-core-admins',
                admin: true,
                default: false,
                requiresApproval: false,
                managementAccess: 'ALL',
                ownerType: null,
              },
              {
                id: 'f99dff4e-f855-11e8-b568-0800200c9a662',
                name: 'jira-other-admins',
                admin: true,
                default: false,
                requiresApproval: false,
                managementAccess: 'ALL',
                ownerType: null,
              },
            ]}
          />
        </AkPage>
      </IntlProvider>
    </ApolloProvider>
  ));
