import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';

import { AccessConfigSidebar } from './access-config-sidebar';

const pageDecorator = (storyFn) => (
  <IntlProvider locale="en">
    <div style={{ width: '500px' }}>
      {storyFn()}
    </div>
  </IntlProvider>
);

storiesOf('Site|Access Config/Sidebar', module)
  .addDecorator(pageDecorator)
  .add('Sidebar for USE access config', () => {
    return (
      <AccessConfigSidebar configType="productAccess" products={[]} />
    );
  }).add('Sidebar for ADMIN access config - All products', () => {
    return (
      <AccessConfigSidebar configType="adminAccess" products={['conf', 'hipchat.cloud', 'jira-admin']} />
    );
  }).add('Sidebar for ADMIN access config - No Jira', () => {
    return (
      <AccessConfigSidebar configType="adminAccess" products={['conf', 'hipchat.cloud']} />
    );
  }).add('Sidebar for ADMIN access config - Only Jira', () => {
    return (
      <AccessConfigSidebar configType="adminAccess" products={['jira-admin']}/>
    );
  }).add('Sidebar for ADMIN access config - No products', () => {
    return (
      <AccessConfigSidebar configType="adminAccess" products={[]}/>
    );
  });
