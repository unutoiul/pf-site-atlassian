import * as React from 'react';
import { graphql, MutationFunc } from 'react-apollo';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import AkButton from '@atlaskit/button';

import { createErrorIcon } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';

import { ApproveImportedGroupMutation, ApproveImportedGroupMutationArgs, RejectImportedGroupMutation, RejectImportedGroupMutationArgs } from '../../../schema/schema-types';
import ApproveImportedGroup from './access-config-approve-imported-group.query.graphql';
import RejectImportedGroup from './access-config-reject-imported-group.query.graphql';
import { ProductId } from './access-config-type';
import { optimisticallyUpdateImportedGroup } from './access-config.query.updater';

export const messages = defineMessages({
  approve: {
    id: 'user-management.site-settings.access-configuration.import.action.approve',
    defaultMessage: 'Approve',
  },
  reject: {
    id: 'user-management.site-settings.access-configuration.import.action.reject',
    defaultMessage: 'Reject',
  },
  errorTitle: {
    id: 'user-management.site-settings.access-configuration.import.error.title',
    defaultMessage: 'Something went wrong!',
  },
  errorDescription: {
    id: 'user-management.site-settings.access-configuration.import.error.banner.title',
    defaultMessage: 'Could not approve or reject access for the "{groupName}" group. Please try again later.',
  },
});

interface OwnProps {
  cloudId: string;
  productId: ProductId;
  groupName: string;
  groupId: string;
}

interface MutationProps {
  approveImportedGroup?: MutationFunc<ApproveImportedGroupMutation, ApproveImportedGroupMutationArgs>;
  rejectImportedGroup?: MutationFunc<RejectImportedGroupMutation, RejectImportedGroupMutationArgs>;
}

export class AccessConfigImportActionsImpl extends React.Component<OwnProps & MutationProps & InjectedIntlProps & FlagProps> {

  public render() {
    return (
      <div>
        <AkButton onClick={this.approveImportedGroup} appearance="link">{this.props.intl.formatMessage(messages.approve)}</AkButton>
        <AkButton onClick={this.rejectImportedGroup} appearance="subtle-link">{this.props.intl.formatMessage(messages.reject)}</AkButton>
      </div>
    );
  }

  private rejectImportedGroup = (): void => {
    if (!this.props.rejectImportedGroup) {
      return;
    }
    this.props.rejectImportedGroup({
      variables: {
        id: this.props.cloudId,
        input: {
          groupId: this.props.groupId,
          productId: this.props.productId,
        },
      },
      update: (store) => optimisticallyUpdateImportedGroup(store, 'revoke', this.props.productId, this.props.groupId),
    }).catch(async () => {
      this.props.showFlag({
        autoDismiss: true,
        icon: createErrorIcon(),
        id: messages.errorTitle.id,
        title: this.props.intl.formatMessage(messages.errorTitle),
        description: this.props.intl.formatMessage(messages.errorDescription, { groupName: this.props.groupName }),
      });
    });
  }

  private approveImportedGroup = (): void => {
    if (!this.props.approveImportedGroup) {
      return;
    }
    this.props.approveImportedGroup({
      variables: {
        id: this.props.cloudId,
        input: {
          groupId: this.props.groupId,
          productId: this.props.productId,
        },
      },
      update: (store) => optimisticallyUpdateImportedGroup(store, 'grant', this.props.productId, this.props.groupId),
    }).catch(async () => {
      this.props.showFlag({
        autoDismiss: true,
        icon: createErrorIcon(),
        id: messages.errorTitle.id,
        title: this.props.intl.formatMessage(messages.errorTitle),
        description: this.props.intl.formatMessage(messages.errorDescription, { groupName: this.props.groupName }),
      });
    });
  }
}

const withApproveImportedGroup = graphql<OwnProps & FlagProps, ApproveImportedGroupMutation>(ApproveImportedGroup, { name: 'approveImportedGroup' });
const withRejectImportedGroup = graphql<OwnProps & FlagProps, RejectImportedGroupMutation>(RejectImportedGroup, { name: 'rejectImportedGroup' });
export const AccessConfigImportActions = withFlag(withRejectImportedGroup(withApproveImportedGroup(injectIntl(AccessConfigImportActionsImpl))));
