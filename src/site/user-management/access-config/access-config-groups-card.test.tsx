import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';

import AkToggle from '@atlaskit/toggle';

import { Button as AnalyticsButton } from 'common/analytics';

import { createMockIntlProp } from '../../../utilities/testing';
import { AccessConfigAddGroupsToProductModal } from './access-config-add-groups-to-product-modal';
import { AccessConfigGroupTable } from './access-config-group-table';
import { AccessConfigGroupCardImpl, messages } from './access-config-groups-card';
import { accessConfig } from './access-config-test-util';
import { ConfigType, Group } from './access-config-type';

describe('Access Config Groups Table Test', () => {

  function createTestComponent(configType: ConfigType, groups: Group[], link?: string, isProductDefault?: boolean): ShallowWrapper {
    return shallow(
      <AccessConfigGroupCardImpl
        cloudId="DUMMY-CLOUD-ID"
        viewOnly={false}
        productId={'jira-core'}
        productName={'Jira Core'}
        configType={configType}
        groups={groups}
        intl={createMockIntlProp()}
        managePermissionsLink={link}
        isProductDefault={!!isProductDefault}
      />,
    );
  }

  it('Product name should display', () => {
    const wrapper = createTestComponent('adminAccess', accessConfig[0].groups, '#');
    expect(wrapper.find('h3').text()).equals('Jira Core');
  });

  it('Should show link if included', () => {
    const wrapper = createTestComponent('adminAccess', accessConfig[0].groups, '#');
    const link = wrapper.find('a');
    expect(link.children().type()).to.equal(FormattedMessage);
    expect(link.children().props().defaultMessage).equals(messages.linkManangePermissions.defaultMessage);
  });

  it('Should hide link if not included', () => {
    const wrapper = createTestComponent('adminAccess', accessConfig[0].groups);
    const link = wrapper.find('a');
    expect(link.length).to.equal(0);
  });

  it('Should set isAddGroupDialogOpen state to true when the add group button is clicked', () => {
    const wrapper = createTestComponent('productAccess', accessConfig[0].groups);
    expect(wrapper.state('isAddGroupDialogOpen')).to.equal(false);
    const addGroup = wrapper.find(AnalyticsButton).filterWhere(btn => btn.children().text() === 'Add group');
    expect(addGroup).to.have.length(1);
    addGroup.simulate('click');
    expect(wrapper.state('isAddGroupDialogOpen')).to.equal(true);
  });

  it('Should exclude all currently selected groups from the groups picker', () => {
    const groups = accessConfig[1].groups;
    const wrapper = createTestComponent('productAccess', groups);
    const addGroupsModal = wrapper.find(AccessConfigAddGroupsToProductModal);
    expect(addGroupsModal).to.have.length(1);
    expect(addGroupsModal.prop('excludedGroupIds')).to.deep.equal(groups.map(group => group.id));
  });

  describe('when component is of Admin Access type', () => {

    it('Should not include toggle', () => {
      const wrapper = createTestComponent('adminAccess', accessConfig[0].groups);
      const toggle = wrapper.find(AkToggle);
      expect(toggle.length).to.equal(0);
    });

    it('Should show table with groups in it', () => {
      const wrapper = createTestComponent('productAccess', accessConfig[1].groups);
      const table = wrapper.find(AccessConfigGroupTable);
      expect(table.length).to.equal(1);
    });
  });

  describe('when component is of Product Access type', () => {

    it('Should include toggle', () => {
      const wrapper = createTestComponent('productAccess', accessConfig[1].groups);
      const toggle = wrapper.find(AkToggle);
      expect(toggle.length).to.equal(1);
    });

    it('When it is a default product, the toggle should be checked', () => {
      const wrapper = createTestComponent('productAccess', accessConfig[1].groups, undefined, true);
      const toggle = wrapper.find(AkToggle);
      expect((toggle.props() as any).isChecked).to.equal(undefined);
    });

    it('When it is not a default product, the toggle should not be checked', () => {
      const wrapper = createTestComponent('productAccess', accessConfig[1].groups, undefined, false);
      const toggle = wrapper.find(AkToggle);
      expect((toggle.props() as any).isDefaultChecked).to.equal(false);
    });

    it('Should show table with groups in it', () => {
      const wrapper = createTestComponent('productAccess', accessConfig[1].groups);
      const table = wrapper.find(AccessConfigGroupTable);
      expect(table.length).to.equal(1);
    });
  });
});
