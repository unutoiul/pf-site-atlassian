import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps } from 'react-router';
import styled from 'styled-components';

import AkSpinner from '@atlaskit/spinner';
import AkTabs from '@atlaskit/tabs';

import { GenericError } from 'common/error';
import { PageLayout } from 'common/page-layout';

import { AccessConfigQuery, ManagementAccess } from '../../../schema/schema-types';
import { AccessConfigImportBanner } from './access-config-import-banner';
import { AccessConfigTab } from './access-config-tab';
import { ProductGroupsConfig, ProductId } from './access-config-type';
import { formatImportedProduct, formatProduct } from './access-config-util';
import AccessConfig from './access-config.query.graphql';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
`;

export const messages = defineMessages({
  pageTitle: {
    id: `user-management.site-settings.access-configuration.groups.page.page.title`,
    defaultMessage: `Product access`,
  },
  pageDescription: {
    id: `user-management.site-settings.access-configuration.groups.page.page.description`,
    defaultMessage: `View and configure which groups provide access to your products.`,
  },
  productAccessTabTitle: {
    id: `user-management.site-settings.access-configuration.groups.page.product.access.tab.title`,
    defaultMessage: `Product access`,
  },
  adminAccessTabTitle: {
    id: `user-management.site-settings.access-configuration.groups.page.admin.access.tab.title`,
    defaultMessage: `Administration access`,
  },
  errorTitle: {
    id: `user-management.site-settings.access-configuration.groups.page.admin.access.error.title`,
    defaultMessage: `Application access page unavailable`,
  },
});

interface SiteParams {
  cloudId: string;
}

type AccessConfigurationPageProps = ChildProps<InjectedIntlProps & RouteComponentProps<SiteParams>, AccessConfigQuery>;

interface AccessConfigurationPageState {
  selectedTabIndex: number;
}

export class AccessConfigurationPageImpl extends React.Component<AccessConfigurationPageProps, AccessConfigurationPageState> {
  public readonly state: Readonly<AccessConfigurationPageState> = {
    selectedTabIndex: 0,
  };

  public render() {
    const { formatMessage } = this.props.intl;

    if (!this.props.data) {
      return null;
    }

    if (this.props.data.loading) {
      return (
        <PageLayout
          title={formatMessage(messages.pageTitle)}
          description={formatMessage(messages.pageDescription)}
        >
          <AkSpinner />
        </PageLayout>
      );
    }

    if (this.props.data.error || !this.props.data.currentSite) {
      return <GenericError header={<FormattedMessage {...messages.errorTitle} />} />;
    }

    const groupsAdminAccessConfig = this.convertGroupAccessToAccessConfigGroup(this.props.data.currentSite.groupsAdminAccessConfig);
    const groupsUseAccessConfig = this.convertGroupAccessToAccessConfigGroup(this.props.data.currentSite.groupsUseAccessConfig);
    const cloudId = this.props.match.params.cloudId;

    const adminGroupsMap = formatProduct(groupsAdminAccessConfig);
    const useGroupsMap = formatProduct(groupsUseAccessConfig);
    const importedGroupsMap = formatImportedProduct(useGroupsMap);

    const tabs = (
    [
      {
        label: formatMessage(messages.productAccessTabTitle),
        content: <Container><AccessConfigTab cloudId={cloudId} config={useGroupsMap} configType="productAccess" /></Container>,
      },
      {
        label: formatMessage(messages.adminAccessTabTitle),
        content: <Container><AccessConfigTab cloudId={cloudId} config={adminGroupsMap} configType="adminAccess" /></Container>,
      },
    ]
    );

    return (
      <PageLayout
        title={formatMessage(messages.pageTitle)}
        description={formatMessage(messages.pageDescription)}
        isFullWidth={true}
      >
        {importedGroupsMap.size > 0 && <AccessConfigImportBanner useGroups={importedGroupsMap} cloudId={this.props.match.params.cloudId} />}
        <AkTabs tabs={tabs} selected={this.state.selectedTabIndex} onSelect={this.onTabSelect} />
      </PageLayout>
    );
  }

  private onTabSelect = (_, index: number) => {
    this.setState({ selectedTabIndex: index });
  }

  // TODO react-apollo upgrade test this
  private convertGroupAccessToAccessConfigGroup = (groupAccessConfig: AccessConfigQuery['currentSite']['groupsAdminAccessConfig'] | AccessConfigQuery['currentSite']['groupsUseAccessConfig']): ProductGroupsConfig[] => {
    return groupAccessConfig.map((groupAccess) => ({
      product: {
        productName: groupAccess.product.productName,
        productId: groupAccess.product.productId as ProductId,
      },
      groups: groupAccess.groups.map((group) => ({
        id: group.id,
        managementAccess: 'NONE' as ManagementAccess,
        admin: group.productPermission && group.productPermission.includes('MANAGE'),
        name: group ? group.name : '',
        default: group ? group.default : false,
        requiresApproval: group ? group.requiresApproval : true,
      })),
    }));
  }
}

const withQuery = graphql<AccessConfigurationPageProps, AccessConfigQuery>(AccessConfig);

export const AccessConfigurationPage = injectIntl(
  withQuery(
    AccessConfigurationPageImpl,
  ),
);
