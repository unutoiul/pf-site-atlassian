import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import {
  defineMessages,
  InjectedIntlProps,
  injectIntl,
} from 'react-intl';

import {
  ButtonGroup as AkButtonGroup,
} from '@atlaskit/button';

import { addGroupProductAccessModalScreenEvent, addGroupToAdminProductUIEventData, addGroupToProductUIEventData, addGroupTrackEventData, AnalyticsClientProps, Button as AnalyticsButton, cancelAddGroupToAdminProductUIEventData, cancelAddGroupToProductUIEventData, ScreenEventSender, withAnalyticsClient } from 'common/analytics';
import { createErrorIcon, createSuccessIcon } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';
import { Group } from 'common/picker';

import { AddGroupsAccessToProductMutation, AddGroupsAccessToProductMutationVariables, ProductType } from '../../../schema/schema-types';
// tslint:disable-next-line:disallow-relative-import-for-common
import { GroupsPickerModal } from '../common/groups-picker-modal';
import addGroupsAccessToProductMutation from './access-config-add-groups-to-product-modal.mutation.graphql';
import { ProductId } from './access-config-type';
import { optimisticallyAddGroup } from './access-config.query.updater';

const messages = defineMessages({
  title: {
    id: 'access.config.add.groups.to.product.modal.title',
    defaultMessage: 'Add group to {productName}',
    description: 'The title of the modal.',
  },
  inputLabelText: {
    id: 'access.config.add.groups.to.product.modal.input.label.text',
    defaultMessage: 'Groups',
    description: 'What the search input text is.',
  },
  placeholderText: {
    id: 'access.config.add.groups.to.product.modal.placeholder.text',
    defaultMessage: 'Search',
    description: 'Search input default text.',
  },
  addButton: {
    id: 'access.config.add.groups.to.product.modal.button.add',
    defaultMessage: 'Add groups',
    description: 'Add action for the popup modal.',
  },
  cancelButton: {
    id: 'access.config.add.groups.to.product.modal.button.cancel',
    defaultMessage: 'Cancel',
    description: 'Cancel action for the popup modal.',
  },
  successFlagTitle: {
    id: 'access.config.add.groups.to.product.modal.flag.success.title',
    defaultMessage: 'Product access updated!',
    description: 'Title to show in the success flag when the groups have been added to the product.',
  },
  successFlagDescription: {
    id: 'access.config.add.groups.to.product.modal.flag.success.description',
    defaultMessage: `You added {groups, plural,
      one {the {groupname} group}
      other {# groups}} to {productName} product access.`,
    description: 'Description to show in the success flag when the groups have been added to the product.',
  },
  errorFlagTitle: {
    id: 'access.config.add.groups.to.product.modal.flag.error.title',
    defaultMessage: 'Unable to update product access. Try again later.',
    description: `Title to show in the error flag when the product access couldn't be updated.`,
  },
});

interface Props {
  isOpen: boolean;
  cloudId: string;
  productId: ProductId;
  productName: string;
  productType: ProductType;
  excludedGroupIds?: string[];
  onDialogDismissed?(): void;
}

interface State {
  isMutationLoading: boolean;
}

export type AccessConfigAddGroupsToProductModalProps = ChildProps<Props, AddGroupsAccessToProductMutation, AddGroupsAccessToProductMutationVariables> & InjectedIntlProps & FlagProps & AnalyticsClientProps;

export class AccessConfigAddGroupsToProductModalImpl extends React.Component<AccessConfigAddGroupsToProductModalProps, State> {
  public readonly state: Readonly<State> = {
    isMutationLoading: false,
  };

  public render() {
    const {
      intl: { formatMessage },
      isOpen,
    } = this.props;

    if (!isOpen) {
      return null;
    }

    return (
      <ScreenEventSender
        event={addGroupProductAccessModalScreenEvent()}
      >
        <GroupsPickerModal
          isOpen={this.props.isOpen}
          cloudId={this.props.cloudId}
          title={formatMessage(messages.title, { productName: this.props.productName })}
          inputLabelText={formatMessage(messages.inputLabelText)}
          placeholderText={formatMessage(messages.placeholderText)}
          renderFooter={this.renderFooter}
          excludedGroupIds={this.props.excludedGroupIds}
          onDialogDismissed={this.onDialogDismissed}
          isExposingScimGroups={true}
        />
      </ScreenEventSender>
    );
  }

  private renderFooter = (getSelectedGroups: () => Group[]) => {
    const { formatMessage } = this.props.intl;
    const selectedGroups = getSelectedGroups();

    const addAccessAnalytics = this.props.productType === 'USE' ? addGroupToProductUIEventData : addGroupToAdminProductUIEventData;
    const cancelAccessAnalytics = this.props.productType === 'USE' ? cancelAddGroupToProductUIEventData : cancelAddGroupToAdminProductUIEventData;

    return (
      <AkButtonGroup>
        <AnalyticsButton
          appearance="primary"
          onClick={this.onAddButtonClicked(selectedGroups)}
          analyticsData={addAccessAnalytics({
            productKey: this.props.productId,
            groupsAddedNumber: selectedGroups.length,
          })}
          isDisabled={!selectedGroups.length}
          isLoading={!!(this.state.isMutationLoading && selectedGroups.length)}
        >
          {formatMessage(messages.addButton)}
        </AnalyticsButton>
        <AnalyticsButton
          appearance="subtle-link"
          onClick={this.onDialogDismissed}
          analyticsData={cancelAccessAnalytics({
            productKey: this.props.productId,
          })}
        >
          {formatMessage(messages.cancelButton)}
        </AnalyticsButton>
      </AkButtonGroup>
    );
  };

  private onAddButtonClicked = (selectedGroups: Group[]) => () => {
    if (!this.props.mutate) {
      return;
    }

    this.setState({ isMutationLoading: true });

    this.props.mutate({
      variables: {
        id: this.props.cloudId,
        input: {
          productId: this.props.productId,
          productType: this.props.productType,
          groups: selectedGroups.map(group => group.id),
        },
      },
      update: (store) => {
        optimisticallyAddGroup(store, this.props.productId, this.props.productType, selectedGroups);
      },
    }).then(() => {
      this.setState({ isMutationLoading: false });

      this.props.analyticsClient.sendTrackEvent({
        cloudId: this.props.cloudId,
        data: addGroupTrackEventData({
          productKey: this.props.productId,
          groupsAddedCount: selectedGroups.length,
          productAccessType: this.props.productType === 'USE' ? 'productAccess' : 'adminAccess',
        }),
      });

      this.props.showFlag({
        autoDismiss: true,
        icon: createSuccessIcon(),
        id: `access-config.add.groups.to.product.success.flag.${Date.now()}`,
        title: this.props.intl.formatMessage(messages.successFlagTitle),
        description: this.props.intl.formatMessage(messages.successFlagDescription, { productName: this.props.productName, groups: selectedGroups.length, groupname: selectedGroups[0].displayName }),
      });

      this.onDialogDismissed();
    }).catch(() => {
      this.onDialogDismissed();

      this.setState({ isMutationLoading: false });

      this.props.showFlag({
        autoDismiss: true,
        icon: createErrorIcon(),
        id: `access-config.add.groups.to.product.error.flag.${Date.now()}`,
        title: this.props.intl.formatMessage(messages.errorFlagTitle),
      });
    });
  };

  private onDialogDismissed = () => {
    if (this.props.onDialogDismissed) {
      this.props.onDialogDismissed();
    }
  };
}

const withMutation = graphql<AccessConfigAddGroupsToProductModalProps, AddGroupsAccessToProductMutation, AddGroupsAccessToProductMutationVariables>(addGroupsAccessToProductMutation);

export const AccessConfigAddGroupsToProductModal = withAnalyticsClient(
  withFlag(
    injectIntl(
      withMutation(
        AccessConfigAddGroupsToProductModalImpl,
      ),
    ),
  ),
);
