import styled from 'styled-components';

import { gridSize as akGridSize } from '@atlaskit/theme';

export const DropdownContainer = styled.div`
  display: block;
  float:right;
`;

export const OptionsHeader = styled.div`
  display: block;
  text-align: right;
  float:right;
  padding-right: ${akGridSize};
`;
