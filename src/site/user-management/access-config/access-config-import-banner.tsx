import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import AkSectionMessage from '@atlaskit/section-message';
import { gridSize as akGridSize } from '@atlaskit/theme';

import { AccessConfigImportModal } from './access-config-import-modal';
import { Group, ProductId } from './access-config-type';

export const messages = defineMessages({
  title: {
    id: 'user-management.site-settings.access-configuration.import.banner.title',
    defaultMessage: 'You need to review groups imported from server',
  },
  description: {
    id: 'user-management.site-settings.access-configuration.import.banner.description',
    defaultMessage: `Imported groups won't have access to products and/or administration until you approve it. Alternatively,
    you can reject a group's access - it will then be removed from the product's access table, but will remain in your groups list.`,
  },
  link: {
    id: 'user-management.site-settings.access-configuration.import.banner.link',
    defaultMessage: `Review imported groups`,
  },
});

interface State {
  isModalOpen: boolean;
}

interface OwnProps {
  cloudId: string;
  useGroups: Map<ProductId, Group[]>;
}

export const PanelWrapper = styled.div`
  padding-bottom: ${akGridSize() * 3}px;
`;

export class AccessConfigImportBannerImpl extends React.Component<OwnProps & InjectedIntlProps, State> {

  public state: Readonly<State> = {
    isModalOpen: false,
  };

  public render() {
    const { formatMessage } = this.props.intl;

    return (
      <PanelWrapper>
        <AkSectionMessage
          title={formatMessage(messages.title)}
          appearance="warning"
          actions={[{
            onClick: this.toggleModal,
            text: formatMessage(messages.link),
          }]}
        >
          {formatMessage(messages.description)}
        </AkSectionMessage>
        <AccessConfigImportModal
          useGroups={this.props.useGroups}
          cloudId={this.props.cloudId}
          onClose={this.toggleModal}
          isOpen={this.state.isModalOpen}
        />
      </PanelWrapper>
    );
  }

  private toggleModal = (): void => this.setState({ isModalOpen: !this.state.isModalOpen });
}

export const AccessConfigImportBanner = injectIntl(AccessConfigImportBannerImpl);
