import { DataProxy } from 'apollo-cache';

import { Group as PickerGroup } from 'common/picker/group';

import { AccessConfigQuery, ProductPermission, ProductType } from '../../../schema/schema-types';
import { ConfigType, ProductId } from './access-config-type';
import AccessConfig from './access-config.query.graphql';

type ProductGroupsConfig = AccessConfigQuery['currentSite']['groupsAdminAccessConfig'][0] | AccessConfigQuery['currentSite']['groupsUseAccessConfig'][0];

function readAccessConfigQuery(store: DataProxy) {
  try {
    return store.readQuery({
      query: AccessConfig,
    }) as AccessConfigQuery;
  } catch (e) {
    return undefined;
  }
}

export function optimisticallyUpdateImportedGroup(store: DataProxy, action: 'grant' | 'revoke', productId: ProductId, groupId: string) {

  const accessConfigResult = readAccessConfigQuery(store);

  if (!accessConfigResult) {
    return;
  }

  function revokeAccess(config): ProductGroupsConfig {

    if (productId !== config.product.productId) {
      return config;
    }

    const newGroups = config.groups.filter(group => group.id !== groupId);

    return {
      ...config,
      groups: newGroups,
    };
  }

  function grantAccess(config): ProductGroupsConfig {
    if (productId !== config.product.productId) {
      return config;
    }

    return {
      ...config,
      groups: config.groups.map(group => group.id === groupId ? { ... group, requiresApproval: false } : group),
    };
  }

  const { currentSite: { groupsUseAccessConfig } } = accessConfigResult;

  const newGroupsUseAccessConfig = action === 'grant' ? groupsUseAccessConfig.map(grantAccess) : groupsUseAccessConfig.map(revokeAccess);

  store.writeQuery({
    query: AccessConfig,
    data: {
      currentSite: {
        ...accessConfigResult.currentSite,
        groupsUseAccessConfig: newGroupsUseAccessConfig,
      },
    },
  });
}

export function optimisticallyUpdateDefaultGroup(store: DataProxy, productId: ProductId, groupId: string, isDefault: boolean) {
  const accessConfigResult = readAccessConfigQuery(store);

  if (!accessConfigResult) {
    return;
  }

  function updateDefaultGroup(config): ProductGroupsConfig {
    if (productId !== config.product.productId) {
      return config;
    }

    return {
      ...config,
      groups: config.groups.map(group => group.id === groupId ? { ... group, default: !isDefault } : group),
    };
  }

  const { currentSite: { groupsUseAccessConfig } } = accessConfigResult;

  const newGroupsUseAccessConfig = groupsUseAccessConfig.map(updateDefaultGroup);

  store.writeQuery({
    query: AccessConfig,
    data: {
      currentSite: {
        ...accessConfigResult.currentSite,
        groupsUseAccessConfig: newGroupsUseAccessConfig,
      },
    },
  });
}

export function optimisticallyRemoveGroup(store: DataProxy, configType: ConfigType, productId: ProductId, groupId: string) {

  const accessConfigResult = readAccessConfigQuery(store);

  if (!accessConfigResult) {
    return;
  }

  function removeGroup(config): ProductGroupsConfig {

    if (productId !== config.product.productId) {
      return config;
    }

    const newGroups = config.groups.filter(group => group.id !== groupId);

    return {
      ...config,
      groups: newGroups,
    };
  }

  const { currentSite: { groupsAdminAccessConfig, groupsUseAccessConfig } } = accessConfigResult;

  const newGroupsUseAccessConfig = configType === 'productAccess' ? groupsUseAccessConfig.map(removeGroup) : groupsUseAccessConfig;
  const newGroupsAdminAccessConfig = configType === 'adminAccess' ? groupsAdminAccessConfig.map(removeGroup) : groupsAdminAccessConfig;

  store.writeQuery({
    query: AccessConfig,
    data: {
      currentSite: {
        ...accessConfigResult.currentSite,
        groupsUseAccessConfig: newGroupsUseAccessConfig,
        groupsAdminAccessConfig: newGroupsAdminAccessConfig,
      },
    },
  });
}

export function optimisticallyAddGroup(store: DataProxy, productId: ProductId, productType: ProductType, groups: PickerGroup[]): void {
  const accessConfigPageResult = readAccessConfigQuery(store);

  if (!accessConfigPageResult) {
    return;
  }

  let { currentSite: { groupsAdminAccessConfig, groupsUseAccessConfig } } = accessConfigPageResult;

  function updateGroupsForProducts(config: ProductGroupsConfig): ProductGroupsConfig {
    if (config.product.productId !== productId) {
      return config;
    }

    const currentGroups = config.groups;
    const currentGroupIds = currentGroups.map(g => g.id);
    const newGroups = groups.filter(group => !currentGroupIds.includes(group.id));

    return {
      ...config,
      groups: [
        ...currentGroups,
        ...newGroups.map(pickerGroup => ({
          id: pickerGroup.id,
          name: pickerGroup.displayName,
          default: false,
          productPermission: [(productType === 'ADMIN') ? 'MANAGE' : 'WRITE'] as ProductPermission[],
          requiresApproval: false,
          __typename: 'GroupAccess',
        })),
      ],
    };
  }

  if (productType === 'ADMIN') {
    groupsAdminAccessConfig = groupsAdminAccessConfig.map(updateGroupsForProducts);
  } else if (productType === 'USE') {
    groupsUseAccessConfig = groupsUseAccessConfig.map(updateGroupsForProducts);
  }

  store.writeQuery({
    query: AccessConfig,
    data: {
      currentSite:  {
        ...accessConfigPageResult.currentSite,
        groupsUseAccessConfig,
        groupsAdminAccessConfig,
      },
    },
  });
}
