import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import { PageLayout } from 'common/page-layout';

import { JsdExportButton } from './jsd-export-button';
import { JsdPageLayout } from './jsd-page-layout';

const getWrapper = (children?: React.ReactChild) => {
  return shallow(<JsdPageLayout children={children} />);
};

describe('JSD page layout', () => {
  it('should render children', () => {
    const DummyComp: React.SFC = () => <div />;
    const wrapper = getWrapper(<DummyComp />);

    expect(wrapper.find(DummyComp).exists()).to.equal(true);
  });

  it('should render a title', () => {
    const pageLayout = getWrapper().find(PageLayout);
    expect(pageLayout.prop('title')).to.not.equal(undefined);
    expect(pageLayout.prop('title')).to.not.equal(null);
  });

  it('should render a description', () => {
    const pageLayout = getWrapper().find(PageLayout);
    expect(pageLayout.prop('description')).to.not.equal(undefined);
    expect(pageLayout.prop('description')).to.not.equal(null);
  });

  it('should render an export button in the actions', () => {
    const wrapper = getWrapper();
    const action = wrapper.find(PageLayout).prop('action') as React.ReactElement<any>;

    expect(shallow(action).find(JsdExportButton).exists()).to.equal(true);
  });
});
