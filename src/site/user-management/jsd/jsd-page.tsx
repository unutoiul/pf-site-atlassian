import { parse } from 'query-string';
import * as React from 'react';
import { InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';

import { FlagProps, withFlag } from 'common/flag';

import { JsdPageLayout } from './jsd-page-layout';
import { JsdTable } from './jsd-table';
import { ActiveStatusFilter } from './jsd-table-filters/jsd-table-active-filter';
import { JsdTableFilter } from './jsd-table-filters/jsd-table-filter';

interface SiteParams {
  cloudId: string;
}

type Props = FlagProps & InjectedIntlProps & RouteComponentProps<SiteParams>;

interface State {
  currentPage: number;
  displayNameFilter: string;
  activeStatusFilter: ActiveStatusFilter;
}

export class JsdPageImpl extends React.Component<Props, State> {
  public readonly state: Readonly<State>;

  public constructor(props) {
    super(props);

    this.state = {
      currentPage: 1,
      displayNameFilter: this.getQueryParams().displayNameFilter,
      activeStatusFilter: this.getQueryParams().activeStatusFilter,
    };
  }

  public componentDidMount() {
    this.setState({
      displayNameFilter: this.getQueryParams().displayNameFilter,
      activeStatusFilter: this.getQueryParams().activeStatusFilter,
    });
  }

  public render() {
    return (
      <JsdPageLayout>
        <JsdTableFilter
          activeStatusFilter={this.state.activeStatusFilter}
          displayNameFilter={this.state.displayNameFilter}
          onActiveStatusFilterChanged={this.onActiveStatusFilterChanged}
          onDisplayNameFilterChanged={this.onDisplayNameFilterChanged}
        />
        <JsdTable
          activeStatusFilter={this.state.activeStatusFilter}
          displayNameFilter={this.state.displayNameFilter}
          currentPage={this.state.currentPage}
          onSetPage={this.onSetPage}
          cloudId={this.props.match.params.cloudId}
        />
      </JsdPageLayout>
    );
  }

  private getQueryParams = () => {
    return {
      displayNameFilter: parse(this.props.location.search).filter,
      activeStatusFilter: parse(this.props.location.search)['active-filter'] || 'all',
    };
  }

  private onSetPage = (page: number): void => {
    this.setState({ currentPage: page });
  };

  private onActiveStatusFilterChanged = (activeStatus: ActiveStatusFilter) => {
    this.setState({
      currentPage: 1,
      activeStatusFilter: activeStatus,
    });
  };

  private onDisplayNameFilterChanged = (displayName: string) => {
    this.setState({
      currentPage: 1,
      displayNameFilter: displayName,
    });
  };
}

export const JsdPage = withRouter(
  withFlag(
    injectIntl(
      JsdPageImpl,
    ),
  ),
);
