import * as React from 'react';
import {
  defineMessages,
  FormattedMessage,
} from 'react-intl';
import styled from 'styled-components';

import { ButtonGroup as AkButtonGroup } from '@atlaskit/button';
import { gridSize as akGridSize } from '@atlaskit/theme';

import { PageLayout } from 'common/page-layout';

import { JsdExportButton } from './jsd-export-button';

const messages = defineMessages({
  title: {
    id: 'jsd.page.layout.title',
    description: 'Page title used for the JSD customer list page',
    defaultMessage: 'Portal customers',
  },
  description: {
    id: 'jsd.page.layout.description',
    description: 'Page description used for the JSD customer list page',
    defaultMessage: `Portal only customers can't access your site directly. They can only log in to your Jira Service Desk customer portals. To create portal only customers, add them to the Customers list in a service desk project. {viewServiceDeskLink}.`,
  },
  viewServiceDeskLink: {
    id: 'jsd.page.layout.viewServiceDeskLink',
    description: 'Link to view service desk',
    defaultMessage: 'View service desk projects',
  },
});

const DescriptionWrapper = styled.div`
  max-width: ${akGridSize() * 65}px;
`;

export class JsdPageLayout extends React.Component {
  public render() {
    return (
      <PageLayout
        isFullWidth={true}
        title={<FormattedMessage {...messages.title} />}
        description={(
          <DescriptionWrapper>
            <FormattedMessage
              {...messages.description}
              values={{
                viewServiceDeskLink: (
                  <a href="/secure/BrowseProjects.jspa?selectedCategory=all&selectedProjectType=service_desk">
                    <FormattedMessage {...messages.viewServiceDeskLink} />
                  </a>
                ),
              }}
            />
          </DescriptionWrapper>
        )}
        action={(
          <AkButtonGroup>
            <JsdExportButton />
          </AkButtonGroup>
        )}
      >
        {this.props.children}
      </PageLayout>
    );
  }
}
