import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import { createMockIntlContext, createMockIntlProp, noop } from '../../../utilities/testing';
import { JsdPageImpl } from './jsd-page';
import { JsdPageLayout } from './jsd-page-layout';
import { JsdTable } from './jsd-table';
import { JsdTableFilter } from './jsd-table-filters/jsd-table-filter';

const mockCloudId = 'corgi-in-your-cloud';

const getWrapper = () => {
  return shallow<{}, { currentPage: number }>((
    <JsdPageImpl
      showFlag={noop}
      hideFlag={noop}
      match={{
        url: '',
        path: '',
        isExact: true,
        params: { cloudId: mockCloudId },
      }}
      location={{} as any}
      history={{} as any}
      intl={createMockIntlProp()}
    />), createMockIntlContext(),
  );
};

describe('JSD page', () => {
  it('should render the jsd page layout', () => {
    const wrapper = getWrapper();
    expect(wrapper.find(JsdPageLayout).exists()).to.equal(true);
  });

  it('should render the jsd table filter', () => {
    const wrapper = getWrapper();
    expect(wrapper.find(JsdTableFilter).exists()).to.equal(true);
  });

  it('should seed the filters with values from the location query parameters', () => {
    const wrapper = shallow(
      <JsdPageImpl
        showFlag={noop}
        hideFlag={noop}
        match={{
          url: '',
          path: '',
          isExact: true,
          params: { cloudId: mockCloudId },
        }}
        location={{
          search: '?filter=josh&active-filter=all',
          ...{} as any,
        }}
        history={{} as any}
        intl={createMockIntlProp()}
      />,
    );

    expect(wrapper.find(JsdTableFilter).props().activeStatusFilter).to.equal('all');
    expect(wrapper.find(JsdTableFilter).props().displayNameFilter).to.equal('josh');
  });

  it('should pass the active filter back to the JsdTableFilter when onActiveStatusFilterChanged is called', () => {
    const wrapper = shallow(
      <JsdPageImpl
        showFlag={noop}
        hideFlag={noop}
        match={{
          url: '',
          path: '',
          isExact: true,
          params: { cloudId: mockCloudId },
        }}
        location={{} as any}
        history={{} as any}
        intl={createMockIntlProp()}
      />,
    );

    wrapper.find(JsdTableFilter).props().onActiveStatusFilterChanged('inactive');
    wrapper.update();

    expect(wrapper.find(JsdTableFilter).props().activeStatusFilter).to.equal('inactive');
    expect(wrapper.state('currentPage')).to.equal(1);
  });

  it('should pass the display name filter back to the JsdTableFilter when onDisplayNameFilterChanged is called', () => {
    const wrapper = shallow(
      <JsdPageImpl
        showFlag={noop}
        hideFlag={noop}
        match={{
          url: '',
          path: '',
          isExact: true,
          params: { cloudId: mockCloudId },
        }}
        location={{} as any}
        history={{} as any}
        intl={createMockIntlProp()}
      />,
    );

    wrapper.find(JsdTableFilter).props().onDisplayNameFilterChanged('Nicolas Cage');
    wrapper.update();

    expect(wrapper.find(JsdTableFilter).props().displayNameFilter).to.equal('Nicolas Cage');
    expect(wrapper.state('currentPage')).to.equal(1);
  });

  describe('jsd table', () => {
    it('should start with the current page of 1', () => {
      const wrapper = getWrapper();
      const table = wrapper.find(JsdTable);
      expect(table.prop('currentPage')).to.equal(1);
    });

    it('should update the state onSetPage changes', () => {
      const wrapper = getWrapper();
      const table = wrapper.find(JsdTable);

      expect(table.prop('onSetPage')).to.be.a('function');

      expect(wrapper.state().currentPage).to.equal(1);
      table.prop('onSetPage')(3);
      expect(wrapper.state().currentPage).to.equal(3);
    });

    it('should have the current cloudId', () => {
      const wrapper = getWrapper();
      const table = wrapper.find(JsdTable);
      expect(table.prop('cloudId')).to.equal(mockCloudId);
    });
  });
});
