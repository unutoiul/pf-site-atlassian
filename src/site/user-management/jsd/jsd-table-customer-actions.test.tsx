import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';

import { FlagProvider } from 'common/flag';

import { createApolloClient } from '../../../apollo-client';
import { noop } from '../../../utilities/testing';
import { JsdTableCustomerActions } from './jsd-table-customer-actions';
import { JsdChangePasswordModal } from './modals/jsd-change-password-modal';
import { JsdEditNameModal } from './modals/jsd-edit-name-modal';
import { JsdGrantRevokeModal } from './modals/jsd-grant-revoke-modal';
import { JsdMigrateAccountModal } from './modals/jsd-migrate-account-modal';

const getWrapper = () => {
  return mount((
    <IntlProvider locale="en">
      <ApolloProvider client={createApolloClient()}>
        <FlagProvider>
          <JsdTableCustomerActions
            cloudId="DUMMY-CLOUD-ID"
            directoryId="DUMMY-DIRECTORY-ID"
            accountId="DUMMY-ACCOUNT-ID"
            accountDisplayName="Four Tet"
            accountActive={true}
            activeStatusFilter="all"
            onCustomerDeleted={noop}
            start={1}
          />
        </FlagProvider>
      </ApolloProvider>
    </IntlProvider>
  ));
};

describe('JSD table customer actions', () => {
  describe('Edit name modal', () => {
    it('should render the modal', () => {
      const wrapper = getWrapper();
      expect(wrapper.find(JsdEditNameModal).exists()).to.equal(true);
    });
  });

  describe('Change password modal', () => {
    it('should render the modal', () => {
      const wrapper = getWrapper();
      expect(wrapper.find(JsdChangePasswordModal).exists()).to.equal(true);
    });
  });

  describe('Migrate account modal', () => {
    it('should render the modal', () => {
      const wrapper = getWrapper();
      expect(wrapper.find(JsdMigrateAccountModal).exists()).to.equal(true);
    });
  });

  describe('Grant revoke modal', () => {
    it('should render the modal', () => {
      const wrapper = getWrapper();
      expect(wrapper.find(JsdGrantRevokeModal).exists()).to.equal(true);
    });
  });
});
