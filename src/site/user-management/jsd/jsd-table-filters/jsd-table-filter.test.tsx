import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import { createMockIntlProp } from '../../../../utilities/testing';
import { JsdTableActiveFilter } from './jsd-table-active-filter';
import { JsdTableFilterImpl } from './jsd-table-filter';
import { JsdTableSearchFilter } from './jsd-table-search-filter';

describe('JsdTableFilter', () => {
  it('should pass the filter props', () => {
    const activeStatusChanged = sinon.stub();
    const displayNameChanged = sinon.stub();

    const wrapper = shallow(
      <JsdTableFilterImpl
        activeStatusFilter="active"
        displayNameFilter="Nicolas Cage"
        onActiveStatusFilterChanged={activeStatusChanged}
        onDisplayNameFilterChanged={displayNameChanged}
        intl={createMockIntlProp()}
      />,
    );

    // tslint:disable-next-line no-unbound-method
    expect(wrapper.find(JsdTableSearchFilter).props().onSet).to.equal(displayNameChanged);
    // tslint:disable-next-line no-unbound-method
    expect(wrapper.find(JsdTableActiveFilter).props().onChanged).to.equal(activeStatusChanged);
  });
});
