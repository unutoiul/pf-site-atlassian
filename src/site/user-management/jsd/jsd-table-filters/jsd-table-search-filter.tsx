import debounce from 'lodash.debounce';
import * as React from 'react';

import { SearchInput } from 'common/search-input';

interface Props {
  selectedDisplayName?: string;
  onSet(displayName?: string): void;
}

export class JsdTableSearchFilter extends React.Component<Props> {
  private searchByDisplayName = debounce((displayName?: string) => {
    this.props.onSet(displayName);
  }, 500);

  public render() {
    return (
      <SearchInput
        initialValue={this.props.selectedDisplayName}
        onChange={this.onSearch}
      />
    );
  }

  private onSearch = (e: React.FormEvent<HTMLInputElement>) => {
    let displayName = (e.target as HTMLInputElement).value;
    if (displayName && displayName.length) {
      displayName = displayName.trim();
    }
    this.searchByDisplayName(displayName === '' ? undefined : displayName);
  };
}
