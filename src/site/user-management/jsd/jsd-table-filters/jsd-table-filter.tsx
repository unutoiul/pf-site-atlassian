import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import { gridSize as akGridSize } from '@atlaskit/theme';

import { ActiveStatusFilter, JsdTableActiveFilter } from './jsd-table-active-filter';
import { JsdTableSearchFilter } from './jsd-table-search-filter';

export const Container = styled.div`
  display: flex;
  padding-bottom: ${akGridSize() * 3}px;
`;

export const Label = styled.span`
  padding: ${akGridSize() * 1.8}px ${akGridSize() * 2}px 0 ${akGridSize() * 2}px;
`;

export const DropdownWrapper = styled.div`
  padding-top: ${akGridSize}px;
`;

const messages = defineMessages({
  filterBy: {
    id: 'jsd.page.table.filter.by',
    description: 'Tagline that comes before the different filter options for the customers list table.',
    defaultMessage: 'Filter by:',
  },
});

interface OwnProps {
  activeStatusFilter: ActiveStatusFilter;
  displayNameFilter?: string;
  onActiveStatusFilterChanged(activeStatus: ActiveStatusFilter): void;
  onDisplayNameFilterChanged(displayName?: string): void;
}

export class JsdTableFilterImpl extends React.Component<OwnProps & InjectedIntlProps> {
  public render() {
    return (
      <Container>
        <JsdTableSearchFilter
          selectedDisplayName={this.props.displayNameFilter}
          onSet={this.props.onDisplayNameFilterChanged}
        />
        <Label>{this.props.intl.formatMessage(messages.filterBy)}</Label>
        <DropdownWrapper>
          <JsdTableActiveFilter
            onChanged={this.props.onActiveStatusFilterChanged}
            activeStatusFilter={this.props.activeStatusFilter}
          />
        </DropdownWrapper>
      </Container>
    );
  }
}

export const JsdTableFilter = injectIntl<OwnProps>(JsdTableFilterImpl);
