import * as React from 'react';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';

import { MultiSelectDropdown } from 'common/multi-select-dropdown';

const messages = defineMessages({
  all: {
    id: 'jsd.table.active.filter.all',
    description: 'Filter used to show all customers in the list table.',
    defaultMessage: 'All customers',
  },
  active: {
    id: 'jsd.table.active.filter.active',
    description: 'Filter used to show active customers in the list table.',
    defaultMessage: 'Active customers',
  },
  inactive: {
    id: 'jsd.table.active.filter.inactive',
    description: 'Filter used to show inactive customers in the list table.',
    defaultMessage: 'Inactive customers',
  },
});

export type ActiveStatusFilter = 'all' | 'active' | 'inactive';

interface OwnProps {
  activeStatusFilter: ActiveStatusFilter;
  onChanged(value: ActiveStatusFilter): void;
}

export class JsdTableActiveFilterImpl extends React.Component<InjectedIntlProps & OwnProps> {
  public render() {
    const allText = (
      <FormattedMessage {...messages.all} />
    );

    return (
      <MultiSelectDropdown
        onChanged={this.onChanged}
        parentItem={{
          id: 'all',
          label: this.props.intl.formatMessage(messages.all),
          isSelected: this.props.activeStatusFilter === 'all',
          isDisabled: this.props.activeStatusFilter === 'all',
        }}
        placeholder={allText}
        allItems={[{
          id: 'active',
          label: this.props.intl.formatMessage(messages.active),
          isSelected: this.props.activeStatusFilter === 'active',
        }, {
          id: 'inactive',
          label: this.props.intl.formatMessage(messages.inactive),
          isSelected: this.props.activeStatusFilter === 'inactive',
        }]}
      />
    );
  }

  private onChanged = (_e, { id, newSelected }) => {
    if (!newSelected) {
      this.props.onChanged('all');

      return;
    }

    if (id === 'all') {
      this.props.onChanged('all');

      return;
    }

    if (id !== 'active' && id !== 'inactive') {
      return;
    }

    this.props.onChanged(id);
  }
}

export const JsdTableActiveFilter = injectIntl<OwnProps>(JsdTableActiveFilterImpl);
