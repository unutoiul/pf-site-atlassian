import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import { SearchInput } from 'common/search-input';

import { waitUntil } from '../../../../utilities/testing';
import { JsdTableSearchFilter } from './jsd-table-search-filter';

describe('JsdTableSearchFilter', () => {
  it('should call onSet when the SearchInput changes', async () => {
    const spy = sinon.spy();
    const wrapper = shallow(
      <JsdTableSearchFilter
        selectedDisplayName="Nicolas Cage"
        onSet={spy}
      />,
    );

    wrapper.find(SearchInput).props().onChange!({
      target: {
        value: 'Sean Bean',
      },
    } as any);

    await waitUntil(() => spy.callCount > 0);

    expect(spy.calledWith('Sean Bean')).to.equal(true);
  });

  it('should call onSet with undefined the SearchInput changes to the empty string', async () => {
    const spy = sinon.spy();
    const wrapper = shallow(
      <JsdTableSearchFilter
        selectedDisplayName="Nicolas Cage"
        onSet={spy}
      />,
    );

    wrapper.find(SearchInput).props().onChange!({
      target: {
        value: '',
      },
    } as any);

    await waitUntil(() => spy.callCount > 0);

    expect(spy.calledWith(undefined)).to.equal(true);
  });

  it('should trim the input', async () => {
    const spy = sinon.spy();
    const wrapper = shallow(
      <JsdTableSearchFilter
        selectedDisplayName="Nicolas Cage"
        onSet={spy}
      />,
    );

    wrapper.find(SearchInput).props().onChange!({
      target: {
        value: ' Thom Yorke ',
      },
    } as any);

    await waitUntil(() => spy.callCount > 0);

    expect(spy.calledWith('Thom Yorke')).to.equal(true);
  });
});
