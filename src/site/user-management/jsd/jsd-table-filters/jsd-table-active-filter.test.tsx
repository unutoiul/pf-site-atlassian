import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import { MultiSelectDropdown } from 'common/multi-select-dropdown';

import { createMockIntlProp, noop } from '../../../../utilities/testing';
import { JsdTableActiveFilterImpl } from './jsd-table-active-filter';

describe('JsdTableActiveFilter', () => {
  it('should check the only the parent checkbox when activeStatus=all is given as a prop', () => {
    const wrapper = shallow(
      <JsdTableActiveFilterImpl
        intl={createMockIntlProp()}
        activeStatusFilter="all"
        onChanged={noop}
      />,
    );

    expect(wrapper.find(MultiSelectDropdown).props().parentItem!.isSelected).to.equal(true);
    expect(wrapper.find(MultiSelectDropdown).props().allItems![0].isSelected).to.equal(false);
    expect(wrapper.find(MultiSelectDropdown).props().allItems![1].isSelected).to.equal(false);
  });

  it('should check the only the active checkbox when activeStatus=active is given as a prop', () => {
    const wrapper = shallow(
      <JsdTableActiveFilterImpl
        intl={createMockIntlProp()}
        activeStatusFilter="active"
        onChanged={noop}
      />,
    );

    expect(wrapper.find(MultiSelectDropdown).props().parentItem!.isSelected).to.equal(false);
    expect(wrapper.find(MultiSelectDropdown).props().allItems![0].isSelected).to.equal(true);
    expect(wrapper.find(MultiSelectDropdown).props().allItems![1].isSelected).to.equal(false);
  });

  it('should check the only the inactive checkbox when activeStatus=inactive is given as a prop', () => {
    const wrapper = shallow(
      <JsdTableActiveFilterImpl
        intl={createMockIntlProp()}
        activeStatusFilter="inactive"
        onChanged={noop}
      />,
    );

    expect(wrapper.find(MultiSelectDropdown).props().parentItem!.isSelected).to.equal(false);
    expect(wrapper.find(MultiSelectDropdown).props().allItems![0].isSelected).to.equal(false);
    expect(wrapper.find(MultiSelectDropdown).props().allItems![1].isSelected).to.equal(true);
  });

  it('should disable the "all" checkbox when all is selected', () => {
    const wrapper = shallow(
      <JsdTableActiveFilterImpl
        intl={createMockIntlProp()}
        activeStatusFilter="all"
        onChanged={noop}
      />,
    );

    expect(wrapper.find(MultiSelectDropdown).props().parentItem!.isDisabled).to.equal(true);
  });

  it('should call the onChange prop with "all" if the MultiSelectDropdown changes to the all state', () => {
    const spy = sinon.spy();
    const wrapper = shallow(
      <JsdTableActiveFilterImpl
        intl={createMockIntlProp()}
        activeStatusFilter="inactive"
        onChanged={spy}
      />,
    );

    wrapper.find(MultiSelectDropdown).props().onChanged!({} as any, { id: 'all', newSelected: true, oldSelected: false });

    expect(spy.calledWith('all')).to.equal(true);
  });

  it('should call the onChange prop with "active" if the MultiSelectDropdown changes to the active state', () => {
    const spy = sinon.spy();
    const wrapper = shallow(
      <JsdTableActiveFilterImpl
        intl={createMockIntlProp()}
        activeStatusFilter="inactive"
        onChanged={spy}
      />,
    );

    wrapper.find(MultiSelectDropdown).props().onChanged!({} as any, { id: 'active', newSelected: true, oldSelected: false });

    expect(spy.calledWith('active')).to.equal(true);
  });

  it('should call the onChange prop with "inactive" if the MultiSelectDropdown changes to the inactive state', () => {
    const spy = sinon.spy();
    const wrapper = shallow(
      <JsdTableActiveFilterImpl
        intl={createMockIntlProp()}
        activeStatusFilter="inactive"
        onChanged={spy}
      />,
    );

    wrapper.find(MultiSelectDropdown).props().onChanged!({} as any, { id: 'inactive', newSelected: true, oldSelected: false });

    expect(spy.calledWith('inactive')).to.equal(true);
  });

  it('should call the onChange prop with "all" if the MultiSelectDropdown turns off the active state', () => {
    const spy = sinon.spy();
    const wrapper = shallow(
      <JsdTableActiveFilterImpl
        intl={createMockIntlProp()}
        activeStatusFilter="active"
        onChanged={spy}
      />,
    );

    wrapper.find(MultiSelectDropdown).props().onChanged!({} as any, { id: 'active', newSelected: false, oldSelected: true });

    expect(spy.calledWith('all')).to.equal(true);
  });

  it('should call the onChange prop with "all" if the MultiSelectDropdown turns off the inactive state', () => {
    const spy = sinon.spy();
    const wrapper = shallow(
      <JsdTableActiveFilterImpl
        intl={createMockIntlProp()}
        activeStatusFilter="inactive"
        onChanged={spy}
      />,
    );

    wrapper.find(MultiSelectDropdown).props().onChanged!({} as any, { id: 'inactive', newSelected: false, oldSelected: true });

    expect(spy.calledWith('all')).to.equal(true);
  });
});
