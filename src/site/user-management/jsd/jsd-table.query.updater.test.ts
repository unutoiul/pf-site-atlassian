import { expect } from 'chai';
import * as sinon from 'sinon';

import { JsdTableQuery } from '../../../schema/schema-types';
import { optimisticallyUpdateCustomer } from './jsd-table.query.updater';

describe('Jsd table query updater', () => {
  let writeQuerySpy: sinon.SinonSpy;

  const sandbox = sinon.sandbox.create();

  beforeEach(() => {
    writeQuerySpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  const DUMMY_CLOUD_ID = 'DUMMY-CLOUD-ID';
  const DUMMY_DIRECTORY_ID = 'DUMMY-DIRECTORY-ID';

  const generateStore = (customers?: any[]) => {
    return {
      readQuery: sinon.stub().returns({
        site: {
          __typename: '',
          jsd: {
            __typename: '',
            directory: {
              __typename: '',
              id: DUMMY_DIRECTORY_ID,
            },
            customers: {
              __typename: '',
              hasMoreResults: false,
              customers: customers ? customers : [
                {
                  id: '1',
                  username: '1',
                  email: 'fourtet@gmail.com',
                  active: true,
                  displayName: 'Not Four Tet',
                  lastLogin: null,
                  __typename: '',
                },
                {
                  id: '2',
                  username: '2',
                  email: 'jonhopkins@gmail.com',
                  active: true,
                  displayName: 'Not Thom Yorke',
                  lastLogin: null,
                  __typename: '',
                },
              ],
            },
          },
        },
      } as JsdTableQuery),

      writeQuery: writeQuerySpy,
    } as any;
  };

  describe('optimisticallyUpdateCustomer', () => {
    it('should update the customers name', () => {
      const store = generateStore();

      expect(writeQuerySpy.called).to.equal(false);

      optimisticallyUpdateCustomer(store, DUMMY_CLOUD_ID, 'all', '1', 1, { name: 'Four Tet' });

      expect(writeQuerySpy.called).to.equal(true);
      expect(writeQuerySpy.getCall(0).args[0].variables).to.deep.equal({
        activeStatus: 'all',
        cloudId: DUMMY_CLOUD_ID,
        count: 20,
        start: 1,
      });

      const fourTet = writeQuerySpy.getCall(0).args[0].data.site.jsd.customers.customers[0];
      expect(fourTet.displayName).to.equal('Four Tet');
    });

    it('should update the customers active status', () => {
      const store = generateStore();

      expect(writeQuerySpy.called).to.equal(false);

      optimisticallyUpdateCustomer(store, DUMMY_CLOUD_ID, 'all', '1', 1, { active: false });

      expect(writeQuerySpy.called).to.equal(true);
      expect(writeQuerySpy.getCall(0).args[0].variables).to.deep.equal({
        activeStatus: 'all',
        cloudId: DUMMY_CLOUD_ID,
        count: 20,
        start: 1,
      });

      const fourTet = writeQuerySpy.getCall(0).args[0].data.site.jsd.customers.customers[0];
      expect(fourTet.active).to.equal(false);
    });

    it('should not update customers which are not targeted by the update', () => {
      const store = generateStore();

      optimisticallyUpdateCustomer(store, DUMMY_CLOUD_ID, 'all', '2', 1, { name: 'Thom Yorke' });

      const fourTet = writeQuerySpy.getCall(0).args[0].data.site.jsd.customers.customers[0];
      const thomYorke = writeQuerySpy.getCall(0).args[0].data.site.jsd.customers.customers[1];

      expect(thomYorke.displayName).to.equal('Thom Yorke');
      expect(fourTet.displayName).to.equal('Not Four Tet');
    });

    it(`should not update the cache if it's empty`, () => {
      const store = generateStore([]);

      expect(writeQuerySpy.called).to.equal(false);
      optimisticallyUpdateCustomer(store, DUMMY_CLOUD_ID, 'all', '2', 1, { name: 'Thom Yorke' });
      expect(writeQuerySpy.called).to.equal(false);
    });
  });
});
