import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import * as sinon from 'sinon';

import { DynamicTableStateless as AkDynamicTableStateless } from '@atlaskit/dynamic-table';
import AkEmptyState from '@atlaskit/empty-state';

import { Account } from 'common/account';

import { JsdCustomer } from '../../../schema/schema-types';
import { createMockIntlContext, createMockIntlProp, noop, waitUntil } from '../../../utilities/testing';
import { JsdTableImpl } from './jsd-table';
import { JsdTableCustomerActions } from './jsd-table-customer-actions';

describe('JSD table', () => {
  const sandbox = sinon.sandbox.create();
  const mockCloudId = 'corgi-in-your-cloud';

  let onSetPageSpy: sinon.SinonSpy;

  const generateCustomer = (active = true, lastLogin = ''): JsdCustomer & { isPlaceholder: boolean } => {
    return {
      id: '1',
      username: 'siriusmo',
      email: 'siriusmo@gmail.com',
      displayName: 'Siriusmo',
      active,
      lastLogin,
      isPlaceholder: false,
    };
  };

  const getWrapper = (data) => {
    return shallow((
      <JsdTableImpl
        currentPage={1}
        cloudId={mockCloudId}
        onSetPage={onSetPageSpy}
        intl={createMockIntlProp()}
        data={{
          refetch: sinon.stub().resolves(),
        } as any}
        displayNameFilter=""
        activeStatusFilter={'all'}
        match={{} as any}
        history={{
          replace: noop,
        } as any}
        location={{} as any}
      />
    ), {
      disableLifecycleMethods: false,
      context: createMockIntlContext().context,
    },
    ).setProps({ data });
  };

  beforeEach(() => {
    onSetPageSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('should render a fake row if the api has additional results', () => {
    const wrapper = getWrapper({
      error: false,
      loading: false,
      site: {
        jsd: {
          directory: {
            id: 'dummy-id',
          },
          customers: {
            customers: [generateCustomer(true)],
            hasMoreResults: true,
          },
        },
      },
    });

    const rows = wrapper.find(AkDynamicTableStateless).props().rows as any;
    expect(rows.length).to.equal(2);
  });

  it(`shouldn't render a fake row if the api doesn't have additional results`, () => {
    const wrapper = getWrapper({
      error: false,
      loading: false,
      site: {
        jsd: {
          directory: {
            id: 'dummy-id',
          },
          customers: {
            customers: [generateCustomer(true)],
            hasMoreResults: false,
          },
        },
      },
    });

    const rows = wrapper.find(AkDynamicTableStateless).props().rows as any;
    expect(rows.length).to.equal(1);
  });

  it('should show an empty state message on error', () => {
    const wrapper = getWrapper({ error: true });

    const emptyState = wrapper.find(AkEmptyState);
    expect(emptyState.exists()).to.equal(true);

    const header: React.ReactElement<any> = emptyState.prop('header');
    expect(header.type).to.equal(FormattedMessage);
    expect(header.props.defaultMessage).contains('Something went wrong');
  });

  it('should show an empty state message when empty', () => {
    const wrapper = getWrapper({
      error: false,
      site: {
        jsd: {
          directory: {
            id: 'dummy-id',
          },
          customers: {
            customers: [],
          },
        },
      },
    });

    const emptyView: React.ReactElement<any> = wrapper.prop('emptyView');
    expect(emptyView.type).to.equal(AkEmptyState);
  });

  it('should render 4 specific column headers', () => {
    const wrapper = getWrapper({
      error: false,
      site: {
        jsd: {
          directory: {
            id: 'dummy-id',
          },
          customers: {
            customers: [],
          },
        },
      },
    });

    const customerCol: React.ReactElement<any> = wrapper.prop('head').cells[0].content;
    expect(customerCol.props.defaultMessage).contains('Customer');

    const lastActiveCol: React.ReactElement<any> = wrapper.prop('head').cells[1].content;
    expect(lastActiveCol.props.defaultMessage).contains('Last active');

    const statusCol: React.ReactElement<any> = wrapper.prop('head').cells[2].content;
    expect(statusCol.props.defaultMessage).contains('Status');

    const actionsCol: React.ReactElement<any> = wrapper.prop('head').cells[3].content;
    expect(actionsCol.props.defaultMessage).contains('Actions');
  });

  it('should render the Customer column as an Account component', () => {
    const wrapper = getWrapper({
      error: false,
      loading: false,
      site: {
        jsd: {
          directory: {
            id: 'dummy-id',
          },
          customers: {
            customers: [generateCustomer()],
          },
        },
      },
    });

    const rows = wrapper.find(AkDynamicTableStateless).props().rows as any;
    const customerCol = shallow(<div>{rows[0].cells[0].content}</div>);

    expect(customerCol.find(Account).exists()).to.equal(true);
  });

  it('should render the Last Active column as a date for customer with lastLogin', () => {
    const wrapper = getWrapper({
      error: false,
      loading: false,
      site: {
        jsd: {
          directory: {
            id: 'dummy-id',
          },
          customers: {
            customers: [generateCustomer(true, '1543351035256')],
          },
        },
      },
    });

    const rows = wrapper.find(AkDynamicTableStateless).props().rows as any;
    const customerCol = shallow(<div>{rows[0].cells[1].content}</div>);

    expect(customerCol.text().length).to.be.greaterThan(1);
    expect(customerCol.find(FormattedMessage).exists()).equals(false);
  });

  it(`should render the Last Active column as a placeholder message for customer that hasn't logged in`, () => {
    const wrapper = getWrapper({
      error: false,
      loading: false,
      site: {
        jsd: {
          directory: {
            id: 'dummy-id',
          },
          customers: {
            customers: [generateCustomer(true, '')],
          },
        },
      },
    });

    const rows = wrapper.find(AkDynamicTableStateless).props().rows as any;
    const customerCol = shallow(<div>{rows[0].cells[1].content}</div>);

    expect(customerCol.find(FormattedMessage).exists()).to.equal(true);
  });

  it('should render the Status column as an active message when the customer is active', () => {
    const wrapper = getWrapper({
      error: false,
      loading: false,
      site: {
        jsd: {
          directory: {
            id: 'dummy-id',
          },
          customers: {
            customers: [generateCustomer(true)],
          },
        },
      },
    });

    const rows = wrapper.find(AkDynamicTableStateless).props().rows as any;
    const customerCol = shallow(<div>{rows[0].cells[2].content}</div>);

    expect(customerCol.find(FormattedMessage).exists()).to.equal(true);
    expect(customerCol.find(FormattedMessage).prop('defaultMessage')).to.equal('Active');
  });

  it('should render the Status column as an inactive message when the customer is inactive', () => {
    const wrapper = getWrapper({
      error: false,
      loading: false,
      site: {
        jsd: {
          directory: {
            id: 'dummy-id',
          },
          customers: {
            customers: [generateCustomer(false)],
          },
        },
      },
    });

    const rows = wrapper.find(AkDynamicTableStateless).props().rows as any;
    const customerCol = shallow(<div>{rows[0].cells[2].content}</div>);

    expect(customerCol.find(FormattedMessage).exists()).to.equal(true);
    expect(customerCol.find(FormattedMessage).prop('defaultMessage')).to.equal('Inactive');
  });

  it('should render the Status column as "deleted" and hide the actions when the customer is deleted', () => {
    const wrapper = getWrapper({
      error: false,
      loading: false,
      site: {
        jsd: {
          directory: {
            id: 'dummy-id',
          },
          customers: {
            customers: [generateCustomer(false)],
          },
        },
      },
    });

    wrapper.setState({ deletedCustomers: ['1'] });

    const rows = wrapper.find(AkDynamicTableStateless).props().rows as any;
    const customerCol = shallow(<div>{rows[0].cells[2].content}</div>);

    expect(customerCol.find(FormattedMessage).exists()).to.equal(true);
    expect(customerCol.find(FormattedMessage).prop('defaultMessage')).to.equal('Deleted');
    expect(rows[0].cells[3].content).to.equal(false);
  });

  it('should render the Actions column correctly', () => {
    const wrapper = getWrapper({
      error: false,
      loading: false,
      site: {
        jsd: {
          directory: {
            id: 'dummy-id',
          },
          customers: {
            customers: [generateCustomer(false)],
          },
        },
      },
    });

    const rows = wrapper.find(AkDynamicTableStateless).props().rows as any;
    const customerCol = shallow(<div>{rows[0].cells[3].content}</div>);

    expect(customerCol.find(JsdTableCustomerActions).exists()).to.equal(true);
  });

  it('should refetch when the displayNameFilter prop changes', () => {
    const refetchSpy = sinon.stub().resolves();
    const wrapper = shallow(
      <JsdTableImpl
        currentPage={1}
        cloudId={'test-cloud-id'}
        onSetPage={noop}
        intl={createMockIntlProp()}
        data={{
          refetch: refetchSpy,
        } as any}
        displayNameFilter={''}
        activeStatusFilter={'all'}
        match={{} as any}
        history={{
          replace: noop,
        } as any}
        location={{} as any}
      />,
    );

    wrapper.setProps({
      displayNameFilter: 'josh',
    });
    wrapper.update();

    expect(refetchSpy.callCount).to.equal(1);
    expect(refetchSpy.getCalls()[0].args[0].variables.displayName).to.equal('josh');
  });

  it('should refetch when the activeStatusFilter prop changes', () => {
    const spy = sinon.stub().resolves();
    const wrapper = shallow(
      <JsdTableImpl
        currentPage={1}
        cloudId={'test-cloud-id'}
        onSetPage={noop}
        intl={createMockIntlProp()}
        data={{
          refetch: spy,
        } as any}
        displayNameFilter={''}
        activeStatusFilter={'all'}
        match={{} as any}
        history={{
          replace: noop,
        } as any}
        location={{} as any}
      />,
    );

    wrapper.setProps({
      activeStatusFilter: 'inactive',
    });
    wrapper.update();

    expect(spy.callCount).to.equal(1);
    expect(spy.getCalls()[0].args[0].variables.activeStatus).to.equal('inactive');
  });

  it('should update the query parameters when the displayNameFilter prop changes', async () => {
    const spy = sinon.spy();
    const wrapper = shallow(
      <JsdTableImpl
        currentPage={1}
        cloudId={'test-cloud-id'}
        onSetPage={noop}
        intl={createMockIntlProp()}
        data={{
          refetch: sinon.stub().resolves(),
        } as any}
        displayNameFilter={''}
        activeStatusFilter={'all'}
        match={{} as any}
        history={{
          replace: spy,
        } as any}
        location={{
          pathname: '/',
        } as any}
      />,
    );

    wrapper.setProps({
      displayNameFilter: 'josh',
    });
    wrapper.update();

    await waitUntil(() => spy.callCount > 0);

    expect(spy.callCount).to.equal(1);
    expect(spy.getCalls()[0].args[0]).to.equal('/?filter=josh');
  });

  it('should update the query parameters when the activeStatusFilter prop changes', async () => {
    const spy = sinon.spy();
    const wrapper = shallow(
      <JsdTableImpl
        currentPage={1}
        cloudId={'test-cloud-id'}
        onSetPage={noop}
        intl={createMockIntlProp()}
        data={{
          refetch: sinon.stub().resolves(),
        } as any}
        displayNameFilter={undefined}
        activeStatusFilter={'all'}
        match={{} as any}
        history={{
          replace: spy,
        } as any}
        location={{
          pathname: '/',
        } as any}
      />,
    );

    wrapper.setProps({
      activeStatusFilter: 'inactive',
    });
    wrapper.update();

    await waitUntil(() => spy.callCount > 0);

    expect(spy.callCount).to.equal(1);
    expect(spy.getCalls()[0].args[0]).to.equal('/?active-filter=inactive');
  });
});
