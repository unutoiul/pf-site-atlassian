import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';

import { JsdPageImpl } from './jsd-page';

const Page: React.SFC = () => (
  <IntlProvider locale="en">
    <JsdPageImpl
      showFlag={undefined as any}
      hideFlag={undefined as any}
      intl={undefined as any}
      match={{
        params: { cloudId: 'dummy-cloud-id' },
        isExact: false,
        path: '',
        url: '',
      }}
      location={{} as any}
      history={{} as any}
    />
  </IntlProvider>
);

storiesOf('Site|JSD', module)
  .add('With customers', () => (
    <Page />
  ));
