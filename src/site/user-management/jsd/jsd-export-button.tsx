import ApolloClient from 'apollo-client';
import * as React from 'react';
import { ApolloConsumer } from 'react-apollo';
import {
  defineMessages,
  FormattedMessage,
  InjectedIntlProps,
  injectIntl,
} from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';

import AkButton from '@atlaskit/button';

import { analyticsClient, AnalyticsClientProps, jsdExportCustomersUIEventData, withAnalyticsClient } from 'common/analytics';
import { createErrorIcon, createSuccessIcon } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';

import { JsdExportQuery } from '../../../schema/schema-types';
import jsdExportQuery from './jsd-export.query.graphql';

const messages = defineMessages({
  actionExport: {
    id: 'jsd.page.layout.action.export',
    description: 'This button links to export customers, which allows admins to generate a CSV file of all JSD customers. CSV is an acronym for "Comma Separated Value", and should likely not be translated.',
    defaultMessage: 'Export customers',
  },
  exportSuccessfulTitle: {
    id: 'jsd.page.layout.action.export.title',
    description: 'The title of a notification shown, after a CSV export has been successfully created for the user to now download. CSV is an acronym for "Comma Separated Value", and should likely not be translated.',
    defaultMessage: 'Successfully exported customers',
  },
  exportSuccessfulDescription: {
    id: 'jsd.page.layout.action.export.description',
    description: 'Text describing how to download a customer list CSV export, after a CSV export has been successfully created for the user to now download. CSV is an acronym for "Comma Separated Value", and should likely not be translated.',
    defaultMessage: 'For your exported list, download a CSV file of all portal-only customers.',
  },
  exportFailedTitle: {
    id: 'jsd.page.layout.action.export.failed.title',
    description: 'The title of a notification shown after the user tried to export a list of customers, but the export failed',
    defaultMessage: 'Unable to export customers',
  },
  exportFailedDescription: {
    id: 'jsd.page.layout.action.export.failed.description',
    description: 'Text shown in a notification after a customer export was attempted, but could not be finished for an unknown reason. Includes instructions on how to attempt this again',
    defaultMessage: 'We ran into some issues when trying to export. Refresh this page and try again.',
  },
  exportSuccessfulAction: {
    id: 'jsd.page.layout.action.export.action',
    description: 'A clickable call to action to download a CSV file of customers, to the users computer. CSV is an acronym for "Comma Separated Value", and should likely not be translated.',
    defaultMessage: 'Download CSV file',
  },
});

interface SiteParams {
  cloudId: string;
}

interface State {
  loading: boolean;
  downloadedData?: Blob;
}

export class JsdExportButtonImpl extends React.Component<RouteComponentProps<SiteParams> & FlagProps & InjectedIntlProps & AnalyticsClientProps, State> {
  private client?: ApolloClient<any>;
  private csvFileName = 'portal-only-customers.csv';

  public readonly state: Readonly<State> = {
    loading: false,
  };

  public render() {
    return (
      <ApolloConsumer>
        {client => {
          this.client = client;

          return (
            <AkButton
              onClick={this.onExportCustomers}
              isLoading={this.state.loading}
            >
              <FormattedMessage {...messages.actionExport} />
            </AkButton>
          );
        }}
      </ApolloConsumer>
    );
  }

  private showExportSuccessFlag = () => {
    const { intl: { formatMessage } } = this.props;
    const { downloadedData } = this.state;

    if (!downloadedData) {
      return null;
    }

    this.props.showFlag({
      id: `jsd-customer-export-success-${Date.now()}`,
      title: formatMessage(messages.exportSuccessfulTitle),
      icon: createSuccessIcon(),
      description: formatMessage(messages.exportSuccessfulDescription),
      actions: [{
        content: (
          <a href={window.URL.createObjectURL(downloadedData)} download={this.csvFileName}>
            {formatMessage(messages.exportSuccessfulAction)}
          </a>
        ),
        onClick: () => {
          this.onDownloadClick();
        },
      }],
    });

    return null;
  }

  private showExportFailedFlag = () => {
    const { intl: { formatMessage } } = this.props;

    this.props.showFlag({
      id: `jsd-customer-export-failed-${Date.now()}`,
      title: formatMessage(messages.exportFailedTitle),
      icon: createErrorIcon(),
      description: formatMessage(messages.exportFailedDescription),
    });
  }

  private onExportCustomers = async () => {
    this.props.analyticsClient.sendUIEvent({
      data: jsdExportCustomersUIEventData(),
    });

    if (!this.client) {
      return;
    }

    try {
      this.setState({ loading: true });

      const { data } = await this.client.query<JsdExportQuery>({
        query: jsdExportQuery,
        variables: {
          cloudId: this.props.match.params.cloudId,
          errorRedirectUrl: window.location.href,
        },
        fetchPolicy: 'network-only',
      });

      this.setState({
        downloadedData: new Blob([data.site.jsd.directory.customerExport], {
          type: 'octet/stream',
        }),
      }, () => {
        this.showExportSuccessFlag();
      });
    } catch (e) {
      analyticsClient.onError(e);
      this.showExportFailedFlag();
    } finally {
      this.setState({ loading: false });
    }
  }

  private onDownloadClick = () => {
    // hack for IE11 since a download is not supported
    if (window.navigator.msSaveOrOpenBlob && this.state.downloadedData) {
      window.navigator.msSaveOrOpenBlob(this.state.downloadedData, this.csvFileName);
    }
  };
}

export const JsdExportButton = injectIntl<{}>(
  withFlag(
    withRouter(
      withAnalyticsClient(
        JsdExportButtonImpl,
      ),
    ),
  ),
);
