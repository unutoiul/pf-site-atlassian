import { expect } from 'chai';
import { mount, ReactWrapper } from 'enzyme';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import * as sinon from 'sinon';

import AkButton from '@atlaskit/button';

import { createMockAnalyticsClient, createMockIntlContext, createMockIntlProp, waitUntil } from '../../../utilities/testing';
import { JsdExportButtonImpl } from './jsd-export-button';

const noop = () => null;
let mockApolloClient;
// tslint:disable-next-line:no-unbound-method
const oldCreateObjectUrl = window.URL.createObjectURL;

function mountWithApolloProvider(node: React.ReactNode): ReactWrapper<any, any> {

  return mount(
    <ApolloProvider client={mockApolloClient}>
      {node}
    </ApolloProvider>,
    createMockIntlContext(),
  );
}

describe('JsdExportButton', () => {
  const sandbox = sinon.sandbox.create();

  beforeEach(() => {
    window.URL.createObjectURL = () => 'https://example.com';

    mockApolloClient = {
      query: sandbox.stub().resolves({
        data: {
          site: {
            jsd: {
              directory: {
                customerExport: 'username,full_name,email,active,last_login',
              },
            },
          },
        },
      }),
    };
  });

  afterEach(() => {
    window.URL.createObjectURL = oldCreateObjectUrl;
    sandbox.restore();
  });

  it('should render a button', () => {
    const wrapper = mountWithApolloProvider(
      <JsdExportButtonImpl
        match={{ params: { cloudId: 'test-cloud-id' } } as any}
        location={{} as any}
        history={{} as any}
        showFlag={noop}
        hideFlag={noop}
        intl={createMockIntlProp()}
        analyticsClient={createMockAnalyticsClient()}
      />,
    );
    const button = wrapper.find(AkButton);
    expect(button.length).to.equal(1);
  });

  it('should call the client with the correct arguments when clicking the button', async () => {
    const showFlagSpy = sinon.spy();
    const wrapper = mountWithApolloProvider(
      <JsdExportButtonImpl
        match={{ params: { cloudId: 'test-cloud-id' } } as any}
        location={{} as any}
        history={{} as any}
        showFlag={showFlagSpy}
        hideFlag={noop}
        intl={createMockIntlProp()}
        analyticsClient={createMockAnalyticsClient()}
      />,
    );
    const button = wrapper.find(AkButton);
    button.simulate('click');
    await waitUntil(() => mockApolloClient.query.callCount > 0);
    expect(mockApolloClient.query.callCount).to.equal(1);
    expect(mockApolloClient.query.getCalls()[0].args[0].variables).to.deep.equal({
      cloudId: 'test-cloud-id',
      errorRedirectUrl: window.location.href,
    });
  });

  it('should show a download flag with an action after clicking on the button', async () => {
    const showFlagSpy = sinon.spy();
    const wrapper = mountWithApolloProvider(
      <JsdExportButtonImpl
        match={{ params: { cloudId: 'test-cloud-id' } } as any}
        location={{} as any}
        history={{} as any}
        showFlag={showFlagSpy}
        hideFlag={noop}
        intl={createMockIntlProp()}
        analyticsClient={createMockAnalyticsClient()}
      />,
    );
    const button = wrapper.find(AkButton);
    button.simulate('click');
    await waitUntil((() => showFlagSpy.callCount > 0));

    expect(showFlagSpy.callCount).to.equal(1);
    expect(showFlagSpy.getCalls()[0].args[0].actions.length).to.equal(1);
    expect(showFlagSpy.getCalls()[0].args[0].id).to.contain('jsd-customer-export-success');
  });

  describe('in the error case', () => {
    beforeEach(() => {
      mockApolloClient.query = sandbox.stub().throws();
    });

    it('should show a error flag when the query throws', async () => {
      const showFlagSpy = sinon.spy();
      const wrapper = mountWithApolloProvider(
        <JsdExportButtonImpl
          match={{ params: { cloudId: 'test-cloud-id' } } as any}
          location={{} as any}
          history={{} as any}
          showFlag={showFlagSpy}
          hideFlag={noop}
          intl={createMockIntlProp()}
          analyticsClient={createMockAnalyticsClient()}
        />,
      );
      const button = wrapper.find(AkButton);
      button.simulate('click');
      await waitUntil(() => showFlagSpy.callCount > 0);
      expect(showFlagSpy.callCount).to.equal(1);
      expect(showFlagSpy.getCalls()[0].args[0].id).to.contain('jsd-customer-export-failed');
    });
  });

});
