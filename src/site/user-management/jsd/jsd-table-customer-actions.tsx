import * as React from 'react';
import { defineMessages } from 'react-intl';

import { Action, Actions } from 'common/actions';

import { ActiveStatusFilter } from './jsd-table-filters/jsd-table-active-filter';
import { JsdChangePasswordModal } from './modals/jsd-change-password-modal';
import { JsdDeleteModal } from './modals/jsd-delete-modal';
import { JsdEditNameModal } from './modals/jsd-edit-name-modal';
import { JsdGrantRevokeModal } from './modals/jsd-grant-revoke-modal';
import { JsdMigrateAccountModal } from './modals/jsd-migrate-account-modal';

const messages = defineMessages({
  actionEditName: {
    id: 'jsd.page.table.customer.actions.edit.name',
    description: 'Edit full name link.',
    defaultMessage: 'Edit full name',
  },
  actionChangePassword: {
    id: 'jsd.page.table.customer.actions.change.password',
    description: 'Change password link.',
    defaultMessage: 'Change password',
  },
  actionMigrate: {
    id: 'jsd.page.table.customer.actions.migrate',
    description: 'Migrate to Atlassian account link.',
    defaultMessage: 'Migrate to Atlassian account',
  },
  actionDeactivate: {
    id: 'jsd.page.table.customer.actions.deactivate',
    description: 'Deactivate account link.',
    defaultMessage: 'Revoke access',
  },
  actionActivate: {
    id: 'jsd.page.table.customer.actions.activate',
    description: 'Deactivate account link.',
    defaultMessage: 'Grant access',
  },
  actionDelete: {
    id: 'jsd.page.table.customer.actions.delete',
    description: 'Delete account link.',
    defaultMessage: 'Delete account',
  },
});

interface Props {
  cloudId: string;
  directoryId: string;
  accountId: string;
  accountDisplayName: string;
  accountActive: boolean;
  activeStatusFilter: ActiveStatusFilter;
  start: number;
  onCustomerDeleted(customerId: string): void;
}

export class JsdTableCustomerActions extends React.Component<Props> {
  public render() {
    return (
      <JsdDeleteModal
        directoryId={this.props.directoryId}
        accountId={this.props.accountId}
        accountDisplayName={this.props.accountDisplayName}
        onCustomerDeleted={this.props.onCustomerDeleted}
      >
        {(openDeleteModal) => (
          <JsdGrantRevokeModal
            cloudId={this.props.cloudId}
            directoryId={this.props.directoryId}
            accountId={this.props.accountId}
            accountDisplayName={this.props.accountDisplayName}
            accountActive={this.props.accountActive}
            activeStatusFilter={this.props.activeStatusFilter}
            start={this.props.start}
          >
            {(openGrantRevokeModal) => (
              <JsdEditNameModal
                cloudId={this.props.cloudId}
                directoryId={this.props.directoryId}
                accountId={this.props.accountId}
                accountDisplayName={this.props.accountDisplayName}
                activeStatusFilter={this.props.activeStatusFilter}
                start={this.props.start}
              >
                {(openEditNameModal) => (
                  <JsdChangePasswordModal
                    directoryId={this.props.directoryId}
                    accountId={this.props.accountId}
                    accountDisplayName={this.props.accountDisplayName}
                  >
                    {(openChangePasswordModal) => (
                      <JsdMigrateAccountModal
                        cloudId={this.props.cloudId}
                        accountId={this.props.accountId}
                        accountDisplayName={this.props.accountDisplayName}
                        activeStatusFilter={this.props.activeStatusFilter}
                        start={this.props.start}
                      >
                        {(openMigrateModal) => (
                          <Actions>
                            <Action
                              actionType="custom"
                              customMessage={messages.actionEditName}
                              onAction={openEditNameModal}
                            />
                            <Action
                              actionType="custom"
                              customMessage={messages.actionChangePassword}
                              onAction={openChangePasswordModal}
                            />
                            <Action
                              actionType="custom"
                              customMessage={messages.actionMigrate}
                              onAction={openMigrateModal}
                            />
                            <Action
                              actionType="custom"
                              customMessage={this.props.accountActive ? messages.actionDeactivate : messages.actionActivate}
                              onAction={openGrantRevokeModal}
                            />
                            <Action
                              actionType="custom"
                              customMessage={messages.actionDelete}
                              onAction={openDeleteModal}
                            />
                          </Actions>
                        )}
                      </JsdMigrateAccountModal>
                    )}
                  </JsdChangePasswordModal>
                )}
              </JsdEditNameModal>
            )}
          </JsdGrantRevokeModal>
        )}
      </JsdDeleteModal>
    );
  }
}
