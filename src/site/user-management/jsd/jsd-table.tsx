import * as moment from 'moment';
import { parse, stringify } from 'query-string';
import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import {
  defineMessages,
  FormattedMessage,
  InjectedIntlProps,
  injectIntl,
} from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';
import styled from 'styled-components';

import {
  DynamicTableStateless as AkDynamicTableStateless,
  HeaderRow as AkHeaderRow,
  Row as AkRow,
} from '@atlaskit/dynamic-table';
import AkEmptyState from '@atlaskit/empty-state';
import {
  colors as AkColors,
  gridSize as akGridSize,
} from '@atlaskit/theme';

import { Account } from 'common/account';
import { analyticsClient } from 'common/analytics';
import { errorFlagMessages } from 'common/error';
import { errorWindowImage, searchErrorImage } from 'common/images';
import { getStart } from 'common/pagination';

import { JsdCustomer, JsdTableQuery, JsdTableQueryVariables } from '../../../schema/schema-types';
import { ROWS_PER_PAGE } from './jsd-constants';
import { JsdTableCustomerActions } from './jsd-table-customer-actions';

import { ActiveStatusFilter } from './jsd-table-filters/jsd-table-active-filter';
import jsdTableQuery from './jsd-table.query.graphql';

export const SubtleCell = styled.span`
  color: ${AkColors.subtleHeading};
`;

const messages = defineMessages({
  errorStateDescription: {
    id: 'jsd.page.table.error.state',
    description: 'Shown when there is an error trying to fetch customers',
    defaultMessage: 'We had trouble trying to fetch your customers. Refresh the page to try again.',
  },
  headerCustomer: {
    id: 'jsd.page.table.header.customer',
    description: 'Used as a table header.',
    defaultMessage: 'Customer',
  },
  headerLastActive: {
    id: 'jsd.page.table.header.last.active',
    description: 'Used as a table header.',
    defaultMessage: 'Last active',
  },
  headerStatus: {
    id: 'jsd.page.table.header.status',
    description: 'Used as a table header.',
    defaultMessage: 'Status',
  },
  headerActions: {
    id: 'jsd.page.table.header.actions',
    description: 'Used as a table header.',
    defaultMessage: 'Actions',
  },
  emptyStateTitle: {
    id: 'jsd.page.table.empty.state',
    description: `Used as a part of a table's empty state when there are no customers.`,
    defaultMessage: `We looked everywhere but don't have anything to show you. Try modifying your search criteria.`,
  },
  active: {
    id: 'jsd.page.table.active',
    description: 'Active label in customer row.',
    defaultMessage: 'Active',
  },
  inactive: {
    id: 'jsd.page.table.inactive',
    description: 'Inactive label in customer row.',
    defaultMessage: 'Inactive',
  },
  deleted: {
    id: 'jsd.page.table.deleted',
    description: 'Deleted label in customer row.',
    defaultMessage: 'Deleted',
  },
  neverLoggedIn: {
    id: 'jsd.page.table.never.logged.in',
    description: 'Never logged in label for users.',
    defaultMessage: 'Never logged in',
  },
});

type OwnProps = InjectedIntlProps & {
  currentPage: number;
  cloudId: string;
  activeStatusFilter: ActiveStatusFilter;
  displayNameFilter?: string;
  onSetPage(page: number): void;
};

type Props = ChildProps<OwnProps, JsdTableQuery>;

interface State {
  // Since we don't have Apollo support yet for adding local virtual fields to schemas,
  // we have to store an array of deleted customers to check against
  deletedCustomers: string[];
}

export class JsdTableImpl extends React.Component<Props & RouteComponentProps<any>, State> {
  public readonly state: Readonly<State> = {
    deletedCustomers: [],
  };

  public async componentDidUpdate(prevProps: Props) {
    if (!this.props.data) {
      return;
    }

    if (prevProps.activeStatusFilter === this.props.activeStatusFilter && prevProps.displayNameFilter === this.props.displayNameFilter) {
      return;
    }

    if (this.props.data.refetch) {
      try {
        await this.props.data.refetch({
          variables: {
            cloudId: this.props.cloudId,
            count: ROWS_PER_PAGE,
            start: getStart(this.props.currentPage, ROWS_PER_PAGE),
            activeStatus: this.props.activeStatusFilter,
            displayName: this.props.displayNameFilter,
          } as JsdTableQueryVariables,
        });
      } catch (e) {
        analyticsClient.onError(e);
      }
    }

    this.updateQueryParams();
  }

  public render() {
    if (this.props.data && this.props.data.error) {
      return (
        <AkEmptyState
          imageUrl={errorWindowImage}
          header={<FormattedMessage {...errorFlagMessages.title} />}
          description={<FormattedMessage {...messages.errorStateDescription} />}
          size="wide"
        />
      );
    }

    const emptyState: React.ReactNode = (
      <AkEmptyState
        size="wide"
        imageUrl={searchErrorImage}
        header={<FormattedMessage {...messages.emptyStateTitle} />}
      />
    );

    return (
      <AkDynamicTableStateless
        rowsPerPage={ROWS_PER_PAGE}
        page={this.props.currentPage}
        head={this.getTableHead()}
        rows={this.getTableRows()}
        isLoading={this.props.data && this.props.data.loading}
        emptyView={emptyState}
        onSetPage={this.props.onSetPage}
      />
    );
  }

  private updateQueryParams = (): void => {
    const { history, location } = this.props;
    const params = {
      'active-filter': this.props.activeStatusFilter === 'all' ? undefined : this.props.activeStatusFilter,
      filter: this.props.displayNameFilter,
    };
    history.replace(`${location.pathname}?${stringify({ ...parse(location.search), ...params })}`);
  }

  private getTableRows = (): AkRow[] => {
    const directoryId = this.props.data
      && this.props.data.site
      && this.props.data.site.jsd.directory.id;

    return this.getPaddedRows().map(customer => {
      if (!customer) {
        return {
          cells: [
            { key: 'customer', content: '' },
            { key: 'last.active', content: '' },
            { key: 'status', content: '' },
            { key: 'actions', content: '' },
          ],
        };
      }

      return {
        cells: [
          {
            key: 'customer',
            content: (
              <Account
                id={customer.id}
                email={customer.email}
                displayName={customer.displayName}
                showAvatar={false}
              />
            ),
          },
          {
            key: 'last.active',
            content: customer.lastLogin ?
              moment(Number(customer.lastLogin)).format('D MMM YYYY, h:mm a')
              : <FormattedMessage {...messages.neverLoggedIn} />,
          },
          {
            key: 'status',
            content: this.getRowStatusContent(customer),
          },
          {
            key: 'actions',
            content: directoryId && !this.isCustomerDeleted(customer.id) && (
              <JsdTableCustomerActions
                cloudId={this.props.cloudId}
                directoryId={directoryId}
                accountId={customer.id}
                accountDisplayName={customer.displayName}
                accountActive={customer.active}
                activeStatusFilter={this.props.activeStatusFilter}
                onCustomerDeleted={this.onCustomerDeleted}
                start={getStart(this.props.currentPage, ROWS_PER_PAGE)}
              />
            ),
          },
        ],
      };
    });
  };

  private getRowStatusContent = (customer: JsdCustomer): React.ReactNode => {
    let message;

    if (this.isCustomerDeleted(customer.id)) {
      message = messages.deleted;
    } else if (customer.active) {
      message = messages.active;
    } else {
      message = messages.inactive;
    }

    return (
      <SubtleCell>
        <FormattedMessage {...message} />
      </SubtleCell>
    );
  };

  private getTableHead = (): AkHeaderRow => {
    return {
      cells: [
        {
          key: 'customer',
          content: <FormattedMessage {...messages.headerCustomer} />,
          width: 40,
        },
        {
          key: 'last.active',
          content: <FormattedMessage {...messages.headerLastActive} />,
          width: 18,
        },
        {
          key: 'status',
          content: <FormattedMessage {...messages.headerStatus} />,
          width: 18,
        },
        {
          key: 'header.actions',
          content: <FormattedMessage {...messages.headerActions} />,
          width: 24,
          inlineStyles: { paddingLeft: `${akGridSize() * 3.5}px` },
        },
      ],
    };
  };

  private getPaddedRows = () => {
    const currentCustomers = this.props.data && this.props.data.site && this.props.data.site.jsd.customers.customers;
    const hasMoreResults: boolean = !!(
      this.props.data && this.props.data.site && this.props.data.site.jsd.customers.hasMoreResults
    );

    const startIndex = (this.props.currentPage - 1) * ROWS_PER_PAGE;
    const paddedArray = Array(this.props.currentPage * ROWS_PER_PAGE);

    if (currentCustomers) {
      paddedArray.splice(startIndex, ROWS_PER_PAGE, ...currentCustomers);

      if (hasMoreResults) {
        paddedArray.push(undefined);
      }
    }

    return paddedArray;
  };

  private onCustomerDeleted = (customerId: string): void => {
    this.setState(prevState => ({
      deletedCustomers: [...prevState.deletedCustomers, customerId],
    }));
  };

  private isCustomerDeleted = (customerId: string): boolean => {
    return this.state.deletedCustomers.includes(customerId);
  };
}

const withQuery = graphql<OwnProps, JsdTableQuery>(jsdTableQuery, {
  options: ({ currentPage, cloudId, activeStatusFilter, displayNameFilter }) => {
    return {
      variables: {
        cloudId,
        count: ROWS_PER_PAGE,
        start: getStart(currentPage, ROWS_PER_PAGE),
        activeStatus: activeStatusFilter,
        displayName: displayNameFilter,
      } as JsdTableQueryVariables,
    };
  },
});

export const JsdTable = injectIntl(withQuery(withRouter(JsdTableImpl)));
