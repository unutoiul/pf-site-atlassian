import { DataProxy } from 'apollo-cache';

import { JsdTableQuery, JsdTableQueryVariables } from '../../../schema/schema-types';

import { ROWS_PER_PAGE } from './jsd-constants';
import { ActiveStatusFilter } from './jsd-table-filters/jsd-table-active-filter';
import query from './jsd-table.query.graphql';

const readJsdTableQuery = (store: DataProxy, variables: JsdTableQueryVariables): JsdTableQuery | undefined => {
  try {
    return store.readQuery({
      query,
      variables,
    }) as JsdTableQuery;
  } catch (e) {
    // Due to https://github.com/apollographql/apollo-feature-requests/issues/1,
    // store.readQuery throws rather than returns null for this case.
    return undefined;
  }
};

interface UpdateProps {
  name?: string;
  active?: boolean;
}

export const optimisticallyUpdateCustomer = (store: DataProxy, cloudId: string, activeStatus: ActiveStatusFilter, accountId: string, start: number, updates: UpdateProps): void => {
  const variables: JsdTableQueryVariables = {
    cloudId,
    count: ROWS_PER_PAGE,
    start,
    activeStatus,
  };

  const tableResult = readJsdTableQuery(store, variables);
  if (!tableResult || !tableResult.site.jsd.customers.customers.length) {
    return;
  }

  tableResult.site.jsd.customers.customers = tableResult.site.jsd.customers.customers.map(customer => {
    if (customer.id === accountId) {
      return {
        ...customer,
        ...(updates.name ? { displayName: updates.name } : {}),
        ...(updates.active !== undefined ? { active: updates.active } : {}),
      };
    }

    return customer;
  });

  store.writeQuery({
    query,
    variables,
    data: tableResult,
  });
};
