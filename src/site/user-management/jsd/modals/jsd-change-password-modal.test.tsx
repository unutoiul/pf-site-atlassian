import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import { createMockIntlProp } from '../../../../utilities/testing';
import { JsdChangePasswordModalImpl } from './jsd-change-password-modal';

const getWrapper = () => {
  return shallow((
    <JsdChangePasswordModalImpl
      directoryId="DUMMY-DIRECTORY-ID"
      accountId="DUMMY-ACCOUNT-ID"
      accountDisplayName="Four Tet"
      intl={createMockIntlProp()}
    >
      {openModal => <button onClick={openModal} />}
    </JsdChangePasswordModalImpl>
  ));
};

describe('JSD change password modal', () => {
  it('should have passed all the required optional fields', () => {
    const wrapper = getWrapper();

    expect(wrapper.prop('successFlag')).not.to.equal(undefined);
    expect(wrapper.prop('field')).not.to.equal(undefined);
    expect(wrapper.prop('initialFieldValue')).to.equal(undefined);
    expect(wrapper.prop('screenEvent')).not.to.equal(undefined);
    expect(wrapper.prop('submitButtonEvent')).not.to.equal(undefined);
    expect(wrapper.prop('cancelButtonEvent')).not.to.equal(undefined);
  });
});
