import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';

import AkButton from '@atlaskit/button';

import { createMockIntlProp } from '../../../../utilities/testing';
import { ActiveStatusFilter } from '../jsd-table-filters/jsd-table-active-filter';
import { JsdGrantRevokeModalImpl } from './jsd-grant-revoke-modal';

const defaultProps = {
  cloudId: 'DUMMY-CLOUD-ID',
  directoryId: 'DUMMY-DIRECTORY-ID',
  accountId: 'DUMMY-ACCOUNT-ID',
  accountDisplayName: 'Four Tet',
  intl: createMockIntlProp(),
  activeStatusFilter: 'all' as ActiveStatusFilter,
  start: 1,
};

storiesOf('Site|JSD/Grant revoke Modal', module)
  .add('Account active grant revoke modal (deactivate modal)', () => (
    <IntlProvider locale="en">
      <JsdGrantRevokeModalImpl {...defaultProps} accountActive={true}>
        {(openModal) => (
          // tslint:disable-next-line:jsx-use-translation-function
          <AkButton onClick={openModal}>Open Modal</AkButton>
        )}
      </JsdGrantRevokeModalImpl>
    </IntlProvider>
  ))
  .add('Account not active grant revoke modal (activate modal)', () => (
    <IntlProvider locale="en">
      <JsdGrantRevokeModalImpl {...defaultProps} accountActive={false}>
        {(openModal) => (
          // tslint:disable-next-line:jsx-use-translation-function
          <AkButton onClick={openModal}>Open Modal</AkButton>
        )}
      </JsdGrantRevokeModalImpl>
    </IntlProvider>
  ));
