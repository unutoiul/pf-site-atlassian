import * as React from 'react';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';

import {
  jsdPortalCustomersActivateAccountModalCancelEvent,
  jsdPortalCustomersActivateAccountModalLinkEvent,
  jsdPortalCustomersActivateAccountModalScreenEvent,
  jsdPortalCustomersActivateAccountModalSubmitEvent,
  jsdPortalCustomersActivateAccountModalTrackEvent,
  jsdPortalCustomersDeactivateAccountModalCancelEvent,
  jsdPortalCustomersDeactivateAccountModalLinkEvent,
  jsdPortalCustomersDeactivateAccountModalScreenEvent,
  jsdPortalCustomersDeactivateAccountModalSubmitEvent,
  jsdPortalCustomersDeactivateAccountModalTrackEvent,
} from 'common/analytics';
import { MutationModal } from 'common/modal';

import { JsdGrantRevokeModalMutationVariables } from '../../../../schema/schema-types';
import { ActiveStatusFilter } from '../jsd-table-filters/jsd-table-active-filter';
import { optimisticallyUpdateCustomer } from '../jsd-table.query.updater';
import mutation from './jsd-grant-revoke-modal.mutation.graphql';

const messages = defineMessages({
  grantHeader: {
    id: 'jsd.grant.revoke.modal.grant.header',
    defaultMessage: 'Grant site access',
    description: 'The header of a modal dialog where you can activate a user',
  },
  revokeHeader: {
    id: 'jsd.grant.revoke.modal.revoke.header',
    defaultMessage: 'Revoke site access',
    description: 'The header of a modal dialog where you can deactivate a user',
  },
  grantBody: {
    id: 'jsd.grant.revoke.modal.grant.body',
    defaultMessage: 'When you grant site access, the customer can log in to the portal. You can revoke site access at any time.',
    description: 'The body of a modal dialog, the portal is a customer facing screen, and access means being able to use that with this customer account',
  },
  revokeBody: {
    id: 'jsd.grant.revoke.modal.revoke.body',
    defaultMessage: `When you revoke site access, the customer won't be able to log in to the portal. You can grant access to the customer at any time.`,
    description: 'The body of a modal dialog, the portal is a customer facing screen, and no access means that this customer account cannot use the portal. It is easy to undo this operation.',
  },
  grantSubmitButton: {
    id: 'jsd.grant.revoke.modal.grant.submit.button',
    defaultMessage: 'Grant access',
    description: 'Primary button text for the grant access to customer account modal. You are granting access for a customer to the customer portal screen.',
  },
  revokeSubmitButton: {
    id: 'jsd.grant.revoke.modal.revoke.submit.button',
    defaultMessage: 'Revoke access',
    description: 'Primary button text for the revoke access to customer account modal. You are removing access for a customer to the customer portal screen.',
  },
  grantSuccessTitle: {
    id: 'jsd.grant.revoke.modal.grant.success.title',
    defaultMessage: `Granted access to customer`,
    description: 'A flag shown after access to the customer portal is given to a particular customer',
  },
  grantSuccessDescription: {
    id: 'jsd.grant.revoke.modal.grant.success.description',
    defaultMessage: `{fieldValue} is now able to log in to the portal.`,
    description: 'A flag shown after access to the customer portal is given to a particular customer. fieldValue is the customers name',
  },
  revokeSuccessTitle: {
    id: 'jsd.grant.revoke.modal.revoke.success.title',
    defaultMessage: `Revoked access to customer`,
    description: 'A flag shown after access to the customer portal is removed to a particular customer. fieldValue is the customers name',
  },
  revokeSuccessDescription: {
    id: 'jsd.grant.revoke.modal.revoke.success.description',
    defaultMessage: `{fieldValue} won't be able to log in to the portal anymore.`,
    description: 'Success flag description on save, after deactivating a customer account',
  },
});

interface Props {
  cloudId: string;
  directoryId: string;
  accountId: string;
  accountDisplayName: string;
  accountActive: boolean;
  activeStatusFilter: ActiveStatusFilter;
  start: number;
  children(openModal: () => void): React.ReactNode;
}

export class JsdGrantRevokeModalImpl extends React.Component<Props & InjectedIntlProps> {
  public render() {
    const isGrantModal = !this.props.accountActive;

    return (
      <MutationModal<JsdGrantRevokeModalMutationVariables>
        successFlag={isGrantModal ? {
          title: messages.grantSuccessTitle,
          description: messages.grantSuccessDescription,
        } : {
          title: messages.revokeSuccessTitle,
          description: messages.revokeSuccessDescription,
        }}
        initialFieldValue={this.props.accountDisplayName}
        body={isGrantModal ? <FormattedMessage {...messages.grantBody} /> : <FormattedMessage {...messages.revokeBody} />}
        header={isGrantModal ? <FormattedMessage {...messages.grantHeader} /> : <FormattedMessage {...messages.revokeHeader} />}
        mutation={mutation}
        mutationOptions={this.getMutationOptions}
        submitButtonText={isGrantModal ? this.props.intl.formatMessage(messages.grantSubmitButton) : this.props.intl.formatMessage(messages.revokeSubmitButton)}
        screenEvent={isGrantModal ? jsdPortalCustomersActivateAccountModalScreenEvent() : jsdPortalCustomersDeactivateAccountModalScreenEvent()}
        trackEvent={{ data: isGrantModal ? jsdPortalCustomersActivateAccountModalTrackEvent() : jsdPortalCustomersDeactivateAccountModalTrackEvent() }}
        submitButtonEvent={{ data: isGrantModal ? jsdPortalCustomersActivateAccountModalSubmitEvent() : jsdPortalCustomersDeactivateAccountModalSubmitEvent() }}
        cancelButtonEvent={{ data: isGrantModal ? jsdPortalCustomersActivateAccountModalCancelEvent() : jsdPortalCustomersDeactivateAccountModalCancelEvent() }}
        openEvent={{ data: isGrantModal ? jsdPortalCustomersActivateAccountModalLinkEvent() : jsdPortalCustomersDeactivateAccountModalLinkEvent() }}
      >
        {openModal => this.props.children(openModal)}
      </MutationModal>
    );
  }
  private getMutationOptions = () => ({
    variables: {
      access: !this.props.accountActive,
      directoryId: this.props.directoryId,
      accountId: this.props.accountId,
    },
    update: (store) => {
      optimisticallyUpdateCustomer(
        store,
        this.props.cloudId,
        this.props.activeStatusFilter,
        this.props.accountId,
        this.props.start,
        { active: !this.props.accountActive },
      );
    },
  });
}

export const JsdGrantRevokeModal = injectIntl(JsdGrantRevokeModalImpl);
