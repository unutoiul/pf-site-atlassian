import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import { createMockIntlProp } from '../../../../utilities/testing';
import { JsdEditNameModalImpl } from './jsd-edit-name-modal';

const getWrapper = () => {
  return shallow((
    <JsdEditNameModalImpl
      cloudId="DUMMY-CLOUD-ID"
      directoryId="DUMMY-DIRECTORY-ID"
      accountId="DUMMY-ACCOUNT-ID"
      accountDisplayName="Four Tet"
      intl={createMockIntlProp()}
      activeStatusFilter="all"
      start={1}
    >
      {openModal => <button onClick={openModal} />}
    </JsdEditNameModalImpl>
  ));
};

describe('JSD edit name modal', () => {
  it('should have passed all the required optional fields', () => {
    const wrapper = getWrapper();

    expect(wrapper.prop('successFlag')).not.to.equal(undefined);
    expect(wrapper.prop('field')).not.to.equal(undefined);
    expect(wrapper.prop('initialFieldValue')).not.to.equal(undefined);
    expect(wrapper.prop('screenEvent')).not.to.equal(undefined);
    expect(wrapper.prop('submitButtonEvent')).not.to.equal(undefined);
    expect(wrapper.prop('cancelButtonEvent')).not.to.equal(undefined);
  });
});
