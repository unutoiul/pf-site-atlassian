import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import { createMockIntlProp } from '../../../../utilities/testing';
import { JsdMigrateAccountModalImpl } from './jsd-migrate-account-modal';

const getWrapper = () => {
  return shallow((
    <JsdMigrateAccountModalImpl
      cloudId="DUMMY-CLOUD-ID"
      accountId="DUMMY-ACCOUNT-ID"
      accountDisplayName="Four Tet"
      intl={createMockIntlProp()}
      activeStatusFilter="all"
      start={1}
    >
      {openModal => <button onClick={openModal} />}
    </JsdMigrateAccountModalImpl>
  ));
};

describe('JSD migrate account modal', () => {
  it('should have passed all the required optional fields', () => {
    const wrapper = getWrapper();

    expect(wrapper.prop('successFlag')).not.to.equal(undefined);
    expect(wrapper.prop('trackEvent')).not.to.equal(undefined);
    expect(wrapper.prop('screenEvent')).not.to.equal(undefined);
    expect(wrapper.prop('submitButtonEvent')).not.to.equal(undefined);
    expect(wrapper.prop('cancelButtonEvent')).not.to.equal(undefined);
    expect(wrapper.prop('openEvent')).not.to.equal(undefined);
  });
});
