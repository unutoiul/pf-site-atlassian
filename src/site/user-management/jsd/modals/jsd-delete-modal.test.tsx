import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import { createMockIntlProp, noop } from '../../../../utilities/testing';
import { JsdDeleteModalImpl } from './jsd-delete-modal';

const getWrapper = () => {
  return shallow((
    <JsdDeleteModalImpl
      directoryId="DUMMY-DIRECTORY-ID"
      accountId="DUMMY-ACCOUNT-ID"
      accountDisplayName="Four Tet"
      intl={createMockIntlProp()}
      onCustomerDeleted={noop}
    >
      {openModal => <button onClick={openModal} />}
    </JsdDeleteModalImpl>
  ));
};

describe('JSD edit name modal', () => {
  it('should have passed all the required optional fields', () => {
    const wrapper = getWrapper();

    expect(wrapper.prop('successFlag')).not.to.equal(undefined);
    expect(wrapper.prop('initialFieldValue')).not.to.equal(undefined);
    expect(wrapper.prop('screenEvent')).not.to.equal(undefined);
    expect(wrapper.prop('submitButtonEvent')).not.to.equal(undefined);
    expect(wrapper.prop('cancelButtonEvent')).not.to.equal(undefined);
  });
});
