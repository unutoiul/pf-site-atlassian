import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import {
  jsdPortalCustomersDeleteModalCancelEvent,
  jsdPortalCustomersDeleteModalLinkEvent,
  jsdPortalCustomersDeleteModalScreenEvent,
  jsdPortalCustomersDeleteModalSubmitEvent,
} from 'common/analytics';
import { MutationModal } from 'common/modal';

import { JsdDeleteModalMutationVariables } from '../../../../schema/schema-types';
import mutation from './jsd-delete-modal.mutation.graphql';

const messages = defineMessages({
  header: {
    id: 'jsd.delete.modal.header',
    defaultMessage: `Delete customer account`,
  },
  body: {
    id: 'jsd.delete.modal.field.label',
    defaultMessage: `When you delete a customer's account, they won't be able to log in to the portal. You can't restore an account once it's been deleted.`,
    description: 'Body of a modal describing what happens when you delete a customer account',
  },
  submitButton: {
    id: 'jsd.delete.modal.submit.button',
    defaultMessage: 'Delete',
    description: 'Primary button text.',
  },
  successTitle: {
    id: 'jsd.delete.modal.success.title',
    defaultMessage: `Deleted customer account`,
    description: 'Success flag title on deletion of a customer account.',
  },
  successDescription: {
    id: 'jsd.delete.modal.success.description',
    defaultMessage: `{fieldValue} won't be able to log in to the portal anymore.`,
    description: 'Success flag description on deletion of a customer account. Field value is the name of the deleted user',
  },
});

interface Props {
  directoryId: string;
  accountId: string;
  accountDisplayName: string;
  children(openModal: () => void): React.ReactNode;
  onCustomerDeleted(customerId: string): void;
}

export class JsdDeleteModalImpl extends React.Component<Props & InjectedIntlProps> {
  public render() {
    return (
      <MutationModal<JsdDeleteModalMutationVariables>
        successFlag={{
          title: messages.successTitle,
          description: messages.successDescription,
        }}
        header={this.props.intl.formatMessage(messages.header)}
        body={this.props.intl.formatMessage(messages.body)}
        mutation={mutation}
        mutationOptions={this.getMutationOptions}
        initialFieldValue={this.props.accountDisplayName}
        submitButtonText={this.props.intl.formatMessage(messages.submitButton)}
        submitButtonAppearance="danger"
        onCompleted={this.onCompleted}
        screenEvent={jsdPortalCustomersDeleteModalScreenEvent()}
        submitButtonEvent={{ data: jsdPortalCustomersDeleteModalSubmitEvent() }}
        cancelButtonEvent={{ data: jsdPortalCustomersDeleteModalCancelEvent() }}
        openEvent={{ data: jsdPortalCustomersDeleteModalLinkEvent() }}
      >
        {openModal => this.props.children(openModal)}
      </MutationModal>
    );
  }

  private getMutationOptions = () => ({
    variables: {
      directoryId: this.props.directoryId,
      accountId: this.props.accountId,
    },
  });

  private onCompleted = () => {
    this.props.onCustomerDeleted(this.props.accountId);
  };
}

export const JsdDeleteModal = injectIntl(JsdDeleteModalImpl);
