import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';

import AkButton from '@atlaskit/button';

import { createMockIntlProp } from '../../../../utilities/testing';
import { ActiveStatusFilter } from '../jsd-table-filters/jsd-table-active-filter';
import { JsdEditNameModalImpl } from './jsd-edit-name-modal';

const defaultProps = {
  cloudId: 'DUMMY-CLOUD-ID',
  directoryId: 'DUMMY-DIRECTORY-ID',
  accountId: 'DUMMY-ACCOUNT-ID',
  accountDisplayName: 'Four Tet',
  intl: createMockIntlProp(),
  activeStatusFilter: 'all' as ActiveStatusFilter,
  start: 1,
};

storiesOf('Site|JSD/Edit Name Modal', module)
  .add('Default', () => (
    <IntlProvider locale="en">
      <JsdEditNameModalImpl {...defaultProps}>
        {(openModal) => (
          // tslint:disable-next-line:jsx-use-translation-function
          <AkButton onClick={openModal}>Open Modal</AkButton>
        )}
      </JsdEditNameModalImpl>
    </IntlProvider>
  ));
