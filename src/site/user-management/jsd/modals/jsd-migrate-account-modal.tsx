import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import {
  jsdPortalCustomersMigrateAccountLinkEvent,
  jsdPortalCustomersMigrateAccountModalCancelEvent,
  jsdPortalCustomersMigrateAccountModalScreenEvent,
  jsdPortalCustomersMigrateAccountModalSubmitEvent,
  jsdPortalCustomersMigrateAccountModalTrackEvent,
} from 'common/analytics';
import { MutationModal } from 'common/modal';

import { JsdMigrateAccountModalMutationVariables } from '../../../../schema/schema-types';
import { ActiveStatusFilter } from '../jsd-table-filters/jsd-table-active-filter';
import { optimisticallyUpdateCustomer } from '../jsd-table.query.updater';
import mutation from './jsd-migrate-account-modal.mutation.graphql';

const messages = defineMessages({
  header: {
    id: 'jsd.migrate.account.modal.header',
    defaultMessage: 'Migrate to Atlassian account',
    description: 'The title of the header for the modal.',
  },
  description: {
    id: 'jsd.migrate.account.modal.description',
    defaultMessage: `When you migrate a user, we transfer their requests to the Atlassian account and give them a new key. Their portal-only account can't be unmigrated or reactivated.`,
    description: 'The description for the modal.',
  },
  submitButton: {
    id: 'jsd.migrate.account.modal.submit.button',
    defaultMessage: 'Migrate',
    description: 'Primary button text.',
  },
  successTitle: {
    id: 'jsd.migrate.account.modal.success.title',
    defaultMessage: 'Migrated customer to an Atlassian account',
    description: 'Success flag title on save.',
  },
  successDescription: {
    id: 'jsd.migrate.account.modal.success.description',
    defaultMessage: `We'll email {fieldValue} with details about how to log in to their Atlassian account.`,
    description: 'Success flag description on save.',
  },
});

interface Props {
  cloudId: string;
  accountId: string;
  accountDisplayName: string;
  activeStatusFilter: ActiveStatusFilter;
  start: number;
  children(openModal: () => void): React.ReactNode;
}

export class JsdMigrateAccountModalImpl extends React.Component<Props & InjectedIntlProps> {
  public render() {
    return (
      <MutationModal<JsdMigrateAccountModalMutationVariables>
        successFlag={{
          title: messages.successTitle,
          description: messages.successDescription,
        }}
        header={this.props.intl.formatMessage(messages.header)}
        body={this.props.intl.formatMessage(messages.description)}
        mutation={mutation}
        mutationOptions={this.getMutationOptions}
        initialFieldValue={this.props.accountDisplayName}
        submitButtonText={this.props.intl.formatMessage(messages.submitButton)}
        submitButtonAppearance="warning"
        trackEvent={{ data: jsdPortalCustomersMigrateAccountModalTrackEvent() }}
        screenEvent={jsdPortalCustomersMigrateAccountModalScreenEvent()}
        submitButtonEvent={{ data: jsdPortalCustomersMigrateAccountModalSubmitEvent() }}
        cancelButtonEvent={{ data: jsdPortalCustomersMigrateAccountModalCancelEvent() }}
        openEvent={{ data: jsdPortalCustomersMigrateAccountLinkEvent() }}
      >
        {openModal => this.props.children(openModal)}
      </MutationModal>
    );
  }

  private getMutationOptions = () => ({
    variables: {
      cloudId: this.props.cloudId,
      accountId: this.props.accountId,
    },
    update: (store) => {
      optimisticallyUpdateCustomer(
        store,
        this.props.cloudId,
        this.props.activeStatusFilter,
        this.props.accountId,
        this.props.start,
        { active: false },
      );
    },
  });
}

export const JsdMigrateAccountModal = injectIntl(JsdMigrateAccountModalImpl);
