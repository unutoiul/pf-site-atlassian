import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import AkFieldTextStateless from '@atlaskit/field-text';

import {
  jsdPortalCustomersEditNameModalCancelEvent,
  jsdPortalCustomersEditNameModalLinkEvent,
  jsdPortalCustomersEditNameModalScreenEvent,
  jsdPortalCustomersEditNameModalSubmitEvent,
} from 'common/analytics';
import { MutationModal } from 'common/modal';

import { JsdEditNameModalMutationVariables } from '../../../../schema/schema-types';
import { ActiveStatusFilter } from '../jsd-table-filters/jsd-table-active-filter';
import { optimisticallyUpdateCustomer } from '../jsd-table.query.updater';
import mutation from './jsd-edit-name-modal.mutation.graphql';

const messages = defineMessages({
  header: {
    id: 'jsd.edit.name.modal.header',
    defaultMessage: `Edit customer's full name`,
  },
  fieldLabel: {
    id: 'jsd.edit.name.modal.field.label',
    defaultMessage: 'Full name',
    description: 'Label that sits above the name input.',
  },
  submitButton: {
    id: 'jsd.edit.name.modal.submit.button',
    defaultMessage: 'Update',
    description: 'Primary button text.',
  },
  successTitle: {
    id: 'jsd.edit.name.modal.success.title',
    defaultMessage: `Updated customer's name`,
    description: 'Success flag title on save.',
  },
  successDescription: {
    id: 'jsd.edit.name.modal.success.description',
    defaultMessage: 'The customers full name is now {fieldValue}.',
    description: 'Success flag description on save.',
  },
});

interface Props {
  cloudId: string;
  directoryId: string;
  accountId: string;
  accountDisplayName: string;
  activeStatusFilter: ActiveStatusFilter;
  start: number;
  children(openModal: () => void): React.ReactNode;
}

export class JsdEditNameModalImpl extends React.Component<Props & InjectedIntlProps> {
  public render() {
    return (
      <MutationModal<JsdEditNameModalMutationVariables>
        successFlag={{
          title: messages.successTitle,
          description: messages.successDescription,
        }}
        header={this.props.intl.formatMessage(messages.header)}
        mutation={mutation}
        mutationOptions={this.getMutationOptions}
        field={this.getField}
        initialFieldValue={this.props.accountDisplayName}
        submitButtonText={this.props.intl.formatMessage(messages.submitButton)}
        screenEvent={jsdPortalCustomersEditNameModalScreenEvent()}
        submitButtonEvent={{ data: jsdPortalCustomersEditNameModalSubmitEvent() }}
        cancelButtonEvent={{ data: jsdPortalCustomersEditNameModalCancelEvent() }}
        openEvent={{ data: jsdPortalCustomersEditNameModalLinkEvent() }}
      >
        {openModal => this.props.children(openModal)}
      </MutationModal>
    );
  }

  private getField = ({ onChange, value }) => (
    <AkFieldTextStateless
      label={this.props.intl.formatMessage(messages.fieldLabel)}
      name="name"
      onChange={onChange}
      value={value}
      shouldFitContainer={true}
      autoFocus={true}
    />
  );

  private getMutationOptions = (name: string) => ({
    variables: {
      name,
      directoryId: this.props.directoryId,
      accountId: this.props.accountId,
    },
    update: (store) => {
      optimisticallyUpdateCustomer(
        store,
        this.props.cloudId,
        this.props.activeStatusFilter,
        this.props.accountId,
        this.props.start,
        { name },
      );
    },
  });
}

export const JsdEditNameModal = injectIntl(JsdEditNameModalImpl);
