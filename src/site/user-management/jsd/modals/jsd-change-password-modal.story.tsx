import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';

import AkButton from '@atlaskit/button';

import { createMockIntlProp } from '../../../../utilities/testing';
import { JsdChangePasswordModalImpl } from './jsd-change-password-modal';

const defaultProps = {
  cloudId: 'DUMMY-CLOUD-ID',
  directoryId: 'DUMMY-DIRECTORY-ID',
  accountId: 'DUMMY-ACCOUNT-ID',
  accountDisplayName: 'Four Tet',
  intl: createMockIntlProp(),
};

storiesOf('Site|JSD/Change Password Modal', module)
  .add('Default', () => (
    <IntlProvider locale="en">
      <JsdChangePasswordModalImpl {...defaultProps}>
        {(openModal) => (
          // tslint:disable-next-line:jsx-use-translation-function
          <AkButton onClick={openModal}>Open Modal</AkButton>
        )}
      </JsdChangePasswordModalImpl>
    </IntlProvider>
  ));
