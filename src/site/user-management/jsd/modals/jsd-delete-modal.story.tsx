import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';

import AkButton from '@atlaskit/button';

import { createMockIntlProp, noop } from '../../../../utilities/testing';
import { JsdDeleteModalImpl } from './jsd-delete-modal';

const defaultProps = {
  cloudId: 'DUMMY-CLOUD-ID',
  directoryId: 'DUMMY-DIRECTORY-ID',
  accountId: 'DUMMY-ACCOUNT-ID',
  accountDisplayName: 'Four Tet',
  intl: createMockIntlProp(),
  onCustomerDeleted: noop,
};

storiesOf('Site|JSD/Delete Modal', module)
  .add('Default', () => (
    <IntlProvider locale="en">
      <JsdDeleteModalImpl {...defaultProps}>
        {(openModal) => (
          // tslint:disable-next-line:jsx-use-translation-function
          <AkButton onClick={openModal}>Open Modal</AkButton>
        )}
      </JsdDeleteModalImpl>
    </IntlProvider>
  ));
