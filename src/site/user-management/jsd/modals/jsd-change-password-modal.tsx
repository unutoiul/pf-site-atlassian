import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import AkFieldTextStateless from '@atlaskit/field-text';

import {
  jsdPortalCustomersChangePasswordLinkEvent,
  jsdPortalCustomersChangePasswordModalCancelEvent,
  jsdPortalCustomersChangePasswordModalScreenEvent,
  jsdPortalCustomersChangePasswordModalSubmitEvent,
} from 'common/analytics';
import { MutationModal } from 'common/modal';

import {
  JsdChangePasswordModalMutationVariables,
} from '../../../../schema/schema-types';
import mutation from './jsd-change-password-modal.mutation.graphql';

const messages = defineMessages({
  header: {
    id: 'jsd.change.password.modal.header',
    defaultMessage: 'Change password',
  },
  fieldLabel: {
    id: 'jsd.change.password.modal.field.label',
    defaultMessage: 'New password',
    description: 'Label that sits above the name input.',
  },
  submitButton: {
    id: 'jsd.change.password.modal.submit.button',
    defaultMessage: 'Save password',
    description: 'Primary button text.',
  },
  successTitle: {
    id: 'jsd.change.password.modal.success.title',
    defaultMessage: `Changed customer's password`,
    description: 'Success flag title on save.',
  },
  successDescription: {
    id: 'jsd.change.password.modal.success.description',
    defaultMessage: `Let {displayName} know about their new password.`,
    description: 'Success flag description on save.',
  },
});

interface Props {
  directoryId: string;
  accountId: string;
  accountDisplayName: string;
  children(openModal: () => void): React.ReactNode;
}

export class JsdChangePasswordModalImpl extends React.Component<Props & InjectedIntlProps> {
  public render() {
    return (
      <MutationModal<JsdChangePasswordModalMutationVariables>
        successFlag={{
          title: messages.successTitle,
          description: messages.successDescription,
        }}
        successFlagValues={{ displayName: this.props.accountDisplayName }}
        header={this.props.intl.formatMessage(messages.header)}
        mutation={mutation}
        mutationOptions={this.getMutationOptions}
        field={this.getField}
        submitButtonText={this.props.intl.formatMessage(messages.submitButton)}
        screenEvent={jsdPortalCustomersChangePasswordModalScreenEvent()}
        submitButtonEvent={{ data: jsdPortalCustomersChangePasswordModalSubmitEvent() }}
        cancelButtonEvent={{ data: jsdPortalCustomersChangePasswordModalCancelEvent() }}
        openEvent={{ data: jsdPortalCustomersChangePasswordLinkEvent() }}
      >
        {openModal => this.props.children(openModal)}
      </MutationModal>
    );
  }

  private getField = ({ onChange, value }) => (
    <AkFieldTextStateless
      label={this.props.intl.formatMessage(messages.fieldLabel)}
      name="password"
      onChange={onChange}
      value={value}
      shouldFitContainer={true}
      autoFocus={true}
      type="password"
    />
  );

  private getMutationOptions = (fieldValue) => ({
    variables: {
      password: fieldValue,
      directoryId: this.props.directoryId,
      accountId: this.props.accountId,
    },
  });
}

export const JsdChangePasswordModal = injectIntl(JsdChangePasswordModalImpl);
