import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import { createMockIntlProp } from '../../../../utilities/testing';
import { JsdGrantRevokeModalImpl } from './jsd-grant-revoke-modal';

const getWrapper = () => {
  return shallow((
    <JsdGrantRevokeModalImpl
      cloudId="DUMMY-CLOUD-ID"
      directoryId="DUMMY-DIRECTORY-ID"
      accountId="DUMMY-ACCOUNT-ID"
      accountDisplayName="Four Tet"
      accountActive={true}
      intl={createMockIntlProp()}
      activeStatusFilter="all"
      start={1}
    >
      {openModal => <button onClick={openModal} />}
    </JsdGrantRevokeModalImpl>
  ));
};

describe('JSD grant access modal', () => {
  it('should have passed all the required optional fields', () => {
    const wrapper = getWrapper();

    expect(wrapper.prop('successFlag')).not.to.equal(undefined);
    expect(wrapper.prop('initialFieldValue')).not.to.equal(undefined);
    expect(wrapper.prop('screenEvent')).not.to.equal(undefined);
    expect(wrapper.prop('submitButtonEvent')).not.to.equal(undefined);
    expect(wrapper.prop('cancelButtonEvent')).not.to.equal(undefined);
    expect(wrapper.prop('trackEvent')).not.to.equal(undefined);
  });

  it('should pass the access:false mutation variable if the account is active', () => {
    const wrapper = shallow((
      <JsdGrantRevokeModalImpl
        cloudId="DUMMY-CLOUD-ID"
        directoryId="DUMMY-DIRECTORY-ID"
        accountId="DUMMY-ACCOUNT-ID"
        accountDisplayName="Four Tet"
        accountActive={true}
        intl={createMockIntlProp()}
        activeStatusFilter="all"
        start={1}
      >
        {openModal => <button onClick={openModal} />}
      </JsdGrantRevokeModalImpl>
    ));

    expect(wrapper.prop('mutationOptions')().variables.access).to.equal(false);
  });

  it('should pass the access:true mutation variable if the account is not active', () => {
    const wrapper = shallow((
      <JsdGrantRevokeModalImpl
        cloudId="DUMMY-CLOUD-ID"
        directoryId="DUMMY-DIRECTORY-ID"
        accountId="DUMMY-ACCOUNT-ID"
        accountDisplayName="Four Tet"
        accountActive={false}
        intl={createMockIntlProp()}
        activeStatusFilter="all"
        start={1}
      >
        {openModal => <button onClick={openModal} />}
      </JsdGrantRevokeModalImpl>
    ));

    expect(wrapper.prop('mutationOptions')().variables.access).to.equal(true);
  });
});
