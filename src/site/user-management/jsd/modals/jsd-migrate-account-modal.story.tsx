import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';

import AkButton from '@atlaskit/button';

import { createMockIntlProp } from '../../../../utilities/testing';
import { ActiveStatusFilter } from '../jsd-table-filters/jsd-table-active-filter';
import { JsdMigrateAccountModalImpl } from './jsd-migrate-account-modal';

const defaultProps = {
  cloudId: 'DUMMY-CLOUD-ID',
  directoryId: 'DUMMY-DIRECTORY-ID',
  accountId: 'DUMMY-ACCOUNT-ID',
  accountDisplayName: 'Four Tet',
  accountUsername: 'ftet',
  intl: createMockIntlProp(),
  activeStatusFilter: 'all' as ActiveStatusFilter,
  start: 1,
};

storiesOf('Site|JSD/Migrate Account Modal', module)
  .add('Default', () => (
    <IntlProvider locale="en">
      <JsdMigrateAccountModalImpl {...defaultProps}>
        {(openModal) => (
          // tslint:disable-next-line:jsx-use-translation-function
          <AkButton onClick={openModal}>Open Modal</AkButton>
        )}
      </JsdMigrateAccountModalImpl>
    </IntlProvider>
  ));
