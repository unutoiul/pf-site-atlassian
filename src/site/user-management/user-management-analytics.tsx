import { User } from 'src/schema/schema-types';

import { UserState } from 'common/analytics';
import { AnalyticsData, TenantType } from 'common/analytics/with-analytics';

const tenantType: TenantType = 'cloudId';

export interface UMAnalyticsData extends AnalyticsData {
  cloudId?: string;
}

export interface AnalyticsProps {
  userStates: UserState[];
}

export function createUMAnalyticsData(data: UMAnalyticsData): AnalyticsData {
  const { cloudId, actionSubject, actionSubjectId, action, subproduct, ...restProps } = data;

  return {
    subproduct,
    tenantType,
    tenantId: cloudId,
    actionSubject,
    action,
    actionSubjectId,
    ...restProps,
  };
}

export function getUserStates(user?: Pick<User, 'activeStatus' | 'presence' | 'active'>): UserState[] {
  const userStates = new Array<UserState>();

  if (!user) {
    return userStates;
  }

  if (user.activeStatus === 'BLOCKED') {
    userStates.push('disabled');
  }
  if (!user.presence) {
    userStates.push('invited');
  }

  if (user.activeStatus === 'DISABLED') {
    userStates.push('revokedAccessToSite');
  }

  if (user.active) {
    userStates.push('grantedAccessToSite');
  }

  return userStates;
}
