import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider, ChildProps } from 'react-apollo';
import { IntlProvider } from 'react-intl';
import { MemoryRouter } from 'react-router-dom';

import AkPage from '@atlaskit/page';

import { FlagProvider } from 'common/flag';

import { createApolloClient } from '../../../apollo-client';
import { GSuitePageQuery } from '../../../schema/schema-types';
import { createMockAnalyticsClient, createMockIntlProp } from '../../../utilities/testing';
import { GSuitePageImpl } from './g-suite-page';

const client = createApolloClient();

const Wrapper = ({ children }) => (
  <ApolloProvider client={client}>
    <IntlProvider locale="en">
      <MemoryRouter>
        <FlagProvider>
          <AkPage>
            {children}
          </AkPage>
        </FlagProvider>
      </MemoryRouter>
    </IntlProvider>
  </ApolloProvider>
);

const pageDefaults = {
  intl: createMockIntlProp(),
  history: { replace: () => null } as any,
  match: { params: { cloudId: 'DUMMY_CLOUD_ID' } } as any,
  location: window.location as any,
  showFlag: () => null,
  hideFlag: () => null,
  dispatch: (_: any): any => null,
  analyticsClient: createMockAnalyticsClient(),
};

const mockDataNoConnection: ChildProps<any, GSuitePageQuery> = {
  loading: false,
  currentSite: {},
};

const loadingData: ChildProps<any, GSuitePageQuery> = {
  loading: true,
};

const errorData: ChildProps<any, GSuitePageQuery> = {
  loading: false,
  error: true,
};

storiesOf('Site|G Suite', module)
  .add('While loading', () => (
    <Wrapper>
      <GSuitePageImpl data={loadingData} {...pageDefaults} />
    </Wrapper>
  ))
  .add('With an error', () => (
    <Wrapper>
      <GSuitePageImpl data={errorData} {...pageDefaults} />
    </Wrapper>
  ))
  .add('With no connection', () => (
    <Wrapper>
      <GSuitePageImpl data={mockDataNoConnection} {...pageDefaults} />
    </Wrapper>
  ));
