import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';

import AkButton from '@atlaskit/button';
import AkSpinner from '@atlaskit/spinner';

import { PageLayout } from 'common/page-layout';

import { createMockIntlProp } from '../../../utilities/testing';
import { GSuitePageImpl, messages } from './g-suite-page';

describe('G Suite Page Test', () => {

  function createTestComponent(data): ShallowWrapper {
    return shallow(
      <GSuitePageImpl
        match={{
          isExact: false,
          path: '',
          url: '',
          params: { cloudId: 'dummy' },
        }}
        intl={createMockIntlProp()}
        data={data}
        showFlag={() => null}
        hideFlag={() => null}
        {...{} as any}
      />,
    );
  }

  it('shows spinner when loading data', () => {
    const wrapper = createTestComponent({ loading: true });
    const pageLayout = wrapper.find(PageLayout);
    expect(pageLayout.length).to.equal(1);
    expect(pageLayout.find(AkSpinner).length).to.equal(1);
  });

  it('shows disconnected page and content when data loaded and no state information', () => {
    const wrapper = createTestComponent({ loading: false, error: false, site: {} });
    const pageLayout = wrapper.find(PageLayout);
    expect(pageLayout.length).to.equal(1);
    const button = wrapper.find(AkButton);
    expect(button.length).to.equal(1);
  });

  it('renders an error on graphql error', () => {
    const wrapper = createTestComponent({ loading: false, error: true });
    expect(wrapper.find('span').text()).to.equal(messages.error.defaultMessage);
  });
});
