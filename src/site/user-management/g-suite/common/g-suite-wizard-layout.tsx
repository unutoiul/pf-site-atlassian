import * as React from 'react';
import styled from 'styled-components';

import { ButtonGroup as AkButtonGroup } from '@atlaskit/button';
import { Grid as AkGrid, GridColumn as AkGridColumn } from '@atlaskit/page';
import { gridSize as akGridSize } from '@atlaskit/theme';

const Page = styled.div`
  padding-top: ${akGridSize() * 5}px;
`;

export const ActionsWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  padding-top: ${akGridSize() * 2}px;
`;

export interface GSuiteWizardLayoutProps {
  header: string;
  children: React.ReactNode;
  actions: React.ReactNode[];
}

export class GSuiteWizardLayoutImpl extends React.Component<GSuiteWizardLayoutProps> {
  public render() {
    return (
      <Page>
        <AkGrid>
          <AkGridColumn medium={3} />
          <AkGridColumn medium={6}>
            <h2>{this.props.header}</h2>
            <div className="children">{this.props.children}</div>
            <ActionsWrapper>
              <AkButtonGroup>
                {this.props.actions}
              </AkButtonGroup>
            </ActionsWrapper>
          </AkGridColumn>
          <AkGridColumn medium={3} />
        </AkGrid>
      </Page>
    );
  }
}

export const GSuiteWizardLayout = GSuiteWizardLayoutImpl;
