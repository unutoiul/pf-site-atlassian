// tslint:disable:jsx-use-translation-function
import { action, storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';

import { GSuiteWizardButton } from './g-suite-wizard-button';

const pageDecorator = (storyFn) => (
  <IntlProvider locale="en">
    <div style={{ padding: '50px 50px 50px 50px' }}>
      {storyFn()}
    </div>
  </IntlProvider>
);

const onClick = () => action('clicked');

storiesOf('Site|G Suite/Wizard/Button', module)
  .addDecorator(pageDecorator)
  .add('Button', () => {
    return (
      <GSuiteWizardButton onClick={onClick} />
    );
  });
