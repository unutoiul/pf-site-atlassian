import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import { ProgressTracker as AkProgressTracker } from '@atlaskit/progress-tracker';

import { FocusedTask } from 'common/focused-task';

export interface GSuiteWizardDrawerProps {
  isOpen?: boolean;
  children: React.ReactNode;
  currentStage: GSuiteStages;
  onClose(): void;
}

export const messages = defineMessages({
  step1: {
    id: 'user-management.g-suite.connect',
    defaultMessage: 'Connect your G Suite',
    description: 'Progress bar indicator; shows drawer content prompting users to connect G Suite to their instance',
  },
  step2: {
    id: 'user-management.g-suite.create',
    defaultMessage: 'Link organization',
    description: 'Progress bar indicator; shows drawer content prompting users to either create or select an organization to connect their G Suite instance to',
  },
  step3: {
    id: 'user-management.g-suite.select',
    defaultMessage: 'Select users',
    description: 'Progress bar indicator; shows drawer content prompting users to either select users to import from G Suite',
  },
  step4: {
    id: 'user-management.g-suite.confirm',
    defaultMessage: 'Confirm',
    description: 'Title of the page showing G Suite information.',
  },
});

export type GSuiteStages = 'connect' | 'organization' | 'link' | 'confirm';
export const stages: GSuiteStages[] = ['connect', 'organization', 'link', 'confirm'];

export class GSuiteWizardDrawerImpl extends React.Component<GSuiteWizardDrawerProps & InjectedIntlProps> {

  public render() {

    const { intl: { formatMessage } } = this.props;

    const items = [
      {
        id: 'connect',
        label: formatMessage(messages.step1),
        status: this.determineCurrentState('connect'),
        percentageComplete: this.determinePercentage('connect'),
      },
      {
        id: 'organization',
        label: formatMessage(messages.step2),
        status: this.determineCurrentState('organization'),
        percentageComplete: this.determinePercentage('organization'),
      },
      {
        id: 'link',
        label: formatMessage(messages.step3),
        status: this.determineCurrentState('link'),
        percentageComplete: this.determinePercentage('link'),
      },
      {
        id: 'confirm',
        label: formatMessage(messages.step4),
        status: this.determineCurrentState('confirm'),
        percentageComplete: this.determinePercentage('confirm'),
      },
    ];

    return (
      <FocusedTask onClose={this.onClose} isOpen={this.props.isOpen || false}>
        <AkProgressTracker spacing="comfortable" items={items} />
        {this.props.children}
      </FocusedTask>
    );
  }

  private onClose = () => {
    this.props.onClose();
  }

  private determinePercentage = (progress) => {
    return this.determineCurrentState(progress) === 'unvisited' ? 0 : this.determineCurrentState(progress) === 'visited' ? 100 : 0;
  }

  private determineCurrentState = (progress): 'current' | 'visited' | 'unvisited' => {
    const itemIndex = stages.findIndex(c => c === progress);
    const currentItemIndex = stages.findIndex(c => c === this.props.currentStage);

    return itemIndex > currentItemIndex ? 'unvisited' : itemIndex === currentItemIndex ? 'current' : 'visited';
  }
}

export const GSuiteWizardDrawer = injectIntl(GSuiteWizardDrawerImpl);
