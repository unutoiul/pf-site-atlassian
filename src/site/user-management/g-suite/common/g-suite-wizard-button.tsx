import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import AkButton, { AkButtonProps } from '@atlaskit/button';
import { gridSize as akGridSize } from '@atlaskit/theme';

import { googleLogo } from 'common/images';

export const GoogleLogo = styled.div`
  background: url(${googleLogo}) no-repeat 5%/contain;
  width: ${akGridSize() * 4}px;
  height: ${akGridSize() * 2.5}px;
`;

export const GoogleButton = styled(AkButton)`
  box-shadow: 0 2px 2px rgba(0,0,0,0.15);
  background-color: #FFF;

  :hover {
    box-shadow: 0 2px 2px rgba(0,0,0,0.2);
  }
`;

export const ButtonText = styled.span`
  font-weight: 600;
  font-family: 'Roboto', sans-serif;
  color: rgba(0,0,0,0.54);
`;

export const messages = defineMessages({
  title: {
    id: 'user-management.site-settings.g-suite.button.text',
    description: 'Button to prompt users to connect G Suite. This opens up a pop-up window for Google authentication.',
    defaultMessage: 'Connect G Suite',
  },
});

export class GSuiteWizardButtonImpl extends React.Component<AkButtonProps & InjectedIntlProps> {

  public render() {

    return (
      <GoogleButton {...this.props} iconBefore={<GoogleLogo />}>
        <ButtonText>{this.props.intl.formatMessage(messages.title)}</ButtonText>
      </GoogleButton>
    );
  }

}

export const GSuiteWizardButton = injectIntl(GSuiteWizardButtonImpl);
