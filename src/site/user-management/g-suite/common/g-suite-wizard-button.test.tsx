import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import { createMockIntlProp } from '../../../../utilities/testing';

import { GSuiteWizardButtonImpl } from './g-suite-wizard-button';

describe('G Suite Wizard Button', () => {

  it('Should have "Connect to G Suite" text', () => {
    const wrapper = shallow(<GSuiteWizardButtonImpl intl={createMockIntlProp()} />);
    expect(wrapper.html()).to.contain('Connect G Suite');
  });

});
