import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import { ButtonGroup as AkButtonGroup } from '@atlaskit/button';

import { GSuiteWizardLayout, GSuiteWizardLayoutProps } from './g-suite-wizard-layout';

describe('G Suite Layout', () => {
  function createTestComponent(props: GSuiteWizardLayoutProps) {
    return shallow(
      <GSuiteWizardLayout {... props}/>,
    );
  }

  it('Should render children', () => {
    const component = createTestComponent({ header: 'Cheddar', children: 'Cheese', actions: ['Brie'] });
    expect(component.find('h2').text()).to.equal('Cheddar');
    expect(component.find('.children').text()).to.equal('Cheese');
    expect(component.find(AkButtonGroup).children().text()).to.equal('Brie');
  });
});
