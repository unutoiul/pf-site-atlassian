import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import { ProgressTracker as AkProgressTracker } from '@atlaskit/progress-tracker';

import { FocusedTask } from 'common/focused-task';

import { createMockIntlProp } from '../../../../utilities/testing';

import { GSuiteWizardDrawerImpl, GSuiteWizardDrawerProps } from './g-suite-wizard-drawer';

describe('G Suite Drawer', () => {
  function createTestComponent(props: GSuiteWizardDrawerProps) {
    return shallow(
      <GSuiteWizardDrawerImpl intl={createMockIntlProp()} {... props}/>,
    );
  }

  describe('When rendering children', () => {

    it('Should render children as child of the FocusedTask', () => {
      // tslint:disable-next-line:jsx-use-translation-function
      const component = createTestComponent({ onClose: () => undefined, currentStage: 'connect', children: <p>Hello world</p> });
      expect(component.find(FocusedTask).children().at(1).text()).to.equal('Hello world');
    });

  });

  describe('When specifying to open or close the focused task', () => {

    it('Should open the FocusedTask', () => {
      const component = createTestComponent({ onClose: () => undefined, currentStage: 'connect', children: <p />, isOpen: true });
      expect(component.find(FocusedTask).prop('isOpen')).to.equal(true);
    });

    it('Should not open the FocusedTask', () => {
      const component = createTestComponent({ onClose: () => undefined, currentStage: 'connect', children: <p />, isOpen: false });
      expect(component.find(FocusedTask).prop('isOpen')).to.equal(false);
    });

    it('Should not open the FocusedTask if unspecified', () => {
      const component = createTestComponent({ onClose: () => undefined, currentStage: 'connect', children: <p /> });
      expect(component.find(FocusedTask).prop('isOpen')).to.equal(false);
    });

  });

  describe('When rendering the progress bar', () => {

    it('Should indicate current "Connect your G Suite" state', () => {
      const component = createTestComponent({ onClose: () => undefined, currentStage: 'connect', children: <span /> });

      const expectedItems = [
        {
          id: 'connect',
          label: 'Connect your G Suite',
          status: 'current',
          percentageComplete: 0,
        },
        {
          id: 'organization',
          label: 'Link organization',
          status: 'unvisited',
          percentageComplete: 0,
        },
        {
          id: 'link',
          label: 'Select users',
          status: 'unvisited',
          percentageComplete: 0,
        },
        {
          id: 'confirm',
          label: 'Confirm',
          status: 'unvisited',
          percentageComplete: 0,
        },
      ];

      expect(component.find(AkProgressTracker).prop('items')).to.deep.equal(expectedItems);
    });

    it('Should indicate current "Select organization" state', () => {
      const component = createTestComponent({ onClose: () => undefined, currentStage: 'organization', children: <span /> });

      const expectedItems = [
        {
          id: 'connect',
          label: 'Connect your G Suite',
          status: 'visited',
          percentageComplete: 100,
        },
        {
          id: 'organization',
          label: 'Link organization',
          status: 'current',
          percentageComplete: 0,
        },
        {
          id: 'link',
          label: 'Select users',
          status: 'unvisited',
          percentageComplete: 0,
        },
        {
          id: 'confirm',
          label: 'Confirm',
          status: 'unvisited',
          percentageComplete: 0,
        },
      ];

      expect(component.find(AkProgressTracker).prop('items')).to.deep.equal(expectedItems);
    });

    it('Should indicate current "Select users" state', () => {
      const component = createTestComponent({ onClose: () => undefined, currentStage: 'link', children: <span /> });

      const expectedItems = [
        {
          id: 'connect',
          label: 'Connect your G Suite',
          status: 'visited',
          percentageComplete: 100,
        },
        {
          id: 'organization',
          label: 'Link organization',
          status: 'visited',
          percentageComplete: 100,
        },
        {
          id: 'link',
          label: 'Select users',
          status: 'current',
          percentageComplete: 0,
        },
        {
          id: 'confirm',
          label: 'Confirm',
          status: 'unvisited',
          percentageComplete: 0,
        },
      ];

      expect(component.find(AkProgressTracker).prop('items')).to.deep.equal(expectedItems);
    });

    it('Should indicate current "Confirm" state', () => {
      const component = createTestComponent({ onClose: () => undefined, currentStage: 'confirm', children: <span /> });

      const expectedItems = [
        {
          id: 'connect',
          label: 'Connect your G Suite',
          status: 'visited',
          percentageComplete: 100,
        },
        {
          id: 'organization',
          label: 'Link organization',
          status: 'visited',
          percentageComplete: 100,
        },
        {
          id: 'link',
          label: 'Select users',
          status: 'visited',
          percentageComplete: 100,
        },
        {
          id: 'confirm',
          label: 'Confirm',
          status: 'current',
          percentageComplete: 0,
        },
      ];

      expect(component.find(AkProgressTracker).prop('items')).to.deep.equal(expectedItems);
    });
  });
});
