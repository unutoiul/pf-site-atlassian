// tslint:disable:jsx-use-translation-function
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';

import AkButton, { ButtonGroup as AkButtonGroup } from '@atlaskit/button';

import { GSuiteWizardLayout } from './g-suite-wizard-layout';

const mockActions = (
  <AkButtonGroup>
    <AkButton>
      Back
    </AkButton>
    <AkButton appearance="primary">
      Continue
    </AkButton>
  </AkButtonGroup>
);

const mockChild = (
  <span>
    You will be asked by Google to confrim that you allow Atlassian to import your users.
    You will need to be a GSuite administrator to connect it
  </span>
);

const pageDecorator = (storyFn) => (
  <IntlProvider locale="en">
    {storyFn()}
  </IntlProvider>
);

storiesOf('Site|G Suite/Wizard/Layout', module)
  .addDecorator(pageDecorator)
  .add('Layout', () => {
    return (
      <GSuiteWizardLayout header="Create your organization" actions={[mockActions]}>
        {mockChild}
      </GSuiteWizardLayout>
    );
  });
