// tslint:disable:jsx-use-translation-function
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';

import AkButton from '@atlaskit/button';

import { GSuiteWizardDrawer } from './g-suite-wizard-drawer';
import { GSuiteWizardLayout } from './g-suite-wizard-layout';

const mockActions: React.ReactNode[] = [
  <AkButton key="back">Back</AkButton>,
  <AkButton key="continue" appearance="primary">Continue</AkButton>,
];

const mockChild = (
  <span style={{ width: '400px', paddingTop: '40px', margin: '0 auto' }}>
    You will be asked by Google to confrim that you allow Atlassian to import your users.
    You will need to be a GSuite administrator to connect it
  </span>
);

const pageDecorator = (storyFn) => (
  <IntlProvider locale="en">
    {storyFn()}
  </IntlProvider>
);

const onClose = () => undefined;

storiesOf('Site|G Suite/Wizard/Drawer', module)
  .addDecorator(pageDecorator)
  .add('Connect stage', () => {
    return (
      <GSuiteWizardDrawer onClose={onClose} isOpen={true} currentStage="connect">
        {mockChild}
      </GSuiteWizardDrawer>
    );
  })
  .add('Create stage', () => {
    return (
      <GSuiteWizardDrawer onClose={onClose} isOpen={true} currentStage="organization">
        {mockChild}
      </GSuiteWizardDrawer>
    );
  }).add('Link stage', () => {
    return (
      <GSuiteWizardDrawer onClose={onClose} isOpen={true} currentStage="link">
        {mockChild}
      </GSuiteWizardDrawer>
    );
  }).add('Confirm stage', () => {
    return (
      <GSuiteWizardDrawer onClose={onClose}isOpen={true} currentStage="confirm">
        {mockChild}
      </GSuiteWizardDrawer>
    );
  }).add('With GSuite Wizard Layout', () => {
    return (
      <GSuiteWizardDrawer onClose={onClose} isOpen={true} currentStage="confirm">
        <GSuiteWizardLayout header="Create your organization" actions={mockActions}>
          {mockChild}
        </GSuiteWizardLayout>
      </GSuiteWizardDrawer>
    );
  });
