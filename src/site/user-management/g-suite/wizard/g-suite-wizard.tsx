import * as React from 'react';
import { InjectedIntlProps, injectIntl } from 'react-intl';

import { GSuiteStages, GSuiteWizardDrawer } from '../../g-suite/common/g-suite-wizard-drawer';
import { GSuiteWizardConnect } from '../steps/g-suite-wizard-step-connect';
import { GSuiteWizardLink } from '../steps/g-suite-wizard-step-link';

export interface GSuiteWizardProps {
  isDrawerOpen: boolean;
  cloudId?: string;
  onClose(): void;
}

interface GSuiteWizardState {
  currentStage: GSuiteStages;
}

export class GSuiteWizardImpl extends React.Component<GSuiteWizardProps & InjectedIntlProps, GSuiteWizardState> {

  public readonly state: Readonly<GSuiteWizardState> = {
    currentStage: 'connect',
  };

  public render() {

    return (
      <GSuiteWizardDrawer
        onClose={this.props.onClose}
        isOpen={this.props.isDrawerOpen}
        currentStage={this.state.currentStage}
      >
        {this.state.currentStage === 'connect' &&
          <GSuiteWizardConnect
            cloudId={this.props.cloudId}
            closeWizard={this.props.onClose}
            onNavStep={this.onNavStep}
          />
        }
        {this.state.currentStage === 'link' &&
          <GSuiteWizardLink
            cloudId={this.props.cloudId}
            closeWizard={this.props.onClose}
            onNavStep={this.onNavStep}
          />
        }
      </GSuiteWizardDrawer>
    );
  }

  private onNavStep = (step: GSuiteStages) => {
    this.setState({ currentStage: step });
  }
}

export const GSuiteWizard = injectIntl(GSuiteWizardImpl);
