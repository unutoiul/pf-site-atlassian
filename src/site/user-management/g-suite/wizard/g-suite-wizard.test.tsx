
import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';

import { createMockIntlProp } from '../../../../utilities/testing';

import { GSuiteWizardDrawer } from '../../g-suite/common/g-suite-wizard-drawer';

import { GSuiteWizardConnect } from '../steps/g-suite-wizard-step-connect';
import { GSuiteWizardImpl, GSuiteWizardProps } from './g-suite-wizard';

describe('G Suite Wizard Test', () => {

  function createTestComponent(props: GSuiteWizardProps): ShallowWrapper {
    return shallow(
      <GSuiteWizardImpl intl={createMockIntlProp()} {...props} />,
    );
  }

  it('Should have the drawer open', () => {
    const wrapper = createTestComponent({ isDrawerOpen: true, onClose: () => undefined });
    expect(wrapper.find(GSuiteWizardDrawer).exists()).to.equal(true);
    expect(wrapper.find(GSuiteWizardDrawer).prop('isOpen')).to.equal(true);
  });

  it('Should have the drawer closed', () => {
    const wrapper = createTestComponent({ isDrawerOpen: false, onClose: () => undefined });
    expect(wrapper.find(GSuiteWizardDrawer).exists()).to.equal(true);
    expect(wrapper.find(GSuiteWizardDrawer).prop('isOpen')).to.equal(false);
  });

  it('Should have the default state show the "connect" step', () => {
    const wrapper = createTestComponent({ isDrawerOpen: true, onClose: () => undefined });
    expect(wrapper.find(GSuiteWizardDrawer).exists()).to.equal(true);
    expect(wrapper.find(GSuiteWizardConnect).exists()).to.equal(true);
  });

  it('Should show the "connect" step when the state is explicitly "connect"', () => {
    let wrapper = createTestComponent({ isDrawerOpen: true, onClose: () => undefined });
    wrapper = wrapper.setState({ currentStage: 'connect' });
    expect(wrapper.find(GSuiteWizardDrawer).exists()).to.equal(true);
    expect(wrapper.find(GSuiteWizardConnect).exists()).to.equal(true);
  });
});
