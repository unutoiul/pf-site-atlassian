import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';

import { createMockAnalyticsClient, createMockIntlProp } from '../../../../utilities/testing';
import { GSuiteWizardConnect } from './g-suite-wizard-step-connect';

const pageDefaults = {
  cloudId: 'DUMMY_CLOUD_ID',
  intl: createMockIntlProp(),
  closeWizard: () => null,
  onNavStep: () => null,
  dispatch: (_: any): any => null,
  analyticsClient: createMockAnalyticsClient(),
};
const pageDecorator = (storyFn) => (
  <IntlProvider locale="en">
    {storyFn()}
  </IntlProvider>
);

storiesOf('Site|G Suite/Steps', module)
  .addDecorator(pageDecorator)
  .add('Connect', () => {
    return (
      <GSuiteWizardConnect {...pageDefaults}/>
    );
  });
