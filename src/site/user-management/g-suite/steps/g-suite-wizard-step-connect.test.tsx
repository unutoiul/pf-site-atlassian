
import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { IntlProvider } from 'react-intl';
import * as sinon from 'sinon';

import { createMockIntlProp, noop, waitUntil } from '../../../../utilities/testing';

import { GSuiteWizardLayout } from '../../g-suite/common/g-suite-wizard-layout';
import { ConnectionStates, GSuiteAuthentication } from './g-suite-wizard-step-authentication';
import { GSuiteWizardConnectImpl, messages } from './g-suite-wizard-step-connect';

describe('G Suite Wizard - Connect step', () => {

  const sandbox = sinon.sandbox.create();

  let mutateStub: sinon.SinonStub;
  let showFlagStub: sinon.SinonStub;
  let nextStepStub: sinon.SinonStub;
  let closeWizardStub: sinon.SinonStub;

  beforeEach(() => {
    mutateStub = sandbox.stub().resolves({ data: { getConnection: { url: 'nothing' } } });
    sandbox.stub(window, 'open');
    showFlagStub = sandbox.stub();
    closeWizardStub = sandbox.stub();
    nextStepStub = sandbox.stub();
  });

  afterEach(() => {
    sandbox.restore();
    sandbox.reset();
  });

  interface TestComponentProps {
    cloudId?: string;
    mutate?: sinon.SinonStub;
  }

  function createTestComponent(props?: TestComponentProps): ShallowWrapper {
    return shallow(
      <GSuiteWizardConnectImpl
        intl={createMockIntlProp()}
        mutate={mutateStub}
        showFlag={showFlagStub}
        hideFlag={noop}
        onNextStep={nextStepStub}
        closeWizard={closeWizardStub}
        {...props}
      />,
    );
  }

  describe('When the "Connect" Google button is clicked', () => {
    it('Should not call mutate if the cloud id does not exist', () => {
      const wrapper = createTestComponent({ cloudId: undefined, mutate: mutateStub });
      const action = wrapper.find(GSuiteWizardLayout).prop('actions')[0];
      const button = shallow(<IntlProvider>{action}</IntlProvider>);
      button.simulate('click');
      expect(mutateStub.called).to.equal(false);
    });

    it('Should throw an error flag and close the drawer if the mutation throws an error', async () => {
      mutateStub.rejects();
      const wrapper = createTestComponent({ cloudId: '54637' });
      const action = wrapper.find(GSuiteWizardLayout).prop('actions')[0];
      const button = shallow(<IntlProvider>{action}</IntlProvider>);
      button.simulate('click');
      expect(mutateStub.called).to.equal(true);
      await waitUntil(() => showFlagStub.callCount > 0);
      expect(showFlagStub.calledWithMatch({
        title: messages.connectErrorTitle.defaultMessage,
        description: messages.connectErrorDescription.defaultMessage,
      })).to.equal(true);
    });

    describe('When the mutation is successfully called', () => {

      function authenticationReturns(state: ConnectionStates) {
        sandbox.stub(GSuiteAuthentication, 'create').returns({
          pollConnectionState: async () => Promise.resolve(state),
        });
      }

      function authenticationResolves() {
        const wrapper = createTestComponent({ cloudId: '54637' });
        const action = wrapper.find(GSuiteWizardLayout).prop('actions')[0];
        const button = shallow(<IntlProvider>{action}</IntlProvider>);
        button.simulate('click');
        expect(wrapper.state('isAuthenticating')).to.equal(true);
        expect(mutateStub.called).to.equal(true);

        return wrapper;
      }

      it('Should call mutate with the correct variables', () => {
        const wrapper = createTestComponent({ cloudId: '54637' });
        const action = wrapper.find(GSuiteWizardLayout).prop('actions')[0];
        const button = shallow(<IntlProvider>{action}</IntlProvider>);
        button.simulate('click');
        expect(mutateStub.called).to.equal(true);
        expect(mutateStub.calledWithMatch({ }));
      });

      it('Should go to the next wizard step if a mutation performs and authentication is successful', async () => {
        authenticationReturns(ConnectionStates.SUCCESS);
        const wrapper = authenticationResolves();
        await waitUntil(() => nextStepStub.callCount === 1);
        expect(wrapper.state('isAuthenticating')).to.equal(false);
      });

      it('Should display an error flag if mutation performs, but authentication returns back in an ABORTED state', async () => {
        authenticationReturns(ConnectionStates.ABORTED);
        const wrapper = authenticationResolves();
        await waitUntil(() => showFlagStub.callCount > 0);
        expect(showFlagStub.calledWithMatch({
          title: messages.connectErrorTitle.defaultMessage,
          description: messages.connectErrorDescriptionAborted.defaultMessage,
        })).to.equal(true);
        expect(closeWizardStub.called).to.equal(true);
        expect(wrapper.state('isAuthenticating')).to.equal(false);
      });

      it('Should display an error flag if mutation performs, but authentication returns back an ORG PERMISSION DENIED state', async () => {
        authenticationReturns(ConnectionStates.ORG_PERMISSION_DENIED);
        const wrapper = authenticationResolves();
        await waitUntil(() => showFlagStub.callCount > 0);
        expect(showFlagStub.calledWithMatch({
          title: messages.connectErrorTitle.defaultMessage,
          description: messages.connectErrorDescriptionOrgUnauthorized.defaultMessage,
        })).to.equal(true);
        expect(closeWizardStub.called).to.equal(true);
        expect(wrapper.state('isAuthenticating')).to.equal(false);
      });

      it('Should display an error flag if mutation performs, but authentication returns back an UNAUTHORIZED state', async () => {
        authenticationReturns(ConnectionStates.UNAUTHORIZED);
        const wrapper = authenticationResolves();
        await waitUntil(() => showFlagStub.callCount > 0);
        expect(showFlagStub.calledWithMatch({
          title: messages.connectErrorTitle.defaultMessage,
          description: messages.connectErrorDescriptionUnauthorized.defaultMessage,
        })).to.equal(true);
        expect(closeWizardStub.called).to.equal(true);
        expect(wrapper.state('isAuthenticating')).to.equal(false);
      });

      it('Should display an error flag if mutation performs, but authentication returns back an UNKNOWN_REASON state', async () => {
        authenticationReturns(ConnectionStates.UNKNOWN_REASON);
        const wrapper = authenticationResolves();
        await waitUntil(() => showFlagStub.callCount > 0);
        expect(showFlagStub.calledWithMatch({
          title: messages.connectErrorTitle.defaultMessage,
          description: messages.connectErrorDescription.defaultMessage,
        })).to.equal(true);
        expect(closeWizardStub.called).to.equal(true);
        expect(wrapper.state('isAuthenticating')).to.equal(false);
      });

      it('Should display an error flag if mutation performs, but authentication returns back an TIMED_OUT state', async () => {
        authenticationReturns(ConnectionStates.TIMED_OUT);
        const wrapper = authenticationResolves();
        await waitUntil(() => showFlagStub.callCount > 0);
        expect(showFlagStub.calledWithMatch({
          title: messages.connectErrorTitle.defaultMessage,
          description: messages.connectErrorDescriptionTimedOut.defaultMessage,
        })).to.equal(true);
        expect(closeWizardStub.called).to.equal(true);
        expect(wrapper.state('isAuthenticating')).to.equal(false);
      });

    });
  });

  describe('When UI elements are shown on the drawer', () => {
    it('Should have an action', () => {
      const wrapper = createTestComponent();
      expect(wrapper.find(GSuiteWizardLayout).exists());

      const actions = wrapper.find(GSuiteWizardLayout).prop('actions');
      expect(actions.length).to.equal(1);
    });

    it('Should have a title', () => {
      const wrapper = createTestComponent();
      expect(wrapper.find(GSuiteWizardLayout).exists());
      expect(wrapper.find(GSuiteWizardLayout).prop('header')).to.equal('Connect your G Suite');
    });

    it('Should have a description', () => {
      const wrapper = createTestComponent();
      expect(wrapper.find(GSuiteWizardLayout).exists());
      expect(wrapper.find(GSuiteWizardLayout).children().at(0).text()).to.contain(messages.description1.defaultMessage);
      expect(wrapper.find(GSuiteWizardLayout).children().at(1).text()).to.contain(messages.description2.defaultMessage);
    });
  });

});
