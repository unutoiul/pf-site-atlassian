import { getConfig } from 'common/config';

export const enum ConnectionStates {
  UNAUTHORIZED,
  UNKNOWN_REASON,
  TIMED_OUT,
  ORG_PERMISSION_DENIED,
  ABORTED,
  SUCCESS,
}

export class GSuiteAuthentication {

  constructor(private authenticationWindow: Window | null, private cloudId: string) {
  }

  public pollConnectionState = async (): Promise<ConnectionStates> => {
    const endTime = Date.now() + 45000;

    const checkCondition = async (resolve, reject) => {
      const result = this.getConnectionState();
      if (result !== undefined) {
        return resolve(result);
      } else if (Date.now() < endTime) {
        setTimeout(checkCondition, 500, resolve, reject);
      } else {
        return ConnectionStates.TIMED_OUT;
      }
    };

    return new Promise<ConnectionStates>(checkCondition);
  };

  private getConnectionState = (): ConnectionStates | undefined => {
    try {
      if (!this.authenticationWindow || this.authenticationWindow.closed) {
        return ConnectionStates.ABORTED;
      }

      // Obtaining the pathname while Google authentication occurs will result in a cross-origin error.
      // After it authenticates, the callback will be of the same origin. Hence, this error will be caught,
      // ignored, and polling will still continue.
      const { pathname } = this.authenticationWindow.location;

      const basePath = `${getConfig().gSuiteBasePath}/${this.cloudId}/callback`;
      const legacyBasePath = `${getConfig().gSuiteBasePathLegacy}/${this.cloudId}/callback`;

      if (pathname === `${basePath}/unauthorised` || pathname === `${legacyBasePath}/unauthorised`) {
        return ConnectionStates.UNAUTHORIZED;
      } else if (pathname === `${basePath}/unknownreason` || pathname === `${legacyBasePath}/unknownreason`) {
        return ConnectionStates.UNKNOWN_REASON;
      } else if (pathname === `${basePath}/orgunauthorised` || pathname === `${legacyBasePath}/orgunauthorised`) {
        return ConnectionStates.ORG_PERMISSION_DENIED;
      } else if (pathname === basePath || pathname === legacyBasePath) {
        return ConnectionStates.SUCCESS;
      }

      return undefined;
    } catch (e) {
      return undefined;
    }
  };

  public static create(authenticationWindow: Window | null, cloudId: string): GSuiteAuthentication {
    return new GSuiteAuthentication(authenticationWindow, cloudId);
  }
}
