// tslint:disable:jsx-use-translation-function
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider, DataValue } from 'react-apollo';
import { IntlProvider } from 'react-intl';
import { MemoryRouter } from 'react-router-dom';

import AkPage from '@atlaskit/page';

import { FlagProvider } from 'common/flag';

import { createApolloClient } from '../../../../apollo-client';
import { GSuitePageQuery } from '../../../../schema/schema-types';
import { createMockAnalyticsClient, createMockIntlProp } from '../../../../utilities/testing';
import { GSuiteWizardLinkImpl } from './g-suite-wizard-step-link';
const client = createApolloClient();

const pageDefaults = {
  cloudId: 'DUMMY_CLOUD_ID',
  intl: createMockIntlProp(),
  closeWizard: () => null,
  onNavStep: () => null,
  dispatch: (_: any): any => null,
  analyticsClient: createMockAnalyticsClient(),
};

const mockDataNoConnection: Partial<DataValue<GSuitePageQuery, any>> = {
  loading: false,
  site: {
    __typename: 'Site',
    id: '1',
    gSuite: {
      __typename: 'gSuite',
      state: null,
      groups: [
        {  __typename: 'group', id: 'Adelaide', name: 'Adelaide' },
        {  __typename: 'group', id: 'Sydney', name: 'Sydney' },
        {  __typename: 'group', id: 'Melbourne', name: 'Melbourne' },
        {  __typename: 'group', id: 'Perth', name: 'Perth' },
      ],
    },
  },
};

const Wrapper = ({ children }) => (
  <ApolloProvider client={client}>
    <IntlProvider locale="en">
      <MemoryRouter>
        <FlagProvider>
            <AkPage>
              {children}
            </AkPage>
        </FlagProvider>
      </MemoryRouter>
    </IntlProvider>
  </ApolloProvider>
);
const pageDecorator = (storyFn) => (
  <IntlProvider locale="en">
    {storyFn()}
  </IntlProvider>
);

storiesOf('Site|G Suite/Steps', module)
  .addDecorator(pageDecorator)
  .add('Link', () => (
    <Wrapper>
      <GSuiteWizardLinkImpl data={mockDataNoConnection as any} {...pageDefaults}/>
    </Wrapper>
));
