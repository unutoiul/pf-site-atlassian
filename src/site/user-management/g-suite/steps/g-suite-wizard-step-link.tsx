import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import AkButton from '@atlaskit/button';
import { AkRadio } from '@atlaskit/field-radio-group';
import AkSelect from '@atlaskit/select';
import AkSpinner from '@atlaskit/spinner';
import { colors as AkColors, gridSize as akGridSize } from '@atlaskit/theme';

import {
  AnalyticsClientProps,
  withAnalyticsClient,
} from 'common/analytics';

import { GSuiteStages } from '../../g-suite/common/g-suite-wizard-drawer';

import { GSuitePageQuery } from '../../../../schema/schema-types';
import { GSuiteWizardLayout } from '../../g-suite/common/g-suite-wizard-layout';
import { Field, Fieldset, FieldsetLabel } from '../../site-access/shared-styles';
import getGSuiteState from '../g-suite-page.query.graphql';

export const USERS = 'users';
export const GROUPS = 'groups';
export const messages = defineMessages({
  title: {
    id: 'organization.member.reactivation.link.title',
    defaultMessage: 'Which users do you want to sync?',
    description: 'Title of GSuiteLink step',
  },

  error: {
    id: 'user-management.site-settings.g-suite.error',
    defaultMessage: 'G Suite configuration could not be retrieved.',
    description: 'Error that shows when G Suite information could not be retrieved.',
  },
  description: {
    id: 'organization.member.reactivation.link.description',
    defaultMessage: 'Choose to sync all users or specific groups. We\'ll save this as your sync settings but you can change it at any time.',
    description: 'Description of GSuiteLink step',
  },
  multiSelectDefault: {
    id: 'organization.member.reactivation.link.multiselect.default',
    defaultMessage: 'Choose a group...',
    description: 'Multiselect default value in GSuiteLink step',
  },
  radioAllUsersTitle: {
    id: 'organization.member.reactivation.link.radioall.name',
    defaultMessage: 'Sync all users',
    description: 'Radio all button title for sync all user in GSuiteLink step',
  },
  radioAllUsersDesc: {
    id: 'organization.member.reactivation.link.radioall.description',
    defaultMessage: 'We\'ll automatically sync all existing users and users who join your team in the future.',
    description: 'Radio all button description for sync all user in GSuiteLink step',
  },
  radioLinkTitle: {
    id: 'organization.member.reactivation.link.radioall.name',
    defaultMessage: 'Select specific groups',
    description: 'Radio specific button title for sync all user in GSuiteLink step',
  },
  radioLinkDesc: {
    id: 'organization.member.reactivation.link.radiospecific.description',
    defaultMessage: 'We\'ll automatically sync all existing members of selected groups and users added to them in the future.',
    description: 'Radio specific button description for sync all user in GSuiteLink step',
  },
  pickerTitle: {
    id: 'organization.member.reactivation.link.picker.title',
    defaultMessage: 'G Suits Groups',
    description: 'Picker title in GSuiteLink step',
  },
  continueButton: {
    id: 'organization.member.reactivation.link.continue.button',
    defaultMessage: 'Continue',
    description: 'Continue button for GSuiteLink step',
  },
  previousButton: {
    id: 'organization.member.reactivation.link.previous.button',
    defaultMessage: 'Back',
    description: 'Back button for GSuiteLink step',
  },
});

const Description = styled.div`
  margin-bottom: ${akGridSize() * 2}px;
`;
const RadioDescription = styled.div`
  margin-left: ${akGridSize() * 3}px;
  color: ${AkColors.subtleHeading};
`;

const GroupRadioContaier = styled.div`
  margin: 0 ${akGridSize() * 6}px;

  label > div > span:last-child{
    font-weight: bold;
  }
`;
const PickerContainer = styled.div`
  padding-left: ${akGridSize() * 3}px;
  margin-top: ${akGridSize() * 2}px;
`;
const StyledError = styled.div`
  margin-bottom: 20px;
`;

interface GSuiteWizardStepLinkState {
  selectType: string;
}
interface GSuiteWizardStepLinkProps {
  cloudId?: string;
  closeWizard(): void;
  onNavStep(step: GSuiteStages): void;
}

type GSuitePageProps = ChildProps<InjectedIntlProps, & GSuitePageQuery> & AnalyticsClientProps & GSuiteWizardStepLinkProps;

export class GSuiteWizardLinkImpl extends React.Component<GSuitePageProps, GSuiteWizardStepLinkState> {

  public state: Readonly<GSuiteWizardStepLinkState> = {
    selectType: USERS,
  };

  public render() {
    const {
      data,
      intl: { formatMessage },
    } = this.props;

    const { selectType } = this.state;
    const gSuiteGroups = !data || !data.site || !data.site.gSuite || !data.site.gSuite.groups || data.site.gSuite.groups;
    const error = !data || data.error;
    const loading = !data || data.loading;

    const actions: React.ReactNode[] = [
      <AkButton key="previous" onClick={this.onPrevious}>{formatMessage(messages.previousButton)}</AkButton>,
      <AkButton key="continue" appearance="primary" onClick={this.onContinue}>{formatMessage(messages.continueButton)}</AkButton>,
    ];

    return (
      <GSuiteWizardLayout header={this.props.intl.formatMessage(messages.title)} actions={actions}>
        < Description >
          {
            formatMessage(messages .description)
          }
        </Description>
        {loading ? <AkSpinner /> : (error ? (
          <StyledError className="error">{formatMessage(messages.error)}</StyledError>
        ) : (<GroupRadioContaier>
          <Fieldset>
              <Field >
                <AkRadio
                  isSelected={selectType === USERS}
                  value={USERS}
                  onChange={this.onRadioChange}
                >
                  {formatMessage(messages.radioAllUsersTitle)}
                </AkRadio>
                <RadioDescription>
                  {formatMessage(messages.radioAllUsersDesc)}
                </RadioDescription>
            </Field>
            <Field>
              <AkRadio
                isSelected={selectType === GROUPS}
                value={GROUPS}
                onChange={this.onRadioChange}
              >
                {formatMessage(messages.radioLinkTitle)}
              </AkRadio>
              <RadioDescription>
                {formatMessage(messages.radioLinkDesc)}
              </RadioDescription>
            </Field>
            <Field>
              {
                this.state.selectType === GROUPS &&
                  <PickerContainer>
                      <FieldsetLabel>{formatMessage(messages.pickerTitle)}</FieldsetLabel>
                      {gSuiteGroups !== true && <AkSelect
                          options={getList(gSuiteGroups)}
                          isMulti={true}
                          isSearchable={true}
                          placeholder={formatMessage(messages.multiSelectDefault)}
                      />}

                  </PickerContainer>
              }
            </Field>
          </Fieldset>
        </GroupRadioContaier>))
        }
      </GSuiteWizardLayout>
    );
  }

  private onPrevious = () => {
    this.props.onNavStep('connect');
  }
  private onContinue = () => {
    this.props.onNavStep('confirm');
  }

  private onRadioChange = (event: any) => {
    const selectType = event.target.value;
    this.setState({
      selectType,
    });
  };
}

const withGSuiteState = graphql<GSuitePageProps, GSuitePageQuery>(getGSuiteState, {
  options: ({ cloudId }) => ({ variables: { cloudId } }),
  skip: ({ cloudId }) => !cloudId,
});

export function getList(list: any) {
  return list.map((item: any) => {
    return({
      label: item.name,
      value: item.name,
    });
  });
}

export const GSuiteWizardLink = (
  injectIntl(
    withAnalyticsClient(
      withGSuiteState(
        GSuiteWizardLinkImpl,
      ),
    ),
  )
);
