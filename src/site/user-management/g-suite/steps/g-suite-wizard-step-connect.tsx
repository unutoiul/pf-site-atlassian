import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import { GetGSuiteConnectionMutation } from 'src/schema/schema-types';

import { FlagProps, withFlag } from 'common/flag';

import { createErrorIcon } from 'common/error';

import { GSuiteStages } from '../../g-suite/common/g-suite-wizard-drawer';

import { GSuiteWizardButton } from '../../g-suite/common/g-suite-wizard-button';
import { GSuiteWizardLayout } from '../../g-suite/common/g-suite-wizard-layout';

import { ConnectionStates, GSuiteAuthentication } from './g-suite-wizard-step-authentication';
import GetGSuiteConnection from './g-suite-wizard-step-connect.mutation.graphql';

export const messages = defineMessages({
  title: {
    id: 'user-management.site-settings.g-suite.wizard.title',
    description: 'Title of the page showing G Suite information.',
    defaultMessage: 'Connect your G Suite',
  },
  description1: {
    id: 'user-management.site-settings.g-suite.wizard.description.1',
    description: 'First paragraph describing the first step of connecting G Suite to the instance.',
    defaultMessage: 'You will be asked by Google to confirm you allow Atlassian to import your users.',
  },
  description2: {
    id: 'user-management.site-settings.g-suite.wizarddescription.2',
    description: 'Second paragraph describing the first step of connecting G Suite to the instance.',
    defaultMessage: 'You need to be a G Suite administrator to connect it.',
  },
  connectErrorTitle: {
    id: 'user-management.site-settings.g-suite.connect.error.title',
    description: 'Second paragraph describing the first step of connecting G Suite to the instance.',
    defaultMessage: 'We could not connect your G Suite instance',
  },
  connectErrorDescription: {
    id: 'user-management.site-settings.g-suite.connect.error.description.generic',
    description: 'Error flag shown when the user cannot connect their G Suite instance to the site',
    defaultMessage: 'Refresh the page to try again, or contact support if this issue persists further.',
  },
  connectErrorDescriptionAborted: {
    id: 'user-management.site-settings.g-suite.connect.error.description.aborted',
    description: 'Error flag shown when the user cannot connect their G Suite instance to the site',
    defaultMessage: 'Google authentication was aborted. Please click "Configure G Suite" to try again.',
  },
  connectErrorDescriptionUnauthorized: {
    id: 'user-management.site-settings.g-suite.connect.error.description.unauthorized',
    description: 'Error flag shown when the user cannot connect their G Suite instance to the site',
    defaultMessage: 'This Google account is not authorized to connect a G Suite instance to this site. Please click "Configure G Suite" to try again.',
  },
  connectErrorDescriptionOrgUnauthorized: {
    id: 'user-management.site-settings.g-suite.connect.error.description.org.unauthorized',
    description: 'Error flag shown when the user cannot connect their G Suite instance to the site',
    defaultMessage: 'This G Suite instance is already connected to another organization.',
  },
  connectErrorDescriptionTimedOut: {
    id: 'user-management.site-settings.g-suite.connect.error.description.time.out',
    description: 'Error flag shown when the user cannot connect their G Suite instance to the site',
    defaultMessage: 'The operation to authenticate with Google timed out. Please click "Configure G Suite" to try again.',
  },
});

export interface GSuiteWizardConnectionProps {
  cloudId?: string;
  closeWizard(): void;
  onNavStep(step: GSuiteStages): void;
}

interface GSuiteWizardConnectState {
  isAuthenticating: boolean;
}

export type GSuiteWizardConnectProps = ChildProps<FlagProps & InjectedIntlProps & GSuiteWizardConnectionProps, GetGSuiteConnectionMutation>;

export class GSuiteWizardConnectImpl extends React.Component<GSuiteWizardConnectProps, GSuiteWizardConnectState> {

  public readonly state: Readonly<GSuiteWizardConnectState> = {
    isAuthenticating: false,
  };

  public render() {

    const actions = [
      // Atlaskit's button loading state will render a spinner, and does not align with Google's design guidelines
      // For this button. Hence, the loading state is represented through the disabled prop.
      (<GSuiteWizardButton onClick={this.onClick} isDisabled={this.state.isAuthenticating}/>),
    ];

    return (
      <GSuiteWizardLayout header={this.props.intl.formatMessage(messages.title)} actions={actions}>
        <p>{this.props.intl.formatMessage(messages.description1)}</p>
        <p>{this.props.intl.formatMessage(messages.description2)}</p>
      </GSuiteWizardLayout>
    );
  }

  private onClick = () => {

    if (!this.props.mutate || !this.props.cloudId) {
      return;
    }

    this.setState({ isAuthenticating: true });

    this.props.mutate({
      variables: {
        cloudId: this.props.cloudId,
      },
    }).then(async response => {
      const popup = window.open(response.data.getConnection.url);
      const authentication = GSuiteAuthentication.create(popup, this.props.cloudId!);
      const result = await authentication.pollConnectionState();
      this.closeWindow(popup);
      this.showAuthenticationState(result);
      this.setState({ isAuthenticating: false });
    }).catch(() => {
      this.props.closeWizard();
      const errorDescription = this.props.intl.formatMessage(messages.connectErrorDescription);
      this.showErrorFlag(errorDescription);
      this.setState({ isAuthenticating: false });
    });
  };

  private showAuthenticationState = (connectionState: ConnectionStates) => {
    if (connectionState === ConnectionStates.SUCCESS) {
      this.props.onNavStep('organization');
    } else {
      this.showErrorFlag(this.getErrorFlagDescription(connectionState));
      this.props.closeWizard();
    }
  }

  private getErrorFlagDescription = (state: ConnectionStates): string => {
    const { formatMessage } = this.props.intl;

    if (state === ConnectionStates.ABORTED) {
      return formatMessage(messages.connectErrorDescriptionAborted);
    } else if (state === ConnectionStates.ORG_PERMISSION_DENIED) {
      return formatMessage(messages.connectErrorDescriptionOrgUnauthorized);
    } else if (state === ConnectionStates.UNAUTHORIZED) {
      return formatMessage(messages.connectErrorDescriptionUnauthorized);
    } else if (state === ConnectionStates.TIMED_OUT) {
      return formatMessage(messages.connectErrorDescriptionTimedOut);
    }

    return formatMessage(messages.connectErrorDescription);
  }

  private showErrorFlag = (description: string): void => {
    this.props.showFlag({
      autoDismiss: true,
      icon: createErrorIcon(),
      id: `user-management.g-suite.connect.error.flag.${Date.now()}`,
      title: this.props.intl.formatMessage(messages.connectErrorTitle),
      description,
    });
  }

  private closeWindow = (popup: Window | null) => {
    if (popup) {
      popup.close();
    }
  }
}

const withCreateGroupMutation = graphql<GSuiteWizardConnectionProps, GetGSuiteConnectionMutation>(GetGSuiteConnection);
export const GSuiteWizardConnect = withCreateGroupMutation(injectIntl(withFlag(GSuiteWizardConnectImpl)));
