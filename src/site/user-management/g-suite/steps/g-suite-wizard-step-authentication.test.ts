import { expect } from 'chai';
import { sandbox as sinonSandbox, SinonSandbox } from 'sinon';

import { getConfig } from 'common/config';

import { ConnectionStates, GSuiteAuthentication } from './g-suite-wizard-step-authentication';

describe('Should return authorized', () => {

  const cloudId: string = 'DUMMY_CLOUD_ID';
  const sandbox: SinonSandbox = sinonSandbox.create();

  let windowMock;

  beforeEach(() => {
    windowMock = sandbox.stub();
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('When the authentication window is closed or does not exist', () => {

    it('Should return ABORTED status when the window does not exist', async () => {
      const authentication = new GSuiteAuthentication(null, cloudId);
      const result = await authentication.pollConnectionState();
      expect(result).to.equal(ConnectionStates.ABORTED);
    });

    it('Should return ABORTED status when the window is closed', async () => {
      windowMock.closed = true;
      const authentication = new GSuiteAuthentication(windowMock, cloudId);
      const result = await authentication.pollConnectionState();
      expect(result).to.equal(ConnectionStates.ABORTED);
    });

  });

  describe('When the authentication window resolves a legacy URL', () => {

    it('Should return SUCCESS status when the callback returns the base path', async () => {
      windowMock.location = { pathname: `${getConfig().gSuiteBasePathLegacy}/${cloudId}/callback` };
      const authentication = new GSuiteAuthentication(windowMock, cloudId);
      const result = await authentication.pollConnectionState();
      expect(result).to.equal(ConnectionStates.SUCCESS);
    });

    it('Should return UNAUTHORIZED status when the callback returns the unauthorized flag', async () => {
      windowMock.location = { pathname: `${getConfig().gSuiteBasePathLegacy}/${cloudId}/callback/unauthorised` };
      const authentication = new GSuiteAuthentication(windowMock, cloudId);
      const result = await authentication.pollConnectionState();
      expect(result).to.equal(ConnectionStates.UNAUTHORIZED);
    });

    it('Should return UNKNOWN_REASON status when the callback returns the unknownreason flag', async () => {
      windowMock.location = { pathname: `${getConfig().gSuiteBasePathLegacy}/${cloudId}/callback/unknownreason` };
      const authentication = new GSuiteAuthentication(windowMock, cloudId);
      const result = await authentication.pollConnectionState();
      expect(result).to.equal(ConnectionStates.UNKNOWN_REASON);
    });

    it('Should return ORG_PERMISSION_DENIED status when the callback returns the orgunauthorized flag', async () => {
      windowMock.location = { pathname: `${getConfig().gSuiteBasePathLegacy}/${cloudId}/callback/orgunauthorised` };
      const authentication = new GSuiteAuthentication(windowMock, cloudId);
      const result = await authentication.pollConnectionState();
      expect(result).to.equal(ConnectionStates.ORG_PERMISSION_DENIED);
    });
  });

  describe('When the authentication window resolves a URL', () => {

    it('Should return SUCCESS status when the callback returns the base path', async () => {
      windowMock.location = { pathname: `${getConfig().gSuiteBasePath}/${cloudId}/callback` };
      const authentication = new GSuiteAuthentication(windowMock, cloudId);
      const result = await authentication.pollConnectionState();
      expect(result).to.equal(ConnectionStates.SUCCESS);
    });

    it('Should return UNAUTHORIZED status when the callback returns the unauthorized flag', async () => {
      windowMock.location = { pathname: `${getConfig().gSuiteBasePath}/${cloudId}/callback/unauthorised` };
      const authentication = new GSuiteAuthentication(windowMock, cloudId);
      const result = await authentication.pollConnectionState();
      expect(result).to.equal(ConnectionStates.UNAUTHORIZED);
    });

    it('Should return UNKNOWN_REASON status when the callback returns the unknownreason flag', async () => {
      windowMock.location = { pathname: `${getConfig().gSuiteBasePath}/${cloudId}/callback/unknownreason` };
      const authentication = new GSuiteAuthentication(windowMock, cloudId);
      const result = await authentication.pollConnectionState();
      expect(result).to.equal(ConnectionStates.UNKNOWN_REASON);
    });

    it('Should return ORG_PERMISSION_DENIED status when the callback returns the orgunauthorized flag', async () => {
      windowMock.location = { pathname: `${getConfig().gSuiteBasePath}/${cloudId}/callback/orgunauthorised` };
      const authentication = new GSuiteAuthentication(windowMock, cloudId);
      const result = await authentication.pollConnectionState();
      expect(result).to.equal(ConnectionStates.ORG_PERMISSION_DENIED);
    });
  });
});
