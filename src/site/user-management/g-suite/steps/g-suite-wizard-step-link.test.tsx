import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import { AkRadio } from '@atlaskit/field-radio-group';
import AkSelect from '@atlaskit/select';
import AkSpinner from '@atlaskit/spinner';

import { createMockIntlProp } from '../../../../utilities/testing';
import {
  getList,
  GROUPS,
  GSuiteWizardLinkImpl,
  messages,
  USERS,
   } from './g-suite-wizard-step-link';

const mockData = {
  site: {
    id: '1',
    gSuite: {
      state: null,
      groups: [
        { id: 'Adelaide', name: 'Adelaide' },
        { id: 'Sydney', name: 'Sydney' },
        { id: 'Melbourne', name: 'Melbourne' },
        { id: 'Perth', name: 'Perth' },
      ],
    },
  },
};

// tslint:disable-next-line:ban
describe('G Suite WizardLink Step', () => {
  function createTestComponent(data?: any) {
    return shallow(
      <GSuiteWizardLinkImpl
        intl={createMockIntlProp()}
        cloudId="DUMMY_CLOUD_ID"
        data={(data) ? data : mockData}
        {...{} as any}
      />);
  }

  it('shows spinner when loading data', () => {
    const wrapper = createTestComponent({ loading: true, error: false });
    const spinner = wrapper.find(AkSpinner);
    expect(spinner.length).to.equal(1);
  });

  it('renders an error on graphql error', () => {
    const wrapper = createTestComponent({ loading: false, error: true });
    expect(wrapper.find('.error')).to.have.lengthOf(1);
    expect(wrapper.find('.error').render().text()).to.equal(messages.error.defaultMessage);
  });

  it('check if correct radios exists', () => {
    const wrapper = createTestComponent();
    const radios = wrapper.find(AkRadio);
    expect(radios.length).to.equal(2);
    const values = [USERS, GROUPS];
    radios.forEach((radio, i) => {
      expect(radio.prop('value'), `${radio.prop('name')} radio (value: ${radio.prop('value')}) should be disabled`).to.equal(values[i]);
    });
  });

  it('check if correct select exists', () => {
    const wrapper = createTestComponent();
    wrapper.setState({ selectType: GROUPS });
    const select = wrapper.find(AkSelect);
    expect(select.length).to.equal(1);
    // radios.forEach((radio, i) => {
    //   expect(radio.prop('value'), `${radio.prop('name')} radio (value: ${radio.prop('value')}) should be disabled`).to.equal(values[i]);
    // });
  });

  it('check if get list is mutated correctly', () => {
    const result = getList(mockData.site.gSuite.groups);
    expect(result).to.deep.equal(result);
    expect(result).to.deep.include({ label: 'Perth', value: 'Perth' });
  });
});
