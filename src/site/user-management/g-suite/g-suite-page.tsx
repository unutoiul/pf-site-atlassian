import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import { connect, DispatchProp } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router';
import styled from 'styled-components';

import AkButton from '@atlaskit/button';
import AkSpinner from '@atlaskit/spinner';
import { gridSize as akGridSize } from '@atlaskit/theme';

import {
  AnalyticsClientProps,
  gSuiteScreenScreenEvent,
  ScreenEventSender,
  withAnalyticsClient,
} from 'common/analytics';
import { FlagProps, withFlag } from 'common/flag';
import { catchInNetImage } from 'common/images';
import { links } from 'common/link-constants';
import { ExternalLink, NavigationDrawer, openDrawer } from 'common/navigation';
import { PageLayout, PageSidebar } from 'common/page-layout';

import { GSuitePageQuery, GSuitePageQueryVariables } from '../../../schema/schema-types';
import getGSuiteState from './g-suite-page.query.graphql';

export const messages = defineMessages({
  title: {
    id: 'user-management.site-settings.g-suite.title',
    defaultMessage: 'G Suite',
    description: 'Title of the page showing G Suite information.',
  },
  description: {
    id: 'user-management.site-settings.g-suite.description',
    defaultMessage: 'Connect your G Suite to import users to your Atlassian site.',
    description: 'Description of the G Suite features.',
  },
  error: {
    id: 'user-management.site-settings.g-suite.error',
    defaultMessage: 'G Suite configuration could not be retrieved.',
    description: 'Error that shows when G Suite information could not be retrieved.',
  },
  sidebarTitle: {
    id: 'user-management.site-settings.g-suite.sidebar.title',
    defaultMessage: 'What you need to know',
    description: 'Title of the sidebar showing contextual G Suite help.',
  },
  sidebarDisconnectedPoint1: {
    id: 'user-management.site-settings.g-suite.sidebar.disconnected.point1',
    defaultMessage: 'Connect to G Suite to import your users – either all or some of them.',
    description: 'Sidebar point describing the advantage of connecting G Suite.',
  },
  sidebarDisconnectedPoint2: {
    id: 'user-management.site-settings.g-suite.sidebar.disconnected.point2',
    defaultMessage: `If you import new users, we'll create accounts for them automatically.`,
    description: 'Sidebar point describing what happens when new users are imported.',
  },
  sidebarDisconnectedPoint3: {
    id: 'user-management.site-settings.g-suite.sidebar.disconnected.point3',
    defaultMessage: `Update your imported users' details in G Suite – we'll sync these changes for you on our end.`,
    description: 'Sidebar point describing what happens when user details are updated in G Suite.',
  },
  sidebarDisconnectedPoint4: {
    id: 'user-management.site-settings.g-suite.sidebar.disconnected.point4',
    defaultMessage: `We'll cancel any domains you've verified with us and start managing your G Suite domains automatically.`,
    description: 'Sidebar point describing what happens to domain claims when G Suite is connected.',
  },
  sidebarMoreInfoPoint: {
    id: 'user-management.site-settings.g-suite.sidebar.more.point',
    defaultMessage: 'Need more info? Check out the {sidebarMoreInfoPointLink}.',
    description: 'Sidebar point containing a link to more help.',
  },
  sidebarMoreInfoPointLink: {
    id: 'user-management.site-settings.g-suite.sidebar.more.point.link',
    defaultMessage: 'G Suite documentation',
    description: 'Link to more G Suite help.',
  },
  disconnectedTitle: {
    id: 'user-management.site-settings.g-suite.disconnected.title',
    defaultMessage: `G Suite isn't configured`,
    description: 'Subtitle that shows when G Suite is not configured.',
  },
  disconnectedDescription: {
    id: 'user-management.site-settings.g-suite.disconnected.description',
    defaultMessage: `G Suite configuration is two simple steps: you connect your G Suite (you need to be an admin there), and select which users you want to sync.`,
    description: 'Description of the steps on how to configure G Suite.',
  },
  configureGSuiteButton: {
    id: 'user-management.site-settings.g-suite.disconnected.button.configure',
    defaultMessage: 'Configure G Suite',
    description: 'Button that triggers the G Suite connection wizard.',
  },
});

const ListItem = styled.li`
  padding: ${akGridSize()}px;
`;

const Image = styled.div`
  background: url(${catchInNetImage}) no-repeat 50%/contain;
  height: 136px;
  margin-bottom: ${akGridSize() * 5}px;
`;

const SectionHeading = styled.h3`
  margin-bottom: ${akGridSize() * 2}px;
`;

const ButtonContainer = styled.div`
  margin-top: ${akGridSize() * 3}px;
`;

interface SiteParams {
  cloudId: string;
}

type GSuitePageProps = ChildProps<FlagProps & InjectedIntlProps & DispatchProp<any> & RouteComponentProps<SiteParams>, GSuitePageQuery> & AnalyticsClientProps;

interface GSuitePageState {
  isConfigModalOpen: boolean;
  isEditDialogOpen: boolean;
  isDisconnectDialogOpen: boolean;
}

export class GSuitePageImpl extends React.Component<GSuitePageProps, GSuitePageState> {

  public state: Readonly<GSuitePageState> = {
    isConfigModalOpen: false,
    isEditDialogOpen: false,
    isDisconnectDialogOpen: false,
  };

  public render() {
    const {
      intl: { formatMessage },
      data: {
        loading = false,
        error = null,
      } = {},
    } = this.props;

    const sidePanel = (
      <PageSidebar>
        <Image />
        <h4>{formatMessage(messages.sidebarTitle)}</h4>
        <ul>
          {this.sideBarProductAccessContent()}
          <ListItem>
            <FormattedMessage
              {...messages.sidebarMoreInfoPoint}
              values={{
                sidebarMoreInfoPointLink: (<ExternalLink href={links.external.gSuite}>{formatMessage(messages.sidebarMoreInfoPointLink)}</ExternalLink>),
              }}
            />
          </ListItem>
        </ul>
      </PageSidebar>
    );

    return (
      <PageLayout
        title={formatMessage(messages.title)}
        description={formatMessage(messages.description)}
        side={sidePanel}
      >
        {loading ? <AkSpinner /> : (error ? (
          <span>{formatMessage(messages.error)}</span>
        ) : (
          <ScreenEventSender
            isDataLoading={loading}
            event={gSuiteScreenScreenEvent()}
          >
            <SectionHeading>{formatMessage(messages.disconnectedTitle)}</SectionHeading>
            <FormattedMessage {...messages.disconnectedDescription} tagName="p" />
            <ButtonContainer>
              <AkButton appearance="primary" onClick={this.showGSuiteWizard}>
                {formatMessage(messages.configureGSuiteButton)}
              </AkButton>

            </ButtonContainer>
          </ScreenEventSender>
        ))}
      </PageLayout>
    );
  }

  private showGSuiteWizard = () => {
    this.props.dispatch(openDrawer(NavigationDrawer.GSuiteWizard));
  }

  private sideBarProductAccessContent(): React.ReactNode[] {
    const {
      intl: { formatMessage },
    } = this.props;

    const isDisconnected = !this.props.data || !this.props.data.site || !this.props.data.site.gSuite || !this.props.data.site.gSuite.state || !this.props.data.site.gSuite.state.user;

    return isDisconnected ? [
      <ListItem key="1">{formatMessage(messages.sidebarDisconnectedPoint1)}</ListItem>,
      <ListItem key="2">{formatMessage(messages.sidebarDisconnectedPoint2)}</ListItem>,
      <ListItem key="3">{formatMessage(messages.sidebarDisconnectedPoint3)}</ListItem>,
      <ListItem key="4">{formatMessage(messages.sidebarDisconnectedPoint4)}</ListItem>,
    ] : [];
  }
}

const withGSuiteState = graphql<GSuitePageProps, GSuitePageQuery, GSuitePageQueryVariables, ChildProps<GSuitePageProps, GSuitePageQuery>>(getGSuiteState, {
  options: ({ match: { params: { cloudId } } }) => ({ variables: { cloudId } }),
  skip: ({ match: { params: { cloudId } } }) => !cloudId,
});

export const GSuitePage = injectIntl(
  withRouter(
    withFlag(
      connect()(
        withGSuiteState(
          withAnalyticsClient<GSuitePageProps>(
            GSuitePageImpl,
          ),
        ),
        ),
    ),
  ),
);
