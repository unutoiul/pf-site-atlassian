import * as React from 'react';
import { ChildProps, compose, graphql } from 'react-apollo';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import { default as AkAppSwitcher } from '@atlaskit/app-switcher';
import AkAppSwitcherIcon from '@atlaskit/icon/glyph/app-switcher';
import AkMenuIcon from '@atlaskit/icon/glyph/menu';
import { AkGlobalItem } from '@atlaskit/navigation';

import {
  analyticsClient,
  AnalyticsClientProps,
  AnalyticsData,
  appSwitcherDropdownClosedEventData,
  appSwitcherDropdownViewEventData,
  withAnalyticsClient,
} from 'common/analytics';
import { NavigationTooltip } from 'common/navigation-tooltip';

import { AppSwitcherQuery, UpdateXFlowDialogMutation } from '../../schema/schema-types';
import { AppSwitcherFeatureFlag, withAppSwitcherFeatureFlag } from '../feature-flags';
import appSwitcherQuery from './app-switcher.query.graphql';
import { ProductUpselling, ProductUpsellingState } from './product-upselling';
import updateXFlowDialogMutation from './update-x-flow-dialog.mutation.graphql';

export const globalAppSwitcherButtonEvent: AnalyticsData = {
  action: 'click',
  subproduct: 'chrome',
  actionSubject: 'sidebarGlobalButton',
  actionSubjectId: 'app-switcher',
};

export const appSwitcherHomeLinkEvent: AnalyticsData = {
  action: 'click',
  subproduct: 'chrome',
  actionSubject: 'sidebarGlobalButton',
  actionSubjectId: 'app-switcher.home',
};

export const appSwitcherSiteAdminLinkEvent: AnalyticsData = {
  action: 'click',
  subproduct: 'chrome',
  actionSubject: 'sidebarGlobalButton',
  actionSubjectId: 'app-switcher.site-admin',
};

export const appSwitcherProductLinkEvent = (productName) => ({
  action: 'click',
  subproduct: 'chrome',
  actionSubject: 'sidebarGlobalButton',
  actionSubjectId: `app-switcher.link.${productName}`,
} as AnalyticsData);

interface InputProps {
  showXFlowDialog(sourceComponent: string, targetProduct: string): void;
}

type AppSwitcherProps = ChildProps<InputProps, AppSwitcherQuery>;

const messages = defineMessages({
  home: {
    defaultMessage: 'Home',
    id: 'chrome.app_switcher.home',
    description: '',
  },
  apps: {
    defaultMessage: 'Apps',
    id: 'chrome.app_switcher.apps',
    description: '',
  },
  configure: {
    defaultMessage: 'Configure',
    id: 'chrome.app_switcher.configure',
    description: '',
  },
  recent: {
    defaultMessage: 'Recent',
    id: 'chrome.app_switcher.recent',
    description: '',
  },
  tryLozenge: {
    defaultMessage: 'Try',
    id: 'chrome.app_switcher.try-other-apps',
    description: '',
  },
  dontShowThisAgain: {
    defaultMessage: 'Don’t show this again',
    id: 'chrome.app_switcher.dont-show-this-again',
    description: '',
  },
  confluenceSpace: {
    defaultMessage: 'Space',
    id: 'chrome.app_switcher.container.confluence-space',
    description: '',
  },
  jiraProject: {
    defaultMessage: 'Project',
    id: 'chrome.app_switcher.container.jira-project',
    description: '',
  },
  suggestedAppConfluence: {
    defaultMessage: 'Collaboration and content sharing',
    id: 'chrome.app_switcher.suggested.application.description.confluence',
    description: '',
  },
  suggestedAppJira: {
    defaultMessage: 'Issue & project tracking software',
    id: 'chrome.app_switcher.suggested.application.description.jira',
    description: '',
  },
  appsLoadError: {
    defaultMessage: 'Could not load linked applications',
    id: 'chrome.app_switcher.applinks-load-error',
    description: '',
  },
  siteAdmin: {
    defaultMessage: 'Site administration',
    id: 'chrome.app_switcher.site-admin',
    description: '',
  },
  applicationSwitcher: {
    defaultMessage: 'Application switcher',
    id: 'chrome.app_switcher.application-switcher',
    description: '',
  },
  globalItemButtonTooltip: {
    id: 'chrome.app_switcher.tooltip',
    defaultMessage: 'Switch apps',
  },
});

export class AppSwitcherImpl extends React.Component<AppSwitcherProps & InjectedIntlProps & AppSwitcherFeatureFlag & AnalyticsClientProps> {
  public render() {
    if (!this.props.data || this.props.data.loading || this.props.data.error) {
      return null;
    }

    const applications = this.remapApplicationNames();

    const { formatMessage } = this.props.intl;

    return (
      <AkAppSwitcher
        recentContainers={[]}
        linkedApplications={{
          configureLink: false,
          error: applications.length === 0,
          apps: applications,
          suggested: this.generateSuggestedApplications(),
        }}
        isSiteAdminLinkEnabled={true}
        isAnonymousUser={false}
        suggestedApplication={{ show: false }}
        i18n={{
          home: formatMessage(messages.home),
          apps: formatMessage(messages.apps),
          configure: formatMessage(messages.configure),
          recent: formatMessage(messages.recent),
          'try.lozenge': formatMessage(messages.tryLozenge),
          'don\'t.show.this.again': formatMessage(messages.dontShowThisAgain),
          'container.confluence-space': formatMessage(messages.confluenceSpace),
          'container.jira-project': formatMessage(messages.jiraProject),
          'suggested.application.description.confluence': formatMessage(messages.suggestedAppConfluence),
          'suggested.application.description.jira': formatMessage(messages.suggestedAppJira),
          'applinks.error': formatMessage(messages.appsLoadError),
          'site-admin': formatMessage(messages.siteAdmin),
        }}
        dropdownOptions={{
          position: 'right bottom',
        }}
        isDropdownOpenInitially={false}
        trigger={this.createTrigger}
        onAppSwitcherOpen={this.onAppSwitcherOpen}
        onAppSwitcherClose={this.onAppSwitcherClose}
        analytics={this.onAnalytics}
      />
    );
  }

  public remapApplicationNames() {
    let applications =
      (this.props.data &&
        this.props.data.currentSite &&
        this.props.data.currentSite.applications) ||
      [];
    const registeredApps =
      (this.props.data &&
        this.props.data.currentSite &&
        this.props.data.currentSite.defaultApps) ||
      [];

    // if there is only one type of Jira, make the product name that.
    const jiraApps = registeredApps.filter(app =>
      app.productId.startsWith('jira-'),
    );

    if (jiraApps.length === 1) {
      applications = applications.map(app => {
        if (app.id === 'jira') {
          app.name = jiraApps[0].productName;
        }

        return app;
      });
    }

    return applications;
  }

  private createTrigger = (): React.ReactNode => {
    const {
      intl: { formatMessage },
      appSwitcherFeatureFlag,
    } = this.props;

    if (appSwitcherFeatureFlag.isLoading) {
      return null;
    }

    const Icon = appSwitcherFeatureFlag.value ? AkAppSwitcherIcon : AkMenuIcon;

    return (
      <NavigationTooltip message={formatMessage(messages.globalItemButtonTooltip)}>
        <AkGlobalItem>
          <Icon
            label={formatMessage(messages.applicationSwitcher)}
            size="medium"
          />
        </AkGlobalItem>
      </NavigationTooltip>
    );
  }

  private propsToProductUpsellingState(): ProductUpsellingState {
    const {
      showXFlowDialog: openXFlow,
      data: {
        currentSite: {
          id: cloudId,
          applications = [],
          settings: {
            serviceDesk: {
              enabled: userHasJSD = false,
            } = {},
            xFlow: {
              enabled: xFlowEnabled = true,
            } = {},
          } = {},
        } = { id: undefined },
      } = {},
    } = this.props;

    return {
      cloudId,
      linkedApplications: applications || [],
      userHasJSD,
      openXFlow,
      xFlowEnabled,
    };
  }

  private generateSuggestedApplications() {
    const productUpsellingState = this.propsToProductUpsellingState();

    return ProductUpselling.generateSuggestedApplications(productUpsellingState);
  }

  private onAppSwitcherOpen = () => {
    const productUpsellingState = this.propsToProductUpsellingState();

    ProductUpselling.trackAppSwitcherOpen(productUpsellingState);

    this.props.analyticsClient.sendScreenEvent({
      cloudId: this.props.data && this.props.data.currentSite && this.props.data.currentSite.id,
      data: appSwitcherDropdownViewEventData(),
    });
  }

  private onAppSwitcherClose = () => {
    this.props.analyticsClient.sendUIEvent({
      cloudId: this.props.data && this.props.data.currentSite && this.props.data.currentSite.id,
      data: appSwitcherDropdownClosedEventData(),
    });
  };

  // Handles the legacy events
  private onAnalytics = (eventName: string, payload?: { product: string }) => {
    // ex: eventName = "appswitcher.app.link.click"
    if (eventName === 'appswitcher.trigger.opened') {
      analyticsClient.eventHandler(globalAppSwitcherButtonEvent);

      return;
    }

    if (eventName === 'appswitcher.trigger.closed') {
      return;
    }

    const linkName = eventName.split('.')[1];
    if (linkName === 'siteAdmin') {
      analyticsClient.eventHandler(appSwitcherSiteAdminLinkEvent);

      return;
    }

    if (linkName === 'home') {
      analyticsClient.eventHandler(appSwitcherHomeLinkEvent);

      return;
    }

    if (!payload || !payload.product) {
      return;
    }

    analyticsClient.eventHandler(
      appSwitcherProductLinkEvent(payload.product),
    );
  };
}

const updateXFlowMutation = graphql<{}, UpdateXFlowDialogMutation, {}, InputProps>(
  updateXFlowDialogMutation,
  {
    props: ({ mutate }) => ({
      showXFlowDialog: async (sourceComponent, targetProduct) => mutate instanceof Function && mutate({ variables: { sourceComponent, targetProduct } }),
    }),
  },
);

export const AppSwitcher = compose(
  updateXFlowMutation,
  graphql<AppSwitcherQuery>(appSwitcherQuery),
  injectIntl,
  withAppSwitcherFeatureFlag,
  withAnalyticsClient,
)(AppSwitcherImpl);
