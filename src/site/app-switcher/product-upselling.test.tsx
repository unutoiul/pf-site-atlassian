import { expect } from 'chai';
import { sandbox as sinonSandbox, SinonSandbox, SinonStub } from 'sinon';

import { xflowAnalyticsClient } from '../xflow/xflow.analytics';
import { ProductUpselling, ProductUpsellingState } from './product-upselling';

describe('ProductUpselling', () => {
  let sandbox: SinonSandbox;
  let mockProductUpsellingState: ProductUpsellingState;
  let mockOpenXFlow: SinonStub;
  let mockTrigger: SinonStub;

  beforeEach(() => {
    sandbox = sinonSandbox.create();

    mockOpenXFlow = sandbox.stub();

    mockProductUpsellingState = {
      cloudId: 'mockCloudId',
      linkedApplications: [
        {
          product: 'confluence',
        },
      ],
      xFlowEnabled: true,
      userHasJSD: false,
      openXFlow: mockOpenXFlow,
    };

    mockTrigger = sandbox.stub(xflowAnalyticsClient, 'triggerXFlowAnalyticsEvent');
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('trackAppSwitcherOpen', () => {
    it('should send an event when JSD was promoted', () => {
      ProductUpselling.trackAppSwitcherOpen(mockProductUpsellingState);

      expect(mockTrigger.calledOnce).to.equal(true);
      expect(mockTrigger.firstCall.args[0]).to.equal('grow0.admin.app-switcher.opened');
      expect(mockTrigger.firstCall.args[1]).to.equal(mockProductUpsellingState.cloudId);
      expect(mockTrigger.firstCall.args[2]).to.contain({
        is_app_switcher_ready: true,
        did_admin_disable_promotions: false,
      });
      expect(mockTrigger.firstCall.args[2].product_to_upsell.value).to.equal('jira-service-desk');
    });

    it('should send an event when confluence was promoted', () => {
      mockProductUpsellingState.linkedApplications = [{ product: 'jira' }];
      ProductUpselling.trackAppSwitcherOpen(mockProductUpsellingState);

      expect(mockTrigger.calledOnce).to.equal(true);
      expect(mockTrigger.firstCall.args[0]).to.equal('grow0.admin.app-switcher.opened');
      expect(mockTrigger.firstCall.args[1]).to.equal(mockProductUpsellingState.cloudId);
      expect(mockTrigger.firstCall.args[2]).to.contain({
        is_app_switcher_ready: true,
        did_admin_disable_promotions: false,
      });
      expect(mockTrigger.firstCall.args[2].product_to_upsell.value).to.equal('confluence');
    });

    it('should send an event even when nothing was promoted', () => {
      mockProductUpsellingState.userHasJSD = true;
      ProductUpselling.trackAppSwitcherOpen(mockProductUpsellingState);

      expect(mockTrigger.calledOnce).to.equal(true);
      expect(mockTrigger.firstCall.args[0]).to.equal('grow0.admin.app-switcher.opened');
      expect(mockTrigger.firstCall.args[1]).to.equal(mockProductUpsellingState.cloudId);
      expect(mockTrigger.firstCall.args[2]).to.contain({
        is_app_switcher_ready: true,
        did_admin_disable_promotions: false,
      });
      expect(mockTrigger.firstCall.args[2].product_to_upsell.value).to.equal('none');
    });

    it('should send an event when the feature is live - admin disabled promotions', () => {
      mockProductUpsellingState.xFlowEnabled = false;
      ProductUpselling.trackAppSwitcherOpen(mockProductUpsellingState);

      expect(mockTrigger.calledOnce).to.equal(true);
      expect(mockTrigger.firstCall.args[0]).to.equal('grow0.admin.app-switcher.opened');
      expect(mockTrigger.firstCall.args[1]).to.equal(mockProductUpsellingState.cloudId);
      expect(mockTrigger.firstCall.args[2]).to.contain({
        is_app_switcher_ready: true,
        did_admin_disable_promotions: true,
      });
      expect(mockTrigger.firstCall.args[2].product_to_upsell.value).to.equal('jira-service-desk');
    });

    it('should send an event when the feature is live - app-switcher is still loading', () => {
      mockProductUpsellingState.linkedApplications = [];
      ProductUpselling.trackAppSwitcherOpen(mockProductUpsellingState);

      expect(mockTrigger.calledOnce).to.equal(true);
      expect(mockTrigger.firstCall.args[0]).to.equal('grow0.admin.app-switcher.opened');
      expect(mockTrigger.firstCall.args[1]).to.equal(mockProductUpsellingState.cloudId);
      expect(mockTrigger.firstCall.args[2]).to.contain({
        is_app_switcher_ready: false,
        did_admin_disable_promotions: false,
      });
      expect(mockTrigger.firstCall.args[2].product_to_upsell.value).to.equal('none');
    });
  });

  describe('generateSuggestedApplications', () => {
    it('should return single JSD item when confluence is installed, and jsd is not installed', () => {
      const result = ProductUpselling.generateSuggestedApplications(mockProductUpsellingState);

      expect(result.length).to.equal(1);
      expect(result[0].name).to.equal('Jira Service Desk');
      expect(result[0].product).to.equal('jira-service-desk');
    });

    it('should return single confluence item when neither confluence and jsd are installed', () => {
      mockProductUpsellingState.linkedApplications = [{ product: 'stride' }];
      const result = ProductUpselling.generateSuggestedApplications(mockProductUpsellingState);

      expect(result.length).to.equal(1);
      expect(result[0].name).to.equal('Confluence');
      expect(result[0].product).to.equal('confluence');
    });

    it('should return single confluence item when confluence is not installed, and jsd is installed', () => {
      mockProductUpsellingState.linkedApplications = [{ product: 'stride' }];
      mockProductUpsellingState.userHasJSD = true;
      const result = ProductUpselling.generateSuggestedApplications(mockProductUpsellingState);

      expect(result.length).to.equal(1);
      expect(result[0].name).to.equal('Confluence');
      expect(result[0].product).to.equal('confluence');
    });

    it('should return an empty array when both confluence and jsd are installed', () => {
      mockProductUpsellingState.userHasJSD = true;
      const result = ProductUpselling.generateSuggestedApplications(mockProductUpsellingState);

      expect(result.length).to.equal(0);
    });

    it('should return an empty array when admin has disabled promotions', () => {
      mockProductUpsellingState.xFlowEnabled = false;
      const result = ProductUpselling.generateSuggestedApplications(mockProductUpsellingState);

      expect(result.length).to.equal(0);
    });

    describe('JSD item onClick handler', () => {
      let onClick: () => void;

      beforeEach(() => {
        const result = ProductUpselling.generateSuggestedApplications(mockProductUpsellingState);
        onClick = result[0].onClick;
      });

      it('should open xFlow dialog with JSD', () => {
        onClick();

        expect(mockOpenXFlow.calledOnce).to.equal(true);
        expect(mockOpenXFlow.calledWith('appswitcher.try-jira-service-desk', 'jira-servicedesk.ondemand')).to.equal(true);
      });

      it('should trigger analytics event', () => {
        onClick();

        expect(mockTrigger.calledOnce).to.equal(true);
        expect(mockTrigger.calledWith('grow0.admin.app-switcher.try.jira-service-desk.clicked', mockProductUpsellingState.cloudId)).to.equal(true);
      });
    });

    describe('confluence item onClick handler', () => {
      let onClick: () => void;

      beforeEach(() => {
        mockProductUpsellingState.linkedApplications = [{ product: 'stride' }];
        const result = ProductUpselling.generateSuggestedApplications(mockProductUpsellingState);
        onClick = result[0].onClick;
      });

      it('should open xFlow dialog with Confluence', () => {
        onClick();

        expect(mockOpenXFlow.calledOnce).to.equal(true);
        expect(mockOpenXFlow.calledWith('appswitcher.try-confluence', 'confluence.ondemand')).to.equal(true);
      });

      it('should trigger analytics event', () => {
        onClick();

        expect(mockTrigger.calledOnce).to.equal(true);
        expect(mockTrigger.calledWith('grow0.admin.app-switcher.try.confluence.clicked', mockProductUpsellingState.cloudId)).to.equal(true);
      });
    });
  });
});
