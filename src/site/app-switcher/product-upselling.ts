import { dangerouslyCreateSafeString } from '@atlassiansox/analytics';

import { xflowAnalyticsClient } from '../xflow/xflow.analytics';

export interface ProductUpsellingState {
  cloudId?: string;
  linkedApplications: any[];
  userHasJSD: boolean;
  xFlowEnabled: boolean;
  openXFlow(sourceComponent: string, targetProduct: string): void;
}

export class ProductUpselling {
  /**
   * Determines if the current site is missing Confluence or JSD, and if not, returns an object required by the app-switcher to suggest this product to the user.
   * If the site is missing both products, then Confluence is prioritised, and JSD is not suggested.
   *
   * @param {ProductUpsellingState} productUpsellingState - used to determine if upselling should happen or not
   * @return {any | null} - an item to put in the suggested applications in the app-switcher.
   */
  private static determineProductToUpsell(productUpsellingState: ProductUpsellingState) {
    const {
      cloudId,
      linkedApplications,
      userHasJSD,
      openXFlow,
    } = productUpsellingState;

    // App-switcher is still loading
    if (linkedApplications.length === 0) {
      return null;
    }

    const userHasConfluence = linkedApplications.some((product) => {
      return product.product === 'confluence';
    });

    if (!userHasConfluence) {
      return {
        name: 'Confluence',
        product: 'confluence',
        onClick: () => {
          openXFlow('appswitcher.try-confluence', 'confluence.ondemand');
          xflowAnalyticsClient.triggerXFlowAnalyticsEvent('grow0.admin.app-switcher.try.confluence.clicked', cloudId);
        },
      };
    }

    if (!userHasJSD) {
      return {
        name: 'Jira Service Desk',
        product: 'jira-service-desk',
        onClick: () => {
          openXFlow('appswitcher.try-jira-service-desk', 'jira-servicedesk.ondemand');
          xflowAnalyticsClient.triggerXFlowAnalyticsEvent('grow0.admin.app-switcher.try.jira-service-desk.clicked', cloudId);
        },
      };
    }

    return null;
  }

  /**
   * Populates suggested applications for the app switcher.
   *
   * @param {ProductUpsellingState} productUpsellingState - used to determine if upselling should happen or not
   * @returns {any[]} - used to populate the app-switcher suggested products
   */
  public static generateSuggestedApplications(productUpsellingState: ProductUpsellingState) {
    const productToUpsell = this.determineProductToUpsell(productUpsellingState);

    return productToUpsell && productUpsellingState.xFlowEnabled ? [productToUpsell] : [];
  }

  public static trackAppSwitcherOpen(productUpsellingState: ProductUpsellingState) {
    const {
      cloudId,
      linkedApplications,
      xFlowEnabled,
    } = productUpsellingState;

    const isAppSwitcherReady = linkedApplications.length > 0;
    const productToUpsell = this.determineProductToUpsell(productUpsellingState);

    xflowAnalyticsClient.triggerXFlowAnalyticsEvent('grow0.admin.app-switcher.opened', cloudId, {
      is_app_switcher_ready: isAppSwitcherReady,
      product_to_upsell: dangerouslyCreateSafeString(productToUpsell && productToUpsell.product || 'none'),
      did_admin_disable_promotions: !xFlowEnabled,
    });
  }
}
