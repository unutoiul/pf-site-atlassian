import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import * as React from 'react';
import { sandbox as sinonSandbox, SinonSandbox, SinonSpy, SinonStub } from 'sinon';

import { default as AkAppSwitcher } from '@atlaskit/app-switcher';
import AkAppSwitcherIcon from '@atlaskit/icon/glyph/app-switcher';
import AkMenuIcon from '@atlaskit/icon/glyph/menu';

import {
  appSwitcherDropdownClosedEventData,
  appSwitcherDropdownViewEventData,
} from 'common/analytics';
import { Application } from 'common/navigation';

import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import { AppSwitcherImpl } from './app-switcher';
import { ProductUpselling } from './product-upselling';

describe('AppSwitcher', () => {
  let sandbox: SinonSandbox;
  let mockTrack: SinonStub;
  let mockGenerateSuggested: SinonStub;
  let mockProps;
  let mockSuggestedApplications;
  let sendScreenEventSpy: SinonSpy;
  let sendUIEventSpy: SinonSpy;

  beforeEach(() => {
    sandbox = sinonSandbox.create();
    sendScreenEventSpy = sandbox.spy();
    sendUIEventSpy = sandbox.spy();

    mockProps = {
      data: {
        currentSite: {
          id: 'mockCloudId',
          applications: [{ id: 'stride', product: 'stride', name: 'Stride', url: 'mockUrl' }, { id: 'jira', product: 'jira', name: 'Jira', url: 'mockUrl' }],
          defaultApps: [{ productId: 'jira-servicedesk', productName: 'Jira Service Desk' }],
          settings: {
            serviceDesk: {
              enabled: true,
            },
            xFlow: {
              enabled: false,
            },
          },
        },
      },
      appSwitcherFeatureFlag: {
        value: false,
        loading: false,
      },
      showXFlowDialog: () => null,
      analyticsClient: {
        sendScreenEvent: sendScreenEventSpy,
        sendUIEvent: sendUIEventSpy,
        sendTrackEvent: () => null,
      },
    };

    mockSuggestedApplications = [{
      name: 'Confluence',
      product: 'confluence',
      onClick: () => null,
    }];

    mockTrack = sandbox.stub(ProductUpselling, 'trackAppSwitcherOpen');
    mockGenerateSuggested = sandbox.stub(ProductUpselling, 'generateSuggestedApplications').returns(mockSuggestedApplications);
  });

  afterEach(() => {
    sandbox.restore();
  });

  it(`shouldn't render if it's in a loading state`, () => {
    const wrapper = shallow(
      <AppSwitcherImpl
        {...{
          ...mockProps,
          data: { loading: true },
        }}
        intl={createMockIntlProp()}
      />,
    );

    expect(wrapper.isEmptyRender()).to.equal(true);
  });

  it(`shouldn't render if it's in an error state`, () => {
    const wrapper = shallow(
      <AppSwitcherImpl
        {...{
          ...mockProps,
          data: { error: true },
        }}
        intl={createMockIntlProp()}
      />,
    );

    expect(wrapper.isEmptyRender()).to.equal(true);
  });

  it(`shouldn't render if data isn't set`, () => {
    const wrapper = shallow(
      <AppSwitcherImpl
        {...{
          ...mockProps,
          data: undefined,
        }}
        intl={createMockIntlProp()}
      />,
    );

    expect(wrapper.isEmptyRender()).to.equal(true);
  });

  it('should pass app props to AkAppSwitcher', () => {
    const applications: Application[] = [
      {
        name: 'JIRA',
        url: 'url-to-jira',
        product: 'JIRA Software',
        adminUrl: '',
        id: '',
      },
    ];
    const data = { currentSite: { applications } };
    const wrapper = shallow(
      <AppSwitcherImpl data={data} intl={createMockIntlProp()} {...{} as any} />,
      createMockIntlContext(),
    );
    const appSwitchers = wrapper.find<any>(AkAppSwitcher);
    expect(appSwitchers).to.have.length(1);
    expect(appSwitchers.at(0).props().linkedApplications.apps).to.have.length(1);
  });

  it('should set an error to linkedApplications if no apps are passed', () => {
    const data = { currentSite: { applications: [] } };
    const wrapper = shallow(
      <AppSwitcherImpl data={data} intl={createMockIntlProp()} {...{} as any} />,
      createMockIntlContext(),
    );
    const appSwitchers = wrapper.find<any>(AkAppSwitcher);
    expect(appSwitchers).to.have.length(1);
    expect(appSwitchers.at(0).props().linkedApplications.error).to.equal(true);
  });

  it('should show the old hamburger icon when the app switcher FF is disabled', () => {
    const props = {
      ...mockProps,
      appSwitcherFeatureFlag: {
        value: false,
        loading: false,
      },
    };

    const wrapper = mount(
      <AppSwitcherImpl
        {...props}
        intl={createMockIntlProp()}
        {...{} as any}
      />,
      createMockIntlContext(),
    );

    expect(wrapper.find(AkMenuIcon).exists()).to.equal(true);
    expect(wrapper.find(AkAppSwitcherIcon).exists()).to.equal(false);
  });

  it('should show the new app switcher icon when the app switcher FF is enabled', () => {
    const props = {
      ...mockProps,
      appSwitcherFeatureFlag: {
        value: true,
        loading: false,
      },
    };

    const wrapper = mount(
      <AppSwitcherImpl
        {...props}
        intl={createMockIntlProp()}
        {...{} as any}
      />,
      createMockIntlContext(),
    );

    expect(wrapper.find(AkMenuIcon).exists()).to.equal(false);
    expect(wrapper.find(AkAppSwitcherIcon).exists()).to.equal(true);
  });

  it('should have Jira Service Desk as the name of Jira in the dropdown', () => {
    const props = {
      ...mockProps,
      appSwitcherFeatureFlag: {
        value: true,
        loading: false,
      },
    };

    const wrapper = shallow(
      <AppSwitcherImpl
        {...props}
        intl={createMockIntlProp()}
        {...{} as any}
      />,
      createMockIntlContext(),
    );

    const jiraApps = (wrapper.instance() as AppSwitcherImpl).remapApplicationNames().filter(app => app.id === 'jira');

    expect(jiraApps.length).to.equal(1);
    expect(jiraApps[0].name).to.equal('Jira Service Desk');
  });

  it('should not rename Jira if there is no valid replacement', () => {
    const thisProps = JSON.parse(JSON.stringify(mockProps));
    thisProps.data.currentSite.defaultApps = [];

    const props = {
      ...thisProps,
      appSwitcherFeatureFlag: {
        value: true,
        loading: false,
      },
    };

    const wrapper = shallow(
      <AppSwitcherImpl
        {...props}
        intl={createMockIntlProp()}
        {...{} as any}
      />,
      createMockIntlContext(),
    );

    const jiraApps = (wrapper.instance() as AppSwitcherImpl).remapApplicationNames().filter(app => app.id === 'jira');

    expect(jiraApps.length).to.equal(1);
    expect(jiraApps[0].name).to.equal('Jira');
  });

  it('should not rename Jira if there is more than one subproduct', () => {
    const thisProps = JSON.parse(JSON.stringify(mockProps));
    thisProps.data.currentSite.defaultApps = [{ productId: 'jira-servicedesk', productName: 'Jira Service Desk' }, { productId: 'jira-software', productName: 'Jira Software' }];

    const props = {
      ...thisProps,
      appSwitcherFeatureFlag: {
        value: true,
        loading: false,
      },
    };

    const wrapper = shallow(
      <AppSwitcherImpl
        {...props}
        intl={createMockIntlProp()}
        {...{} as any}
      />,
      createMockIntlContext(),
    );

    const jiraApps = (wrapper.instance() as AppSwitcherImpl).remapApplicationNames().filter(app => app.id === 'jira');

    expect(jiraApps.length).to.equal(1);
    expect(jiraApps[0].name).to.equal('Jira');
  });

  describe('onAppSwitcherOpen', () => {
    let onAppSwitcherOpen;

    beforeEach(() => {
      const wrapper = shallow(
        <AppSwitcherImpl {...mockProps} intl={createMockIntlProp()} {...{} as any} />,
        createMockIntlContext(),
      );

      const appSwitcher = wrapper.find(AkAppSwitcher) as any;
      onAppSwitcherOpen = appSwitcher.prop('onAppSwitcherOpen');
    });

    it('should trigger an event via trackAppSwitcherOpen', () => {
      onAppSwitcherOpen();

      expect(mockTrack.calledOnce).to.equal(true);
      expect(mockTrack.calledWith({
        cloudId: mockProps.data.currentSite.id,
        linkedApplications: mockProps.data.currentSite.applications,
        userHasJSD: mockProps.data.currentSite.settings.serviceDesk.enabled,
        openXFlow: mockProps.showXFlowDialog,
        xFlowEnabled: mockProps.data.currentSite.settings.xFlow.enabled,
      })).to.equal(true);
    });

    it('should send a screen event', () => {
      expect(sendScreenEventSpy.called).to.equal(false);
      onAppSwitcherOpen();
      expect(sendScreenEventSpy.calledWith({
        cloudId: mockProps.data.currentSite.id,
        data: appSwitcherDropdownViewEventData(),
      })).to.equal(true);
    });
  });

  describe('onAppSwitcherClose', () => {
    it('should send a UI event', () => {
      const wrapper = shallow(
        <AppSwitcherImpl {...mockProps} intl={createMockIntlProp()} {...{} as any} />,
        createMockIntlContext(),
      );

      const appSwitcher = wrapper.find(AkAppSwitcher) as any;
      expect(sendScreenEventSpy.called).to.equal(false);
      appSwitcher.prop('onAppSwitcherClose')();

      expect(sendUIEventSpy.calledWith({
        cloudId: mockProps.data.currentSite.id,
        data: appSwitcherDropdownClosedEventData(),
      })).to.equal(true);
    });
  });

  describe('linkedApplications.suggested', () => {
    it('should get its suggested items from the generator', () => {
      const wrapper = shallow(
        <AppSwitcherImpl {...mockProps} intl={createMockIntlProp()} {...{} as any} />,
        createMockIntlContext(),
      );

      const appSwitcher = wrapper.find(AkAppSwitcher) as any;

      const suggestedApplications = appSwitcher.prop('linkedApplications').suggested;

      expect(suggestedApplications).to.equal(mockSuggestedApplications);

      expect(mockGenerateSuggested.calledOnce).to.equal(true);
      expect(mockGenerateSuggested.calledWith({
        cloudId: mockProps.data.currentSite.id,
        linkedApplications: mockProps.data.currentSite.applications,
        userHasJSD: mockProps.data.currentSite.settings.serviceDesk.enabled,
        openXFlow: mockProps.showXFlowDialog,
        xFlowEnabled: mockProps.data.currentSite.settings.xFlow.enabled,
      })).to.equal(true);
    });
  });
});
