import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { Route } from 'react-router-dom';

import { PermissionErrorPage } from 'common/permission-error-page';

import { SiteRoutesImpl } from './site-routes';

describe('Site routes', () => {
  const siteRoutesWrapper = (
    props = {
      match: null,
      location: null,
      history: null,
      loading: false,
      hasError: false,
      hasPermission: true,
    },
  ) => {
    return shallow(
      <SiteRoutesImpl {...props as any} />,
    );
  };

  it('should render routes if there\'s no errors and the user has permission', () => {
    const wrapper = siteRoutesWrapper({
      match: null,
      location: null,
      history: null,
      loading: false,
      hasError: false,
      hasPermission: true,
    });

    expect(wrapper.find(PermissionErrorPage).exists()).to.equal(false);
    expect(wrapper.find(Route).exists()).to.equal(true);
  });

  it('should render routes if it\'s loading', () => {
    const wrapper = siteRoutesWrapper({
      match: null,
      location: null,
      history: null,
      loading: true,
      hasError: false,
      hasPermission: true,
    });

    expect(wrapper.find(PermissionErrorPage).exists()).to.equal(false);
    expect(wrapper.find(Route).exists()).to.equal(true);
  });

  it('should render the permission error page on error', () => {
    const wrapper = siteRoutesWrapper({
      match: null,
      location: null,
      history: null,
      loading: false,
      hasError: true,
      hasPermission: false,
    });

    expect(wrapper.find(PermissionErrorPage).exists()).to.equal(true);
    expect(wrapper.find(Route).exists()).to.equal(false);
  });

  it('should render the permission error page if the user doesn\'t have permission', () => {
    const wrapper = siteRoutesWrapper({
      match: null,
      location: null,
      history: null,
      loading: false,
      hasError: false,
      hasPermission: false,
    });

    expect(wrapper.find(PermissionErrorPage).exists()).to.equal(true);
    expect(wrapper.find(Route).exists()).to.equal(false);
  });
});
