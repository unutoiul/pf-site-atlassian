import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import { Redirect, Route, RouteComponentProps, Switch } from 'react-router-dom';

import { PickLoader } from 'common/loading';
import { PermissionErrorPage } from 'common/permission-error-page';

import { CurrentUserSitesQuery } from '../../schema/schema-types';
import { EmojiPage } from '../../site/emoji';
import { UserConnectedAppsPage } from '../user-connected-apps/user-connected-apps-page';
import { AccessConfigurationPage } from '../user-management/access-config/access-config-page';
import { AccessRequestsPage } from '../user-management/access-requests/access-requests-page';
import { GroupDetailPage } from '../user-management/groups/group-detail-page';
import { GroupsPage } from '../user-management/groups/groups-page';
import { JsdPage } from '../user-management/jsd/jsd-page';
import { SiteAccessPage } from '../user-management/site-access/site-access-page';
import { UserDetailsPage } from '../user-management/users/user-details/user-details-page/user-details-page';
import { UsersPage } from '../user-management/users/user-list/users-page';
import currentUserSitesQuery from './current-user-sites.query.graphql';

const loadBillingPages = async () => import(
  /* webpackChunkName: "billing-pages" */
  '../../apps/billing',
);

const BillHistoryPage = PickLoader(
  loadBillingPages,
  ({ BillHistory }) => BillHistory,
);

const BillEstimatePage = PickLoader(
  loadBillingPages,
  ({ BillEstimate }) => BillEstimate,
);

const BillOverviewPage = PickLoader(
  loadBillingPages,
  ({ BillOverview }) => BillOverview,
);

const ManageSubscriptionsPage = PickLoader(
  loadBillingPages,
  ({ ManageSubscriptions }) => ManageSubscriptions,
);

const PaymentDetailPage = PickLoader(
  loadBillingPages,
  ({ PaymentDetail }) => PaymentDetail,
);

const DiscoverNewProductsPage = PickLoader(
  loadBillingPages,
  ({ DiscoverNewProducts }) => DiscoverNewProducts,
);

export interface SiteRouteProps {
  cloudId: string;
}

interface SiteRoutesOwnProps {
  hasPermission: boolean;
  loading: boolean;
  hasError: boolean;
}

type SiteRoutesProps = ChildProps<RouteComponentProps<SiteRouteProps> & SiteRoutesOwnProps, CurrentUserSitesQuery>;

export class SiteRoutesImpl extends React.Component<SiteRoutesProps, {}> {
  public render() {
    const {
      loading,
      hasError,
      hasPermission,
      match: { params: { cloudId } },
    } = this.props;

    const routes = (
      <Switch>
        <Route path="/s/:cloudId/users/:userId" component={UserDetailsPage} />
        <Route path="/s/:cloudId/users" component={UsersPage} />
        <Route path="/s/:cloudId/groups" component={GroupsPage} exact={true} />
        <Route path="/s/:cloudId/groups/:groupId" component={GroupDetailPage} exact={true} />
        <Route path="/s/:cloudId/access-requests" component={AccessRequestsPage} />
        <Route path="/s/:cloudId/jira-service-desk/portal-only-customers" component={JsdPage} />
        <Route path="/s/:cloudId/user-connected-apps" component={UserConnectedAppsPage} />

        <Route path="/s/:cloudId/signup" component={SiteAccessPage} />
        <Route path="/s/:cloudId/apps" component={AccessConfigurationPage} />
        <Route path="/s/:cloudId/emoji" component={EmojiPage} />

        <Redirect exact={true} from="/s/:cloudId/billing" to={`/s/${cloudId}/billing/overview`} />
        <Route path="/s/:cloudId/billing/overview" component={BillOverviewPage} />
        <Route path="/s/:cloudId/billing/estimate" component={BillEstimatePage} />
        <Route path="/s/:cloudId/billing/paymentdetails" component={PaymentDetailPage} />
        <Route path="/s/:cloudId/billing/applications" component={ManageSubscriptionsPage} />
        <Route path="/s/:cloudId/billing/history" component={BillHistoryPage} />
        <Route path="/s/:cloudId/billing/addapplication" component={DiscoverNewProductsPage} />
      </Switch>
    );

    if (loading) {
      return routes;
    }

    if (hasError || !hasPermission) {
      return <PermissionErrorPage />;
    }

    return routes;
  }
}

const query = currentUserSitesQuery;

export const SiteRoutes = graphql<RouteComponentProps<SiteRouteProps>, CurrentUserSitesQuery, {}, SiteRoutesOwnProps>(query, {
  props: ({ ownProps, data }): SiteRoutesOwnProps => ({
    hasPermission: !!(data && data.currentUser && data.currentUser.sites && data.currentUser.sites.filter(site => site.id === ownProps.match.params.cloudId).length),
    loading: !!(data && data.loading),
    hasError: !!(data && data.error),
  }),
})(SiteRoutesImpl);
