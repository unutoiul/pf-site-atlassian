import * as React from 'react';
import {
  defineMessages,
  FormattedDate,
  FormattedMessage,
  InjectedIntlProps,
  injectIntl,
} from 'react-intl';
import { Link } from 'react-router-dom';

import AkButton from '@atlaskit/button';
import AkDynamicTable, { Row as AkRow } from '@atlaskit/dynamic-table';

import { EmptyTableView } from 'common/table';

import {
  AnalyticsClientProps,
  strideCancelExportButtonClickEvent,
  strideCopyExportButtonClickEvent,
  strideDeleteExportButtonClickEvent,
  strideDownloadExportButtonClickEvent,
  strideExportDetailsLinkClickEvent,
  stridePageScreen,
  withAnalyticsClient,
} from 'common/analytics';

import { ExportDownload } from './export-download';
import { ExportStatus } from './status';

import { StrideExport } from '../../../src/schema/schema-types';

export const messages = defineMessages({
  download: {
    id: 'stride.page.download',
    defaultMessage: 'Download package',
    description: 'Text to be displayed on a tooltip for a button that downloads an export package',
  },
  copyDownloadLink: {
    id: 'stride.page.copyDownloadLink',
    defaultMessage: 'Copy download link',
    description: 'Text to be displayed on a tooltip for a button that copies the download url to the clipboard',
  },
  downloadLink: {
    id: 'stride.page.downloadLink',
    defaultMessage: 'Download link',
    description: 'Text to be displayed on the success flag when a user copies the download link to their clipboard',
  },
  tableHeaderExportID: {
    id: 'stride.page.tableHeaderExportID',
    defaultMessage: 'Export ID',
    description: 'The header text for a table column that displays the identifier for an export',
  },
  tableHeaderStartDate: {
    id: 'stride.page.tableHeaderStartDate',
    defaultMessage: 'Export created',
    description: 'The header text for a table column that displays the date and time that an export was started',
  },
  tableHeaderStatus: {
    id: 'stride.page.tableHeaderStatus',
    defaultMessage: 'Status',
    description: 'The header text for a table column that displays the status of an export',
  },
  tableHeaderDownload: {
    id: 'stride.page.tableHeaderDownload',
    defaultMessage: 'Download',
    description: 'The header text for a table column that displays buttons that allow a user to access their export',
  },
  cancelExport: {
    id: 'stride.page.cancelExport',
    defaultMessage: 'Cancel',
    description: 'The text to be displayed on a button that cancels an export',
  },
  deleteExport: {
    id: 'stride.page.deleteExport',
    defaultMessage: 'Delete',
    description: 'The text to be dispalyed on a button that deletes an export',
  },
  emptyTable: {
    id: 'stride.page.emptyTable',
    defaultMessage: 'No exports available.',
    description: 'The text to be displayed in the table when there are no exports to show',
  },
});

type PickedStrideExport = Pick<StrideExport, 'id' | 'startedAt' | 'status' | 'downloadLocation'>;

interface OwnProps {
  isLoading: boolean;
  exports: PickedStrideExport[];
  onCancelExport(exportId: string);
  onDeleteExport(exportId: string);
}

type StrideExportsTableProps = InjectedIntlProps & OwnProps & AnalyticsClientProps;
export class StrideExportsTableImpl extends React.Component<StrideExportsTableProps> {

  public render() {
    return (
      <AkDynamicTable
        isLoading={this.props.isLoading}
        head={this.getHeader()}
        rows={this.props.exports.map(this.getRowFromStrideExport)}
        isFixedSize={true}
        emptyView={
          <EmptyTableView>
            <FormattedMessage
                {...messages.emptyTable}
            />
          </EmptyTableView>
        }
      />
    );
  }

  private getHeader = () => {
    return {
      cells: [
        {
          key: 'exportid',
          content: <FormattedMessage {...messages.tableHeaderExportID} />,
          shouldTruncate: true,
          width: 25,
        },
        {
          key: 'startdate',
          content: <FormattedMessage {...messages.tableHeaderStartDate} />,
          width: 20,
        },
        {
          key: 'status',
          content: <FormattedMessage {...messages.tableHeaderStatus} />,
          width: 10,
        },
        {
          key: 'download',
          content: <FormattedMessage {...messages.tableHeaderDownload} />,
          width: 15,
        },
        {
          key: 'action',
          content: '',
          width: 10,
        },
      ],
    };
  }

  private getRowFromStrideExport = (strideExport: PickedStrideExport): AkRow => {
    return {
      cells: [
        {
          key: 'exportid',
          content: (
            <Link to={`./stride/${strideExport.id}`} onClick={this.sendExportDetailsUIEvent(strideExport.id)}>
              {strideExport.id}
            </Link>
          ),
        },
        {
          key: 'startdate',
          content: (
              <FormattedDate
                  value={strideExport.startedAt}
                  month="short"
                  day="2-digit"
                  year="numeric"
                  hour="2-digit"
                  minute="2-digit"
                  second="2-digit"
              />
          ),
        },
        {
          key: 'status',
          content: <ExportStatus status={strideExport.status} />,
        },
        {
          key: 'download',
          content: this.getDownloadContentFromStrideExport(strideExport),
        },
        {
          key: 'action',
          content: this.getActionContentFromStrideExport(strideExport),
        },
      ],
    };
  }

  private getDownloadContentFromStrideExport = (strideExport: PickedStrideExport): React.ReactNode => {
    if (!strideExport.downloadLocation) {
      return null;
    }

    return (
      <ExportDownload
        compact={true}
        link={strideExport.downloadLocation}
        downloadAnalyticsEvent={strideDownloadExportButtonClickEvent(strideExport.id, stridePageScreen)}
        copyAnalyticsEvent={strideCopyExportButtonClickEvent(strideExport.id, stridePageScreen)}
      />
    );
  }

  private getActionContentFromStrideExport(strideExport: PickedStrideExport): React.ReactNode {
    if (strideExport.status === 'pending') {
      return <AkButton onClick={this.onCancelExport(strideExport.id)}><FormattedMessage {...messages.cancelExport} /></AkButton>;
    } else {
      return <AkButton onClick={this.onDeleteExport(strideExport.id)} appearance="danger"><FormattedMessage {...messages.deleteExport} /></AkButton>;
    }
  }

  private onDeleteExport = (exportId: string) => () => {
    this.props.analyticsClient.sendUIEvent(strideDeleteExportButtonClickEvent(exportId));
    this.props.onDeleteExport(exportId);
  }

  private onCancelExport = (exportId: string) => () => {
    this.props.analyticsClient.sendUIEvent(strideCancelExportButtonClickEvent(exportId));
    this.props.onCancelExport(exportId);
  }

  private sendExportDetailsUIEvent = (exportId: string) => () => {
    this.props.analyticsClient.sendUIEvent(strideExportDetailsLinkClickEvent(exportId));
  }

}

export const StrideExportsTable = injectIntl(
  withAnalyticsClient(
    StrideExportsTableImpl,
  ),
);
