import * as React from 'react';
import { ChildProps, graphql, MutationFunc } from 'react-apollo';
import {
  defineMessages,
  FormattedMessage,
  InjectedIntlProps,
  injectIntl,
} from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router-dom';

import AkButton from '@atlaskit/button';
import AkTooltip from '@atlaskit/tooltip';

import {
  AnalyticsClientProps,
  ScreenEventSender,
  strideCancelCancelExportButtonClickEvent,
  strideCancelDeleteExportButtonClickEvent,
  strideCancelExportDialogScreenEvent,
  strideConfirmCancelExportButtonClickEvent,
  strideConfirmDeleteExportButtonClickEvent,
  strideCreateExportButtonClickEvent,
  strideDeleteExportDialogScreenEvent,
  stridePageScreenEvent,
  withAnalyticsClient,
} from 'common/analytics';
import { PageLayout } from 'common/page-layout';

import {
  CancelStrideExportMutation,
  CancelStrideExportMutationVariables,
  CreateStrideExportInput,
  CreateStrideExportMutation,
  CreateStrideExportMutationVariables,
  DeleteStrideExportMutation,
  DeleteStrideExportMutationVariables,
  StrideExportsQuery,
  StrideExportsQueryVariables,
} from '../../../src/schema/schema-types';
import CancelStrideExport from './cancel-export.mutation.graphql';
import CreateStrideExport from './create-export.mutation.graphql';
import DeleteStrideExport from './delete-export.mutation.graphql';
import strideExportsQuery from './stride-page-exports.query.graphql';

import { ConfirmationDialog } from './confirmation-dialog';
import { CreateExportDialog, CreateExportFormData } from './export-create-dialog';
import { StrideExportsTable } from './stride-exports-table';
import { StridePendingExportMessage } from './stride-pending-export-message';

export const messages = defineMessages({
  title: {
    id: 'stride.page.title',
    defaultMessage: 'Stride Exports',
    description: 'The title for a page that displays a user\'s Stride exports',
  },
  error: {
    id: 'stride.page.error',
    defaultMessage: `We couldn't retrieve the exports for this site. Try again soon.`,
    description: 'The description of an error that occurs when the user\'s exports could not be retreived',
  },
  copyDownloadLinkFlagTitle: {
    id: 'stride.page.copyDownloadLinkFlagTitle',
    defaultMessage: `Download link copied!`,
    description: 'Text to be displayed in a flag when a user successfully copies a download link to their clipboard',
  },
  confirm: {
    id: 'stride.page.confirmDelete',
    defaultMessage: 'Are you sure?',
    description: 'Text to be displayed in a dialog allowing the user to confirm that they want to perform a certain action',
  },
  deleteDialogHeader: {
    id: 'stride.export-delete.header',
    defaultMessage: `Delete export?`,
    description: 'Header on a dialog that allows a user to delete an export',
  },
  deleteDialogDescription: {
    id: 'stride.export-delete.description',
    defaultMessage: `Are you sure you want to delete this export?`,
    description: 'Subheader on a dialog that allows a user to delete an export',
  },
  deleteDialogConfirmButton: {
    id: 'stride.export-delete.buttonConfirm',
    defaultMessage: 'Delete export',
    description: 'Text on a button inside a dialog that allows a user to delete an export',
  },
  deleteDialogDismissButton: {
    id: 'stride.export-delete.buttonDismiss',
    defaultMessage: 'Cancel',
    description: 'Text on a button inside a dialog that allows a user to dismiss the dialog',
  },
  cancelDialogHeader: {
    id: 'stride.export-cancel.header',
    defaultMessage: `Cancel export?`,
    description: 'Header on a dialog that allows a user to cancel an export',
  },
  cancelDialogDescription: {
    id: 'stride.export-cancel.description',
    defaultMessage: `Are you sure you want to cancel this export?`,
    description: 'Subheader on a dialog that allows a user to cancel an export',
  },
  cancelDialogConfirmButton: {
    id: 'stride.export-cancel.buttonConfirm',
    defaultMessage: 'Cancel export',
    description: 'Text on a button inside a dialog that allows a user to cancel an export',
  },
  cancelDialogDismissButton: {
    id: 'stride.export-cancel.buttonDismiss',
    defaultMessage: 'Dismiss',
    description: 'Text on a button inside a dialog that allows a user to dismiss the dialog',
  },
  createExportDialogButton: {
    id: 'stride.export-create-dialog.button',
    defaultMessage: 'Create export',
    description: 'A button that opens a dialog that allows the user to create an export of the Stride data for their cloud',
  },
  createExportDialogButtonDisabledTooltip: {
    id: 'stride.export-create-dialog.buttonTooltip',
    defaultMessage: 'You cannot create an export while one is pending',
    description: 'Text displayed on a tooltip for the create export button because creating exports is disabled when one is pending',
  },
});

interface SiteParams {
  cloudId: string;
}

interface DeleteDerivedProps {
  deleteStrideExport: MutationFunc<DeleteStrideExportMutation, DeleteStrideExportMutationVariables>;
}
interface CancelDerivedProps {
  cancelStrideExport: MutationFunc<CancelStrideExportMutation, CancelStrideExportMutationVariables>;
}
interface CreateDerivedProps {
  createStrideExport: MutationFunc<CreateStrideExportMutation, CreateStrideExportMutationVariables>;
}

type GraphQLDerivedProps = DeleteDerivedProps & CancelDerivedProps & CreateDerivedProps;

type StridePageProps = ChildProps<InjectedIntlProps & RouteComponentProps<SiteParams> & AnalyticsClientProps, StrideExportsQuery> & GraphQLDerivedProps;

interface StridePageState {
  isDeleteDialogOpen: boolean;
  isDeleting: boolean;
  isCancelDialogOpen: boolean;
  isCancelling: boolean;
  isCreateExportDialogOpen: boolean;
  isCreating: boolean;
  actionedExportId: string | null;
}

export class StridePageImpl extends React.Component<StridePageProps, StridePageState> {
  public constructor(props: StridePageProps) {
    super(props);
    this.state = {
      isDeleteDialogOpen: false,
      isDeleting: false,
      isCancelDialogOpen: false,
      isCancelling: false,
      isCreateExportDialogOpen: false,
      isCreating: false,
      actionedExportId: null,
    };
  }

  public render() {
    const {
        data: {
            stride: {
                strideExports = [],
            } = {},
          loading = false,
          error = null,
        } = {},
      } = this.props;

    const { actionedExportId } = this.state;

    const anyExportsPending = strideExports.some(e => e.status === 'pending');

    if (error) {
      return (
        <PageLayout>
          <span><FormattedMessage {...messages.error} /></span>
        </PageLayout>
      );
    }

    return (
        <PageLayout
            title={<FormattedMessage {...messages.title} />}
            isFullWidth={true}
            action={this.getPageAction()}
        >
          <ScreenEventSender
            isDataLoading={loading}
            event={stridePageScreenEvent()}
          >
            <StridePendingExportMessage
              pending={anyExportsPending}
            />
            <StrideExportsTable
                isLoading={loading}
                exports={strideExports}
                onCancelExport={this.onCancelDialogOpen}
                onDeleteExport={this.onDeleteDialogOpen}
            />
            <CreateExportDialog
                isOpen={this.state.isCreateExportDialogOpen}
                isLoading={this.state.isCreating}
                onCreate={this.createExport}
                onDismiss={this.onCreateExportDialogDismiss}
            />
            <ConfirmationDialog
                key="stride-export-delete-dialog"
                isOpen={this.state.isDeleteDialogOpen}
                onDismiss={this.onDeleteDialogDismiss}
                onConfirm={this.deleteExport}
                isLoading={this.state.isDeleting}
                header={<FormattedMessage {...messages.deleteDialogHeader} />}
                description={<FormattedMessage {...messages.deleteDialogDescription} />}
                confirmButtonMessage={<FormattedMessage {...messages.deleteDialogConfirmButton} />}
                dismissButtonMessage={<FormattedMessage {...messages.deleteDialogDismissButton} />}
                screenEvent={strideDeleteExportDialogScreenEvent()}
                confirmAnalyticsEvent={actionedExportId ? strideConfirmDeleteExportButtonClickEvent(actionedExportId) : null}
                dismissAnalyticsEvent={actionedExportId ? strideCancelDeleteExportButtonClickEvent(actionedExportId) : null}
            />
            <ConfirmationDialog
                key="stride-export-cancel-dialog"
                isOpen={this.state.isCancelDialogOpen}
                onDismiss={this.onCancelDialogDismiss}
                onConfirm={this.cancelExport}
                isLoading={this.state.isCancelling}
                header={<FormattedMessage {...messages.cancelDialogHeader} />}
                description={<FormattedMessage {...messages.cancelDialogDescription} />}
                confirmButtonMessage={<FormattedMessage {...messages.cancelDialogConfirmButton} />}
                dismissButtonMessage={<FormattedMessage {...messages.cancelDialogDismissButton} />}
                screenEvent={strideCancelExportDialogScreenEvent()}
                confirmAnalyticsEvent={actionedExportId ? strideConfirmCancelExportButtonClickEvent(actionedExportId) : null}
                dismissAnalyticsEvent={actionedExportId ? strideCancelCancelExportButtonClickEvent(actionedExportId) : null}
            />
          </ScreenEventSender>
        </PageLayout>
    );
  }

  public getPageAction() {
    const {
        data: {
            stride: {
                strideExports = [],
            } = {},
            loading = false,
        } = {},
        intl: { formatMessage },
    } = this.props;

    const anyExportsPending = strideExports.some(e => e.status === 'pending');
    const button = (
      <AkButton
        onClick={this.onCreateExportDialogOpen}
        appearance="primary"
        isDisabled={anyExportsPending || loading}
      >
        <FormattedMessage {...messages.createExportDialogButton} />
      </AkButton>
    );

    return anyExportsPending
      ? (
        <AkTooltip
          content={formatMessage(messages.createExportDialogButtonDisabledTooltip)}
        >
          {button}
        </AkTooltip>
      )
      : button;
  }

  private onDeleteDialogOpen = (exportId: string) => {
    this.setState({
      isDeleteDialogOpen: true,
      actionedExportId: exportId,
    });
  }
  private onDeleteDialogDismiss = () => this.setState({
    isDeleteDialogOpen: false,
    actionedExportId: null,
  });

  private onCancelDialogOpen = (exportId: string) => {
    this.setState({
      isCancelDialogOpen: true,
      actionedExportId: exportId,
    });
  }

  private onCancelDialogDismiss = () => this.setState({ isCancelDialogOpen: false });

  private onCreateExportDialogOpen = () => {
    this.props.analyticsClient.sendUIEvent(strideCreateExportButtonClickEvent());
    this.setState({ isCreateExportDialogOpen: true });
  }
  private onCreateExportDialogDismiss = () => this.setState({ isCreateExportDialogOpen: false });

  private cancelExport = (): void => {
    const strideExportId = this.state.actionedExportId;
    const {
      match: {
        params: { cloudId },
      },
      data: {
        stride = null,
      } = {},
    } = this.props;

    if (!strideExportId || !stride) {
      return;
    }

    this.setState({
      isCancelling: true,
    });
    this.props.cancelStrideExport({
      variables: {
        cloudId,
        exportId: strideExportId,
      },
      update: (cache) => {
        this.updateExportsInCache(cache, cloudId, stride.strideExports.map(e => (
          e.id === strideExportId
            ? { ...e, status: 'cancelled' }
            : e
        )));
      },
    }).then(() => {
      this.setState({
        isCancelDialogOpen: false,
        actionedExportId: null,
        isCancelling: false,
      });
    }).catch(() => {
      this.setState({
        isCancelling: false,
      });
    });
  };

  private deleteExport = (): void => {
    const strideExportId = this.state.actionedExportId;
    if (!strideExportId) {
      return;
    }
    const {
          match: {
              params: { cloudId },
          },
          deleteStrideExport,
      } = this.props;

    this.setState({
      isDeleting: true,
    });
    deleteStrideExport({
      variables: {
        cloudId,
        exportId: strideExportId,
      },
      update: (cache) => {
        this.removeExportFromCache(cache, cloudId, strideExportId);
      },
    }).then(() => {
      this.setState({
        isDeleteDialogOpen: false,
        actionedExportId: null,
        isDeleting: false,
      });
    }).catch(() => {
      this.setState({
        isDeleting: false,
      });
    });
  };

  private createExport = (formData: CreateExportFormData) => {
    const {
          match: {
              params: { cloudId },
          },
          createStrideExport,
      } = this.props;
    this.setState({
      isCreating: true,
    });
    createStrideExport({
      variables: {
        cloudId,
        input: this.normalizeFormDataToInput(formData),
      },
      update: (cache, { data }) => {
        this.addExportToCache(cache, cloudId, data!.createStrideExport);
      },
    }).then(() => {
      this.setState({
        isCreating: false,
        isCreateExportDialogOpen: false,
      });
    }).catch(() => {
      this.setState({
        isCreating: false,
      });
    });
  };

  private addExportToCache = (cache, cloudId, strideExport) => {
    const {
      data: {
        stride = null,
      } = {},
    } = this.props;

    if (!stride) {
      return;
    }

    this.updateExportsInCache(cache, cloudId, [
      strideExport,
      ...stride.strideExports,
    ]);
  };

  private removeExportFromCache = (cache, cloudId, exportId) => {
    const {
      data: {
        stride = null,
      } = {},
    } = this.props;

    if (!stride) {
      return;
    }

    this.updateExportsInCache(cache, cloudId, stride.strideExports.filter(e => e.id !== exportId));
  };

  private updateExportsInCache = (cache, cloudId, strideExports) => {
    const queryVariables = { cloudId };
    const queryResult = cache.readQuery({
      query: strideExportsQuery,
      variables: {
        cloudId,
      },
    }) as StrideExportsQuery;
    if (!queryResult) {
      return;
    }
    const { stride } = queryResult;
    cache.writeQuery({
      query: strideExportsQuery,
      variables: queryVariables,
      data: {
        stride: {
          ...stride,
          strideExports,
        },
      },
    });
  };

  private normalizeFormDataToInput = (data: CreateExportFormData): CreateStrideExportInput => {
    const {
      includePublicRooms,
      includePrivateRooms,
      includeDirects,
      oldest,
      newest,
      passphrase,
    } = data;

    return {
      includePublicRooms,
      includePrivateRooms,
      includeDirects,
      ...(oldest === '' ? {} : { startDate: oldest }),
      ...(newest === '' ? {} : { endDate: newest }),
      passphrase,
    };
  };

}

const withExports = graphql<RouteComponentProps<SiteParams>, StrideExportsQuery, StrideExportsQueryVariables>(strideExportsQuery, {
  options: ({ match: { params: { cloudId } } }) => {
    return {
      variables: {
        cloudId,
      },
      pollInterval: 60000,
    };
  },
});

const withCreateExport = graphql<RouteComponentProps<SiteParams>, CreateStrideExportMutation, {}, CreateDerivedProps>(CreateStrideExport, {
  name: 'createStrideExport',
});

const withDeleteExport = graphql<RouteComponentProps<SiteParams> & CreateDerivedProps, DeleteStrideExportMutation, {}, DeleteDerivedProps>(DeleteStrideExport, {
  name:  'deleteStrideExport',
});

const withCancelExport = graphql<RouteComponentProps<SiteParams> & CreateDerivedProps & DeleteDerivedProps, CancelStrideExportMutation, {}, CancelDerivedProps>(CancelStrideExport, {
  name:  'cancelStrideExport',
});

export const StridePage =
  withRouter(
    withExports(
      withCreateExport(
        withDeleteExport(
          withCancelExport(
            injectIntl(
              withAnalyticsClient(
                StridePageImpl,
              ),
            ),
          ),
        ),
      ),
    ),
  );
