import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';

import { createMockAnalyticsClient, createMockIntlProp } from '../../utilities/testing';

import { DownloadButtonImpl } from './download';

describe('Stride Download Button', () => {
  const getProps = () => ({
    link: 'http://atlassian.com',
    compact: false,
    message: {
      id: 'a',
      defaultMessage: 'a',
    },
    intl: createMockIntlProp(),
    analyticsClient: createMockAnalyticsClient(),
  });

  it('should set download attr to false if props.download is false', () => {
    const wrapper = mount(
      <DownloadButtonImpl {...getProps()} download={false} />,
    );
    const a = wrapper.find('a');
    expect(a.prop('download')).to.equal(false);
  });

  it('should set download attr to true if props.download is true and filename is not provided', () => {
    const wrapper = mount(
      <DownloadButtonImpl {...getProps()} download={true} />,
    );
    const a = wrapper.find('a');
    expect(a.prop('download')).to.equal(true);
  });

  it('should set download attr to filename if props.download is true and filename is provided', () => {
    const wrapper = mount(
      <DownloadButtonImpl {...getProps()} download={true} filename="x.png"/>,
    );
    const a = wrapper.find('a');
    expect(a.prop('download')).to.equal('x.png');
  });
});
