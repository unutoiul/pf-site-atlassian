import * as React from 'react';
import {
  FormattedMessage,
  InjectedIntlProps,
  injectIntl,
} from 'react-intl';
import styled from 'styled-components';

import AkButton from '@atlaskit/button';
import AkDownloadIcon from '@atlaskit/icon/glyph/download';
import AkTooltip from '@atlaskit/tooltip';

import { AnalyticsClientProps, UIEvent, withAnalyticsClient } from 'common/analytics';

const DownloadLink = styled.a`
  &:hover{
   text-decoration: none;
  }
`;

interface OwnProps {
  link: string;
  compact: boolean;
  message: FormattedMessage.MessageDescriptor;
  download?: boolean;
  filename?: string;
  analyticsUIEvent?: UIEvent;
}

type DownloadProps = OwnProps & InjectedIntlProps & AnalyticsClientProps;

export class DownloadButtonImpl extends React.Component<DownloadProps> {
  public static defaultProps: Partial<DownloadProps> = {
    download: true,
  };

  public render() {
    const {
      link,
      compact,
      message,
      download,
      filename,
      intl: { formatMessage },
    } = this.props;
    const downloadAttr = download ? filename || true : false;
    if (compact) {
      return (
        <AkTooltip content={<span>{formatMessage(message)}</span>}>
          <DownloadLink href={link} download={downloadAttr}>
            <AkButton
              iconAfter={
                <AkDownloadIcon
                  label={formatMessage(message)}
                />
              }
              onClick={this.onDownload}
            />
          </DownloadLink>
        </AkTooltip>
      );
    } else {
      return (
        <DownloadLink href={link} download={downloadAttr}>
          <AkButton
            iconBefore={
              <AkDownloadIcon
                label={formatMessage(message)}
              />
            }
            onClick={this.onDownload}
          >
            {formatMessage(message)}
          </AkButton>
        </DownloadLink>
      );
    }
  }

  private onDownload = () => {
    const { analyticsUIEvent } = this.props;
    if (analyticsUIEvent) {
      this.props.analyticsClient.sendUIEvent(analyticsUIEvent);
    }
  }
}

export const DownloadButton = injectIntl(withAnalyticsClient(DownloadButtonImpl));
