import * as React from 'react';
import {
  defineMessages,
  injectIntl,
} from 'react-intl';
import styled from 'styled-components';

import { akGridSizeUnitless } from '@atlaskit/util-shared-styles';

import { UIEvent } from 'common/analytics';

import { DownloadButton } from './download';

const DownloadButtonWrapper = styled.div`
  margin: ${akGridSizeUnitless / 2}px;
  margin-left: 0;
  display: inline-block;
`;

export const messages = defineMessages({
  integrationsTooltip: {
    id: 'stride.integrations',
    defaultMessage: 'Download CSV file',
    description: 'Text to be displayed on a tooltip for a button that downloads an integration package',
  },
});

interface OwnProps {
  link: string;
  downloadAnalyticsEvent?: UIEvent;
}
type Props = OwnProps;

export const IntegrationsDownloadImpl = (props: Props) => {
  const {
    downloadAnalyticsEvent,
  } = props;

  return (
      <React.Fragment>
        <DownloadButtonWrapper>
          <DownloadButton
            message={messages.integrationsTooltip}
            analyticsUIEvent={downloadAnalyticsEvent}
            compact={false}
            download={false}
            {...props}
          />
        </DownloadButtonWrapper>
      </React.Fragment>
  );
};

export const IntegrationsDownload = injectIntl(IntegrationsDownloadImpl);
