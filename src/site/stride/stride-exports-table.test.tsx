import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { sandbox as sinonSandbox } from 'sinon';

import AkDynamicTable from '@atlaskit/dynamic-table';

import { StrideExportStatus } from '../../../src/schema/schema-types';
import { createMockAnalyticsClient, createMockIntlProp } from '../../utilities/testing';
import { StrideExportsTableImpl } from './stride-exports-table';

describe('StrideExportsTable', () => {
  const sandbox = sinonSandbox.create();
  const getProps = () => ({
    exports: [
      {
        id: 'e1',
        createdAt: '1999-01-01',
        startedAt: '1999-01-01',
        expiresAt: '1999-01-15',
        status: 'completed' as StrideExportStatus,
        downloadLocation: 'http://atlassian.com' ,
      },
      {
        id: 'e2',
        createdAt: '1999-01-01',
        startedAt: '1999-01-01',
        expiresAt: '1999-01-15',
        status: 'pending' as StrideExportStatus,
      },
      {
        id: 'e3',
        createdAt: '1999-01-01',
        startedAt: '1999-01-01',
        expiresAt: '1999-01-15',
        status: 'completed' as StrideExportStatus,
      },
    ],
    intl: createMockIntlProp(),
    analyticsClient: createMockAnalyticsClient(),
    onCancelExport: sandbox.stub(),
    onDeleteExport: sandbox.stub(),
    onLocationCopy: sandbox.stub(),
    isLoading: false,
  });

  it('should render a AkDynamicTable', () => {
    const props = getProps();
    const wrapper = shallow(
      <StrideExportsTableImpl {...props} />,
    );
    expect(wrapper.find(AkDynamicTable).exists()).to.equal(true);
  });

  it('should render 1 row per export', () => {
    const props = getProps();
    const wrapper = shallow(
      <StrideExportsTableImpl {...props} />,
    );
    const table = wrapper.find(AkDynamicTable);
    expect(table.prop('rows').length).to.equal(props.exports.length);
  });

  it('should call onDeleteExport prop when export is deleted', () => {
    const props = getProps();
    const wrapper = shallow(
      <StrideExportsTableImpl {...props} />,
    );
    const table = wrapper.find(AkDynamicTable);
    const content = shallow(
      table
        .prop('rows')[0]
        .cells
        .find(c => c.key === 'action')
        .content,
    );
    // The actual button is nested in an AnalyticsContext, and ten
    // on top of that is wrapped in 2 HOCs, so this is the most reliably
    // way I thought of to find it
    const button = content.findWhere(n => n.prop('onClick'));
    const onClick: any = button.prop('onClick');
    if (onClick) {
      onClick();
    }
    expect(props.onDeleteExport.called).to.equal(true);
  });

  it('should call onCancelExport prop when export is cancelled', () => {
    const props = getProps();
    const wrapper = shallow(
      <StrideExportsTableImpl {...props} />,
    );
    const table = wrapper.find(AkDynamicTable);
    const content = shallow(
      table
        .prop('rows')[1]
        .cells
        .find(c => c.key === 'action')
        .content,
    );
    const button = content.findWhere(n => n.prop('onClick'));
    const onClick: any = button.prop('onClick');
    if (onClick) {
      onClick();
    }
    expect(props.onCancelExport.called).to.equal(true);
  });

  it('should render download content for a completed export', () => {
    const props = getProps();
    const wrapper = shallow(
      <StrideExportsTableImpl {...props} />,
    );
    const table = wrapper.find(AkDynamicTable);
    const content =
      table
        .prop('rows')[0]
        .cells
        .find(c => c.key === 'download')
        .content;
    expect(content).not.to.equal(null);
  });

  it('should not render download content for a non-completed export', () => {
    const props = getProps();
    const wrapper = shallow(
      <StrideExportsTableImpl {...props} />,
    );
    const table = wrapper.find(AkDynamicTable);
    const content =
      table
        .prop('rows')[1]
        .cells
        .find(c => c.key === 'download')
        .content;
    expect(content).to.equal(null);
  });
});
