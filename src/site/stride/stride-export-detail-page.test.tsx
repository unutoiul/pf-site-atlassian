import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import AkDynamicTable from '@atlaskit/dynamic-table';

import { StrideExportStatus } from '../../../src/schema/schema-types';
import { createMockAnalyticsClient, createMockIntlProp } from '../../utilities/testing';
import { StrideExportDetailPageImpl } from './stride-export-detail-page';
import { StridePendingExportMessage } from './stride-pending-export-message';

describe('StrideExportDetailPage', () => {
  const getProps = () => ({
    data: {
      stride: {
        strideExport: {
          id: 'e1',
          createdAt: '1999-01-01',
          startedAt: '1999-01-01',
          expiresAt: '1999-01-15',
          creatorName: 'Johnny Atlas',
          creatorEmail: 'johnny@las.co',
          status: 'completed' as StrideExportStatus,
          download: { location: 'http://atlassian.com' },
          settings: {
            includePublicRooms: true,
            includePrivateRooms: true,
            includeDirects: true,
          },
        },
      },
      ...{} as any,
    },
    analyticsClient: createMockAnalyticsClient(),
    intl: createMockIntlProp(),
    match: {
      params: {
        cloudId: 'c1',
        exportId: 'e1',
      },
      ...{} as any,
    },
    location: {} as any,
    history: {} as any,
  });

  it('should render a AkDynamicTable', () => {
    const props = getProps();
    const wrapper = shallow(<StrideExportDetailPageImpl {...props}/>);
    expect(wrapper.find(AkDynamicTable).exists()).to.equal(true);
  });

  it('should not render a table if there was an error', () => {
    const props = getProps();
    props.data.error = true;
    const wrapper = shallow(<StrideExportDetailPageImpl {...props}/>);
    expect(wrapper.find(AkDynamicTable).exists()).to.equal(false);
  });

  it('should render a table if data is loading', () => {
    const props = getProps();
    props.data.loading = true;
    const wrapper = shallow(<StrideExportDetailPageImpl {...props}/>);
    expect(wrapper.find(AkDynamicTable).prop('isLoading')).to.equal(true);
  });

  it('should show a message when the export is pending', () => {
    const props = getProps();
    props.data.stride.strideExport.status = 'pending';
    const wrapper = shallow(
      <StrideExportDetailPageImpl {...props} />,
    );

    expect(wrapper.find(StridePendingExportMessage).prop('pending')).to.equal(true);
  });

  it('should NOT show a message when the export is not pending', () => {
    const props = getProps();
    const wrapper = shallow(
      <StrideExportDetailPageImpl {...props} />,
    );

    expect(wrapper.find(StridePendingExportMessage).prop('pending')).to.equal(false);
  });
});
