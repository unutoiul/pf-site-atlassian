import * as React from 'react';
import {
  defineMessages,
  FormattedMessage,
  InjectedIntlProps,
  injectIntl,
} from 'react-intl';

import AkLozenge from '@atlaskit/lozenge';

import { StrideExportStatus } from '../../../src/schema/schema-types';

const statusAppearances: Record<StrideExportStatus, string> = {
  completed: 'success',
  pending: 'inprogress',
  failed: 'removed',
  cancelled: 'moved',
  unknown: 'default',
};

export const messages = defineMessages({
  completed: {
    id: 'stride.export-status.completed',
    defaultMessage: 'Completed',
    description: 'The text to be displayed in a lozenge when the export status is in "completed" state.',
  },
  pending: {
    id: 'stride.export-status.pending',
    defaultMessage: 'In progress',
    description: 'The text to be displayed in a lozenge when the export status is in "pending" state.',
  },
  failed: {
    id: 'stride.export-status.failed',
    defaultMessage: 'Failed',
    description: 'The text to be displayed in a lozenge when the export status is in "failed" state.',
  },
  cancelled: {
    id: 'stride.export-status.cancelled',
    defaultMessage: 'Cancelled',
    description: 'The text to be displayed in a lozenge when the export status is in "cancelled" state.',
  },
  unknown: {
    id: 'stride.export-status.unknown',
    defaultMessage: 'Unknown',
    description: 'The text to be displayed in a lozenge when the export status is in "unknown" state.',
  },
});

interface OwnProps {
  status: StrideExportStatus;
}

type ExportStatusProps = OwnProps & InjectedIntlProps;

export class ExportStatusImpl extends React.Component<ExportStatusProps> {
  public render() {
    const { status } = this.props;

    return (
      <AkLozenge appearance={statusAppearances[status]} isBold={true}>
        <FormattedMessage {...messages[status]} />
      </AkLozenge>
    );
  }
}

export const ExportStatus = injectIntl(ExportStatusImpl);
