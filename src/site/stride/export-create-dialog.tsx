import * as React from 'react';
import {
  defineMessages,
  FormattedMessage,
  InjectedIntlProps,
  injectIntl,
} from 'react-intl';
import styled from 'styled-components';

import { ButtonGroup as AkButtonGroup, default as AkButton } from '@atlaskit/button';
import { Checkbox as AkCheckbox } from '@atlaskit/checkbox';
import { DatePicker as AkDatePicker } from '@atlaskit/datetime-picker';
import AkTextField from '@atlaskit/field-text';
import {
  colors as akColors,
  gridSize as akGridSize,
} from '@atlaskit/theme';

import { Label as AkLabel } from '@atlaskit/field-base';

import { ContentReveal } from 'common/content-reveal';

import {
  AnalyticsClientProps,
  ScreenEventSender,
  strideCancelCreateExportButtonClickEvent,
  strideCreateExportDialogScreenEvent,
  strideSubmitCreateExportButtonClickEvent,
  withAnalyticsClient,
} from 'common/analytics';

import { FieldDescription } from 'common/field-description';
import { ModalDialog } from 'common/modal';

const Grid = styled.div`
  display: grid;
  grid-template-columns: 47.5% 47.5%;
  justify-content: space-between;
`;

const CheckboxContainer = styled.div`
  margin: ${akGridSize() * 2}px 0 0 -${akGridSize()}px;
`;

const DateErrorMessage = styled.div`
  color: ${akColors.R400};
  margin: ${akGridSize() / 2}px 0 0;
`;

export const messages = defineMessages({
  header: {
    id: 'stride.export-create.header',
    defaultMessage: 'Create an export',
    description: 'The header for a dialog that allows the user to create an export',
  },
  createButton: {
    id: 'stride.export-create.createButton',
    defaultMessage: 'Create export',
    description: 'A button that submits a form that creates an export',
  },
  dismissButton: {
    id: 'stride.export-create.dismissButton',
    defaultMessage: 'Cancel',
    description: 'A button that closes a dialog that creates an export',
  },
  includeHeader: {
    id: 'stride.export-create.includeHeader',
    defaultMessage: 'Select what content you would like to export',
    description: 'The header for selecting what conversations to include in an export',
  },
  expiryWarning: {
    id: 'stride.export-create.expiryWarning',
    defaultMessage: 'Your export will be automatically deleted in 14 days. Also note that files from your messages will be included in the export as links to those files, and those links will expire in 14 days.',
    description: 'The warning text that informs users their export will expire after 14 days',
  },
  includePublicRoomsLabel: {
    id: 'stride.export-create.includePublicRoomsLabel',
    defaultMessage: 'Include public rooms',
    description: 'A checkbox for including public rooms in an export',
  },
  includePrivateRoomsLabel: {
    id: 'stride.export-create.includePrivateRoomsLabel',
    defaultMessage: 'Include private rooms',
    description: 'A checkbox for including private rooms in an export',
  },
  includeDirectsLabel: {
    id: 'stride.export-create.includeDirectsLabel',
    defaultMessage: 'Include 1-1 chats',
    description: 'A checkbox for including direct chats in an export',
  },
  defaultDateRange: {
    id: 'stride.export-create.defaultDateRange',
    defaultMessage: 'By default, we will export data from the beginning of your site to the present.',
    description: 'The informational text that tells the user the default date range for the export',
  },
  selectDateRange: {
    id: 'stride.export-create.selectDateRange',
    defaultMessage: 'Or you may select specific dates to export.',
    description: 'The text for the link that allows the user to select a custom date range',
  },
  datePlaceholder: {
    id: 'stride.export-create.datePlaceholder',
    defaultMessage: 'e.g. 09/15/2018',
    description: 'The placeholder for a form field that allows the user to select a date',
  },
  dateErrorInvalid: {
    id: 'stride.export-create.dateErrorInvalid',
    defaultMessage: 'The Oldest and Newest dates you\'ve selected are invalid.',
    description: 'An error message to be displayed when the user tries to submit the form with a start date that is after the end date',
  },
  dateErrorFuture: {
    id: 'stride.export-create.dateErrorFuture',
    defaultMessage: 'The Oldest date cannot be in the future. Please select a date in the past.',
    description: 'An error message to be displayed when the user tries to submit the form with a start date that is in the future',
  },
  oldestLabel: {
    id: 'stride.export-create.oldestLabel',
    defaultMessage: 'Oldest',
    description: 'The label for a form field that allows the user to select the oldest date for history to be in cluded in an export',
  },
  oldestDescription: {
    id: 'stride.export-create.oldestDescription',
    defaultMessage: 'The export will only contain history occurring on or after this date. Leave empty to get the history from the creation of the group (Dates are UTC).',
    description: 'The description for a form field that allows the user to select the oldest date for history to be in cluded in an export',
  },
  newestLabel: {
    id: 'stride.export-create.newestLabel',
    defaultMessage: 'Newest',
    description: 'The label for a form field that allows the user to select the latest date for history to be in cluded in an export',
  },
  newestDescription: {
    id: 'stride.export-create.newestDescription',
    defaultMessage: 'The export will only contain history occurring on or before this date. Leave empty to get the history until today (Dates are UTC).',
    description: 'The description for a form field that allows the user to select the latest date for history to be in cluded in an export',
  },
  passphraseLabel: {
    id: 'stride.export-create.passphraseLabel',
    defaultMessage: 'Passphrase',
    description: 'The label for a text field that allows the user to specify a passphrase',
  },
  passphraseDescription: {
    id: 'stride.export-create.passphraseDescription',
    defaultMessage: 'The passphrase must be at least 8 characters long. It can\'t be recovered, and needs to be communicated to the final owner of the export.',
    description: 'The description for a text field that allows the user to specify a passphrase',
  },
  passphraseConfirmLabel: {
    id: 'stride.export-create.passphraseConfirmLabel',
    defaultMessage: 'Confirm passphrase',
    description: 'The label for a text field that allows the user to confirm their passphrase',
  },
  passphraseErrorLength: {
    id: 'stride.export-create.passphraseErrorLength',
    defaultMessage: 'The passphrase must be at least {MIN_PASSPHRASE_LENGTH} characters',
    description: 'An error message to be displayed when the user tries to submit the form with a passphrase that does not reach the minimum length',
  },
  passphraseErrorDoesNotMatch: {
    id: 'stride.export-create.passphraseErrorDoesNotMatch',
    defaultMessage: 'The passphrases do not match',
    description: 'An error message to be displayed when the user tries to submit the form with a passphrase that does not match their confirmed passphrase',
  },
  passphraseErrorWhitespace: {
    id: 'stride.export-create.passphraseErrorWhitespace',
    defaultMessage: 'The passphrase cannot begin or end with a space',
    description: 'An error message to be displayed when the user tries to submit the form with a passphrase that contains leading or trailing whitespace characters',
  },
});

const MIN_PASSPHRASE_LENGTH = 8;
const enum DateError {
  Invalid,
  Future,
}
const enum PassphraseError {
  Length,
  DoesNotMatch,
  Whitespace,
}

export interface CreateExportFormData {
  includePublicRooms: boolean;
  includePrivateRooms: boolean;
  includeDirects: boolean;
  oldest: string;
  newest: string;
  passphrase: string;
  passphraseConfirm: string;
}

interface OwnProps {
  isOpen: boolean;
  isLoading: boolean;
  onCreate(CreateExportFormData): void;
  onDismiss(): void;
}

type CreateExportDialogProps = OwnProps & InjectedIntlProps & AnalyticsClientProps;

interface CreateExportDialogState {
  formData: CreateExportFormData;
  dateError: DateError | null;
  passphraseError: PassphraseError | null;
}

export class CreateExportDialogImpl extends React.Component<CreateExportDialogProps, CreateExportDialogState> {
  public constructor(props) {
    super(props);
    this.state = {
      formData: {
        includePublicRooms: true,
        includePrivateRooms: true,
        includeDirects: true,
        oldest: '',
        newest: '',
        passphrase: '',
        passphraseConfirm: '',
      },
      dateError: null,
      passphraseError: null,
    };
  }

  public render() {
    const {
      isOpen,
      isLoading,
      intl: { formatMessage },
    } = this.props;
    const {
      formData,
      dateError,
      passphraseError,
    } = this.state;

    return (
      <ModalDialog
        header={<FormattedMessage {...messages.header} />}
        footer={(
          <AkButtonGroup>
            <AkButton
              appearance="primary"
              onClick={this.onCreate}
              isLoading={isLoading}
            >
              <FormattedMessage {...messages.createButton} />
            </AkButton>
            <AkButton
              appearance="subtle-link"
              onClick={this.onDismiss}
              isDisabled={isLoading}
            >
              <FormattedMessage {...messages.dismissButton} />
            </AkButton>
          </AkButtonGroup>
        )}
        isOpen={isOpen}
        width="medium"
        onClose={this.onDismiss}
      >
        <ScreenEventSender
          event={strideCreateExportDialogScreenEvent()}
        >
          <h5><FormattedMessage {...messages.includeHeader} /></h5>
          <FieldDescription><FormattedMessage {...messages.expiryWarning} /></FieldDescription>
          <CheckboxContainer>
            <AkCheckbox
              defaultChecked={formData.includePublicRooms}
              label={formatMessage(messages.includePublicRoomsLabel)}
              onChange={this.onIncludePublicRoomsChange}
              isFullWidth={true}
            />
          </CheckboxContainer>
          <CheckboxContainer>
            <AkCheckbox
              defaultChecked={formData.includePrivateRooms}
              label={formatMessage(messages.includePrivateRoomsLabel)}
              onChange={this.onIncludePrivateRoomsChange}
              isFullWidth={true}
            />
          </CheckboxContainer>
          <CheckboxContainer>
            <AkCheckbox
              defaultChecked={formData.includeDirects}
              label={formatMessage(messages.includeDirectsLabel)}
              onChange={this.onIncludeDirectsChange}
              isFullWidth={true}
            />
          </CheckboxContainer>

          <Grid>
            <AkTextField
              id="create-export-passphrase"
              onChange={this.onPassphraseChange}
              label={formatMessage(messages.passphraseLabel)}
              required={true}
              shouldFitContainer={true}
              type="password"
              value={formData.passphrase}
              isInvalid={passphraseError !== null}
              invalidMessage={this.getPassphraseErrorMessage()}
            />
            <AkTextField
              id="create-export-passphrase-confirm"
              onChange={this.onPassphraseConfirmChange}
              label={formatMessage(messages.passphraseConfirmLabel)}
              required={true}
              shouldFitContainer={true}
              type="password"
              value={formData.passphraseConfirm}
              isInvalid={passphraseError !== null}
              invalidMessage={this.getPassphraseErrorMessage()}
            />
          </Grid>
          <FieldDescription><FormattedMessage {...messages.passphraseDescription} /></FieldDescription>

          <h5><FormattedMessage {...messages.defaultDateRange} /></h5>
          <ContentReveal label={formatMessage(messages.selectDateRange)} onClose={this.onCloseDateRange}>
            <Grid>
              <div>
                <AkLabel htmlFor="react-select-oldest-datepicker--input" label={formatMessage(messages.oldestLabel)}/>
                <AkDatePicker
                  id="oldest-datepicker"
                  dateFormat="MM/DD/YYYY"
                  value={formData.oldest}
                  placeholder={formatMessage(messages.datePlaceholder)}
                  onChange={this.onOldestChange}
                  isInvalid={dateError !== null}
                />
                <FieldDescription><FormattedMessage {...messages.oldestDescription} /></FieldDescription>
              </div>
              <div>
                <AkLabel htmlFor="react-select-newest-datepicker--input" label={formatMessage(messages.newestLabel)}/>
                <AkDatePicker
                  id="newest-datepicker"
                  dateFormat="MM/DD/YYYY"
                  value={formData.newest}
                  placeholder={formatMessage(messages.datePlaceholder)}
                  onChange={this.onNewestChange}
                  isInvalid={dateError === DateError.Invalid}
                />
                <FieldDescription><FormattedMessage {...messages.newestDescription} /></FieldDescription>
              </div>
            </Grid>
            <DateErrorMessage>
              {this.getDateErrorMessage()}
            </DateErrorMessage>
          </ContentReveal>
        </ScreenEventSender>
      </ModalDialog>
    );
  }

  private onIncludePublicRoomsChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState(prevState => ({
      formData: {
        ...prevState.formData,
        includePublicRooms: !!event.target.checked,
      },
    }));
  }

  private onIncludePrivateRoomsChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState(prevState => ({
      formData: {
        ...prevState.formData,
        includePrivateRooms: !!event.target.checked,
      },
    }));
  }

  private onIncludeDirectsChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState(prevState => ({
      formData: {
        ...prevState.formData,
        includeDirects: !!event.target.checked,
      },
    }));
  }

  private onCloseDateRange = () => {
    this.setState(prevState => ({
      formData: {
        ...prevState.formData,
        oldest: '',
        newest: '',
      },
      dateError: null,
    }));
  }

  private onOldestChange = (value) => {
    this.setState(prevState => ({
      formData: {
        ...prevState.formData,
        oldest: value,
      },
    }));
  }

  private onNewestChange = (value) => {
    this.setState(prevState => ({
      formData: {
        ...prevState.formData,
        newest: value,
      },
    }));
  }

  private onPassphraseChange = ({ currentTarget: { value } }) => {
    this.setState(prevState => ({
      formData: {
        ...prevState.formData,
        passphrase: value,
      },
    }));
  }

  private onPassphraseConfirmChange = ({ currentTarget: { value } }) => {
    this.setState(prevState => ({
      formData: {
        ...prevState.formData,
        passphraseConfirm: value,
      },
    }));
  }

  private onCreate = () => {
    const isPassphraseValid = this.validatePassphrase();
    const areDatesValid = this.validateDates();
    if (!isPassphraseValid || !areDatesValid) {
      return;
    }
    this.props.onCreate(this.state.formData);
    this.props.analyticsClient.sendUIEvent(strideSubmitCreateExportButtonClickEvent());
  }

  private onDismiss = () => {
    this.props.onDismiss();
    this.props.analyticsClient.sendUIEvent(strideCancelCreateExportButtonClickEvent());
  }

  private validateDates = () => {
    const { oldest, newest } = this.state.formData;
    let error: DateError | null = null;

    if (oldest && newest && oldest > newest) {
      error = DateError.Invalid;
    } else if (oldest && oldest > new Date().toISOString()) {
      error = DateError.Future;
    }

    this.setState({
      dateError: error,
    });

    return error === null;
  }

  private validatePassphrase = () => {
    const { passphrase, passphraseConfirm } = this.state.formData;
    let error: PassphraseError | null = null;

    if (passphrase.length < MIN_PASSPHRASE_LENGTH) {
      error = PassphraseError.Length;
    } else if (/^\s|\s$/.test(passphrase)) {
      error = PassphraseError.Whitespace;
    } else if (passphrase !== passphraseConfirm) {
      error = PassphraseError.DoesNotMatch;
    }

    this.setState({
      passphraseError: error,
    });

    return error === null;
  }

  private getDateErrorMessage = () => {
    const err = this.state.dateError;
    if (err === DateError.Invalid) {
      return <FormattedMessage {...messages.dateErrorInvalid} />;
    }
    if (err === DateError.Future) {
      return <FormattedMessage {...messages.dateErrorFuture} />;
    }

    return null;
  }

  private getPassphraseErrorMessage = () => {
    const err = this.state.passphraseError;
    if (err === PassphraseError.Length) {
      return <FormattedMessage {...messages.passphraseErrorLength} values={{ MIN_PASSPHRASE_LENGTH }}/>;
    }
    if (err === PassphraseError.DoesNotMatch) {
      return <FormattedMessage {...messages.passphraseErrorDoesNotMatch} />;
    }
    if (err === PassphraseError.Whitespace) {
      return <FormattedMessage {...messages.passphraseErrorWhitespace} />;
    }

    return null;
  }

}

export const CreateExportDialog = injectIntl(
  withAnalyticsClient(
    CreateExportDialogImpl,
  ),
);
