import * as React from 'react';
import {
  defineMessages,
  InjectedIntlProps,
  injectIntl,
} from 'react-intl';
import styled from 'styled-components';

import { akGridSizeUnitless } from '@atlaskit/util-shared-styles';

import { UIEvent } from 'common/analytics';
import { TextCopy } from 'common/text-copy';

import { DownloadButton } from './download';

const DownloadButtonWrapper = styled.div`
  margin: ${akGridSizeUnitless / 2}px;
  margin-left: 0;
  display: inline-block;
`;

export const messages = defineMessages({
  downloadTooltip: {
    id: 'stride.download',
    defaultMessage: 'Download package',
    description: 'Text to be displayed on a tooltip for a button that downloads an export package',
  },
  copyDownloadLink: {
    id: 'stride.copyDownloadLink',
    defaultMessage: 'Copy download link',
    description: 'Text to be displayed on a tooltip for a button that copies the download url to the clipboard',
  },
  downloadLink: {
    id: 'stride.downloadLink',
    defaultMessage: 'Download link',
    description: 'Text to be displayed on the success flag when a user copies the download link to their clipboard',
  },
});

interface OwnProps {
  link: string;
  compact: boolean;
  downloadAnalyticsEvent?: UIEvent;
  copyAnalyticsEvent?: UIEvent;
}
type Props = OwnProps & InjectedIntlProps;

export const ExportDownloadImpl = (props: Props) => {
  const {
    downloadAnalyticsEvent,
    copyAnalyticsEvent,
    intl: { formatMessage },
  } = props;

  return (
      <React.Fragment>
        <DownloadButtonWrapper>
          <DownloadButton
            message={messages.downloadTooltip}
            analyticsUIEvent={downloadAnalyticsEvent}
            {...props}
          />
        </DownloadButtonWrapper>
        <DownloadButtonWrapper>
          <TextCopy
            value={props.link}
            label={formatMessage(messages.downloadLink)}
            compact={true}
            compactTooltipMessage={formatMessage(messages.copyDownloadLink)}
            analyticsUIEvent={copyAnalyticsEvent}
          />
        </DownloadButtonWrapper>
      </React.Fragment>
  );
};

export const ExportDownload = injectIntl(ExportDownloadImpl);
