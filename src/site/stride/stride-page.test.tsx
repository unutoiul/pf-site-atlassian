import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { sandbox as sinonSandbox } from 'sinon';

import { PageLayout } from 'common/page-layout';

import { createMockAnalyticsClient, createMockIntlContext, createMockIntlProp } from '../../utilities/testing';

import { ConfirmationDialog } from './confirmation-dialog';
import { CreateExportDialog } from './export-create-dialog';
import { StrideExportsTable } from './stride-exports-table';
import { messages, StridePageImpl } from './stride-page';
import { StridePendingExportMessage } from './stride-pending-export-message';

describe('StridePage', () => {
  const sandbox = sinonSandbox.create();
  const getProps: any = () => ({
    data: {
      stride: {
        strideExports: [
          {
            id: 'e1',
            status: 'completed',
            createdAt: '1999-01-01',
            startedAt: '1999-01-01',
            expiresAt: '1999-01-15',
          },
        ],
      },
      loading: false,
      error: null,
    },
    intl: createMockIntlProp(),
    location: null as any,
    match: {
      params: {
        cloudId: '1',
      },
    },
    analyticsClient: createMockAnalyticsClient(),
    deleteStrideExport: sandbox.stub().returns(Promise.resolve()),
    cancelStrideExport: sandbox.stub().returns(Promise.resolve()),
    createStrideExport: sandbox.stub().returns(Promise.resolve()),
    showFlag: sandbox.stub(),
    history: null as any,
  });

  const getMockCache: any = () => ({
    readQuery: sandbox.stub().returns({ stride: {} }),
    writeQuery: sandbox.stub(),
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('should render an error if there is an error', () => {
    const props = getProps();
    props.data.error = 'idksomethingtruthy';
    const wrapper = shallow(
      <StridePageImpl {...props} />,
    );
    const message = wrapper.find(FormattedMessage);
    expect(message.exists()).to.equal(true);
    expect(message.prop('defaultMessage')).to.equal(messages.error.defaultMessage);
  });

  it('should show the exports table', () => {
    const wrapper = shallow(
      <StridePageImpl {...getProps()} />,
      createMockIntlContext(),
    );
    expect(wrapper.find(StrideExportsTable).exists()).to.equal(true);
  });

  it('should show an action button that opens the create dialog', () => {
    const wrapper = shallow(
      <StridePageImpl {...getProps()} />,
    );

    expect(wrapper.find(CreateExportDialog).prop('isOpen')).to.equal(false);
    const action = shallow(wrapper.find(PageLayout).prop('action') as any);
    (action.findWhere(n => n.prop('onClick')).prop('onClick'))();
    wrapper.update();
    expect(wrapper.find(CreateExportDialog).prop('isOpen')).to.equal(true);

  });

  it('should disable the action button when there is a pending export', () => {
    const props = getProps();
    props.data.stride.strideExports.push({
      id: 'e4',
      status: 'pending',
    });
    const wrapper = shallow(
      <StridePageImpl {...props} />,
    );

    expect(wrapper.find(CreateExportDialog).prop('isOpen')).to.equal(false);
    const action = shallow(wrapper.find(PageLayout).prop('action') as any);
    const button = action.findWhere(n => n.prop('onClick'));
    expect(button.prop('isDisabled')).to.equal(true);
  });

  it('should show a message when there is a pending export', () => {
    const props = getProps();
    props.data.stride.strideExports.push({
      id: 'e4',
      status: 'pending',
    });
    const wrapper = shallow(
      <StridePageImpl {...props} />,
    );

    expect(wrapper.find(StridePendingExportMessage).prop('pending')).to.equal(true);
  });

  it('should NOT show a message when there are zero pending exports', () => {
    const props = getProps();
    const wrapper = shallow(
      <StridePageImpl {...props} />,
    );

    expect(wrapper.find(StridePendingExportMessage).prop('pending')).to.equal(false);
  });

  it('should set correct state when export is deleted from table', () => {
    const props = getProps();
    const wrapper = shallow(
      <StridePageImpl {...props} />,
    );
    const table = wrapper.find(StrideExportsTable);
    table.prop('onDeleteExport')('e1');
    expect(wrapper.state('isDeleteDialogOpen')).to.equal(true);
    expect(wrapper.state('actionedExportId')).to.equal('e1');
  });

  it('should set correct state when export is cancelled from table', () => {
    const props = getProps();
    const wrapper = shallow(
      <StridePageImpl {...props} />,
    );
    const table = wrapper.find(StrideExportsTable);
    table.prop('onCancelExport')('e1');
    expect(wrapper.state('isCancelDialogOpen')).to.equal(true);
    expect(wrapper.state('actionedExportId')).to.equal('e1');
  });

  describe('deleteStrideExport', () => {
    it('should call deleteStrideExport when export is deleted from the dialog', () => {
      const props = getProps();
      const wrapper = shallow(
        <StridePageImpl {...props} />,
    );

      const table = wrapper.find(StrideExportsTable);
      table.prop('onDeleteExport')('e1');

      const dialog = wrapper
        .find(ConfirmationDialog)
        .at(0);
      const onConfirm: any = dialog.prop('onConfirm');
      if (onConfirm) {
        onConfirm();
      }
      expect(props.deleteStrideExport.args[0][0]).to.deep.include({
        variables: {
          cloudId: '1',
          exportId: 'e1',
        },
      });
    });

    it('should delete the export from the cache', () => {
      const props = getProps();
      const wrapper = shallow(
        <StridePageImpl {...props} />,
    );

      const table = wrapper.find(StrideExportsTable);
      table.prop('onDeleteExport')('e1');

      const dialog = wrapper
        .find(ConfirmationDialog)
        .at(0);
      const onConfirm: any = dialog.prop('onConfirm');
      if (onConfirm) {
        onConfirm();
      }

      const updaterFn = props.deleteStrideExport.args[0][0].update;
      const mockCache = getMockCache();
      updaterFn(mockCache);
      expect(mockCache.writeQuery.args[0][0].data).to.deep.include({
        stride: {
          ...props.data.stride,
          strideExports: [],
        },
      });
    });

  });

  describe('cancelStrideExport', () => {
    it('should call cancelStrideExport when export is cancelled from the dialog', () => {
      const props = getProps();
      const wrapper = shallow(
        <StridePageImpl {...props} />,
    );

      const table = wrapper.find(StrideExportsTable);
      table.prop('onCancelExport')('e1');

      const dialog = wrapper
        .find(ConfirmationDialog)
        .at(1);
      const onConfirm: any = dialog.prop('onConfirm');
      if (onConfirm) {
        onConfirm();
      }
      expect(props.cancelStrideExport.args[0][0]).to.deep.include({
        variables: {
          cloudId: '1',
          exportId: 'e1',
        },
      });
    });

    it('should cancel the export in the cache', () => {
      const props = getProps();
      const wrapper = shallow(
        <StridePageImpl {...props} />,
      );

      const table = wrapper.find(StrideExportsTable);
      table.prop('onCancelExport')('e1');

      const dialog = wrapper
        .find(ConfirmationDialog)
        .at(1);
      const onConfirm: any = dialog.prop('onConfirm');
      if (onConfirm) {
        onConfirm();
      }

      const updaterFn = props.cancelStrideExport.args[0][0].update;
      const mockCache = getMockCache();
      updaterFn(mockCache);
      expect(mockCache.writeQuery.args[0][0].data).to.deep.include({
        stride: {
          ...props.data.stride,
          strideExports: [{
            ...props.data.stride.strideExports.find(e => e.id === 'e1'),
            status: 'cancelled',
          }],
        },
      });
    });
  });

  describe('createStrideExport', () => {
    it('should call createStrideExport when an export is created from the dialog', () => {
      const props = getProps();
      const wrapper = shallow(
        <StridePageImpl {...props} />,
    );

      wrapper.find(CreateExportDialog).prop('onCreate')({
        includePublicRooms: true,
        includePrivateRooms: false,
        includeDirects: true,
        oldest: '2018-01-01',
        newest: '2018-01-20',
        passphrase: 'abcdefghijklmnopq',
        passphraseConfirm: 'abcdefghijklmnopq',
      });

      expect(props.createStrideExport.args[0][0]).to.deep.include({
        variables: {
          cloudId: props.match.params.cloudId,
          input: {
            includePublicRooms: true,
            includePrivateRooms: false,
            includeDirects: true,
            startDate: '2018-01-01',
            endDate: '2018-01-20',
            passphrase: 'abcdefghijklmnopq',
          },
        },
      });
    });

    it('should add the export to the cache', () => {
      const props = getProps();
      const wrapper = shallow(
        <StridePageImpl {...props} />,
    );

      wrapper.find(CreateExportDialog).prop('onCreate')({
        includePublicRooms: true,
        includePrivateRooms: false,
        includeDirects: true,
        oldest: '2018-01-01',
        newest: '2018-01-20',
        passphrase: 'abcdefghijklmnopq',
        passphraseConfirm: 'abcdefghijklmnopq',
      });

      const updaterFn = props.createStrideExport.args[0][0].update;
      const mockCache = getMockCache();
      updaterFn(mockCache, { data: { createStrideExport: { id: 'e2' } } });
      expect(mockCache.writeQuery.args[0][0].data).to.deep.include({
        stride: {
          ...props.data.stride,
          strideExports: [
            { id: 'e2' },
            ...props.data.stride.strideExports,
          ],
        },
      });
    });
  });
});
