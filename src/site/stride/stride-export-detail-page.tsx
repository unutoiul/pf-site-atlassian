import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import {
  defineMessages,
  FormattedDate,
  FormattedMessage,
  InjectedIntlProps,
  injectIntl,
} from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router-dom';

import AkDynamicTable, { Row as AkRow } from '@atlaskit/dynamic-table';

import {
  AnalyticsClientProps,
  ScreenEventSender,
  strideCopyExportButtonClickEvent,
  strideDownloadAppInstallsButtonClickEvent,
  strideDownloadExportButtonClickEvent,
  strideExportDetailScreen,
  strideExportDetailScreenScreenEvent,
  withAnalyticsClient,
} from 'common/analytics';
import { getConfig } from 'common/config';
import { PageLayout } from 'common/page-layout';

import { StrideExport, StrideExportDetailQuery } from '../../schema/schema-types';
import { ExportDownload } from './export-download';
import { IntegrationsDownload } from './integrations-download';
import { ExportStatus } from './status';
import { StridePendingExportMessage } from './stride-pending-export-message';

import StrideExportDetail from './stride-export-detail.query.graphql';

export const messages = defineMessages({
  title: {
    id: 'stride.export-detail.title',
    defaultMessage: 'Stride Exports',
    description: 'The title for a page that displays details on a Stride export',
  },
  fieldNameId: {
    id: 'stride.export-detail.fieldNameId',
    defaultMessage: 'Export ID',
    description: 'The field name displayed for the id of an export',
  },
  fieldNameStatus: {
    id: 'stride.export-detail.fieldNameStatus',
    defaultMessage: 'Status',
    description: 'The field name displayed for the status of an export',
  },
  fieldNameStartedAt: {
    id: 'stride.export-detail.fieldNameStartedAt',
    defaultMessage: 'Export created',
    description: 'The field name displayed for the creation datetime of an export',
  },
  fieldNameExpiresAt: {
    id: 'stride.export-detail.fieldNameExpiresAt',
    defaultMessage: 'Export expires',
    description: 'The field name displayed for the expiry datetime of an export',
  },
  fieldNameCreator: {
    id: 'stride.export-detail.fieldNameCreator',
    defaultMessage: 'Export creator',
    description: 'The field name displayed for the creator of an export',
  },
  fieldNameOldest: {
    id: 'stride.export-detail.fieldNameOldest',
    defaultMessage: 'Oldest message history',
    description: 'The field name displayed for the date of the oldest history in an export',
  },
  fieldNameNewest: {
    id: 'stride.export-detail.fieldNameNewest',
    defaultMessage: 'Newest message history',
    description: 'The field name displayed for the date of the newest history in an export',
  },
  fieldNameIncludePublicRooms: {
    id: 'stride.export-detail.fieldNameIncludePublicRooms',
    defaultMessage: 'Include public room messages',
    description: 'The field name displayed for whether or not public room history was included in the export',
  },
  fieldNameIncludePrivateRooms: {
    id: 'stride.export-detail.fieldNameIncludePrivateRooms',
    defaultMessage: 'Include private room messages',
    description: 'The field name displayed for whether or not private room history was included in the export',
  },
  fieldNameIncludeDirects: {
    id: 'stride.export-detail.fieldNameIncludeDirects',
    defaultMessage: 'Include 1-1 messages',
    description: 'The field name displayed for whether or not direct conversation history was included in the export',
  },
  fieldNameDownload: {
    id: 'stride.export-detail.fieldNameDownload',
    defaultMessage: 'Stride export archive',
    description: 'The field name displayed for the download buttons of the export',
  },
  fieldNameIntegrations: {
    id: 'stride.export-detail.fieldNameIntegrations',
    defaultMessage: 'Stride App installs CSV',
    description: 'The field name displayed for the download button of the integration export',
  },
  valueTrue: {
    id: 'stride.export-detail.valueTrue',
    defaultMessage: 'Yes',
    description: 'The message to be displayed to represent a true boolean value',
  },
  valueFalse: {
    id: 'stride.export-detail.valueFalse',
    defaultMessage: 'No',
    description: 'The message to be displayed to represent a false boolean value',
  },
  error: {
    id: 'stride.export-detail.error',
    defaultMessage: `We couldn't retrieve the export. Try again soon.`,
    description: 'The description of an error that occurs when the user\'s export could not be retreived',
  },
});

type PickedStrideExport = Pick<StrideExport, 'id' | 'startedAt' | 'expiresAt' | 'creatorName' | 'creatorEmail' | 'status' | 'settings' | 'downloadLocation' | 'integrationsLocation'>;
interface RouteParams {
  cloudId: string;
  exportId: string;
}

type GraphQLDerivedProps = ChildProps<{}, StrideExportDetailQuery>;
type StrideExportDetailPageProps = InjectedIntlProps & RouteComponentProps<RouteParams> & GraphQLDerivedProps & AnalyticsClientProps;
export class StrideExportDetailPageImpl extends React.Component<StrideExportDetailPageProps> {
  public render() {
    const {
      match: {
        params: {
          exportId,
        },
      },
      data: {
        stride: {
          strideExport = null,
        } = {},
        loading = false,
        error = null,
      } = {},
    } = this.props;

    const isExportPending = strideExport !== null && strideExport.status === 'pending';

    if (error) {
      return (
        <PageLayout
            title={<FormattedMessage {...messages.title} values={{ exportId }}/>}
        >
          <span><FormattedMessage {...messages.error} /></span>
        </PageLayout>
      );
    }

    return (
        <PageLayout
          title={<FormattedMessage {...messages.title} />}
          isFullWidth={true}
        >
          <ScreenEventSender
            isDataLoading={loading}
            event={strideExportDetailScreenScreenEvent()}
          >
            <StridePendingExportMessage
              pending={isExportPending}
            />
            <AkDynamicTable
              isLoading={loading}
              head={this.getHeader()}
              rows={strideExport ? this.getRowsFromStrideExport(strideExport) : null}
              isFixedSize={true}
            />
          </ScreenEventSender>
        </PageLayout >
    );
  }

  private getHeader = () => {
    return {
      cells: [
        {
          key: 'fieldName',
          width: 33,
        },
        {
          key: 'value',
        },
      ],
    };
  }

  private getRowsFromStrideExport = (strideExport: PickedStrideExport): AkRow[] => {
    const {
      match: {
        params: {
          cloudId, exportId,
        },
      },
    } = this.props;
    const rows = [
      this.getRow(messages.fieldNameId, strideExport.id),
      this.getRow(messages.fieldNameStatus, <ExportStatus status={strideExport.status} />),
      this.getRow(messages.fieldNameCreator, this.getExportCreator(strideExport.creatorName, strideExport.creatorEmail)),
      this.getRow(messages.fieldNameStartedAt, (
        <FormattedDate
          value={strideExport.startedAt}
          month="long"
          day="2-digit"
          year="numeric"
          hour="2-digit"
          minute="2-digit"
          second="2-digit"
        />
      )),
      this.getRow(messages.fieldNameExpiresAt, strideExport.expiresAt ? (
        <FormattedDate
          value={strideExport.expiresAt}
          month="long"
          day="2-digit"
          year="numeric"
          hour="2-digit"
          minute="2-digit"
          second="2-digit"
        />
      ) : null),
      this.getRow(messages.fieldNameIncludePublicRooms, this.getValueFromBoolean(strideExport.settings.includePublicRooms)),
      this.getRow(messages.fieldNameIncludePrivateRooms, this.getValueFromBoolean(strideExport.settings.includePrivateRooms)),
      this.getRow(messages.fieldNameIncludeDirects, this.getValueFromBoolean(strideExport.settings.includeDirects)),
    ];

    if (strideExport.settings.startDate) {
      rows.push(
        this.getRow(messages.fieldNameOldest, (
          <FormattedDate
            value={strideExport.settings.startDate}
            month="2-digit"
            day="2-digit"
            year="numeric"
          />
        )),
      );
    }

    if (strideExport.settings.endDate) {
      rows.push(
        this.getRow(messages.fieldNameNewest, (
          <FormattedDate
            value={strideExport.settings.endDate}
            month="2-digit"
            day="2-digit"
            year="numeric"
          />
        )),
      );
    }

    if (strideExport.downloadLocation) {
      rows.push(
        this.getRow(messages.fieldNameDownload, (
          <ExportDownload
            compact={false}
            link={strideExport.downloadLocation}
            downloadAnalyticsEvent={strideDownloadExportButtonClickEvent(strideExport.id, strideExportDetailScreen)}
            copyAnalyticsEvent={strideCopyExportButtonClickEvent(strideExport.id, strideExportDetailScreen)}
          />
        )),
      );
    }

    if (strideExport.integrationsLocation) {
      const integrationsUrl = `${getConfig().strideUrl}/${cloudId}/export/integrations/${exportId}`;
      rows.push(
        this.getRow(messages.fieldNameIntegrations, (
          <IntegrationsDownload
            link={integrationsUrl}
            downloadAnalyticsEvent={strideDownloadAppInstallsButtonClickEvent(strideExport.id, strideExportDetailScreen)}
          />
        )),
      );
    }

    return rows;
  }

  private getRow = (fieldMessage: FormattedMessage.MessageDescriptor, value) => {
    return {
      cells: [
        {
          key: 'fieldName',
          content: <b><FormattedMessage {...fieldMessage} /></b>,
        },
        {
          key:  'value',
          content: value,
        },
      ],
    };
  }

  private getValueFromBoolean = (b: boolean) => {
    return b
      ? <FormattedMessage {...messages.valueTrue} />
      : <FormattedMessage {...messages.valueFalse} />;
  }

  private getExportCreator = (creatorName, creatorEmail) => {
    return `${creatorName} - ${creatorEmail}`;
  }
}

type WithStrideExportInProps = InjectedIntlProps & RouteComponentProps<RouteParams> & AnalyticsClientProps;
type WithStrideExportsOutProps = StrideExportDetailPageProps;
const withStrideExport = graphql<WithStrideExportInProps, StrideExportDetailQuery, {}, WithStrideExportsOutProps>(StrideExportDetail, {
  options:  ({ match:  { params:  { cloudId, exportId } } }) => {
    return {
      variables:  {
        cloudId, exportId,
      },
      pollInterval: 60000,
    };
  },
});

export const StrideExportDetailPage = injectIntl(
  withRouter(
    withAnalyticsClient(
      withStrideExport(
        StrideExportDetailPageImpl,
      ),
    ),
  ),
);
