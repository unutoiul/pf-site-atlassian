import * as React from 'react';

import { ButtonGroup as AkButtonGroup, default as AkButton } from '@atlaskit/button';

import { AnalyticsClientProps, ScreenEvent, ScreenEventSender, UIEvent, withAnalyticsClient } from 'common/analytics';
import { ModalDialog } from 'common/modal';

interface OwnProps {
  header: React.ReactNode;
  description: React.ReactNode;
  confirmButtonMessage: React.ReactNode;
  dismissButtonMessage: React.ReactNode;
  isOpen: boolean;
  screenEvent?: ScreenEvent;
  confirmAnalyticsEvent?: UIEvent | null;
  dismissAnalyticsEvent?: UIEvent | null;
  isLoading: boolean;
  onConfirm(): void;
  onDismiss(): void;
}

type ConfirmationDialogImplProps = OwnProps & AnalyticsClientProps;
export class ConfirmationDialogImpl extends React.Component<ConfirmationDialogImplProps> {
  public render() {
    const {
      isOpen,
      isLoading,
      header,
      confirmButtonMessage,
      dismissButtonMessage,
      description,
      screenEvent,
      onDismiss,
    } = this.props;

    const wrappedDescription = screenEvent
      ? (
        <ScreenEventSender
          event={screenEvent}
        >
         {description}
        </ScreenEventSender>
      )
      : description;

    return (
      <ModalDialog
        header={header}
        footer={(
          <AkButtonGroup>
            <AkButton
              appearance="danger"
              onClick={this.onConfirm}
              isLoading={isLoading}
            >
            {confirmButtonMessage}
            </AkButton>
            <AkButton
              appearance="subtle-link"
              onClick={this.onDismiss}
              isDisabled={isLoading}
            >
              {dismissButtonMessage}
            </AkButton>
          </AkButtonGroup>
        )}
        isOpen={isOpen}
        width="small"
        onClose={onDismiss}
      >
        {wrappedDescription}
      </ModalDialog>
    );
  }

  private onConfirm = () => {
    if (this.props.confirmAnalyticsEvent) {
      this.props.analyticsClient.sendUIEvent(this.props.confirmAnalyticsEvent);
    }
    this.props.onConfirm();
  }

  private onDismiss = () => {
    if (this.props.dismissAnalyticsEvent) {
      this.props.analyticsClient.sendUIEvent(this.props.dismissAnalyticsEvent);
    }
    this.props.onDismiss();
  }
}

export const ConfirmationDialog = withAnalyticsClient(ConfirmationDialogImpl);
