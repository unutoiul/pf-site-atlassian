import * as React from 'react';
import {
  defineMessages,
  FormattedMessage,
  InjectedIntlProps,
  injectIntl,
} from 'react-intl';
import styled from 'styled-components';

import { gridSize as akGridSize } from '@atlaskit/theme';

import AkSectionMessage from '@atlaskit/section-message';

const Wrapper = styled.div`
  margin-bottom: ${akGridSize() * 3}px;
`;

export const messages = defineMessages({
  pendingMessage: {
    id: 'stride.pending.export.message',
    defaultMessage: 'There is an export job currently in progress. Exporting your data may take some time. We\'ll send you an email when your export is finished.',
    description: 'Text to be displayed in a section message when an export is pending',
  },
});

interface OwnProps {
  pending: boolean;
}

type StridePendingExportMessageProps = InjectedIntlProps & OwnProps;
export class StridePendingExportMessageImpl extends React.Component<StridePendingExportMessageProps> {

  public render() {
    const { pending } = this.props;

    if (!pending) {
      return null;
    }

    return (
      <Wrapper>
        <AkSectionMessage appearance="info">
          <p><FormattedMessage {...messages.pendingMessage} /></p>
        </AkSectionMessage>
      </Wrapper>
    );
  }

}

export const StridePendingExportMessage = injectIntl(
  StridePendingExportMessageImpl,
);
