import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import AkSectionMessage from '@atlaskit/section-message';

import { createMockAnalyticsClient, createMockIntlProp } from '../../utilities/testing';
import { StridePendingExportMessageImpl } from './stride-pending-export-message';

describe('StridePendingExportMessage', () => {
  const getProps = () => ({
    pending: false,
    analyticsClient: createMockAnalyticsClient(),
    intl: createMockIntlProp(),
    match: {
      params: {
        cloudId: 'c1',
        exportId: 'e1',
      },
      ...{} as any,
    },
    location: {} as any,
    history: {} as any,
  });

  it('should render a AkSectionMessage if props.pending is true', () => {
    const props = getProps();
    props.pending = true;
    const wrapper = shallow(<StridePendingExportMessageImpl {...props}/>);
    expect(wrapper.find(AkSectionMessage).exists()).to.equal(true);
  });

  it('should not render a AkSectionMessage if props.pending is false', () => {
    const props = getProps();
    const wrapper = shallow(<StridePendingExportMessageImpl {...props}/>);
    expect(wrapper.find(AkSectionMessage).exists()).to.equal(false);
  });
});
