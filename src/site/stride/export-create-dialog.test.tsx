import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { sandbox as sinonSandbox } from 'sinon';

import { Checkbox as AkCheckbox } from '@atlaskit/checkbox';
import AkTextField from '@atlaskit/field-text';

import { default as AkButton } from '@atlaskit/button';
import { DatePicker as AkDatePicker } from '@atlaskit/datetime-picker';

import { ModalDialog } from 'common/modal';

import { createMockAnalyticsClient, createMockIntlProp } from '../../utilities/testing';
import { CreateExportDialogImpl } from './export-create-dialog';

describe('Create Export Dialog', () => {
  const sandbox = sinonSandbox.create();
  const getProps = () => ({
    onCreate: sandbox.stub(),
    onDismiss: sandbox.stub(),
    isOpen: true,
    isLoading: false,
    analyticsClient: createMockAnalyticsClient(),
    intl: createMockIntlProp(),
  });

  it('should not call onCreate prop if form is submitted and the passphrase is less than 8 characters', () => {
    const props = getProps();
    const wrapper = shallow(<CreateExportDialogImpl {...props}/>);
    wrapper.find(AkTextField).filterWhere(n => n.prop('id') === 'create-export-passphrase').prop('onChange')!({
      currentTarget: {
        value: 'abc',
      },
      ...{} as any,
    });

    const footer = shallow(wrapper.find(ModalDialog).prop('footer') as React.ReactElement<any>);
    footer.find(AkButton).first().prop('onClick')!();
    expect(props.onCreate.called).not.to.equal(true);
  });

  it('should not call onCreate prop if form is submitted and the passphrase has leading/trailing space characters', () => {
    const props = getProps();
    const wrapper = shallow(<CreateExportDialogImpl {...props}/>);
    wrapper.find(AkTextField).filterWhere(n => n.prop('id') === 'create-export-passphrase').prop('onChange')!({
      currentTarget: {
        value: '  abcd  ',
      },
      ...{} as any,
    });

    const footer = shallow(wrapper.find(ModalDialog).prop('footer') as React.ReactElement<any>);
    footer.find(AkButton).first().prop('onClick')!();
    expect(props.onCreate.called).not.to.equal(true);
  });

  it('should not call onCreate prop if form is submitted and the passphrase is 8 space characters', () => {
    const props = getProps();
    const wrapper = shallow(<CreateExportDialogImpl {...props}/>);
    wrapper.find(AkTextField).filterWhere(n => n.prop('id') === 'create-export-passphrase').prop('onChange')!({
      currentTarget: {
        value: '        ',
      },
      ...{} as any,
    });

    const footer = shallow(wrapper.find(ModalDialog).prop('footer') as React.ReactElement<any>);
    footer.find(AkButton).first().prop('onClick')!();
    expect(props.onCreate.called).not.to.equal(true);
  });

  it('should not call onCreate prop if form is submitted and the passphrases do not match', () => {
    const props = getProps();
    const wrapper = shallow(<CreateExportDialogImpl {...props}/>);

    wrapper.find(AkTextField).filterWhere(n => n.prop('id') === 'create-export-passphrase').prop('onChange')!({
      currentTarget: {
        value: 'abcdefghijklmnopq',
      },
      ...{} as any,
    });

    wrapper.find(AkTextField).filterWhere(n => n.prop('id') === 'create-export-passphrase-confirm').prop('onChange')!({
      currentTarget: {
        value: 'abcdeklmnopq',
      },
      ...{} as any,
    });

    const footer = shallow(wrapper.find(ModalDialog).prop('footer') as React.ReactElement<any>);
    footer.find(AkButton).first().prop('onClick')!();
    expect(props.onCreate.called).not.to.equal(true);
  });

  it('should call onCreate prop if form is submitted and the passphrases match', () => {
    const props = getProps();
    const wrapper = shallow(<CreateExportDialogImpl {...props}/>);

    wrapper.find(AkTextField).filterWhere(n => n.prop('id') === 'create-export-passphrase').prop('onChange')!({
      currentTarget: {
        value: 'abcdefghijklmnopq',
      },
      ...{} as any,
    });

    wrapper.find(AkTextField).filterWhere(n => n.prop('id') === 'create-export-passphrase-confirm').prop('onChange')!({
      currentTarget: {
        value: 'abcdefghijklmnopq',
      },
      ...{} as any,
    });

    const footer = shallow(wrapper.find(ModalDialog).prop('footer') as React.ReactElement<any>);
    footer.find(AkButton).first().prop('onClick')!();
    expect(props.onCreate.called).to.equal(true);
  });

  it('should call onCreate prop with correct values when submitted', () => {
    const props = getProps();
    const wrapper = shallow(<CreateExportDialogImpl {...props}/>);

    type onChange = (x: any) => any;
    (wrapper.find(AkCheckbox).at(0).prop<onChange>('onChange'))({ target: { checked: true } });
    (wrapper.find(AkCheckbox).at(1).prop<onChange>('onChange'))({ target: { checked: false } });
    (wrapper.find(AkCheckbox).at(2).prop<onChange>('onChange'))({ target: { checked: true } });

    (wrapper.find(AkDatePicker).at(0).prop<onChange>('onChange'))('2018-01-01');
    (wrapper.find(AkDatePicker).at(1).prop<onChange>('onChange'))('2018-01-20');

    wrapper.find(AkTextField).filterWhere(n => n.prop('id') === 'create-export-passphrase').prop('onChange')!({
      currentTarget: {
        value: 'abcdefghijklmnopq',
      },
      ...{} as any,
    });

    wrapper.find(AkTextField).filterWhere(n => n.prop('id') === 'create-export-passphrase-confirm').prop('onChange')!({
      currentTarget: {
        value: 'abcdefghijklmnopq',
      },
      ...{} as any,
    });

    const footer = shallow(wrapper.find(ModalDialog).prop('footer') as React.ReactElement<any>);
    footer.find(AkButton).first().prop('onClick')!();

    expect(props.onCreate.args[0][0]).to.deep.equal({
      includePublicRooms: true,
      includePrivateRooms: false,
      includeDirects: true,
      oldest: '2018-01-01',
      newest: '2018-01-20',
      passphrase: 'abcdefghijklmnopq',
      passphraseConfirm: 'abcdefghijklmnopq',
    });

  });
});
