import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';

import { createMockIntlProp } from '../../utilities/testing';
import { UserConnectedAppsImpl } from './user-connected-apps';
import { generateApps } from './user-connected-apps-mock-data-utils';

const cloudId = 'DUMMY_CLOUD_ID';
const pageDefaults = {
  intl: createMockIntlProp(),
};

storiesOf('Site|User-Connected Apps/User-Connected Apps', module)
  .add('Loading', () => {

    return (
      <UserConnectedAppsImpl {...pageDefaults} cloudId="1234" />
    );
  })
  .add('Error', () => {
    return (
      <UserConnectedAppsImpl
        {...pageDefaults}
        cloudId="1234"
        data={{ error: { message: 'Error Loading Data' } } as any}
      />
    );
  })
  .add('Working', () => {
    return (
      <Router>
        <UserConnectedAppsImpl
          {...pageDefaults}
          cloudId="1234"
          data={{
            site: {
              __typename: 'Site',
              id: cloudId,
              userConnectedApps: generateApps(25),
            },
          } as any}
        />
      </Router>
    );
  });
