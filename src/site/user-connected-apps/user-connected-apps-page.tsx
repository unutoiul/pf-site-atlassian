import * as React from 'react';
import {
  defineMessages,
  InjectedIntlProps,
  injectIntl,
} from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';

import { PageLayout } from 'common/page-layout';

import {
  UserConnectedAppsPageFeatureFlag,
  withUserConnectedAppsPageFeatureFlag,
} from '../feature-flags/with-user-connected-apps-page-feature-flag';
import { UserConnectedApps } from './user-connected-apps';

const messages = defineMessages({
  pageTitle: {
    id: 'user-management.site-settings.user-connected-apps.page.title',
    description: 'User-connected apps page title',
    defaultMessage: 'Connected apps',
  },
  pageDescription: {
    id: 'user-management.site-settings.user-connected-apps.page.description',
    description: 'User-connected apps page description',
    defaultMessage: 'Users have authorized these apps to connect to your site on their behalf. View the details for each app to see a list of permissions the app is allowed to access for each user.',
  },
});

interface SiteParams {
  cloudId: string;
}

type Props = RouteComponentProps<SiteParams> & InjectedIntlProps & UserConnectedAppsPageFeatureFlag;

export class UserConnectedAppsPageImpl extends React.Component<Props> {
  public render() {
    const {
      intl: {
        formatMessage,
      },
      userConnectedAppsFeatureFlag,
    } = this.props;

    if (!userConnectedAppsFeatureFlag.value) {
      return null;
    }

    return (
      <PageLayout
        title={formatMessage(messages.pageTitle)}
        description={formatMessage(messages.pageDescription)}
        isFullWidth={true}
      >
        <UserConnectedApps
          cloudId={this.props.match.params.cloudId}
        />
      </PageLayout>
    );
  }
}

export const UserConnectedAppsPage = withRouter<{}>(
  injectIntl(
    withUserConnectedAppsPageFeatureFlag(UserConnectedAppsPageImpl),
  ),
);
