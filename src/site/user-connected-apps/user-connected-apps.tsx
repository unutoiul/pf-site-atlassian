import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import { defineMessages, FormattedMessage } from 'react-intl';

import AkDynamicTable, { HeaderRow as AkHeaderRow, Row as AkRow } from '@atlaskit/dynamic-table';
import AkSpinner from '@atlaskit/spinner';

import {
  analyticsClient,
  ScreenEventSender,
  userConnectedAppsScreenEvent,
} from 'common/analytics';
import { GenericError, ValidationError } from 'common/error';
import { EmptyTableView } from 'common/table';

import { UserConnectedAppsQuery } from '../../schema/schema-types';
import { AppDetailsLink } from './user-connected-apps-details-link';
import { AppSummary } from './user-connected-apps-summary';
import userConnectedAppsDataQuery from './user-connected-apps.query.graphql';

export const DEFAULT_ROWS_PER_PAGE = 20;
export const messages = defineMessages({
  tableEmpty: {
    id: 'user-management.site-settings.user-connected-apps.table.empty',
    description: 'Message in the user-connected apps table listing when no apps are found.',
    defaultMessage: 'No apps connected',
  },
});

export interface UserConnectedAppsProps {
  cloudId: string;
}

type Props = ChildProps<UserConnectedAppsProps, UserConnectedAppsQuery>;

export class UserConnectedAppsImpl extends React.Component<Props> {
  public render() {
    const {
      data,
      cloudId,
    } = this.props;

    if (data && data.error) {
      const error = data.error;
      if (ValidationError.findIn(error).some(({ code }) => code.startsWith('DAC-') || code.startsWith('DACDEV-'))) {
        analyticsClient.onError(error);
      }

      return <GenericError />;
    }

    if (!data || data.loading) {
      return (
        <AkSpinner />
      );
    }

    const head: AkHeaderRow = {
      cells: [],
    };

    const rows: AkRow[] = data.site
      ? data.site.userConnectedApps.map(app => {

        return {
          cells: [
            {
              key: 'appSummary',
              content: <AppSummary app={app} />,
            },
            {
              key: 'appDetailsLink',
              content: <AppDetailsLink app={app} cloudId={cloudId}/>,
            },
          ],
        };
      }) : [];

    const emptyView = (
        <EmptyTableView>
          <FormattedMessage {...messages.tableEmpty} />
        </EmptyTableView>
      );

    return (
      <ScreenEventSender event={userConnectedAppsScreenEvent(cloudId, rows.length)}>
        <AkDynamicTable
          emptyView={emptyView}
          head={head}
          rows={rows}
          rowsPerPage={DEFAULT_ROWS_PER_PAGE}
        />
      </ScreenEventSender>
    );
  }
}

export const withUserConnectedAppsQuery = graphql<UserConnectedAppsProps, UserConnectedAppsQuery>(
  userConnectedAppsDataQuery,
  {
    options: (componentProps) => ({
      variables: {
        id: componentProps.cloudId,
      },
    }),
  },
);

export const UserConnectedApps = withUserConnectedAppsQuery(UserConnectedAppsImpl);
