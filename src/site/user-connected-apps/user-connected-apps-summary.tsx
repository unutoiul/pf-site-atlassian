import * as React from 'react';
import styled from 'styled-components';

import AkAvatar from '@atlaskit/avatar';
import {
  colors as akColors,
  fontSize as akFontSize,
  gridSize as akGridSize,
} from '@atlaskit/theme';

import { TruncatedField } from 'common/truncated-field';

import { UserConnectedAppsQuery } from '../../schema/schema-types';

const Container = styled.div`
  display: flex;
  padding: ${akGridSize() / 2}px;
`;

const AppDetails = styled.div`
  padding-left: ${akGridSize}px;
`;

const AppName = styled.div`
  color: ${akColors.text};
`;

const AppDescription = styled.div`
  color: ${akColors.subtleText};
  font-size: ${akFontSize() * 0.85}px;
`;

export class AppSummary extends React.Component<{ app: UserConnectedAppsQuery['site']['userConnectedApps'][0] }> {
  public render() {
    const {
      app,
    } = this.props;

    return (
      <Container key={app.id}>
        <AkAvatar
          src={app.avatarUrl}
          appearance="square"
        />
        <AppDetails>
          <AppName>{app.name}</AppName>
          <AppDescription>
            <TruncatedField maxWidth={`${akGridSize() * 82}px`}>
              {app.description}
            </TruncatedField>
          </AppDescription>
        </AppDetails>
      </Container>
    );
  }
}
