import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import { Link } from 'react-router-dom';

import { gridSize as akGridSize } from '@atlaskit/theme';

import {
  AnalyticsClientProps,
  Referrer,
  userConnectedAppsDetailsLinkClickEvent,
  withAnalyticsClient,
} from 'common/analytics';

import { UserConnectedAppsQuery } from '../../schema/schema-types';
import { util } from '../../utilities/admin-hub';

export const messages = defineMessages({
  appDetailsLabel: {
    id: 'user-management.site-settings.user-connected-apps.table.show.access',
    description: 'Link label to show which users have given access to an app',
    defaultMessage: 'Authorization details',
  },
});

type AppDetailsLinkProps = {
  cloudId: string;
  app: UserConnectedAppsQuery['site']['userConnectedApps'][0];
} & InjectedIntlProps & AnalyticsClientProps;

class AppDetailsLinkImpl extends React.Component<AppDetailsLinkProps> {
  public render() {
    const {
      app,
      cloudId,
      intl: { formatMessage },
    } = this.props;

    return (
      <Referrer value="appDetailsLink">
        <Link
          style={{ float: 'right', marginRight: `${akGridSize()}px` }}
          to={`${util.siteAdminBasePath}/s/${cloudId}/user-connected-apps/${app.id}`}
          onClick={this.sendUserConnectedAppsDetailsLinkClickEvent(cloudId, app.id)}
        >
          {formatMessage(messages.appDetailsLabel)}
        </Link>
      </Referrer>
    );
  }

  private sendUserConnectedAppsDetailsLinkClickEvent = (cloudId: string, oAuthClientId: string) => () => {
    this.props.analyticsClient.sendUIEvent(userConnectedAppsDetailsLinkClickEvent(cloudId, oAuthClientId));
  }
}

export const AppDetailsLink = injectIntl(
  withAnalyticsClient(
    AppDetailsLinkImpl,
  ),
);
