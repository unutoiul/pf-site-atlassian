import Loadable from 'react-loadable';

import { PageLoading } from 'common/loading';

export const LoadableUserConnectedAppsPage = Loadable({
  loader: async () => (
    // tslint:disable-next-line space-in-parens
    await import(/* webpackChunkName: "user-connected-apps-page" */ './user-connected-apps-page')
  ).UserConnectedAppsPage,
  loading: PageLoading,
});
