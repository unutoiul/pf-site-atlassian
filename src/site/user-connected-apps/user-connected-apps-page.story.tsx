import { storiesOf } from '@storybook/react';
import * as React from 'react';
import ApolloProvider from 'react-apollo/ApolloProvider';
import { IntlProvider } from 'react-intl';
import { Route, StaticRouter } from 'react-router-dom';

import { configureApolloClientWithSiteFeatureFlag } from 'common/feature-flags/test-utils';

import { createApolloClient } from '../../apollo-client';
import { createMockIntlProp } from '../../utilities/testing';

import { userConnectedAppsPageFeatureFlagKey } from '../feature-flags/flag-keys';
import { generateApps, writeAppsQuery } from './user-connected-apps-mock-data-utils';
import { UserConnectedAppsPage } from './user-connected-apps-page';

const cloudId = 'DUMMY_CLOUD_ID';
const pageDefaults = {
  intl: createMockIntlProp(),
  history: { replace: () => null } as any,
  match: { params: { cloudId } } as any,
  location: window.location as any,
};

const createClientWithSiteFeatureFlags = () => {
  return configureApolloClientWithSiteFeatureFlag({
    // tslint:disable-next-line no-console
    client: createApolloClient(console.log.bind(console)),
    flagKey: userConnectedAppsPageFeatureFlagKey,
    flagValue: true,
    siteId: cloudId,
  });
};

const CreateTestComponent = ({ client }) => {
  return (
    <IntlProvider locale="en">
      <ApolloProvider client={client}>
        <StaticRouter context={{}} location={`/admin/s/${cloudId}/user-connected-apps`}>
          <Route path="/admin/s/:cloudId/user-connected-apps">
            <UserConnectedAppsPage {...pageDefaults} />
          </Route>
        </StaticRouter>
      </ApolloProvider>
    </IntlProvider>
  );
};

storiesOf('Site|User-Connected Apps/User-Connected Apps Page', module)
  .add('With apps', () => {
    const client = createClientWithSiteFeatureFlags();
    writeAppsQuery(cloudId, client, generateApps(25));

    return (
      <CreateTestComponent client={client} />
    );
  })
  .add('With no apps', () => {
    const client = createClientWithSiteFeatureFlags();
    writeAppsQuery(cloudId, client, []);

    return (
      <CreateTestComponent client={client} />
    );
  });
