import ApolloClient from 'apollo-client/ApolloClient';

import userConnectedAppsUserQuery from 'common/account/connected-account.query.graphql';

import { ConnectedAccountQuery, UserConnectedAppsQuery } from '../../schema/schema-types';

import userConnectedAppsQuery from './user-connected-apps.query.graphql';

export type QueriedApp = UserConnectedAppsQuery['site']['userConnectedApps'][0];

export const generateAppId = (appNum: number) => `app-${appNum}`;

export const generateApps = (limit: number, scopes: QueriedScope[] = [], ...userGrants: QueriedGrant[]): QueriedApp[] => {
  return Array.from({ length: limit }, (_, i) => {
    const appNum = i + 1;

    return {
      __typename: 'UserConnectedApp',
      id: generateAppId(appNum),
      name: `App number ${appNum}`,
      vendorName: `Vendor ${appNum}`,
      description: `This is the ${appNum}${
        // tslint:disable-next-line no-sparse-arrays no-bitwise
      [, 'st', 'nd', 'rd'][appNum % 100 >> 3 ^ 1 && appNum % 10] || 'th'
        } app! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque turpis leo, fringilla quis est nec, tristique congue turpis.`,
      avatarUrl: null,
      scopes,
      userGrants,
    };
  });
};

export type QueriedScope = QueriedApp['scopes'][0];

export const generateScopes = (...scopeNames: string[]): QueriedScope[] => scopeNames.map(name => ({
  id: name.toLowerCase().replace(' ', ':'),
  untranslatedName: name,
  untranslatedDescription: `Lets you ${name.toLowerCase()}`,
  __typename: 'AppScope',

}));

export type QueriedGrant = QueriedApp['userGrants'][0];

export const generateGrant = (appId: string, accountId: string, createdAt: string, scopes: QueriedScope[]): QueriedGrant => ({
  __typename: 'UserGrant',
  id: `by-${accountId}-for-${appId}`,
  accountId,
  createdAt,
  scopes,
});

export const writeAppsQuery = (cloudId: string, client: ApolloClient<any>, appsData: QueriedApp[]) => {
  client.writeQuery<UserConnectedAppsQuery>({
    query: userConnectedAppsQuery,
    variables: {
      id: cloudId,
    },
    data: {
      site: {
        __typename: 'Site',
        id: cloudId,
        userConnectedApps: appsData,
      },
    },
  });
};

export const generateUserId = (displayName: string) => 'user:id:' + displayName.toLowerCase().replace(' ', '-');

export const writeUserDetailsQuery = (client: ApolloClient<any>, cloudId: string, displayName: string, exists: boolean, active = true) => {
  client.writeQuery({
    query: userConnectedAppsUserQuery,
    variables: {
      cloudId,
      id: generateUserId(displayName),
    },
    data: !exists ? { user: null } : {
      user: {
        __typename: 'User',
        userDetails: {
          __typename: 'UserDetails',
          id: generateUserId(displayName),
          email: `${displayName.toLowerCase().replace(' ', '.')}@example.com`,
          displayName,
          active,
        },
      },
    } as ConnectedAccountQuery,
  });
};
