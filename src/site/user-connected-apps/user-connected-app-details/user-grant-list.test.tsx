import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import { generateAppId, generateApps, generateGrant, generateScopes } from '../user-connected-apps-mock-data-utils';
import { UserGrantListView, UserGrantListViewProps } from './user-grant-list-view';

import { Props, UserGrantListImpl } from './user-grant-list';

describe('User-connected apps grant list', () => {
  const generateApp = (): Props['app'] => ({
    id: '418',
    name: 'Jira for teapots',
    description: 'Works with most cafe series appliances',
    avatarUrl: null,
    scopes: [],
    userGrants: [],
    vendorName: 'Sunbeam',
  });

  function createTestComponent(props: Partial<Props>) {
    const defaultProps: Props = {
      analyticsClient: {
        init: sinon.stub(),
        sendScreenEvent: sinon.stub(),
        sendTrackEvent: sinon.stub(),
        sendUIEvent: sinon.stub(),
      },
      cloudId: 'cloud-id',
      app: generateApp(),
    };

    return shallow(<UserGrantListImpl {...defaultProps} {...props} />);
  }

  const genericAppId = generateAppId(1);
  const genericAccountId = 'account:id:1';
  const genericGrantedDate = '2018-11-05T04:45:53.518Z';
  const genericScopes = generateScopes('brew peppermint tea', 'brew english breakfast');
  const genericGrantScopes = generateScopes('brew peppermint tea', 'alter thermostat temperature');
  const genericGrants = [
    generateGrant(genericAppId, genericAccountId, genericGrantedDate, genericGrantScopes),
  ];

  const generateTestApp = (scopes = genericScopes, grants = genericGrants) =>
    generateApps(1, scopes, ...grants)[0];

  it('Sets view components props correctly', () => {
    const app = generateTestApp();
    const componentUnderTest = createTestComponent({ app });
    const userGrantListView = componentUnderTest.find(UserGrantListView);
    const props: UserGrantListViewProps = userGrantListView.props();

    expect(props.cloudId).to.equal('cloud-id');
    expect(props.pageNumber).to.equal(1);
    expect(props.entries.length).to.equal(1);

    const entry = props.entries[0];
    expect(entry.accountId).to.equal(app.userGrants[0].accountId);
    expect(entry.createdAt).to.equal(app.userGrants[0].createdAt);
    expect(entry.expanded).to.be.false();
    expect(entry.grantId).to.equal(`by-${app.userGrants[0].accountId}-for-${genericAppId}`);
  });

  it('Works with multiple rows', () => {
    const app = generateTestApp(
      genericScopes,
      [
        generateGrant(genericAppId, 'account:id:1', genericGrantedDate, genericGrantScopes),
        generateGrant(genericAppId, 'account:id:2', genericGrantedDate, genericGrantScopes),
        generateGrant(genericAppId, 'account:id:3', genericGrantedDate, genericGrantScopes),
      ],
    );
    const componentUnderTest = createTestComponent({ app });
    const props: UserGrantListViewProps = componentUnderTest.find(UserGrantListView).props();
    const entries = props.entries;

    expect(entries).to.have.lengthOf(3);
    expect(entries[0].expanded).to.be.false();
    expect(entries[1].expanded).to.be.false();
    expect(entries[2].expanded).to.be.false();
  });

  it('Shows and hides granted scopes list on collapse/expand', () => {
    const app = generateTestApp();
    const componentUnderTest = createTestComponent({ app });
    let props: UserGrantListViewProps = componentUnderTest.update().find(UserGrantListView).props();

    expect(props.entries.length).to.equal(1);
    expect(props.entries[0].expanded).to.be.false();

    props.onUpdateEntryExpanded(props.entries[0], true);
    props = componentUnderTest.update().find(UserGrantListView).props();
    expect(props.entries[0].expanded).to.be.true();
  });

  it('Responds to changing of page number', () => {
    const app = generateTestApp();
    const componentUnderTest = createTestComponent({ app });
    let props: UserGrantListViewProps = componentUnderTest.update().find(UserGrantListView).props();

    expect(props.pageNumber).to.equal(1);

    props.onSetPage(2);
    props = componentUnderTest.update().find(UserGrantListView).props();
    expect(props.pageNumber).to.equal(2);

    props.onSetPage(4);
    props = componentUnderTest.update().find(UserGrantListView).props();
    expect(props.pageNumber).to.equal(4);
  });
});
