import * as React from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';
import styled from 'styled-components';

import AkAvatar from '@atlaskit/avatar';
import { colors as akColors, gridSize as akGridSize } from '@atlaskit/theme';

const LogoGraphicWrapper = styled.div`
  display: flex;
  flex-direction: row;
`;

const TextContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding-left: ${akGridSize() * 2}px;
`;

const LogoText = styled.div`
  font-weight: 600;
  font-size: ${akGridSize() * 3}px;
`;

const secondaryTextRelativeSize = 0.85;

const VendorNameText = styled.div`
  color: ${akColors.subtleText};
  font-size: ${secondaryTextRelativeSize}rem;
  font-weight: 400;
`;
const messages = defineMessages({
  createdBy: {
    id: 'user-management.site-settings.user-connected-app-details.createdBy',
    description: 'User-connected app details created by text',
    defaultMessage: 'Created by {vendorName}',
  },
});

interface LogoProps {
  name: string;
  vendorName?: string | null;
  url?: string | null;
}

export class LogoWithAppName extends React.Component<LogoProps> {

  public render() {
    const vendorName = this.props.vendorName;

    return (
      <LogoGraphicWrapper>
        <AkAvatar src={this.props.url} alt={this.props.name} appearance="square" size="xlarge" />
        <TextContainer>
          <LogoText>{this.props.name}</LogoText>
          {vendorName ? <VendorNameText><FormattedMessage {...messages.createdBy} values={{ vendorName }}/></VendorNameText> : ''}
        </TextContainer>
      </LogoGraphicWrapper>
    );
  }
}
