import { storiesOf } from '@storybook/react';
import ApolloClient from 'apollo-client/ApolloClient';
import * as React from 'react';
import { DataValue } from 'react-apollo';
import ApolloProvider from 'react-apollo/ApolloProvider';
import { IntlProvider } from 'react-intl';
import { Route, StaticRouter } from 'react-router-dom';

import { configureApolloClientWithSiteFeatureFlag } from 'common/feature-flags/test-utils';

import { createApolloClient } from '../../../apollo-client';
import { UserConnectedAppsQuery } from '../../../schema/schema-types';
import { createMockIntlProp } from '../../../utilities/testing';
import { userConnectedAppsPageFeatureFlagKey } from '../../feature-flags/flag-keys';
import {
  generateAppId,
  generateApps,
  generateGrant,
  generateScopes,
  generateUserId,
  writeAppsQuery,
  writeUserDetailsQuery,
} from '../user-connected-apps-mock-data-utils';

import { UserConnectedAppsDetailsPage, UserConnectedAppsDetailsPageImpl } from './user-connected-apps-details-page';

const cloudId = '1289-1245';
const appId = generateAppId(1);
const location = `/admin/s/${cloudId}/user-connected-apps/${appId}`;

const pageDefaults = {
  intl: createMockIntlProp(),
  history: { replace: () => null } as any,
  match: { params: { cloudId, appId } } as any,
  location: location as any,
  userConnectedAppsFeatureFlag: { value: true, isLoading: false },
};

const createClientWithFeatureFlags = (appsPage = true) => {
  // tslint:disable-next-line no-console
  const client = createApolloClient(console.log.bind(console));
  configureApolloClientWithSiteFeatureFlag({
    client,
    flagKey: userConnectedAppsPageFeatureFlagKey,
    flagValue: appsPage,
    siteId: cloudId,
  });

  return client;
};

const createTestComponent = (details: DataValue<UserConnectedAppsQuery>) => {
  return <UserConnectedAppsDetailsPageImpl {...pageDefaults} data={details} />;
};

const StoryContext: React.SFC<{ client: ApolloClient<any> }> = ({ client, children }) => {
  return (
    <IntlProvider locale="en">
      <ApolloProvider client={client}>
        <StaticRouter context={{}} location={location}>
          <Route path="/admin/s/:cloudId/user-connected-apps/:appId">
            {children}
          </Route>
        </StaticRouter>
      </ApolloProvider>
    </IntlProvider>
  );
};

const grantingUsers = [
  'Quirinus Quirrell',
  'Gilderoy Lockhart',
  'Remus Lupin',
  'Alastor Moody',
  'Dolores Umbridge',
  'Severus Snape',
  'Amycus Carrow',
];

storiesOf('Site|User-Connected Apps/User-Connected Apps Details Page', module)
  .add('Default', () => {
    const client = createClientWithFeatureFlags();

    grantingUsers
      .filter(name => name !== 'Alastor Moody')
      .forEach((name) => writeUserDetailsQuery(
        client,
        cloudId,
        name,
        name !== 'Severus Snape',
        name !== 'Quirinus Quirrell',
      ));
    const grants = grantingUsers.map((name, index) => generateGrant(
      appId,
      generateUserId(name),
      `2018-11-02T01:32:4${index}.246Z`,
      generateScopes('teach defence against the dark arts'),
    ));
    grants[grants.length - 1].scopes = generateScopes('teach the dark arts');
    grants[grants.length - 3].scopes.push(...generateScopes('conduct inquisitions'));

    const apps = generateApps(
      1,
      generateScopes('teach defence against the dark arts'),
      ...grants,
    );
    writeAppsQuery(cloudId, client, apps);

    return (
      <StoryContext client={client}>
        <UserConnectedAppsDetailsPage {...pageDefaults} />
      </StoryContext>
    );
  })
  .add('Loading', () => (
    <UserConnectedAppsDetailsPageImpl {...pageDefaults} data={{ loading: true } as any} />
  ))
  .add('App Not Found', () => (
    createTestComponent({ loading: false, site: { userConnectedApps: [] } } as any)
  ));
