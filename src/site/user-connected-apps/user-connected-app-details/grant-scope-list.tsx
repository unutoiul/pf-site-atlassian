import * as React from 'react';

import { AppScope } from '../../../schema/schema-types';
import { compareScopes } from './compare-scopes';

export interface DescriptionAndScopeContainerProps {
  standardScopes: AppScope[];
  extraScopes: AppScope[];
}

export class GrantScopeList extends React.Component<DescriptionAndScopeContainerProps> {

  public render() {
    return (
      <ul>
        {this.props.standardScopes.sort(compareScopes).map(scope => (
          <li key={scope.id}><p>{scope.untranslatedName}</p></li>
        ))}
        {
          this.props.extraScopes.sort(compareScopes).map(scope => (
            <li key={scope.id}>
              <p>{scope.untranslatedName}</p>
              <p>{scope.untranslatedDescription}</p>
            </li>
          ))}
      </ul>
    );
  }
}
