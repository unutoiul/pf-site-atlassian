import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';

import { generateScopes } from '../user-connected-apps-mock-data-utils';
import { DescriptionAndScopeContainerProps, GrantScopeList } from './grant-scope-list';

describe('User-connected grant scope list', () => {
  function createTestComponent(props: Partial<DescriptionAndScopeContainerProps>) {
    const defaultProps: DescriptionAndScopeContainerProps = {
      standardScopes: [],
      extraScopes: [],
    };

    return mount(<GrantScopeList {...defaultProps} {...props} />);
  }

  const genericStandardScopes = generateScopes('Brew peppermint tea', 'Brew english breakfast');
  const genericExtraScopes = generateScopes('Alter thermostat temperature');

  it('Creates the correct (sorted) list of scopes (omitting descriptions on standard scopes)', () => {
    const componentUnderTest = createTestComponent({ standardScopes: genericStandardScopes, extraScopes: genericExtraScopes });
    const listItems = componentUnderTest.first().find('li');
    expect(listItems).to.have.lengthOf(3);

    expect(listItems.at(0).text()).to.contain('Brew english breakfast');
    expect(listItems.at(0).text()).not.to.contain('Lets you brew english breakfast');

    expect(listItems.at(1).text()).to.contain('Brew peppermint tea');
    expect(listItems.at(1).text()).not.to.contain('Lets you brew peppermint tea');

    expect(listItems.at(2).text()).to.contain('Alter thermostat temperature');
    expect(listItems.at(2).text()).to.contain('Lets you alter thermostat temperature');
  });
});
