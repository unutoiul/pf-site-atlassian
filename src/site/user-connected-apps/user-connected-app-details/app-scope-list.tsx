import * as React from 'react';
import styled from 'styled-components';

import { AppScope } from '../../../schema/schema-types';
import { compareScopes } from './compare-scopes';

export interface DescriptionAndScopeContainerProps {
  scopes: AppScope[];
}

const ScopeListItemHeader = styled.li`
  font-weight: 600;
`;

const ScopeListItemDescription = styled.p`
  font-weight: 400;
`;

export class AppScopeList extends React.Component<DescriptionAndScopeContainerProps> {

  public render() {

    return (
      <ul>
        {
          this.props.scopes.sort(compareScopes).map(scope => (
            <ScopeListItemHeader key={scope.id}>{scope.untranslatedName}
              <ScopeListItemDescription>
                {scope.untranslatedDescription}
              </ScopeListItemDescription>
            </ScopeListItemHeader>
          ))
        }
      </ul>
    );
  }
}
