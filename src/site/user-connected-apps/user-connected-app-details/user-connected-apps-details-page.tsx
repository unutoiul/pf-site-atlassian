import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import { defineMessages, FormattedMessage } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';

import AkSpinner from '@atlaskit/spinner';

import {
  ScreenEventSender,
  userConnectedAppDetailsScreenEvent,
} from 'common/analytics';
import { GenericError } from 'common/error';
import { PageLayout } from 'common/page-layout';

import { UserConnectedApp, UserConnectedAppsQuery } from '../../../schema/schema-types';
import {
  UserConnectedAppsPageFeatureFlag,
  withUserConnectedAppsPageFeatureFlag,
} from '../../feature-flags/with-user-connected-apps-page-feature-flag';
import { SiteRouteProps } from '../../routes/site-routes';
import userConnectedAppsDataQuery from '../user-connected-apps.query.graphql';
import { LogoWithAppName } from './logo-with-app-name';
import { ScopeContainer } from './scope-container';
import { UserDetailsBreadcrumb } from './user-details-breadcrumb';
import { UserGrantList } from './user-grant-list';

export const messages = defineMessages({
  appNotFound: {
    id: 'user-management.site-settings.user-connected-apps.app.not.found',
    description: 'Error message in the user-connected app details page when the request app is not found.',
    defaultMessage: 'App not found',
  },
});

interface SiteParams {
  cloudId: string;
  appId: string;
}

type UserConnectedAppDetailsProps = RouteComponentProps<SiteParams> & UserConnectedAppsPageFeatureFlag;

type Props = ChildProps<UserConnectedAppDetailsProps, UserConnectedAppsQuery>;

export class UserConnectedAppsDetailsPageImpl extends React.Component<Props> {
  public render() {
    const {
      data,
      userConnectedAppsFeatureFlag,
      match: { params: { cloudId, appId } },
    } = this.props;

    if (!userConnectedAppsFeatureFlag.value) {
      return null;
    }

    if (!data || data.loading) {
      return <AkSpinner />;
    }

    if (data.error) {
      return <GenericError />;
    }

    const app: UserConnectedApp | undefined = data
      && data.site
      && (data.site.userConnectedApps || [])
      .find(({ id }) => id === appId);

    if (!app) {
      return <GenericError description={<FormattedMessage {...messages.appNotFound} />} />;
    }

    const { name: appName, vendorName: appVendor, id: oAuthClientId } = app;
    const userCount = app.userGrants.length;

    return (
      <ScreenEventSender event={userConnectedAppDetailsScreenEvent(cloudId, userCount, { appName, oAuthClientId, appVendor })}>
        <PageLayout
          isFullWidth={true}
          title={<LogoWithAppName url={app.avatarUrl} name={app.name} vendorName={app.vendorName} />}
          pageParent={<UserDetailsBreadcrumb cloudId={cloudId} app={app} />}
          description={<p>{app.description}</p>}
        >
          <ScopeContainer appName={app.name} scopes={app.scopes} />
          <UserGrantList app={app} cloudId={cloudId}/>
        </PageLayout>
      </ScreenEventSender>
    );
  }
}

export const withUserConnectedAppsQuery = graphql<RouteComponentProps<SiteRouteProps>, UserConnectedAppsQuery>(
  userConnectedAppsDataQuery,
  {
    options:  ({ match: { params: { cloudId } } }) => ({
      variables:  {
        id: cloudId,
      },
    }),
  },
);

export const UserConnectedAppsDetailsPage =
  withRouter<{}>(withUserConnectedAppsQuery(withUserConnectedAppsPageFeatureFlag(UserConnectedAppsDetailsPageImpl)));
