import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import { gridSize as akGridSize } from '@atlaskit/theme';

import {
  AnalyticsClientProps,
  userConnectedAppBackToAllAppsLinkClickEvent,
  withAnalyticsClient,
} from 'common/analytics';
import { PageParentLink } from 'common/page-parent-link/page-parent-link';

import { UserConnectedApp } from '../../../schema/schema-types';
import { util } from '../../../utilities/admin-hub';

const Container = styled.div`
  padding: ${akGridSize() * 2}px 0;
`;

const messages = defineMessages({
  userConnectedPageTitle: {
    id: 'user-management.site-settings.user-connected-apps.breadcrumbs.title',
    description: 'User-connected apps page title',
    defaultMessage: 'Connected apps',
  },
});

interface UserDetailsBreadcrumbProps {
  cloudId: string;
  app: UserConnectedApp;
}

export type Props = UserDetailsBreadcrumbProps & AnalyticsClientProps & InjectedIntlProps;

export class UserDetailsBreadcrumbImpl extends React.Component<Props> {
  public render() {
    return (
      <Container>
        <PageParentLink
          linkText={this.props.intl.formatMessage(messages.userConnectedPageTitle)}
          linkLocation={`${util.siteAdminBasePath}/s/${this.props.cloudId}/user-connected-apps`}
          onClick={this.sendBackToAppsLinkClickEvent}
        />
      </Container>
    );
  }

  private sendBackToAppsLinkClickEvent = () => {
    const {
      cloudId,
      app: { name: appName, vendorName: appVendor, id: oAuthClientId },
    } = this.props;
    this.props.analyticsClient.sendUIEvent(userConnectedAppBackToAllAppsLinkClickEvent(cloudId, {
      oAuthClientId,
      appName,
      appVendor,
    }));
  };
}

export const UserDetailsBreadcrumb = injectIntl(withAnalyticsClient(UserDetailsBreadcrumbImpl));
