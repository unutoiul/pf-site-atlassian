import { expect } from 'chai';
import { mount, shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';
import { StaticRouter } from 'react-router';

import AkSpinner from '@atlaskit/spinner';

import { GenericError } from 'common/error';
import { PageLayout } from 'common/page-layout';

import { createApolloClient } from '../../../apollo-client';

import { LogoWithAppName } from './logo-with-app-name';
import { ScopeContainer } from './scope-container';
import { UserConnectedAppsDetailsPageImpl } from './user-connected-apps-details-page';
import { UserDetailsBreadcrumb } from './user-details-breadcrumb';
import { UserGrantList } from './user-grant-list';

describe('User Connected App Details page layout', () => {

  const cloudId = '1289-1245';
  const defaultAppId = '1234';
  const app = {
    __typename: 'UserConnectedApp',
    id: `${defaultAppId}`,
    name: 'Zapier',
    description: 'Zapier automatically moves information between Stride and the other apps you use during your day, so you can focus on your most important work.',
    vendorName: 'Zapier Corp',
    avatarUrl: 'https://avatar-cdn.stg.internal.atlassian.com/211ab15258c2a2d008c5d2b8e30910a0?by=hash',
    scopes: [
      {
        __typename: 'AppScope',
        id: 'view:jira-issue',
        untranslatedName: 'View jira issue data',
        untranslatedDescription: 'Read jira project and issue data, search for issues, and objects associated with issues like attachments and work logs.',
      },
      {
        __typename: 'AppScope',
        id: 'view:user',
        untranslatedName: 'View user profile settings',
        untranslatedDescription: 'View user information in Jira that the user has access to, including usernames, email addresses and avatars.',
      },
    ],
    userGrants: [],
  };
  const appDetailsWithScope = {
    site: {
      id: cloudId,
      __typename: 'Site',
      userConnectedApps: [
        app,
      ],
    },
  };

  function createTestComponent(data, appId = defaultAppId): ShallowWrapper {
    return shallow(
      <UserConnectedAppsDetailsPageImpl
        data={data}
        userConnectedAppsFeatureFlag={{ value: true } as any}
        match={{ params: { cloudId, appId } } as any}
        location={{} as any}
        history={{} as any}
      />,
    );
  }

  it('should render the page with title, breadcrumbs and scopes', () => {
    const wrapper = createTestComponent(appDetailsWithScope);
    const pageLayout = wrapper.find(PageLayout);

    expect(pageLayout.length).to.equal(1);

    const titleNode = mount(
      <IntlProvider locale="en">
        {pageLayout.props().title}
      </IntlProvider> as any);

    const titleProps = titleNode.find(LogoWithAppName).props();
    expect(titleProps.name).to.equal(app.name);
    expect(titleProps.url).to.equal(app.avatarUrl);
    expect(titleProps.vendorName).to.equal(app.vendorName);

    const apolloClient = createApolloClient();

    const breadcrumbsNode = mount(
      <ApolloProvider client={apolloClient}>
        <IntlProvider locale="en">
          <StaticRouter context={{}}>
            {pageLayout.props().pageParent}
          </StaticRouter>
        </IntlProvider>
      </ApolloProvider> as any);

    const breadcrumbsNodeProps = breadcrumbsNode.find(UserDetailsBreadcrumb).props();

    expect(breadcrumbsNodeProps.cloudId).to.equal(cloudId);
    expect(breadcrumbsNodeProps.app.name).to.equal(app.name);

    expect(pageLayout.props().description).to.not.equal(undefined);

    const scopeContainer = wrapper.find(ScopeContainer);

    expect(scopeContainer.length).to.equal(1);
    expect(scopeContainer.prop('appName')).to.equal(app.name);
    expect(scopeContainer.prop('scopes')).to.equal(app.scopes);

    const grantListContainer = wrapper.find(UserGrantList);

    expect(grantListContainer.prop('cloudId')).to.equal(cloudId);
    expect(grantListContainer.prop('app')).to.deep.equal(app);
  });

  it('should show loading state when data is loading', () => {
    const wrapper = createTestComponent({ loading: true });
    const spinner = wrapper.find(AkSpinner);

    expect(spinner.length).to.equal(1);
  });

  it('should render generic error when app not found', () => {
    const wrapper = createTestComponent(appDetailsWithScope, 'random-app-id');
    const spinner = wrapper.find(GenericError);

    expect(spinner.length).to.equal(1);
  });
});
