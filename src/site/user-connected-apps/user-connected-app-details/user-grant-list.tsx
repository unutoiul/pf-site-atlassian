import * as React from 'react';

import {
  AnalyticsClientProps,
  userConnectedAppDetailsHideGrantDetailsLinkClickEvent as hideEvent,
  userConnectedAppDetailsShowGrantDetailsLinkClickEvent as showEvent,
  withAnalyticsClient,
} from 'common/analytics';

import { UserConnectedApp, UserGrant } from '../../../schema/schema-types';

import { UserGrantEntry, UserGrantListView } from './user-grant-list-view';

interface UserGrantListProps {
  app: UserConnectedApp;
  cloudId: string;
}

export type Props = UserGrantListProps & AnalyticsClientProps;

interface State {
  expandedRows: Record<string, boolean>;
  pageNumber: number;
}

export class UserGrantListImpl extends React.Component<Props, State> {
  public readonly state: Readonly<State> = {
    expandedRows: {},
    pageNumber: 1,
  };

  public render() {
    const { app } = this.props;

    const entries: UserGrantEntry[] = [];

    for (const grant of app.userGrants) {
      entries.push(this.buildEntry(grant));
    }

    return (
      <UserGrantListView
        entries={entries}
        cloudId={this.props.cloudId}
        pageNumber={this.state.pageNumber}
        onSetPage={this.setPage}
        onUpdateEntryExpanded={this.updateEntryExpanded}
      />
    );
  }

  public buildEntry = (grant: UserGrant): UserGrantEntry => {
    const appScopes = this.props.app.scopes;
    const standardScopes = grant.scopes.filter(
      (grantScope) => appScopes.some((appScope) => appScope.id === grantScope.id),
    );

    const extraScopes = grant.scopes.filter(
      (grantScope) => !appScopes.some((appScope) => appScope.id === grantScope.id),
    );

    return {
      grantId: grant.id,
      accountId: grant.accountId,
      createdAt: grant.createdAt,
      expanded: !!this.state.expandedRows[grant.id],
      standardScopes,
      extraScopes,
    };
  }

  private updateEntryExpanded = ({ grantId, createdAt: grantDate, accountId, extraScopes }: UserGrantEntry, expanded: boolean) => {
    const {
      cloudId,
      app: { name: appName, vendorName: appVendor, id: oAuthClientId },
    } = this.props;

    const isGrantScopesStandard = extraScopes.length > 0;
    const eventFunction = expanded ? showEvent : hideEvent;

    this.props.analyticsClient.sendUIEvent(eventFunction(
      cloudId,
      { oAuthClientId, appName, appVendor },
      { grantDate, accountId, isGrantScopesStandard },
    ));

    this.setState(
      prevState => ({
        expandedRows: { ...prevState.expandedRows, [grantId]: expanded },
      }),
    );
  }

  private setPage = (pageNumber) => this.setState({ pageNumber });
}

export const UserGrantList = withAnalyticsClient(UserGrantListImpl);
