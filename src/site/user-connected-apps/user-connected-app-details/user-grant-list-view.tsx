import * as React from 'react';
import { defineMessages, FormattedDate, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import { DynamicTableStateless as AkDynamicTable, Row as AkRow } from '@atlaskit/dynamic-table';
import { gridSize as akGridSize } from '@atlaskit/theme';

import { ContentReveal } from 'common/content-reveal';
import { EmptyTableView } from 'common/table';

import { ConnectedAccount } from 'common/account/connected-account';

import { AppScope } from '../../../schema/schema-types';
import { GrantScopeList } from './grant-scope-list';

export const DEFAULT_ROWS_PER_PAGE = 20 * 2;

const margin = akGridSize() * 3;
const Label = styled.legend`
  font-weight: 600;
  margin: ${margin}px 0 ${margin}px 0;
`;

export const ExpanderContainer = styled.div`
  display: flex;
  justify-content: flex-end;
`;

const messages = defineMessages({
  accessAuthorization: {
    id: 'user-management.site-settings.user-connected-app-details.grants.accessAuthorization',
    description: 'User authorizations heading',
    defaultMessage: 'Authorizations',
  },
});

const tableMessages = defineMessages({
  user: {
    id: 'user-management.site-settings.user-connected-app-details.grants.user',
    description: 'User column title',
    defaultMessage: 'User',
  },
  connected: {
    id: 'user-management.site-settings.user-connected-app-details.grants.connected',
    description: 'Connection date column title',
    defaultMessage: 'Authorized',
  },
  expandLink: {
    id: 'user-management.site-settings.user-connected-app-details.grants.expandLink',
    description: 'Link to expand row to show authorization details',
    defaultMessage: 'Details',
  },
  emptyTableMessage: {
    id: 'user-management.site-settings.user-connected-app-details.grants.emptyTableMessage',
    description: 'Error message when the table contents could not be shown',
    defaultMessage: 'Something went wrong while fetching grants. Try again later.',
  },
  grantIncludedScopesHeading: {
    id: 'user-management.site-settings.user-connected-app-details.grantIncludedScopesHeading',
    description: 'Lead-in text for list of scopes user has authorized',
    defaultMessage: 'Authorized {appName} to have access to:',
  },
  grantExcludedScopesHeading: {
    id: 'user-management.site-settings.user-connected-app-details.grantExcludedScopesHeading',
    description: 'Lead-in text for list of scopes user has not authorized',
    defaultMessage: 'Hasn\'t authorized {appName} to have access to:',
  },
});

export interface UserGrantEntry {
  grantId: string;
  accountId: string;
  createdAt: string;
  expanded: boolean;
  standardScopes: AppScope[];
  extraScopes: AppScope[];
}

export interface UserGrantListViewProps {
  entries: UserGrantEntry[];
  cloudId: string;
  pageNumber: number;
  onSetPage(pageNumber: number);
  onUpdateEntryExpanded(entry: UserGrantEntry, expanded: boolean);
}

export type Props = UserGrantListViewProps & InjectedIntlProps;

export class UserGrantListViewImpl extends React.Component<Props> {
  public render() {
    const tableRows: AkRow[] = [];

    for (const entry of this.props.entries) {
      tableRows.push(this.buildUserRow(entry));
      tableRows.push(this.buildExpandingScopesRow(entry));
    }

    return (
      <React.Fragment>
        <Label>
          {this.props.intl.formatMessage(messages.accessAuthorization)}
        </Label>
        <AkDynamicTable
          head={this.tableHeaders()}
          emptyView={
            <EmptyTableView>
              {this.props.intl.formatMessage(tableMessages.emptyTableMessage)}
            </EmptyTableView>
          }
          page={this.props.pageNumber}
          rowsPerPage={DEFAULT_ROWS_PER_PAGE}
          rows={tableRows}
          onSetPage={this.props.onSetPage}
        />
      </React.Fragment>
    );
  }

  private rowAppearance = (isExpanded: boolean) => isExpanded ? {} : {
    'aria-hidden': true,
    style: { display: 'none' },
  };

  private buildUserRow = (entry: UserGrantEntry): AkRow => {
    const onOpen = () => this.props.onUpdateEntryExpanded(entry, true);
    const onClose = () => this.props.onUpdateEntryExpanded(entry, false);

    return {
      key: entry.grantId,
      cells: [
        {
          key: 'user',
          content: <ConnectedAccount cloudId={this.props.cloudId} userId={entry.accountId} />,
        },
        {
          key: 'connected-at',
          content: <FormattedDate value={entry.createdAt} year="numeric" month="short" day="numeric" />,
        },
        {
          key: 'expand-link',
          content: (
              <ExpanderContainer>
                <ContentReveal
                  label={this.props.intl.formatMessage(tableMessages.expandLink)}
                  onOpen={onOpen}
                  onClose={onClose}
                />
              </ExpanderContainer>
            ),
        },
      ],
    };
  };

  private buildExpandingScopesRow = (entry: UserGrantEntry): AkRow => {
    const cellPadding = `${akGridSize() * 3}px`;

    return {
      key: `${entry.grantId}-scopes-row`,
      ...this.rowAppearance(entry.expanded),
      cells: [
        {
          key: `scopes-view`,
          style: { paddingLeft: cellPadding },
          content: (
            <GrantScopeList standardScopes={entry.standardScopes} extraScopes={entry.extraScopes} />
          ),
          colSpan: 3,
        },
      ],
    };
  };

  private tableHeaders = () => {
    const { formatMessage } = this.props.intl;

    return {
      cells: [
        {
          key: 'User',
          content: formatMessage(tableMessages.user),
          isSortable: false,
          shouldTruncate: false,
          width: 33,
        },
        {
          key: 'Connected',
          content: formatMessage(tableMessages.connected),
          isSortable: false,
          shouldTruncate: false,
          width: 33,
        },
      ],
    };
  };

}

export const UserGrantListView = injectIntl(UserGrantListViewImpl);
