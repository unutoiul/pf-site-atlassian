import { AppScope } from '../../../schema/schema-types';

export const compareScopes = (a: AppScope, b: AppScope) => {
  return a.untranslatedName.toLocaleLowerCase().localeCompare(b.untranslatedName.toLocaleLowerCase());
};
