import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';

import { generateScopes } from '../user-connected-apps-mock-data-utils';
import { AppScopeList, DescriptionAndScopeContainerProps } from './app-scope-list';

describe('User-connected app scope list', () => {
  function createTestComponent(props: Partial<DescriptionAndScopeContainerProps>) {
    const defaultProps: DescriptionAndScopeContainerProps = {
      scopes: [],
    };

    return mount(<AppScopeList {...defaultProps} {...props} />);
  }

  const genericScopes = generateScopes('Brew peppermint tea', 'Brew english breakfast', 'Alter thermostat temperature');

  it('Creates the correct (sorted) list of items', () => {
    const componentUnderTest = createTestComponent({ scopes: genericScopes });
    const listItems = componentUnderTest.first().find('li');
    expect(listItems).to.have.lengthOf(3);

    expect(listItems.at(0).text()).to.contain('Alter thermostat temperature');
    expect(listItems.at(0).text()).to.contain('Lets you alter thermostat temperature');

    expect(listItems.at(1).text()).to.contain('Brew english breakfast');
    expect(listItems.at(1).text()).to.contain('Lets you brew english breakfast');

    expect(listItems.at(2).text()).to.contain('Brew peppermint tea');
    expect(listItems.at(2).text()).to.contain('Lets you brew peppermint tea');
  });
});
