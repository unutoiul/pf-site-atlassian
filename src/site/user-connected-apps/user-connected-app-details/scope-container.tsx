import * as React from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';
import styled from 'styled-components';

import { gridSize as akGridSize } from '@atlaskit/theme';

import { AppScope } from '../../../schema/schema-types';
import { AppScopeList } from './app-scope-list';

interface DescriptionAndScopeContainerProps {
  scopes: AppScope[];
  appName: string;
}

const messages = defineMessages({
  scopeHeader: {
    id: 'user-management.site-settings.user-connected-app-details.scopeHeader',
    description: 'App scope details heading',
    defaultMessage: 'Permissions',
  },
  appScopeStatement: {
    id: 'user-management.site-settings.user-connected-app-details.appScopeStatement',
    description: 'Lead-in text for listing of app scopes',
    defaultMessage: '{appName} can perform the following actions on behalf of the user:',
  },
});

const Label = styled.legend`
  font-weight: 600;
  margin-bottom: ${akGridSize}px;
`;

export class ScopeContainer extends React.Component<DescriptionAndScopeContainerProps> {

  public render() {
    const appName = this.props.appName;

    return (
      <React.Fragment>
        <Label><FormattedMessage {...messages.scopeHeader} /></Label>
        <FormattedMessage {...messages.appScopeStatement} values={{ appName }}/>
        <AppScopeList scopes={this.props.scopes} />
      </React.Fragment>
    );
  }
}
