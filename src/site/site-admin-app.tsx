import * as React from 'react';
import { compose } from 'react-apollo';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import { connect, DispatchProp } from 'react-redux';
import { Redirect, Route, RouteComponentProps, RouteProps, Switch } from 'react-router-dom';
import styled from 'styled-components';

import { createErrorIcon } from 'common/error';
import { EyeballAnalytics } from 'common/eyeball';
import { FeedbackDialog } from 'common/feedback';
import { FlagProps, withFlag } from 'common/flag';
import { tablet } from 'common/responsive/breakpoints';
import { Page } from 'common/responsive/Page';

import {
  closeDrawer,
  hideDrawerLink,
  NavigationAction,
  openDrawer,
  removeNavigationSection,
  showDrawerLink,
  updateNavigationSection,
} from 'common/navigation';

import { withOrganizationRedirect } from '../organization/organizations/organization-redirect';
import { RootState } from '../store';
import { getRelativeURI } from '../utilities/uri';
import { CsatSurvey } from './csat';
import { IframePage } from './iframe-page/iframe-page';
import { Navigation, NavigationDrawers } from './navigation/navigation';
import { navMounters, routes } from './navigation/routing';
import { PageMigrationRedirect } from './page-redirect/page-redirect';
import { TrustedUserCommunicationModal } from './trusted-user-communication';
import { XFlowDialog } from './xflow/xflow-dialog';

const messages = defineMessages({
  iframeErrorTitle: {
    id: 'chrome.app.iframe-error-title',
    defaultMessage: `This action isn't available in our new experience`,
    description: 'Title for an error that gets displayed when opening a link fails',
  },
  iframeErrorBody: {
    id: 'chrome.app.iframe-error-body',
    defaultMessage: `Switch back to the previous experience to complete this action. Don't worry, you won't lose any functionality.`,
    description: 'Body for an error that gets displayed when opening a link fails',
  },
  linkToOldSiteAdmin: {
    id: 'chrome.app.iframe-error-link',
    defaultMessage: 'Switch back',
    description: 'Link text for opening old Site Admin',
  },
});

/*
 * This div needs absolute positioning because it needs to be set within its parent div, which has relative positioning.
 * https://css-tricks.com/absolute-positioning-inside-relative-positioning/
 * Fixes a safari bug where the height must be specified in the parent in order to use "height: 100%" in the child.
 * Fixes an IE11 bug where the height must be 100vh in the child, rather than 100%. An explicit height is required here. (Caused ADMIN-309)
*/
const IframeContainer = styled.div`
  left: 0;
  top: 0;
  width: 100vw;
  height: 100vh;

  @media (min-width: ${tablet}px) {
    width: 100%;
  }

  position: absolute;
  display: block;
  overflow-y: hidden;
`;

interface IframeVisibilityControllerProps {
  onVisible(): void;

  onInvisible(): void;
}

class IframeVisibilityController extends React.Component<IframeVisibilityControllerProps> {
  public componentDidMount() {
    this.props.onVisible();
  }

  public componentWillUnmount() {
    this.props.onInvisible();
  }

  public render() {
    return null;
  }
}

interface AppProps extends DispatchProp<any>, RouteComponentProps<any> {
  drawerOpen: boolean;
}

interface AppState {
  iframeVisible: boolean;
  switchRoutes?: any;
  globalRoutes?: any;
}

// tslint:disable-next-line:max-classes-per-file
export class SiteAdminAppImpl extends React.Component<AppProps & FlagProps & InjectedIntlProps, AppState> {
  public state: AppState = {
    iframeVisible: false,
  };

  public componentDidMount() {
    let isWindowUnloading = false;

    window.onbeforeunload = () => {
      isWindowUnloading = true;
    };

    window.addEventListener('popstate', (e: PopStateEvent) => {
      if (e.state && e.state.type === NavigationAction.CHROME_DRAWER_OPEN) {
        this.props.dispatch(openDrawer(e.state.drawer, false));
      } else if (this.props.drawerOpen) {
        this.props.dispatch(closeDrawer(false));
      }
    });

    window.addEventListener('message', (message) => {
      if (message.origin !== window.location.origin) {
        return;
      }

      if (message.data.type !== 'unloading') {
        return;
      }

      if (isWindowUnloading) {
        return;
      }

      const reloadUrl = getRelativeURI(window.location).addSearch('chrome', 'false').addSearch('react', 'false').href();
      const retryUrl = getRelativeURI(window.location).addSearch('react', 'false').href();

      this.props.showFlag({
        id: 'IFRAME_UNLOAD',
        title: this.props.intl.formatMessage(messages.iframeErrorTitle),
        description: this.props.intl.formatMessage(messages.iframeErrorBody),
        icon: createErrorIcon(),
        actions: [
          {
            content: (
              <a href={retryUrl}>
                <FormattedMessage {...messages.linkToOldSiteAdmin} />
              </a>
            ),
          },
        ],
      });

      const iframe = document.querySelector('iframe');
      if (iframe) {
        iframe.src = reloadUrl;
      } else {
        throw new Error('Critical: iframe was not found, investigate immediately!');
      }
    }, false);
  }

  public getMounterProps() {
    const { dispatch } = this.props;

    return {
      updateNavigationSection: (section) => dispatch(updateNavigationSection(section)),
      removeNavigationSection: (sectionId) => dispatch(removeNavigationSection(sectionId)),
      showDrawerLink: (drawer) => dispatch(showDrawerLink(drawer)),
      hideDrawerLink: (drawer) => dispatch(hideDrawerLink(drawer)),
      registerRoutes: this.registerRoutes,
      unregisterRoutes: this.unregisterRoutes,
    };
  }

  public registerRoutes = (data) => {
    const switchRoutes = { ...this.state.switchRoutes };
    (data.switchRoutes || []).forEach(r => switchRoutes[r.path] = r);
    const globalRoutes = { ...this.state.globalRoutes };
    (data.globalRoutes || []).forEach(r => globalRoutes[r.path] = r);
    this.setState({
      switchRoutes,
      globalRoutes,
    });
  };

  public unregisterRoutes = (data) => {
    const switchRoutes = { ...this.state.switchRoutes };
    (data.switchRoutes || []).forEach(r => delete switchRoutes[r.path]);
    const globalRoutes = { ...this.state.globalRoutes };
    (data.globalRoutes || []).forEach(r => delete globalRoutes[r.path]);
    this.setState({
      switchRoutes,
      globalRoutes,
    });
  };

  public showIframe = () => {
    this.setState({ iframeVisible: true });
  };

  public hideIframe = () => {
    this.setState({ iframeVisible: false });
  };

  public createIframeVisibilityController = () => {
    return (
      <IframeVisibilityController
        onVisible={this.showIframe}
        onInvisible={this.hideIframe}
      />
    );
  };

  public render() {
    const { location, history, match } = this.props;

    const mounterProps = this.getMounterProps();
    const mounters = navMounters.map(m => <m.mounter key={m.id} {...mounterProps} />);

    const switchRoutes = [
      ...routes,
      ...Object.values<RouteProps>(this.state.switchRoutes || {}),
    ].map(r => <Route key={r.path} {...r} />);
    const globalRoutes = [
      ...Object.values<RouteProps>(this.state.globalRoutes || {}),
    ].map(r => <Route key={r.path} {...r} />);

    const style: React.CSSProperties = this.props.drawerOpen ? { height: '100vh', overflow: 'hidden' } : {};

    return (
      <div style={style}>
        <Page
          navigation={(
            <Navigation location={this.props.location} />
          )}
          drawers={<NavigationDrawers />}
        >
          <FeedbackDialog />
          <CsatSurvey />
          <XFlowDialog />
          <PageMigrationRedirect />
          <TrustedUserCommunicationModal />

          {this.props.children}

          <EyeballAnalytics />
          {mounters}

          {globalRoutes}
          <IframeContainer>
            <IframePage
              history={history}
              match={match}
              location={location}
              isVisible={this.state.iframeVisible}
            />
          </IframeContainer>
          <Switch>
            <Redirect exact={true} from="/admin" to={{ pathname: '/admin/users' }} />
            {switchRoutes}
            <Route render={this.createIframeVisibilityController} />
          </Switch>
        </Page>
      </div>
    );
  }
}

const mapStateToProps = (state: RootState) => ({
  drawerOpen: state.chrome.navigationDrawers.openDrawer !== undefined,
});

export const SiteAdminApp = compose(
  withOrganizationRedirect,
  withFlag,
  connect(mapStateToProps),
  injectIntl,
)(SiteAdminAppImpl);
