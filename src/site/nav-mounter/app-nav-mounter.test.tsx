import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { spy } from 'sinon';

import { Application, NavItem } from 'common/navigation';

import { createMockIntlContext } from '../../utilities/testing';
import { AppNavMounter } from './app-nav-mounter';

const app1: Application = {
  id: 'app1',
  name: 'App 1',
  url: 'app1-url',
  adminUrl: 'app1-admin-url',
  product: 'product-1',
};

const app2: Application = {
  id: 'app2',
  name: 'App 2',
  url: 'app2-url',
  adminUrl: 'app2-admin-url',
  product: 'product-2',
};

const stride: Application = {
  id: 'stride',
  name: 'Stride',
  url: 'stride-url',
  adminUrl: 'stride-admin-url',
  product: 'hipchat.cloud',
};

describe('App Nav Mounter Tests', () => {
  interface RelevantProps {
    data: {
      loading: boolean,
      error?: any,
      currentSite?: {
        applications: Application[],
        id?: string,
      },
    };
    updateNavigationSection(section: NavItem): void;
  }

  function createWrapper(props: RelevantProps) {
    const AppNavMounterWithoutApollo = (AppNavMounter as any).WrappedComponent;

    return shallow<RelevantProps>(
      <AppNavMounterWithoutApollo {...props} />,
      createMockIntlContext(),
    );
  }

  it('should update navigation sections if applications data is available', () => {
    const updateSpy = spy();
    createWrapper({
      data: { loading: false, currentSite: { applications: [app1, app2] } },
      updateNavigationSection: updateSpy,
    });

    expect(updateSpy.callCount).to.equal(1);
  });

  it('should override certain properties for Stride application links if the FF is on', () => {
    const updateSpy = spy();
    createWrapper({
      data: { loading: false, currentSite: { applications: [app1, app2, stride], id: '123' } },
      updateNavigationSection: updateSpy,
    });

    expect(updateSpy.callCount).to.equal(1);
    const args = updateSpy.getCall(0).args;
    const strideLink = args[0].links.find(l => l.title.defaultMessage === 'Stride');
    expect(strideLink.path).to.equal('/admin/s/123/stride');
    expect(strideLink.isExternal).to.equal(false);
  });

  it('should not update navigation sections if data is loading', () => {
    const updateSpy = spy();
    createWrapper({
      data: { loading: true },
      updateNavigationSection: updateSpy,
    });

    expect(updateSpy.callCount).to.equal(0);
  });

  it('should not update navigation sections if there is error loading data', () => {
    const updateSpy = spy();
    createWrapper({
      data: { loading: false, error: { messages:  'Could not fetch applications' } },
      updateNavigationSection: updateSpy,
    });

    expect(updateSpy.callCount).to.equal(0);
  });

  it('should update navigation sections once the data loads', () => {
    const updateSpy = spy();
    const wrapper = createWrapper({
      data: { loading: true },
      updateNavigationSection: updateSpy,
    });

    const newUpdateSpy = spy();
    wrapper.setProps({
      data: { loading: false, currentSite: { applications: [app1, app2] } },
      updateNavigationSection: newUpdateSpy,
    });

    expect(updateSpy.callCount).to.equal(0);
    expect(newUpdateSpy.callCount).to.equal(1);
  });

  it('should not update navigation sections if data hasn\'t been updated', () => {
    const updateSpy = spy();
    const wrapper = createWrapper({
      data: { loading: false, currentSite: { applications: [app1, app2] } },
      updateNavigationSection: updateSpy,
    });

    const newUpdateSpy = spy();
    wrapper.setProps({
      data: { loading: false, currentSite: { applications: [app1, app2] } },
      updateNavigationSection: newUpdateSpy,
    });

    expect(updateSpy.callCount).to.equal(1);
    expect(newUpdateSpy.callCount).to.equal(0);
  });

  it('should update navigation sections if data has been updated', () => {
    const updateSpy = spy();
    const wrapper = createWrapper({
      data: { loading: false, currentSite: { applications: [app1, app2] } },
      updateNavigationSection: updateSpy,
    });

    const newUpdateSpy = spy();
    wrapper.setProps({
      data: { loading: false, currentSite: { applications: [app1] } },
      updateNavigationSection: newUpdateSpy,
    });

    expect(updateSpy.callCount).to.equal(1);
    expect(newUpdateSpy.callCount).to.equal(1);
  });
});
