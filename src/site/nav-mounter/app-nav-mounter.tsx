import * as isEqual from 'lodash.isequal';
import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';

import { Application, NavigationLinkIconGlyph, NavigationMounterProps, NavigationSectionId, NavItem } from 'common/navigation';

import { CurrentSiteApplicationsQuery } from '../../schema/schema-types';
import currentSiteApplicationsQuery from './app-nav-mounter.query.graphql';

declare interface OwnProps {
  data: {
    loading: boolean,
    error?: any,
  };
}

declare type AppNavMounterProps = ChildProps<NavigationMounterProps & OwnProps, CurrentSiteApplicationsQuery>;

function getAppIcon(hostType: string): NavigationLinkIconGlyph {
  switch (hostType) {
    case 'jira': return NavigationLinkIconGlyph.Jira;
    case 'confluence': return NavigationLinkIconGlyph.Confluence;
    case 'hipchat.cloud': return NavigationLinkIconGlyph.Stride;
    default: return NavigationLinkIconGlyph.Settings;
  }
}

function toNavItem(app: Application, cloudId: string): NavItem {
  const navItem = {
    analyticsSubjectId: `link-${app.product}`,
    path: app.adminUrl,
    title: { id: app.id, defaultMessage: app.name } ,
    isExternal: true,
    icon: getAppIcon(app.product),
    analyticsMeta: { attributes: { app: app.product } },
  };
  if (app.product === 'hipchat.cloud') {
    return {
      ...navItem,
      path: `/admin/s/${cloudId}/stride`,
      isExternal: false,
    };

  }

  return navItem;
}

function updateNavIfNeeded(nextProps: Readonly<AppNavMounterProps>, currentApplications: Application[]) {
  const {
      updateNavigationSection,
      data: {
        loading,
        error,
        currentSite: {
          applications: nextApplications = [],
          id: currentSiteId = '',
        } = {},
      },
    } = nextProps;

  if (loading || error) {
    return;
  }

  if (isEqual(currentApplications, nextApplications)) {
    return;
  }

  updateNavigationSection({
    id: NavigationSectionId.Applications,
    links: (nextApplications || []).map<NavItem>(app => toNavItem(app, currentSiteId)),
  });
}

class AppNavMounterImpl extends React.Component<AppNavMounterProps> {
  public componentWillMount() {
    updateNavIfNeeded(this.props, []);
  }

  public componentWillReceiveProps(nextProps: Readonly<AppNavMounterProps>, _) {
    const {
      data: {
        currentSite: {
          applications = [],
        } = {},
      },
    } = this.props;

    updateNavIfNeeded(nextProps, applications || []);
  }

  public render() {
    return null;
  }
}

export const AppNavMounter = graphql<AppNavMounterProps, CurrentSiteApplicationsQuery>(currentSiteApplicationsQuery)(AppNavMounterImpl);
