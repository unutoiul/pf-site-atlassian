import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import { defineMessages } from 'react-intl';

import { NavigationMounterProps, NavigationSectionId } from 'common/navigation';

import { JsdSettingsQuery } from '../../schema/schema-types';
import { OLD_JSD_ROUTE } from '../page-redirect/page-redirect';
import jsdSettingsQuery from './jsd-settings.query.graphql';

export declare type JsdNavMounterProps = ChildProps<NavigationMounterProps, JsdSettingsQuery>;

const messages = defineMessages({
  portalOnlyCustomers: {
    id: 'chrome.app-nav.jsd.portal-only',
    defaultMessage: 'Portal only customers',
  },
});

export class JsdNavMounterImpl extends React.Component<JsdNavMounterProps> {
  private updated = false;

  public componentWillMount() {
    this.updateNavIfNeeded(this.props);
  }

  public componentWillReceiveProps(nextProps: Readonly<JsdNavMounterProps>, _) {
    this.updateNavIfNeeded(nextProps);
  }

  public shouldComponentUpdate() {
    return !this.updated;
  }

  public render() {
    return null;
  }

  private updateNavIfNeeded(nextProps: Readonly<JsdNavMounterProps>) {
    if (this.updated) {
      return;
    }

    const {
      data: {
        currentSite: {
          id: cloudId = '',
          settings: {
            serviceDesk: { enabled: newValue = false } = {},
          } = {},
          flags: {
            jsdADG3Migration = false,
          } = {},
        } = {},
      } = {},
    } = nextProps;

    if (!newValue || !cloudId || !nextProps.data || nextProps.data.loading || nextProps.data.error) {
      return;
    }

    nextProps.updateNavigationSection({
      id: NavigationSectionId.JiraServiceDesk,
      links: [
        {
          analyticsSubjectId: 'portal-only-customers',
          path: jsdADG3Migration ? `/admin/s/${cloudId}/jira-service-desk/portal-only-customers` : OLD_JSD_ROUTE,
          title: messages.portalOnlyCustomers,
        },
      ],
    });
    this.updated = true;
  }
}

export const JsdNavMounter = graphql<NavigationMounterProps, JsdSettingsQuery>(jsdSettingsQuery)(JsdNavMounterImpl);
