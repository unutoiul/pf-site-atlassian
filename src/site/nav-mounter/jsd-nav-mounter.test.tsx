import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { spy } from 'sinon';

import { NavItem } from 'common/navigation';

import { createMockIntlContext } from '../../utilities/testing';
import { JsdNavMounterImpl } from './jsd-nav-mounter';

describe('JSD Nav Mounter Tests', () => {
  interface RelevantProps {
    data: {
      loading: boolean,
      error?: any,
      currentSite?: {
        id?: string;
        settings?: {
          serviceDesk?: {
            enabled: boolean,
          },
        },
        flags?: {
          jsdADG3Migration?: boolean,
        }
      },
    };
    updateNavigationSection(section: NavItem): void;
  }

  function createWrapper(props: RelevantProps) {
    return shallow<RelevantProps>(
      <JsdNavMounterImpl {...props as any} />,
      createMockIntlContext(),
    );
  }

  it('should register routes if the data has already loaded and JSD is enabled', () => {
    const updateSpy = spy();
    createWrapper({
      data: { loading: false, currentSite: { id: 'dummy-id', settings: { serviceDesk: { enabled: true } }, flags: { jsdADG3Migration: true } } },
      updateNavigationSection: updateSpy,
    });

    expect(updateSpy.callCount).to.equal(1);
  });

  it('should not register routes if the data has already loaded and JSD is not enabled', () => {
    const updateSpy = spy();
    createWrapper({
      data: { loading: false, currentSite: { id: 'dummy-id', settings: { serviceDesk: { enabled: false } }, flags: { jsdADG3Migration: true } } },
      updateNavigationSection: updateSpy,
    });

    expect(updateSpy.callCount).to.equal(0);
  });

  it('should not register anything while data is loading', () => {
    const updateSpy = spy();
    createWrapper({
      data: { loading: true },
      updateNavigationSection: updateSpy,
    });

    expect(updateSpy.callCount).to.equal(0);
  });

  it('should not register anything if there was a load error', () => {
    const updateSpy = spy();
    createWrapper({
      data: { loading: false, error: {} },
      updateNavigationSection: updateSpy,
    });

    expect(updateSpy.callCount).to.equal(0);
  });

  it('should register routes once the data loads (if JSD is enabled)', () => {
    const updateSpy = spy();
    const wrapper = createWrapper({
      data: { loading: true },
      updateNavigationSection: updateSpy,
    });

    const newUpdateSpy = spy();
    wrapper.setProps({
      data: { loading: false, currentSite: { id: 'dummy-id', settings: { serviceDesk: { enabled: true } }, flags: { jsdADG3Migration: true } } },
      updateNavigationSection: newUpdateSpy,
    });

    expect(updateSpy.callCount).to.equal(0);
    expect(newUpdateSpy.callCount).to.equal(1);
  });

  it('should not register routes once the data loads (if JSD is disabled)', () => {
    const updateSpy = spy();
    const wrapper = createWrapper({
      data: { loading: true },
      updateNavigationSection: updateSpy,
    });

    const newUpdateSpy = spy();
    wrapper.setProps({
      data: { loading: false, currentSite: { id: 'dummy-id', settings: { serviceDesk: { enabled: false } }, flags: { jsdADG3Migration: true } } },
      updateNavigationSection: newUpdateSpy,
    });

    expect(updateSpy.callCount).to.equal(0);
    expect(newUpdateSpy.callCount).to.equal(0);
  });

  it('should register the new path if the JSD migration feature flag is enabled', () => {
    const updateSpy = spy();
    const wrapper = createWrapper({
      data: { loading: true },
      updateNavigationSection: updateSpy,
    });

    wrapper.setProps({
      data: { loading: false, currentSite: { id: 'dummy-id', settings: { serviceDesk: { enabled: true } }, flags: { jsdADG3Migration: true } } },
    });

    expect(((updateSpy.getCalls()[0].args[0]as NavItem).links![0].path)).to.equal('/admin/s/dummy-id/jira-service-desk/portal-only-customers');
  });

  it('should register the old path if the JSD migration feature flag is disabled', () => {
    const updateSpy = spy();
    const wrapper = createWrapper({
      data: { loading: true },
      updateNavigationSection: updateSpy,
    });

    wrapper.setProps({
      data: { loading: false, currentSite: { id: 'dummy-id', settings: { serviceDesk: { enabled: true } }, flags: { jsdADG3Migration: false } } },
    });

    expect(((updateSpy.getCalls()[0].args[0]as NavItem).links![0].path)).to.equal('/admin/jira-service-desk/portal-only-customers');
  });
});
