import * as React from 'react';
import { graphql } from 'react-apollo';
import { defineMessages, FormattedMessage } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';

import { Modal as AkModal } from '@atlaskit/onboarding';
import { akGridSizeUnitless } from '@atlaskit/util-shared-styles';

import { siteAdminMigrationImage } from 'common/images';
import { links } from 'common/link-constants';
import { ExternalLink } from 'common/navigation';
import { onboardingMutation, onboardingQuery } from 'common/onboarding';
import { withOrganizationsCount, WithOrgsCountProps } from 'common/user';

import { DismissOnboardingMutation, DismissOnboardingMutationArgs, OnboardingQuery, OnboardingQueryVariables } from '../../schema/schema-types';
import { QueryDerivedProps as CsatSurveyQueryDerivedProps, withCsatSurveyQuery } from '../csat/csat-survey';
import { SiteAdminMigrationFeatureFlag, withSiteAdminMigrationFeatureFlag } from '../feature-flags';

export const siteAdminMigrationDialogOnboardingKey = 'site.admin.page.migration';

interface OnboardingQueryDerivedProps {
  isDismissed: boolean;
  isLoading: boolean;
  hasError: boolean;
}

interface MutationDerivedProps {
  dismissDialog(): void;
}

const messages = defineMessages({
  headingWithOrgs: {
    id: 'chrome.site.admin.migration.with.orgs.heading',
    defaultMessage: 'New admin experience all in one hub!',
    description: 'The heading of a dialog that will indicate that we are designing/creating the future for administrators of our site',
  },
  bodyWithOrgs: {
    id: 'chrome.site.admin.migration.with.orgs.body',
    defaultMessage: 'You can now manage all your sites and organizations in one place at admin.atlassian.com. Learn more about the changes at our {link}.',
    description: 'This is an announcement that we combined two different sections of the website into one. The last sentence should read  "Learn more about the changes at our [Atlassian cloud blog]."',
  },
  headingWithoutOrgs: {
    id: 'chrome.site.admin.migration.without.orgs.heading',
    defaultMessage: 'The new site admin is here!',
    description: 'The heading of a dialog that will indicate that we are designing/creating the future for administrators of our site',
  },
  bodyWithoutOrgs: {
    id: 'chrome.site.admin.migration.without.orgs.body',
    defaultMessage: 'We\'ve officially moved to admin.atlassian.com. You can now manage all of your sites in one place. Learn more about the changes at our {link}.',
    description: 'This is an announcement that we changed the url they were used to. The last sentence should read "Learn more about the changes at our [Atlassian cloud blog]."',
  },
  link: {
    id: 'chrome.site.admin.migration.link',
    defaultMessage: 'Atlassian cloud blog',
    description: 'Used in the sentence "[Learn more about the changes at our] Atlassian cloud blog."',
  },
  close: {
    id: 'chrome.site.admin.migration.close',
    defaultMessage: 'Got it',
    description: 'The user will click this button that says "Got it" which means they understood the message presented to them in the dialog',
  },
});

type SiteAdminMigrationDerivedProps = CsatSurveyQueryDerivedProps & OnboardingQueryDerivedProps & MutationDerivedProps & SiteAdminMigrationFeatureFlag & WithOrgsCountProps & RouteComponentProps<{ cloudId }>;

export class SiteAdminMigrationDialogImpl extends React.Component<SiteAdminMigrationDerivedProps> {
  public render() {
    const {
      isDismissed,
      isCsatSurveyEnabled,
      isCsatSurveyDismissed,
      isCsatSurveyLoading,
      hasError,
      totalOrganizations,
      totalOrganizationsLoadError,
      siteAdminMigrationFeatureFlag: {
        isLoading,
        value: isDialogEnabled,
      },
    } = this.props;
    if (isLoading || !isDialogEnabled || isDismissed || hasError) {
      return null;
    }
    // We don't want to show both surveys at the same time. See ADMPF-123
    if (isCsatSurveyEnabled && !isCsatSurveyDismissed || (isCsatSurveyLoading === undefined ? true : isCsatSurveyLoading)) {
      return null;
    }
    const hasOrgs: boolean = (!totalOrganizationsLoadError || Object.keys(totalOrganizationsLoadError).length === 0) && !!totalOrganizations && totalOrganizations > 0;
    const heading = hasOrgs ? <FormattedMessage {...messages.headingWithOrgs} /> : <FormattedMessage {...messages.headingWithoutOrgs} />;

    return (
      <AkModal
        heading={heading}
        image={siteAdminMigrationImage}
        actions={[{
          onClick: this.onCancel,
          text: <FormattedMessage {...messages.close} />,
        }]}
        autoFocus={true}
        width={akGridSizeUnitless * 58}
      >
        <FormattedMessage
          {...(hasOrgs ? messages.bodyWithOrgs : messages.bodyWithoutOrgs)}
          values={{
            link: (
              <ExternalLink
                href={links.external.siteAdminMigration}
                hideReferrer={false}
              >
                <FormattedMessage {...messages.link} />
              </ExternalLink>
            ),
          }}
          tagName="p"
        />
      </AkModal>
    );
  }

  private onCancel = () => {
    this.props.dismissDialog();
  }
}

const withSiteAdminMigrationQuery = graphql<CsatSurveyQueryDerivedProps & RouteComponentProps<{ cloudId }>, OnboardingQuery, OnboardingQueryVariables, OnboardingQueryDerivedProps>(
  onboardingQuery,
  {
    props: ({ data }) => ({
      isDismissed: !!(data && data.onboarding && data.onboarding.dismissed),
      isLoading: !!(data && data.loading),
      hasError: !!(data && data.error),
    }),
    options: () => ({
      variables: {
        id: siteAdminMigrationDialogOnboardingKey,
      },
    }),
  },
);

const withDismissSiteAdminMigrationMutation = graphql<OnboardingQueryDerivedProps & CsatSurveyQueryDerivedProps & RouteComponentProps<{ cloudId }>, DismissOnboardingMutation, DismissOnboardingMutationArgs, MutationDerivedProps>(
  onboardingMutation,
  {
    props: ({ mutate }) => ({
      dismissDialog: async () => mutate && mutate({}),
    }),
    options: () => ({
      variables: {
        id: siteAdminMigrationDialogOnboardingKey,
      },
    }),
  },
);

export const SiteAdminMigrationDialog =
  withRouter<{}>(
    withCsatSurveyQuery(
      withSiteAdminMigrationQuery(
        withDismissSiteAdminMigrationMutation(
          withSiteAdminMigrationFeatureFlag(
            withOrganizationsCount(
              SiteAdminMigrationDialogImpl,
            ),
          ),
        ),
      ),
    ),
  );
