import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { sandbox as sinonSandbox, SinonSandbox, SinonStub } from 'sinon';

import { Modal as AkModal } from '@atlaskit/onboarding';

import { links } from 'common/link-constants';
import { ExternalLink } from 'common/navigation';

import { createMockIntlContext } from '../../utilities/testing';
import { SiteAdminMigrationDialogImpl } from './site-admin-migration-dialog';

describe('Site Admin Migration Dialog', () => {
  let sandbox: SinonSandbox;
  let dismissDialog: SinonStub;

  beforeEach(() => {
    sandbox = sinonSandbox.create();
    dismissDialog = sandbox.stub();
  });

  afterEach(() => {
    sandbox.restore();
  });

  const createWrapper = ({ isInLoadingState = false, siteAdminMigrationEnabled = false, alreadyDismissed = false, error = false, isCsatSurveyEnabled = false, isCsatSurveyLoading = false, isCsatSurveyDismissed = true, totalOrganizations = 0 }) => (
    shallow(
      <SiteAdminMigrationDialogImpl
        siteAdminMigrationFeatureFlag={{ isLoading: isInLoadingState, value: siteAdminMigrationEnabled }}
        isLoading={isInLoadingState}
        isCsatSurveyEnabled={isCsatSurveyEnabled}
        isCsatSurveyDismissed={isCsatSurveyDismissed}
        isCsatSurveyLoading={isCsatSurveyLoading}
        isDismissed={alreadyDismissed}
        hasError={error}
        dismissDialog={dismissDialog}
        totalOrganizations={totalOrganizations}
        totalOrganizationsLoadError={{} as any}
        match={{ params: { cloudId: 'test-cloud-id' } } as any}
        location={{} as any}
        history={{} as any}
      />, createMockIntlContext(),
    )
  );

  describe('UI components', () => {
    it('should render modal dialog when user has not seen dialog and feature flag turned on', () => {
      const wrapper = createWrapper({ siteAdminMigrationEnabled: true });
      expect(wrapper.find(AkModal).length).to.equal(1);
    });

    it('should render external link in modal dialog', () => {
      const wrapper = createWrapper({ siteAdminMigrationEnabled: true });
      const formattedMessages = wrapper.find(AkModal).dive().find(FormattedMessage);
      const link = formattedMessages.at(1).dive().find(ExternalLink);
      expect(link.length).to.equal(1);
      expect(link.props().href).to.deep.equal(links.external.siteAdminMigration);
    });

    it('should update call count when button is clicked modal dialog', () => {
      const wrapper = createWrapper({ siteAdminMigrationEnabled: true });
      const actions = (wrapper.find(AkModal).props() as any).actions[0];
      expect(actions.onClick).to.exist();
    });

    it('should not render modal dialog when feature flag turned off', () => {
      const wrapper = createWrapper({});
      expect(wrapper.find(AkModal).length).to.equal(0);
    });

    it('should not render modal dialog when feature flag is loading', () => {
      const wrapper = createWrapper({ isInLoadingState: true });
      expect(wrapper.find(AkModal).length).to.equal(0);
    });

    it('should not render modal dialog when csat survey feature flag is loading', () => {
      const wrapper = createWrapper({ siteAdminMigrationEnabled: true, isCsatSurveyLoading: true });
      expect(wrapper.find(AkModal).length).to.equal(0);
    });

    it('should not render modal dialog on error', () => {
      const wrapper = createWrapper({ siteAdminMigrationEnabled: true, error: true });
      expect(wrapper.find(AkModal).length).to.equal(0);
    });

    it('should not render modal dialog when the csat survey feature flag is enabled and not dismissed', () => {
      const wrapper = createWrapper({ siteAdminMigrationEnabled: true, isCsatSurveyEnabled: true, isCsatSurveyDismissed: false });
      expect(wrapper.find(AkModal).length).to.equal(0);
    });

    it('should render the correct messages if the user has no orgs', () => {
      const wrapper = createWrapper({ siteAdminMigrationEnabled: true, totalOrganizations: 0 });
      const formattedMessages = wrapper.find(AkModal).dive().find(FormattedMessage);
      expect(formattedMessages.at(0).props().defaultMessage).to.contain('The new site admin is here!');
      expect(formattedMessages.at(1).props().defaultMessage).to.contain('You can now manage all of your sites in one place');
    });

    it('should render the correct messages if the user has at least 1 org', () => {
      const wrapper = createWrapper({ siteAdminMigrationEnabled: true, totalOrganizations: 1 });
      const formattedMessages = wrapper.find(AkModal).dive().find(FormattedMessage);
      expect(formattedMessages.at(0).props().defaultMessage).to.contain('New admin experience all in one hub!');
      expect(formattedMessages.at(1).props().defaultMessage).to.contain('You can now manage all your sites and organizations in one place');
    });
  });

  describe('analytics and have-i-seen-it', () => {
    it('should not render modal dialog when it has been seen', () => {
      const wrapper = createWrapper({ siteAdminMigrationEnabled: true, alreadyDismissed: true });
      expect(wrapper.find(AkModal).length).to.equal(0);
    });
  });
});
