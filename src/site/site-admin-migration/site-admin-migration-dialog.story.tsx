import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';

import { FlagProvider } from 'common/flag';

import { createApolloClient } from '../../apollo-client';
import { SiteAdminMigrationDialogImpl } from './site-admin-migration-dialog';

const client = createApolloClient();

storiesOf('Site|Site Admin Migration', module)
  .add('No Orgs', () => {
    return (
    <ApolloProvider client={client}>
      <IntlProvider locale="en">
        <FlagProvider>
          <SiteAdminMigrationDialogImpl
            siteAdminMigrationFeatureFlag={{
              isLoading: false,
              value: true,
            }}
            isLoading={false}
            hasError={false}
            isDismissed={false}
            isCsatSurveyEnabled={false}
            isCsatSurveyLoading={false}
            isCsatSurveyDismissed={true}
            dismissDialog={action('Dismiss Dialog')}
            totalOrganizations={0}
            totalOrganizationsLoadError={undefined}
            match={{ params: { cloudId: 'test-cloud-id' } } as any}
            location={{} as any}
            history={{} as any}
          />
        </FlagProvider>
      </IntlProvider>
    </ApolloProvider>
    );
  })
  .add('With Orgs', () => {
    return (
    <ApolloProvider client={client}>
      <IntlProvider locale="en">
        <FlagProvider>
          <SiteAdminMigrationDialogImpl
            siteAdminMigrationFeatureFlag={{
              isLoading: false,
              value: true,
            }}
            isLoading={false}
            hasError={false}
            isDismissed={false}
            isCsatSurveyEnabled={false}
            isCsatSurveyLoading={false}
            isCsatSurveyDismissed={true}
            dismissDialog={action('Dismiss Dialog')}
            totalOrganizations={2}
            totalOrganizationsLoadError={undefined}
            match={{ params: { cloudId: 'test-cloud-id' } } as any}
            location={{} as any}
            history={{} as any}
          />
        </FlagProvider>
      </IntlProvider>
    </ApolloProvider>
    );
  })
  .add('With Error', () => {
    return (
    <ApolloProvider client={client}>
      <IntlProvider locale="en">
        <FlagProvider>
          <SiteAdminMigrationDialogImpl
            siteAdminMigrationFeatureFlag={{
              isLoading: false,
              value: true,
            }}
            isLoading={false}
            hasError={false}
            isDismissed={false}
            isCsatSurveyEnabled={false}
            isCsatSurveyLoading={false}
            isCsatSurveyDismissed={true}
            dismissDialog={action('Dismiss Dialog')}
            totalOrganizations={2}
            totalOrganizationsLoadError={{ message: 'ohno', graphQLErrors: [], networkError: null, extraInfo: null, name: 'thiserror' }}
            match={{ params: { cloudId: 'test-cloud-id' } } as any}
            location={{} as any}
            history={{} as any}
          />
        </FlagProvider>
      </IntlProvider>
    </ApolloProvider>
    );
  });
