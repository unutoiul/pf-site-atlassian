import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { sandbox as sinonSandbox, SinonSandbox, SinonStub } from 'sinon';

import { Modal as AkModal } from '@atlaskit/onboarding';

import { ScreenEventSender } from 'common/analytics';

import { links } from 'common/link-constants';
import { ExternalLink } from 'common/navigation';

import { createMockIntlContext } from '../../utilities/testing';
import { TrustedUserCommunicationModalImpl } from './trusted-user-communication-modal';

describe('Trusted User Communication Dialog', () => {
  let sandbox: SinonSandbox;
  let dismissDialog: SinonStub;
  let analyticsSpy;

  beforeEach(() => {
    sandbox = sinonSandbox.create();
    dismissDialog = sandbox.stub();
    analyticsSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  const createWrapper = ({ isInLoadingState = false, trustedUserCommunicationEnabled = true, alreadyDismissed = false, error = false, isTargetedTrustedUserInstance = true }) => (
    shallow(
      <TrustedUserCommunicationModalImpl
        trustedUsersCommunicationFeatureFlag={{ isLoading: isInLoadingState, value: trustedUserCommunicationEnabled }}
        isLoading={isInLoadingState}
        isDismissed={alreadyDismissed}
        hasError={error}
        match={{ params: { cloudId: 'my-site-id' } } as any}
        location={{} as any}
        history={{} as any}
        dismissDialog={dismissDialog}
        isTargetedTrustedUserInstance={isTargetedTrustedUserInstance}
        analyticsClient={{ sendUIEvent: analyticsSpy, ...{} as any }}
      />
      , createMockIntlContext(),
    )
  );

  describe('UI components', () => {
    it('should render modal dialog when user has not seen dialog and feature flag turned on', () => {
      const wrapper = createWrapper({});
      expect(wrapper.find(AkModal).length).to.equal(1);
    });

    it('should not render modal dialog when the instance is not isTargetedTrustedUserInstance', () => {
      const wrapper = createWrapper({ isTargetedTrustedUserInstance: false });
      expect(wrapper.find(AkModal).length).to.equal(0);
    });

    it('should render external link in modal dialog', () => {
      const wrapper = createWrapper({});
      const formattedMessages = wrapper.find(AkModal).find(FormattedMessage);
      expect(formattedMessages.length).to.equal(2);
      const link = wrapper.find(AkModal).find(ExternalLink);
      expect(link.length).to.equal(1);
      expect(link.props().href).to.deep.equal(links.external.trustedUsers);
    });

    it('should update call count when button is clicked modal dialog', () => {
      const wrapper = createWrapper({});
      const actions = (wrapper.find(AkModal).props() as any).actions[0];
      expect(actions.onClick).to.exist();
    });

    it('should not render modal dialog when feature flag turned off', () => {
      const wrapper = createWrapper({ trustedUserCommunicationEnabled: false });
      expect(wrapper.find(AkModal).length).to.equal(0);
    });

    it('should not render modal dialog on error', () => {
      const wrapper = createWrapper({ error: true });
      expect(wrapper.find(AkModal).length).to.equal(0);
    });

    it('should not render modal dialog on loading', () => {
      const wrapper = createWrapper({ isInLoadingState: true });
      expect(wrapper.find(AkModal).length).to.equal(0);
    });

  });

  describe('analytics and have-i-seen-it', () => {
    it('should send UI event when user clicks button', () => {
      const wrapper = createWrapper({});
      const actions = (wrapper.find(AkModal).props() as any).actions[0];
      expect(dismissDialog.callCount).to.equal(0);
      actions.onClick();
      expect(dismissDialog.callCount).to.equal(1);
      expect(analyticsSpy.callCount).to.equal(1);
    });

    it('should send UI event when user clicks link', () => {
      const wrapper = createWrapper({});
      const formattedMessages = wrapper.find(AkModal).find(FormattedMessage);
      expect(formattedMessages.length).to.equal(2);
      const link = wrapper.find(AkModal).find(ExternalLink);
      link.simulate('click');
      expect(analyticsSpy.callCount).to.equal(1);
    });

    it('should send screen event when user sees modal', () => {
      const wrapper = createWrapper({});
      expect(wrapper.find(ScreenEventSender).prop('event')).to.deep.equal({
        data: {
          name: 'trustedUserCommunicationScreenEvent',
        },
      });

    });

    it('should not render modal dialog when it has been seen', () => {
      const wrapper = createWrapper({ alreadyDismissed: true });
      expect(wrapper.find(AkModal).length).to.equal(0);
    });

  });
});
