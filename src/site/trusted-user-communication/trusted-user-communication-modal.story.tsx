import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';

import { FlagProvider } from 'common/flag';

import { TrustedUserCommunicationModalImpl } from './trusted-user-communication-modal';

const defaultProps = {
  trustedUsersCommunicationFeatureFlag: {
    isLoading: false,
    value: true,
  },
  match: { params: { cloudId: 'my-site-id' } } as any,
  location: {} as any,
  history: {} as any,
  isLoading: false,
  hasError: false,
  isDismissed: false,
  dismissDialog: action('Dismiss Dialog'),
  isTargetedTrustedUserInstance: true,
  analyticsClient: { sendUIEvent: action('Send UI Event'), ...{} as any },
};

const modalDecorator = (storyFn) => (
  <IntlProvider locale="en">
    <FlagProvider>
      {storyFn()}
    </FlagProvider>
  </IntlProvider>
);

storiesOf('Site|Trusted User Communication', module)
  .addDecorator(modalDecorator)
  .add('Default', () => {
    return (
      <TrustedUserCommunicationModalImpl
        {...defaultProps}
      />
    );
  })
  .add('Feature flag is off', () => {
    return (
      <TrustedUserCommunicationModalImpl
        {...defaultProps}
        trustedUsersCommunicationFeatureFlag={{
          isLoading: false,
          value: false,
        }}
      />
    );
  })
  .add('Feature flag is loading', () => {
    return (
      <TrustedUserCommunicationModalImpl
        {...defaultProps}
        trustedUsersCommunicationFeatureFlag={{
          isLoading: true,
          value: false,
        }}
      />
    );
  })
  .add('Onboarding query is loading', () => {
    return (
      <TrustedUserCommunicationModalImpl
        {...defaultProps}
        isLoading={true}
      />
    );
  })
  .add('Onboarding query has error', () => {
    return (
      <TrustedUserCommunicationModalImpl
        {...defaultProps}
        hasError={true}
      />
    );
  })
  .add('Onboarding modal has been dismissed in the past', () => {
    return (
      <TrustedUserCommunicationModalImpl
        {...defaultProps}
        isDismissed={true}
      />
    );
  })
  .add('Onboarding modal is not targeted Trusted User instance', () => {
    return (
      <TrustedUserCommunicationModalImpl
        {...defaultProps}
        isTargetedTrustedUserInstance={false}
      />
    );
  });
