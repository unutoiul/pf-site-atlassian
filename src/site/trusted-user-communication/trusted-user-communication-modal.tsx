import * as React from 'react';
import { compose, graphql } from 'react-apollo';
import { defineMessages, FormattedMessage } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';

import { Modal as AkModal } from '@atlaskit/onboarding';

import {
  AnalyticsClientProps,
  ScreenEventSender,
  ScreenEventSenderProps,
  trustedUserCommunicationDismissedEventData,
  trustedUserCommunicationLinkEventData,
  trustedUserCommunicationScreenEvent,
  withAnalyticsClient,
} from 'common/analytics';

import { trustedUserHeroImage } from 'common/images';
import { links } from 'common/link-constants';
import { ExternalLink } from 'common/navigation';
import { onboardingMutation, onboardingQuery } from 'common/onboarding';

import { DismissOnboardingMutation, DismissOnboardingMutationArgs, OnboardingQuery, RolePermissionsQuery } from '../../schema/schema-types';
import { getCloudIdFromPath } from '../../utilities/get-id-from-path';
import { TrustedUsersCommunicationFeatureFlag, withTrustedUsersCommunicationFeatureFlag } from '../feature-flags';
import rolePermissionsQuery from '../user-management/users/users-quick-invite/users-quick-invite.query.graphql';

export const trustedUserCommunicationDialogOnboardingKey = 'trusted.user.communication.onboarding';

interface QueryDerivedProps {
  isDismissed: boolean;
  isLoading: boolean;
  hasError: boolean;
  isTargetedTrustedUserInstance: boolean;
}

interface MutationDerivedProps {
  dismissDialog(): void;
}

const messages = defineMessages({
  heading: {
    id: 'trusted.user.communication.heading',
    defaultMessage: 'Trusted users are getting more permissions',
    description: 'The heading of a dialog that will indicate that we are updating the future capabilities for trusted users of our site',
  },
  body: {
    id: 'trusted.user.communication.body',
    defaultMessage: 'Keep an eye out for more ways trusted users can help you. In addition to adding new products, trusted users will eventually be able to invite users and administer products.',
    description: 'The body of a dialog that will indicate that we are updating the future capabilities for trusted users of our site',
  },
  link: {
    id: 'trusted.user.communication.body.link',
    defaultMessage: 'Learn more about trusted users.',
    description: 'The link of a dialog that will indicate that we are updating the future capabilities for trusted users of our site',
  },
  confirm: {
    id: 'trusted.user.communication.confirm',
    defaultMessage: 'Got it!',
    description: 'Confirmation button for the trusted users early communication modal',
  },
});

type TrustedUserCommunicationDerivedProps = QueryDerivedProps & MutationDerivedProps & AnalyticsClientProps & TrustedUsersCommunicationFeatureFlag;

export class TrustedUserCommunicationModalImpl extends React.Component<TrustedUserCommunicationDerivedProps & RouteComponentProps<{ cloudId }>> {
  public render() {
    const {
      isDismissed,
      hasError,
      isLoading,
      trustedUsersCommunicationFeatureFlag: {
        isLoading: isFlagLoading,
        value: isModalEnabled,
      },
      isTargetedTrustedUserInstance,
    } = this.props;

    if (isLoading || isFlagLoading || !isModalEnabled || isDismissed || hasError || !isTargetedTrustedUserInstance) {
      return null;
    }

    return (
      <ScreenEventSender {...this.getScreenEventProps()}>
        <AkModal
          width={460}
          heading={<FormattedMessage {...messages.heading} />}
          image={trustedUserHeroImage}
          key="trusted-user-communication"
          actions={[{
            onClick: this.onConfirm,
            text: <FormattedMessage {...messages.confirm} />,
          }]}
        >
          <FormattedMessage
            {...messages.body}
            tagName="p"
          />
          <ExternalLink
            href={`${links.external.trustedUsers}`}
            hideReferrer={false}
            onClick={this.onTrustedUsersLinkClick}
          >
            <FormattedMessage {...messages.link} />
          </ExternalLink>
        </AkModal>
      </ScreenEventSender>
    );
  }

  private onConfirm = () => {
    this.props.dismissDialog();
    this.props.analyticsClient.sendUIEvent({
      data: trustedUserCommunicationDismissedEventData(),
    });
  };

  private onTrustedUsersLinkClick = () => {
    this.props.analyticsClient.sendUIEvent({
      data: trustedUserCommunicationLinkEventData(),
    });
  };

  private getScreenEventProps(): ScreenEventSenderProps {
    return {
      isDataLoading: this.props.trustedUsersCommunicationFeatureFlag.isLoading,
      event: trustedUserCommunicationScreenEvent(),
    };
  }
}

const withTrustedUserCommunicationQuery = graphql<RouteComponentProps<{ cloudId }>, OnboardingQuery>(
  onboardingQuery,
  {
    props: ({ data }) => ({
      isDismissed: data && data.onboarding && data.onboarding.dismissed,
      isLoading: !!(data && data.loading),
      hasError: !!(data && data.error),
    } as any),
    options: () => ({
      variables: {
        id: trustedUserCommunicationDialogOnboardingKey,
      },
    }),
  },
);

const withDismissTrustedUserCommunicationMutation = graphql<QueryDerivedProps & RouteComponentProps<{ cloudId }>, DismissOnboardingMutation, DismissOnboardingMutationArgs, MutationDerivedProps>(
  onboardingMutation,
  {
    props: ({ mutate }) => ({
      dismissDialog: async () => mutate instanceof Function && mutate({}),
    }),
    options: () => ({
      variables: {
        id: trustedUserCommunicationDialogOnboardingKey,
      },
    }),
  },
);

const withRolePermissionsQuery = graphql<QueryDerivedProps & RouteComponentProps<{ cloudId }>, RolePermissionsQuery>(rolePermissionsQuery, {
  skip: ({ location: { pathname } }) => !getCloudIdFromPath(pathname),
  props: ({ data }) => ({
    isTargetedTrustedUserInstance: data
    && !data.error
    && !data.loading
    && data.rolePermissions
    && data.rolePermissions.permissionIds
    && data.rolePermissions.permissionIds.length > 0
    && data.rolePermissions.permissionIds.includes('add-products')
    && !data.rolePermissions.permissionIds.includes('invite-users'),
  } as any),
  options: ({ location: { pathname } }) => ({
    variables: {
      cloudId: getCloudIdFromPath(pathname),
    },
  }),
});

export const TrustedUserCommunicationModal = compose(
  withRouter,
  withTrustedUserCommunicationQuery,
  withDismissTrustedUserCommunicationMutation,
  withRolePermissionsQuery,
  withAnalyticsClient,
  withTrustedUsersCommunicationFeatureFlag,
)(TrustedUserCommunicationModalImpl);
