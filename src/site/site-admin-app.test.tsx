import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { sandbox as sinonSandbox, SinonSandbox, SinonSpy } from 'sinon';

import { NavigationAction } from '../store/action-types';
import { SiteAdminAppImpl } from './site-admin-app';

const AppDisconnected = SiteAdminAppImpl as any;

describe('App container', () => {
  let dispatchSpy: SinonSpy;
  let sandbox: SinonSandbox;

  beforeEach(() => {
    sandbox = sinonSandbox.create();
    dispatchSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('renders children', () => {
    const app = shallow(
      <AppDisconnected dispatch={dispatchSpy}>
        <div className="child" />
      </AppDisconnected>,
    );
    expect(app.find('.child').length).to.equal(1);
  });

  describe('history changes', () => {
    function renderAndExpectSubscription(node) {
      const spy = sandbox.spy(window, 'addEventListener');

      const app = shallow(node);
      app.instance().componentDidMount!();

      const popstateCall = spy.getCalls().find(c => c.args[0] === 'popstate');
      expect(popstateCall).to.not.equal(undefined);
      const callback: (e: Partial<PopStateEvent>) => void = popstateCall!.args[1];

      return {
        app,
        callback,
      };
    }

    it('should close an open drawer on popstate', () => {
      const { app, callback } = renderAndExpectSubscription(<AppDisconnected dispatch={dispatchSpy} drawerOpen={true} />);
      app.instance().componentDidMount!();

      callback({});

      expect(dispatchSpy.calledWith({
        type: NavigationAction.CHROME_DRAWER_CLOSE,
        modifyHistory: false,
      })).to.equal(true);
    });

    it('should reopen a drawer on popstate', () => {
      const { app, callback } = renderAndExpectSubscription(<AppDisconnected dispatch={dispatchSpy} />);
      app.instance().componentDidMount!();

      callback({
        state: {
          type: NavigationAction.CHROME_DRAWER_OPEN,
          drawer: 'test',
        },
      });

      expect(dispatchSpy.calledWith({
        type: NavigationAction.CHROME_DRAWER_OPEN,
        drawer: 'test',
        modifyHistory: false,
      })).to.equal(true);
    });
  });
});
