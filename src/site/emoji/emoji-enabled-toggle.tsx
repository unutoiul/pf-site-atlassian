import * as React from 'react';
import { ChildProps, compose, graphql } from 'react-apollo';
import { InjectedIntlProps, injectIntl } from 'react-intl';

import { ToggleStateless as AkToggle } from '@atlaskit/toggle';

import { allowUploadToggleData, analyticsClient } from 'common/analytics';
import { UIData } from 'common/analytics/new-analytics-types';

import emojiSettingQuery from './emoji-setting.query.graphql';
import updateEmojiSettingsMutation from './update-emoji-settings.mutation.graphql';

import { EmojiSettingQuery, UpdateEmojiSettingsMutation, UpdateEmojiSettingsMutationVariables } from '../../schema/schema-types';
import { settingMessages } from './emoji-messages';
import { EnabledProps } from './emoji.types';
import { ToggleStyle } from './styles';

type PropsWithQuery = ChildProps<EnabledProps, EmojiSettingQuery>;
type PropsWithMutation = ChildProps<EnabledProps, UpdateEmojiSettingsMutation>;

interface EmojiAnalytics {
  fireAnalyticsEvent?(data: UIData): void;
}

export class EmojiEnabledToggleImpl extends React.PureComponent<PropsWithQuery & PropsWithMutation & InjectedIntlProps & EmojiAnalytics> {
  public render() {
    const { data, onError, intl: { formatMessage } } = this.props;

    if (data && data.error) {
      onError(settingMessages.error);

      return <b>{formatMessage(settingMessages.error)}</b>;
    }
    if (this.isLoading()) {
      return <p>{formatMessage(settingMessages.loadingState)}</p>;
    }

    return (
      <ToggleStyle>
        <h6>{formatMessage(settingMessages.label)}</h6>
        <AkToggle isChecked={data && data.currentSite && data.currentSite.settings.emoji.uploadEnabled} onChange={this.onChange} size="large" />
      </ToggleStyle>
    );
  }

  private changeStatus = (uploadEnabled: boolean) => {
    if (!this.props.data || !this.props.data.currentSite || !this.props.mutate) {
      return;
    }

    if (this.props.fireAnalyticsEvent) {
      this.props.fireAnalyticsEvent(allowUploadToggleData(uploadEnabled));
    }

    const id = this.props.data.currentSite.settings.emoji.id;
    this.props.mutate({
      variables: {
        input: {
          uploadEnabled,
        },
        id,
      },
      optimisticResponse: {
        updateEmojiSettings: {
          __typename: 'EmojiSettings',
          id,
          uploadEnabled,
        },
      },
    }).catch((error) => {
      analyticsClient.onError(error);
    });
  };

  private makeEnabled = () => this.changeStatus(true);
  private makeDisabled = () => this.changeStatus(false);

  private onChange = () => {
    if (!this.props.data || !this.props.data.currentSite) {
      return;
    }
    if (this.props.data.currentSite.settings.emoji.uploadEnabled) {
      this.makeDisabled();
    } else {
      this.makeEnabled();
    }
  };

  private isLoading = (): boolean => {
    const { data } = this.props;

    return !data || data.loading || !data.currentSite || !data.currentSite.settings.emoji;
  };
}

const query = graphql<EnabledProps, EmojiSettingQuery>(emojiSettingQuery);

const mutation = graphql<{}, UpdateEmojiSettingsMutation, UpdateEmojiSettingsMutationVariables, any>(updateEmojiSettingsMutation);

export const EmojiEnabledToggle = compose(
  query,
  mutation,
)(injectIntl(EmojiEnabledToggleImpl));
