// tslint:disable:jsx-use-translation-function
import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import { createMockIntlContext } from '../../utilities/testing';
import { LoadableEmojiPage } from './emoji-page-loadable';

// TODO: For some reason this test is now failing because it has some conflict with the integration test emoji loading
// need to figure out why and re-enable this test
describe.skip('LoadableEmojiPage', () => {
  it('should return the emoji page wrapped in Loadable()', () => {
    const wrapper = shallow(
      <LoadableEmojiPage />,
      createMockIntlContext(),
    );

    expect(wrapper.props()).to.include({
      isLoading: true,
      pastDelay: false,
      timedOut: false,
      error: null,
    });
  });
});
