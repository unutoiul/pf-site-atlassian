import * as React from 'react';
import Loadable from 'react-loadable';

import { PageLoading } from 'common/loading';

export const LoadableEmojiPage = Loadable({
  // tslint:disable-next-line space-in-parens
  loader: async () => import(
    /* webpackChunkName: "emoji" */
    './emoji-page',
  ),
  loading: PageLoading,
  render({ EmojiPage }, props) {
    return <EmojiPage {...props} />;
  },
});
