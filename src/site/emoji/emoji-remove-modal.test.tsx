import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import AkButton from '@atlaskit/button';

import { emojiDeletionModalEvent } from 'common/analytics';

import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import { EmojiRemoveModalImpl } from './emoji-remove-modal';
import { ModalProps } from './emoji.types';

const empty = () => undefined;

function mountEmojiModal(custom) {
  const props: ModalProps = {
    onCancel: custom.onCancel || empty,
    onSubmit: custom.onSubmit || empty,
    isOpen: true,
    emojiToRemove: custom.emojiToRemove || null,
    shortName: custom.shortName || '',
    fireAnalyticsEvent: custom.fireAnalyticsEvent,
  };

  return mount(
    <EmojiRemoveModalImpl {...props} intl={createMockIntlProp()} />,
    createMockIntlContext(),
  );
}

describe('EmojiRemoveModal', () => {
  it('Should call onSubmit prop handler', () => {
    const spy = sinon.spy();
    const modal = mountEmojiModal({ onSubmit: spy });
    const button = modal.find(AkButton).at(0);
    button.simulate('click');
    expect(spy.callCount).to.equal(1);
  });

  it('Should call onCancel prop handler', () => {
    const spy = sinon.spy();
    const modal = mountEmojiModal({ onCancel: spy });
    const button = modal.find(AkButton).at(1);
    button.simulate('click');
    expect(spy.callCount).to.equal(1);
  });

  it('Should render provided emojiToRemove node in modal', () => {
    const element = <div id="testId" />;
    const modal = mountEmojiModal({ emojiToRemove: element });
    const test = modal.find('#testId');
    expect(test).to.have.length(1);
  });

  it('Should add shortname to modal', () => {
    const shortName = 'test name';
    const modal = mountEmojiModal({ shortName });
    expect(modal.text().indexOf('will be replaced with test name')).to.not.equal(-1);
  });

  it('Should fire analytics event once loaded', () => {
    const spy = sinon.spy();
    mountEmojiModal({ fireAnalyticsEvent: spy });
    expect(spy.callCount).to.equal(1);
    expect(spy.lastCall.calledWithMatch(
      emojiDeletionModalEvent(),
    ));
  });
});
