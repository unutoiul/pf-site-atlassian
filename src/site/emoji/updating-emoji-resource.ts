import { EmojiDescription as AkEmojiDescription, EmojiResource as AkEmojiResource } from '@atlaskit/emoji';

export class UpdatingEmojiResource extends AkEmojiResource {
  protected onUpload;

  constructor(config, callback) {
    super(config);
    this.onUpload = (emoji) => {
      callback(emoji);
    };
  }

  public addUnknownEmoji(emoji: AkEmojiDescription) {
    super.addUnknownEmoji(emoji);
    this.onUpload(emoji);
  }
}
