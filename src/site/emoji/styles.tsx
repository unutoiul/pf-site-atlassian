import styled from 'styled-components';

import { colors as akColors, gridSize as akGridSize } from '@atlaskit/theme';

export const AddStyle = styled.div`
  h3 {
    margin-bottom: ${akGridSize() * 2}px;
  }
`;

export const ModalBodyStyle = styled.div`
  img {
    margin-top: -${akGridSize() * 0.5}px;
    padding: ${akGridSize() * 0.5}px;
  }

  div {
    padding-top: ${akGridSize() * 3}px;
  }
`;

export const ToggleStyle = styled.div`
  h6 {
    text-transform: inherit;
    color: ${akColors.N100};
  }
`;

//// Search
export const searchIcon = 'search-icon';
export const input = 'input';

export const SearchStyle = styled.div`
  margin-bottom: ${akGridSize()}px;

  .${searchIcon} {
    opacity: 0.5;
  }

  .${input} {
    background: transparent;
    border: 0;
    font-size: 14px;
    outline: none;
    width: 100%;

    &:invalid {
      box-shadow: none;
    }

    &::-ms-clear {
      display: none;
    }
  }

  h3 {
    margin: ${akGridSize() * 2}px 0 ${akGridSize()}px 0;
  }
`;
