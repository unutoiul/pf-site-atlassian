import * as React from 'react';
import { FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';

import {
  EmojiDescription as AkEmojiDescription,
  EmojiResource as AkEmojiResource,
  EmojiSearchResult as AkEmojiSearchResult,
  EmojiUploader as AkEmojiUploader,
  ResourcedEmoji as AkResourcedEmoji,
  toEmojiId as akToEmojiId,
} from '@atlaskit/emoji';
import AkFieldBase from '@atlaskit/field-base';
import AkSuccessIcon from '@atlaskit/icon/glyph/check-circle';
import AkSearchIcon from '@atlaskit/icon/glyph/search';
import { colors as akColors } from '@atlaskit/theme';

import { getConfig } from 'common/config';
import { createErrorIcon } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';
import { PageLayout } from 'common/page-layout';

import {
  analyticsClient,
  AnalyticsClientProps,
  emojiAdminScreenEvent,
  emojiDeletionCancelButton,
  emojiDeletionConfirmButton,
  emojiSearchTypedData,
  emojiTableRemoveData,
  ScreenEventSender,
  withAnalyticsClient,
} from 'common/analytics';
import { ScreenData, UIData } from 'common/analytics/new-analytics-types';

import * as styles from './styles';

import { EmojiEnabledToggle } from './emoji-enabled-toggle';
import { defaultMessages, flagMessages } from './emoji-messages';
import { EmojiRemoveModal } from './emoji-remove-modal';
import { EmojiTable } from './emoji-table';
import { UpdatingEmojiResource } from './updating-emoji-resource';

const getEmojiProvider = (siteId: string, onEmojiAdded) => {
  return new UpdatingEmojiResource({
    providers: [{
      url: `${getConfig().apiUrl}/emoji/${siteId}/site`,
    }],
    allowUpload: true,
  }, onEmojiAdded);
};

interface EmojiPageState {
  provider?: AkEmojiResource;
  emojis: AkEmojiDescription[];
  searchQuery?: string;
  emojiToRemove: AkEmojiDescription | undefined;
  defaultPage: number;
}

interface RouteParamProps {
  cloudId: string;
}

export type EmojiPageProps = RouteComponentProps<RouteParamProps> & FlagProps & InjectedIntlProps & AnalyticsClientProps;

export class EmojiPageImpl extends React.Component<EmojiPageProps, EmojiPageState> {
  public state: Readonly<EmojiPageState> = {
    emojis: [],
    emojiToRemove: undefined,
    defaultPage: 1,
  };
  private loading: boolean = true;

  constructor(props: EmojiPageProps) {
    super(props);

    if (!props.match.params.cloudId) {
      return;
    }

    this.state = {
      ...this.state,
      provider: this.getProvider(props.match.params.cloudId),
    };
  }

  public componentWillReceiveProps(nextProps) {
    if (this.state.provider || !nextProps.siteId) {
      return;
    }
    // if the emoji provider hasn't been set already do it now.
    const emojiProvider = this.getProvider(nextProps.siteId);
    this.setState({ provider: emojiProvider });
    this.loading = false;
  }

  public componentWillUnmount() {
    const { provider } = this.state;
    if (provider) {
      provider.unsubscribe({ result: this.onSearchResult, error: this.handlePageError });
    }
  }

  public setLoading(state: boolean) {
    this.loading = state;
  }

  public render() {
    const { formatMessage } = this.props.intl;
    const { params: { cloudId } } = this.props.match;

    if (!cloudId) {
      this.handlePageError();
    }

    return (
      <ScreenEventSender event={emojiAdminScreenEvent()}>
        <PageLayout
          title={formatMessage(defaultMessages.title)}
          isFullWidth={true}
          description={formatMessage(defaultMessages.description)}
        >
          {this.getEmojiAdder()}
          {this.getEnabledDisplay()}
          {this.getModal()}
          {this.getSearchBar()}
          {this.getEmojiTable()}
        </PageLayout>
      </ScreenEventSender>
    );
  }

  private getProvider(siteId): AkEmojiResource {
    const emojiProvider = getEmojiProvider(siteId, this.onEmojiAdded);
    emojiProvider.subscribe({ result: this.onSearchResult, error: this.handlePageError });
    emojiProvider.filter(this.state.searchQuery);

    return emojiProvider;
  }

  private getEmojiAdder = () => {
    const { provider } = this.state;
    const { formatMessage } = this.props.intl;

    if (provider) {
      return (
        <styles.AddStyle>
          <h3>{formatMessage(defaultMessages.uploadHeading)}</h3>
          <AkEmojiUploader
            emojiProvider={Promise.resolve(provider)}
          />
        </styles.AddStyle>
      );
    }

    return null;
  }

  private getEnabledDisplay = () => <EmojiEnabledToggle onError={this.showError} fireAnalyticsEvent={this.fireUIEvent} />;

  private getModal = () => {
    const { emojiToRemove, provider } = this.state;

    return emojiToRemove && provider ? (
      <EmojiRemoveModal
        isOpen={!!emojiToRemove}
        emojiToRemove={<AkResourcedEmoji emojiProvider={Promise.resolve(provider)} emojiId={akToEmojiId(emojiToRemove)} />}
        shortName={emojiToRemove.shortName}
        onSubmit={this.removeEmoji}
        onCancel={this.removeCancel}
        fireAnalyticsEvent={this.fireScreenEvent}
      />
    ) : null;
  };

  private getSearchBar = () => (
    <styles.SearchStyle>
      <h3>{this.props.intl.formatMessage(defaultMessages.manageHeading)}</h3>
      <AkFieldBase
        appearance="standard"
        label="Search"
        isCompact={true}
        isLabelHidden={true}
      >
        <input className={styles.input} onInput={this.onInput} />
        <div className={styles.searchIcon} >
          <AkSearchIcon label="Search" />
        </div>
      </AkFieldBase>
    </styles.SearchStyle>
  );

  private getEmojiTable = () => {
    const { emojis, provider, searchQuery, defaultPage } = this.state;

    return (
      <EmojiTable
        provider={provider}
        query={searchQuery || ''}
        emojis={emojis}
        removeHandler={this.showModal}
        defaultPage={defaultPage}
        rowsPerPage={15}
        id={this.props.match.params.cloudId}
        loading={this.loading || !provider}
        fireAnalyticsEvent={this.fireUIEvent}
      />
    );
  };

  private removeEmoji = (): void => {
    const { emojiToRemove, provider, searchQuery } = this.state;
    this.setLoading(true);
    this.hideModal(true);

    if (!provider || !emojiToRemove) {
      return;
    }

    this.fireUIEvent(emojiDeletionConfirmButton(
      emojiToRemove.id!,
      emojiToRemove.creatorUserId!,
      emojiToRemove.createdDate!,
    ));

    provider.deleteSiteEmoji(emojiToRemove).then(success => {
      if (success) {
        this.searchForEmoji(searchQuery);
      } else {
        this.showError(defaultMessages.deleteError);
      }
      this.setLoading(false);
    }).catch((error) => {
      this.showError(defaultMessages.deleteError);
      this.setLoading(false);
      analyticsClient.onError(error);
    });
  };

  private searchForEmoji = (searchQuery?: string) => {
    const { provider } = this.state;
    this.setState({ searchQuery });

    if (provider) {
      provider.filter(searchQuery);
    }
  };

  private onEmojiAdded = (emoji: AkEmojiDescription): void => {
    const { provider } = this.state;
    const { formatMessage } = this.props.intl;

    const icon = provider
      ? <AkResourcedEmoji emojiProvider={Promise.resolve(provider)} emojiId={akToEmojiId(emoji)} />
      : (
      <div style={{ color: akColors.G300 }}>
        <AkSuccessIcon label={formatMessage(flagMessages.iconLabel)} />
      </div>
    );

    this.props.showFlag({
      autoDismiss: true,
      description: formatMessage(flagMessages.description, { emojiName: emoji.shortName }),
      icon,
      id: `emoji-upload-${emoji.id}`,
      title: formatMessage(flagMessages.header),
    });
  }

  private fireUIEvent = (data: UIData): void => {
    this.props.analyticsClient.sendUIEvent({
      data,
    });
  }

  private fireScreenEvent = (data: ScreenData): void => {
    this.props.analyticsClient.sendScreenEvent({
      data,
    });
  }

  private onSearchResult = (result: AkEmojiSearchResult): void => {
    this.setLoading(false);
    this.setState({ emojis: result.emojis });
    if (result.query === undefined) {
      return;
    }
    this.fireUIEvent(
      emojiSearchTypedData(result.query.length, result.emojis.length),
    );
  };

  private showModal = (emoji: AkEmojiDescription, currentPage: number) => {
    this.fireUIEvent(emojiTableRemoveData(emoji.id!));
    this.setState({
      emojiToRemove: emoji,
      defaultPage: currentPage,
    });
  };

  private removeCancel = () => this.hideModal(false);

  private hideModal = (removedEmoji?: boolean) => {
    const { emojiToRemove } = this.state;

    if (!removedEmoji) {
      this.fireUIEvent(emojiDeletionCancelButton(emojiToRemove!.id!));
    }
    this.setState({ emojiToRemove: undefined });
  }

  private onInput = (e) => {
    this.setState({ defaultPage: 1 });
    this.searchForEmoji(e.target.value);
  };

  private handlePageError = () => this.showError(defaultMessages.errorBody);

  private showError = (errorMessage: FormattedMessage.MessageDescriptor) => {
    const { formatMessage } = this.props.intl;
    this.setLoading(true);
    this.props.showFlag({
      autoDismiss: true,
      description: formatMessage(errorMessage),
      icon: createErrorIcon(),
      id: 'emoji-page-error',
      title: formatMessage(defaultMessages.errorTitle),
    });
  };
}

export const EmojiPage = withFlag<{}>(
  withRouter(
    injectIntl(
      withAnalyticsClient(EmojiPageImpl),
    ),
  ),
);
