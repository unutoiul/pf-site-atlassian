import { ApolloQueryResult } from 'apollo-client';
import * as React from 'react';
import { graphql } from 'react-apollo';
import { InjectedIntlProps, injectIntl } from 'react-intl';

import AkAvatar, { AvatarItem as AkAvatarItem } from '@atlaskit/avatar';
import AkButton from '@atlaskit/button';
import { DynamicTableStateless as AkDynamicTable } from '@atlaskit/dynamic-table';
import { EmojiDescription as AkEmojiDescription, ResourcedEmoji as AkResourcedEmoji, toEmojiId as akToEmojiId } from '@atlaskit/emoji';
import AkEmptyState from '@atlaskit/empty-state';

import { emojiTablePaginationData, emojiTableSortData } from 'common/analytics';
import { EmptyTableView, getPaginationMessages } from 'common/table';

import { MembersListQuery as MembersListQueryResponse, MembersListQueryVariables } from '../../schema/schema-types';
import { tableMessages } from './emoji-messages';
import {
  ADDED_ON_SORTKEY,
  applyCreatorDetails,
  convertEmojiDates,
  getCreatorIdsByPage,
  SHORTNAME_SORTKEY,
  sortEmoji, ZERO_DATE,
} from './emoji-table-utils';
import { EmojiDescriptionWithCreatorDetails, EmojiTableProps, SortOrder } from './emoji.types';
import membersListQuery from './members-list.query.graphql';

// tslint:disable-next-line no-require-imports no-var-requires
const conversationLogo: string = require('./conversation.svg');

interface EmojiTableState {
  sortedEmojis: EmojiDescriptionWithCreatorDetails[];
  currentPage: number;
  sortKey: string;
  sortOrder: SortOrder;
}

const toggleSortOrder = (order: SortOrder): SortOrder => {
  if (order === SortOrder.ASC) {
    return SortOrder.DESC;
  }

  return SortOrder.ASC;
};

interface DerivedProps {
  emojis: EmojiDescriptionWithCreatorDetails[];
  loadUsersOnPage?(page: number, emojis: EmojiDescriptionWithCreatorDetails[]): Promise<ApolloQueryResult<any>>;
}

/**
 * Slightly unintuitively this component is associated only with a GraphQL query to retrieve users
 * for display in that one particular column. The majority of the row data is actually set directly
 * as a property.
 */
export class EmojiTableImpl extends React.Component<EmojiTableProps & DerivedProps & InjectedIntlProps, EmojiTableState> {
  constructor(props) {
    super(props);

    const defaultSortKey = 'shortname';
    const defaultSortOrder = SortOrder.ASC;

    this.state = {
      currentPage: this.props.defaultPage,
      sortKey: defaultSortKey,
      sortOrder: defaultSortOrder,
      sortedEmojis: sortEmoji(defaultSortKey, defaultSortOrder, props.emojis),
    };
  }

  public static getDerivedStateFromProps(props: EmojiTableProps & DerivedProps & InjectedIntlProps, state: EmojiTableState) {
    const { sortKey, sortOrder } = state;
    if (props.emojis === state.sortedEmojis) {
      return null;
    }
    const sortedList = sortEmoji(sortKey, sortOrder, props.emojis);

    return { sortedEmojis: sortedList };
  }

  public componentWillUpdate(nextProps) {
    if (nextProps.query !== this.props.query) {
      this.setState({ currentPage: 1 });
    }
  }

  public render() {
    const { currentPage, sortKey, sortOrder } = this.state;
    const emojiRows = this.createRows();
    const { query, rowsPerPage, intl: { formatMessage }, loading } = this.props;

    return (
      <AkDynamicTable
        head={this.createHeading()}
        rows={emojiRows}
        rowsPerPage={this.props.emojis.length <= rowsPerPage ? undefined : rowsPerPage}
        page={currentPage}
        paginationi18n={this.getPaginationMessages()}
        onSetPage={this.onSetPage}
        sortKey={sortKey}
        sortOrder={SortOrder[sortOrder] as any}
        onSort={this.onSort}
        emptyView={
          !!query ? (
            <EmptyTableView>
              {formatMessage(tableMessages.query, { query })}
            </EmptyTableView>
          ) :
          <AkEmptyState
            header={formatMessage(tableMessages.empty)}
            imageUrl={conversationLogo}
          />
        }
        isLoading={loading || !emojiRows}
      />
    );
  }

  private createHeading() {
    const { formatMessage } = this.props.intl;

    return {
      cells: [
        {
          key: 'emoji',
          content: formatMessage(tableMessages.emoji),
          isSortable: false,
          shouldTruncate: false,
          width: 15,
        },
        {
          key: SHORTNAME_SORTKEY,
          content: formatMessage(tableMessages.name),
          isSortable: true,
          shouldTruncate: false,
          width: 25,
        },
        {
          key: ADDED_ON_SORTKEY,
          content: formatMessage(tableMessages.dateAdded),
          isSortable: true,
          shouldTruncate: false,
          width: 15,
        },
        {
          key: 'addedby',
          content: formatMessage(tableMessages.addedBy),
          isSortable: false,
          shouldTruncate: false,
          width: 30,
        },
        {
          key: 'remove',
          content: null,
          isSortable: false,
          shouldTruncate: false,
          width: 15,
        },
      ],
    };
  }

  private createRows() {
    const { emojis, provider, intl: { formatMessage, formatDate } } = this.props;
    if (!provider) {
      return undefined;
    }
    const promiseProvider = Promise.resolve(provider);

    return emojis.map((emoji) => {
      let { createdDateObject } = emoji;
      let formattedDate;
      if (createdDateObject && createdDateObject !== ZERO_DATE) {
        formattedDate = formatDate(createdDateObject, {
          year: 'numeric',
          month: 'numeric',
          day: 'numeric',
        });
      } else {
        createdDateObject = ZERO_DATE;
        formattedDate = formatMessage(tableMessages.unknownDate);
      }

      return {
        cells: [
          {
            key: 'emoji',
            content: <AkResourcedEmoji emojiProvider={promiseProvider} emojiId={akToEmojiId(emoji)} />,
          },
          {
            key: emoji.shortName,
            content: emoji.shortName,
          },
          {
            key: createdDateObject.getTime(),
            content: formattedDate,
          },
          {
            key: 'avatar',
            content: this.getAvatar(emoji),
          },
          {
            key: 'buttons',
            content: (
            <AkButton
              appearance="subtle-link"
              ariaHaspopup={true}
              onClick={this.onRemove(emoji)}
            >
              {formatMessage(tableMessages.remove)}
            </AkButton>),
          },
        ],
      };
    });
  }

  private onRemove = (emoji: AkEmojiDescription) => () => {
    const { currentPage } = this.state;
    this.props.removeHandler(emoji, currentPage);
  }

  private onSetPage = (currentPage: number) => {
    const oldPage = this.state.currentPage;

    if (currentPage === oldPage) {
      return;
    }

    this.setState({ currentPage });

    if (this.props.fireAnalyticsEvent) {
      this.props.fireAnalyticsEvent(
        emojiTablePaginationData(oldPage, currentPage),
      );
    }

    // fetch more user information for the creators of emojis on the new page
    if (this.props.loadUsersOnPage) {
      // tslint:disable-next-line:no-floating-promises
      this.props.loadUsersOnPage(currentPage, this.state.sortedEmojis);
    }
  }

  private onSort = (sortItem) => {
    const { sortKey, sortOrder } = this.state;
    const { key } = sortItem;
    if (!key) {
      return;
    }
    const order = key !== sortKey ? SortOrder.ASC : toggleSortOrder(sortOrder);
    const sortedEmojis = sortEmoji(key, order, this.props.emojis);

    if (this.props.fireAnalyticsEvent) {
      this.props.fireAnalyticsEvent(
        emojiTableSortData(
          key === SHORTNAME_SORTKEY ? 'shortName' : 'addedOn',
          order === SortOrder.ASC,
        ),
      );
    }

    this.setState({
      sortKey: key,
      sortOrder: order,
      sortedEmojis,
    });
  }

  private getAvatar = (emoji: EmojiDescriptionWithCreatorDetails) => {
    const { intl: { formatMessage } } = this.props;
    let fullName: string = formatMessage(tableMessages.unknownUser);
    let url;

    if (emoji.creator) {
      fullName = emoji.creator.fullName;
      url = emoji.creator.avatarUrl;
    }

    return (
      <AkAvatarItem
        avatar={(
          <AkAvatar
            appearance="circle"
            borderColor="white"
            enableTooltip={false}
            name={fullName}
            size="small"
            src={url}
          />
        )}
        backgroundColor="transparent"
        isHover={false}
        isSelected={false}
        primaryText={fullName}
      />
    );
  };

  private getPaginationMessages = () => {
    return getPaginationMessages(this.props.intl.formatMessage.bind(this.props.intl));
  }
}

const userListQuery = graphql<EmojiTableProps, MembersListQueryResponse, MembersListQueryVariables, DerivedProps>(membersListQuery, {
  options: (componentProps) => {
    const { emojis, rowsPerPage, defaultPage, id } = componentProps;

    return {
      variables: {
        id,
        aaId: getCreatorIdsByPage(emojis, rowsPerPage, defaultPage),
      },
    };
  },
  props: ({ ownProps: { emojis, rowsPerPage }, data }) => {
    let convertedEmoji = convertEmojiDates(emojis);

    if (data && !data.loading && !data.error && data.membersList) {
      convertedEmoji = applyCreatorDetails(convertedEmoji, data.membersList);
    }

    return {
      loadUsersOnPage: async (page: number, sortedEmojis: EmojiDescriptionWithCreatorDetails[]) => {
        return data!.fetchMore({
          variables: {
            aaId: getCreatorIdsByPage(sortedEmojis, rowsPerPage, page),
          },
          updateQuery: (prev, { fetchMoreResult }) => {
            const prevResult = prev as MembersListQueryResponse;
            const moreResult = fetchMoreResult as MembersListQueryResponse;
            if (!moreResult) {
              return prevResult;
            }

            const emojiWithUsers = applyCreatorDetails(emojis, moreResult.membersList);

            // combine the latest usersList with any existing query results
            return {
              emojis: emojiWithUsers,
              ...prev,
            };
          },
        });
      },
      emojis: convertedEmoji,
    };
  },
});

export const EmojiTable = userListQuery(injectIntl(EmojiTableImpl));
