import { defineMessages } from 'react-intl';

export const defaultMessages = defineMessages({
  title: {
    id: 'app.site.emoji.title',
    defaultMessage: 'Custom emojis',
    description: 'Emojis that have been added by users of this site',
  },
  description: {
    id: 'app.site.emoji.description',
    defaultMessage: 'Custom emojis can be added by your users and will be available to everyone on the site.',
    description: 'Message explaining what this page is about',
  },
  errorTitle: {
    id: 'app.site.emoji.error.title',
    defaultMessage: 'Something went wrong',
    description: 'Error message that an issue has occurred',
  },
  errorBody: {
    id: 'app.site.emoji.error.body',
    defaultMessage: 'We couldn\'t properly load the page. Please reload the page and try again.',
    description: 'Error message telling the user to reload the page to see the content correctly.',
  },
  deleteError: {
    id: 'app.site.emoji.delete',
    defaultMessage: 'We couldn\'t delete that emoji. Please reload the page and try again.',
    description: 'Error message that an emoji could not be deleted and saying that the user should reload to try again',
  },
  uploadHeading: {
    id: 'app.site.emoji.heading.upload',
    defaultMessage: 'Upload custom emojis',
    description: 'Subtitle telling the user that this section is where they can upload their own custom emojis',
  },
  manageHeading: {
    id: 'app.site.emoji.heading.manage',
    defaultMessage: 'Manage custom emojis',
    description: 'Subtitle telling the user that this section is where they can search for and delete custom emojis for the site',
  },
});

export const settingMessages = defineMessages({
  error: {
    id: 'app.site.emoji.setting.error',
    defaultMessage: 'We were unable to update your settings. Please reload the page and try again.',
    description: 'Error message for when settings cannot be updated and giving users advice on how to resolve the error',
  },
  loadingState: {
    id: 'app.site.emoji.setting.loading',
    defaultMessage: 'Loading...',
    description: 'Message for when the page is not fully loaded, as a placeholder for a setting toggle',
  },
  label: {
    id: 'app.site.emoji.setting.label',
    defaultMessage: 'Allow people to add custom emojis ',
    description: 'Label for a toggle indicating that if the toggle is on then users are able to add custom emojis to the site',
  },
});

export const tableMessages = defineMessages({
  emoji: {
    id: 'app.site.emoji.table.emoji',
    defaultMessage: 'Emoji',
    description: 'Table column heading for the column showing the emoji image',
  },
  name: {
    id: 'app.site.emoji.table.name',
    defaultMessage: 'Name',
    description: 'Table column heading for the column showing the name of the emoji',
  },
  dateAdded: {
    id: 'app.site.emoji.table.dateAdded',
    defaultMessage: 'Added on',
    description: 'Table column heading for the column showing the date the emoji was created',
  },
  addedBy: {
    id: 'app.site.emoji.table.addedBy',
    defaultMessage: 'Added by',
    description: 'Table column heading for the column showing the user who added this emoji',
  },
  empty: {
    id: 'app.site.emoji.table.empty',
    defaultMessage: 'No custom emojis have been added yet.',
    description: 'Message for when there are no emojis on the site yet',
  },
  query: {
    id: 'app.site.emoji.table.query',
    defaultMessage: 'No emojis containing "{query}" were found',
    description: 'Message shown when the user search for a given query which matched no emojis',
  },
  remove: {
    id: 'app.site.emoji.table.remove',
    defaultMessage: 'Remove',
    description: 'Button text allowing the user to remove an emoji from the site',
  },
  unknownUser: {
    id: 'app.site.emoji.table.unknownUser',
    defaultMessage: 'Unknown user',
    description: 'Message shown when the user who created an emoji is not known',
  },
  unknownDate: {
    id: 'app.site.emoji.table.unknownDate',
    defaultMessage: 'Unknown date',
    description: 'Message shown when the date that an emoji was created is not known',
  },
});

export const modalMessages = defineMessages({
  header: {
    id: 'app.site.emoji.modal.header',
    defaultMessage: 'Remove emoji',
    description: 'Title for the popup which allows the user to remove a given emoji',
  },
  remove: {
    id: 'app.site.emoji.modal.remove',
    defaultMessage: 'Remove',
    description: 'Button which confirms that the user wants to remove the emoji',
  },
  cancel: {
    id: 'app.site.emoji.modal.cancel',
    defaultMessage: 'Cancel',
    description: 'Button which allows the user to cancel the action and not delete the emoji',
  },
  prompt: {
    id: 'app.site.emoji.modal.prompt',
    defaultMessage: 'Are you sure you want to remove this emoji?',
    description: 'Message asking the user if they are sure they intend to remove the given emoji',
  },
  replacement: {
    id: 'app.site.emoji.modal.replacement',
    defaultMessage: 'All existing instances of {emojiToRemove} will be replaced with {shortName}',
    description: 'Message explaining what will happen to old usages of the emoji',
  },
});

export const flagMessages = defineMessages({
  header: {
    id: 'app.site.emoji.flag.header',
    defaultMessage: 'New emoji added',
    description: 'A new custom emoji has been uploaded to the site',
  },
  iconLabel: {
    id: 'app.site.emoji.flag.iconLabel',
    defaultMessage: 'success',
    description: 'The upload was successful or worked correctly',
  },
  description: {
    id: 'app.site.emoji.flag.description',
    defaultMessage: 'You added a new emoji {emojiName}',
    description: 'The user just added a new custom emoji to the site',
  },
});
