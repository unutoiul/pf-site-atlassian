import * as React from 'react';
import { FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';

import AkButton from '@atlaskit/button';

import { emojiDeletionModalEvent } from 'common/analytics';
import { ScreenData } from 'common/analytics/new-analytics-types';
import { ModalDialog } from 'common/modal';

import { modalMessages } from './emoji-messages';
import { ModalProps } from './emoji.types';
import { ModalBodyStyle } from './styles';

interface EmojiAnalytics {
  fireAnalyticsEvent?(data: ScreenData): void;
}

export class EmojiRemoveModalImpl extends React.PureComponent<ModalProps & InjectedIntlProps & EmojiAnalytics> {
  public render() {
    const { onCancel, onSubmit, intl: { formatMessage } } = this.props;

    if (this.props.fireAnalyticsEvent) {
      this.props.fireAnalyticsEvent(emojiDeletionModalEvent());
    }

    return (
      <ModalDialog
        width="small"
        isOpen={true}
        header={formatMessage(modalMessages.header)}
        footer={
          <div>
            <AkButton appearance="danger" onClick={onSubmit}>{formatMessage(modalMessages.remove)}</AkButton>
            <AkButton appearance="subtle-link" onClick={onCancel}>{formatMessage(modalMessages.cancel)}</AkButton>
          </div>
        }
        onClose={onCancel}
      >
        {this.generateModalBody()}
      </ModalDialog>
    );
  }

  private generateModalBody = () => {
    const { emojiToRemove, shortName, intl: { formatMessage } } = this.props;

    return (
      <ModalBodyStyle>
        {formatMessage(modalMessages.prompt)}
        <div>
          <FormattedMessage
            {...modalMessages.replacement}
            values={{ emojiToRemove, shortName }}
          />
        </div>
      </ModalBodyStyle>
    );
  };
}

export const EmojiRemoveModal: React.ComponentClass<ModalProps> = injectIntl(EmojiRemoveModalImpl);
