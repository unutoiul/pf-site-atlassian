import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import {
  EmojiDescription as AkEmojiDescription,
  EmojiResource as AkEmojiResource,
  EmojiUploader as AkEmojiUploader,
} from '@atlaskit/emoji';
import AkFieldBase from '@atlaskit/field-base';

import { PageLayout } from 'common/page-layout';

import {
  emojiAdminScreenEvent,
  emojiDeletionCancelButton,
  emojiDeletionConfirmButton,
  emojiSearchTypedData,
  emojiTableRemoveData,
  ScreenEventSender,
} from 'common/analytics';

import { createMockAnalyticsClient, createMockIntlContext, createMockIntlProp } from '../../utilities/testing';

import { EmojiPageImpl as EmojiPageStateless } from './emoji-page';
import { EmojiRemoveModal } from './emoji-remove-modal';
import { EmojiTable } from './emoji-table';
import { UpdatingEmojiResource } from './updating-emoji-resource';

function shallowEmojiPageWithData(
  cloudId = 'DUMMY-SITE-ID',
  showFlag = () => null,
  analyticsClient?,
) {
  const match: any = {
    params: {
      cloudId,
    },
  };

  analyticsClient = analyticsClient || createMockAnalyticsClient();

  return shallow(
    <EmojiPageStateless match={match} showFlag={showFlag} hideFlag={() => null} intl={createMockIntlProp()} analyticsClient={analyticsClient} {...{} as any}/>,
    createMockIntlContext(),
  );
}

const emoji: AkEmojiDescription = {
  shortName: 'example',
  id: 'id-id-id-id',
  createdDate: '2019',
  creatorUserId: 'user-id',
  type: 'custom',
  category: 'custom',
  representation: {
    imagePath: '...',
  } as any,
  searchable: true,
};

describe('EmojiPage', () => {
  let sandbox;
  beforeEach(() => {
    sandbox = sinon.sandbox.create();
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('page layout should be rendered with title', () => {
    const title = 'Custom emojis';
    const wrapper = shallowEmojiPageWithData();
    const pageLayout = wrapper.find(PageLayout);
    expect(pageLayout.props().title).to.deep.equal(title);
  });

  it('should show flag on error', () => {
    const showFlagSpy = sinon.spy();
    shallowEmojiPageWithData('', showFlagSpy);
    expect(showFlagSpy.callCount).to.equal(1);
  });

  it('should show emoji uploader', () => {
    const wrapper = shallowEmojiPageWithData();
    expect(wrapper.find(AkEmojiUploader)).to.have.length(1);
  });

  it('should show a flag on upload', () => {
    const emojiShortName = 'sample_emoji';
    const showFlagSpy = sinon.spy();
    const wrapper = shallowEmojiPageWithData('my-site', showFlagSpy);
    const provider: UpdatingEmojiResource = wrapper.state('provider');
    provider.addUnknownEmoji({ shortName: emojiShortName } as AkEmojiDescription);
    expect(showFlagSpy.callCount).to.equal(1);
    expect(showFlagSpy.getCall(0).args[0].description).contains(emojiShortName);
  });

  it('should update table when new emoji come from the provider', async () => {
    const wrapper = shallowEmojiPageWithData('my-site');
    const provider: UpdatingEmojiResource = wrapper.state('provider');

    const emojisBefore: AkEmojiDescription[] = wrapper.state('emojis');

    const sampleEmoji = { shortName: 'extra_emoji' } as AkEmojiDescription;

    (provider as any).notifyResult({
      emojis: [sampleEmoji],
      query: undefined,
    });

    wrapper.update();

    const emojisAfter: AkEmojiDescription[] = wrapper.state('emojis');
    expect(emojisAfter).to.have.length(emojisBefore.length + 1);
  });

  it('should display a search bar', () => {
    const wrapper = shallowEmojiPageWithData();
    expect(wrapper.find(AkFieldBase)).to.have.length(1);
  });

  it('should not have a provider on initial load with no siteId', () => {
    const wrapper = shallowEmojiPageWithData('');
    expect(wrapper.state('provider')).to.equal(undefined);
  });

  it('should have a provider on initial load with siteId passed', () => {
    const wrapper = shallowEmojiPageWithData('my-site');
    expect(wrapper.state('provider')).to.not.equal(undefined);
  });

  it('should display an emoji table when there is a provider', () => {
    const wrapper = shallowEmojiPageWithData();
    wrapper.setState({ provider: sinon.createStubInstance(AkEmojiResource) });
    expect(wrapper.find(EmojiTable)).to.have.length(1);
  });

  it('should set searchQuery based on text input', () => {
    const wrapper = shallowEmojiPageWithData();
    wrapper.setState({ provider: sinon.createStubInstance(AkEmojiResource) });
    const searchInput = wrapper.find(AkFieldBase).findWhere(component => component.name() === 'input');
    searchInput.simulate('focus');
    // type "test"
    searchInput.simulate('input', {
      target: {
        value: 'test',
      },
    });
    expect(wrapper.state('searchQuery')).to.equal('test');
  });

  it('should call EmojiResource.filter with query', () => {
    const wrapper = shallowEmojiPageWithData();
    const provider = sinon.createStubInstance(AkEmojiResource);
    const spy = sandbox.spy();
    provider.filter = spy;
    wrapper.setState({ provider });
    const searchInput = wrapper.find(AkFieldBase).findWhere(component => component.name() === 'input');
    searchInput.simulate('focus');
    // type "test"
    searchInput.simulate('input', {
      target: {
        value: 'test',
      },
    });
    expect(spy.callCount).to.equal(1);
    expect(spy.getCall(0).args[0]).to.equal('test');
  });

  it('should trigger screen event on load', () => {
    const match: any = {
      params: {
        cloudId: 'DUMMY-SITE-ID',
      },
    };
    const mockAnalyticsClient = createMockAnalyticsClient();
    const wrapper = shallow((
      <EmojiPageStateless
        match={match}
        showFlag={() => null}
        intl={createMockIntlProp()}
        analyticsClient={mockAnalyticsClient}
        {...{} as any}
      />
    ), createMockIntlContext());

    const screenEventSender = wrapper.find(ScreenEventSender);

    expect(screenEventSender.props().event).to.deep.equal(
      emojiAdminScreenEvent(),
    );
  });

  it('should trigger search event on typing in text input', () => {
    const filter = sinon.stub();
    const subscribe = sinon.stub();
    // Initial call should do nothing
    filter.callsFake(() => subscribe.lastCall.args[0].result({ // Other calls should return some matches for testing
      emojis: [{}, {}],
      query: 'test',
    })).onFirstCall().callsFake(() => subscribe.lastCall.args[0].result({
      query: undefined,
      emojis: [{}, {}, {}],
    }));

    // tslint:disable-next-line no-unbound-method
    const backupSubscribe = AkEmojiResource.prototype.subscribe;
    // tslint:disable-next-line no-unbound-method
    const backupFilter = AkEmojiResource.prototype.filter;
    // Use mock implementations
    AkEmojiResource.prototype.subscribe = subscribe;
    AkEmojiResource.prototype.filter = filter;

    const mockAnalyticsClient = createMockAnalyticsClient();
    const wrapper = shallowEmojiPageWithData('sample', () => null, mockAnalyticsClient);
    const searchInput = wrapper.find(AkFieldBase).findWhere(component => component.name() === 'input');
    const spy = mockAnalyticsClient.sendUIEvent;
    expect(spy.callCount).to.equal(0);

    searchInput.simulate('focus');
    searchInput.simulate('input', {
      target: {
        value: 'test',
      },
    });

    expect(spy.callCount).to.equal(1);
    expect(spy.lastCall.calledWithMatch(emojiSearchTypedData(4, 2)));
    // Unmock EmojiResource
    AkEmojiResource.prototype.subscribe = backupSubscribe;
    AkEmojiResource.prototype.filter = backupFilter;
  });

  it('should trigger deletion event', () => {
    const mockAnalyticsClient = createMockAnalyticsClient();
    const spy = mockAnalyticsClient.sendUIEvent;
    const provider = sinon.createStubInstance(AkEmojiResource);
    const wrapper = shallowEmojiPageWithData('sample', () => null, mockAnalyticsClient);
    wrapper.setState({ provider });
    const table = wrapper.find(EmojiTable);
    const removeHandler = table.prop('removeHandler');
    expect(removeHandler).to.exist();
    removeHandler({
      shortName: 'example',
      id: 'id-id-id-id',
    } as AkEmojiDescription, 1);

    expect(spy.callCount).to.equal(1);
    expect(spy.lastCall.calledWithMatch({
      data: emojiTableRemoveData('id-id-id-id'),
    })).to.be.true();
  });

  it('should trigger deletion cancel event', () => {
    const mockAnalyticsClient = createMockAnalyticsClient();
    const provider = sinon.createStubInstance(AkEmojiResource);
    const wrapper = shallowEmojiPageWithData('sample', () => null, mockAnalyticsClient);
    wrapper.setState({ provider });
    const table = wrapper.find(EmojiTable);
    table.prop('removeHandler')(emoji, 1);

    const modal = wrapper.find(EmojiRemoveModal);
    expect(modal.length).to.equal(1);
    modal.prop('onCancel')();
    const spy = mockAnalyticsClient.sendUIEvent;
    expect(spy.callCount).to.equal(2);
    expect(spy.lastCall.calledWithMatch({
      data: emojiTableRemoveData(emoji.id!),
    }));
    expect(spy.lastCall.calledWithMatch({
      data: emojiDeletionCancelButton(emoji.id!),
    }));
  });

  it('should trigger deletion confirmation event', () => {
    const mockAnalyticsClient = createMockAnalyticsClient();
    const provider = sinon.createStubInstance(AkEmojiResource);
    // Make the provider resolve the deleteSiteEmoji call or the test will fail
    provider.deleteSiteEmoji.resolves(true);
    const wrapper = shallowEmojiPageWithData('sample', () => null, mockAnalyticsClient);
    wrapper.setState({ provider });
    const table = wrapper.find(EmojiTable);

    table.prop('removeHandler')(emoji, 1);

    const modal = wrapper.find(EmojiRemoveModal);
    expect(modal.length).to.equal(1);
    modal.prop('onSubmit')();
    const spy = mockAnalyticsClient.sendUIEvent;
    expect(spy.callCount).to.equal(2);
    expect(spy.getCall(0).calledWithMatch({
      data: emojiTableRemoveData(emoji.id!),
    })).to.be.true();
    expect(spy.lastCall.calledWithMatch({
      data: emojiDeletionConfirmButton(
        emoji.id!,
        emoji.creatorUserId!,
        emoji.createdDate!,
      ),
    }));
  });
});
