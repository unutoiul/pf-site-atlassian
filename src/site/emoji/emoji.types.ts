import { FormattedMessage } from 'react-intl';

import { EmojiDescription as AkEmojiDescription, EmojiProvider as AkEmojiProvider } from '@atlaskit/emoji';

import { ScreenData, UIData } from 'common/analytics/new-analytics-types';

import { UserMinimalDisplayDetails } from '../../utilities/directory-service';

export enum SortOrder {
  ASC,
  DESC,
}

export interface EmojiDescriptionWithCreatorDetails extends AkEmojiDescription {
  creator?: UserMinimalDisplayDetails;
  createdDateObject?: Date;
}

export interface EmojiTableProps {
  emojis: EmojiDescriptionWithCreatorDetails[];
  id: string;
  defaultPage: number;
  provider?: AkEmojiProvider;
  query: string;
  rowsPerPage: number;
  loading: boolean;
  removeHandler(emoji: AkEmojiDescription, currentPage: number): void;
  fireAnalyticsEvent?(data: UIData): void;
}

export interface EnabledProps {
  onError(message: FormattedMessage.MessageDescriptor): void;
}

export interface ModalProps {
  isOpen: boolean;
  emojiToRemove: JSX.Element;
  shortName: string;
  onSubmit(): void;
  onCancel(): void;
  fireAnalyticsEvent?(data: ScreenData): void;
}

export interface SearchProps {
  onInput(e: any): void;
}
