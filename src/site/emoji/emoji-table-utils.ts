/* Functions supporting the display of the emoji-table. */

import { EmojiDescription as AkEmojiDescription } from '@atlaskit/emoji';

import { nonNullOrUndefined } from '../../utilities/arrays';
import { UserMinimalDisplayDetails } from '../../utilities/directory-service';
import { EmojiDescriptionWithCreatorDetails, SortOrder } from './emoji.types';

export const ZERO_DATE = new Date(0);

/** The key indicating a sort on emoji shortName is required. */
export const SHORTNAME_SORTKEY = 'shortname';

/** The key indicating a sort on emoji addedOn property is required. */
export const ADDED_ON_SORTKEY = 'addedon';

/**
 * Convert the ISO8601 date string in each emoji to a Date object for better sorting. If an
 * emoji is missing a 'addedOn' entry then a default ZERO_DATE is added for predictable
 * sorting behaviour.
 *
 * @param {EmojiDescription} emojis the emoji to have their dates processed
 * @returns {EmojiDescriptionWithCreatorDetails[]} the emoji with Date objects set
 */
export const convertEmojiDates = (emojis: EmojiDescriptionWithCreatorDetails[]): EmojiDescriptionWithCreatorDetails[] => {
  return emojis.map<EmojiDescriptionWithCreatorDetails>((emoji) => {
    if (!emoji.createdDateObject) {
      emoji.createdDateObject = convertToDate(emoji.createdDate);
    }

    return emoji;
  });
};

/**
 * Sort the array of emoji based on a particular key and in a particular order.
 *
 * @param {string} sortKey the key to sort the emoji on
 * @param {SortOrder} sortOrder the order to sort by
 * @param {EmojiDescriptionWithCreatorDetails[]} emoji the emoji to be sorted
 * @return {EmojiDescriptionWithCreatorDetails[]} sorted array of emoji or an unchanged array of the sortKey is not valid.
 */
export const sortEmoji = (sortKey: string, sortOrder: SortOrder, emoji: EmojiDescriptionWithCreatorDetails[]): EmojiDescriptionWithCreatorDetails[] => {
  const modifier = sortOrder === SortOrder.ASC ? 1 : -1;
  let compareField;

  if (!emoji.length) {
    return [];
  }

  if (sortKey === SHORTNAME_SORTKEY) {
    compareField = 'shortName';
  } else if (sortKey === ADDED_ON_SORTKEY) {
    compareField = 'createdDateObject';
  } else {
    return emoji;
  }

  return emoji.sort((a, b) => {
    const valA = a[compareField];
    const valB = b[compareField];

    if (!valA && !valB) {
      return 0;
    }

    if (!valA || valA < valB) {
      return -modifier;
    }

    if (!valB || valA > valB) {
      return modifier;
    }

    return 0;
  });
};

/**
 * Get user details for all the emoji creators in the specified page of  emoji.
 *
 * @param {EmojiDescription[]} emojis all the emojis, sorted in the same order as they are being displayed.
 * @param {number} pageSize the maximum number of emoji shown per page
 * @param {number} currentPage the current page of emoji to be shown, starting from 1. This determines the creator ids to be returned
 * @return {string[]} an array of unique aaId of the creators for the specified page of emoji
 */
export const getCreatorIdsByPage = (emojis: AkEmojiDescription[], pageSize: number, currentPage: number): string[] => {
  if (currentPage < 1 || pageSize <= 0 || !emojis.length) {
    return [];
  }

  const begin = (currentPage - 1) * pageSize;
  const end = begin + pageSize;

  if (begin >= emojis.length) {
    return [];
  }

  const pageOfEmoji = emojis.slice(begin, end);

  return Array.from(new Set(pageOfEmoji.map(emoji => emoji.creatorUserId).filter(nonNullOrUndefined)));
};

/**
 * Update the supplied emoji with user details from the userDetails array. If an emoji has no creatorUserId
 * property, or if there is no matching userDetails then the emoji will returned unmodified.
 *
 * @param {EmojiDescription[]} emojis the emojis to be updated to include UserMinimalDisplayDetails objects
 * @param {UserMinimalDisplayDetails[]} userDetails the user details to be applied to the emoji
 * @return {EmojiDescriptionWithCreatorDetails[]} the supplied emoji but with any matching user details applied.
 */
export const applyCreatorDetails = (emojis: AkEmojiDescription[], userDetails: UserMinimalDisplayDetails[]): EmojiDescriptionWithCreatorDetails[] => {
  const userDetailsByAaId = new Map<string, UserMinimalDisplayDetails>();
  userDetails.forEach(user => {
    // New Relic reported an error here about `user` being undefined.
    // Most likely that is un-sanitized data from directory service, so we just add a check
    if (user) {
      userDetailsByAaId.set(user.id, user);
    }
  });

  return emojis.map<EmojiDescriptionWithCreatorDetails>((emoji) => {
    if (!emoji.creatorUserId) {
      return emoji;
    }
    const creator = userDetailsByAaId.get(emoji.creatorUserId);

    if (!creator) {
      return emoji;
    }

    return {
      creator,
      ...emoji,
    };
  });
};

/**
 * Convert the supplied String to a Date.
 *
 * @param {string} dateString an ISO8601 Date Format to be parsed
 * @return {Date} the Date. ZERO_DATE will be returned if the string cannot be parsed.
 */
const convertToDate = (dateString: string | undefined): Date => {
  if (!dateString) {
    return ZERO_DATE;
  }

  const date = new Date(dateString);

  return isNaN(date.getTime()) ? ZERO_DATE : date;
};
