
import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import AkButton from '@atlaskit/button';
import { DynamicTableStateless as AkDynamicTable } from '@atlaskit/dynamic-table';
import { EmojiDescription as AkEmojiDescription, EmojiResource as AkEmojiResource, ResourcedEmoji as AkResourcedEmoji } from '@atlaskit/emoji';

import { emojiTablePaginationData, emojiTableSortData } from 'common/analytics';

import { createMockIntlProp } from '../../utilities/testing';
import { EmojiTableImpl } from './emoji-table';

const stripColons = (shortname: string): string => shortname.replace(/:/g, '');
const sortEmojis = (emojis: AkEmojiDescription[]): AkEmojiDescription[] => {
  const sorted: AkEmojiDescription[] = [];
  emojis.forEach(emoji => sorted.push(emoji));
  sorted.sort((a, b) => stripColons(a.shortName).localeCompare(stripColons(b.shortName)));

  return sorted;
};
const extractShortNames = (emojis: AkEmojiDescription[]): string[] => {
  const shortNames: string[] = [];
  emojis.forEach(emoji => shortNames.push(emoji.shortName));

  return shortNames;
};

function* generateEmojis(count: number): IterableIterator<AkEmojiDescription> {
  for (let i = 1; i <= count; i++) {
    yield { shortName: `generated-emoji-#${i}` } as AkEmojiDescription;
  }
}

describe('EmojiTable', () => {
  const emojisData: AkEmojiDescription[] = Array.from(generateEmojis(40));
  const sortedEmojis = sortEmojis(emojisData);

  function mountTable(props: any = {}) {
    const defaultPage = props.defaultPage || 1;
    const rowsPerPage = props.rowsPerPage || 20;
    const removeHandler = props.removeHandler || {};
    const query = props.query || '';
    const emojis = props.emojis || emojisData;
    const mockProvider = sinon.createStubInstance(AkEmojiResource) as AkEmojiResource;
    const loading = props.loading || false;
    const fireAnalyticsEvent = props.fireAnalyticsEvent || (() => null);
    const loadUsersOnPage = props.loadUsersOnPage || undefined;

    return mount(
      <EmojiTableImpl
        defaultPage={defaultPage}
        emojis={emojis}
        provider={mockProvider}
        query={query}
        rowsPerPage={rowsPerPage}
        removeHandler={removeHandler}
        id={'DUMMY_SITE_ID'}
        intl={createMockIntlProp()}
        loading={loading}
        fireAnalyticsEvent={fireAnalyticsEvent}
        loadUsersOnPage={loadUsersOnPage}
      />,
    );
  }

  let sandbox: sinon.SinonSandbox;

  beforeEach(() => {
    sandbox = sinon.sandbox.create();
    // TODO: Remove this once the console error in loading the application is fixed.
    sandbox.stub(console, 'error');
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('Should pass loading prop down to DynamicTable', () => {
    const table = mountTable({ loading: true });
    expect(table.find(AkDynamicTable).at(0).prop('isLoading')).to.equal(true);
  });

  it('Should display all passed in emojis if rowsPerPage is less than numEmojis', () => {
    const numEmojis = sortedEmojis.length;
    const table = mountTable({ rowsPerPage: numEmojis + 1 });
    expect(table.find(AkResourcedEmoji)).to.have.length(sortedEmojis.length);
  });

  it('Should not display pagination nav if there are more rowsPerPage than numEmojis', () => {
    const numEmojis = sortedEmojis.length;
    const table = mountTable({ rowsPerPage: numEmojis + 1 });
    expect(table.find(AkDynamicTable).at(0).prop('rowsPerPage')).to.equal(undefined);
  });

  it('Should display pagination nav if there are more rowsPerPage than numEmojis', () => {
    const numEmojis = sortedEmojis.length;
    const rowsPerPage = numEmojis - 3;
    const table = mountTable({ rowsPerPage });
    expect(table.find(AkDynamicTable).at(0).prop('rowsPerPage')).to.equal(rowsPerPage);
  });

  it('Should display emojis up to rowsPerPage', () => {
    const table = mountTable({ rowsPerPage: 10 });
    expect(table.find(AkResourcedEmoji)).to.have.length(10);
  });

  it('Should display emojis sorted lexicographically by shortname', () => {
    const emojis: AkEmojiDescription[] = [
      { shortName: 'b' } as AkEmojiDescription,
      { shortName: 'B' } as AkEmojiDescription,
      { shortName: 'a' } as AkEmojiDescription,
    ];
    const numEmojis = emojis.length;
    const table = mountTable({ emojis, rowsPerPage: numEmojis + 1 });

    expect(table.find(AkResourcedEmoji).at(0).prop('emojiId').shortName).to.equal('B');
    expect(table.find(AkResourcedEmoji).at(1).prop('emojiId').shortName).to.equal('a');
    expect(table.find(AkResourcedEmoji).at(2).prop('emojiId').shortName).to.equal('b');
  });

  it('Should start on the default page passed in through props', () => {
    const sortedShortNames = extractShortNames(sortedEmojis);
    const table = mountTable({ defaultPage: 2, rowsPerPage: 10 });
    // First emoji on second page should be 11th in lexicographical order
    expect(table.find(AkResourcedEmoji).at(0).prop('emojiId').shortName).to.deep.equal(sortedShortNames[10]);
  });

  it('Should display a remove button for every row', () => {
    const table = mountTable({ rowsPerPage: 10 });
    const removeButtons = table.find(AkButton).filterWhere(button => button.text() === 'Remove');
    expect(removeButtons).to.have.length(10);
  });

  it('Should call the remove handler when the remove button is clicked', () => {
    const spy = sinon.spy();
    const table = mountTable({ removeHandler: spy });
    const button = table.find(AkButton).at(0);
    button.simulate('click');
    expect(spy.callCount).to.equal(1);
  });

  it('Should pass the correct args to the remove handler', () => {
    const spy = sinon.spy();
    const table = mountTable({ defaultPage: 2, rowsPerPage: 10, removeHandler: spy });
    const button = table.find(AkButton).at(0);
    button.simulate('click');
    expect(spy.getCall(0).args[0]).to.deep.equal(sortedEmojis[10]);
    expect(spy.getCall(0).args[1]).to.equal(2);
  });

  it('Should display an appropriate message if no emojis', () => {
    const table = mountTable({ emojis: [] });
    expect(table.text().indexOf('No custom emojis have been added yet.')).to.not.equal(-1);
  });

  it('Should display an appropriate message if query has no results', () => {
    const table = mountTable({ emojis: [], query: 'blah' });
    expect(table.text().indexOf('No emojis containing "blah" were found')).to.not.equal(-1);
  });

  it('Should fire analytics event on reordering the table', () => {
    const spy = sinon.spy();
    const table = mountTable({ fireAnalyticsEvent: spy });
    expect(table.find('th')).to.have.length(5);
    const sortByName = table.find('th').at(1);
    const addedOn = table.find('th').at(2);

    sortByName.simulate('click');
    expect(spy.callCount).to.equal(1);
    expect(spy.getCall(0).calledWithMatch(emojiTableSortData('shortName', false))).to.be.true();

    sortByName.simulate('click');
    expect(spy.callCount).to.equal(2);
    expect(spy.getCall(1).calledWithMatch(emojiTableSortData('shortName', true))).to.be.true();

    addedOn.simulate('click');
    expect(spy.callCount).to.equal(3);
    expect(spy.getCall(2).calledWithMatch(emojiTableSortData('addedOn', true))).to.be.true();

    sortByName.simulate('click');
    expect(spy.callCount).to.equal(4);
    expect(spy.getCall(3).calledWithMatch(emojiTableSortData('shortName', true))).to.be.true();
  });

  it('Should fire analytics event on paginating', () => {
    const spy = sinon.spy();
    const table = mountTable({ emojis: Array.from(generateEmojis(40)), fireAnalyticsEvent: spy, rowsPerPage: 10 });
    const paginates = table.find(AkButton).filterWhere(component => !isNaN(+component.text()));

    // Paginates has 6 matches: [<, 1, 2, 3, 4, >]
    let last = 1;
    let calls = 0;
    for (const page of [4, 2, 3, 1, 4, 4]) {
      paginates.at(page).simulate('click');
      if (page !== last) {
        calls += 1;
        expect(spy.lastCall.calledWithMatch(emojiTablePaginationData(last, page))).to.be.true();
        last = page;
      }
      expect(spy.callCount).to.equal(calls);
    }
  });

  it('Should call loadUsersOnPage with a sorted emoji list', () => {
    const unsortedEmojis: AkEmojiDescription[] = [
      { shortName: ':wrong:' } as AkEmojiDescription,
      { shortName: ':order:' } as AkEmojiDescription,
    ];

    const loadUsersSpy = sinon.spy();

    const table = mountTable({
      emojis: unsortedEmojis,
      rowsPerPage: 5,
      loadUsersOnPage: loadUsersSpy,
    });

    const akTable = table.find(AkDynamicTable);
    expect(akTable).to.have.length(1);
    expect(akTable).not.to.equal(undefined);

    const onSetPage = akTable.prop('onSetPage');
    expect(onSetPage).to.not.equal(undefined);
    // Wrapping in if to prevent ts complaining
    if (onSetPage) {
      onSetPage(2);
    }
    // Check loadUsersOnPage called with emojis list in sorted order
    expect(loadUsersSpy.callCount).to.be.equal(1);
    const emojis = loadUsersSpy.lastCall.args[1];
    expect(emojis[0].shortName).to.be.equal(':order:');
    expect(emojis[1].shortName).to.be.equal(':wrong:');
  });
});
