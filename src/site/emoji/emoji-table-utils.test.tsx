import { expect } from 'chai';

import { UserMinimalDisplayDetails } from '../../utilities/directory-service';
import { ADDED_ON_SORTKEY, applyCreatorDetails, convertEmojiDates, getCreatorIdsByPage, SHORTNAME_SORTKEY, sortEmoji, ZERO_DATE } from './emoji-table-utils';
import { EmojiDescriptionWithCreatorDetails, SortOrder } from './emoji.types';

describe('emoji-table-utils', () => {
  describe('convertEmojiDates', () => {
    it('should create zero dates for emoji with no createdDate', () => {
      const emoji: EmojiDescriptionWithCreatorDetails[] = [
        {
          shortName: 'wubba',
          type: 'standard',
          category: 'PEOPLE',
          representation: undefined,
          searchable: true,
        },
        {
          shortName: 'lubba',
          type: 'standard',
          category: 'PEOPLE',
          representation: undefined,
          searchable: true,
        },
      ];

      const convertedEmoji = convertEmojiDates(emoji);
      expect(convertedEmoji).to.have.lengthOf(emoji.length);
      emoji.forEach(e => {
        expect(e.createdDateObject).to.equal(ZERO_DATE);
      });
    });

    it('should only convert dates for emoji that need it' , () => {
      const emoji: EmojiDescriptionWithCreatorDetails[] = [
        {
          shortName: 'wubba',
          type: 'standard',
          category: 'PEOPLE',
          createdDate: '2017-10-16T00:21:23.853Z',
          representation: undefined,
          searchable: true,
        },
        {
          shortName: 'lubba',
          type: 'standard',
          category: 'PEOPLE',
          createdDate: '2017-10-16T00:21:23.853Z',
          createdDateObject: new Date(2015, 6, 24),
          representation: undefined,
          searchable: true,
        },
      ];

      const convertedEmoji = convertEmojiDates(emoji);
      expect(convertedEmoji).to.have.lengthOf(2);
      expect(convertedEmoji[0].createdDateObject!.getFullYear()).to.equal(2017);
      expect(convertedEmoji[1].createdDateObject!.getFullYear()).to.equal(2015);
    });

    it('should set zero date for badly formatted dates', () => {
      const emoji: EmojiDescriptionWithCreatorDetails[] = [
        {
          shortName: 'wubba',
          type: 'standard',
          category: 'PEOPLE',
          createdDate: 'apples and pears',
          representation: undefined,
          searchable: true,
        },
        {
          shortName: 'lubba',
          type: 'standard',
          category: 'PEOPLE',
          createdDate: 'monkey trousers',
          representation: undefined,
          searchable: true,
        },
      ];

      const convertedEmoji = convertEmojiDates(emoji);
      expect(convertedEmoji).to.have.lengthOf(emoji.length);
      emoji.forEach(e => {
        expect(e.createdDateObject).to.equal(ZERO_DATE);
      });
    });
  });

  describe('convertEmojiDates', () => {
    it('should return same order if sortkey not valid', () => {
      const emoji: EmojiDescriptionWithCreatorDetails[] = [
        {
          shortName: 'wubba',
          type: 'standard',
          category: 'PEOPLE',
          createdDate: '2017-10-16T00:21:23.853Z',
          representation: undefined,
          searchable: true,
        },
        {
          shortName: 'lubba',
          type: 'standard',
          category: 'PEOPLE',
          createdDate: '2017-10-16T00:21:23.853Z',
          representation: undefined,
          searchable: true,
        },
      ];

      const sortedEmoji = sortEmoji('madeUpSortKey', SortOrder.DESC, emoji);

      expect(sortedEmoji).to.deep.equal(emoji);
    });

    it('should return the same order if sortkey property is not present on emoji', () => {
      const emoji: EmojiDescriptionWithCreatorDetails[] = [
        {
          shortName: 'wubba',
          type: 'standard',
          category: 'PEOPLE',
          createdDate: '2017-10-16T00:21:23.853Z',
          representation: undefined,
          searchable: true,
        },
        {
          shortName: 'lubba',
          type: 'standard',
          category: 'PEOPLE',
          createdDate: '2017-10-16T00:21:23.853Z',
          representation: undefined,
          searchable: true,
        },
      ];

      const sortedEmoji = sortEmoji(ADDED_ON_SORTKEY, SortOrder.DESC, emoji);

      expect(sortedEmoji).to.deep.equal(emoji);
    });

    it('should sort descending and ascending', () => {
      const emoji: EmojiDescriptionWithCreatorDetails[] = [
        {
          shortName: 'wubba',
          type: 'standard',
          category: 'PEOPLE',
          createdDate: '2017-10-16T00:21:23.853Z',
          representation: undefined,
          searchable: true,
        },
        {
          shortName: 'dubdub',
          type: 'standard',
          category: 'PEOPLE',
          createdDate: '2017-10-16T00:21:23.853Z',
          representation: undefined,
          searchable: true,
        },
        {
          shortName: 'lubba',
          type: 'standard',
          category: 'PEOPLE',
          createdDate: '2017-10-16T00:21:23.853Z',
          representation: undefined,
          searchable: true,
        },
      ];

      const descSortedEmoji = sortEmoji(SHORTNAME_SORTKEY, SortOrder.DESC, emoji);

      expect(descSortedEmoji).to.have.lengthOf(3);
      expect(descSortedEmoji[0].shortName).to.equal('wubba');
      expect(descSortedEmoji[1].shortName).to.equal('lubba');
      expect(descSortedEmoji[2].shortName).to.equal('dubdub');

      const ascSortedEmoji = sortEmoji(SHORTNAME_SORTKEY, SortOrder.ASC, emoji);
      expect(ascSortedEmoji).to.have.lengthOf(3);
      expect(ascSortedEmoji[0].shortName).to.equal('dubdub');
      expect(ascSortedEmoji[1].shortName).to.equal('lubba');
      expect(ascSortedEmoji[2].shortName).to.equal('wubba');
    });

    it('should return an empty list on sort if passed an empty list', () => {
      const empty = sortEmoji(SHORTNAME_SORTKEY, SortOrder.DESC, []);
      expect(empty).to.deep.equal([]);
    });
  });

  describe('getCreatorIdsByPage', () => {
    const testEmoji: EmojiDescriptionWithCreatorDetails[] = [
      {
        shortName: 'wubba',
        type: 'standard',
        category: 'PEOPLE',
        creatorUserId: 'abcdef',
        representation: undefined,
        searchable: true,
      },
      {
        shortName: 'lubba',
        type: 'standard',
        category: 'PEOPLE',
        creatorUserId: 'abcdef',
        representation: undefined,
        searchable: true,
      },
      {
        shortName: 'dubdub',
        type: 'standard',
        category: 'PEOPLE',
        creatorUserId: 'mnopqrs',
        representation: undefined,
        searchable: true,
      },
    ];

    it('should return an empty set when parameters are out of range', () => {
      expect(getCreatorIdsByPage(testEmoji, 0, 1)).to.deep.equal([]);
      expect(getCreatorIdsByPage(testEmoji, 1, 0)).to.deep.equal([]);
      expect(getCreatorIdsByPage(testEmoji, 10, 3)).to.deep.equal([]);
    });

    it('should return the second page of creatorIds', () => {
      const creatorIds = getCreatorIdsByPage(testEmoji, 2, 2);
      expect(creatorIds).to.have.lengthOf(1);
      expect(creatorIds[0]).to.equal('mnopqrs');
    });

    it('should not return duplicate creator Ids', () => {
      const creatorIds = getCreatorIdsByPage(testEmoji, 10, 1);
      expect(creatorIds).to.have.lengthOf(2);
      expect(creatorIds[0]).to.equal('abcdef');
      expect(creatorIds[1]).to.equal('mnopqrs');
    });
  });

  describe('applyCreatorDetails', () => {
    const userDetails: UserMinimalDisplayDetails[] = [
      {
        id: 'mnopqrs',
        fullName: 'Monkey Trousers',
        avatarUrl: 'http://example.org/Monkey.png',
      },
      {
        id: 'abcdef',
        fullName: 'Simian Waistcoat',
        avatarUrl: 'http://example.org/Simian.png',
      },
      {
        id: 'ghijkl',
        fullName: 'Gorilla Bowler',
        avatarUrl: 'http://example.org/Gorilla.png',
      },
    ];

    it('should apply all matching users', () => {
      const testEmoji: EmojiDescriptionWithCreatorDetails[] = [
        {
          shortName: 'wubba',
          type: 'standard',
          category: 'PEOPLE',
          creatorUserId: 'abcdef',
          representation: undefined,
          searchable: true,
        },
        {
          shortName: 'lubba',
          type: 'standard',
          category: 'PEOPLE',
          creatorUserId: 'ghijkl',
          representation: undefined,
          searchable: true,
        },
        {
          shortName: 'dubdub',
          type: 'standard',
          category: 'PEOPLE',
          creatorUserId: 'mnopqrs',
          representation: undefined,
          searchable: true,
        },
      ];

      const updatedEmoji = applyCreatorDetails(testEmoji, userDetails);
      expect(updatedEmoji).to.have.lengthOf(3);
      expect(updatedEmoji[0].creator).to.deep.equal(userDetails[1]);
      expect(updatedEmoji[1].creator).to.deep.equal(userDetails[2]);
      expect(updatedEmoji[2].creator).to.deep.equal(userDetails[0]);
    });

    it('should leave emoji untouched if there is no user Id or no matching user details', () => {
      const testEmoji: EmojiDescriptionWithCreatorDetails[] = [
        {
          shortName: 'wubba',
          type: 'standard',
          category: 'PEOPLE',
          representation: undefined,
          searchable: true,
        },
        {
          shortName: 'lubba',
          type: 'standard',
          category: 'PEOPLE',
          creatorUserId: 'xyz',
          representation: undefined,
          searchable: true,
        },
      ];

      const updatedEmoji = applyCreatorDetails(testEmoji, userDetails);
      expect(updatedEmoji).to.have.lengthOf(2);
      expect(updatedEmoji[0].creator === undefined).to.equal(true);
      expect(updatedEmoji[1].creator === undefined).to.equal(true);
    });
  });
});
