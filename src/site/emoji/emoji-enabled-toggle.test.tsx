import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import { ToggleStateless as AkToggle } from '@atlaskit/toggle';

import { allowUploadToggleData } from 'common/analytics';

import { createMockIntlProp } from '../../utilities/testing';
import { EmojiEnabledToggleImpl } from './emoji-enabled-toggle';

function shallowEnabledToggleWithData(data: any = { currentSite: { settings: { emoji: { id: 'DUMMY-SITE-ID' } } } }, onError?: any) {
  const mutationSpy = sinon.spy();
  const analyticsSpy = sinon.spy();
  const mutation = (...args) => {
    mutationSpy(...args);

    return {
      catch: () => null,
    };
  };

  return {
    wrapper: shallow(
      <EmojiEnabledToggleImpl
        data={data}
        mutate={mutation as any}
        onError={onError}
        intl={createMockIntlProp()}
        fireAnalyticsEvent={analyticsSpy}
      />,
    ),
    mutate: mutationSpy,
    analytics: analyticsSpy,
  };
}

describe('EmojiEnabledToggle', () => {
  it('Should call the onError prop as an error handler', () => {
    const spy = sinon.spy();
    shallowEnabledToggleWithData({ error: true }, spy);
    expect(spy.callCount).to.equal(1);
  });

  it('Clicking the toggle should call mutate with an optimistic response', () => {
    const { wrapper, mutate } = shallowEnabledToggleWithData();
    const onChange: () => {} = wrapper.find(AkToggle).prop('onChange');
    onChange();
    expect(mutate.getCall(0).args[0].optimisticResponse).to.deep.equal({
      updateEmojiSettings: {
        __typename: 'EmojiSettings',
        uploadEnabled: true,
        id: 'DUMMY-SITE-ID',
      },
    });
  });

  it('Clicking the toggle should call mutate with the correct variables', () => {
    const { wrapper, mutate } = shallowEnabledToggleWithData();
    const onChange: () => {} = wrapper.find(AkToggle).prop('onChange');
    onChange();
    expect(mutate.getCall(0).args[0].variables).to.deep.equal({
      id: 'DUMMY-SITE-ID',
      input: {
        uploadEnabled: true,
      },
    });
  });

  it('Clicking the toggle when off should trigger true analytics event', () => {
    const { analytics, wrapper } = shallowEnabledToggleWithData();
    const onChange: () => {} = wrapper.find(AkToggle).prop('onChange');
    onChange();
    expect(analytics.callCount).to.equal(1);
    expect(analytics.lastCall.calledWithMatch(allowUploadToggleData(true))).to.be.true();
  });

  it('Clicking the toggle when on should trigger false analytics event', () => {
    const { analytics, wrapper } = shallowEnabledToggleWithData({
      currentSite: { settings: { emoji: { uploadEnabled: true } } },
    });
    const onChange: () => {} = wrapper.find(AkToggle).prop('onChange');
    onChange();
    expect(analytics.callCount).to.equal(1);
    expect(analytics.lastCall.calledWithMatch(allowUploadToggleData(false))).to.be.true();
  });
});
