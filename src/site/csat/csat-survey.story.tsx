import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';

import { FlagProvider } from 'common/flag';

import { createMockIntlProp } from '../../utilities/testing';
import { CsatSurveyImpl } from './csat-survey';

storiesOf('Site|Csat Survey', module)
  .add('Default', () => {
    return (
      <IntlProvider locale="en">
        <FlagProvider>
          <CsatSurveyImpl
            isCsatSurveyDismissed={false}
            isCsatSurveyEnabled={true}
            dismissCsat={action('Dismiss Csat')}
            match={{ params: { cloudId: 'my-site-id' } } as any}
            location={{} as any}
            history={{} as any}
            aaId="557057:26ead946-4955-46ef-a11e-b1f9ae987de7"
            intl={createMockIntlProp()}
            siteId={'my-site-id'}
            analyticsClient={{ sendUIEvent: action('Send UI Event'), ...{} as any }}
          />
        </FlagProvider>
      </IntlProvider>
    );
  });
