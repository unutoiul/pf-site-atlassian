import { expect } from 'chai';
import { mount, ReactWrapper } from 'enzyme';
import * as React from 'react';
import { sandbox as sinonSandbox, SinonSandbox, SinonStub } from 'sinon';

import { FlagProvider } from 'common/flag';
import { ExternalLink } from 'common/navigation';

import { FlagRenderer } from 'common/flag/flag-provider';

import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import { CsatSurveyImpl } from './csat-survey';

describe('CsatSurvey', () => {
  let sandbox: SinonSandbox;
  let dismissCsat: SinonStub;
  let mutate: SinonStub;

  beforeEach(() => {
    sandbox = sinonSandbox.create();
    dismissCsat = sandbox.stub();
    mutate = sandbox.stub();
  });

  afterEach(() => {
    sandbox.restore();
  });

  class CsatHelper {
    private wrapper: ReactWrapper;

    constructor(isSurveyEnabled: boolean = true, isDismissed: boolean = false) {
      this.wrapper = mount((
        <FlagProvider>
          <CsatSurveyImpl
            isCsatSurveyDismissed={isDismissed}
            isCsatSurveyEnabled={isSurveyEnabled}
            match={{ params: { cloudId: 'my-site-id' } } as any}
            location={{} as any}
            history={{} as any}
            dismissCsat={dismissCsat}
            aaId="557057:26ead946-4955-46ef-a11e-b1f9ae987de7"
            intl={createMockIntlProp()}
            siteId={'my-site-id'}
            analyticsClient={{ sendUIEvent: sandbox.stub(), ...{} as any }}
          />
          <FlagRenderer/>
        </FlagProvider>
      ), createMockIntlContext());
    }

    public get text(): string {
      try {
        return this.wrapper.text();
      } catch (_) {
        return '*error*';
      }
    }

    public get html(): string {
      try {
        return this.wrapper.debug();
      } catch (_) {
        return '*error*';
      }
    }

    public get linkURL(): string {
      return this.wrapper.find(ExternalLink)
        .filterWhere((e) => e.html().indexOf('Answer 2 short questions') > -1).getDOMNode().getAttribute('href') || '';
    }

    public get linkNode(): ReactWrapper {
      return this.wrapper.find(ExternalLink)
        .filterWhere((e) => e.html().indexOf('Answer 2 short questions') > -1).first();
    }

    public get cancelNode(): ReactWrapper {
      return this.wrapper.find('CrossIcon');
    }
  }
  it('renders the dialog if not already dismissed and survey is enabled', () => {
    const helper = new CsatHelper(true, false);

    expect(helper.html).to.match(/Hi, I\\'m Kevin!/);
  });

  it('does not render the dialog if survey is not enabled', () => {
    const helper = new CsatHelper(false, false);

    expect(helper.html).to.not.match(/Hi, I\\'m Kevin!/);
  });

  it('does not render the dialog if survey was already dismissed', () => {
    const helper = new CsatHelper(true, true);

    expect(helper.html).to.not.match(/Hi, I\\'m Kevin!/);
  });

  it('creates the link with uuid query param', () => {
    const helper = new CsatHelper();

    expect(helper.linkURL).to.equal('https://surveys.atlassian.com/jfe/form/SV_9yFraEfJlYLcfQh?UUID=557057:26ead946-4955-46ef-a11e-b1f9ae987de7');
  });

  it('calls dismissCsat if link is clicked', () => {
    mutate.returns(Promise.resolve('OK'));
    const helper = new CsatHelper();

    const link: ReactWrapper = helper.linkNode;

    expect(dismissCsat.callCount).to.equal(0);
    link.simulate('click');
    expect(dismissCsat.callCount).to.equal(1);
  });

  it('calls dismissCsat if cancel is clicked', () => {
    mutate.returns(Promise.resolve('OK'));

    const helper = new CsatHelper();

    const cancel: ReactWrapper = helper.cancelNode;

    expect(dismissCsat.callCount).to.equal(0);
    cancel.simulate('click');
    expect(dismissCsat.callCount).to.equal(1);
  });
});
