import * as React from 'react';
import { graphql } from 'react-apollo';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';

import AkAvatar from '@atlaskit/avatar';

import {
  AnalyticsClientProps,
  csatSurveyDismissedEventData,
  csatSurveyLinkEventData,
  withAnalyticsClient,
} from 'common/analytics';
import { Flag } from 'common/flag';
import { csatImage } from 'common/images';
import { links } from 'common/link-constants';
import { ExternalLink } from 'common/navigation';
import { onboardingMutation } from 'common/onboarding';

import { CsatSurveyQuery, CsatSurveyQueryVariables, DismissOnboardingMutation, DismissOnboardingMutationArgs } from '../../schema/schema-types';
import { csatEvalCohortFlagKey, csatEvalCohortOnboardingKey } from '../feature-flags/flag-keys';
import csatSurveyQuery from './csat-survey.query.graphql';

export interface QueryDerivedProps {
  aaId?: string;
  siteId?: string;
  isCsatSurveyDismissed: boolean;
  isCsatSurveyEnabled: boolean;
  isCsatSurveyLoading?: boolean;
}

interface MutationDerivedProps {
  dismissCsat(): void;
}

const messages = defineMessages({
  close: {
    id: 'chrome.csat.close.alt.text',
    defaultMessage: 'Close',
    description: 'The alternative text that is specified on the button which closes the focused task (also known as modal dialog)',
  },
  surveyTitle: {
    id: 'chrome.csat.message.title',
    defaultMessage: 'Hi, I\'m Kevin!',
  },
  surveyDescription: {
    id: 'chrome.csat.message.description',
    defaultMessage: 'I\'m a developer on the admin team. I\'d love to hear your thoughts on Site Admin.',
  },
  surveyAction: {
    id: 'chrome.csat.action',
    defaultMessage: 'Answer 2 short questions',
  },
});

type CsatSurveyDerivedProps = QueryDerivedProps & MutationDerivedProps & InjectedIntlProps & AnalyticsClientProps;

export class CsatSurveyImpl extends React.Component<CsatSurveyDerivedProps & RouteComponentProps<{ cloudId }>> {

  public render() {
    const { aaId, isCsatSurveyEnabled, isCsatSurveyDismissed, intl: { formatMessage } } = this.props;
    if (!isCsatSurveyEnabled || isCsatSurveyDismissed) {
      return null;
    }

    return (
      <Flag
        id={csatEvalCohortFlagKey}
        title={formatMessage(messages.surveyTitle)}
        description={formatMessage(messages.surveyDescription)}
        icon={<AkAvatar src={csatImage} size="small" />}
        onDismissed={this.onCancel}
        actions={[{
          content: (
            <ExternalLink
              href={`${links.external.csatSurvey}?UUID=${aaId}`}
              hideReferrer={false}
            >
              {formatMessage(messages.surveyAction)}
            </ExternalLink>
          ),
          onClick: this.onSurveyLinkClick,
        }]}
      />
    );
  }

  private onCancel = () => {
    this.props.dismissCsat();
    this.props.analyticsClient.sendUIEvent({
      cloudId: this.props.siteId,
      data: csatSurveyDismissedEventData(),
    });
  }

  private onSurveyLinkClick = () => {
    this.props.dismissCsat();
    this.props.analyticsClient.sendUIEvent({
      cloudId: this.props.siteId,
      data: csatSurveyLinkEventData(),
    });
  };
}

export const withCsatSurveyQuery = graphql<RouteComponentProps<{ cloudId }>, CsatSurveyQuery, CsatSurveyQueryVariables, QueryDerivedProps>(
  csatSurveyQuery,
  {
    props: ({ data }) => ({
      aaId: data && data.currentUser && data.currentUser.id || undefined,
      siteId: data && data.site && data.site.id,
      isCsatSurveyEnabled: !!(data && data.site && data.site.flag && data.site.flag.value),
      isCsatSurveyDismissed: !!(data && data.onboarding && data.onboarding.dismissed),
      isCsatSurveyLoading: !!(data && data.loading),
    }),
    options: ({ match: { params: { cloudId } } }) => ({
      variables: {
        cloudId,
        onboardingKey: csatEvalCohortOnboardingKey,
        flagKey: csatEvalCohortFlagKey,
      },
    }),
    skip: ({ match: { params: { cloudId } } }) => !cloudId,
  },
);

const withDismissCsatMutation = graphql<QueryDerivedProps & RouteComponentProps<{ cloudId }>, DismissOnboardingMutation, DismissOnboardingMutationArgs, MutationDerivedProps>(
  onboardingMutation,
  {
    props: ({ mutate }) => ({
      dismissCsat: async () => mutate instanceof Function && mutate({}),
    }),
    options: () => ({
      variables: {
        id: csatEvalCohortOnboardingKey,
      },
    }),
  },
);

export const CsatSurvey =
  withRouter<{}>(
    withCsatSurveyQuery(
      withDismissCsatMutation(
        withAnalyticsClient(
          injectIntl(CsatSurveyImpl),
        ),
      ),
    ),
  );
