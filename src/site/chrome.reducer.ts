import { combineReducers } from 'redux';

import { onboardingReducer, OnboardingState } from 'common/onboarding/onboarding.reducer';

import { DrawerLinkState, navigationDrawerLinksReducer } from './navigation/navigation-drawer-links.reducer';
import { navigationDrawers as navigationDrawersReducer, NavigationDrawersState } from './navigation/navigation-drawers.reducer';
import { navigationSections as navigationSectionsReducer, NavigationState } from './navigation/navigation-sections.reducer';

export interface ChromeState {
  readonly drawerLinks: DrawerLinkState;
  readonly navigationDrawers: NavigationDrawersState;
  readonly navigationSections: NavigationState;
  readonly onboarding: OnboardingState;
}

export const chromeReducer = combineReducers({
  navigationDrawers: navigationDrawersReducer,
  navigationSections: navigationSectionsReducer,
  drawerLinks: navigationDrawerLinksReducer,
  onboarding: onboardingReducer,
});
