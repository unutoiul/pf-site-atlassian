import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { sandbox as sinonSandbox, SinonSandbox, SinonStub } from 'sinon';

import { AkXFlow, InputProps, XFlowDialog } from './xflow-dialog';
import { xflowAnalyticsClient } from './xflow.analytics';

const XFlowDialogWithoutApollo = XFlowDialog.WrappedComponent;

describe('XFlowDialog', () => {
  let sandbox: SinonSandbox;
  let mockCloseDialog: SinonStub;

  interface RelevantProps extends InputProps {
    data: {
      currentSite: {
        id: string;
      };
    };
  }

  let mockProps: RelevantProps;

  beforeEach(() => {
    sandbox = sinonSandbox.create();

    mockCloseDialog = sandbox.stub();

    mockProps = {
      targetProduct: 'mockTargetProduct',
      sourceComponent: 'mockSourceComponent',
      closeDialog: mockCloseDialog,
      isDialogOpen: false,
      data: {
        currentSite: {
          id: 'mockCloudId',
        },
      },
    };
  });

  afterEach(() => {
    sandbox.restore();
  });

  const createShallowWrapper = (props: RelevantProps) => {
    return shallow(<XFlowDialogWithoutApollo {...props} />);
  };

  describe('component rendered', () => {
    it('should not show XFlow when isDialogOpen is false', () => {
      const wrapper = createShallowWrapper({ ...mockProps, isDialogOpen: false });

      const akXFlow = wrapper.find(AkXFlow);
      expect(akXFlow.exists()).to.equal(false);
    });

    it('should show XFlow when isDialogOpen is true', () => {
      const wrapper = createShallowWrapper({ ...mockProps, isDialogOpen: true });

      const akXFlow = wrapper.find(AkXFlow);
      expect(akXFlow.exists()).to.equal(true);
      expect(akXFlow.prop<string>('sourceComponent')).to.equal(mockProps.sourceComponent);
      expect(akXFlow.prop<string>('sourceContext')).to.equal('site-administration');
      expect(akXFlow.prop<string>('targetProduct')).to.equal(mockProps.targetProduct);
      expect(akXFlow.prop<string>('isCrossSell')).to.equal(true);
      expect(akXFlow.prop<() => void>('onComplete')).to.equal(mockCloseDialog);
    });

    it('should fire XFlow events via handler when XFlow events are triggered', () => {
      const mockEventName = 'eventName';
      const mockEventProps = { product: 'confluence' };

      const stub = sandbox.stub(xflowAnalyticsClient, 'triggerXFlowAnalyticsEvent');

      const wrapper = createShallowWrapper({ ...mockProps, isDialogOpen: true });

      const akXFlow = wrapper.find(AkXFlow);
      const onAnalyticsEvent = akXFlow.prop<(name: string, props: object) => void>('onAnalyticsEvent');

      onAnalyticsEvent(mockEventName, mockEventProps);
      expect(stub.calledOnce).to.equal(true);
      expect(stub.calledWith(mockEventName, mockProps.data.currentSite.id, mockEventProps));
    });

    it('should call closeDialog when XFlow calls onComplete', () => {
      const wrapper = createShallowWrapper({ ...mockProps, isDialogOpen: true });

      const akXFlow = wrapper.find(AkXFlow);
      const onComplete = akXFlow.prop<() => void>('onComplete');

      onComplete();

      expect(mockCloseDialog.calledOnce).to.equal(true);
      expect(mockCloseDialog.calledWith());
    });
  });
});
