import analytics from '@atlassiansox/analytics';

interface GasClient {
  options: {
    cloud_id: string,
  };
  trigger(name: string, properties: object): void;
}

const buildGasClient = (): GasClient => {
  if (__NODE_ENV__ === 'test') {
    return {
      options: {
        cloud_id: '',
      },
      trigger: () => null,
    };
  } else {
    return analytics.getGasClient({
      devMode: __NODE_ENV__ !== 'production',
      batchIntervalMs: 5000,
      server: window.location.hostname,
      product: 'crowd',
      user: '-',
    });
  }
};

export class XflowAnalytics {
  private analyticsClient: GasClient;

  constructor() {
    this.analyticsClient = buildGasClient();
  }

  public triggerXFlowAnalyticsEvent = (name, cloudId, attributes = {}) => {
    this.analyticsClient.options.cloud_id = cloudId;
    this.analyticsClient.trigger(name, attributes);
  };
}

export const xflowAnalyticsClient = new XflowAnalytics();
