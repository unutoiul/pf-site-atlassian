import * as React from 'react';
import { ChildProps, compose, graphql } from 'react-apollo';

import { dangerouslyCreateSafeString } from '@atlassiansox/analytics';

import { PickLoader } from 'common/loading/pick-loader';

import closeXFlowDialogMutation from './close-x-flow-dialog.mutation.graphql';
import xFlowDialogQuery from './xflow-dialog.query.graphql';
import xFlowQuery from './xflow.query.graphql';

import { XFlowDialogQuery, XFlowQuery } from '../../schema/schema-types';
import { xflowAnalyticsClient } from './xflow.analytics';

export interface InputProps {
  targetProduct: string;
  isDialogOpen: boolean;
  sourceComponent: string;
  closeDialog(): void;
}

export const AkXFlow = PickLoader(
  async () => import(
    /* webpackChunkName: "xflow" */
    '@atlassiansox/xflow-ui',
  ),
  (xflow) => xflow.XFlow,
);

export type XFlowDialogProps = ChildProps<InputProps, XFlowQuery>;

export class XFlowDialogImpl extends React.Component<XFlowDialogProps> {
  public render() {
    const { isDialogOpen, sourceComponent, closeDialog, targetProduct } = this.props;

    if (!isDialogOpen) {
      return null;
    }

    return (
      <AkXFlow
        sourceComponent={sourceComponent}
        sourceContext="site-administration"
        targetProduct={targetProduct}
        onComplete={closeDialog}
        onAnalyticsEvent={this.onAnalyticsEvent}
        isCrossSell={true}
      />
    );
  }

  private onAnalyticsEvent = (name, attributes) => {
    const {
      data: {
        currentSite: {
          id: cloudId = null,
        } = {},
      } = {},
    } = this.props;

    const safeProps = {};

    for (const key in attributes) {
      if (attributes.hasOwnProperty(key)) {
        const value = attributes[key];
        safeProps[key] = (typeof value === 'string') ? dangerouslyCreateSafeString(value) : value;
      }
    }
    xflowAnalyticsClient.triggerXFlowAnalyticsEvent(`grow0.${name}`, cloudId, safeProps);
  }
}

interface WithXFlowStateProps {
  isDialogOpen: boolean;
  sourceComponent: string | null;
  targetProduct: string | null;
}
const withXFlowState = graphql<InputProps, XFlowDialogQuery, {}, WithXFlowStateProps>(
  xFlowDialogQuery,
  {
    props: ({
      data: {
        ui: {
          xflow: {
            isDialogOpen = false,
            sourceComponent = null,
            targetProduct = null,
          } = {},
        } = {},
      } = {},
    }) => ({ isDialogOpen, sourceComponent, targetProduct }),
  },
);

const updateXFlowMutation = graphql<InputProps, {}, {}, any>(
  closeXFlowDialogMutation,
  {
    props: ({ mutate }) => ({
      closeDialog: async () => mutate instanceof Function && mutate({ }),
    }),
  },
);

export const XFlowDialog = compose(
  withXFlowState,
  updateXFlowMutation,
  graphql<XFlowQuery>(xFlowQuery),
)(XFlowDialogImpl);
