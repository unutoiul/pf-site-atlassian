import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import { RouteComponentProps } from 'react-router';
import { Redirect } from 'react-router-dom';

import { UserDetailRedirectQuery } from '../../schema/schema-types';
import userDetailRedirectQuery from './user-detail-redirect.query.graphql';

interface OwnProps {
  cloudId: string;
  identifier?: string;
}

type UserDetailRedirectPageProps = ChildProps<OwnProps, UserDetailRedirectQuery>;

export class UserDetailRedirectImpl extends React.Component<UserDetailRedirectPageProps & RouteComponentProps<any>> {
  public render() {
    if (!this.props.data || this.props.data.loading) {
      return null;
    }

    const overviewRedirect = <Redirect to={`/admin/s/${this.props.cloudId}/users`} />;

    if (this.props.data.error || !this.props.data.siteUsersList) {
      return overviewRedirect;
    }

    const firstUser = this.props.data.siteUsersList.users[0];

    if (this.props.data.siteUsersList.total === 0 || !firstUser) {
      return overviewRedirect;
    }

    if (this.props.data.siteUsersList.total > 1) {
      return <Redirect to={`/admin/s/${this.props.cloudId}/users?displayName=${this.props.identifier}`} />;
    }

    const userId = firstUser.id;

    return <Redirect to={`/admin/s/${this.props.cloudId}/users/${userId}`} />;
  }
}

export const UserDetailRedirect =
  graphql<UserDetailRedirectPageProps, UserDetailRedirectQuery>(userDetailRedirectQuery, {
    options: ({ cloudId, identifier }) => {
      return {
        variables: {
          cloudId,
          identifier,
        },
      };
    },
  })(
    UserDetailRedirectImpl,
  );
