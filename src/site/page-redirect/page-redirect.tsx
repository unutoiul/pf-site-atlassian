import { parse } from 'query-string';
import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import { RouteComponentProps, withRouter } from 'react-router';
import { Redirect } from 'react-router-dom';

import { analyticsClient } from 'common/analytics';
import { getConfig } from 'common/config';

import { PageRedirectQuery } from '../../schema/schema-types';
import { createUMAnalyticsData } from '../../site/user-management/user-management-analytics';
import { GroupDetailRedirect } from './group-detail-redirect';
import pageRedirectQuery from './page-redirect.query.graphql';
import { UserDetailRedirect } from './user-detail-redirect';

type RedirectProps = RouteComponentProps<{}> & ChildProps<null, PageRedirectQuery>;

export const OLD_ACCESS_CONFIG_ROUTE = '/admin/accessconfig';
export const OLD_ADMIN_ROUTE = '/admin';
export const OLD_APPS_ROUTE = '/admin/apps';
export const OLD_BILLING_ROUTE = '/admin/billing';
export const OLD_EMOJI_ROUTE = '/admin/emoji';
export const OLD_GROUPS_DETAILS_ROUTE = '/admin/groups/view';
export const OLD_GROUPS_OVERVIEW_ROUTE = '/admin/groups';
export const OLD_GSUITE_ROUTE = '/admin/gsync';
export const OLD_JSD_ROUTE = '/admin/jira-service-desk/portal-only-customers';
export const OLD_SECURITY_ROUTE = '/admin/security';
export const OLD_SIGNUP_ROUTE = '/admin/signup';
export const OLD_USERS_DETAILS_ROUTE = '/admin/users/view';
export const OLD_USERS_EXPORT_ROUTE = '/admin/users/export';
export const OLD_USERS_INVITE_ROUTE = '/admin/users/invite';
export const OLD_USERS_ROUTE = '/admin/users';

const stripAdminPrefix = (path: string): string => path.slice(7);
const matches = (paths, pathname = window.location.pathname) => paths.some(path => (
  typeof path === 'string' ?
    pathname === path :
    pathname.match(path)
));

class PageMigrationRedirectImpl extends React.Component<RedirectProps> {
  public render() {
    if (!this.props.data || this.props.data.loading || !this.props.data.currentSite) {
      return null;
    }

    const {
      selfSignupADG3Migration,
      accessConfigADG3Migration,
      groupsADG3Migration,
      gSuiteADG3Migration,
      usersADG3Migration,
      userExportADG3Migration,
      jsdADG3Migration,
      siteAdminPageMigrationRedirect,
    } = this.props.data.currentSite.flags;

    if (siteAdminPageMigrationRedirect) {
      this.siteAdminRedirect(this.props.data);

      return null;
    } else {

      const signup = matches([OLD_SIGNUP_ROUTE], this.props.location.pathname);
      const apps = matches([OLD_ACCESS_CONFIG_ROUTE, OLD_APPS_ROUTE], this.props.location.pathname);
      const users = matches([OLD_USERS_ROUTE, OLD_ADMIN_ROUTE], this.props.location.pathname);
      const billing = this.props.location.pathname.startsWith(OLD_BILLING_ROUTE);
      const gSuite = matches([OLD_GSUITE_ROUTE], this.props.location.pathname);
      const emoji = matches([OLD_EMOJI_ROUTE], this.props.location.pathname);
      const jsd = matches([OLD_JSD_ROUTE], this.props.location.pathname);
      const groupsOverview = matches([OLD_GROUPS_OVERVIEW_ROUTE], this.props.location.pathname);
      const groupsDetails = matches([OLD_GROUPS_DETAILS_ROUTE], this.props.location.pathname);
      const userInvite = matches([OLD_USERS_INVITE_ROUTE], this.props.location.pathname);
      const userExport = matches([OLD_USERS_EXPORT_ROUTE], this.props.location.pathname);
      const usersDetails = matches([OLD_USERS_DETAILS_ROUTE], this.props.location.pathname);

      const cloudId = this.props.data.currentSite.id;

      if (signup && selfSignupADG3Migration) {
        analyticsClient.eventHandler(createUMAnalyticsData({
          cloudId,
          subproduct: 'siteSettings',
          action: 'pageRedirect',
          actionSubject: 'siteSettingsPage',
          actionSubjectId: 'selfSignupPage',
        }));

        return <Redirect to={`/admin/s/${cloudId}/signup`} />;
      } else if (apps && accessConfigADG3Migration) {
        analyticsClient.eventHandler(createUMAnalyticsData({
          cloudId,
          subproduct: 'userManagement',
          action: 'pageRedirect',
          actionSubject: 'userManagementPage',
          actionSubjectId: 'accessConfigPage',
        }));

        return <Redirect to={`/admin/s/${cloudId}/apps`} />;
      } else if (billing) {
        analyticsClient.eventHandler(createUMAnalyticsData({
          cloudId,
          subproduct: 'billing',
          action: 'pageRedirect',
          actionSubject: 'billingPage',
          actionSubjectId: '',
        }));
        const billingSubPath = stripAdminPrefix(this.props.location.pathname);

        return <Redirect to={`/admin/s/${cloudId}/${billingSubPath}${this.props.location.search || ''}`} />;
      } else if (groupsOverview && groupsADG3Migration) {
        analyticsClient.eventHandler(createUMAnalyticsData({
          cloudId,
          subproduct: 'userManagement',
          action: 'pageRedirect',
          actionSubject: 'userManagementPage',
          actionSubjectId: 'groupsPage',
        }));

        return <Redirect to={`/admin/s/${cloudId}/groups`} />;
      } else if (groupsDetails && groupsADG3Migration) {
        analyticsClient.eventHandler(createUMAnalyticsData({
          cloudId,
          subproduct: 'userManagement',
          action: 'pageRedirect',
          actionSubject: 'userManagementPage',
          actionSubjectId: 'groupsPage',
        }));

        const groupName = parse(this.props.location.search).groupname;

        return <GroupDetailRedirect cloudId={cloudId} groupDisplayName={groupName} />;
      } else if (users && usersADG3Migration) {
        analyticsClient.eventHandler(createUMAnalyticsData({
          cloudId,
          subproduct: 'userManagement',
          action: 'pageRedirect',
          actionSubject: 'userManagementPage',
          actionSubjectId: 'usersPage',
        }));

        return <Redirect to={`/admin/s/${cloudId}/users`} />;
      } else if (jsd && jsdADG3Migration) {
        analyticsClient.eventHandler(createUMAnalyticsData({
          cloudId,
          subproduct: 'userManagement',
          action: 'pageRedirect',
          actionSubject: 'userManagementPage',
          actionSubjectId: 'jsdPage',
        }));

        return <Redirect to={`/admin/s/${cloudId}/jira-service-desk/portal-only-customers`} />;
      } else if (usersDetails && usersADG3Migration) {
        analyticsClient.eventHandler(createUMAnalyticsData({
          cloudId,
          subproduct: 'userManagement',
          action: 'pageRedirect',
          actionSubject: 'userManagementPage',
          actionSubjectId: 'usersPage',
        }));
        const param = parse(this.props.location.search);
        const identifier = param.username || param.email;

        return <UserDetailRedirect cloudId={cloudId} identifier={identifier} />;
      } else if (gSuite && gSuiteADG3Migration) {
        analyticsClient.eventHandler(createUMAnalyticsData({
          cloudId,
          subproduct: 'userManagement',
          action: 'pageRedirect',
          actionSubject: 'userManagementPage',
          actionSubjectId: 'gSuitePage',
        }));

        return <Redirect to={`/admin/s/${cloudId}/gsuite`} />;
      } else if (userInvite && usersADG3Migration) {
        analyticsClient.eventHandler(createUMAnalyticsData({
          cloudId,
          subproduct: 'userManagement',
          action: 'pageRedirect',
          actionSubject: 'userManagementPage',
          actionSubjectId: 'userInvitePage',
        }));

        return <Redirect to={`/admin/s/${cloudId}/users?invite=true`} />;
      } else if (userExport && userExportADG3Migration) {
        analyticsClient.eventHandler(createUMAnalyticsData({
          cloudId,
          subproduct: 'userManagement',
          action: 'pageRedirect',
          actionSubject: 'userManagementPage',
          actionSubjectId: 'userExportPage',
        }));

        return <Redirect to={`/admin/s/${cloudId}/users?export=true`} />;
      } else if (emoji) {
        analyticsClient.eventHandler(createUMAnalyticsData({
          cloudId,
          subproduct: 'siteSettings',
          action: 'pageRedirect',
          actionSubject: 'page',
          actionSubjectId: 'emoji',
        }));

        return <Redirect to={`/admin/s/${cloudId}/emoji`} />;
      }

      return null;

    }
  }

  public siteAdminRedirect(data) {
    const { pathname, search } = this.props.location;
    const cloudId = data.currentSite.id;

    const SITE_ADMIN_ACCESS_REQUESTS_ROUTE = /\/admin\/s\/\S+\/access-requests/g;
    const SITE_ADMIN_APPS_ROUTE = /\/admin\/s\/\S+\/apps\/?$/g;
    const SITE_ADMIN_BILLING_ROUTE = /\/admin\/s\/\S+\/billing\S*/g;
    const SITE_ADMIN_EMOJI_ROUTE = /\/admin\/s\/\S+\/emoji\/?$/g;
    const SITE_ADMIN_GROUPS_DETAIL_ROUTE = /\/admin\/s\/\S+\/groups\/\S+/g;
    const SITE_ADMIN_GROUPS_ROUTE = /\/admin\/s\/\S+\/groups\/?$/g;
    const SITE_ADMIN_GSUITE_ROUTE = /\/admin\/s\/\S+\/gsuite\/?$/g;
    const SITE_ADMIN_JSD_ROUTE = /\/admin\/s\/\S+\/jira-service-desk\/portal-only-customers\/?$/g;
    const SITE_ADMIN_SIGNUP_ROUTE = /\/admin\/s\/\S+\/signup\/?$/g;
    const SITE_ADMIN_SITE_URL_ROUTE = /\/admin\/s\/\S+\/site-url/g;
    const SITE_ADMIN_STRIDE_DETAIL_ROUTE = /\/admin\/s\/\S+\/stride\/\S+/g;
    const SITE_ADMIN_STRIDE_ROUTE = /\/admin\/s\/\S+\/stride/g;
    const SITE_ADMIN_USER_CONNECTED_APPS_DETAIL_ROUTE = /\/admin\/s\/\S+\/user-connected-apps\/\S+/g;
    const SITE_ADMIN_USER_CONNECTED_APPS_ROUTE = /\/admin\/s\/\S+\/user-connected-apps/g;
    const SITE_ADMIN_USER_DETAIL_ROUTE = /\/admin\/s\/\S+\/users\/\S+/g;
    const SITE_ADMIN_USERS_ROUTE = /\/admin\/s\/\S+\/users\/?$/g;

    const apps = matches([OLD_ACCESS_CONFIG_ROUTE, OLD_APPS_ROUTE], pathname);
    const billing = pathname.startsWith(OLD_BILLING_ROUTE);
    const emoji = matches([OLD_EMOJI_ROUTE], pathname);
    const groups = matches([OLD_GROUPS_DETAILS_ROUTE, OLD_GROUPS_OVERVIEW_ROUTE], pathname);
    const gSuite = matches([OLD_GSUITE_ROUTE], pathname);
    const jsd = matches([OLD_JSD_ROUTE], pathname);
    const security = matches([OLD_SECURITY_ROUTE], pathname);
    const signup = matches([OLD_SIGNUP_ROUTE], pathname);
    const userExport = matches([OLD_USERS_EXPORT_ROUTE], pathname);
    const userInvite = matches([OLD_USERS_INVITE_ROUTE], pathname);
    const users = matches([OLD_USERS_ROUTE, OLD_ADMIN_ROUTE], pathname);
    const usersDetails = matches([OLD_USERS_DETAILS_ROUTE], pathname);
    const whiteListedSiteAdminRoutes = matches([
      SITE_ADMIN_ACCESS_REQUESTS_ROUTE,
      SITE_ADMIN_APPS_ROUTE,
      SITE_ADMIN_BILLING_ROUTE,
      SITE_ADMIN_EMOJI_ROUTE,
      SITE_ADMIN_GROUPS_DETAIL_ROUTE,
      SITE_ADMIN_GROUPS_ROUTE,
      SITE_ADMIN_GSUITE_ROUTE,
      SITE_ADMIN_JSD_ROUTE,
      SITE_ADMIN_SIGNUP_ROUTE,
      SITE_ADMIN_SITE_URL_ROUTE,
      SITE_ADMIN_STRIDE_DETAIL_ROUTE,
      SITE_ADMIN_STRIDE_ROUTE,
      SITE_ADMIN_USER_CONNECTED_APPS_DETAIL_ROUTE,
      SITE_ADMIN_USER_CONNECTED_APPS_ROUTE,
      SITE_ADMIN_USER_DETAIL_ROUTE,
      SITE_ADMIN_USERS_ROUTE,
    ], pathname,
    );

    const baseSiteAdminRoute = `${getConfig().goToAdminLinkUrl}/s/`;

    if (users) {
      window.location.replace(`${baseSiteAdminRoute}${cloudId}/users${search || ''}`);
    } else if (apps) {
      window.location.replace(`${baseSiteAdminRoute}${cloudId}/apps${search || ''}`);
    } else if (userInvite) {
      window.location.replace(`${baseSiteAdminRoute}${cloudId}/users?invite=true`);
    } else if (security) {
      window.location.replace(`${getConfig().goToAdminLinkUrl}/atlassian-access`);
    } else if (userExport) {
      window.location.replace(`${baseSiteAdminRoute}${cloudId}/users?export=true`);
    } else if (usersDetails) {
      window.location.replace(`${baseSiteAdminRoute}${cloudId}/users${search || ''}`);
    } else if (billing) {
      window.location.replace(`${baseSiteAdminRoute}${cloudId}/${stripAdminPrefix(pathname)}${search || ''}`);
    } else if (emoji) {
      window.location.replace(`${baseSiteAdminRoute}${cloudId}/emoji`);
    } else if (gSuite) {
      window.location.replace(`${baseSiteAdminRoute}${cloudId}/gsuite`);
    } else if (jsd) {
      window.location.replace(`${baseSiteAdminRoute}${cloudId}/jira-service-desk/portal-only-customers`);
    } else if (signup) {
      window.location.replace(`${baseSiteAdminRoute}${cloudId}/signup`);
    } else if (groups) {
      window.location.replace(`${baseSiteAdminRoute}${cloudId}/groups${search || ''}`);
    } else if (whiteListedSiteAdminRoutes) {
      window.location.replace(`${getConfig().goToAdminLinkUrl}/${stripAdminPrefix(pathname)}${search || ''}`);
    } else {
      analyticsClient.onError(new Error('Route not found for page redirect'));
    }

    return null;
  }
}

const siteFeatureFlags = graphql<{}, PageRedirectQuery>(
  pageRedirectQuery,
);

export const PageMigrationRedirect = (
  siteFeatureFlags(
    withRouter(
      PageMigrationRedirectImpl,
    ),
  )
);
