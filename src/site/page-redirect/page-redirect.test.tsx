import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { Redirect } from 'react-router-dom';
import { sandbox as sinonSandbox, SinonSpy } from 'sinon';

import { analyticsClient } from 'common/analytics';
import { getConfig } from 'common/config';

import { createMockIntlContext } from '../../utilities/testing';
import { PageMigrationRedirect } from './page-redirect';

describe('PageRedirect', () => {
  let sandbox;
  let eventHandlerSpy: SinonSpy;
  const cloudId = 'dummy-cloud-id';
  let location = '';
  const setLocation = (replacementLocation) => location = replacementLocation;

  beforeEach(() => {
    sandbox = sinonSandbox.create();
    location = '';
    sandbox.stub(window.location, 'replace').callsFake(setLocation);
    eventHandlerSpy = sandbox.spy(analyticsClient, 'onEvent');
  });

  afterEach(() => {
    sandbox.restore();
  });

  function createTestComponent(): ShallowWrapper {
    const PageMigrationRedirectWithoutApollo = (PageMigrationRedirect as any).WrappedComponent;

    return shallow(
      <PageMigrationRedirectWithoutApollo data={{ loading: true }} />,
      createMockIntlContext(),
    );
  }

  function generatePath(path: string, search?: string) {
    return {
      location: {
        pathname: path,
        search,
      },
    };
  }

  function generateFeatureFlagData({ signupFFEnabled = false, appsFFEnabled = false, groupsFFEnabled = false, usersFFEnabled = false, userExportFFEnabled = false, jsdFFEnabled = false, pageMigrationFFEnabled = false } = {}) {
    return {
      data: {
        loading: false,
        currentSite: {
          id: cloudId,
          flags: {
            selfSignupADG3Migration: signupFFEnabled,
            accessConfigADG3Migration: appsFFEnabled,
            groupsADG3Migration: groupsFFEnabled,
            usersADG3Migration: usersFFEnabled,
            userExportADG3Migration: userExportFFEnabled,
            jsdADG3Migration: jsdFFEnabled,
            siteAdminPageMigrationRedirect: pageMigrationFFEnabled,
          },
        },
      },
    };
  }
  describe('WithoutSiteAdminPageMigration', () => {
    it('shouldn\'t redirect when data is loading', () => {
      const wrapper = createTestComponent();
      expect(wrapper.find(Redirect).length).to.equal(0);
      expect(location).to.equal('');
    });

    it('should redirect for /admin/apps when the feature flag is enabled', () => {
      const wrapper = createTestComponent();
      expect(wrapper.find(Redirect).length).to.equal(0);

      wrapper.setProps({ ...generatePath('/admin/apps'), ...generateFeatureFlagData({ appsFFEnabled: true }) });
      expect(wrapper.find(Redirect).first().props().to).to.equal(`/admin/s/${cloudId}/apps`);
    });

    it('should redirect for /admin/accessconfig when the feature flag is enabled', () => {
      const wrapper = createTestComponent();
      expect(wrapper.find(Redirect).length).to.equal(0);

      wrapper.setProps({ ...generatePath('/admin/accessconfig'), ...generateFeatureFlagData({ appsFFEnabled: true }) });
      expect(wrapper.find(Redirect).first().props().to).to.equal(`/admin/s/${cloudId}/apps`);
    });

    it('should not redirect for /admin/apps when the feature flag is disabled', () => {
      const wrapper = createTestComponent();
      expect(wrapper.find(Redirect).length).to.equal(0);

      wrapper.setProps({ ...generatePath('/admin/apps'), ...generateFeatureFlagData({ appsFFEnabled: false }) });
      expect(wrapper.find(Redirect).length).to.equal(0);
    });

    it('should not redirect for /admin/accessconfig when the feature flag is disabled', () => {
      const wrapper = createTestComponent();
      expect(wrapper.find(Redirect).length).to.equal(0);

      wrapper.setProps({ ...generatePath('/admin/accessconfig'), ...generateFeatureFlagData({ appsFFEnabled: false }) });
      expect(wrapper.find(Redirect).length).to.equal(0);
    });

    it('should redirect for /admin/signup when the feature flag is enabled', () => {
      const wrapper = createTestComponent();
      expect(wrapper.find(Redirect).length).to.equal(0);

      wrapper.setProps({ ...generatePath('/admin/signup'), ...generateFeatureFlagData({ signupFFEnabled: true }) });
      expect(wrapper.find(Redirect).first().props().to).to.equal(`/admin/s/${cloudId}/signup`);
    });

    it('should redirect for /admin/billing when the feature flag is enabled', () => {
      const wrapper = createTestComponent();
      expect(wrapper.find(Redirect).length).to.equal(0);

      wrapper.setProps({ ...generatePath('/admin/billing'), ...generateFeatureFlagData() });
      expect(wrapper.find(Redirect).first().props().to).to.equal(`/admin/s/${cloudId}/billing`);
    });

    it('should redirect for /admin/billing?search when the feature flag is enabled', () => {
      const wrapper = createTestComponent();
      expect(wrapper.find(Redirect).length).to.equal(0);

      wrapper.setProps({ ...generatePath('/admin/billing', '?search'), ...generateFeatureFlagData() });
      expect(wrapper.find(Redirect).first().props().to).to.equal(`/admin/s/${cloudId}/billing?search`);
    });

    it('should redirect for /admin/billing/overview when the feature flag is enabled', () => {
      const wrapper = createTestComponent();
      expect(wrapper.find(Redirect).length).to.equal(0);

      wrapper.setProps({ ...generatePath('/admin/billing/overview'), ...generateFeatureFlagData() });
      expect(wrapper.find(Redirect).first().props().to).to.equal(`/admin/s/${cloudId}/billing/overview`);
    });

    it('shouldn\'t redirect for paths other than /admin/signup or /admin/apps even when the feature flag is enabled', () => {
      const wrapper = createTestComponent();
      expect(wrapper.find(Redirect).length).to.equal(0);

      wrapper.setProps({ ...generatePath('/admin/users'), ...generateFeatureFlagData({ signupFFEnabled: true, appsFFEnabled: true }) });
      expect(wrapper.find(Redirect).length).to.equal(0);
    });

    it('should redirect for /admin/groups when the feature flag is enabled', () => {
      const wrapper = createTestComponent();
      expect(wrapper.find(Redirect).length).to.equal(0);

      wrapper.setProps({ ...generatePath('/admin/groups'), ...generateFeatureFlagData({ groupsFFEnabled: true }) });
      expect(wrapper.find(Redirect).first().props().to).to.equal(`/admin/s/${cloudId}/groups`);
    });

    it('shouldn\'t redirect for /admin/users/export when the feature flag is enabled', () => {
      const wrapper = createTestComponent();
      expect(wrapper.find(Redirect).length).to.equal(0);

      wrapper.setProps({ ...generatePath('/admin/users/export'), ...generateFeatureFlagData({ usersFFEnabled: true }) });
      expect(wrapper.find(Redirect).length).to.equal(0);
    });

    it('should redirect for /admin/users when the feature flag is enabled', () => {
      const wrapper = createTestComponent();
      expect(wrapper.find(Redirect).length).to.equal(0);

      wrapper.setProps({ ...generatePath('/admin/users'), ...generateFeatureFlagData({ usersFFEnabled: true }) });
      expect(wrapper.find(Redirect).first().props().to).to.equal(`/admin/s/${cloudId}/users`);
    });

    it('should redirect for /admin/jira-service-desk/portal-only-customers when the feature flag is enabled', () => {
      const wrapper = createTestComponent();
      expect(wrapper.find(Redirect).length).to.equal(0);

      wrapper.setProps({ ...generatePath('/admin/jira-service-desk/portal-only-customers'), ...generateFeatureFlagData({ jsdFFEnabled: true }) });
      expect(wrapper.find(Redirect).first().props().to).to.equal(`/admin/s/${cloudId}/jira-service-desk/portal-only-customers`);
    });

    it('should redirect for /admin/users/invite when the feature flag is enabled', () => {
      const wrapper = createTestComponent();
      expect(wrapper.find(Redirect).length).to.equal(0);

      wrapper.setProps({ ...generatePath('/admin/users/invite'), ...generateFeatureFlagData({ usersFFEnabled: true }) });
      expect(wrapper.find(Redirect).first().props().to).to.equal(`/admin/s/${cloudId}/users?invite=true`);
    });

    it('should redirect for /admin/users/export when the feature flag is enabled', () => {
      const wrapper = createTestComponent();
      expect(wrapper.find(Redirect).length).to.equal(0);

      wrapper.setProps({ ...generatePath('/admin/users/export'), ...generateFeatureFlagData({ userExportFFEnabled: true }) });
      expect(wrapper.find(Redirect).first().props().to).to.equal(`/admin/s/${cloudId}/users?export=true`);
    });

    it('shouldn\'t redirect for /admin/apps when the feature flag is disabled', () => {
      const wrapper = createTestComponent();
      expect(wrapper.find(Redirect).length).to.equal(0);

      wrapper.setProps({ ...generatePath('/admin/apps'), ...generateFeatureFlagData() });
      expect(wrapper.find(Redirect).length).to.equal(0);
    });

    it('shouldn\'t redirect for /admin/signup when the feature flag is disabled', () => {
      const wrapper = createTestComponent();
      expect(wrapper.find(Redirect).length).to.equal(0);

      wrapper.setProps({ ...generatePath('/admin/signup'), ...generateFeatureFlagData() });
      expect(wrapper.find(Redirect).length).to.equal(0);
    });

    it('should send analytics on direct for /admin/signup', () => {
      const wrapper = createTestComponent();
      expect(wrapper.find(Redirect).length).to.equal(0);

      wrapper.setProps({ ...generatePath('/admin/signup'), ...generateFeatureFlagData({ signupFFEnabled: true }) });

      expect(eventHandlerSpy.called).to.equal(true);
      expect(eventHandlerSpy.firstCall.args[0]).to.deep.equal({
        subproduct: 'siteSettings',
        action: 'pageRedirect',
        actionSubject: 'siteSettingsPage',
        actionSubjectId: 'selfSignupPage',
        tenantId: cloudId,
        tenantType: 'cloudId',
      });
    });

    it('shouldn\'t redirect for /admin/groups when the feature flag is disabled', () => {
      const wrapper = createTestComponent();
      expect(wrapper.find(Redirect).length).to.equal(0);

      wrapper.setProps({ ...generatePath('/admin/groups'), ...generateFeatureFlagData() });
      expect(wrapper.find(Redirect).length).to.equal(0);
    });

    it('shouldn\'t redirect for /admin/jira-service-desk/portal-only-customers when the feature flag is disabled', () => {
      const wrapper = createTestComponent();
      expect(wrapper.find(Redirect).length).to.equal(0);

      wrapper.setProps({ ...generatePath('/admin/jira-service-desk/portal-only-customers'), ...generateFeatureFlagData() });
      expect(wrapper.find(Redirect).length).to.equal(0);
    });
  });
  describe('WithSiteAdminMigration', () => {
    describe('OldRoutes', () => {
      it('should redirect access config route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/accessconfig`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/apps`);
      });
      it('should redirect apps route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/apps`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/apps`);
      });
      it('should redirect base billing route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/billing`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/billing`);
      });
      it('should redirect billing add applications route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/billing/addapplication`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/billing/addapplication`);
      });
      it('should redirect billing estimate route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/billing/estimate`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/billing/estimate`);
      });
      it('should redirect billing history route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/billing/history`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/billing/history`);
      });
      it('should redirect billing overview route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/billing/overview`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/billing/overview`);
      });
      it('should redirect billing payment details route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/billing/paymentdetails`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/billing/paymentdetails`);
      });
      it('should redirect emoji route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/emoji`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/emoji`);
      });
      it('should redirect groups route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/groups`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/groups`);
      });
      it('should redirect gsuite route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/gsync`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/gsuite`);
      });
      it('should redirect jsd route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/jira-service-desk/portal-only-customers`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/jira-service-desk/portal-only-customers`);
      });
      it('should redirect security route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/security`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/atlassian-access`);
      });
      it('should redirect user export route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/users/export`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/users?export=true`);
      });
      it('should redirect user invite route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/users/invite`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/users?invite=true`);
      });
      it('should redirect users route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/users`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/users`);
      });
      it('should redirect user details route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/users/view`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/users`);
      });
    });
    describe('SiteAdminRoutes', () => {
      it('should redirect access requests route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/s/${cloudId}/access-requests`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/access-requests`);
      });
      it('should redirect apps route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/s/${cloudId}/apps`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/apps`);
      });
      it('should redirect base billing route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/s/${cloudId}/billing`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/billing`);
      });
      it('should redirect billing add applications route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/s/${cloudId}/billing/addapplication`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/billing/addapplication`);
      });
      it('should redirect billing applications route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/s/${cloudId}/billing/applications`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/billing/applications`);
      });
      it('should redirect billing estimate route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/s/${cloudId}/billing/estimate`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/billing/estimate`);
      });
      it('should redirect billing history route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/s/${cloudId}/billing/history`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/billing/history`);
      });
      it('should redirect billing overview route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/s/${cloudId}/billing/overview`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/billing/overview`);
      });
      it('should redirect billing payment details route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/s/${cloudId}/billing/paymentdetails`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/billing/paymentdetails`);
      });
      it('should redirect emoji route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/s/${cloudId}/emoji`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/emoji`);
      });
      it('should redirect groups detail route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/s/${cloudId}/groups/super-awesome-group-id`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/groups/super-awesome-group-id`);
      });
      it('should redirect groups route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/s/${cloudId}/groups`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/groups`);
      });
      it('should redirect gsuite route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/s/${cloudId}/gsuite`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/gsuite`);
      });
      it('should redirect jsd route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/s/${cloudId}/jira-service-desk/portal-only-customers`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/jira-service-desk/portal-only-customers`);
      });
      it('should redirect signup route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/s/${cloudId}/signup`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/signup`);
      });
      it('should redirect site url route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/s/${cloudId}/site-url`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/site-url`);
      });
      it('should redirect stride detail route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/s/${cloudId}/stride/stride-export-id`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/stride/stride-export-id`);
      });
      it('should redirect stride route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/s/${cloudId}/stride`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/stride`);
      });
      it('should redirect user connected apps detail route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/s/${cloudId}/user-connected-apps/my-app-id`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/user-connected-apps/my-app-id`);
      });
      it('should redirect user connected apps route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/s/${cloudId}/user-connected-apps`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/user-connected-apps`);
      });
      it('should redirect user detail route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/s/${cloudId}/users/59995:user-id`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/users/59995:user-id`);
      });
      it('should redirect users route', () => {
        const wrapper = createTestComponent();
        expect(location).to.equal('');

        wrapper.setProps({ ...generatePath(`/admin/s/${cloudId}/users`), ...generateFeatureFlagData({ pageMigrationFFEnabled: true }) });
        expect(location).to.equal(`${getConfig().goToAdminLinkUrl}/s/${cloudId}/users`);
      });
    });
  });
});
