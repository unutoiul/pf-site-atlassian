import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { Redirect } from 'react-router-dom';
import { sandbox as sinonSandbox } from 'sinon';

import { GroupDetailRedirect } from './group-detail-redirect';

describe('GroupDetailRedirect', () => {

  let sandbox;

  beforeEach(() => {
    sandbox = sinonSandbox.create();
  });

  afterEach(() => {
    sandbox.restore();
  });

  function createTestComponent(): ShallowWrapper {
    const GroupDetailRedirectWithoutApollo = (GroupDetailRedirect as any).WrappedComponent;

    return shallow(
      <GroupDetailRedirectWithoutApollo data={{ loading: true }} />,
    );
  }

  it('shouldn\'t redirect when data is loading', () => {
    const wrapper = createTestComponent();
    expect(wrapper.find(Redirect).length).to.equal(0);
  });

  it('shouldn\'t redirect when data is not present', () => {
    const GroupDetailRedirectWithoutApollo = (GroupDetailRedirect as any).WrappedComponent;
    const wrapper = shallow(<GroupDetailRedirectWithoutApollo />);
    expect(wrapper.find(Redirect).length).to.equal(0);
  });

  it('should redirect to the groups page with no matches', () => {
    const wrapper = createTestComponent();
    wrapper.setProps({
      cloudId: 'dummy-id',
      groupDisplayName: 'name',
      data: {
        loading: false,
        groupList: {
          groups: [],
        },
      },
    });
    expect(wrapper.find(Redirect).first().props().to).to.equal('/admin/s/dummy-id/groups');
  });

  it('should redirect to the groups page in the error case', () => {
    const wrapper = createTestComponent();
    wrapper.setProps({
      cloudId: 'dummy-id',
      groupDisplayName: 'name',
      data: {
        loading: false,
        error: true,
      },
    });
    expect(wrapper.find(Redirect).first().props().to).to.equal('/admin/s/dummy-id/groups');
  });

  it('should redirect to the first page with one match', () => {
    const wrapper = createTestComponent();
    wrapper.setProps({
      cloudId: 'dummy-id',
      groupDisplayName: 'name',
      data: {
        loading: false,
        groupList: {
          groups: [
            { id: 'dummy-group-id', name: 'name' },
          ],
        },
      },
    });
    expect(wrapper.find(Redirect).first().props().to).to.equal('/admin/s/dummy-id/groups/dummy-group-id');
  });

  it('should ignore case when redirecting', () => {
    const wrapper = createTestComponent();
    wrapper.setProps({
      cloudId: 'dummy-id',
      groupDisplayName: 'all-caps-group',
      data: {
        loading: false,
        groupList: {
          groups: [
            { id: 'all-caps-group-id', name: 'ALL-CAPS-GROUP' },
          ],
        },
      },
    });
    expect(wrapper.find(Redirect).first().props().to).to.equal('/admin/s/dummy-id/groups/all-caps-group-id');
  });

  it('should redirect to matching page with more than one match', () => {
    const wrapper = createTestComponent();
    wrapper.setProps({
      cloudId: 'dummy-id',
      groupDisplayName: 'name 1',
      data: {
        loading: false,
        groupList: {
          groups: [
            { id: 'dummy-group-id1', name: 'name 1' },
            { id: 'dummy-group-id2', name: 'name 2' },
          ],
        },
      },
    });
    expect(wrapper.find(Redirect).first().props().to).to.equal('/admin/s/dummy-id/groups/dummy-group-id1');
  });

});
