import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import { RouteComponentProps } from 'react-router';
import { Redirect } from 'react-router-dom';

import { GroupDetailRedirectQuery } from '../../schema/schema-types';
import groupDetailRedirectQuery from './group-detail-redirect.query.graphql';

interface OwnProps {
  cloudId: string;
  groupDisplayName?: string;
}

type GroupDetailRedirectPageProps = ChildProps<OwnProps, GroupDetailRedirectQuery>;

export class GroupDetailRedirectImpl extends React.Component<GroupDetailRedirectPageProps & RouteComponentProps<any>> {
  public render() {
    if (!this.props.data || this.props.data.loading) {
      return null;
    }

    if (this.props.data.error) {
      return <Redirect to={`/admin/s/${this.props.cloudId}/groups`} />;
    }

    if (!this.props.data.groupList || !this.props.data.groupList.groups) {
      return <Redirect to={`/admin/s/${this.props.cloudId}/groups`} />;
    }

    const matchingGroupIds = this.props.data.groupList.groups.filter(group => group.name.toLowerCase() === (this.props.groupDisplayName || '').toLowerCase());

    if (matchingGroupIds.length === 0) {
      return <Redirect to={`/admin/s/${this.props.cloudId}/groups`} />;
    }

    const groupId = matchingGroupIds[0].id;

    return <Redirect to={`/admin/s/${this.props.cloudId}/groups/${groupId}`} />;
  }
}

export const GroupDetailRedirect =
  graphql<GroupDetailRedirectPageProps, GroupDetailRedirectQuery>(groupDetailRedirectQuery, {
    options: ({ cloudId, groupDisplayName }) => {
      return {
        variables: {
          cloudId,
          displayName: groupDisplayName,
        },
      };
    },
  })(
    GroupDetailRedirectImpl,
  );
