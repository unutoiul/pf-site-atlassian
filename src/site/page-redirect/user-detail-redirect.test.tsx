import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { Redirect } from 'react-router-dom';
import { sandbox as sinonSandbox } from 'sinon';

import { UserDetailRedirect } from './user-detail-redirect';

describe('UserDetailRedirect', () => {

  let sandbox;
  sandbox = sinonSandbox.create();

  afterEach(() => {
    sandbox.restore();
  });

  function createTestComponent(): ShallowWrapper {
    const UserDetailRedirectWithoutApollo = (UserDetailRedirect as any).WrappedComponent;

    return shallow(
      <UserDetailRedirectWithoutApollo data={{ loading: true }} />,
    );
  }

  it('shouldn\'t redirect when data is loading', () => {
    const wrapper = createTestComponent();
    expect(wrapper.find(Redirect).length).to.equal(0);
  });

  it('shouldn\'t redirect when data is not present', () => {
    const UserDetailRedirectWithoutApollo = (UserDetailRedirect as any).WrappedComponent;
    const wrapper = shallow(<UserDetailRedirectWithoutApollo />);
    expect(wrapper.find(Redirect).length).to.equal(0);
  });

  it('should redirect to the users page with no matches', () => {
    const wrapper = createTestComponent();
    wrapper.setProps({
      cloudId: 'dummy-id',
      identifier: 'name',
      data: {
        loading: false,
        siteUsersList: {
          total: 0,
          users: [],
        },
      },
    });
    expect(wrapper.find(Redirect).first().props().to).to.equal('/admin/s/dummy-id/users');
  });

  it('should redirect to the users page in the error case', () => {
    const wrapper = createTestComponent();
    wrapper.setProps({
      cloudId: 'dummy-id',
      identifier: 'name',
      data: {
        loading: false,
        error: true,
      },
    });
    expect(wrapper.find(Redirect).first().props().to).to.equal('/admin/s/dummy-id/users');
  });

  it('should redirect to the first user with one match', () => {
    const wrapper = createTestComponent();
    wrapper.setProps({
      cloudId: 'dummy-id',
      identifier: 'name',
      data: {
        loading: false,
        siteUsersList: {
          total: 1,
          users: [{ id: 'dummy-user-id' }],
        },
      },
    });
    expect(wrapper.find(Redirect).first().props().to).to.equal('/admin/s/dummy-id/users/dummy-user-id');
  });

  it('should redirect to the overview page with a displayName query for more than one match', () => {
    const wrapper = createTestComponent();
    wrapper.setProps({
      cloudId: 'dummy-id',
      identifier: 'name 1',
      data: {
        loading: false,
        siteUsersList: {
          total: 2,
          users: [{ id: 'dummy-user-id' }, { id: 'dummy-user-id2' }],
        },
      },
    });
    expect(wrapper.find(Redirect).first().props().to).to.equal('/admin/s/dummy-id/users?displayName=name 1');
  });

});
