import * as React from 'react';
import { graphql, OptionProps } from 'react-apollo';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import AkSpinner from '@atlaskit/spinner';

import { GenericError } from 'common/error';
import { PageLayout } from 'common/page-layout';

import { AdminBreadcrumb } from '../organization/breadcrumbs';

import { PageTemplateCurrentUserOrgsQuery } from '../schema/schema-types';
import pageTemplateCurrentUserOrgsQuery from './page-template-current-user-orgs.query.graphql';

const messages = defineMessages({
  title: {
    id: 'organization.example.title',
    defaultMessage: 'Example page',
  },
  description: {
    id: 'organization.example.description',
    defaultMessage: 'This is an example page template!',
  },
  userId: {
    id: 'organization.example.userId',
    defaultMessage: 'Your user ID:',
  },
});

interface CurrentUserOrgsProps {
  currentUser: PageTemplateCurrentUserOrgsQuery['currentUser'];
  currentUserError: boolean;
}
type CurrentUserOrgsPageImplProps = WithCurrentUserOrgsOutProps & InjectedIntlProps;

export class CurrentUserOrgsPageImpl extends React.Component<CurrentUserOrgsPageImplProps> {

  public render() {
    const { intl: { formatMessage } } = this.props;

    return (
      <PageLayout title={formatMessage(messages.title)} description={formatMessage(messages.description)} collapsedNav={false} breadcrumbs={<AdminBreadcrumb />}>
        {this.getContent()}
      </PageLayout>
    );
  }

  private getContent() {
    const { currentUser, currentUserError, intl: { formatMessage } } = this.props;

    // be sure to be defensive and add checks for undefined data, don't assume that no error means data is defined
    if (currentUserError) {
      return <GenericError />;
    }

    // Handle loading state either with explicit loading variable or by checking validity of loaded data
    if (!currentUser) {
      return <AkSpinner />;
    }

    return (
      <div>
        {/* be defensive and make sure any props you use here are checked for validity in the error or loading checks */}
        <div>{formatMessage(messages.userId)} {currentUser.id}</div>
        {currentUser.organizations.map(org => <div key={org.id}>{org.name}</div>)}
      </div>
    );
  }

}

type WithCurrentUserOrgsOutProps = CurrentUserOrgsProps;

const withCurrentUserOrgs = graphql<{}, PageTemplateCurrentUserOrgsQuery, {}, WithCurrentUserOrgsOutProps>(
  pageTemplateCurrentUserOrgsQuery, {
    props: (props: OptionProps<{}, PageTemplateCurrentUserOrgsQuery>): WithCurrentUserOrgsOutProps => ({
      ...props.ownProps,
      currentUser: getCurrentUser(props),
      currentUserError: !!(props.data && props.data.error),
    }),
  },
);

const getCurrentUser = (props: OptionProps<{}, PageTemplateCurrentUserOrgsQuery>): CurrentUserOrgsProps['currentUser'] => {
  if (!props.data || props.data.loading || props.data.error || !props.data.currentUser) {
    return null;
  }

  return props.data.currentUser;
};

export const CurrentUserOrgsPage =
  withCurrentUserOrgs(
    injectIntl(
      CurrentUserOrgsPageImpl,
    ),
  )
  ;
