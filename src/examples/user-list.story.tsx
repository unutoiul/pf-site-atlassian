// tslint:disable jsx-use-translation-function react-this-binding-issue max-classes-per-file prefer-function-over-method
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';
import styled from 'styled-components';

import { AnalyticsListener as AkAnalyticsListener } from '@atlaskit/analytics';
import { DynamicTableStateless as AkDynamicTableStateless, HeaderRow as AkHeaderRow, Row as AkRow } from '@atlaskit/dynamic-table';

import { Account, AccountLoading } from 'common/account';
import { Action, ActionProps, Actions } from 'common/actions';
import { OscillatingBlock } from 'common/loading';

import { random, randomBetween } from '../utilities/random';
import { getRandomUser, TestUserData } from '../utilities/testing';

const Container = styled.div`
  margin: 50px;
`;

const Box = styled.div`
  ${(props: { width?: string }) => props.width ? `width: ${props.width};` : ''}
`;

interface User extends TestUserData {
  actions: Array<ActionProps & { key: React.Key }>;
}

function getRandomActions(username: string): Array<ActionProps & { key: React.Key }> {
  const weightedIsDisabledValuesPool: boolean[] = Array(10).fill(true).concat(Array(90).fill(false));

  const action1: ActionProps & { key: React.Key } = {
    key: 'edit',
    actionType: 'edit',
    isDisabled: random(weightedIsDisabledValuesPool),
    disabledReason: { id: new Date().getTime().toString(), defaultMessage: 'Edit action is disabled because why not?' },
    onAction: action(`Editing user ${username}`),
  };
  const action2: ActionProps & { key: React.Key } = {
    key: 'remove',
    actionType: 'remove',
    isDisabled: random(weightedIsDisabledValuesPool),
    disabledReason: { id: new Date().getTime().toString(), defaultMessage: 'Remove action is disabled because why not?' },
    onAction: action(`Removing user ${username}`),
  };
  const action3: ActionProps & { key: React.Key } = {
    key: 'custom',
    actionType: 'custom',
    isDisabled: random(weightedIsDisabledValuesPool),
    disabledReason: { id: new Date().getTime().toString(), defaultMessage: 'Custom action is disabled because why not?' },
    customMessage: { id: `msg-id`, defaultMessage: 'Do thing' },
    onAction: action(`Doing thing with user ${username}`),
  };

  const actions: Array<ActionProps & { key: React.Key }> = [];
  if (random([true, false])) {
    actions.push(action1);
  }
  if (random([true, false])) {
    actions.push(action2);
  }
  if (random([true, false])) {
    actions.push(action3);
  }

  return actions.length > 0
    ? actions
    : [{
      key: 'view',
      actionType: 'custom',
      isDisabled: random(weightedIsDisabledValuesPool),
      disabledReason: { id: new Date().getTime().toString(), defaultMessage: 'View action is disabled because why not?' },
      customMessage: { id: `msg-view`, defaultMessage: 'View' },
      onAction: action(`Viewing with user ${username}`),
    }];
}

function getRandomUsers(count: number): User[] {
  const arr: null[] = Array.apply(null, Array(count));

  return arr.map(() => {
    const user = getRandomUser();

    return {
      ...user,
      actions: getRandomActions(user.username),
    };
  });
}

interface ExampleUsersListProps {
  users: User[];
  rowsPerPage: number;
  page: number;
  loadPage(page: number): void;
}

class ExampleUsersList extends React.Component<ExampleUsersListProps> {
  public render() {
    const {
      users,
      loadPage,
      page,
      rowsPerPage,
    } = this.props;

    return (
      <AkDynamicTableStateless
        head={this.getTableHeader()}
        isLoading={!users}
        rows={users && users.map(this.formatUser)}
        rowsPerPage={rowsPerPage}
        onSetPage={loadPage}
        page={page}
      />
    );
  }

  private getTableHeader(): AkHeaderRow {
    return {
      cells: [
        {
          key: 'user',
          content: 'User',
          style: {
            paddingLeft: '4px',
            width: '40%',
          },
        },
        {
          key: 'some-data',
          content: 'Some data',
          style: {
            width: '40%',
          },
        },
        {
          key: 'options',
          content: 'Options',
          style: {
            paddingLeft: '28px',
            minWidth: '100px',
          },
        },
      ],
    };
  }

  private formatUser = (user: User): AkRow => {
    if (!user) {
      return {
        cells: [
          {
            key: 'account',
            content: <div><AccountLoading /></div>,
          },
          {
            key: 'some-data',
            content: (
              <div>
                <OscillatingBlock width="50%" heightPx={16} />
                <OscillatingBlock width="80%" heightPx={16} />
              </div>
            ),
          },
          {
            key: 'actions',
            content: (
              <div style={{ maxWidth: '200px', paddingLeft: '20px', display: 'flex', justifyContent: 'space-between' }}>
                <span style={{ display: 'inline-block', width: '40%' }}>
                  <OscillatingBlock heightPx={32} />
                </span>
                <span style={{ display: 'inline-block', width: '20%' }}>
                  <OscillatingBlock heightPx={32} />
                </span>
              </div>
            ),
          },
        ],
      };
    }

    return {
      cells: [
        {
          key: 'account',
          content: (
            <Account
              id={user.id}
              email={user.email}
              displayName={user.displayName}
            />
          ),
        },
        {
          key: 'some-data',
          content: (
            <span>{user.displayName} has an email of {user.email}. That's all we know</span>
          ),
        },
        {
          key: 'actions',
          content: (
            <Actions>
              {user.actions.map(a => <Action {...a as any} key={a.key} />)}
            </Actions>
          ),
        },
      ],
    };
  }
}

storiesOf('Examples|Users List', module)
  .add('Usage pattern', () => {
    interface ExampleState {
      users: User[];
      page: number;
      total?: number;
    }

    interface UserPageData {
      users: User[];
      total: number;
      start: number;
    }

    class Example extends React.Component<{}, ExampleState> {
      private ROWS_PER_PAGE = 50;
      private currentLoadTimeout: number | undefined = undefined;

      public state: ExampleState = {
        users: [],
        page: 1,
      };

      public componentDidMount() {
        this.loadPage(1);
      }

      public componentWillUnmount() {
        clearTimeout(this.currentLoadTimeout!);
      }

      public render() {
        return (
          <IntlProvider locale="en">
            <AkAnalyticsListener onEvent={action('[Analytics]')}>
              <Container>
                <p>
                  There are various components that go into composing a list of users. You can see how it would look like below.
                  The users are loaded using an artificial two second delay.
                </p>
                <p>
                  <strong>Note</strong>, that in this example the <code>"page"</code> is stored in the React state.
                  In real world you would take that from URL parameters, as well as any filtering data.
                </p>
                <p>
                  The recommendation for page size for a list of users is <strong>50</strong>, and is based on the most common user base sizes.
                </p>
                <Box width="100%">
                  <ExampleUsersList
                    users={this.state.users}
                    loadPage={this.loadPage}
                    rowsPerPage={this.ROWS_PER_PAGE}
                    page={this.state.page}
                  />
                </Box>
              </Container>
            </AkAnalyticsListener>
          </IntlProvider>
        );
      }

      private loadPage = (page: number) => {
        this.setState({ page });
        this.loadPageWithTotalViaGraphQL(page).then(({ users, total, start }) => {
          const usersWithPlaceholders = Array(total).fill(null);
          usersWithPlaceholders.splice(start, users.length, ...users);

          this.setState({
            total,
            users: usersWithPlaceholders,
          });
        }).catch(() => null);
      }

      private async loadPageWithTotalViaGraphQL(page: number): Promise<UserPageData> {
        return new Promise<UserPageData>(resolve => {
          clearTimeout(this.currentLoadTimeout!);

          this.currentLoadTimeout = window.setTimeout(() => {
            const total = this.state.total || randomBetween(60, 120);

            const start = this.ROWS_PER_PAGE * (page - 1);
            const end = Math.min(this.ROWS_PER_PAGE * page - 1, total - 1);
            const count = end - start + 1;

            const loadedUsers = getRandomUsers(count);

            const x = {
              users: loadedUsers,
              total,
              start,
            };
            resolve(x);
          }, 2000);
        });
      }
    }

    return <Example />;
  });
