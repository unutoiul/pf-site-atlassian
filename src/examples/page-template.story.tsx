import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';
import { StaticRouter } from 'react-router-dom';

import AkPage from '@atlaskit/page';

import { createApolloClient } from '../apollo-client';
import { createMockIntlProp } from '../utilities/testing';
import pageTemplateCurrentUserOrgsQuery from './page-template-current-user-orgs.query.graphql';

import { CurrentUserOrgsPage, CurrentUserOrgsPageImpl } from './page-template-example';

const sharedProps = {
  intl: createMockIntlProp(),
  currentUserError: undefined,
  currentUser: undefined,
};

storiesOf('Examples|Example Page Template', module)
  .add('Default', () => {
    const client = createApolloClient();

    client.writeQuery({
      query: pageTemplateCurrentUserOrgsQuery,
      data: {
        currentUser: {
          id: '1234',
          // __typename is required for writeQuery to work correctly
          __typename: 'CurrentUser',
          organizations: [
            {
              id: 'org1234',
              name: 'Super Example Org',
              __typename: 'Organization',
            },
            {
              id: 'org1244',
              name: 'Not So Super Example Org',
              __typename: 'Organization',
            },
          ],
        },
      },
    });

    return (
      <ApolloProvider client={client}>
        <IntlProvider locale="en">
          <StaticRouter context={{}}>
            <AkPage>
              <CurrentUserOrgsPage />
            </AkPage>
          </StaticRouter>
        </IntlProvider>
      </ApolloProvider>
    );
  })
  .add('Loading', () => {
    const client = createApolloClient();

    return (
      <ApolloProvider client={client}>
        <IntlProvider locale="en">
          <StaticRouter context={{}}>
            <AkPage>
              <CurrentUserOrgsPageImpl {...sharedProps as any} />
            </AkPage>
          </StaticRouter>
        </IntlProvider>
      </ApolloProvider>
    );
  })
  .add('Error', () => {
    const client = createApolloClient();

    return (
      <ApolloProvider client={client}>
        <IntlProvider locale="en">
          <StaticRouter context={{}}>
            <AkPage>
              <CurrentUserOrgsPageImpl {...sharedProps as any} currentUserError={true} />
            </AkPage>
          </StaticRouter>
        </IntlProvider>
      </ApolloProvider>
    );
  })
;
