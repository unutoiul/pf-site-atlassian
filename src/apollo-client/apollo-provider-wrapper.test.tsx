import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import { GraphQLError } from 'graphql';
import * as React from 'react';
import { sandbox as sinonSandbox, SinonSandbox, SinonSpy } from 'sinon';

import { analyticsClient } from 'common/analytics';
import { AuthenticationError, ConflictError, createErrorIcon, RequestError } from 'common/error';

import { ApolloProviderWrapperImpl } from './apollo-provider-wrapper';

describe('ApolloProviderWrapper', () => {
  describe('handleErrors', () => {
    let sandbox: SinonSandbox;
    let onErrorSpy: SinonSpy;
    let showFlagSpy: SinonSpy;
    let app: ShallowWrapper;

    beforeEach(() => {
      sandbox = sinonSandbox.create();

      onErrorSpy = sandbox.spy(analyticsClient, 'onError');
      showFlagSpy = sandbox.spy();

      app = shallow(
        <ApolloProviderWrapperImpl showFlag={showFlagSpy} hideFlag={() => null}>
          <div />
        </ApolloProviderWrapperImpl>,
      );
    });

    afterEach(() => {
      sandbox.restore();
    });

    it('shows error flags with correct arguments', () => {
      const instance = (app.instance() as ApolloProviderWrapperImpl);

      const error = {
        id: 'id',
        title: 'title',
        description: 'description',
      };

      instance.handleErrors([{ originalError: error }]);

      expect(showFlagSpy.calledWith({
        autoDismiss: true,
        icon: createErrorIcon(),
        id: error.id,
        title: error.title,
        description: error.description,
      })).to.equal(true);
    });

    it('does not show error flags if error should be skipped', () => {
      const instance = (app.instance() as ApolloProviderWrapperImpl);

      const error = {
        id: 'id',
        title: 'title',
        description: 'description',
        legacySkipDefaultErrorFlag: true,
      };

      instance.handleErrors([{ originalError: error }]);

      expect(showFlagSpy.callCount).to.equal(0);
    });

    it('should submit non-nullable GraphQL errors to analytics client', () => {
      const instance = (app.instance() as ApolloProviderWrapperImpl);
      const errorMsg = 'Cannot return null for non-nullable field Group.id.';
      const error = new GraphQLError(
        errorMsg,
        undefined,
        undefined,
        undefined,
        ['groupList', 'groups', 6, 'id'],
        new Error(errorMsg),
      );

      instance.handleErrors([error]);

      expect(onErrorSpy.callCount).to.equal(1);

      const args = onErrorSpy.getCalls()[0].args;
      expect(args).to.have.length(1);
      expect(args[0] instanceof Error).to.equal(true);
      expect(args[0].message).to.equal(errorMsg);
    });

    it('should submit non-authentication errors to analytics client', () => {
      const instance = (app.instance() as ApolloProviderWrapperImpl);
      const errorMsg = 'test-error';
      const error = new GraphQLError(
        errorMsg,
        undefined,
        undefined,
        undefined,
        undefined,
        new Error(errorMsg),
      );

      instance.handleErrors([error]);

      expect(onErrorSpy.callCount).to.equal(1);

      const args = onErrorSpy.getCalls()[0].args;
      expect(args).to.have.length(1);
      expect(args[0] instanceof Error).to.equal(true);
      expect(args[0].message).to.equal('test-error');
    });

    it('should not submit authentication errors to analytics client', () => {
      const instance = (app.instance() as ApolloProviderWrapperImpl);
      const errorMsg = 'test-error';
      const error = new GraphQLError(
        errorMsg,
        undefined,
        undefined,
        undefined,
        undefined,
        new AuthenticationError({ message: errorMsg }),
      );

      instance.handleErrors([error]);

      expect(onErrorSpy.callCount).to.equal(0);
    });

    it('should not submit ignored request errors to analytics client', () => {
      const instance = (app.instance() as ApolloProviderWrapperImpl);
      const errorMsg = 'test-error';
      const error = new GraphQLError(
        errorMsg,
        undefined,
        undefined,
        undefined,
        undefined,
        new RequestError({ ignore: true }),
      );

      instance.handleErrors([error]);

      expect(onErrorSpy.callCount).to.equal(0);
    });

    it('should not submit ignored conflict errors to analytics client', () => {
      const instance = (app.instance() as ApolloProviderWrapperImpl);
      const errorMsg = 'test-error';
      const error = new GraphQLError(
        errorMsg,
        undefined,
        undefined,
        undefined,
        undefined,
        new ConflictError({ ignore: true }),
      );

      instance.handleErrors([error]);

      expect(onErrorSpy.callCount).to.equal(0);
    });
  });
});
