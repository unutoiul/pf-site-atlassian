import * as React from 'react';
import { ApolloProvider } from 'react-apollo';

import { analyticsClient } from 'common/analytics';
import {
  AuthenticationError,
  BadRequestError,
  ConflictError,
  createErrorIcon,
  GoneError,
  PreconditionFailedError,
  RequestError,
  ValidationError,
} from 'common/error';
import { FlagProps, withFlag } from 'common/flag';

import { createApolloClient, RedirectFunc } from './apollo-client';

interface Props {
  redirectToLogin?: RedirectFunc;
}

export class ApolloProviderWrapperImpl extends React.Component<Props & FlagProps, never> {
  private apolloClient = createApolloClient(this.handleErrors.bind(this));

  public render() {
    return (
      <ApolloProvider client={this.apolloClient}>
        {this.props.children}
      </ApolloProvider>
    );
  }

  public handleErrors(errors) {
    if (!errors) {
      return;
    }

    errors.forEach((graphQLError) => {
      const originalError = graphQLError.originalError || graphQLError;

      if (this.shouldLogError(originalError)) {
        analyticsClient.onError(originalError);
      }

      if (!originalError) {
        this.showFlag(graphQLError);

        return;
      }

      if (originalError.status === 401 && this.props.redirectToLogin) {
        this.props.redirectToLogin();
      }

      // Once @handleErrorsLegacy() is removed this can be removed,
      // since @handleErrors() never displays a flag automatically
      if (!originalError.legacySkipDefaultErrorFlag) {
        this.showFlag(originalError);
      }
    });
  }

  private shouldLogError = (e: any) => {
    if (e instanceof AuthenticationError) {
      return false;
    }
    if (e instanceof ValidationError) {
      return false;
    }
    if (e instanceof RequestError && e.ignore) {
      return false;
    }
    if (e instanceof GoneError) {
      return false;
    }
    if (e instanceof ConflictError && e.ignore) {
      return false;
    }
    if (e instanceof PreconditionFailedError) {
      return false;
    }
    if (e instanceof BadRequestError && e.ignore) {
      return false;
    }

    return true;
  };

  private showFlag(error) {
    this.props.showFlag({
      autoDismiss: true,
      icon: createErrorIcon(),
      id: error.id || 'default-error-flag',
      title: error.title || 'Something went wrong.',
      description: error.description || 'Please try again later.',
    });
  }
}

export const ApolloProviderWrapper = withFlag(ApolloProviderWrapperImpl);
