import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloClient } from 'apollo-client';
import { ApolloLink } from 'apollo-link';
import { onError } from 'apollo-link-error';
import { SchemaLink } from 'apollo-link-schema';
import { withClientState } from 'apollo-link-state';
import { GraphQLSchema } from 'graphql';

import { uiState } from '../schema/resolvers/ui';
import { schema as generatedSchema } from '../schema/schema';

export type RedirectFunc = () => void;

// This only handles graphQLErrors and not networkError
// read more about the distinction here: http://apollo-link-docs.netlify.com/docs/link/links/error.html#Options
const errorLink = (errorHandler) => {
  return onError(({ graphQLErrors }) => {
    if (errorHandler) {
      errorHandler(graphQLErrors);
    }
  });
};

const stateLink = (cache) => withClientState({ ...uiState, cache });

export function createApolloClient(errorHandler?: (error) => void, schema: GraphQLSchema = generatedSchema) {
  const cache: InMemoryCache = new InMemoryCache();

  return new ApolloClient({
    link: ApolloLink.from([
      errorLink(errorHandler),
      stateLink(cache),
      new SchemaLink({ schema }),
    ]),
    cache,
  });
}
