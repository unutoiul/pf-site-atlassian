import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import { SiteCard } from 'common/site-card';

import { DerivedProps, UnlinkedSitesImpl } from './unlinked-sites';
import { LaunchpadSitesFeatureFlagResults } from './with-launchpad-sites-feature-flag';

describe('UnlinkedSites', () => {

  const sitesMock = [{
    __typename: 'OrgSite',
    id: '1363002c-4b37-303f-b9e3-06734d54989a',
    siteUrl: 'pf-site-admin-ui-prod.atlassian.net',
    displayName: 'Product Fabric Site',
    avatar: 'https://site-admin-avatar-cdn.staging.public.atl-paas.net/avatars/240/jersey.png',
    products: [
      {
        __typename: 'Product',
        key: 'jira-core',
        name: 'Jira Core',
      },
    ],
  }];

  const shallowUnlinkedSites = ({
    isLoading = false,
    hasError = false,
    sites = sitesMock,
    launchpadSitesFeatureFlag = {
      isLoading: false,
      value: true,
    }}: Partial<LaunchpadSitesFeatureFlagResults & DerivedProps>) => {
    return shallow((
      <UnlinkedSitesImpl
        isLoading={isLoading}
        hasError={hasError}
        sites={sites}
        launchpadSitesFeatureFlag={launchpadSitesFeatureFlag}
      />
    ));
  };

  it('should render', () => {
    const wrapper = shallowUnlinkedSites({});
    const siteCards = wrapper.find(SiteCard);

    expect(siteCards.length).to.equal(1);
  });

  it('should not render with no sites', () => {
    const wrapper = shallowUnlinkedSites({ sites: [] });

    expect(wrapper.type()).to.equal(null);
  });

  it('should not render when loading', () => {
    const wrapper = shallowUnlinkedSites({ isLoading: true });

    expect(wrapper.type()).to.equal(null);
  });

  it('should not render when has error', () => {
    const wrapper = shallowUnlinkedSites({ hasError: true });

    expect(wrapper.type()).to.equal(null);
  });

  it('should not render when feature flag loading', () => {
    const wrapper = shallowUnlinkedSites({ launchpadSitesFeatureFlag: {
      isLoading: true,
      value: true,
    }});

    expect(wrapper.type()).to.equal(null);
  });

  it('should not render when feature flag disabled', () => {
    const wrapper = shallowUnlinkedSites({ launchpadSitesFeatureFlag: {
      isLoading: false,
      value: false,
    }});

    expect(wrapper.type()).to.equal(null);
  });

});
