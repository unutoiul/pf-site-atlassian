import { FeatureFlagProps, withUserFeatureFlag } from 'common/feature-flags';

const launchpadSitesFeatureFlag = 'admin.launchpad-with-sites';

export interface LaunchpadSitesFeatureFlagResults {
  launchpadSitesFeatureFlag: FeatureFlagProps;
}

export function withLaunchpadSitesFeatureFlag<TOwnProps>(Component: React.ComponentType<TOwnProps & LaunchpadSitesFeatureFlagResults>): React.ComponentClass<TOwnProps> {
  return withUserFeatureFlag<TOwnProps, LaunchpadSitesFeatureFlagResults>({
    flagKey: launchpadSitesFeatureFlag,
    defaultValue: false,
    name: 'launchpadSitesFeatureFlag',
  })(Component);
}
