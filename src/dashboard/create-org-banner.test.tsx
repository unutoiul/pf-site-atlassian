import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import { Banner } from 'common/banner';

import { CreateOrgBanner } from './create-org-banner';

describe('CreateOrgBanner', () => {
  it('should render a Banner', () => {
    const wrapper = shallow(
      <CreateOrgBanner />,
    );
    const banner = wrapper.find(Banner);

    expect(banner.exists()).to.equal(true);
    expect(banner.prop('message')).to.be.an('object');
    expect(banner.prop('image')).not.to.equal(null);
    expect(banner.prop('actions')).to.have.lengthOf(2);
  });
});
