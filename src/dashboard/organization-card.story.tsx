import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';
import { BrowserRouter as Router } from 'react-router-dom';

import { AnalyticsListener as AkAnalyticsListener } from '@atlaskit/analytics';

import { createStorybookAnalyticsClient } from '../utilities/storybooks';
import { OrganizationCardImpl } from './organization-card';

const OrganizationCardImplStory = (props: OrganizationCardImpl['props']) => {
  return (
    <AkAnalyticsListener onEvent={action('[Analytics]')}>
      <IntlProvider locale="en">
        <Router>
          <OrganizationCardImpl {...props} />
        </Router>
      </IntlProvider>
    </AkAnalyticsListener>
  );
};

storiesOf('Dashboard|Organization Card', module)
  .add('regular state', () => (
    <OrganizationCardImplStory
      id="test-org-id"
      name="Acme Inc"
      cardPosition={1}
      analyticsClient={createStorybookAnalyticsClient()}
      meta={{
        hasUnlinkedSites: false,
        hasVerifiedDomains: true,
        totalDomains: 3,
        totalMembers: 124,
        hasError: false,
        isLoading: false,
        sites: [],
        hasAtlassianAccess: false,
      }}
      launchpadSitesFeatureFlag={{ isLoading: false, value: false }}
    />
  ))
  .add('with sites', () => (
    <OrganizationCardImplStory
      id="test-org-id"
      name="Acme Inc"
      cardPosition={1}
      analyticsClient={createStorybookAnalyticsClient()}
      meta={{
        hasUnlinkedSites: true,
        hasVerifiedDomains: true,
        totalDomains: 3,
        totalMembers: 124,
        hasError: false,
        isLoading: false,
        sites: [
          {
            __typename: 'OrgSite',
            id: '1363002c-4b37-303f-b9e3-06734d54989a',
            siteUrl: 'pf-site-admin-ui-prod.atlassian.net',
            displayName: 'Product Fabric Site',
            avatar: 'https://site-admin-avatar-cdn.staging.public.atl-paas.net/avatars/240/jersey.png',
            products: [
              {
                __typename: 'Product',
                key: 'jira-core',
                name: 'Jira Core',
              },
            ],
          },
        ],
        hasAtlassianAccess: false,
      }}
      launchpadSitesFeatureFlag={{ isLoading: false, value: true }}
    />
  ))
  .add('with no verified domains', () => (
    <OrganizationCardImplStory
      id="test-org-id"
      name="Acme Inc"
      cardPosition={1}
      analyticsClient={createStorybookAnalyticsClient()}
      meta={{
        hasUnlinkedSites: false,
        hasVerifiedDomains: false,
        totalDomains: 3,
        totalMembers: 124,
        hasError: false,
        isLoading: false,
        sites: [],
        hasAtlassianAccess: false,
      }}
      launchpadSitesFeatureFlag={{ isLoading: false, value: false }}
    />
  ))
  .add('with long name', () => (
    <OrganizationCardImplStory
      id="test-org-id"
      name="Interesting Organization with super-duper long name. You never would have thought that a user would create such an organization, but here it is nevertheless, no matter what your expectations were. Did you handle this gracefully? I hope so, because if not, you are in a very big trouble, mate!"
      cardPosition={1}
      analyticsClient={createStorybookAnalyticsClient()}
      meta={{
        hasUnlinkedSites: false,
        hasVerifiedDomains: true,
        totalDomains: 3,
        totalMembers: 124,
        hasError: false,
        isLoading: false,
        sites: [],
        hasAtlassianAccess: false,
      }}
      launchpadSitesFeatureFlag={{ isLoading: false, value: false }}
    />
  ))
  .add('with long name and no verified domains', () => (
    <OrganizationCardImplStory
      id="test-org-id"
      name="Interesting Organization with super-duper long name. You never would have thought that a user would create such an organization, but here it is nevertheless, no matter what your expectations were. Did you handle this gracefully? I hope so, because if not, you are in a very big trouble, mate!"
      cardPosition={1}
      analyticsClient={createStorybookAnalyticsClient()}
      meta={{
        hasUnlinkedSites: false,
        hasVerifiedDomains: false,
        totalDomains: 3,
        totalMembers: 124,
        hasError: false,
        isLoading: false,
        sites: [],
        hasAtlassianAccess: false,
      }}
      launchpadSitesFeatureFlag={{ isLoading: false, value: false }}
    />
  ))
  .add('with Atlassian Access', () => (
    <OrganizationCardImplStory
      id="test-org-id"
      name="This org has Atlassian Access!"
      cardPosition={1}
      analyticsClient={createStorybookAnalyticsClient()}
      meta={{
        hasUnlinkedSites: false,
        hasVerifiedDomains: false,
        totalDomains: 3,
        totalMembers: 124,
        hasError: false,
        isLoading: false,
        sites: [],
        hasAtlassianAccess: true,
      }}
      launchpadSitesFeatureFlag={{ isLoading: false, value: true }}
    />
  ))
  .add('with meta loading', () => (
    <OrganizationCardImplStory
      id="test-org-id"
      name="Acme Inc"
      cardPosition={1}
      analyticsClient={createStorybookAnalyticsClient()}
      meta={{
        hasUnlinkedSites: false,
        hasVerifiedDomains: undefined,
        totalDomains: undefined,
        totalMembers: undefined,
        hasError: false,
        isLoading: true,
        sites: [],
      } as any}
      launchpadSitesFeatureFlag={{ isLoading: false, value: false }}
    />
  ))
  .add('with meta load error', () => (
    <OrganizationCardImplStory
      id="test-org-id"
      name="Acme Inc"
      cardPosition={1}
      analyticsClient={createStorybookAnalyticsClient()}
      meta={{
        hasUnlinkedSites: false,
        hasVerifiedDomains: undefined,
        totalDomains: undefined,
        totalMembers: undefined,
        hasError: true,
        isLoading: false,
        sites: [],
      } as any}
      launchpadSitesFeatureFlag={{ isLoading: false, value: false }}
    />
  ))
  ;
