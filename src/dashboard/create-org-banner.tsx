import * as React from 'react';
import { defineMessages, FormattedHTMLMessage } from 'react-intl';

import {
  Banner,
  BannerActionProps,
} from 'common/banner';

const messages = defineMessages({
  message: {
    id: 'create.org.banner.body',
    defaultMessage: `Create an organization and verify your domains so that you can
      easily manage all your users. Then you can subscribe to <strong>Identity Manager</strong>
      for security <strong>SAML single sign-on</strong>, <strong>two-step verification</strong>
      and <strong>password policy</strong>.`,
  },
  actionCreateOrg: {
    id: 'create.org.banner.create.org',
    defaultMessage: 'Create organization',
  },
  actionLearnMore: {
    id: 'create.org.banner.learn.more',
    defaultMessage: 'Learn more',
  },
});

export class CreateOrgBanner extends React.Component<{}> {
  public render() {
    const actions: BannerActionProps[] = [
      {
        text: messages.actionCreateOrg,
        onClick: () => null,
      },
      {
        text: messages.actionLearnMore,
        onClick: () => null,
      },
    ];

    return (
      <Banner
        imageMaxHeight={125}
        message={<FormattedHTMLMessage {...messages.message} />}
        actions={actions}
        // tslint:disable-next-line:no-require-imports
        image={require('./org.svg')}
      />
    );
  }
}
