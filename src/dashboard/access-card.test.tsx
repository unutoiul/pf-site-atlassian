import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import { Card } from 'common/card';

import { noop } from '../utilities/testing';
import { AccessCardImpl } from './access-card';

describe('AccessCard', () => {
  it(`should put the org id in the card's href`, () => {
    const wrapper = shallow(
      <AccessCardImpl match={{} as any} location={{} as any} history={{ replace: noop } as any} orgId="test-org-id" />,
    );

    expect(wrapper.find(Card).props().href).to.equal('/o/test-org-id/billing/estimate');

  });
});
