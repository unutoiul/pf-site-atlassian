import * as React from 'react';
import { graphql, OptionProps } from 'react-apollo';
import { Redirect } from 'react-router';

import AkSpinner from '@atlaskit/spinner';

import { PageLayout } from 'common/page-layout';

import { OrganizationsForDashboardRedirectQuery } from '../schema/schema-types';
import organizationsForDashboardRedirectQuery from './dashboard-redirect.query.graphql';
interface OrgSummary {
  id: string;
  hasVerifiedDomain: boolean;
}

export interface DashboardRedirectProps {
  isLoading: boolean;
  hasError: boolean;
  organizations: OrgSummary[];
  children?: React.ReactElement<any>;
}

export class DashboardRedirectImpl extends React.Component<DashboardRedirectProps> {

  public render() {
    const { isLoading, hasError, organizations } = this.props;

    if (hasError) {
      return React.Children.only(this.props.children);
    }

    if (isLoading) {
      return (
        <PageLayout collapsedNav={true}>
          <AkSpinner />
        </PageLayout>
      );
    }

    if (organizations.length === 0) {
      return <Redirect to="/atlassian-access" />;
    }

    if (organizations.length === 1) {
      if (organizations[0].hasVerifiedDomain) {
        return <Redirect to={`/o/${organizations[0].id}/members`} />;
      }

      return <Redirect to={`/o/${organizations[0].id}/overview`} />;
    }

    return React.Children.only(this.props.children);
  }
}

export const hasVerifiedDomain = (organization: NonNullable<OrganizationsForDashboardRedirectQuery['currentUser']>['organizations'][0]): boolean => {
  return !!(organization.domainClaim && organization.domainClaim.domains && organization.domainClaim.domains.some(domain => !!(domain && domain.verified)));
};

function getSummary(props: OptionProps<{}, OrganizationsForDashboardRedirectQuery>): OrgSummary[] {
  const {
    data: {
      currentUser = null,
    } = {},
  } = props;

  if (!currentUser) {
    return [];
  }

  return (currentUser.organizations || []).map<OrgSummary>(o => ({
    id: o.id,
    hasVerifiedDomain: hasVerifiedDomain(o),
  }));
}

export const DashboardRedirect = graphql<{}, OrganizationsForDashboardRedirectQuery, {}, DashboardRedirectProps>(
  organizationsForDashboardRedirectQuery, {
    props: (p): DashboardRedirectProps => ({
      isLoading: !!p.data && p.data.loading,
      hasError: !!(p.data && p.data.error),
      organizations: getSummary(p),
    }),
  })(DashboardRedirectImpl);
