import { ApolloError } from 'apollo-client';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import AkSpinner from '@atlaskit/spinner';

import { ScreenEventSender } from 'common/analytics';
import { GenericError } from 'common/error';
import { PermissionErrorPage } from 'common/permission-error-page';

import { createMockIntlProp } from '../utilities/testing';
import { DashboardImpl, DashboardProps } from './dashboard';
import { DashboardRedirect } from './dashboard-redirect';
import { DashboardWithSitesRedirect } from './dashboard-with-sites-redirect';

describe('Dashboard', () => {

  const shallowWrapper = (props: Partial<DashboardProps>, data: any = { loading: true }) => {
    const defaultProps = {
      intl: createMockIntlProp(),
      data,
      launchpadSitesFeatureFlag: {
        isLoading: false,
        value: false,
      },
    };

    return shallow(<DashboardImpl {...defaultProps} {...props as any} />);
  };

  it('should render spinner when both sites and organization counts are loading', () => {
    const wrapper = shallowWrapper({ totalSites: undefined, totalOrganizations: undefined });

    const spinner = wrapper.find<AkSpinner>(AkSpinner);
    expect(spinner).to.not.equal(undefined);
    expect(spinner).to.have.length(1);
    expect(spinner.props().size).to.equal('medium');
  });

  it('should render spinner when sites are zero and organization is loading', () => {
    const wrapper = shallowWrapper({ totalSites: 0, totalOrganizations: undefined });

    const spinner = wrapper.find<AkSpinner>(AkSpinner);
    expect(spinner).to.not.equal(undefined);
    expect(spinner).to.have.length(1);
    expect(spinner.props().size).to.equal('medium');
  });

  it('should render spinner when organizations are zero and sites is loading', () => {
    const wrapper = shallowWrapper({ totalSites: undefined, totalOrganizations: 0 });

    const spinner = wrapper.find<AkSpinner>(AkSpinner);
    expect(spinner).to.not.equal(undefined);
    expect(spinner).to.have.length(1);
    expect(spinner.props().size).to.equal('medium');
  });

  it('should render spinner when launchpad with sites feature flag is loading', () => {
    const wrapper = shallowWrapper({ launchpadSitesFeatureFlag: { isLoading: true, value: false } });

    const spinner = wrapper.find<AkSpinner>(AkSpinner);
    expect(spinner).to.not.equal(undefined);
    expect(spinner).to.have.length(1);
    expect(spinner.props().size).to.equal('medium');
  });

  it('should render old redirect when launchpad with sites feature flag is false', () => {
    const wrapper = shallowWrapper({ launchpadSitesFeatureFlag: { isLoading: false, value: false } });

    const oldRedirect = wrapper.find(DashboardRedirect);
    expect(oldRedirect.exists()).to.equal(true);
    const newRedirect = wrapper.find(DashboardWithSitesRedirect);
    expect(newRedirect.exists()).to.equal(false);
  });

  it('should render new redirect when launchpad with sites feature flag is true', () => {
    const wrapper = shallowWrapper({ launchpadSitesFeatureFlag: { isLoading: false, value: true } });

    const oldRedirect = wrapper.find(DashboardRedirect);
    expect(oldRedirect.exists()).to.equal(false);
    const newRedirect = wrapper.find(DashboardWithSitesRedirect);
    expect(newRedirect.exists()).to.equal(true);
  });

  it('should render permission denied when sites and organizations are both zero', () => {
    const wrapper = shallowWrapper({ totalSites: 0, totalOrganizations: 0 });

    const permissionError = wrapper.find(PermissionErrorPage);
    expect(permissionError).to.not.equal(undefined);
    expect(permissionError).to.have.length(1);

    const spinner = wrapper.find(AkSpinner);
    expect(spinner).to.have.length(0);
  });

  it('should render error', () => {
    const error = new ApolloError({});
    const wrapper = shallowWrapper({ totalOrganizationsLoadError: error });

    const permissionError = wrapper.find(GenericError);
    expect(permissionError).to.not.equal(undefined);
    expect(permissionError).to.have.length(1);

    const spinner = wrapper.find(AkSpinner);
    expect(spinner).to.have.length(0);
  });

  it('should render page when has at least 1 site and orgs are still loading', () => {
    const wrapper = shallowWrapper({ totalSites: 1, totalOrganizations: undefined });

    const permissionError = wrapper.find(PermissionErrorPage);
    expect(permissionError).to.have.length(0);

    const spinner = wrapper.find(AkSpinner);
    expect(spinner).to.have.length(0);
  });

  it('should render page when has at least 1 org and sites are still loading', () => {
    const wrapper = shallowWrapper({ totalSites: undefined, totalOrganizations: 1 });

    const permissionError = wrapper.find(PermissionErrorPage);
    expect(permissionError).to.have.length(0);

    const spinner = wrapper.find(AkSpinner);
    expect(spinner).to.have.length(0);
  });

  it('should render page when has loaded site and org data and has one or more of each', () => {
    const wrapper = shallowWrapper({ totalSites: 1, totalOrganizations: 2 });

    const permissionError = wrapper.find(PermissionErrorPage);
    expect(permissionError).to.have.length(0);

    const spinner = wrapper.find(AkSpinner);
    expect(spinner).to.have.length(0);
  });

  it('should not report screen event for old dashboard', () => {
    const wrapper = shallowWrapper({ totalSites: 1, totalOrganizations: 2 });

    const screenEventSender = wrapper.find(ScreenEventSender);
    expect(screenEventSender.exists()).to.equal(false);
  });

  it('should report screen event for launchpad', () => {
    const wrapper = shallowWrapper({
      totalSites: 1,
      totalOrganizations: 2,
      launchpadSitesFeatureFlag: {
        isLoading: false,
        value: true,
      },
    });
    const screenEventSender = wrapper.find(ScreenEventSender);

    expect(screenEventSender.props().event).to.deep.equal({
      data: {
        attributes: {
          totalOrgCount: 2,
          totalSiteCount: 1,
        },
        name: 'launchpadScreen',
      },
    });
  });
});
