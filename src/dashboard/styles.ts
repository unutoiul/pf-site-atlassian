// tslint:disable:restrict-plus-operands
import styled from 'styled-components';

import {
  fontSize as akFontSize,
  gridSize as akGridSize,
} from '@atlaskit/theme';

export const SectionTitle = styled.h2`
  padding-top: ${akGridSize()}px;
  padding-bottom: ${akGridSize() * 1.5}px;
  margin-top: 0;
  font-size: ${akFontSize() + 2}px;
`;
