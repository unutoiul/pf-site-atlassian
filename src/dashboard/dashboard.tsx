import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import AkSpinner from '@atlaskit/spinner';

import { GenericError } from 'common/error';
import { PageLayout } from 'common/page-layout';
import { Media } from 'common/responsive/Matcher';

import { PermissionErrorPage } from 'common/permission-error-page';
import { withOrganizationsCount, WithOrgsCountProps, withSitesCount, WithSitesCountProps } from 'common/user';

import { launchpadScreenEvent, Referrer, ScreenEventSender } from 'common/analytics';

import { DashboardRedirect } from './dashboard-redirect';
import { DashboardWithSitesRedirect } from './dashboard-with-sites-redirect';
import { OrganizationsOverview } from './organizations-overview';
import { UnlinkedSites } from './unlinked-sites';
import { LaunchpadSitesFeatureFlagResults, withLaunchpadSitesFeatureFlag } from './with-launchpad-sites-feature-flag';

const messages = defineMessages({
  title: {
    id: 'dashboard.title',
    defaultMessage: 'Admin',
    description: 'Title for an administration page that shows organizations and products',
  },
  description: {
    id: 'dashboard.description',
    defaultMessage: 'Centrally manage accounts and security policies globally across your sites and products.',
  },
});

const enum PageReadyState {
  Unknown,
  PermissionDenied,
  OK,
  Error,
}

export type DashboardProps = InjectedIntlProps & WithSitesCountProps & WithOrgsCountProps & LaunchpadSitesFeatureFlagResults;

export class DashboardImpl extends React.Component<DashboardProps> {
  public render() {
    const { formatMessage } = this.props.intl;

    const pageReadyState = this.getReadyState();

    if (pageReadyState === PageReadyState.PermissionDenied) {
      return <PermissionErrorPage />;
    } else if (pageReadyState === PageReadyState.Error) {
      return (
        <PageLayout
          title={formatMessage(messages.title)}
          description={formatMessage(messages.description)}
          collapsedNav={true}
        >
          <GenericError />
        </PageLayout>
      );
    } else if (pageReadyState === PageReadyState.Unknown) {
      return this.renderFeatureFlaggedDashboardRedirect((
        <PageLayout collapsedNav={true}>
          <AkSpinner />
        </PageLayout>
      ));
    }

    return this.renderFeatureFlaggedDashboardRedirect(
      this.renderScreenAnalyticsWrapper((
        <Media.Matches>
          {(_, pickMatch) => (
            <PageLayout
              title={formatMessage(messages.title)}
              description={formatMessage(messages.description)}
              collapsedNav={true}
              isFullWidth={pickMatch({
                mobile: true,
                tablet: false,
              })}
            >
              <OrganizationsOverview />
              <UnlinkedSites />
            </PageLayout>
          )}
        </Media.Matches>
      )),
    );
  }

  private renderFeatureFlaggedDashboardRedirect = (children?: React.ReactNode) => {
    const { launchpadSitesFeatureFlag } = this.props;

    if (launchpadSitesFeatureFlag.isLoading) {
      return (
        <PageLayout collapsedNav={true}>
          <AkSpinner />
        </PageLayout>
      );
    }

    if (launchpadSitesFeatureFlag.value) {
      return (
      <DashboardWithSitesRedirect>
        {children}
      </DashboardWithSitesRedirect>
      );
    }

    return (
      <DashboardRedirect>
        {children}
      </DashboardRedirect>
    );
  }

  private renderScreenAnalyticsWrapper = (children: React.ReactNode) => {
    const { launchpadSitesFeatureFlag, totalOrganizations, totalSites } = this.props;

    if (launchpadSitesFeatureFlag.value) {
      return (
        <ScreenEventSender event={launchpadScreenEvent(totalOrganizations!, totalSites!)} >
          <Referrer value="launchpad">
            {children}
          </Referrer>
        </ScreenEventSender>
      );
    }

    return children;
  };

  private getReadyState(): PageReadyState {
    const {
      totalSites,
      totalOrganizations,
      totalOrganizationsLoadError,
      totalSitesLoadError,
      launchpadSitesFeatureFlag,
    } = this.props;

    if (totalOrganizationsLoadError || totalSitesLoadError) {
      return PageReadyState.Error;
    }

    if ((totalSites === undefined && totalOrganizations === undefined) ||
      (totalSites === undefined && totalOrganizations === 0) ||
      (totalSites === 0 && totalOrganizations === undefined) ||
      launchpadSitesFeatureFlag.isLoading) {
      return PageReadyState.Unknown;
    }

    if (totalSites === 0 && totalOrganizations === 0) {
      return PageReadyState.PermissionDenied;
    }

    if ((totalSites && totalSites > 0) || (totalOrganizations && totalOrganizations > 0)) {
      return PageReadyState.OK;
    }

    return PageReadyState.Unknown;
  }

}

export const Dashboard = injectIntl(
  withLaunchpadSitesFeatureFlag(
    withSitesCount(
      withOrganizationsCount(DashboardImpl),
    ),
  ),
);
