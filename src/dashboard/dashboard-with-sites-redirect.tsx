import * as React from 'react';
import { graphql, OptionProps } from 'react-apollo';
import { Redirect } from 'react-router';

import AkSpinner from '@atlaskit/spinner';

import { PageLayout } from 'common/page-layout';

import { DashboardWithSitesRedirectQuery, OrgSite } from '../schema/schema-types';
import { util } from '../utilities/redirect';
import dashboardWithSitesRedirectQuery from './dashboard-with-sites-redirect.query.graphql';

interface OrgSummary {
  id: string;
  hasVerifiedDomain: boolean;
  sitesCount: number;
}

export interface DashboardWithSitesRedirectProps {
  isLoading: boolean;
  hasError: boolean;
  organizations: OrgSummary[];
  unlinkedSites: Array<Partial<OrgSite>>;
  children?: React.ReactElement<any>;
  redirect?(p: string): void;
}

export class DashboardWithSitesRedirectImpl extends React.Component<DashboardWithSitesRedirectProps> {

  public render() {
    const { isLoading, hasError, organizations, unlinkedSites, redirect = util.redirect } = this.props;

    if (hasError) {
      return React.Children.only(this.props.children);
    }

    if (isLoading) {
      return (
        <PageLayout collapsedNav={true}>
          <AkSpinner />
        </PageLayout>
      );
    }

    // 0 org & 1 site => Site Admin
    if (organizations.length === 0 && unlinkedSites.length === 1) {
      redirect(`https://${unlinkedSites[0].siteUrl}/admin`);

      return null;
    }

    // 1 org & 0 sites => orgs overview or members
    if (organizations.length === 1 && organizations[0].sitesCount === 0 && unlinkedSites.length === 0) {
      if (organizations[0].hasVerifiedDomain) {
        return <Redirect to={`/o/${organizations[0].id}/members`} />;
      }

      return <Redirect to={`/o/${organizations[0].id}/overview`} />;
    }

    // else Launchpad
    return React.Children.only(this.props.children);
  }
}

export const hasVerifiedDomain = (organization: NonNullable<DashboardWithSitesRedirectQuery['currentUser']>['organizations'][0]): boolean => {
  return !!(organization.domainClaim && organization.domainClaim.domains && organization.domainClaim.domains.some(domain => !!(domain && domain.verified)));
};

function getOrgSummary(props: OptionProps<{}, DashboardWithSitesRedirectQuery>): OrgSummary[] {
  const {
    data: {
      currentUser = null,
    } = {},
  } = props;

  if (!currentUser) {
    return [];
  }

  return (currentUser.organizations || []).map<OrgSummary>(o => ({
    id: o.id,
    hasVerifiedDomain: hasVerifiedDomain(o),
    sitesCount: (o.sites && o.sites.length) || 0,
  }));
}

export const DashboardWithSitesRedirect = graphql<{}, DashboardWithSitesRedirectQuery, {}, DashboardWithSitesRedirectProps>(
  dashboardWithSitesRedirectQuery, {
    props: (p): DashboardWithSitesRedirectProps => ({
      isLoading: !!p.data && p.data.loading,
      hasError: !!(p.data && p.data.error),
      organizations: getOrgSummary(p),
      unlinkedSites: (p.data && p.data.currentUser && p.data.currentUser.unlinkedSites) || [],
    }),
  })(DashboardWithSitesRedirectImpl);
