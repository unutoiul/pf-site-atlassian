import * as React from 'react';
import { graphql, OptionProps } from 'react-apollo';
import { defineMessages, FormattedMessage } from 'react-intl';

import AkSpinner from '@atlaskit/spinner';

import { GenericError } from 'common/error';

import { OrganizationsOverviewQuery } from '../schema/schema-types';
import { OrganizationCard } from './organization-card';
import organizationsOverviewQuery from './organizations-overview.graphql';
import { SectionTitle } from './styles';

const messages = defineMessages({
  title: {
    id: 'dashboard.orgs.title',
    defaultMessage: `{
      organizations,
      plural,
      one {Organization}
      other {Organizations}
    }`,
    description: 'Title of a section that lists different organizations',
  },
});

export interface OrgSummary {
  id: string;
  name: string;
}

export interface OrganizationsOverviewDerivedProps {
  isLoading: boolean;
  hasError: boolean;
  organizations: OrgSummary[];
}

export class OrganizationsOverviewImpl extends React.Component<OrganizationsOverviewDerivedProps> {
  public render() {
    const {
      hasError,
      isLoading,
      organizations,
    } = this.props;

    if (isLoading) {
      return <AkSpinner />;
    }

    if (hasError) {
      return <GenericError />;
    }

    if (organizations.length === 0) {
      return null;
    }

    return (
      <React.Fragment>
        <SectionTitle>
          <FormattedMessage values={{ organizations: organizations.length }} {...messages.title} />
        </SectionTitle>
        {organizations.map((org, index) => <OrganizationCard key={org.id} id={org.id} name={org.name} cardPosition={index + 1} />)}
      </React.Fragment>
    );
  }
}

function getSummary(props: OptionProps<{}, OrganizationsOverviewQuery>): OrgSummary[] {
  const defaultOrganizations = [];
  const {
    data: {
      currentUser = {
        organizations: defaultOrganizations,
      },
    } = {},
  } = props;

  return (currentUser && currentUser.organizations) || defaultOrganizations;
}

export const OrganizationsOverview = graphql<{}, OrganizationsOverviewQuery, {}, OrganizationsOverviewDerivedProps>(
  organizationsOverviewQuery,
  {
    props: (p): OrganizationsOverviewDerivedProps => ({
      isLoading: !!(p.data && p.data.loading),
      hasError: !!(p.data && p.data.error),
      organizations: getSummary(p),
    }),
  },
)(OrganizationsOverviewImpl);
