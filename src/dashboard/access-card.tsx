import * as React from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import styled from 'styled-components';

import AkButton from '@atlaskit/button';
import { gridSize as akGridSize } from '@atlaskit/theme';

import { Card } from 'common/card';
import { accessLogoImage } from 'common/images';
interface OwnProps {
  orgId: string;
}

const AccessLogo = styled.img`
  padding: ${akGridSize}px 0 ${akGridSize}px ${akGridSize}px;
  width: ${akGridSize() * 25}px;
`;

const AccessCardAction = styled.div`
  display: flex;
  align-items: center;
`;

const defaultMessages = defineMessages({
  bill: {
    id: 'access.card.view.bill',
    defaultMessage: `View Access bill`,
    description: 'Button that shows you the bill estimate for an Atlassian product called Atlassian Access. Access here is a product name.',
  },
});

export class AccessCardImpl extends React.Component<OwnProps & RouteComponentProps<{}>> {
  public render() {
    return (
      <Card
        href={`/o/${this.props.orgId}/billing/estimate`}
        // tslint:disable-next-line:jsx-use-translation-function (Since its a product name and not meant to be translated anyway)
        icon={<AccessLogo src={accessLogoImage} alt="Atlassian Access" />}
        mainContent={''}
        actions={(
          <AccessCardAction>
            <AkButton onClick={this.goToBilling}>
              <FormattedMessage {...defaultMessages.bill} />
            </AkButton>
          </AccessCardAction>
        )}
      />
    );
  }

  private goToBilling = () => {
    this.props.history.push(`/o/${this.props.orgId}/billing/estimate`);
  }
}

export const AccessCard = withRouter<OwnProps>(AccessCardImpl);
