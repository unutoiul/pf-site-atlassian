import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import { Card } from 'common/card';

import { createMockAnalyticsClient } from '../utilities/testing';
import { OrganizationCardImpl } from './organization-card';

describe('OrganizationCard', () => {
  it('should render the Atlassian Access card if the org has atlassian access', () => {
    const wrapper = shallow(
      <OrganizationCardImpl
        id="test-org-id"
        cardPosition={0}
        name="Bigtyper Inc."
        meta={{
          isLoading: false,
          hasError: false,
          hasVerifiedDomains: false,
          hasUnlinkedSites: false,
          totalDomains: 0,
          totalMembers: 0,
          sites: [],
          hasAtlassianAccess: true,
        }}
        analyticsClient={createMockAnalyticsClient()}
        launchpadSitesFeatureFlag={{
          value: true,
          isLoading: false,
        }}
      />,
    );

    const details = wrapper.find(Card).props().details! as any;
    expect(details.length).to.equal(1);
  });

  it('should not render the Atlassian Access card if the org does not have atlassian access', () => {
    const wrapper = shallow(
      <OrganizationCardImpl
        id="test-org-id"
        cardPosition={0}
        name="Bigtyper Inc."
        meta={{
          isLoading: false,
          hasError: false,
          hasVerifiedDomains: false,
          hasUnlinkedSites: false,
          totalDomains: 0,
          totalMembers: 0,
          sites: [],
          hasAtlassianAccess: false,
        }}
        analyticsClient={createMockAnalyticsClient()}
        launchpadSitesFeatureFlag={{
          value: true,
          isLoading: false,
        }}
      />,
    );

    const details = wrapper.find(Card).props().details! as any;
    expect(details).to.equal(null);
  });

  it('should not render the Atlassian Access card if the feature flag is off', () => {
    const wrapper = shallow(
      <OrganizationCardImpl
        id="test-org-id"
        cardPosition={0}
        name="Bigtyper Inc."
        meta={{
          isLoading: false,
          hasError: false,
          hasVerifiedDomains: false,
          hasUnlinkedSites: false,
          totalDomains: 0,
          totalMembers: 0,
          sites: [],
          hasAtlassianAccess: true,
        }}
        analyticsClient={createMockAnalyticsClient()}
        launchpadSitesFeatureFlag={{
          value: false,
          isLoading: false,
        }}
      />,
    );

    const details = wrapper.find(Card).props().details! as any;
    expect(details).to.equal(null);
  });
});
