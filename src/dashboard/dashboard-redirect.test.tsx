import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { Redirect } from 'react-router';
import { DashboardWithSitesRedirectQuery } from 'src/schema/schema-types';

import AkSpinner from '@atlaskit/spinner';

import { GenericError } from 'common/error';

import { DashboardRedirectImpl, DashboardRedirectProps, hasVerifiedDomain } from './dashboard-redirect';

describe('DashboardRedirect', () => {

  it('should render spinner when loading ', () => {
    const props: DashboardRedirectProps = {
      isLoading: true,
      hasError: false,
      organizations: undefined,
    } as any;
    const wrapper = shallow(
      <DashboardRedirectImpl {...props}>
        <div id="child" />
      </DashboardRedirectImpl>,
    );

    const spinner = wrapper.find<any>(AkSpinner);
    expect(spinner).to.not.equal(undefined);
    expect(spinner).to.have.length(1);

    const child = wrapper.find('#child');
    expect(child).to.have.length(0);
  });

  it('should render child when there is an error ', () => {
    const props: DashboardRedirectProps = {
      isLoading: true,
      hasError: true,
      organizations: undefined,
    } as any;
    const wrapper = shallow(
      <DashboardRedirectImpl {...props}>
        <div id="child" />
      </DashboardRedirectImpl>,
    );

    const genericError = wrapper.find<any>(GenericError);
    expect(genericError).to.have.length(0);

    const spinner = wrapper.find<any>(AkSpinner);
    expect(spinner).to.have.length(0);

    const child = wrapper.find('#child');
    expect(child).to.have.length(1);
  });

  it('should render redirect to atlassian-access with 0 orgs', () => {
    const props: DashboardRedirectProps = {
      isLoading: false,
      hasError: false,
      organizations: [],
    };
    const wrapper = shallow(
      <DashboardRedirectImpl {...props}>
        <div id="child" />
      </DashboardRedirectImpl>,
    );

    const redirect = wrapper.find<any>(Redirect);
    expect(redirect).to.have.length(1);
    expect(redirect.props().to).to.equal('/atlassian-access');

    const child = wrapper.find('#child');
    expect(child).to.have.length(0);
  });

  it('should render redirect to overview with 1 org and no domain claim', () => {
    const props: DashboardRedirectProps = {
      isLoading: false,
      hasError: false,
      organizations: [{
        id: '1',
        hasVerifiedDomain: false,
      }],
    };
    const wrapper = shallow(
      <DashboardRedirectImpl {...props}>
        <div id="child" />
      </DashboardRedirectImpl>,
    );

    const redirect = wrapper.find<any>(Redirect);
    expect(redirect).to.have.length(1);
    expect(redirect.props().to).to.equal('/o/1/overview');

    const child = wrapper.find('#child');
    expect(child).to.have.length(0);
  });

  it('should render redirect to members with 1 org and a domain claim', () => {
    const props: DashboardRedirectProps = {
      isLoading: false,
      hasError: false,
      organizations: [{
        id: '1',
        hasVerifiedDomain: true,
      }],
    };
    const wrapper = shallow(
      <DashboardRedirectImpl {...props}>
        <div id="child" />
      </DashboardRedirectImpl>,
    );

    const redirect = wrapper.find<any>(Redirect);
    expect(redirect).to.have.length(1);
    expect(redirect.props().to).to.equal('/o/1/members');

    const child = wrapper.find('#child');
    expect(child).to.have.length(0);
  });

  it('should render child for admin with 2 orgs', () => {
    const props: DashboardRedirectProps = {
      isLoading: false,
      hasError: false,
      organizations: [
        { id: '1', hasVerifiedDomain: true },
        { id: '2', hasVerifiedDomain: false },
      ],
    };
    const wrapper = shallow(
      <DashboardRedirectImpl {...props}>
        <div id="child" />
      </DashboardRedirectImpl>,
    );

    const child = wrapper.find('#child');
    expect(child).to.not.equal(undefined);
    expect(child).to.have.length(1);
  });

  describe('hasVerifiedDomain', () => {
    it('should return true for a verified domain', () => {
      const org: Partial<NonNullable<DashboardWithSitesRedirectQuery['currentUser']>['organizations'][0]> = {
        domainClaim: {
          domains: [
            { domain: 'test.com', verified: false, __typename: '' },
            { domain: 'test2.com', verified: true, __typename: '' },
          ],
          __typename: '',
        },
        __typename: '',
      };

      expect(hasVerifiedDomain(org as any)).to.equal(true);
    });

    it('should return false for no verified domains', () => {
      const org: Partial<NonNullable<DashboardWithSitesRedirectQuery['currentUser']>['organizations'][0]> = {
        domainClaim: {
          domains: [
            { domain: 'test.com', verified: false, __typename: '' },
            { domain: 'test2.com', verified: false, __typename: '' },
          ],
          __typename: '',
        },
        __typename: '',
      };

      expect(hasVerifiedDomain(org as any)).to.equal(false);
    });

    it('should return false for no domains', () => {
      const org: Partial<NonNullable<DashboardWithSitesRedirectQuery['currentUser']>['organizations'][0]> = {
        domainClaim: {
          domains: [],
          __typename: '',
        },
      };

      expect(hasVerifiedDomain(org as any)).to.equal(false);
    });
  });

});
