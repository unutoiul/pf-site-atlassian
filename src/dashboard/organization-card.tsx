import * as React from 'react';
import { graphql, OptionProps } from 'react-apollo';
import { defineMessages, FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import {
  colors as akColors,
  fontSize as akFontSize,
  gridSize as akGridSize,
} from '@atlaskit/theme';

import { AddSiteButton } from 'common/add-site-button/add-site-button';
import {
  AnalyticsClientProps,
  domainLinkClickEventData,
  launchpadAddSiteEvent,
  launchpadScreen,
  managedAccountsLinkClickEventData,
  manageOrgsScreen,
  orgNameLinkClickEventData,
  verifyDomainLinkClickEventData,
  withAnalyticsClient,
} from 'common/analytics';
import { UIData } from 'common/analytics/new-analytics-types';
import { Card } from 'common/card';
import { OrgIcon } from 'common/icons';
import { OscillatingBlock } from 'common/loading';
import { SiteCard } from 'common/site-card';
import { Centered } from 'common/styled';

import { OrganizationCardQuery, OrganizationCardQueryVariables } from '../schema/schema-types';
import { AccessCard } from './access-card';
import organizationCardQuery from './organization-card.query.graphql';
import { LaunchpadSitesFeatureFlagResults, withLaunchpadSitesFeatureFlag } from './with-launchpad-sites-feature-flag';

const messages = defineMessages({
  accounts: {
    id: 'adminhub.dashboard.org-overview.accounts',
    defaultMessage: `{
      accounts,
      plural,
      one {# managed account}
      other {# managed accounts}
    }`,
  },
  domains: {
    id: 'adminhub.dashboard.org-overview.domains',
    defaultMessage: `{
      domains,
      plural,
      one {# domain}
      other {# domains}
    }`,
  },
  verifyDomains: {
    id: 'adminhub.dashboard.org-overview.verify-domains',
    defaultMessage: 'Verify your domains',
  },
});

// tslint:disable-next-line:jsx-use-translation-function
const Divider = () => <span style={{ fontSize: 0 }}>&nbsp;</span>;

const SingleLineCardLink = styled(Link)`
  display: inline-block;
  white-space: nowrap;
  margin-left: ${akGridSize() * 2}px;
`;

const SubtleLink = styled(SingleLineCardLink)`
  &, &:hover {
    color: ${akColors.N400};
  }
`;

const AddSiteWrapper = styled.div`
  display: inline-block;
  white-space: nowrap;
  margin-left: ${akGridSize() * 2}px;
`;

export interface OrganizationCardOwnProps {
  id: string;
  name: string;
  cardPosition: number;
}

interface DerivedProps {
  meta: {
    isLoading: boolean;
    hasError: boolean;
    hasVerifiedDomains: boolean;
    hasUnlinkedSites: boolean;
    totalDomains: number;
    totalMembers: number;
    sites: OrganizationCardQuery['organization']['sites'];
    hasAtlassianAccess: boolean;
  };
}

export class OrganizationCardImpl extends React.Component<OrganizationCardOwnProps & DerivedProps & AnalyticsClientProps & LaunchpadSitesFeatureFlagResults> {
  public render() {
    const {
      id,
      name,
    } = this.props;

    let childCards = this.renderSites();
    const accessCard = this.renderAccessCard();
    if (accessCard) {
      childCards = [accessCard, ...(childCards || [])];
    }

    return (
      <Card
        href={`/o/${id}/overview`}
        mainContent={name}
        icon={<OrgIcon primaryColor={akColors.N0} />}
        actions={this.renderOrgMeta()}
        onClick={this.onCardClick}
        details={childCards}
        detailsSeparator="space"
        color="dark"
      />
    );
  }

  private renderOrgMeta() {
    const {
      id,
      meta: {
        isLoading,
        hasError,
        hasUnlinkedSites,
        hasVerifiedDomains,
        totalDomains: domains,
        totalMembers: accounts,
      },
    } = this.props;

    if (hasError) {
      return null;
    }

    if (isLoading) {
      return (
        <Centered>
          <OscillatingBlock width={`${akGridSize() * 20}px`} heightPx={akFontSize()} />
        </Centered>
      );
    }

    return hasVerifiedDomains
      ? (
        <Centered>
          <SubtleLink
            to={`/o/${id}/domains`}
            onClick={this.onDomainLinkClick}
          >
            <FormattedMessage values={{ domains }} {...messages.domains} />
          </SubtleLink>
          <Divider />
          <SubtleLink
            to={`/o/${id}/members`}
            onClick={this.onManagedAccountsLinkClick}
          >
            <FormattedMessage values={{ accounts: accounts < 0 ? '-' : accounts }} {...messages.accounts} />
          </SubtleLink>
          {hasUnlinkedSites && (
              <AddSiteWrapper>
                <AddSiteButton appearance="default" orgId={id} analyticsData={launchpadAddSiteEvent} />
              </AddSiteWrapper>
          )}

        </Centered>
      )
      : (
        <Centered>
          <SingleLineCardLink
            to={`/o/${id}/domains`}
            onClick={this.onVerifyDomainsLinkClick}
          >
            <FormattedMessage {...messages.verifyDomains} />
          </SingleLineCardLink>
        </Centered>
      );
  }

  private renderSites(): Array<React.ReactElement<any>> | null {
    const { launchpadSitesFeatureFlag, meta: { sites } } = this.props;

    if (launchpadSitesFeatureFlag.isLoading || launchpadSitesFeatureFlag.value === false || sites.length === 0) {
      return null;
    }

    return sites.map((site) => (
      <SiteCard key={site.id} site={site} screen={launchpadScreen} />
    ));
  }

  private renderAccessCard() {
    if (this.props.meta.isLoading || this.props.meta.hasError || !this.props.meta.hasAtlassianAccess || this.props.launchpadSitesFeatureFlag.isLoading || !this.props.launchpadSitesFeatureFlag.value) {
      return null;
    }

    return <AccessCard key="access" orgId={this.props.id} />;
  }

  private sendCardAnalyticsData(eventDataCreator: (cardPosition: number, source: string) => UIData): void {
    const {
      id,
      cardPosition,
      analyticsClient: { sendUIEvent },
      launchpadSitesFeatureFlag,
    } = this.props;

    const source = (launchpadSitesFeatureFlag.isLoading || !launchpadSitesFeatureFlag.value) ? manageOrgsScreen : launchpadScreen;

    sendUIEvent({
      orgId: id,
      data: eventDataCreator(cardPosition, source),
    });
  }

  private onCardClick = () => {
    this.sendCardAnalyticsData(orgNameLinkClickEventData);
  }

  private onVerifyDomainsLinkClick = () => {
    this.sendCardAnalyticsData(verifyDomainLinkClickEventData);
  }

  private onDomainLinkClick = () => {
    this.sendCardAnalyticsData(domainLinkClickEventData);
  }

  private onManagedAccountsLinkClick = () => {
    this.sendCardAnalyticsData(managedAccountsLinkClickEventData);
  }
}

function getOrgMeta(props: OptionProps<OrganizationCardOwnProps, OrganizationCardQuery>): DerivedProps {
  const defaultTotal = 0;
  const {
    data: {
      loading = false,
      error = null,
      organization: {
        domainClaim: {
          domains = [],
        } = {},
        users: {
          memberTotal = { total: defaultTotal },
        } = {},
        sites,
      } = {} as Partial<OrganizationCardQuery['organization']>,
      currentUser = { } as Partial<OrganizationCardQuery['currentUser']>,
      products: { identityManager = false } = {},
    } = {},
  } = props;

  return {
    meta: {
      isLoading: loading,
      hasError: !!error,
      hasVerifiedDomains: (domains || []).some(d => d.verified),
      hasUnlinkedSites: (!!currentUser && !!currentUser.unlinkedSites && currentUser.unlinkedSites.length > 0),
      totalDomains: (domains || []).length,
      totalMembers: (memberTotal && memberTotal.total) || defaultTotal,
      sites: (sites || []),
      hasAtlassianAccess: identityManager,
    },
  };
}

const withOrganizationMeta = graphql<OrganizationCardOwnProps & LaunchpadSitesFeatureFlagResults, OrganizationCardQuery, OrganizationCardQueryVariables, DerivedProps>(
  organizationCardQuery,
  {
    props: getOrgMeta,
    options: ({ id, launchpadSitesFeatureFlag: { isLoading, value } }: OrganizationCardOwnProps & LaunchpadSitesFeatureFlagResults) => ({
      variables: { id, sitesFeatureFlagDisabled: isLoading || !value } as OrganizationCardQueryVariables,
    }),
  },
);

export const OrganizationCard = withLaunchpadSitesFeatureFlag(
  withOrganizationMeta(
    withAnalyticsClient(
      OrganizationCardImpl,
    ),
  ),
);
