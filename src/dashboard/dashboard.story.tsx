import { storiesOf } from '@storybook/react';
import ApolloClient, { ApolloError } from 'apollo-client';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';
import { BrowserRouter as Router } from 'react-router-dom';
import { OrganizationCardQuery } from 'src/schema/schema-types';

import AkPage from '@atlaskit/page';

import userFeatureFlagQuery from 'common/feature-flags/user-feature-flag.query.graphql';

import { createApolloClient } from '../apollo-client';

import { createMockIntlProp } from '../utilities/testing';
import { DashboardImpl } from './dashboard';

import organizationCardQuery from './organization-card.query.graphql';
import organizationsOverviewQuery from './organizations-overview.graphql';
import unlinkedSitesQuery from './unlinked-sites.query.graphql';

interface GraphqlOrgForDashboardStory {
  id: string;
  name: string;
  users: {
    __typename: 'OrganizationUsers',
    memberTotal: {
      __typename: 'MemberTotal',
      total: number,
      enabledTotal: number,
      disabledTotal: number,
    },
  };
  domainClaim: {
    domains: Array<{
      domain: string;
      verified: boolean;
      __typename: 'Domain'
    }>,
    __typename: 'DomainClaim',
  };
  sites: OrganizationCardQuery['organization']['sites'];
  __typename: 'Organization';
}

export interface GraphqlOrgSummaryForStories {
  id: string;
  name: string;
  totalMembers: number;
  totalDomains: number;
  hasVerifiedDomains: boolean;
}

export const gqlOrgSummary = ({ id, name, totalMembers, totalDomains, hasVerifiedDomains }: GraphqlOrgSummaryForStories): GraphqlOrgForDashboardStory => ({
  id,
  name,
  users: {
    memberTotal: {
      total: totalMembers,
      enabledTotal: totalMembers,
      disabledTotal: 0,
      __typename: 'MemberTotal',
    },
    __typename: 'OrganizationUsers',
  },
  domainClaim: {
    domains: Array(totalDomains).fill({ domain: '', verified: hasVerifiedDomains, __typename: 'Domain' }),
    __typename: 'DomainClaim',
  },
  sites: id === 'org-1' ? [{
    __typename: 'OrgSite',
    id: '1363002c-4b37-303f-b9e3-06734d54989a',
    siteUrl: 'pf-site-admin-ui-prod.atlassian.net',
    displayName: 'Product Fabric Site',
    avatar: 'https://site-admin-avatar-cdn.staging.public.atl-paas.net/avatars/240/jersey.png',
    products: [
      {
        __typename: 'Product',
        key: 'jira-core',
        name: 'Jira Core',
      },
    ],
  }] : [],
  __typename: 'Organization',
});

export function clientWithOrgData({ client, orgs, withSites }: {
  client: ApolloClient<any>,
  orgs: GraphqlOrgForDashboardStory[],
  withSites: boolean,
}) {
  const gqlOrgs = orgs.map(org => ({
    ...org,
    __typename: 'Organization',
  }));

  client.writeQuery({
    query: organizationsOverviewQuery,
    data: {
      currentUser: {
        id: '1234',
        __typename: 'CurrentUser',
        organizations: gqlOrgs,
      },
    },
  });
  gqlOrgs.forEach(organization => {
    client.writeQuery({
      query: organizationCardQuery,
      data: { organization },
      variables: {
        id: organization.id,
        sitesFeatureFlagDisabled: !withSites,
      },
    });
    client.writeQuery({
      query: unlinkedSitesQuery,
      data: {
        currentUser: {
          id: '1234',
          __typename: 'CurrentUser',
          unlinkedSites: [{
            __typename: 'OrgSite',
            id: '1363002c-4b37-303f-b9e3-06734d54989b',
            siteUrl: 'acme.atlassian.net',
            displayName: 'ACME Test Site',
            avatar: 'https://site-admin-avatar-cdn.staging.public.atl-paas.net/avatars/240/jersey.png',
            products: [
              {
                __typename: 'Product',
                key: 'jira-core',
                name: 'Jira Core',
              },
              {
                __typename: 'Product',
                key: 'confluence',
                name: 'Confluence',
              },
            ],
          }],
        },
      },
      variables: {
        sitesFeatureFlagDisabled: !withSites,
      },
    });
  });
  client.writeQuery({
    query: userFeatureFlagQuery,
    data: {
      currentUser: {
        id: '1234',
        __typename: 'CurrentUser',
        flag: {
          __typename: 'FeatureFlag',
          id: 'admin.launchpad-with-sites',
          value: withSites,
        },
      },
    },
    variables: {
      flagKey: 'admin.launchpad-with-sites',
    },
  });
}

const sharedProps = {
  intl: createMockIntlProp(),
  totalOrganizations: 1,
  totalSites: 1,
  totalOrganizationsLoadError: undefined,
  totalSitesLoadError: undefined,
  launchpadSitesFeatureFlag: { isLoading: false, value: false },
};

storiesOf('Dashboard|Dashboard', module)
  .add('Loading state', () => {
    const client = createApolloClient();

    return (
      <ApolloProvider client={client} >
        <IntlProvider locale="en">
          <AkPage>
            <DashboardImpl {...sharedProps} totalSites={undefined} totalOrganizations={undefined} />
          </AkPage>
        </IntlProvider>
      </ApolloProvider>
    );
  })
  .add('Permission denied state', () => {
    const client = createApolloClient();

    return (
      <ApolloProvider client={client} >
        <IntlProvider locale="en">
          <AkPage>
            <DashboardImpl {...sharedProps} totalSites={0} totalOrganizations={0} />
          </AkPage>
        </IntlProvider>
      </ApolloProvider>
    );
  })
  .add('Error state', () => {
    const client = createApolloClient();
    const error = new ApolloError({});

    return (
      <ApolloProvider client={client} >
        <IntlProvider locale="en">
          <AkPage>
            <DashboardImpl {...sharedProps} totalSites={undefined} totalOrganizations={undefined} totalOrganizationsLoadError={error} />
          </AkPage>
        </IntlProvider>
      </ApolloProvider>
    );
  })
  .add('Regular state', () => {
    const client = createApolloClient();
    clientWithOrgData({
      withSites: false,
      client,
      orgs: [
        gqlOrgSummary({
          id: 'org-1',
          name: 'Acme Inc.',
          totalMembers: 25,
          totalDomains: 3,
          hasVerifiedDomains: true,
        }),
        gqlOrgSummary({
          id: 'org-2',
          name: 'Example LLC',
          totalMembers: 234,
          totalDomains: 2,
          hasVerifiedDomains: false,
        }),
      ],
    });

    return (
      <ApolloProvider client={client} >
        <IntlProvider locale="en">
          <Router>
            <AkPage>
              <DashboardImpl {...sharedProps} />
            </AkPage>
          </Router>
        </IntlProvider>
      </ApolloProvider>
    );
  })
  .add('With linked sites', () => {
    const client = createApolloClient();
    clientWithOrgData({
      withSites: true,
      client,
      orgs: [
        gqlOrgSummary({
          id: 'org-1',
          name: 'Acme Inc.',
          totalMembers: 25,
          totalDomains: 3,
          hasVerifiedDomains: true,
        }),
        gqlOrgSummary({
          id: 'org-2',
          name: 'Example LLC',
          totalMembers: 234,
          totalDomains: 2,
          hasVerifiedDomains: false,
        }),
      ],
    });

    return (
      <ApolloProvider client={client} >
        <IntlProvider locale="en">
          <Router>
            <AkPage>
              <DashboardImpl {...sharedProps} launchpadSitesFeatureFlag={{ isLoading: false, value: true }} />
            </AkPage>
          </Router>
        </IntlProvider>
      </ApolloProvider>
    );
  });
