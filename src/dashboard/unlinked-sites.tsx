import * as React from 'react';
import { graphql } from 'react-apollo';
import { defineMessages, FormattedMessage } from 'react-intl';
import styled from 'styled-components';

import { gridSize as akGridSize } from '@atlaskit/theme';

import { SiteCard } from 'common/site-card';

import { launchpadScreen } from 'common/analytics';

import { OrgSite, UnlinkedSitesQuery, UnlinkedSitesQueryVariables } from '../schema/schema-types';
import { SectionTitle } from './styles';
import unlinkedSitesQuery from './unlinked-sites.query.graphql';
import { LaunchpadSitesFeatureFlagResults, withLaunchpadSitesFeatureFlag } from './with-launchpad-sites-feature-flag';

const messages = defineMessages({
  title: {
    id: 'dashboard.sites.title',
    defaultMessage: 'Products',
    description: 'Title of a section that lists different products',
  },
});

const Wrapper = styled.div`
  margin-top: ${akGridSize() * 3}px;
`;

export interface DerivedProps {
  isLoading: boolean;
  hasError: boolean;
  sites: OrgSite[];
}

export class UnlinkedSitesImpl extends React.Component<LaunchpadSitesFeatureFlagResults & DerivedProps> {
  public render() {
    const { isLoading, hasError, sites } = this.props;

    if (isLoading || hasError || !this.isFeatureFlagEnabled() || sites.length === 0) {
      return null;
    }

    return (
      <Wrapper>
        <SectionTitle>
          <FormattedMessage {...messages.title} />
        </SectionTitle>
        {this.renderSites()}
      </Wrapper>
    );
  }

  private isFeatureFlagEnabled = () => {
    const { launchpadSitesFeatureFlag: { isLoading, value } } = this.props;

    return !isLoading && value;
  }

  private renderSites = () => this.props.sites.map((site) => (
    <SiteCard key={site.id} site={site} screen={launchpadScreen} />
  ));
}

const withUnlinkedSitesQuery = graphql<LaunchpadSitesFeatureFlagResults, UnlinkedSitesQuery, UnlinkedSitesQueryVariables, DerivedProps>(
  unlinkedSitesQuery,
  {
    props: ({ data }): DerivedProps => ({
      hasError: !!(data && data.error),
      isLoading: !!data && data.loading,
      sites: (data && data.currentUser && data.currentUser.unlinkedSites) || [],
    }),
    options: ({ launchpadSitesFeatureFlag: { isLoading, value } }: {} & LaunchpadSitesFeatureFlagResults) => ({
      variables: { sitesFeatureFlagDisabled: isLoading || !value } as UnlinkedSitesQueryVariables,
    }),
  },
);

export const UnlinkedSites = withLaunchpadSitesFeatureFlag<{}>(
  withUnlinkedSitesQuery(
    UnlinkedSitesImpl,
  ),
);
