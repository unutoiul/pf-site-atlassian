import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';
import { BrowserRouter as Router } from 'react-router-dom';

import { AnalyticsListener as AkAnalyticsListener } from '@atlaskit/analytics';

import { createApolloClient } from '../apollo-client';
import { clientWithOrgData, gqlOrgSummary, GraphqlOrgSummaryForStories } from './dashboard.story';
import { OrganizationsOverview, OrganizationsOverviewDerivedProps, OrganizationsOverviewImpl } from './organizations-overview';

const OrganizationsOverviewStory = ({ orgs }: { orgs: GraphqlOrgSummaryForStories[] }) => {
  const client = createApolloClient();
  clientWithOrgData({
    client,
    orgs: orgs.map(gqlOrgSummary),
    withSites: true,
  });

  return (
    <ApolloProvider client={client} >
      <AkAnalyticsListener onEvent={action('[Analytics]')}>
        <IntlProvider locale="en">
          <Router>
            <OrganizationsOverview />
          </Router>
        </IntlProvider>
      </AkAnalyticsListener>
    </ApolloProvider>
  );
};

const OrganizationsOverviewImplStory = (props: OrganizationsOverviewDerivedProps) => {
  return (
    <AkAnalyticsListener onEvent={action('[Analytics]')}>
      <IntlProvider locale="en">
        <Router>
          <OrganizationsOverviewImpl {...props} />
        </Router>
      </IntlProvider>
    </AkAnalyticsListener>
  );
};

storiesOf('Dashboard|Organizations Overview', module)
  .add('Regular state', () => (
    <OrganizationsOverviewStory
      orgs={[
        {
          id: 'org-1',
          name: 'Some Organization',
          totalMembers: 25,
          totalDomains: 3,
          hasVerifiedDomains: true,
        },
        {
          id: 'org-2',
          name: 'Another Organization',
          totalMembers: 4978,
          totalDomains: 1,
          hasVerifiedDomains: true,
        },
        {
          id: 'org-3',
          name: 'Interesting Organization with super-duper long name. You never would have thought that a user would create such an organization, but here it is nevertheless, no matter what your expectations were. Did you handle this gracefully? I hope so, because if not, you are in a very big trouble, mate!',
          totalMembers: 576,
          totalDomains: 2,
          hasVerifiedDomains: true,
        },
        {
          id: 'org-4',
          name: 'Brand new org',
          totalMembers: 0,
          totalDomains: 0,
          hasVerifiedDomains: true,
        },
        {
          id: 'org-5',
          name: 'Interesting Organization with super-duper long name. You never would have thought that a user would create such an organization, but here it is nevertheless, no matter what your expectations were. Did you handle this gracefully? I hope so, because if not, you are in a very big trouble, mate!',
          totalMembers: 576,
          totalDomains: 2,
          hasVerifiedDomains: false,
        },
      ]}
    />
  ))
  .add('One org', () => (
    <OrganizationsOverviewStory
      orgs={[
        {
          id: 'org-1',
          name: 'Some Organization',
          totalMembers: 25,
          totalDomains: 3,
          hasVerifiedDomains: true,
        },
      ]}
    />
  ))
  .add('Loading state', () => (
    <OrganizationsOverviewImplStory
      isLoading={true}
      hasError={false}
      organizations={[]}
    />
  ))
  .add('Error state', () => (
    <OrganizationsOverviewImplStory
      isLoading={false}
      hasError={true}
      organizations={[]}
    />
  ))
  .add('With no orgs', () => (
    <OrganizationsOverviewImplStory
      isLoading={false}
      hasError={false}
      organizations={[]}
    />
  ))
  ;
