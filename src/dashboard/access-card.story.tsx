import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';

import { AccessCard } from './access-card';

storiesOf('Dashboard|Access Card', module)
  .add('regular state', () => (
    <Router>
      <AccessCard
        orgId="test-org-id"
      />
    </Router>
  ))
  ;
