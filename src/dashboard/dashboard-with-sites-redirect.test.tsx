import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { Redirect } from 'react-router';
import * as sinon from 'sinon';
import { DashboardWithSitesRedirectQuery } from 'src/schema/schema-types';

import AkSpinner from '@atlaskit/spinner';

import { GenericError } from 'common/error';

import { DashboardWithSitesRedirectImpl, DashboardWithSitesRedirectProps, hasVerifiedDomain } from './dashboard-with-sites-redirect';

describe('DashboardWithSitesRedirect', () => {

  const getWrapper = (props: DashboardWithSitesRedirectProps) => shallow((
    <DashboardWithSitesRedirectImpl {...props}>
      <div id="child" />
    </DashboardWithSitesRedirectImpl>
  ));

  it('should render spinner when loading ', () => {
    const props: DashboardWithSitesRedirectProps = {
      isLoading: true,
      hasError: false,
      organizations: undefined,
    } as any;
    const wrapper = getWrapper(props);

    const spinner = wrapper.find(AkSpinner);
    expect(spinner).to.not.equal(undefined);
    expect(spinner).to.have.length(1);

    const child = wrapper.find('#child');
    expect(child).to.have.length(0);
  });

  it('should render child when there is an error ', () => {
    const props: DashboardWithSitesRedirectProps = {
      isLoading: true,
      hasError: true,
      organizations: undefined,
    } as any;
    const wrapper = getWrapper(props);

    const genericError = wrapper.find(GenericError);
    expect(genericError).to.have.length(0);

    const spinner = wrapper.find(AkSpinner);
    expect(spinner).to.have.length(0);

    const child = wrapper.find('#child');
    expect(child).to.have.length(1);
  });

  it('should render child for admin with 0 orgs & 0 sites', () => {
    const props: DashboardWithSitesRedirectProps = {
      isLoading: false,
      hasError: false,
      organizations: [],
      unlinkedSites: [],
    };
    const wrapper = getWrapper(props);

    const redirect = wrapper.find(Redirect);
    expect(redirect).to.have.length(0);

    const child = wrapper.find('#child');
    expect(child).to.have.length(1);
  });

  it('should call the redirect prop to external site admin with 0 orgs & 1 site', () => {
    const redirectSpy = sinon.spy();
    const props: DashboardWithSitesRedirectProps = {
      isLoading: false,
      hasError: false,
      organizations: [],
      unlinkedSites: [{
        siteUrl: 'siteurl.com',
      }],
      redirect: redirectSpy,
    };
    const wrapper = getWrapper(props);

    expect(redirectSpy.callCount).to.equal(1);
    expect(redirectSpy.getCalls()[0].args[0]).to.equal('https://siteurl.com/admin');

    const child = wrapper.find('#child');
    expect(child).to.have.length(0);
  });

  it('should render child for admin with 0 orgs and multiple sites', () => {
    const props: DashboardWithSitesRedirectProps = {
      isLoading: false,
      hasError: false,
      organizations: [],
      unlinkedSites: [
        { id: 'site1' },
        { id: 'site2' },
      ],
    };
    const wrapper = getWrapper(props);

    const child = wrapper.find('#child');
    expect(child).to.not.equal(undefined);
    expect(child).to.have.length(1);
  });

  it('should render redirect to overview with 1 org, no sites, and no domain claim', () => {
    const props: DashboardWithSitesRedirectProps = {
      isLoading: false,
      hasError: false,
      organizations: [{
        id: '1',
        hasVerifiedDomain: false,
        sitesCount: 0,
      }],
      unlinkedSites: [],
    };
    const wrapper = getWrapper(props);

    const redirect = wrapper.find(Redirect);
    expect(redirect).to.have.length(1);
    expect(redirect.props().to).to.equal('/o/1/overview');

    const child = wrapper.find('#child');
    expect(child).to.have.length(0);
  });

  it('should render redirect to members with 1 org, no sites, and a domain claim', () => {
    const props: DashboardWithSitesRedirectProps = {
      isLoading: false,
      hasError: false,
      organizations: [{
        id: '1',
        hasVerifiedDomain: true,
        sitesCount: 0,
      }],
      unlinkedSites: [],
    };
    const wrapper = getWrapper(props);

    const redirect = wrapper.find(Redirect);
    expect(redirect).to.have.length(1);
    expect(redirect.props().to).to.equal('/o/1/members');

    const child = wrapper.find('#child');
    expect(child).to.have.length(0);
  });

  it('should render child for admin with 1 org and 1 unlinked site', () => {
    const props: DashboardWithSitesRedirectProps = {
      isLoading: false,
      hasError: false,
      organizations: [
        { id: '1', hasVerifiedDomain: true, sitesCount: 0 },
      ],
      unlinkedSites: [
        { id: 'site' },
      ],
    };
    const wrapper = getWrapper(props);

    const child = wrapper.find('#child');
    expect(child).to.not.equal(undefined);
    expect(child).to.have.length(1);
  });

  it('should render child for admin with 1 org and 1 linked site', () => {
    const props: DashboardWithSitesRedirectProps = {
      isLoading: false,
      hasError: false,
      organizations: [
        { id: '1', hasVerifiedDomain: true, sitesCount: 1 },
      ],
      unlinkedSites: [],
    };
    const wrapper = getWrapper(props);

    const child = wrapper.find('#child');
    expect(child).to.not.equal(undefined);
    expect(child).to.have.length(1);
  });

  it('should render child for admin with 1 org and multiple sites', () => {
    const props: DashboardWithSitesRedirectProps = {
      isLoading: false,
      hasError: false,
      organizations: [
        { id: '1', hasVerifiedDomain: true, sitesCount: 4 },
      ],
      unlinkedSites: [],
    };
    const wrapper = getWrapper(props);

    const child = wrapper.find('#child');
    expect(child).to.not.equal(undefined);
    expect(child).to.have.length(1);
  });

  it('should render child for admin with multiple orgs and 0 sites', () => {
    const props: DashboardWithSitesRedirectProps = {
      isLoading: false,
      hasError: false,
      organizations: [
        { id: '1', hasVerifiedDomain: true, sitesCount: 0 },
        { id: '2', hasVerifiedDomain: true, sitesCount: 0 },
      ],
      unlinkedSites: [],
    };
    const wrapper = getWrapper(props);

    const child = wrapper.find('#child');
    expect(child).to.not.equal(undefined);
    expect(child).to.have.length(1);
  });

  it('should render child for admin with multiple orgs and 1 site', () => {
    const props: DashboardWithSitesRedirectProps = {
      isLoading: false,
      hasError: false,
      organizations: [
        { id: '1', hasVerifiedDomain: true, sitesCount: 0 },
        { id: '2', hasVerifiedDomain: true, sitesCount: 0 },
      ],
      unlinkedSites: [{ id: '3' }],
    };
    const wrapper = getWrapper(props);

    const child = wrapper.find('#child');
    expect(child).to.not.equal(undefined);
    expect(child).to.have.length(1);
  });

  it('should render child for admin with multiple orgs and multiple sites', () => {
    const props: DashboardWithSitesRedirectProps = {
      isLoading: false,
      hasError: false,
      organizations: [
        { id: '1', hasVerifiedDomain: true, sitesCount: 0 },
        { id: '2', hasVerifiedDomain: true, sitesCount: 0 },
      ],
      unlinkedSites: [
        { id: '3' },
        { id: '4' },
      ],
    };
    const wrapper = getWrapper(props);

    const child = wrapper.find('#child');
    expect(child).to.not.equal(undefined);
    expect(child).to.have.length(1);
  });

  describe('hasVerifiedDomain', () => {
    it('should return true for a verified domain', () => {
      const org: Partial<NonNullable<DashboardWithSitesRedirectQuery['currentUser']>['organizations'][0]> = {
        domainClaim: {
          domains: [
            { domain: 'test.com', verified: false, __typename: '' },
            { domain: 'test2.com', verified: true, __typename: '' },
          ],
          __typename: '',
        },
        __typename: '',
      };

      expect(hasVerifiedDomain(org as any)).to.equal(true);
    });

    it('should return false for no verified domains', () => {
      const org: Partial<NonNullable<DashboardWithSitesRedirectQuery['currentUser']>['organizations'][0]> = {
        domainClaim: {
          domains: [
            { domain: 'test.com', verified: false, __typename: '' },
            { domain: 'test2.com', verified: false, __typename: '' },
          ],
          __typename: '',
        },
        __typename: '',
      };

      expect(hasVerifiedDomain(org as any)).to.equal(false);
    });

    it('should return false for no domains', () => {
      const org: Partial<NonNullable<DashboardWithSitesRedirectQuery['currentUser']>['organizations'][0]> = {
        domainClaim: {
          domains: [],
          __typename: '',
        },
      };

      expect(hasVerifiedDomain(org as any)).to.equal(false);
    });
  });

});
