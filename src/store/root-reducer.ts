import { combineReducers, Reducer } from 'redux';

import { RootState } from '.';

import { appsReducer } from '../organization/apps.reducer';
import { chromeReducer } from '../site/chrome.reducer';

export function createRootReducer(): Reducer<RootState> {
  return combineReducers<RootState>({
    apps: appsReducer,
    chrome: chromeReducer,
  });
}
