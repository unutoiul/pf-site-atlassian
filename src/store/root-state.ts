import { AppState } from '../organization/apps.reducer';
import { ChromeState } from '../site/chrome.reducer';

export interface RootState {
  readonly chrome: ChromeState;
  readonly apps: AppState;
}
