import {
  ChromeDrawerClose,
  ChromeDrawerLinkHide,
  ChromeDrawerLinkShow,
  ChromeDrawerOpen,
  ChromeNavigationSectionRemove,
  ChromeNavigationSectionUpdate,
  NavigationAction,
} from 'common/navigation';

import {
  FinishOnboarding,
  NextStep,
  OnboardingActionTypes,
  PreviousStep,
  QueueOnboarding,
} from 'common/onboarding/onboarding.actions';

import {
  CreateOrganization,
  OrganizationKeys,
  StartPollingOrganization,
  StopCreateOrganization,
} from '../organization/organizations/organizations.actions';

export type Action =
  | CreateOrganization
  | ChromeDrawerClose
  | ChromeDrawerOpen
  | ChromeDrawerLinkHide
  | ChromeDrawerLinkShow
  | ChromeNavigationSectionRemove
  | ChromeNavigationSectionUpdate
  | StartPollingOrganization
  | StopCreateOrganization
  | StopCreateOrganization
  | QueueOnboarding
  | FinishOnboarding
  | NextStep
  | PreviousStep;

export {
  NavigationAction,
  OrganizationKeys,
  OnboardingActionTypes,
};
