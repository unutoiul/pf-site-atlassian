import { createStore as reduxCreateStore, Store } from 'redux';

import { RootState } from '.';
import { createRootReducer } from './root-reducer';

export const createStore = (): Store<RootState> => reduxCreateStore(
  createRootReducer(),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
);
