import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import { AsyncSelect as AkAsyncSelect } from '@atlaskit/select';
import { fontSize as akFontSize } from '@atlaskit/theme';

import { analyticsClient } from 'common/analytics';
import { isGroupModifiableForUser } from 'common/group-permissions';

import { AsyncInlineGroupSelectQuery, AsyncInlineGroupSelectQueryVariables } from '../../schema/schema-types';
import { debounce } from '../../utilities/decorators';
import asyncInlineGroupSelectQuery from './async-inline-group-select.query.graphql';

const messages = defineMessages({
  groupMembershipPlaceholder: {
    id: 'user.invite.drawer.email.group.membership.placeholder',
    defaultMessage: 'Add groups',
    description: 'Placeholder text for the group membership input.',
  },
});

interface OwnProps {
  cloudId?: string;
  onChange(item: [{ value: string }]): void;
}

type Props = ChildProps<OwnProps & InjectedIntlProps, AsyncInlineGroupSelectQuery>;
interface ReactSelectOption {
  label: string;
  description: string;
  value: string;
}

const formatOptionLabel = (option: ReactSelectOption, { context }) => {
  if (context === 'menu') {
    return (
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
        }}
      >
        <div>{option.label}</div>
        {option.description ? (
          <div
            style={{
              fontSize: akFontSize() * 0.85,
              fontStyle: 'italic',
            }}
          >
            {option.description}
          </div>
        ) : null}
      </div>
    );
  } else if (context === 'value') {
    return (
      <span>{option.label}</span>
    );
  }

  return option.value;
};

export class AsyncInlineGroupSelectImpl extends React.Component<Props> {
  public constructor(props) {
    super(props);

    this.query = this.query.bind(this);
  }
  public render() {
    const { intl: { formatMessage } } = this.props;
    // tslint:disable no-unbound-method react-this-binding-issue

    return (
      <AkAsyncSelect
        formatOptionLabel={formatOptionLabel}
        components={{ DropdownIndicator: null }}
        isClearable={true}
        isMulti={true}
        defaultOptions={true}
        placeholder={formatMessage(messages.groupMembershipPlaceholder)}
        loadOptions={this.query}
        onChange={this.props.onChange}
      />
    );
    // tslint:enable no-unbound-method react-this-binding-issue
  }

  @debounce('onInputChange')
  private query(inputValue: string, callback: (options: any) => void) {
    if (!this.props.data || !this.props.cloudId) {
      return;
    }

    this.props.data.fetchMore({
      variables: {
        cloudId: this.props.cloudId,
        displayName: inputValue,
      } as AsyncInlineGroupSelectQueryVariables,
      updateQuery: (previousResult, { fetchMoreResult }) => fetchMoreResult || previousResult,
    }).then(() => {
      if (!this.props.data || !this.props.data.groupList || !this.props.data.groupList.groups || !this.props.data.currentUser) {
        return;
      }

      const isSystemAdmin = this.props.data.currentUser.isSystemAdmin;

      const groups = this.props.data.groupList.groups
        .filter(group => isGroupModifiableForUser(group, !!isSystemAdmin, false))
        .map<ReactSelectOption>(group => ({
          label: group.name,
          description: group.description || '',
          value: group.id,
        }));

      callback(groups);
    }).catch(e => {
      analyticsClient.onError(e);
    });

  }
}

const withQuery = graphql<OwnProps & InjectedIntlProps, AsyncInlineGroupSelectQuery, {}, Props>(asyncInlineGroupSelectQuery, {
  options: (props: Props) => ({
    variables: {
      cloudId: props.cloudId,
      displayName: '',
    },
  }),
});

export const AsyncInlineGroupSelect = injectIntl(
  withQuery(
    AsyncInlineGroupSelectImpl,
  ),
);
