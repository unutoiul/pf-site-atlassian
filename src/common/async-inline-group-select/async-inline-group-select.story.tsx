// tslint:disable:jsx-use-translation-function
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';

import {
  Field as AkField,
} from '@atlaskit/form';

import { AsyncInlineGroupSelect } from 'common/async-inline-group-select/async-inline-group-select';

import { createApolloClient } from '../../apollo-client';
import { ApolloClientStorybookWrapper } from '../../utilities/storybooks';

// tslint:disable-next-line:no-console
const client = createApolloClient((e) => console.error(e));

storiesOf('Common|Async Inline Select', module)
  .add('Inline Group Picker', () => {
    return (
      <ApolloClientStorybookWrapper client={client}>
        <p>Note that this component is not wired up to refetch data on query</p>
        <ApolloProvider client={client}>
          <IntlProvider locale="en">
            <AkField label="Group Membership">
              <AsyncInlineGroupSelect cloudId="test" onChange={() => null} />
            </AkField>
          </IntlProvider>
        </ApolloProvider>
      </ApolloClientStorybookWrapper>
    );
  });
