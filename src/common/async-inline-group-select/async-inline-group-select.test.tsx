import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { spy, stub } from 'sinon';

import { AsyncSelect as AkAsyncSelect } from '@atlaskit/select';

import { createMockIntlProp, wait } from '../../utilities/testing';
import { AsyncInlineGroupSelectImpl } from './async-inline-group-select';

const noop = () => null;

describe('Async inline group select', () => {
  it('should fire onChange when the AsyncSelect onChange is fired', () => {
    const onChangeSpy = spy();
    const wrapper = shallow(
      <AsyncInlineGroupSelectImpl
        onChange={onChangeSpy}
        intl={createMockIntlProp()}
        cloudId={'test-cloud-id'}
      />,
    );

    wrapper.find(AkAsyncSelect).simulate('change');
    expect(onChangeSpy.called).to.equal(true);
  });

  it('should call fetchMore with the cloud id and input value when the input changes', async () => {
    const fetchMoreStub = stub().returns(Promise.resolve());
    const wrapper = shallow(
      <AsyncInlineGroupSelectImpl
        onChange={noop}
        intl={createMockIntlProp()}
        data={{
          fetchMore: fetchMoreStub,
          ...{} as any,
        }}
        cloudId={'test-cloud-id'}
      />,
    );
    const loadOptions: (query, callback) => void = wrapper.find(AkAsyncSelect).prop('loadOptions');
    loadOptions('test query', noop);

    await wait(500); // wait for the debounce

    expect(fetchMoreStub.firstCall.args[0].variables.cloudId).to.equal('test-cloud-id');
    expect(fetchMoreStub.firstCall.args[0].variables.displayName).to.equal('test query');
  });

  // This test has been flakey. https://hello.atlassian.net/browse/ADMPF-902
  it.skip('should update the AsyncSelect with the current groups', async () => {
    const fetchMoreStub = stub().returns(Promise.resolve());
    const wrapper = shallow(
      <AsyncInlineGroupSelectImpl
        onChange={noop}
        intl={createMockIntlProp()}
        data={{
          groupList: {
            groups: [
              {
                name: 'group0',
                description: 'group0 description',
                id: 'group-id-0',
              },
              {
                name: 'group1',
                description: 'group1 description',
                id: 'group-id-1',
              },
              {
                name: 'group2',
                description: 'group2 description',
                id: 'group-id-2',
              },
            ],
          },
          currentUser: {
            isSystemAdmin: false,
          },
          fetchMore: fetchMoreStub,
          ...{} as any,
        }}
        cloudId={'test-cloud-id'}
      />,
    );

    const callbackSpy = spy();
    const loadOptions: (query, callback) => void = wrapper.find(AkAsyncSelect).prop('loadOptions');
    loadOptions('test query', callbackSpy);

    await wait(500); // wait for the debounce

    expect(callbackSpy.callCount).to.equal(1);
    const groups = callbackSpy.args[0][0];
    expect(groups.length).to.equal(3);
    expect(groups[0].value).to.equal('group-id-0');
    expect(groups[1].value).to.equal('group-id-1');
    expect(groups[2].value).to.equal('group-id-2');
    expect(groups[0].label).to.equal('group0');
    expect(groups[1].label).to.equal('group1');
    expect(groups[2].label).to.equal('group2');
    expect(groups[0].description).to.equal('group0 description');
    expect(groups[1].description).to.equal('group1 description');
    expect(groups[2].description).to.equal('group2 description');
  });
});
