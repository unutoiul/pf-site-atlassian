import { ActionProps } from 'common/actions';

export { UserSelectedItem } from './user-selected-item';
export { UserSuggestionItem } from './user-suggestion-item';
export interface User {
  id: string;
  displayName: string;
  email: string;
  username: string;
  actions: Array<ActionProps & { key: React.Key }>;
}
