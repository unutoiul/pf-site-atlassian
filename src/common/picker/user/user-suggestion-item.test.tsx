// tslint:disable react-this-binding-issue jsx-use-translation-function
import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';

import AkItem from '@atlaskit/item';

import { UserSuggestionItem } from './user-suggestion-item';

describe('User Suggestion Item', () => {
  const defaultUser = {
    id: 'test-id',
    displayName: 'some name',
    email: 'some@email.com',
    username: 'some-username',
    actions: [],
  };
  function mountUserSuggestionItem({ user = defaultUser } = {}) {
    return mount(
      <UserSuggestionItem
        item={user}
        select={(_) => null}
        isHighlighted={false}
      />,
    );
  }

  it('should render', () => {
    const wrapper = mountUserSuggestionItem();

    expect(wrapper.find(AkItem).length).to.eq(1);
  });

  it('should not render if null/falsy item is provided', () => {
    const wrapper = mountUserSuggestionItem({ user: null } as any);

    expect(wrapper.find(AkItem).length).to.eq(0);
  });
});
