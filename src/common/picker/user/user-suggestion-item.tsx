import * as React from 'react';

import AkItem from '@atlaskit/item';

import { Account } from 'common/account';
import { SuggestionItemProps } from 'common/picker/picker';

import { User } from './';

export class UserSuggestionItem extends React.Component<SuggestionItemProps<User>> {
  public render() {
    const {
      select,
      isHighlighted,
      item,
    } = this.props;

    if (!item) {
      return null;
    }

    return (
      <AkItem isSelected={isHighlighted} onClick={select}>
        <Account
          id={item.id}
          email={item.email}
          displayName={item.displayName}
        />
      </AkItem>
    );
  }
}
