// tslint:disable jsx-use-translation-function
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl, IntlProvider } from 'react-intl';

import { Picker } from '../picker';
import { User } from './';
import { UserSelectedItem } from './user-selected-item';
import { UserSuggestionItem } from './user-suggestion-item';

interface UserPickerState {
  query: string;
  users: User[];
}

const getUserRepo = (): User[] => {
  const firstNames = [
    'Ruslan',
    'Ashwini',
    'Joshua',
    'Stephen',
    'Ian',
    'Tyler',
    'Swati',
    'Lingbo',
    'Helena',
  ];
  const lastNames = [
    'Arkhipau',
    'Rattihalli',
    'Nelson',
    'Bennett',
    'Tracey',
    'Van Hoomissen',
    'Raju',
    'Lu',
    'Lu',
  ];
  const ids = [
    '557058:43d09406-ff6e-41ed-9aae-fca16196e32b',
    '557057:26ead946-4955-46ef-a11e-b1f9ae987de7',
    '557057:03eeccea-d769-4c8b-b410-7619d2c7f640',
    '557057:c8534c8f-2190-4751-8c20-94a352b87440',
    '557058:d5096927-8106-4697-91f5-d7c86f3a8cb3',
    '557058:a26fd84b-c461-4c5c-ba46-a1d135eadbe0',
    '557058:a4e0cd3c-aabf-4aa3-ba57-07a40750c4af',
    '557057:d9e8d338-0f96-4ff6-9f81-41f566d9a113',
    '5a0a212739622c0fae911a0c',
  ];

  return ids.map((id, i) => ({
    id,
    displayName: `${firstNames[i]} ${lastNames[i]}`,
    email: `${firstNames[i].toLowerCase()}-${lastNames[i].toLowerCase()}@atlassian.com`,
    username: `${firstNames[i].toLowerCase()}`,
    actions: [],
  }));
};

function renderUserSuggestionItem(user: User, isHighlighted: boolean, isSelected: boolean, select: (user: User) => any): React.ReactNode {
  if (isSelected) {
    return null;
  }

  return <UserSuggestionItem {...{ item: user, isHighlighted, select }} />;
}

function renderUserSelectedItem(user: User, unselect: (user: User) => any) {

  return <UserSelectedItem {...{ item: user, unselect }} />;
}

const messages = defineMessages({
  titleText: {
    id: 'user-picker-title-text',
    defaultMessage: 'Add Users',
  },
  placeholderText: {
    id: 'user-picker-placeholder-text',
    defaultMessage: 'Start searching',
  },
  inputLabelText: {
    id: 'user-picker-label-text',
    defaultMessage: 'Search for users to add',
  },
});

class UserPickerExampleImpl extends React.Component<InjectedIntlProps, UserPickerState> {
  public state: Readonly<UserPickerState> = {
    query: '',
    users: getUserRepo(),
  };

  public onSelect = (user: User) => {
    const { users } = this.state;
    this.setState({
      users: users.filter(u => u.id !== user.id),
    });
  }

  public onRemove = (user: User) => {
    const { users } = this.state;
    this.setState({
      users: [...users, user],
    });
  }

  public userAlreadySelected(user: User, selectedUsers: User[]) {
    return selectedUsers.map(u => u.id).includes(user.id);
  }

  public onQueryChange = (query: string, selectedUsers: User[]) => {
    this.setState({
      users: getUserRepo().filter(
        u => u.displayName.toLowerCase().includes(query.toLowerCase())
          && !this.userAlreadySelected(u, selectedUsers),
      ),
    });
  }

  public render() {
    const {
      intl: { formatMessage },
    } = this.props;

    return (
      <Picker
        isOpen={true}
        title={formatMessage(messages.titleText)}
        inputLabelText={formatMessage(messages.inputLabelText)}
        placeholderText={formatMessage(messages.placeholderText)}
        items={this.state.users.slice(0, 5)}
        loading={false}
        renderItem={renderUserSuggestionItem}
        renderItemPreview={renderUserSelectedItem}
        onSelect={this.onSelect}
        onRemove={this.onRemove}
        onQueryChange={this.onQueryChange}
        maxLength={NaN}
      />
    );
  }
}

const UserPickerExample = injectIntl<{}>(UserPickerExampleImpl);

storiesOf('Common|Picker/User', module)
  .add('Basic usage', () => {
    return (
      <IntlProvider locale="en">
        <UserPickerExample />
      </IntlProvider>
    );
  });
