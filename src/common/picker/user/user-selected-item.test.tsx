// tslint:disable react-this-binding-issue jsx-use-translation-function
import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';

import AkTag from '@atlaskit/tag';

import { createMockIntlContext, createMockIntlProp } from '../../../utilities/testing';
import { UserSelectedItemImpl } from './user-selected-item';

describe('User Selected Item', () => {
  const defaultUser = {
    id: 'test-id',
    displayName: 'some name',
    email: 'some@email.com',
    username: 'some-username',
    actions: [],
  };
  function mountUserSelectedItem({ user = defaultUser } = {}) {
    return mount(
      <UserSelectedItemImpl
        item={user}
        unselect={(_) => null}
        intl={createMockIntlProp()}
      />,
      createMockIntlContext(),
    );
  }

  it('should render', () => {
    const wrapper = mountUserSelectedItem();

    expect(wrapper.find(AkTag).length).to.eq(1);
  });

  it('should not render if null/falsy item is provided', () => {
    const wrapper = mountUserSelectedItem({ user: null } as any);

    expect(wrapper.find(AkTag).length).to.eq(0);
  });
});
