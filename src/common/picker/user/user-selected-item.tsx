import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import AkAvatar from '@atlaskit/avatar';
import AkTag from '@atlaskit/tag';

import { SelectedItemProps } from 'common/picker/picker';

import { getAvatarUrlByUserId } from 'common/avatar';

import { User } from './';

const messages = defineMessages({
  removeButtonText: {
    id: 'rm-btn-txt-user-selected-item',
    defaultMessage: 'remove',
  },
});
export class UserSelectedItemImpl extends React.Component<SelectedItemProps<User> & InjectedIntlProps> {

  public render() {
    const {
      item,
      unselect,
      intl: { formatMessage },
    } = this.props;

    if (!item) {
      return null;
    }

    return (
      <AkTag
        appearance="rounded"
        text={item.displayName}
        removeButtonText={formatMessage(messages.removeButtonText)}
        onAfterRemoveAction={unselect}
        elemBefore={<AkAvatar size="xsmall" src={getAvatarUrlByUserId(item.id)} />}
      />
    );
  }
}

export const UserSelectedItem = injectIntl(UserSelectedItemImpl);
