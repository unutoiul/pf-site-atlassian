import * as React from 'react';

import AkItem from '@atlaskit/item';

import { GroupName } from 'common/group-name';

import { SuggestionItemProps } from '../picker';
import { Group } from './';

export class GroupSuggestionItem extends React.Component<SuggestionItemProps<Group>> {
  public render() {
    const {
      select,
      isHighlighted,
      item,
    } = this.props;

    if (!item) {
      return null;
    }

    return (
      <AkItem
        isSelected={isHighlighted}
        onClick={select}
        description={item.description}
      >
        <GroupName name={item.displayName} managementAccess={item.managementAccess} ownerType={item.ownerType} />
      </AkItem>
    );
  }
}
