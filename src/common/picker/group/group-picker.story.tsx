
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl, IntlProvider } from 'react-intl';

import { ManagementAccess, OwnerType, SitePrivilege } from '../../../schema/schema-types';
import { random, randomBetween } from '../../../utilities/random';
import { Picker } from '../picker';
import { Group } from './';
import { GroupSelectedItem } from './group-selected-item';
import { GroupSuggestionItem } from './group-suggestion-item';
interface GroupPickerState {
  query: string;
  groups: Group[];
}

const getRandomGroup = () => {
  const prefixes = [
    'jira',
    'confluence',
    'site',
    'stride',
    'global',
    'sydney',
    'mtv',
  ];

  const suffixes = [
    'users',
    'developers',
    'admins',
    'designers',
    'contractors',
    'team',
  ];

  const descriptions = [
    'No description',
    'Long description, long description, long description, Long description, long description, long description',
    'user access to platforms',
    'some other kind of user access',
    'someemail@atlassian.com',
  ];

  const displayName = `${random(prefixes)}-${random(suffixes)}`;
  const id = displayName;
  const description = random(descriptions);
  const ownerType: OwnerType | null = randomBetween(0, 3) < 1 ? 'EXT_SCIM' : null;
  const managementAccess: ManagementAccess = ownerType ? 'READ_ONLY' : 'NONE';
  const unmodifiable = managementAccess === 'READ_ONLY';
  const sitePrivilege: SitePrivilege = 'NONE';

  return {
    displayName,
    id,
    description,
    managementAccess,
    ownerType,
    unmodifiable,
    sitePrivilege,
  };
};

function renderGroupSuggestionItem(group: Group, isHighlighted: boolean, isSelected: boolean, select: (group: Group) => any): React.ReactNode {
  if (isSelected) {
    return null;
  }

  return <GroupSuggestionItem {...{ item: group, isHighlighted, select }} />;
}

function renderGroupSelectedItem(group: Group, unselect: (group: Group) => any) {

  return <GroupSelectedItem {...{ item: group, unselect }} />;
}

export function getRandomGroups(count: number): Group[] {
  const arr: null[] = Array.apply(null, Array(count));

  return arr.map(getRandomGroup);
}

const groupRepo = getRandomGroups(20);

const messages = defineMessages({
  titleText: {
    id: 'group-picker-title-text',
    defaultMessage: 'Add Groups',
  },
  placeholderText: {
    id: 'group-picker-placeholder-text',
    defaultMessage: 'Start searching',
  },
  inputLabelText: {
    id: 'group-picker-label-text',
    defaultMessage: 'Search for groups to add',
  },
});
class GroupPickerExampleImpl extends React.Component<InjectedIntlProps, GroupPickerState> {
  public state: Readonly<GroupPickerState> = {
    query: '',
    groups: groupRepo,
  };

  public onSelect = (group: Group) => {
    const { groups } = this.state;
    this.setState({
      groups: groups.filter(g => g.id !== group.id),
    });
  }

  public onRemove = (group: Group) => {
    const { groups } = this.state;
    this.setState({
      groups: [...groups, group],
    });
  }

  public onQueryChange = (query: string, _: Group[]) => {
    this.setState({
      groups: groupRepo.filter(
        g => g.displayName.toLowerCase().includes(query.toLowerCase()),
      ),
    });
  }

  public render() {
    const {
      intl: { formatMessage },
    } = this.props;

    return (
      <Picker
        title={formatMessage(messages.titleText)}
        isOpen={true}
        inputLabelText={formatMessage(messages.inputLabelText)}
        placeholderText={formatMessage(messages.placeholderText)}
        items={this.state.groups.slice(0, 5)}
        loading={false}
        renderItem={renderGroupSuggestionItem}
        renderItemPreview={renderGroupSelectedItem}
        onSelect={this.onSelect}
        onRemove={this.onRemove}
        onQueryChange={this.onQueryChange}
        maxLength={NaN}
      />
    );
  }
}

const GroupPickerExample = injectIntl<{}>(GroupPickerExampleImpl);

storiesOf('Common|Picker/Group', module)
  .add('Basic usage', () => {
    return (
      <IntlProvider locale="en">
        <GroupPickerExample />
      </IntlProvider>
    );
  });
