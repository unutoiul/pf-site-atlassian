import { ManagementAccess, OwnerType, SitePrivilege } from '../../../schema/schema-types';

export { GroupSelectedItem } from './group-selected-item';
export { GroupSuggestionItem } from './group-suggestion-item';

export interface Group {
  id: string;
  displayName: string;
  description: string;
  managementAccess: ManagementAccess;
  ownerType?: OwnerType | null;
  unmodifiable: boolean;
  sitePrivilege: SitePrivilege;
}
