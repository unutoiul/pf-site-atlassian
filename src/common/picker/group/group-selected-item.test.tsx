// tslint:disable react-this-binding-issue jsx-use-translation-function
import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';

import AkTag from '@atlaskit/tag';

import { createMockIntlContext, createMockIntlProp } from '../../../utilities/testing';
import { GroupSelectedItemImpl } from './group-selected-item';

describe('Group Selected Item', () => {
  const defaultGroup = {
    id: 'test-id',
    displayName: 'Some name',
    description: 'Some desc',
  };
  function mountGroupSelectedItem({ group = defaultGroup }: any = {}) {
    return mount(
      <GroupSelectedItemImpl
        item={group}
        unselect={() => null}
        intl={createMockIntlProp()}
      />,
      createMockIntlContext(),
    );
  }

  it('should render', () => {
    const wrapper = mountGroupSelectedItem();

    expect(wrapper.find(AkTag).length).to.eq(1);
  });

  it('should not render if null/falsy item is provided', () => {
    const wrapper = mountGroupSelectedItem({ group: null });

    expect(wrapper.find(AkTag).length).to.eq(0);
  });
});
