import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import AkTag from '@atlaskit/tag';

import { SelectedItemProps } from 'common/picker/picker';

import { Group } from './';

const messages = defineMessages({
  removeButtonText: {
    id: 'rm-btn-txt-group-selected-item',
    defaultMessage: 'remove',
  },
});
export class GroupSelectedItemImpl extends React.Component<SelectedItemProps<Group> & InjectedIntlProps> {

  public render() {
    const {
      item,
      unselect,
      intl: { formatMessage },
    } = this.props;

    if (!item) {
      return null;
    }

    return (
      <AkTag
        appearance="rounded"
        text={item.displayName}
        removeButtonText={formatMessage(messages.removeButtonText)}
        onAfterRemoveAction={unselect}
      />
    );
  }
}

export const GroupSelectedItem = injectIntl(GroupSelectedItemImpl);
