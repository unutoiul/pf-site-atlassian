// tslint:disable react-this-binding-issue jsx-use-translation-function
import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import { IntlProvider } from 'react-intl';

import AkItem from '@atlaskit/item';

import { GroupSuggestionItem } from './group-suggestion-item';

describe('Group Suggestion Item', () => {
  const defaultGroup = {
    id: 'test-id',
    displayName: 'Some name',
    description: 'Some desc',
  };

  function mountGroupSuggestionItem({ group = defaultGroup }: any = {}) {
    return mount(
      <IntlProvider locale="en">
        <GroupSuggestionItem
          item={group}
          select={() => null}
          isHighlighted={false}
        />
      </IntlProvider>,
    );
  }

  it('should render', () => {
    const wrapper = mountGroupSuggestionItem();

    expect(wrapper.find(AkItem).length).to.eq(1);
  });

  it('should not render if null/falsy item is provided', () => {
    const wrapper = mountGroupSuggestionItem({ group: null });

    expect(wrapper.find(AkItem).length).to.eq(0);
  });
});
