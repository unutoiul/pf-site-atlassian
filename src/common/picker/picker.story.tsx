// tslint:disable react-this-binding-issue jsx-use-translation-function
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import * as range from 'lodash.range';
import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl, IntlProvider } from 'react-intl';

import AkButton, { ButtonGroup as AkButtonGroup } from '@atlaskit/button';
import AkItem from '@atlaskit/item';
import AkTag from '@atlaskit/tag';

import { ErrorMessage } from 'common/picker';

import { random } from '../../utilities/random';
import { Picker } from './picker';

const getRandomItem = (id) => {
  const names = [
    'Thing 1',
    'Thing 2',
    'Some other thing',
    'Another thing',
    'second to last thing',
    'last thing',
  ];

  const descriptions = [
    'No description',
    'Long description, long description, long description, Long description, long description, long description',
    'user access to platforms',
    'some other kind of user access',
    'someemail@atlassian.com',
  ];

  const displayName = random(names);
  const description = random(descriptions);

  return {
    id,
    displayName,
    description,
  } as Item;
};

interface Item {
  id: string;
  displayName: string;
  description: string;
}

export function getRandomItems(count: number): Item[] {
  const ids = range(0, count);

  return ids.map(getRandomItem);
}

interface PickerExampleState {
  query: string;
  items: Item[];
}

const renderSelectedItem = (item: Item, unselect: (item: Item) => any): React.ReactNode => (
  <AkTag
    appearance="rounded"
    text={item.displayName}
    removeButtonText={'remove'}
    onAfterRemoveAction={unselect}
    key={item.id}
  />
);

const renderSuggestedItem = (item: Item, isHighlighted: boolean, isSelected: boolean, select: (item: Item) => any): React.ReactNode => {
  if (isSelected) {
    return null;
  }

  return (
    <AkItem
      key={item.id}
      isSelected={isHighlighted}
      onClick={select}
      description={item.description}
    >
      {item.displayName}
    </AkItem>
  );
};

const itemRepo = getRandomItems(20);

const messages = defineMessages({
  titleText: {
    id: 'picker-title-text',
    defaultMessage: 'Add Items',
  },
  placeholderText: {
    id: 'picker-placeholder-text',
    defaultMessage: 'Start searching',
  },
  inputLabelText: {
    id: 'picker-label-text',
    defaultMessage: 'Search for items to add',
  },
});
class PickerExampleImpl extends React.Component<InjectedIntlProps & any, PickerExampleState> {

  public state: Readonly<PickerExampleState> = {
    query: '',
    items: itemRepo,
  };

  public picker: React.ReactNode;

  public render() {
    const {
      loading,
      errorMessage,
      items,
      onSelectCallback,
      onRemoveCallback,
      onQueryChangeCallback,
      renderFooter,
      intl: { formatMessage },
      getSelections,
    } = this.props;

    const noop = (_) => null;

    const onSelectWrapper = onSelectCallback || noop;
    const onRemoveWrapper = onRemoveCallback || noop;
    const onQueryChangeWrapper = onQueryChangeCallback || noop;

    return (
      <div>
        <Picker
          isOpen={true}
          title={formatMessage(messages.titleText)}
          inputLabelText={formatMessage(messages.inputLabelText)}
          placeholderText={formatMessage(messages.placeholderText)}
          items={items || this.state.items.slice(0, 15)}
          loading={loading || undefined}
          errorMessage={errorMessage || null}
          renderItem={renderSuggestedItem}
          renderItemPreview={renderSelectedItem}
          renderFooter={renderFooter || noop}
          onSelect={(item: Item) => {
            this.onSelect(item);
            onSelectWrapper(item);
          }}
          onRemove={(item: Item) => {
            this.onRemove(item);
            onRemoveWrapper(item);
          }}
          onQueryChange={(q: string, selectedItems: Item[]) => {
            this.onQueryChange(q, selectedItems);
            onQueryChangeWrapper(q, selectedItems);
          }}
          ref={picker => this.picker = picker}
        />
        {getSelections ? <p>hello</p> : null}
      </div>

    );
  }

  public getSelections() {
    return (this.picker as any).getSelections();
  }

  private onSelect = (item: Item) => {
    const { items } = this.state;
    this.setState({
      items: items.filter(i => i.id !== item.id),
    });
  }

  private onRemove = (item: Item) => {
    const { items } = this.state;
    this.setState({
      items: [...items, item],
    });
  }

  private itemAlreadySelected = (item: Item, selectedItems: Item[]) => {
    return selectedItems.map(i => i.id).includes(item.id);
  }

  private onQueryChange = (query: string, selectedItems: Item[]) => {
    this.setState({
      items: itemRepo.filter(
        i => i.displayName.toLowerCase().includes(query.toLowerCase())
          && !this.itemAlreadySelected(i, selectedItems),
      ),
    });
  }
}

const PickerExample = injectIntl(PickerExampleImpl);

storiesOf('Common|Picker', module)
  .add('Loading state', () => {
    return (
      <IntlProvider locale="en">
        <PickerExample loading={true} />
      </IntlProvider>
    );
  })
  .add('No results state', () => {
    return (
      <IntlProvider locale="en">
        <PickerExample items={[]} />
      </IntlProvider>
    );
  })
  .add('Error state', () => {
    return (
      <IntlProvider locale="en">
        <PickerExample errorMessage={<ErrorMessage />} />
      </IntlProvider>
    );
  })
  .add('Custom error message', () => {
    const renderErrorMessage = () => <div>Custom Error Message</div>;

    return (
      <IntlProvider locale="en">
        <PickerExample
          errorMessage={renderErrorMessage()}
        />
      </IntlProvider>
    );
  })
  .add('Custom footer', () => {
    const renderFooter = (_, onClose) => (
      <AkButtonGroup>
        <AkButton appearance={'primary'} onClick={action('add')}>Add</AkButton>
        <AkButton appearance={'link'} onClick={onClose}>Close</AkButton>
      </AkButtonGroup>
    );

    return (
      <IntlProvider locale="en">
        <PickerExample renderFooter={renderFooter} />
      </IntlProvider>
    );
  })
  .add('Picker actions', () => {
    return (
      <IntlProvider locale="en">
        <PickerExample
          onSelectCallback={action('onSelect')}
          onRemoveCallback={action('onRemove')}
          onQueryChangeCallback={action('onQueryChange')}
        />
      </IntlProvider>
    );
  })
  .add('Get selected items', () => {
    const selectionCallback = (items) => {
      action('getSelection')(items);
    };
    const renderFooter = (getSelectedItems, _) => (
      <AkButtonGroup>
        <AkButton appearance={'link'} onClick={() => selectionCallback(getSelectedItems())}>Get Selected Items</AkButton>
      </AkButtonGroup>
    );

    return (
      <IntlProvider locale="en">
        <PickerExample renderFooter={renderFooter} />
      </IntlProvider>
    );
  });
