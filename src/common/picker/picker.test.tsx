// tslint:disable react-this-binding-issue jsx-use-translation-function
import { expect } from 'chai';
import { mount } from 'enzyme';
import times from 'lodash.times';
import * as React from 'react';
import { spy } from 'sinon';

import AkFieldBase from '@atlaskit/field-base';
import AkSpinner from '@atlaskit/spinner';

import { createMockIntlContext, waitUntil } from '../../utilities/testing';
import { Picker, PickerProps } from './picker';
import { EmptyResultsMessage } from './picker-empty-message';
import { ErrorMessage } from './picker-error-message';

describe('Picker', () => {
  function mountPicker({ items, onQueryChange, onRemove, onSelect, loading, errorMessage, maxLength, renderFooter, renderItem }: Partial<PickerProps> = {}) {

    const noop = () => null;

    return mount(
      <Picker
        isOpen={true}
        title={'Some title'}
        inputLabelText={'Some input label text'}
        placeholderText={'placeholder text'}
        items={items || []}
        loading={loading || false}
        errorMessage={errorMessage || null}
        renderItem={renderItem || noop}
        renderItemPreview={noop}
        onSelect={onSelect || noop}
        onRemove={onRemove || noop}
        onQueryChange={onQueryChange || noop}
        maxLength={maxLength || NaN}
        renderFooter={renderFooter || noop}
      />,
      createMockIntlContext(),
    );
  }

  describe('render', () => {
    it('should render the picker', () => {
      const wrapper = mountPicker();
      const fieldBase = wrapper.find(AkFieldBase);
      expect(fieldBase).to.have.length(1);
    });

    it('should render a spinner when in loading state', () => {
      const wrapper = mountPicker({ loading: true });
      const loadingSpinner = wrapper.find(AkSpinner);
      expect(loadingSpinner).to.have.length(1);
    });

    it('should render an error message when in an error state', () => {
      const renderErrorMessage = () => <ErrorMessage />;
      const wrapper = mountPicker({
        errorMessage: renderErrorMessage(),
      });
      const errorMessage = wrapper.find(ErrorMessage);
      expect(errorMessage).to.have.length(1);
    });

    it('should render a no results message when items are empty', () => {
      const wrapper = mountPicker();
      const errorMessage = wrapper.find(EmptyResultsMessage);
      expect(errorMessage).to.have.length(1);
    });
  });

  describe('Input', () => {
    it('should focus the input', () => {
      const wrapper = mountPicker();
      const picker = wrapper.instance() as any;
      const focusSpy = spy(picker.searchInput, 'focus');

      picker.focusSearchInput();

      expect(focusSpy.called).to.equal(true);
    });

  });

  describe('Item selection', () => {
    const mountWithItemSelectionSpies = ({ items = [], maxLength = NaN }: Partial<PickerProps> = {}) => {
      const onQueryChangeSpy = spy();
      const onRemoveSpy = spy();
      const onSelectSpy = spy();
      const wrapper = mountPicker({ maxLength, items, onQueryChange: onQueryChangeSpy, onRemove: onRemoveSpy, onSelect: onSelectSpy });
      const picker = wrapper.instance() as any;
      wrapper.update();

      const selectItem = (item) => {
        picker.selectItem(item);

        return {
          onSelectSpy,
          picker,
          remove,
          selectItem,
        };
      };

      const remove = (item) => {
        picker.remove(item);

        return {
          onRemoveSpy,
          picker,
        };
      };

      return {
        remove,
        selectItem,
        picker,
        onQueryChangeSpy,
        onSelectSpy,
      };
    };

    describe('SelectItem', () => {
      it('should add the item to the selectedItems list when selectItem is called', async () => {
        const spies = mountWithItemSelectionSpies();
        expect(spies.picker.getSelections().length).to.eq(0);
        const item = { id: 'test-id' };

        spies.selectItem(item);

        await waitUntil(() => spies.picker.getSelections().length > 0);
        expect(spies.picker.getSelections()).to.have.members([item]);
      });

      it('should call the onSelect props method if selectItem is called', async () => {
        const items = [{ id: 'test-id' }];

        const spies = mountWithItemSelectionSpies({ items })
          .selectItem(items[0]);

        await waitUntil(() => spies.onSelectSpy.called === true);
        expect(spies.onSelectSpy.called).to.equal(true);
        expect(spies.onSelectSpy.calledWith(items[0])).to.equal(true);
      });

      it('should call the onQueryChange props method if selectItem is called', async () => {
        const items = [{ id: 'test-id' }, { id: 'test-id-2' }, { id: 'test-id-3' }];

        const spies = mountWithItemSelectionSpies({ items });
        spies.selectItem(items[0]);

        await waitUntil(() => spies.onSelectSpy.called === true);
        expect(spies.onQueryChangeSpy.called).to.equal(true);
        expect(spies.onQueryChangeSpy.calledWith('', [items[0]])).to.equal(true);
      });

      it('should not call onSelectItem if the max size of the picker is met', async () => {
        const items = [{ id: 'test-id' }, { id: 'test-id-2' }, { id: 'test-id-3' }];
        const spies = mountWithItemSelectionSpies({ items, maxLength: 1 });

        spies.picker.selectItem(items[0]);
        spies.picker.selectItem(items[1]);

        await waitUntil(() => spies.onSelectSpy.called === true);
        expect(spies.onSelectSpy.calledOnce).to.equal(true);
      });
    });

    describe('RemoveItem', () => {
      it('should remove the item from the selectedItems list when remove is called', async () => {
        const items = [{ id: 'test-id' }, { id: 'test-id-2' }, { id: 'test-id-3' }];

        const spies = mountWithItemSelectionSpies({ items })
          .selectItem(items[0]);

        await waitUntil(() => spies.picker.getSelections().length > 0);
        let selectedItems = spies.picker.getSelections();
        expect(selectedItems).to.have.members([items[0]]);

        spies.remove(items[0]);

        await waitUntil(() => spies.picker.getSelections().length === 0);
        selectedItems = spies.picker.getSelections();
        expect(selectedItems.length).to.eq(0);
      });

      it('should call the onRemove action when remove action called ', async () => {
        const items = [{ id: 'test-id' }, { id: 'test-id-2' }, { id: 'test-id-3' }];
        const spies = mountWithItemSelectionSpies({ items })
          .remove(items[1]);

        await waitUntil(() => spies.onRemoveSpy.called === true);

        expect(spies.onRemoveSpy.calledWith(items[1])).to.equal(true);
      });
    });
  });

  describe('Highlighting', () => {
    const mountPickerWithHighlightSpies = ({ items = [] }: Partial<PickerProps> = {}) => {
      const renderItem = (_) => {
        return <div />;
      };
      const wrapper = mountPicker({ items, renderItem });
      const picker = wrapper.instance() as any;
      const highlightItemAtIndexSpy = spy(picker, 'highlightItemAtIndex');
      const highlightSelectedItemAtIndexSpy = spy(picker, 'highlightSelectedItemAtIndex');
      const selectItemSpy = spy(picker, 'selectItem');

      const subjects = {
        picker,
        highlightItemAtIndexSpy,
        highlightSelectedItemAtIndexSpy,
        selectItemSpy,
      };
      wrapper.update();

      const highlightItemBelow = (numTimes = 1) => {
        times(numTimes, picker.highlightItemBelow);

        return subjects;
      };

      const selectHighlightedItem = () => {
        picker.selectHighlightedItem();

        return {
          ...subjects,
          highlightSelectedItemAtIndex,
        };
      };

      const performHighlightedAction = () => {
        picker.performHighlightedAction();

        return {
          ...subjects,
        };
      };

      const highlightItemAbove = (numTimes = 1) => {

        times(numTimes, picker.highlightItemAbove);

        return {
          ...subjects,
          selectHighlightedItem,
          performHighlightedAction,
        };
      };

      const highlightSelectedItemAtIndex = (index) => {
        picker.highlightSelectedItemAtIndex(index);

        return {
          ...subjects,
          highlightItemAbove,
          highlightItemBelow,
          performHighlightedAction,
        };
      };

      const selectItem = (item) => {
        picker.selectItem(item);

        return {
          ...subjects,
          highlightItemAbove,
          highlightItemBelow,
          highlightSelectedItemAtIndex,
        };
      };

      const actions = {
        highlightItemAbove,
        highlightItemBelow,
        selectHighlightedItem,
        highlightSelectedItemAtIndex,
        selectItem,
        performHighlightedAction,
      };

      return {
        ...actions,
        ...subjects,
      };
    };

    describe('Highlighting Items', () => {
      describe('default state', () => {
        it('should have first item selected on initial mount', () => {
          const items = [{ id: '1' }, { id: '2' }, { id: '3' }];
          const spies = mountPickerWithHighlightSpies({ items });

          expect(spies.picker.state.highlighted.index).to.equal(0);
        });
      });
      describe('highlightItemAbove()', () => {

        it('should not call highlightItemAtIndex if size <= 0', () => {
          const spies = mountPickerWithHighlightSpies().highlightItemAbove();

          expect(spies.highlightItemAtIndexSpy.called).to.equal(false);
        });

        it('should call  if size > 0', () => {
          const items = [{ id: '1' }, { id: '2' }, { id: '3' }];
          const spies = mountPickerWithHighlightSpies({ items }).highlightItemAbove();

          expect(spies.highlightItemAtIndexSpy.called).to.equal(true);
        });

        it('should call with lastIndex if currentIndex is 0', () => {
          const items = [{ id: '1' }, { id: '2' }, { id: '3' }];
          const spies = mountPickerWithHighlightSpies({ items })
            .highlightItemAbove(1);

          expect(spies.highlightItemAtIndexSpy.lastCall.calledWith(2)).to.equal(true);
        });

        it('if highlightItemIndex is > 0, should call with highlightItemIndex minus one', () => {
          const items = [{ id: '1' }, { id: '2' }, { id: '3' }];
          //  first call is to set the index to zero, then two additional calls to set to index = 1
          const spies = mountPickerWithHighlightSpies({ items })
            .highlightItemAbove(3);
          expect(spies.highlightItemAtIndexSpy.calledWith(1)).to.equal(true);
        });

        it('should select first item in items list if selected item is highlighted', () => {
          const items = [{ id: '1' }, { id: '2' }, { id: '3' }];
          //  first call is to set the index to zero, then two additional calls to set to index = 1
          const spies = mountPickerWithHighlightSpies({ items })
            .selectItem(items[1])
            .highlightSelectedItemAtIndex(0)
            .highlightItemAbove();

          expect(spies.highlightItemAtIndexSpy.calledWith(0)).to.equal(true);
        });
      });
    });

    describe('highlightItemBelow', () => {
      it('should not call highlightItemAtIndex if size <= 0', () => {
        const items = [];
        const spies = mountPickerWithHighlightSpies({ items })
          .highlightItemBelow();

        expect(spies.highlightItemAtIndexSpy.called).to.equal(false);
      });

      it('should call highlightItemAtIndex if size > 0', () => {
        const items = [{ id: '1' }, { id: '2' }, { id: '3' }];
        const spies = mountPickerWithHighlightSpies({ items })
          .highlightItemBelow();

        expect(spies.highlightItemAtIndexSpy.called).to.equal(true);
      });

      it('should call with index plus 1 if highlightItemIndex is >= 0', () => {
        const items = [{ id: '1' }, { id: '2' }, { id: '3' }];
        const spies = mountPickerWithHighlightSpies({ items })
          //  first call is to set the index to zero, then the additional call to set to index = 1
          .highlightItemBelow(2);

        expect(spies.highlightItemAtIndexSpy.calledWith(1)).to.equal(true);
      });

      it('if highlightItemIndex is lastIndex should call with 0', () => {
        const items = [{ id: '1' }, { id: '2' }, { id: '3' }];
        const spies = mountPickerWithHighlightSpies({ items })
          //  first call is to set the index to zero, then the additional 3 call to set to index = 0
          .highlightItemBelow(3);

        expect(spies.highlightItemAtIndexSpy.lastCall.calledWith(0)).to.equal(true);
      });

      it('should select first item in items list if selected item is highlighted', () => {
        const items = [{ id: '1' }, { id: '2' }, { id: '3' }];
        //  first call is to set the index to zero, then two additional calls to set to index = 1
        const spies = mountPickerWithHighlightSpies({ items })
          .selectItem(items[1])
          .highlightSelectedItemAtIndex(0)
          .highlightItemBelow();

        expect(spies.highlightItemAtIndexSpy.calledWith(0)).to.equal(true);
      });
    });

    describe('selectHighlightedItem', () => {
      it('Should call selectItem when highlighted index is not null', () => {
        const items = [{ id: '1' }, { id: '2' }, { id: '3' }];
        const spies = mountPickerWithHighlightSpies({ items })
          .selectHighlightedItem();

        expect(spies.selectItemSpy.calledWith({ id: '1' })).to.equal(true);
      });

      it('Should do nothing when highlightItem is null in an empty item list', () => {
        const items = [];
        const spies = mountPickerWithHighlightSpies({ items })
          .selectHighlightedItem();

        expect(spies.selectItemSpy.called).to.equal(false);
      });
    });

    describe('Highlighting Selected Items', () => {
      describe('Selected Item Focus', () => {
        it('should highlight selected item', () => false);
        it('should highlight item when up or down is selected', () => false);
      });
    });

    describe('Highlighted Actions', () => {
      describe('performHighlightedAction', () => {
        it('should select items when an item is highlighted', () => {
          const items = [{ id: '1' }, { id: '2' }, { id: '3' }];
          const spies = mountPickerWithHighlightSpies({ items })
            .performHighlightedAction();

          expect(spies.selectItemSpy.calledWith({ id: '1' })).to.equal(true);
        });

        it('should remove a selected item when a selected item is highlighted', async () => {
          const items = [{ id: '1' }, { id: '2' }, { id: '3' }];
          const spies = mountPickerWithHighlightSpies({ items })
            .selectItem(items[0]);

          await waitUntil(() => spies.picker.getSelections().length > 0);
          expect(spies.picker.getSelections().length).to.equal(1);

          spies
            .highlightSelectedItemAtIndex(0)
            .performHighlightedAction();

          await waitUntil(() => spies.picker.getSelections().length === 0, 500);
          expect(spies.picker.getSelections().length).to.equal(0);
        });
      });
    });
  });
});
