import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import { MessageWrapper } from './picker.styled';

const messages = defineMessages({
  noResults: {
    id: 'no-results-txt',
    defaultMessage: 'No results',
  },
});
class EmptyResultsMessageImpl extends React.Component<InjectedIntlProps> {
  public render() {
    const {
      intl: { formatMessage },
    } = this.props;

    return (
      <MessageWrapper>
        {formatMessage(messages.noResults)}
      </MessageWrapper>
    );
  }
}

export const EmptyResultsMessage = injectIntl<{}>(EmptyResultsMessageImpl);
