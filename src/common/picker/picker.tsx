import * as debounce from 'lodash.debounce';
import * as React from 'react';

import AkFieldBase, { Label as AkLabel } from '@atlaskit/field-base';
import { ItemGroup as AkItemGroup } from '@atlaskit/item';
import AkModalDialog, { ModalTransition as AkModalTransition } from '@atlaskit/modal-dialog';
import AkSpinner from '@atlaskit/spinner';
import AkTagGroup from '@atlaskit/tag-group';

import { isElementVisible } from '../../utilities/visible';
import { EmptyResultsMessage } from './picker-empty-message';
import { PickerTheme } from './picker-theme';

import {
  BodyWrapper,
  FooterWrapper,
  FormWrapper,
  Input,
  InputBaseWrapper,
  ResultsWrapper,
  ScrollableWrapper,
  SpinnerWrapper,
} from './picker.styled';

export interface Item {
  id: string | number;
}

export interface PickerProps {
  title: string;
  isOpen: boolean;
  inputLabelText: string;
  placeholderText: string;
  items: Item[];
  maxLength?: number;
  loading?: boolean;
  errorMessage?: React.ReactNode;
  autoFocus?: boolean;
  getSelectionsCallback?(items: Item[]): void;
  onSelect?(item: Item): void;
  onRemove?(item: Item): void;
  renderItem(
    item: Item,
    isHighlighted: boolean,
    isSelected: boolean,
    select: (item: Item) => any,
  ): React.ReactNode;
  renderItemPreview(item: Item, unselect: (item: Item) => any): React.ReactNode;
  renderFooter?(getSelectedItems: () => Item[]): React.ReactNode;
  onQueryChange(query: string, selectedItems: Item[]): void;
  ref?(picker: React.ReactNode): void;
  onDialogDismissed?(): void;
}

const enum HighlightedItemType {
  Item,
  SelectedItem,
}

const enum HighlightDirection {
  Above,
  Below,
}

interface HighlightedItem {
  type: HighlightedItemType;
  index: number | null;
}

interface PickerState {
  highlighted: HighlightedItem | null;
  selectedItems: Item[];
  searchQuery: string;
  listContainerScrollHeight: number;
  listContainerClientHeight: number;
}

const defaultFireRateLimits: FireRateLimits = {
  SELECT: {
    ms: 50,
  },
  REMOVE: {
    ms: 50,
  },
  QUERY_DIRECTORY: {
    ms: 100,
  },
};

interface FireRateLimits {
  SELECT: {
    ms: number;
  };
  REMOVE: {
    ms: number;
  };
  QUERY_DIRECTORY: {
    ms: number;
  };
}

export interface SelectedItemProps<T> {
  item: T;
  unselect(item: T): any;
}

export interface SuggestionItemProps<T> {
  item: T;
  isHighlighted: boolean;
  select(item: T): any;
}

// tslint:disable-next-line:no-empty
const noop = () => {};

export class Picker extends React.Component<PickerProps, PickerState> {
  public static defaultProps: Partial<PickerProps> = {
    isOpen: false,
    items: [],
    maxLength: NaN,
    loading: false,
    errorMessage: null,
    autoFocus: true,
    renderFooter: _ => null,
  };

  public readonly state: Readonly<PickerState> = {
    highlighted: null,
    selectedItems: [],
    searchQuery: '',
    listContainerScrollHeight: 0,
    listContainerClientHeight: 0,
  };

  public selectItem;
  public remove;
  public queryDirectory;

  private searchInput: any = null;
  private itemListRef: any = null;
  private itemListDomRef?: HTMLElement[];
  private itemListScrollContainer?: HTMLElement;

  constructor(props) {
    super(props);

    this.selectItem = debounce(this._selectItem, defaultFireRateLimits.SELECT);
    this.remove = debounce(this._remove, defaultFireRateLimits.REMOVE);
    this.queryDirectory = debounce(this._queryDirectory, defaultFireRateLimits.QUERY_DIRECTORY);
  }

  public static getDerivedStateFromProps(nextProps: PickerProps, prevState: PickerState): PickerState {
    return {
      ...prevState,
      selectedItems: nextProps.isOpen ? prevState.selectedItems : [],
    };
  }

  public componentDidMount() {
    if (this.searchInput && this.props.autoFocus) {
      this.searchInput.focus();
    }
    if (this.props.items.length > 0) {
      this.highlightItemAtIndex(0);
    }
  }

  public componentDidUpdate() {
    if (!this.itemListRef) {
      return;
    }

    if (this.state.listContainerScrollHeight !== 0 &&
        this.state.listContainerClientHeight !== 0
    ) {
      return;
    }

    const clientHeight = this.itemListRef.clientHeight;
    const scrollHeight = this.itemListRef.scrollHeight;

    if (scrollHeight === 0 || clientHeight === 0) {
      return;
    }

    this.handleItemListRef();
    this.setState({
      listContainerScrollHeight: scrollHeight,
      listContainerClientHeight: clientHeight,
    });
  }

  public focusSearchInput = () => {
    this.searchInput.focus();
    this.forceUpdate();
  };

  public highlightItemAbove = () => {
    this.highlightItem(HighlightDirection.Above);
  }

  public highlightItemBelow = () => {
    this.highlightItem(HighlightDirection.Below);
  }

  public performHighlightedAction = () => {
    const highlightedItemType = this.getCurrentHighlightedItemType();
    if (highlightedItemType === HighlightedItemType.Item) {
      this.selectHighlightedItem();
    }
    if (highlightedItemType === HighlightedItemType.SelectedItem) {
      this.removeHighlightedItem();
    }
  }

  public getSelections = () => {
    return this.state.selectedItems;
  };

  public highlightItemAtIndex(index) {
    this.scrollItemIntoViewIfNeeded(this.getCurrentHighlightedItemIndex(), index);
    this.highlightAtIndex(HighlightedItemType.Item, index);
  }

  public highlightSelectedItemAtIndex(index) {
    this.highlightAtIndex(HighlightedItemType.SelectedItem, index);
  }

  public selectHighlightedItem() {
    const index = this.getCurrentHighlightedItemIndex();
    if (index == null || this.props.items[index] == null) {
      return;
    }
    this.selectItem(this.props.items[index]);
  }

  public render() {
    if (!this.props.isOpen) {
      return null;
    }

    return (
      <AkModalTransition>
        <div
          onKeyDown={this.handleKeyPress}
          role={this.props.title}
        >
          <AkModalDialog
            width="small"
            heading={this.props.title}
            components={{
              Footer: () => <FooterWrapper>{this.renderFooter()}</FooterWrapper>,
            }}
            onClose={this.props.onDialogDismissed || noop}
            scrollBehavior="outside"
          >
            <BodyWrapper>
              <FormWrapper
                autoComplete="off"
                onSubmit={this.handleSubmit}
              >
                {this.renderInput()}
              </FormWrapper>
              <ResultsWrapper>
                {this.renderResults()}
              </ResultsWrapper>
            </BodyWrapper>
          </AkModalDialog>
        </div>
      </AkModalTransition>
    );
  }

  private handleKeyPress = e => {
    if (e.keyCode === 13) {
      this.handleEnterKey(e);

      return;
    }

    if (e.keyCode === 8 || e.keyCode === 46) {
      this.handleBackspace(e);

      return;
    }

    if (e.keyCode === 38) {
      this.handleUpKey(e);

      return;
    }

    if (e.keyCode === 40) {
      this.handleDownKey(e);
    }
  }

  private handleEnterKey = e => {
    e.preventDefault();
    this.performHighlightedAction();
  };

  private highlightItem = (direction: HighlightDirection) => {
    this.focusSearchInput();

    const size = this.props.items.length;
    if (size === 0) {
      return;
    }

    if (this.getCurrentHighlightedItemType() !== HighlightedItemType.Item) {
      this.highlightItemAtIndex(0);

      return;
    }

    const selectedIndex = this.getCurrentHighlightedItemIndex();
    const lastIndex = size - 1;

    if (selectedIndex == null || selectedIndex - 1 > size) {
      // When selectedIndex is null, nothing is selected, and we can safely highlight the first item (not caring about Above/Below here for simplicity)
      // Not really sure when (selectedIndex - 1 > size) condition would be true, but the code this was copied from had it, so we'll keep it in for now. The best guess here is that when you hit "Enter" when the last item was highlighted, the array shrinks and you get this condition.
      this.highlightItemAtIndex(0);

      return;
    }

    if (direction === HighlightDirection.Above) {
      this.highlightItemAtIndex(
        selectedIndex === 0 ? lastIndex : selectedIndex - 1,
      );
    } else {
      this.highlightItemAtIndex(
        (!selectedIndex && selectedIndex !== 0) || selectedIndex === lastIndex
          ? 0
          : selectedIndex + 1,
      );
    }
  }

  private highlightAtIndex(type: HighlightedItemType, index: number | null) {
    this.setState({
      highlighted: {
        type,
        index,
      },
    });
  }

  private handleUpKey = e => {
    e.preventDefault();
    this.highlightItemAbove();
  }

  private handleDownKey = e => {
    e.preventDefault();
    this.highlightItemBelow();
  }

  private handleInputRef = ref => {
    this.searchInput = ref;
  };

  private scrollItemIntoViewIfNeeded = (currentIndex: number | null, newIndex: number): void => {
    if (currentIndex == null) {
      return;
    }
    if (!this.itemListDomRef || !this.itemListScrollContainer || this.itemListDomRef.length === 0 || this.itemListDomRef[newIndex] == null) {
      return;
    }

    const item = this.itemListDomRef[newIndex];
    if (isElementVisible(item, this.itemListScrollContainer)) {
      return;
    }
    const ScrollAlign = {
      Top: true,
      Bottom: false,
    };
    if (newIndex < currentIndex) {
      item.scrollIntoView(ScrollAlign.Top);
    } else {
      item.scrollIntoView(ScrollAlign.Bottom);
    }
  }

  private isSearchActiveElement() {
    return this.searchInput === document.activeElement;
  }

  private isSelected(item: Item): boolean {
    return this.state.selectedItems.map(i => i.id).includes(item.id);
  }

  private isHighlighted(item: Item, index: number): boolean {
    return index === this.getCurrentHighlightedItemIndex() ||
      this.state.selectedItems.map(i => i.id).indexOf(item.id) !== -1;
  }

  private handleSubmit = e => {
    e.preventDefault();
  };

  private handleBackspace = e => {
    if (this.getCurrentHighlightedItemType() === HighlightedItemType.SelectedItem) {
      this.removeHighlightedItem();

      return;
    }
    const val = e.target.value;

    const { selectedItems } = this.state;

    if (selectedItems && selectedItems.length && !val) {
      e.preventDefault();
      this.remove(selectedItems[selectedItems.length - 1]);
    }
  };

  private _remove = (item: Item) => {
    this.setState({
      selectedItems: this.state.selectedItems.filter(i => i.id !== item.id),
    });
    if (this.props.onRemove) {
      this.props.onRemove(item);
    }
  };

  private _queryDirectory = (query: string, selectedItems: Item[]) => {
    this.props.onQueryChange(query, selectedItems);
  };

  private _selectItem = (item: Item) => {
    const { selectedItems } = this.state;
    const itemsLength = selectedItems.length;
    if (!this.props.maxLength || isNaN(this.props.maxLength) || itemsLength < this.props.maxLength) {
      this.setState({
        selectedItems: [...this.state.selectedItems, item],
      });
      if (this.props.onSelect) {
        this.props.onSelect(item);
      }
    }
    this.focusSearchInput();
    this.setState({ searchQuery: '' });
    this.props.onQueryChange('', this.state.selectedItems);
  };

  private onChange = e => {
    this.setState({ searchQuery: e.target.value });
    this.queryDirectory(e.target.value, this.getSelections());
  };

  private renderInput() {
    const {
      renderItemPreview,
      inputLabelText,
      placeholderText,
    } = this.props;

    return (
      <InputBaseWrapper role="button" onClick={this.focusSearchInput}>
        <AkLabel
          isFirstChild={true}
          label={inputLabelText}
        />
        <AkFieldBase
          isFocused={this.isSearchActiveElement()}
          isFitContainerWidthEnabled={true}
        >
          <ScrollableWrapper>
            <AkTagGroup>
              {this.state.selectedItems.map((item, index) =>
                // tslint:disable-next-line react-this-binding-issue
                <span key={item.id} onFocus={() => this.onSelectedItemFocus(index)}>
                  {renderItemPreview(item, () => this.remove(item))}
                </span>,
              )}
              <Input
                innerRef={this.handleInputRef}
                placeholder={
                  this.state.selectedItems.length > 0
                    ? ''
                    : placeholderText
                }
                spellCheck={false}
                onChange={this.onChange}
                type="text"
                autoFocus={true}
                value={this.state.searchQuery}
              />
            </AkTagGroup>
          </ScrollableWrapper>
        </AkFieldBase>
      </InputBaseWrapper>
    );
  }

  private renderResults() {
    const { errorMessage, loading, items, renderItem } = this.props;

    if (errorMessage) {
      return errorMessage;
    }

    if (loading) {
      return (
        <SpinnerWrapper>
          <AkSpinner />
        </SpinnerWrapper>
      );
    }

    if (items.length === 0) {
      return <EmptyResultsMessage />;
    }

    const renderedItems = items
      .filter((item: Item, index: number) => (
        renderItem(item, this.isHighlighted(item, index), this.isSelected(item), () => this.selectItem(item) != null)
      ))
      .map((item: Item, index: number) => {
        return (
          // tslint:disable-next-line react-this-binding-issue
          <div key={item.id} onFocus={() => this.onSuggestionItemFocus(index)}>
            {renderItem(item, this.isHighlighted(item, index), this.isSelected(item), () => this.selectItem(item))}
          </div>
        );
      });

    return (
      <PickerTheme>
        <AkItemGroup>
          <div ref={this.setItemListRef}>
            {renderedItems}
          </div>
        </AkItemGroup>
      </PickerTheme>
    );
  }

  private setItemListRef = (el: HTMLDivElement) => {
    this.itemListRef = el;
  }

  private handleItemListRef = () => {
    const getScrollParent = (node: HTMLElement | null | undefined): HTMLElement | undefined => {
      if (!node) {
        return undefined;
      }

      if (node.scrollHeight > node.clientHeight) {
        return node;
      } else {
        return getScrollParent(node.parentElement);
      }
    };
    const parent = getScrollParent(this.itemListRef);
    this.itemListScrollContainer = parent;
    this.itemListDomRef =
      (this.itemListRef && this.itemListRef.children && this.itemListRef.children.length) ?
        Array.from(this.itemListRef.children) :
        undefined;
  }

  private onSuggestionItemFocus(index) {
    this.highlightItemAtIndex(index);
  }

  private onSelectedItemFocus(index) {
    this.highlightSelectedItemAtIndex(index);
  }

  private removeHighlightedItem() {
    const index = this.getCurrentHighlightedItemIndex();
    if (index == null || this.state.selectedItems[index] == null) {
      return;
    }
    this.remove(this.state.selectedItems[index]);
  }

  private getCurrentHighlightedItemType = (): HighlightedItemType | null => {
    if (this.state.highlighted) {
      return this.state.highlighted.type;
    }

    return null;
  };

  private getCurrentHighlightedItemIndex = (): number | null => {
    if (this.state.highlighted) {
      return this.state.highlighted.index;
    }

    return null;
  };

  private renderFooter(): React.ReactNode {
    const { renderFooter } = this.props;

    if (!renderFooter) {
      return undefined;
    }

    return renderFooter(this.getSelections);
  }
}
