import * as React from 'react';
import { InjectedIntlProps, injectIntl } from 'react-intl';

import { errorFlagMessages } from 'common/error';

import { MessageWrapper } from './picker.styled';

class ErrorMessageImpl extends React.Component<InjectedIntlProps> {
  public render() {
    const {
      intl: { formatMessage },
    } = this.props;

    return (
      <MessageWrapper>
        {formatMessage(errorFlagMessages.title)}
      </MessageWrapper>
    );
  }
}

export const ErrorMessage = injectIntl<{}>(ErrorMessageImpl);
