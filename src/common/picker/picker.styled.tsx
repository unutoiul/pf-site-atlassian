import styled from 'styled-components';

import {
  borderRadius as AkBorderRadius,
  colors as akColors,
  gridSize as akGridSize,
} from '@atlaskit/theme';

export const MessageWrapper = styled.div`
  padding-left: ${akGridSize}px;
  padding-bottom: ${akGridSize() * 4}px;
`;

export const Input = styled.input`
  outline: none;
  border: none;
  background: transparent;
`;

export const SpinnerWrapper = styled.div`
  padding: ${akGridSize() * 2}px;
`;

export const ScrollableWrapper = styled.div`
  max-height: ${akGridSize() * 12}px;
  overflow-y: auto;
`;

export const FooterWrapper = styled.div`
  display: block;
  text-align: end;
  margin: ${akGridSize}px;
`;

export const BodyWrapper = styled.div`
  height: ${akGridSize() * 50}px;
  display: flex;
  flex-direction: column;
`;

export const ResultsWrapper = styled.div`
  overflow-y: auto;
  padding-left: ${akGridSize}px;
  padding-right: ${akGridSize}px;
  border-radius: ${AkBorderRadius}px;
`;

export const FormWrapper = styled.form`
  width: ${akGridSize() * 45}px;
  background-color: ${akColors.N0};
  padding-bottom: ${akGridSize}px;
`;

export const InputBaseWrapper = styled.div`
  padding: 0 ${akGridSize}px ${akGridSize}px ${akGridSize}px;
  padding-bottom: 0;
`;

export const LabelWrapper = styled.div`
  margin-top: ${akGridSize() * -1.5}px;
`;
