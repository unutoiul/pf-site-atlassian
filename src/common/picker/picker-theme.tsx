import { darken, mix } from 'polished';
import * as React from 'react';
import { ThemeProvider } from 'styled-components';

import { ItemTheme as AkItemTheme, itemThemeNamespace as akItemThemeNamespace } from '@atlaskit/item';
import { colors as akColors, gridSize as akGridSize } from '@atlaskit/theme';

const generateTheme = (
  padding: number,
  baseBgColor: string,
  textColor: string,
  secondaryTextColor: string,
  focusColor?: string,
): AkItemTheme => ({
  afterItemSpacing: {
    compact: 0,
    default: 0,
  },
  beforeItemSpacing: {
    compact: 0,
    default: 0,
  },
  borderRadius: 0,
  focus: {
    outline: focusColor || '',
  },
  padding: {
    default: {
      bottom: padding,
      left: padding,
      right: padding,
      top: padding,
    },
    compact: {
      bottom: padding * 0.5,
      left: padding * 0.5,
      right: padding * 0.5,
      top: padding * 0.5,
    },
  },
  default: {
    background:  'rgba(0, 0, 0, 0)',
    text: textColor,
    secondaryText: secondaryTextColor,
  },
  hover: {
    background: darken(0.05, baseBgColor),
    text: textColor,
    secondaryText: secondaryTextColor,
  },
  selected: {
    background: darken(0.05, baseBgColor),
    text: textColor,
    secondaryText: secondaryTextColor,
  },
  active: {
    background: darken(0.1, baseBgColor),
    text: textColor,
    secondaryText: secondaryTextColor,
  },
  disabled: {
    background: baseBgColor,
    text: mix(0.5, baseBgColor, textColor),
    secondaryText: mix(0.5, baseBgColor, secondaryTextColor),
  },
});

const theme = generateTheme(
  akGridSize(),
  akColors.N0,
  akColors.N900,
  akColors.N80,
  undefined,
);

export class PickerTheme extends React.Component {
  public render() {
    const { children } = this.props;

    return (
      <ThemeProvider theme={{ [akItemThemeNamespace]: theme }}>
        {children}
      </ThemeProvider>
    );
  }
}
