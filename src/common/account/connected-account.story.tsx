import { storiesOf } from '@storybook/react';
import * as React from 'react';
import ApolloProvider from 'react-apollo/ApolloProvider';
import { BrowserRouter as Router } from 'react-router-dom';

import { createApolloClient } from '../../apollo-client';
import { generateUserId, writeUserDetailsQuery } from '../../site/user-connected-apps/user-connected-apps-mock-data-utils';
import { createMockIntlProp } from '../../utilities/testing';
import { ConnectedAccount, ConnectedUserDetailsImpl } from './connected-account';

const cloudId = 'DUMMY_CLOUD_ID';
const pageDefaults = {
  intl: createMockIntlProp(),
  history: { replace: () => null } as any,
  match: { params: { cloudId } } as any,
  location: window.location as any,
  cloudId,
  userId: '1234',
};

const CreateTestComponent = ({ client, userId }) => {
  return (
    <ApolloProvider client={client}>
      <Router>
        <ConnectedAccount {...pageDefaults} userId={userId} />
      </Router>
    </ApolloProvider>
  );
};

storiesOf('Common|Connected Account', module)
  .add('Standard User', () => {
    // tslint:disable-next-line no-console
    const client = createApolloClient(console.log.bind(console));
    writeUserDetailsQuery(client, cloudId, 'First User', true, true);

    return (
      <CreateTestComponent client={client} userId={generateUserId('First User')} />
    );
  })
  .add('Inactive User', () => {
    // tslint:disable-next-line no-console
    const client = createApolloClient(console.log.bind(console));
    writeUserDetailsQuery(client, cloudId, 'Second User', true, false);

    return (
      <CreateTestComponent client={client} userId={generateUserId('Second User')} />
    );
  })
  .add('Loading', () => {
    return (
      <ConnectedUserDetailsImpl
        userId={generateUserId('Error User')}
        data={{ loading: true, error: null, networkStatus: '200' } as any}
        cloudId="2342"
        intl={{ formatMessage: ({ defaultMessage }) => defaultMessage || '' } as any}
      />
    );
  })
  .add('No user found ', () => {
    // tslint:disable-next-line no-console
    const client = createApolloClient(console.log.bind(console));
    writeUserDetailsQuery(client, cloudId, 'Removed User', false);

    return (
      <CreateTestComponent client={client} userId={generateUserId('Removed User')} />
    );
  })
  .add('Error loading user ', () => {
    return (
      <ConnectedUserDetailsImpl
        userId={generateUserId('Error User')}
        intl={createMockIntlProp()}
        data={{ loading: false, error: { message: 'Error!' }, networkStatus: '404' } as any}
        cloudId="2342"
      />
    );
  });
