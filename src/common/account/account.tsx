import * as React from 'react';

import AkAvatar from '@atlaskit/avatar';

import { getAvatarUrlByUserId } from 'common/avatar';

import {
  AdditionalContent,
  Container,
  Content,
  IconContainer,
  PrimaryText,
  PrimaryTextContainer,
  SecondaryText,
  TextWithContent,
} from './account.styled';

export interface AccountProps {
  id: string;
  email: string;
  displayName: string;
  showAvatar?: boolean;
  isDisabled?: boolean;
  lozenges?: React.ReactNode[];
  icons?: React.ReactNode[];
  children?: never;
}

export class Account extends React.Component<AccountProps> {
  public static defaultProps: Partial<AccountProps> = {
    showAvatar: true,
  };

  public render() {
    const {
      displayName,
      email,
      isDisabled,
      lozenges = [],
      icons = [],
      id,
      showAvatar,
    } = this.props;

    const hasAdditionalContent = lozenges.length > 0 || icons.length > 0;

    return (
      <Container>
        {showAvatar && (
          <AkAvatar src={getAvatarUrlByUserId(id)} size="medium" isDisabled={isDisabled} />
        )}
        <Content>
          <PrimaryTextContainer>
            <TextWithContent>
              <PrimaryText isDisabled={isDisabled}>{displayName}</PrimaryText>
              {hasAdditionalContent && (
                <AdditionalContent>
                  {lozenges}
                  {icons.map((icon, index) => <IconContainer key={index}>{icon}</IconContainer>)}
                </AdditionalContent>
              )}
            </TextWithContent>
          </PrimaryTextContainer>
          <SecondaryText>{email}</SecondaryText>
        </Content>
      </Container>
    );
  }
}
