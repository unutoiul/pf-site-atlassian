import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import { Link } from 'react-router-dom';

import AkLozenge from '@atlaskit/lozenge';

import { ConnectedAccountQuery } from '../../schema/schema-types';
import { util } from '../../utilities/admin-hub';
import { Account } from './account';
import { AccountLoading } from './account-loading';
import userDetailsQuery from './connected-account.query.graphql';

interface ConnectedAccountOwnProps {
  cloudId: string;
  userId: string;
}

type ConnectedAccountProps = ChildProps<ConnectedAccountOwnProps, ConnectedAccountQuery>;

const withUserDetails = graphql<ConnectedAccountProps, ConnectedAccountQuery>(userDetailsQuery, {
  options: ({ cloudId, userId }) => ({
    variables: {
      cloudId,
      id: userId,
    },
  }),
});

const messages = defineMessages({
  formerUserName: {
    id: 'user-management.site-settings.user-connected-app-user-details.formerUserName',
    description: 'Name of a user who has been deleted or removed from the site',
    defaultMessage: 'Former user',
  },
  errorLoadingUser: {
    id: 'user-management.site-settings.user-connected-app-user-details.errorLoadingUser',
    description: 'Message to display instead of the user name when the user could not be loaded',
    defaultMessage: 'Error loading user',
  },
  deletedOrRemovedLabel: {
    id: 'user-management.site-settings.user-connected-app-user-details.deletedOrRemovedLabel',
    description: 'Label indicating the user has been deleted or removed',
    defaultMessage: 'Removed',
  },
  errorLoadingUserLabel: {
    id: 'user-management.site-settings.user-connected-app-user-details.errorLoadingUserLabel',
    description: 'Label indicating the user could not be loaded',
    defaultMessage: 'Error',
  },
});

export const ConnectedUserDetailsImpl: React.SFC<ConnectedAccountProps & InjectedIntlProps> = ({ data, cloudId, intl: { formatMessage } }) => {
  if (!data || data.loading) {
    return <AccountLoading />;
  }

  if (data && data.error) {
    return (
      <Account
        id="error-loading-user"
        email=""
        displayName={formatMessage(messages.errorLoadingUser)}
        isDisabled={true}
        lozenges={[
          <AkLozenge appearance="removed" key="error-loading-user-lozenge">
            <FormattedMessage {...messages.errorLoadingUserLabel} />
          </AkLozenge>,
        ]}
      />
    );
  }

  if (!data.user) {
    return (
      <Account
        id="unknown-user"
        email=""
        displayName={formatMessage(messages.formerUserName)}
        isDisabled={true}
        lozenges={[
          <AkLozenge appearance="default" key="deleted-user-lozenge">
            <FormattedMessage {...messages.deletedOrRemovedLabel} />
          </AkLozenge>,
        ]}
      />
    );
  }

  return (
    <Link to={`${util.siteAdminBasePath}/s/${cloudId}/users/${data.user.userDetails.id}`} style={{  textDecoration: 'none' }}>
      <Account
        id={data.user.userDetails.id}
        email={data.user.userDetails.email}
        displayName={data.user.userDetails.displayName}
        isDisabled={!data.user.userDetails.active}
      />
    </Link>
  );
};

export const ConnectedAccount = withUserDetails(injectIntl(ConnectedUserDetailsImpl));
