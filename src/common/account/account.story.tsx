// tslint:disable jsx-use-translation-function react-this-binding-issue
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import styled from 'styled-components';

import AkButton from '@atlaskit/button';
import AkWarningIcon from '@atlaskit/icon/glyph/warning';
import AkLozenge from '@atlaskit/lozenge';
import AkTooltip from '@atlaskit/tooltip';

import { Account } from './account';
import { AccountLoading } from './account-loading';

const Container = styled.div`
  width: 400px;
  margin-bottom: 10px;
`;

const user = {
  displayName: 'John Doe',
  email: 'john.doe@acme.com',
  id: '557058:43d09406-ff6e-41ed-9aae-fca16196e32b',
};
const userWithLongName = {
  ...user,
  displayName: 'I told you all this before, when you have a swimming pool do not use chlorine, use salt water, the healing, salt water is the healing.',
  email: 'reallyreallyreallyreallyreallyreallyreallyreallyreallyreallyreallyreallylongemail@acme.com',
};

storiesOf('Common|Account', module)
  .add('Examples', () => {
    class Example extends React.Component<{}, { loading: boolean }> {
      public state = {
        loading: true,
      };

      public render() {
        return (
          <div style={{ padding: '20px' }}>
            <p>Basic usage:</p>
            <Container>
              <Account {...user} />
            </Container>
            <p>With long texts:</p>
            <Container>
              <Account {...userWithLongName} />
            </Container>
            <p>With lozenge:</p>
            <Container>
              <Account
                {...userWithLongName}
                lozenges={[
                  <AkLozenge key="teammate">Teammate</AkLozenge>,
                ]}
              />
            </Container>
            <p>Disabled state:</p>
            <Container>
              <Account
                {...userWithLongName}
                isDisabled={true}
                lozenges={[
                  <AkLozenge key="disabled">Disabled</AkLozenge>,
                ]}
                icons={[
                  <AkTooltip key="warning" content="This user has multi-factor authentication disabled">
                    <AkWarningIcon label="" />
                  </AkTooltip>,
                ]}
              />
            </Container>
            <p>Loading state:</p>
            <Container>
              {this.state.loading
                ? <AccountLoading />
                : (<Account
                  {...userWithLongName}
                  lozenges={[
                    <AkLozenge key="teammate">Teammate</AkLozenge>,
                  ]}
                  icons={[
                    <AkTooltip key="warning" content="This user has multi-factor authentication disabled">
                      <AkWarningIcon label="" />
                    </AkTooltip>,
                  ]}
                />)}
              <AkButton onClick={() => this.setState({ loading: !this.state.loading })}>Toggle loading</AkButton>
            </Container>
          </div>
        );
      }
    }

    return <Example />;
  })
  .add('Loading performance', () => (
    <div>
      {Array(100).fill(null).map((_, i) => <AccountLoading key={i} />)}
    </div>
  ));
