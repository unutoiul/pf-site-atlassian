import * as React from 'react';

import {
  fontSize as akFontSize,
} from '@atlaskit/theme';
import { akGridSizeUnitless } from '@atlaskit/util-shared-styles';

import { OscillatingBlock } from '../loading';
import { Container, Content, PrimaryTextContainer, SecondaryText, secondaryTextRelativeSize } from './account.styled';

export interface AccountLoadingProps {
  children?: never;
}

const secondaryTextBlockHeight = Math.floor(akFontSize() * secondaryTextRelativeSize);

export class AccountLoading extends React.Component<AccountLoadingProps> {
  public render() {
    return (
      <Container>
        <div>
          <OscillatingBlock
            width={`${akGridSizeUnitless * 4}px`}
            heightPx={akGridSizeUnitless * 4}
            rounded={true}
            borderWidthPx={akGridSizeUnitless * 0.25}
          />
        </div>
        <Content>
          <PrimaryTextContainer>
            <OscillatingBlock heightPx={akGridSizeUnitless * 2} />
          </PrimaryTextContainer>
          <SecondaryText>
            <OscillatingBlock heightPx={secondaryTextBlockHeight} width="60%" />
          </SecondaryText>
        </Content>
      </Container>
    );
  }
}
