import styled from 'styled-components';

import {
  colors as akColors,
  fontSize as akFontSize,
  gridSize as akGridSize,
} from '@atlaskit/theme';

export const Container = styled.div`
  align-items: center;
  box-sizing: border-box;
  color: inherit;
  display: flex;
  font-size: inherit;
  font-style: normal;
  font-weight: normal;
  line-height: 1;
  margin: 0;
  padding: ${akGridSize() / 2}px;
  text-align: left;
  text-decoration: none;
  width: 100%;
`;

export const Truncate = styled.div`
  overflow-x: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
`;

const contentLineHeight = 1.4;

export const Content = styled.div`
  max-width: 100%;
  min-width: 0;
  line-height: ${contentLineHeight};
  padding-left: ${akGridSize}px;
`;

// Icon widths are not tied to grid-size. We're using the "medium" size of an icon here
export const IconContainer = styled.div`
  width: ${akGridSize() * 3}px;
  display: inline-block;
`;

// we set height so that icons display better
export const PrimaryTextContainer = styled.div`
  height: ${akFontSize() * contentLineHeight}px;
`;

export const TextWithContent = styled.div`
  display: flex;
  justify-content: flex-start;
`;

export const PrimaryText = styled<{}>(Truncate) `
  color: ${(props: { isDisabled?: boolean }) => props.isDisabled ? akColors.subtleText : akColors.text};
  flex: 1 1 100%;
`;

export const AdditionalContent = styled.div`
  display: inline-flex;
  align-items: center;
  white-space: nowrap;
  margin-left: ${akGridSize}px;
`;

export const secondaryTextRelativeSize = 0.85;

export const SecondaryText = styled(Truncate) `
  color: ${akColors.subtleText};
  font-size: ${secondaryTextRelativeSize}em;
`;
