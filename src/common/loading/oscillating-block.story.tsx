// tslint:disable jsx-use-translation-function react-this-binding-issue
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import styled from 'styled-components';

import { Checkbox as AkCheckbox } from '@atlaskit/checkbox';
import AkFieldRange from '@atlaskit/field-range';

import { OscillatingBlock, OscillatingBlockProps } from './oscillating-block';

const Container = styled.div`
  margin: 50px;
`;

storiesOf('Common|Oscillating Block', module)
  .add('Overwhelming example', () => {
    class Example extends React.Component<{}, OscillatingBlockProps> {
      public state = {
        borderWidthPx: 0,
        heightPx: 40,
        width: '50px',
        rounded: false,
      };

      public render() {
        return (
          <Container>
            <p>
              You would normally just use <code>{'<OscillatingBlock />'}</code> component:
            </p>
            <OscillatingBlock {...this.state} />
            <div>
              <label>Border width = <code>"{this.state.borderWidthPx}px"</code></label>
              <AkFieldRange
                value={this.state.borderWidthPx}
                min={0}
                max={10}
                step={1}
                onChange={(value: number) => this.setState({ borderWidthPx: value })}
              />
            </div>
            <div>
              <label>Height = <code>"{this.state.heightPx}px"</code></label>
              <AkFieldRange
                value={this.state.heightPx}
                min={10}
                max={100}
                step={1}
                onChange={(value: number) => this.setState({ heightPx: value })}
              />
            </div>
            <div>
              <label>Width = <code>"{this.state.width}"</code></label>
              <AkFieldRange
                value={parseInt(this.state.width, 10)}
                min={10}
                max={1000}
                step={1}
                onChange={(value: number) => this.setState({ width: `${value}px` })}
              />
            </div>
            <div>
              <AkCheckbox
                value="rounded"
                label="Rounded"
                onChange={({ isChecked }) => this.setState({ rounded: isChecked })}
                name="checkbox-rounded"
              />
            </div>
          </Container>
        );
      }
    }

    return <Example />;
  });
