import * as React from 'react';
import styled from 'styled-components';

import {
  akColorN0,
  akColorN20,
  akColorN30,
  akGridSizeUnitless,
} from '@atlaskit/util-shared-styles';

const loadingIndicatorPathWidth = akGridSizeUnitless * 50;

// Start and end have "paddings" to account for gradient background
const loadingIndicatorPathStart = akGridSizeUnitless * -25;
const loadingIndicatorPathEnd = akGridSizeUnitless * 25;

const OscillationBase = styled.span`
  animation: oscillate 1s linear infinite;
  background: linear-gradient(to right, ${akColorN20} 8%, ${akColorN30} 18%, ${akColorN20} 33%);
  background-size: ${loadingIndicatorPathWidth}px;
  display: inline-block;
  font-size: 0;
  line-height: 0;
  vertical-align: middle;

  @keyframes oscillate {
    0% {
      background-position: ${loadingIndicatorPathStart}px 0;
    }
    100% {
      background-position: ${loadingIndicatorPathEnd}px 0;
    }
  }
`;

const Container = styled.div`
  max-width: ${loadingIndicatorPathWidth}px;
  overflow-x: hidden;
`;

const OscillatingBlockStyled = styled<OscillatingBlockProps>(OscillationBase as any) `
  ${({ borderWidthPx }) => borderWidthPx
    ? `border: ${borderWidthPx}px solid ${akColorN0}`
    : ''};
  ${({ rounded }) => rounded ? 'border-radius: 50%' : ''};
  height: ${({ heightPx }) => `${heightPx || (akGridSizeUnitless * 2.5)}px`};
  width: ${({ width }) => width || '80%'};
`;

export interface OscillatingBlockProps {
  borderWidthPx?: number;
  heightPx?: number;
  rounded?: boolean;
  width?: string;
}

export const OscillatingBlock: React.StatelessComponent<OscillatingBlockProps> = (props) => (
  <Container>
    <OscillatingBlockStyled {...props} />
  </Container>
);
