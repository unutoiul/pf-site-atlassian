import * as React from 'react';
import { InjectedIntlProps, injectIntl } from 'react-intl';
import { LoadingComponentProps } from 'react-loadable';

import { createErrorIcon, errorFlagMessages } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';

export const PageLoadingImpl: React.SFC<LoadingComponentProps & FlagProps & InjectedIntlProps> = (props) => {
  if (props.error || props.timedOut) {
    props.showFlag({
      autoDismiss: true,
      icon: createErrorIcon(),
      id: 'react-loadable-error',
      title: props.intl.formatMessage(errorFlagMessages.title),
      description: props.intl.formatMessage(errorFlagMessages.description),
    });

    return null;
  } else if (props.pastDelay) {
    // Currently we're rendering a blank page for the loading state
    return null;
  }

  return null;
};

export const PageLoading = withFlag(injectIntl(PageLoadingImpl));
