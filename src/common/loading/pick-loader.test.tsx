import { expect } from 'chai';
import { mount } from 'enzyme';
import { object } from 'prop-types';
import * as React from 'react';
import { IntlProvider } from 'react-intl';
import * as sinon from 'sinon';

import { hideFlagContextKey, hideFlagShape, showFlagContextKey, showFlagShape } from 'common/flag/flag-provider';

import { waitUntil } from '../../utilities/testing';
import { PickLoader } from './pick-loader';

describe('Pick loader', () => {
  const intlProvider = new IntlProvider({ locale: 'en' }, {});
  const { intl } = intlProvider.getChildContext();

  it('should load named export', async () => {
    const MyComponent: React.SFC<{ secretProp: number }> = ({ secretProp }) => <span>{secretProp}</span>;
    const MyAsyncComponent = PickLoader(
      async () => Promise.resolve({ Component1: MyComponent }),
      lib => lib.Component1,
    );
    const spy = sinon.spy();

    const wrapper = mount(<MyAsyncComponent secretProp={42} />, {
      context: {
        [hideFlagContextKey]: spy,
        [showFlagContextKey]: spy,
        intl,
      },
      childContextTypes: {
        [showFlagContextKey]: showFlagShape,
        [hideFlagContextKey]: hideFlagShape,
        intl: object,
      },
    });

    await waitUntil(() => wrapper.update().find(MyComponent).length > 0);

    const component = wrapper.update().find(MyComponent);
    expect(component.exists()).to.equal(true);
    expect(component.props()).to.deep.equal({ secretProp: 42 });

    expect(spy.called).to.equal(false);
  });
});
