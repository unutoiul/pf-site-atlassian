import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import { errorFlagMessages } from 'common/error';

import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import { createErrorIcon } from '../error/error-icons';
import { PageLoadingImpl as LoadingStateless } from './page-loading';

const shallowLoading = (props) => {
  return shallow(
    <LoadingStateless
      showFlag={props.showFlag}
      // tslint:disable-next-line:react-this-binding-issue
      hideFlag={() => null}
      isLoading={props.isLoading || false}
      timedOut={props.timedOut || false}
      pastDelay={props.pastDelay || false}
      error={props.error || null}
      intl={createMockIntlProp()}
    />,
    createMockIntlContext(),
  );
};

const defaultErrorArgs = {
  autoDismiss: true,
  icon: createErrorIcon(),
  id: 'react-loadable-error',
  title: errorFlagMessages.title.defaultMessage,
  description: errorFlagMessages.description.defaultMessage,
};

describe('Loading component', () => {
  let sandbox;

  beforeEach(() => {
    sandbox = sinon.sandbox.create();
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('shouldn\'t render anything on success', () => {
    const wrapper = shallowLoading({
      isLoading: false,
      pastDelay: true,
      timedOut: false,
      error: false,
    });

    expect(wrapper.isEmptyRender()).to.equal(true);
  });

  it('shouldn\'t render anything on errors', () => {
    const showFlagSpy = sandbox.spy();
    const wrapper = shallowLoading({
      error: true,
      showFlag: showFlagSpy,
    });

    expect(wrapper.isEmptyRender()).to.equal(true);
  });

  it('should show an error flag on error', () => {
    const showFlagSpy = sandbox.spy();

    shallowLoading({
      error: true,
      showFlag: showFlagSpy,
    });

    expect(showFlagSpy.calledWith(defaultErrorArgs)).to.equal(true);
  });

  it('should show an error flag on timeout', () => {
    const showFlagSpy = sandbox.spy();

    shallowLoading({
      error: false,
      timedOut: true,
      showFlag: showFlagSpy,
    });

    expect(showFlagSpy.calledWith(defaultErrorArgs)).to.equal(true);
  });
});
