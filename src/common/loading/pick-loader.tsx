import * as React from 'react';
import Loadable from 'react-loadable';

import { PageLoading } from './page-loading';

export function PickLoader<T extends object, U extends React.ComponentType>(importer: () => Promise<T>, pick: (x: T) => U): U {
  return Loadable({
    loader: importer,
    loading: PageLoading,
    render(lib, props) {
      const Component: React.ComponentType = pick(lib);

      return <Component {...props} />;
    },
  }) as any;
}
