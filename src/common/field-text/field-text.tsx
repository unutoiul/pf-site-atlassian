import * as React from 'react';

import { FieldTextStateless as AkFieldTextStateless, Props as AkFieldTextProps } from '@atlaskit/field-text';

interface State {
  value: string | undefined;
  // tslint:disable-next-line:react-unused-props-and-state it IS used in getDerivedStateFromProps
  previousValue: string | undefined;
}

// we need this sometimes until https://ecosystem.atlassian.net/projects/AK/queues/issue/AK-5511 is resolved
export class FieldTextFixed extends React.Component<AkFieldTextProps, State> {
  public readonly state: Readonly<State> = {
    value: this.props.value,
    previousValue: this.props.value,
  };

  public handleOnChange = (e: any) => {
    this.setState({ value: e.target.value });
    if (this.props.onChange) {
      this.props.onChange(e);
    }
  };

  public static getDerivedStateFromProps(nextProps: Readonly<AkFieldTextProps>, prevState: State): Partial<State> | null {
    if (prevState.previousValue !== nextProps.value) {
      return {
        previousValue: nextProps.value,
        value: nextProps.value,
      };
    }

    return null;
  }

  public render() {
    return (
      <AkFieldTextStateless
        {...this.props}
        value={this.state.value}
        onChange={this.handleOnChange}
      />
    );
  }
}
