import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { spy } from 'sinon';

import AkInlineEdit from '@atlaskit/inline-edit';

import { TextInlineEdit, TextInlineEditProps } from './text-inline-edit';

describe('TextInlineEdit', () => {
  class ComponentWrapper {
    private wrapper: ShallowWrapper<any, any>;

    constructor(props: TextInlineEditProps) {
      this.wrapper = shallow(<TextInlineEdit {...props} />);
    }

    public get props() {
      return this.inlineEdit.props();
    }

    public get readInputValue(): string {
      return this.readInput.props.value;
    }

    public updateValue(newValue: string): void {
      this.editInput.props.onChange({ target: { value: newValue } });
      this.wrapper.update();
    }

    public cancelUpdate(): void {
      this.inlineEdit.simulate('cancel');
    }

    public confirmUpdate(): void {
      this.inlineEdit.simulate('confirm');
    }

    private get editInput(): React.Component<any> {
      return this.wrapper.find<any>(AkInlineEdit).props().editView;
    }

    private get inlineEdit(): ShallowWrapper<any, any> {
      return this.wrapper.find<any>(AkInlineEdit);
    }

    private get readInput(): React.Component<any> {
      return this.wrapper.find<any>(AkInlineEdit).props().readView;
    }
  }

  it('propagates all props to `InlineEdit` component', () => {
    const wrapper = new ComponentWrapper({
      label: 'Test Label',
      id: 'test-id',
      initialValue: '',
      onSave: spy(),
      testProp: 1,
    } as any);

    expect(wrapper.props.label).to.equal('Test Label');
    expect(wrapper.props.id).to.equal('test-id');
    expect(wrapper.props.initialValue).to.equal('');
    expect(wrapper.props.testProp).to.equal(1);
  });

  it('initiates the value correctly', () => {
    const wrapper = new ComponentWrapper({
      label: 'Test Label',
      id: 'test-id',
      initialValue: 'initial value',
      onSave: spy(),
    });

    expect(wrapper.readInputValue).to.equal('initial value');
  });

  it('updates the value on input change', () => {
    const wrapper = new ComponentWrapper({
      label: 'Test Label',
      id: 'test-id',
      initialValue: 'initial',
      onSave: spy(),
    });

    wrapper.updateValue('new value');
    expect(wrapper.readInputValue).to.equal('new value');
  });

  it('reverts the value on cancel', () => {
    const wrapper = new ComponentWrapper({
      label: 'Test Label',
      id: 'test-id',
      initialValue: 'initial',
      onSave: spy(),
    });

    wrapper.updateValue('new value');
    expect(wrapper.readInputValue).to.equal('new value');

    wrapper.cancelUpdate();
    expect(wrapper.readInputValue).to.equal('initial');
  });

  it('updates value on confirm', () => {
    const wrapper = new ComponentWrapper({
      label: 'Test Label',
      id: 'test-id',
      initialValue: 'initial',
      onSave: spy(),
    });

    wrapper.updateValue('new value');
    wrapper.confirmUpdate();
    expect(wrapper.readInputValue).to.equal('new value');
  });

  it('confirmed value persists after subsequent cancels', () => {
    const wrapper = new ComponentWrapper({
      label: 'Test Label',
      id: 'test-id',
      initialValue: 'initial',
      onSave: spy(),
    });

    wrapper.updateValue('updated value');
    wrapper.confirmUpdate();
    expect(wrapper.readInputValue).to.equal('updated value');

    wrapper.updateValue('further updated value');
    expect(wrapper.readInputValue).to.equal('further updated value');

    wrapper.cancelUpdate();
    expect(wrapper.readInputValue).to.equal('updated value');
  });

  it('onSave is invoked after confirm', () => {
    const onSaveSpy = spy();
    const wrapper = new ComponentWrapper({
      label: 'Test Label',
      id: 'test-id',
      initialValue: 'initial',
      onSave: onSaveSpy,
    });

    wrapper.updateValue('new value');
    wrapper.confirmUpdate();
    expect(onSaveSpy.called).to.equal(true);
  });
});
