import * as React from 'react';

import AkInlineEdit from '@atlaskit/inline-edit';
import AkSingleLineTextInput from '@atlaskit/input';

export interface TextInlineEditProps {
  id: string;
  initialValue: string;
  label: string;

  /**
   * This component doesn't support children
   *
   * @type {never}
   * @memberof Props
   */
  children?: never;

  onSave(editValue: string): void;
}

interface State {
  editValue: string;
  readValue: string;
}

export class TextInlineEdit extends React.Component<TextInlineEditProps, State> {
  constructor(props: TextInlineEditProps, ...args) {
    super(props, ...args);

    this.state = {
      editValue: props.initialValue,
      readValue: props.initialValue,
    };
  }

  public render() {
    const { id } = this.props;

    return (
      <AkInlineEdit
        label={this.props.label}
        labelHtmlFor={id}
        editView={this.renderInput({ isEditing: true, id })}
        readView={this.renderInput({ isEditing: false, id })}
        onConfirm={this.onConfirm}
        onCancel={this.onCancel}
        {...this.props}
      />
    );
  }

  private onChange = e =>
    this.setState({ editValue: e.target.value })

  private onConfirm = () => {
    this.setState(state => ({ readValue: state.editValue }));
    this.props.onSave(this.state.editValue);
  }

  private onCancel = () => {
    this.setState(state => ({ editValue: state.readValue }));
  };

  private renderInput = ({ isEditing, id }) => (
    <AkSingleLineTextInput
      id={id}
      isEditing={isEditing}
      isInitiallySelected={true}
      value={this.state.editValue}
      onChange={this.onChange}
    />
  );
}
