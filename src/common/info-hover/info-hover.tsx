import * as React from 'react';
import styled from 'styled-components';

import AkInfoIcon from '@atlaskit/icon/glyph/info';
import AkInlineDialog from '@atlaskit/inline-dialog';
import {
  colors as akColors,
  gridSize as akGridSize,
} from '@atlaskit/theme';

export interface InfoHoverProps {
  dialogContent: JSX.Element;
  persistTime?: number;
  type?: 'onboarding' | 'info';
  hasMargin?: boolean;
}

export interface InfoHoverState {
  mouseOnInfoIcon: boolean;
  mouseOnInfoDialog: boolean;
  infoDialogOpen: boolean;
}

const IconWrapper = styled.span`
  position: relative;
`;

const BlueIcon = props => <IconWrapper><AkInfoIcon {...props} primaryColor={akColors.B400} /></IconWrapper>;
const PurpleIcon = props => <IconWrapper><AkInfoIcon {...props} primaryColor={akColors.P300} /></IconWrapper>;

const defaultPersistTime = 1000;

export class InfoHover extends React.Component<InfoHoverProps, InfoHoverState> {
  public static defaultProps: Partial<InfoHoverProps> = {
    hasMargin: true,
  };

  public readonly state: Readonly<InfoHoverState> = {
    infoDialogOpen: false,
    mouseOnInfoIcon: false,
    mouseOnInfoDialog: false,
  };

  public render() {
    const Dialog = styled.div`
      display: inline-block;
      margin: ${this.props.hasMargin ? akGridSize() / 2 : 0}px;
    `;

    return (
      <Dialog>
        <AkInlineDialog
          content={
            <div
              onMouseEnter={this.onInfoDialogMouseIn}
              onMouseLeave={this.onInfoDialogMouseOut}
            >
              {this.props.dialogContent}
            </div>
          }
          isOpen={this.state.infoDialogOpen}
          placement="right-start"
        >
          <div role="tooltip" onMouseEnter={this.onInfoIconMouseIn} onMouseLeave={this.onInfoIconMouseOut}>
            {this.props.type && this.props.type === 'info' ? <BlueIcon label="" /> : <PurpleIcon label="" />}
          </div>
        </AkInlineDialog>
      </Dialog>
    );
  }

  private onInfoIconMouseIn = () => {
    this.setState({
      mouseOnInfoIcon: true,
      infoDialogOpen: true,
    });
  };

  private onInfoIconMouseOut = () => {
    this.setState({ mouseOnInfoIcon: false });
    setTimeout(this.closeInfoDialog, this.props.persistTime === undefined ? defaultPersistTime : this.props.persistTime);
  };

  private onInfoDialogMouseIn = () => {
    this.setState({ mouseOnInfoDialog: true });
  };

  private onInfoDialogMouseOut = () => {
    this.setState({
      infoDialogOpen: false,
      mouseOnInfoDialog: false,
    });
  };

  private closeInfoDialog = () => {
    if (!this.state.mouseOnInfoDialog && !this.state.mouseOnInfoIcon) {
      this.setState({ infoDialogOpen: false });
    } else {
      setTimeout(this.closeInfoDialog, this.props.persistTime === undefined ? defaultPersistTime : this.props.persistTime);
    }
  };

}
