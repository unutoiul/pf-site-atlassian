// tslint:disable:jsx-use-translation-function
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import styled from 'styled-components';

import { InfoHover } from 'common/info-hover/info-hover';

const Wrapper = styled.div`
  margin: 25px;
  max-width: 500px;
`;

storiesOf('Common|Info Hover', module)
  .add('Info hover with dialog content - No type specified', () => {
    return (
      <Wrapper>
        <InfoHover dialogContent={<div>Hello world!</div>}/>
      </Wrapper>
    );
  })
  .add('Info hover with dialog content - Type explicity is onboarding', () => {
    return (
      <Wrapper>
        <InfoHover type="onboarding" dialogContent={<div>Hello world!</div>}/>
      </Wrapper>
    );
  }).add('Info hover with dialog content - Type explicity is info', () => {
    return (
      <Wrapper>
        <InfoHover type="info" dialogContent={<div>Hello world!</div>}/>
      </Wrapper>
    );
  });
