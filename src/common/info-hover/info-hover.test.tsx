// tslint:disable:jsx-use-translation-function
import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import AkInlineDialog from '@atlaskit/inline-dialog';

import { InfoHover, InfoHoverProps, InfoHoverState } from './info-hover';

describe('info dialog', () => {
  const PERSIST_TIME = 5000;
  let clock;

  class InfoHoverHelper {
    private wrapper: ShallowWrapper<InfoHoverProps, InfoHoverState>;

    constructor() {
      this.wrapper = shallow(<InfoHover dialogContent={<h1 data-test={true}>Saxon is cool</h1>} persistTime={PERSIST_TIME} />);
    }

    public get isInfoDialogOpen(): boolean {
      return this.wrapper.find(AkInlineDialog).prop('isOpen');
    }

    public hoverIcon(): void {
      this.infoIcon.simulate('mouseenter');
    }

    public leaveIcon(): void {
      this.infoIcon.simulate('mouseleave');
    }

    public waitShort(): void {
      clock.tick(PERSIST_TIME / 2);
      this.wrapper.update();
    }

    public waitLong(): void {
      clock.tick(PERSIST_TIME);
      this.wrapper.update();
    }

    public hoverDialogContent(): void {
      this.dialogContent.simulate('mouseenter');
    }

    public leaveDialogContent(): void {
      this.dialogContent.simulate('mouseleave');
    }

    private get infoIcon(): ShallowWrapper<any, any> {
      return this.wrapper.find('div[role="tooltip"]').at(0);
    }

    private get dialogContent(): ShallowWrapper {
      const content = this.wrapper.find(AkInlineDialog).prop('content');

      return shallow(content as any);
    }
  }

  beforeEach(() => {
    clock = sinon.useFakeTimers();
  });

  afterEach(() => {
    clock.restore();
  });

  it('mousing over info icon should bring up info dialog, mousing away should make it disappear', () => {
    const helper = new InfoHoverHelper();

    expect(helper.isInfoDialogOpen).to.equal(false);
    helper.hoverIcon();
    expect(helper.isInfoDialogOpen).to.equal(true);
    helper.leaveIcon();

    helper.waitLong();
    expect(helper.isInfoDialogOpen).to.equal(false);
  });

  it('mousing over info and then mousing over info dialog should make dialog persist', async () => {
    const helper = new InfoHoverHelper();

    expect(helper.isInfoDialogOpen).to.equal(false);
    helper.hoverIcon();
    expect(helper.isInfoDialogOpen).to.equal(true);
    helper.leaveIcon();
    helper.waitShort(); // Allow some time for cursor to leave infoIcon and reach infoDialog

    helper.hoverDialogContent();
    helper.waitLong();
    expect(helper.isInfoDialogOpen).to.equal(true);
    helper.leaveDialogContent();
    expect(helper.isInfoDialogOpen).to.equal(false);
  });
});
