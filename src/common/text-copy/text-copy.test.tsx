import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { spy } from 'sinon';

import AkButton from '@atlaskit/button';
import AkTextField from '@atlaskit/field-text';

import { userProvisioningCopyApiKeyEventData } from 'common/analytics';

import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import { OwnProps, TextCopyImpl } from './text-copy';

describe('TextCopy', () => {
  let showFlagSpy;
  let sendUIEventSpy;

  const build = (props: Partial<OwnProps> = {}) => {
    showFlagSpy = spy();
    sendUIEventSpy = spy();

    return (
      <TextCopyImpl
        analyticsClient={{ sendUIEvent: sendUIEventSpy } as any}
        label="test"
        value="1234"
        showFlag={showFlagSpy}
        hideFlag={() => null}
        analyticsUIEvent={{ data: userProvisioningCopyApiKeyEventData() }}
        intl={createMockIntlProp()}

        {...props}
      />
    );
  };

  it('should show flag when copy button clicked', () => {
    const wrapper = mount(build(), createMockIntlContext());
    wrapper.find(AkButton).simulate('click');
    expect(showFlagSpy.callCount).to.equal(1);
  });

  it('should send ui event when copy button clicked', () => {
    const wrapper = mount(build(), createMockIntlContext());
    wrapper.find(AkButton).simulate('click');
    expect(sendUIEventSpy.callCount).to.equal(1);
  });

  it('should be disabled', () => {
    const wrapper = shallow(build({ isDisabled: true }), createMockIntlContext());
    expect(wrapper.find(AkButton).prop('isDisabled')).to.equal(true);
    expect(wrapper.find(AkTextField).prop('disabled')).to.equal(true);
  });

  it('should display alternative button label', () => {
    const alternativeButtonLabel = {
      id: 'some-id',
      defaultMessage: 'some default message',
      description: 'some description',
    };
    const wrapper = shallow(build({ alternativeButtonLabel }), createMockIntlContext());
    expect(wrapper.find(FormattedMessage).prop('defaultMessage')).to.equal('some default message');
  });
});
