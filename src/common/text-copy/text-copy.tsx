import * as React from 'react';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import AkButton from '@atlaskit/button';
import AkTextField from '@atlaskit/field-text';
import AkCopyIcon from '@atlaskit/icon/glyph/copy';
import { gridSize as akGridSize } from '@atlaskit/theme';
import AkTooltip from '@atlaskit/tooltip';

import { AnalyticsClientProps, UIEvent, withAnalyticsClient } from 'common/analytics';
import { createErrorIcon, createSuccessIcon } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';

const messages = defineMessages({
  copyButtonLabel: {
    id: 'common.copyTextField.buttonLabel',
    defaultMessage: 'Copy',
    description: 'A button that copies text from a text field',
  },
  copySuccess: {
    id: 'common.copyTextField.success',
    defaultMessage: 'Success!',
    description: 'A dialog title that is shown when text is copied succesfully',
  },
  copySuccessDescription: {
    id: 'common.copyTextField.success.description',
    defaultMessage: '{label} was copied to the clipboard.',
    description: 'A dialog description that is shown when text is copied successfully. Label is replaced with the name of the value that was copied.',
  },
  copyFail: {
    id: 'common.copyTextField.fail',
    defaultMessage: 'Uh oh! Something went wrong.',
    description: 'A dialog title that is shown when text fails to copy',
  },
  copyFailDescription: {
    id: 'common.copyTextField.fail.description',
    defaultMessage: 'There was an issue copying the {label} to the clipboard.',
    description: 'A dialog description that is shown when text fails to copy. Label is replaced with the name of the value that failed to copy.',
  },
});

const Container = styled.div`
  display: flex;
`;

const Input = styled.div`
  flex-grow: 1;
  cursor: text;
`;

const Button = styled.div`
  margin-top: ${akGridSize() * 5}px;
  padding: 0 0 0 ${akGridSize() / 2}px;
  button, a:link, a:visited, a:hover, a:active  {
    height: ${akGridSize() * 5}px;
  }
`;

export interface OwnProps {
  label: string;
  value: string;
  analyticsUIEvent?: UIEvent;
  compact?: boolean;
  compactTooltipMessage?: string;
  isDisabled?: boolean;
  alternativeButtonLabel?: FormattedMessage.MessageDescriptor;
}

export class TextCopyImpl extends React.Component<OwnProps & FlagProps & InjectedIntlProps & AnalyticsClientProps> {
  public static defaultProps = {
    compact: false,
    compactTooltipMessage: '',
  };
  private ref?: HTMLFormElement;

  public render() {
    const { label, value, compact, compactTooltipMessage, isDisabled } = this.props;

    if (compact) {
      return (
        <AkTooltip content={compactTooltipMessage}>
            <AkButton iconAfter={<AkCopyIcon label={label} onClick={this.copyValue}/>} isDisabled={isDisabled}/>
        </AkTooltip>
      );
    } else {
      return (
        <Container>
          <Input>
            <AkTextField
              ref={this.setRef}
              value={value}
              label={label}
              shouldFitContainer={true}
              isReadOnly={true}
              disabled={isDisabled}
            />
          </Input>
          <Button>
            <AkButton onClick={this.copyValue} isDisabled={isDisabled}>
              <FormattedMessage {...(this.props.alternativeButtonLabel || messages.copyButtonLabel)} />
            </AkButton>
          </Button>
        </Container>
      );
    }
  }

  private setRef = (input) => {
    if (input) {
      this.ref = input;
    }
  }

  private copyValue = () => {
    let textArea;
    if (this.props.compact) {
      textArea = document.createElement('textarea');
      textArea.value = this.props.value;
      document.body.appendChild(textArea);
      textArea.select();
    } else {
      if (!this.ref) {
        return;
      }
      this.ref.input.select();
    }

    const { showFlag, label, intl: { formatMessage } } = this.props;
    this.sendAnalyticsEvent();

    if (this.copy()) {
      showFlag({
        autoDismiss: true,
        icon: createSuccessIcon(),
        id: `common.copy.text.field.success.${Date.now()}`,
        title: formatMessage(messages.copySuccess),
        description: formatMessage(messages.copySuccessDescription, { label }),
      });
    } else {
      showFlag({
        autoDismiss: true,
        icon: createErrorIcon(),
        id: `common.copy.text.field.error.${Date.now()}`,
        title: formatMessage(messages.copyFail),
        description: formatMessage(messages.copyFailDescription, { label }),
      });
    }
    if (this.props.compact) {
      document.body.removeChild(textArea);
    }
  }

  private sendAnalyticsEvent = () => {
    const { analyticsClient, analyticsUIEvent } = this.props;
    if (analyticsUIEvent) {
      analyticsClient.sendUIEvent(analyticsUIEvent);
    }
  }

  // copy command is deprecated and non standard, so might not work
  private copy = (): boolean => {
    let isSupported: boolean;
    try {
      isSupported = document.queryCommandSupported('copy');
    } catch (_) {
      isSupported = false;
    }

    if (!isSupported) {
      return false;
    }

    let isEnabled: boolean;
    try {
      isEnabled = document.queryCommandEnabled('copy');
    } catch (_) {
      isEnabled = false;
    }

    if (!isEnabled) {
      return false;
    }

    try {
      return document.execCommand('copy');
    } catch (_) {
      return false;
    }
  }
}

export const TextCopy =
  withFlag(
    injectIntl(
      withAnalyticsClient(
        TextCopyImpl,
      ),
    ),
  )
;
