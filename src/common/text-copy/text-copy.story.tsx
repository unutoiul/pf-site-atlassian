
import { storiesOf } from '@storybook/react';
import * as React from 'react';

import { IntlProvider } from 'react-intl';

import { FlagProvider } from 'common/flag';

import { TextCopy } from './text-copy';

storiesOf('Common|Text copy', module)
  .add('Default', () => {

    return (
      <IntlProvider locale="en">
        <FlagProvider>
          <TextCopy label="Token" value="235fs-294jASGj913jg0-498-ffds" />
        </FlagProvider>
      </IntlProvider>
    );
  })
  .add('Compact', () => {

    return (
      <IntlProvider locale="en">
        <FlagProvider>
          <TextCopy label="Token" value="235fs-294jASGj913jg0-498-ffds" compact={true} compactTooltipMessage="Copy token"/>
        </FlagProvider>
      </IntlProvider>
    );
  })
;
