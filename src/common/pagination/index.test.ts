import { expect } from 'chai';
import { Location } from 'history';

import { getPage, getStart, sanitizePage } from 'common/pagination';

describe('Pagination Helpers', () => {
  describe('getStart', () => {
    it('returns the correct start index', () => {
      expect(getStart(1, 10)).to.equal(1);
      expect(getStart(2, 10)).to.equal(11);
      expect(getStart(3, 3)).to.equal(7);
    });
  });

  describe('sanitizePage', () => {
    it('returns 1 when nothing is provided', () => {
      expect(sanitizePage()).to.equal(1);
    });
    it('returns the number when a number greater than or equal to 1 is provided', () => {
      expect(sanitizePage('1')).to.equal(1);
      expect(sanitizePage('2')).to.equal(2);
    });
    it('returns 1 when a number less than 1 is provided', () => {
      expect(sanitizePage('0')).to.equal(1);
      expect(sanitizePage('-1')).to.equal(1);
    });
    it('returns 1 when non-number characters are provided', () => {
      expect(sanitizePage('')).to.equal(1);
      expect(sanitizePage('foo bar')).to.equal(1);
    });
  });

  describe('getPage', () => {
    it('returns the default page value when no page param', () => {
      const stubbedLocation: Location = {
        pathname: '',
        state: '',
        hash: '',
        search: '',
      };
      expect(getPage(stubbedLocation)).to.equal(1);
    });
    it('returns the page value from search param', () => {
      const stubbedLocation: Location = {
        pathname: '',
        state: '',
        hash: '',
        search: '?page=2',
      };
      expect(getPage(stubbedLocation)).to.equal(2);
    });
  });
});
