import { Location } from 'history';
import { parse } from 'query-string';

export function getStart(page: number, rowsPerPage: number) {
  return (page - 1) * rowsPerPage + 1;
}

export function sanitizePage(page?: string) {
  // tslint:disable-next-line:no-bitwise
  return Math.max(1, ~~page!);
}

export function getPage(location: Location) {
  const { page } = parse(location.search);

  return sanitizePage(page);
}
