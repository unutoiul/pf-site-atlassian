import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import { GroupNameContainer, GroupNameImpl } from './group-name';

import { createMockIntlProp } from '../../utilities/testing';

function getWrapper(name, readOnly) {
  if (readOnly) {
    return shallow(<GroupNameImpl name={name} managementAccess="READ_ONLY" ownerType="EXT_SCIM" intl={createMockIntlProp()} />).find(GroupNameContainer);
  }

  return shallow(<GroupNameImpl name={name} managementAccess="ALL" intl={createMockIntlProp()} />).find(GroupNameContainer);
}

describe('GroupName Component', () => {
  it('should render a regular group name normally', () => {
    const wrapper = getWrapper('Group name', false);

    expect(wrapper.children().length).to.equal(1);
    expect(wrapper.find('LockFilledIcon').length).to.equal(0);
  });

  it('should render a SCIM synced group name with a lock', () => {
    const wrapper = getWrapper('SCIM Group name', true);

    expect(wrapper.children().length).to.equal(2);
    expect(wrapper.find('LockFilledIcon').length).to.equal(1);
  });
});
