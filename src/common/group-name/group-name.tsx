import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import AkLockFilledIcon from '@atlaskit/icon/glyph/lock-filled';
import {
  colors as AkColors,
  gridSize as akGridSize,
} from '@atlaskit/theme';
import AkTooltip from '@atlaskit/tooltip';

import { ManagementAccess, OwnerType } from '../../schema/schema-types';

const messages = defineMessages({
  lockLabel: {
    id: 'organization.userProvisioning.groups.lock.label',
    defaultMessage: 'Group synced from external directory',
    description: 'A tool tip on a lock icon explaining why the group is not editable',
  },
});

export const GroupNameContainer = styled.div`
  display: flex;
  flex-flow: row nowrap;
  justify-content: flex-start;
  align-items: center;
  margin-right: ${akGridSize() / 3}px;
`;

const GroupNameWrapper = styled.span`
  flex-shrink: 10;
  text-overflow: ellipsis;
  white-space: nowrap;
  display: inline-block;
  overflow: hidden;
`;

const LockIconMargin = styled.div`
  flex-shrink: 0;
  justify-self: flex-end;
  min-width: ${akGridSize() * 2}px;
  margin-left: ${akGridSize() / 2}px;

  svg {
    vertical-align: middle;
    margin-top: -2px;
  }
`;

interface OwnProps {
  name: string;
  managementAccess: ManagementAccess;
  ownerType?: OwnerType | null;
  size?: 'small' | 'medium' | 'large' | 'xlarge';
}
export type GroupNameProps = InjectedIntlProps & OwnProps;
export class GroupNameImpl extends React.Component<GroupNameProps> {
  public render() {
    const { name, size, intl: { formatMessage } } = this.props;

    return (
      <GroupNameContainer>
        <GroupNameWrapper>
          {name}
        </GroupNameWrapper>
        {this.readOnly() && (
          <LockIconMargin>
            <AkTooltip
              content={formatMessage(messages.lockLabel)}
              position="right"
            >
              <AkLockFilledIcon
                label=""
                size={size ? size : 'small'}
                primaryColor={AkColors.N500}
              />
            </AkTooltip>
          </LockIconMargin>
        )}
      </GroupNameContainer>
    );
  }
  public readOnly = () => {
    const { managementAccess, ownerType } = this.props;

    return (managementAccess === 'READ_ONLY' && ownerType === 'EXT_SCIM');
  }
}

export const GroupName =
  injectIntl(
    GroupNameImpl,
  );
