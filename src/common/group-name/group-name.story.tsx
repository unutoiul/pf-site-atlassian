import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';
import styled from 'styled-components';

import {
  Field as AkField,
} from '@atlaskit/form';

import { GroupName } from 'common/group-name';

const Wrapper = styled.div`
margin: 25px;
max-width: 500px;
`;

storiesOf('Common|Group Name', module)
  .add('SCIM', () => {
    return (
      <IntlProvider locale="en">
        <Wrapper>
          <AkField>
            <GroupName name="This is a SCIM group" managementAccess="READ_ONLY" ownerType="EXT_SCIM" />
          </AkField>
        </Wrapper>
      </IntlProvider>
    );
  }).add('Title SCIM', () => {
    return (
      <IntlProvider locale="en">
        <Wrapper>
          <AkField>
            <h1><GroupName name="This is a SCIM group" managementAccess="READ_ONLY" ownerType="EXT_SCIM" size="large" /></h1>
          </AkField>
        </Wrapper>
      </IntlProvider>
    );
  }).add('Non SCIM', () => {
    return (
      <IntlProvider locale="en">
        <Wrapper>
          <AkField>
            <GroupName name="This is not a SCIM group" managementAccess="ALL" />
          </AkField>
        </Wrapper>
      </IntlProvider>
    );
  });
