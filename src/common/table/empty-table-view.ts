import styled from 'styled-components';

import {
  colors as akColors,
} from '@atlaskit/theme';

export const EmptyTableView = styled.em`
  color: ${akColors.N100};
`;
