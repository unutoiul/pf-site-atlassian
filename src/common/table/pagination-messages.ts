import { defineMessages, FormattedMessage } from 'react-intl';

const messages = defineMessages({
  previous: {
    id: 'common.table.previous',
    defaultMessage: 'Prev',
    description: 'Title for previous page link for paginated tables',
  },
  next: {
    id: 'common.table.next',
    defaultMessage: 'Next',
    description: 'Title for next page link for paginated tables',
  },
});

export const getPaginationMessages = (formatMessage: (messageDescriptor: FormattedMessage.MessageDescriptor) => string) => ({
  prev: formatMessage(messages.previous),
  next: formatMessage(messages.next),
});
