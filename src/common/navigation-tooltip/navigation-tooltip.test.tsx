// tslint:disable jsx-use-translation-function
import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import { NavigationTooltip } from '.';
import { createMockIntlContext } from '../../utilities/testing';

const tooltip = (
  <NavigationTooltip message="Learn more">
    <h1>Heading</h1>
  </NavigationTooltip>
);

describe('NavigationTooltip', () => {
  it('should render its children', () => {
    const wrapper = shallow(tooltip, createMockIntlContext());
    expect(wrapper.childAt(0).type()).to.equal('h1');
  });

  it('should render the correct message', () => {
    const wrapper = shallow(tooltip, createMockIntlContext());
    expect(wrapper.prop('content')).to.equal('Learn more');
  });

  it('should default to position right', () => {
    const wrapper = shallow(tooltip, createMockIntlContext());
    expect(wrapper.prop('position')).to.equal('right');
  });
});
