import * as React from 'react';

import AkToolTip from '@atlaskit/tooltip';

const GLOBAL_NAV_TOOLTIP_POSITION = 'right';

export interface NavigationTooltipProps {
  message: string;
}

export class NavigationTooltip extends React.Component<NavigationTooltipProps> {
  public render() {
    return (
      <AkToolTip
        content={this.props.message}
        position={GLOBAL_NAV_TOOLTIP_POSITION}
      >
        {this.props.children}
      </AkToolTip>
    );
  }
}
