import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import AkButton from '@atlaskit/button';

import { Action, ActionSubject } from 'common/analytics/new-analytics-types';

import { createMockAnalyticsClient, noop } from '../../utilities/testing';
import { MutationModalSubmitButton } from './mutation-modal-submit-button';

describe('MutationModalSubmitButton', () => {
  it('should pass props to the button', () => {
    const wrapper = shallow(
      <MutationModalSubmitButton
        onSubmit={noop}
        mutate={noop as any}
        loading={true}
        disabled={false}
        appearance="danger"
        text="test text"
        analyticsClient={createMockAnalyticsClient()}
      />,
    );

    expect(wrapper.find(AkButton).length).to.equal(1);
    expect(wrapper.find(AkButton).props().appearance).to.equal('danger');
    expect(wrapper.find(AkButton).props().children).to.equal('test text');
    expect(wrapper.find(AkButton).props().isLoading).to.equal(true);
    expect(wrapper.find(AkButton).props().isDisabled).to.equal(false);
  });

  it('should pass mutate to submit on clicking', () => {
    const mutateSpy = sinon.stub().resolves();
    const submitSpy = sinon.spy();

    const wrapper = shallow(
      <MutationModalSubmitButton
        onSubmit={submitSpy}
        mutate={mutateSpy}
        loading={true}
        disabled={false}
        appearance="danger"
        text="test text"
        analyticsClient={createMockAnalyticsClient()}
      />,
    );

    wrapper.find(AkButton).simulate('click');

    expect(submitSpy.callCount).to.equal(1);
    expect(submitSpy.getCalls()[0].args[0]).to.equal(mutateSpy);
  });

  it('should submit a ui event if one is present', () => {
    const mockAnalytics = createMockAnalyticsClient();
    const wrapper = shallow(
      <MutationModalSubmitButton
        onSubmit={noop}
        mutate={noop as any}
        loading={true}
        disabled={false}
        appearance="danger"
        text="test text"
        analyticsClient={mockAnalytics}
        event={{
          data: {
            action: Action.Clicked,
            actionSubject: ActionSubject.Button,
            actionSubjectId: 'submit',
          },
        }}
      />,
    );

    wrapper.find(AkButton).simulate('click');

    expect(mockAnalytics.sendUIEvent.calledWith({
      data: {
        action: Action.Clicked,
        actionSubject: ActionSubject.Button,
        actionSubjectId: 'submit',
      },
    })).to.equal(true);
  });
});
