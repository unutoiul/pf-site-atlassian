// tslint:disable jsx-use-translation-function
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { defineMessages, IntlProvider } from 'react-intl';

import AkButton from '@atlaskit/button';
import { FieldTextStateless as AkTextFieldStateless } from '@atlaskit/field-text';

import { Referrer } from 'common/analytics';
import { TrackActionSubject } from 'common/analytics/new-analytics-types';
import { FlagProvider } from 'common/flag';
import { MutationModal } from 'common/modal/mutation-modal';

import { MutationModalReferrerVariablesMutationVariables } from '../../schema/schema-types';
import mutationModalReferrerVariableMutation from './mutation-modal-referrer-variable.test-mutation.graphql';
import mutationModalReferrerMutation from './mutation-modal-referrer.test-mutation.graphql';

const messages = defineMessages({
  title: {
    id: 'mutation.modal.story.title',
    defaultMessage: 'Success',
  },
  description: {
    id: 'mutation.modal.story.description',
    defaultMessage: 'Updated successfully',
  },
});

storiesOf('Common|Modal Dialog – Mutation Modal', module)
  .add('With just a header and a mutation', () => {

    return (
      <IntlProvider locale="en">
        <FlagProvider>
          <pre>
            The Mutation Modal is a simple way to make a modal that does some mutation. Pass in a 'mutation' as a prop (imported from 'your-mutation.mutation.graphql') and just give that to the component.
            It will do that mutation when the user clicks the submit button. That's it!
          </pre>
          <MutationModal
            header={<p>Absolute MVP mutation modal</p>}
            mutation={mutationModalReferrerMutation}
            mutationOptions={() => ({})}
            submitButtonText="Update"
          >
            {openModal => (
              <AkButton onClick={openModal}>Open modal</AkButton>
            )}
          </MutationModal>
        </FlagProvider>
      </IntlProvider>
    );
  })
  .add('With a body and success flag', () => {

    return (
      <IntlProvider locale="en">
        <FlagProvider>
          <pre>The Mutation Modal will show a success flag only if you specify the 'successFlag' prop.</pre>
          <MutationModal
            successFlag={{
              title: messages.title,
              description: messages.description,
            }}
            header={<p>Enable awesome feature</p>}
            body={<p>Are you sure you want to enable this amazing, incredible feature?</p>}
            mutation={mutationModalReferrerMutation}
            mutationOptions={() => ({})}
            submitButtonText="Update"
          >
            {openModal => (
              <AkButton onClick={openModal}>Open modal</AkButton>
            )}
          </MutationModal>
        </FlagProvider>
      </IntlProvider>
    );
  })
  .add('With different appearances', () => {
    return (
      <IntlProvider locale="en">
        <FlagProvider>
          <pre>You can control the appearance of the mutation modal's primary button with 'submitButtonAppearance'</pre>
          <MutationModal
            header={<p>Danger</p>}
            body={<p>This is a danger modal</p>}
            mutation={mutationModalReferrerMutation}
            mutationOptions={() => ({})}
            submitButtonText={'Danger'}
            submitButtonAppearance={'danger'}
          >
            {openModal => (
              <AkButton onClick={openModal}>Danger modal</AkButton>
            )}
          </MutationModal>
          <MutationModal
            header={<p>Warning</p>}
            body={<p>This is a warning modal</p>}
            mutation={mutationModalReferrerMutation}
            mutationOptions={() => ({})}
            submitButtonText={'Warning'}
            submitButtonAppearance={'warning'}
          >
            {openModal => (
              <AkButton onClick={openModal}>Warning modal</AkButton>
            )}
          </MutationModal>
          <MutationModal
            header={<p>Primary</p>}
            body={<p>This is a primary modal</p>}
            mutation={mutationModalReferrerMutation}
            mutationOptions={() => ({})}
            submitButtonText={'Primary'}
            submitButtonAppearance={'primary'}
          >
            {openModal => (
              <AkButton onClick={openModal}>Primary modal</AkButton>
            )}
          </MutationModal>
        </FlagProvider>
      </IntlProvider>
    );
  })
  .add('With analytics events', () => {
    return (
      <IntlProvider locale="en">
        <FlagProvider>
          <pre>
            The mutation modal will send off a screen event for the modal itself if you specify one, and a track event for submission if you specify one.
          </pre>
          <MutationModal
            successFlag={{
              title: messages.title,
              description: messages.description,
            }}
            header={<p>Enable awesome feature</p>}
            body={<p>Are you sure you want to enable this amazing, incredible feature?</p>}
            mutation={mutationModalReferrerMutation}
            mutationOptions={() => ({})}
            screenEvent={{
              data: {
                name: '',
              },
            }}
            trackEvent={{
              data: {
                source: '',
                action: '',
                actionSubject: TrackActionSubject.User,
                actionSubjectId: '',
              },
            }}
            submitButtonText="Update"
          >
            {openModal => (
              <Referrer value="direct">
                <AkButton onClick={openModal}>Open modal</AkButton>
              </Referrer>
            )}
          </MutationModal>
        </FlagProvider>
      </IntlProvider>
    );
  })
  .add('With a basic single field', () => {
    const field = ({ onChange, value }) => (
      <AkTextFieldStateless
        shouldFitContainer={true}
        onChange={onChange}
        value={value || ''}
        label="Test field"
      />
    );

    return (
      <IntlProvider locale="en">
        <FlagProvider>
          <pre>{`
            The mutation modal can take a single field as a prop. The 'field' prop takes a Component (a function) that returns a component to be rendered inside the modal.

            This component is controlled by the Mutation Modal, and as such, is given onChange and a value from the MutationModal. The state of this component is maintained inside MutationModal, and is by default mapped to a mutation variable!

            This is useful if you need to be able to fill in a mutation variable with a single user defined value, such as "Edit your name" or "Change the password". In this case, you would tell the component which variable it maps to with the fieldMutationVariableName prop.

            MutationModal needs to know about your mutation to type check it correctly, so pass in the automatically generated mutation variables as a JSX generic argument.

            In this case it's MutationModalReferrerVariablesMutationVariables, but it'd be imported from 'schema/schema-types'.
          `}</pre>
          <MutationModal<MutationModalReferrerVariablesMutationVariables>
            successFlag={{
              title: messages.title,
              description: messages.description,
            }}
            header={<p>Enable awesome feature</p>}
            body={<p>Are you sure you want to enable this amazing, incredible feature?</p>}
            mutation={mutationModalReferrerVariableMutation}
            mutationOptions={(fieldValue) => ({
              variables: {
                value: fieldValue,
              },
            })}
            field={field}
            submitButtonText="Update"
          >
            {openModal => (
              <AkButton onClick={openModal}>Open modal</AkButton>
            )}
          </MutationModal>
        </FlagProvider>
      </IntlProvider>
    );
  })
  .add('With a custom form in the body', () => {
    interface ExampleState {
      value: string;
    }

    class Example extends React.Component<any, ExampleState> {
      public state = {
        value: '',
      };

      public render() {
        return (
          <IntlProvider locale="en">
            <FlagProvider>
              <pre>{`
                If one simple field is not enough, you can embed anything into the body, including a form or some fields. If you do that, you can then pass the variable through to the mutation by specifying custom 'mutationOptions'.
                Again, make sure you pass through the automatically generated MutationVariable types from 'schema/schema-types' for your mutation, so that the mutation options get typed correctly.
                You pass these through using a JSX generic.
              `}</pre>
              <MutationModal<MutationModalReferrerVariablesMutationVariables>
                successFlag={{
                  title: messages.title,
                  description: messages.description,
                }}
                header={<p>Enable awesome feature</p>}
                body={
                  <div>
                    <p>Are you sure you want to enable this amazing, incredible feature?</p>
                    <AkTextFieldStateless
                      shouldFitContainer={true}
                      onChange={this.onChange}
                      value={this.state.value}
                      label="Mutation variable"
                    />
                  </div>
                }
                mutation={mutationModalReferrerVariableMutation}
                mutationOptions={this.getMutationOptions}
                onModalClose={this.clearModal}
                submitButtonText="Update"
              >
                {openModal => (
                  <AkButton onClick={openModal}>Open modal</AkButton>
                )}
              </MutationModal>
            </FlagProvider>
          </IntlProvider>
        );
      }

      private getMutationOptions = () => ({
        variables: {
          value: this.state.value,
        },
      });

      private clearModal = () => {
        this.setState({ value: '' });
      }

      private onChange = (event) => {
        this.setState({ value: event.target.value });
      }
    }

    return <Example />;

  });
