import styled from 'styled-components';

import { gridSize as akGridSize } from '@atlaskit/theme';

export const StyledModalDialog = styled.span`
  div {
    z-index: 700;
  }
`;

export const IconWrapper = styled.span`
  padding-right: ${akGridSize() * 1.5}px;
`;

export const ListItems = styled.ul`
  list-style: none;
  padding-left: ${akGridSize() * 2}px;
`;
