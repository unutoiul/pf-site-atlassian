import { DocumentNode } from 'graphql';
import * as React from 'react';
import { Mutation, MutationFn, MutationOptions, OperationVariables } from 'react-apollo';
import { FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';

import AkButton, { ButtonGroup as AkButtonGroup } from '@atlaskit/button';

import {
  analyticsClient as monitoringAnalyticsClient,
  AnalyticsClientProps,
  ScreenEvent,
  ScreenEventSender,
  TrackEvent,
  UIEvent,
  withAnalyticsClient,
} from 'common/analytics';

import { createErrorIcon, createSuccessIcon, errorFlagMessages } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';

import { ModalDialog } from './modal-dialog';
import { MutationModalSubmitButton } from './mutation-modal-submit-button';

export type ChangeHandler = (e: React.ChangeEvent<HTMLInputElement>) => void;

interface OwnProps<MutationVariables> {
  header: React.ReactNode;
  body?: React.ReactNode;
  width?: number | 'small' | 'medium' | 'large' | 'x-large';
  mutation: DocumentNode;
  successFlag?: {
    title: FormattedMessage.MessageDescriptor;
    description: FormattedMessage.MessageDescriptor;
  };
  successFlagValues?: Record<string, string>;
  submitButtonText: React.ReactNode;
  submitButtonAppearance?: 'danger' | 'primary' | 'warning';
  screenEvent?: ScreenEvent;
  trackEvent?: TrackEvent;
  submitButtonEvent?: UIEvent;
  cancelButtonEvent?: UIEvent;
  openEvent?: UIEvent;
  initialFieldValue?: any;
  mutationOptions(fieldValue: any): MutationOptions<any, MutationVariables>;
  onModalClose?(): void;
  onError?(args: {
    e: any;
    showErrorFlag(args: {
      title: string;
      description: string;
    }): void;
  });
  onCompleted?(): void;
  field?(args: {
    value: any;
    onChange: ChangeHandler;
  }): React.ReactNode;
  children(openModal: () => void): React.ReactNode;
}

// We pass in void as the mutation variable type as the default props need to be static.
// This is fine so long as we don't rely on the mutation variables for our default props.
type DefaultProps = Readonly<Required<Pick<OwnProps<void>,
  'width' | 'submitButtonAppearance'
  >>>;

export type MutationModalProps<MutationVariables> = OwnProps<MutationVariables> & AnalyticsClientProps & InjectedIntlProps & FlagProps;

interface OwnState {
  loading: boolean;
  isOpen: boolean;
  fieldValue: any;
}

const messages = {
  buttonClose: {
    id: 'common.modal-dialog.button.close',
    defaultMessage: 'Cancel',
    description: 'A button that closes a generic modal dialog',
  },
};

const ScreenEvent: React.SFC<{ screenEvent?: ScreenEvent }> = props => {
  if (!props.screenEvent) {
    // We need to wrap it in a fragment to avoid a bug in the react types
    // https://github.com/facebook/react/issues/12155
    return <React.Fragment>{props.children}</React.Fragment>;
  }

  return (
    <ScreenEventSender event={props.screenEvent}>
      {props.children}
    </ScreenEventSender>
  );
};

export class MutationModalImpl<MutationVariables> extends React.Component<MutationModalProps<MutationVariables> & DefaultProps, OwnState> {
  public static defaultProps: DefaultProps = {
    width: 'small',
    submitButtonAppearance: 'primary',
  };

  public readonly state: Readonly<OwnState> = {
    loading: false,
    isOpen: false,
    fieldValue: this.props.initialFieldValue,
  };

  public render() {
    return (
      <Mutation
        mutation={this.props.mutation}
        onCompleted={this.props.onCompleted}
      >
        {(mutate) => (
          <React.Fragment>
            <ModalDialog
              isOpen={this.state.isOpen}
              header={this.props.header}
              footer={this.getFooter(mutate)}
              width={this.props.width}
              onClose={this.onClose}
            >
              <ScreenEvent screenEvent={this.props.screenEvent}>
                {this.props.body}
                {this.props.field && this.props.field({ onChange: this.onFieldChange, value: this.state.fieldValue })}
              </ScreenEvent>
            </ModalDialog>
            {this.props.children(this.onOpen)}
          </React.Fragment>
        )}
      </Mutation>
    );
  }

  private getFooter = (mutate: MutationFn<any, OperationVariables>) => {
    return (
      <AkButtonGroup>
        <MutationModalSubmitButton<MutationVariables>
          appearance={this.props.submitButtonAppearance}
          text={this.props.submitButtonText}
          loading={this.state.loading}
          mutate={mutate}
          onSubmit={this.submit}
          disabled={!!(this.props.field && !this.state.fieldValue)}
          analyticsClient={this.props.analyticsClient}
          event={this.props.submitButtonEvent}
        />
        <AkButton
          appearance="subtle"
          onClick={this.onCancelClicked}
        >
          <FormattedMessage {...messages.buttonClose} />
        </AkButton>
      </AkButtonGroup>
    );
  };

  private onFieldChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      fieldValue: event.target.value,
    });
  }

  private onOpen = () => {
    if (this.props.openEvent) {
      this.props.analyticsClient.sendUIEvent(this.props.openEvent);
    }

    this.setState({
      isOpen: true,
      fieldValue: this.props.initialFieldValue,
    });
  }

  private onClose = () => {
    this.setState({
      isOpen: false,
      fieldValue: undefined,
    });
  }

  private onCancelClicked = () => {
    if (this.props.cancelButtonEvent) {
      this.props.analyticsClient.sendUIEvent(this.props.cancelButtonEvent);
    }

    this.onClose();
  };

  private submit = async (mutate: MutationFn<any, MutationVariables>) => {
    const {
      intl: { formatMessage },
      successFlag,
    } = this.props;

    this.setState({ loading: true });

    try {
      await mutate(this.props.mutationOptions(this.state.fieldValue));

      if (this.props.trackEvent) {
        this.props.analyticsClient.sendTrackEvent(this.props.trackEvent);
      }

      if (successFlag) {
        this.props.showFlag({
          id: `mutation-modal-${Date.now()}-success`,
          title: formatMessage(successFlag.title || '', { fieldValue: this.state.fieldValue, ...this.props.successFlagValues }),
          description: formatMessage(successFlag.description, { fieldValue: this.state.fieldValue, ...this.props.successFlagValues }),
          icon: createSuccessIcon(),
          autoDismiss: true,
        });
      }
    } catch (e) {
      if (this.props.onError) {
        this.props.onError({
          e,
          showErrorFlag: ({ title, description }) => {
            if (!this.props.showFlag) {
              return;
            }
            this.props.showFlag({
              id: `mutation-modal-${Date.now()}-error`,
              title,
              description,
              icon: createErrorIcon(),
            });
          },
        });
      } else {
        monitoringAnalyticsClient.onError(e);

        this.props.showFlag({
          id: `mutation-modal-${Date.now()}-error`,
          title: formatMessage(errorFlagMessages.title),
          description: formatMessage(errorFlagMessages.description),
          icon: createErrorIcon(),
          autoDismiss: true,
        });
      }

    } finally {
      this.setState({
        loading: false,
      }, () => {
        if (this.props.onModalClose) {
          this.props.onModalClose();
        }
        this.onClose();
      });
    }
  }
}

// tslint:disable-next-line:max-classes-per-file
export class MutationModal<MutationVariables> extends React.Component<OwnProps<MutationVariables>, OwnState> {
  private readonly WrappedComponent =
    withAnalyticsClient<OwnProps<MutationVariables>>(
      injectIntl<OwnProps<MutationVariables> & AnalyticsClientProps>(
        withFlag<OwnProps<MutationVariables> & AnalyticsClientProps & InjectedIntlProps>(
          MutationModalImpl,
        ),
      ),
    );

  public render() {
    return <this.WrappedComponent {...this.props} />;
  }
}
