import * as React from 'react';
import styled from 'styled-components';

import AkModalDialog, { ModalTransition as AkModalTransition } from '@atlaskit/modal-dialog';

const Title = styled.h4`
  align-items: center;
  display: flex;
  font-size: 20px;
  font-style: inherit;
  font-weight: 500;
  letter-spacing: -0.008em;
  line-height: 1;
  margin: 0;
  min-width: 0;
  padding: 20px;
`;

const Footer = styled.div`
  display: flex;
  align-items: flex-end;
  flex-direction: column;
  padding: 20px;
`;

interface Props {
  isOpen: boolean;
  header: React.ReactNode;
  footer?: React.ReactElement<any>;
  width: number | 'small' | 'medium' | 'large' | 'x-large';
  height?: number | string;
  onClose(): void;
}

export class ModalDialog extends React.Component<Props> {
  public render() {
    const { header, footer, onClose, width, isOpen, height } = this.props;

    const footerComponent = footer ? {
      Footer: () => (<Footer>{footer}</Footer>),
    } : {};
    const components = {
      Header: () => (<Title>{header}</Title>),
      ...footerComponent,
    };

    return (
      <AkModalTransition>
        {isOpen && <AkModalDialog
          components={components}
          onClose={onClose}
          width={width}
          height={height}
        >
          {this.props.children}
        </AkModalDialog>}
      </AkModalTransition>
    );
  }
}
