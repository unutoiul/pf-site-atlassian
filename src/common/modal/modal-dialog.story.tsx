// tslint:disable jsx-use-translation-function
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import * as React from 'react';

import AkButton from '@atlaskit/button';

import { ModalDialog } from './modal-dialog';

storiesOf('Common|Modal Dialog', module)
  .add('Default', () => {
    return (
      <ModalDialog
        header="Enable awesome feature"
        footer={(
          <AkButton
            onClick={action('Enable clicked')}
            appearance="primary"
          >
            {'Enable'}
          </AkButton>
        )}
        isOpen={true}
        width="medium"
        onClose={action('Close Dialog')}
      >
        <p>{'Tons of content that you can put in here!!!!!!'}</p>
      </ModalDialog>
    );
  });
