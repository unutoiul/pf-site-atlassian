import * as React from 'react';
import { MutationFn } from 'react-apollo';

import AkButton from '@atlaskit/button';

import {
  NewAnalyticsClient,
  UIEvent,
} from 'common/analytics';

interface OwnProps<MutationVariables> {
  appearance: 'primary' | 'warning' | 'danger';
  text: React.ReactNode;
  loading: boolean;
  disabled: boolean;
  mutate: MutationFn<any, MutationVariables>;
  analyticsClient: NewAnalyticsClient;
  event?: UIEvent;
  onSubmit(mutate: MutationFn<any, MutationVariables>): void;
}

export class MutationModalSubmitButton<MutationVariables> extends React.Component<OwnProps<MutationVariables>> {
  public render() {
    return (
      <AkButton
        appearance={this.props.appearance}
        onClick={this.submit}
        isLoading={this.props.loading}
        isDisabled={this.props.disabled}
      >
        {this.props.text}
      </AkButton>
    );
  }

  private submit = () => {
    if (this.props.event) {
      this.props.analyticsClient.sendUIEvent(this.props.event);
    }

    this.props.onSubmit(this.props.mutate);
  };
}
