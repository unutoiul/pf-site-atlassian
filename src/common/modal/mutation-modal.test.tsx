// tslint:disable:jsx-use-translation-function
import { expect } from 'chai';
import { mount } from 'enzyme';
import { addMockFunctionsToSchema, makeExecutableSchema } from 'graphql-tools';
import { func } from 'prop-types';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { defineMessages, IntlProvider } from 'react-intl';
import * as sinon from 'sinon';

import AkButton from '@atlaskit/button';

import { NewAnalyticsContext } from 'common/analytics';
import { Action, ActionSubject } from 'common/analytics/new-analytics-types';
import { hideFlagContextKey, showFlagContextKey } from 'common/flag/flag-provider';
import { ModalDialog } from 'common/modal';

import { createApolloClient } from '../../apollo-client';
import { typeDefs } from '../../schema/schema-defs';
import { MutationModalGroupVariableMutationVariables } from '../../schema/schema-types';
import { createMockAnalyticsClient, MockAnalyticsClient, wait, waitUntil } from '../../utilities/testing';
import { MutationModal, MutationModalProps } from './mutation-modal';
import mutationModalGroupVariableMutation from './mutation-modal-group-variable.test-mutation.graphql';
import mutationModalGroupMutation from './mutation-modal-group.test-mutation.graphql';

const messages = defineMessages({
  title: {
    id: 'mutation.modal.test.title',
    defaultMessage: 'Title',
  },
  description: {
    id: 'mutation.modal.test.description',
    defaultMessage: 'Description',
  },
});

const noop = () => null;

describe('MutationModal', () => {
  let createGroupStub: sinon.SinonStub;
  let showFlagSpy: sinon.SinonSpy;
  let mockAnalyticsClient: MockAnalyticsClient;

  beforeEach(() => {
    createGroupStub = sinon.stub().resolves(true);
    showFlagSpy = sinon.spy();
    mockAnalyticsClient = createMockAnalyticsClient();
  });

  afterEach(() => {
    createGroupStub.reset();
    showFlagSpy.reset();
  });

  function createMockApolloClient() {
    const mockSchema = makeExecutableSchema({ typeDefs });
    addMockFunctionsToSchema({
      schema: mockSchema,
      mocks: {
        Mutation: () => ({
          createGroup: createGroupStub,
        }),
      },
    });

    return createApolloClient(() => null, mockSchema);
  }

  function mountWithProviders(node: React.ReactNode) {
    return mount(
      <NewAnalyticsContext.Provider value={mockAnalyticsClient}>
        <ApolloProvider client={createMockApolloClient()}>
          <IntlProvider locale="en">
            {node}
          </IntlProvider>
        </ApolloProvider>
      </NewAnalyticsContext.Provider>,
      {
        context: {
          [showFlagContextKey]: showFlagSpy,
          [hideFlagContextKey]: noop,
        },
        childContextTypes: {
          [showFlagContextKey]: func,
          [hideFlagContextKey]: func,
        },
      });
  }

  function mountMutationModal<MutationVariables>(mutationModalProps?: Partial<MutationModalProps<MutationVariables>>) {
    return mountWithProviders(
      <MutationModal<MutationVariables>
        header={<p id="header">Header</p>}
        body={<p id="body">Body</p>}
        mutation={mutationModalGroupMutation}
        mutationOptions={() => ({})}
        submitButtonText="Save"
        {...mutationModalProps}
      >
        {openModal => (
          <AkButton id="trigger" onClick={openModal} />
        )}
      </MutationModal>,
    );
  }

  describe('with a basic mutation', () => {
    it('should render its render prop children', () => {
      const wrapper = mountMutationModal();

      expect(wrapper.find(AkButton).length).to.equal(1);
    });

    it('should render the header and body', () => {
      const wrapper = mountMutationModal();

      wrapper.find('Button[id="trigger"]').simulate('click');

      expect(wrapper.find('p[id="header"]').length).to.equal(1);
      expect(wrapper.find('p[id="body"]').length).to.equal(1);
    });

    it('should open when clicking on the trigger', () => {
      const wrapper = mountMutationModal();

      wrapper.find('Button[id="trigger"]').simulate('click');
      expect(wrapper.find(ModalDialog).props().isOpen).to.equal(true);
    });

    it('should trigger the mutation when clicking on the primary action', async () => {
      const wrapper = mountMutationModal();

      wrapper.find('Button[id="trigger"]').simulate('click');
      wrapper.find('Button[appearance="primary"]').simulate('click');

      await waitUntil(() => createGroupStub.callCount > 0);

      expect(createGroupStub.callCount).to.equal(1);
    });

    it('should not show a success flag by default after the primary button is clicked', async () => {
      const wrapper = mountMutationModal();

      wrapper.find('Button[id="trigger"]').simulate('click');
      wrapper.find('Button[appearance="primary"]').simulate('click');

      await waitUntil(() => createGroupStub.callCount > 0);

      expect(showFlagSpy.callCount).to.equal(0);
    });

    it('should show a success flag if a title and description is given, after the primary button is clicked', async () => {
      const wrapper = mountMutationModal({
        successFlag: {
          title: messages.title,
          description: messages.description,
        },
      });

      wrapper.find('Button[id="trigger"]').simulate('click');
      wrapper.find('Button[appearance="primary"]').simulate('click');

      await waitUntil(() => createGroupStub.callCount > 0);

      expect(showFlagSpy.callCount).to.equal(1);
    });

    it('should show a error flag if the resolver throws', async () => {
      createGroupStub = sinon.stub().throws();
      const wrapper = mountMutationModal({
        successFlag: {
          title: messages.title,
          description: messages.description,
        },
      });

      wrapper.find('Button[id="trigger"]').simulate('click');
      wrapper.find('Button[appearance="primary"]').simulate('click');

      await waitUntil(() => createGroupStub.callCount > 0);

      expect(showFlagSpy.callCount).to.equal(1);
      expect(showFlagSpy.getCalls()[0].args[0].id).to.contain('error');
    });

    it('should delegate to onError if the resolver throws and onError is specified', async () => {
      createGroupStub = sinon.stub().throws();
      const onErrorSpy = sinon.spy();
      const wrapper = mountMutationModal({
        successFlag: {
          title: messages.title,
          description: messages.description,
        },
        onError: onErrorSpy,
      });

      wrapper.find('Button[id="trigger"]').simulate('click');
      wrapper.find('Button[appearance="primary"]').simulate('click');

      await waitUntil(() => createGroupStub.callCount > 0);

      expect(showFlagSpy.callCount).to.equal(0);
      expect(onErrorSpy.callCount).to.equal(1);
    });

    it('should send a screen and track analytic event if the events are specified', async () => {
      const wrapper = mountMutationModal({
        successFlag: {
          title: messages.title,
          description: messages.description,
        },
        screenEvent: {
          data: {
            name: 'screen',
          },
        },
        trackEvent: {
          data: {
            source: 'source',
            action: 'action',
            actionSubject: 'actionSubject' as any,
          },
        },
      });

      wrapper.find('Button[id="trigger"]').simulate('click');
      expect(mockAnalyticsClient.sendScreenEvent.callCount).to.equal(1);
      expect(mockAnalyticsClient.sendTrackEvent.callCount).to.equal(0);
      wrapper.find('Button[appearance="primary"]').simulate('click');
      await waitUntil(() => mockAnalyticsClient.sendTrackEvent.callCount > 0);
      expect(mockAnalyticsClient.sendTrackEvent.callCount).to.equal(1);
    });

    it('should not send a track event if the mutation fails', async () => {
      createGroupStub = sinon.stub().throws();
      const wrapper = mountMutationModal({
        successFlag: {
          title: messages.title,
          description: messages.description,
        },
        screenEvent: {
          data: {
            name: 'screen',
          },
        },
        trackEvent: {
          data: {
            source: 'source',
            action: 'action',
            actionSubject: 'actionSubject' as any,
          },
        },
      });

      wrapper.find('Button[id="trigger"]').simulate('click');
      expect(mockAnalyticsClient.sendScreenEvent.callCount).to.equal(1);
      expect(mockAnalyticsClient.sendTrackEvent.callCount).to.equal(0);
      wrapper.find('Button[appearance="primary"]').simulate('click');
      await wait(1);
      expect(mockAnalyticsClient.sendTrackEvent.callCount).to.equal(0);
    });

    it('should send a ui event on cancel if one is present', async () => {
      const wrapper = mountMutationModal({
        cancelButtonEvent: {
          data: {
            action: Action.Clicked,
            actionSubject: ActionSubject.Button,
            actionSubjectId: 'submit',
          },
        },
      });

      wrapper.find('Button[id="trigger"]').simulate('click');
      expect(mockAnalyticsClient.sendUIEvent.callCount).to.equal(0);
      wrapper.find('Button[appearance="subtle"]').simulate('click');
      expect(mockAnalyticsClient.sendUIEvent.callCount).to.equal(1);
    });

    it('should send a ui event on open if one is present', async () => {
      const wrapper = mountMutationModal({
        openEvent: {
          data: {
            action: Action.Clicked,
            actionSubject: ActionSubject.Button,
            actionSubjectId: 'something',
          },
        },
      });

      expect(mockAnalyticsClient.sendUIEvent.callCount).to.equal(0);
      wrapper.find('Button[id="trigger"]').simulate('click');
      expect(mockAnalyticsClient.sendUIEvent.callCount).to.equal(1);
    });
  });

  describe('with a mutation that contains a variable', () => {
    it('should allow variables to be passed to the mutation', async () => {
      const wrapper = mountMutationModal<MutationModalGroupVariableMutationVariables>({
        successFlag: {
          title: messages.title,
          description: messages.description,
        },
        mutation: mutationModalGroupVariableMutation,
        mutationOptions: () => ({
          variables: {
            name: 'Test mutation',
          },
        }),
      });

      wrapper.find('Button[id="trigger"]').simulate('click');
      wrapper.find('Button[appearance="primary"]').simulate('click');

      await waitUntil(() => createGroupStub.callCount > 0);

      expect(createGroupStub.callCount).to.equal(1);
      expect(createGroupStub.getCalls()[0].args[1].input).to.deep.equal({ name: 'Test mutation' });
    });
  });
});
