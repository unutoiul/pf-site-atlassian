import * as React from 'react';
import styled from 'styled-components';

import AkButton, { AkButtonProps } from '@atlaskit/button';
import AkInlineDialog from '@atlaskit/inline-dialog';
import { gridSize as akGridSize } from '@atlaskit/theme';

interface ButtonHoverProps {
  dialogMessage?: React.ReactNode;
  position?: string;
}

interface ButtonHoverState {
  isDialogOpen: boolean;
}

export const DisabledActionContainer = styled.div`
  white-space: normal;

  /*
    Workaround for a Chrome bug where onMouseLeave isn't fired for disabled form elements.
    https://bugs.chromium.org/p/chromium/issues/detail?id=120132
    https://github.com/facebook/react/issues/4251)
  */
  button[disabled] {
    pointer-events: none;
  }
`;

const InlineWrapper = styled.div`
  max-width: ${akGridSize() * 37.5}px;
`;

export class ButtonHover extends React.Component<ButtonHoverProps & AkButtonProps, ButtonHoverState> {

  public readonly state: Readonly<ButtonHoverState> = {
    isDialogOpen: false,
  };

  public render() {

    if (!this.props.dialogMessage) {
      return (
        <AkButton
          isDisabled={this.props.isDisabled}
          {...this.props}
        >
          {this.props.children}
        </AkButton>
      );
    }

    return (
      <DisabledActionContainer
        onMouseEnter={this.openDialog}
        onMouseLeave={this.closeDialog}
      >
        <AkInlineDialog
          isOpen={this.state.isDialogOpen}
          content={<InlineWrapper>{this.props.dialogMessage}</InlineWrapper>}
          placement={this.props.position ? this.props.position : 'left'}
        >
          <AkButton
            isDisabled={this.props.isDisabled}
            {...this.props}
          >
            {this.props.children}
          </AkButton>
        </AkInlineDialog>
      </DisabledActionContainer>
    );
  }

  private openDialog = () => {
    this.setState({ isDialogOpen: true });
  }

  private closeDialog = () => {
    this.setState({ isDialogOpen: false });
  }
}
