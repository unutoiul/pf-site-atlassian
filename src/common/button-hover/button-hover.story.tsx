// tslint:disable:jsx-use-translation-function
import { storiesOf } from '@storybook/react';
import * as React from 'react';

import { ButtonHover } from '.';

storiesOf('Common|Button Hover', module)
  .add('Enabled', () => (
    <div style={{ paddingLeft: '400px' }}>
      <ButtonHover>
        Snakes
      </ButtonHover>
    </div>
  )).add('Disabled but no message', () => (
    <div style={{ paddingLeft: '400px' }}>
      <ButtonHover isDisabled={true}>
        Snakes
      </ButtonHover>
    </div>
  )).add('Disabled but with message', () => (
    <div style={{ paddingLeft: '400px' }}>
      <ButtonHover isDisabled={true} dialogMessage={<span>More snakes</span>}>
        Snakes
      </ButtonHover>
    </div>
  ));
