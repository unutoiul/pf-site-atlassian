// tslint:disable jsx-use-translation-function
import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import AkButton from '@atlaskit/button';
import AkInlineDialog from '@atlaskit/inline-dialog';

import { ButtonHover } from '.';

describe('Info Button Hover', () => {

  it('should be enabled and have no inline dialog if there is no dialog message', () => {
    const wrapper = shallow(<ButtonHover>Snakes</ButtonHover>);
    expect(wrapper.find(AkButton).exists()).to.equal(true);
    expect(wrapper.find(AkButton).prop('isDisabled')).to.equal(undefined);
    expect(wrapper.find(AkInlineDialog).exists()).to.equal(false);
  });

  it('should be disabled and have no inline dialog if there is no dialog message', () => {
    const wrapper = shallow(<ButtonHover isDisabled={true}>Snakes</ButtonHover>);
    expect(wrapper.find(AkButton).exists()).to.equal(true);
    expect(wrapper.find(AkButton).prop('isDisabled')).to.equal(true);
    expect(wrapper.find(AkInlineDialog).exists()).to.equal(false);
  });

  it('should be disabled and have an inline dialog if there is a dialog message', () => {
    const wrapper = shallow(<ButtonHover isDisabled={true} dialogMessage="On a plane">Snakes</ButtonHover>);
    expect(wrapper.find(AkButton).exists()).to.equal(true);
    expect(wrapper.find(AkButton).prop('isDisabled')).to.equal(true);
    expect(wrapper.find(AkInlineDialog).exists()).to.equal(true);
    const content = shallow(wrapper.find(AkInlineDialog).prop('content'));
    expect(content.children().text()).to.equal('On a plane');
  });

  it('should be enabled and have an inline dialog if there is a dialog message', () => {
    const wrapper = shallow(<ButtonHover isDisabled={false} dialogMessage="On a plane">Snakes</ButtonHover>);
    expect(wrapper.find(AkButton).exists()).to.equal(true);
    expect(wrapper.find(AkButton).prop('isDisabled')).to.equal(false);
    expect(wrapper.find(AkInlineDialog).exists()).to.equal(true);
  });

  it('should pass any extra props to the button with a dialog message', () => {
    const wrapper = shallow(<ButtonHover appearance="danger" spacing="compact" dialogMessage="ON A PLANE">SNAKES</ButtonHover>);
    expect(wrapper.find(AkButton).exists()).to.equal(true);
    expect(wrapper.find(AkButton).prop('appearance')).to.equal('danger');
    expect(wrapper.find(AkButton).prop('spacing')).to.equal('compact');
  });

  it('should pass any extra props to the button without a dialog message', () => {
    const wrapper = shallow(<ButtonHover appearance="danger" spacing="compact">SNAKES</ButtonHover>);
    expect(wrapper.find(AkButton).exists()).to.equal(true);
    expect(wrapper.find(AkButton).prop('appearance')).to.equal('danger');
    expect(wrapper.find(AkButton).prop('spacing')).to.equal('compact');
  });
});
