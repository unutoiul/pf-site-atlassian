// tslint:disable:jsx-use-translation-function
import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import * as React from 'react';
import { IntlProvider } from 'react-intl';

import { createMockIntlContext } from '../../utilities/testing';
import { CountIndicator } from './count-indicator';

describe('Count Indicator', () => {
  it(`should render '-' if count is undefined`, () => {
    const wrapper = shallow(<CountIndicator header={<p>My counter</p>} count={undefined} />);

    const html = wrapper.html();
    expect(html.includes('My counter')).to.equal(true);
    expect(html.includes('-')).to.equal(true);
  });

  it(`should render non zero count`, () => {
    const wrapper = mount(
      <IntlProvider locale="en">
        <CountIndicator header={<p>My counter</p>} count={1234} />
      </IntlProvider>,
      createMockIntlContext(),
    );

    const html = wrapper.html();
    expect(html.includes('My counter')).to.equal(true);
    expect(html.includes('1,234')).to.equal(true);
  });

  it(`should render zero count`, () => {
    const wrapper = mount(
      <IntlProvider locale="en">
        <CountIndicator header={<p>My counter</p>} count={0} />
      </IntlProvider>,
    );

    const html = wrapper.html();
    expect(html.includes('My counter')).to.equal(true);
    expect(html.includes('0')).to.equal(true);
  });
});
