import * as React from 'react';
import { FormattedNumber } from 'react-intl';
import styled from 'styled-components';

import {
  colors as akColors,
  fontSize as akFontSize,
  gridSize as akGridSize,
} from '@atlaskit/theme';

export const TotalContainer = styled.dl`
  margin: 0;
  padding: 0;
  display: flex;
  flex-direction: column;
  padding-right: ${akGridSize() * 4}px;
`;

export const SubtleHeading = styled.dt`
  margin: 0;
  padding: 0;
  color: ${akColors.N200};
  font-size: ${akFontSize() - 2}px;
`;

export const LargeNumber = styled.dd`
  margin: 0;
  padding: 0;
  color: ${akColors.text};
  font-size: 1.8em;
`;

export interface CountIndicatorProps {
  header: React.ReactNode;
  count: number | undefined;
}

export class CountIndicator extends React.Component<CountIndicatorProps> {
  public render() {
    const { header, count } = this.props;

    return (
      <TotalContainer>
        <SubtleHeading>{header}</SubtleHeading>
        <LargeNumber>{count === undefined ? '-' : <FormattedNumber value={count} />}</LargeNumber>
      </TotalContainer>
    );
  }
}
