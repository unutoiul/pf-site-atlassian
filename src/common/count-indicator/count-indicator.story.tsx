import { boolean, number } from '@storybook/addon-knobs';
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';

import { CountIndicator } from './count-indicator';

storiesOf('Common|Count Indicator', module)
  .add('Default', () => {
    const isCountUndefined = boolean('Count undefined', false);
    let count = number('Count', 143123);

    if (isCountUndefined) {
      count = undefined;
    }

    return (
      <IntlProvider locale="en">
        <CountIndicator header="Custom header" count={count} />
      </IntlProvider>
    );
  });
