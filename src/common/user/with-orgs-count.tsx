import { ApolloError } from 'apollo-client';
import { graphql, OptionProps } from 'react-apollo';

import withAccessCountsOrganizationsQuery from './with-access-counts-organizations.query.graphql';

import { WithAccessCountsOrganizationsQuery } from '../../schema/schema-types';

export interface WithOrgsCountProps {
  totalOrganizations: number | undefined;
  totalOrganizationsLoadError: ApolloError | undefined;
}

export const organizationsCountQuery = withAccessCountsOrganizationsQuery;

function getOrganizationsCount<TInProps>(props: OptionProps<TInProps, WithAccessCountsOrganizationsQuery>): WithOrgsCountProps['totalOrganizations'] {
  if (!props.data || props.data.loading || props.data.error || !props.data.currentUser || !props.data.currentUser.organizations) {
    return undefined;
  }

  return props.data.currentUser.organizations.length;
}

export function withOrganizationsCount<TInProps>(Component: React.ComponentType<TInProps & WithOrgsCountProps>): React.ComponentClass<TInProps> {
  return graphql<TInProps, WithAccessCountsOrganizationsQuery, {}, TInProps & WithOrgsCountProps>(
    organizationsCountQuery,
    {
      props: (p: OptionProps<TInProps, WithAccessCountsOrganizationsQuery>): TInProps & WithOrgsCountProps => (
        // tslint:disable-next-line:prefer-object-spread Typescript fails to spread a generic type, this is a workaround mentioned here: https://github.com/Microsoft/TypeScript/pull/13288#issuecomment-367396818
        Object.assign(
          {},
          p.ownProps,
          {
            totalOrganizations: getOrganizationsCount(p),
            totalOrganizationsLoadError: p.data && p.data.error,
          },
        )
      ),
    },
  )(Component);
}
