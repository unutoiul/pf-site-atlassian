import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import AkAvatar from '@atlaskit/avatar';
import AkDropdownMenu from '@atlaskit/dropdown-menu';
import { AkGlobalItem } from '@atlaskit/navigation';

import { analyticsClient } from 'common/analytics';
import { getConfig } from 'common/config';

import {
  globalUserMenuButtonEvent,
  UserMenuImpl,
} from './user-menu';

import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';

describe('User Menu', () => {
  describe('rendering', () => {
    it('should render DropdownMenu, AkGlobalItem, and Avatar', () => {
      const data = {
        currentUser: {
          id: 'foo',
        },
      };
      const menu = shallow(
        <UserMenuImpl data={data} intl={createMockIntlProp()} {...{} as any} />,
        createMockIntlContext(),
      );
      expect(menu.find(AkDropdownMenu).length).to.equal(1);
      expect(menu.find(AkGlobalItem).length).to.equal(1);
      expect(menu.find(AkAvatar).length).to.equal(1);
    });
  });
  describe('functionality', () => {
    it('should populate DropdownMenu and Avatar based on props', () => {
      const data = {
        currentUser: {
          id: '557057:26ead946-4955-46ef-a11e-b1f9ae987de7',
        },
      };
      const menu = shallow(
        <UserMenuImpl data={data} intl={createMockIntlProp()} {...{} as any} />,
        createMockIntlContext(),
      );

      const menuSections: any[] = (menu.find(AkDropdownMenu).props()).items!;
      const items = menuSections[0].items;
      const avatar = menu.find(AkAvatar);
      expect(items[1].href).to.equal(getConfig().atlassianAccountUrl);
      expect(items[2].href).to.equal('/logout');
      expect(items[0].content).to.equal('Give feedback');
      expect(items[0].id).to.equal('give_feedback');
      expect(avatar.prop('src')).to.equal(`${getConfig().avatarUrl}/557057:26ead946-4955-46ef-a11e-b1f9ae987de7?by=id&s=48`);
    });
  });
  describe('analytics', () => {
    let sandbox;
    let userMenu;
    let eventHandlerSpy;

    beforeEach(() => {
      sandbox = sinon.sandbox.create();
      eventHandlerSpy = sandbox.spy(analyticsClient, 'onEvent');

      const data = {
        currentUser: {
          id: 'foo',
        },
      };
      userMenu = shallow(
        <UserMenuImpl data={data} intl={createMockIntlProp()} {...{} as any} />,
        createMockIntlContext(),
      );
    });

    afterEach(() => {
      sandbox.restore();
    });

    it('should emit global button event on click', () => {
      const userMenuItem = userMenu.find(AkGlobalItem).at(0);

      userMenuItem.simulate('click', {
        preventDefault: () => (null),
      });

      expect(eventHandlerSpy.calledWith(
        globalUserMenuButtonEvent,
      )).to.equal(true);
    });
  });
});
