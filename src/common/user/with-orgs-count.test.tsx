import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import { ChildProps } from 'react-apollo';
import { MockedProvider } from 'react-apollo/test-utils';

import { WithAccessCountsOrganizationsQuery } from '../../schema/schema-types';
import { organizationsCountQuery, withOrganizationsCount, WithOrgsCountProps } from './with-orgs-count';

describe('withOrganizationsCount', () => {
  type MockOrgData = Partial<ChildProps<{}, WithAccessCountsOrganizationsQuery>['data']>;

  async function mountTestComponent(
    mockData: MockOrgData,
    propAssertions: (props: WithOrgsCountProps) => void,
  ) {
    return new Promise(done => {
      class TestComponentImpl extends React.Component<WithOrgsCountProps> {
        public componentWillReceiveProps(props: WithOrgsCountProps) {
          propAssertions(props);
          done();
          // invoke the callback once, and stop asserting after that by setting this function to noop
          this.componentWillReceiveProps = () => void 0;
        }
        public render() {
          return null;
        }
      }
      const TestComponent = withOrganizationsCount<{}>(TestComponentImpl);

      const mocks = [
        { request: { query: organizationsCountQuery }, result: { data: mockData } },
      ];

      mount((
        <MockedProvider mocks={mocks} addTypename={false}>
          <TestComponent />
        </MockedProvider>
      ));
    });
  }

  it('should only propagate known props when loading', async () => {
    const mockData: MockOrgData = { loading: true, currentUser: null };

    await mountTestComponent(mockData, props => {
      const propNames = Object.keys(props);
      expect(propNames).to.have.length(2);
      expect(propNames).to.include('totalOrganizations');
      expect(propNames).to.include('totalOrganizationsLoadError');
    });
  });

  it('should only propagate known props when loaded', async () => {
    const mockData: MockOrgData = { currentUser: { __typename: '', id: 'test-account-id', organizations: [{ __typename: '', id: '1' }] } };

    await mountTestComponent(mockData, props => {
      const propNames = Object.keys(props);
      expect(propNames).to.have.length(2);
      expect(propNames).to.include('totalOrganizations');
      expect(propNames).to.include('totalOrganizationsLoadError');
    });
  });

  it('should keep count as undefined while the data is loading', async () => {
    const mockData: MockOrgData = { loading: true, currentUser: null };

    await mountTestComponent(mockData, props => {
      expect(props.totalOrganizations).to.eq(undefined);
      expect(props.totalOrganizationsLoadError).to.eq(undefined);
    });
  });

  it('should count as soon as it loads with no orgs', async () => {
    const mockData: MockOrgData = { currentUser: { __typename: '', id: 'test-account-id', organizations: [] } };

    await mountTestComponent(mockData, props => {
      expect(props.totalOrganizations).to.eq(0);
      expect(props.totalOrganizationsLoadError).to.eq(undefined);
    });
  });

  it('should count as soon as it loads with 1 org', async () => {
    const mockData: MockOrgData = { currentUser: { __typename: '', id: 'test-account-id', organizations: [{ __typename: '', id: '1' }] } };

    await mountTestComponent(mockData, props => {
      expect(props.totalOrganizations).to.eq(1);
      expect(props.totalOrganizationsLoadError).to.eq(undefined);
    });
  });

  it('should count as soon as it loads with 2 orgs', async () => {
    const mockData: MockOrgData = { currentUser: { __typename: '', id: 'test-account-id', organizations: [{ __typename: '', id: '1' }, { __typename: '', id: '2' }] } };

    await mountTestComponent(mockData, props => {
      expect(props.totalOrganizations).to.eq(2);
      expect(props.totalOrganizationsLoadError).to.eq(undefined);
    });
  });
});
