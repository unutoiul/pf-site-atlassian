import { ApolloError } from 'apollo-client';
import { graphql, OptionProps } from 'react-apollo';

import withAccessCountsSitesQuery from './with-access-counts-sites.query.graphql';

import { WithAccessCountsSitesQuery } from '../../schema/schema-types';

export interface WithSitesCountProps {
  totalSites: number | undefined;
  totalSitesLoadError: ApolloError | undefined;
}

function getSitesCount<TInProps>(props: OptionProps<TInProps, WithAccessCountsSitesQuery>): WithSitesCountProps['totalSites'] {
  if (!props.data || props.data.loading || props.data.error || !props.data.currentUser || !props.data.currentUser.sites) {
    return undefined;
  }

  return props.data.currentUser.sites.length;
}

export const sitesCountQuery = withAccessCountsSitesQuery;

export function withSitesCount<TInProps>(Component: React.ComponentType<TInProps & WithSitesCountProps>): React.ComponentClass<TInProps> {
  return graphql<TInProps, WithAccessCountsSitesQuery, {}, TInProps & WithSitesCountProps>(
    sitesCountQuery,
    {
      props: (p: OptionProps<TInProps, WithAccessCountsSitesQuery>): TInProps & WithSitesCountProps => (
        // tslint:disable-next-line:prefer-object-spread Typescript fails to spread a generic type, this is a workaround mentioned here: https://github.com/Microsoft/TypeScript/pull/13288#issuecomment-367396818
        Object.assign(
          {},
          p.ownProps,
          {
            totalSites: getSitesCount(p),
            totalSitesLoadError: p.data && p.data.error,
          },
        )
      ),
    },
  )(Component);
}
