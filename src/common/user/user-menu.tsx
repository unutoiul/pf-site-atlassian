import * as React from 'react';
import { ChildProps, compose, graphql } from 'react-apollo';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import AkAvatar from '@atlaskit/avatar';
import AkDropdownMenu from '@atlaskit/dropdown-menu';
import { AkGlobalItem } from '@atlaskit/navigation';

import {
  analyticsClient,
  AnalyticsData,
} from 'common/analytics';
import { getAvatarUrlByUserId } from 'common/avatar/avatar.actions';
import { getConfig } from 'common/config';
import { NavigationTooltip } from 'common/navigation-tooltip';

import updateIsFeedbackOpenMutation from './update-is-feedback-open.mutation.graphql';
import userMenuQuery from './user-menu.query.graphql';

import { UpdateIsFeedbackOpenMutation, UserMenuQuery } from '../../schema/schema-types';

export const globalUserMenuButtonEvent: AnalyticsData = {
  action: 'click',
  subproduct: 'chrome',
  actionSubject: 'sidebarGlobalButton',
  actionSubjectId: 'user-menu',
};

export const userMenuItemProfileEvent: AnalyticsData = {
  action: 'click',
  subproduct: 'chrome',
  actionSubject: 'sidebarGlobalButton',
  actionSubjectId: 'user-menu.profile',
};

export const userMenuItemLogoutEvent: AnalyticsData = {
  action: 'click',
  subproduct: 'chrome',
  actionSubject: 'sidebarGlobalButton',
  actionSubjectId: 'user-menu.logout',
};

export const userMenuItemFeedbackEvent: AnalyticsData = {
  action: 'click',
  subproduct: 'chrome',
  actionSubject: 'sidebarGlobalButton',
  actionSubjectId: 'user-menu.feedback',
};

const GIVE_FEEDBACK_ID = 'give_feedback';

const messages = defineMessages({
  yourProfile: {
    id: 'pf-site-admin-ui.your-profile-link-text',
    defaultMessage: 'Your profile',
  },
  logout: {
    id: 'pf-site-admin-ui.logout-link-text',
    defaultMessage: 'Log out',
  },
  giveFeedback: {
    id: 'pf-site-admin-ui.give-feedback',
    defaultMessage: 'Give feedback',
  },
  globalItemButtonTooltip: {
    id: 'pf-site-admin-ui.tooltip.user-menu',
    defaultMessage: 'Your profile and settings',
  },
});

export interface Props {
  showFeedback(): void;
}

export class UserMenuImpl extends React.Component<ChildProps<Props & InjectedIntlProps, UserMenuQuery>> {

  public render() {
    const { formatMessage } = this.props.intl;

    const src = this.props.data && !this.props.data.loading && this.props.data.currentUser
      ? getAvatarUrlByUserId(this.props.data.currentUser.id)
      : undefined;

    return (
      <AkDropdownMenu
        items={this.createDropdownItems()}
        shouldFlip={false}
        position="right bottom"
        onItemActivated={this.onItemActivated}
      >
        <NavigationTooltip message={formatMessage(messages.globalItemButtonTooltip)}>
          <AkGlobalItem onClick={this.onClick}>
            <AkAvatar
              src={src}
              size="small"
            />
          </AkGlobalItem>
        </NavigationTooltip>
      </AkDropdownMenu>
    );
  }

  private createDropdownItems() {
    const { formatMessage } = this.props.intl;

    const items = [
      {
        content: formatMessage(messages.giveFeedback),
        href: '',
        id: GIVE_FEEDBACK_ID,
        analyticsData: userMenuItemFeedbackEvent,
      }, {
        content: formatMessage(messages.yourProfile),
        href: getConfig().atlassianAccountUrl,
        target: '_blank',
        analyticsData: userMenuItemProfileEvent,
      }, {
        content: formatMessage(messages.logout),
        href: getConfig().logout,
        analyticsData: userMenuItemLogoutEvent,
      },
    ];

    return [
      {
        items,
      },
    ];
  }

  private onClick = e => {
    e.preventDefault();
    analyticsClient.eventHandler(globalUserMenuButtonEvent);
  }

  private onItemActivated = (item) => {
    if (item.item.id === GIVE_FEEDBACK_ID) {
      this.props.showFeedback();
    }
    if (item.item.analyticsData) {
      analyticsClient.eventHandler(item.item.analyticsData);
    }
  }
}

const openFeedbackMutation = graphql<Props, UpdateIsFeedbackOpenMutation, {}, Props>(
  updateIsFeedbackOpenMutation,
  {
    props: ({ mutate }) => ({
      showFeedback: async () => mutate instanceof Function && mutate({}),
    }),
  },
);

export const UserMenu = compose(
  graphql<UserMenuQuery>(userMenuQuery),
  openFeedbackMutation,
  injectIntl,
)(UserMenuImpl);
