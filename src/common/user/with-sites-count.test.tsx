import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import { ChildProps } from 'react-apollo';
import { MockedProvider } from 'react-apollo/test-utils';

import { WithAccessCountsSitesQuery } from '../../schema/schema-types';
import { sitesCountQuery, withSitesCount, WithSitesCountProps } from './with-sites-count';

describe('withSitesCount', () => {
  type MockSiteData = Partial<ChildProps<{}, WithAccessCountsSitesQuery>['data']>;

  async function mountTestComponent(
    mockData: MockSiteData,
    propAssertions: (props: WithSitesCountProps) => void,
  ) {
    return new Promise(done => {
      class TestComponentImpl extends React.Component<WithSitesCountProps> {
        public componentWillReceiveProps(props: WithSitesCountProps) {
          propAssertions(props);
          done();
          // invoke the callback once, and stop asserting after that by setting this function to noop
          this.componentWillReceiveProps = () => void 0;
        }
        public render() {
          return null;
        }
      }
      const TestComponent = withSitesCount<{}>(TestComponentImpl);

      const mocks = [
        { request: { query: sitesCountQuery }, result: { data: mockData } },
      ];

      mount((
        <MockedProvider mocks={mocks} addTypename={false}>
          <TestComponent />
        </MockedProvider>
      ));
    });
  }

  it('should only propagate known props when loading', async () => {
    const mockData: MockSiteData = { loading: true, currentUser: null };

    await mountTestComponent(mockData, props => {
      const propNames = Object.keys(props);
      expect(propNames).to.have.length(2);
      expect(propNames).to.include('totalSites');
      expect(propNames).to.include('totalSitesLoadError');
    });
  });

  it('should only propagate known props when loaded', async () => {
    const mockData: MockSiteData = { currentUser: { __typename: '', id: 'test-account-id', sites: [{ __typename: '', id: '1' }] } };

    await mountTestComponent(mockData, props => {
      const propNames = Object.keys(props);
      expect(propNames).to.have.length(2);
      expect(propNames).to.include('totalSites');
      expect(propNames).to.include('totalSitesLoadError');
    });
  });

  it('should keep count as undefined while the data is loading', async () => {
    const mockData: MockSiteData = { loading: true, currentUser: null };

    await mountTestComponent(mockData, props => {
      expect(props.totalSites).to.eq(undefined);
      expect(props.totalSitesLoadError).to.eq(undefined);
    });
  });

  it('should count as soon as it loads with no sites', async () => {
    const mockData: MockSiteData = { currentUser: { __typename: '', id: 'test-account-id', sites: [] } };

    await mountTestComponent(mockData, props => {
      expect(props.totalSites).to.eq(0);
      expect(props.totalSitesLoadError).to.eq(undefined);
    });
  });

  it('should count as soon as it loads with 1 site', async () => {
    const mockData: MockSiteData = { currentUser: { __typename: '', id: 'test-account-id', sites: [{ __typename: '', id: '1' }] } };

    await mountTestComponent(mockData, props => {
      expect(props.totalSites).to.eq(1);
      expect(props.totalSitesLoadError).to.eq(undefined);
    });
  });

  it('should count as soon as it loads with 2 sites', async () => {
    const mockData: MockSiteData = { currentUser: { __typename: '', id: 'test-account-id', sites: [{ __typename: '', id: '1' }, { __typename: '', id: '2' }] } };

    await mountTestComponent(mockData, props => {
      expect(props.totalSites).to.eq(2);
      expect(props.totalSitesLoadError).to.eq(undefined);
    });
  });
});
