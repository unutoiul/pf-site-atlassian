import * as React from 'react';
import { graphql } from 'react-apollo';

import { Drawer, DrawerCloseSubscriberQuery } from '../../schema/schema-types';
import drawerCloseSubscriberQuery from './with-drawer-close-subscriber.query.graphql';

interface OwnProps {
  wrappedComponent: React.ComponentType<WithDrawerCloseSubscriberProps>;
}
export interface WithDrawerCloseSubscriberProps {
  whenDrawerClosed(callback: () => void, drawerName?: Drawer): void;
}

interface QueryDerivedProps {
  activeDrawer: Drawer | undefined;
}

export class WithDrawerCloseSubscriberImpl extends React.Component<QueryDerivedProps & OwnProps> {
  private callback?: () => void;
  private drawerName?: string;

  public render() {
    const { wrappedComponent: WrappedComponent } = this.props;

    return (
      <WrappedComponent
        {...this.props}
        whenDrawerClosed={this.subscribe}
      />
    );
  }

  public componentDidUpdate(prevProps: WithDrawerCloseSubscriberImpl['props']) {
    if (!this.callback || this.props.activeDrawer !== 'NONE') {
      return;
    }

    // if a specific drawer name wasn't provided with callback, then call callback when any drawer is closed
    if (!this.drawerName && prevProps.activeDrawer !== 'NONE') {
      this.callback();
    }

    // if drawer name was provided, only callback if the drawer that was closed matches
    if (prevProps.activeDrawer === this.drawerName) {
      this.callback();
    }
  }

  private subscribe = (callback: () => void, drawerName?: Drawer) => {
    this.callback = callback;
    this.drawerName = drawerName;
  }
}

export function withDrawerCloseSubscriber<TOwnProps>(WrappedComponent: React.ComponentType<TOwnProps & WithDrawerCloseSubscriberProps>): React.ComponentClass<TOwnProps> {

  // tslint:disable-next-line:max-classes-per-file
  class WithDrawerCloseSubscriberWrapper extends React.Component<TOwnProps & QueryDerivedProps> {
    public render() {
      return <WithDrawerCloseSubscriberImpl {...this.props} wrappedComponent={WrappedComponent} />;
    }
  }

  const withDrawerCloseSubscriberQuery = graphql<TOwnProps, DrawerCloseSubscriberQuery, {}, QueryDerivedProps>(
    drawerCloseSubscriberQuery,
    {
      props: ({ data }): QueryDerivedProps => {
        return {
          activeDrawer: data && data.ui && data.ui.navigation.activeDrawer,
        };
      },
      options: {
        fetchPolicy: 'cache-only',
      },
    },
  );

  return withDrawerCloseSubscriberQuery(WithDrawerCloseSubscriberWrapper);
}
