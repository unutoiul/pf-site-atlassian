import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import * as React from 'react';
import { ChildProps } from 'react-apollo';
import { MockedProvider } from 'react-apollo/test-utils';
import * as sinon from 'sinon';

import { DrawerCloseSubscriberQuery } from '../../schema/schema-types';
import { withDrawerCloseSubscriber, WithDrawerCloseSubscriberImpl, WithDrawerCloseSubscriberProps } from './with-drawer-close-subscriber';
import drawerCloseSubscriberQuery from './with-drawer-close-subscriber.query.graphql';

describe('withDrawerCloseSubscriber', () => {

  const sandbox = sinon.sandbox.create();
  const callbackSpy = sandbox.spy();

  afterEach(() => {
    sandbox.reset();
  });

  class TestComponent extends React.Component<WithDrawerCloseSubscriberProps> {
    public render() {
      return null;
    }
  }

  const getDrawerCloseSubscriber = () => {
    const TestWrappedComponent = withDrawerCloseSubscriber<{}>(TestComponent);

    const mockData: Partial<ChildProps<{}, DrawerCloseSubscriberQuery>['data']> = {
      ui: {
        __typename: 'UI',
        navigation: {
          __typename: 'Navigation',
          activeDrawer: 'ORG_SITE_LINKING',
        },
      },
    };

    const mocks = [
      { request: { query: drawerCloseSubscriberQuery }, result: { data: mockData } },
    ];

    return mount((
      <MockedProvider mocks={mocks} addTypename={false}>
        <TestWrappedComponent />
      </MockedProvider>
    ));
  };

  it('should render', () => {
    const wrapper = getDrawerCloseSubscriber();
    const component = wrapper.find(WithDrawerCloseSubscriberImpl);

    expect(component.exists()).to.equal(true);
  });

  it('should call callback when any drawer closes', async () => {
    const wrapper = shallow<WithDrawerCloseSubscriberImpl>(<WithDrawerCloseSubscriberImpl wrappedComponent={TestComponent} activeDrawer="ORG_SITE_LINKING" />);
    const component = wrapper.find(TestComponent);

    component.props().whenDrawerClosed(callbackSpy);
    wrapper.setProps({ activeDrawer: 'NONE' });

    expect(callbackSpy.called).to.equal(true);
  });

  it('should call callback when specified drawer closes', async () => {
    const wrapper = shallow<WithDrawerCloseSubscriberImpl>(<WithDrawerCloseSubscriberImpl wrappedComponent={TestComponent} activeDrawer="ORG_SITE_LINKING" />);
    const component = wrapper.find(TestComponent);

    component.props().whenDrawerClosed(callbackSpy, 'ORG_SITE_LINKING');
    wrapper.setProps({ activeDrawer: 'NONE' });

    expect(callbackSpy.called).to.equal(true);
  });

  it('should not call callback when a non-specified drawer closes', async () => {
    const wrapper = shallow<WithDrawerCloseSubscriberImpl>(<WithDrawerCloseSubscriberImpl wrappedComponent={TestComponent} activeDrawer="ORG_SITE_LINKING" />);
    const component = wrapper.find(TestComponent);

    component.props().whenDrawerClosed(callbackSpy, 'CREATE_ADMIN_API_KEY_DRAWER');
    wrapper.setProps({ activeDrawer: 'NONE' });

    expect(callbackSpy.called).to.equal(false);
  });

});
