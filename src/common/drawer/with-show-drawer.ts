import * as React from 'react';
import { graphql } from 'react-apollo';

import { analyticsClient } from 'common/analytics';

import { Drawer, ShowDrawerMutation, ShowDrawerMutationVariables } from '../../schema/schema-types';
import showDrawerMutation from './with-show-drawer.mutation.graphql';

export interface ShowDrawerProps {
  showDrawer(drawer: Drawer): void;
}

const withShowDrawerMutation = graphql<any, ShowDrawerMutation, ShowDrawerMutationVariables, ShowDrawerProps>(
  showDrawerMutation,
  {
    props: (options): ShowDrawerProps => ({
      showDrawer: (drawer: Drawer) => {
        if (!(options.mutate instanceof Function)) {
          throw new Error('"showDrawer" mutation function missing');
        }

        options.mutate({
          variables: { drawer } as ShowDrawerMutationVariables,
        }).catch((error) => {
          analyticsClient.onError(error);
        });
      },
    }),
  },
);

export function withShowDrawer<TOwnProps>(Component: React.ComponentType<TOwnProps & ShowDrawerProps>): React.ComponentClass<TOwnProps> {
  return withShowDrawerMutation(Component);
}
