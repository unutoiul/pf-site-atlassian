import { envType } from '@atlassiansox/analytics-web-client';

function getEnvironment(): DeployEnv {
  if (__NODE_ENV__ === 'development') {
    return 'local';
  }

  if (__NODE_ENV__ === 'test') {
    return window.__env__ || 'local';
  }

  return window.__env__;
}

interface EnvironmentConfig<T = string> {
  local?: T;
  /**
   * This should point to **staging** environment of the service in question.
   * The login procedure goes through staging environment right now, and it won't
   * set cookies necessary for authentication in a ddev environment.
   * Consistency matters, and since not all of the services have ddev version,
   * we simply rely on their staging versions, which are more prod-like anyway.
   */
  ddev: T;
  'stg-east': T;
  'prod-east': T;
  'prod-west': T;
}

interface RedirectUrlConfig {
  url: string;
  returnParamName: string;
}

interface Config {
  signup: RedirectUrlConfig;
  login: RedirectUrlConfig;
  logout: string;
  avatarUrl: string;
  atlassianAccountUrl: string;
  featureFlagClient: string;
  directoryUrl: string;
  webAnalyticsConfig: WebAnalyticsConfig;
  feedbackUrl: string;
  apiUrl: string;
  siteAdminAvatarCdn: string;
  orgUrl: string;
  goToAdminLinkUrl: string;
  billingUrl: string;
  externalDirectoryUrl: string;
  identityFeatureFlagUrl: string;
  strideUrl: string;
  caasUrl: string;
  cnasUrl: string;
  cofsUrl: string;
  cadsUrl: string;
  gSuiteBasePathLegacy: string;
  gSuiteBasePath: string;
  umBasePath: string;
  umBasePathDirect: string;
  jiraUrl: string;
  xflowBasePath: string;
  analyticsClientEnvironment: string;
  newAnalyticsClientEnvironment: string;
}

const localhost = typeof window === 'undefined'
  ? 'http://localhost'
  : `${window.location.protocol}//${window.location.hostname}`;

const signupConfig: EnvironmentConfig<RedirectUrlConfig> = {
  local: {
    url: '/admin/dev-local-login',
    returnParamName: 'dest-url',
  },
  ddev: {
    url: '/login',
    returnParamName: 'dest-url',
  },
  'stg-east': {
    url: '/login',
    returnParamName: 'dest-url',
  },
  'prod-east': {
    url: '/login',
    returnParamName: 'dest-url',
  },
  'prod-west': {
    url: '/login',
    returnParamName: 'dest-url',
  },
};

const loginConfig: EnvironmentConfig<RedirectUrlConfig> = {
  local: {
    url: '/admin/dev-local-login',
    returnParamName: 'dest-url',
  },
  ddev: {
    url: 'https://id.stg.internal.atlassian.com/login',
    returnParamName: 'continue',
  },
  'stg-east': {
    url: 'https://id.stg.internal.atlassian.com/login',
    returnParamName: 'continue',
  },
  'prod-east': {
    url: 'https://id.atlassian.com/login',
    returnParamName: 'continue',
  },
  'prod-west': {
    url: 'https://id.atlassian.com/login',
    returnParamName: 'continue',
  },
};

const logoutConfig: EnvironmentConfig = {
  local: '/logout',
  ddev: 'https://id.stg.internal.atlassian.com/logout',
  'stg-east': 'https://id.stg.internal.atlassian.com/logout',
  'prod-east': 'https://id.atlassian.com/logout',
  'prod-west': 'https://id.atlassian.com/logout',
};

const avatarUrlConfig: EnvironmentConfig = {
  local: 'https://avatar-cdn.atlassian.com',
  ddev: 'https://avatar-cdn.atlassian.com',
  'stg-east': 'https://avatar-cdn.stg.internal.atlassian.com',
  'prod-east': 'https://avatar-cdn.atlassian.com',
  'prod-west': 'https://avatar-cdn.atlassian.com',
};

const atlassianAccountUrlConfig: EnvironmentConfig = {
  local: 'https://id.stg.internal.atlassian.com',
  ddev: 'https://id.stg.internal.atlassian.com',
  'stg-east': 'https://id.stg.internal.atlassian.com',
  'prod-east': 'https://id.atlassian.com',
  'prod-west': 'https://id.atlassian.com',
};

const featureFlagClientConfig: EnvironmentConfig = {
  local: '59966b09904d620b14af2043',
  ddev: '59a8f19b7cc2130b315dcc9e',
  'stg-east': '59a8f19b7cc2130b315dcc9e',
  'prod-east': '59966b09904d620b14af2044',
  'prod-west': '59966b09904d620b14af2044',
};

const feedbackConfig: EnvironmentConfig = {
  local: `${localhost}:3002`,
  ddev: 'https://pf-feedback-proxy.us-east-1.staging.public.atl-paas.net',
  'stg-east': 'https://pf-feedback-proxy.us-east-1.staging.public.atl-paas.net',
  'prod-east': 'https://pf-feedback-proxy.us-east-1.staging.public.atl-paas.net',
  'prod-west': 'https://pf-feedback-proxy.us-east-1.staging.public.atl-paas.net',
};

const apiUrlConfig: EnvironmentConfig = {
  local: `${localhost}:3002/gateway/api`,
  ddev: '/gateway/api',
  'stg-east': '/gateway/api',
  'prod-east': '/gateway/api',
  'prod-west': '/gateway/api',
};

const caasUrlConfig: EnvironmentConfig = {
  local: '/caas',
  ddev: 'https://connect.staging.public.atl-paas.net',
  'stg-east': 'https://connect.staging.public.atl-paas.net',
  'prod-east': 'https://connect.prod.public.atl-paas.net',
  'prod-west': 'https://connect.prod.public.atl-paas.net',
};

interface WebAnalyticsConfig {
  segmentApiKey: string;
}
const webAnalyticsConfig: EnvironmentConfig<WebAnalyticsConfig> = {
  local: { segmentApiKey: '99lmm2XPSxf3JKyvRhlnjvQDqKTZkd6i' },
  ddev: { segmentApiKey: '99lmm2XPSxf3JKyvRhlnjvQDqKTZkd6i' },
  'stg-east': { segmentApiKey: '99lmm2XPSxf3JKyvRhlnjvQDqKTZkd6i' },
  'prod-east': { segmentApiKey: 'MM28W4LN7uacO0sTj1Tw9MHp4k16oyqV' },
  'prod-west': { segmentApiKey: 'MM28W4LN7uacO0sTj1Tw9MHp4k16oyqV' },
};

const siteAdminAvatarCdnConfig: EnvironmentConfig = {
  local: 'https://site-admin-avatar-cdn.stg.public.atl-paas.net',
  ddev: 'https://site-admin-avatar-cdn.stg.public.atl-paas.net',
  'stg-east': 'https://site-admin-avatar-cdn.stg.public.atl-paas.net',
  'prod-east': 'https://site-admin-avatar-cdn.prod.public.atl-paas.net',
  'prod-west': 'https://site-admin-avatar-cdn.prod.public.atl-paas.net',
};

const goToAdminLinkUrl: EnvironmentConfig = {
  local: `${localhost}:${typeof window !== 'undefined' && window.location.port || 3001}`,
  ddev: 'https://admin.stg.atlassian.com',
  'stg-east': 'https://admin.stg.atlassian.com',
  'prod-east': 'https://admin.atlassian.com',
  'prod-west': 'https://admin.atlassian.com',
};

const newAnalyticsClientEnvironmentConfig: EnvironmentConfig = {
  local: envType.LOCAL,
  ddev: envType.DEV,
  'stg-east': envType.STAGING,
  'prod-east': envType.PROD,
  'prod-west': envType.PROD,
};

function getConfigWithDefaults<T>(environmentConfig: EnvironmentConfig<T>) {
  return environmentConfig[getEnvironment()] || environmentConfig['prod-east'];
}

export function getConfig(): Config {
  return {
    signup: getConfigWithDefaults(signupConfig),
    login: getConfigWithDefaults(loginConfig),
    logout: getConfigWithDefaults(logoutConfig),
    avatarUrl: getConfigWithDefaults(avatarUrlConfig),
    atlassianAccountUrl: getConfigWithDefaults(atlassianAccountUrlConfig),
    featureFlagClient: getConfigWithDefaults(featureFlagClientConfig),
    directoryUrl: `${getConfigWithDefaults(apiUrlConfig)}/directory`,
    webAnalyticsConfig: getConfigWithDefaults(webAnalyticsConfig),
    feedbackUrl: getConfigWithDefaults(feedbackConfig),
    apiUrl: getConfigWithDefaults(apiUrlConfig),
    siteAdminAvatarCdn: getConfigWithDefaults(siteAdminAvatarCdnConfig),
    orgUrl: `${getConfigWithDefaults(apiUrlConfig)}/adminhub/organization`,
    goToAdminLinkUrl: getConfigWithDefaults(goToAdminLinkUrl),
    billingUrl: `${getConfigWithDefaults(apiUrlConfig)}/billing-ux`,
    externalDirectoryUrl: `${getConfigWithDefaults(apiUrlConfig)}/adminhub/external-directory/manage-directory`,
    identityFeatureFlagUrl: '/admin/rest/um/1/features',
    strideUrl: `${getConfigWithDefaults(apiUrlConfig)}/chat/site`,
    caasUrl: `${getConfigWithDefaults(caasUrlConfig)}`,
    cnasUrl: `${getConfigWithDefaults(apiUrlConfig)}/hostname/cloud`,
    cofsUrl: `${getConfigWithDefaults(apiUrlConfig)}/cofs`,
    cadsUrl: `/gateway/api/adminhub/customer-directory`,
    gSuiteBasePathLegacy: '/adminhub/google-sync-legacy/clouds',
    gSuiteBasePath: '/gateway/api/adminhub/google-sync-legacy/clouds',
    umBasePath: '/gateway/api/adminhub/um/site',
    umBasePathDirect: '/admin/rest/web/site',
    jiraUrl: '/gateway/api/ex/jira',
    xflowBasePath: `${getConfigWithDefaults(apiUrlConfig)}/xflow`,
    analyticsClientEnvironment: getEnvironment(),
    newAnalyticsClientEnvironment: getConfigWithDefaults(newAnalyticsClientEnvironmentConfig),
  };
}
