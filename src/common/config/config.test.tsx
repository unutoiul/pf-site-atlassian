import { expect } from 'chai';

import { getConfig } from './config';

function setEnv(env) {
  window.__env__ = env;
}

describe('Config', () => {
  let initialEnv;
  before(() => {
    initialEnv = window.__env__;
  });

  beforeEach(() => {
    setEnv(undefined);
  });

  after(() => {
    setEnv(initialEnv);
  });

  it('should have relative loginUrl URL during local dev, and absolute loginUrl URL for other environments', () => {
    setEnv('local');
    expect(getConfig().login.url.indexOf('/')).to.equal(0);
    setEnv('ddev');
    expect(getConfig().login.url.indexOf('https://')).to.equal(0);
    setEnv('stg-east');
    expect(getConfig().login.url.indexOf('https://')).to.equal(0);
    setEnv('prod-east');
    expect(getConfig().login.url.indexOf('https://')).to.equal(0);
  });

  it('should take loginUrl from production config in unknown environments', () => {
    setEnv('__undefined environment__');
    const undefinedConfig = getConfig().login.url;

    setEnv('prod-east');
    const prodConfig = getConfig().login.url;
    expect(prodConfig).to.equal(undefinedConfig);
  });
});
