// tslint:disable:jsx-use-translation-function
import { storiesOf } from '@storybook/react';
import * as React from 'react';

import AkButton, { ButtonGroup as AkButtonGroup } from '@atlaskit/button';
import { Label as AkLabel } from '@atlaskit/field-base';
import AkTextField from '@atlaskit/field-text';

import {
  ActionsBarCheckCircleIcon,
  ActionsBarLabel,
  ActionsBarModalFooter,
  ActionsBarSmallButton,
} from '.';

storiesOf('Common|Actions Bar/Actions Bar Check Circle Icon', module)
  .add('With label', () => (
    <ActionsBarCheckCircleIcon label="Checked" />
  ));

storiesOf('Common|Actions Bar/Actions Bar Label', module)
  .add('Default', () => (
    <ActionsBarLabel>
      <AkLabel label="Default label" />
    </ActionsBarLabel>
  ));

storiesOf('Common|Actions Bar/Actions Bar Modal Footer', module)
  .add('Aligned left, with buttons', () => (
    <ActionsBarModalFooter alignment="left">
      <AkButtonGroup>
        <AkButton appearance="primary">
          Submit
        </AkButton>
        <AkButton>
          Cancel
        </AkButton>
      </AkButtonGroup>
    </ActionsBarModalFooter>
  ));

storiesOf('Common|Actions Bar/Actions Bar Small Button', module)
  .add('As a button', () => (
    <ActionsBarSmallButton>
      <AkButton appearance="primary">
        Submit
      </AkButton>
    </ActionsBarSmallButton>
  ))
  .add('As a text field', () => (
    <ActionsBarSmallButton>
      <AkTextField
        disabled={false}
        min={1}
        required={false}
        isLabelHidden={true}
        label="test"
        value="150"
        compact={true}
        name="passwordexpiry"
        type="number"
      />
    </ActionsBarSmallButton>
  ));
