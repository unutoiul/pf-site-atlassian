import * as React from 'react';
import styled from 'styled-components';

import AKCheckCircleIcon from '@atlaskit/icon/glyph/check-circle';
import { akColorG300, akGridSizeUnitless } from '@atlaskit/util-shared-styles';

import { ButtonGroup } from '../button-group';

const ActionsBar = styled.div`
  align-items: center;
  display: flex;
  padding-top: ${akGridSizeUnitless * 2.5}px;
`;

export const ActionsBarSearch = styled.div`
  flex-grow: 1;
`;

export const ActionsBarHeading = styled.div`
  flex-grow: 1;
`;

export const ActionsBarButton = styled.div`
  padding-left: 10px;
`;

export const ActionsBarSmallButton = styled.div`
  margin: 3px;
  width: ${akGridSizeUnitless * 8}px;
`;

export const ActionsBarLabel = styled.div`
  display: flex;
  font-weight: 600;
  position: absolute;
  margin-top: -${akGridSizeUnitless * 2}px;
`;

export const ActionsBarCheckCircleIcon = props => <AKCheckCircleIcon {...props} primaryColor={akColorG300} />;

export { ActionsBar, ButtonGroup as ActionsBarModalFooter };
