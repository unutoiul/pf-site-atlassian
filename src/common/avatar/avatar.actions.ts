import { getConfig } from 'common/config';

export function getAvatarUrlByUserId(id, sizePx = 48) {
  return `${getConfig().avatarUrl}/${id}?by=id&s=${sizePx}`;
}
