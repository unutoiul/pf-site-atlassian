import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import { SinonSpy, SinonStub, spy, stub } from 'sinon';

import { ComponentTimingRoot } from 'common/performance-metrics';

import { ComponentTiming } from './component-timing';
import { PerformanceProp, Reporter } from './types';

let mockPerformance: {
  mark: SinonSpy,
  measure: SinonSpy,
  getEntriesByName: SinonStub,
  now: SinonStub,
};

let mockReporter: {
  reportFirstMeaningfulData: SinonSpy,
  reportLoadEvent: SinonSpy,
};

interface WrapperProps {
  isLoaded: boolean;
  isMeaningful: boolean;
}

class Wrapper extends React.Component<WrapperProps> {
  public render() {
    return (
      <ComponentTimingRoot reporter={mockReporter as Reporter} performance={mockPerformance as PerformanceProp}>
        <ComponentTiming id="test" category="page" isLoaded={this.props.isLoaded} isMeaningful={this.props.isMeaningful} />
      </ComponentTimingRoot>
    );
  }
}

describe('Component timing', () => {
  beforeEach(() => {
    mockPerformance = {
      mark: spy(),
      measure: spy(),
      getEntriesByName: stub().returns([{ duration: 1 }, { duration: 2 }]),
      now: stub().returns(1),
    };

    mockReporter = {
      reportLoadEvent: spy(),
      reportFirstMeaningfulData: spy(),
    };
  });

  it('should render children', () => {
    const wrapper = mount(
      <ComponentTimingRoot reporter={mockReporter as Reporter} performance={mockPerformance as PerformanceProp}>
        <ComponentTiming id="test" category="page" isLoaded={true} isMeaningful={true}>
          <div id="hi" />
        </ComponentTiming>
      </ComponentTimingRoot>,
    );

    expect(wrapper.find('div').length).to.equal(1);
  });

  describe('with a reporter', () => {

    it('should report a load event when loaded is set to true', () => {
      const wrapper = mount(
        <Wrapper isMeaningful={true} isLoaded={false} />,
      );

      expect(mockReporter.reportLoadEvent.called).to.equal(false);

      wrapper.setProps({ isLoaded: true });

      expect(mockReporter.reportLoadEvent.called).to.equal(true);
    });

    it('should report a meaningful data event the first time the loading state is entered', () => {
      const wrapper = mount(
        <Wrapper isMeaningful={true} isLoaded={false} />,
      );

      expect(mockReporter.reportFirstMeaningfulData.callCount).to.equal(0);

      wrapper.setProps({ isLoaded: true });

      expect(mockReporter.reportFirstMeaningfulData.callCount).to.equal(1);

      wrapper.setProps({ isLoaded: false });
      wrapper.setProps({ isLoaded: true });

      expect(mockReporter.reportFirstMeaningfulData.callCount).to.equal(1);
    });

    it('should report a meaningful data event only if the event is meaningful', () => {
      const wrapper = mount(
        <Wrapper isMeaningful={false} isLoaded={false} />,
      );

      expect(mockReporter.reportFirstMeaningfulData.callCount).to.equal(0);

      wrapper.setProps({ isLoaded: true });

      expect(mockReporter.reportFirstMeaningfulData.callCount).to.equal(0);
    });

    it('should mark two performance entries and measure once when a load event happens', () => {
      const wrapper = mount(
        <Wrapper isMeaningful={true} isLoaded={false} />,
      );

      expect(mockReporter.reportFirstMeaningfulData.callCount).to.equal(0);

      wrapper.setProps({ isLoaded: true });

      expect(mockPerformance.mark.callCount).to.equal(2);
      expect(mockPerformance.measure.callCount).to.equal(1);
    });

    it('should mark and measure multiple times for multiple load events', () => {
      const wrapper = mount(
        <Wrapper isMeaningful={true} isLoaded={false} />,
      );

      expect(mockReporter.reportFirstMeaningfulData.callCount).to.equal(0);

      wrapper.setProps({ isLoaded: true });
      wrapper.setProps({ isLoaded: false });
      wrapper.setProps({ isLoaded: true });
      wrapper.setProps({ isLoaded: false });
      wrapper.setProps({ isLoaded: true });

      expect(mockPerformance.mark.callCount).to.equal(6);
      expect(mockPerformance.measure.callCount).to.equal(3);
    });

    it('should report a load event when the component mounts in the loaded state', () => {
      mount(
        <Wrapper isMeaningful={true} isLoaded={true} />,
      );

      expect(mockReporter.reportFirstMeaningfulData.called).to.equal(true);
      expect(mockReporter.reportLoadEvent.called).to.equal(true);
    });
  });

});
