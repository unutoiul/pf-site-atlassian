export function reportFirstInputDelay() {
  window.perfMetrics.onFirstInputDelay((delay: number, event: Event) => {

    if (__NODE_ENV__ !== 'production') {
      // tslint:disable-next-line:no-console
      console.log(`[Performance] ${delay} – first input delay. (type: ${event.type})`);
    }

    if (!window.newrelic) {
      return;
    }

    window.newrelic.addPageAction('metric.fid', {
      delay,
      type: event.type,
    });
  });
}
