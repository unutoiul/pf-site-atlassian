export type Category = 'page' | 'component';
export interface TimingEvent {
  time: number;
  duration: number;
  id: string;
  category: Category;
  isMeaningful: boolean;
}

export interface ComponentTimingRootContext {
  performance: PerformanceProp;
  onLoad(timingEvent: TimingEvent): void;
}

export interface Reporter {
  reportLoadEvent(timingEvent: TimingEvent): void;
  reportFirstMeaningfulData(timingEvent: TimingEvent): void;
}

export type PerformanceProp = Performance | Pick<Performance, 'mark' | 'measure' | 'getEntriesByName' | 'now'>;
