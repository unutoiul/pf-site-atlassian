import { analyticsClient } from 'common/analytics';

import { reportFirstInputDelay } from './first-input-delay';
import { reportTimeToInteractive } from './time-to-interactive';

export function reportPerformanceMetrics() {
  reportTimeToInteractive().catch(error => analyticsClient.onError(error));
  reportFirstInputDelay();
}
