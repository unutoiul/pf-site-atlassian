export async function reportTimeToInteractive() {
  const ttiPolyfill = await import(
    /* webpackChunkName: 'tti-polyfill' */
    'tti-polyfill',
  );

  ttiPolyfill.getFirstConsistentlyInteractive().then((tti) => {
    if (__NODE_ENV__ !== 'production') {
      // tslint:disable-next-line:no-console
      console.log(`[Performance] ${tti} – time to first consistently interactive`);
    }

    if (!window.newrelic) {
      return;
    }
    window.newrelic.addPageAction('metric.tti', {
      delay: tti,
    });
  });
}
