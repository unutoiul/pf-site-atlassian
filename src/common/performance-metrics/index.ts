export { Category, TimingEvent, ComponentTimingRootContext, Reporter } from './types';
export { ComponentTimingRoot } from './component-timing-root';
export { newRelicReporter } from './new-relic-reporter';
export { makeComponentTimingHoc } from './with-component-timing';
export { reportPerformanceMetrics } from './report-performance-metrics';
