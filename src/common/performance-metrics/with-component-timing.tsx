import * as React from 'react';

import { ComponentTiming } from './component-timing';
import { Category } from './types';

interface ComponentTimingHocOptions<TOwnProps> {
  id: string;
  category: Category;
  isMeaningful: boolean;
  isLoaded(props: TOwnProps): boolean;
}

export function makeComponentTimingHoc<TOwnProps>(options: ComponentTimingHocOptions<TOwnProps>) {
  // tslint:disable-next-line:no-unused
  return function withComponentTiming(WrappedComponent: React.ComponentType<TOwnProps>): React.ComponentClass<TOwnProps> {
    return class extends React.Component<TOwnProps> {

      public static WrappedComponent = WrappedComponent;

      public render() {
        return (
          <ComponentTiming
            id={options.id}
            key={options.id}
            category={options.category}
            isMeaningful={options.isMeaningful}
            isLoaded={options.isLoaded(this.props)}
          >
            <WrappedComponent
              {...this.props}
            />
          </ComponentTiming>
        );
      }
    };
  };

}
