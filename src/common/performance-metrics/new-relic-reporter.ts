import { Reporter, TimingEvent } from './types';

export const newRelicReporter: Reporter = {
  reportLoadEvent(timingEvent: TimingEvent) {
    if (__NODE_ENV__ !== 'production') {
      // tslint:disable-next-line:no-console
      console.log('[New relic] addPageAction', { timingEvent, now: window.performance.now() });
    }

    if (!window.newrelic) {
      return;
    }

    window.newrelic.addPageAction(`metric.load-event.${timingEvent.id}`, {
      time: timingEvent.time,
      duration: timingEvent.duration,
      category: timingEvent.category,
      isMeaningful: timingEvent.isMeaningful,
    });
  },

  reportFirstMeaningfulData(timingEvent: TimingEvent) {
    if (__NODE_ENV__ !== 'production') {
      // tslint:disable-next-line:no-console
      console.log('[New relic] finished', { timingEvent, now: window.performance.now() });
    }

    if (!window.newrelic) {
      return;
    }

    window.newrelic.finished();
  },
};
