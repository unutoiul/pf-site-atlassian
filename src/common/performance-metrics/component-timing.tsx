import * as React from 'react';

import { RootConsumer } from './component-timing-root';
import { Category, ComponentTimingRootContext } from './types';

interface OwnProps {
  id: string;
  category: Category;
  isLoaded: boolean;
  isMeaningful: boolean;
}

export class ComponentTiming extends React.Component<OwnProps> {
  private root: ComponentTimingRootContext = {
    onLoad: () => null,
    performance: window.performance,
  };
  private isLoading: boolean = false;

  public componentDidMount() {
    this.startTiming();
    if (this.props.isLoaded) {
      this.stopTiming();
    }
  }

  public componentWillUnmount() {
    this.stopTiming();
  }

  public componentDidUpdate(prevProps: OwnProps) {
    this.checkLoading(prevProps.isLoaded, this.props.isLoaded);
  }

  public render() {
    return (
      <RootConsumer>
        {(root) => {
          this.root = root;

          return this.props.children;
        }}
      </RootConsumer>
    );
  }

  private performance() {
    return this.root.performance;
  }

  private startTiming() {
    this.isLoading = true;
    this.performance().mark(this.getStartMarkName());
  }

  private stopTiming() {
    if (!this.isLoading) {
      return;
    }
    this.isLoading = false;
    this.performance().mark(this.getEndMarkName());
    this.performance().measure(
      this.getMeasureName(),
      this.getStartMarkName(),
      this.getEndMarkName(),
    );
    this.reportMostRecentLoadTiming();
  }

  private checkLoading(prevIsLoaded: boolean, isLoaded: boolean) {
    if (!prevIsLoaded && isLoaded) {
      this.stopTiming();

    } else if (prevIsLoaded && !isLoaded) {
      this.startTiming();
    }
  }

  private reportMostRecentLoadTiming() {
    this.root.onLoad({
      id: this.props.id,
      category: this.props.category,
      duration: this.getMostRecentDuration(),
      time: this.performance().now(),
      isMeaningful: this.props.isMeaningful,
    });
  }

  private getMostRecentDuration() {
    const performanceEntries = this.performance().getEntriesByName(
      this.getMeasureName(),
      'measure',
    );

    return performanceEntries[performanceEntries.length - 1].duration;
  }

  private getStartMarkName() {
    return `${this.props.id}-load-start`;
  }

  private getEndMarkName() {
    return `${this.props.id}-load-end`;
  }

  private getMeasureName() {
    return `⌛️ ${this.props.id} [load]`;
  }
}
