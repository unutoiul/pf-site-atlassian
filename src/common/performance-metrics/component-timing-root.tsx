import * as React from 'react';

import { ComponentTimingRootContext, PerformanceProp, Reporter, TimingEvent } from './types';

interface OwnProps {
  reporter: Reporter;
  performance?: PerformanceProp;
}

const { Provider, Consumer } = React.createContext<ComponentTimingRootContext>(
  {
    onLoad: () => null,
    performance: window.performance,
  },
);

export const RootConsumer = Consumer;

export class ComponentTimingRoot extends React.Component<OwnProps> {
  private seenMeaningfulLoad = false;

  public render() {
    const { performance } = this.props;

    return (
      <Provider
        value={{
          onLoad: this.onLoad,
          performance: (performance || window.performance) }
        }
      >
        {this.props.children}
      </Provider>
    );
  }

  private onLoad = (timingEvent: TimingEvent) => {
    const isFirstMeaningful = !this.seenMeaningfulLoad && timingEvent.isMeaningful;
    if (timingEvent.isMeaningful) {
      this.seenMeaningfulLoad = true;
    }
    this.props.reporter.reportLoadEvent(timingEvent);
    if (isFirstMeaningful) {
      this.props.reporter.reportFirstMeaningfulData(timingEvent);
    }
  };

}
