import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import { SinonSpy, SinonStub, spy, stub } from 'sinon';

import { ComponentTimingRoot } from './component-timing-root';

let mockPerformance: {
  mark: SinonSpy,
  measure: SinonSpy,
  getEntriesByName: SinonStub,
  now: SinonStub,
};

let mockReporter: {
  reportFirstMeaningfulData: SinonSpy,
  reportLoadEvent: SinonSpy,
};

describe('Component timing root', () => {
  beforeEach(() => {
    mockPerformance = {
      mark: spy(),
      measure: spy(),
      getEntriesByName: stub().returns([{ duration: 1 }, { duration: 2 }]),
      now: stub().returns(1),
    };

    mockReporter = {
      reportLoadEvent: spy(),
      reportFirstMeaningfulData: spy(),
    };
  });

  it('should render children', () => {
    const wrapper = mount(
      <ComponentTimingRoot reporter={mockReporter} performance={mockPerformance}>
        <div id="hi" />
      </ComponentTimingRoot>,
    );

    expect(wrapper.find('div').length).to.equal(1);
  });

});
