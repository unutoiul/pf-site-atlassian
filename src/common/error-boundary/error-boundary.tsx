import * as React from 'react';

import { analyticsClient } from 'common/analytics';

interface ErrorBoundaryState {
  hasError: boolean;
}

interface OwnProps {
  children: JSX.Element;
}

export class ErrorBoundary extends React.Component<OwnProps, ErrorBoundaryState> {
  public readonly state: Readonly<ErrorBoundaryState> = {
    hasError: false,
  };

  public componentDidCatch(error) {
    this.setState({ hasError: true });
    analyticsClient.onError(error);
  }

  public render() {
    if (this.state.hasError) {
      // TODO: Decide on how to surface errors.
      return null;
    }

    return this.props.children;
  }
}
