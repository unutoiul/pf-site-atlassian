// tslint:disable:jsx-use-translation-function
import { storiesOf } from '@storybook/react';
import * as React from 'react';

import { ErrorBoundary } from './error-boundary';

const TestComponent = (props) => {
  if (props.error) {
    throw new Error('Unhandled error in rendering component');
  }

  return (
    <div>
      Successfully rendered the Component. Hurray!!!!!
    </div>
  );
};

storiesOf('Common|Error Boundary', module)
  .add('Without error', () => (
    <ErrorBoundary>
      <TestComponent error={false} />
    </ErrorBoundary>
  ))
  .add('With error', () => (
    <ErrorBoundary>
      <TestComponent error={true} />
    </ErrorBoundary>
  ));
