// tslint:disable:jsx-use-translation-function
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import styled from 'styled-components';

import { Checkbox as AkCheckbox } from '@atlaskit/checkbox';

import { FieldDescription } from './field-description';

const Wrapper = styled.div`
  margin: 25px;
`;

storiesOf('Common|Field Description', module)
  .add('Description with checkbox group', () => {
    return (
      <Wrapper>
        <FieldDescription>Application access</FieldDescription>
        <AkCheckbox
          value="Checkbox One"
          label="Checkbox One"
          name="checkbox-one"
        />
        <AkCheckbox
          label="Checkbox Two"
          value="Checkbox Two"
          name="checkbox-two"
        />
        <AkCheckbox
          label="Checkbox Three"
          value="Checkbox Three"
          name="checkbox-three"
        />
      </Wrapper>
    );
  });
