import styled from 'styled-components';

import {
  colors as akColors,
  fontSize as akFontSize,
  gridSize as akGridSize,
} from '@atlaskit/theme';

export const FieldDescription = styled.div`
  color: ${akColors.N300};
  font-size: ${akFontSize() - 3}px;
  margin-top: ${akGridSize() / 2}px;
`;
