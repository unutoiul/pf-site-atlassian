import * as React from 'react';
import styled from 'styled-components';

import { Grid as AkGrid, GridColumn as AkGridColumn } from '@atlaskit/page';
import { gridSize as akGridSize } from '@atlaskit/theme';

import { PageHeader as AkPageHeader } from 'common/responsive/PageHeader';

import { util } from '../../utilities/admin-hub';
import { addCollapsedNavProp } from './add-collapsed-nav-prop';

const Page = styled.div`
  padding-top: ${akGridSize}px;
  padding-bottom: ${akGridSize() * 6}px;
`;

const Header = styled.div`
  padding-bottom: ${akGridSize() * 5}px;
`;

export interface CenteredPageLayoutProps {
  breadcrumbs?: React.ReactNode;
}

export class CenteredPageLayoutImpl extends React.Component<CenteredPageLayoutProps> {
  public render() {
    const { children } = this.props;

    return (
      <Page>
        <Header>
          {this.getHeader()}
        </Header>
        <AkGrid>
          <AkGridColumn medium={1} />
          <AkGridColumn medium={10}>
            {children}
          </AkGridColumn>
          <AkGridColumn medium={1} />
        </AkGrid>
      </Page>
    );
  }

  private getHeader() {
    const { breadcrumbs } = this.props;

    if (!breadcrumbs) {
      return null;
    }

    return (
      <AkGrid spacing="comfortable">
        <AkGridColumn medium={8}>
          <AkPageHeader breadcrumbs={util.isAdminHub() ? breadcrumbs : null} />
        </AkGridColumn>
      </AkGrid>
    );
  }
}

export const CenteredPageLayout = addCollapsedNavProp(CenteredPageLayoutImpl);
