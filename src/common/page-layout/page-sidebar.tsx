import * as React from 'react';
import styled, { css } from 'styled-components';

import {
  borderRadius as akBorderRadius,
  gridSize as akGridSize,
} from '@atlaskit/theme';

import { depthZ100 } from 'common/styled';

type Placement = 'beforeText' | 'afterText';
interface ImageProps {
  placement?: Placement;
  source: string;
}

export const Image = styled.div`
  ${(props: ImageProps) => css`background: url(${props.source}) no-repeat 50%/contain;`}
  height: ${akGridSize() * 20}px;
  ${(props: ImageProps) => props.placement === 'afterText'
    ? css`margin-top: ${ akGridSize() * 5 }px;`
    : props.placement === 'beforeText' ?
    css`margin-bottom: ${ akGridSize() * 5 }px;`
    : ''
  }
`;

const PageSidebarBox = styled.div`
  border-radius: ${akBorderRadius}px;
  ${depthZ100}
  padding: ${akGridSize() * 3}px;
  margin-bottom: ${akGridSize() * 3}px;

  ul {
    padding-left: ${akGridSize() * 2.5}px;
  }

  li {
    margin-bottom: ${akGridSize}px;
  }
`;

export interface PageSidebarProps {
  imgAfterSrc?: string;
  imgBeforeSrc?: string;
}

export const PageSidebar = ({ children, imgAfterSrc, imgBeforeSrc }: React.Component<PageSidebarProps>['props']) => (
  <React.Fragment>
    {imgBeforeSrc && <Image source={imgBeforeSrc} placement="beforeText"/>}
    <PageSidebarBox>
      {children}
    </PageSidebarBox>
    {imgAfterSrc && <Image source={imgAfterSrc} placement="afterText"/>}
  </React.Fragment>
);
