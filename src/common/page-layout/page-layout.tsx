import * as React from 'react';
import styled, { css } from 'styled-components';

import AkAvatar from '@atlaskit/avatar';
import { Grid as AkGrid, GridColumn as AkGridColumn } from '@atlaskit/page';
import {
  colors as akColors,
  gridSize as akGridSize,
} from '@atlaskit/theme';

import { ErrorBoundary } from 'common/error-boundary';
import { PageHeader as AkPageHeader } from 'common/responsive/PageHeader';
import { Centered } from 'common/styled';

import { util } from '../../utilities/admin-hub';
import { addCollapsedNavProp } from './add-collapsed-nav-prop';

export interface PageLayoutProps {
  title?: React.ReactNode;
  isDisabled?: boolean;
  subtitle?: any;
  children?: any;
  description?: any;
  side?: any;
  icon?: any;
  isFullWidth?: boolean;
  action?: JSX.Element;
  breadcrumbs?: React.ReactNode;
  pageParent?: React.ReactNode;
}

const Page = styled.div`
  padding-top: ${akGridSize() * 2}px;
  padding-bottom: ${akGridSize() * 4}px;
`;

export const Title = styled.div`
  ${
  ({ isDisabled }: { isDisabled: boolean }) => isDisabled
    ? css`color: ${akColors.N70};`
    : ''
  }
`;

export const Subtitle = styled.span`
  font-style: italic;
  font-size: 12px;
  ${
  ({ isDisabled }: { isDisabled: boolean }) => isDisabled
    ? css`color: ${akColors.N70};`
    : ''
  }
`;

export const Icon = styled.div`
  padding-right: ${akGridSize() * 2}px;
  float: left;
  ${
  ({ isDisabled }: { isDisabled: boolean }) => isDisabled
    ? css`filter: grayscale(100%);`
    : ''
  }
`;

export const Children = styled.div`
  padding-top: ${akGridSize() * 3}px;
`;

export class PageLayoutImpl extends React.Component<PageLayoutProps> {
  public render() {
    const {
      icon,
      title,
      isDisabled = false,
      subtitle,
      action,
      description,
      children,
      side,
      breadcrumbs,
      pageParent,
      isFullWidth,
    } = this.props;

    return (
      <ErrorBoundary>
        <Page>
          <AkGrid spacing="comfortable">
            {!side && !isFullWidth && <AkGridColumn medium="2" />}
            <AkGridColumn medium={this.determineChildGridSize()}>
              {pageParent}
              <AkPageHeader
                bottomBar={subtitle && <Subtitle isDisabled={isDisabled}>{subtitle}</Subtitle>}
                actions={action}
                breadcrumbs={util.isAdminHub() ? breadcrumbs : null}
              >
                <Centered axis="vertical">
                  {icon && <Icon isDisabled={isDisabled}><AkAvatar src={icon} size="large" /></Icon>}
                  {title && <Title isDisabled={isDisabled}>{title}</Title>}
                </Centered>
              </AkPageHeader>
              {description && <div>{description}</div>}
              {children && <Children>{children}</Children>}
            </AkGridColumn>
            {side && (
              <AkGridColumn medium={4}>
                <Children>{side}</Children>
              </AkGridColumn>
            )}
          </AkGrid>
        </Page>
      </ErrorBoundary>
    );
  }

  private determineChildGridSize = (): number => {
    if (this.props.side || !this.props.isFullWidth) {
      return akGridSize();
    }

    return akGridSize() * 1.5;
  }
}

export const PageLayout = addCollapsedNavProp(PageLayoutImpl);
