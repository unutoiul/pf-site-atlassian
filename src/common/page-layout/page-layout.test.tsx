// tslint:disable jsx-use-translation-function
import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import AkAvatar from '@atlaskit/avatar';
import AkButton from '@atlaskit/button';
import { GridColumn as AkGridColumn } from '@atlaskit/page';

import { PageHeader as AkPageHeader } from 'common/responsive/PageHeader';

import { Icon, PageLayoutImpl, Subtitle, Title } from './page-layout';

describe('PageLayout', () => {

  it('renders children', () => {
    const page = shallow(
      <PageLayoutImpl title="Hello world!">
        <div className="child" />
      </PageLayoutImpl>,
    );
    expect(page.find('.child').length).to.equal(1);
  });

  it('renders the main content full width when isFullWidth is provided', () => {
    const page = shallow(
      <PageLayoutImpl isFullWidth={true} />,
    );
    const col = page.find(AkGridColumn) as AkGridColumn;
    expect(col.first().props().medium).to.equal(12);
  });

  it('doesn\'t render the title if not provided', () => {
    const page = shallow(
      <PageLayoutImpl>
        <div className="spinner" />
      </PageLayoutImpl>,
    );
    expect(page.find('h1').length).to.equal(0);
  });

  it('renders the description', () => {
    const page = shallow(
      <PageLayoutImpl title="Hello world!" description={<p>A description</p>} />,
    );
    expect(page.contains(<p>A description</p>)).to.equal(true);
  });

  it('renders the side', () => {
    const page = shallow(
      <PageLayoutImpl title="Hello world!" side={<p>A side</p>} />,
    );
    expect(page.contains(<p>A side</p>)).to.equal(true);
  });

  it('doesn\'t render the side container if not provided', () => {
    const page = shallow(
      <PageLayoutImpl title="content" />,
    );
    const columns = page.find(AkGridColumn);
    const header = columns.last().find(AkPageHeader);
    expect(columns.length).to.equal(2);
    expect(header.exists()).to.equal(true);
  });

  it('renders the action if provided', () => {
    const button = (
      <AkButton isDisabled={false} appearance="primary">
        Manage
      </AkButton>
    );

    const page = shallow(<PageLayoutImpl action={button} />);
    const header = page.find(AkPageHeader);

    expect(header.props().actions).to.equal(button);
  });

  describe('on enabled state or if the isDisabled prop is not provided', () => {
    it('renders the title', () => {
      const page = shallow(
        <PageLayoutImpl title="Hello world!" />,
      );

      expect(page.contains(<Title isDisabled={false}>Hello world!</Title>)).to.equal(true);
    });

    it('renders the subtitle', () => {
      const subtitleText = 'this is a subtitle';

      const page = shallow(
        <PageLayoutImpl title="Hello world!" subtitle={subtitleText} />,
      );

      const header = page.find(AkPageHeader);
      expect(header.props().bottomBar).to.deep.equal(<Subtitle isDisabled={false}>{subtitleText}</Subtitle>);
    });

    it('renders the icon', () => {
      const page = shallow(
        <PageLayoutImpl title="Hello world!" icon="icon.png" />,
      );

      const icon = page.find<{ isDisabled: boolean }>(Icon);
      expect(icon).to.have.length(1);
      expect(icon.props().isDisabled).to.equal(false);
      const avatar = page.find(AkAvatar) as AkAvatar;
      expect(avatar.props().src).to.deep.equal('icon.png');
    });
  });

  describe('on disabled state', () => {
    it('renders the title', () => {
      const page = shallow(
        <PageLayoutImpl title="Hello world!" isDisabled={true} />,
      );
      expect(page.contains(<Title isDisabled={true}>Hello world!</Title>)).to.equal(true);
    });

    it('renders the subtitle', () => {
      const subtitleText = 'this is a subtitle';

      const page = shallow(
        <PageLayoutImpl title="Hello world!" subtitle={subtitleText} isDisabled={true} />,
      );

      const header = page.find(AkPageHeader);
      expect(header.props().bottomBar).to.deep.equal(<Subtitle isDisabled={true}>{subtitleText}</Subtitle>);
    });

    it('renders the icon', () => {
      const page = shallow(
        <PageLayoutImpl title="Hello world!" icon="icon.png" isDisabled={true} />,
      );

      const icon = page.find<{ isDisabled: boolean }>(Icon);
      expect(icon).to.have.length(1);
      expect(icon.props().isDisabled).to.equal(true);
      const avatar = page.find(AkAvatar) as AkAvatar;
      expect(avatar.props().src).to.deep.equal('icon.png');
    });
  });

  describe('does not render components if the prop is not provided', () => {
    it('does not render the icon', () => {
      const page = shallow(
        <PageLayoutImpl title="Hello world!" />,
      );
      expect(page.find(Icon)).to.have.length(0);
    });

    it('does not render the subtitle', () => {
      const page = shallow(
        <PageLayoutImpl title="Hello world!" />,
      );
      expect(page.find(Subtitle)).to.have.length(0);
    });
  });

});
