import * as React from 'react';
import { graphql, OptionProps } from 'react-apollo';

import { analyticsClient } from 'common/analytics';

import { SetContainerNavVisibilityMutation, SetContainerNavVisibilityMutationVariables } from '../../schema/schema-types';
import toggleNavMutation from './add-collapsed-nav-prop.mutation.graphql';

export interface AddedProps {
  collapsedNav?: boolean;
}

export function addCollapsedNavProp<TOwnProps>(Component: React.ComponentType<TOwnProps>): React.ComponentClass<TOwnProps & AddedProps> {
  interface GraphqlDerivedProps {
    setContainerNavVisibility(show: boolean): void;
  }

  const withContainerNavVisibilitySetter = graphql<TOwnProps & AddedProps, SetContainerNavVisibilityMutation, SetContainerNavVisibilityMutationVariables, GraphqlDerivedProps>(
    toggleNavMutation,
    {
      props: ({ mutate }: OptionProps<TOwnProps, SetContainerNavVisibilityMutation>): GraphqlDerivedProps => ({
        setContainerNavVisibility: (show: boolean) =>
          mutate instanceof Function && mutate({
            variables: { show } as SetContainerNavVisibilityMutationVariables,
          }).catch((error) => {
            analyticsClient.onError(error);

            return;
          }),
      }),
    },
  );

  const isVisible = (props: NavVisibilityControllerImpl['props']): boolean => {
    const {
      collapsedNav,
    } = props;

    return !collapsedNav;
  };

  interface State {
    shouldShowNav?: boolean;
  }

  class NavVisibilityControllerImpl extends React.Component<TOwnProps & AddedProps & GraphqlDerivedProps, State> {
    public readonly state: Readonly<State> = {};

    public static getDerivedStateFromProps(nextProps: NavVisibilityControllerImpl['props'], prevState: State) {
      const newShouldShow = isVisible(nextProps);
      if (prevState.shouldShowNav === newShouldShow) {
        return null;
      }

      return {
        shouldShowNav: newShouldShow,
      };
    }

    public componentDidMount() {
      const { shouldShowNav } = this.state;

      if (typeof shouldShowNav !== 'undefined') {
        this.props.setContainerNavVisibility(shouldShowNav);
      }
    }

    public componentDidUpdate(_, nextState: State) {
      if (this.state.shouldShowNav === nextState.shouldShowNav || typeof nextState.shouldShowNav === 'undefined') {
        return;
      }

      this.props.setContainerNavVisibility(nextState.shouldShowNav);
    }

    public render() {
      const {
        collapsedNav, // tslint:disable-line:no-unused - we are stripping this one to not provide it to the `Component />`
        toggleNavigation, // tslint:disable-line:no-unused - we are stripping this one to not provide it to the `Component />`
        ...rest
      } = this.props as any; // we put `any` here to avoid TS error about destructuring a generic type

      return <Component {...rest} />;
    }
  }

  return withContainerNavVisibilitySetter(NavVisibilityControllerImpl);
}
