// tslint:disable:jsx-use-translation-function
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import styled from 'styled-components';

import AkButton from '@atlaskit/button';
// tslint:disable-next-line:no-import-side-effect
import '@atlaskit/css-reset';
import AkDropdownMenu, { DropdownItem as AkDropdownItem } from '@atlaskit/dropdown-menu';
import AkPage from '@atlaskit/page';
import { akGridSizeUnitless } from '@atlaskit/util-shared-styles';

import { catchInNetImage } from 'common/images';

import { ButtonGroup } from '../button-group';
import { CenteredPageLayoutImpl } from './centered-page-layout';
import { PageLayoutImpl } from './page-layout';

const ActionButton = styled(ButtonGroup) `
  margin-left: auto;
`;

const SideImage = styled.div`
  background: url(${catchInNetImage}) no-repeat 50%/contain;
  height: 150px;
  margin-top: ${akGridSizeUnitless * 5}px;
`;

const defaults = {
  title: 'Managed Accounts',
  isDisabled: false,
  subtitle: 'This is an example subtitle...',
  description: `Pop-up whatever banjo mixtape, listicle authentic heirloom ennui
    waistcoat typewriter PBR&B tousled yr farm-to-table. DIY affogato glossier vape adaptogen.
  `,
  icon: 'https://pbs.twimg.com/profile_images/848471660860538880/pevXVsIp.jpg',
  actionButton: (
    <ActionButton>
      <AkButton isDisabled={false} appearance="primary">
        Manage
      </AkButton>
    </ActionButton>
  ),
  actionDropdown: (
    <AkDropdownMenu
      triggerType="button"
      trigger="Dropdown"
      position="bottom right"
      triggerButtonProps={{ isDisabled: false }}
    >
      <AkDropdownItem>Item</AkDropdownItem>
    </AkDropdownMenu>
  ),
  children: (
    <section>
      <p>Celiac cliche distillery seitan tattooed keytar church-key.</p>
      <p>Knausgaard subway tile four dollar toast.</p>
    </section>
  ),
  side: (<SideImage />),
};

storiesOf('Common|Page Layout', module)
  .add('With a description', () => {
    return (
      <AkPage>
        <PageLayoutImpl
          title={defaults.title}
          subtitle={defaults.subtitle}
          icon={defaults.icon}
          isDisabled={false}
          description={defaults.description}
        />
      </AkPage>
    );
  })
  .add('With children', () => {
    return (
      <AkPage>
        <PageLayoutImpl
          title={defaults.title}
          subtitle={defaults.subtitle}
          icon={defaults.icon}
          isDisabled={false}
          description={defaults.description}
          children={defaults.children}
        />
      </AkPage>
    );
  })
  .add('With a side', () => {
    return (
      <AkPage>
        <PageLayoutImpl
          title={defaults.title}
          subtitle={defaults.subtitle}
          icon={defaults.icon}
          isDisabled={false}
          description={defaults.description}
          children={defaults.children}
          side={defaults.side}
        />
      </AkPage>
    );
  });

storiesOf('Common|Page Layout/Header', module)
  .add('With a title', () => {
    return (
      <AkPage>
        <PageLayoutImpl
          title={defaults.title}
          isDisabled={false}
        />
      </AkPage>
    );
  })
  .add('With an icon', () => {
    return (
      <AkPage>
        <PageLayoutImpl
          title={defaults.title}
          isDisabled={false}
          icon={defaults.icon}
        />
      </AkPage>
    );
  })
  .add('With a subtitle', () => {
    return (
      <AkPage>
        <PageLayoutImpl
          title={defaults.title}
          isDisabled={false}
          icon={defaults.icon}
          subtitle={defaults.subtitle}
        />
      </AkPage>
    );
  })
  .add('With an action button', () => {
    return (
      <AkPage>
        <PageLayoutImpl
          title={defaults.title}
          isDisabled={false}
          icon={defaults.icon}
          subtitle={defaults.subtitle}
          action={defaults.actionButton}
        />
      </AkPage>
    );
  })
  .add('With an action dropdown', () => {
    return (
      <AkPage>
        <PageLayoutImpl
          title={defaults.title}
          isDisabled={false}
          icon={defaults.icon}
          subtitle={defaults.subtitle}
          action={defaults.actionDropdown}
        />
      </AkPage>
    );
  })
  .add('With all and disabled', () => {
    return (
      <AkPage>
        <PageLayoutImpl
          title={defaults.title}
          isDisabled={true}
          icon={defaults.icon}
          subtitle={defaults.subtitle}
          action={defaults.actionButton}
        />
      </AkPage>
    );
  });

storiesOf('Common|Page Layout/Centered', module)
  .add('Default', () => {
    return (
      <AkPage>
        <CenteredPageLayoutImpl>
          <h1>Australian coffee culture, a brief history</h1>
          <p>With all the British settlers in the early days tea was the number one brew in Australia. Sydney only had filter coffee until the 1930’s but this all changed when espresso arrived Down Under. The first commercial espresso machine was installed in Café Florentino, Bourke Street Melbourne in 1928. Not long after, coffee machines made their way over to Sydney and the coffee movement slowly started to infiltrate society.</p>
          <p>After WWII, two things happened that gave the coffee culture even more of a boost. Firstly, the Australian government lifted controls on the import of coffee. Secondly, the Australian government launched a new immigration program, which meant that for the first time Australia would accept non-British European immigrants. Following this initiative, hundreds of thousands of Europeans arrived, including a large group of Italians and Greeks who loved their espresso coffee so much they shared this experience with friends in their new-found home. Italian-style coffee lounges soon appeared everywhere in Sydney’s suburbs and the rest is history.</p>
        </CenteredPageLayoutImpl>
      </AkPage>
    );
  });
