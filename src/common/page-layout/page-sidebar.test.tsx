import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import { catchInNetImage } from 'common/images';

import { Image, PageSidebar } from './page-sidebar';

describe('PageSidebar', () => {

  it('renders children', () => {
    const page = shallow(
      <PageSidebar>
        <div className="child" />
      </PageSidebar>,
    );
    expect(page.find('.child').length).to.equal(1);
  });

  it('renders an image before text if given', () => {
    const page = shallow(
      <PageSidebar imgBeforeSrc={catchInNetImage}>
        <div className="child" />
      </PageSidebar>,
    );

    expect(page.find(Image).length).to.equal(1);
  });

  it('renders an image after text if given', () => {
    const page = shallow(
      <PageSidebar imgAfterSrc={catchInNetImage} >
        <div className="child" />
      </PageSidebar > ,
    );
    expect(page.find(Image).length).to.equal(1);
  });

  it('renders two images if given', () => {
    const page = shallow(
      <PageSidebar imgAfterSrc={catchInNetImage} imgBeforeSrc={catchInNetImage}>
        <div className="child" />
      </PageSidebar>,
    );
    expect(page.find(Image).length).to.equal(2);
  });

});
