// tslint:disable jsx-use-translation-function
import { storiesOf } from '@storybook/react';
import * as React from 'react';

import { DropdownItem as AkDropdownItem, DropdownItemGroup as AkDropdownItemGroup } from '@atlaskit/dropdown-menu';

import { SingleSelectDropdownMenu } from './single-select-dropdown-menu';

storiesOf('Common|Single Select Dropdown Menu', module)
  .add('With dividers and no title', () => {
    return (
      <SingleSelectDropdownMenu
        trigger={'Choices'}
        triggerType="button"
        withDivider={true}
      >
        <AkDropdownItemGroup>
          <AkDropdownItem>
            {'Option 1'}
          </AkDropdownItem>
          <AkDropdownItem>
            {'Option 2'}
          </AkDropdownItem>
        </AkDropdownItemGroup>
        <AkDropdownItemGroup>
          <AkDropdownItem>
            {'Option 3'}
          </AkDropdownItem>
          <AkDropdownItem>
            {'Option 4'}
          </AkDropdownItem>
        </AkDropdownItemGroup>
        <AkDropdownItemGroup>
          <AkDropdownItem>
            {'Option 5'}
          </AkDropdownItem>
          <AkDropdownItem>
            {'Option 6'}
          </AkDropdownItem>
          <AkDropdownItem>
            {'Option 7'}
          </AkDropdownItem>
        </AkDropdownItemGroup>
      </SingleSelectDropdownMenu>
    );
  }).add('With dividers and title', () => {
    return (
      <SingleSelectDropdownMenu
        trigger={'Choices choices Choices choices'}
        triggerType="button"
        withDivider={true}
        maxWidth={100}
      >
        <AkDropdownItemGroup title="Title 1">
          <AkDropdownItem>
            {'Option 1'}
          </AkDropdownItem>
          <AkDropdownItem>
            {'Option 2'}
          </AkDropdownItem>
        </AkDropdownItemGroup>
        <AkDropdownItemGroup title="Title 2">
          <AkDropdownItem>
            {'Option 3'}
          </AkDropdownItem>
          <AkDropdownItem>
            {'Option 4'}
          </AkDropdownItem>
        </AkDropdownItemGroup>
        <AkDropdownItemGroup title="Title 3">
          <AkDropdownItem>
            {'Option 5'}
          </AkDropdownItem>
          <AkDropdownItem>
            {'Option 6'}
          </AkDropdownItem>
          <AkDropdownItem>
            {'Option 7'}
          </AkDropdownItem>
        </AkDropdownItemGroup>
      </SingleSelectDropdownMenu>
    );
  });
