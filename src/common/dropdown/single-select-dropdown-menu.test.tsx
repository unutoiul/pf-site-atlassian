// tslint:disable:jsx-use-translation-function
import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import AkDropdown, { DropdownItem as AkDropdownItem, DropdownItemGroup as AkDropdownItemGroup } from '@atlaskit/dropdown-menu';

import { DropdownDivider, DropdownMenuProps, SingleSelectDropdownMenu } from './single-select-dropdown-menu';

describe('Single Select Dropdown Menu', () => {
  const defaultProps = {
    withDivider: false,
    maxWidth: undefined,
  };

  const createShallowWrapper = (props: DropdownMenuProps = defaultProps) => {
    return shallow(
      <SingleSelectDropdownMenu
        children={getTestChildren()}
        {...props}
        withDivider={props.withDivider}
        maxWidth={props.maxWidth}
      />,
    );
  };

  const getTestChildren = (): React.ReactNode => {

    return (
      <React.Fragment>
        <AkDropdownItemGroup>
          <AkDropdownItem>
            {'Option 1'}
          </AkDropdownItem>
          <AkDropdownItem>
            {'Option 2'}
          </AkDropdownItem>
        </AkDropdownItemGroup>
        <AkDropdownItemGroup>
          <AkDropdownItem>
            {'Option 3'}
          </AkDropdownItem>
          <AkDropdownItem>
            {'Option 4'}
          </AkDropdownItem>
        </AkDropdownItemGroup>
        <AkDropdownItemGroup>
          <AkDropdownItem>
            {'Option 5'}
          </AkDropdownItem>
          <AkDropdownItem>
            {'Option 6'}
          </AkDropdownItem>
          <AkDropdownItem>
            {'Option 7'}
          </AkDropdownItem>
        </AkDropdownItemGroup>
      </React.Fragment>
    );
  };

  describe('with default dropdown props', () => {
    it('should render the correct components', () => {
      const wrapper = createShallowWrapper();

      expect(wrapper.find(AkDropdown).length).to.equal(1);
      expect(wrapper.find(AkDropdownItemGroup).length).to.equal(3);
    });

    it('should correctly pass props to AkDropdown', () => {
      const props = {
        isOpen: true,
        appearance: 'tall' as 'tall' | 'default',
        trigger: 'Test Dropdown',
      };

      const wrapper = createShallowWrapper(props);

      const dropdown = wrapper.find(AkDropdown) as any;

      expect(dropdown.length).to.equal(1);
      expect(dropdown.props().isOpen).to.equal(true);
      expect(dropdown.props().appearance).to.equal('tall');
      expect(dropdown.props().trigger).to.equal('Test Dropdown');
    });

    it('should not render a divider when withDivider is false', () => {
      const props = {
        isOpen: true,
        trigger: 'Test Dropdown',
        withDivider: false,
      };

      const wrapper = createShallowWrapper(props);

      const divider = wrapper.find(DropdownDivider);

      expect(divider.length).to.equal(0);
    });

    it('should render a divider when withDivider is true', () => {
      const props = {
        isOpen: true,
        trigger: 'Test Dropdown',
        withDivider: true,
      };

      const wrapper = createShallowWrapper(props);

      const divider = wrapper.find(DropdownDivider);

      expect(divider.length).to.equal(1);
    });

    it('should correctly utilize maxWidth', () => {
      const props = {
        isOpen: true,
        trigger: 'Test Dropdown',
        maxWidth: 100,
      };

      const wrapper = createShallowWrapper(props);

      const dropdown = wrapper.filter(AkDropdown);
      const trigger = dropdown.props().trigger as any;

      expect(trigger.props.maxWidth).to.equal(100);
    });
  });
});
