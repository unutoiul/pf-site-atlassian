import * as React from 'react';
import styled from 'styled-components';

import AkDropdown, { AkDropdownProps } from '@atlaskit/dropdown-menu';

import { colors as akColors, gridSize as akGridSize } from '@atlaskit/theme';

import { TruncatedField } from 'common/truncated-field';

export type DropdownMenuProps = CustomDropdownProps & AkDropdownProps;

interface CustomDropdownProps {
  withDivider?: boolean;
  maxWidth?: number;
}

export const DropdownDivider = styled.div`
  > * {
    padding: ${akGridSize() * 0.5}px 0 ${akGridSize() * 0.5}px 0;
    border-bottom: ${akGridSize() * 0.25}px solid ${akColors.N40};
    box-sizing: content-box;

    &:first-child {
      padding-top: 0;
    }

    &:last-child {
      border-bottom: none;
      padding-bottom: 0;
    }
  }
`;

export const WidthConstrainer = styled.div`
  display: flex;
  text-align: left;
  align-content: space-between;
  ${({ maxWidth }: { maxWidth: number | undefined }) => maxWidth ? `width: ${maxWidth}px;` : ''}

  button {
    vertical-align: top;
  }
`;

export class SingleSelectDropdownMenu extends React.Component<DropdownMenuProps> {
  public render() {
    const { withDivider } = this.props;

    if (withDivider) {
      return (
        <AkDropdown {...this.props} trigger={this.getTriggerTitle()} >
          <DropdownDivider>
            {this.props.children}
          </DropdownDivider>
        </AkDropdown>
      );
    }

    return (
      <AkDropdown {...this.props} trigger={this.getTriggerTitle()}/>
    );
  }

  private getTriggerTitle = () => {
    const { maxWidth, trigger } = this.props;

    if (maxWidth) {
      return (
        <WidthConstrainer maxWidth={maxWidth}>
          <TruncatedField maxWidth={`${maxWidth}px`}>
            {trigger}
          </TruncatedField>
        </WidthConstrainer>
      );
    }

    return trigger;
  }
}
