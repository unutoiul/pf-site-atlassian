import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import AkFieldBase from '@atlaskit/field-base';

import { createMockIntlContext } from '../../utilities/testing';
import { SearchInput } from './search-input';

const findInput = (wrapper) => wrapper
  .find(AkFieldBase)
  .findWhere(component => component.name() === 'input');

describe('SearchInput', () => {
  it('should render with an initial value', () => {
    const wrapper = mount((
      <SearchInput
        initialValue="Jon Hopkins"
      />
    ), createMockIntlContext());

    expect(findInput(wrapper).prop('value')).to.equal('Jon Hopkins');
  });

  it('should render a search box which takes a callback', () => {
    const onInputSpy = sinon.spy();
    const wrapper = mount((
      <SearchInput onChange={onInputSpy} />
    ), createMockIntlContext());

    const searchInput = findInput(wrapper);
    searchInput.simulate('focus');
    searchInput.simulate('change', {
      target: {
        value: 'test',
      },
    });

    expect(onInputSpy.called).to.equal(true);
  });
});
