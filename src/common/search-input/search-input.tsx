import * as React from 'react';
import styled from 'styled-components';

import {
  defineMessages,
  InjectedIntlProps,
  injectIntl,
} from 'react-intl';

import { gridSize as akGridSize } from '@atlaskit/theme';

import AkFieldBase from '@atlaskit/field-base';
import AkSearchIcon from '@atlaskit/icon/glyph/search';

const messages = defineMessages({
  label: {
    id: 'search.input.label',
    defaultMessage: 'Search',
  },
});

const SearchStyle = styled.div`
  box-sizing: border-box;
  padding: ${akGridSize}px 0;

  div {
    display: flex;
    justify-content: flex-end;
  }

  .search-icon {
    opacity: 0.5;
  }

  .input {
    background: transparent;
    border: 0;
    font-size: 14px;
    outline: none;

    &:invalid {
      box-shadow: none;
    }

    &::-ms-clear {
      display: none;
    }
  }
`;

export interface SearchInputProps {
  children?: never;
  size?: number;
  initialValue?: string;
  onChange?(event: React.FormEvent<HTMLInputElement>): void;
  onClick?(event: React.MouseEvent<HTMLInputElement>): void;
}

interface SearchInputState {
  value: string;
}

export class SearchInputImpl extends React.Component<SearchInputProps & InjectedIntlProps, SearchInputState> {
  public state: SearchInputState = {
    value: this.props.initialValue || '',
  };

  public render() {
    const { intl: { formatMessage } } = this.props;

    return (
      <SearchStyle>
        <AkFieldBase
          appearance="standard"
          label={formatMessage(messages.label)}
          isCompact={true}
          isLabelHidden={true}
          isFitContainerWidthEnabled={true}
        >
          <input
            className="input"
            value={this.state.value}
            onChange={this.handleChange}
            onClick={this.props.onClick}
            size={this.props.size || undefined}
          />
          <AkSearchIcon label={formatMessage(messages.label)} />
        </AkFieldBase>
      </SearchStyle>
    );
  }

  private handleChange = (event: React.FormEvent<HTMLInputElement>) => {
    this.setState({ value: event.currentTarget.value });
    if (this.props.onChange) {
      this.props.onChange(event);
    }
  };
}

export const SearchInput = injectIntl<SearchInputProps>(SearchInputImpl);
