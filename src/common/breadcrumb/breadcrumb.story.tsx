import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { MemoryRouter } from 'react-router-dom';

import { BreadcrumbsStateless as AkBreadcrumbsStateless } from '@atlaskit/breadcrumbs';

import { AnalyticsData } from 'common/analytics';

import { Breadcrumb } from './breadcrumb';

const mockAnalyticsData: AnalyticsData = {
  actionSubject: 'page',
  action: 'dummy',
  actionSubjectId: 'dummy',
  subproduct: 'organization',
};

storiesOf('Common|Breadcrumb', module)
  .add('Single item', () => {
    return (
      <MemoryRouter>
        <Breadcrumb
          href="/"
          text="Admin"
          hasSeparator={false}
          analyticsData={mockAnalyticsData}
        />
      </MemoryRouter>
    );
  })
  .add('Multiple items with separator', () => {
    return (
      <MemoryRouter>
        <AkBreadcrumbsStateless>
          <Breadcrumb
            href="/"
            text="Admin"
            hasSeparator={true}
            analyticsData={mockAnalyticsData}
          />
          <Breadcrumb
            href="/"
            text="Groups"
            hasSeparator={false}
            analyticsData={mockAnalyticsData}
          />
        </AkBreadcrumbsStateless>
      </MemoryRouter>
    );
  })
  .add('Multiple items with long text', () => {
    return (
      <MemoryRouter>
        <AkBreadcrumbsStateless>
          <Breadcrumb
            href="/"
            text="Admin"
            hasSeparator={true}
            analyticsData={mockAnalyticsData}
          />
          <Breadcrumb
            href="/"
            text="The Awesome Aussie Company Est. 1923"
            hasSeparator={false}
            analyticsData={mockAnalyticsData}
          />
        </AkBreadcrumbsStateless>
      </MemoryRouter>
    );
  });
