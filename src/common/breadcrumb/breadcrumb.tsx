import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';

import { AnalyticsData, BreadcrumbsItem } from 'common/analytics';
import { RouterLink } from 'common/navigation';

export interface BreadcrumbProps {
  href: string;
  text: React.ReactNode;
  hasSeparator?: boolean;
  analyticsData?: AnalyticsData;
}

export class BreadcrumbImpl extends React.Component<BreadcrumbProps & RouteComponentProps<any>> {
  public render() {
    return (
      <BreadcrumbsItem
        component={RouterLink}
        truncationWidth={200}
        {...this.props}
      />
    );
  }
}

export const Breadcrumb = withRouter<BreadcrumbProps>(BreadcrumbImpl);
