import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import { AnalyticsData, BreadcrumbsItem } from 'common/analytics';

import { BreadcrumbImpl } from './breadcrumb';

describe('Breadcrumb', () => {
  it('should render an BreadcrumbsItem with valid props', () => {
    const analyticsData: AnalyticsData = {
      action: 'click',
      actionSubject: 'orgBreadcrumb',
      actionSubjectId: 'org',
      subproduct: 'organization',
    };

    const wrapper = shallow((
      <BreadcrumbImpl
        href="/some-url"
        text="Admin"
        hasSeparator={true}
        match={null as any}
        history={null as any}
        location={null as any}
        analyticsData={analyticsData}
      />
    ));

    const theBreadcrumb = wrapper.find(BreadcrumbsItem);

    expect(theBreadcrumb).to.have.lengthOf(1);
    expect(theBreadcrumb.props()).to.include({
      href: '/some-url',
      text: 'Admin',
      hasSeparator: true,
      analyticsData,
    });
    expect(theBreadcrumb.prop('component')).not.to.equal(null);
  });
});
