import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { spy } from 'sinon';

import AkButton from '@atlaskit/button';

import { createMockIntlContext } from '../../utilities/testing';
import { IdentityManagerLearnMoreButtonImpl } from './identity-manager-learn-more-button';

describe('IdentityManagerLearnMoreButton', () => {
  it('should trigger the identity manager drawer mutation', () => {
    const showIdentityManagerDrawer = spy();

    const wrapper = shallow(
      <IdentityManagerLearnMoreButtonImpl
        analyticsUIEvent={{} as any}
        analyticsClient={{ sendUIEvent: () => undefined } as any}
        isPrimary={true}
        showDrawer={showIdentityManagerDrawer}
      />,
      createMockIntlContext(),
    );
    const button = wrapper.find(AkButton);
    expect(button.exists()).to.equal(true);
    expect(showIdentityManagerDrawer.callCount).to.equal(0);
    button.simulate('click');

    expect(showIdentityManagerDrawer.callCount).to.equal(1);
    expect(showIdentityManagerDrawer.getCalls()[0].args[0]).to.equal('IDM');
  });
});
