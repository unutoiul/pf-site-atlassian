import * as React from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';
import styled from 'styled-components';

import AkButton from '@atlaskit/button';
import { akGridSizeUnitless } from '@atlaskit/util-shared-styles';

import { AnalyticsClientProps, UIEvent, withAnalyticsClient } from 'common/analytics';
import { ShowDrawerProps, withShowDrawer } from 'common/drawer';

const AnalyticsButtonContainer = styled.div`
  padding-top: ${akGridSizeUnitless * 2}px;
`;

const messages = defineMessages({
  learnMoreIMButton: {
    id: 'common.learn.more.im.button',
    defaultMessage: 'Learn more',
    description: 'The text for Learn More',
  },
});

interface OwnProps {
  analyticsUIEvent: UIEvent;
  isPrimary: boolean;
  isLink?: boolean;
}

type AllProps = OwnProps & ShowDrawerProps & AnalyticsClientProps;

export class IdentityManagerLearnMoreButtonImpl extends React.Component<AllProps> {
  public render() {
    const {
      isPrimary,
      isLink,
    } = this.props;
    const button = (
      <AkButton
        onClick={this.onClick}
        appearance={isPrimary ? 'primary' : isLink ? 'link' : 'default'}
        spacing={isLink ? 'none' : 'default'}
      >
        <FormattedMessage {...messages.learnMoreIMButton} />
      </AkButton>);

    if (isLink) {
      return button;
    }

    return (
      <AnalyticsButtonContainer>
        {button}
      </AnalyticsButtonContainer>
    );
  }

  private onClick = () => {
    const { analyticsUIEvent, analyticsClient, showDrawer } = this.props;

    analyticsClient.sendUIEvent(analyticsUIEvent);
    showDrawer('IDM');
  }
}

export const IdentityManagerLearnMoreButton =
  withShowDrawer(
    withAnalyticsClient(
      IdentityManagerLearnMoreButtonImpl,
    ),
  )
;
