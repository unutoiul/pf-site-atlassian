import * as React from 'react';

import { analyticsClient, AnalyticsData } from 'common/analytics';

// Active user criteria (user has been on page for at least 10 seconds)
const ACTIVE_USER_THRESHOLD = 10 * 1000;

const onEyeballEvent = () => {
  analyticsClient.onEvent({
    ...baseEyeballEventPayload,
  });
};

const baseEyeballEventPayload: AnalyticsData = {
  subproduct: 'chrome',
  actionSubject: 'page',
  action: 'eyeball',
  actionSubjectId: 'eyeball',
};

const waitForActiveUserCriteria = (eyeballEvent) => {
  setTimeout(eyeballEvent, ACTIVE_USER_THRESHOLD);
};

interface EyeballAnalyticsProps {
  onEyeballEvent?(): void;
  waitForActiveUserCriteria?(onEyeballEvent): void;
}

export class EyeballAnalytics extends React.Component<EyeballAnalyticsProps> {

  public static defaultProps = {
    onEyeballEvent,
    waitForActiveUserCriteria,
  };

  public eyeballEventEmitted: boolean = false;

  public emitEyeballEvent() {
    if (!this.props.waitForActiveUserCriteria) {
      return;
    }

    this.props.waitForActiveUserCriteria(
      this.props.onEyeballEvent,
    );
  }

  public render() {
    if (!this.hasEyeballEventBeenEmitted()) {
      this.emitEyeballEvent();
      this.eyeballEventEmitted = true;
    }

    return null;
  }

  private hasEyeballEventBeenEmitted() {
    return this.eyeballEventEmitted === true;
  }
}
