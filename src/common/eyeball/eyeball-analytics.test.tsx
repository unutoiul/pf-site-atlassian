import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { sandbox as sinonSandbox, SinonSandbox, spy, stub } from 'sinon';

import { analyticsClient } from 'common/analytics';

import { EyeballAnalytics } from './eyeball-analytics';

describe('EyeballAnalytics', () => {
  let sandbox: SinonSandbox;

  beforeEach(() => {
    sandbox = sinonSandbox.create();
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('should emit eyeball event if active user criteria is met', () => {
    const waitForActiveUserCriteriaStub = (onEyeballEvent) => {
      onEyeballEvent();
    };
    const analyticsSpy = sandbox.spy(analyticsClient, 'onEvent');

    shallow(
      <EyeballAnalytics
        waitForActiveUserCriteria={waitForActiveUserCriteriaStub}
      />,
    );

    expect(analyticsSpy.callCount).to.equal(1);
    expect(analyticsSpy.getCalls()[0].args[0].action).to.equal('eyeball');
  });

  it('should not emit eyeball event if eyeball event has already been emitted', () => {
    stub(EyeballAnalytics.prototype as any, 'hasEyeballEventBeenEmitted')
      .callsFake(
      () => (true),
    );
    const emitEyeballSpy = spy(EyeballAnalytics.prototype as any, 'emitEyeballEvent');

    shallow(
      <EyeballAnalytics />,
    );

    expect(emitEyeballSpy.notCalled).to.equal(true);
  });
});
