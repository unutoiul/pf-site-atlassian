// tslint:disable:jsx-use-translation-function
import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { spy } from 'sinon';

import AkButton from '@atlaskit/button';
import AkChevronDownIcon from '@atlaskit/icon/glyph/chevron-down';
import AkChevronUpIcon from '@atlaskit/icon/glyph/chevron-up';

import { createMockIntlProp } from '../../utilities/testing';

import { ContentRevealImpl } from './content-reveal';

describe('Content Reveal', () => {
  const testLabel = 'Lorem ipsum';
  const testContent = <p>Lorem ipsum is cool.</p>;
  const upChevron = <AkChevronUpIcon label="Expand" />;
  const downChevron = <AkChevronDownIcon label="Expanded" />;
  const onOpenSpy = spy();
  const onCloseSpy = spy();

  const getWrapper = ({ label, children, onOpen, onClose }) => {
    return shallow((
      <ContentRevealImpl
        label={label}
        intl={createMockIntlProp()}
        onOpen={onOpen}
        onClose={onClose}
      >
        {children}
      </ContentRevealImpl>
    ));
  };

  it('should render unexpanded', () => {
    const wrapper = getWrapper({ label: testLabel, children: testContent, onOpen: undefined, onClose: undefined });
    expect(wrapper.text()).to.not.include('Lorem ipsum is cool.');
  });

  it('should render the label in the button', () => {
    const wrapper = getWrapper({ label: testLabel, children: testContent, onOpen: undefined, onClose: undefined });
    expect(wrapper.find(AkButton).prop('children')).to.equal(testLabel);
  });

  it('should render the content when clicked', () => {
    const wrapper = getWrapper({ label: testLabel, children: testContent, onOpen: undefined, onClose: undefined });

    expect(wrapper.text()).to.not.include('Lorem ipsum is cool.');
    wrapper.find(AkButton).simulate('click');
    expect(wrapper.text()).to.include('Lorem ipsum is cool.');
  });

  it('should render the up chevron when not expanded', () => {
    const wrapper = getWrapper({ label: testLabel, children: testContent, onOpen: undefined, onClose: undefined });
    expect(wrapper.find(AkButton).prop('iconAfter')).to.deep.equal(upChevron);
  });

  it('should render the down chevron when expanded', () => {
    const wrapper = getWrapper({ label: testLabel, children: testContent, onOpen: undefined, onClose: undefined });

    wrapper.find(AkButton).simulate('click');
    expect(wrapper.find(AkButton).prop('iconAfter')).to.deep.equal(downChevron);
  });

  it('should call the onOpen prop when expanded', () => {
    const wrapper = getWrapper({ label: testLabel, children: testContent, onOpen: onOpenSpy, onClose: undefined });

    wrapper.find(AkButton).simulate('click');
    expect(onOpenSpy.callCount).to.equal(1);
  });

  it('should call the onClose prop when collapsed', () => {
    const wrapper = getWrapper({ label: testLabel, children: testContent, onOpen: undefined, onClose: onCloseSpy });

    wrapper.find(AkButton).simulate('click');
    expect(onCloseSpy.callCount).to.equal(0);
    wrapper.find(AkButton).simulate('click');
    expect(onCloseSpy.callCount).to.equal(1);
  });
});
