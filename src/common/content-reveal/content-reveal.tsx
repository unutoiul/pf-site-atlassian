import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import AkButton from '@atlaskit/button';
import AkChevronDownIcon from '@atlaskit/icon/glyph/chevron-down';
import AkChevronUpIcon from '@atlaskit/icon/glyph/chevron-up';

const messages = defineMessages({
  downIconLabel: {
    id: 'content.reveal.down.icon.label',
    defaultMessage: 'Expanded',
    description: 'Expanded icon.',
  },
  upIconLabel: {
    id: 'content.reveal.up.icon.label',
    defaultMessage: 'Expand',
    description: 'Unexpanded icon.',
  },
});

interface Props {
  label: string;
  onOpen?(): void;
  onClose?(): void;
}

interface State {
  isExpanded: boolean;
}

export class ContentRevealImpl extends React.Component<Props & InjectedIntlProps, State> {
  public readonly state: Readonly<State> = {
    isExpanded: false,
  };

  public render() {
    const { formatMessage } = this.props.intl;

    return (
      <React.Fragment>
        <AkButton
          appearance="link"
          onClick={this.onButtonClick}
          iconAfter={this.state.isExpanded ? (
            <AkChevronDownIcon label={formatMessage(messages.downIconLabel)} />
          ) : (
            <AkChevronUpIcon label={formatMessage(messages.upIconLabel)} />
          )}
          spacing="none"
        >
          {this.props.label}
        </AkButton>
        {this.state.isExpanded && (
          <div>{this.props.children}</div>
        )}
      </React.Fragment>
    );
  }

  public onButtonClick = () => {
    this.setState(prevState => ({
      isExpanded: !prevState.isExpanded,
    }), () => {
      if (this.state.isExpanded) {
        if (this.props.onOpen) {
          this.props.onOpen();
        }
      } else {
        if (this.props.onClose) {
          this.props.onClose();
        }
      }
    });
  };
}

export const ContentReveal = injectIntl(ContentRevealImpl);
