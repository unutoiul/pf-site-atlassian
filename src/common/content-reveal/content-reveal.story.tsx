// tslint:disable:jsx-use-translation-function
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';
import styled from 'styled-components';

import { ContentReveal } from './content-reveal';

const Wrapper = styled.div`
  margin: 25px;
  max-width: 500px;
`;

storiesOf('Common|Content Reveal', module)
  .add('Default', () => {
    return (
      <IntlProvider>
        <Wrapper>
          <ContentReveal label="Reveal content">
            <h5>Revealed Lorem Ipsum</h5>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis sunt dolore nihil beatae sed ratione
              incidunt totam eaque reprehenderit non? Eos corporis necessitatibus consequatur eligendi quis consequuntur dignissimos dolores minima?
            </p>
          </ContentReveal>
        </Wrapper>
      </IntlProvider>
    );
  });
