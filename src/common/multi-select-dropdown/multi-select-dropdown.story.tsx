// tslint:disable jsx-use-translation-function
import { storiesOf } from '@storybook/react';
import * as React from 'react';

import { MultiSelectDropdown } from './multi-select-dropdown';

storiesOf('Common|Muli Select Dropdown', module)
  .add('Disabled, loading, or no with items', () => {
    return (
      <MultiSelectDropdown
        placeholder={<span>Default label</span>}
      />
    );
  }).add('With one item selected', () => {
    return (
      <MultiSelectDropdown
        placeholder={<span>Default label</span>}
        allItems={[{
          id: 'fluffy-puppies',
          label: 'Fluffy Puppies',
          isSelected: true,
        }]}
      />
    );
  }).add('With two items selected', () => {
    return (
      <MultiSelectDropdown
        placeholder={<span>Default label</span>}
        allItems={[{
          id: 'fluffy-puppies',
          label: 'Fluffy Puppies',
          isSelected: true,
        }, {
          id: 'fluffy-kittens',
          label: 'Fluffy Kittens',
          isSelected: true,
        }]}
      />
    );
  });
