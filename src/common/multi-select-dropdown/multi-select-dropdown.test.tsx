// tslint:disable:jsx-use-translation-function
import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { spy } from 'sinon';

import AkDropdown, {
  DropdownItemCheckbox as AkDropdownItemCheckbox,
} from '@atlaskit/dropdown-menu';

import { TruncatedField } from 'common/truncated-field';

import { MultiSelectDropdown, MultiSelectDropdownItem, MultiSelectDropdownProps, NumberWrapper } from './multi-select-dropdown';

const createTestComponent = (
  { parentItem, allItems, onChanged }: Pick<MultiSelectDropdownProps, 'parentItem' | 'allItems' | 'onChanged'>,
) => {
  return shallow(
    <MultiSelectDropdown
      placeholder={<span>Click me!</span>}
      parentItem={parentItem}
      allItems={allItems}
      onChanged={onChanged}
    />,
  );
};

describe('Multi Select Dropdown', () => {

  describe('on disabled state', () => {
    const items: MultiSelectDropdownItem[] = [
      {
        id: 'DUMMY_CHILD_ITEM_1',
        label: 'Dummy child item #1',
        isSelected: false,
      },
    ];

    it('should be disabled if there is no child items', () => {
      const wrapper = createTestComponent({});
      expect(wrapper.find(AkDropdown).exists()).to.equal(true);
      expect(wrapper.find(AkDropdown).props().triggerButtonProps).to.deep.equal({ isDisabled: true });
    });

    it('should not be disabled if there are child items', () => {
      const wrapper = createTestComponent({ allItems: items });
      expect(wrapper.find(AkDropdown).exists()).to.equal(true);
      expect(wrapper.find(AkDropdown).props().triggerButtonProps).to.be.undefined();
    });
  });

  describe('with no items selected', () => {
    const items: MultiSelectDropdownItem[] = [
      {
        id: 'DUMMY_CHILD_ITEM_1',
        label: 'Dummy child item #1',
        isSelected: false,
      },
    ];

    it('should have the default label as the trigger', () => {
      const wrapper = createTestComponent({ allItems: items });
      expect(wrapper.find(AkDropdown).exists()).to.equal(true);
      const dropdownLabel = shallow(wrapper.find(AkDropdown).prop('trigger') as any);
      expect(dropdownLabel.find('span').text()).to.equal('Click me!');
    });

  });

  describe('with one items selected', () => {
    const items: MultiSelectDropdownItem[] = [
      {
        id: 'DUMMY_CHILD_ITEM_1',
        label: 'Dummy child item #1',
        isSelected: true,
      },
      {
        id: 'DUMMY_CHILD_ITEM_2',
        label: 'Dummy child item #2',
        isSelected: false,
      },
    ];

    it('should have the checked item as the trigger', () => {
      const wrapper = createTestComponent({ allItems: items });
      expect(wrapper.find(AkDropdown).exists()).to.equal(true);
      const dropdownLabel = shallow(wrapper.find(AkDropdown).prop('trigger') as any);
      expect(dropdownLabel.find(TruncatedField).children().text()).to.equal('Dummy child item #1');
      expect(dropdownLabel.find(NumberWrapper).exists()).to.equal(false);
    });

  });

  describe('with more than one items selected', () => {
    const items: MultiSelectDropdownItem[] = [
      {
        id: 'DUMMY_CHILD_ITEM_1',
        label: 'Dummy child item #1',
        isSelected: true,
      },
      {
        id: 'DUMMY_CHILD_ITEM_2',
        label: 'Dummy child item #2',
        isSelected: true,
      },
    ];

    it('should have the checked items as the trigger', () => {
      const wrapper = createTestComponent({ allItems: items });
      expect(wrapper.find(AkDropdown).exists()).to.equal(true);
      const dropdownLabel = shallow(wrapper.find(AkDropdown).prop('trigger') as any);
      expect(dropdownLabel.find(TruncatedField).children().text()).to.equal('Dummy child item #1, Dummy child item #2');
      expect(dropdownLabel.find(NumberWrapper).children().text()).to.equal('(2)');
    });

  });

  describe('onChanged', () => {
    it('should propagate the react event', () => {
      const onChangedSpy = spy();
      const wrapper = createTestComponent({
        allItems: [
          {
            id: 'DUMMY_CHILD_ITEM_1',
            label: 'Dummy child item #1',
            isSelected: true,
          },
        ],
        onChanged: onChangedSpy,
      });

      const reactEvent = { currentTarget: { id: 'test-1' } };
      wrapper.find(AkDropdownItemCheckbox).at(0).simulate('click', reactEvent);

      expect(onChangedSpy.callCount).to.equal(1);
      expect(onChangedSpy.getCall(0).args[0]).to.equal(reactEvent);
    });

    describe('attribute propagation', () => {
      it('should set item id correctly', () => {
        const wrapper = createTestComponent({
          allItems: [
            {
              id: 'test-1',
              label: 'Item 1',
              isSelected: true,
            },
            {
              id: 'test-2',
              label: 'Item 2',
              isSelected: false,
            },
          ],
        });

        const checkbox = wrapper.find(AkDropdownItemCheckbox);
        expect(checkbox).to.have.length(2);
        expect(checkbox.at(0).prop('id')).to.equal('test-1');
        expect(checkbox.at(1).prop('id')).to.equal('test-2');
      });

      it('should propagate item id to onChanged callback', () => {
        const onChangedSpy = spy();
        const wrapper = createTestComponent({
          allItems: [
            {
              id: 'test-1',
              label: 'Item 1',
              isSelected: true,
            },
            {
              id: 'test-2',
              label: 'Item 2',
              isSelected: false,
            },
          ],
          onChanged: onChangedSpy,
        });

        const reactEvent = { currentTarget: { id: 'test-1' } };
        wrapper.find(AkDropdownItemCheckbox).at(0).simulate('click', reactEvent);

        expect(onChangedSpy.callCount).to.equal(1);
        expect(onChangedSpy.getCall(0).args[1].id).to.equal('test-1');
      });

      it('should propagate item selected state to onChanged callback', () => {
        const onChangedSpy = spy();
        const wrapper = createTestComponent({
          allItems: [
            {
              id: 'test-1',
              label: 'Item 1',
              isSelected: true,
            },
            {
              id: 'test-2',
              label: 'Item 2',
              isSelected: false,
            },
          ],
          onChanged: onChangedSpy,
        });

        const reactEvent1 = { currentTarget: { id: 'test-1' } };
        wrapper.find(AkDropdownItemCheckbox).at(0).simulate('click', reactEvent1);

        expect(onChangedSpy.callCount).to.equal(1);
        expect(onChangedSpy.getCall(0).args[1]).to.deep.equal({
          id: 'test-1',
          oldSelected: true,
          newSelected: false,
        });

        onChangedSpy.reset();
        const reactEvent2 = { currentTarget: { id: 'test-2' } };
        wrapper.find(AkDropdownItemCheckbox).at(1).simulate('click', reactEvent2);
        expect(onChangedSpy.callCount).to.equal(1);
        expect(onChangedSpy.getCall(0).args[1]).to.deep.equal({
          id: 'test-2',
          oldSelected: false,
          newSelected: true,
        });
      });
    });

  });

});
