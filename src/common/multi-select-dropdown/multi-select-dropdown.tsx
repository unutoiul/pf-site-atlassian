import * as React from 'react';
import styled from 'styled-components';

import AkDropdown, {
  DropdownItemCheckbox as AkDropdownItemCheckbox,
  DropdownItemGroupCheckbox as AkDropdownItemGroupCheckbox,
} from '@atlaskit/dropdown-menu';
import { gridSize as akGridSize } from '@atlaskit/theme';

import { TruncatedField } from 'common/truncated-field';

export interface MultiSelectDropdownChangedEvent {
  id: string;
  oldSelected: boolean;
  newSelected: boolean;
}

export interface MultiSelectDropdownProps {
  /**
   * The item that sits at the top of all other items. This is generally used to denote smth like "All" or "None" options
   */
  parentItem?: MultiSelectDropdownItem;
  allItems?: MultiSelectDropdownItem[];
  /**
   * Displayed when no other item is selected
   */
  placeholder: React.ReactNode;
  /**
   * Fired when a certain checkbox value is changed.
   * You can use the second function argument to get the item's id and its selection state.
   */
  onChanged?(e: React.ChangeEvent<HTMLInputElement>, event: MultiSelectDropdownChangedEvent): void;
}

export interface MultiSelectDropdownItem {
  label: string;
  id: string;
  isSelected: boolean;
  isDisabled?: boolean;
}

const DROPDOWN_WIDTH = akGridSize() * 23;
const LABEL_WIDTH = akGridSize() * 16;

const DropdownWrapper = styled.div`
  width: ${DROPDOWN_WIDTH}px;
`;

const Wrapper = styled.div`
  display: flex;
  align-content: space-between;
  text-align: left;
  width: ${LABEL_WIDTH}px;

  button {
    vertical-align: top;
  }
`;

export const NumberWrapper = styled.span`
  vertical-align: top;
  padding-left: ${akGridSize}px;
`;

export const Divider = styled.hr`
  margin: ${akGridSize}px;
  border-style: none;
`;

export class MultiSelectDropdown extends React.Component<MultiSelectDropdownProps> {
  public render() {
    const {
      placeholder,
      allItems,
      parentItem,
    } = this.props;

    if (!allItems) {
      return (
        <AkDropdown
          shouldFitContainer={true}
          triggerButtonProps={{ isDisabled: true }}
          triggerType="button"
          trigger={<Wrapper>{placeholder}</Wrapper>}
        />
      );
    }

    const selected = allItems.filter(item => item.isSelected).map(item => item.label);

    if (parentItem && parentItem.isSelected) {
      selected.push(parentItem.label);
    }

    const dropdownLabel = (
      <Wrapper>
        <TruncatedField maxWidth={`${LABEL_WIDTH}px`}>
          {selected.length ? selected.join(', ') : parentItem && parentItem.isSelected ? parentItem.label : placeholder}
        </TruncatedField>
        {selected.length > 1 && <NumberWrapper>{`(${selected.length})`}</NumberWrapper>}
      </Wrapper>
    );

    return (
      <DropdownWrapper>
        <AkDropdown shouldFitContainer={true} triggerType="button" trigger={dropdownLabel}>
          {parentItem &&
            <React.Fragment>
              <AkDropdownItemGroupCheckbox>
                {this.generateCheckbox(parentItem)}
              </AkDropdownItemGroupCheckbox>
              <Divider />
            </React.Fragment>
          }
          <AkDropdownItemGroupCheckbox>
            {allItems.map(this.generateCheckbox)}
          </AkDropdownItemGroupCheckbox>
        </AkDropdown>
      </DropdownWrapper>
    );
  }

  private generateCheckbox = (item: MultiSelectDropdownItem): React.ReactNode => {
    return (
      <AkDropdownItemCheckbox
        id={item.id}
        key={item.id}
        onClick={item.isSelected ? this.onUnselect : this.onSelect}
        isSelected={item.isSelected}
        isDisabled={item.isDisabled || false}
      >
        {item.label}
      </AkDropdownItemCheckbox>
    );
  }

  private onSelect = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.onSelectionChanged(e, true);
  };

  private onUnselect = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.onSelectionChanged(e, false);
  };

  private onSelectionChanged(e: React.ChangeEvent<HTMLInputElement>, newIsSelected: boolean) {
    const { onChanged } = this.props;

    if (!(onChanged instanceof Function)) {
      return;
    }

    onChanged(e, {
      id: e.currentTarget.id,
      oldSelected: !newIsSelected,
      newSelected: newIsSelected,
    });
  }
}
