// tslint:disable:jsx-use-translation-function
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';

import AkDynamicTable from '@atlaskit/dynamic-table';

import { TextReveal } from '.';

const head = {
  cells: [
    {
      key: '1',
      content: 'Text Reveal Field',
      width: 20,
    },
    {
      key: '2',
      content: 'Normal Field',
      width: 80,
    },
  ],
};

const rows = [
  {
    cells: [
      {
        key: '1',
        content: (
          <div style={{ width: '200px' }}>
            <TextReveal>
              Lorem ipsum dolor sit amet consectetur adipisicing elit.
              Fuga ad porro ratione facere rem vitae iusto culpa maiores
              eveniet deserunt quibusdam quae reprehenderit iste quidem,
              autem libero ea quia. Dolor.
            </TextReveal>
          </div>
        ),
      },
      {
        key: '2',
        content: (
          <div>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Repellat, commodi officiis recusandae debitis ipsa eveniet
            sapiente necessitatibus ad tempora sit nobis non. Unde rem
            velit quidem porro eum repellendus dolorum!
          </div>
        ),
      },
    ],
  },
];

storiesOf('Common|Text Reveal', module)
  .add('Default', () => {
    return (
      <IntlProvider locale="en">
        <AkDynamicTable
          caption="Table"
          head={head}
          rows={rows}
        />
      </IntlProvider>
    );
  });
