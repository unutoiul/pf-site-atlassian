import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import { IntlProvider } from 'react-intl';

import { createMockIntlContext } from '../../utilities/testing';
import { TextReveal } from './text-reveal';

describe('TextReveal', () => {
  const getWrapper = (description: string | null) => {
    return mount((
      <IntlProvider locale="en">
        <TextReveal>
          {description}
        </TextReveal>
      </IntlProvider>
    ), createMockIntlContext());
  };

  it('should display the text "No description" if no description exists', () => {
    const wrapper = getWrapper(null);
    expect(wrapper.text()).to.equal('No description');
  });

  // NOTE: Enzyme does not seem to support offsetWidth/offsetHeight on elements, so we cannot
  // reliably simulate detection of width/height changes for now.
  it.skip('should render the entire text if expanded');
});
