import * as React from 'react';
import {
  defineMessages,
  FormattedMessage,
  injectIntl,
} from 'react-intl';
import styled from 'styled-components';

import AkButton from '@atlaskit/button';
import {
  colors as akColors,
  fontSize as akFontSize,
  gridSize as akGridSize,
} from '@atlaskit/theme';

const messages = defineMessages({
  noDescription: {
    id: 'text-reveal.no.description',
    defaultMessage: 'No description',
  },
  more: {
    id: 'text-reveal.more',
    defaultMessage: 'Show more',
  },
  less: {
    id: 'text-reveal.less',
    defaultMessage: 'Show less',
  },
});

const Description = styled.div`
  padding-top: ${akGridSize() / 2}px;
  font-size: ${akFontSize() - 2}px;
  display: flex;
  align-items: flex-end;
`;

const NoDescription = styled.div`
  padding-top: ${akGridSize() / 2}px;
  color: ${akColors.N100};
  font-size: ${akFontSize() - 2}px;
  font-style: italic;
`;

export const MoreLinkWrapper = styled(AkButton)`
  padding: 0 0 0 ${akGridSize() / 2}px;
  margin: 0;
`;

const DescriptionWrapper = styled.span`
  overflow: hidden;
  text-overflow: ellipsis;
  flex: 1;
  white-space: ${(props: { nowrap: boolean }) => props.nowrap ? 'nowrap' : 'initial'};
`;

interface ExpandLinkProps {
  expanded: boolean;
  onClick(): void;
}

const ExpandLink = injectIntl<ExpandLinkProps>(
  ({ onClick, expanded }: ExpandLinkProps) => (
    <MoreLinkWrapper
      appearance="subtle-link"
      spacing="none"
      onClick={onClick}
    >
      {expanded ? (
        <FormattedMessage {...messages.less} />
      ) : (
        <FormattedMessage {...messages.more} />
      )}
    </MoreLinkWrapper>
  ),
);

export interface TextRevealProps {
  children: string | null;
}

interface TextRevealState {
  expanded: boolean;
  isOverflown: boolean;
}

export class TextReveal extends React.Component<TextRevealProps, TextRevealState> {
  public state: TextRevealState = {
    expanded: false,
    isOverflown: false,
  };

  public render() {
    const { children } = this.props;

    if (!children) {
      return (
        <NoDescription>
          <FormattedMessage {...messages.noDescription} />
        </NoDescription>
      );
    }

    return (
      <Description>
        <DescriptionWrapper
          innerRef={this.setRef}
          nowrap={!this.state.expanded}
        >
          {children}
          {this.state.expanded && (
            <ExpandLink
              onClick={this.toggleExpanded}
              expanded={this.state.expanded}
            />
          )}
        </DescriptionWrapper>
        {!this.state.expanded && this.state.isOverflown && (
          <ExpandLink
            onClick={this.toggleExpanded}
            expanded={this.state.expanded}
          />
        )}
      </Description>
    );
  }

  private toggleExpanded = () => {
    this.setState(prevState => ({ expanded: !prevState.expanded }));
  };

  private setRef = (ref) => {
    if (ref) {
      this.setState({ isOverflown: ref.offsetWidth < ref.scrollWidth });
    }
  };
}
