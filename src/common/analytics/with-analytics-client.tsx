import * as React from 'react';

import { NewAnalyticsClient } from './new-analytics-client';
import { NewAnalyticsContext } from './new-analytics-context';

export interface AnalyticsClientProps {
  analyticsClient: NewAnalyticsClient;
}

export function withAnalyticsClient<TOwnProps>(
  WrappedComponent: React.ComponentType<TOwnProps & AnalyticsClientProps>,
): React.ComponentClass<TOwnProps> {
  return class extends React.Component<TOwnProps> {
    public static WrappedComponent = WrappedComponent;

    public render() {
      return (
        <NewAnalyticsContext.Consumer>
          {this.wrappedComponentRenderer}
        </NewAnalyticsContext.Consumer>
      );
    }

    private wrappedComponentRenderer = (analyticsClient: NewAnalyticsClient): React.ReactNode => (
      <WrappedComponent
        {...this.props}
        analyticsClient={analyticsClient}
      />
    )
  };
}
