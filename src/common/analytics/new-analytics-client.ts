import PlatformAnalyticsClient, {
  tenantType,
  userType,
} from '@atlassiansox/analytics-web-client';

import { getConfig } from 'common/config';

import { apiFacade, MeClient } from '../../schema/api/client';
import { util as adminHubUtil } from '../../utilities/admin-hub';
import { getCloudIdFromPath, getOrgIdFromPath } from '../../utilities/get-id-from-path';
import { analyticsClient as newRelicClient } from './analytics-client';
import { ScreenData, ScreenEvent, TrackData, TrackEvent, UIData, UIEvent } from './new-analytics-types';

const product = {
  siteAdmin: 'siteAdmin',
  adminHub: 'adminHub',
};

type Event = UIEvent | ScreenEvent | TrackEvent;

export interface NewAnalyticsClient {
  init(): void;
  sendUIEvent(e: UIEvent): void;
  sendScreenEvent(e: ScreenEvent): void;
  sendTrackEvent(e: TrackEvent): void;
}

export class AnalyticsWebClient implements NewAnalyticsClient {
  private client: PlatformAnalyticsClient;

  constructor(
    private meClient: MeClient,
    private logEvent?: (...args) => void,
    client?: PlatformAnalyticsClient,
    private getPathname: () => string = () => window.location.pathname,
  ) {
    this.client = client || new PlatformAnalyticsClient({
      env: getConfig().newAnalyticsClientEnvironment,
      product: 'admin',
      subproduct: adminHubUtil.isAdminHub() ? product.adminHub : product.siteAdmin,
      version: '1.0.0',
      locale: 'en-US',
    });
  }

  public init = () => {
    // tslint:disable no-floating-promises
    this.setAtlassianAccountId();
  }

  public static create(): AnalyticsWebClient {
    return new AnalyticsWebClient(apiFacade.meClient);
  }

  public sendUIEvent = (e: UIEvent) => {
    this.sendEvent(e, this.client.sendUIEvent.bind(this.client));
  }

  public sendScreenEvent = (e: ScreenEvent) => {
    this.sendEvent(e, this.client.sendScreenEvent.bind(this.client));
    newRelicClient.onPageAction({
      name: 'screen.viewed',
      attributes: {
        screenName: e.data.name,
      },
    });
  }

  public sendTrackEvent = (e: TrackEvent) => {
    this.sendEvent(e, this.client.sendTrackEvent.bind(this.client));
  }

  private sendEvent(e: Event, send: (data: UIData | ScreenData | TrackData) => void) {
    this.client.clearTenantInfo();

    const cloudIdFromPath = getCloudIdFromPath(this.getPathname());
    const orgIdFromPath = getOrgIdFromPath(this.getPathname());

    if (e.cloudId || cloudIdFromPath) {
      this.client.setTenantInfo(tenantType.CLOUD_ID, e.cloudId || cloudIdFromPath);
    }

    if (e.orgId || orgIdFromPath) {
      this.client.setTenantInfo(tenantType.ORG_ID, e.orgId || orgIdFromPath);
    }

    if (__NODE_ENV__ !== 'development' && __NODE_ENV__ !== 'test') {
      send(e.data);
    } else if (__NODE_ENV__ !== 'test') {
      // We don't want console log lines showing up in our tests, so we only have the default as console.log outside of tests
      // tslint:disable-next-line:no-console
      (this.logEvent && this.logEvent.bind(this) || console.log.bind(this))(`[New analytics client]`, e.data);
    } else {
      if (this.logEvent) {
        this.logEvent(`[New analytics client]`, e.data);
      }
    }
  }

  private async setAtlassianAccountId() {
    try {
      const currentUser = await this.meClient.getCurrentUser();
      this.client.setUserInfo(userType.ATLASSIAN_ACCOUNT, currentUser.account_id);
    } catch (_) {
      // noop because we dont want to double-up on reporting errors
      // for the getCurrentUser call failing as we are already reporting those elsewhere
    }
  }
}
