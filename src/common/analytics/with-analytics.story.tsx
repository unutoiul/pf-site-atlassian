// tslint:disable jsx-use-translation-function
import { storiesOf } from '@storybook/react';
import * as React from 'react';

import { AnalyticsListener as AkAnalyticsListener } from '@atlaskit/analytics';
import AkButton from '@atlaskit/button';

import { withClickAnalytics } from './with-analytics';

class AnalyticsWrapper extends React.Component<any, any> {
  public state = { events: [] };
  public render() {
    return (
      <AkAnalyticsListener onEvent={this.onEvent}>
        <div>
          {this.props.children}
          <pre>{this.state.events.map(e => JSON.stringify(e)).join('\n')}</pre>
        </div>
      </AkAnalyticsListener>
    );
  }

  private onEvent = (_, data) => this.setState({ events: [...this.state.events, data] });
}

storiesOf('Common|With Click Analytics', module)
  .add('Simple usage', () => {
    const ButtonWithAnalytics = withClickAnalytics()(AkButton);

    return (
      <div>
        <p>
          Use <code>withClickAnalytics</code> to make your React component report "click" events.
        </p>
        <p>
          Click on a button to see what gets sent.
        </p>
        <AnalyticsWrapper>
          <ButtonWithAnalytics
            analyticsData={{ action: 'click', actionSubject: 'genericButton', actionSubjectId: 'this-button', subproduct: 'chrome' } as any}
          >
            Analyze my clicks
          </ButtonWithAnalytics>
        </AnalyticsWrapper>
      </div>
    );
  });
