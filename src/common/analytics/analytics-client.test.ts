import { expect } from 'chai';
import { SinonSpy, spy } from 'sinon';

import { AnalyticsClient, PageActionEvent } from './analytics-client';
import { AnalyticsData } from './with-analytics';

describe('Analytics client', () => {
  let spies: {
    init: SinonSpy;
    onEvent: SinonSpy;
    onError: SinonSpy;
    onPageAction: SinonSpy;
  };
  let analyticsClient: AnalyticsClient;
  beforeEach(() => {
    spies = {
      init: spy(),
      onEvent: spy(),
      onError: spy(),
      onPageAction: spy(),
    };
    analyticsClient = new AnalyticsClient(spies);
  });

  it('submits an event onPageLoad', () => {
    analyticsClient.onPageLoad();

    expect(spies.onEvent.callCount).to.equal(1);
    const data: AnalyticsData = spies.onEvent.args[0][1];
    expect(data.action).to.equal('load');
    expect(data.actionSubject).to.equal('page');
  });

  it('propagates properties to analytics implementor', () => {
    const analyticsData = {} as any;
    analyticsClient.onEvent(analyticsData);

    expect(spies.onEvent.callCount).to.equal(1);
    const data: AnalyticsData = spies.onEvent.args[0][1];
    expect(data).to.equal(analyticsData);
  });

  it('propagates error details to analytics implementor', () => {
    const analyticsError = new Error('Test error');
    analyticsClient.onError(analyticsError);

    expect(spies.onError.callCount).to.equal(1);
    const error = spies.onError.args[0][0];
    expect(error).to.equal(analyticsError);
  });

  it('sends page action data', () => {
    const pageActionEvent = {
      name: 'test.name',
      attributes: {
        testAttribute: 'testValue',
      },
    } as PageActionEvent;

    analyticsClient.onPageAction(pageActionEvent);

    expect(spies.onPageAction.callCount).to.equal(1);
    const calledPageActionEvent = spies.onPageAction.args[0];
    expect(calledPageActionEvent).to.deep.equals([
      'test.name',
      {
        testAttribute: 'testValue',
      },
    ]);
  });
});
