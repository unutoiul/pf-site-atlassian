import { expect } from 'chai';
import * as sinon from 'sinon';

import {
  tenantType,
} from '@atlassiansox/analytics-web-client';

import { confirmMemberDeletion, jsdExportCustomersUIEventData } from 'common/analytics';

import { AnalyticsWebClient } from './new-analytics-client';

const noop = () => null;

describe('NewAnalyticsClient', () => {
  it('should send all data specified to the platform analytics client', () => {
    const logSpy = sinon.spy();
    const platformAnalyticsClientSpy = {
      clearTenantInfo: sinon.spy(),
      setTenantInfo: sinon.spy(),
      sendUIEvent: noop,
    };

    const client = new AnalyticsWebClient({} as any, logSpy, platformAnalyticsClientSpy as any, () => '/o/fake-org-id/users');

    const eventData = confirmMemberDeletion({
      orgId: 'test',
      memberId: 'test',
      products: [
        'product-1',
        'product-2',
      ],
    });

    client.sendUIEvent({
      data: eventData,
    });

    expect(logSpy.callCount).to.equal(1);
    expect(logSpy.getCalls()[0].args[0]).to.equal(`[New analytics client]`);
    expect(logSpy.getCalls()[0].args[1]).to.deep.equal(eventData);
  });

  it('should send a cloud id from the URL if one is not specified in an event', () => {
    const platformAnalyticsClientSpy = {
      clearTenantInfo: sinon.spy(),
      setTenantInfo: sinon.spy(),
      sendUIEvent: noop,
    };

    const client = new AnalyticsWebClient({} as any, noop, platformAnalyticsClientSpy as any, () => '/s/fake-cloud-id/users');

    client.sendUIEvent({
      data: jsdExportCustomersUIEventData(),
    });

    expect(platformAnalyticsClientSpy.clearTenantInfo.callCount).to.equal(1);
    expect(platformAnalyticsClientSpy.setTenantInfo.callCount).to.equal(1);

    expect(platformAnalyticsClientSpy.setTenantInfo.getCalls()[0].args[0]).to.equal(tenantType.CLOUD_ID);
    expect(platformAnalyticsClientSpy.setTenantInfo.getCalls()[0].args[1]).to.equal('fake-cloud-id');
  });

  it('should send a org id from the URL if one is not specified in an event', () => {
    const platformAnalyticsClientSpy = {
      clearTenantInfo: sinon.spy(),
      setTenantInfo: sinon.spy(),
      sendUIEvent: noop,
    };

    const client = new AnalyticsWebClient({} as any, noop, platformAnalyticsClientSpy as any, () => '/o/fake-org-id/managed-accounts');

    client.sendUIEvent({
      data: jsdExportCustomersUIEventData(),
    });

    expect(platformAnalyticsClientSpy.clearTenantInfo.callCount).to.equal(1);
    expect(platformAnalyticsClientSpy.setTenantInfo.callCount).to.equal(1);

    expect(platformAnalyticsClientSpy.setTenantInfo.getCalls()[0].args[0]).to.equal(tenantType.ORG_ID);
    expect(platformAnalyticsClientSpy.setTenantInfo.getCalls()[0].args[1]).to.equal('fake-org-id');
  });

  it('should send a cloud id from the params if one is specified in the event', () => {
    const platformAnalyticsClientSpy = {
      clearTenantInfo: sinon.spy(),
      setTenantInfo: sinon.spy(),
      sendUIEvent: noop,
    };

    const client = new AnalyticsWebClient({} as any, noop, platformAnalyticsClientSpy as any, () => '/s/url-cloud-id/users');

    client.sendUIEvent({
      cloudId: 'event-cloud-id',
      data: jsdExportCustomersUIEventData(),
    });

    expect(platformAnalyticsClientSpy.clearTenantInfo.callCount).to.equal(1);
    expect(platformAnalyticsClientSpy.setTenantInfo.callCount).to.equal(1);

    expect(platformAnalyticsClientSpy.setTenantInfo.getCalls()[0].args[0]).to.equal(tenantType.CLOUD_ID);
    expect(platformAnalyticsClientSpy.setTenantInfo.getCalls()[0].args[1]).to.equal('event-cloud-id');
  });

  it('should send a org id from the params if one is specified in the event', () => {
    const platformAnalyticsClientSpy = {
      clearTenantInfo: sinon.spy(),
      setTenantInfo: sinon.spy(),
      sendUIEvent: noop,
    };

    const client = new AnalyticsWebClient({} as any, noop, platformAnalyticsClientSpy as any, () => '/o/url-org-id/managed-accounts');

    client.sendUIEvent({
      orgId: 'event-org-id',
      data: jsdExportCustomersUIEventData(),
    });

    expect(platformAnalyticsClientSpy.clearTenantInfo.callCount).to.equal(1);
    expect(platformAnalyticsClientSpy.setTenantInfo.callCount).to.equal(1);

    expect(platformAnalyticsClientSpy.setTenantInfo.getCalls()[0].args[0]).to.equal(tenantType.ORG_ID);
    expect(platformAnalyticsClientSpy.setTenantInfo.getCalls()[0].args[1]).to.equal('event-org-id');
  });
});
