import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';

import { NewAnalyticsContext } from './new-analytics-context';
import { withAnalyticsClient } from './with-analytics-client';

describe('withAnalyticsClient', () => {

  const TestComponent = ({ analyticsClient }) => {
    if (analyticsClient) {
      return <div />;
    }

    return null;
  };

  const TestWrappedComponent = withAnalyticsClient<{}>(TestComponent);

  const mountWithAnalyticsClientWrappedComponent = (analyticsClient) => {
    return mount((
      <NewAnalyticsContext.Provider value={analyticsClient}>
        <TestWrappedComponent />
      </NewAnalyticsContext.Provider>
    ));
  };

  it('should pass analyticsClient from context as prop to WrappedComponent', () => {
    const client = {};
    const wrapper = mountWithAnalyticsClientWrappedComponent(client);

    const testComponent = wrapper.find(TestComponent);

    expect(testComponent.props().analyticsClient).to.equal(client);
  });
});
