import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { spy } from 'sinon';

import { ScreenEvent } from 'common/analytics';

import { ScreenEventSenderImpl, ScreenEventSenderProps } from './screen-event-sender';

const event: ScreenEvent = {
  data: {
    name: 'testEvent',
    attributes: {},
  },
};

describe('ScreenEventSender', () => {
  it('should send a screen event on load, only once', () => {
    const sendScreenEvent = spy();

    const wrapper = shallow<ScreenEventSenderProps>(
      (
        <ScreenEventSenderImpl
          analyticsClient={{ sendScreenEvent, ...({} as any) }}
          event={event}
          isDataLoading={true}
          data={{ loading: false } as any}
        />
      ),
      { disableLifecycleMethods: false },
    );

    expect(sendScreenEvent.callCount).to.equal(0);
    wrapper.setProps({ isDataLoading: false });
    expect(sendScreenEvent.callCount).to.equal(1);
    wrapper.setProps({ event: { data: { newData: true } } as any });
    expect(sendScreenEvent.callCount).to.equal(1);
  });

  it('should send a screen event on mount (and only once) if no loading indicator has been provided', () => {
    const sendScreenEvent = spy();

    const wrapper = shallow<ScreenEventSenderProps>(
      (
        <ScreenEventSenderImpl
          analyticsClient={{ sendScreenEvent, ...({} as any) }}
          event={event}
          data={{ loading: false } as any}
        />
      ),
      { disableLifecycleMethods: false },
    );

    expect(sendScreenEvent.callCount).to.equal(1);
    wrapper.setProps({ event: { data: { newData: true } } as any });
    expect(sendScreenEvent.callCount).to.equal(1);
  });
});
