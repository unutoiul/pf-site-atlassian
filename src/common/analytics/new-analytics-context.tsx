import * as React from 'react';

import { NewAnalyticsClient } from './new-analytics-client';

const noop = () => undefined;
export const NewAnalyticsContext = React.createContext<NewAnalyticsClient>({
  init: noop,
  sendScreenEvent: noop,
  sendTrackEvent: noop,
  sendUIEvent: noop,
});
