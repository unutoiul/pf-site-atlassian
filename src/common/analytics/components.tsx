import { Link as ReactRouterLink, LinkProps } from 'react-router-dom';

import {
  BreadcrumbsItem as AkBreadcrumbsItem,
} from '@atlaskit/breadcrumbs';

import AkButton, {
  AkButtonProps,
} from '@atlaskit/button';

import {
  AkDropdownItemProps,
  DropdownItem as AkDropdownItem,
} from '@atlaskit/dropdown-menu';

import { withClickAnalytics } from './with-analytics';

export const Button = withClickAnalytics<AkButtonProps>()(AkButton);
export const DropdownItem = withClickAnalytics<AkDropdownItemProps>()(AkDropdownItem);
export const Link = withClickAnalytics<LinkProps>()(ReactRouterLink);
export const BreadcrumbsItem = withClickAnalytics<any>()(AkBreadcrumbsItem);
