import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';

import { UpdateReferrerMutation } from '../../schema/schema-types';
import { analyticsClient } from './analytics-client';
import updateReferrerMutation from './referrer.mutation.graphql';

type ReferrerType = 'direct' |
  'breadcrumb' |
  'createGroupDialog' |
  'groupsTableLink' |
  'appDetailsLink' |
  'navigation'|
  'userProvisioningScreen' |
  'launchpad' |
  'password' |
  'saml' |
  'twoStep' |
  'orgOverview';

interface OwnProps {
  value: ReferrerType;
}

export class ReferrerImpl extends React.Component<ChildProps<OwnProps, UpdateReferrerMutation>> {

  public render() {
    return <span aria-hidden={true} role="button" onClick={this.onClick}>{this.props.children}</span>;
  }

  private onClick = () => {
    if (!this.props.mutate) {
      return;
    }

    this.props.mutate({
      variables: {
        referrer: this.props.value,
      },
    }).catch(e => {
      analyticsClient.onError(e);
    });
  }
}

const withMutation = graphql<OwnProps, UpdateReferrerMutation>(updateReferrerMutation);

export const Referrer = withMutation(ReferrerImpl);
