import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { stub } from 'sinon';

import { createApolloClient } from '../../apollo-client';
import { ReferrerImpl } from './referrer';

describe('Referrer', () => {
  it('Calls mutate when clicked', () => {
    const apolloClient = createApolloClient();
    const mutateSpy = stub().resolves();

    const wrapper = mount(
      <ApolloProvider client={apolloClient}>
        <ReferrerImpl value="groupsTableLink" mutate={mutateSpy}>
          <button />
        </ReferrerImpl>
      </ApolloProvider>,
    );

    wrapper.find(ReferrerImpl).simulate('click');
    expect(mutateSpy.called).to.equal(true);
  });
});
