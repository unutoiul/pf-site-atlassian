// past tense verbs describing the action
export const enum Action {
  Clicked = 'clicked',
  Submitted = 'submitted',
  Closed = 'closed',
  Typed = 'typed',
  Searched = 'searched',
  Created = 'created',
  Updated = 'updated',
  Deleted = 'deleted',
  Invited = 'invited',
  Selected = 'selected',
  Focused = 'focused',
  Blurred = 'blurred',
}

export const enum TrackAction {
  Created = 'created',
  Activated = 'activated',
  Added = 'added',
  Deactivated = 'deactivated',
  Deleted = 'deleted',
  Changed = 'changed',
  Submitted = 'submitted',
  Approved = 'approved',
  Denied = 'denied',
  Invited = 'invited',
  Reset = 'reset',
  Resent = 'resent',
  Impersonated = 'impersonated',
  Regenerated = 'regenerated',
  Toggled = 'toggled',
  Granted = 'granted',
  Revoked = 'revoked',
  Saved = 'saved',
  AddedToGroups = 'addedToGroups',
  Exposed = 'exposed',
  Downloaded = 'downloaded',
  Migrated = 'migrated',
  Verified = 'verified',
}

export const enum ActionSubject {
  Button = 'button',
  Form = 'form',
  Link = 'link',
  AppSwitcherDropdown = 'appSwitcherDropdown',
  Dialog = 'dialog',
  Field = 'field',
  NavigationItem = 'navigationItem',
  Toggle = 'toggle',
  Checkbox = 'checkbox',
  DropListItem = 'dropListItem',
}

export const enum TrackActionSubject {
  Customer = 'customer',
  Group = 'group',
  Member = 'member',
  Form = 'form',
  Token = 'token',
  User = 'user',
  UserList = 'userList',
  UserPassword = 'userPassword',
  Invitation = 'invitation',
  ProductAccess = 'productAccess',
  Organization = 'organization',
  AdminApiKey = 'adminApiKey',
  Checkbox = 'checkbox',
  UserPermission = 'userPermission',
  SiteAccess = 'siteAccess',
  Feature = 'feature',
  NewUserAccess = 'newUserAccess',
  InviteUrlLinkToggled = 'inviteUrlLinkToggled',
  InviteUrlLinkRegenerated = 'inviteUrlLinkRegenerated',
  Domain = 'domain',
}

export interface UIEvent {
  cloudId?: string;
  orgId?: string;
  data: UIData;
}

export interface ScreenEvent {
  cloudId?: string;
  orgId?: string;
  data: ScreenData;
}

export interface TrackEvent {
  cloudId?: string;
  orgId?: string;
  data: TrackData;
}

export type UIData = Readonly<{
  action: Action;
  actionSubject: ActionSubject;
  actionSubjectId: string;
  objectType?: string;
  objectId?: string;
  source?: string;
  object?: string;
  container?: string;
  attributes?: object;
  tags?: object;
}>;

export type ScreenData = Readonly<{
  name: string;
  attributes?: object;
  tags?: object;
}>;

export type TrackData = Readonly<{
  source: string;
  action: TrackAction | string;
  actionSubject: TrackActionSubject;
  actionSubjectId?: string;
  attributes?: object;
  tags?: object;
}>;
