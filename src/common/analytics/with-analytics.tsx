import * as React from 'react';

import {
  cleanProps as akCleanProps,
  withAnalytics as akWithAnalytics,
} from '@atlaskit/analytics';

import { ActionSubject as NewActionSubject } from './new-analytics-types';

export type TenantType = 'cloudId'
  | 'organizationId'
  ;

export type AnalyticsActionSubject = 'sidebarContainerButton'
  | 'sidebarGlobalButton'
  | 'createDrawerButton'
  | 'page'
  | 'feedbackDialogButton'
  ;

export type AnalyticsSubproduct = 'chrome'
  | 'billing'
  | 'organization'
  | 'siteSettings'
  | 'userManagement'
  | 'dashboard'
  ;

export type DashboardAnalyticsActionSubject = 'orgCard'
  ;

export type OrgAnalyticsActionSubject = 'organizationPageLoad'
  | 'passwordPolicyButton'
  | 'resetPasswordButton'
  | 'renameOrgButton'
  | 'createOrgButton'
  | 'createOrgPage'
  | 'orgLandingPageIMEval'
  | 'passwordPolicyIMEval'
  | 'samlIMEval'
  | 'twoStepVerificationIMEval'
  | 'identityManagerEvaluationPage'
  | 'enforceTwoStepVerificationButton'
  | 'deactivateTwoStepVerificationEnforcementButton'
  | 'updateOrgMemberDetailsButton'
  | 'resetOrgMemberPasswordButton'
  | 'twoStepVerificationMemberToggle'
  | 'twoStepVerificationMemberDeleteEnrolment'
  | 'enableOrgMemberButton'
  | 'disableOrgMemberButton'
  | 'exportBillableMembersButton'
  | 'mfaUnenrolledFilterToggle'
  | 'verifyDomainButton'
  | 'removeDomainButton'
  | 'orgDetailsLearnMoreButton'
  | 'landingPageVerifyDomainsButton'
  | 'landingPageIMLearnMoreButton'
  | 'removeOrgAdminButton'
  | 'addOrgAdminButton'
  | 'addSamlButton'
  | 'editSamlButton'
  | 'deleteSamlButton'
  | 'migrateSamlToAuth0Button'
  | 'migrateSamlToLegacyButton'
  | 'movedOrgOnboardingPage'
  | 'createOrgOnboardingPage'
  | 'noOrgOnboardingPage'
  | 'orgBreadcrumb'
  ;

export type UMAnalyticsActionSubject = 'siteSettingsPage'
  | 'saveSelfSignupButton'
  | 'groupCreateDialogButton'
  | 'groupEditDialogButton'
  | 'groupDeleteDialogButton'
  | 'addMembersDialogButton'
  | 'userManagementPage'
  | 'groupsScreen';

export type BillingAnalyticsActionSubject = 'billingPage';

export type ActionSubject = AnalyticsActionSubject
  | OrgAnalyticsActionSubject
  | UMAnalyticsActionSubject
  | DashboardAnalyticsActionSubject
  | BillingAnalyticsActionSubject
  | NewActionSubject
  ;

export type Product = 'siteAdmin'
  | 'adminHub'
  ;

/**
 * Represents the data structure that can be handled by analytics client.
 *
 * @export
 * @interface AnalyticsData
 */
export interface AnalyticsData {
  /**
   * Configured once, do not use it unless you now what you're doing.
   */
  env?: DeployEnv;
  /**
   * Configured once, do not use it unless you now what you're doing.
   */
  product?: Product;
  /**
   * What part of Cloud Admin application triggered the event.
   */
  subproduct?: AnalyticsSubproduct;

  /**
   * Configured once, do not use it unless you now what you're doing.
   */
  tenantType?: TenantType;
  /**
   * Configured once, do not use it unless you now what you're doing.
   */
  tenantId?: string;

  /**
   * Has no use for now, do not use it unless you now what you're doing.
   */
  // containerType?: string;
  /**
   * Has no use for now, do not use it unless you now what you're doing.
   */
  // containerId?: string;
  /**
   * Has no use for now, do not use it unless you now what you're doing.
   */
  // context?: string;

  /*
   * Used to describe the type of object that an action was performed on
   */
  objectType?: string;

  /*
   * Identifier for the object actioned upon
   */
  objectId?: string;

  /**
   * Used to describe the type of object that triggered an action. It should be generic enough to encompass various action subject ids.
   */
  actionSubject: ActionSubject;
  /**
   * What was the action taken? I.e. "click", "open/close", etc.
   */
  action: string;
  /**
   * Used to help one distinguish various events with same "action" and "actionSubject" from one another.
   */
  actionSubjectId: string;
  /**
   * A hash of any additional data (but directly related to the event!) that you want to pass along. Do not abuse it! No PII (such as user email), no user-generated content, no generic info.
   */
  attributes?: object;
  /**
   * Has no use for now, do not use it unless you now what you're doing.
   */
  // categories?: string[];
}

export interface AnalyticsProps {
  analyticsData?: AnalyticsData;
}

export declare type AnalyticsComponentDecorator<P> = (Component: React.ComponentType<P>) => React.ComponentClass<P & AnalyticsProps>;

function withAnalytics<P>(eventMap = {}, defaultProps: Partial<AnalyticsData> = {}): (Component: React.ComponentType<P>) => React.ComponentClass<P & AnalyticsProps> {

  return (Component: React.ComponentType<P>): React.ComponentClass<P & AnalyticsProps> => {
    return akWithAnalytics(
      ({ children, ...props }) => (
        <Component {...akCleanProps(props)} >
          {children}
        </Component>
      ),
      eventMap,
      {
        // if we don't set analyticsId then `@atlaskit/analytics` won't propagate event.
        // On the other hand, `@atlassian/analytics-web-client` doesn't care about it,
        // so we're not allowing consumers to set it.
        analyticsId: 'cloud-admin',
        analyticsData: defaultProps,
      },
    );
  };
}

/**
 * Creates a decorator to be used for creating analytics-capable higher order components.
 *
 * @param {Partial<AnalyticsData>} [defaultProps={}] defaults for analytics data that'll be common for this HOC.
 * @returns {AnalyticsComponentDecorator<P>} Factory for HOC that has analytics reporting capabilities and accepts an additional `analyticsData` property
 */
export function withClickAnalytics<P>(defaultProps: Partial<AnalyticsData> = {}): (Component: React.ComponentType<P>) => React.ComponentClass<P & AnalyticsProps> {
  return withAnalytics<P>({ onClick: 'click' }, defaultProps);
}

/**
 * Creates a decorator to be used for creating analytics-capable higher order components where their primary interaction emits a 'change' event.
 *
 * @param {Partial<AnalyticsData>} [defaultProps={}] defaults for analytics data that'll be common for this HOC.
 * @returns {AnalyticsComponentDecorator<P>} Factory for HOC that has analytics reporting capabilities and accepts an additional `analyticsData` property
 */
export function withChangeAnalytics<P>(defaultProps: Partial<AnalyticsData> = {}): (Component: React.ComponentType<P>) => React.ComponentClass<P & AnalyticsProps> {
  return withAnalytics<P>({ onChange: 'change' }, defaultProps);
}
