import * as bowser from 'bowser';

import analyticsWebClient from '@atlassian/analytics-web-client';

import { getConfig } from 'common/config';
import { RequestError } from 'common/error';

import { util as adminHubUtil } from '../../utilities/admin-hub';
import { AnalyticsData } from './with-analytics';
declare var newrelic: any;

export interface PageActionEvent {
  name: string;
  attributes: object;
}

interface ClientImpl {
  init(apiKey: string, options: AnalyticsData): void;
  onEvent(_, data: AnalyticsData): void;
  onError(error: Error): void;
  onPageAction(name: string, attributes: object): void;
}

let defaultImpl: ClientImpl;
if (__NODE_ENV__ === 'production') {
  const supportedBrowsersMinVersions = {
    chrome: '70',
    android: '70',
    firefox: '63',
    msie: '11',
    msedge: '17',
    safari: '12',
    ios: '11',
  };

  // if error handler function returns true (ie browser is not supported), new relic will ignore the error
  newrelic.setErrorHandler(() => {
    return bowser.isUnsupportedBrowser(supportedBrowsersMinVersions, true, window.navigator.userAgent);
  });

  defaultImpl = {
    init: analyticsWebClient.init.bind(analyticsWebClient),
    onEvent: analyticsWebClient.onEvent.bind(analyticsWebClient),
    onError: newrelic.noticeError.bind(newrelic),
    onPageAction: newrelic.addPageAction.bind(newrelic),
  };
} else if (__NODE_ENV__ === 'test') {
  defaultImpl = {
    init: () => void 0,
    onEvent: () => void 0,
    onError: () => void 0,
    onPageAction: () => void 0,
  };
} else {
  defaultImpl = {
    init: () => void 0,
    // tslint:disable-next-line:no-console
    onEvent: (_, data) => console.log(`[Analytics] ${JSON.stringify(data)}`),
    // tslint:disable-next-line:no-console
    onError: (error) => console.log(`[Analytics-Error] ${error.stack}`),
    // tslint:disable-next-line:no-console
    onPageAction: (name, attributes) => console.log(`[Analytics:page-action] ${name}: ${JSON.stringify(attributes)}`),
  };
}

export class AnalyticsClient {
  constructor(
    private impl: ClientImpl,
  ) {
    this.impl.init(getConfig().webAnalyticsConfig.segmentApiKey, {
      env: getConfig().analyticsClientEnvironment,
      product: adminHubUtil.isAdminHub() ? 'adminHub' : 'siteAdmin',
    } as AnalyticsData);
  }

  /**
   * Used internally. Do not report analytics through this event - use React wrappers instead.
   *
   * @param {any} data analytics data to report
   * @returns {void}
   */
  public onEvent(data: AnalyticsData): void {
    this.impl.onEvent(null, data);
  }

  public eventHandler = (event: AnalyticsData) => {
    this.onEvent(event);
  }

  public onPageLoad(): void {
    this.onEvent({
      subproduct: 'chrome',
      actionSubject: 'page',
      action: 'load',
      actionSubjectId: 'load',
      attributes: {
        referrer: document.referrer,
        pathname: window.location.href,
      },
    });
  }

  public onPageAction({ name, attributes }: PageActionEvent) {
    this.impl.onPageAction(name, attributes);
  }

  /**
   * Used for reporting errors that happen.
   *
   * @param {(Error | string)} error the error to report - can be a plain string or an exception. If you have an exception reference - use it, since we'll also get the stacktrace.
   * @returns {void}
   */
  public onError(error: Error | RequestError): void {
    if (error instanceof RequestError && (error.status === 401 || error.status === 403)) {
      return;
    }
    this.impl.onError(error);
  }
}
export const analyticsClient = new AnalyticsClient(defaultImpl);
