import { UserConnectedApp, VerificationType } from '../../schema/schema-types';
import { ConfigType } from '../../site/user-management/access-config/access-config-type';
import { Permissions } from '../../site/user-management/users/user-permission-section/permissions';
import { Action, ActionSubject, ScreenData, ScreenEvent, TrackAction, TrackActionSubject, TrackData, TrackEvent, UIData, UIEvent } from './new-analytics-types';

interface ScreenDataAttributesI18n {
  language: string;
  isI18nEnabled: boolean;
}

interface OriginIdGeneratedAttributes {
  originIdGenerated: string;
  originProduct: string;
}

interface OriginIdAttributes {
  originId: string;
  originProduct: string;
}

const siteAccessScreen = 'siteAccessScreen';

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14608
export const noPermissionScreenEvent = (): ScreenEvent => ({
  data: {
    name: 'noPermissionScreen',
  },
});

// https://data-portal.us-east-1.staging.public.atl-paas.net/eventcatalog/view/1434
export const selfSignUpSaveClickEventData = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'saveSelfSignupButton',
  source: siteAccessScreen,
});

// https://data-portal.us-east-1.staging.public.atl-paas.net/eventcatalog/view/1445
export const selfSignUpViewEventData = (): ScreenData => ({
  name: siteAccessScreen,
});

interface SelfSignUpTrackAttributes {
  isOpenInvite: boolean;
  restrictions: string;
  notify: boolean;
}

// https://data-portal.us-east-1.staging.public.atl-paas.net/eventcatalog/view/1435
export const selfSignUpTrackEventData = (attributes: SelfSignUpTrackAttributes): TrackData => ({
  action: TrackAction.Submitted,
  actionSubject: TrackActionSubject.Form,
  actionSubjectId: 'siteAccessForm',
  source: siteAccessScreen,
  attributes,
});

interface RemoveDomainClickAttributes {
  verified: boolean;
}

// https://data-portal.us-east-1.staging.public.atl-paas.net/eventcatalog/view/1689
export const removeDomainClickEventData = (attributes: RemoveDomainClickAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'removeDomainButton',
  source: 'domainClaimScreen',
  attributes,
});

export const launchpadScreen = 'launchpadScreen';

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14658
export const launchpadScreenEvent = (totalOrgCount: number, totalSiteCount: number): ScreenEvent => ({
  data: {
    name: launchpadScreen,
    attributes: {
      totalOrgCount,
      totalSiteCount,
    },
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14002
export const launchpadAddSiteEvent = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'addSiteButton',
  source: launchpadScreen,
});

export const manageOrgsScreen = 'manageOrgsScreen';

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14003
export const orgNameLinkClickEventData = (cardPosition: number, source: string): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Link,
  actionSubjectId: 'orgNameLink',
  source,
  attributes: {
    cardPosition,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14004
export const verifyDomainLinkClickEventData = (cardPosition: number, source: string): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Link,
  actionSubjectId: 'verifyDomainLink',
  source,
  attributes: {
    cardPosition,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14005
export const domainLinkClickEventData = (cardPosition: number, source: string): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Link,
  actionSubjectId: 'domainLink',
  source,
  attributes: {
    cardPosition,
  },
});

// https://data-portal.us-east-1.staging.public.atl-paas.net/eventcatalog/view/15196
export const verifyDomainTrackEventData = (orgId: string, method: VerificationType): TrackData => ({
  action: TrackAction.Verified,
  actionSubject: TrackActionSubject.Domain,
  actionSubjectId: orgId,
  source: 'verifyDomainModal',
  attributes: {
    method,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14006
export const managedAccountsLinkClickEventData = (cardPosition: number, source: string): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Link,
  actionSubjectId: 'managedAccountsLink',
  source,
  attributes: {
    cardPosition,
  },
});

export type AtlassianAccessLearnMoreScreen = 'orgOverviewScreen' | 'twoStepVerificationScreen' | 'passwordMgmtScreen' | 'samlScreen' | UserProvisioningScreen | 'memberDetailsApiTokensScreen';
export const orgOverviewScreen = 'orgOverviewScreen';
export type OrgOverviewScreenVariation = 'noDomainClaim' | 'domainNoAccess' | 'withAccess' | 'withSite';
export const twoStepVerificationScreen: AtlassianAccessLearnMoreScreen = 'twoStepVerificationScreen';
export const passwordMgmtScreen: AtlassianAccessLearnMoreScreen = 'passwordMgmtScreen';
export const samlScreen: AtlassianAccessLearnMoreScreen = 'samlScreen';

export type OrgNameFormSource = 'orgOverviewScreen';

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/13999
export const orgOverviewScreenEvent = (orgId: string, variation: OrgOverviewScreenVariation, linkedSitesCount?: number): ScreenEvent => ({
  orgId,
  data: {
    name: orgOverviewScreen,
    attributes: {
      variation,
      currentSiteCount: linkedSitesCount,
    },
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14007
export const siteCardLinkClickEventData = (siteId: string, source: string): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Link,
  actionSubjectId: 'siteLink',
  source,
  attributes: {
    siteId,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14842
export const siteCardManageUsersLink = (siteId: string, source: string, isLinkedSite?: boolean): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Link,
  actionSubjectId: 'siteManageUsersLink',
  source,
  attributes: {
    siteId,
    isLinkedSite,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14843
export const siteCardManageGroupsLink = (siteId: string, source: string, isLinkedSite?: boolean): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Link,
  actionSubjectId: 'siteManageGroupsDropdownLink',
  source,
  attributes: {
    siteId,
    isLinkedSite,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14844
export const siteCardGrantProductAccessLink = (siteId: string, source: string, isLinkedSite?: boolean): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Link,
  actionSubjectId: 'siteProductAccessDropdownLink',
  source,
  attributes: {
    siteId,
    isLinkedSite,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14845
export const siteCardViewBillingDropdownLink = (siteId: string, source: string, isLinkedSite?: boolean): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Link,
  actionSubjectId: 'siteViewBillingDropdownLink',
  source,
  attributes: {
    siteId,
    isLinkedSite,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14000
export const renameOrgButtonEventData = (source: OrgNameFormSource): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'renameOrgButton',
  source,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14001
export const renameOrgConfirmButtonEventData = (source: OrgNameFormSource): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'renameOrgConfirmButton',
  source,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13492
export const atlassianAccessLearnMoreEventData = (screen: AtlassianAccessLearnMoreScreen): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'learnMoreAccessButton',
  source: screen,
});

type UserProvisioningScreen = 'userProvisioningIntroScreen' | 'userProvisioningCreateTokenScreen' | 'userProvisioningRegenerateTokenScreen' | 'userProvisioningTokenDetailsScreen' | 'userProvisioningScreen';

export const userProvisioningIntroScreen: UserProvisioningScreen = 'userProvisioningIntroScreen';

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13376
export const userProvisioningIntroScreenEvent = (): ScreenEvent => ({
  data: {
    name: userProvisioningIntroScreen,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13380
export const userProvisioningIntroCreateDirectoryEventData = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'createTokenButton',
  source: userProvisioningIntroScreen,
});

const createTokenScreen: UserProvisioningScreen = 'userProvisioningCreateTokenScreen';

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13378
export const userProvisioningCreateDirectoryScreenEvent = (): ScreenEvent => ({
  data: {
    name: createTokenScreen,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13380
export const userProvisioningCreateDirectoryEventData = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'createTokenButton',
  source: createTokenScreen,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13381
export const userProvisioningCancelCreateDirectoryEventData = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'cancelCreateTokenButton',
  source: createTokenScreen,
});

const regenerateTokenScreen: UserProvisioningScreen = 'userProvisioningRegenerateTokenScreen';

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/13865
export const userProvisioningRegenerateApiKeyScreenEvent = (): ScreenEvent => ({
  data: {
    name: regenerateTokenScreen,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/13866
export const userProvisioningRegenerateApiKeyEventData = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'regenerateTokenButton',
  source: regenerateTokenScreen,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/13867
export const userProvisioningCancelRegenerateApiKeyEventData = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'cancelRegenerateTokenButton',
  source: regenerateTokenScreen,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/13868
export const scimApiKeyRegeneratedTrackData = (): TrackData => ({
  action: TrackAction.Regenerated,
  actionSubject: TrackActionSubject.Token,
  actionSubjectId: 'scimTokenRegeneration',
  source: regenerateTokenScreen,
});

const userProvisioningTokenDetailsScreen: UserProvisioningScreen = 'userProvisioningTokenDetailsScreen';

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13379
export const userProvisioningApiKeyDetailsScreenEvent = (): ScreenEvent => ({
  data: {
    name: userProvisioningTokenDetailsScreen,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13382
export const userProvisioningFinishCreateDirectoryEventData = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'finishTokenCreationButton',
  source: userProvisioningTokenDetailsScreen,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13383
export const userProvisioningCopyApiKeyEventData = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'copyTokenButton',
  source: userProvisioningTokenDetailsScreen,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13384
export const userProvisioningCopyBaseUrlEventData = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'copyBaseUrlButton',
  source: userProvisioningTokenDetailsScreen,
});

const userProvisioningScreen: UserProvisioningScreen = 'userProvisioningScreen';

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13448
export const userProvisioningAddSiteEventData = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Link,
  actionSubjectId: 'addSiteBannerLink',
  source: userProvisioningScreen,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13377
export const userProvisioningScreenEvent = ({ orgId, syncedUserCount, syncedGroupCount, siteCount }): ScreenEvent => ({
  orgId,
  data: {
    name: userProvisioningScreen,
    attributes: {
      syncedUserCount,
      syncedGroupCount,
      siteCount,
    },
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13385
export const userProvisioningGroupsLink = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Link,
  actionSubjectId: 'viewSyncedGroupsLink',
  source: userProvisioningScreen,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13386
export const userProvisioningDirectoryLink = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Link,
  actionSubjectId: 'viewTokenLink',
  source: userProvisioningScreen,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13387
export const userProvisioningAuditLogLink = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Link,
  actionSubjectId: 'viewAuditLogLink',
  source: userProvisioningScreen,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/13927
export const userProvisioningProductAccessLink = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Link,
  actionSubjectId: 'viewProductAccessLink',
  source: userProvisioningScreen,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/13928
export const userProvisioningProductAccessAddSiteEventData = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Link,
  actionSubjectId: 'addSiteProductAccessLink',
  source: userProvisioningScreen,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13388
export const userProvisioningRemoveDirectoryButton = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'revokeTokenButton',
  source: userProvisioningScreen,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13389
export const scimDirectoryCreatedTrackData = (): TrackData => ({
  action: TrackAction.Created,
  actionSubject: TrackActionSubject.Token,
  actionSubjectId: 'scimTokenCreation',
  source: createTokenScreen,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13390
export const scimDirectoryRemovedTrackData = (): TrackData => ({
  action: TrackAction.Created,
  actionSubject: TrackActionSubject.Token,
  actionSubjectId: 'scimTokenRevoked',
  source: userProvisioningScreen,
});

// TODO add event catalog link
export const csatSurveyLinkEventData = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Link,
  actionSubjectId: 'csatSurveyLink',
  source: 'csatSurveyScreen',
});

// TODO add event catalog link
export const csatSurveyDismissedEventData = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'csatSurveyClose',
  source: 'csatSurveyScreen',
});

// https://data-portal.us-east-1.staging.public.atl-paas.net/eventcatalog/view/1880
export const siteAdminSidebarViewEventData = (): ScreenData => ({
  name: 'siteAdminSidebar',
});

const appSwitcherSharedProps = {
  attributes: {
    packageName: '@atlaskit/app-switcher',
    packageVersion: '7.0.0',
    componentName: 'AkAppSwitcher',
  },
};

// https://data-portal.us-east-1.staging.public.atl-paas.net/eventcatalog/view/1890
export const appSwitcherDropdownViewEventData = (): ScreenData => ({
  name: 'appSwitcherDropdown',
  ...appSwitcherSharedProps,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13392
export const groupsScreenScreenEvent = ({ groupCount }): ScreenEvent => ({
  data: {
    name: 'groupsScreen',
    attributes: {
      groupCount,
    },
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/14150
export const gSuiteScreenScreenEvent = (): ScreenEvent => ({
  data: {
    name: 'gSuiteScreen',
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13393
export const createGroupModalScreenEvent = (): ScreenEvent => ({
  data: {
    name: 'createGroupModal',
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13394
export const addMembersToGroupModalScreenEvent = ({ groupId }): ScreenEvent => ({
  data: {
    name: 'addMembersToGroupModal',
    attributes: {
      groupId,
    },
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13395
export const groupDetailsScreenScreenEvent = ({ groupId, memberCount }): ScreenEvent => ({
  data: {
    name: 'groupDetailsScreen',
    attributes: {
      groupId,
      memberCount,
    },
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13396
export const editGroupDescriptionModalScreenEvent = ({ groupId, memberCount }): ScreenEvent => ({
  data: {
    name: 'editGroupDescriptionModal',
    attributes: {
      groupId,
      memberCount,
    },
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13397
export const deleteGroupModalScreenEvent = ({ groupId, memberCount }): ScreenEvent => ({
  data: {
    name: 'deleteGroupModal',
    attributes: {
      groupId,
      memberCount,
    },
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13398
export const createGroupTrackEventData = (
  { groupId, description, memberCheckBox }:
    { groupId: string, description: boolean, memberCheckBox: boolean },
): TrackData => ({
  action: TrackAction.Created,
  actionSubject: TrackActionSubject.Group,
  actionSubjectId: groupId,
  source: 'groupsScreen',
  attributes: {

    description,
    memberCheckBox,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13399
export const addMemberTrackEventData = (
  { source, groupId, memberCountAdded }:
    { source: 'groupsScreen' | 'groupsDetailsScreen', groupId: string, memberCountAdded: number },
): TrackData => ({
  action: TrackAction.Added,
  actionSubject: TrackActionSubject.Member,
  actionSubjectId: groupId,
  source,
  attributes: {
    memberCountAdded,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13400
export const deletedGroupTrackEventData = (
  { groupId, memberCount }:
    { groupId: string, memberCount: number },
): TrackData => ({
  action: TrackAction.Deleted,
  source: 'groupsScreen',
  actionSubject: TrackActionSubject.Group,
  actionSubjectId: groupId,
  attributes: {
    memberCount,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13401
export const createGroupButtonClickedEventData = (
  { groupCount }:
    { groupCount?: number },
): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'createGroupButton',
  source: 'groupsScreen',
  attributes: {
    groupCount,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13402
export const groupListLearnMoreLinkClickedEventData = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Link,
  actionSubjectId: 'learnMoreLink',
  source: 'groupsScreen',
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13403?
export const groupNameLinkClickedEventData = (
  { groupId, groupCount, userCount, defaultAccess, productAdmin, hasSearchQuery, external }:
    { groupId: string, groupCount?: number, userCount: number, defaultAccess: boolean, productAdmin: boolean, hasSearchQuery: boolean, external: boolean },
): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Link,
  actionSubjectId: 'groupNameLink',
  source: 'groupsScreen',
  attributes: {
    groupId,
    groupCount,
    userCount,
    defaultAccess,
    productAdmin,
    hasSearchQuery,
    external,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13404
export const groupListSearchClickedEventData = (
  { groupCount }:
    { groupCount?: number },
): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Field,
  actionSubjectId: 'groupSearchField',
  source: 'groupsScreen',
  attributes: {
    groupCount,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13447
export const groupListSearchTypedEventData = (
  { groupCount, keyWord }:
    { groupCount?: number, keyWord: string },
): UIData => ({
  action: Action.Searched,
  actionSubject: ActionSubject.Field,
  actionSubjectId: 'groupSearchField',
  source: 'groupsScreen',
  attributes: {
    groupCount,
    keyWord,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13406
export const createGroupConfirmButtonClickedEventData = (
  { groupId, description, memberCheckBox }:
    { groupId: string, description: boolean, memberCheckBox: boolean },
): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'createGroupConfirmButton',
  source: 'groupsScreen',
  attributes: {
    groupId,
    description,
    memberCheckBox,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13407
export const createGroupCancelButtonClickedEventData = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'createGroupCancelButton',
  source: 'groupsScreen',
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13408
export const addMembersConfirmButtonClickedEventData = (
  { groupId, memberCount, membersAddedCount }:
    { groupId: string, memberCount: number, membersAddedCount: number },
): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'addMemberToGroupConfirm',
  source: 'groupDetailsScreen',
  attributes: {
    groupId,
    memberCount,
    membersAddedCount,
    newTotalMemberCount: memberCount + membersAddedCount,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13409
export const addMembersCancelButtonClickedEventData = (
  { groupId, memberCount }:
    { groupId: string, memberCount: number },
): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'addMemberToGroupCancel',
  source: 'groupsScreen',
  attributes: {
    groupId,
    memberCount,
  },
});

interface GroupDetailButtonAttributes {
  groupId: string;
  memberCount: number;
}

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13410
export const editGroupDescriptionButtonClickedEventData = (
  { groupId, memberCount }:
    GroupDetailButtonAttributes,
): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'editGroupDescriptionButton',
  source: 'groupDetailsScreen',
  attributes: {
    groupId,
    memberCount,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13411
export const addMembersButtonClickedEventData = (
  { groupId, memberCount }:
    GroupDetailButtonAttributes,
): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'addMembersButton',
  source: 'groupDetailsScreen',
  attributes: {
    groupId,
    memberCount,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13412
export const deleteGroupButtonClickedEventData = (
  { groupId, memberCount }:
    GroupDetailButtonAttributes,
): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'deleteGroupButton',
  source: 'groupDetailsScreen',
  attributes: {
    groupId,
    memberCount,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13413
export const editGroupAccessLinkClickedEventData = (
  { groupId, memberCount }:
    GroupDetailButtonAttributes,
): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Link,
  actionSubjectId: 'editGroupAccessLink',
  source: 'groupDetailsScreen',
  attributes: {
    groupId,
    memberCount,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13414
export const removeMemberLinkClickedEventData = (
  { groupId, memberCount }:
    GroupDetailButtonAttributes,
): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Link,
  actionSubjectId: 'removeMemberLink',
  source: 'groupDetailsScreen',
  attributes: {
    groupId,
    memberCount,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13415
export const deleteGroupConfirmButtonEventData = (
  { groupId, memberCount }:
    GroupDetailButtonAttributes,
): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'deleteGroupConfirmButton',
  source: 'groupDetailsScreen',
  attributes: {
    groupId,
    memberCount,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13416
export const deleteGroupCancelButtonEventData = (
  { groupId, memberCount }:
    GroupDetailButtonAttributes,
): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'deleteGroupCancelButton',
  source: 'groupDetailsScreen',
  attributes: {
    groupId,
    memberCount,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13417
export const editGroupDescriptionConfirmButtonEventData = (
  { groupId, memberCount }:
    GroupDetailButtonAttributes,
): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'editGroupDescriptionConfirmButton',
  source: 'groupDetailsScreen',
  attributes: {
    groupId,
    memberCount,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/eventcatalog/view/13418
export const editGroupDescriptionCancelButtonEventData = (
  { groupId, memberCount }:
    GroupDetailButtonAttributes,
): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'editGroupDescriptionCancelButton',
  source: 'groupDetailsScreen',
  attributes: {
    groupId,
    memberCount,
  },
});

// https://data-portal.us-east-1.staging.public.atl-paas.net/eventcatalog/view/1891
export const appSwitcherDropdownClosedEventData = (): UIData => ({
  action: Action.Closed,
  actionSubject: ActionSubject.AppSwitcherDropdown,
  actionSubjectId: 'appSwitcherIcon',
  source: 'siteAdminScreen',
  ...appSwitcherSharedProps,
});

// https://data-portal.us-east-1.staging.public.atl-paas.net/eventcatalog/view/1777
export const i18nScreenEvent = (attributes: ScreenDataAttributesI18n): ScreenEvent => ({
  data: {
    name: 'i18nProvider',
    attributes,
  },
});

// https://data-portal.us-east-1.staging.public.atl-paas.net/eventcatalog/view/2002
export const accessOrgMovedScreenEvent = (): ScreenEvent => ({
  data: {
    name: 'accessOrgMovedScreen',
  },
});

// https://data-portal.us-east-1.staging.public.atl-paas.net/eventcatalog/view/2003
export const accessNoOrgScreenEvent = (isNewLozengeShown: boolean, cloudId?: string): ScreenEvent => ({
  cloudId,
  data: {
    name: 'accessNoOrgScreen',
    attributes: {
      isNewLozengeShown,
    },
  },
});

// TODO add event catalog link - currently they cannot be distinguished by actionSubjectIds
export const atlassianAccessPricingUIEventData = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Link,
  actionSubjectId: 'learnAboutAtlassianAccessPricingLink',
  source: 'accessNoOrgScreen',
});

// TODO add event catalog link - currently they cannot be distinguished by actionSubjectIds
export const continueToAccessUIEventData = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'continueToAccess',
  source: 'accessNoOrgScreen',
});

// TODO add event catalog link - currently they cannot be distinguished by actionSubjectIds
export const learnAboutAccessUIEventData = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Link,
  actionSubjectId: 'learnAboutAccessLink',
  source: 'accessNoOrgScreen',
});

const managedAccountsDownloadScreen = 'managedAccountsDownloadScreen';

// https://data-portal.us-east-1.staging.public.atl-paas.net/eventcatalog/view/2201
export const managedAccountsDownloadScreenData = (): ScreenEvent => ({
  data: {
    name: managedAccountsDownloadScreen,
  },
});

const managedAccountsDownloadErrorScreen = 'managedAccountsDownloadErrorScreen';

// https://data-portal.us-east-1.staging.public.atl-paas.net/eventcatalog/view/13605
export const managedAccountsDownloadErrorScreenData = (): ScreenEvent => ({
  data: {
    name: managedAccountsDownloadErrorScreen,
  },
});

const managedAccountsDownloadExpiredScreen = 'managedAccountsDownloadExpiredScreen';

// https://data-portal.us-east-1.staging.public.atl-paas.net/eventcatalog/view/13606
export const managedAccountsDownloadExpiredScreenData = (): ScreenEvent => ({
  data: {
    name: managedAccountsDownloadExpiredScreen,
  },
});

const managedAccountsScreen = 'managedAccountsScreen';

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15090
export const managedAccountsScreenEvent = (): ScreenEvent => ({
  data: {
    name: managedAccountsScreen,
  },
});

// https://data-portal.us-east-1.staging.public.atl-paas.net/eventcatalog/view/2202
export const initiateManagedAcountsExportData = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'initiateManagedAcountsExport',
  source: managedAccountsScreen,
});

// https://data-portal.us-east-1.staging.public.atl-paas.net/eventcatalog/view/2203
export const downloadManagedAccountsCsvData = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'downloadManagedAccountsCsv',
  source: managedAccountsDownloadScreen,
});

export interface ManagedAccountsScreenCommonAttributes {
  /**
   * Captures the number of managed accounts displayed on the current screen for the admin, e.g. the number of users returned after search or filter is applied.
   */
  managedUsersCountOnScreen: number;
  /**
   * The total count of managed accounts for an organization.
   */
  totalManagedUsersCount: number;
}

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14582
export const editAccountLinkClickEventData = (attributes: ManagedAccountsScreenCommonAttributes & {
  /**
   * The number of page the admin is on
   */
  page: number;
  /**
   * The position of the user on the page
   */
  numberOnPage: number;
  /**
   * Whether the clicked account is a fuzzy match or exact match based on the search term last seen
   */
  fuzzyResults: boolean;
}): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Link,
  actionSubjectId: 'editAccountLink',
  source: managedAccountsScreen,
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14583
export const managedAccountsSearchFieldClickEventData = (attributes: ManagedAccountsScreenCommonAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Field,
  actionSubjectId: 'searchField',
  source: managedAccountsScreen,
  attributes,
});

const accessRequestsScreen = 'accessRequestSiteAdminScreen';
const denyAccessRequestModalScreen = 'denyAccessRequestModalScreen';

export interface AccessRequestAttributes {
  requesterUserId: string;
  requestedForUserId: string;
  status: string;
  requestedProduct: string;
  requestedTime: string;
  type: string;
  hasVerifiedEmail: boolean;
  subProducts?: string;
}

interface DenialReasonLengthAttribute {
  denialReasonLength: number;
}

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/13979
export const accessRequestsViewData = (attributes?: OriginIdAttributes): ScreenData => ({
  name: accessRequestsScreen,
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/13980
export const denyAccessRequestModalViewData = (attributes: AccessRequestAttributes): ScreenData => ({
  name: denyAccessRequestModalScreen,
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15415
export const denyAccessTrackData = (attributes: AccessRequestAttributes & DenialReasonLengthAttribute): TrackData => ({
  action: TrackAction.Denied,
  actionSubject: TrackActionSubject.ProductAccess,
  actionSubjectId: attributes.requesterUserId,
  source: accessRequestsScreen,
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15414
export const approveAccessTrackData = (attributes: AccessRequestAttributes & OriginIdGeneratedAttributes): TrackData => ({
  action: TrackAction.Approved,
  actionSubject: TrackActionSubject.ProductAccess,
  actionSubjectId: attributes.requesterUserId,
  source: accessRequestsScreen,
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/13981
export const approveAccessUIData = (attributes: AccessRequestAttributes & OriginIdGeneratedAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'approveUserAccessRequestButton',
  source: accessRequestsScreen,
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/13982
export const denyAccessUIData = (attributes: AccessRequestAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'denyUserAccessRequestButton',
  source: accessRequestsScreen,
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/13983
export const denyAccessConfirmUIData = (attributes: AccessRequestAttributes & DenialReasonLengthAttribute): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'confirmDenyUserAccessRequestButton',
  source: denyAccessRequestModalScreen,
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/13984
export const showProfileDropDownUIEventData = (attributes: AccessRequestAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'accessRequestViewUserProfileButton',
  source: accessRequestsScreen,
  attributes,
});

// https://data-portal.us-east-1.staging.public.atl-paas.net/eventcatalog/view/13792
export const addSiteToOrgModalScreenEvent = (attributes: { siteCount: number | undefined }): ScreenEvent => ({
  data: {
    name: 'addSiteToOrgModal',
    attributes,
  },
});

// https://data-portal.us-east-1.staging.public.atl-paas.net/eventcatalog/view/13793
export const addSiteToOrgSummaryModalScreenEvent = (attributes: { siteId: string, orgAdminCount: number | undefined }): ScreenEvent => ({
  data: {
    name: 'addSiteToOrgSummaryModal',
    attributes,
  },
});

// https://data-portal.us-east-1.staging.public.atl-paas.net/eventcatalog/view/13794
export const siteAddedSuccessModalScreenEvent = (attributes: { siteId: string }): ScreenEvent => ({
  data: {
    name: 'siteAddedSuccessModal',
    attributes,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14002
export const orgOverviewAddSiteEventData = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'addSiteButton',
  source: orgOverviewScreen,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/13803
export const stridePageScreen = 'strideExportTableScreen';
export const stridePageScreenEvent = (): ScreenEvent => ({
  data: {
    name: stridePageScreen,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/13804
export const strideExportDetailScreen = 'strideExportDetailScreen';
export const strideExportDetailScreenScreenEvent = (): ScreenEvent => ({
  data: {
    name: strideExportDetailScreen,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/13805
export const strideCreateExportDialogScreen = 'strideExportCreateModal';
export const strideCreateExportDialogScreenEvent = (): ScreenEvent => ({
  data: {
    name: strideCreateExportDialogScreen,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/13806
export const strideDeleteExportDialogScreen = 'strideExportDeleteModal';
export const strideDeleteExportDialogScreenEvent = (): ScreenEvent => ({
  data: {
    name: strideDeleteExportDialogScreen,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/13807
export const strideCancelExportDialogScreen = 'strideExportCancelModal';
export const strideCancelExportDialogScreenEvent = (): ScreenEvent => ({
  data: {
    name: strideCancelExportDialogScreen,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/13848
export const strideCreateExportButtonClickEvent = (): UIEvent => ({
  data: {
    action: Action.Clicked,
    actionSubject: ActionSubject.Button,
    actionSubjectId: 'createExportButton',
    source: stridePageScreen,
    objectType: 'export',
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/13849
export const strideDeleteExportButtonClickEvent = (exportId: string): UIEvent => ({
  data: {
    action: Action.Clicked,
    actionSubject: ActionSubject.Button,
    actionSubjectId: 'deleteExportButton',
    source: stridePageScreen,
    objectType: 'export',
    objectId: exportId,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/13850
export const strideCancelExportButtonClickEvent = (exportId: string): UIEvent => ({
  data: {
    action: Action.Clicked,
    actionSubject: ActionSubject.Button,
    actionSubjectId: 'cancelExportButton',
    source: stridePageScreen,
    objectType: 'export',
    objectId: exportId,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/13845
export const strideDownloadExportButtonClickEvent = (exportId: string, source: string): UIEvent => ({
  data: {
    action: Action.Clicked,
    actionSubject: ActionSubject.Button,
    actionSubjectId: 'downloadExportButton',
    source,
    objectType: 'export',
    objectId: exportId,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/13864
export const strideCopyExportButtonClickEvent = (exportId: string, source: string): UIEvent => ({
  data: {
    action: Action.Clicked,
    actionSubject: ActionSubject.Button,
    actionSubjectId: 'copyExportButton',
    source,
    objectType: 'export',
    objectId: exportId,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14130
export const strideDownloadAppInstallsButtonClickEvent = (exportId: string, source: string): UIEvent => ({
  data: {
    action: Action.Clicked,
    actionSubject: ActionSubject.Button,
    actionSubjectId: 'downloadAppInstallsButton',
    source,
    objectType: 'export',
    objectId: exportId,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/13838
export const strideExportDetailsLinkClickEvent = (exportId: string): UIEvent => ({
  data: {
    action: Action.Clicked,
    actionSubject: ActionSubject.Link,
    actionSubjectId: 'exportDetailsLink',
    source: stridePageScreen,
    objectType: 'export',
    objectId: exportId,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/13843
export const strideSubmitCreateExportButtonClickEvent = (): UIEvent => ({
  data: {
    action: Action.Clicked,
    actionSubject: ActionSubject.Button,
    actionSubjectId: 'submitCreateExportButton',
    source: strideCreateExportDialogScreen,
    objectType: 'export',
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/13844
export const strideCancelCreateExportButtonClickEvent = (): UIEvent => ({
  data: {
    action: Action.Clicked,
    actionSubject: ActionSubject.Button,
    actionSubjectId: 'cancelCreateExportButton',
    source: strideCreateExportDialogScreen,
    objectType: 'export',
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/13839
export const strideConfirmDeleteExportButtonClickEvent = (exportId: string): UIEvent => ({
  data: {
    action: Action.Clicked,
    actionSubject: ActionSubject.Button,
    actionSubjectId: 'confirmDeleteExportButton',
    source: strideDeleteExportDialogScreen,
    objectType: 'export',
    objectId: exportId,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/13847
export const strideCancelDeleteExportButtonClickEvent = (exportId: string): UIEvent => ({
  data: {
    action: Action.Clicked,
    actionSubject: ActionSubject.Button,
    actionSubjectId: 'cancelDeleteExportButton',
    source: strideDeleteExportDialogScreen,
    objectType: 'export',
    objectId: exportId,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/13841
export const strideConfirmCancelExportButtonClickEvent = (exportId: string): UIEvent => ({
  data: {
    action: Action.Clicked,
    actionSubject: ActionSubject.Button,
    actionSubjectId: 'confirmCancelExportButton',
    source: strideCancelExportDialogScreen,
    objectType: 'export',
    objectId: exportId,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/13842
export const strideCancelCancelExportButtonClickEvent = (exportId: string): UIEvent => ({
  data: {
    action: Action.Clicked,
    actionSubject: ActionSubject.Button,
    actionSubjectId: 'cancelCancelExportButton',
    source: strideCancelExportDialogScreen,
    objectType: 'export',
    objectId: exportId,
  },
});

// https://data-portal.us-east-1.staging.public.atl-paas.net/eventcatalog/view/13960
export const nextButton = (attributes: { currentSiteCount: number | undefined, currentOrgCount: number | undefined },
): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'nextButton',
  source: 'addSiteToOrgModal',
  attributes,
});

// https://data-portal.us-east-1.staging.public.atl-paas.net/eventcatalog/view/13963
export const closeIcon = (attributes: { currentSiteCount: number | undefined, currentOrgCount: number | undefined },
): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'closeIcon',
  source: 'orgSiteLinkingDrawer',
  attributes,
});

// https://data-portal.us-east-1.staging.public.atl-paas.net/eventcatalog/view/13964
export const completeButton = (attributes: { currentSiteCount: number | undefined, currentOrgAdminCount: number | undefined, currentOrgCount: number | undefined },
): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'completeButton',
  source: 'addSiteToOrgSummaryModal',
  attributes,
});

// https://data-portal.us-east-1.staging.public.atl-paas.net/eventcatalog/view/13965
export const backButton = (attributes: { currentSiteCount: number | undefined, siteId: string, siteAdminCount: number | undefined, currentOrgAdminCount: number | undefined },
): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'backButton',
  source: 'addSiteToOrgSummaryModal',
  attributes,
});

// https://data-portal.us-east-1.staging.public.atl-paas.net/eventcatalog/view/13966
export const siteAddedSuccessCloseButton = (attributes: { currentSiteCount: number | undefined, siteId: string, currentOrgCount: number | undefined },
): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'siteAddedSuccessCloseButton',
  source: 'siteAddedSuccessScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/13967
export const linkSiteToOrgTrackData = (attributes: { currentSiteCount: number | undefined, siteId: string, currentOrgCount: number | undefined },
): TrackData => ({
  action: TrackAction.Added,
  actionSubject: TrackActionSubject.ProductAccess,
  actionSubjectId: 'addSiteProductAccessLink',
  source: 'siteAddedSuccessScreen',
  attributes,
});

export const userConnectedAppsScreen = 'userConnectedAppsScreen';

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14102
export const userConnectedAppsScreenEvent = (cloudId: string, appCount: number): ScreenEvent => ({
  cloudId,
  data: {
    name: userConnectedAppsScreen,
    attributes: {
      appCount,
    },
  },
});

export type FocusedTaskUIEventMeta = Partial<Pick<UIEvent, 'cloudId' | 'orgId'> & { data: Pick<UIEvent['data'], 'source' | 'attributes'> }>;

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14249
export const focusedTaskCloseButtonClickEvent = (meta?: FocusedTaskUIEventMeta): UIEvent => ({
  ...meta,
  data: {
    ...(meta && meta.data),
    action: Action.Clicked,
    actionSubject: ActionSubject.NavigationItem,
    actionSubjectId: 'closeIcon',
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14250
export const focusedTaskLogoButtonClickEvent = (meta?: FocusedTaskUIEventMeta): UIEvent => ({
  ...meta,
  data: {
    ...(meta && meta.data),
    action: Action.Clicked,
    actionSubject: ActionSubject.NavigationItem,
    actionSubjectId: 'productLogo',
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14251
export const orgCreatedTrackEvent = (orgId: string): TrackEvent => ({
  orgId,
  data: {
    action: TrackAction.Created,
    actionSubject: TrackActionSubject.Organization,
    actionSubjectId: orgId,
    source: createOrgScreen,
  },
});

export const createOrgScreen = 'createOrgScreen';

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14252
export const confirmOrgCreationButtonClickedEvent = (): UIEvent => ({
  data: {
    action: Action.Clicked,
    actionSubject: ActionSubject.Button,
    actionSubjectId: 'confirmCreateOrgButton',
    source: createOrgScreen,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14253
export const cancelOrgCreationButtonClickedEvent = (): UIEvent => ({
  data: {
    action: Action.Clicked,
    actionSubject: ActionSubject.Button,
    actionSubjectId: 'cancelOrgButton',
    source: createOrgScreen,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15136
export const createOrgButtonClickedEvent = (): UIEvent => ({
  data: {
    action: Action.Clicked,
    actionSubject: ActionSubject.Button,
    actionSubjectId: 'createOrgButton',
    source: 'accessOnboardingScreen',
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15135
export const learnMoreAccessLinkClickedEvent = (): UIEvent => ({
  data: {
    action: Action.Clicked,
    actionSubject: ActionSubject.Link,
    actionSubjectId: 'learnMoreAccessLink',
    source: 'accessOnboardingScreen',
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15137
export const learnMoreDomainLinkClickedEvent = (): UIEvent => ({
  data: {
    action: Action.Clicked,
    actionSubject: ActionSubject.Link,
    actionSubjectId: 'learnMoreDomainLink',
    source: 'accessOnboardingScreen',
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15138
export const findOutMoreLinkClickedEvent = (): UIEvent => ({
  data: {
    action: Action.Clicked,
    actionSubject: ActionSubject.Link,
    actionSubjectId: 'findOutMoreLink',
    source: 'learnMoreAccess',
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15139
export const learnPricingLinkClickedEvent = (): UIEvent => ({
  data: {
    action: Action.Clicked,
    actionSubject: ActionSubject.Link,
    actionSubjectId: 'learnPricingLink',
    source: 'learnMoreAccess',
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15140
export const tryFreeButtonClickedEvent = (): UIEvent => ({
  data: {
    action: Action.Clicked,
    actionSubject: ActionSubject.Button,
    actionSubjectId: 'tryFreeButton',
    source: 'learnMoreAccess',
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14944
export const orgAdminScreenEvent = (): ScreenEvent => ({
  data: {
    name: 'orgAdminScreen',
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15083
export const domainScreenEvent = (): ScreenEvent => ({
  data: {
    name: 'domainsScreen',
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15085
export const createOrgScreenEvent = (): ScreenEvent => ({
  data: {
    name: 'createOrgScreen',
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15086
export const samlScreenEvent = (hasIdentityManager: boolean, hasDomainsVerified: boolean): ScreenEvent => ({
  data: {
    name: samlScreen,
    attributes: {
      hasIdentityManager,
      hasDomainsVerified,
    },
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15087
export const passwordMgmtScreenEvent = (hasIdentityManager: boolean, hasDomainsVerified: boolean): ScreenEvent => ({
  data: {
    name: passwordMgmtScreen,
    attributes: {
      hasIdentityManager,
      hasDomainsVerified,
    },
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15089
export const twoStepVerificationScreenEvent = (hasIdentityManager: boolean, hasDomainsVerified: boolean): ScreenEvent => ({
  data: {
    name: twoStepVerificationScreen,
    attributes: {
      hasIdentityManager,
      hasDomainsVerified,
    },
  },
});

// <Admin API>

export const adminApiKeysScreen = 'adminApiKeysScreen';

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14178
export const apiKeysScreenEvent = (): ScreenEvent => ({
  data: {
    name: adminApiKeysScreen,
  },
});

export const createAdminApiKeyDrawerEventMeta: FocusedTaskUIEventMeta = {
  data: {
    source: adminApiKeysScreen,
  },
};

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14333
export const adminApiOpenCreateAdminApiKeyDrawerEventData = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'openCreateAdminApiKeyDrawerButton',
  source: adminApiKeysScreen,
});

const adminApiCreateKeyScreen = 'adminApiCreateKeyScreen';

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14325
export const adminApiCreateKeyScreenEvent = (): ScreenEvent => ({
  data: {
    name: adminApiCreateKeyScreen,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14326
export const adminApiCancelCreateAdminApiKeyEventData = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'cancelCreateAdminApiKeyButton',
  source: adminApiCreateKeyScreen,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14327
export const adminApiCreateAdminApiKeyEventData = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'createAdminApiKeyButton',
  source: adminApiCreateKeyScreen,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14328
export const adminApiKeyCreatedTrackData = (): TrackData => ({
  action: TrackAction.Created,
  actionSubject: TrackActionSubject.AdminApiKey,
  actionSubjectId: 'adminApiKeyCreation',
  source: adminApiCreateKeyScreen,
});

const adminApiKeyDetailsScreen = 'adminApiKeyDetailsScreen';

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14329
export const adminApiApiKeyDetailsScreenEvent = (): ScreenEvent => ({
  data: {
    name: adminApiKeyDetailsScreen,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14330
export const adminApiCopyApiKeyEventData = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'copyAdminApiKeyButton',
  source: adminApiKeyDetailsScreen,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14331
export const adminApiCopyOrgIdEventData = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'copyAdminApiOrgIdButton',
  source: adminApiKeyDetailsScreen,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14332
export const adminApiFinishCreateApiKeyEventData = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'finishAdminApiKeyCreationButton',
  source: adminApiKeyDetailsScreen,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14374
export const adminApiRevokeApiKeyEventData = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'revokeAdminApiKeyButton',
  source: adminApiKeysScreen,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14373
export const adminApiKeyRevokedTrackData = (): TrackData => ({
  action: TrackAction.Deleted,
  actionSubject: TrackActionSubject.AdminApiKey,
  actionSubjectId: 'adminApiKeyRevoked',
  source: adminApiKeysScreen,
});

// </Admin API>

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14152
export const userConnectedAppsDetailsLinkClickEvent = (cloudId: string, oAuthClientId: string): UIEvent => ({
  cloudId,
  data: {
    action: Action.Clicked,
    actionSubject: ActionSubject.Link,
    actionSubjectId: 'appDetailsLink',
    source: userConnectedAppsScreen,
    attributes: {
      oAuthClientId,
    },
  },
});

export const userConnectedAppDetailsScreen = 'userConnectedAppDetailsScreen';

export interface UserConnectedAppDetailsData {
  oAuthClientId: UserConnectedApp['id'];
  appName: UserConnectedApp['name'];
  appVendor?: UserConnectedApp['vendorName'];
}

export interface UserConnectedAppDetailsShowGrantDetailsLinkData {
  accountId: string;
  grantDate: string;
  isGrantScopesStandard: boolean;
}

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14151
export const userConnectedAppDetailsScreenEvent = (
  cloudId: string,
  userCount: number,
  { oAuthClientId, appName, appVendor }: UserConnectedAppDetailsData,
): ScreenEvent => ({
  cloudId,
  data: {
    name: userConnectedAppDetailsScreen,
    attributes: {
      oAuthClientId,
      appName,
      appVendor,
      userCount,
    },
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14153
export const userConnectedAppBackToAllAppsLinkClickEvent = (
  cloudId: string,
  { oAuthClientId, appName, appVendor }: UserConnectedAppDetailsData,
): UIEvent => ({
  cloudId,
  data: {
    action: Action.Clicked,
    actionSubject: ActionSubject.Link,
    actionSubjectId: 'backToAllAppsLink',
    objectType: 'app',
    objectId: oAuthClientId,
    source: userConnectedAppDetailsScreen,
    attributes: {
      appName,
      appVendor,
    },
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14225
export const userConnectedAppDetailsShowGrantDetailsLinkClickEvent = (
  cloudId: string,
  { oAuthClientId, appName, appVendor }: UserConnectedAppDetailsData,
  { accountId, grantDate, isGrantScopesStandard }: UserConnectedAppDetailsShowGrantDetailsLinkData,
): UIEvent => ({
  cloudId,
  data: {
    action: Action.Clicked,
    actionSubject: ActionSubject.Link,
    actionSubjectId: 'showGrantDetailsLink',
    objectType: 'app',
    objectId: oAuthClientId,
    source: userConnectedAppDetailsScreen,
    attributes: {
      appName,
      appVendor,
      accountId,
      grantDate,
      isGrantScopesStandard,
    },
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14226
export const userConnectedAppDetailsHideGrantDetailsLinkClickEvent = (
  cloudId: string,
  { oAuthClientId, appName, appVendor }: UserConnectedAppDetailsData,
  { accountId, grantDate, isGrantScopesStandard }: UserConnectedAppDetailsShowGrantDetailsLinkData,
): UIEvent => ({
  cloudId,
  data: {
    action: Action.Clicked,
    actionSubject: ActionSubject.Link,
    actionSubjectId: 'hideGrantDetailsLink',
    objectType: 'app',
    objectId: oAuthClientId,
    source: userConnectedAppDetailsScreen,
    attributes: {
      appName,
      appVendor,
      accountId,
      grantDate,
      isGrantScopesStandard,
    },
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14600
export const cancelMemberDeletionModalScreenEvent = (): ScreenEvent => ({
  data: {
    name: 'cancelMemberDeletionModal',
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14769
export const openCancelMemberDeletionModalButton = (attributes: { orgId: string, memberId: string },
): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'cancelMemberDeletionModal',
  source: 'memberDetailScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14764
export const closeCancelDeletionDialogButton = (attributes: { orgId: string, memberId: string },
): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'closeCancelDeletion',
  source: 'cancelMemberDeletionModal',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14765
export const confirmCancelDeletionDialogButton = (attributes: { orgId: string, memberId: string },
): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'confirmCancelDeletion',
  source: 'cancelMemberDeletionModal',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15115
export const reactivateMemberModalScreenEvent = (): ScreenEvent => ({
  data: {
    name: 'reactivateMemberModal',
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15112
export const openReactivateMemberModalButton = (attributes: { orgId: string, memberId: string },
): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'reactivateMemberModal',
  source: 'memberDetailScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15113
export const closeReactivateDialogButton = (attributes: { orgId: string, memberId: string },
): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'closeReactivate',
  source: 'reactivateMemberModal',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15114
export const confirmReactivateDialogButton = (attributes: { orgId: string, memberId: string },
): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'confirmReactivate',
  source: 'reactivateMemberModal',
  attributes,
});

export type UserState = 'grantedAccessToSite' | 'revokedAccessToSite' | 'disabled' | 'invited';

export interface RemovedUserFromGroupAttributes {
  groupId: string;
}

export interface AddedUserToGroupAttributes {
  groupsAddedCount: number;
}

export interface BaseUserPageAttributes {
  userState: UserState[];
  userId: string;
}

export interface UserInviteAttributes {
  numberOfUsersInvited: number;
  numberOfDomainsAddedToDRS?: number;
  numberOfDomainsNotAddedToDRS?: number;
  productsGrantedAccess?: Array<{ [productKey: string]: boolean; }>;
  numberOfGroupsAdded?: number;
  isPersonalizedEmailSent?: boolean;
  isInviteSent?: boolean;
  permissionType: string;
  isTrustedUserEnabled: boolean;
}

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14241
export const removedUserFromSiteTrackEventData = (attributes: BaseUserPageAttributes): TrackData => ({
  action: 'removedFromSite',
  actionSubject: TrackActionSubject.User,
  source: 'removeUserFromSiteModal',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14242
export const impersonatedUserTrackEventData = (attributes: BaseUserPageAttributes): TrackData => ({
  action: 'impersonated',
  actionSubject: TrackActionSubject.User,
  source: 'userDetailsScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14243
export const resetUserPasswordTrackEventData = (attributes: BaseUserPageAttributes): TrackData => ({
  action: 'reset',
  actionSubject: TrackActionSubject.UserPassword,
  source: 'userDetailsScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14244
export const changedUserSiteAccessTrackEventData = (attributes: BaseUserPageAttributes): TrackData => ({
  action: 'changedSiteAccess',
  actionSubject: TrackActionSubject.User,
  source: 'userDetailsScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14245
export const changedUserProductAccessTrackEventData = (attributes: BaseUserPageAttributes): TrackData => ({
  action: 'changedProductAccess',
  actionSubject: TrackActionSubject.User,
  source: 'userDetailsScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14246
export const resentUserInvitationTrackEventData = (attributes: BaseUserPageAttributes): TrackData => ({
  action: 'resent',
  actionSubject: TrackActionSubject.Invitation,
  source: 'userDetailsScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14417
export const userInviteDrawerScreenEventData = (): ScreenData => ({
  name: 'userInviteDrawer',
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14409
export const userInviteSentTrackEventData = (attributes: UserInviteAttributes & OriginIdGeneratedAttributes): TrackData => ({
  action: TrackAction.Invited,
  actionSubject: TrackActionSubject.User,
  source: 'userInviteDrawer',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14416
export const userInviteSentUIEventData = (attributes: UserInviteAttributes & OriginIdGeneratedAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'inviteUsers',
  source: 'userInviteDrawer',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14750
export const userInviteChangedUserPermissionDrawerTrackEventData = (attributes: UserPermissionChangeAttributes): TrackData => ({
  action: 'changed',
  actionSubject: TrackActionSubject.UserPermission,
  source: 'userInviteDrawer',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14752
export const userInviteClickedUserPermissionUIEventData = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.DropListItem,
  actionSubjectId: 'userPermissions',
  source: 'userInviteDrawer',
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14416
export const userQuickInviteSentUIEventData = (attributes: UserInviteAttributes & OriginIdGeneratedAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'inviteUsers',
  source: 'userListScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14409
export const userQuickInviteSentTrackEventData = (attributes: UserInviteAttributes & OriginIdGeneratedAttributes): TrackData => ({
  action: Action.Invited,
  actionSubject: TrackActionSubject.User,
  source: 'userListScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14418
export const userInviteCancelledUIEventData = (attributes: UserInviteAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'cancelInviteUsers',
  source: 'userInviteDrawer',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14419
export const userInviteClosedUIEventData = (attributes: UserInviteAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'closeInviteUsers',
  source: 'userInviteDrawer',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14621
export const userListScreenEvent = (attributes: {
  userCountOnScreen: number,
  totalUserCount: number,
  quickInviteVisible: boolean,
  userTypesSelected: string[],
  productKeysSelected: string[],
  adminTypesSelected: string[],
}): ScreenEvent => ({
  data: {
    name: 'userListScreen',
    attributes,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14622
export const userListProductAccessModalScreenEvent = (attributes: {
  userId: string,
  selectedProductKeys: string[],
  unselectedProductKeys: string[],
}): ScreenEvent => ({
  data: {
    name: 'userListProductAccessModal',
    attributes,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14623
export const userListAddToGroupModalScreenEvent = (attributes: {
  userId: string,
  countGroupsAvailable: number,
}): ScreenEvent => ({
  data: {
    name: 'userListAddToGroupModal',
    attributes,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14624
export const userListNoResultsScreenEvent = (attributes: {
  countSearchCharacters: number,
  userTypesSelected: string[],
  productKeysSelected: string[],
  adminTypesSelected: string[],
}): ScreenEvent => ({
  data: {
    name: 'userListNoResultsScreen',
    attributes,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14625
export const userListGrantAccessModalScreenEvent = (attributes: {userId: string}): ScreenEvent => ({
  data: {
    name: 'userListGrantAccessModal',
    attributes,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14626
export const userListRevokeAccessModalScreenEvent = (attributes: {userId: string}): ScreenEvent => ({
  data: {
    name: 'userListRevokeAccessModal',
    attributes,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14247
export const addedUserToGroupTrackEventData = (attributes: BaseUserPageAttributes & AddedUserToGroupAttributes): TrackData => ({
  action: 'addedToGroup',
  actionSubject: TrackActionSubject.User,
  source: 'addUserToGroupModal',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14248
export const removedUserFromGroupTrackEventData = (attributes: BaseUserPageAttributes & RemovedUserFromGroupAttributes): TrackData => ({
  action: 'removedFromGroup',
  actionSubject: TrackActionSubject.User,
  source: 'removeUserFromGroupModal',
  attributes,
});

interface AddUserToGroupsModalAttributes {
  groupCount: number;
  groupIds: string[];
}

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14403
export const addUserToGroupsUIEventData = (attributes: BaseUserPageAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'addUserToGroups',
  source: 'userDetailsScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14404
export const addUserToGroupsConfirmUIEventData = (attributes: BaseUserPageAttributes & AddUserToGroupsModalAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'addUserToGroupsConfirm',
  source: 'addUserToGroupsModal',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14405
export const addUserToGroupsCancelUIEventData = (attributes: BaseUserPageAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'addUserToGroupsCancel',
  source: 'addUserToGroupsModal',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14339
export const confimRemoveUserFromSiteUIEventData = (attributes: BaseUserPageAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'confimRemoveUserFromSite',
  source: 'removeUserFromSiteModal',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14340
export const cancelRemoveUserFromSiteUIEventData = (attributes: BaseUserPageAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'cancelRemoveUserFromSite',
  source: 'removeUserFromSiteModal',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14341
export const removeUserFromSiteUIEventData = (attributes: BaseUserPageAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'removeUserFromSite',
  source: 'userDetailsScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/13916
export const removeUserFromSiteModalScreenEvent = (): ScreenEvent => ({
  data: {
    name: 'removeUserFromSiteModal',
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/13917
export const addUserToGroupsModalScreenEvent = (): ScreenEvent => ({
  data: {
    name: 'addUserToGroupsModal',
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/13918
export const removeUserFromGroupsModalScreenEvent = (): ScreenEvent => ({
  data: {
    name: 'removeUserFromGroupsModal',
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14430
export const resetPasswordUIEventData = (attributes: BaseUserPageAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'resetPassword',
  source: 'userDetailsScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14431
export const logInAsUserUIEventData = (attributes: BaseUserPageAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'logInAsUser',
  source: 'userDetailsScreen',
  attributes,
});

interface UpdateUserStatusAdditionalAttributes {
  updateType?: 'grant' | 'revoke';
}

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14432
export const updateUserActiveStatusUIEventData = (attributes: BaseUserPageAttributes & UpdateUserStatusAdditionalAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'updatedUserActiveStatus',
  source: 'userDetailsScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14433
export const updatedUserProductsUIEventData = (attributes: BaseUserPageAttributes & UpdateUserStatusAdditionalAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'updatedUserProducts',
  source: 'userDetailsScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14434
export const viewGroupUIEventData = (attributes: BaseUserPageAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'viewGroup',
  source: 'userDetailsScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14435
export const reInviteUserUIEventData = (attributes: BaseUserPageAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'reInviteUser',
  source: 'userDetailsScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14436
export const removeUserFromGroupUIEventData = (attributes: BaseUserPageAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'removedUserFromGroup',
  source: 'userDetailsScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14438
export const confirmRemoveUserFromGroupUIEventData = (attributes: BaseUserPageAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'confirmRemoveUserFromGroup',
  source: 'removeUserFromGroupModal',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14437
export const cancelRemoveUserFromGroupUIEventData = (attributes: BaseUserPageAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'cancelRemoveUserFromGroup',
  source: 'removeUserFromGroupModal',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14266
const regenerateInviteUrlModalName = 'regenerateInviteUrlModal';
export const regenerateInviteUrlModal = (
  cloudId: string,
  productKey: string,
  currentExpirationDate: string,
  currentDaysRemaining: number,
): ScreenEvent => ({
  cloudId,
  data: {
    name: regenerateInviteUrlModalName,
    attributes: {
      productKey,
      currentExpirationDate,
      currentDaysRemaining,
    },
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14267
type CheckboxState = 'enabled' | 'disabled';
export const inviteUrlCheckbox = (attributes: {
  cloudId: string,
  productKey: string,
  currentState: CheckboxState,
  newState: CheckboxState,
  currentExpirationDate?: string,
  currentDaysRemaining?: number,
}): UIEvent => ({
  data: {
    action: Action.Clicked,
    actionSubject: ActionSubject.Checkbox,
    actionSubjectId: 'inviteUrlCheckbox',
    source: siteAccessScreen,
    objectType: 'inviteUrl',
    attributes,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14269
export const copyInviteUrlButton = (attributes: {
  cloudId: string,
  productKey: string,
  currentExpirationDate?: string,
  currentDaysRemaining?: number,
}): UIEvent => ({
  data: {
    action: Action.Clicked,
    actionSubject: ActionSubject.Button,
    actionSubjectId: 'copyInviteUrlButton',
    source: siteAccessScreen,
    objectType: 'inviteUrl',
    attributes,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14270
export const regenerateInviteUrlLink = (attributes: {
  cloudId: string,
  productKey: string,
  currentExpirationDate?: string,
  currentDaysRemaining?: number,
}): UIEvent => ({
  data: {
    action: Action.Clicked,
    actionSubject: ActionSubject.Button,
    actionSubjectId: 'regenerateInviteUrlLink',
    source: siteAccessScreen,
    objectType: 'inviteUrl',
    attributes,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14271
export const confirmRegenerateInviteUrlLink = (attributes: {
  cloudId: string,
  productKey: string,
  currentExpirationDate?: string,
  currentDaysRemaining?: number,
}): UIEvent => ({
  data: {
    action: Action.Clicked,
    actionSubject: ActionSubject.Button,
    actionSubjectId: 'confirmRegenerateInviteUrlLink',
    source: regenerateInviteUrlModalName,
    objectType: 'inviteUrl',
    attributes,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14272
export const cancelRegenerateInviteUrlLink = (attributes: {
  cloudId: string,
  productKey: string,
  currentExpirationDate?: string,
  currentDaysRemaining?: number,
}): UIEvent => ({
  data: {
    action: Action.Clicked,
    actionSubject: ActionSubject.Button,
    actionSubjectId: 'cancelRegenerateInviteUrlLink',
    source: regenerateInviteUrlModalName,
    objectType: 'inviteUrl',
    attributes,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14875
export const inviteUrlLinkToggled = (cloudId, attributes: {
  productKey: string,
  currentState: CheckboxState,
  newState: CheckboxState,
  currentExpirationDate?: string,
  currentDaysRemaining?: number,
  newExpirationDate?: string,
  newDaysRemaining?: number,
}): TrackEvent => ({
  cloudId,
  data: {
    action: TrackAction.Toggled,
    actionSubject: TrackActionSubject.InviteUrlLinkToggled,
    source: siteAccessScreen,
    attributes,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14876
export const inviteUrlLinkRegenerated = (cloudId, attributes: {
  productKey: string,
  currentExpirationDate?: string,
  currentDaysRemaining?: number,
  newExpirationDate?: string,
  newDaysRemaining?: number,
}): TrackEvent => ({
  cloudId,
  data: {
    action: TrackAction.Toggled,
    actionSubject: TrackActionSubject.InviteUrlLinkRegenerated,
    source: regenerateInviteUrlModalName,
    attributes,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14390
export const jsdExportCustomersUIEventData = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'exportCustomersButton',
  source: 'jsdPortalCustomersScreen',
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14336
export const memberDeletionErrorsScreenEvent = (orgId: string, memberId: string): ScreenEvent => ({
  orgId,
  data: {
    name: 'memberDeletionSPIErrorsScreen',
    attributes: {
      orgId,
      memberId,
    },
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14761
export const memberDeletionScreenEvent = (orgId: string, memberId: string): ScreenEvent => ({
  orgId,
  data: {
    name: 'memberDeletionSPIModal',
    attributes: {
      orgId,
      memberId,
    },
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14763
export const confirmMemberDeletion = (attributes: { orgId: string, memberId: string, products: string[] },
): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'confirmMemberDeletion',
  source: 'memberDeletionSPIModal',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14762
export const closeMemberDeletion = (attributes: { orgId: string, memberId: string },
): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'closeMemberDeletion',
  source: 'memberDeletionSPIModal',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15143
export const memberDeactivationScreenEvent = (orgId: string, memberId: string): ScreenEvent => ({
  orgId,
  data: {
    name: 'memberDeactivationSPIModal',
    attributes: {
      orgId,
      memberId,
    },
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15200
export const memberDeactivationErrorScreenEvent = (orgId: string, memberId: string): ScreenEvent => ({
  orgId,
  data: {
    name: 'memberDeactivationSPIErrorModal',
    attributes: {
      orgId,
      memberId,
    },
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15144
export const confirmMemberDeactivation = (attributes: { orgId: string, memberId: string, products: string[] },
): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'confirmMemberDeactivation',
  source: 'memberDeactivationSPIModal',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15145
export const closeMemberDeactivation = (attributes: { orgId: string, memberId: string },
): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'closeMemberDeactivation',
  source: 'memberDeactivationSPIModal',
  attributes,
});

export interface UserPermissionChangeAttributes {
  previousPermissionType: Permissions;
  newPermissionType: Permissions;
}

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14750
export const changedUserPermissionTrackEventData = (attributes: BaseUserPageAttributes & UserPermissionChangeAttributes): TrackData => ({
  action: 'changed',
  actionSubject: TrackActionSubject.UserPermission,
  source: 'userDetailsScreen',
  attributes,
});

export interface FailedUserPermissionChangeAttributes {
  previousPermissionType: Permissions;
  failedPermissionType: Permissions;
  failureReason: string;
}

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14751
export const failedUserPermissionTrackEventData = (attributes: BaseUserPageAttributes & FailedUserPermissionChangeAttributes): TrackData => ({
  action: 'failed',
  actionSubject: TrackActionSubject.UserPermission,
  source: 'userDetailsScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14752
export const selectedUserPermissionUIEventData = (attributes: BaseUserPageAttributes & UserPermissionChangeAttributes): UIData => ({
  action: Action.Selected,
  actionSubject: ActionSubject.DropListItem,
  actionSubjectId: 'userPermissions',
  source: 'userDetailsScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14597
export const deleteAccountFocusedTaskScreenEvent = (): ScreenEvent => ({
  data: {
    name: 'deleteAccountFocusedTaskScreen',
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15122
export const deactivateAccountFocusedTaskScreenEvent = (): ScreenEvent => ({
  data: {
    name: 'deactivateAccountFocusedTaskScreen',
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14599
export const cancelDeleteButton = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'cancelDelete',
  source: 'deleteAccountFocusedTaskScreen',
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15123
export const cancelDeactivateButton = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'cancelDeactivate',
  source: 'deactivateAccountFocusedTaskScreen',
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14570
export const deleteAccountButton = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'deleteAccount',
  source: 'deleteAccountFocusedTaskScreen',
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15124
export const deactivateAccountButton = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'deactivateAccount',
  source: 'deactivateAccountFocusedTaskScreen',
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14573
export const deactivateAccountLink = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Link,
  actionSubjectId: 'deactivateAccountLink',
  source: 'deleteAccountFocusedTaskScreen',
});

interface UserListAttributes {
  userId: string;
  userStatus: string;
  userRole: string;
}

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14628
export const userListScreenInviteResent = (attributes: BaseUserPageAttributes): TrackData => ({
  action: TrackAction.Resent,
  actionSubject: TrackActionSubject.Invitation,
  source: 'userListScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14629
export const userListGrantAccessModalSiteAccessGranted = (attributes: {
  lastSeenOnSite: string,
  productAccess: string[],
} & UserListAttributes,
): TrackData => ({
  action: TrackAction.Granted,
  actionSubject: TrackActionSubject.SiteAccess,
  source: 'userListGrantAccessModal',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14630
export const userListGrantAccessModalSiteAccessRevoked = (attributes: {
  lastSeenOnSite: string,
  productAccess: string[],
} & UserListAttributes,
): TrackData => ({
  action: TrackAction.Revoked,
  actionSubject: TrackActionSubject.SiteAccess,
  source: 'userListRevokeAccessModal',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14631
export const userListGrantAccessModalProductAccessSaved = (attributes: {
  productAccessBefore: string[],
  productAccessAfter: string[],
} & UserListAttributes,
): TrackData => ({
  action: TrackAction.Saved,
  actionSubject: TrackActionSubject.SiteAccess,
  source: 'userListProductAccessModal',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14633
export const userListOpenInviteUserDrawer = (attributes: {
  userCountOnScreen?: number,
  totalUserCount?: number,
},
): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'openInviteUserDrawer',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14634
export const userListExportUsers = (attributes: {
  userCountOnScreen?: number,
  totalUserCount?: number,
},
): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'exportUsers',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14635
export const userListSearchUser = (attributes: {
  userCountOnScreen: number,
  totalUserCount: number,
  countSearchCharacters: number,
}): UIData => ({
  action: Action.Updated,
  actionSubject: ActionSubject.Field,
  actionSubjectId: 'searchUser',
  source: 'userListScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14636
// https://hello.atlassian.net/browse/ADMPF-892: add userTypesSelected attribute
export const userListNoSearchResultsInviteUsers = (attributes: {
  countSearchCharacters: number,
}): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'noSearchResultsInviteUsers',
  source: 'userListScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14637
export const userListUsersFilter = (attributes: {
  filters: string[],
}): UIData => ({
  action: Action.Selected,
  actionSubject: ActionSubject.DropListItem,
  actionSubjectId: 'usersFilter',
  source: 'userListScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14638
export const userListProductFilter = (attributes: {
  filters: string[],
}): UIData => ({
  action: Action.Selected,
  actionSubject: ActionSubject.DropListItem,
  actionSubjectId: 'productFilter',
  source: 'userListScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14639
export const userListAdminFilter = (attributes: {
  filters: string[],
}): UIData => ({
  action: Action.Selected,
  actionSubject: ActionSubject.DropListItem,
  actionSubjectId: 'adminFilter',
  source: 'userListScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14640
// https://hello.atlassian.net/browse/ADMPF-890: add product attribute
export const userListResendInviteAction = (attributes: {
  clickedViaFilterResults: boolean,
} & UserListAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'resendInvite',
  source: 'userListScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14641
// https://hello.atlassian.net/browse/ADMPF-890: add product attribute
export const userListGrantSiteAccessAction = (attributes: {
  clickedViaFilterResults: boolean,
  lastSeenOnSite: string,
} & UserListAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'grantSiteAccess',
  source: 'userListScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14642
// https://hello.atlassian.net/browse/ADMPF-890: add product attribute
export const userListRevokeSiteAccessAction = (attributes: {
  clickedViaFilterResults: boolean,
  lastSeenOnSite: string,
} & UserListAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'revokeSiteAccess',
  source: 'userListScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14643
// https://hello.atlassian.net/browse/ADMPF-890: add product attribute
export const userListUpdateProductAccessAction = (attributes: {
  clickedViaFilterResults: boolean,
  lastSeenOnSite: string,
} & UserListAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'updateProductAccess',
  source: 'userListScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14644
// https://hello.atlassian.net/browse/ADMPF-890: add product attribute
export const userListAddToGroupsAction = (attributes: {
  clickedViaFilterResults: boolean,
  lastSeenOnSite: string,
} & UserListAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'addToGroups',
  source: 'userListScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14645
// https://hello.atlassian.net/browse/ADMPF-890: add product attribute
export const userListShowDetailsAction = (attributes: {
  clickedViaFilterResults: boolean,
  lastSeenOnSite: string,
} & UserListAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'showDetails',
  source: 'userListScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14646
// https://hello.atlassian.net/browse/ADMPF-891: Add paging event

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14647
export const userListSaveProductAccess = (attributes: {
  productAccessBefore: string[],
  productAccessAfter: string[],
} & UserListAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'saveProductAccess',
  source: 'userListProductAccessModal',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14648
// https://hello.atlassian.net/browse/ADMPF-892: tests (pending users-activate-user-modal.tsx tests)
export const userListCancelProductAccess = (attributes: UserListAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'cancelProductAccess',
  source: 'userListProductAccessModal',
  attributes,
});

// https://hello.atlassian.net/browse/ADMPF-892: Needs to be added to the data portal
// https://hello.atlassian.net/browse/ADMPF-890: add product attribute
// https://hello.atlassian.net/browse/ADMPF-892: tests (pending users-activate-user-modal.tsx tests)
export const userListGrantSiteAccess = (attributes: {
  lastSeenOnSite: string,
} & UserListAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'grantSiteAccess',
  source: 'userListGrantAccessModal',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14651
// https://hello.atlassian.net/browse/ADMPF-892: tests (pending users-activate-user-modal.tsx tests)
export const userListCancelGrantSiteAccess = (attributes: UserListAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'cancelGrantSiteAccess',
  source: 'userListGrantAccessModal',
  attributes,
});

// https://hello.atlassian.net/browse/ADMPF-892: Needs to be added to the data portal
// https://hello.atlassian.net/browse/ADMPF-890: add product attribute
// https://hello.atlassian.net/browse/ADMPF-892: tests (pending users-activate-user-modal.tsx tests)
export const userListRevokeSiteAccess = (attributes: {
  lastSeenOnSite: string,
} & UserListAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'revokeSiteAccess',
  source: 'userListRevokeAccessModal',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14652
// https://hello.atlassian.net/browse/ADMPF-892: tests (pending users-activate-user-modal.tsx tests)
export const userListCancelRevokeSiteAccess = (attributes: UserListAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'cancelRevokeSiteAccess',
  source: 'userListRevokeAccessModal',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14760
// @TODO Implement this
export const jsdPortalCustomersScreenEvent = (attributes: { customerCountOnScreen: number }): ScreenEvent => ({
  data: {
    name: 'jsdPortalCustomersScreen',
    attributes,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14759
export const jsdPortalCustomersEditNameModalScreenEvent = (): ScreenEvent => ({
  data: {
    name: 'jsdPortalCustomersEditNameModal',
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14816
export const jsdPortalCustomersDeactivateAccountModalScreenEvent = (): ScreenEvent => ({
  data: {
    name: 'jsdPortalCustomersDeactivateAccountModal',
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14817
export const jsdPortalCustomersActivateAccountModalScreenEvent = (): ScreenEvent => ({
  data: {
    name: 'jsdPortalCustomersActivateAccountModal',
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14780
export const jsdPortalCustomersChangePasswordModalScreenEvent = (): ScreenEvent => ({
  data: {
    name: 'jsdPortalCustomersChangePasswordModal',
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14821
export const jsdPortalCustomersDeleteModalScreenEvent = (): ScreenEvent => ({
  data: {
    name: 'jsdPortalCustomersDeleteAccountModal',
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15081
export const jsdPortalCustomersMigrateAccountModalScreenEvent = (): ScreenEvent => ({
  data: {
    name: 'jsdPortalCustomersMigrateAccountModal',
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14573
export const jsdPortalCustomersDeactivateAccountModalLinkEvent = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Link,
  actionSubjectId: 'deactivateAccountLink',
  source: 'jsdPortalCustomersScreen',
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14818
export const jsdPortalCustomersActivateAccountModalLinkEvent = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Link,
  actionSubjectId: 'activateAccountLink',
  source: 'jsdPortalCustomersScreen',
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14766
export const jsdPortalCustomersEditNameModalLinkEvent = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Link,
  actionSubjectId: 'editNameLink',
  source: 'jsdPortalCustomersScreen',
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14781
export const jsdPortalCustomersChangePasswordLinkEvent = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Link,
  actionSubjectId: 'changePasswordLink',
  source: 'jsdPortalCustomersScreen',
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14822
export const jsdPortalCustomersDeleteModalLinkEvent = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Link,
  actionSubjectId: 'deleteAccountLink',
  source: 'jsdPortalCustomersScreen',
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15082
export const jsdPortalCustomersMigrateAccountLinkEvent = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Link,
  actionSubjectId: 'migrateAccountLink',
  source: 'jsdPortalCustomersScreen',
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14767
export const jsdPortalCustomersDeactivateAccountModalSubmitEvent = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'submitButton',
  source: 'jsdPortalCustomersDeactivateAccountModal',
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14767
export const jsdPortalCustomersActivateAccountModalSubmitEvent = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'submitButton',
  source: 'jsdPortalCustomersActivateAccountModal',
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14767
export const jsdPortalCustomersEditNameModalSubmitEvent = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'submitButton',
  source: 'jsdPortalCustomersEditNameModal',
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14767
export const jsdPortalCustomersDeleteModalSubmitEvent = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'submitButton',
  source: 'jsdPortalCustomersDeleteAccountModal',
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14782
export const jsdPortalCustomersChangePasswordModalSubmitEvent = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'changePasswordModalSubmitButton',
  source: 'jsdPortalCustomersChangePasswordModal',
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15084
export const jsdPortalCustomersMigrateAccountModalSubmitEvent = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'migrateAccountModalSubmitButton',
  source: 'jsdPortalCustomersMigrateAccountModal',
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14768
export const jsdPortalCustomersEditNameModalCancelEvent = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'editNameModalCancelButton',
  source: 'jsdPortalCustomersEditNameModal',
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14783
export const jsdPortalCustomersChangePasswordModalCancelEvent = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'changePasswordModalCancelButton',
  source: 'jsdPortalCustomersChangePasswordModal',
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15101
export const emojiAdminScreenEvent = (): ScreenEvent => ({
  data: {
    name: 'emojiScreen',
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15102
export const emojiDeletionModalEvent = (): ScreenData => ({
  name: 'emojiDeletionModal',
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15103
export const allowUploadToggleData = (uploadEnabled: boolean): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Toggle,
  actionSubjectId: 'allowEmojiUploadToggle',
  source: 'emojiScreen',
  attributes: {
    uploadEnabled,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15104
export const emojiSearchTypedData = (
  queryLength: number,
  numMatches: number,
): UIData => ({
  action: Action.Searched,
  actionSubject: ActionSubject.Field,
  actionSubjectId: 'emojiSearchField',
  source: 'emojiScreen',
  attributes: {
    queryLength,
    numMatches,
  },
});

export type EmojiTableSortKey = 'shortName' | 'addedOn';

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15105
export const emojiTableSortData = (key: EmojiTableSortKey, ascending: boolean): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'emojiTableSortHeading',
  source: 'emojiScreen',
  attributes: {
    key,
    ascending,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15106
export const emojiTablePaginationData = (
  previousPage: number,
  currentPage: number,
): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'emojiTablePagination',
  source: 'emojiScreen',
  attributes: {
    previousPage,
    currentPage,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15107
export const emojiTableRemoveData = (emojiId: string): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'emojiTableRemoveButton',
  source: 'emojiScreen',
  attributes: {
    emojiId,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15108
export const emojiDeletionConfirmButton = (
  emojiId: string,
  creatorUserId: string,
  createdDate: string,
): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'emojiDeletionConfirmButton',
  source: 'emojiDeletionModal',
  attributes: {
    emojiId,
    creatorUserId,
    createdDate,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15109
export const emojiDeletionCancelButton = (emojiId: string): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'emojiDeletionCancelButton',
  source: 'emojiDeletionModal',
  attributes: {
    emojiId,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14571
export const jsdPortalCustomersDeleteModalCancelEvent = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'cancelButton',
  source: 'jsdPortalCustomersDeleteAccountModal',
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15088
export const jsdPortalCustomersMigrateAccountModalCancelEvent = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'migrateAccountModalCancelButton',
  source: 'jsdPortalCustomersMigrateAccountModal',
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14823
export const jsdPortalCustomersDeleteModalTrackEvent = (): TrackData => ({
  action: TrackAction.Deleted,
  actionSubject: TrackActionSubject.Customer,
  source: 'jsdPortalCustomersScreen',
});

export const jsdPortalCustomersDeactivateAccountModalCancelEvent = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'cancelButton',
  source: 'jsdPortalCustomersDeactivateAccountModal',
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14571
export const jsdPortalCustomersActivateAccountModalCancelEvent = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'cancelButton',
  source: 'jsdPortalCustomersActivateAccountModal',
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14819
export const jsdPortalCustomersDeactivateAccountModalTrackEvent = (): TrackData => ({
  action: TrackAction.Activated,
  actionSubject: TrackActionSubject.Customer,
  source: 'jsdPortalCustomersScreen',
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14820
export const jsdPortalCustomersActivateAccountModalTrackEvent = (): TrackData => ({
  action: TrackAction.Deactivated,
  actionSubject: TrackActionSubject.Customer,
  source: 'jsdPortalCustomersScreen',
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15091
export const jsdPortalCustomersMigrateAccountModalTrackEvent = (): TrackData => ({
  action: TrackAction.Migrated,
  actionSubject: TrackActionSubject.Customer,
  source: 'jsdPortalCustomersScreen',
});

interface NewUserAccessAttributes {
  productKey: string;
  hasAccess: boolean;
}

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14858
export const toggledNewUserAccessTrackEventData = (attributes: NewUserAccessAttributes): TrackData => ({
  action: 'toggled',
  actionSubject: TrackActionSubject.NewUserAccess,
  source: 'productAccessConfigurationScreen',
  attributes,
});

interface DefaultGroupAttributes {
  productKey: string;
  groupId: string;
}

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14857
export const makeDefaultGroupTrackEventData = (attributes: DefaultGroupAttributes): TrackData => ({
  action: 'madeDefault',
  actionSubject: TrackActionSubject.Group,
  source: 'productAccessConfigurationScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14856
export const removeDefaultGroupTrackEventData = (attributes: DefaultGroupAttributes): TrackData => ({
  action: 'removedDefault',
  actionSubject: TrackActionSubject.Group,
  source: 'productAccessConfigurationScreen',
  attributes,
});

interface RemoveGroupAttributes {
  productKey: string;
  groupId: string;
}

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14855
export const removeGroupTrackEventData = (attributes: RemoveGroupAttributes): TrackData => ({
  action: 'removed',
  actionSubject: TrackActionSubject.Group,
  source: 'productAccessConfigurationScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14855
export const removeAdminGroupTrackEventData = (attributes: RemoveGroupAttributes): TrackData => ({
  action: 'removed',
  actionSubject: TrackActionSubject.Group,
  source: 'adminProductAccessConfigurationScreen',
  attributes,
});

interface AddGroupAttributes {
  productKey: string;
  groupsAddedCount: number;
  productAccessType: ConfigType;
}

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14854
export const addGroupTrackEventData = (attributes: AddGroupAttributes): TrackData => ({
  action: 'add',
  actionSubject: TrackActionSubject.Group,
  source: 'addGroupProductAccessModal',
  attributes,
});

interface NewUserAccessAttributes {
  productKey: string;
  hasAccess: boolean;
}

// // https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14853
export const newUserAccessUIEventData = (attributes: NewUserAccessAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Toggle,
  actionSubjectId: 'newUserAccess',
  source: 'productAccessConfigurationScreen',
  attributes,
});

interface AddGroupUIAttributes {
  productKey: string;
}

export const addGroupUIEventData = (attributes: AddGroupUIAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'addGroup',
  source: 'productAccessConfigurationScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14852
export const makeGroupDefaultUIEventData = (attributes: ShowGroupDetailsAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'makeGroupDefault',
  source: 'productAccessConfigurationScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14851
export const removeGroupDefaultUIEventData = (attributes: ShowGroupDetailsAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'removeGroupDefault',
  source: 'productAccessConfigurationScreen',
  attributes,
});

interface AddGroupToProductAttributes {
  productKey: string;
}

interface ShowGroupDetailsAttributes {
  productKey: string;
  groupId: string;
}

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14850
export const showGroupDetailsUIEventData = (attributes: ShowGroupDetailsAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'showGroupDetails',
  source: 'productAccessConfigurationScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14850
export const showAdminGroupDetailsUIEventData = (attributes: ShowGroupDetailsAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'showGroupDetails',
  source: 'adminProductAccessConfigurationScreen',
  attributes,
});

interface RemoveGroupFromProductAttributes {
  productKey: string;
}

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14849
export const removeGroupFromProductUIEventData = (attributes: RemoveGroupFromProductAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'removeGroupFromProduct',
  source: 'productAccessConfigurationScreen',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14849
export const removeGroupFromAdminProductUIEventData = (attributes: RemoveGroupFromProductAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'removeGroupFromProduct',
  source: 'adminProductAccessConfigurationScreen',
  attributes,
});

interface AddGroupToProductAttributes {
  productKey: string;
  groupsAddedNumber: number;
}

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14848
export const addGroupToProductUIEventData = (attributes: AddGroupToProductAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'addGroup',
  source: 'addGroupAccessModal',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14848
export const addGroupToAdminProductUIEventData = (attributes: AddGroupToProductAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'addGroup',
  source: 'addGroupAccessModal',
  attributes,
});

interface CancelAddGroupToProductAttributes {
  productKey: string;
}

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14847
export const cancelAddGroupToProductUIEventData = (attributes: CancelAddGroupToProductAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'cancelAddGroupToProduct',
  source: 'addGroupAccessModal',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14846
export const cancelAddGroupToAdminProductUIEventData = (attributes: CancelAddGroupToProductAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'cancelAddGroupToAdminProduct',
  source: 'addGroupAccessModal',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15117
export const exportUserViewEventData = (usersInSiteCount: number): ScreenData => ({
  name: 'exportUserDrawer',
  attributes: {
    usersInSiteCount,
  },
});

type UserStatusSelected = 'onlyActiveUsers' | 'allUsers';

interface DownloadExportUserTrackAttributes {
  downloadedAllUsers: boolean;
  selectedGroupsCount?: number;
  userStatusSelected: UserStatusSelected;
  additionalDataSelectedGroupMembership: boolean;
  additionalDataSelectedProductAccess: boolean;
  usersInSiteCount: number;
}

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15142
export const downloadExportUserTrackEventData = (attributes: DownloadExportUserTrackAttributes): TrackData => ({
  action: TrackAction.Downloaded,
  actionSubject: TrackActionSubject.UserList,
  actionSubjectId: 'downloadExportUserList',
  source: 'exportUserDrawer',
  attributes,
});

interface DownloadExportUserUIAttributes {
  downloadedAllUsers: boolean;
  selectedGroupsCount?: number;
  userStatusSelected: UserStatusSelected;
  additionalDataSelectedGroupMembership: boolean;
  additionalDataSelectedProductAccess: boolean;
}

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15118
export const downloadExportUserUIEventData = (attributes: DownloadExportUserUIAttributes): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'downloadExportUserList',
  source: 'exportUserDrawer',
  attributes,
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15119
export const cancelExportUserUIEventData = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'cancelExportUserList',
  source: 'exportUserDrawer',
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14797
export const manageSubscriptionsXSellFeatureExposed = (source: string, attributes: {
  flagKey: string,
  reason: string,
  ruleId: string,
  value: boolean,
}): TrackEvent => ({
  data: {
    action: TrackAction.Exposed,
    actionSubject: TrackActionSubject.Feature,
    source,
    attributes,
  }});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14860
export const addGroupProductAccessModalScreenEvent = (): ScreenEvent => ({
  data: {
    name: 'addGroupProductAccessModal',
  },
});

interface ProductAccessAttributes {
  productKeys: string[];
  productCount: number;
}

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14861
export const adminProductAccessConfigurationScreenEvent = (attributes: ProductAccessAttributes): ScreenEvent => ({
  data: {
    name: 'adminProductAccessConfigurationScreen',
    attributes,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14862
export const productAccessConfigurationScreenEvent = (attributes: ProductAccessAttributes): ScreenEvent => ({
  data: {
    name: 'productAccessConfigurationScreen',
    attributes,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/14863
export const importAccessConfigurationScreenEvent = (attributes: ProductAccessAttributes): ScreenEvent => ({
  data: {
    name: 'importAccessConfigurationScreen',
    attributes,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15204
export const trustedUserCommunicationScreenEvent = (): ScreenEvent => ({
  data: {
    name: 'trustedUserCommunicationScreenEvent',
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15206
export const trustedUserCommunicationLinkEventData = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Link,
  actionSubjectId: 'trustedUserCommunicationLink',
  source: 'trustedUserCommunicationScreen',
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15205
export const trustedUserCommunicationDismissedEventData = (): UIData => ({
  action: Action.Clicked,
  actionSubject: ActionSubject.Button,
  actionSubjectId: 'trustedUserCommunicationCloseButton',
  source: 'trustedUserCommunicationScreen',
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15211
export const accessIntroScreenEvent = (): ScreenEvent => ({
  data: {
    name: 'accessIntroScreen',
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15222
export const accessTrialModalScreenEvent = (): ScreenEvent => ({
  data: {
    name: 'accessTrialModal',
  },
});

interface MemberDetailsAttributes {
  tokensCount: number | undefined;
}

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15312
export const memberDetailsScreenEvent = (attributes: MemberDetailsAttributes): ScreenEvent => ({
  data: {
    name: 'memberDetailsScreen',
    attributes,
  },
});

// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15394
// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15395
// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15396
// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15397
// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15398
// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15399
// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15400
// https://data-portal.us-east-1.prod.public.atl-paas.net/analytics/registry/15401
export const navigationItemEvent = (source: string, actionSubjectId: string): UIEvent => ({
  data: {
    action: Action.Clicked,
    actionSubject: ActionSubject.NavigationItem,
    actionSubjectId,
    source,
  },
});
