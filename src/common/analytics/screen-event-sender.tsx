import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';

import { analyticsClient } from './analytics-client';
import { ScreenEvent } from './new-analytics-types';
import { AnalyticsClientProps, withAnalyticsClient } from './with-analytics-client';

import { ClearReferrerMutation, ScreenEventSenderQuery } from '../../schema/schema-types';
import clearReferrerMutation from './screen-event-sender.mutation.graphql';
import screenEventSenderQuery from './screen-event-sender.query.graphql';

export interface ScreenEventSenderProps {
  event: ScreenEvent;
  // If the page data hasn't loaded yet, the user hasn't really seen the screen.
  // Use this prop for more accurate reporting.
  isDataLoading?: boolean;
}

export class ScreenEventSenderImpl extends React.Component<ChildProps<ScreenEventSenderProps & AnalyticsClientProps, ScreenEventSenderQuery & ClearReferrerMutation>> {
  private eventSent = false;

  public componentDidMount() {
    this.sendAnalyticsIfLoaded();
  }

  public componentDidUpdate() {
    this.sendAnalyticsIfLoaded();
  }

  public render() {
    return this.props.children;
  }

  private sendAnalyticsIfLoaded() {
    if (this.eventSent || this.props.isDataLoading || !this.props.data || this.props.data.loading) {
      return;
    }

    const event = {
      ...this.props.event,
      data: {
        ...this.props.event.data,
        attributes: {
          ...(this.props.event.data.attributes || {}),
          referrer: this.props.data.ui && this.props.data.ui.analytics.referrer,
        },
      },
    };

    this.props.analyticsClient.sendScreenEvent(event);

    this.eventSent = true;

    if (!this.props.mutate) {
      return;
    }
    this.props.mutate({}).catch(e => {
      analyticsClient.onError(e);
    });
  }
}

const withQuery = graphql<ScreenEventSenderProps, ScreenEventSenderQuery>(screenEventSenderQuery);
const withMutation = graphql<ScreenEventSenderProps, ClearReferrerMutation>(clearReferrerMutation);

export const ScreenEventSender = withQuery(
  withMutation(
    withAnalyticsClient(
      ScreenEventSenderImpl,
    ),
  ),
);
