import { expect } from 'chai';

import { isGroupModifiableForUser, isGroupVisibleForUser } from './group-permissions';

describe('group-permissions functions', () => {
  describe('isGroupVisibleForUser', () => {
    it('should return false for a system group when the user is not a system admin', () => {
      expect(isGroupVisibleForUser({
        unmodifiable: true,
        sitePrivilege: 'SYS_ADMIN',
        managementAccess: 'NONE',
        ownerType: 'EXT_SCIM',
      }, false)).to.equal(false);
    });

    it('should return true for a system group when the user is a system admin', () => {
      expect(isGroupVisibleForUser({
        unmodifiable: true,
        sitePrivilege: 'SYS_ADMIN',
        managementAccess: 'NONE',
        ownerType: 'EXT_SCIM',
      }, true)).to.equal(true);
    });

    it('should return true for a SCIM group when the user is not a system admin', () => {
      expect(isGroupVisibleForUser({
        unmodifiable: true,
        sitePrivilege: 'NONE',
        managementAccess: 'READ_ONLY',
        ownerType: 'EXT_SCIM',
      }, false)).to.equal(true);
    });

    it('should return true for a SCIM group when the user is a system admin', () => {
      expect(isGroupVisibleForUser({
        unmodifiable: true,
        sitePrivilege: 'NONE',
        managementAccess: 'READ_ONLY',
        ownerType: 'EXT_SCIM',
      }, true)).to.equal(true);
    });

    it('should return true for a regular group when the user is not a system admin', () => {
      expect(isGroupVisibleForUser({
        unmodifiable: false,
        sitePrivilege: 'NONE',
        managementAccess: 'ALL',
      }, false)).to.equal(true);
    });

    it('should return true if a regular group when the user is a system admin', () => {
      expect(isGroupVisibleForUser({
        unmodifiable: false,
        sitePrivilege: 'NONE',
        managementAccess: 'ALL',
      }, true)).to.equal(true);
    });
  });

  describe('isGroupModifiableForUser', () => {
    it('should return false for a system group when the user is not a system admin', () => {
      expect(isGroupModifiableForUser({
        unmodifiable: true,
        sitePrivilege: 'SYS_ADMIN',
        managementAccess: 'NONE',
        ownerType: 'EXT_SCIM',
      }, false, false)).to.equal(false);
    });

    it('should return true for a system group when the user is a system admin', () => {
      expect(isGroupModifiableForUser({
        unmodifiable: true,
        sitePrivilege: 'SYS_ADMIN',
        managementAccess: 'NONE',
        ownerType: 'EXT_SCIM',
      }, true, false)).to.equal(true);
    });

    it('should return false for a SCIM group when the user is not a system admin', () => {
      expect(isGroupModifiableForUser({
        unmodifiable: true,
        sitePrivilege: 'NONE',
        managementAccess: 'READ_ONLY',
        ownerType: 'EXT_SCIM',
      }, false, false)).to.equal(false);
    });

    it('should return false for a SCIM group when the user is a system admin', () => {
      expect(isGroupModifiableForUser({
        unmodifiable: true,
        sitePrivilege: 'NONE',
        managementAccess: 'READ_ONLY',
        ownerType: 'EXT_SCIM',
      }, true, false)).to.equal(false);
    });

    it('should return true for a regular group when the user is not a system admin', () => {
      expect(isGroupModifiableForUser({
        unmodifiable: false,
        sitePrivilege: 'NONE',
        managementAccess: 'ALL',
      }, false, false)).to.equal(true);
    });

    it('should return true for a regular group when the user is a system admin', () => {
      expect(isGroupModifiableForUser({
        unmodifiable: false,
        sitePrivilege: 'NONE',
        managementAccess: 'ALL',
      }, true, false)).to.equal(true);
    });

    it('should return false for a non SCIM read only group when the user is not a system admin', () => {
      expect(isGroupModifiableForUser({
        unmodifiable: false,
        sitePrivilege: 'NONE',
        managementAccess: 'READ_ONLY',
      }, false, false)).to.equal(false);
    });

    it('should return false for a non SCIM read only group when the user is is a system admin', () => {
      expect(isGroupModifiableForUser({
        unmodifiable: false,
        sitePrivilege: 'NONE',
        managementAccess: 'READ_ONLY',
      }, true, false)).to.equal(false);
    });

    it('should return true for a SCIM when SCIM groups should be exposed', () => {
      expect(isGroupModifiableForUser({
        unmodifiable: true,
        sitePrivilege: 'SYS_ADMIN',
        managementAccess: 'NONE',
        ownerType: 'EXT_SCIM',
      }, true, true)).to.equal(true);
    });
  });
});
