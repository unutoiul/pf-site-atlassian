import { Group } from '../../schema/schema-types';

type GroupPartial = Pick<Group, 'unmodifiable' | 'managementAccess' | 'ownerType' | 'sitePrivilege'>;

function isSystemGroup(group: GroupPartial) {
  if (isScimGroup(group)) {
    return false;
  }

  return group.sitePrivilege === 'SYS_ADMIN' || (group.sitePrivilege === 'NONE' && group.unmodifiable);
}

function isScimGroup(group: GroupPartial) {
  return (group.managementAccess === 'READ_ONLY' && group.ownerType === 'EXT_SCIM');
}

export function isGroupVisibleForUser(group: GroupPartial, isUserSystemAdmin: boolean) {
  if (isSystemGroup(group)) {
    return isUserSystemAdmin;
  }

  return true;
}

export function isGroupModifiableForUser(group: GroupPartial, isUserSystemAdmin: boolean, isExposingScimGroup: boolean) {
 // Note: we do not check group.unmodifiable here, as a System admin could modify an unmodifiable group

  if (isScimGroup(group) && isExposingScimGroup) {
    return true;
  }

  if (group.managementAccess === 'READ_ONLY') {
    return false;
  }

  if (isSystemGroup(group)) {
    return isUserSystemAdmin;
  }

  return true;
}
