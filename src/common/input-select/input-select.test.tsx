// tslint:disable:jsx-use-translation-function
import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import { InputSelect } from './input-select';

describe('Input Select', () => {
  it('should render a multi select without a dropdown', () => {
    const wrapper = shallow(<InputSelect />);

    expect(wrapper.props().components).to.deep.equal({ DropdownIndicator: null });
    expect(wrapper.props().isMulti).to.equal(true);
    expect(wrapper.props().menuIsOpen).to.equal(false);
  });

  describe('placeholder prop', () => {
    it('should render a placeholder if provided', () => {
      const wrapper = shallow(<InputSelect placeholder="Enter things" />);
      expect(wrapper.props().placeholder).to.equal('Enter things');
    });

    it('should render an empty placeholder if none is provided', () => {
      const wrapper = shallow(<InputSelect />);
      expect(wrapper.props().placeholder).to.equal('');
    });
  });
});
