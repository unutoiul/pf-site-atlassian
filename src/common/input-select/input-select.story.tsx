// tslint:disable:jsx-use-translation-function
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import styled from 'styled-components';

import {
  Field as AkField,
} from '@atlaskit/form';

import { InputSelect } from './input-select';

const Wrapper = styled.div`
  margin: 25px;
  max-width: 500px;
`;

storiesOf('Common|Input Select', module)
  .add('With default creation keys', () => {
    return (
      <Wrapper>
        <p>
          Uses the default tab and enter keys to create selections.
        </p>
        <AkField label="Email Adress(es)">
          <InputSelect />
        </AkField>
      </Wrapper>
    );
  })
  .add('With custom creation keys: enter and comma', () => {
    return (
      <Wrapper>
        <p>
          Uses custom creation keys: enter and comma.
        </p>
        <AkField label="Email Adress(es)">
          <InputSelect creationKeys={['Enter', ',']} />
        </AkField>
      </Wrapper>
    );
  });
