import * as React from 'react';

import AkSelect from '@atlaskit/select';

interface Props {
  placeholder?: string;
  creationKeys?: string[];
  isRequired?: boolean;
  onValuesChange?(currentValues: string[]): void;
}

interface State {
  inputValue: string;
  values: Value[];
}

interface Value {
  label: string;
  value: string;
}

export class InputSelect extends React.Component<Props, State> {
  public static readonly defaultProps: Partial<Props> = {
    creationKeys: ['Tab', 'Enter'],
    isRequired: false,
    placeholder: '',
  };

  public readonly state: Readonly<State> = {
    inputValue: '',
    values: [],
  };

  public render() {
    return (
      <AkSelect
        components={{ DropdownIndicator: null }}
        inputValue={this.state.inputValue}
        isClearable={true}
        isMulti={true}
        menuIsOpen={false}
        onInputChange={this.handleInputChange}
        onKeyDown={this.handleKeyDown}
        onChange={this.handleChange}
        value={this.state.values}
        placeholder={this.props.placeholder}
        isRequired={this.props.isRequired}
      />
    );
  }

  public handleChange = (values) => {
    this.setState({ values }, () => {
      if (this.props.onValuesChange) {
        this.props.onValuesChange(this.mapToOutputValues(this.state.values));
      }
    });
  };

  public handleInputChange = (inputValue: string) => {
    this.setState({ inputValue });
  };

  public handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (!this.state.inputValue) {
      return;
    }

    if (!(this.props.creationKeys && this.props.creationKeys.includes(event.key))) {
      return;
    }

    const isNewValueUnique = !this.state.values.some(value => value.value === this.state.inputValue);
    if (!isNewValueUnique) {
      this.setState({ inputValue: '' });

      return;
    }

    this.setState({
      inputValue: '',
      values: [...this.state.values, ...this.createValues(this.state.inputValue)],
    }, () => {
      if (this.props.onValuesChange) {
        this.props.onValuesChange(this.mapToOutputValues(this.state.values));
      }
    });

    event.preventDefault();
  };

  public createValues = (label: string): Value[] => (
    label.split(',').map(part => ({
      label: part,
      value: part,
    }),
  ));

  public mapToOutputValues = (values: Value[]): string[] => {
    return values.map(value => value.value);
  };
}
