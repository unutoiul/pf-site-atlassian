import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import { ActionSubject, AnalyticsClientProps, Button as AnalyticsButton, UIEvent, withAnalyticsClient } from 'common/analytics';
import { links } from 'common/link-constants';
import { ValuePropHeader } from 'common/value-prop';

import { createOrgAnalyticsData } from '../../organization/organizations/organization-analytics';

const messagesShared = defineMessages({
  headerBody: {
    id: 'security.value.prop.page.aa.shared.header.title',
    defaultMessage: 'Gain company-wide visibility, security and control over your Atlassian Cloud infrastructure so you can scale with confidence.',
  },
  headerLinkCopy: {
    id: 'security.value.prop.page.aa.shared.header.title.link.copy',
    defaultMessage: 'Find out more',
  },
});

const messagesAtlassianAccess = defineMessages({
  headerTitle: {
    id: 'security.value.prop.page.aa.header.title',
    defaultMessage: 'Atlassian Access',
  },
});

interface OwnProps {
  analyticsActionSubject: ActionSubject;
  learnMoreAnalyticsEvent?: UIEvent;
}

export class AtlassianAccessValuePropHeaderImpl extends React.Component<InjectedIntlProps & OwnProps & AnalyticsClientProps> {
  public render() {
    const {
      intl: { formatMessage },
      analyticsActionSubject,
    } = this.props;

    const learnMoreLink = (
      <AnalyticsButton
        href={links.external.atlassianAccess}
        appearance="link"
        spacing="none"
        target="_blank"
        onClick={this.onLearnMoreClick}
        analyticsData={createOrgAnalyticsData({
          action: 'click',
          actionSubject: analyticsActionSubject,
          actionSubjectId: 'learnAboutIDMEAPLink',
        })}
      >
        {formatMessage(messagesShared.headerLinkCopy)}
      </AnalyticsButton>
    );

    return (
      <ValuePropHeader
        title={formatMessage(messagesAtlassianAccess.headerTitle)}
        description={formatMessage(messagesShared.headerBody)}
        learnMoreLink={learnMoreLink}
        // tslint:disable-next-line:no-require-imports
        imagePath={require('./images/hero.svg')}
      />
    );
  }

  private onLearnMoreClick = () => {
    if (!this.props.learnMoreAnalyticsEvent) {
      return;
    }

    this.props.analyticsClient.sendUIEvent(this.props.learnMoreAnalyticsEvent);
  }
}

export const AtlassianAccessValuePropHeader = withAnalyticsClient(injectIntl(AtlassianAccessValuePropHeaderImpl));
