// tslint:disable:no-require-imports
import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import {
  Grid as AkGrid,
  GridColumn as AkGridColumn,
} from '@atlaskit/page';
import { gridSize as akGridSize } from '@atlaskit/theme';

import { ActionSubject, AnalyticsClientProps, Button as AnalyticsButton, UIEvent, withAnalyticsClient } from 'common/analytics';
import { links } from 'common/link-constants';

import { createOrgAnalyticsData } from '../../organization/organizations/organization-analytics';

const messagesShared = defineMessages({
  sectionSAMLTitle: {
    id: 'security.value.prop.page.shared.section.saml.title',
    defaultMessage: 'SAML single sign-on',
  },
  sectionSAMLCopy: {
    id: 'security.value.prop.page.shared.section.saml.copy',
    defaultMessage: 'Save time and resources for your IT team by integrating with your identity provider to deliver a better login experience for your users.',
  },
  sectionTwoStepTitle: {
    id: 'security.value.prop.page.shared.section.two.step.title',
    defaultMessage: 'Enforced two-step verification',
  },
  sectionTwoStepCopy: {
    id: 'security.value.prop.page.shared.section.two.step.copy',
    defaultMessage: 'Ensure the right people get the right access. Have confidence that all your users use two-step verification to log in.',
  },
  sectionPasswordTitle: {
    id: 'security.value.prop.page.shared.section.password.title',
    defaultMessage: 'Password management',
  },
  sectionPasswordCopy: {
    id: 'security.value.prop.page.shared.section.password.copy',
    defaultMessage: 'Be sure that your Atlassian Cloud users are using the best practices to set strong passwords.',
  },
  sectionSupportTitle: {
    id: 'security.value.prop.page.shared.section.support.title',
    defaultMessage: 'Priority Support',
  },
  sectionSupportCopy: {
    id: 'security.value.prop.page.shared.section.support.copy',
    defaultMessage: 'Get round-the-clock one-hour response times for critical issues from our support team.',
  },
});

const messagesAtlassianAccess = defineMessages({
  sectionPricingCopy: {
    id: 'security.value.prop.page.aa.section.pricing.copy',
    defaultMessage: 'Atlassian Access is billed on accounts accessing the following Atlassian Cloud products: Jira family, Confluence, Stride, Bitbucket and Trello (coming soon). ',
  },
  sectionPricingLink: {
    id: 'security.value.prop.page.aa.section.pricing.link',
    defaultMessage: 'Learn more about pricing',
  },
});

const SectionHeader = styled.h5`
  margin-top: ${akGridSize}px;
`;

const ColumnContent = styled.div`
  margin-top: ${akGridSize() * 6}px;
`;

const ColumnContentBottom = styled.div`
  margin-top: ${akGridSize() * 7}px;
`;

const CenteredText = styled.div`
  text-align: center;
  padding-top: ${akGridSize() * 4}px;
`;

const BodyContainer = styled.div`
  padding-top: ${akGridSize() * 2}px;
`;

interface OwnProps {
  analyticsActionSubject: ActionSubject;
  pricingAnalyticsEvent?: UIEvent;
}

export class AtlassianAccessValuePropBodyImpl extends React.Component<InjectedIntlProps & OwnProps & AnalyticsClientProps> {
  public render() {
    const { intl: { formatMessage }, analyticsActionSubject } = this.props;

    return (
      <BodyContainer>
        <AkGrid>
          <AkGridColumn medium="1" />
          <AkGridColumn medium="8">
            <AkGrid spacing="cosy">
              <AkGridColumn medium="4">
                <ColumnContent>
                  <img
                    src={require('./images/icon-SSO.svg')}
                    alt={formatMessage(messagesShared.sectionSAMLTitle)}
                  />
                  <SectionHeader>{formatMessage(messagesShared.sectionSAMLTitle)}</SectionHeader>
                  <p>{formatMessage(messagesShared.sectionSAMLCopy)}</p>
                </ColumnContent>
              </AkGridColumn>
              <AkGridColumn medium="4">
                <ColumnContent>
                  <img
                    src={require('./images/icon-2SV.svg')}
                    alt={formatMessage(messagesShared.sectionTwoStepTitle)}
                  />
                  <SectionHeader>{formatMessage(messagesShared.sectionTwoStepTitle)}</SectionHeader>
                  <p>{formatMessage(messagesShared.sectionTwoStepCopy)}</p>
                </ColumnContent>
              </AkGridColumn>
            </AkGrid>
            <AkGrid spacing="cosy">
              <AkGridColumn medium="4">
                <ColumnContentBottom>
                  <img
                    src={require('./images/icon-password.svg')}
                    alt={formatMessage(messagesShared.sectionPasswordTitle)}
                  />
                  <SectionHeader>{formatMessage(messagesShared.sectionPasswordTitle)}</SectionHeader>
                  <p>{formatMessage(messagesShared.sectionPasswordCopy)}</p>
                </ColumnContentBottom>
              </AkGridColumn>
              <AkGridColumn medium="4">
                <ColumnContentBottom>
                  <img
                    src={require('./images/icon-support.svg')}
                    alt={formatMessage(messagesShared.sectionSupportTitle)}
                  />
                  <SectionHeader>{formatMessage(messagesShared.sectionSupportTitle)}</SectionHeader>
                  <p>
                    {formatMessage(messagesShared.sectionSupportCopy)}
                  </p>
                </ColumnContentBottom>
              </AkGridColumn>
              <AkGridColumn medium="8">
                <CenteredText>
                    <div>
                      {formatMessage(messagesAtlassianAccess.sectionPricingCopy)}
                      <AnalyticsButton
                        href={links.external.atlassianAccessPricing}
                        onClick={this.onAtlassianAccessPricingClick}
                        appearance="link"
                        spacing="none"
                        target="_blank"
                        analyticsData={createOrgAnalyticsData({
                          action: 'click',
                          actionSubject: analyticsActionSubject,
                          actionSubjectId: 'learnAboutAtlassianAccessPricingLink',
                        })}
                      >
                        {formatMessage(messagesAtlassianAccess.sectionPricingLink)}
                      </AnalyticsButton>
                    </div>
                </CenteredText>
              </AkGridColumn>
            </AkGrid>
          </AkGridColumn>
          <AkGridColumn medium="1" />
        </AkGrid>
      </BodyContainer>
    );
  }

  private onAtlassianAccessPricingClick = () => {
    if (!this.props.pricingAnalyticsEvent) {
      return;
    }

    this.props.analyticsClient.sendUIEvent(this.props.pricingAnalyticsEvent);
  }
}

export const AtlassianAccessValuePropBody = withAnalyticsClient(injectIntl(AtlassianAccessValuePropBodyImpl));
