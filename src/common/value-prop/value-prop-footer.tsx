import * as React from 'react';
import styled from 'styled-components';

import { gridSize as akGridSize } from '@atlaskit/theme';

import { Centered } from 'common/styled/centered';

const FooterWrapper = styled.div`
  margin-top: ${akGridSize() * 3}px;
  margin-bottom: ${akGridSize() * 2}px;
`;

export class ValuePropFooter extends React.Component {
  public render() {
    return (
      <FooterWrapper>
        <Centered>
          {this.props.children}
        </Centered>
      </FooterWrapper>
    );
  }
}
