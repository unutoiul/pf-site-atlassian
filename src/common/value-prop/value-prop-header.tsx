import * as React from 'react';
import styled from 'styled-components';

import {
  Grid as AkGrid,
  GridColumn as AkGridColumn,
} from '@atlaskit/page';
import { gridSize as akGridSize } from '@atlaskit/theme';

const LinkWrapper = styled.div`
  margin-top: ${akGridSize() * 2}px;
`;

const DescriptionHeader = styled.h1`
  padding-top: ${akGridSize() * 4}px;
`;

const DescriptionWrapper = styled.div`
  width: ${akGridSize() * 45}px;
  margin-top: ${akGridSize() * 2}px;
  margin-bottom: ${akGridSize() * 2}px;
`;

const ImageWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
`;

const Image = styled.img`
  min-height: ${akGridSize() * 27}px;
`;

interface ValuePropHeaderProps {
  title: string;
  description: React.ReactNode;
  learnMoreLink?: React.ReactNode;
  imagePath: string;
}

export class ValuePropHeader extends React.Component<ValuePropHeaderProps> {
  public render() {
    const {
      title,
      description,
      learnMoreLink,
      imagePath,
    } = this.props;

    return (
      <AkGrid>
        <AkGridColumn medium={6}>
          <DescriptionHeader>
            {title}
          </DescriptionHeader>
          <DescriptionWrapper>
            {description}
          </DescriptionWrapper>
          {learnMoreLink && (
            <LinkWrapper>
              {learnMoreLink}
            </LinkWrapper>
          )}
        </AkGridColumn>
        <AkGridColumn medium={4}>
          <ImageWrapper>
            <Image src={imagePath} alt={undefined} role="presentation" />
          </ImageWrapper>
        </AkGridColumn>
      </AkGrid>
    );
  }
}
