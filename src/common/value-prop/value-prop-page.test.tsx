import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import AkSpinner from '@atlaskit/spinner';

import AkPage from '@atlaskit/page';

import { CenteredPageLayout } from 'common/page-layout';

import { createMockContext, createMockIntlProp } from '../../utilities/testing';
import { ValuePropPageImpl } from './value-prop-page';

describe('ValuePropPage', () => {
  let renderHeaderSpy;
  let renderBodySpy;
  let renderFooterSpy;

  const sandbox = sinon.sandbox.create();

  beforeEach(() => {
    renderHeaderSpy = sandbox.spy();
    renderBodySpy = sandbox.spy();
    renderFooterSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  const mountValuePropPage = ({
    loading = false,
    hasError = false,
  } = {}) => {
    const renderHeader = renderHeaderSpy;
    const renderBody = renderBodySpy;
    const renderFooter = renderFooterSpy;
    const props = {
      loading,
      hasError,
      renderHeader,
      renderBody,
      renderFooter,
    };

    const wrapper = mount(
      <AkPage>
        <ValuePropPageImpl
          intl={createMockIntlProp()}
          {...props}
        />
      </AkPage>,
      createMockContext({ intl: true, apollo: true }),
    );

    return {
      wrapper,
      renderHeader,
      renderBody,
      renderFooter,
    };
  };

  it('should render page', () => {
    const { wrapper } = mountValuePropPage();

    expect(wrapper.find(CenteredPageLayout).length).to.equal(1);
  });

  it('should render loading spinner when loading', () => {
    const { wrapper } = mountValuePropPage({ loading: true });

    expect(wrapper.find(AkSpinner).length).to.equal(1);
  });

  it('should show error message when hasError', () => {
    const { wrapper } = mountValuePropPage({ hasError: true });

    expect(wrapper.text()).to.include('Something went wrong');
  });
});
