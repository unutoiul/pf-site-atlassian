// tslint:disable jsx-use-translation-function react-this-binding-issue
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';

import AkButton, { ButtonGroup as AkButtonGroup } from '@atlaskit/button';
import AkPage from '@atlaskit/page';

import { ValuePropHeader } from 'common/value-prop';
import { ValuePropFooter } from 'common/value-prop/value-prop-footer';

import { createApolloClient } from '../../apollo-client';

import { createMockIntlProp } from '../../utilities/testing';
import { ValuePropPageImpl } from './value-prop-page';

const renderVPBody = () => (
  <p>{`Lorem ipsum dolor amet gastropub tbh slow-carb aesthetic forage tacos hell of kinfolk pitchfork. Kale chips fixie kitsch mlkshk disrupt kinfolk tilde venmo beard pug tofu tumeric roof party ramps adaptogen. Four loko microdosing XOXO, schlitz fashion axe actually gluten-free asymmetrical meditation VHS af activated charcoal salvia bespoke organic. Lyft activated charcoal sriracha, typewriter kitsch ennui fashion axe meggings woke. Keytar gentrify blog, master cleanse blue bottle hammock umami prism keffiyeh everyday carry glossier. Seitan quinoa heirloom waistcoat church-key poutine. Normcore kitsch raclette, pinterest brooklyn hella messenger bag. Umami flexitarian paleo, banh mi hammock farm-to-table distillery heirloom slow-carb pickled.

  Oh. You need a little dummy text for your mockup? How quaint.

  I bet you’re still using Bootstrap too…

  Atlassian Access is enabled`}</p>
);

const description = 'ixie etsy listicle, lomo small batch pitchfork four loko kinfolk before they sold out air plant ugh. Mustache etsy cloud bread pinterest skateboard fixie forage bicycle rights tattooed umami twee flexitarian chillwave fam venmo. Pabst swag truffaut marfa pinterest narwhal cronut fashion axe pork belly af banjo pug.';

const renderVPHeader = () => (
  <ValuePropHeader
    title={'Atlassian Access'}
    // tslint:disable-next-line:no-require-imports
    imagePath={require('../atlassian-access-value-prop/images/hero.svg')}
    description={description}
    learnMoreLink={
      <a href="#">
        Learn more
      </a>
    }
  />
);

const renderVPFooter = () => (
  <ValuePropFooter>
    <AkButtonGroup>
      <AkButton appearance="primary" >Do action</AkButton>
    </AkButtonGroup>
  </ValuePropFooter>
);

const client = createApolloClient();

const renderValuePropPage = ({
  loading = false,
  renderHeader = renderVPHeader,
  renderBody = renderVPBody,
  renderFooter = renderVPFooter,
  hasError = false,
} = {}) => (
    <ApolloProvider client={client}>
      <IntlProvider locale="en">
        <AkPage>
          <ValuePropPageImpl
            loading={loading}
            renderHeader={renderHeader}
            renderBody={renderBody}
            renderFooter={renderFooter}
            hasError={hasError}
            intl={createMockIntlProp()}
          />
        </AkPage>
      </IntlProvider>
    </ApolloProvider>
  );

storiesOf('Common|Value Prop Page', module)
  .add('Default', () => (
    renderValuePropPage()
  ))
  .add('Loading state', () => (
    renderValuePropPage({ loading: true })
  ))
  .add('Error state', () => (
    renderValuePropPage({ loading: false, hasError: true })
  ));
