import * as React from 'react';
import { InjectedIntlProps, injectIntl } from 'react-intl';

import AkSpinner from '@atlaskit/spinner';

import { GenericError } from 'common/error';
import { CenteredPageLayout } from 'common/page-layout';

interface OwnProps {
  isNavCollapsed?: boolean;
  renderHeader(): React.ReactNode;
  renderBody(): React.ReactNode;
  renderFooter(): React.ReactNode;
}

interface DerivedProps {
  loading: boolean;
  hasError: boolean;
}

export type ValuePropPageProps = OwnProps & DerivedProps & InjectedIntlProps;
export class ValuePropPageImpl extends React.Component<ValuePropPageProps> {
  public render() {
    const {
      loading,
      hasError,
      renderHeader,
      renderBody,
      renderFooter,
      isNavCollapsed = true,
    } = this.props;

    return (
      <CenteredPageLayout collapsedNav={isNavCollapsed}>
        {loading && <AkSpinner size="medium" />}
        {!loading && hasError && <GenericError />}
        {!loading && !hasError &&
          <div>
            {renderHeader()}
            {renderBody()}
            {renderFooter()}
          </div>
        }
      </CenteredPageLayout>
    );
  }
}

export const ValuePropPage = injectIntl<OwnProps>(ValuePropPageImpl);
