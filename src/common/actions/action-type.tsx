import * as React from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';

export type ActionType = 'remove' | 'edit';

const messages = defineMessages({
  edit: {
    defaultMessage: 'Edit',
    id: 'chrome.common.list-actions.edit',
  },
  remove: {
    defaultMessage: 'Remove',
    id: 'chrome.common.list-actions.remove',
  },
});

export function toMessage(actionType: ActionType): React.ReactNode {
  switch (actionType) {
    case 'remove': return <FormattedMessage {...messages.remove} />;
    case 'edit': return <FormattedMessage {...messages.edit} />;
    default: {
      if (__NODE_ENV__ !== 'production') {
        // tslint:disable-next-line:no-console
        console.error(`Unknown actionType: ${JSON.stringify(actionType)}; please check your usage of <Action /> component`);
      }

      return null;
    }
  }
}
