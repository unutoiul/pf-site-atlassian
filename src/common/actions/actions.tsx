import * as React from 'react';
import { InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import AkButton, { AkButtonProps } from '@atlaskit/button';
import AkDropdown, { AkDropdownItemProps, DropdownItem as AkDropdownItem } from '@atlaskit/dropdown-menu';
import AkMoreIcon from '@atlaskit/icon/glyph/more';
import AkTooltip from '@atlaskit/tooltip';
import { akGridSizeUnitless } from '@atlaskit/util-shared-styles';

import { Button, DropdownItem } from '../analytics';
import { Action, ActionProps } from './action';

function isActionNode(node: React.ReactChild): node is React.ReactElement<ActionProps> {
  return typeof node === 'object' && node.type === Action;
}

function getReactKeyForItem(item: React.ReactElement<ActionProps>, index: number): React.Key {
  if (item.props.actionType === 'custom') {
    return `${item.props.actionType}-${item.props.customMessage.id}`;
  }

  return item.props.actionType || item.key || index;
}

const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0 ${akGridSizeUnitless}px;
`;

export class ActionsImpl extends React.Component<InjectedIntlProps> {
  public render() {
    const { children } = this.props;

    const actionsArray = React.Children.toArray(children).filter(isActionNode);

    const dropdownButton = (
      <AkButton
        appearance="subtle"
        iconAfter={<AkMoreIcon label="" />}
      />
    );

    if (actionsArray.length === 0) {
      return null;
    }

    return (
      <Container>
        {this.renderFirstItem(actionsArray[0])}
        {actionsArray.length > 1 && (
          <AkDropdown
            trigger={dropdownButton}
            position="bottom right"
          >
            {actionsArray.slice(1).map(this.renderDropdownItem)}
          </AkDropdown>
        )}
      </Container>
    );
  }

  private wrapIfDisabled(item: React.ReactElement<ActionProps>, content: JSX.Element, key?: React.Key): JSX.Element {
    return item.props.isDisabled && item.props.disabledReason
      ? (
        <AkTooltip
          {...(typeof key !== 'undefined' ? { key } : {})}
          content={this.props.intl.formatMessage(item.props.disabledReason)}
        >
          {content}
        </AkTooltip>
      )
      : content;
  }

  private renderFirstItem(item: React.ReactElement<ActionProps>): JSX.Element {
    const buttonProps: AkButtonProps = {
      appearance: 'link',
      onClick: item.props.onAction, // tslint:disable-line no-unbound-method
      isDisabled: item.props.isDisabled,
    };

    const content = item.props.analyticsData
      ? (
        <Button
          {...buttonProps}
          analyticsData={item.props.analyticsData}
        >
          {item}
        </Button>
      )
      : (
        <AkButton {...buttonProps} >
          {item}
        </AkButton>
      );

    return this.wrapIfDisabled(item, content);
  }

  private renderDropdownItem = (item: React.ReactElement<ActionProps>, index: number): JSX.Element => {
    const key = getReactKeyForItem(item, index);
    const props: AkDropdownItemProps & React.Attributes = {
      onClick: item.props.onAction, // tslint:disable-line no-unbound-method
      isDisabled: item.props.isDisabled,
      key,
    };

    const content = item.props.analyticsData
      ? (
        <DropdownItem
          {...props}
          analyticsData={item.props.analyticsData}
        >
          {item}
        </DropdownItem>
      )
      : (
        <AkDropdownItem {...props} >
          {item}
        </AkDropdownItem>
      );

    return this.wrapIfDisabled(item, content, key);
  }
}

export const Actions = injectIntl<{}>(ActionsImpl);
