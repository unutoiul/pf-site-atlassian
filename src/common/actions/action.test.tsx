import { expect } from 'chai';
import { mount, ReactWrapper } from 'enzyme'; // "react-intl" needs to mount in order for it to translate messages correclt
import * as React from 'react';

import { createMockIntlContext } from '../../utilities/testing';
import { Action, ActionProps } from './action';

describe('Action', () => {
  function createWrapper(props: Partial<ActionProps>): ReactWrapper<ActionProps, {}> {
    const noop = () => void 0;

    return mount((
      <Action
        onAction={noop}
        {...props as any}
      />
    ),
      createMockIntlContext(),
    );
  }

  it('should be able to render built-in message', () => {
    const wrapper = createWrapper({
      actionType: 'edit',
    });

    expect(wrapper.text()).to.equal('Edit');
  });

  it('should be able to render custom message', () => {
    const wrapper = createWrapper({
      actionType: 'custom',
      customMessage: { defaultMessage: 'Test Message', id: 'test-id' },
    });

    expect(wrapper.text()).to.equal('Test Message');
  });
});
