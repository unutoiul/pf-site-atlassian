// tslint:disable jsx-use-translation-function no-console react-this-binding-issue
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';
import styled from 'styled-components';

import { AnalyticsListener as AkAnalyticsListener } from '@atlaskit/analytics';

import { Action } from './action';
import { Actions } from './actions';

const Container = styled.div`
  margin: 50px;
`;

const Box = styled.div`
  ${(props: { width?: number }) => props.width ? `width: ${props.width}px;` : ''}
  border: 1px solid gray;
  margin-bottom: 10px;
`;

storiesOf('Common|Actions', module)
  .add('Usage', () => (
    <IntlProvider locale="en">
      <Container>
        <p>
          The <code>{'<Actions/>'}</code> component accepts one or more <code>{'<Action />'}</code> components as its children.
        </p>
        <p>
          <strong>Note</strong>, that the borders around components are added by the storybook samples.
        </p>
        <Box width={150}>
          <Actions>
            <Action onAction={action('Triggered action 1')} actionType="edit" />
          </Actions>
        </Box>
        <p>
          The dots will be placed on the right-hand side of the container
        </p>
        <Box width={150}>
          <Actions>
            <Action onAction={action('Triggered action 1')} actionType="edit" />
            <Action onAction={action('Triggered action 2')} actionType="remove" />
          </Actions>
        </Box>
        <Box width={250}>
          <Actions>
            <Action onAction={action('Triggered action 1')} actionType="edit" />
            <Action onAction={action('Triggered action 2')} actionType="remove" />
          </Actions>
        </Box>
        <p>
          Analytics are baked into actions, so you only need to provide the appropriate <code>AnalyticsData</code>.
        </p>
        <p>
          Try triggering one of the below actions,
        </p>
        <Box width={150}>
          <AkAnalyticsListener onEvent={action('[Analytics]')}>
            <Actions>
              <Action
                onAction={action('Triggered action 1')}
                actionType="edit"
                analyticsData={{
                  action: 'click',
                  actionSubject: 'userList' as any,
                  actionSubjectId: 'action-1',
                  subproduct: 'chrome',
                  attributes: { random: 'attribute' },
                }}
              />
              <Action
                onAction={action('Triggered action 2')}
                actionType="remove"
                analyticsData={{
                  action: 'click',
                  actionSubject: 'userList' as any,
                  actionSubjectId: 'action-2',
                  subproduct: 'chrome',
                  attributes: { other: 'attribute' },
                }}
              />
            </Actions>
          </AkAnalyticsListener>
        </Box>
        <p>
          Custom actions types can either be defined by extending the <code>ActionType</code> enum, or by providing <code>actionType="custom"</code> along with a <code>customMessage</code> prop:
        </p>
        <Box width={250}>
          <Actions>
            <Action
              onAction={action('Triggered custom action 1')}
              actionType="custom"
              customMessage={{ defaultMessage: 'Custom Action 1', id: 'custom-1' }}
            />
            <Action
              onAction={action('Triggered custom action 2')}
              actionType="custom"
              customMessage={{ defaultMessage: 'Custom Action 2', id: 'custom-2' }}
            />
          </Actions>
        </Box>
        <p>
          Actions can be disabled, which is controlled by <code>isDisabled</code> prop. If that is the case, you need to specified the <code>disabledReason</code> property to indicate why this action is not available to the user:
        </p>
        <Box width={150}>
          <Actions>
            <Action
              actionType="edit"
              isDisabled={true}
              disabledReason={{ id: 'dis.msg.1', defaultMessage: 'You cannot do Action#1 because Y' }}
            />
            <Action
              actionType="remove"
              isDisabled={true}
              disabledReason={{ id: 'dis.msg.2', defaultMessage: 'You cannot do Action#2 because Z' }}
            />
            <Action
              onAction={action('Triggered custom action 3')}
              actionType="custom"
              customMessage={{ defaultMessage: 'Custom Action 3', id: 'custom3' }}
            />
          </Actions>
        </Box>
      </Container>
    </IntlProvider>
  ));
