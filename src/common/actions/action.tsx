import * as React from 'react';
import { FormattedMessage } from 'react-intl';

import { AnalyticsProps } from '../analytics';
import { ActionType, toMessage } from './action-type';

export type ActionProps = AnalyticsProps & {
  actionType: ActionType | 'custom';
  isDisabled?: boolean;
  disabledReason?: FormattedMessage.MessageDescriptor;
  customMessage?: FormattedMessage.MessageDescriptor;
  children?: never;
  onAction?(options?: object): void;
} & ({
  actionType: ActionType;
  customMessage?: never;
} | {
  actionType: 'custom';
  customMessage: FormattedMessage.MessageDescriptor;
});

export class Action extends React.Component<ActionProps> {
  public render() {
    const {
      actionType,
      customMessage,
    } = this.props;

    return actionType === 'custom'
      ? customMessage && <FormattedMessage {...customMessage} />
      : toMessage(actionType);
  }
}
