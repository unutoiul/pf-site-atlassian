// tslint:disable jsx-use-translation-function react-this-binding-issue prefer-function-over-method
import { expect } from 'chai';
import { mount, ReactWrapper } from 'enzyme'; // styled-components support for shallow sucks
import * as React from 'react';
import { InjectedIntl } from 'react-intl';
import { spy } from 'sinon';

import AkButton from '@atlaskit/button';
import {
  default as AkDropdown,
  DropdownItem as AkDropdownItem,
  DropdownMenuStateless as AkDropdownMenuStateless,
} from '@atlaskit/dropdown-menu';
import AkMoreIcon from '@atlaskit/icon/glyph/more';
import AkTooltip from '@atlaskit/tooltip';

import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import { Action } from './action';
import { ActionsImpl } from './actions';

describe('Actions', () => {
  function generateAction(index: number, handler: () => void = () => null) {
    return (
      <Action
        actionType="custom"
        customMessage={{ defaultMessage: `Action #${index}`, id: `action-${index}` }}
        onAction={handler}
      />
    );
  }

  class ActionsHelper {
    private wrapper: ReactWrapper<{}, {}>;

    constructor(node: React.ReactElement<{}>) {
      this.wrapper = mount(node, createMockIntlContext());
    }

    public get text(): string {
      return this.wrapper.text();
    }

    public get hasDropdown(): boolean {
      return this.dropdown.length > 0;
    }

    public get dropdownText(): string {
      this.openDropdown();

      return this.dropdown.text();
    }

    public triggerAction(zeroBasedIndex: number): void {
      if (zeroBasedIndex === 0) {
        this.triggerFirstAction();

        return;
      }

      this.openDropdown();
      this.wrapper.find(AkDropdownItem).at(zeroBasedIndex - 1).simulate('click');
    }

    public itemTooltipText(zeroBasedIndex: number): string | undefined {
      if (zeroBasedIndex === 0) {
        return this.getTooltipContent(this.firstActionTrigger);
      }

      this.openDropdown();

      return this.getTooltipContent(this.wrapper.find(AkDropdownItem).at(zeroBasedIndex - 1));
    }

    private getTooltipContent(elem: ReactWrapper<any, any>): string | undefined {
      const itemTooltip = elem.closest(AkTooltip);
      if (!itemTooltip || itemTooltip.length === 0) {
        return undefined;
      }

      return itemTooltip.at(0).prop('content');
    }

    private openDropdown(): void {
      if (!this.isDropdownOpen) {
        this.dropdownTrigger.simulate('click');
      }
    }

    private get firstActionTrigger() {
      return this.wrapper.find(AkButton).at(0);
    }

    private triggerFirstAction(): void {
      this.firstActionTrigger.simulate('click');
    }

    private get isDropdownOpen(): boolean {
      // this relies on atlaskit internals, see here if this stops working
      // https://bitbucket.org/atlassian/atlaskit-mk-2/src/master/packages/elements/dropdown-menu/src/components/DropdownMenu.js
      return this.dropdown.find(AkDropdownMenuStateless).prop('isOpen');
    }

    private get dropdown() {
      return this.wrapper.find(AkDropdown);
    }

    private get dropdownTrigger() {
      return this.wrapper.find(AkMoreIcon).at(0);
    }
  }

  let intl: InjectedIntl;

  beforeEach(() => {
    intl = createMockIntlProp();
  });

  it('should be able to render a single action', () => {
    const helper = new ActionsHelper((
      <ActionsImpl intl={intl}>
        {generateAction(1)}
      </ActionsImpl>
    ));

    expect(helper.text).to.equal('Action #1');
    expect(helper.hasDropdown).to.equal(false);
  });

  it('should handle multiple actions', () => {
    const helper = new ActionsHelper((
      <ActionsImpl intl={intl}>
        {generateAction(1)}
        {generateAction(2)}
      </ActionsImpl>
    ));

    expect(helper.text).to.equal('Action #1');
    expect(helper.hasDropdown).to.equal(true);
  });

  it('should present all actions except the first one inside the dropdown', () => {
    const helper = new ActionsHelper((
      <ActionsImpl intl={intl}>
        {generateAction(1)}
        {generateAction(2)}
        {generateAction(3)}
      </ActionsImpl>
    ));

    expect(helper.dropdownText).to.include('Action #2');
    expect(helper.dropdownText).to.include('Action #3');
  });

  it('should trigger appropriate callbacks when the actions are clicked', () => {
    const spies = [spy(), spy(), spy()];

    const helper = new ActionsHelper((
      <ActionsImpl intl={intl}>
        {generateAction(1, spies[0])}
        {generateAction(2, spies[1])}
        {generateAction(3, spies[2])}
      </ActionsImpl>
    ));

    helper.triggerAction(0);
    helper.triggerAction(1);
    helper.triggerAction(2);
    spies.forEach(s => {
      expect(s.callCount).to.equal(1);
    });
  });

  describe('disabled state', () => {
    it('should not trigger actions on disabled items', () => {
      const spies = [spy(), spy(), spy()];

      const helper = new ActionsHelper((
        <ActionsImpl intl={intl}>
          <Action
            actionType="custom"
            customMessage={{ defaultMessage: `Action #0`, id: `action-0` }}
            onAction={spies[0]}
            isDisabled={true}
            disabledReason={{ id: 'msg-0', defaultMessage: 'msg-0' }}
          />
          <Action
            actionType="custom"
            customMessage={{ defaultMessage: `Action #1`, id: `action-1` }}
            onAction={spies[1]}
            isDisabled={true}
            disabledReason={{ id: 'msg-1', defaultMessage: 'msg-1' }}
          />
          <Action
            actionType="custom"
            customMessage={{ defaultMessage: `Action #2`, id: `action-2` }}
            onAction={spies[2]}
            isDisabled={true}
            disabledReason={{ id: 'msg-2', defaultMessage: 'msg-2' }}
          />
        </ActionsImpl>
      ));

      helper.triggerAction(0);
      helper.triggerAction(1);
      helper.triggerAction(2);
      spies.forEach(s => {
        expect(s.callCount).to.equal(0);
      });
    });

    it('should have a tooltip for each disabled action', () => {
      const spies = [spy(), spy(), spy()];

      const helper = new ActionsHelper((
        <ActionsImpl intl={intl}>
          <Action
            actionType="custom"
            customMessage={{ defaultMessage: `Action #0`, id: `action-0` }}
            onAction={spies[0]}
            isDisabled={true}
            disabledReason={{ id: 'msg-0', defaultMessage: 'Message 0' }}
          />
          <Action
            actionType="custom"
            customMessage={{ defaultMessage: `Action #1`, id: `action-1` }}
            onAction={spies[1]}
          />
          <Action
            actionType="custom"
            customMessage={{ defaultMessage: `Action #2`, id: `action-2` }}
            onAction={spies[2]}
            isDisabled={true}
            disabledReason={{ id: 'msg-2', defaultMessage: 'Message 2' }}
          />
        </ActionsImpl>
      ));

      expect(helper.itemTooltipText(0)).to.equal('Message 0');
      expect(helper.itemTooltipText(1)).to.equal(undefined);
      expect(helper.itemTooltipText(2)).to.equal('Message 2');
    });
  });
});
