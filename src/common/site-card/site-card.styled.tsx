import styled from 'styled-components';

import {
  colors as akColors,
  fontSize as akFontSize,
  gridSize as akGridSize,
} from '@atlaskit/theme';

export const ActionsWrapper = styled.div`
  display: flex;
  margin-left: ${akGridSize() * 2}px;
  flex-basis: auto;
  flex-shrink: 0;
`;

export const ProductWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  &:not(:last-child) {
    margin-bottom: ${akGridSize() * 2}px;
  }
`;

const normalLineHeight = 20 / 14;

export const ProductListWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 ${akGridSize}px;
`;

export const SiteName = styled.div`
  font-weight: 600;
  color: ${akColors.N800};
  line-height: ${normalLineHeight};
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
`;

export const SiteUrl = styled.div`
  color: ${akColors.N200};
  font-size: ${akFontSize() - 2}px;
  line-height: ${normalLineHeight};
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
`;

export const ProductNameWrapper = styled.div`
  margin-left: ${akGridSize() / 2}px;
  font-weight: 400;
`;
