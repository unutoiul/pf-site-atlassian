import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';
import { BrowserRouter as Router } from 'react-router-dom';
import styled from 'styled-components';

import { launchpadScreen } from 'common/analytics';

import { SiteCard, SiteCardProps } from './site-card';

const siteWithProducts = {
  id: 'my-site',
  displayName: 'acme-usa',
  siteUrl: 'acme-usa.atlassian.net',
  avatar: '',
  products: [
    {
      key: 'bitbucket',
      name: 'Bitbucket',
    },
    {
      key: 'confluence',
      name: 'Confluence',
    },
    {
      key: 'jira-ops',
      name: 'Jira Ops',
    },
  ],
};

const siteWithoutProducts = {
  id: 'my-site',
  displayName: 'acme-asia',
  siteUrl: 'acme-asia.atlassian.net',
  avatar: '',
  products: [],
};

const WrapperPadding = styled.div`
  padding: 20px;
`;

const StorybookSiteCard = (props: SiteCardProps) => (
  <IntlProvider locale="en">
    <Router>
      <WrapperPadding>
        <SiteCard {...props} />
      </WrapperPadding>
    </Router>
  </IntlProvider>
);

storiesOf('Common|Site Card', module)
  .add('Default', () => (
    <StorybookSiteCard
      site={siteWithProducts}
      orgId="test-org-id"
      screen={launchpadScreen}
    />
  ))
  .add('No products', () => (
    <StorybookSiteCard
      site={siteWithoutProducts}
      orgId="test-org-id"
      screen={launchpadScreen}
    />
  ))
  ;
