import * as React from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';

import AkAvatar from '@atlaskit/avatar';
import AkButton, { ButtonGroup as AkButtonGroup } from '@atlaskit/button';
import AkDropdown from '@atlaskit/dropdown-menu';
import AkMoreIcon from '@atlaskit/icon/glyph/more';

import { AnalyticsClientProps, DropdownItem, siteCardGrantProductAccessLink, siteCardLinkClickEventData, siteCardManageGroupsLink, siteCardManageUsersLink, siteCardViewBillingDropdownLink, withAnalyticsClient } from 'common/analytics';
import { Card } from 'common/card';
import { ProductIcon, ProductName } from 'common/icons';
import { Centered } from 'common/styled';
import { EmptyTableView } from 'common/table';

import { UIData } from 'common/analytics/new-analytics-types';

import { OrgSite, Product } from '../../schema/schema-types';
import {
  ActionsWrapper,
  ProductListWrapper,
  ProductNameWrapper,
  ProductWrapper,
  SiteName,
  SiteUrl,
} from './site-card.styled';

const messages = defineMessages({
  noProducts: {
    id: 'site.card.no.products.message',
    defaultMessage: 'This site has no currently active products',
    description: 'This text is displayed when a site card has no associated products.',
  },
  manageUsers: {
    id: 'site.card.manage.users',
    defaultMessage: 'Manage users',
    description: 'A link from a site card to a screen where you can do admin tasks on user accounts',
  },
  viewBill: {
    id: 'site.card.view.bill',
    defaultMessage: 'View site bill',
    description: `A link from a site card to a screen where you can manage your site's bill`,
  },
  manageGroups: {
    id: 'site.card.manage.groups',
    defaultMessage: 'Manage groups',
    description: 'A link from a site card to a screen where you can add and remove users from groups',
  },
  grantProductAccess: {
    id: 'site.card.product.access',
    defaultMessage: 'Grant product access',
    description: 'A link from a site card to a screen where you can give users access to Atlassian products',
  },
  viewBilling: {
    id: 'site.card.view.billing',
    defaultMessage: 'View billing',
    description: 'A link from a site card to a screen where you can view billing estimates and manage billing information',
  },
});

const Product: React.SFC<{ name: ProductName }> = ({ name }) => (
  <ProductWrapper>
    <ProductIcon productName={name} />
    <ProductNameWrapper>{name}</ProductNameWrapper>
  </ProductWrapper>
);

const Heading: React.SFC<{ siteName: string, siteUrl: string }> = ({ siteName, siteUrl }) => (
  <React.Fragment>
    <SiteName>{siteName}</SiteName>
    <SiteUrl>{siteUrl}</SiteUrl>
  </React.Fragment>
);

export interface SiteCardProps {
  site: OrgSite;
  orgId?: string;
  screen: string;
}

export class SiteCardImpl extends React.Component<SiteCardProps & AnalyticsClientProps> {
  public render() {
    const { site } = this.props;

    return (
      <Card
        href={`https://${site.siteUrl}/admin`}
        isExternalLink={true}
        icon={<AkAvatar appearance="square" size="large" src={site.avatar} />}
        mainContent={<Heading siteName={site.displayName || site.siteUrl} siteUrl={site.siteUrl} />}
        details={this.renderProductSection()}
        onClick={this.onCardClick}
        actions={this.renderActions()}
      />
    );
  }

  private onCardClick = () => {
    const {
      site: { id },
      orgId,
      screen,
      analyticsClient: { sendUIEvent },
    } = this.props;

    sendUIEvent({
      orgId,
      data: siteCardLinkClickEventData(id, screen),
    });
  }

  private renderProductSection = () => {
    const { products } = this.props.site;

    if (products.length === 0) {
      return (
        <Centered>
          <EmptyTableView>
            <FormattedMessage {...messages.noProducts} />
          </EmptyTableView>
        </Centered>
      );
    }

    return (
      <ProductListWrapper>
        {products.map(product => (
          <Product key={product.key} name={product.name as ProductName} />),
        )}
      </ProductListWrapper>
    );
  };

  private renderActions = () => {
    return (
      <ActionsWrapper>
        <AkButtonGroup>
          <AkButton href={`https://${this.props.site.siteUrl}/admin/users`} onClick={this.sendManageUsersAnalyticsEvent}>
            <FormattedMessage {...messages.manageUsers}/>
          </AkButton>
          <AkButton href={`https://${this.props.site.siteUrl}/admin/billing/overview`} onClick={this.sendManageUsersAnalyticsEvent}>
            <FormattedMessage {...messages.viewBill}/>
          </AkButton>

          <AkDropdown
            trigger={(
              <AkButton
                iconAfter={<AkMoreIcon label="" />}
              />
            )}
            position="bottom left"
          >
            <DropdownItem href={`https://${this.props.site.siteUrl}/admin/groups`} analyticsData={this.getLinkClickAnalyticsEvent(siteCardManageGroupsLink).data}>
              <FormattedMessage {...messages.manageGroups} />
            </DropdownItem>
            <DropdownItem href={`https://${this.props.site.siteUrl}/admin/apps`} analyticsData={this.getLinkClickAnalyticsEvent(siteCardGrantProductAccessLink).data}>
              <FormattedMessage {...messages.grantProductAccess}/>
            </DropdownItem>
            <DropdownItem href={`https://${this.props.site.siteUrl}/admin/s/${this.props.site.id}/billing/overview`} analyticsData={this.getLinkClickAnalyticsEvent(siteCardViewBillingDropdownLink).data}>
              <FormattedMessage {...messages.viewBilling}/>
            </DropdownItem>
          </AkDropdown>
        </AkButtonGroup>
      </ActionsWrapper>
    );
  };

  private getLinkClickAnalyticsEvent = (event: (id: string, screen: string, isLinkedSite: boolean) => UIData) => {
    const {
      screen,
      site: {
        id,
      },
      orgId,
    } = this.props;

    return { data: event(id, screen, orgId !== undefined) };
  }

  private sendManageUsersAnalyticsEvent = () => {
    this.props.analyticsClient.sendUIEvent(this.getLinkClickAnalyticsEvent(siteCardManageUsersLink));
  };
}

export const SiteCard = withAnalyticsClient(SiteCardImpl);
