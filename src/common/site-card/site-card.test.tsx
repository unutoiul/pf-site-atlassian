import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import * as React from 'react';
import { IntlProvider } from 'react-intl';
import { MemoryRouter } from 'react-router';

import AkAvatar from '@atlaskit/avatar';
import AkButton from '@atlaskit/button';

import { DropdownItem, launchpadScreen } from 'common/analytics';
import { SiteCardImpl } from 'common/site-card/site-card';

import { OrgSite } from '../../schema/schema-types';
import { createMockAnalyticsClient } from '../../utilities/testing';

const testSite: OrgSite = {
  id: 'site-id',
  displayName: 'acme-usa',
  siteUrl: 'acme-usa.atlassian.com',
  avatar: '',
  products: [],
};

const testProducts = [
  {
    key: 'bitbucket',
    name: 'Bitbucket',
  },
  {
    key: 'jira-software',
    name: 'Jira Software',
  },
];

const analyticsClient = createMockAnalyticsClient();

const mountSiteCard = ({ site, products }) => {
  return mount(
    <IntlProvider locale="en">
      <MemoryRouter>
        <SiteCardImpl
          site={{ ...site, products }}
          orgId="test-org-id"
          analyticsClient={analyticsClient}
          screen={launchpadScreen}
        />
      </MemoryRouter>
    </IntlProvider>,
  );
};

describe('SiteCard', () => {

  afterEach(() => {
    analyticsClient.reset();
  });

  const mountExampleSiteCard = () => mountSiteCard({
    site: testSite,
    products: testProducts,
  });

  const mountSiteCardWithNoProducts = () => mountSiteCard({
    site: testSite,
    products: [],
  });

  it('should render site info', () => {
    const wrapper = mountExampleSiteCard();

    const avatar = wrapper.find(AkAvatar);

    expect(avatar.length).to.equal(1);
    expect(wrapper.text().includes('acme-usa')).to.equal(true);
  });

  it('should render product info', () => {
    const wrapper = mountExampleSiteCard();

    const products = wrapper.find('Product');

    expect(products.length).to.equal(2);
    expect(products.first().text().includes('Bitbucket')).to.equal(true);
  });

  it('should display a message when product list is empty', () => {
    const wrapper = mountSiteCardWithNoProducts();

    expect(wrapper.text().includes('This site has no currently active products')).to.equal(true);
  });

  it('should trigger analytics when clicking on the manage users action', () => {
    const wrapper = mountSiteCard({
      site: testSite,
      products: testProducts,
    });

    wrapper.find(AkButton).at(0).props().onClick!();

    expect(analyticsClient.sendUIEvent.getCalls()[0].args[0].data.actionSubjectId).to.equal('siteManageUsersLink');
  });

  it('should trigger pass the analytics data to the manage groups action', () => {
    const wrapper = shallow(
      <IntlProvider locale="en">
        <MemoryRouter>
          <SiteCardImpl
            site={testSite}
            orgId="test-org-id"
            analyticsClient={analyticsClient}
            screen={launchpadScreen}
          />
        </MemoryRouter>
      </IntlProvider>,
    );
    const cardWrapper = wrapper.find(SiteCardImpl).dive().dive();

    expect(cardWrapper.find(DropdownItem).at(0).props().analyticsData!.actionSubjectId).to.equal('siteManageGroupsDropdownLink');
  });

  it('should trigger pass the analytics data to the grant product access action', () => {
    const wrapper = shallow(
      <IntlProvider locale="en">
        <MemoryRouter>
          <SiteCardImpl
            site={testSite}
            orgId="test-org-id"
            analyticsClient={analyticsClient}
            screen={launchpadScreen}
          />
        </MemoryRouter>
      </IntlProvider>,
    );
    const cardWrapper = wrapper.find(SiteCardImpl).dive().dive();

    expect(cardWrapper.find(DropdownItem).at(1).props().analyticsData!.actionSubjectId).to.equal('siteProductAccessDropdownLink');
  });

  it('should trigger pass the analytics data to the view billing action', () => {
    const wrapper = shallow(
      <IntlProvider locale="en">
        <MemoryRouter>
          <SiteCardImpl
            site={testSite}
            orgId="test-org-id"
            analyticsClient={analyticsClient}
            screen={launchpadScreen}
          />
        </MemoryRouter>
      </IntlProvider>,
    );
    const cardWrapper = wrapper.find(SiteCardImpl).dive().dive();

    expect(cardWrapper.find(DropdownItem).at(2).props().analyticsData!.actionSubjectId).to.equal('siteViewBillingDropdownLink');
  });
});
