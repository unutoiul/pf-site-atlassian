import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import { connect, DispatchProp } from 'react-redux';
import styled from 'styled-components';

import { atlassianAccessLearnMoreEventData, AtlassianAccessLearnMoreScreen, UIEvent } from 'common/analytics';
import { IdentityManagerLearnMoreButton } from 'common/identity-manager-learn-more-button';
import { PageSidebar } from 'common/page-layout';

const messages = defineMessages({
  title: {
    id: 'identity.manager.evaluate.title',
    defaultMessage: 'Ensure security compliance',
  },
  button: {
    id: 'identity.manager.evaluate.button',
    defaultMessage: 'Learn more',
  },
});

const Description = styled.div`
  display: flex;
  align-items: center;
`;

// tslint:disable-next-line:no-var-requires no-require-imports
const Cloud = require('./cloud.svg');

export interface IdentityManagerEvaluationCardProps {
  orgId: string;
  analyticsScreenForEvalButton: AtlassianAccessLearnMoreScreen;
  cardDescription: string;
}

export class IdentityManagerEvaluationCardImpl extends React.PureComponent<IdentityManagerEvaluationCardProps & DispatchProp<any> & InjectedIntlProps> {
  public render() {

    const { formatMessage } = this.props.intl;

    return (
      <PageSidebar>
        <h2>{formatMessage(messages.title)}</h2>
        <Description>
          <p>{this.props.cardDescription}</p>
          <img width="30%" alt={formatMessage(messages.title)} src={Cloud} />
        </Description>
        <IdentityManagerLearnMoreButton
          isPrimary={true}
          analyticsUIEvent={this.createAtlassianAccessLearnMoreAnalyticsEvent()}
        />
      </PageSidebar>
    );
  }

  private createAtlassianAccessLearnMoreAnalyticsEvent = (): UIEvent => {
    return {
      orgId: this.props.orgId,
      data: atlassianAccessLearnMoreEventData(this.props.analyticsScreenForEvalButton),
    };
  }
}

export const IdentityManagerEvaluationCard = connect()(injectIntl(IdentityManagerEvaluationCardImpl));
