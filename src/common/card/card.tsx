import * as React from 'react';
import styled from 'styled-components';

import AkItem from '@atlaskit/item';
import {
  borderRadius as akBorderRadius,
  colors as akColors,
  gridSize as akGridSize,
} from '@atlaskit/theme';

import { tablet } from 'common/responsive/breakpoints';
import { depthZ100 } from 'common/styled';

import { getCardLinkComponent } from './card-link';

const CardContainer = styled.div`
  background-color: ${
    ({ color }: { color: CardColor }) => color === 'light'
      ? akColors.N0
      : akColors.N20
  };
  border-radius: ${akBorderRadius}px;
  padding: ${akGridSize() * 2}px;
  ${depthZ100}

  &:not(:last-child) {
    margin-bottom: ${akGridSize() * 1.5}px;
  }
`;

export const LineSeparator = styled.div`
  height: ${akGridSize() / 4}px;
  border-radius: ${akBorderRadius}px;
  background-color: ${akColors.N40};
  margin: ${akGridSize() * 2}px 0;
`;

export const SpaceSeparator = styled.div`
  margin-top: ${akGridSize() * 2}px;
`;

const MainDataContainer = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;

  @media (min-width: ${tablet}px) {
    flex-wrap: nowrap;
  }
`;

type CardColor = 'light' | 'dark';

export interface CardProps {
  href: string;
  icon: React.ReactNode;
  mainContent: React.ReactNode;
  isExternalLink?: boolean;
  actions?: React.ReactNode;
  details?: React.ReactNode;
  detailsSeparator?: 'line' | 'space';
  color?: CardColor;
  onClick?(): void;
}

export class Card extends React.Component<CardProps> {
  public static defaultProps = {
    color: 'light',
    detailsSeparator: 'line',
  };

  public render() {
    const {
      href,
      icon,
      mainContent,
      isExternalLink,
      actions,
      onClick,
      color,
    } = this.props;

    const linkComponent = getCardLinkComponent(isExternalLink);

    return (
      <CardContainer color={color!}>
        <MainDataContainer>
          <AkItem
            href={href}
            linkComponent={linkComponent}
            elemBefore={icon}
            onClick={onClick}
          >
            {mainContent}
          </AkItem>

          {actions}
        </MainDataContainer>

        {this.renderDetails()}
      </CardContainer>
    );
  }

  private renderDetails = () => {
    const { details, detailsSeparator } = this.props;

    if (!details) {
      return null;
    }

    return detailsSeparator === 'space' ? (
      <SpaceSeparator>
        {details}
      </SpaceSeparator>
    ) : (
      <React.Fragment>
        <LineSeparator />
        {details}
      </React.Fragment>
    );
  }
}
