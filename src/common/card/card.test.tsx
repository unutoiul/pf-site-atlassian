import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import itParam from 'mocha-param';
import * as React from 'react';
import { MemoryRouter as Router } from 'react-router-dom';
import { spy } from 'sinon';

import AkItem from '@atlaskit/item';

import { Card, CardProps, LineSeparator, SpaceSeparator } from './card';

describe('Card', () => {

  const shallowCard = ({ separator, details }: { separator?: CardProps['detailsSeparator'], details?: React.ReactNode } = {}) => {
    return shallow((
      <Card
        href="some-href"
        icon={null}
        mainContent={null}
        detailsSeparator={separator}
        details={details}
      />),
    );
  };

  // tslint:disable-next-line:no-invalid-template-strings "mocha-param" is substituting the value, so no need for a template string
  itParam('should trigger onClick handler when AkItem is clicked when "isExternalLink" is ${value}', [true, false], (value) => {
    const onClickSpy = spy();

    // we explicitly use "mount" here to test integration with AkItem
    const wrapper = mount((
      <Router>
        <Card
          href="some-href"
          icon={null}
          isExternalLink={value}
          mainContent={null}
          onClick={onClickSpy}
        />
      </Router>
    ),
    );

    wrapper.find(AkItem).simulate('click');
    wrapper.update();

    expect(onClickSpy.callCount).to.equal(1);
  });

  it('should render in with details and line separator', () => {
    const wrapper = shallowCard({ separator: 'line', details: 'hello' });
    const lineSeparator = wrapper.find(LineSeparator);
    const backgroundSeparator = wrapper.find(SpaceSeparator);

    expect(lineSeparator.exists()).to.equal(true);
    expect(backgroundSeparator.exists()).to.equal(false);
  });

  it('should render with details and space separator', () => {
    const wrapper = shallowCard({ separator: 'space', details: 'hello' });
    const lineSeparator = wrapper.find(LineSeparator);
    const backgroundSeparator = wrapper.find(SpaceSeparator);

    expect(lineSeparator.exists()).to.equal(false);
    expect(backgroundSeparator.exists()).to.equal(true);
  });

  it('should not render separator with no details', () => {
    const wrapper = shallowCard();
    const lineSeparator = wrapper.find(LineSeparator);
    const backgroundSeparator = wrapper.find(SpaceSeparator);

    expect(lineSeparator.exists()).to.equal(false);
    expect(backgroundSeparator.exists()).to.equal(false);
  });
});
