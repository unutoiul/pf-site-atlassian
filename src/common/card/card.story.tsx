// tslint:disable jsx-use-translation-function
import { action } from '@storybook/addon-actions';
import { boolean } from '@storybook/addon-knobs';
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { MemoryRouter as Router } from 'react-router-dom';

import AkButton, { ButtonGroup as AkButtonGroup } from '@atlaskit/button';
import AkLightbulbIcon from '@atlaskit/icon/glyph/lightbulb';

import { Card } from './card';

storiesOf('Common|Card', module)
  .add('Default', () => {
    const isExternalLink = boolean('Is external link', false);
    const hasActions = boolean('Has actions', false);
    const hasDetails = boolean('Has details', false);
    const hasOnClick = boolean('Has on click', false);
    const lineSeparator = boolean('Line separator', true);
    const lightColor = boolean('Light color', true);

    return (
      <Router>
        <Card
          key={JSON.stringify({ isExternalLink, hasActions, hasDetails, hasOnClick })}
          href={isExternalLink ? 'https://atlassian.com' : '/'}
          icon={<AkLightbulbIcon label="" />}
          mainContent="Some content"
          actions={hasActions ? (
            <AkButtonGroup>
              <AkButton onClick={action('Action')}>Action</AkButton>
              <AkButton onClick={action('Other action')}>Other action</AkButton>
            </AkButtonGroup>
          ) : undefined}
          details={hasDetails ? <p>Here are some details about the content. This can be anything</p> : undefined}
          isExternalLink={isExternalLink}
          onClick={hasOnClick ? action('onClick') : undefined}
          detailsSeparator={lineSeparator ? 'line' : 'space'}
          color={lightColor ? 'light' : 'dark'}
        />
      </Router>
    );
  },
  );
