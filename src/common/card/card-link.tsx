// tslint:disable max-classes-per-file
import * as React from 'react';
import { Link as ReactRouterLink } from 'react-router-dom';
import styled, { css } from 'styled-components';

import {
  colors as akColors,
  fontSize as akFontSize,
} from '@atlaskit/theme';

import { ExternalLink } from 'common/navigation';
import { Centered } from 'common/styled';

const cardLinkMixin = css`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  font-size: ${akFontSize}px;
  font-weight: 500;

  &, &:hover {
    color: ${akColors.heading};
  }
`;

const StyledRouterLink = styled(ReactRouterLink)`
  ${cardLinkMixin}
`;

const StyledExternalLink = styled(ExternalLink)`
  ${cardLinkMixin}
`;

// the only props we can rely on to be passed down from AkItem are "href" and "target"
type ReliableAkItemLinkComponentProps = {
  href: string;
  // we are testing our integration with this prop in `card.test.tsx`
  onClick?(): void;
} & React.Component['props'];

class CardLink extends React.Component<ReliableAkItemLinkComponentProps> {
  public render() {
    const {
      href,
      onClick,
      children,
    } = this.props;

    return (
      <StyledRouterLink to={href} onClick={onClick}>
        <Centered>
          {children}
        </Centered>
      </StyledRouterLink>
    );
  }
}

class CardExternalLink extends React.Component<ReliableAkItemLinkComponentProps> {
  public render() {
    const {
      href,
      onClick,
      children,
    } = this.props;

    return (
      <StyledExternalLink href={href} onClick={onClick}>
        <Centered>
          {children}
        </Centered>
      </StyledExternalLink>
    );
  }
}

export function getCardLinkComponent(
  isExternal: boolean | undefined,
  // this needs to be a class, otherwise React gives a warning:
  // > Stateless function components cannot be given refs. Attempts to access this ref will fail
  // most likely that's due to atlaskit internals
): React.ComponentClass<ReliableAkItemLinkComponentProps> {
  return isExternal ? CardExternalLink : CardLink;
}
