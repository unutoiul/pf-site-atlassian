// tslint:disable jsx-use-translation-function
import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import { IdentityManagerLearnMoreButton } from 'common/identity-manager-learn-more-button';

import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import { IdentityManagerEvaluationCardImpl } from './identity-manager-evaluation-card';

describe('Identity Manager Evaluation Card', () => {

  function shallowIdentityManagerCard() {
    return shallow(
      <IdentityManagerEvaluationCardImpl
        orgId="DUMMY-TEST-ORG-ID"
        analyticsActionSubject="testPageActionSubject"
        cardDescription="Test Description"
        intl={createMockIntlProp()}
        {...{} as any}
      />,
      createMockIntlContext(),
    );
  }

  it('should render UI elements correctly', () => {
    const wrapper = shallowIdentityManagerCard();
    expect(wrapper.contains(<h2>Ensure security compliance</h2>)).to.equal(true);
    expect(wrapper.contains(<p>Test Description</p>)).to.equal(true);
    const learnMoreButton = wrapper.find(IdentityManagerLearnMoreButton);
    expect(learnMoreButton).to.have.length(1);
  });
});
