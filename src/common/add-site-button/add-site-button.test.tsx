import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import AkButton from '@atlaskit/button';

import { launchpadAddSiteEvent, launchpadScreen } from 'common/analytics';
import { Action, ActionSubject } from 'common/analytics/new-analytics-types';

import { waitUntil } from '../../utilities/testing';
import { AddSiteButtonImpl } from './add-site-button';

describe('AddSiteButton', () => {

  const orgId = 'test-org';
  const sandbox = sinon.sandbox.create();
  const historyPushSpy = sandbox.spy();
  const sendUIEventSpy = sandbox.spy();

  afterEach(() => {
    sandbox.reset();
  });

  const shallowAddSiteButton = () => {
    return shallow((
      <AddSiteButtonImpl
        appearance="default"
        orgId={orgId}
        analyticsData={launchpadAddSiteEvent}
        analyticsClient={{ sendUIEvent: sendUIEventSpy } as any}
        history={{ push: historyPushSpy } as any}
        match={{} as any}
        location={{} as any}
      />
    ));
  };

  it('should render', () => {
    const wrapper = shallowAddSiteButton();
    const button = wrapper.find(AkButton);

    expect(button.exists()).to.equal(true);
  });

  it('should set route when clicked', async () => {
    const wrapper = shallowAddSiteButton();
    const button = wrapper.find(AkButton);

    button.simulate('click');
    await waitUntil(() => historyPushSpy.callCount > 0);

    expect(historyPushSpy.calledWithExactly(`/o/${orgId}/org-sites`)).to.equal(true);
  });

  it('should send UI event when clicked', async () => {
    const wrapper = shallowAddSiteButton();
    const button = wrapper.find(AkButton);

    button.simulate('click');
    await waitUntil(() => sendUIEventSpy.callCount > 0);

    expect(sendUIEventSpy.called).to.equal(true);
    expect(sendUIEventSpy.calledWithExactly({
      orgId,
      data: {
        action: Action.Clicked,
        actionSubject: ActionSubject.Button,
        actionSubjectId: 'addSiteButton',
        source: launchpadScreen,
      },
    })).to.equal(true);
  });
});
