import * as React from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';

import AkButton, { AkButtonProps } from '@atlaskit/button';

import { AnalyticsClientProps, withAnalyticsClient } from 'common/analytics';
import { UIData } from 'common/analytics/new-analytics-types';

const messages = defineMessages({
  addSite: {
    id: 'common.add.site.button',
    defaultMessage: 'Add site',
    description: 'This is a button used to add a site to an organization.',
  },
});

interface OwnProps {
  appearance: AkButtonProps['appearance'];
  orgId: string;
  analyticsData(): UIData;
}

export class AddSiteButtonImpl extends React.Component<OwnProps & AnalyticsClientProps & RouteComponentProps<any>> {
  public render() {
    return (
      <AkButton onClick={this.openOrgSiteLinkingDrawer} appearance={this.props.appearance}>
        <FormattedMessage {...messages.addSite} />
      </AkButton>
    );
  }

  private openOrgSiteLinkingDrawer = () => {
    const { analyticsClient, analyticsData, orgId, history } = this.props;

    analyticsClient.sendUIEvent({
      orgId,
      data: analyticsData(),
    });

    history.push(`/o/${orgId}/org-sites`);
  }
}

export const AddSiteButton =
  withRouter(
    withAnalyticsClient(
      AddSiteButtonImpl,
    ),
  )
;
