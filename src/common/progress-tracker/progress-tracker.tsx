import * as React from 'react';
import styled from 'styled-components';

import AkCheckCircleIcon from '@atlaskit/icon/glyph/check-circle';
import AkRadioIcon from '@atlaskit/icon/glyph/radio';
import { gridSize as akGridSize } from '@atlaskit/theme';
import { akColorB400, akColorN50 } from '@atlaskit/util-shared-styles';

import { dashedLineImage } from 'common/images';

export enum ProgressState {
  Completed,
  Incomplete,
  Selected,
}

export interface ProgressItem {
  item: React.ReactNode;
  state: ProgressState;
}

interface OwnProps {
  items: ProgressItem[];
}

const IconWrapper = styled.span`
  align-self: center;
`;

export const CompletedIcon = props => <IconWrapper><AkCheckCircleIcon {...props} primaryColor={akColorN50} /></IconWrapper>;

const Padding = styled.div`
  padding: 12px;
`;

export const SelectedIcon = props => <IconWrapper><AkRadioIcon {...props} primaryColor={akColorB400} /></IconWrapper>;

export const IncompleteIcon = props => <IconWrapper><AkRadioIcon {...props} primaryColor={akColorN50} /></IconWrapper>;

const ListItem = styled.div`
  display: flex;
`;

const Content = styled.div`
  padding-top: ${akGridSize() * 2}px;
`;

const ItemContainer = styled.li`
  height: ${akGridSize() * 14}px;
`;

// exported for tests
export const DashedImage = styled.div`
  background: url(${dashedLineImage}) no-repeat;
  height: ${akGridSize() * 8}px;
  margin-left: 22px;
  clear: left;
`;

const ProgressList = styled.ol`
  padding: 0;
  list-style-type: none;

  /* Stylelint does not like this way of doing things, although it's perfectly valid in styled-components */
  /* stylelint-disable-next-line declaration-empty-line-before,selector-type-no-unknown */
  ${ItemContainer}:last-child ${DashedImage} {
    display: none;
  }
`;

const chooseIcon = (itemState: ProgressState) => {
  switch (itemState) {
    case ProgressState.Completed:
      return <Padding><CompletedIcon label={''} size={'medium'} /></Padding>;
    case ProgressState.Incomplete:
      return <IncompleteIcon label={''} size={'xlarge'} />;
    case ProgressState.Selected:
      return <SelectedIcon label={''} size={'xlarge'} />;
    default:
      return <CompletedIcon label={''} size={'medium'} />;
  }
};

export function renderProgressItem(item: ProgressItem): JSX.Element {
  return (
    <ListItem>
      <div>
        {chooseIcon(item.state)}
        <DashedImage />
      </div>
      <Content>{item.item}</Content>
    </ListItem>
  );
}

export class DeclarativeProgressTracker extends React.Component<{}, {}> {
  public render() {
    const { children } = this.props;
    const childrenCount = React.Children.count(children);

    if (childrenCount === 0) {
      return null;
    }

    return (
      <ProgressList>
        {React.Children.map(children, (item, index) => (
          <ItemContainer key={index}>
            {item}
          </ItemContainer>
        ))}
      </ProgressList>
    );
  }
}

// tslint:disable-next-line:max-classes-per-file
export class ProgressTracker extends React.Component<OwnProps> {
  constructor(props) {
    super(props);
    if (__NODE_ENV__ === 'development') {
      // tslint:disable-next-line:no-console
      console.warn('ProgressTracker is deprecated, use DeclarativeProgressTracker instead');
    }
  }

  public render() {

    return (this.props.items.length > 0 && (
      <ProgressList>
        {
          this.props.items.map((item, index) => (
            <ItemContainer key={index}>
              <ListItem>
                {chooseIcon(item.state)}
                <Content>{item.item}</Content>
              </ListItem>
              {index < this.props.items.length - 1 && <DashedImage />}
            </ItemContainer>
          ))
        }
      </ProgressList>)
    );
  }
}
