// tslint:disable:jsx-use-translation-function
import { storiesOf } from '@storybook/react';
import * as React from 'react';

import { ProgressState, ProgressTracker } from '.';

storiesOf('Common|Progress Tracker', module)
  .add('With completed, selected, and incomplete states', () => {
    const items = [
      {
        state: ProgressState.Completed,
        item: <p>Compelted item</p>,
      },
      {
        state: ProgressState.Selected,
        item: <p>Currently working on</p>,
      },
      {
        state: ProgressState.Incomplete,
        item: <p>Future thing to do</p>,
      },
    ];

    return (
      <ProgressTracker items={items} />
    );
  });
