import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';

import AkCheckCircleIcon from '@atlaskit/icon/glyph/check-circle';
import AkRadioIcon from '@atlaskit/icon/glyph/radio';

import { DashedImage, ProgressState, ProgressTracker } from '.';

describe('Progress Tracker', () => {

  it('renders items with correct state', () => {
    const items = [
      {
        state: ProgressState.Completed,
        item: <h1 />,
      },
      {
        state: ProgressState.Incomplete,
        item: <h2 />,
      },
      {
        state: ProgressState.Selected,
        item: <h3 />,
      },
    ];

    const wrapper = mount(<ProgressTracker items={items}/>);

    expect(wrapper.find('li')).to.have.length(3);
    expect(wrapper.find('h1')).to.have.length(1);
    expect(wrapper.find('h2')).to.have.length(1);
    expect(wrapper.find('h3')).to.have.length(1);
    expect(wrapper.find(AkCheckCircleIcon)).to.have.length(1); // Completed icon
    expect(wrapper.find(AkRadioIcon)).to.have.length(2); // Incomplete and selected icon
    expect(wrapper.find(DashedImage)).to.have.length(2); // Dashes between items

  });

  it('renders nothing when given nothing', () => {
    const wrapper = mount(<ProgressTracker items={[]} />);

    expect(wrapper.find('ol')).to.have.length(0);
    expect(wrapper.find('li')).to.have.length(0);
  });

});
