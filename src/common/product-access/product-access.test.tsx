import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import { Checkbox as AkCheckbox } from '@atlaskit/checkbox';

import { InfoHover } from 'common/info-hover';

import { ProductAccess, ProductAccessProps } from './product-access';

describe('ProductAccess', () => {
  const noop = () => null;

  const getWrapper = (props: Pick<ProductAccessProps, 'products' | 'tooltipStyle' | 'isReadOnly'>) => {
    return shallow((
      <ProductAccess
        products={props.products}
        tooltipStyle={props.tooltipStyle}
        isReadOnly={props.isReadOnly}
        onCheckboxChange={noop}
      />
    ), { disableLifecycleMethods: false });
  };

  describe('readonly state', () => {
    it('should render disabled checkboxes', () => {
      const wrapper = getWrapper({
        products: [
          {
            productId: 'jira-servicedesk',
            productName: 'Jira Service Desk',
            accessLevel: 'ADMIN',
            isInitiallyChecked: false,
          },
        ],
        tooltipStyle: 'inline',
        isReadOnly: true,
      });

      expect(wrapper.find(AkCheckbox).prop('isDisabled')).to.equal(true);
    });
  });

  describe('tooltip', () => {
    it('should render the inline version when specified', () => {
      const wrapper = getWrapper({
        products: [
          {
            productId: 'jira-servicedesk',
            productName: 'Jira Service Desk',
            accessLevel: 'ADMIN',
            isInitiallyChecked: true,
          },
          {
            productId: 'jira-core',
            productName: 'Jira Core',
            accessLevel: 'NONE',
            isInitiallyChecked: false,
          },
        ],
        tooltipStyle: 'inline',
        isReadOnly: false,
      });

      expect(wrapper.find(InfoHover).exists()).to.equal(false);
    });

    it('should render the hover version when specified', () => {
      const wrapper = getWrapper({
        products: [
          {
            productId: 'jira-servicedesk',
            productName: 'Jira Service Desk',
            accessLevel: 'ADMIN',
            isInitiallyChecked: true,
          },
          {
            productId: 'jira-core',
            productName: 'Jira Core',
            accessLevel: 'NONE',
            isInitiallyChecked: false,
          },
        ],
        tooltipStyle: 'hover',
        isReadOnly: false,
      });

      expect(wrapper.find(InfoHover).exists()).to.equal(true);
    });

    it(`should render if jira core isn't selected but another jira product is`, () => {
      const wrapper = getWrapper({
        products: [
          {
            productId: 'jira-servicedesk',
            productName: 'Jira Service Desk',
            accessLevel: 'ADMIN',
            isInitiallyChecked: true,
          },
          {
            productId: 'jira-software',
            productName: 'Jira Software',
            accessLevel: 'NONE',
            isInitiallyChecked: false,
          },
          {
            productId: 'jira-core',
            productName: 'Jira Core',
            accessLevel: 'NONE',
            isInitiallyChecked: false,
          },
        ],
        tooltipStyle: 'hover',
        isReadOnly: false,
      });

      expect(wrapper.find(InfoHover).exists()).to.equal(true);
    });

    it('should render the jira core checkbox checked and disabled if the tooltip is showing', () => {
      const wrapper = getWrapper({
        products: [
          {
            productId: 'jira-servicedesk',
            productName: 'Jira Service Desk',
            accessLevel: 'ADMIN',
            isInitiallyChecked: true,
          },
          {
            productId: 'jira-software',
            productName: 'Jira Software',
            accessLevel: 'NONE',
            isInitiallyChecked: false,
          },
          {
            productId: 'jira-core',
            productName: 'Jira Core',
            accessLevel: 'NONE',
            isInitiallyChecked: false,
          },
        ],
        tooltipStyle: 'hover',
        isReadOnly: false,
      });

      expect(wrapper.find(InfoHover).exists()).to.equal(true);

      const jiraCoreCheckbox = wrapper.find(AkCheckbox)
        .findWhere(n => n.prop('name') === 'jira-core');

      expect(jiraCoreCheckbox.exists()).to.equal(true);
      expect(jiraCoreCheckbox.prop('isChecked')).to.equal(true);
      expect(jiraCoreCheckbox.prop('isDisabled')).to.equal(true);
    });
  });
});
