// tslint:disable:jsx-use-translation-function
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';
import styled from 'styled-components';

import { ProductAccess } from './product-access';

const Wrapper = styled.div`
  margin: 25px;
  max-width: 500px;
`;

const noop = () => null;

storiesOf('Common|Product Access', module)
  .add('With the hover tooltip style', () => {
    return (
      <IntlProvider locale="en">
        <Wrapper>
          <ProductAccess
            tooltipStyle="hover"
            onCheckboxChange={noop}
            isReadOnly={false}
            products={[
              {
                productId: 'jira-servicedesk',
                productName: 'Jira Service Desk',
                presence: '2017-07-10T14:00:00Z',
                accessLevel: 'NONE',
                isInitiallyChecked: true,
              },
              {
                productId: 'jira-software',
                productName: 'Jira Software',
                presence: '2017-07-10T14:00:00Z',
                accessLevel: 'NONE',
                isInitiallyChecked: true,
              },
              {
                productId: 'jira-core',
                productName: 'Jira Core',
                presence: '2017-07-10T14:00:00Z',
                accessLevel: 'NONE',
                isInitiallyChecked: false,
              },
              {
                productId: 'conf',
                productName: 'Confluence',
                presence: '2017-07-10T14:00:00Z',
                accessLevel: 'ADMIN',
                isInitiallyChecked: false,
              },
            ]}
          />
        </Wrapper>
      </IntlProvider>
    );
  })
  .add('With the inline tooltip style', () => {
    return (
      <IntlProvider locale="en">
        <Wrapper>
          <ProductAccess
            tooltipStyle="inline"
            onCheckboxChange={noop}
            isReadOnly={false}
            products={[
              {
                productId: 'jira-servicedesk',
                productName: 'Jira Service Desk',
                presence: '2017-07-10T14:00:00Z',
                accessLevel: 'NONE',
                isInitiallyChecked: true,
              },
              {
                productId: 'jira-software',
                productName: 'Jira Software',
                presence: '2017-07-10T14:00:00Z',
                accessLevel: 'NONE',
                isInitiallyChecked: true,
              },
              {
                productId: 'jira-core',
                productName: 'Jira Core',
                presence: '2017-07-10T14:00:00Z',
                accessLevel: 'NONE',
                isInitiallyChecked: false,
              },
              {
                productId: 'conf',
                productName: 'Confluence',
                presence: '2017-07-10T14:00:00Z',
                accessLevel: 'ADMIN',
                isInitiallyChecked: true,
              },
            ]}
          />
        </Wrapper>
      </IntlProvider>
    );
  })
  .add('In readonly mode', () => {
    return (
      <IntlProvider locale="en">
        <Wrapper>
          <ProductAccess
            tooltipStyle="hover"
            onCheckboxChange={noop}
            isReadOnly={true}
            products={[
              {
                productId: 'jira-servicedesk',
                productName: 'Jira Service Desk',
                presence: '2017-07-10T14:00:00Z',
                accessLevel: 'NONE',
                isInitiallyChecked: true,
              },
              {
                productId: 'jira-software',
                productName: 'Jira Software',
                presence: '2017-07-10T14:00:00Z',
                accessLevel: 'NONE',
                isInitiallyChecked: true,
              },
              {
                productId: 'jira-core',
                productName: 'Jira Core',
                presence: '2017-07-10T14:00:00Z',
                accessLevel: 'NONE',
                isInitiallyChecked: true,
              },
              {
                productId: 'conf',
                productName: 'Confluence',
                presence: '2017-07-10T14:00:00Z',
                accessLevel: 'ADMIN',
                isInitiallyChecked: false,
              },
            ]}
          />
        </Wrapper>
      </IntlProvider>
    );
  });
