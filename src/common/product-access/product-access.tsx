import * as React from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';
import styled from 'styled-components';

import {
  Checkbox as AkCheckbox,
} from '@atlaskit/checkbox';

import {
  colors as akColors,
  fontSize as akFontSize,
  gridSize as akGridSize,
} from '@atlaskit/theme';

import { InfoHover } from 'common/info-hover';

import { AccessLevel, ProductAccess as ProductAccessType } from '../../schema/schema-types';

const messages = defineMessages({
  jiraCoreIncluded: {
    id: 'product.access.jira.core.included',
    defaultMessage: 'Jira Core is included with the other Jira products.',
    description: 'Popover explaining they get Jira Core automatically.',
  },
});

export const CheckboxWrapper = styled.div`
  display: flex;
  align-items: center;
`;

const InfoHoverWrapper = styled.div`
  margin-left: ${akGridSize()}px;
  height: ${akGridSize() * 3}px;
`;

const InlineWrapper = styled.div`
  font-size: ${akFontSize() - 4}px;
  color: ${akColors.subtleHeading};
  margin: 0 0 0 ${akGridSize() * 3.75}px;
`;

export type ProductWithChecked = Pick<ProductAccessType, 'productId' | 'productName' | 'presence'> & {
  readonly isInitiallyChecked: boolean;
  accessLevel?: AccessLevel;
};

export interface ProductsChanged extends ProductWithChecked {
  isChecked: boolean;
}

export interface ProductAccessProps {
  products: ProductWithChecked[];
  tooltipStyle: 'inline' | 'hover';
  isReadOnly?: boolean;

  onCheckboxChange(products: ProductsChanged[]): void;

  onAfterMount?(products: ProductsChanged[]): void;
}

interface State {
  products: ProductsChanged[];
}

export class ProductAccess extends React.Component<ProductAccessProps, State> {
  public static defaultProps: Partial<ProductAccessProps> = {
    isReadOnly: false,
  };

  public readonly state: Readonly<State> = {
    products: [],
  };

  public componentDidMount() {
    this.setState({
      products: this.props.products.map(propProduct => ({
        ...propProduct,
        isChecked: propProduct.isInitiallyChecked,
      })),
    }, () => {
      if (this.props.onAfterMount) {
        this.props.onAfterMount(this.state.products);
      }
    });
  }

  public render() {
    if (!this.state.products.length) {
      return null;
    }

    return (
      <React.Fragment>
        {
          this.state.products.map(productAccess => {
            const shouldRenderTooltip = this.shouldRenderTooltip(productAccess.productId);

            return (
              <div key={productAccess.productId}>
                {this.renderWrapper((
                  <React.Fragment>
                    <AkCheckbox
                      name={productAccess.productId}
                      label={productAccess.productName}
                      isChecked={productAccess.isChecked || shouldRenderTooltip}
                      isDisabled={this.props.isReadOnly || shouldRenderTooltip}
                      onChange={this.onCheckboxClicked}
                    />
                    {shouldRenderTooltip && this.renderTooltip()}
                  </React.Fragment>
                ))}
              </div>
            );
          })
        }
      </React.Fragment>
    );
  }

  public renderWrapper = (element) => {
    if (this.props.tooltipStyle === 'inline') {
      return element;
    }

    return (
      <CheckboxWrapper>
        {element}
      </CheckboxWrapper>
    );
  }

  public shouldRenderTooltip = (currentProductId: string): boolean => {
    if (currentProductId !== 'jira-core') {
      return false;
    }

    return this.state.products.some(product => {
      return product.productId.startsWith('jira')
        && product.productId !== 'jira-core'
        && product.isChecked;
    });
  };

  public renderTooltip = (): React.ReactNode => {
    if (this.props.tooltipStyle === 'inline') {
      return (
        <InlineWrapper>
          <FormattedMessage {...messages.jiraCoreIncluded} />
        </InlineWrapper>
      );
    } else {
      return (
        <InfoHoverWrapper>
          <InfoHover
            hasMargin={false}
            dialogContent={(
              <FormattedMessage {...messages.jiraCoreIncluded} tagName="div" />
            )}
          />
        </InfoHoverWrapper>
      );
    }
  };

  public onCheckboxClicked = (event: React.ChangeEvent<HTMLInputElement>) => {
    const updatedProducts = this.state.products.map(product => {
      const newProduct: ProductsChanged = product;

      if (newProduct.productId === event.target.name) {
        newProduct.isChecked = !!event.target.checked;
      }

      return newProduct;
    });

    this.setState({ products: updatedProducts }, () => {
      this.props.onCheckboxChange(this.state.products);
    });
  };
}
