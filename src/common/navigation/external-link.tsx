import * as React from 'react';

import { withClickAnalytics } from 'common/analytics';

export interface ExternalLinkProps {
  href: string;
  children?: React.ReactNode;
  className?: string;
  hideReferrer?: boolean;
  onClick?(): void;
}

export class ExternalLink extends React.Component<ExternalLinkProps> {
  public render() {
    const { href, children, onClick, className, hideReferrer = true } = this.props;

    return (
      <a
        className={className}
        href={href}
        onClick={onClick}
        target="_blank"
        rel={`noopener${hideReferrer ? ' noreferrer' : ''}`}
      >
        {children}
      </a>
    );
  }
}

export const ExternalLinkWithClickAnalytics = withClickAnalytics<ExternalLinkProps>()(ExternalLink);
