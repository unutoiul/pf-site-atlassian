/**
 * Represents a valid SectionId. When you define a new id,
 * make sure to extend this enum
 */
export enum NavigationSectionId {
  Organizations = 'organizations',
  SiteUrl = 'site-url',
  SiteSettings = 'site-settings',
  Billing = 'pricing',
  Applications = 'application-settings',
  UserManagement = 'usermanagement',
  Authentication = 'authentication',
  JiraServiceDesk = 'jira-service-desk',
  Subscriptions = 'subscriptions',
  IdentityManager = 'identity-manager',
}
