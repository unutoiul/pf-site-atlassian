import { expect } from 'chai';
import { mount, ReactWrapper } from 'enzyme';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { MemoryRouter } from 'react-router-dom';
import { spy } from 'sinon';

import AkShortcutIcon from '@atlaskit/icon/glyph/shortcut';
import AkLozenge from '@atlaskit/lozenge';
import AkNavigation, { AkNavigationItem } from '@atlaskit/navigation';

import { RouterLink } from 'common/navigation';

import { createApolloClient } from '../../apollo-client';
import { createMockIntlContext } from '../../utilities/testing';
import { NavigationLinkWithoutApollo } from './navigation-link';

function mountWithNavigationContext(node: React.ReactNode, initialEntries = [{ pathname: '/' }]): ReactWrapper<any, any> {
  const apolloClient = createApolloClient();

  return mount(
    <MemoryRouter initialEntries={initialEntries}>
      <ApolloProvider client={apolloClient}>
        <AkNavigation>
          {node}
        </AkNavigation>
      </ApolloProvider>
    </MemoryRouter>, createMockIntlContext(),
  );
}

describe('navigation-link', () => {

  describe('props', () => {
    it('should render with target=_blank only when links are external', () => {
      const internalLink = mountWithNavigationContext(<NavigationLinkWithoutApollo path="#foo" />);
      expect(internalLink.find(NavigationLinkWithoutApollo).find('a').at(0).props().target).to.not.equal('_blank');

      const externalLink = mountWithNavigationContext(<NavigationLinkWithoutApollo isExternal={true} path="#foo" />);
      expect(externalLink.find(NavigationLinkWithoutApollo).find('a').at(0).props().target).to.equal('_blank');
    });

    it('should render the path given', () => {
      const internalLink = mountWithNavigationContext(<NavigationLinkWithoutApollo path="#foo" />);
      expect(internalLink.find(NavigationLinkWithoutApollo).find('a').at(0).props().href).to.equal('/#foo');
    });

    it('should pass null as the icon if it is not specified', () => {
      const internalLink = mountWithNavigationContext(<NavigationLinkWithoutApollo path="#foo" />);

      const iconProp = internalLink.find(AkNavigationItem).prop('icon');

      expect(iconProp).to.equal(null);
    });

    it('should pass isSelected as true if current location matches path', () => {
      const currentPath = '/test/path';
      const internalLink = mountWithNavigationContext(
        <NavigationLinkWithoutApollo path={currentPath} />,
        [{ pathname: currentPath }],
      );
      expect(internalLink.find(AkNavigationItem).prop('isSelected')).to.equal(true);
    });

    it('should pass isSelected as true if current location is subpage of path', () => {
      const internalLink = mountWithNavigationContext(
        <NavigationLinkWithoutApollo path="/test/path" />,
        [{ pathname: '/test/path/subpage' }],
      );
      expect(internalLink.find(AkNavigationItem).prop('isSelected')).to.equal(true);
    });

    it('should pass isSelected as false if current location does not match path', () => {
      const internalLink = mountWithNavigationContext(
        <NavigationLinkWithoutApollo path="/test/foo" />,
        [{ pathname: '/test/bar' }],
      );
      expect(internalLink.find(AkNavigationItem).prop('isSelected')).to.equal(false);
    });

    it('should render a shortcut icon only when external', () => {
      const internalLink = mountWithNavigationContext(<NavigationLinkWithoutApollo path="#foo" />);
      expect(internalLink.find(NavigationLinkWithoutApollo).find(AkShortcutIcon).length).to.equal(0);
      const externalLink = mountWithNavigationContext(<NavigationLinkWithoutApollo isExternal={true} path="#foo" />);
      expect(externalLink.find(NavigationLinkWithoutApollo).find(AkShortcutIcon).length).to.equal(1);
    });

    it('should render a router link if there is a path', () => {
      const internalLink = mountWithNavigationContext(<NavigationLinkWithoutApollo path="#foo" />);
      expect(internalLink.find(RouterLink).exists()).to.equal(true);
    });

    it('should not render a router link if there is no path', () => {
      const mock = () => {
        return;
      };
      const internalLink = mountWithNavigationContext(<NavigationLinkWithoutApollo onClick={mock} />);
      expect(internalLink.find(RouterLink).exists()).to.equal(false);
    });

    describe('"NEW" lozenge', () => {
      it('should be rendered when the prop is present', () => {
        const internalLink = mountWithNavigationContext(<NavigationLinkWithoutApollo lozenge={{ type: 'new' }} />);

        expect(internalLink.find(AkLozenge)).to.have.lengthOf(1);
      });

      it('should report item clicks via "markItemAsViewed" prop when the item is new and the lozenge should only be shown once', () => {
        const markItemAsViewedSpy = spy();
        const internalLink = mountWithNavigationContext(<NavigationLinkWithoutApollo lozenge={{ type: 'new', interactOnce: true }} markItemAsViewed={markItemAsViewedSpy} />);

        internalLink.find(AkNavigationItem).simulate('click');

        expect(markItemAsViewedSpy.calledOnce).to.equal(true);
      });

      it('should not report item clicks via "markItemAsViewed" prop when the item is new and the lozenge should always be shown', () => {
        const markItemAsViewedSpy = spy();
        const internalLink = mountWithNavigationContext(<NavigationLinkWithoutApollo lozenge={{ type: 'new' }} markItemAsViewed={markItemAsViewedSpy} />);

        internalLink.find(AkNavigationItem).simulate('click');

        expect(markItemAsViewedSpy.calledOnce).to.equal(false);
      });
    });
  });
});
