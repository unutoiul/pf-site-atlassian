import { DrawerLink, NavigationDrawer, SectionUpdateParams } from 'common/navigation';

export enum NavigationAction {
  CHROME_NAVIGATION_SECTION_UPDATE = 'CHROME_NAVIGATION_SECTION_UPDATE',
  CHROME_NAVIGATION_SECTION_REMOVE = 'CHROME_NAVIGATION_SECTION_REMOVE',
  CHROME_DRAWER_LINK_SHOW = 'CHROME_DRAWER_LINK_SHOW',
  CHROME_DRAWER_LINK_HIDE = 'CHROME_DRAWER_LINK_HIDE',
  CHROME_DRAWER_OPEN = 'CHROME_DRAWER_OPEN',
  CHROME_DRAWER_CLOSE = 'CHROME_DRAWER_CLOSE',
}

export interface ChromeNavigationSectionUpdate {
  type: NavigationAction.CHROME_NAVIGATION_SECTION_UPDATE;
  section: SectionUpdateParams;
}
export const updateNavigationSection = (section: SectionUpdateParams): ChromeNavigationSectionUpdate => ({
  type: NavigationAction.CHROME_NAVIGATION_SECTION_UPDATE,
  section,
});

export interface ChromeNavigationSectionRemove {
  type: NavigationAction.CHROME_NAVIGATION_SECTION_REMOVE;
  sectionId: string;
}
export const removeNavigationSection = (sectionId): ChromeNavigationSectionRemove => ({
  type: NavigationAction.CHROME_NAVIGATION_SECTION_REMOVE,
  sectionId,
});

export interface ChromeDrawerLinkShow {
  type: NavigationAction.CHROME_DRAWER_LINK_SHOW;
  drawerLink: DrawerLink;
}
export const showDrawerLink = (drawerLink): ChromeDrawerLinkShow => ({
  type: NavigationAction.CHROME_DRAWER_LINK_SHOW,
  drawerLink,
});

export interface ChromeDrawerLinkHide {
  type: NavigationAction.CHROME_DRAWER_LINK_HIDE;
  drawerLink: DrawerLink;
}
export const hideDrawerLink = (drawerLink): ChromeDrawerLinkHide => ({
  type: NavigationAction.CHROME_DRAWER_LINK_HIDE,
  drawerLink,
});

export interface ChromeDrawerOpen {
  type: NavigationAction.CHROME_DRAWER_OPEN;
  drawer: NavigationDrawer;
  modifyHistory: boolean;
}
export const openDrawer = (drawer: NavigationDrawer, modifyHistory: boolean = true): ChromeDrawerOpen => ({
  type: NavigationAction.CHROME_DRAWER_OPEN,
  drawer,
  modifyHistory,
});
export interface ChromeDrawerClose {
  type: NavigationAction.CHROME_DRAWER_CLOSE;
  modifyHistory: boolean;
}
export const closeDrawer = (modifyHistory: boolean = true): ChromeDrawerClose => ({
  type: NavigationAction.CHROME_DRAWER_CLOSE,
  modifyHistory,
});
