import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';

import { NavigationLinkIconGlyph } from 'common/navigation';

import { createMockIntlProp } from '../../utilities/testing';
import { NavigationLinkIconImpl } from './navigation-link-icon';

describe('NavigationLinkIcon', () => {
  describe('props', () => {
    it('should render an icon if the provided icon glyph string is supported', () => {
      const internalLink = mount(<NavigationLinkIconImpl icon={NavigationLinkIconGlyph.People} intl={createMockIntlProp()} />);
      expect(internalLink.find(NavigationLinkIconImpl).children()).to.have.length(1);
    });
  });
});
