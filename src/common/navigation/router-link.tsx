import * as React from 'react';
import { Link } from 'react-router-dom';

interface RouterLinkProps {
  href: string;
  className: string;
  onMouseDown(): void;
  onMouseEnter(): void;
  onMouseLeave(): void;
  onClick(): void;
}

export class RouterLink extends React.PureComponent<RouterLinkProps> {
  public render() {
    const {
      href,
      children,
      onMouseDown,
      onClick,
      className,
      onMouseEnter,
      onMouseLeave,
    } = this.props;

    return href ? (
      <Link
        className={className}
        onMouseDown={onMouseDown}
        onMouseEnter={onMouseEnter}
        onMouseLeave={onMouseLeave}
        onClick={onClick}
        to={href}
      >
        {children}
      </Link>
    ) : (
      children
    );
  }
}
