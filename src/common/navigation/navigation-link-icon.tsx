import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import AkAddonIcon from '@atlaskit/icon/glyph/addon';
import AkAppAccessIcon from '@atlaskit/icon/glyph/app-access';
import AkArrowLeftIcon from '@atlaskit/icon/glyph/arrow-left';
import AkBillingFilledIcon from '@atlaskit/icon/glyph/creditcard';
import AkDiscoverFilledIcon from '@atlaskit/icon/glyph/discover-filled';
import AkEditFilledIcon from '@atlaskit/icon/glyph/edit-filled';
import AkEmojiIcon from '@atlaskit/icon/glyph/emoji/emoji';
import AkFeedbackIcon from '@atlaskit/icon/glyph/feedback';
import AkGsuiteIcon from '@atlaskit/icon/glyph/gsuite';
import AkInviteTeamIcon from '@atlaskit/icon/glyph/invite-team';
import AkLockFilledIcon from '@atlaskit/icon/glyph/lock-filled';
import AkMediaServicesGridIcon from '@atlaskit/icon/glyph/media-services/grid';
import AkOfficeBuildingIcon from '@atlaskit/icon/glyph/office-building';
import AkOfficeBuildingFilledIcon from '@atlaskit/icon/glyph/office-building-filled';
import AkPeopleIcon from '@atlaskit/icon/glyph/people';
import AkPeopleGroupIcon from '@atlaskit/icon/glyph/people-group';
import AkPersonIcon from '@atlaskit/icon/glyph/person';
import AkSettingsIcon from '@atlaskit/icon/glyph/settings';
import AkSignInIcon from '@atlaskit/icon/glyph/sign-in';
import AkTaskIcon from '@atlaskit/icon/glyph/task';
import AkTrayIcon from '@atlaskit/icon/glyph/tray';

import {
  ConfluenceIcon as AkConfluenceIcon,
  JiraIcon as AkJiraIcon,
  StrideIcon as AkStrideIcon,
} from '@atlaskit/logo';

import { NavigationLinkIconGlyph } from './navigation-icon-glyphs';

const messages = defineMessages({
  altText: {
    id: 'chrome.navigation.link-icon-label',
    description: 'Alt text for the navigation link icons',
    defaultMessage: 'Icon',
  },
});

interface NavigationLinkIconProps {
  icon: NavigationLinkIconGlyph;
}

function getIcon(glyph: NavigationLinkIconGlyph): React.ComponentClass<any> | null {
  switch (glyph) {
    case NavigationLinkIconGlyph.App:
      return AkAddonIcon;
    case NavigationLinkIconGlyph.AppAccess:
      return AkAppAccessIcon;
    case NavigationLinkIconGlyph.ArrowLeft:
      return AkArrowLeftIcon;
    case NavigationLinkIconGlyph.Billing:
      return AkBillingFilledIcon;
    case NavigationLinkIconGlyph.BillingManage:
      return AkMediaServicesGridIcon;
    case NavigationLinkIconGlyph.Confluence:
      return AkConfluenceIcon;
    case NavigationLinkIconGlyph.DiscoverFilled:
      return AkDiscoverFilledIcon;
    case NavigationLinkIconGlyph.EditFilled:
      return AkEditFilledIcon;
    case NavigationLinkIconGlyph.Emoji:
      return AkEmojiIcon;
    case NavigationLinkIconGlyph.Feedback:
      return AkFeedbackIcon;
    case NavigationLinkIconGlyph.GsuiteIcon:
      return AkGsuiteIcon;
    case NavigationLinkIconGlyph.InviteTeam:
      return AkInviteTeamIcon;
    case NavigationLinkIconGlyph.Jira:
      return AkJiraIcon;
    case NavigationLinkIconGlyph.LockFilled:
      return AkLockFilledIcon;
    case NavigationLinkIconGlyph.OfficeBuilding:
      return AkOfficeBuildingIcon;
    case NavigationLinkIconGlyph.OfficeBuildingFilled:
      return AkOfficeBuildingFilledIcon;
    case NavigationLinkIconGlyph.People:
      return AkPeopleIcon;
    case NavigationLinkIconGlyph.PeopleGroup:
      return AkPeopleGroupIcon;
    case NavigationLinkIconGlyph.Person:
      return AkPersonIcon;
    case NavigationLinkIconGlyph.Settings:
      return AkSettingsIcon;
    case NavigationLinkIconGlyph.SignIn:
      return AkSignInIcon;
    case NavigationLinkIconGlyph.Stride:
      return AkStrideIcon;
    case NavigationLinkIconGlyph.Task:
      return AkTaskIcon;
    case NavigationLinkIconGlyph.Tray:
      return AkTrayIcon;
    default:
      return null;
  }
}

export class NavigationLinkIconImpl extends React.Component<NavigationLinkIconProps & InjectedIntlProps> {
  public render() {
    const LinkIcon = getIcon(this.props.icon);
    if (!LinkIcon) {
      return null;
    }

    return (
      <LinkIcon label={this.props.intl.formatMessage(messages.altText)} size="medium" secondaryColor="inherit" />
    );
  }
}

export const NavigationLinkIcon = injectIntl(NavigationLinkIconImpl);
