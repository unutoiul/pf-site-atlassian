import { FormattedMessage } from 'react-intl';
import { RouteProps } from 'react-router-dom';

import { NavigationLinkIconGlyph } from './navigation-icon-glyphs';
import { NavigationSectionId } from './navigation-section-id';

export enum NavigationDrawer {
  Create,
  UserInvite,
  UserExport,
  GSuiteWizard,
}

/**
 * Represents information about a drawer link.
 * The label must be a localization descriptor.
 * Path should be in the format of "/admin/your/thing".
 *
 * @export
 * @interface DrawerLink
 */
export interface DrawerLink {
  path: string;
  title: FormattedMessage.MessageDescriptor;
}

// we allow items to have random IDs, but we want section updates/removals to be based on a predefined list of IDs
export declare type NavItemId = NavigationSectionId | string;

export type NavItemLozenge = {
  type: 'new' | 'beta',
  /**
   * If set to `true` ensures that the lozenge will only be shown until the user clicks on the item.
   * The next time user opens administration page - lozenge will be hidden.
   *
   * @type {boolean}
   */
  interactOnce?: boolean;
} | never;

export declare type NavItem = {
  title: FormattedMessage.MessageDescriptor;
  id?: NavItemId;
  path?: string;
  links?: NavItem[];
  priority?: number;
  isExternal?: boolean;
  icon?: NavigationLinkIconGlyph;
  analyticsSubjectId?: string;
  analyticsMeta?: object;
  flat?: boolean;
  disabled?: boolean;
  lozenge?: NavItemLozenge;
} & ({ id: NavItemId } | { path: string }); // make sure that either 'id' or 'path' is provided

export interface RouteRegistrationData {
  switchRoutes?: RouteProps[];
  globalRoutes?: RouteProps[];
}

export declare type SectionUpdateParams = {
  id: NavigationSectionId;
  title?: FormattedMessage.MessageDescriptor;
  links?: NavItem[];
  path?: string
} & ({ links: NavItem[] } | { path: string } | { title: FormattedMessage.MessageDescriptor }); // make sure that at least something is updated

export interface NavigationMounterProps {
  registerRoutes(data: RouteRegistrationData): void;
  unregisterRoutes(data: RouteRegistrationData): void;
  updateNavigationSection(section: SectionUpdateParams): void;
  removeNavigationSection(sectionId: NavigationSectionId): void;
  showDrawerLink(drawerLink: DrawerLink): void;
  hideDrawerLink(drawerLink: DrawerLink): void;
}

export interface Application {
  name: string;
  url: string;
  adminUrl: string;
  id: string;
  product: string;
}
