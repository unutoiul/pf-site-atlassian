import * as React from 'react';
import { graphql, OptionProps } from 'react-apollo';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import styled from 'styled-components';

import AkShortcutIcon from '@atlaskit/icon/glyph/shortcut';
import AkLozenge from '@atlaskit/lozenge';
import { AkNavigationItem } from '@atlaskit/navigation';
import { gridSize as akGridSize } from '@atlaskit/theme';

import { AnalyticsProps, Referrer, withClickAnalytics } from 'common/analytics';
import { onboardingMutation, onboardingQuery } from 'common/onboarding';

import { DismissOnboardingMutation, DismissOnboardingMutationVariables, OnboardingQuery, OnboardingQueryVariables } from '../../schema/schema-types';
import { ExternalLink } from './external-link';
import { NavigationLinkIconGlyph } from './navigation-icon-glyphs';
import { NavigationLinkIcon } from './navigation-link-icon';
import { NavItem, NavItemLozenge } from './navigation.prop-types';
import { RouterLink } from './router-link';

const messages = defineMessages({
  newTab: {
    id: 'chrome.navigation-link.open-in-new-tab',
    description: 'Used as an alternative accessible text to describe that an icon will open the text in a new tab',
    defaultMessage: 'Open in new tab',
  },
  newLozengeText: {
    id: 'chrome.navigation-link.new-lozenge-text',
    description: 'Used to indicate a navigation item as "New" functionality to drag user attention to',
    defaultMessage: 'New',
  },
});

const LozengeContainer = styled.div`
  display: inline-block;
  margin: 0 ${akGridSize}px;
`;

export interface NavigationLinkProps extends AnalyticsProps {
  id?: string;
  path?: string;
  title?: string;
  isExternal?: boolean;
  isSelected?: boolean;
  icon?: NavigationLinkIconGlyph;
  lozenge?: NavItem['lozenge'];
  iconRight?: React.ReactNode;
  onClick?(event: Event): void;
}

interface NewLozengeMutationProps {
  markItemAsViewed?(): void;
}

export const AkNavigationItemWithAnalytics = withClickAnalytics<any>()(AkNavigationItem);

class NavigationLinkImpl extends React.Component<NavigationLinkProps & RouteComponentProps<any> & InjectedIntlProps & NewLozengeMutationProps> {
  public static defaultProps = {
    onClick: () => null,
    path: '',
    title: '',
    isExternal: false,
  };

  public render() {
    const {
      path,
      isExternal,
      icon,
      iconRight,
      analyticsData,
    } = this.props;

    const textAfter = iconRight || (isExternal && <AkShortcutIcon label={this.props.intl.formatMessage(messages.newTab)} size="small" />) || null;

    const LinkComponent = isExternal ? ExternalLink : RouterLink;
    const NavItemComponent = analyticsData ? AkNavigationItemWithAnalytics : AkNavigationItem;

    return (
      <Referrer value="navigation">
        <NavItemComponent
          icon={icon ? <NavigationLinkIcon icon={icon} /> : null}
          href={path}
          text={this.getContent()}
          onClick={this.getOnClickHandler()}
          linkComponent={path ? LinkComponent : null}
          isSelected={'isSelected' in this.props ? this.props.isSelected : this.isSelected()}
          textAfter={textAfter}
          analyticsData={analyticsData}
        />
      </Referrer>
    );
  }

  private getContent(): React.ReactNode {
    const {
      title,
    } = this.props;

    return (
      <React.Fragment>
        {title}
        {this.getLozenge()}
      </React.Fragment>
    );
  }

  private getLozenge() {
    const {
      lozenge,
    } = this.props;

    if (lozenge && lozenge.type === 'new') {
      return (
        <LozengeContainer>
          <AkLozenge
            appearance="new"
          >
            <FormattedMessage {...messages.newLozengeText} />
          </AkLozenge>
        </LozengeContainer>
      );
    }

    return null;
  }

  private getOnClickHandler() {
    const {
      onClick,
      lozenge,
    } = this.props;

    if (!lozenge || !isNewLozengeInteractableOnce(lozenge)) {
      return onClick;
    }

    return this.handleNewItemClick;
  }

  private handleNewItemClick = (...args: any[]): any => {
    const {
      onClick,
      markItemAsViewed,
    } = this.props;

    if (markItemAsViewed instanceof Function) {
      markItemAsViewed();
    }

    if (onClick instanceof Function) {
      return (onClick as any)(...args);
    }

    return undefined;
  }

  private isSelected = () => {
    if (!this.props.path) {
      return false;
    }

    // not using router.isActive() since we also
    // have nested paths that we want to match (i.e. /admin/users/view)
    return this.props.location.pathname.indexOf(this.props.path) === 0;
  }
}

function isNewLozengeInteractableOnce(lozenge: NavItemLozenge | undefined): boolean {
  return !!lozenge && lozenge.type === 'new' && !!lozenge.interactOnce;
}

const getQueryOrMutationVariables = (props: NavigationLinkProps): (OnboardingQueryVariables & DismissOnboardingMutationVariables) => {
  return {
    id: `new-lozenge-${props.id}`,
  };
};

const getOverriddenLozenge = ({ data, ownProps: { lozenge } }: OptionProps<NavigationLinkProps, OnboardingQuery>): NavigationLinkProps['lozenge'] => {
  if (!data || data.loading || data.error || !data.onboarding || data.onboarding.dismissed) {
    return undefined;
  }

  return lozenge;
};

const withNewLozengeInteractedStatus = graphql<NavigationLinkProps, OnboardingQuery, OnboardingQueryVariables, NavigationLinkProps>(onboardingQuery, {
  options: (props) => ({
    variables: getQueryOrMutationVariables(props),
  }),
  skip: (props) => !isNewLozengeInteractableOnce(props.lozenge),
  props: (optionProps: OptionProps<NavigationLinkProps, OnboardingQuery>) => ({
    lozenge: getOverriddenLozenge(optionProps),
  }),
});
const withNewLozengeUpdateInteractedStatus = graphql<NavigationLinkProps, DismissOnboardingMutation, DismissOnboardingMutationVariables, NewLozengeMutationProps>(onboardingMutation, {
  options: (props) => ({
    variables: getQueryOrMutationVariables(props),
  }),
  skip: (props) => !isNewLozengeInteractableOnce(props.lozenge),
  props: ({ mutate, ownProps }: OptionProps<NavigationLinkProps, DismissOnboardingMutation>) => ({
    markItemAsViewed: () => mutate instanceof Function && mutate({
      refetchQueries: [
        { query: onboardingQuery, variables: getQueryOrMutationVariables(ownProps) },
      ],
    }).catch(_ => {
      /* the error would have been logged by the resolver, no need to duplicate it */
    }),
  }),
});

export const NavigationLinkWithoutApollo = withRouter(injectIntl(NavigationLinkImpl));

export const NavigationLink = withNewLozengeInteractedStatus(
  withNewLozengeUpdateInteractedStatus(NavigationLinkWithoutApollo),
);
