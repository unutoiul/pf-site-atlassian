// tslint:disable:jsx-use-translation-function
import { storiesOf } from '@storybook/react';
import * as React from 'react';

import AkButton from '@atlaskit/button';

import { ButtonGroup } from '.';

storiesOf('Common|Button Group', module)
  .add('Aligned left, with buttons', () => (
    <ButtonGroup alignment="left">
      <AkButton appearance="primary">
        Submit
      </AkButton>
      <AkButton>
        Cancel
      </AkButton>
    </ButtonGroup>
  ));
