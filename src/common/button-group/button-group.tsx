import * as React from 'react';
import styled from 'styled-components';

import { akGridSizeUnitless } from '@atlaskit/util-shared-styles';

interface ButtonGroupBaseProps {
  className?: string;
  alignment?: 'left' | 'center' | 'right';
}

const ButtonGroupBase: React.StatelessComponent<ButtonGroupBaseProps> = props => (
  <div className={props.className}>
    {props.children}
  </div>
);

export const ButtonGroup = styled(ButtonGroupBase) `
  text-align: ${props => props.alignment || 'left'};

  button + button {
    margin-left: ${akGridSizeUnitless / 2}px;
  }
`;
