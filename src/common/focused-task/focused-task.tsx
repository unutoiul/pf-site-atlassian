import * as React from 'react';
import { defineMessages, injectIntl } from 'react-intl';

import AkCrossIcon from '@atlaskit/icon/glyph/cross';
import { AkCustomDrawer } from '@atlaskit/navigation';

import {
  AnalyticsClientProps,
  focusedTaskCloseButtonClickEvent,
  focusedTaskLogoButtonClickEvent,
  FocusedTaskUIEventMeta,
  withAnalyticsClient,
} from 'common/analytics';
import { GlobalAtlassianIcon } from 'common/icons';

const defaultMessages = defineMessages({
  close: {
    id: 'common.focused-task.close.alt.text',
    defaultMessage: 'Close',
    description: 'The alternative text that is specified on the button which closes the focused task (also known as modal dialog)',
  },
});

export interface FocusedTaskProps {
  isOpen: boolean;
  /**
   * The partial data that gets sent along with click events happening for this focused task.
   * Provide orgId/cloudId, source screen name, attributes.
   */
  uiEvent?: FocusedTaskUIEventMeta;
  onClose?(): void;
}

const BackIcon = injectIntl<{ onClick?(): any }>(({ onClick, intl: { formatMessage } }) => (
  <AkCrossIcon
    onClick={onClick}
    label={formatMessage(defaultMessages.close)}
  />
));

export class FocusedTaskImpl extends React.Component<FocusedTaskProps & AnalyticsClientProps> {
  public render() {
    const {
      isOpen,
      onClose,
      children,
    } = this.props;

    return (
      <AkCustomDrawer
        width="full"
        isOpen={isOpen}
        backIcon={<BackIcon onClick={this.onBackIconClick} />}
        primaryIcon={<GlobalAtlassianIcon onClick={this.onLogoClick} />}
        onBackButton={onClose}
      >
        {children}
      </AkCustomDrawer>
    );
  }

  private onBackIconClick = () => {
    const {
      analyticsClient: { sendUIEvent },
      uiEvent,
    } = this.props;

    if (uiEvent) {
      sendUIEvent(focusedTaskCloseButtonClickEvent(uiEvent));
    }
  }

  private onLogoClick = () => {
    const {
      analyticsClient: { sendUIEvent },
      uiEvent,
    } = this.props;

    if (uiEvent) {
      sendUIEvent(focusedTaskLogoButtonClickEvent(uiEvent));
    }
  }
}

export const FocusedTask = withAnalyticsClient(FocusedTaskImpl);
