// tslint:disable jsx-use-translation-function
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';

import AkButton from '@atlaskit/button';
import AkSelect from '@atlaskit/select';

import { random } from '../../utilities/random';
import { FocusedTask } from './focused-task';

storiesOf('Common|Focused Task', module)
  .add('Default', () => {
    class Example extends React.Component<{}, { isOpen: boolean }> {
      public state = { isOpen: false };

      public render() {
        return (
          <IntlProvider locale="en">
            <React.Fragment>
              <AkButton onClick={this.open}>Open</AkButton>
              <FocusedTask
                isOpen={this.state.isOpen}
                onClose={this.close}
              >
                <p>
                  The content of your focused task would go here
                </p>
                <div>
                  Here's some selector for you to play with:
                  <AkSelect
                    id="test-select"
                    placeholder="Selector"
                    options={
                      Array(12).fill(null).map((_, month) => ({
                        value: month,
                        label: month.toLocaleString(undefined, { minimumIntegerDigits: 2 }),
                      }))
                    }
                  />
                </div>
                <div>
                  Below you can find a bunch of rectangles that will fill the screen to make focused task long enough for a scroll bar to appear
                  {
                    Array(100)
                      .fill(null)
                      .map(() => random(['green', 'blue', 'red', 'orange', 'violet']))
                      .map((color, index) => (
                        <div style={{ margin: '10px' }} key={index}>
                          <span
                            style={{
                              backgroundColor: color,
                              display: 'inline-block',
                              width: '100px',
                              height: '100px',
                            }}
                          />
                        </div>
                      ))
                  }
                </div>
              </FocusedTask>
            </React.Fragment>
          </IntlProvider>
        );
      }

      private open = () => this.setState({ isOpen: true });
      private close = () => {
        action('Closed focused task')();
        this.setState({ isOpen: false });
      }
    }

    return <Example />;
  },
);
