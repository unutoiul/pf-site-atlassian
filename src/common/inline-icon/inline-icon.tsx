import * as React from 'react';
import styled from 'styled-components';

import AkButton from '@atlaskit/button';
import AKInlineDialog from '@atlaskit/inline-dialog';

const InlineBlock = styled.div`
  display: inline-block;
`;

interface InlineDialogState {
  isOpen: boolean;
}

interface InlineDialogProps {
  tooltipContent: React.ReactNode;
  tooltipPosition?: string;
  icon: React.ReactElement<any>;
}

export class InlineIcon extends React.Component<InlineDialogProps, InlineDialogState> {

  public state = {
    isOpen: false,
  };

  public render() {
    const { tooltipContent, tooltipPosition, icon } = this.props;

    return (
      <InlineBlock>
        <AKInlineDialog
          content={tooltipContent}
          position={tooltipPosition}
          isOpen={this.state.isOpen}
          onClose={this.handleOnClose}
        >
          <AkButton appearance="subtle-link" onClick={this.handleClick} iconBefore={icon}/>
        </AKInlineDialog>
      </InlineBlock>
    );
  }

  protected handleOnClose = (data) => {
    this.setState({
      isOpen: data.isOpen,
    });
  };

  private handleClick = () => {
    this.setState(state => ({
      isOpen: !state.isOpen,
    }));
  };
}
