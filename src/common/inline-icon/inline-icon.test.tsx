import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';

import AkButton from '@atlaskit/button';
import AkWarningIcon from '@atlaskit/icon/glyph/warning';

import { InlineIcon } from './inline-icon';

const icon = <AkWarningIcon label="aaa"/>;

describe('Inline Icon', () => {
  it('Renders icon', () => {
    const inlineIcon = mount(
      <InlineIcon icon={icon} tooltipContent={'aaa'}/>,
    );

    expect(inlineIcon.find<any>(AkWarningIcon)).to.have.length(1);
  });

  it('Check tooltip is closed initially', () => {
    const inlineIcon = mount(
      <InlineIcon icon={icon} tooltipContent={<div id="testContent" />}/>,
    );
    expect(inlineIcon.find('#testContent')).to.have.length(0);
  });

  it('Check toggle tooltip', () => {
    const inlineIcon = mount(
      <InlineIcon icon={icon} tooltipContent={<div id="testToggle" />}/>,
    );
    const warningButton = inlineIcon.find(AkButton);
    warningButton.at(0).simulate('click');
    expect(inlineIcon.find('#testToggle')).to.have.length(1);

    warningButton.at(0).simulate('click');
    expect(inlineIcon.find('#testToggle')).to.have.length(0);
  });
});
