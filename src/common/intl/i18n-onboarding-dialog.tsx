// tslint:disable jsx-use-translation-function
// file not going to be translated so that users, who are currently using and understanding English, will have an update they definitely understand (just in case for some reason, they cannot understand their browser language)

import * as React from 'react';

import { OnboardingDialog } from 'common/onboarding-v2';

export const i18nOnboardingId = 'i18n';

export class I18nOnboardingDialogImpl extends React.Component {
  public render() {
    return (
      <OnboardingDialog
        name={i18nOnboardingId}
        target="HelpMenu"
        dialogPlacement="bottom right"
        targetRadius={100}
        pulse={true}
        actionTitle="Ok, got it"
      >
        <h3>We've gone international!</h3>
        <p>
          Most of Atlassian admin pages now use the language setting in your browser.
          To see how to change back to English, click the help button. We'd love feedback on this change!
        </p>
      </OnboardingDialog>
    );
  }
}

export const I18nOnboardingDialog = I18nOnboardingDialogImpl;
