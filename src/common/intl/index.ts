export { locale } from './locale';
export {
  FeatureFlagI18nGerman,
  withI18nGermanFeatureFlag,
  FeatureFlagI18nJapanese,
  withI18nJapaneseFeatureFlag,
} from './with-i18n-feature-flag';
export {
  i18nOnboardingId,
  I18nOnboardingDialog,
} from './i18n-onboarding-dialog';
