import { FeatureFlagProps, withUserFeatureFlag } from 'common/feature-flags';

import { i18nGermanFeatureFlagKey, i18nJapaneseFeatureFlagKey } from './flag-keys';
export interface FeatureFlagI18nGerman {
  i18nGermanFeatureFlag: FeatureFlagProps;
}
export interface FeatureFlagI18nJapanese {
  i18nJapaneseFeatureFlag: FeatureFlagProps;
}
export function withI18nGermanFeatureFlag<TOwnProps>(
  c: React.ComponentType<TOwnProps & FeatureFlagI18nGerman>,
): React.ComponentClass<TOwnProps> {
  return withUserFeatureFlag<TOwnProps, FeatureFlagI18nGerman>({
    flagKey: i18nGermanFeatureFlagKey,
    defaultValue: false,
    name: 'i18nGermanFeatureFlag',
  })(c);
}
export function withI18nJapaneseFeatureFlag<TOwnProps>(
  c: React.ComponentType<TOwnProps & FeatureFlagI18nJapanese>,
): React.ComponentClass<TOwnProps> {
  return withUserFeatureFlag<TOwnProps, FeatureFlagI18nJapanese>({
    flagKey: i18nJapaneseFeatureFlagKey,
    defaultValue: false,
    name: 'i18nJapaneseFeatureFlag',
  })(c);
}
