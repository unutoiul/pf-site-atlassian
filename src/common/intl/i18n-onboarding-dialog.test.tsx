import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import { OnboardingDialog } from 'common/onboarding-v2';

import { I18nOnboardingDialog, i18nOnboardingId } from './i18n-onboarding-dialog';

describe('I18nOnboardingDialog', () => {
  const onboardingDialogWrapper =
    shallow(
      <I18nOnboardingDialog
      />,
    );

  it('should render OnboardingDialog', () => {
    expect(onboardingDialogWrapper.find(OnboardingDialog).length).to.equal(1);
    expect((onboardingDialogWrapper.find(OnboardingDialog).props() as any).name).to.equal(i18nOnboardingId);
    expect((onboardingDialogWrapper.find(OnboardingDialog).props() as any).target).to.equal('HelpMenu');
    expect((onboardingDialogWrapper.find(OnboardingDialog).props() as any).dialogPlacement).to.equal('bottom right');
    expect((onboardingDialogWrapper.find(OnboardingDialog).props() as any).pulse).to.equal(true);
    expect((onboardingDialogWrapper.find(OnboardingDialog).props() as any).actionTitle).to.equal('Ok, got it');
  });

  it('should render all correct message from I18nOnboardingDialogImpl', () => {
    expect(onboardingDialogWrapper.at(0).find('p').props().children).to.equal(`Most of Atlassian admin pages now use the language setting in your browser. To see how to change back to English, click the help button. We'd love feedback on this change!`);
    expect(onboardingDialogWrapper.at(0).find('h3').props().children).to.equal(`We've gone international!`);
  });
});
