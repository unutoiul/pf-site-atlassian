export const locale = {
  languageToLocale: (language: string) => (
    language.split('-')[0]
  ),
};
