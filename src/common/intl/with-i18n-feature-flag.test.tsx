import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';

import { ComposeUserFeatureFlagApolloClient } from 'common/feature-flags/test-utils';

import { i18nGermanFeatureFlagKey, i18nJapaneseFeatureFlagKey } from './flag-keys';
import {
  FeatureFlagI18nGerman,
  withI18nGermanFeatureFlag,
  withI18nJapaneseFeatureFlag,
} from './with-i18n-feature-flag';

describe('withI18nFeatureFlag', () => {
  const TestComponent = ({ i18nGermanFeatureFlag, i18nJapaneseFeatureFlag }) => <div>{JSON.stringify(i18nGermanFeatureFlag)}{JSON.stringify(i18nJapaneseFeatureFlag)}</div>;
  const TestWrappedComponent = withI18nGermanFeatureFlag<{}>(
    withI18nJapaneseFeatureFlag<FeatureFlagI18nGerman>(TestComponent),
    );

  const mountWrappedComponentWithI18nFeatureFlags = () => {
    const flags = [
      { flagKey: i18nGermanFeatureFlagKey, flagValue: true },
      { flagKey: i18nJapaneseFeatureFlagKey, flagValue: true },
    ];

    return mount(
      <ComposeUserFeatureFlagApolloClient flags={flags}>
            <TestWrappedComponent />
      </ComposeUserFeatureFlagApolloClient>,
    );
  };

  it('should pass i18n feature flag to component', () => {
    const wrapper = mountWrappedComponentWithI18nFeatureFlags();
    const testComponent = wrapper.find(TestComponent);
    expect(testComponent.props()).to.deep.equal({
      i18nGermanFeatureFlag: {
        isLoading: false,
        value: true,
        defaultValue: false,
      },
      i18nJapaneseFeatureFlag: {
        isLoading: false,
        value: true,
        defaultValue: false,
      },
    });
  });
});
