import * as React from 'react';
import {
  defineMessages,
  InjectedIntlProps,
  injectIntl,
} from 'react-intl';

import AkEmptyState from '@atlaskit/empty-state';

import { noPermissionScreenEvent, ScreenEventSender } from 'common/analytics';
import { PageLayout } from 'common/page-layout';

const messages = defineMessages({
  title: {
    id: 'permission.errors.site.header',
    defaultMessage: 'You need permission',
  },
  description: {
    id: 'permission.errors.site.description',
    defaultMessage: 'Request access from your administrator.',
  },
});

export class PermissionErrorPageImpl extends React.Component<InjectedIntlProps> {
  public render() {
    const { formatMessage } = this.props.intl;

    return (
      <ScreenEventSender
        event={noPermissionScreenEvent()}
      >
        <PageLayout isFullWidth={true} collapsedNav={true}>
          <AkEmptyState
            // tslint:disable-next-line:no-require-imports
            imageUrl={require('./lock.svg')}
            header={formatMessage(messages.title)}
            description={formatMessage(messages.description)}
            size="narrow"
          />
        </PageLayout>
      </ScreenEventSender>
    );
  }
}

export const PermissionErrorPage = injectIntl<{}>(PermissionErrorPageImpl);
