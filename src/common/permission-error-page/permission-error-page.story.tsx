import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';

import AkPage from '@atlaskit/page';

import { createApolloClient } from '../../apollo-client';
import { PermissionErrorPage } from './permission-error-page';

const client = createApolloClient();

storiesOf('Common|Permission Error Page', module)
  .add('Default', () => {
    return (
      <ApolloProvider client={client}>
        <IntlProvider locale="en">
          <AkPage>
            <PermissionErrorPage />
          </AkPage>
        </IntlProvider>
      </ApolloProvider>
    );
  });
