import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { FormattedHTMLMessage, IntlProvider } from 'react-intl';
import styled from 'styled-components';

import { Banner } from './banner';

const Container = styled.div`
  width: 800px;
  margin: 24px;
`;

storiesOf('Common|Banner', module)
  .add('Default', () => {
    const message = {
      id: 'test.body',
      defaultMessage: `<strong>Lorem ipsum</strong> dolor amet keffiyeh blue bottle man bun farm-to-table.
        Palo santo you probably haven't heard of them fingerstache salvia. Jianbing yuccie salvia tattooed.
        Pop-up woke copper mug pour-over twee quinoa salvia yuccie tumeric.`,
    };

    return (
      <IntlProvider locale="en">
        <Container>
          <Banner
            message={<FormattedHTMLMessage {...message} />}
            actions={[
              {
                text: {
                  id: 'test',
                  defaultMessage: 'Create something',
                },
                onClick: () => null,
              },
              {
                text: {
                  id: 'test',
                  defaultMessage: 'Learn more',
                },
                onClick: () => null,
              },
            ]}
            // tslint:disable-next-line:no-require-imports
            image={require('../../dashboard/org.svg')}
            imageMaxHeight={130}
          />
        </Container>
      </IntlProvider>
    );
  });
