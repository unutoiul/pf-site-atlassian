import * as React from 'react';
import { FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import styled, { ThemeProvider } from 'styled-components';

import AkButton, { ButtonGroup as AkButtonGroup } from '@atlaskit/button';
import {
  borderRadius as AkBorderRadius,
  colors as AkColors,
  gridSize as akGridSize,
  themed as AkThemed,
} from '@atlaskit/theme';

import { buttonTheme, buttonThemeNamespace } from './button-theme';

const borderColor = AkThemed({
  light: AkColors.N60A,
  dark: AkColors.DN60A,
});

const shadowColor = AkThemed({
  light: AkColors.N50A,
  dark: AkColors.DN50A,
});

const boxShadow = props => {
  const border = `0 0 1px ${borderColor(props)}`;
  const shadow = `0 ${akGridSize() / 2}px ${akGridSize()}px -${akGridSize() / 4}px ${shadowColor(props)}`;

  return [border, shadow].join(',');
};

const Box = styled.div`
  width: 100%;
  background: ${AkColors.P300};
  border-radius: ${AkBorderRadius}px;
  box-shadow: ${boxShadow};
  box-sizing: border-box;
  color: ${AkColors.N0};
  padding: ${akGridSize() * 2}px ${akGridSize() * 3}px;
  display: flex;
  align-items: center;
`;

const Content = styled.div`
  flex: 2 1 auto;
`;

const ContentFooter = styled.div`
  margin-top: ${akGridSize() * 2}px;
`;

const SidebarImage = styled<{ maxHeight?: number }, 'img'>('img')`
  flex: 1 1 auto;
  padding: ${akGridSize() / 2}px;
  max-height: ${({ maxHeight }) => maxHeight ? `${maxHeight}px` : 'none' };
`;

export interface BannerProps {
  message: React.ReactNode;
  image?: string;
  imageMaxHeight?: number;
  actions: BannerActionProps[];
}

export interface BannerActionProps {
  text: FormattedMessage.MessageDescriptor;
  onClick?(): any;
}

export class BannerImpl extends React.Component<BannerProps & InjectedIntlProps> {
  public render() {
    const {
      message,
      image,
      imageMaxHeight,
      actions,
      intl: { formatMessage },
    } = this.props;

    return (
      <ThemeProvider theme={{ [buttonThemeNamespace]: buttonTheme }}>
        <Box>
          <Content>
            {message}
            <ContentFooter>
              <AkButtonGroup>
                {actions ? actions.map(({ text, onClick }, idx) => {
                  const variant = idx ? 'subtle-link' : 'primary';

                  return (
                    <AkButton key={idx} appearance={variant} onClick={onClick}>
                      {formatMessage(text)}
                    </AkButton>
                  );
                }) : null}
              </AkButtonGroup>
            </ContentFooter>
          </Content>
          {image ? <SidebarImage src={image} maxHeight={imageMaxHeight} /> : null}
        </Box>
      </ThemeProvider>
    );
  }
}

export const Banner = injectIntl(BannerImpl);
