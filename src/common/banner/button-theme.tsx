import {
  colors as AkColors,
  themed as AkThemed,
} from '@atlaskit/theme';

export const buttonThemeNamespace = '@atlaskit-shared-theme/button';

export const buttonTheme = {
  primary: {
    background: {
      default: AkThemed({ light: AkColors.P400, dark: AkColors.P400 }),
      hover: AkThemed({ light: AkColors.P200, dark: AkColors.P200 }),
      active: AkThemed({ light: AkColors.P500, dark: AkColors.P500 }),
      disabled: AkThemed({ light: AkColors.N30, dark: AkColors.DN70 }),
      selected: AkThemed({ light: AkColors.R500, dark: AkColors.R500 }),
    },
    boxShadowColor: {
      focus: AkThemed({ light: AkColors.P100, dark: AkColors.P100 }),
    },
    color: {
      default: AkThemed({ light: AkColors.N0, dark: AkColors.N0 }),
      disabled: AkThemed({ light: AkColors.N0, dark: AkColors.DN30 }),
      selected: AkThemed({ light: AkColors.N0, dark: AkColors.N0 }),
    },
  },
  'subtle-link': {
    color: {
      default: AkThemed({ light: AkColors.N0, dark: AkColors.N0 }),
      disabled: AkThemed({ light: AkColors.N0, dark: AkColors.N0 }),
      selected: AkThemed({ light: AkColors.N0, dark: AkColors.N0 }),
    },
  },
};
