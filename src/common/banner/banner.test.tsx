import { expect } from 'chai';
import { mount, ReactWrapper } from 'enzyme';
import * as React from 'react';
import { FormattedHTMLMessage } from 'react-intl';

import { createMockIntlContext } from '../../utilities/testing';
import { Banner, BannerProps } from './banner';

describe('Banner', () => {
  const createWrapper = (props?: Partial<BannerProps>): ReactWrapper<BannerProps> => {
    const message = {
      id: 'test.message',
      defaultMessage: 'Test description',
    };

    return mount((
        <Banner
          message={<FormattedHTMLMessage {...message} />}
          actions={[
            {
              text: {
                id: 'action.create',
                defaultMessage: 'Create',
              },
            },
          ]}
          {...props as any}
        />
      ),
      createMockIntlContext(),
    );
  };

  it('should render a message', () => {
    const wrapper = createWrapper();
    expect(wrapper.text()).to.include('Test description');
  });

  it('should render an image', () => {
    const wrapper = createWrapper({
      image: 'test-image.svg',
    });

    expect(wrapper.find('img').exists()).to.equal(true);
  });

  it('should render actions', () => {
    const wrapper = createWrapper({
      actions: [
        {
          text: {
            id: 'action.create',
            defaultMessage: 'Create',
          },
        },
        {
          text: {
            id: 'action.delete',
            defaultMessage: 'Delete',
          },
        },
      ],
    });

    expect(wrapper.find('button')).to.have.length(2);
  });
});
