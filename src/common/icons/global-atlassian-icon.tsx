import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import { AtlassianIcon as AkAtlassianIcon } from '@atlaskit/logo';

import { NavigationTooltip } from 'common/navigation-tooltip';

const messages = defineMessages({
  logoAltText: {
    id: 'chrome.navigation.global-logo-label',
    description: 'Alt text for the logo',
    defaultMessage: 'Atlassian logo',
  },
  globalItemButtonTooltip: {
    id: 'chrome.navigation.global-logo.tooltip',
    defaultMessage: 'Admin',
  },
});

export interface GlobalAtlassianIconProps {
  onClick?(): void;
}

class GlobalAtlassianIconImpl extends React.Component<GlobalAtlassianIconProps & InjectedIntlProps> {
  public render() {
    const {
      intl: { formatMessage },
      onClick,
    } = this.props;

    return (
      <NavigationTooltip message={formatMessage(messages.globalItemButtonTooltip)}>
        <AkAtlassianIcon
          label={formatMessage(messages.logoAltText)}
          size="large"
          onClick={onClick}
        />
      </NavigationTooltip>
    );
  }
}

export const GlobalAtlassianIcon = injectIntl(GlobalAtlassianIconImpl);
