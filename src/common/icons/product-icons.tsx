import * as React from 'react';
import styled from 'styled-components';

import {
  AtlassianLogo as AkAtlassianLogo,
  BitbucketIcon as AkBitbucketIcon,
  ConfluenceIcon as AkConfluenceIcon,
  JiraCoreIcon as AkJiraCoreIcon,
  JiraServiceDeskIcon as AkJiraServiceDeskIcon,
  JiraSoftwareIcon as AkJiraSoftwareIcon,
  StrideIcon as AkStrideIcon,
} from '@atlaskit/logo';
import { colors as akColors, gridSize as akGridSize } from '@atlaskit/theme';

// tslint:disable-next-line:no-require-imports no-var-requires
const jiraIncidentManagerLogoImagePath = require('./jira-incident-manager.svg');
// tslint:disable-next-line:no-require-imports no-var-requires
const opsGenieLogoImagePath = require('./opsgenie.svg');

export type ProductName =
  'Bitbucket' |
  'Jira Ops' |
  'Jira Core' |
  'Jira Service Desk' |
  'Jira Software' |
  'Opsgenie' |
  'Stride' |
  'Confluence';

const IconContainer = styled.div`
  width: ${akGridSize() * 3}px;
  height: ${akGridSize() * 3}px;
  padding: ${akGridSize() / 4}px;
  box-sizing: border-box;
`;

const Icon = styled.div`
  width: ${akGridSize() * 2.5}px;
  height: ${akGridSize() * 2.5}px;
  background: center url(${({ imageUrl }: { imageUrl: string }) => imageUrl}) no-repeat;
`;

interface ProductIconProps {
  productName: ProductName;
}

export class ProductIcon extends React.Component<ProductIconProps > {
  public render() {
    const { productName } = this.props;

    const iconProps = {
      size: 'small',
      iconColor: akColors.B200,
      iconGradientStart: akColors.B400,
      iconGradientStop: akColors.B200,
    };
    switch (productName.toLowerCase()) {
      case 'bitbucket':
        return <AkBitbucketIcon label={productName} {...iconProps} />;
      case 'jira ops':
        return <IconContainer><Icon imageUrl={jiraIncidentManagerLogoImagePath} /></IconContainer>;
      case 'jira core':
        return <AkJiraCoreIcon label={productName} {...iconProps} />;
      case 'jira service desk':
        return <AkJiraServiceDeskIcon label={productName} {...iconProps} />;
      case 'jira software':
        return <AkJiraSoftwareIcon label={productName}{...iconProps} />;
      case 'stride':
        return <AkStrideIcon label={productName} {...iconProps} />;
      case 'confluence':
        return <AkConfluenceIcon label={productName} {...iconProps} />;
      case 'opsgenie':
        return <IconContainer><Icon imageUrl={opsGenieLogoImagePath} /></IconContainer>;
      default:
        return <AkAtlassianLogo label={productName} {...iconProps} />;
    }
  }
}
