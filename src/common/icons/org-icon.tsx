import * as React from 'react';
import styled, { css } from 'styled-components';

import AkOfficeBuildingIcon from '@atlaskit/icon/glyph/office-building';
import {
  borderRadius as akBorderRadius,
  colors as akColors,
  gridSize as akGridSize,
} from '@atlaskit/theme';

import { Centered } from 'common/styled';

function square(size: OrgIconSize) {
  const multiplier = size === 'large' ? 5 : 4;

  return css`
    width: ${akGridSize() * multiplier}px;
    height: ${akGridSize() * multiplier}px;
  `;
}

const OrgAvatarBackground = styled(Centered) `
  ${(props: { size: OrgIconSize }) => square(props.size)}
  border-radius: ${akBorderRadius}px;
  background-color: ${akColors.N900};
`;

export type OrgIconSize = 'large' | 'medium';

export interface OrgIconProps {
  size?: OrgIconSize;
  primaryColor?: string;
}

export const OrgIcon = (props: OrgIconProps) => (
  <OrgAvatarBackground size={props.size || 'medium'}>
    <AkOfficeBuildingIcon label="" size={props.size || 'medium'} primaryColor={props.primaryColor} />
  </OrgAvatarBackground>
);
