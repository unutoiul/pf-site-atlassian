import * as React from 'react';
import styled from 'styled-components';

interface WrapperProps {
  maxWidth: string;
}

interface OwnProps {
  children: React.ReactNode;
  title?: string;
}

const Wrapper = styled.span`
  max-width: ${({ maxWidth }: WrapperProps) => maxWidth};
  text-overflow: ellipsis;
  white-space: nowrap;
  display: inline-block;
  overflow: hidden;
`;

export class TruncatedField extends React.Component<OwnProps & WrapperProps> {
  public render() {

    const { title, children, maxWidth } = this.props;

    return (
        <Wrapper
          title={title || undefined}
          maxWidth={maxWidth}
        >
          {children}
        </Wrapper>
    );
  }
}
