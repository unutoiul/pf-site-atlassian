// tslint:disable:jsx-use-translation-function
import { storiesOf } from '@storybook/react';
import * as React from 'react';

import AkDynamicTable from '@atlaskit/dynamic-table';
import { akGridSizeUnitless } from '@atlaskit/util-shared-styles';

import { TruncatedField } from '.';

const head = {
  cells: [
    {
      key: '1',
      content: 'Truncated Field',
    },
    {
      key: '2',
      content: 'Normal Field',
    },
  ],
};

const longContent = `Lorem ipsum dolor sit amet consectetur adipisicing elit.
Fuga ad porro ratione facere rem vitae iusto culpa maiores
eveniet deserunt quibusdam quae reprehenderit iste quidem,
autem libero ea quia. Dolor.`;

const rows = [
  {
    cells: [
      {
        key: '1',
        content: (
          <TruncatedField title={longContent} maxWidth={`${akGridSizeUnitless * 30}px`}>
            {longContent}
          </TruncatedField>
        ),
      },
      {
        key: '2',
        content: (
          <div>
            {longContent}
          </div>
        ),
      },
    ],
  },
  {
    cells: [
      {
        key: '1',
        content: (
          <TruncatedField maxWidth={`${akGridSizeUnitless * 30}px`}>
            $500,000,000.00
          </TruncatedField>
        ),
      },
      {
        key: '2',
        content: (
          <div>
            $500,000,000.00
        </div>
        ),
      },
    ],
  },
];

storiesOf('Common|Truncated Field', module)
  .add('Truncated and normal field in a table', () => {
    return (
      <AkDynamicTable
        caption="Table"
        head={head}
        rows={rows}
        emptyView="Empty"
        width="200"
      />
    );
  });
