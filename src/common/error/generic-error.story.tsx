// tslint:disable:jsx-use-translation-function
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';
import styled from 'styled-components';

import { gridSize as akGridSize } from '@atlaskit/theme';

import { GenericError, GenericErrorMessage } from './generic-error';

storiesOf('Common|Generic Error', module)
  .add('Default', () => {
    const Page = styled.div`
      padding: ${akGridSize() * 4}px ${akGridSize() * 2}px;
      margin: 0 auto;
      max-width:  ${akGridSize() * 100}px;
    `;

    const ExampleContainer = styled.div`
      border: 1px solid #DFE1E6;
      margin: ${akGridSize() * 3}px;
      padding: ${akGridSize() * 3}px;
    `;

    return (
      <IntlProvider locale="en">
        <Page>
          <h2>Error flags</h2>
          <p>
            If you are displaying an error flag, you can use errorFlagMessages.title and errorFlagMessages.description:
            <ExampleContainer>
              <pre>{
`  props.showFlag({
    autoDismiss: true,
    icon: createErrorIcon(),
    id: 'react-loadable-error',
    title: props.intl.formatMessage(errorFlagMessages.title),
    description: props.intl.formatMessage(errorFlagMessages.description),
  });`
                }
              </pre>
            </ExampleContainer>
          </p>
          <p>However, you should try to use a more specific message for either title or description if possible.</p>
          <h2>GenericErrorMessage component</h2>
          <p>For errors other than flags, use the GenericErrorMessage component if you just need text, example:</p>
          <ExampleContainer><GenericErrorMessage /></ExampleContainer>
          <h2>GenericError component</h2>
          <p>Use the GenericError component for an error with image. This could be shown as a standalone page or as part of a page where dynamic data fails to load. You can override either the title or description with more detailed messages. Example:</p>
          <ExampleContainer>
            <GenericError />
          </ExampleContainer>
        </Page>
      </IntlProvider>

    );
  });
