import { ApolloError } from 'apollo-client';
import { expect } from 'chai';

import { RequestError } from '.';
import { isPreconditionFailedError, PreconditionFailedError } from './precondition-failed-error';

describe('Precondition Failed error', () => {
  describe('isPreconditionFailedError', () => {
    it('should return true for PreconditionFailedError', () => {
      const error = new PreconditionFailedError({});

      expect(isPreconditionFailedError(error)).to.equal(true);
    });

    it('should return false for non PreconditionFailedError', () => {
      const error = new RequestError({});

      expect(isPreconditionFailedError(error)).to.equal(false);
    });
  });

  describe('findIn', () => {
    it('should find PreconditionFailedError', () => {
      const preconditionFailedError = new PreconditionFailedError({});
      const requestError = new RequestError({});
      const apolloError = new ApolloError({
        graphQLErrors: [
          { originalError: preconditionFailedError } as any,
          { originalError: requestError } as any,
        ],
      });

      expect(PreconditionFailedError.findIn(apolloError).length).to.equal(1);
    });

    it('should not find PreconditionFailedError when there is none', () => {
      const requestError = new RequestError({});
      const apolloError = new ApolloError({ graphQLErrors: [{ originalError: requestError } as any] });

      expect(PreconditionFailedError.findIn(apolloError).length).to.equal(0);
    });
  });

});
