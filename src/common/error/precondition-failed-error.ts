import { ApolloError } from 'apollo-client';

import { RequestError, RequestErrorInfo } from './request-error';

export const isPreconditionFailedError = (e: any): e is PreconditionFailedError => {
  return e instanceof PreconditionFailedError;
};

export class PreconditionFailedError extends RequestError {
  public name: string = 'PreconditionFailedError';

  constructor(options: RequestErrorInfo) {
    super(options);

    // fixing the `instanceof` operators, see https://github.com/Microsoft/TypeScript/wiki/Breaking-Changes#extending-built-ins-like-error-array-and-map-may-no-longer-work
    Object.setPrototypeOf(this, PreconditionFailedError.prototype);
  }

  public static findIn({ graphQLErrors = [] }: ApolloError): PreconditionFailedError[] {
    return graphQLErrors
      .map(graphQLError => graphQLError.originalError)
      .filter(isPreconditionFailedError);
  }
}
