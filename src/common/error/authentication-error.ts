import { RequestError, RequestErrorInfo } from './request-error';

export class AuthenticationError extends RequestError {
  public name: string = 'AuthenticationError';

  constructor(options: RequestErrorInfo) {
    super(options);

    // fixing the `instanceof` operators, see https://github.com/Microsoft/TypeScript/wiki/Breaking-Changes#extending-built-ins-like-error-array-and-map-may-no-longer-work
    Object.setPrototypeOf(this, AuthenticationError.prototype);
  }
}
