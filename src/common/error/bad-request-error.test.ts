import { ApolloError } from 'apollo-client';
import { expect } from 'chai';

import { RequestError } from '.';
import { BadRequestError, isBadRequestError } from './bad-request-error';

describe('Bad request error', () => {
  describe('isBadRequestError', () => {
    it('should return true for BadRequestError', () => {
      const error = new BadRequestError({});

      expect(isBadRequestError(error)).to.equal(true);
    });

    it('should return false for non BadRequestError', () => {
      const error = new RequestError({});

      expect(isBadRequestError(error)).to.equal(false);
    });
  });

  describe('findIn', () => {
    it('should find BadRequestError', () => {
      const badRequestError = new BadRequestError({});
      const requestError = new RequestError({});
      const apolloError = new ApolloError({ graphQLErrors: [
        { originalError: badRequestError } as any,
        { originalError: requestError } as any,
      ] });

      expect(BadRequestError.findIn(apolloError).length).to.equal(1);
    });

    it('should not find BadRequestError when there is none', () => {
      const requestError = new RequestError({});
      const apolloError = new ApolloError({ graphQLErrors: [{ originalError: requestError } as any] });

      expect(BadRequestError.findIn(apolloError).length).to.equal(0);
    });
  });

});
