import * as React from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';

import AkEmptyState from '@atlaskit/empty-state';

import { errorWindowImage } from 'common/images';

// These should only be used for error flags, use the components for all other error messages
export const errorFlagMessages = defineMessages({
  title: {
    id: 'common.generic-error.title',
    defaultMessage: 'Something went wrong',
    description: 'Generic error message title',
  },
  description: {
    id: 'common.generic-error.description',
    defaultMessage: 'Try again later.',
    description: 'Generic error message description',
  },
});

const errorMessages = defineMessages({
  error: {
    id: 'common.generic-error',
    defaultMessage: 'Something went wrong. Try again later.',
  },
});

export class GenericErrorMessage extends React.Component {
  public render() {
    return <FormattedMessage {...errorMessages.error} />;
  }
}

interface GenericErrorProps {
  header?: React.ReactNode;
  description?: React.ReactNode;
}

// tslint:disable-next-line:max-classes-per-file
export class GenericError extends React.Component<GenericErrorProps> {
  public render() {
    return (
      <AkEmptyState
        imageUrl={errorWindowImage}
        header={this.props.header || <FormattedMessage {...errorFlagMessages.title} />}
        description={this.props.description || <FormattedMessage {...errorFlagMessages.description} />}
        size="wide"
      />
    );
  }
}
