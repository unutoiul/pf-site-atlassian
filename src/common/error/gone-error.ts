import { ApolloError } from 'apollo-client';

import { RequestError, RequestErrorInfo } from './request-error';

export const isGoneError = (e: any): e is GoneError => {
  return e instanceof GoneError;
};

export class GoneError extends RequestError {
  public name: string = 'GoneError';

  constructor(options: RequestErrorInfo) {
    super(options);

    // fixing the `instanceof` operators, see https://github.com/Microsoft/TypeScript/wiki/Breaking-Changes#extending-built-ins-like-error-array-and-map-may-no-longer-work
    Object.setPrototypeOf(this, GoneError.prototype);
  }

  public static findIn({ graphQLErrors = [] }: ApolloError): GoneError[] {
    return graphQLErrors
      .map(graphQLError => graphQLError.originalError)
      .filter(isGoneError);
  }
}
