import { ApolloError } from 'apollo-client';

import { RequestError, RequestErrorInfo } from './request-error';

const isAuthorizationError = (e: any): e is AuthorizationError => {
  return e instanceof AuthorizationError;
};

export class AuthorizationError extends RequestError {
  public name: string = 'AuthorizationError';

  constructor(options: RequestErrorInfo) {
    super(options);

    // fixing the `instanceof` operators, see https://github.com/Microsoft/TypeScript/wiki/Breaking-Changes#extending-built-ins-like-error-array-and-map-may-no-longer-work
    Object.setPrototypeOf(this, AuthorizationError.prototype);
  }

  public static findIn({ graphQLErrors = [] }: ApolloError): AuthorizationError[] {
    return graphQLErrors
      .map(graphQLError => graphQLError.originalError)
      .filter(isAuthorizationError);
  }
}
