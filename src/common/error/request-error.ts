export interface RequestErrorInfo {
  message?: string;
  status?: number;
  title?: string;
  description?: string;
  id?: string;
  ignore?: boolean;
  legacySkipDefaultErrorFlag?: boolean;
}

export class RequestError extends Error {
  public name: string = 'RequestError';
  public message!: string;
  public status?: number;
  public title?: string;
  public description?: string;
  public id?: string;
  public ignore?: boolean;
  public legacySkipDefaultErrorFlag?: boolean;

  constructor({ message, status, title, description, id, ignore, legacySkipDefaultErrorFlag }: RequestErrorInfo) {
    super(message ? message : `Failed with status: ${status}`);
    this.status = status;
    this.title = title;
    this.description = description;
    this.id = id;
    this.ignore = ignore;
    this.legacySkipDefaultErrorFlag = legacySkipDefaultErrorFlag;

    // fixing the `instanceof` operators, see https://github.com/Microsoft/TypeScript/wiki/Breaking-Changes#extending-built-ins-like-error-array-and-map-may-no-longer-work
    Object.setPrototypeOf(this, RequestError.prototype);
  }

  public updateDetails({ title, description }: Pick<RequestErrorInfo, 'title' | 'description'>): void {
    if (title) {
      this.title = title;
    }
    if (description) {
      this.description = description;
    }
  }

  public static isRequestError = (e: any): e is RequestError => {
    return e instanceof RequestError;
  }
}
