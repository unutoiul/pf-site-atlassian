import { ApolloError } from 'apollo-client';

import { RequestError, RequestErrorInfo } from './request-error';

export const isBadRequestError = (e: any): e is BadRequestError => {
  return e instanceof BadRequestError;
};

export class BadRequestError extends RequestError {
  public name: string = 'BadRequestError';

  constructor(options: RequestErrorInfo) {
    super(options);

    // fixing the `instanceof` operators, see https://github.com/Microsoft/TypeScript/wiki/Breaking-Changes#extending-built-ins-like-error-array-and-map-may-no-longer-work
    Object.setPrototypeOf(this, BadRequestError.prototype);
  }

  public static findIn({ graphQLErrors = [] }: ApolloError): BadRequestError[] {
    return graphQLErrors
      .map(graphQLError => graphQLError.originalError)
      .filter(isBadRequestError);
  }
}
