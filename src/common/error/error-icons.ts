import * as React from 'react';

import AKCheckCircleIcon from '@atlaskit/icon/glyph/check-circle';
import AkWarningIcon from '@atlaskit/icon/glyph/warning';
import { akColorG300, akColorR300, akColorY300 } from '@atlaskit/util-shared-styles';

export const createErrorIcon = () => React.createElement(AkWarningIcon, { label: '', primaryColor: akColorY300 });
export const createDangerIcon = () => React.createElement(AkWarningIcon, { label: '', primaryColor: akColorR300 });
export const createSuccessIcon = () => React.createElement(AKCheckCircleIcon, { label: '', primaryColor: akColorG300 });
