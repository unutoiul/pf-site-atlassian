import { ApolloError } from 'apollo-client';

import { RequestError, RequestErrorInfo } from './request-error';

export interface ValidationErrorInfo extends RequestErrorInfo {
  code: string;
}

const isValidationError = (e: any): e is ValidationError => {
  return e instanceof ValidationError;
};

export class ValidationError extends RequestError {
  public name: string = 'ValidationError';
  public code: string;

  constructor({ code, ...options }: ValidationErrorInfo) {
    super({
      message: `Validation failed: "${code}"`,
      ...options,
    });

    this.code = code;

    // fixing the `instanceof` operators, see https://github.com/Microsoft/TypeScript/wiki/Breaking-Changes#extending-built-ins-like-error-array-and-map-may-no-longer-work
    Object.setPrototypeOf(this, ValidationError.prototype);
  }

  public static findIn({ graphQLErrors = [] }: ApolloError): ValidationError[] {
    return graphQLErrors
      .map(graphQLError => graphQLError.originalError)
      .filter(isValidationError);
  }
}
