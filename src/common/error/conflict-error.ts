import { ApolloError } from 'apollo-client';

import { RequestError, RequestErrorInfo } from './request-error';

export const isConflictError = (e: any): e is ConflictError => {
  return e instanceof ConflictError;
};

export class ConflictError extends RequestError {
  public name: string = 'ConflictError';

  constructor(options: RequestErrorInfo) {
    super(options);

    // fixing the `instanceof` operators, see https://github.com/Microsoft/TypeScript/wiki/Breaking-Changes#extending-built-ins-like-error-array-and-map-may-no-longer-work
    Object.setPrototypeOf(this, ConflictError.prototype);
  }

  // tslint:disable-next-line:check-fetch-result
  public async setMessageFromResponse(response: Response) {
    // If response contains a message, add it to error description so that it gets shown in error flag
    const contentType = response.headers.get('content-type');

    try {
      if (contentType && contentType.includes('application/json')) {
        const json = await response.clone().json();
        if (json.message) {
          this.message = json.message;
        }
      }
    } catch (_) {
      // no handling is necessary here
    }
  }

  public static findIn({ graphQLErrors = [] }: ApolloError): ConflictError[] {
    return graphQLErrors
      .map(graphQLError => graphQLError.originalError)
      .filter(isConflictError);
  }
}
