import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { spy } from 'sinon';

import { Spotlight as AkSpotlight } from '@atlaskit/onboarding';

import { OnboardingDialogImpl } from 'common/onboarding-v2/onboarding-dialog';

describe('OnboardingDialog', () => {
  const onboardingDialogWrapper = ({
    isLoading = false,
    isDismissed = false,
    hasError = false,
    name = 'test',
    actionTitle = 'OK',
    mutate = spy(),
  }) => {
    const data = {
      loading: isLoading,
      onboarding: {
        dismissed: isDismissed,
      },
      hasError,
    } as any;

    return shallow((
      <OnboardingDialogImpl
        hasError={data.hasError}
        loading={data.loading}
        onboarding={data.onboarding}
        name={name}
        target="target"
        actionTitle={actionTitle}
        mutate={mutate}
      />
    ));
  };

  it('should render Spotlight', () => {
    const wrapper = onboardingDialogWrapper({});
    expect(wrapper.find(AkSpotlight).length).to.equal(1);
  });

  it('should not render the Spotlight if Apollo is still loading', () => {
    const wrapper = onboardingDialogWrapper({ isLoading: true });
    expect(wrapper.find(AkSpotlight).length).to.equal(0);
  });

  it('should not render the Spotlight if it has been dismissed', () => {
    const wrapper = onboardingDialogWrapper({ isDismissed: true });
    expect(wrapper.find(AkSpotlight).length).to.equal(0);
  });

  it('should not render the Spotlight if Apollo has an error', () => {
    const wrapper = onboardingDialogWrapper({ hasError: true });
    expect(wrapper.find(AkSpotlight).length).to.equal(0);
  });

  it('should call mutate once user clicks button on Spotlight', () => {
    const mutateSpy = spy();
    const wrapper = onboardingDialogWrapper({ mutate: mutateSpy });
    expect(mutateSpy.callCount).to.equal(0);
    const actions = (wrapper.find(AkSpotlight).props() as any).actions[0];
    actions.onClick();
    expect(mutateSpy.called).to.equal(true);
    expect(mutateSpy.callCount).to.equal(1);
  });

  it('should say correct text for button', () => {
    const wrapper = onboardingDialogWrapper({});
    const actions = (wrapper.find(AkSpotlight).props() as any).actions[0];
    expect(actions.text).to.equal('OK');
  });

});
