import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';

import { Spotlight as AkSpotlight } from '@atlaskit/onboarding';

import { analyticsClient } from 'common/analytics';

import dismissOnboardingMutation from './dismiss-onboarding.mutation.graphql';
import onboardingQuery from './onboarding.query.graphql';

import { DismissOnboardingMutation, OnboardingQuery, OnboardingQueryVariables } from '../../schema/schema-types';

interface SpotlightAction {
  text: string;
  onClick(): void;
}

interface Props {
  name: string;
  actionTitle: string;
  target?: string;
  pulse?: boolean;
  dialogPlacement?: string;
  targetBgColor?: string;
  targetRadius?: number;
  targetNode?: React.ReactNode;
}

interface DerivedProps {
  onboarding?: OnboardingQuery['onboarding'];
  loading: boolean;
  hasError: boolean;
}

export class OnboardingDialogImpl extends React.Component<ChildProps<Props & OnboardingQuery & DerivedProps, DismissOnboardingMutation>> {
  public render() {
    const { onboarding, loading, hasError, actionTitle } = this.props;

    if (loading || !onboarding || hasError) {
      return null;
    }

    if (onboarding.dismissed) {
      return null;
    }

    return (
      <AkSpotlight
        {...this.props}
        actions={[{
          onClick: this.finishOnboarding,
          text: actionTitle,
        } as SpotlightAction]}
      >
        {this.props.children}
      </AkSpotlight>
    );
  }

  private finishOnboarding = async () => {
    if (!this.props.mutate) {
      return;
    }

    await this.props.mutate({
      variables: {
        id: this.props.name,
      },
      optimisticResponse: {
        dismissOnboarding: {
          id: this.props.name,
          dismissed: true,
          __typename: 'Onboarding',
        },
      },
    }).catch((error) => {
      analyticsClient.onError(error);
    });
  }
}

const withDismissData = graphql<Props, OnboardingQuery, OnboardingQueryVariables, DerivedProps>(onboardingQuery, {
  options: (props: Props) => ({
    variables: {
      id: props.name,
    },
  }),
  props: ({ data }): DerivedProps => ({
    onboarding: data && data.onboarding,
    loading: !!(data && data.loading),
    hasError: !!(data && data.error),
  }),
});

const withDismissMutation = graphql<Props & DerivedProps, DismissOnboardingMutation>(dismissOnboardingMutation);

export const OnboardingDialog = withDismissData(withDismissMutation(OnboardingDialogImpl));
