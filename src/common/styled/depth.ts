import { css, InterpolationValue } from 'styled-components';

import {
  colors as akColors,
} from '@atlaskit/theme';

export type DepthValue = 'z100' | 'z200' | 'z300' | 'z400' | 'z500';

function zIndex(value: DepthValue): number {
  switch (value) {
    case 'z100': return 100;
    case 'z200': return 200;
    case 'z300': return 300;
    case 'z400': return 400;
    case 'z500': return 500;
    default: throw new Error(`Unknown depth value (${JSON.stringify(value)}`);
  }
}

function shadow(value: DepthValue): string {
  // This values are explicitly **not** tied to the gridSize.
  // Values are based on a CodePen referenced here: https://extranet.atlassian.com/display/ADG/Depth+product+spec+1.0+spec
  switch (value) {
    case 'z100': return '0 1px 1px';
    case 'z200': return '0 4px 8px -2px';
    case 'z300': return '0 8px 16px -4px';
    case 'z400': return '0 12px 24px -6px';
    case 'z500': return '0 20px 32px -8px';
    default: throw new Error(`Unknown depth value (${JSON.stringify(value)}`);
  }
}

// see https://extranet.atlassian.com/display/ADG/Depth+product+spec+1.0+spec
function depth(value: DepthValue): InterpolationValue[] {
  return css`
    z-index: ${zIndex(value)};
    box-shadow: ${shadow(value)} ${akColors.N50A}, 0 0 1px ${akColors.N60A};
  `;
}

export const depthAnimated = css`
  transition: box-shadow 200ms;
`;
export const depthZ100 = depth('z100');
export const depthZ200 = depth('z200');
export const depthZ300 = depth('z300');
export const depthZ400 = depth('z400');
export const depthZ500 = depth('z500');
