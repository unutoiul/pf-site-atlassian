import * as React from 'react';
import styled from 'styled-components';

import AkBase, { Label as AkLabel } from '@atlaskit/field-base';
import AkTag from '@atlaskit/tag';
import AkTagGroup from '@atlaskit/tag-group';
import { akColorN300, akGridSizeUnitless } from '@atlaskit/util-shared-styles';

const Text = styled.input`
  background: transparent;
  border: 0;
  box-sizing: border-box;
  color: inherit;
  cursor: inherit;
  font-size: 14px;
  outline: none;
  width: 100%;

  &::-ms-clear {
    display: none;
  }

  &:invalid {
    box-shadow: none;
  }
`;

const FieldDescription = styled.div`
  color: ${akColorN300};
  font-size: 11px;
  margin: ${akGridSizeUnitless / 2}px 0 ${akGridSizeUnitless}px;
`;

const CustomTagGroup = styled.div`
  margin-left: -${akGridSizeUnitless / 2}px;
`;

export interface MultiTaggerProps {
  compact?: boolean;
  description?: React.ReactNode;
  disabled?: boolean;
  isReadOnly?: boolean;
  required?: boolean;
  isInvalid?: boolean;
  isLabelHidden?: boolean;
  invalidMessage?: React.ReactNode;
  shouldFitContainer?: boolean;
  autoFocus?: boolean;
  label?: string;
  name?: string;
  placeholder?: string;
  values: string[];
  id?: string;
  removeText: string;
  splitOn?: RegExp;
  onAdd(value: string[]): void;
  onRemove(value: string): void;
}

export class MultiTagger extends React.Component<MultiTaggerProps> {
  public static defaultProps: Partial<MultiTaggerProps> = {
    compact: false,
    disabled: false,
    isReadOnly: false,
    required: false,
    isInvalid: false,
    values: [],
    splitOn: /[\s,]/,
  };

  private input: HTMLInputElement | undefined;

  public onKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key !== 'Enter') {
      return;
    }
    e.preventDefault();
    this.triggerAdd(this.getValues());
    if (this.input) {
      this.input.value = '';
    }
  }

  public onChange = () => {
    const values = this.getValues();
    if (values.length < 2) {
      return;
    }
    this.triggerAdd(values);
    if (this.input) {
      this.input.value = '';
    }
  }

  public onBlur = () => {
    this.triggerAdd(this.getValues());
    if (this.input) {
      this.input.value = '';
    }
  }

  public focus() {
    if (this.input) {
      this.input.focus();
    }
  }

  public render() {
    return (
      <div>
        <AkLabel
          label={this.props.label}
          htmlFor={this.props.id}
          isLabelHidden={this.props.isLabelHidden}
          isRequired={this.props.required}
        />
        <AkBase
          isCompact={this.props.compact}
          isDisabled={this.props.disabled}
          isInvalid={this.props.isInvalid}
          isReadOnly={this.props.isReadOnly}
          isRequired={this.props.required}
          invalidMessage={this.props.invalidMessage}
          isFitContainerWidthEnabled={this.props.shouldFitContainer}
        >
          <Text
            type="text"
            disabled={this.props.disabled}
            readOnly={this.props.isReadOnly}
            name={this.props.name}
            placeholder={this.props.placeholder}
            required={this.props.required}
            onKeyDown={this.onKeyDown}
            onChange={this.onChange}
            onBlur={this.onBlur}
            id={this.props.id}
            autoFocus={this.props.autoFocus}
            // tslint:disable-next-line:react-this-binding-issue
            innerRef={i => this.input = i}
          />
        </AkBase>
        {this.props.description && (
          <FieldDescription>{this.props.description}</FieldDescription>
        )}
        <CustomTagGroup>
          <AkTagGroup>
            {this.props.values.map((value) => (
              <AkTag
                key={value}
                text={value}
                removeButtonText={!this.props.disabled && !this.props.isReadOnly && this.props.removeText}
                onAfterRemoveAction={this.props.onRemove}
              />
            ))}
          </AkTagGroup>
        </CustomTagGroup>
      </div>
    );
  }

  private getValues(): string[] {
    if (!this.input || !this.props.splitOn) {
      return [];
    }

    return this.input.value.split(this.props.splitOn);
  }

  private triggerAdd(values: string[]) {
    const nonEmptyValues = values.map(_ => _.trim()).filter(_ => _ !== '');
    if (!nonEmptyValues.length) {
      return;
    }
    this.props.onAdd(nonEmptyValues);
  }
}
