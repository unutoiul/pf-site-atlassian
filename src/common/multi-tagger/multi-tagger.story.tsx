// tslint:disable:jsx-use-translation-function
import { storiesOf } from '@storybook/react';
import * as React from 'react';

import { MultiTagger as MultiTaggerStateless, MultiTaggerProps } from './multi-tagger';

interface MultiTaggerState {
  values: string[];
}

class MultiTagger extends React.Component<Partial<MultiTaggerProps>, MultiTaggerState> {
  constructor(props) {
    super(props);
    this.state = {
      values: props.values || [],
    };
  }

  public render() {
    return (
      <MultiTaggerStateless
        {...this.props}
        values={this.state.values}
        onAdd={this.onAdd}
        onRemove={this.onRemove}
        removeText="remove me"
      />
    );
  }

  private onAdd = (text: string[]) => {
    const newValues = text.filter(t => !this.state.values.includes(t));
    if (!newValues.length) {
      return;
    }
    this.setState({
      values: [...this.state.values, ...newValues],
    });
  }

  private onRemove = (text: string) => {
    this.setState({
      values: this.state.values.filter(t => t !== text),
    });
  }
}

storiesOf('Common|Multi Tagger', module)
  .add('Default', () => (
    <MultiTagger
      autoFocus={true}
    />
  ))
  .add('With values', () => (
    <MultiTagger
      autoFocus={true}
      values={['one', 'two', 'three']}
    />
  ))
  .add('With values in disabled state', () => (
    <MultiTagger
      autoFocus={true}
      values={['one', 'two', 'three']}
      disabled={true}
    />
  ))
  .add('With values in read-only state', () => (
    <MultiTagger
      autoFocus={true}
      values={['one', 'two', 'three']}
      isReadOnly={true}
    />
  ))
  .add('With a lot of values in restricted width container', () => (
    <div style={{ width: '300px' }}>
      <MultiTagger
        autoFocus={true}
        values={['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten']}
        shouldFitContainer={true}
      />
    </div>
  ))
  .add('With values, a label and a description in restricted width container', () => (
    <div style={{ width: '400px' }}>
      <MultiTagger
        label="Entries"
        autoFocus={true}
        description="Separate multiple email entries with a comma/space."
        values={['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten']}
        shouldFitContainer={true}
      />
    </div>
  ));
