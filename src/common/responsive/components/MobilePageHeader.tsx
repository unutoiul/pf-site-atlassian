import * as React from 'react';
import * as isEqual from 'react-fast-compare';

import { PageContext } from './ResponsivePage';

export class MobilePageHeader extends React.PureComponent<{
  actions?: React.ReactNode,
  children?: React.ReactNode,
  bottomBar?: React.ReactNode,
  breadcrumbs?: React.ReactNode,
}> {
  public static contextType = PageContext;

  public componentDidMount() {
    this.update();
  }

  public componentDidUpdate(oldProps) {
    // we are comparing props with React Elements inside
    // thus we have to use special library, not to fall into the endless recursion
    if (!isEqual(this.props, oldProps)) {
      this.update();
    }
  }

  public componentWillUnmount() {
    this.context.setHeader({
      header: null,
      breadcrumbs: null,
      actions: null,
      bottomBar: null,
    });
  }

  public update() {
    const { children: header, breadcrumbs, actions, bottomBar } = this.props;
    this.context.setHeader({
      header,
      breadcrumbs,
      actions,
      bottomBar,
    });
  }

  public render() {
    return null;
  }
}
