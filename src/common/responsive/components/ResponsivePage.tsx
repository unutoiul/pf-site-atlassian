import memoizeOne from 'memoize-one';
import * as React from 'react';
import FocusLock from 'react-focus-lock';
import { ScrollLocky } from 'react-scroll-locky';
import styled from 'styled-components';

import AkMobileHeader from '@atlaskit/mobile-header';
import AkPage from '@atlaskit/page';

import { ResizeReactive } from 'common/responsive/ResizeReactive';

import { Media } from 'common/responsive/Matcher';

export interface HeaderContent {
  header?: React.ReactNode;
  breadcrumps?: React.ReactNode;
  bottomBar?: React.ReactNode;
  actions?: React.ReactNode;
}

interface State {
  isOpen: boolean;
  header: HeaderContent;
}

interface PageContextType {
  setHeader(header: HeaderContent): void;
}

export const PageContext = React.createContext<PageContextType>({
  setHeader() {
    // empty default
    return;
  },
});

const Block = styled.div`
   margin: 20px;
`;

const SizeLimiterBase: React.SFC<{ height: string, className?: string }> = ({ className, children }) => (
  <div className={className}>{children}</div>
);

// This patch needed to adopt menu size for a "moment" mobile window
const SizeLimiter = styled(SizeLimiterBase)`
  & div {
   max-height: ${(props) => props.height};
  }
`;

export class ResponsivePage extends React.Component<{
  navigation: React.ReactElement<any>;
  drawers: React.ReactElement<any>;
}, State> {
  public state: State = {
    isOpen: false,
    header: {},
  };

  private getContextValue = memoizeOne((setHeader: any) => ({ setHeader }));

  public render() {
    const { isOpen, header } = this.state;
    const { children = '...', navigation, drawers, ...rest } = this.props;
    const drawerlessNavigation = React.cloneElement(navigation, { withoutDrawers: true });

    // use "inlined media matches" to preserve tree structure, and keep components on breakpoint change.
    return (
      <AkPage
        navigation={(
          <React.Fragment>
            <Media.Above mobile={true}>
              {drawerlessNavigation}
            </Media.Above>
            {drawers}
          </React.Fragment>
        )}
        {...rest}
      >
        <Media.Below including={true} mobile={true}>
          <AkMobileHeader
            drawerState={isOpen && 'navigation'}
            menuIconLabel={'Menu'}
            navigation={this.showNavigation}
            secondaryContent={header.actions}
            pageHeading={header.header}
            onNavigationOpen={this.openNavigation}
            onDrawerClose={this.closeDrawer}
          />
        </Media.Below>
        <Block>
          <Media.Below including={true} mobile={true}>
            {header.breadcrumps}
            {header.bottomBar}
          </Media.Below>
          <PageContext.Provider value={this.getContextValue(this.setHeader)}>
            {children}
          </PageContext.Provider>
        </Block>
      </AkPage>
    );
  }

  private setHeader = (header: HeaderContent) => {
    this.setState({ header, isOpen: false });
  };

  private openNavigation = () => this.setState({ isOpen: true });
  private closeDrawer = () => this.setState({ isOpen: false });
  private showNavigation = (isOpen: boolean) => (
    <ResizeReactive>
      {({ height }) => (
        <SizeLimiter height={height}>
          <ScrollLocky enabled={isOpen} onEscape={this.closeDrawer}>
            <FocusLock disabled={!isOpen}>
              {React.cloneElement(this.props.navigation, { withoutDrawers: true })}
            </FocusLock>
          </ScrollLocky>
        </SizeLimiter>
      )}
    </ResizeReactive>
  );
}
