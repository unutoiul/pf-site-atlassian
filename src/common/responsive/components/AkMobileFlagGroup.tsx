import cx from 'classnames';
import * as React from 'react';
import { CSSTransition, TransitionGroup } from 'react-transition-group';

import styles from './mobile-flag.less';

interface FlagProps {
  id: string;
  isDismissAllowed?: boolean;
  appearance?: string;

  onDismissed?(flagId: string): void;
}

export type NotificationFlag = React.ReactElement<FlagProps>;

interface Props {
  children: null | NotificationFlag[];

  onDismissed(flagId: string): void;
}

export class AkMobileFlagGroup extends React.Component<Props, {}> {

  public render() {
    return (
      <div className={styles.group}>
        <TransitionGroup>
          {this.renderChildren()}
        </TransitionGroup>
      </div>
    );
  }

  private renderChildren = () => {
    const { children, onDismissed } = this.props;

    return React.Children.map(children, (flag: NotificationFlag) => {
      const isDismissAllowed = true;
      const { id, appearance } = flag.props;

      return (
        <CSSTransition
          key={id}
          timeout={300}
          classNames={{
            enterActive: styles.wrapperVisible,
            enterDone: styles.wrapperVisible,
            exitActive: styles.wrapperExiting,
          }}
        >
          <div className={cx(styles.wrapper, appearance && styles[appearance])}>
            {React.cloneElement(flag, { onDismissed, isDismissAllowed, appearance: 'normal' })}
          </div>
        </CSSTransition>
      );
    });
  };
}
