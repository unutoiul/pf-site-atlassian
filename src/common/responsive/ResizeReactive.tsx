import * as React from 'react';

export const width100vh = (): string => window ? `${window.innerWidth}px` : '100vw';
export const height100vh = (): string => window ? `${window.innerHeight}px` : '100vh';

interface Props {
  children(props: { width: string, height: string }): React.ReactNode;
}

export class ResizeReactive extends React.Component<Props> {
  public componentDidMount() {
    if (typeof window !== 'undefined') {
      window.addEventListener('resize', this.onResize);
    }
  }

  public componentWillUnmount() {
    if (typeof window !== 'undefined') {
      window.removeEventListener('resize', this.onResize);
    }
  }

  public onResize = () => this.forceUpdate();

  public render() {
    return this.props.children({ width: width100vh(), height: height100vh() });
  }
}
