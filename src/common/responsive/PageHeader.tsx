import * as React from 'react';

import AkPageHeader from '@atlaskit/page-header';

import { MobilePageHeader } from './components/MobilePageHeader';
import { Media } from './Matcher';

export const PageHeader: React.SFC<{
  breadcrumbs?: React.ReactNode,
  actions?: React.ReactNode,
  children?: React.ReactNode,
  bottomBar?: React.ReactNode,
  disableTitleStyles?: boolean,
}> = (props) => (
  <Media.Matcher
    mobile={<MobilePageHeader {...props} />}
    tablet={<AkPageHeader {...props} />}
  />
);
