// tablet breakpoint set to 850, as long as 850 is a minimal width for AkPage to look good.
export const tablet = 850;
export const desktop = 1023;

// THIS FILE SHOULD BE IN SYNC WITH LESS FILE
