import * as React from 'react';
import { createMediaMatcher } from 'react-media-match';

import { UserFeatureFlag } from 'common/feature-flags';

import { desktop, tablet } from './breakpoints';

export const Media = createMediaMatcher({
  mobile: `(max-width: ${tablet}px)`,
  tablet: `(max-width: ${desktop - 1}px)`,
  desktop: `(min-width: ${desktop}px)`,
});

export const MediaSettings = React.createContext(false);

// BUX nightwatch tests have to be able to disable responsibility
// This could be covered by feature flag due to run order - some endpoints could be already used, and mocks got broken
const responsiveAllowed = typeof window !== 'undefined' && (window.location.href.indexOf('disableResponsive') < 0);

export const MediaProvider: React.SFC = ({ children }) => (
  <UserFeatureFlag flagKey="admin.responsive.nav" defaultValue={true}>
    {flag =>
      <MediaSettings.Provider value={flag.value && responsiveAllowed}>
        {
          flag.value && responsiveAllowed
            ? <Media.Provider>{children}</Media.Provider>
            : children
        }
      </MediaSettings.Provider>
    }
  </UserFeatureFlag>
);
