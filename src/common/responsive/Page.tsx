// tslint:disable:react-no-dangerous-html
import * as React from 'react';
import { Helmet } from 'react-helmet';

import AkPage from '@atlaskit/page';

import { ResponsivePage } from './components/ResponsivePage';
import { MediaSettings } from './Matcher';

interface OwnProps {
  navigation: React.ReactElement<any>;
  drawers: React.ReactElement<any>;
}

export const Page: React.SFC<OwnProps> = ({ ...props }) => (
  <MediaSettings.Consumer>
    {responsive => (
      responsive
        ? (
          <React.Fragment>
            <Helmet>
              {/* Could be remove after Responsive FF deprecation */}
              <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
            </Helmet>
            <style
              dangerouslySetInnerHTML={{
                __html: `@media (max-width: 767px) {#root {overflow: scroll;min-height: 100%; -webkit-overflow-scrolling: touch;}}`,
              }}
            />
            {/* Could be moved to global styles after Responsive FF deprecation */}
            <ResponsivePage {...props} />
          </React.Fragment>
        ) : (
          <AkPage {...props} />
        )
    )}
  </MediaSettings.Consumer>
);
