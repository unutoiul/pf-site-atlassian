import * as React from 'react';
import { Link, LinkProps } from 'react-router-dom';
import styled from 'styled-components';

import AkArrowLeftIcon from '@atlaskit/icon/glyph/arrow-left';
import {
  colors as akColors,
  gridSize as akGridSize,
} from '@atlaskit/theme';

import { Referrer } from 'common/analytics';

export const PageParentLinkWrapper = styled(Link)`
  padding: ${akGridSize() * 2}px 0 ${akGridSize() * 2}px 0;
  display: flex;
  align-items: center;
  color: ${akColors.subtleHeading};
  :link, :visited, :hover, :active  {
    color: ${akColors.subtleHeading};
    outline: none;
    text-decoration: none;
  }
`;

export const LinkText = styled.span`
  padding-left: ${akGridSize() * 2}px;
`;

interface OwnProps {
  linkText: string;
  linkLocation: string;
  onClick?: LinkProps['onClick'];
}

export class PageParentLink extends React.Component<OwnProps> {
  public render() {
    return (
      <Referrer value="breadcrumb">
        <PageParentLinkWrapper to={this.props.linkLocation} onClick={this.props.onClick}>
          <AkArrowLeftIcon label={this.props.linkText} />
          <LinkText>{this.props.linkText}</LinkText>
        </PageParentLinkWrapper>
      </Referrer>
    );
  }
}
