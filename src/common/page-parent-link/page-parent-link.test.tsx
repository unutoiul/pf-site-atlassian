import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import AkArrowLeftIcon from '@atlaskit/icon/glyph/arrow-left';

import { LinkText, PageParentLink, PageParentLinkWrapper } from 'common/page-parent-link/page-parent-link';

describe('Page Parent', () => {
  it('Should render a page parent component', () => {
    const wrapper = shallow(
      <PageParentLink
        linkLocation="/atlassian"
        linkText="Fluffy Golden Retrievers"
      />,
    );
    expect(wrapper.find(PageParentLinkWrapper).props().to).to.equal('/atlassian');
    expect(wrapper.find(AkArrowLeftIcon).exists()).to.equal(true);
    expect(wrapper.find(LinkText).children().text()).to.equal('Fluffy Golden Retrievers');
  });

  it('Should wire up the provided handlers', () => {
    const clickHandler = sinon.stub();
    const wrapper = shallow(
      <PageParentLink
        linkLocation="/atlassian"
        linkText="Fluffy Golden Retrievers"
        onClick={clickHandler}
      />,
    );
    wrapper.find(PageParentLinkWrapper).simulate('click');
    expect(clickHandler.called).to.be.true();
  });
});
