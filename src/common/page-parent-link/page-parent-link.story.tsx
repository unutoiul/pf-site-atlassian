import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { StaticRouter } from 'react-router';

import { PageParentLink } from 'common/page-parent-link/page-parent-link';

storiesOf('Common|Page Parent', module)
  .add('With a page parent', () => (
    <StaticRouter context={{}}>
      <PageParentLink
        linkLocation="/atlassian"
        linkText="Fluffy Golden Retrievers"
      />
    </StaticRouter>
  ),
);
