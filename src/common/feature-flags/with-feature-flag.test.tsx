// tslint:disable jsx-use-translation-function
import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';

import { configureApolloClientWithUserFeatureFlag } from 'common/feature-flags/test-utils';

import { createApolloClient } from '../../apollo-client';

import { UserFeatureFlag } from './feature-flag';
import { FeatureFlagAddedProp, withUserFeatureFlag } from './with-feature-flag';

describe('withUserFeatureFlag', () => {

  const TestComponent = (props: FeatureFlagAddedProp) => <div>{JSON.stringify(props)}</div>;
  const TestWrappedComponent = withUserFeatureFlag<{}>({
    flagKey: 'test',
    defaultValue: false,
    name: 'featureFlag',
  })(TestComponent);
  const mountWrappedComponentWithApolloFeatureFlags = ({
    flagKey = 'test',
  } = {}) => {
    const client = configureApolloClientWithUserFeatureFlag({
      client: createApolloClient(),
      flagKey,
      flagValue: true,
    });

    return mount(
      <ApolloProvider client={client}>
        <TestWrappedComponent />
      </ApolloProvider>,
    );
  };

  it('should pass flagKey and defaultValue to FeatureFlag component', () => {
    const wrapper = mountWrappedComponentWithApolloFeatureFlags({
      flagKey: 'test.key',
    });

    const featureFlag = wrapper.find(UserFeatureFlag);

    expect(featureFlag.props().defaultValue).to.equal(false);
    expect(featureFlag.props().flagKey).to.equal('test');
  });

  it('passes featureFlag as prop to WrappedComponent', () => {
    const wrapper = mountWrappedComponentWithApolloFeatureFlags();

    const testComponent = wrapper.find(TestComponent);

    expect(testComponent.props()).to.deep.equal({
      featureFlag: {
        defaultValue: false,
        value: true,
        isLoading: false,
      },
    });
  });
});
