import { ApolloClient } from 'apollo-client';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';

import { createApolloClient } from '../../apollo-client';
import orgFeatureFlagQuery from './org-feature-flag.query.graphql';
import siteFeatureFlagQuery from './site-feature-flag.query.graphql';
import userFeatureFlagQuery from './user-feature-flag.query.graphql';

export const configureApolloClientWithUserFeatureFlag = ({ client, flagKey, flagValue, userId = 'test-aid' }: {
  client: ApolloClient<any>,
  flagKey: string,
  flagValue: boolean,
  userId?: string,
}) => {
  client.writeQuery({
    query: userFeatureFlagQuery,
    variables: {
      flagKey,
    },
    data: {
      currentUser: {
        __typename: 'CurrentUser',
        id: userId,
        flag: {
          __typename: 'UserFlag',
          id: flagKey,
          value: flagValue,
        },
      },
    },
  });

  return client;
};

export const configureApolloClientWithSiteFeatureFlag = ({ client, flagKey, flagValue, siteId = 'test-id' }: {
  client: ApolloClient<any>,
  flagKey: string,
  flagValue: boolean,
  siteId?: string,
}) => {
  client.writeQuery({
    query: siteFeatureFlagQuery,
    variables: {
      flagKey,
    },
    data: {
      currentSite: {
        __typename: 'CurrentSite',
        id: siteId,
        flag: {
          __typename: 'FeatureFlag',
          id: flagKey,
          value: flagValue,
        },
      },
    },
  });

  return client;
};

export const configureApolloClientWithOrgFeatureFlag = ({ client, flagKey, flagValue, orgId }: {
  client: ApolloClient<any>,
  flagKey: string,
  flagValue: boolean,
  orgId: string,
}) => {
  client.writeQuery({
    query: orgFeatureFlagQuery,
    variables: {
      flagKey,
      orgId,
    },
    data: {
      organization: {
        __typename: 'Organization',
        id: orgId,
        flag: {
          __typename: 'FeatureFlag',
          id: flagKey,
          value: flagValue,
        },
      },
    },
  });

  return client;
};

interface MockFeatureFlagApolloClientArgs {
  children: React.ReactChild;
  flagKey: string;
  flagValue?: boolean;
}

export const MockUserFeatureFlagApolloClient = ({ children, flagKey, flagValue = false }: MockFeatureFlagApolloClientArgs) => {
  const client = createApolloClient();

  return (
    <ApolloProvider
      client={
        configureApolloClientWithUserFeatureFlag({
          client,
          flagKey,
          flagValue,
        })}
    >
      {children}
    </ApolloProvider>
  );
};

export const MockSiteFeatureFlagApolloClient = ({ children, flagKey, flagValue = false }: MockFeatureFlagApolloClientArgs) => {
  const client = createApolloClient();

  return (
    <ApolloProvider
      client={
        configureApolloClientWithSiteFeatureFlag({
          client,
          flagKey,
          flagValue,
        })}
    >
      {children}
    </ApolloProvider>
  );
};

interface MockOrgFeatureFlagApolloClientArgs {
  orgId: string;
}

export const MockOrgFeatureFlagApolloClient = ({ children, flagKey, flagValue = false, orgId }: MockFeatureFlagApolloClientArgs & MockOrgFeatureFlagApolloClientArgs) => {
  const client = createApolloClient();

  return (
    <ApolloProvider
      client={
        configureApolloClientWithOrgFeatureFlag({
          client,
          flagKey,
          flagValue,
          orgId,
        })}
    >
      {children}
    </ApolloProvider>
  );
};

interface ComposeFeatureFlagApolloClientArgs {
  children: React.ReactChild;
  flags: Array<{
    flagKey: string;
    flagValue?: boolean;
  }>;
}
export const ComposeUserFeatureFlagApolloClient = ({ children, flags }: ComposeFeatureFlagApolloClientArgs) => {
  let client = createApolloClient();
  flags.forEach(flag => {
    client = configureApolloClientWithUserFeatureFlag({
      client,
      flagKey: flag.flagKey,
      flagValue: flag.flagValue || false,
    });
  });

  return (
    <ApolloProvider client={client}>
      {children}
    </ApolloProvider>
  );
};
export const ComposeSiteFeatureFlagApolloClient = ({ children, flags }: ComposeFeatureFlagApolloClientArgs) => {
  let client = createApolloClient();
  flags.forEach(flag => {
    client = configureApolloClientWithSiteFeatureFlag({
      client,
      flagKey: flag.flagKey,
      flagValue: flag.flagValue || false,
    });
  });

  return (
    <ApolloProvider client={client}>
      {children}
    </ApolloProvider>
  );
};
export const ComposeOrgFeatureFlagApolloClient = ({ children, flags, orgId }: ComposeFeatureFlagApolloClientArgs & MockOrgFeatureFlagApolloClientArgs) => {
  let client = createApolloClient();
  flags.forEach(flag => {
    client = configureApolloClientWithOrgFeatureFlag({
      client,
      flagKey: flag.flagKey,
      flagValue: flag.flagValue || false,
      orgId,
    });
  });

  return (
    <ApolloProvider client={client}>
      {children}
    </ApolloProvider>
  );
};
