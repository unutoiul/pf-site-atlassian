import * as React from 'react';

import {
  FeatureFlagProps,
  OrgFeatureFlag,
  SiteFeatureFlag,
  UserFeatureFlag,
} from './feature-flag';

interface WithFeatureFlagOpts<TProps, K extends keyof TProps = keyof TProps> {
  flagKey: string;
  defaultValue: boolean;
  name: K;
}

export interface FeatureFlagAddedProp {
  featureFlag: FeatureFlagProps;
}

export function withFeatureFlag<TOwnProps, TRemappedProps = FeatureFlagAddedProp>(FeatureFlagComponent: React.ComponentClass<any>) {
  return (opts: WithFeatureFlagOpts<TRemappedProps>) => {
    const { name = 'featureFlag', flagKey, defaultValue } = opts;

    const mapFlagToProps = (flag: FeatureFlagProps): TRemappedProps => ({ [name]: flag } as any);

    return (WrappedComponent: React.ComponentType<TOwnProps & TRemappedProps>): React.ComponentClass<TOwnProps> => {
      return class extends React.Component<TOwnProps, {}> {
        public static WrappedComponent = WrappedComponent;

        public render() {
          return (
            <FeatureFlagComponent
              flagKey={flagKey}
              defaultValue={defaultValue}
            >
              {(flag: FeatureFlagProps) => {
                return (
                  <WrappedComponent
                    {...this.props}
                    {...mapFlagToProps(flag)}
                  />
                );
              }}
            </FeatureFlagComponent>
          );
        }
      };
    };
  };
}

export function withUserFeatureFlag<TOwnProps, TRemappedProps = FeatureFlagAddedProp>(opts: WithFeatureFlagOpts<TRemappedProps>) {
  return withFeatureFlag<TOwnProps, TRemappedProps>(UserFeatureFlag)(opts);
}

export function withSiteFeatureFlag<TOwnProps, TRemappedProps = FeatureFlagAddedProp>(opts: WithFeatureFlagOpts<TRemappedProps>) {
  return withFeatureFlag<TOwnProps, TRemappedProps>(SiteFeatureFlag)(opts);
}

export function withOrgFeatureFlag<TOwnProps, TRemappedProps = FeatureFlagAddedProp>(opts: WithFeatureFlagOpts<TRemappedProps>) {
  return withFeatureFlag<TOwnProps, TRemappedProps>(OrgFeatureFlag)(opts);
}
