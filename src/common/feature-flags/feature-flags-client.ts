import { initialize, LDClient, LDFlagSet } from 'ldclient-js';

import { getConfig } from 'common/config';

import { PromiseQueue } from '../../utilities/promise-queue';

const clientSideId = getConfig().featureFlagClient;

export class FeatureFlagsClient {
  private static instance: FeatureFlagsClient;
  private ldClient: LDClient | undefined;
  private queue: PromiseQueue = new PromiseQueue();
  private currentlyIdentifiedUser: string | undefined;

  public static getInstance(): FeatureFlagsClient {
    if (!FeatureFlagsClient.instance) {
      FeatureFlagsClient.instance = new FeatureFlagsClient();
    }

    return FeatureFlagsClient.instance;
  }

  public async allFlags(userKey: string): Promise<LDFlagSet> {
    return this.queue.push(async () => {
      await this.identifyUser(userKey);

      return this.ldClient!.allFlags();
    });
  }

  private async identifyUser(userKey: string): Promise<void> {
    if (this.currentlyIdentifiedUser === userKey) {
      return;
    }

    await this.initLdClient(userKey);

    return new Promise<void>(resolve => {
      this.ldClient!.identify({
        key: userKey,
        anonymous: true,
      }, undefined, () => {
        this.currentlyIdentifiedUser = userKey;
        resolve();
      });
    });
  }

  private async initLdClient(userKey: string): Promise<void> {
    // We only want one instance of the LDClient per instance
    if (this.ldClient) {
      return;
    }

    return new Promise<void>(resolve => {
      this.ldClient = initialize(clientSideId, {
        key: userKey,
        anonymous: true,
      });

      this.ldClient.on('ready', () => {
        this.currentlyIdentifiedUser = userKey;
        resolve();
      });
    });
  }
}
