import * as React from 'react';
import { graphql, OptionProps } from 'react-apollo';
import { RouteComponentProps, withRouter } from 'react-router-dom';

import {
  OrgFeatureFlagsQuery,
  OrgFeatureFlagsQueryVariables,
  SiteFeatureFlagsQuery,
  SiteFeatureFlagsQueryVariables,
  UserFeatureFlagsQuery,
  UserFeatureFlagsQueryVariables,
} from '../../schema/schema-types';

import orgFeatureFlagQuery from './org-feature-flag.query.graphql';
import siteFeatureFlagQuery from './site-feature-flag.query.graphql';
import userFeatureFlagQuery from './user-feature-flag.query.graphql';

interface OwnProps {
  flagKey: string;
  orgId?: string;
  defaultValue: boolean;
}

interface DerivedProps {
  isLoading: boolean;
  hasError: boolean;
  value: boolean;
}

export interface FeatureFlagProps {
  value: boolean;
  isLoading: boolean;
}

export class FeatureFlagImpl extends React.Component<OwnProps & DerivedProps & RouteComponentProps<any>> {
  public render() {
    const { hasError, isLoading, defaultValue = false, value } = this.props;
    const children = this.props.children as any;

    if (hasError) {
      return children({ isLoading, value: defaultValue, defaultValue });
    }

    return children({ isLoading, value, defaultValue });
  }
}

const withUserFlagData = graphql<OwnProps, UserFeatureFlagsQuery, UserFeatureFlagsQueryVariables, DerivedProps>(userFeatureFlagQuery, {
  props: (componentProps: OptionProps<OwnProps, UserFeatureFlagsQuery>): DerivedProps => ({
    isLoading: !!componentProps.data && componentProps.data.loading,
    hasError: !!(componentProps.data && componentProps.data.error),
    value: !!(componentProps.data && componentProps.data.currentUser && componentProps.data.currentUser.flag && componentProps.data.currentUser.flag.value),
  }),

  options: (props) => ({
    variables: {
      flagKey: props.flagKey,
    },
  }),
});

const withSiteFlagData = graphql<OwnProps, SiteFeatureFlagsQuery, SiteFeatureFlagsQueryVariables, DerivedProps>(siteFeatureFlagQuery, {
  props: (componentProps: OptionProps<OwnProps, SiteFeatureFlagsQuery>): DerivedProps => ({
    isLoading: !!componentProps.data && componentProps.data.loading,
    hasError: !!(componentProps.data && componentProps.data.error),
    value: !!(componentProps.data && componentProps.data.currentSite && componentProps.data.currentSite.flag && componentProps.data.currentSite.flag.value),
  }),

  options: (props) => ({
    variables: {
      flagKey: props.flagKey,
    },
  }),
});

const withOrgFlagData = graphql<OwnProps & RouteComponentProps<any>, OrgFeatureFlagsQuery, OrgFeatureFlagsQueryVariables, DerivedProps>(orgFeatureFlagQuery, {
  props: (componentProps): DerivedProps => ({
    isLoading: !!componentProps.data && componentProps.data.loading,
    hasError: !!(componentProps.data && componentProps.data.error),
    value: !!(componentProps.data && componentProps.data.organization && componentProps.data.organization.flag && componentProps.data.organization.flag.value),
  }),

  options: (props) => ({
    variables: {
      orgId: props.match.params.orgId,
      flagKey: props.flagKey,
    },
  }),

  skip: (props) => !props.match.params.orgId,
});

export const UserFeatureFlag = withUserFlagData(FeatureFlagImpl);
export const SiteFeatureFlag = withSiteFlagData(FeatureFlagImpl);
export const OrgFeatureFlag = withRouter(withOrgFlagData(FeatureFlagImpl));
