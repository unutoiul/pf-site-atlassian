import { expect } from 'chai';
import * as LdClient from 'ldclient-js';
import * as sinon from 'sinon';

import { FeatureFlagsClient } from './feature-flags-client';

describe('FeatureFlagsClient', () => {
  const sandbox = sinon.sandbox.create();
  const flags = {
    flag1: true,
    flag2: false,
    'some.other.flag': 'some-flags-can-have-random-values!',
  };

  afterEach(() => {
    sandbox.restore();
  });

  it('should return all the feature flags', async () => {
    const featureFlagsClient = new FeatureFlagsClient();

    sandbox.stub(LdClient, 'initialize').callsFake(() => ({
      on: (action, callback) => (action === 'ready' ? callback() : null),
      allFlags: () => flags,
      identify: (_, __, done) => done(),
    }));

    const value = await featureFlagsClient.allFlags('stub-user-key');

    expect(value).to.deep.equal(flags);
  });

  it('should only call identify once per user key', async () => {
    const featureFlagsClient = new FeatureFlagsClient();
    const identifySpy = sandbox.spy();

    sandbox.stub(LdClient, 'initialize').callsFake(() => ({
      on: (action, callback) => (action === 'ready' ? callback() : null),
      allFlags: () => [],
      identify: (_, __, done) => {
        identifySpy();
        done();
      },
    }));

    await Promise.all([
      featureFlagsClient.allFlags('stub-user-key'),
      featureFlagsClient.allFlags('stub-user-key'),
    ]);

    expect(identifySpy.callCount).to.equal(1);

    await Promise.all([
      featureFlagsClient.allFlags('stub-user-key-2'),
      featureFlagsClient.allFlags('stub-user-key-2'),
    ]);

    expect(identifySpy.callCount).to.equal(2);
  });
});
