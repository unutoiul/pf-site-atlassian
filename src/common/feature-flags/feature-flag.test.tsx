// tslint:disable jsx-use-translation-function
import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { MemoryRouter, Route } from 'react-router-dom';

import {
  configureApolloClientWithOrgFeatureFlag,
  configureApolloClientWithSiteFeatureFlag,
  configureApolloClientWithUserFeatureFlag,
} from './test-utils';

import {
  FeatureFlagImpl,
  FeatureFlagProps,
  OrgFeatureFlag,
  SiteFeatureFlag,
  UserFeatureFlag,
} from './feature-flag';

import { createApolloClient } from '../../apollo-client';

describe('Feature Flag', () => {
  const FeatureFlagWithData = (
    { hasError = false,
      isLoading = false,
      value = false,
      flagKey,
      defaultValue = false,
      children,
    },
  ) => (
      <FeatureFlagImpl
        flagKey={flagKey}
        value={value}
        defaultValue={defaultValue}
        isLoading={isLoading}
        hasError={hasError}
        match={null as any}
        location={null as any}
        history={null as any}
      >
        {children}
      </FeatureFlagImpl>
    );

  describe('FeatureFlagImpl', () => {
    it('should render children with FeatureFlag passed', () => {
      const wrapper = mount(
        <FeatureFlagWithData
          flagKey={'test.key'}
          value={true}
        >
          {(featureFlag: FeatureFlagProps) => (
            featureFlag.value
              ? <p>enabled</p>
              : <p>disabled</p>
          )}
        </FeatureFlagWithData>,
      );

      expect(wrapper.text()).to.include('enabled');
    });

    it('should render children with default value if hasError is true', () => {
      const wrapper = mount(
        <FeatureFlagWithData
          flagKey={'test.key'}
          value={true}
          defaultValue={false}
          hasError={true}
        >
          {({ flagValue }) => (
            flagValue
              ? <p>enabled</p>
              : <p>disabled</p>
          )}
        </FeatureFlagWithData >,
      );

      expect(wrapper.text()).to.include('disabled');
    });

    it('should render children with loading prop as true if loading is set to true', () => {
      const wrapper = mount(
        <FeatureFlagWithData
          flagKey={'test.key'}
          isLoading={true}
        >
          {({ isLoading }) => (
            isLoading
              ? <p>loading</p>
              : <p>data</p>
          )}
        </FeatureFlagWithData >,
      );

      expect(wrapper.text()).to.include('loading');
    });
  });

  describe('UserFeatureFlag', () => {
    const FeatureFlagWithApollo = ({ flagKey, children }) => (
      <ApolloProvider
        client={
          configureApolloClientWithUserFeatureFlag({
            client: createApolloClient(),
            flagKey,
            flagValue: true,
          })}
      >
        <UserFeatureFlag
          flagKey={flagKey}
          defaultValue={false}
        >
          {children}
        </UserFeatureFlag>
      </ApolloProvider>
    );

    it('should fetch and retrieve feature flag value from apollo', () => {
      const wrapper = mount(
        <FeatureFlagWithApollo
          flagKey="test.key"
        >
          {({ isLoading, value }) => {
            const isEnabled = value;

            if (isLoading) {
              return null;
            }

            if (isEnabled) {
              return <p>enabled</p>;
            }

            return <p>disabled</p>;
          }}
        </FeatureFlagWithApollo>,
      );

      expect(wrapper.text()).to.include('enabled');
    });
  });

  describe('SiteFeatureFlag', () => {
    const FeatureFlagWithApollo = ({ flagKey, children }) => (
      <ApolloProvider
        client={
          configureApolloClientWithSiteFeatureFlag({
            client: createApolloClient(),
            flagKey,
            flagValue: true,
          })}
      >
        <SiteFeatureFlag
          flagKey={flagKey}
          defaultValue={false}
        >
          {children}
        </SiteFeatureFlag>
      </ApolloProvider>
    );

    it('should fetch and retrieve feature flag value from apollo', () => {
      const wrapper = mount(
        <FeatureFlagWithApollo
          flagKey="test.key"
        >
          {({ isLoading, value }) => {
            const isEnabled = value;

            if (isLoading) {
              return null;
            }

            if (isEnabled) {
              return <p>enabled</p>;
            }

            return <p>disabled</p>;
          }}
        </FeatureFlagWithApollo>,
      );

      expect(wrapper.text()).to.include('enabled');
    });
  });

  describe('OrgFeatureFlag', () => {
    const orgId = 'test-org-id';

    const FeatureFlagWithApollo = ({ flagKey, children }) => (
      <MemoryRouter initialEntries={[{ pathname: `/o/${orgId}/overview` }]}>
        <Route path="/o/:orgId/overview">
          <ApolloProvider
            client={
              configureApolloClientWithOrgFeatureFlag({
                client: createApolloClient(),
                flagKey,
                flagValue: true,
                orgId,
              })}
          >
            <OrgFeatureFlag
              flagKey={flagKey}
              defaultValue={false}
            >
              {children}
            </OrgFeatureFlag>
          </ApolloProvider>
        </Route>
      </MemoryRouter>
    );

    it('should fetch and retrieve feature flag value from apollo', () => {
      const wrapper = mount(
        <FeatureFlagWithApollo
          flagKey="test.key"
        >
          {({ isLoading, value }) => {
            const isEnabled = value;

            if (isLoading) {
              return null;
            }

            if (isEnabled) {
              return <p>enabled</p>;
            }

            return <p>disabled</p>;
          }}
        </FeatureFlagWithApollo>,
      );

      expect(wrapper.text()).to.include('enabled');
    });
  });
});
