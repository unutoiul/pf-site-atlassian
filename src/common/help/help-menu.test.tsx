import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import AkDropdownMenu from '@atlaskit/dropdown-menu';
import AkQuestionCircleIcon from '@atlaskit/icon/glyph/question-circle';
import { AkGlobalItem } from '@atlaskit/navigation';
import { SpotlightTarget as AkSpotlightTarget } from '@atlaskit/onboarding';

import { analyticsClient } from 'common/analytics';

import { I18nOnboardingDialog, i18nOnboardingId, locale } from 'common/intl';

import {
  globalHelpMenuButtonEvent,
  HelpMenuImpl,
  helpMenuLinks,
} from './help-menu';

import { createMockIntlProp } from '../../utilities/testing';

describe('Help Menu', () => {
  let helpMenu;

  describe('rendering when language is English', () => {

    beforeEach(() => {
      helpMenu = shallow(
        <HelpMenuImpl intl={createMockIntlProp()} i18nGermanFeatureFlag={{ isLoading: false, value: false }} i18nJapaneseFeatureFlag={{ isLoading: false, value: false }} />,
      );
    });

    it('should not render spotlight onboarding dialog when language is not targeted', () => {
      expect(helpMenu.find(I18nOnboardingDialog).length).to.equal(0);
    });

    it('should render DropdownMenu, GlobalItem and Icon', () => {
      expect(helpMenu.find(AkDropdownMenu).length).to.equal(1);
      expect(helpMenu.find(AkGlobalItem).length).to.equal(1);
      expect(helpMenu.find(AkQuestionCircleIcon).length).to.equal(1);
    });

    it('should populate DropdownMenu with help links', () => {
      const menuSections: any[] = (helpMenu.find(AkDropdownMenu).props()).items;
      const helpSections = menuSections[0].items;
      const legalSections = menuSections[1].items;
      expect(helpSections.length).to.equal(1);
      expect(legalSections.length).to.equal(2);
      expect(helpSections[0].href).to.equal(helpMenuLinks.atlassianDocumentationUrl);
    });
  });

  describe('rendering when language is not English', () => {
    let sandbox;
    const createWrapper = (isInLoadingState: boolean, i18nEnabled: boolean) => (
      shallow(
        <HelpMenuImpl intl={createMockIntlProp()} i18nGermanFeatureFlag={{ isLoading: isInLoadingState, value: i18nEnabled }} i18nJapaneseFeatureFlag={{ isLoading: isInLoadingState, value: i18nEnabled }} />,
      )
    );

    beforeEach(() => {
      sandbox = sinon.sandbox.create();
      sandbox.stub(locale, 'languageToLocale').returns('de');
    });

    afterEach(() => {
      sandbox.restore();
    });

    it('should render spotlight onboarding dialog when i18n is enabled, LaunchDarkly stopped loading, and language is targeted', () => {
      helpMenu = createWrapper(false, true);
      expect(helpMenu.find(I18nOnboardingDialog).length).to.equal(1);
      const id: string = helpMenu.find(I18nOnboardingDialog).dive().props().name;
      expect(id).to.equal(i18nOnboardingId);
      expect(helpMenu.find(AkSpotlightTarget).length).to.equal(1);
      const targetName: string = helpMenu.find(AkSpotlightTarget).props().name;
      expect(targetName).to.equal('HelpMenu');
    });

    it('should not render spotlight onboarding dialog when i18n is enabled, LaunchDarkly is loading, and language is targeted', () => {
      helpMenu = createWrapper(true, true);
      expect(helpMenu.find(I18nOnboardingDialog).length).to.equal(0);
    });

    it('should not render spotlight onboarding dialog when i18n is not enabled, LaunchDarkly stopped loading, and language is targeted', () => {
      helpMenu = createWrapper(false, false);
      expect(helpMenu.find(I18nOnboardingDialog).length).to.equal(0);
    });

    it('should populate DropdownMenu with change language settings link', () => {
      helpMenu = createWrapper(false, true);
      const menuSections: any[] = (helpMenu.find(AkDropdownMenu).props()).items;
      const helpSections = menuSections[0].items;
      const legalSections = menuSections[1].items;
      expect(helpSections.length).to.equal(2);
      expect(legalSections.length).to.equal(2);
      expect(helpSections[1].href).to.equal(helpMenuLinks.changeLanguageSettingsUrl);
    });
  });

  describe('analytics', () => {
    let sandbox;
    let eventHandlerSpy;

    beforeEach(() => {
      sandbox = sinon.sandbox.create();
      eventHandlerSpy = sandbox.spy(analyticsClient, 'onEvent');

      helpMenu = shallow(
        <HelpMenuImpl intl={createMockIntlProp()} i18nGermanFeatureFlag={{ isLoading: false, value: false }} i18nJapaneseFeatureFlag={{ isLoading: false, value: false }} />,
      );
    });

    afterEach(() => {
      sandbox.restore();
    });

    it('should emit global button event when clicked', () => {
      const questionIcon = helpMenu.find(AkGlobalItem).at(0);

      questionIcon.simulate('click', {
        preventDefault: () => (null),
      });

      expect(eventHandlerSpy.calledWith(
        globalHelpMenuButtonEvent,
      )).to.equal(true);
    });
  });
});
