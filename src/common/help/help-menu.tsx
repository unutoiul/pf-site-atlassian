import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import AkDropdownMenu from '@atlaskit/dropdown-menu';
import AkQuestionCircleIcon from '@atlaskit/icon/glyph/question-circle';
import { AkGlobalItem } from '@atlaskit/navigation';
import { SpotlightTarget as AkSpotlightTarget } from '@atlaskit/onboarding';

import {
  analyticsClient,
  AnalyticsData,
} from 'common/analytics';
import { FeatureFlagI18nGerman, FeatureFlagI18nJapanese, I18nOnboardingDialog, withI18nGermanFeatureFlag, withI18nJapaneseFeatureFlag } from 'common/intl';
import { NavigationTooltip } from 'common/navigation-tooltip';

import { isCurrentLangEnabled } from '../../i18n/languages';

export const helpMenuLinks = {
  termsOfUseUrl: 'https://www.atlassian.com/legal/customer-agreement',
  privacyPolicyUrl: 'https://www.atlassian.com/legal/privacy-policy',
  atlassianDocumentationUrl: 'https://confluence.atlassian.com',
  changeLanguageSettingsUrl: 'https://confluence.atlassian.com/x/ATgWN',
};

export const globalHelpMenuButtonEvent: AnalyticsData = {
  action: 'click',
  subproduct: 'chrome',
  actionSubject: 'sidebarGlobalButton',
  actionSubjectId: 'help-menu',
};

export const helpMenuItemDocumentationEvent: AnalyticsData = {
  action: 'click',
  subproduct: 'chrome',
  actionSubject: 'sidebarGlobalButton',
  actionSubjectId: 'help-menu.atlassian-documentation',
};

export const helpMenuItemChangeLanguageEvent: AnalyticsData = {
  action: 'click',
  subproduct: 'chrome',
  actionSubject: 'sidebarGlobalButton',
  actionSubjectId: 'help-menu.change-language-settings',
};

export const helpMenuItemTermsofUseEvent: AnalyticsData = {
  action: 'click',
  subproduct: 'chrome',
  actionSubject: 'sidebarGlobalButton',
  actionSubjectId: 'help-menu.terms-of-use',
};

export const helpMenuItemPrivacyPolicyEvent: AnalyticsData = {
  action: 'click',
  subproduct: 'chrome',
  actionSubject: 'sidebarGlobalButton',
  actionSubjectId: 'help-menu.privacy-policy',
};

const messages = defineMessages({
  helpHeading: {
    id: 'pf-site-admin-ui.help-menu.help-heading',
    defaultMessage: 'Help',
  },
  legalHeading: {
    id: 'pf-site-admin-ui.help-menu.legal-heading',
    defaultMessage: 'Legal',
  },
  atlassianDocumentation: {
    id: 'pf-site-admin-ui.help-menu.documentation',
    defaultMessage: 'Atlassian Documentation',
  },
  termsOfUse: {
    id: 'pf-site-admin-ui.help-menu.terms-of-use',
    defaultMessage: 'Terms of use',
  },
  privacyPolicy: {
    id: 'pf-site-admin-ui.help-menu.privacy-policy',
    defaultMessage: 'Privacy policy',
  },
  globalItemButtonTooltip: {
    id: 'pf-site-admin-ui.tooltip.help-menu',
    defaultMessage: 'Help',
  },
});

/*  The questionIconColorProp is referenced and used in this way in
    order to work around AtlasKit's lack of Typescript support.
    The AkIcon component doesnt support passing color props and
    tslint throws errors. Will ultimately remove in the future.
    AtlasKit Ticket: https://ecosystem.atlassian.net/browse/AK-3545
*/
const questionIconColorProp = {
  primaryColor: 'white',
  secondaryColor: 'inherit',
};

export class HelpMenuImpl extends React.Component<InjectedIntlProps & FeatureFlagI18nGerman & FeatureFlagI18nJapanese> {

  public render() {
    const { formatMessage } = this.props.intl;

    return (
      <AkDropdownMenu
        items={this.getHelpMenuDropdownItems(this.shouldShowSpotlight())}
        shouldFlip={false}
        position="right bottom"
        onItemActivated={this.onItemActivated}
      >
        <NavigationTooltip message={formatMessage(messages.globalItemButtonTooltip)}>
          <AkGlobalItem onClick={this.onClick}>
            {this.shouldShowSpotlight() ? (
              <React.Fragment>
                <AkSpotlightTarget name="HelpMenu">
                  <AkQuestionCircleIcon label="help" size="medium" {...questionIconColorProp} />
                </AkSpotlightTarget>
                <I18nOnboardingDialog />
              </React.Fragment>
            ) : (
                <AkQuestionCircleIcon label="help" size="medium" {...questionIconColorProp} />
              )}
          </AkGlobalItem>
        </NavigationTooltip>
      </AkDropdownMenu>

    );
  }
  private shouldShowSpotlight = () => {
    const { i18nGermanFeatureFlag, i18nJapaneseFeatureFlag } = this.props;
    const i18nFlags: Array<{ value: boolean, isLoading: boolean, locale: string }> = [
      { ...i18nGermanFeatureFlag, locale: 'de' },
      { ...i18nJapaneseFeatureFlag, locale: 'ja' },
    ];

    return isCurrentLangEnabled(i18nFlags);
  }

  private onClick = e => {
    e.preventDefault();
    analyticsClient.eventHandler(globalHelpMenuButtonEvent);
  }

  private onItemActivated = (item) => {
    if (item.item.analyticsData) {
      analyticsClient.eventHandler(item.item.analyticsData);
    }
  }

  private getHelpMenuDropdownItems(shouldShowSpotlight: boolean) {
    const { formatMessage } = this.props.intl;
    const {
      termsOfUseUrl,
      privacyPolicyUrl,
      atlassianDocumentationUrl,
      changeLanguageSettingsUrl,
    } = helpMenuLinks;

    const helpItems = [
      {
        content: formatMessage(messages.atlassianDocumentation),
        href: atlassianDocumentationUrl,
        target: '_blank',
        analyticsData: helpMenuItemDocumentationEvent,
      },
    ];

    const legalItems = [
      {
        content: formatMessage(messages.termsOfUse),
        href: termsOfUseUrl,
        target: '_blank',
        analyticsData: helpMenuItemTermsofUseEvent,
      },
      {
        content: formatMessage(messages.privacyPolicy),
        href: privacyPolicyUrl,
        target: '_blank',
        analyticsData: helpMenuItemPrivacyPolicyEvent,
      },
    ];
    if (shouldShowSpotlight) {
      helpItems.push(
        {
          // left in English because we want users to use the default language to navigate changing language settings in case they don't understand their browser language settings
          content: 'Change language settings',
          href: changeLanguageSettingsUrl,
          target: '_blank',
          analyticsData: helpMenuItemChangeLanguageEvent,
        },
      );
    }

    return (
      [
        {
          heading: formatMessage(messages.helpHeading),
          items: helpItems,
        },
        {
          heading: formatMessage(messages.legalHeading),
          items: legalItems,
        },
      ]
    );
  }
}
export const HelpMenu: React.ComponentClass = withI18nGermanFeatureFlag(withI18nJapaneseFeatureFlag(injectIntl(HelpMenuImpl)));
