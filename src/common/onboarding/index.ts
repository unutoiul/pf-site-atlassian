import dismissOnboardingMutation from './dismiss-onboarding.mutation.graphql';
import onboardingQueryGql from './onboarding.query.graphql';

export const onboardingMutation = dismissOnboardingMutation;
export const onboardingQuery = onboardingQueryGql;

export enum OnboardingIdType {
  MOVED_ORG = 'MovedOrg',
}
