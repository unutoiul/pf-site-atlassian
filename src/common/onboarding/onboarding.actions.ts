export enum OnboardingActionTypes {
  QUEUE_ONBOARDING = 'START_ONBOARDING',
  NEXT_STEP = 'NEXT_STEP',
  PREVIOUS_STEP = 'PREVIOUS_STEP',
  FINISH_ONBOARDING = 'FINISH_ONBOARDING',
}

export interface QueueOnboarding {
  type: OnboardingActionTypes.QUEUE_ONBOARDING;
  name: string;
}

export const queueOnboarding = (name: string): QueueOnboarding => ({
  type: OnboardingActionTypes.QUEUE_ONBOARDING,
  name,
});

export interface NextStep {
  type: OnboardingActionTypes.NEXT_STEP;
}

export const nextStep = (): NextStep => ({
  type: OnboardingActionTypes.NEXT_STEP,
});

export interface PreviousStep {
  type: OnboardingActionTypes.PREVIOUS_STEP;
}

export const previousStep = (): PreviousStep => ({
  type: OnboardingActionTypes.PREVIOUS_STEP,
});

export interface FinishOnboarding {
  type: OnboardingActionTypes.FINISH_ONBOARDING;
  name: string;
}

export const finishOnboarding = (name): FinishOnboarding => ({
  type: OnboardingActionTypes.FINISH_ONBOARDING,
  name,
});
