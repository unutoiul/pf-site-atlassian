import * as React from 'react';
import { ChildProps, compose, graphql } from 'react-apollo';
import { connect, DispatchProp } from 'react-redux';

import { Spotlight as AkSpotlight } from '@atlaskit/onboarding';

import { finishOnboarding } from 'common/onboarding/onboarding.actions';

import onboardingQuery from './onboarding.query.graphql';

import { OnboardingQuery } from '../../schema/schema-types';
import { RootState } from '../../store';

type OnClickFunction = () => void;

interface Props {
  name: string;
  stage: number;
  target?: string;
  dialogPlacement?: string;
  targetBgColor?: string;
  targetOnClick?: OnClickFunction;
  targetRadius?: number;
}

interface PropsFromState {
  currentName: string;
  currentStage: number;
}

const mapStateToProps = (state: RootState): PropsFromState => ({
  currentName: state.chrome.onboarding.onboardingQueue[0],
  currentStage: state.chrome.onboarding.currentStage,
});

export class OnboardingDialogImpl extends React.Component<ChildProps<Props & PropsFromState & DispatchProp, OnboardingQuery>, never> {

  public componentWillReceiveProps(nextProps) {
    const hasBeenDismissed = nextProps.data.onboarding && nextProps.data.onboarding.dismissed;
    if (hasBeenDismissed) {
      this.props.dispatch(finishOnboarding(this.props.name));
    }
  }

  public render() {
    const { name, stage, currentName, currentStage, children } = this.props;

    const nameAndStage = `${name}--${stage}`;
    const currentNameAndStage = `${currentName}--${currentStage}`;

    return this.shouldRenderSpotlight(nameAndStage, currentNameAndStage) ?
    (
      <AkSpotlight
          {...this.props}
      >
        {children}
      </AkSpotlight>
    ) :
    null;
  }

  private shouldRenderSpotlight(target, current) {
    const { data: { loading = false, onboarding = null } = {} } = this.props;

    return target === current && !loading && onboarding && !onboarding.dismissed;
  }
}

export const OnboardingDialog = compose(
  connect<PropsFromState, {}, Props>(mapStateToProps),
  graphql<{}, OnboardingQuery>(onboardingQuery, {
    options: (props: Props) => ({
      variables: {
        id: props.name,
      },
    }),
  }),
)(OnboardingDialogImpl);
