import {
  Action,
  OnboardingActionTypes,
} from '../../store/action-types';

export interface OnboardingState {
  onboardingQueue: ReadonlyArray<string>;
  currentStage: number;
}

export const onboardingReducer = (state = { onboardingQueue: [], currentStage: 0 }, action: Action): OnboardingState => {
  switch (action.type) {
    case OnboardingActionTypes.QUEUE_ONBOARDING:
      return {
        ...state,
        onboardingQueue: [
          ...state.onboardingQueue,
          action.name,
        ],
      };
    case OnboardingActionTypes.NEXT_STEP:
      return {
        ...state,
        currentStage: state.currentStage + 1,
      };
    case OnboardingActionTypes.PREVIOUS_STEP:
      return {
        ...state,
        currentStage: Math.max(state.currentStage - 1, 0),
      };
    case OnboardingActionTypes.FINISH_ONBOARDING:
      return {
        ...state,
        onboardingQueue: state.onboardingQueue.filter((item) => item !== action.name),
      };
    default:
      return state;
  }
};
