import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { spy } from 'sinon';

import { Spotlight as AkSpotlight } from '@atlaskit/onboarding';

import { waitUntil } from '../../utilities/testing';
import { OnboardingDialogImpl } from './onboarding-dialog';
import { finishOnboarding } from './onboarding.actions';

describe('OnboardingDialog', () => {

  let dispatch;

  beforeEach(() => {
    dispatch = spy();
  });

  function createStatelessOnboardingDialog({
    isLoading = false,
    isDismissed = false,
    name = 'test',
    stage = 0,
    currentName = 'test',
    currentStage = 0,
  }: any) {
    const data = {
      loading: isLoading,
      onboarding: {
        dismissed: isDismissed,
      },
    } as any;

    return (
      <OnboardingDialogImpl
        data={data}
        name={name}
        stage={stage}
        currentName={currentName}
        currentStage={currentStage}
        target="target"
        dispatch={dispatch}
      >
        <div/>
      </OnboardingDialogImpl>
    );
  }

  it('should render Spotlight', () => {
    const wrapper = shallow(
      createStatelessOnboardingDialog({}),
    );
    expect(wrapper.find(AkSpotlight).length).to.equal(1);
  });

  it('should not render the Spotlight if data.loading=true', () => {
    const wrapper = shallow(
      createStatelessOnboardingDialog({ isLoading: true }),
    );
    expect(wrapper.find(AkSpotlight).length).to.equal(0);
  });

  it('should not render the Spotlight if dismissed=true', () => {
    const wrapper = shallow(
      createStatelessOnboardingDialog({ isDismissed: true }),
    );
    expect(wrapper.find(AkSpotlight).length).to.equal(0);
  });

  it('should not render the Spotlight if current name does not match', () => {
    const wrapper = shallow(
      createStatelessOnboardingDialog({ currentName: 'other' }),
    );
    expect(wrapper.find(AkSpotlight).length).to.equal(0);
  });

  it('should not render the Spotlight if stage does not match', () => {
    const wrapper = shallow(
      createStatelessOnboardingDialog({ stage: 1 }),
    );
    expect(wrapper.find(AkSpotlight).length).to.equal(0);
  });

  it('should finish onboarding flow if dismissed', async () => {
    const wrapper = shallow(
      createStatelessOnboardingDialog({}),
    );
    wrapper.setProps({
      data: {
        onboarding: {
          dismissed: true,
        },
      },
    });
    await waitUntil(() => dispatch.calledWith(finishOnboarding('test')));
  });
});
