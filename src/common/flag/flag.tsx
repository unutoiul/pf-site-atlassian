import * as React from 'react';

import { FlagDescriptor } from './flag-provider';
import { FlagProps, withFlag } from './with-flag';

class FlagImpl extends React.Component<FlagDescriptor & FlagProps> {
  public componentDidMount() {
    const { id, title, description, icon, onDismissed, actions, showFlag } = this.props;

    showFlag({
      id,
      title,
      description,
      icon,
      onDismissed,
      actions,
    });
  }

  public componentWillUnmount() {
    this.props.hideFlag(this.props.id);
  }

  public render() {
    return null;
  }
}

export const Flag = withFlag(FlagImpl);
