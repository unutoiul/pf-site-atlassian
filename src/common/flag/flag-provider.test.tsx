// tslint:disable jsx-use-translation-function no-unbound-method react-this-binding-issue
import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';

import AkFlag, {
  FlagGroup as AkFlagGroup,
 } from '@atlaskit/flag';

import {
  FlagDescriptor,
  FlagProvider,
  FlagRenderer,
  hideFlagContextKey,
  hideFlagShape,
  showFlagContextKey,
  showFlagShape,
} from './flag-provider';

import { FlagProps } from './with-flag';

describe('FlagProvider', () => {
  let comps: TestComponent[];

  interface TestComponentProps {
    content?: string;
  }

  class TestComponent extends React.Component<TestComponentProps & FlagProps> {
    public static contextTypes = {
      [showFlagContextKey]: showFlagShape,
      [hideFlagContextKey]: hideFlagShape,
    };

    public context!: FlagProps;

    public componentDidMount() {
      comps.push(this);
    }

    public componentWillUnmount() {
      comps.splice(comps.indexOf(this), 1);
    }

    public render() {
      return this.props.content ? <span>{this.props.content}</span> : null;
    }
  }

  beforeEach(() => {
    comps = [];
  });

  it('should provide valid context to its children', () => {
    mount((
      <FlagProvider>
        <TestComponent
          showFlag={() => null}
          hideFlag={() => null}
        />
      </FlagProvider>
    ));

    expect(comps[0]).to.not.equal(undefined);

    const context: FlagProps = comps[0].context;
    expect(context).to.be.a('object');
    expect(context).to.have.property(showFlagContextKey);
    expect(context[showFlagContextKey]).to.be.a('function');
    expect(context).to.have.property(hideFlagContextKey);
    expect(context[hideFlagContextKey]).to.be.a('function');
  });

  it('should render its children', () => {
    const Comp1 = ({ children }) => <div>{children}</div>;
    const Comp2 = () => <p>test</p>;

    const wrapper = mount((
      <FlagProvider>
        <Comp1>
          <TestComponent
            showFlag={() => null}
            hideFlag={() => null}
          />
        </Comp1>
        <Comp2 />
      </FlagProvider>
    ));

    expect(wrapper.find(Comp1).length).to.equal(1);
    expect(wrapper.find(Comp2).length).to.equal(1);
    expect(wrapper.find(TestComponent).length).to.equal(1);
  });

  it('should show a flag when showFlag() is called', () => {
    const wrapper = mount((
      <FlagProvider>
        <TestComponent
          showFlag={() => null}
          hideFlag={() => null}
        />
        <FlagRenderer/>
      </FlagProvider>
    ));

    wrapper.find(TestComponent).instance().context.showFlag({
      icon: <div />,
      id: '1',
      title: '1',
      autoDismiss: false,
    } as FlagDescriptor);

    wrapper.update();
    expect(wrapper.find(AkFlagGroup).prop('children')).to.have.length(1);
  });

  it('should hide a flag when hideFlag() is called', () => {
    const wrapper = mount((
      <FlagProvider>
        <TestComponent
          showFlag={() => null}
          hideFlag={() => null}
        />
        <FlagRenderer/>
      </FlagProvider>
    ));

    wrapper.find(TestComponent).instance().context.showFlag({
      icon: <div />,
      id: '2',
      title: '2',
      autoDismiss: false,
    } as FlagDescriptor);

    wrapper.update();
    expect(wrapper.find(AkFlagGroup).prop('children')).to.have.length(1);
    wrapper.find(TestComponent).instance().context.hideFlag('2');
    wrapper.update();
    expect(wrapper.find(AkFlagGroup).prop('children')).to.equal(null);
  });

  it('should render the newest flag on top', () => {
    const wrapper = mount((
      <FlagProvider>
        <TestComponent
          showFlag={() => null}
          hideFlag={() => null}
        />
        <FlagRenderer/>
      </FlagProvider>
    ));

    wrapper.find(TestComponent).instance().context.showFlag({
      icon: <div />,
      id: '1',
      title: '1',
      autoDismiss: false,
    } as FlagDescriptor);

    wrapper.find(TestComponent).instance().context.showFlag({
      icon: <div />,
      id: '2',
      title: '2',
      autoDismiss: false,
    } as FlagDescriptor);

    wrapper.find(TestComponent).instance().context.showFlag({
      icon: <div />,
      id: '3',
      title: '3',
      autoDismiss: false,
    } as FlagDescriptor);

    wrapper.update();
    expect(wrapper.find(AkFlag).first().prop('id')).to.equal('3');
    expect(wrapper.find(AkFlag).last().prop('id')).to.equal('1');
  });
});
