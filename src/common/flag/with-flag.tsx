import * as React from 'react';

import {
  FlagDescriptor,
  hideFlagContextKey,
  hideFlagShape,
  showFlagContextKey,
  showFlagShape,
} from './flag-provider';

export interface FlagProps {
  showFlag(flag: FlagDescriptor): void;
  hideFlag(flagId: string): void;
}

export function withFlag<TOwnProps>(WrappedComponent: React.ComponentType<TOwnProps & FlagProps>): React.ComponentClass<TOwnProps> {
  return class extends React.Component<TOwnProps> {
    public static contextTypes = {
      [showFlagContextKey]: showFlagShape,
      [hideFlagContextKey]: hideFlagShape,
    };

    public static WrappedComponent = WrappedComponent;

    public render() {
      return (
        <WrappedComponent
          {...this.props}
          showFlag={this.context.showFlag}
          hideFlag={this.context.hideFlag}
        />
      );
    }
  };
}
