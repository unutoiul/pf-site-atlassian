// tslint:disable:jsx-use-translation-function
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import * as React from 'react';

import AkErrorIcon from '@atlaskit/icon/glyph/editor/error';

import { Flag } from './flag';
import { FlagProvider } from './flag-provider';

storiesOf('Common|Flag', module)
  .add('Default', () => {
    return (
      <FlagProvider>
        <Flag
          id="my-awesome-flag"
          title="This is flag title"
          description="Some descriptive description for the flag"
          icon={<AkErrorIcon label="my-error-icon"/>}
          onDismissed={action('Dismiss flag')}
          actions={[{ content: 'Learn More', onClick: action('Clicked learn more') }]}
        />
      </FlagProvider>
    );
  });
