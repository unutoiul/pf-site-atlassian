import * as PropTypes from 'prop-types';
import * as React from 'react';

import AkFlag, {
  AutoDismissFlag as AkAutoDismissFlag,
  FlagGroup as AkFlagGroup,
} from '@atlaskit/flag';

import { AkMobileFlagGroup, NotificationFlag } from 'common/responsive/components/AkMobileFlagGroup';
import { Media } from 'common/responsive/Matcher';

export const showFlagContextKey = 'showFlag';
export const showFlagShape = PropTypes.func.isRequired;

export const hideFlagContextKey = 'hideFlag';
export const hideFlagShape = PropTypes.func.isRequired;

export type FlagAppearance = 'error' | 'info' | 'normal' | 'success' | 'warning';

export interface FlagAction {
  content: any;
  href?: string;
  target?: string;

  onClick?(e: React.MouseEvent<HTMLAnchorElement | HTMLButtonElement | HTMLSpanElement>): void;
}

export interface FlagDescriptor {
  id: string;
  title: string;
  description?: string | React.ReactElement<any>;
  icon: React.ReactNode;
  appearance?: FlagAppearance;
  actions?: FlagAction[];
  autoDismiss?: boolean;

  onDismissed?(): void;
}

type FlagMap = Map<string, FlagDescriptor>;

// TODO: extract me to another file, or FeatureFlag please
const DEBUG_E2E = true;

const E2EAdapter: React.StatelessComponent<{ flag: FlagDescriptor }> = ({ flag, children }) => (
  DEBUG_E2E
    ? (
      <div
        data-flag-e2e={true}
        data-flag-id={flag.id}
        data-title={flag.title}
        data-type={flag.appearance}
      >
        {children}
      </div>
    ) : (
      <div>{children}</div>
    )
);

const FlagContext = React.createContext<{
  flags: null | NotificationFlag[],
  hide(id: string): void;
}>({} as any);

export const FlagRenderer: React.SFC = () => (
  <FlagContext.Consumer>
    {({ flags, hide }) => (
      <Media.Matcher
        mobile={<AkMobileFlagGroup onDismissed={hide}>{flags}</AkMobileFlagGroup>}
        tablet={<AkFlagGroup onDismissed={hide}>{flags}</AkFlagGroup>}
      />
    )}
  </FlagContext.Consumer>
);

export class FlagProvider extends React.Component {
  public static childContextTypes = {
    [showFlagContextKey]: showFlagShape,
    [hideFlagContextKey]: hideFlagShape,
  };

  private flags: FlagMap = new Map<string, FlagDescriptor>();

  public getChildContext() {
    return {
      [showFlagContextKey]: this.show,
      [hideFlagContextKey]: this.hide,
    };
  }

  public render() {
    return (
      <FlagContext.Provider
        value={{
          flags: this.renderFlags(),
          hide: this.hide,
        }}
      >
        {this.props.children}
      </FlagContext.Provider>
    );
  }

  private renderFlags() {
    if (!this.flags.size) {
      return null;
    }

    return Array.from(this.flags.values())
      .reverse()
      .map(flag => {
        const FlagComponent = flag.autoDismiss ? AkAutoDismissFlag : AkFlag;

        return (
          <FlagComponent
            id={flag.id}
            key={flag.id}
            description={flag.description}
            title={flag.title}
            icon={<E2EAdapter flag={flag}>{flag.icon}</E2EAdapter>}
            appearance={flag.appearance}
            actions={flag.actions}
          />
        );
      });
  }

  private show = (flag: FlagDescriptor) => {
    if (this.flags.has(flag.id)) {
      if (__NODE_ENV__ === 'development') {
        // tslint:disable-next-line:no-console
        console.warn(`FlagProvider: a flag with the id=${flag.id} is already shown`);
      }

      return;
    }

    this.flags.set(flag.id, flag);
    this.forceUpdate();
  };

  private hide = (flagId: string) => {
    const flag = this.flags.get(flagId);
    if (!flag) {
      return;
    }
    this.flags.delete(flagId);
    if (flag.onDismissed) {
      flag.onDismissed();
    }
    this.forceUpdate();
  };
}
