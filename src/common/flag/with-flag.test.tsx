import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';

import { FlagProvider } from './flag-provider';
import { withFlag } from './with-flag';

describe('withFlag', () => {
  it('passes showFlag() prop to the wrapped component', () => {
    const TestComponent = ({ value }) => <div>{value}</div>;
    const WrapperComponent = withFlag(TestComponent);
    const wrapper = mount((
      <FlagProvider>
        <WrapperComponent value="test" />
      </FlagProvider>
    )) as any;

    expect(wrapper.find(WrapperComponent).find(TestComponent).prop('showFlag'))
      .to.be.a('function');
  });

  it('passes hideFlag() prop to the wrapped component', () => {
    const TestComponent = ({ value }) => <div>{value}</div>;
    const WrapperComponent = withFlag(TestComponent);
    const wrapper = mount((
      <FlagProvider>
        <WrapperComponent value="test" />
      </FlagProvider>
    )) as any;

    expect(wrapper.find(WrapperComponent).find(TestComponent).prop('hideFlag'))
      .to.be.a('function');
  });
});
