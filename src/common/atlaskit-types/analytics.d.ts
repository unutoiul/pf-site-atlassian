import * as React from 'react';

import { AnalyticsProps as AkAnalyticsProps } from '@atlaskit/analytics';

declare module '@atlaskit/analytics' {
  export function withAnalytics<TProps>(
    component: React.ComponentType<any>,
    map: EventMapOrFunction,
    defaultProps: AkAnalyticsProps,
    withDelegation?: boolean,
  ): React.ComponentClass<TProps & AkAnalyticsProps>;
}
