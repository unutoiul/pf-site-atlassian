// tslint:disable max-classes-per-file
declare module '@atlaskit/dynamic-table' {
  import * as React from 'react';

  export interface HeadCell {
    key: React.Key;
    content: React.ReactNode;
    isSortable?: boolean;
    width?: number;
    shouldTruncate?: boolean;
    // Style is not a valid prop. To be deprecated at a later date.
    // https://bitbucket.org/atlassian/atlaskit-mk-2/src/master/packages/core/dynamic-table/src/components/TableHeadCell.js
    style?: any;
    inlineStyles?: any;
  }

  export interface HeaderRow {
    cells: HeadCell[];
  }

  export interface Cell {
    key: React.Key;
    content: React.ReactNode;
    colSpan?: number;
    style?: Record<string, string | number | boolean | undefined>;
  }

  export interface Row {
    cells: Cell[];
    key?: string;
    style?: Record<string, string | number | boolean | undefined>;
    'aria-hidden'?: boolean;
  }

  export type SortOrder = 'ASC' | 'DESC';

  export interface DynamicTableStatelessProps {
    rows?: Row[];
    head?: HeaderRow;
    caption?: React.ReactNode;
    emptyView?: React.ReactNode;
    loadingSpinnerSize?: 'small' | 'large';
    isLoading?: boolean;
    isFixedSize?: boolean;
    rowsPerPage?: number;
    page?: number;
    paginationi18n?: {
      prev: string,
      next: string,
    };
    sortKey?: string;
    sortOrder?: SortOrder;
    isRankable?: boolean;
    isRankingDisabled?: boolean;
    onSetPage?(page: number): any;
    onSort?(item: { key: React.Key, sortOrder: SortOrder, item: Cell }): any;
    onRankStart?(params: any): any;
    onRankEnd?(params: any): any;
  }

  export class DynamicTableStateless extends React.Component<DynamicTableStatelessProps> { }
  export default class DynamicTable extends React.Component<any> { }
}
