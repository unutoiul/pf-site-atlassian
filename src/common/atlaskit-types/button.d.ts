// tslint:disable max-classes-per-file
/*
  Typings were removed in `@atlaskit/button` 6.0.0. We maintain a separate version here.
*/
declare module '@atlaskit/button' {
  import {
    AnchorHTMLAttributes,
    Component,
    ReactChild,
    ReactNode,
  } from 'react';

  export interface AkButtonProps extends AnchorHTMLAttributes<HTMLAnchorElement> {
    appearance?: 'primary' | 'default' | 'subtle' | 'link' | 'subtle-link' | 'warning' | 'danger' | 'help';
    shouldFitContainer?: boolean;
    type?: 'button' | 'submit';
    href?: string;
    target?: string;
    form?: string;
    isDisabled?: boolean;
    spacing?: 'default' | 'compact' | 'none';
    isSelected?: boolean;
    isLoading?: boolean;
    theme?: 'default' | 'dark';
    iconBefore?: ReactChild;
    iconAfter?: ReactChild;
    className?: string;
    tabIndex?: number;
    ariaHaspopup?: boolean;
    ariaExpanded?: boolean;
    ariaControls?: string;
    id?: string;
    children?: ReactNode;
    component?: ReactNode;
    onClick?(): void;
  }

  export interface AkButtonGroupProps {
    appearance?: 'primary' | 'default' | 'subtle' | 'link' | 'subtle-link' | 'warning' | 'danger' | 'help';
    children?: ReactNode;
  }

  export class ButtonGroup extends Component<AkButtonGroupProps, {}> { }
  export default class extends Component<AkButtonProps, {}> { }
}
