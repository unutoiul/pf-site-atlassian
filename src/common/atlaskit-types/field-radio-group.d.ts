// tslint:disable max-classes-per-file
declare module '@atlaskit/field-radio-group' {
  import * as React from 'react';

  export interface AkRadioProps {
    value: string;
    name?: string;
    isSelected?: boolean;
    isDisabled?: boolean;
    onChange(event: React.ChangeEvent<HTMLInputElement>): any;
  }

  class AkFieldRadioGroup extends React.Component<any> { }

  export class AkRadio extends React.Component<AkRadioProps> { }
  export default AkFieldRadioGroup;
}
