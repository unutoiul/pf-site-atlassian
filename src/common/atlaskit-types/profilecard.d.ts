declare module '@atlaskit/profilecard' {
  import { PureComponent } from 'react';

  type PresenceTypes =
  | 'none'
  | 'available'
  | 'busy'
  | 'unavailable'
  | 'focus';

  interface ProfileCardAction {
    id: string;
    label: string;
    callback?(): void;
    shouldRender?(): void;
  }

  interface AkProfilecardProps {
    isCensored?: boolean;
    isActive?: boolean;
    isBot?: boolean;
    avatarUrl?: string;
    fullName?: string;
    meta?: string;
    nickname?: string;
    email?: string;
    location?: string;
    timestring?: string;
    presence?: PresenceTypes;
    actions?: ProfileCardAction[];
    isLoading?: boolean;
    hasError?: boolean;
    errorType?: ProfileCardErrorType;
    presenceMessage?: string;
    clientFetchProfile?(): void;
    analytics?(): void;
  }

  interface ProfileCardErrorType {
    reason: 'default' | 'NotFound';
  }

  export class AkProfilecard extends PureComponent<AkProfilecardProps> { }
}
