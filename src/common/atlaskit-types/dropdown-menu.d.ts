// tslint:disable max-classes-per-file
declare module '@atlaskit/dropdown-menu' {
  import {
    AnchorHTMLAttributes,
    Component,
    ReactChild,
    ReactNode,
  } from 'react';

  import { AkButtonProps } from '@atlaskit/button';

  export interface AkDropdownProps {
    appearance?: 'default' | 'tall';
    boundariesElement?: 'viewport' | 'window' | 'scrollParent';
    triggerType?: 'default' | 'button';
    trigger?: React.ReactNode;
    triggerButtonProps?: AkButtonProps;
    items?: any[];
    onItemActivated?: any;
    position?: string;
    shouldFlip?: boolean;
    isDisabled?: boolean;
    shouldFitContainer?: boolean;
  }

  export interface AkDropdownItemProps {
    id?: string;
    isDisabled?: boolean;
    href?: string;
    onChanged?(e): void;
    onClick?(e): void;
  }

  export interface DropdownGroupItemProps {
    id?: string;
    title?: string;
  }

  export class DropdownMenuStateless extends Component<{}> { }
  export class DropdownItemGroup extends Component<DropdownGroupItemProps> { }
  export class DropdownItemGroupCheckbox extends Component<{}> { }
  export class DropdownItem extends Component<AkDropdownItemProps> { }
  export class DropdownItemCheckbox extends Component<any> {}
  export default class AkDropdown extends Component<AkDropdownProps> { }
}
