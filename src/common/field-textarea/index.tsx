import * as React from 'react';

import FieldTextAreaStateless, { FieldTextAreaStatelessProps } from './field-textarea-stateless';

export { FieldTextAreaStateless };

interface FieldTextAreaState {
  value?: string;
}

// tslint:disable-next-line:no-default-export
export default class FieldTextArea extends React.PureComponent<FieldTextAreaStatelessProps, FieldTextAreaState> {
  private textarea: HTMLTextAreaElement | undefined;

  public state = {
    value: this.props.value,
  };

  public focus() {
    if (!this.textarea) {
      return;
    }
    this.textarea.focus();
  }

  public render() {
    return (
      <FieldTextAreaStateless
        {...this.props}
        value={this.state.value}
        onChange={this.handleOnChange}
        // tslint:disable-next-line:react-this-binding-issue
        ref={i => this.textarea = i as any}
      />
    );
  }

  private handleOnChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    this.setState({ value: e.target.value });
    this.props.onChange(e);
  }
}
