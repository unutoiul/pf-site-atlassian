import * as React from 'react';
import styled from 'styled-components';

import AkBase, { Label as AkLabel } from '@atlaskit/field-base';

const TextArea = styled.textarea`
  background: transparent;
  border: 0;
  box-sizing: border-box;
  color: inherit;
  cursor: inherit;
  font-size: 14px;
  outline: none;
  width: 100%;

  &::-ms-clear {
    display: none;
  }

  &:invalid {
    box-shadow: none;
  }
`;

export interface FieldTextAreaStatelessProps {
  compact?: boolean;
  disabled?: boolean;
  isReadOnly?: boolean;
  required?: boolean;
  isInvalid?: boolean;
  isLabelHidden?: boolean;
  invalidMessage?: React.ReactNode;
  shouldFitContainer?: boolean;
  isSpellCheckEnabled?: boolean;
  autoFocus?: boolean;
  label?: string;
  name?: string;
  placeholder?: string;
  value?: string;
  id?: string;
  rows?: number;
  onChange(e: React.ChangeEvent<HTMLTextAreaElement>): void;
}

// tslint:disable-next-line:no-default-export
export default class FieldTextAreaStateless extends React.PureComponent<FieldTextAreaStatelessProps> {
  public static defaultProps = {
    compact: false,
    disabled: false,
    isReadOnly: false,
    required: false,
    isInvalid: false,
    isSpellCheckEnabled: true,
  };

  private textarea: HTMLTextAreaElement | undefined;

  public focus() {
    if (!this.textarea) {
      return;
    }

    this.textarea.focus();
  }

  public render() {
    return (
      <div>
        <AkLabel
          label={this.props.label}
          htmlFor={this.props.id}
          isLabelHidden={this.props.isLabelHidden}
          isRequired={this.props.required}
        />
        <AkBase
          isCompact={this.props.compact}
          isDisabled={this.props.disabled}
          isInvalid={this.props.isInvalid}
          isReadOnly={this.props.isReadOnly}
          isRequired={this.props.required}
          invalidMessage={this.props.invalidMessage}
          isFitContainerWidthEnabled={this.props.shouldFitContainer}
        >
          <TextArea
            disabled={this.props.disabled}
            readOnly={this.props.isReadOnly}
            name={this.props.name}
            placeholder={this.props.placeholder}
            value={this.props.value}
            required={this.props.required}
            onChange={this.props.onChange}
            id={this.props.id}
            autoFocus={this.props.autoFocus}
            spellCheck={this.props.isSpellCheckEnabled}
            rows={this.props.rows}
            // tslint:disable-next-line:react-this-binding-issue
            innerRef={i => this.textarea = i}
          />
        </AkBase>
      </div>
    );
  }
}
