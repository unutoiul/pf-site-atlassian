import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { spy, stub } from 'sinon';

import AkFieldBase from '@atlaskit/field-base';

import { Button } from 'common/analytics';
import { ModalDialog } from 'common/modal';

import { createMockIntlContext, createMockIntlProp, waitUntil } from '../../utilities/testing';
import { FeedbackDialogImpl } from './feedback-dialog';

describe('FeedbackDialog', () => {
  it('renders the dialog if isOpen prop is true', () => {
    const wrapper = shallow(
      <FeedbackDialogImpl
        isOpen={true}
        intl={createMockIntlProp()}
        {...{} as any}
      />,
      createMockIntlContext(),
    );

    expect(wrapper.containsMatchingElement(ModalDialog as any)).to.equal(true);
  });

  it('does not render the dialog if isOpen prop is false', () => {
    const wrapper = shallow(
      <FeedbackDialogImpl
        isOpen={false}
        intl={createMockIntlProp()}
        {...{} as any}
      />,
      createMockIntlContext(),
    );

    expect(wrapper.containsMatchingElement(ModalDialog as any)).to.equal(false);
  });

  it('calls dismissFeedback if Send is clicked', () => {
    const dismissFeedback = spy();
    const showFlagSpy = spy();
    const user = { currentUser: 'test@test.com' };

    const mutate = stub();
    mutate.returns(Promise.resolve('OK'));

    const wrapper: any = shallow(
      <FeedbackDialogImpl
        isOpen={true}
        dismissFeedback={dismissFeedback}
        mutate={mutate}
        showFlag={showFlagSpy}
        data={user}
        intl={createMockIntlProp()}
        {...{} as any}
      />,
      createMockIntlContext(),
    );

    const footer: any = wrapper.find(ModalDialog).prop('footer');
    const button = shallow(footer).find(Button)
      .filterWhere((e) => e.html().indexOf('Send') > -1);

    expect(dismissFeedback.callCount).to.equal(0);
    button.simulate('click');
    expect(dismissFeedback.callCount).to.equal(1);
  });

  it('calls mutate with correct parameters if Send is clicked', () => {
    const showFlagSpy = spy();
    const mutate = stub();
    mutate.returns(Promise.resolve('OK'));

    const dismissFeedback = spy();
    const data = {
      currentUser: {
        email: 'foo@example.com',
      },
    };
    const feedbackText = 'This is great';

    const wrapper = shallow(
      <FeedbackDialogImpl
        isOpen={true}
        mutate={mutate}
        data={data}
        dismissFeedback={dismissFeedback}
        showFlag={showFlagSpy}
        intl={createMockIntlProp()}
        {...{} as any}
      />,
      createMockIntlContext(),
    );

    wrapper.find(AkFieldBase).children().simulate('change', { target: { value: feedbackText } });

    const footer: any = wrapper.find(ModalDialog).prop('footer');
    const button = shallow(footer).find(Button)
      .filterWhere((e) => e.html().indexOf('Send') > -1);

    expect(mutate.callCount).to.equal(0);

    button.simulate('click');

    expect(mutate.calledWithMatch({
      variables: {
        comment: feedbackText,
        email: data.currentUser.email,
      },
    })).to.equal(true);
  });

  it('calls flag.show with correct parameters if Send is clicked', async () => {
    const mutate = stub();
    mutate.returns(Promise.resolve('OK'));

    const showFlagSpy = spy();
    const data = {
      currentUser: {
        email: 'foo@example.com',
      },
    };

    const wrapper = shallow(
      <FeedbackDialogImpl
        isOpen={true}
        mutate={mutate}
        data={data}
        showFlag={showFlagSpy}
        dismissFeedback={spy()}
        intl={createMockIntlProp()}
        {...{} as any}
      />,
      createMockIntlContext(),
    );

    const footer: any = wrapper.find(ModalDialog).prop('footer');

    const button = shallow(footer).find(Button)
      .filterWhere((e) => e.html().indexOf('Send') > -1);

    expect(showFlagSpy.callCount).to.equal(0);

    button.simulate('click');

    await waitUntil(() => showFlagSpy.callCount > 0);

    expect(showFlagSpy.calledWithMatch({
      title: 'Thank you for your feedback!',
      id: 'chrome.feedback.success',
      description: 'Your feedback has been submitted.',
    })).to.equal(true);
  });

  it('dismisses the modal and shows the success flag optimistically', () => {
    const mutate = stub();
    mutate.returns(Promise.reject('some failure'));

    const dismissFeedback = spy();
    const showFlagSpy = spy();
    const data = {
      currentUser: {
        email: 'foo@example.com',
      },
    };
    const feedbackText = 'This is great';

    const wrapper = shallow(
      <FeedbackDialogImpl
        isOpen={true}
        mutate={mutate}
        data={data}
        dismissFeedback={dismissFeedback}
        showFlag={showFlagSpy}
        intl={createMockIntlProp()}
        {...{} as any}
      />,
      createMockIntlContext(),
    );

    wrapper.find(AkFieldBase).children().simulate('change', {
      target: {
        value: feedbackText,
      },
    });

    const footer: any = wrapper.find(ModalDialog).prop('footer');
    const button = shallow(footer).find(Button)
      .filterWhere((e) => e.html().indexOf('Send') > -1);

    expect(mutate.notCalled).to.equal(true);
    expect(dismissFeedback.notCalled).to.equal(true);
    expect(showFlagSpy.notCalled).to.equal(true);

    button.simulate('click');

    expect(mutate.calledOnce).to.equal(true);
    expect(dismissFeedback.calledOnce).to.equal(true);
    expect(showFlagSpy.callCount).to.equal(1);
  });

  it('calls dismissFeedback if Cancel is clicked', () => {
    const showFlagSpy = spy();
    const dismissFeedback = spy();
    const wrapper = shallow(
      <FeedbackDialogImpl
        isOpen={true}
        dismissFeedback={dismissFeedback}
        showFlag={showFlagSpy}
        hideFlag={() => null}
        intl={createMockIntlProp()}
      />,
      createMockIntlContext(),
    );

    const footer: any = wrapper.find(ModalDialog).prop('footer');
    const button = shallow(footer).find(Button)
      .filterWhere((e) => e.html().indexOf('Cancel') > -1);

    expect(dismissFeedback.callCount).to.equal(0);
    button.simulate('click');
    expect(dismissFeedback.callCount).to.equal(1);
  });
});
