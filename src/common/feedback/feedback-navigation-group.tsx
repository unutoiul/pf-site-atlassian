import * as React from 'react';
import { graphql } from 'react-apollo';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import { AkNavigationItemGroup } from '@atlaskit/navigation';

import { NavigationLink, NavigationLinkIconGlyph } from 'common/navigation';

import { UpdateIsFeedbackOpenMutation } from '../../schema/schema-types';
import updateIsFeedbackOpenMutation from './feedback-navigation-group.mutation.graphql';

const messages = defineMessages({
  newExperienceTitle: {
    id: 'chrome.navigation.feedback.new-experience-title',
    defaultMessage: 'New Admin Experience',
  },
  feedbackLabel: {
    id: 'chrome.navigation.feedback.feedback-label',
    defaultMessage: 'Give feedback',
  },
});

export interface DerivedProps {
  showFeedback(): void;
}

export class FeedbackNavigationGroupImpl extends React.PureComponent<DerivedProps & InjectedIntlProps> {
  public render() {
    const { formatMessage } = this.props.intl;

    return (
      <AkNavigationItemGroup title={formatMessage(messages.newExperienceTitle)}>
        <NavigationLink
          icon={NavigationLinkIconGlyph.Feedback}
          title={formatMessage(messages.feedbackLabel)}
          onClick={this.onFeedbackClick}
          analyticsData={{ action: 'click', actionSubject: 'sidebarContainerButton', actionSubjectId: 'feedback', subproduct: 'chrome' }}
        />
      </AkNavigationItemGroup>
    );
  }

  private onFeedbackClick = () => {
    this.props.showFeedback();
  }
}

const withShowFeedback = graphql<{}, UpdateIsFeedbackOpenMutation, {}, DerivedProps>(
  updateIsFeedbackOpenMutation,
  {
    props: ({ mutate }) => ({
      showFeedback: async () => mutate instanceof Function && mutate({}),
    }),
  },
);

export const FeedbackNavigationGroup = withShowFeedback(injectIntl(FeedbackNavigationGroupImpl));
