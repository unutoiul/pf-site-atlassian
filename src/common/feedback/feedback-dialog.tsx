import * as React from 'react';
import { ChildProps, compose, graphql } from 'react-apollo';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import { ButtonGroup as AkButtonGroup } from '@atlaskit/button';
import AkFieldBase from '@atlaskit/field-base';
import { AkRadio } from '@atlaskit/field-radio-group';
import AkCheckCircleIcon from '@atlaskit/icon/glyph/check-circle';
import { akColorN20, akColorN200, akGridSizeUnitless } from '@atlaskit/util-shared-styles';

import { Button } from 'common/analytics';
import { FlagProps, withFlag } from 'common/flag';
import { ModalDialog } from 'common/modal';
import { ExternalLinkWithClickAnalytics } from 'common/navigation';

import dismissFeedbackMutationMutation from './dismiss-feedback-mutation.mutation.graphql';
import feedbackDialogIsOpenQuery from './feedback-dialog-is-open.query.graphql';
import feedbackDialogQuery from './feedback-dialog.query.graphql';
import submitFeedbackMutation from './submit-feedback.mutation.graphql';

import { DissmissFeedbackMutationMutation, FeedbackDialogIsOpenQuery, FeedbackDialogQuery, SubmitFeedbackMutation } from '../../schema/schema-types';

const Icon = AkCheckCircleIcon as any; // The type for AtlasKit icons is missing some properties

const ModalSection = styled.section`
  margin-top: ${akGridSizeUnitless * 2}px;

  &:first-child {
    margin-top: 0;
  }
`;

const Label = styled.div`
  color: ${akColorN200};
  font-size: 12px;
  line-height: 1;
  font-style: normal;
  font-weight: 600;
  margin-bottom: ${akGridSizeUnitless}px;
`;

const TextArea = styled.textarea`
  border: 1px solid transparent;
  font-size: inherit;
  font-weight: inherit;
  line-height: inherit;
  font-family: inherit;
  width: 100%;
  height: ${akGridSizeUnitless * 10}px;
  resize: none;
  padding: ${akGridSizeUnitless}px;
  outline: none;

  &:disabled {
    background-color: ${akColorN20};
  }
`;

const RadioRowContainer = styled.div`
  display: flex;
  align-items: baseline;
`;

const messages = defineMessages({
  feedbackTitle: {
    id: 'chrome.feedback.title',
    defaultMessage: 'Give feedback',
  },
  feedbackDescription: {
    id: 'chrome.feedback.description',
    defaultMessage: 'Tell us about your experience using Administration so that we can make it even better.',
  },
  contactQuestion: {
    id: 'chrome.feedback.contact-question',
    defaultMessage: 'May we contact you to follow up on your feedback?',
  },
  contactAnswerYes: {
    id: 'chrome.feedback.contact-answer-yes',
    defaultMessage: 'Yes',
  },
  contactAnswerNo: {
    id: 'chrome.feedback.contact-answer-no',
    defaultMessage: 'No',
  },
  feedbackQuestion: {
    id: 'chrome.feedback.feedback-question',
    defaultMessage: 'Share your thoughts',
  },
  buttonSend: {
    id: 'chrome.feedback.button-send',
    defaultMessage: 'Send',
  },
  buttonCancel: {
    id: 'chrome.feedback.button-cancel',
    defaultMessage: 'Cancel',
  },
  flagTitle: {
    id: 'chrome.feedback.flag-title',
    defaultMessage: 'Thank you for your feedback!',
  },
  flagDescription: {
    id: 'chrome.feedback.flag-description',
    defaultMessage: 'Your feedback has been submitted.',
  },
  visitSupportSite: {
    id: 'chrome.feedback.visit-support-site',
    defaultMessage: 'For help and support, visit {supportSite}',
  },
  supportSiteLinkText: {
    id: 'chrome.feedback.support-site-link-text',
    defaultMessage: 'support.atlassian.com',
  },
});

interface Props {
  isOpen: boolean;
  dismissFeedback(): void;
}

interface State {
  consentToBeContacted: string;
  comment: string;
}

export class FeedbackDialogImpl extends React.Component<ChildProps<Props & FlagProps & InjectedIntlProps, FeedbackDialogQuery>, State> {
  public state = {
    consentToBeContacted: 'yes',
    comment: '',
  };

  public render() {
    const { isOpen } = this.props;
    const { consentToBeContacted } = this.state;
    const { formatMessage } = this.props.intl;

    if (!isOpen) {
      return null;
    }

    const answerItems = [
      { name: formatMessage(messages.contactAnswerYes), value: 'yes' },
      { name: formatMessage(messages.contactAnswerNo), value: 'no' },
    ];

    return (
      <ModalDialog
        isOpen={isOpen}
        header={formatMessage(messages.feedbackTitle)}
        footer={(
          <AkButtonGroup>
            <Button
              appearance="primary"
              onClick={this.onSubmit}
              analyticsData={{ action: 'click', actionSubject: 'feedbackDialogButton', actionSubjectId: 'send', subproduct: 'chrome' }}
            >
              {formatMessage(messages.buttonSend)}
            </Button>
            <Button
              appearance="link"
              onClick={this.onCancel}
              analyticsData={{ action: 'click', actionSubject: 'feedbackDialogButton', actionSubjectId: 'cancel', subproduct: 'chrome' }}
            >
              {formatMessage(messages.buttonCancel)}
            </Button>
          </AkButtonGroup>
        )}
        width={450}
        onClose={this.onCancel}
      >
        <ModalSection>
          <p>
            <FormattedMessage
              {...messages.visitSupportSite}
              values={{
                supportSite: (
                  <ExternalLinkWithClickAnalytics
                    href="https://support.atlassian.com"
                    hideReferrer={false}
                    analyticsData={{ action: 'click', actionSubject: 'feedbackDialogButton', actionSubjectId: 'support', subproduct: 'chrome' }}
                  >
                    <FormattedMessage {...messages.supportSiteLinkText} />
                  </ExternalLinkWithClickAnalytics>
                ),
              }}
            />
          </p>
        </ModalSection>
        <ModalSection>
          <Label>{formatMessage(messages.feedbackQuestion)}</Label>
          <AkFieldBase appearance="standard" isFitContainerWidthEnabled={true} isPaddingDisabled={true} >
            <TextArea
              placeholder={formatMessage(messages.feedbackDescription)}
              onChange={this.handleTextAreaChange}
              autoFocus={true}
            />
          </AkFieldBase>
        </ModalSection>

        <ModalSection>
          <Label>{formatMessage(messages.contactQuestion)}</Label>
          <RadioRowContainer>
            {
              answerItems.map(radio => (
                <RadioRowContainer key={radio.name}>
                  <AkRadio
                    key={radio.value}
                    name={radio.name}
                    value={radio.value}
                    isSelected={consentToBeContacted === radio.value}
                    onChange={this.onContactAnswerChange}
                  >
                    {radio.name}
                  </AkRadio>
                </RadioRowContainer>
              ))
            }
          </RadioRowContainer>
        </ModalSection>
      </ModalDialog>
    );
  }

  private onCancel = () => {
    this.props.dismissFeedback();
    this.setState({ consentToBeContacted: 'yes' });
  };

  private onContactAnswerChange = (event) => {
    this.setState({ consentToBeContacted: event.target.value });
  };

  private handleTextAreaChange = (e) => {
    this.setState({
      comment: e.target.value.trim(),
    });
  };

  private webInfo() {
    return (`
  Customer can be contacted: ${this.state.consentToBeContacted}
  Location: ${window.location.href}
  Browser Language: ${navigator.language}
  Referrer: ${document.referrer}
  User-Agent: ${navigator.userAgent}
  Screen Resolution: ${screen.width} x ${screen.height}
  Build Version: ${window.__build_version__}
    `);
  }

  private onSubmit = () => {
    const { formatMessage } = this.props.intl;

    if (this.props.mutate) {
      const email = (this.props.data && this.props.data.currentUser && this.props.data.currentUser.email) || '';
      this.props.mutate({
        variables: {
          comment: this.state.comment,
          email,
          webInfo: this.webInfo(),
        },
      }).catch(() => {
        // If there's an error here we're not handling it,
        // since we're optimistically showing a success flag
        // due to endpoint constraints @see ADMIN-561

        return;
      });
    }

    this.props.dismissFeedback();

    this.props.showFlag({
      icon: <Icon primaryColor="green" label="" />,
      id: 'chrome.feedback.success',
      title: formatMessage(messages.flagTitle),
      description: formatMessage(messages.flagDescription),
      autoDismiss: true,
    });
  };
}

const withSubmitFeedbackMutation = graphql<SubmitFeedbackMutation>(submitFeedbackMutation);

const query = graphql<{}, FeedbackDialogQuery>(feedbackDialogQuery);

const queryIsOpen = graphql<Props, FeedbackDialogIsOpenQuery, {}, Partial<Props>>(
  feedbackDialogIsOpenQuery,
  {
    props: ({ data }) => ({
      isOpen: data && data.ui && data.ui.isFeedbackOpen,
    }),
  });

const dismissFeedbackMutation = graphql<Props, DissmissFeedbackMutationMutation, {}, Partial<Props>>(
  dismissFeedbackMutationMutation,
  {
    props: ({ mutate }) => ({
      dismissFeedback: async () => mutate instanceof Function && mutate({}),
    }),
  },
);

export const FeedbackDialog = compose(
  queryIsOpen,
  dismissFeedbackMutation,
  withSubmitFeedbackMutation,
  query,
  withFlag,
  injectIntl,
)(FeedbackDialogImpl);
