import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { spy } from 'sinon';

import { NavigationLink, NavigationLinkIconGlyph } from 'common/navigation';

import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import { FeedbackNavigationGroupImpl } from './feedback-navigation-group';

describe('FeedbackNavigationGroup', () => {

  it('should call showFeedback if feedback link is clicked', () => {
    const showFeedback = spy();
    const wrapper = shallow(
      <FeedbackNavigationGroupImpl showFeedback={showFeedback} intl={createMockIntlProp()} />,
      createMockIntlContext(),
    );
    expect(showFeedback.callCount).to.equal(0);
    wrapper.findWhere(n => n.matchesElement(<NavigationLink icon={NavigationLinkIconGlyph.Feedback} />)).simulate('click');
    expect(showFeedback.callCount).to.equal(1);
  });
});
