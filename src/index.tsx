// Since we have side effects, the order of these imports matter.
// These must come before other imports.
// tslint:disable:origin-import-grouping
// tslint:disable:no-import-side-effect
import '@atlaskit/css-reset';
import './polyfills';
// tslint:enable:origin-import-grouping
// tslint:enable:no-import-side-effect

import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { reportPerformanceMetrics } from 'common/performance-metrics';

import { App } from './app';

(() => {
  // This is required because in local development, an iframe may load a relative url
  // (/admin/users?react=false&chrome=false) from within an iframe.
  // If we are serving the app through User Management, that will serve the original UM page (not the React application)
  // If we are serving the app directly, that will load the full React application again, ignoring the query parameters.
  // This causes an infinite loop of loading and reloading the React application. We can break this loop by checking
  // the presence of this query parameter, which is given when the iframe tries to load that version of the current page.
  // Same can happen on admin hub if the user navigates to "https://<Admin Hub>/admin" page
  if (window.location.search.indexOf('react=false') > -1) {
    document.body.innerHTML = '';

    return;
  }

  ReactDOM.render(
    <App />,
    document.getElementById('root'),
    reportPerformanceMetrics,
  );
})();
