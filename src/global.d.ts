// add all used node environment values here for type-checking
type NodeEnv = 'development' | 'test' | 'production' | never;
type DeployEnv = 'local' | 'ddev' | 'stg-east' | 'prod-east' | 'prod-west' | never;

interface Window {
  __build_version__: string;
  __env__: DeployEnv;
  __cdn_url__: string;
  /**
   * This variable gets set to a "auth-redirect-marker" if user has arrived from auth page. It is an indicator
   * that the user has undergone some login process during this very browsing session. The undergone login process
   * is represented by the actual marker value.
   */
  __auth_redirect__: string;
  perfMetrics: any; // added by https://github.com/GoogleChromeLabs/first-input-delay
  newrelic: any;
  __REDUX_DEVTOOLS_EXTENSION__?(...args: any[]): any;
}

declare var __NODE_ENV__: NodeEnv;
