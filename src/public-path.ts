declare var __webpack_public_path__: string;

if (process.env.NODE_ENV === 'production') {
  __webpack_public_path__ = window.__cdn_url__;
}
