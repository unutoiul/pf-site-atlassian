import { boolean } from '@storybook/addon-knobs';
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';

import AkPage from '@atlaskit/page';

import { createApolloClient } from '../../apollo-client';
import { MemberAccessQuery } from '../../schema/schema-types';
import { ListAtlassianServices } from './list-atlassian-services';

storiesOf('Organization|List products and services', module)
  .add('Default', () => {
    const mockMemberAccessData: MemberAccessQuery['member']['memberDetails']['memberAccess'] = {
      __typename: '',
      errors: [],
      sites: [
        {
          __typename: '',
          siteUrl: 'hamburger.atlassian.net',
          isSiteAdmin: true,
        },
        {
          __typename: '',
          siteUrl: 'hotdog.com',
          isSiteAdmin: false,
        },
        {
          __typename: '',
          siteUrl: 'empty.com',
          isSiteAdmin: false,
        },
        {
          __typename: '',
          siteUrl: 'apples.com',
          isSiteAdmin: true,
        },
      ],
      products: [],
    };

    const mockMemberAccessDataWithProducts: MemberAccessQuery['member']['memberDetails']['memberAccess'] = {
      __typename: '',
      errors: [],
      sites: [
        {
          __typename: '',
          siteUrl: 'hamburger.atlassian.net',
          isSiteAdmin: true,
        },
        {
          __typename: '',
          siteUrl: 'hotdog.com',
          isSiteAdmin: false,
        },
        {
          __typename: '',
          siteUrl: 'empty.com',
          isSiteAdmin: false,
        },
        {
          __typename: '',
          siteUrl: 'apples.com',
          isSiteAdmin: true,
        },
      ],
      products: [
        {
          __typename: '',
          siteUrl: 'hamburger.atlassian.net',
          productName: 'Confluence',
          productKey: 'confluence.ondemand',
        },
        {
          __typename: '',
          siteUrl: 'hamburger.atlassian.net',
          productName: 'Jira',
          productKey: 'jira-software.ondemand',
        },
        {
          __typename: '',
          siteUrl: 'hotdog.com',
          productName: 'My dog',
          productKey: 'hotdog',
        },
      ],
    };

    const client = createApolloClient();
    let mockData = mockMemberAccessData;
    const noSitesOrProducts = boolean('No sites / Products', false);
    const mockDataWithProducts = boolean('With products', false);

    if (mockDataWithProducts) {
      mockData = mockMemberAccessDataWithProducts;
    }
    if (noSitesOrProducts) {
      mockData = { __typename: '', sites: [], products: [], errors: [] };
    }

    return (
      <ApolloProvider client={client}>
        <IntlProvider locale="en">
          <AkPage>
            <ListAtlassianServices memberAccess={mockData} />
          </AkPage>
        </IntlProvider>
      </ApolloProvider>
    );

  });
