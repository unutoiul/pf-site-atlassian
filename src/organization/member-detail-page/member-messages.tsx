import { defineMessages } from 'react-intl';

export const defaultMessages = defineMessages({
  error: {
    id: 'organization.member.detail.error',
    defaultMessage: 'Cannot load user details',
  },
  organizationNotFound: {
    id: 'organization.member.detail.organization.not.found',
    defaultMessage: 'Organization cannot be found',
  },
  deactivatedState: {
    id: 'organization.member.detail.deactivated.state',
    defaultMessage: 'User is deactivated',
  },
  unverifiedEmailDomain: {
    id: 'organization.member.detail.unverified.email.domain',
    defaultMessage: 'You can only change the email address to one with a {domainMessage}.',
    description: 'A message that gets displayed when the admin tried to change the user\'s email to an unsupported domain. { domainMessage } will be replaced by a message with id = "organization.member.detail.verified.domain", which by default is "verified domain',
  },
  verifiedDomain: {
    id: 'organization.member.detail.verified.domain',
    defaultMessage: 'verified domain',
    description: 'This is used as an link to the domains page on Atlassian Access',
  },
  subtitle: {
    id: 'organization.member.detail.subtitle',
    defaultMessage: 'Account details',
    desciption: 'Subtitle of a page which displays account details',
  },
  description: {
    id: 'organization.member.detail.description',
    defaultMessage: `Changes will apply across all Atlassian account services. Changing the email domain of this user's email
      address will remove them from this organization, and Atlassian Access security policies will stop being applied.
      <a href="https://confluence.atlassian.com/x/rrX-Nw" target="_blank" rel="noopener noreferrer"> Learn more about managing users.</a>`,
  },
  submit: {
    id: 'organization.member.detail.submit',
    defaultMessage: 'Save changes',
  },
  notFound: {
    id: 'organization.member.detail.not.found',
    defaultMessage: 'Members cannot be found',
  },
  edit: {
    id: 'organization.member.detail.edit',
    defaultMessage: 'Edit user details',
  },
  actions: {
    id: 'organization.member.detail.actions',
    defaultMessage: 'Actions',
  },
  resetPassword: {
    id: 'organization.member.detail.reset.password',
    defaultMessage: 'Reset password',
  },
  activate: {
    id: 'organization.member.detail.activate',
    defaultMessage: 'Reactivate account',
  },
  deactivate: {
    id: 'organization.member.detail.deactivate',
    defaultMessage: 'Deactivate account',
  },
  delete: {
    id: 'organization.member.detail.delete',
    defaultMessage: 'Delete account',
  },
  exclude: {
    id: 'organization.member.detail.exclude',
    defaultMessage: 'Exclude from two-step verification',
  },
  enforce: {
    id: 'organization.member.detail.enforce',
    defaultMessage: 'Enforce two-step verification',
  },
  disableMfa: {
    id: 'organization.member.detail.disable.mfa',
    defaultMessage: 'Disable two-step verification',
  },
  memberAccessErrorTitle: {
    id: 'organization.member.detail.memberAccess.error.title',
    defaultMessage: 'No product information found',
  },
  duplicateEmailError: {
    id: 'organization.member.detail.member.error.duplicateEmailDescription',
    defaultMessage: 'Unable to update the user because the provided email is already taken.',
    description: 'Error message to show when trying to set a user email to an email that belongs to a different user',
  },
  cancelDelete: {
    id: 'organization.member.detail.canceldelete',
    defaultMessage: 'Cancel deletion',
    description: `Label for 'Cancel Delete' option in drop down on member detail page`,
  },
});

export const otherDetailsMessages = defineMessages({
  otherDetails: {
    id: 'organization.member.detail.other.other.details',
    defaultMessage: 'Other details',
  },
  mfaExempt: {
    id: 'organization.member.detail.other.mfa.exempt',
    defaultMessage: 'Excluded from two-step verification',
  },
});

export const labelMessages = defineMessages({
  displayName: {
    id: 'organization.member.detail.label.display.name',
    defaultMessage: 'Name',
  },
  emailAddress: {
    id: 'organization.member.detail.label.email.address',
    defaultMessage: 'Email address',
  },
  jobTitle: {
    id: 'organization.member.detail.label.job.title',
    defaultMessage: 'Job title',
  },
});

export const statusMessages = defineMessages({
  details: {
    id: 'organization.member.detail.status.details',
    defaultMessage: `You've updated this account's details.`,
  },
  enforceMfa: {
    id: 'organization.member.detail.status.enforced.tsv',
    defaultMessage: `You've enforced two-step verification.`,
  },
  excludeMfa: {
    id: 'organization.member.detail.status.exclude.tsv',
    defaultMessage: `You've excluded this account.`,
  },
  disabledMfa: {
    id: 'organization.member.detail.status.disabled.tsv',
    defaultMessage: `You've disabled two-step-verification.`,
  },
  resetPassword: {
    id: 'organization.member.detail.status.reset.password',
    defaultMessage: `You've reset this account's password.`,
  },
  deactivatedAccountTitle: {
    id: 'organization.member.detail.status.deactivated.account.title',
    defaultMessage: `You deactivated the account.`,
  },
  deactivatedAccountDescription: {
    id: 'organization.member.detail.status.deactivated.account.description',
    defaultMessage: `{memberName} no longer has access to Atlassian account services.`,
  },
  enableAccount: {
    id: 'organization.member.detail.status.enable.account',
    defaultMessage: `You reactivated the account.`,
  },
  deleteAccountTitle: {
    id: 'organization.member.detail.status.delete.account.title',
    defaultMessage: `You scheduled the account for deletion`,
    description: 'Title of success flag when admin deletes a member',
  },
  deleteAccount: {
    id: 'organization.member.detail.status.delete.account.description',
    defaultMessage: `{memberName} no longer has access to Atlassian account services. We'll notify them by email.`,
    description: 'Message to be shown on success flag when admin deletes a member',
  },
  deleteAccountFailed: {
    id: 'organization.member.detail.status.delete.account.failure',
    defaultMessage: `We weren’t able to delete {memberName}'s account. Please try again.`,
    description: 'Message to be shown on error flag when admin tries to delete a member',
  },
  cancelDeleteAccountTitle: {
    id: 'organization.member.detail.status.cancel.delete.account.title',
    defaultMessage: `You canceled the account deletion`,
    description: 'Title of success flag when admin cancels deletion of a member',
  },
  cancelDeleteAccount: {
    id: 'organization.member.detail.status.cancel.delete.account.description',
    defaultMessage: `{memberName} has access to Atlassian account services again. We'll let them know by email.`,
    description: 'Message to be shown on success flag when admin cancels the deletion of a member',
  },
  cancelDeleteAccountFailed: {
    id: 'organization.member.detail.status.cancel.delete.account.failure',
    defaultMessage: `We weren’t able to cancel the deletion of {memberName}'s account. Please try again.`,
    description: 'Message to be shown on error flag when admin tries to cancel the deletion of a member',
  },
  deactivateAccountFailed: {
    id: 'organization.member.detail.status.deactivate.account.failure',
    defaultMessage: `We weren’t able to deactivate {memberName}'s account. Please try again.`,
    description: 'Message to be shown on error flag when admin tries to deactivate a member',
  },
});
