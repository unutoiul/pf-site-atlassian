import { boolean } from '@storybook/addon-knobs';
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';

import AkPage from '@atlaskit/page';

import { createApolloClient } from '../../apollo-client';
import { MemberDetailsQuery } from '../../schema/schema-types';
import { createMockAnalyticsClient } from '../../utilities/testing';
import { MemberDisableModalImpl } from './member-disable-modal';

storiesOf('Organization|Member Detail Page', module)
  .add('Member Deactivation Modal', () => {

    const canDeactivateResultError: MemberDetailsQuery['member']['canDeactivateAccount'] = {
      __typename: '',
      errors: [
        {
          __typename: '',
          code: 'HELLO',
          message: 'Error 1',
          link: 'http://atlassian.com',
        },
        {
          __typename: '',
          code: 'HELLO2',
          message: 'Error 2',
          link: 'http://atlassian.com',
        },
      ],
      warnings: [],
    };

    const canDeactivateResultWarnings: MemberDetailsQuery['member']['canDeactivateAccount'] = {
      __typename: '',
      warnings: [
        {
          __typename: '',
          code: 'HELLO',
          message: 'Warning 1',
          link: 'http://atlassian.com',
        },
        {
          __typename: '',
          code: 'HELLO2',
          message: 'Warning 2',
          link: 'http://atlassian.com',
        },
      ],
      errors: [],
    };

    const canDeactivateResultEmpty: MemberDetailsQuery['member']['canDeactivateAccount'] = {
      __typename: '',
      warnings: [],
      errors: [],
    };

    const client = createApolloClient();
    let mockData = canDeactivateResultEmpty;

    const hasErrors = boolean('errors', false);
    const haswarnings = boolean('warnings', false);

    const fakeMember = {
      displayName: 'Fake Member',
      orgId: 'FAKE-ORG-1',
      memberId: 'FAKE-MEMBER-1',
    };

    const fakeProducts = [
      'product-1',
      'product-2',
    ];

    if (haswarnings) {
      mockData = canDeactivateResultWarnings;
    }

    if (hasErrors) {
      mockData = canDeactivateResultError;
    }

    const toggleDialog = () => {
      // do nothing
    };

    return (
      <ApolloProvider client={client}>
        <IntlProvider locale="en">
          <AkPage>
            <MemberDisableModalImpl
              isOpen={true}
              isDisabling={false}
              displayName={fakeMember.displayName}
              orgId={fakeMember.orgId}
              memberId={fakeMember.memberId}
              canDeactivateResult={mockData}
              disableMember={toggleDialog}
              toggleDialog={toggleDialog}
              analyticsClient={createMockAnalyticsClient()}
              products={fakeProducts}
            />
          </AkPage>
        </IntlProvider>
      </ApolloProvider>
    );

  });
