import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import { DynamicTableStateless as AkDynamicTableStateless } from '@atlaskit/dynamic-table';

import { MemberDetailsQuery } from '../../schema/schema-types';
import { createMockIntlContext } from '../../utilities/testing';
import { MemberApiTokenTable } from './member-api-token-table';

const mockMemberTokenData: MemberDetailsQuery['member']['memberTokens'] = [
  {
    __typename: 'memberToken',
    id: 'api-token-id-1',
    label: 'Alex\'s Api-token 1',
    createdAt: '2019-01-03T00:11:46.608Z',
    lastAccess: '2019-01-05T00:10:46.768Z',
  },
  {
    __typename: 'memberToken',
    id: 'api-token-id-2',
    label: 'Alex\'s Api-token 2',
    createdAt: '2019-01-03T00:11:46.608Z',
    lastAccess: '',
  },
];
describe('Member API Token Table', () => {
  function shallowMemberTokenTable({ mockData = mockMemberTokenData } = {}) {
    return shallow(
      <MemberApiTokenTable
        tokens={mockData}
        hasIdentity={true}
        orgId="foo"
      />,
      createMockIntlContext(),
    );
  }

  it('should render tokens', () => {
    const wrapper = shallowMemberTokenTable();

    const table = wrapper.find(AkDynamicTableStateless);
    expect(table.exists()).to.equal(true);
    expect(table.props().rows).to.have.length(2);
  });
});
