import * as React from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';

import AkButton from '@atlaskit/button';

import { ModalDialog } from 'common/modal';

import {
  AnalyticsClientProps,
  closeReactivateDialogButton,
  confirmReactivateDialogButton,
  reactivateMemberModalScreenEvent,
  ScreenEventSender,
  withAnalyticsClient,
} from 'common/analytics';

import { MemberAccessQuery } from '../../schema/schema-types';

import { ListAtlassianServices } from './list-atlassian-services';

interface ReactivateMemberModalProps {
  isOpen: boolean;
  displayName: string;
  orgId: string;
  memberId: string;
  memberAccess: MemberAccessQuery['member']['memberDetails']['memberAccess'];
  reactivateMember(): void;
  hideDialog(): void;
}

const messages = defineMessages({
  title: {
    id: 'organization.member.reactivation.modal.title',
    description: 'Dialog title for Reactivate user dialog',
    defaultMessage: 'Reactivate account?',
  },
  reactivateUserModalText: {
    id: 'organization.member.reactivation.modal.description',
    description: 'Main description text for dialog body',
    defaultMessage: '{memberName} will regain access to Atlassian account services. They\'ll have access to the same sites and products they had before their account was deactivated, including:',
  },
  reactivateUserModalTextNoSites: {
    id: 'organization.member.reactivation.description.nosites',
    description: 'Main description text for dialog body when there are no sites or services',
    defaultMessage: '{memberName} will regain access to Atlassian account services again and site admins will be able to give them access to sites and products.',
  },
  reactivateUserModalTextBilling: {
    id: 'organization.member.reactivation.modal.description.billing',
    description: 'Billing warning at the bottom of the dialog',
    defaultMessage: `We'll start billing you for this user again.`,
  },
  cancel: {
    id: 'organization.member.reactivation.modal.cancel',
    description: 'Label for Cancel reactivation button',
    defaultMessage: 'Cancel',
  },
  reactivateAccount: {
    id: 'organization.member.reactivation.modal.account',
    description: 'Label for Reactivate account button',
    defaultMessage: 'Reactivate account',
  },
});

export class ReactivateMemberModalImpl extends React.PureComponent<ReactivateMemberModalProps & AnalyticsClientProps> {
  public render() {
    const { isOpen, hideDialog, memberAccess, displayName } = this.props;
    const hasAtlassianServices = memberAccess && memberAccess.sites.length > 0;
    const description = hasAtlassianServices ? messages.reactivateUserModalText : messages.reactivateUserModalTextNoSites;
    const reactivateMemberModalScreenEventData = reactivateMemberModalScreenEvent();

    return (
        <ModalDialog
          width="small"
          header={<FormattedMessage {...messages.title} />}
          isOpen={isOpen}
          onClose={hideDialog}
          footer={this.modalDialogFooter()}
        >
          <ScreenEventSender event={reactivateMemberModalScreenEventData}>
            <FormattedMessage
              tagName="p"
              {...description}
              values={{
                memberName: (displayName),
              }}
            />

            <ListAtlassianServices memberAccess={memberAccess} />

            <FormattedMessage
              tagName="p"
              {...messages.reactivateUserModalTextBilling}
            />
          </ScreenEventSender>
        </ModalDialog>
    );
  }

  private reactivateWithAnalytics = () => {
    this.props.reactivateMember();
    this.props.analyticsClient.sendUIEvent({
      data: confirmReactivateDialogButton({
        orgId: this.props.orgId,
        memberId: this.props.memberId,
      }),
    });
  }

  private hideDialogWithAnalytics = () => {
    this.props.hideDialog();

    this.props.analyticsClient.sendUIEvent({
      data: closeReactivateDialogButton({
        orgId: this.props.orgId,
        memberId: this.props.memberId,
      }),
    });
  }

  private modalDialogFooter = () => {
    return (
      <div>
        <AkButton
          onClick={this.hideDialogWithAnalytics}
          appearance="subtle-link"
        >
          <FormattedMessage {...messages.cancel} />
        </AkButton>
        <AkButton
          onClick={this.reactivateWithAnalytics}
          appearance="primary"
          id="reactivateAccountModal"
        >
          <FormattedMessage {...messages.reactivateAccount} />
        </AkButton>
      </div>
    );
  }
}

export const ReactivateMemberModal = withAnalyticsClient(
  ReactivateMemberModalImpl,
);
