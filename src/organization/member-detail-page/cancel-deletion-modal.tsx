import * as React from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';

import AkButton from '@atlaskit/button';

import { ButtonGroup } from 'common/button-group';
import { ModalDialog } from 'common/modal';

import {
  AnalyticsClientProps,
  cancelMemberDeletionModalScreenEvent,
  closeCancelDeletionDialogButton,
  confirmCancelDeletionDialogButton,
  ScreenEventSender,
  withAnalyticsClient,
} from 'common/analytics';

import { MemberAccessQuery } from '../../schema/schema-types';

import { ListAtlassianServices } from './list-atlassian-services';

interface CancelDeletionModalProps {
  isOpen: boolean;
  displayName: string;
  orgId: string;
  memberId: string;
  memberAccess: MemberAccessQuery['member']['memberDetails']['memberAccess'];
  cancelDeletion(): void;
  hideDialog(): void;
}

const messages = defineMessages({
  title: {
    id: 'organization.member.deletion.cancel.modal.title',
    description: 'Dialog title for cancel delete dialog',
    defaultMessage: 'Cancel deletion?',
  },
  cancelDeletionModalText: {
    id: 'organization.member.deletion.cancel.modal.description',
    description: 'Main description text for dialog body',
    defaultMessage: '{memberName} will have access to their Atlassian account services again:',
  },
  cancelDeletionModalTextNoSites: {
    id: 'organization.member.deletion.cancel.modal.description.nosites',
    description: 'Main description text for dialog body when there are no sites or services',
    defaultMessage: '{memberName} will have access to Atlassian account services again and site admins will be able to give them access to sites and products.',
  },
  cancelDeletionModalTextBilling: {
    id: 'organization.member.deletion.cancel.modal.description.billing',
    description: 'Billing warning at the bottom of the dialog',
    defaultMessage: `We'll start billing you for this user again.`,
  },
  cancelDeletion: {
    id: 'organization.member.deletion.cancel.modal.canceldeletion',
    description: 'Label for Cancel deletion button',
    defaultMessage: 'Cancel deletion',
  },
  cancel: {
    id: 'organization.member.deletion.cancel.modal.cancel',
    description: 'Label for Cancel button that closes the dialog without doing anything',
    defaultMessage: 'Never mind',
  },
});

export class CancelDeletionModalImpl extends React.PureComponent<CancelDeletionModalProps & AnalyticsClientProps> {
  public render() {
    const { isOpen, hideDialog, memberAccess, displayName } = this.props;
    const hasAtlassianServices = memberAccess && memberAccess.sites.length > 0;
    const description = hasAtlassianServices ? messages.cancelDeletionModalText : messages.cancelDeletionModalTextNoSites;
    const cancelMemberDeletionModalScreenEventData = cancelMemberDeletionModalScreenEvent();

    return (
        <ModalDialog
          width="small"
          header={<FormattedMessage {...messages.title} />}
          isOpen={isOpen}
          onClose={hideDialog}
          footer={this.modalDialogFooter()}
        >
          <ScreenEventSender event={cancelMemberDeletionModalScreenEventData}>
            <FormattedMessage
              tagName="p"
              {...description}
              values={{
                memberName: (displayName),
              }}
            />

            <ListAtlassianServices memberAccess={memberAccess} />

            <FormattedMessage
              tagName="p"
              {...messages.cancelDeletionModalTextBilling}
            />
          </ScreenEventSender>
        </ModalDialog>
    );
  }

  private cancelDeletionWithAnalytics = () => {
    this.props.cancelDeletion();
    this.props.analyticsClient.sendUIEvent({
      data: confirmCancelDeletionDialogButton({
        orgId: this.props.orgId,
        memberId: this.props.memberId,
      }),
    });
  }

  private hideDialogWithAnalytics = () => {
    this.props.hideDialog();

    this.props.analyticsClient.sendUIEvent({
      data: closeCancelDeletionDialogButton({
        orgId: this.props.orgId,
        memberId: this.props.memberId,
      }),
    });
  }

  private modalDialogFooter = () => {
    return (
      <ButtonGroup alignment="right">
        <AkButton
          onClick={this.hideDialogWithAnalytics}
          appearance="subtle-link"
        >
          <FormattedMessage {...messages.cancel} />
        </AkButton>
        <AkButton
          onClick={this.cancelDeletionWithAnalytics}
          appearance="primary"
        >
          <FormattedMessage {...messages.cancelDeletion} />
        </AkButton>
      </ButtonGroup>
    );
  }
}

export const CancelDeletionModal = withAnalyticsClient(
  CancelDeletionModalImpl,
);
