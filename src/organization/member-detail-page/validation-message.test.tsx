import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import { Error, Success, ValidationMessage } from './validation-message';

describe('Validation Message', () => {
  describe('when hasValidationError flag is a falsey value', () => {
    it('does not display any text', () => {
      const text = 'hello world';
      const message = <div className="child">{text}</div>;
      const wrapper = shallow(
        <ValidationMessage invalidMessage={message} hasValidationError={undefined as any}/>,
      );

      expect(wrapper.find('.child').length).to.equal(0);
    });
  });

  describe('when hasValidationError flag is true', () => {
    it('displays an invalid message', () => {
      const text = 'hello world';
      const message = <div className="child">{text}</div>;
      const wrapper = shallow(
        <ValidationMessage invalidMessage={message} hasValidationError={true}/>,
      );
      expect(wrapper.find('.child').length).to.equal(1);
      expect(wrapper.find(Error).exists()).to.equal(true);
    });

    it('displays the correct invalidMessage', () => {
      const text = 'hello world';
      const message = <div className="child">{text}</div>;
      const wrapper = shallow(
        <ValidationMessage invalidMessage={message} hasValidationError={true} />,
      );

      const child = wrapper.find('.child');
      expect(child.length).to.equal(1);
      expect(wrapper.find(Error).exists()).to.equal(true);
      expect(child.props().children).to.contain('hello world');
    });

    it('displays an invalid message when both invalid and valid messages are supplied', () => {
      const invalidText = 'hello world';
      const message = <div className="invalid-child">{invalidText}</div>;
      const validText = 'goodbye world';
      const messageTwo = <div className="valid-child">{validText}</div>;

      const wrapper = shallow(
        <ValidationMessage invalidMessage={message} validMessage={messageTwo} hasValidationError={true} />,
      );

      expect(wrapper.find('div').length).to.equal(1);
      expect(wrapper.find(Error).exists()).to.equal(true);
      expect(wrapper.find('div').props().children).to.contain('hello world');
    });
  });

  describe('when hasValidationError flag is false', () => {
    it('displays a valid message', () => {
      const text = 'hello world';
      const validMessage = <div className="child">{text}</div>;
      const invalidText = 'goodbye world';
      const invalidMessage = <div className="child">{invalidText}</div>;
      const wrapper = shallow(
        <ValidationMessage invalidMessage={invalidMessage} validMessage={validMessage} hasValidationError={false} />,
      );
      expect(wrapper.find('.child').length).to.equal(1);
      expect(wrapper.find(Success).exists()).to.equal(true);
    });

    it('displays no message if one is not provided', () => {
      const text = 'hello world';
      const message = <div className="child">{text}</div>;
      const wrapper = shallow(
        <ValidationMessage invalidMessage={message} hasValidationError={false} />,
      );
      expect(wrapper.find('div').length).to.equal(0);
      expect(wrapper.find(Success).exists()).to.equal(false);
    });

    it('displays the correct validMessage', () => {
      const text = 'hello world';
      const validMessage = <div className="child">{text}</div>;
      const invalidText = 'goodbye world';
      const invalidMessage = <div className="child">{invalidText}</div>;
      const wrapper = shallow(
        <ValidationMessage invalidMessage={invalidMessage} validMessage={validMessage} hasValidationError={false} />,
      );

      const child = wrapper.find('.child');
      expect(child.length).to.equal(1);
      expect(child.props().children).to.contain('hello world');
      expect(wrapper.find(Success).exists()).to.equal(true);
    });

    it('displays a valid message when both invalid and valid messages are supplied, but hasValidationError is false', () => {
      const invalidText = 'hello world';
      const message = <div className="invalid-child">{invalidText}</div>;
      const validText = 'goodbye world';
      const messageTwo = <div className="valid-child">{validText}</div>;

      const wrapper = shallow(
        <ValidationMessage invalidMessage={message} validMessage={messageTwo} hasValidationError={false} />,
      );

      expect(wrapper.find('div').length).to.equal(1);
      expect(wrapper.find(Success).exists()).to.equal(true);
      expect(wrapper.find('div').props().children).to.contain('goodbye world');
    });
  });
});
