import { boolean } from '@storybook/addon-knobs';
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';

import AkPage from '@atlaskit/page';

import { createApolloClient } from '../../apollo-client';
import { MemberAccessQuery } from '../../schema/schema-types';
import { ProductAccessTable } from './product-access-table';

storiesOf('Organization|Product Access Table', module)
  .add('Default', () => {
    const mockMemberAccessData: MemberAccessQuery['member']['memberDetails']['memberAccess'] = {
      __typename: 'MemberAccess',
      errors: [],
      sites: [],
      products: [
        {
          __typename: 'MemberSiteAccess',
          productName: 'Bitbucket',
          productKey: 'bitbucket.cloud',
          siteUrl: 'hamburger.atlassian.net',
        },
        {
          __typename: 'MemberSiteAccess',
          productName: 'Confluence',
          productKey: 'confluence.ondemand',
          siteUrl: 'hamburger.atlassian.net',
        },
        {
          __typename: 'MemberSiteAccess',
          productName: 'Confluence',
          productKey: 'confluence.ondemand',
          siteUrl: 'acme.atlassian.net',
        },
        {
          __typename: 'MemberSiteAccess',
          productName: 'Confluence',
          productKey: 'confluence.ondemand',
          siteUrl: 'acme-support.atlassian.net',
        },
        {
          __typename: 'MemberSiteAccess',
          productName: 'Confluence',
          productKey: 'confluence.ondemand',
          siteUrl: 'vmware.atlassian.net',
        },
        {
          __typename: 'MemberSiteAccess',
          productName: 'Confluence',
          productKey: 'confluence.ondemand',
          siteUrl: 'cisco.atlassian.net',
        },
        {
          __typename: 'MemberSiteAccess',
          productName: 'JIRA Core',
          productKey: 'jira-core.ondemand',
          siteUrl: 'hamburger.atlassian.net',
        },
        {
          __typename: 'MemberSiteAccess',
          productName: 'JIRA Service Desk',
          productKey: 'jira-incident-manager.ondemand',
          siteUrl: 'hamburger.atlassian.net',
        },
        {
          __typename: 'MemberSiteAccess',
          productName: 'JIRA Software',
          productKey: 'jira-core.ondemand',
          siteUrl: 'hamburger.atlassian.net',
        },
        {
          __typename: 'MemberSiteAccess',
          productName: 'Stride',
          productKey: 'hipchat.cloud',
          siteUrl: 'hamburger.atlassian.net',
        },
        {
          __typename: 'MemberSiteAccess',
          productName: 'Stride',
          productKey: 'hipchat.cloud',
          siteUrl: 'hotdog.com',
        },
        {
          __typename: 'MemberSiteAccess',
          productName: 'Opsgenie',
          productKey: 'opsgenie',
          siteUrl: 'glutenfreepizza.com',
        },
      ],
    };

    const client = createApolloClient();
    const isTableEmpty = boolean('Empty', false);
    let mockData: MemberAccessQuery['member']['memberDetails']['memberAccess'] = mockMemberAccessData;

    if (isTableEmpty) {
      mockData = null!;
    }

    return (
      <ApolloProvider client={client}>
        <IntlProvider locale="en">
            <AkPage>
              <ProductAccessTable
                memberAccess={mockData}
              />
            </AkPage>
        </IntlProvider>
      </ApolloProvider>
    );
  })
;
