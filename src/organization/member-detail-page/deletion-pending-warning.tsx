import * as React from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';
import styled from 'styled-components';

import AkAvatar from '@atlaskit/avatar';
import AkButton from '@atlaskit/button';
import AkErrorIcon from '@atlaskit/icon/glyph/error';
import {
  borderRadius as AkBorderRadius,
  colors as akColors,
  gridSize as akGridSize,
} from '@atlaskit/theme';

import { links } from 'common/link-constants';

const messages = defineMessages({
  learnMore: {
    id: 'organization.member.deletion.learn.more',
    description: 'Action button text to learn more about member deletion',
    defaultMessage: 'Learn more',
  },

  formerUser: {
    id: 'organization.member.deletion.former.user',
    description: 'What we call a user who has been deleted, and no longer has a name',
    defaultMessage: 'Former user',
  },

  heading: {
    id: 'organization.member.deletion.heading',
    description: 'Action button text to cancel deletion process for an account',
    defaultMessage: 'This account will be permanently deleted',
  },

  description: {
    id: 'organization.member.deletion.description',
    description: '',
    defaultMessage: `You won't be able to cancel the deletion and restore the account after {deleteDate}. {memberFullName} no longer has access to Atlassian account services. Their deleted account will appear to other users as shown below:`,
  },
});

const WarningWrapper = styled.div`
  background: ${akColors.R50};
  border-radius: ${AkBorderRadius}px;
  display: flex;
  padding: ${akGridSize() * 2}px;
  flex: 0 0 auto;
`;

const WarningIconContainer = styled.div`
  width: ${akGridSize() * 5}px;
  flex: 0 0 auto;
`;

const WarningContent = styled.div`
  padding-top: ${akGridSize() * 0.5}px;
  flex: 1 0;
`;

export const AvatarWrapper = styled.div`
  display: flex;
  align-items: center;
  margin-top: ${akGridSize}px;
  margin-bottom: ${akGridSize}px;
`;

export const Mention = styled.strong`
  font-weight: 500;
  margin: ${akGridSize}px;
`;

export const WarningActions = styled.div`
  margin-left: -${akGridSize() * 1.5}px;
`;

interface AllProps {
  deleteDate?: string;
  memberFullName: string;
  deletedBy?: string;
}

export class DeletionPendingWarning extends React.Component<AllProps> {

  public render() {
    const { deleteDate, memberFullName } = this.props;

    return (
      <WarningWrapper>
        <WarningIconContainer>
          <AkErrorIcon size="medium" primaryColor={akColors.R400} label="" />
        </WarningIconContainer>
        <WarningContent>
          <FormattedMessage
            {...messages.heading}
            tagName="h5"
          />
          <FormattedMessage
            {...messages.description}
            values={{
              deleteDate: deleteDate || '',
              memberFullName,
            }}
            tagName="p"
          />

          <AvatarWrapper>
            <AkAvatar src={null} borderColor={akColors.R50} />
            <Mention><FormattedMessage {...messages.formerUser} /></Mention>
          </AvatarWrapper>

          <WarningActions>
            <AkButton appearance="link" href={links.external.rightToBeForgotten} target={'_blank'}>
              <FormattedMessage {...messages.learnMore} />
            </AkButton>
          </WarningActions>

        </WarningContent>
      </WarningWrapper>
    );
  }
}
