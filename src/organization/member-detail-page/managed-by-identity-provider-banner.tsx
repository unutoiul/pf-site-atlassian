import * as React from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';

import AkSectionMessage from '@atlaskit/section-message';

import { MemberProvisioningSource } from '../../schema/schema-types';

const messages = defineMessages({
  heading: {
    id: 'member-details.idp-banner.heading',
    defaultMessage: 'Account is managed by your identity provider',
    description: 'Lets the admin know that account management is not under their full control inside Atlassian Access.',
  },
  bodyScim: {
    id: 'member-details.idp-banner.scim.body',
    defaultMessage: 'Manage the email address and profile details in your identity provider.',
    description: 'Lets the admin know that some settings are managed outside of Atlassian Access',
  },
  bodyGsync: {
    id: 'member-details.idp-banner.gsync.body',
    defaultMessage: 'Manage the profile details and security policies in Google admin.',
    description: 'Lets the admin know that some settings are managed outside of Atlassian Access',
  },
});

export const ManagedByIdentityProviderBanner = ({ idpType }: { idpType: MemberProvisioningSource }) => (
  <AkSectionMessage
    appearance="info"
    title={<FormattedMessage {...messages.heading} />}
  >
    <FormattedMessage {...(idpType === 'SCIM' ? messages.bodyScim : messages.bodyGsync)} />
  </AkSectionMessage>
);
