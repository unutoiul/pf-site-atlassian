import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import * as sinon from 'sinon';

import AkButton from '@atlaskit/button';

import { ModalDialog } from 'common/modal';

import { ScreenEventSender } from 'common/analytics';

import { MemberDeletionModalImpl } from './member-deletion-modal';

describe('MemberDeletionModal', () => {

  const fakeMember = {
    displayName: 'Fake Member',
    orgId: 'FAKE-ORG-1',
    memberId: 'FAKE-MEMBER-1',
  };

  const canCloseResultError = {
    errors: [
      {
        code: 'HELLO',
        message: 'Thing',
        link: 'http://atlassian.com',
      },
      {
        code: 'HELLO2',
        message: 'Thing2',
      },
    ],
    warnings: [],
  };

  const canCloseResultWarnings = {
    warnings: [
      {
        code: 'HELLO',
        message: 'Thing',
        link: 'http://atlassian.com',
      },
      {
        code: 'HELLO2',
        message: 'Thing2',
      },
    ],
    errors: [],
  };

  const fakeProducts = [
    'product-1',
    'product-2',
  ];

  const sandbox = sinon.sandbox.create();
  let modal;
  let sendUIEventSpy;
  let noop;
  let closeSpy;
  let deleteSpy;

  beforeEach(() => {
    closeSpy = sandbox.spy();
    deleteSpy = sandbox.spy();
    noop = sandbox.spy();
    sendUIEventSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  function shallowMemberDeletionModal(isOpen, canCloseResult) {
    return shallow(
      <MemberDeletionModalImpl
        isOpen={isOpen}
        hideDialog={closeSpy}
        confirmDelete={deleteSpy}
        canCloseResult={canCloseResult}
        displayName={fakeMember.displayName}
        orgId={fakeMember.orgId}
        memberId={fakeMember.memberId}
        gracePeriod={14}
        analyticsClient={{ init: noop, sendScreenEvent: noop, sendUIEvent: sendUIEventSpy, sendTrackEvent: noop }}
        products={fakeProducts}
      /> ,
    );
  }

  describe('when canClose has errors', () => {

    beforeEach(() => {
      modal = shallowMemberDeletionModal(true, canCloseResultError).find(ModalDialog);
    });

    it('error dialog is opened', () => {
      expect(modal.props().isOpen).to.equal(true);
    });

    it('renders title for error dialog', () => {
      const title = modal.props().header as React.ReactElement<any>;
      expect(title.type).to.equal(FormattedMessage);
      expect(title.props.defaultMessage).to.equal(`You can't delete this account`);
    });

    it('renders the error message', () => {
      const body = modal.find(FormattedMessage);
      expect(body).to.have.lengthOf(1);
      expect(body.props().defaultMessage).to.contain(`The account for {displayName} can't be deleted because:`);

      const errorList = modal.find('li');
      expect(errorList).to.have.lengthOf(2);

      expect(errorList.at(0).text()).to.equals('Thing');
      expect(errorList.at(1).text()).to.equals('Thing2');
    });

    it('renders ok button', () => {
      const okButton = shallow(modal.props().footer).find(AkButton);

      expect(okButton).to.have.lengthOf(1);
      expect(okButton.find(FormattedMessage).props().defaultMessage).to.deep.equal('OK');
      expect(okButton.props().appearance).to.equals('primary');
    });

    it('sends correct screen event to analytics', () => {
      const screenEventSender = modal.find(ScreenEventSender);

      expect(screenEventSender).to.be.lengthOf(1);
      expect(screenEventSender.props().event).to.deep.equal({
        orgId: fakeMember.orgId,
        data: {
          attributes: {
            memberId: fakeMember.memberId,
            orgId: fakeMember.orgId,
          },
          name: 'memberDeletionSPIErrorsScreen',
        },
      });
    });

    it('closes the dialog when OK is pressed', () => {
      const okButton = shallow(modal.props().footer).find(AkButton);

      expect(closeSpy.callCount).to.equal(0);

      okButton.simulate('click');
      expect(closeSpy.callCount).to.equal(1);
    });

  });

  describe('when canClose has warnings but no errors', () => {
    beforeEach(() => {
      modal = shallowMemberDeletionModal(true, canCloseResultWarnings).find(ModalDialog);
    });

    it('renders title for error dialog', () => {
      const title = modal.props().header as React.ReactElement<any>;
      expect(title.type).to.equal('span');
    });

    it('renders the warnings message', () => {
      const body = modal.find(FormattedMessage);

      expect(body).to.have.lengthOf(5);
      expect(body.at(0).props().defaultMessage).to.contain(`{memberName} will`);
      expect(body.at(1).props().defaultMessage).to.contain(`immediately lose access`);
      expect(body.at(2).props().defaultMessage).to.contain(`to all Atlassian account services.`);
      expect(body.at(3).props().defaultMessage).to.contain(`Before you delete their account, you should know that:`);
      expect(body.at(4).props().defaultMessage).to.contain(`We'll deactivate their account for {gracePeriod} days, during which you can cancel the deletion.`);

      const warningsList = modal.find('li');
      expect(warningsList).to.have.lengthOf(2);

      expect(warningsList.at(0).text()).to.equals('Thing');
      expect(warningsList.at(1).text()).to.equals('Thing2');
    });

    it('renders cancel button', () => {
      const buttons = shallow(modal.props().footer).find(AkButton);

      expect(buttons).to.have.lengthOf(2);

      const cancelButton = buttons.at(0);
      expect(cancelButton.find(FormattedMessage).props().defaultMessage).to.deep.equal('Cancel');
      expect(cancelButton.props().appearance).to.equals('default');

      const deleteButton = buttons.at(1);
      expect(deleteButton.find(FormattedMessage).props().defaultMessage).to.deep.equal('Delete account');
      expect(deleteButton.props().appearance).to.equals('danger');
    });

    it('sends correct screen event to analytics', () => {
      const screenEventSender = modal.find(ScreenEventSender);

      expect(screenEventSender).to.be.lengthOf(1);
      expect(screenEventSender.props().event).to.deep.equal({
        orgId: fakeMember.orgId,
        data: {
          attributes: {
            memberId: fakeMember.memberId,
            orgId: fakeMember.orgId,
          },
          name: 'memberDeletionSPIModal',
        },
      });
    });

    it('invokes hideDialog() when cancel is pressed', () => {
      const cancelButton = shallow(modal.props().footer).find(AkButton).at(0);

      expect(sendUIEventSpy.callCount).to.equal(0);

      cancelButton.simulate('click');
      expect(closeSpy.callCount).to.equal(1);

      expect(sendUIEventSpy.callCount).to.equal(1);
      expect(sendUIEventSpy.lastCall.args[0]).to.deep.equal({
        data: {
          action: 'clicked',
          actionSubject: 'button',
          actionSubjectId: 'closeMemberDeletion',
          attributes: {
            memberId: fakeMember.memberId,
            orgId: fakeMember.orgId,
          },
          source: 'memberDeletionSPIModal',
        },
      });
    });

    it('invokes confirmDelete() when delete button is pressed', () => {
      const deleteButton = shallow(modal.props().footer).find(AkButton).at(1);

      expect(sendUIEventSpy.callCount).to.equal(0);

      deleteButton.simulate('click');
      expect(deleteSpy.callCount).to.equal(1);

      expect(sendUIEventSpy.callCount).to.equal(1);
      expect(sendUIEventSpy.lastCall.args[0]).to.deep.equal({
        data: {
          action: 'clicked',
          actionSubject: 'button',
          actionSubjectId: 'confirmMemberDeletion',
          attributes: {
            memberId: fakeMember.memberId,
            orgId: fakeMember.orgId,
            products: fakeProducts,
          },
          source: 'memberDeletionSPIModal',
        },
      });
    });

  });

  describe('when there are no warnings or errors', () => {
    beforeEach(() => {
      modal = shallowMemberDeletionModal(true, { errors: [], warnings: [] }).find(ModalDialog);
    });

    it('renders title for error dialog', () => {
      const title = modal.props().header as React.ReactElement<any>;
      expect(title.type).to.equal('span');
    });

    it('renders the warnings message', () => {
      const body = modal.find(FormattedMessage);
      expect(body).to.have.lengthOf(4);
      expect(body.at(0).props().defaultMessage).to.contain(`{memberName} will`);
      expect(body.at(1).props().defaultMessage).to.contain(`immediately lose access`);
      expect(body.at(2).props().defaultMessage).to.contain(`to all Atlassian account services.`);
      expect(body.at(3).props().defaultMessage).to.contain(`We'll deactivate their account for {gracePeriod} days, during which you can cancel the deletion.`);

      const warningsList = modal.find('li');
      expect(warningsList).to.have.lengthOf(0);
    });

    it('renders cancel button', () => {
      const buttons = shallow(modal.props().footer).find(AkButton);

      expect(buttons).to.have.lengthOf(2);

      const cancelButton = buttons.at(0);
      expect(cancelButton.find(FormattedMessage).props().defaultMessage).to.deep.equal('Cancel');
      expect(cancelButton.props().appearance).to.equals('default');

      const deleteButton = buttons.at(1);
      expect(deleteButton.find(FormattedMessage).props().defaultMessage).to.deep.equal('Delete account');
      expect(deleteButton.props().appearance).to.equals('danger');
    });
  });

});
