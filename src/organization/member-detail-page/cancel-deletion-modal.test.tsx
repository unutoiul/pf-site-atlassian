import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import * as sinon from 'sinon';

import AkButton from '@atlaskit/button';

import { ScreenEventSender } from 'common/analytics';
import { ModalDialog } from 'common/modal';

import { CancelDeletionModalImpl } from './cancel-deletion-modal';
import { ListAtlassianServices } from './list-atlassian-services';

describe('CancelDeletionModal', () => {

  let cancelDeletionSpy;
  let cancelDialogSpy;

  const defaultMemberAccess = {
    products: [
      {
        productName: 'Product A',
        siteUrl: 'site1',
      },
      {
        productName: 'Product B',
        siteUrl: 'site1',
      },
    ],
    sites: [
      {
        siteUrl: 'site1',
        isSiteAdmin: false,
      },
    ],
  };

  const fakeMember = {
    displayName: 'Fake Member',
    orgId: 'FAKE-ORG-1',
    memberId: 'FAKE-MEMBER-1',
  };

  const sandbox = sinon.sandbox.create();

  let sendUIEventSpy;
  let noop;

  beforeEach(() => {
    cancelDeletionSpy = sandbox.spy();
    cancelDialogSpy = sandbox.spy();
    noop = sandbox.spy();
    sendUIEventSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  function shallowCancelDeletionModal(isOpen, memberAccess) {
    return shallow(
      <CancelDeletionModalImpl
        isOpen={isOpen}
        cancelDeletion={cancelDeletionSpy}
        hideDialog={cancelDialogSpy}
        displayName={fakeMember.displayName}
        orgId={fakeMember.orgId}
        memberId={fakeMember.memberId}
        memberAccess={memberAccess}
        analyticsClient={{ init: noop, sendScreenEvent: noop, sendUIEvent: sendUIEventSpy, sendTrackEvent: noop }}
      />,
    );
  }

  it('should send analytics screen track event', () => {
    const wrapper = shallowCancelDeletionModal(true, defaultMemberAccess);

    expect(wrapper.find(ScreenEventSender).prop('event')).to.deep.equal({
      data: {
        name: 'cancelMemberDeletionModal',
      },
    });
  });

  it('should contain correct header and footer', () => {
    const wrapper = shallowCancelDeletionModal(false, defaultMemberAccess);
    const modal = wrapper.find(ModalDialog);

    expect(modal.props().isOpen).to.equal(false);

    const title = modal.props().header as React.ReactElement<any>;
    expect(title.type).to.equal(FormattedMessage);
    expect(title.props.defaultMessage).to.equal('Cancel deletion?');

    const cancelButton = shallow(modal.props().footer as React.ReactElement<any>).find(AkButton).first();
    expect(cancelButton.find(FormattedMessage).props().defaultMessage).to.deep.equal('Never mind');

    const cancelDeletionButton = shallow(modal.props().footer as React.ReactElement<any>).find(AkButton).last();
    expect(cancelDeletionButton.find(FormattedMessage).props().defaultMessage).to.equal('Cancel deletion');
    expect(cancelDeletionButton.props().appearance).to.deep.equal('primary');

  });

  it('should contain correct description', () => {
    const wrapper = shallowCancelDeletionModal(true, defaultMemberAccess);
    const messages = wrapper.find(FormattedMessage);
    expect(messages).to.have.lengthOf(2);
    expect(messages.at(0).props().defaultMessage).to.contain('{memberName} will have access to their Atlassian account services again:');
    expect(messages.at(0).props().values).to.contain({ memberName: (fakeMember.displayName) });
  });

  it('should contain different description when user does not have access to any site', () => {
    const wrapper = shallowCancelDeletionModal(true, null);
    const messages = wrapper.find(FormattedMessage);

    expect(messages).to.have.lengthOf(2);
    expect(messages.at(0).props().defaultMessage).to.contain('{memberName} will have access to Atlassian account services again and site admins will be able to give them access to sites and products.');
    expect(messages.at(0).props().values).to.contain({ memberName: (fakeMember.displayName) });
  });

  it('should contain correct billing warning', () => {
    const wrapper = shallowCancelDeletionModal(true, defaultMemberAccess);
    const messages = wrapper.find(FormattedMessage);
    expect(messages).to.have.lengthOf(2);
    expect(messages.at(1).props().defaultMessage).to.contain('We\'ll start billing you for this user again.');
  });

  it('should contain the list of sites', () => {
    const wrapper = shallowCancelDeletionModal(true, defaultMemberAccess);
    expect(wrapper.find(ListAtlassianServices)).to.have.lengthOf(1);
    expect(wrapper.find(ListAtlassianServices).at(0).props().memberAccess).to.be.equals(defaultMemberAccess);
  });

  describe('button actions', () => {

    it('clicking cancel deletion button should trigger cancelDeletion', () => {
      const wrapper = shallowCancelDeletionModal(true, defaultMemberAccess);
      const button = shallow(wrapper.props().footer).find(AkButton).last();

      expect(cancelDeletionSpy.callCount).to.equal(0);

      button.simulate('click');
      expect(cancelDeletionSpy.callCount).to.equal(1);

      expect(sendUIEventSpy.callCount).to.equal(1);
      expect(sendUIEventSpy.lastCall.args[0]).to.deep.equal({
        data: {
          action: 'clicked',
          actionSubject: 'button',
          actionSubjectId: 'confirmCancelDeletion',
          attributes: {
            memberId: fakeMember.memberId,
            orgId: fakeMember.orgId,
          },
          source: 'cancelMemberDeletionModal',
        },
      });
    });

    it('clicking on the cancel button should trigger cancel action', () => {
      const wrapper = shallowCancelDeletionModal(true, defaultMemberAccess);
      const button = shallow(wrapper.props().footer).find(AkButton).first();

      expect(cancelDialogSpy.callCount).to.equal(0);
      button.simulate('click');
      expect(cancelDialogSpy.callCount).to.equal(1);

      expect(sendUIEventSpy.callCount).to.equal(1);
      expect(sendUIEventSpy.lastCall.args[0]).to.deep.equal({
        data: {
          action: 'clicked',
          actionSubject: 'button',
          actionSubjectId: 'closeCancelDeletion',
          attributes: {
            memberId: fakeMember.memberId,
            orgId: fakeMember.orgId,
          },
          source: 'cancelMemberDeletionModal',
        },
      });
    });
  });
});
