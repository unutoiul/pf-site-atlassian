import * as moment from 'moment';
import * as React from 'react';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import AkButton from '@atlaskit/button';
import AkWarningIcon from '@atlaskit/icon/glyph/warning';
import {
  borderRadius as AkBorderRadius,
  colors as akColors,
  gridSize as akGridSize,
} from '@atlaskit/theme';

import { links } from 'common/link-constants';

import { DeactivatedInfo, MemberStatus } from '../../schema/schema-types';

const messages = defineMessages({
  learnMore: {
    id: 'organization.member.deactivated.learn.more',
    description: 'Action button text to learn more about user deactivation',
    defaultMessage: 'Learn more',
  },

  heading: {
    id: 'organization.member.deactivated.heading',
    description: 'Banner header for deactivated account',
    defaultMessage: 'This account is deactivated',
  },

  commonDescription: {
    id: 'organization.member.deactivated.description',
    description: 'Banner info for deactivated account',
    defaultMessage: `{memberFullName} no longer has access to Atlassian account services.`,
  },

  fullDescription: {
    id: 'organization.member.deactivated.full.description',
    description: 'Banner info for deactivated account',
    defaultMessage: `{memberFullName} no longer has access to Atlassian account services. Their account was deactivated on {deactivatedDate}.`,
  },

  exportControlsDescription: {
    id: 'organization.member.deactivated.by.export.controls.description',
    description: 'Banner info for account deactivated by export controls',
    defaultMessage: `{memberFullName} no longer has access to Atlassian account services. Their account was deactivated by Atlassian.`,
  },
});

const WarningWrapper = styled.div`
  background: ${akColors.Y50};
  border-radius: ${AkBorderRadius}px;
  display: flex;
  padding: ${akGridSize() * 2}px;
  flex: 0 0 auto;
  margin-bottom: ${akGridSize() * 5}px;
`;

const WarningIconContainer = styled.div`
  width: ${akGridSize() * 5}px;
  flex: 0 0 auto;
`;

const WarningContent = styled.div`
  padding-top: ${akGridSize() * 0.5}px;
  flex: 1 0;
`;

export const WarningActions = styled.div`
  margin-left: -${akGridSize() * 1.5}px;
`;

interface InactiveMemberWarningProps {
  memberFullName: string;
  active: MemberStatus;
  deactivatedInfo?: DeactivatedInfo | null;
}

type AllProps = InactiveMemberWarningProps & InjectedIntlProps;

export class InactiveMemberWarningImpl extends React.Component<AllProps> {

  public render() {
    const { memberFullName, active, deactivatedInfo } = this.props;
    const learnMoreLink = active === 'BLOCKED' ? '' : links.external.accountDeactivation;
    const deactivatedDate =
      deactivatedInfo ?
        this.props.intl.formatDate(moment(deactivatedInfo.deactivatedOn).toDate(), { day: '2-digit', month: 'long', year: 'numeric' }) :
        '';

    return (
      <WarningWrapper>
        <WarningIconContainer>
          <AkWarningIcon size="medium" primaryColor={akColors.Y400} label="" />
        </WarningIconContainer>
        <WarningContent>
          <FormattedMessage
            {...messages.heading}
            tagName="h5"
          />
          {active === 'DISABLED' && deactivatedInfo &&
            <FormattedMessage
              {...messages.fullDescription}
              values={{
                memberFullName,
                deactivatedDate,
              }}
              tagName="p"
            />
          }
          {active === 'DISABLED' && !deactivatedInfo &&
            <FormattedMessage
              {...messages.commonDescription}
              values={{
                memberFullName,
              }}
              tagName="p"
            />
          }
          {active === 'BLOCKED' &&
            <FormattedMessage
              {...messages.exportControlsDescription}
              values={{
                memberFullName,
              }}
              tagName="p"
            />
          }

          {active === 'DISABLED' &&
            <WarningActions>
              <AkButton appearance="link" href={learnMoreLink} target={'_blank'}>
                <FormattedMessage {...messages.learnMore} />
              </AkButton>
            </WarningActions>
          }

        </WarningContent>
      </WarningWrapper>
    );
  }
}

export const InactiveMemberWarning = injectIntl(InactiveMemberWarningImpl);
