import { boolean } from '@storybook/addon-knobs';
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';
import { MemberDetailsCanCloseQuery } from 'src/schema/schema-types';

import AkPage from '@atlaskit/page';

import { createApolloClient } from '../../apollo-client';
import { createMockAnalyticsClient } from '../../utilities/testing';
import { MemberDeletionModalImpl } from './member-deletion-modal';

storiesOf('Organization|Member Detail Page', module)
  .add('Member Deletion Modal', () => {

    const canCloseResultError: MemberDetailsCanCloseQuery['member']['canCloseAccount'] = {
      __typename: '',
      errors: [
        {
          __typename: '',
          code: 'HELLO',
          message: 'Error 1',
          link: 'http://atlassian.com',
        },
        {
          __typename: '',
          code: 'HELLO2',
          message: 'Error 2',
          link: 'http://atlassian.com',
        },
      ],
      warnings: [],
    };

    const canCloseResultWarnings: MemberDetailsCanCloseQuery['member']['canCloseAccount'] = {
      __typename: '',
      warnings: [
        {
          __typename: '',
          code: 'HELLO',
          message: 'Warning 1',
          link: 'http://atlassian.com',
        },
        {
          __typename: '',
          code: 'HELLO2',
          message: 'Warning 2',
          link: 'http://atlassian.com',
        },
      ],
      errors: [],
    };

    const canCloseResultEmpty: MemberDetailsCanCloseQuery['member']['canCloseAccount'] = {
      __typename: '',
      warnings: [],
      errors: [],
    };

    const client = createApolloClient();
    let mockData = canCloseResultEmpty;

    const hasErrors = boolean('errors', false);
    const haswarnings = boolean('warnings', false);

    const fakeMember = {
      displayName: 'Fake Member',
      orgId: 'FAKE-ORG-1',
      memberId: 'FAKE-MEMBER-1',
    };

    const fakeProducts = [
      'product-1',
      'product-2',
    ];

    if (haswarnings) {
      mockData = canCloseResultWarnings;
    }

    if (hasErrors) {
      mockData = canCloseResultError;
    }

    const hideDialog = () => {
      // do nothing
    };

    return (
      <ApolloProvider client={client}>
        <IntlProvider locale="en">
          <AkPage>
            <MemberDeletionModalImpl
              confirmDelete={hideDialog}
              canCloseResult={mockData}
              displayName={fakeMember.displayName}
              orgId={fakeMember.orgId}
              memberId={fakeMember.memberId}
              isOpen={true}
              gracePeriod={14}
              products={fakeProducts}
              hideDialog={hideDialog}
              analyticsClient={createMockAnalyticsClient()}
            />
          </AkPage>
        </IntlProvider>
      </ApolloProvider>
    );

  });
