import { boolean } from '@storybook/addon-knobs';
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';

import AkPage from '@atlaskit/page';

import { createApolloClient } from '../../apollo-client';
import { MemberAccessQuery } from '../../schema/schema-types';
import { SiteAdminTable } from './site-admin-table';

storiesOf('Organization|Site Admin Table', module)
  .add('Default', () => {
    const mockMemberAccessData: MemberAccessQuery['member']['memberDetails']['memberAccess'] = {
      errors: [],
      sites: [
        {
          __typename: 'MemberSiteAccess',
          siteUrl: 'hamburger.atlassian.net',
          isSiteAdmin: true,
        },
        {
          __typename: 'MemberSiteAccess',
          siteUrl: 'hotdog.com',
          isSiteAdmin: false,
        },
        {
          __typename: 'MemberSiteAccess',
          siteUrl: 'empty.com',
          isSiteAdmin: false,
        },
        {
          __typename: 'MemberSiteAccess',
          siteUrl: 'apples.com',
          isSiteAdmin: true,
        },
      ],
      products: [],
      __typename: 'MemberAccess',
    };

    const client = createApolloClient();
    const isTableEmpty = boolean('Empty', false);
    let mockData: MemberAccessQuery['member']['memberDetails']['memberAccess'] | null = mockMemberAccessData;

    if (isTableEmpty) {
      mockData = null;
    }

    return (
      <ApolloProvider client={client}>
        <IntlProvider locale="en">
            <AkPage>
              <SiteAdminTable
                memberAccess={mockData}
              />
            </AkPage>
        </IntlProvider>
      </ApolloProvider>
    );
  })
;
