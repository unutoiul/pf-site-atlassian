// tslint:disable jsx-use-translation-function

import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import AkAvatar from '@atlaskit/avatar';
import AkButton from '@atlaskit/button';
import AkErrorIcon from '@atlaskit/icon/glyph/error';

import { links } from 'common/link-constants';

import { AvatarWrapper, DeletionPendingWarning, WarningActions } from './deletion-pending-warning';

describe('Deletion Pending Warning', () => {
  const defaultProps = {
    deleteDate: 'deleteDate string',
    memberFullName: 'Full Name',
    deletedBy: 'Deleted ByName',
  };
  const shallowRender = (props = {}) => shallow(
    <DeletionPendingWarning
        {...defaultProps}
        {...props}
    />,
  );

  it('renders an error icon', () => {
    const wrapper = shallowRender();
    expect(wrapper.find(AkErrorIcon).length).to.equal(1);
  });

  it('renders correct heading', () => {
    const wrapper = shallowRender();
    const heading = wrapper.find('FormattedMessage').at(0);
    expect(heading.props()).to.deep.include({
      tagName: 'h5',
      id: 'organization.member.deletion.heading',
    });
  });

  it('renders example of avatar', () => {
    const wrapper = shallowRender();
    const avatarWrapper = wrapper.find(AvatarWrapper);
    expect(avatarWrapper.find(AkAvatar).length).to.equal(1);
    expect(avatarWrapper.find('FormattedMessage').at(0).props()).to.deep.include({
      id: 'organization.member.deletion.former.user',
    });
  });

  describe('action buttons', () => {
    it('renders link to learn more', () => {
      const wrapper = shallowRender();
      const learnMoreButton = wrapper.find(WarningActions).find(AkButton);
      expect(learnMoreButton.props()).to.contain({
        appearance: 'link',
        href: links.external.rightToBeForgotten,
        target: '_blank',
      });
      expect(learnMoreButton.children().at(0).prop('id')).to.equal('organization.member.deletion.learn.more');
    });
  });

});
