import * as React from 'react';
import styled from 'styled-components';

import { gridSize as akGridSize } from '@atlaskit/theme';

import { MemberAccessQuery } from '../../schema/schema-types';

interface ListAtlassianServicesProps {
  memberAccess: MemberAccessQuery['member']['memberDetails']['memberAccess'];
}

interface SitesAndProducts {
  [site: string]: string[];
}

const gridSizeTimes = factor => akGridSize() * factor;

export const ListItems = styled.ul`
  list-style: none;
  padding-left: ${gridSizeTimes(2)}px;
`;

export class ListAtlassianServices extends React.PureComponent<ListAtlassianServicesProps> {

  public render() {

    const { memberAccess } = this.props;

    if (!memberAccess || memberAccess.sites.length === 0) {
      return null;
    }
    const sitesServices = this.getSitesAndProducts(memberAccess);

    return (
      <ListItems>
        {Object.keys(sitesServices).map((site) => (
          <li key={site}>
            {site}
            <br />
            <small>{sitesServices[site].join(', ')}</small>
          </li>
        ))}
      </ListItems>
    );
  }

  private getAllSites = (memberAccess: ListAtlassianServicesProps['memberAccess']): SitesAndProducts => {
    const sitesAndProducts = {};

    memberAccess!.sites.forEach((site) => {
      sitesAndProducts[site.siteUrl] = [];
    });

    return sitesAndProducts;
  }

  private getSitesAndProducts = (memberAccess: ListAtlassianServicesProps['memberAccess']): SitesAndProducts => {
    const sitesAndProducts = this.getAllSites(memberAccess);

    memberAccess!.products.forEach((product) => {
      if (!sitesAndProducts[product.siteUrl]) {
        sitesAndProducts[product.siteUrl] = [];
      }
      if (product.productName) {
        sitesAndProducts[product.siteUrl].push(product.productName);
      }
    });

    return sitesAndProducts;
  };

}
