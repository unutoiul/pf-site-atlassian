import * as React from 'react';
import { defineMessages, FormattedDate, FormattedMessage } from 'react-intl';
import styled from 'styled-components';

import AKButton from '@atlaskit/button';
import { DynamicTableStateless as AkDynamicTableStateless, Row as AkRow } from '@atlaskit/dynamic-table';
import { gridSize as akGridSize } from '@atlaskit/theme';

import { EmptyTableView } from 'common/table';

import { IdentityManagerLearnMoreButton } from 'common/identity-manager-learn-more-button';

import { atlassianAccessLearnMoreEventData, UIEvent } from 'common/analytics';

import { MemberDetailsQuery, MemberToken } from '../../schema/schema-types';

const messages = defineMessages({
  title: {
    id: 'organization.member.detail.memberApiTokenTable.title',
    defaultMessage: 'API Tokens',
    description: 'Tokens that a member has.',
  },
  name: {
    id: 'organization.member.detail.memberApiTokenTable.name',
    defaultMessage: 'Name',
    description: 'The name of the token taken from the label property of the token.',
  },
  lastAccess: {
    id: 'organization.member.detail.memberApiTokenTable.lastAccess',
    defaultMessage: 'Last accessed',
    description: 'The date of the last time the token was used.',
  },
  createdAt: {
    id: 'organization.member.detail.memberApiTokenTable.createdAt',
    defaultMessage: 'Created on',
    description: 'The date the token was created.',
  },
  signUp: {
    id: 'organization.member.detail.memberApiTokenTable.signUp',
    defaultMessage: 'Sign Up for Atlassian Access to revoke tokens and gain company-wide visibility, security, and control over your product. ',
    description: 'The message that is displayed when an Admin user has not purchased the Access product.',
  },
  revoke: {
    id: 'organization.member.detail.memberApiTokenTable.revoke',
    defaultMessage: 'Revoke',
    description: 'When you want to remove a token from a member so they can not use it any more.',
  },
  empty: {
    id: 'organization.member.detail.memberApiTokenTable.empty',
    defaultMessage: 'No API tokens',
    description: 'The empty state message.',
  },
});

const Title = styled.h2`
  margin-top: ${akGridSize() * 6}px;
  margin-bottom: ${akGridSize() * 2}px;
`;

const AlignTop = styled.div`
  padding-top: ${akGridSize() * 2}px;
  td {
    vertical-align: top;
    padding-top: ${akGridSize}px;
    padding-bottom: ${akGridSize}px;
  }
`;

interface MemberApiTokenProps {
  tokens: MemberDetailsQuery['member']['memberTokens'];
  hasIdentity: boolean;
  orgId: string;
}

export class MemberApiTokenTable extends React.Component<MemberApiTokenProps> {
  public render() {
    return (
      <React.Fragment>
        <Title><FormattedMessage {...messages.title} /></Title>
        {this.getContent()}
      </React.Fragment>
    );
  }

  private getContent() {
    const { tokens, hasIdentity } = this.props;

    return (
      <React.Fragment>
        {this.showSignUp(hasIdentity)}
        <AlignTop>
          <AkDynamicTableStateless
            isLoading={!tokens}
            head={this.getHead(hasIdentity)}
            rows={this.getRows(tokens, hasIdentity)}
            emptyView={<EmptyTableView><FormattedMessage {...messages.empty} /></EmptyTableView>}
          />
        </AlignTop>
      </React.Fragment>
    );
  }

  private getHead = (hasIdentity: boolean) => {
    const cells: { cells: Array<{ key: string, content: any }> } = {
      cells: [
        {
          key: 'name',
          content: <FormattedMessage {...messages.name} />,
        },
        {
          key: 'lastAccessed',
          content: <FormattedMessage {...messages.lastAccess} />,
        },
        {
          key: 'createdAt',
          content: <FormattedMessage {...messages.createdAt} />,
        },
      ],
    };

    if (hasIdentity) {
      cells.cells.push({
        key: 'revoke',
        content: '',
      });
    }

    return cells;
  }

  private getRows = (tokens: MemberApiTokenProps['tokens'], hasIdentity: boolean): AkRow[] => {
    const rows: AkRow[] = [];
    if (tokens) {
      tokens.forEach((token: MemberToken) => {
        const row = {
          key: token.id,
          cells: [
            {
              key: 'name',
              content: token.label,
            },
            {
              key: 'lastAccess',
              content: token.lastAccess ? <FormattedDate value={token.lastAccess}/> : null,
            },
            {
              key: 'createdAt',
              content: <FormattedDate value={token.createdAt}/>,
            },
          ],
        };
        if (hasIdentity) {
          // TODO: make revoke a button and have it do something
          row.cells.push({
            key: 'revoke',
            content: (
              <AKButton
                appearance="link"
                spacing="none"
                target="_blank"
              >
                <FormattedMessage {...messages.revoke}/>
              </AKButton>
            ),
          });
        }
        rows.push(row);
      });
    }

    return rows;
  }

  private showSignUp = (hasIdentity: boolean) => {

    const event: UIEvent = {
      orgId: this.props.orgId,
      data: atlassianAccessLearnMoreEventData('memberDetailsApiTokensScreen'),
    };

    return !hasIdentity ?
      (
        <React.Fragment>
          <FormattedMessage {...messages.signUp}/>
          <IdentityManagerLearnMoreButton analyticsUIEvent={event} isPrimary={false} isLink={true}/>
        </React.Fragment>
      ) : null;
  }
}
