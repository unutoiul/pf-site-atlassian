import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import * as sinon from 'sinon';

import AkButton from '@atlaskit/button';

import { Button as AnalyticsButton, ScreenEventSender } from 'common/analytics';
import { ModalDialog } from 'common/modal';

import { MemberDisableModalImpl } from './member-disable-modal';

describe('MemberDisableModal', () => {

  const fakeMember = {
    displayName: 'Trash Turkey',
    orgId: 'FAKE-ORG-1',
    memberId: 'FAKE-MEMBER-1',
  };

  const fakeProducts = [
    'product-1',
    'product-2',
  ];

  const canDeactivateResultError = {
    errors: [
      {
        code: 'HELLO',
        message: 'Error1',
        link: 'http://atlassian.com',
      },
      {
        code: 'Error2',
        message: 'Error2',
      },
    ],
    warnings: [],
  };

  const canDeactivateResultWarnings = {
    warnings: [
      {
        code: 'HELLO',
        message: 'Warning1',
        link: 'http://atlassian.com',
      },
      {
        code: 'HELLO2',
        message: 'Warning2',
      },
    ],
    errors: [],
  };

  const canDeactivateResult = {
    warnings: [],
    errors: [],
  };

  let disableMemberSpy;
  let toggleDialogSpy;
  let sendUIEventSpy;
  let noop;
  const sandbox = sinon.sandbox.create();

  beforeEach(() => {
    disableMemberSpy = sandbox.spy();
    toggleDialogSpy = sandbox.spy();
    noop = sandbox.spy();
    sendUIEventSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  function shallowMemberDisableModal(isOpen, isDisabling, canDeactivate) {
    return shallow(
      <MemberDisableModalImpl
        isOpen={isOpen}
        isDisabling={isDisabling}
        displayName={fakeMember.displayName}
        orgId={fakeMember.orgId}
        memberId={fakeMember.memberId}
        canDeactivateResult={canDeactivate}
        disableMember={disableMemberSpy}
        toggleDialog={toggleDialogSpy}
        analyticsClient={{ init: noop, sendScreenEvent: noop, sendUIEvent: sendUIEventSpy, sendTrackEvent: noop }}
        products={fakeProducts}
      />,
    );
  }

  function validateTitle(modal, titleText) {
    const title = shallow(modal.props().header).find(FormattedMessage);
    expect(title).to.have.length(1);
    expect(title.props().defaultMessage).to.deep.equal(titleText);
  }

  function validateCancelButton(modal) {
    const cancelButton = shallow(modal.props().footer as React.ReactElement<any>).find(AnalyticsButton).first();
    expect(cancelButton).to.have.length(1);
    expect(cancelButton.find(FormattedMessage).props().defaultMessage).to.deep.equal('Cancel');
  }

  function validateDisableButton(modal) {
    const disableButton = shallow(modal.props().footer as React.ReactElement<any>).find(AnalyticsButton).last();
    expect(disableButton).to.have.length(1);
    expect(disableButton.find(FormattedMessage).props().defaultMessage).to.deep.equal('Deactivate account');
    expect(disableButton.props().appearance).to.deep.equal('warning');
    expect(disableButton.props().isLoading).to.equal(false);
  }

  function validateModalContents(modal) {
    const body = modal.find(FormattedMessage);
    expect(body.at(0).props().defaultMessage).to.deep.equals('{displayName} will {boldedText} to all Atlassian account services.');
    expect(body.at(0).props().values.displayName).to.equals('Trash Turkey');

    const boldText = body.at(0).props().values.boldedText as FormattedMessage;
    expect(boldText.props.defaultMessage).to.deep.equals('immediately lose access');
  }

  function validateWarningMessage(modal) {
    const body = modal.find(FormattedMessage);
    expect(body.at(1).props().defaultMessage).to.deep.equals('Before you deactivate their account, you should know that:');
  }
  function validateList(modal, expectedMessages) {
    const list = modal.find('li');
    expect(list).to.have.lengthOf(expectedMessages.length);

    expectedMessages.forEach((message, index) => {
      expect(list.at(index).text()).to.equals(message);
    });
  }

  it('should contain correct content', () => {
    const wrapper = shallowMemberDisableModal(false, false, canDeactivateResult);
    const modal = wrapper.find(ModalDialog);

    expect(modal.props().isOpen).to.equal(false);

    validateTitle(modal, 'Deactivate account?');

    validateCancelButton(modal);

    validateDisableButton(modal);

    validateModalContents(modal);

  });

  it('displays the warning message', () => {
    const wrapper = shallowMemberDisableModal(false, false, canDeactivateResultWarnings);
    const modal = wrapper.find(ModalDialog);

    validateTitle(modal, 'Deactivate account?');

    validateCancelButton(modal);

    validateDisableButton(modal);

    validateModalContents(modal);

    validateWarningMessage(modal);

    validateList(modal, ['Warning1', 'Warning2']);
  });

  it('renders the errors correctly', () => {
    const wrapper = shallowMemberDisableModal(false, false, canDeactivateResultError);
    const modal = wrapper.find(ModalDialog);

    validateTitle(modal, `You can't deactivate this account`);

    const body = modal.find(FormattedMessage);
    expect(body.props().defaultMessage).to.deep.equals(`The account for {displayName} can't be deactivated because:`);
    expect(body.props().values!.displayName).to.equals('Trash Turkey');

    validateList(modal, ['Error1', 'Error2']);

    const okButton = shallow(modal.props().footer as React.ReactElement<any>).find(AkButton);
    expect(okButton).to.have.length(1);
    expect(okButton.find(FormattedMessage).props().defaultMessage).to.deep.equal('OK');
    expect(okButton.props().appearance).to.equals('primary');
  });

  it('sends error screen event to analytics', () => {
    const wrapper = shallowMemberDisableModal(false, false, canDeactivateResultError);
    const modal = wrapper.find(ModalDialog);
    const screenEventSender = modal.find(ScreenEventSender);

    expect(screenEventSender).to.be.lengthOf(1);
    expect(screenEventSender.props().event).to.deep.equal({
      orgId: fakeMember.orgId,
      data: {
        attributes: {
          memberId: fakeMember.memberId,
          orgId: fakeMember.orgId,
        },
        name: 'memberDeactivationSPIErrorModal',
      },
    });
  });

  it('sends correct screen event to analytics', () => {
    const wrapper = shallowMemberDisableModal(false, false, canDeactivateResult);
    const modal = wrapper.find(ModalDialog);
    const screenEventSender = modal.find(ScreenEventSender);

    expect(screenEventSender).to.be.lengthOf(1);
    expect(screenEventSender.props().event).to.deep.equal({
      orgId: fakeMember.orgId,
      data: {
        attributes: {
          memberId: fakeMember.memberId,
          orgId: fakeMember.orgId,
        },
        name: 'memberDeactivationSPIModal',
      },
    });
  });

  describe('button actions', () => {

    it('when sending a request disable a user the button should be disabled ', () => {
      const wrapper = shallowMemberDisableModal(true, true, canDeactivateResult);
      const modal = wrapper.find(ModalDialog);
      const button = shallow(modal.props().footer as React.ReactElement<any>).find(AnalyticsButton).last();
      expect(button.props().isLoading).to.equal(true);
    });

    it('clicking disable account button should trigger disableMember', () => {
      const wrapper = shallowMemberDisableModal(true, false, canDeactivateResult);
      const modal = wrapper.find(ModalDialog);
      const button = shallow(modal.props().footer as React.ReactElement<any>).find(AnalyticsButton).last();
      expect(disableMemberSpy.callCount).to.equal(0);
      button.simulate('click');
      expect(disableMemberSpy.callCount).to.equal(1);

      expect(sendUIEventSpy.callCount).to.equal(1);
      expect(sendUIEventSpy.lastCall.args[0]).to.deep.equal({
        data: {
          action: 'clicked',
          actionSubject: 'button',
          actionSubjectId: 'confirmMemberDeactivation',
          attributes: {
            memberId: fakeMember.memberId,
            orgId: fakeMember.orgId,
            products: fakeProducts,
          },
          source: 'memberDeactivationSPIModal',
        },
      });
    });

    it('clicking on the cancel button should trigger cancel action', () => {
      const wrapper = shallowMemberDisableModal(true, false, canDeactivateResult);
      const modal = wrapper.find(ModalDialog);
      const button = shallow(modal.props().footer as React.ReactElement<any>).find(AnalyticsButton).first();
      expect(toggleDialogSpy.callCount).to.equal(0);
      button.simulate('click');
      expect(toggleDialogSpy.callCount).to.equal(1);

      expect(sendUIEventSpy.callCount).to.equal(1);
      expect(sendUIEventSpy.lastCall.args[0]).to.deep.equal({
        data: {
          action: 'clicked',
          actionSubject: 'button',
          actionSubjectId: 'closeMemberDeactivation',
          attributes: {
            memberId: fakeMember.memberId,
            orgId: fakeMember.orgId,
          },
          source: 'memberDeactivationSPIModal',
        },
      });
    });

    it('clicking on the OK button closes the dialog', () => {
      const wrapper = shallowMemberDisableModal(true, false, canDeactivateResult);
      const modal = wrapper.find(ModalDialog);
      const okButton = shallow(modal.props().footer as React.ReactElement<any>).find(AnalyticsButton).first();

      expect(toggleDialogSpy.callCount).to.equal(0);

      okButton.simulate('click');
      expect(toggleDialogSpy.callCount).to.equal(1);
    });
  });
});
