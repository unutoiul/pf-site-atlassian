import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import { createMockIntlContext } from '../../utilities/testing';
import { ListAtlassianServices, ListItems } from './list-atlassian-services';

describe('ListAtlassianServices', () => {

  const sandbox = sinon.sandbox.create();

  let wrapper: ShallowWrapper;

  const memberAccess = {
    products: [
      {
        productName: 'Product A',
        siteUrl: 'site1',
      },
      {
        productName: 'Product B',
        siteUrl: 'site1',
      },
      {
        productName: 'Product A',
        siteUrl: 'site2',
      },
    ],
    sites: [
      {
        siteUrl: 'site1',
        isSiteAdmin: false,
      },
      {
        siteUrl: 'site2',
        isSiteAdmin: false,
      },
      {
        siteUrl: 'site3',
        isSiteAdmin: false,
      },
    ],
  };

  const sitesAndProducts = {
    site1: ['Product A', 'Product B'],
    site2: ['Product A'],
    site3: [],
  };

  beforeEach(() => {
    wrapper = shallowListAtlassianServices(memberAccess);
  });

  afterEach(() => {
    sandbox.restore();
  });

  function shallowListAtlassianServices(memberServicesAccess) {
    return shallow(
      <ListAtlassianServices memberAccess={memberServicesAccess} />,
      createMockIntlContext(),
    );
  }

  it('renders unordered list', () => {
    expect(wrapper.find(ListItems)).to.have.lengthOf(1);
  });

  it('renders exactly three list items', () => {
    expect(wrapper.find(ListItems).children()).to.have.lengthOf(3);
  });

  it('renders the contents correctly', () => {
    const ul = wrapper.find(ListItems);

    Object.keys(sitesAndProducts).forEach((site, idx) => {
      const item = ul.childAt(idx);
      expect(item.key()).to.equals(site);
      expect(item.html()).to.contain(site);
      expect(item.find('br')).to.have.lengthOf(1);
      expect(item.find('small')).to.have.lengthOf(1);
      if (sitesAndProducts[site] !== []) {
        expect(item.find('small').text()).to.equals(sitesAndProducts[site].join(', '));
      } else {
        expect(item.find('small').text()).to.be.empty();
      }
    });
  });

  it('does not render anything when no sites or products are available', () => {
    wrapper = shallowListAtlassianServices({
      sites: [],
      products: [],
    });

    expect(wrapper).to.be.empty();
  });

  it('does not render anything when memberAccess is null', () => {
    wrapper = shallowListAtlassianServices(null);
    expect(wrapper).to.be.empty();
  });

});
