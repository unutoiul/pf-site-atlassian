import { action } from '@storybook/addon-actions';
import { boolean } from '@storybook/addon-knobs';
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';

import AkPage from '@atlaskit/page';

import { createApolloClient } from '../../apollo-client';
import { MemberAccessQuery } from '../../schema/schema-types';
import { createMockAnalyticsClient } from '../../utilities/testing';
import { ReactivateMemberModalImpl } from './reactivate-member-modal';

storiesOf('Organization|Member Detail Page', module)
  .add('Reactivate Modal', () => {
    const mockMemberAccessData: MemberAccessQuery['member']['memberDetails']['memberAccess'] = {
      __typename: '',
      errors: [],
      sites: [
        {
          __typename: '',
          siteUrl: 'hamburger.atlassian.net',
          isSiteAdmin: true,
        },
        {
          __typename: '',
          siteUrl: 'hotdog.com',
          isSiteAdmin: false,
        },
        {
          __typename: '',
          siteUrl: 'empty.com',
          isSiteAdmin: false,
        },
        {
          __typename: '',
          siteUrl: 'apples.com',
          isSiteAdmin: true,
        },
      ],
      products: [],
    };

    const mockMemberAccessDataWithProducts: MemberAccessQuery['member']['memberDetails']['memberAccess'] = {
      __typename: '',
      errors: [],
      sites: [
        {
          __typename: '',
          siteUrl: 'hamburger.atlassian.net',
          isSiteAdmin: true,
        },
        {
          __typename: '',
          siteUrl: 'hotdog.com',
          isSiteAdmin: false,
        },
        {
          __typename: '',
          siteUrl: 'empty.com',
          isSiteAdmin: false,
        },
        {
          __typename: '',
          siteUrl: 'apples.com',
          isSiteAdmin: true,
        },
      ],
      products: [
        {
          __typename: '',
          siteUrl: 'hamburger.atlassian.net',
          productName: 'Confluence',
          productKey: 'confluence.ondemand',
        },
        {
          __typename: '',
          siteUrl: 'hamburger.atlassian.net',
          productName: 'Jira',
          productKey: 'jira-core.ondemand',
        },
        {
          __typename: '',
          siteUrl: 'hotdog.com',
          productName: 'My dog',
          productKey: 'hipchat.cloud',
        },
      ],
    };

    const client = createApolloClient();
    let mockData = mockMemberAccessData;
    const noSitesOrProducts = boolean('No sites / Products', false);
    const mockDataWithProducts = boolean('With products', false);

    const fakeMember = {
      displayName: 'Fake Member',
      orgId: 'FAKE-ORG-1',
      memberId: 'FAKE-MEMBER-1',
    };

    if (mockDataWithProducts) {
      mockData = mockMemberAccessDataWithProducts;
    }
    if (noSitesOrProducts) {
      mockData = { __typename: '', sites: [], products: [], errors: [] };
    }

    const hideDialog = action('Hide modal');
    const reactivateMember = action('Reactivate member clicked');

    return (
      <ApolloProvider client={client}>
        <IntlProvider locale="en">
          <AkPage>
            <ReactivateMemberModalImpl
              displayName={fakeMember.displayName}
              orgId={fakeMember.orgId}
              memberId={fakeMember.memberId}
              isOpen={true}
              memberAccess={mockData}
              reactivateMember={reactivateMember}
              hideDialog={hideDialog}
              analyticsClient={createMockAnalyticsClient()}
            />
          </AkPage>
        </IntlProvider>
      </ApolloProvider>
    );

  });
