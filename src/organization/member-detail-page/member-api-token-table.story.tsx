import { boolean } from '@storybook/addon-knobs';
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';

import AkPage from '@atlaskit/page';

import { createApolloClient } from '../../apollo-client';
import { MemberDetailsQuery } from '../../schema/schema-types';
import { MemberApiTokenTable } from './member-api-token-table';

const mockMemberTokenData: MemberDetailsQuery['member']['memberTokens'] = [
  {
    __typename: 'memberToken',
    id: 'api-token-id-1',
    label: 'Alex\'s Api-token 1',
    createdAt: '2019-01-03T00:11:46.608Z',
    lastAccess: '2019-01-05T00:10:46.768Z',
  },
  {
    __typename: 'memberToken',
    id: 'api-token-id-2',
    label: 'Alex\'s Api-token 2',
    createdAt: '2019-01-03T00:11:46.608Z',
    lastAccess: '',
  },
];

storiesOf('Organization|Member Api Token Table', module)
  .add('Default', () => {

    const client = createApolloClient();
    const isTableEmpty = boolean('Empty', false);
    const hasAccess = boolean('Has not purchased Access', false);
    let mockData: MemberDetailsQuery['member']['memberTokens'] = mockMemberTokenData;

    if (isTableEmpty) {
      mockData = [];
    }

    return (
      <ApolloProvider client={client}>
        <IntlProvider locale="en">
          <AkPage>
            <MemberApiTokenTable
              hasIdentity={!hasAccess}
              tokens={mockData}
              orgId="foo"
            />
          </AkPage>
        </IntlProvider>
      </ApolloProvider>
    );
  });
