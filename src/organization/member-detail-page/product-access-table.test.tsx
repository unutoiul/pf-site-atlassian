import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import { DynamicTableStateless as AkDynamicTableStateless } from '@atlaskit/dynamic-table';

import { MemberAccessQuery } from '../../schema/schema-types';
import { createMockIntlContext } from '../../utilities/testing';
import { ProductAccessTable } from './product-access-table';

describe('Product Access Table', () => {

  const mockMemberAccessData: MemberAccessQuery['member']['memberDetails']['memberAccess'] = {
    __typename: 'MemberAccess',
    errors: [],
    sites: [],
    products: [
      {
        __typename: 'MemberSiteAccess',
        productName: 'Bitbucket',
        productKey: 'bitbucket.cloud',
        siteUrl: 'hamburger.atlassian.net',
      },
      {
        __typename: 'MemberSiteAccess',
        productName: 'Confluence',
        productKey: 'confluence.ondemand',
        siteUrl: 'hamburger.atlassian.net',
      },
      {
        __typename: 'MemberSiteAccess',
        productName: 'Confluence',
        productKey: 'confluence.ondemand',
        siteUrl: 'acme.atlassian.net',
      },
      {
        __typename: 'MemberSiteAccess',
        productName: 'Confluence',
        productKey: 'confluence.ondemand',
        siteUrl: 'acme-support.atlassian.net',
      },
      {
        __typename: 'MemberSiteAccess',
        productName: 'Confluence',
        productKey: 'confluence.ondemand',
        siteUrl: 'vmware.atlassian.net',
      },
      {
        __typename: 'MemberSiteAccess',
        productName: 'Confluence',
        productKey: 'confluence.ondemand',
        siteUrl: 'cisco.atlassian.net',
      },
      {
        __typename: 'MemberSiteAccess',
        productName: 'JIRA Core',
        productKey: 'jira-core.ondemand',
        siteUrl: 'hamburger.atlassian.net',
      },
      {
        __typename: 'MemberSiteAccess',
        productName: 'JIRA Service Desk',
        productKey: 'jira-incident-manager.ondemand',
        siteUrl: 'hamburger.atlassian.net',
      },
      {
        __typename: 'MemberSiteAccess',
        productName: 'JIRA Software',
        productKey: 'jira-core.ondemand',
        siteUrl: 'hamburger.atlassian.net',
      },
      {
        __typename: 'MemberSiteAccess',
        productName: 'Stride',
        productKey: 'hipchat.cloud',
        siteUrl: 'hamburger.atlassian.net',
      },
      {
        __typename: 'MemberSiteAccess',
        productName: 'Stride',
        productKey: 'hipchat.cloud',
        siteUrl: 'hotdog.com',
      },
    ],
  };

  function shallowProductAccessTable({ mockData = mockMemberAccessData } = {}) {
    return shallow(
      <ProductAccessTable
        memberAccess={mockData}
      />,
      createMockIntlContext(),
    );
  }

  it('should render products', () => {
    const wrapper = shallowProductAccessTable();

    const table = wrapper.find(AkDynamicTableStateless);
    expect(table.exists()).to.equal(true);
    expect(table.props().rows).to.have.length(6);
  });

  it('should render empty when memberAccess is null', () => {
    const wrapper = shallowProductAccessTable({ mockData: null! });

    const table = wrapper.find(AkDynamicTableStateless);
    expect(table.exists()).to.equal(true);
    expect(table.props().rows).to.have.length(0);
  });

  it('should render empty when products is null', () => {
    const mockMemberAccessNullProductsData: MemberAccessQuery['member']['memberDetails']['memberAccess'] = {
      errors: [],
      sites: [],
      products: null!,
      __typename: 'MemberAccess',
    };
    const wrapper = shallowProductAccessTable({ mockData: mockMemberAccessNullProductsData });

    const table = wrapper.find(AkDynamicTableStateless);
    expect(table.exists()).to.equal(true);
    expect(table.props().rows).to.have.length(0);
  });

  it('should render empty when products is empty', () => {
    const mockMemberAccessEmptyProductsData: MemberAccessQuery['member']['memberDetails']['memberAccess'] = {
      errors: [],
      sites: [],
      products: [],
      __typename: 'MemberAccess',
    };
    const wrapper = shallowProductAccessTable({ mockData: mockMemberAccessEmptyProductsData });

    const table = wrapper.find(AkDynamicTableStateless);
    expect(table.exists()).to.equal(true);
    expect(table.props().rows).to.have.length(0);
  });
});
