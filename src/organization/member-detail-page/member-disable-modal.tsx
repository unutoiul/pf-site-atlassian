import * as React from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';
import styled from 'styled-components';

import AkButton from '@atlaskit/button';
import { gridSize as akGridSize } from '@atlaskit/theme';

import {
  AnalyticsClientProps,
  Button as AnalyticsButton,
  closeMemberDeactivation,
  confirmMemberDeactivation,
  memberDeactivationErrorScreenEvent,
  memberDeactivationScreenEvent,
  ScreenEventSender,
  withAnalyticsClient,
} from 'common/analytics';
import { createErrorIcon } from 'common/error';
import { IconWrapper, ModalDialog, StyledModalDialog } from 'common/modal';

import { MemberDetailsQuery } from '../../schema/schema-types';

export interface MemberDisableModalProps {
  isOpen: boolean;
  isDisabling: boolean;
  displayName: string;
  orgId: string;
  memberId: string;
  products: string[];
  canDeactivateResult: MemberDetailsQuery['member']['canDeactivateAccount'];
  disableMember(): void;
  toggleDialog(): void;
}

const messages = defineMessages({
  title: {
    id: 'organization.member.detail.deactivate.modal.title',
    defaultMessage: 'Deactivate account?',
  },
  titleErrors: {
    id: 'organization.member.detail.deactivate.modal.title.errors',
    defaultMessage: `You can't deactivate this account`,
    description: 'Title for deactivation error modal dialog',
  },
  verifiedModalText: {
    id: 'organization.member.detail.deactivate.modal.description',
    defaultMessage: '{displayName} will {boldedText} to all Atlassian account services.',
  },
  verifiedModalTextBolded: {
    id: 'organization.member.detail.deactivate.modal.description.bolded',
    defaultMessage: 'immediately lose access',
  },
  modalTextError: {
    id: 'organization.member.detail.deactivate.modal.description.error',
    defaultMessage: `The account for {displayName} can't be deactivated because:`,
    description: 'Message to show when there are errors on deactivation dialog',
  },
  bodyWarnings: {
    id: 'organization.member.detail.deactivate.modal.description.warnings',
    description: 'Second paragraph on the deletion modal in case of warnings',
    defaultMessage: `Before you deactivate their account, you should know that:`,
  },
  disable: {
    id: 'organization.member.detail.deactivate.modal.deactivate',
    defaultMessage: 'Deactivate account',
  },
  cancel: {
    id: 'organization.member.detail.deactivate.modal.cancel',
    defaultMessage: 'Cancel',
  },
  ok: {
    id: 'organization.member.detail.deactivate.modal.ok',
    description: 'Label for OK button',
    defaultMessage: 'OK',
  },
});

const gridSizeTimes = factor => akGridSize * factor;

export const ListItems = styled.ul`
  list-style: none;
  padding-left: ${gridSizeTimes(2)}px;
`;

export class MemberDisableModalImpl extends React.PureComponent<MemberDisableModalProps & AnalyticsClientProps> {
  public render() {
    const { displayName, orgId, memberId, canDeactivateResult } = this.props;

    const hasErrors = canDeactivateResult && canDeactivateResult.errors && canDeactivateResult.errors.length;

    const hasWarnings = canDeactivateResult && canDeactivateResult.warnings && canDeactivateResult.warnings.length;

    const modalFooter = hasErrors ? this.modalDialogFooterErrors : this.modalDialogFooter;
    const modalHeader = hasErrors ? this.modalHeaderErrors : this.modalDialogHeader;

    const screenEventData = hasErrors ? memberDeactivationErrorScreenEvent(orgId, memberId) :
      memberDeactivationScreenEvent(orgId, memberId);

    return (
      <StyledModalDialog>
        <ModalDialog
          width="small"
          header={modalHeader()}
          isOpen={this.props.isOpen}
          onClose={this.props.toggleDialog}
          footer={modalFooter()}
        >
          <ScreenEventSender event={screenEventData}>
            {this.modalBody(displayName, hasErrors, hasWarnings, canDeactivateResult)}
          </ScreenEventSender>
        </ModalDialog>
      </StyledModalDialog>
    );
  }

  private modalBody(displayName, hasErrors, hasWarnings, canDeactivateResult) {
    if (hasErrors) {
      return (
        <React.Fragment>
          <FormattedMessage
            {...messages.modalTextError}
            values={{
              displayName,
            }}
          />
        {this.messagesList(canDeactivateResult.errors)}
      </React.Fragment>
      );
    }

    return (
      <React.Fragment>
        <FormattedMessage
          {...messages.verifiedModalText}
          values={{
            displayName,
            boldedText: (
              <FormattedMessage
                {...messages.verifiedModalTextBolded}
                tagName="strong"
              />
            ),
          }}
        />
        {
          hasWarnings &&
            <React.Fragment>
              <FormattedMessage {...messages.bodyWarnings} tagName="p" />
              {this.messagesList(canDeactivateResult.warnings)}
            </React.Fragment> || null
        }
      </React.Fragment>
    );
  }

  private modalDialogHeader = () => {
    return <span><IconWrapper>{createErrorIcon()}</IconWrapper><FormattedMessage {...messages.title} /></span>;
  };

  private modalHeaderErrors = () => {
    return <span><FormattedMessage {...messages.titleErrors} /></span>;
  }

  private modalDialogFooter = () => {
    return (
      <div>
        <AnalyticsButton
          onClick={this.hideDialogWithAnalytics}
          appearance="subtle-link"
        >
          <FormattedMessage {...messages.cancel}/>
        </AnalyticsButton>
          <AnalyticsButton
            onClick={this.confirmDeactivationWithAnalytics}
            isLoading={this.props.isDisabling}
            appearance="warning"
            id="deactivateAccountModal"
          >
            <FormattedMessage {...messages.disable}/>
          </AnalyticsButton>
      </div>
    );
  }

  private modalDialogFooterErrors = () => {
    return (
      <div>
        <AkButton
          onClick={this.props.toggleDialog}
          appearance="primary"
        >
          <FormattedMessage {...messages.ok} />
        </AkButton>
      </div>
    );
  }

  private messagesList = (items) => {
    if (items && items.length > 0) {
      return (
        <ListItems>
          {items.map(({ message }) => (
            <li key={message}>
              {message}
              </li>
          ))}
        </ListItems>
      );
    }

    return null;
  }

  private confirmDeactivationWithAnalytics = () => {
    const { disableMember, analyticsClient, orgId, memberId, products } = this.props;

    disableMember();

    analyticsClient.sendUIEvent({
      data: confirmMemberDeactivation({
        orgId,
        memberId,
        products,
      }),
    });
  };

  private hideDialogWithAnalytics = () => {
    const { toggleDialog, analyticsClient, orgId, memberId } = this.props;

    toggleDialog();

    analyticsClient.sendUIEvent({
      data: closeMemberDeactivation({
        orgId,
        memberId,
      }),
    });
  };
}

export const MemberDisableModal: React.ComponentClass<MemberDisableModalProps> = withAnalyticsClient(MemberDisableModalImpl);
