import * as React from 'react';
import styled from 'styled-components';

import AkSuccessIcon from '@atlaskit/icon/glyph/editor/success';
import AkErrorIcon from '@atlaskit/icon/glyph/error';
import { colors as akColors, gridSize as akGridSize, typography as akTypography } from '@atlaskit/theme';

// Custom Validator Message that allows for using FormattedHTMLMessages.

const IconWrapper = styled.span`
  display: inline-block;
  vertical-align: text-top;
`;

export const Error = () => (
  <IconWrapper>
    <AkErrorIcon label="" size="small" />
  </IconWrapper>
);
export const Success = () => (
  <IconWrapper>
    <AkSuccessIcon label="" size="small" />
  </IconWrapper>
);

interface MessageProps {
  invalid: boolean;
  children?: any;
}

const Message = styled.div`
  ${akTypography.h200}
  font-weight: normal;
  color: ${({ invalid }: MessageProps) => (invalid ? akColors.R400 : akColors.G400)};
  margin-top: ${akGridSize() * 2}px;
`;

interface OwnProps {
  invalidMessage: React.ReactNode;
  validMessage?: React.ReactNode;
  hasValidationError: boolean;
}

export class ValidationMessage extends React.Component<OwnProps> {
  public static defaultProps: Partial<OwnProps> = {
    hasValidationError: undefined,
  };

  public render() {
    const { invalidMessage, validMessage, hasValidationError } = this.props;
    if (hasValidationError) {
      return (
        <Message invalid={true}>
          <Error />
          {invalidMessage}
        </Message>
      );

    } else if (validMessage && !hasValidationError) {
      return (
        <Message invalid={false}>
          <Success />
          {validMessage}
        </Message>
      );
    }

    return null;
  }
}
