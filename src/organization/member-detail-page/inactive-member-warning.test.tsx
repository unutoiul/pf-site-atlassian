// tslint:disable jsx-use-translation-function

import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import AkButton from '@atlaskit/button';
import AkWarningIcon from '@atlaskit/icon/glyph/warning';

import { links } from 'common/link-constants';

import { MemberStatus } from '../../schema/schema-types/document-types';
import { InactiveMemberWarningImpl, WarningActions } from './inactive-member-warning';

import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';

describe('Inactive Member Warning', () => {
  const defaultStatus: MemberStatus = 'DISABLED';
  const defaultProps = {
    memberFullName: 'Full Name',
    active: defaultStatus,
  };

  const blockedStatus: MemberStatus = 'BLOCKED';
  const exportControlsProps = {
    memberFullName: 'Full Name',
    active: blockedStatus,
  };

  const shallowRender = (props = {}) => shallow(
    <InactiveMemberWarningImpl
        intl={createMockIntlProp()}
        {...defaultProps}
        {...props}
    />,
    createMockIntlContext(),
  );

  it('renders warning icon', () => {
    const wrapper = shallowRender();
    expect(wrapper.find(AkWarningIcon).length).to.equal(1);
  });

  it('renders correct heading', () => {
    const wrapper = shallowRender();
    const heading = wrapper.find('FormattedMessage').at(0);
    expect(heading.props()).to.deep.include({
      tagName: 'h5',
      id: 'organization.member.deactivated.heading',
    });
  });

  describe('description', () => {
    it('renders correct description for users blocked by export controls', () => {
      const wrapper = shallowRender(exportControlsProps);
      const description = wrapper.find('FormattedMessage').at(1);
      expect(description.props()).to.deep.include({
        tagName: 'p',
        id: 'organization.member.deactivated.by.export.controls.description',
      });
    });
    it('renders generic description if there is no deactivated info', () => {
      const wrapper = shallowRender();
      const description = wrapper.find('FormattedMessage').at(1);
      expect(description.props()).to.deep.include({
        tagName: 'p',
        id: 'organization.member.deactivated.description',
      });
    });
    it('renders full description if there is deactivated info', () => {
      const wrapper = shallowRender({
        ...defaultProps,
        deactivatedInfo: {
          deactivatedOn: new Date().toISOString(),
          deactivatedBy: '123',
        },
      });
      const description = wrapper.find('FormattedMessage').at(1);
      expect(description.props()).to.deep.include({
        tagName: 'p',
        id: 'organization.member.deactivated.full.description',
      });
    });
  });

  describe('action buttons', () => {
    it('renders link to learn more if the user is not blocked by export controls', () => {
      const wrapper = shallowRender();
      const learnMoreButton = wrapper.find(WarningActions).find(AkButton);
      expect(learnMoreButton.props()).to.contain({
        appearance: 'link',
        href: links.external.accountDeactivation,
        target: '_blank',
      });
      expect(learnMoreButton.children().at(0).prop('id')).to.equal('organization.member.deactivated.learn.more');
    });

    it('does not render link to learn more if the user is blocked by export controls', () => {
      const wrapper = shallowRender(exportControlsProps);
      const learnMoreButton = wrapper.find(WarningActions).find(AkButton);

      expect(learnMoreButton.children().length).to.equal(0);
    });
  });

});
