import { ApolloError, ApolloQueryResult } from 'apollo-client';
import * as moment from 'moment';
import * as React from 'react';
import { compose, graphql, MutationOpts, OptionProps, QueryResult } from 'react-apollo';
import { FormattedHTMLMessage, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import { Link, RouteComponentProps } from 'react-router-dom';
import styled from 'styled-components';

import AkButton, { ButtonGroup as AkButtonGroup } from '@atlaskit/button';
import AkDropdownMenu from '@atlaskit/dropdown-menu';
import AkFieldTextStateless from '@atlaskit/field-text';
import AkWarningIcon from '@atlaskit/icon/glyph/warning';
import { Grid as AkGrid, GridColumn as AkGridColumn } from '@atlaskit/page';
import AkSpinner from '@atlaskit/spinner';
import { gridSize as akGridSize } from '@atlaskit/theme';
import { akColorY300, akGridSizeUnitless } from '@atlaskit/util-shared-styles';

import {
  DeactivateUserOverviewScreen as AkDeactivateUserOverviewScreen,
  DeleteUserOverviewScreen as AkDeleteUserOverviewScreen,
  FocusedTaskCloseAccount as AkFocusedTaskCloseAccount,
} from '@atlaskit/focused-task-close-account';

import {
  analyticsClient,
  AnalyticsClientProps,
  Button as AnalyticsButton,
  cancelDeactivateButton,
  cancelDeleteButton,
  deactivateAccountButton,
  deactivateAccountFocusedTaskScreenEvent,
  deactivateAccountLink,
  deleteAccountButton,
  deleteAccountFocusedTaskScreenEvent,
  memberDetailsScreenEvent,
  openCancelMemberDeletionModalButton,
  openReactivateMemberModalButton,
  ScreenEventSender,
  withAnalyticsClient,
} from 'common/analytics';
import { getAvatarUrlByUserId } from 'common/avatar';
import {
  ConflictError,
  createErrorIcon,
  createSuccessIcon,
  errorFlagMessages,
  GenericError,
  PreconditionFailedError,
  RequestError,
} from 'common/error';
import { FlagAction, FlagProps, withFlag } from 'common/flag';
import { PageLayout, PageSidebar } from 'common/page-layout';

import { links } from 'common/link-constants';

import { OrgBreadcrumbs } from '../breadcrumbs';
import { ManagedAccountsBreadcrumb } from '../members/managed-accounts-breadcrumb';
import { CancelDeletionModal } from './cancel-deletion-modal';

import {
  ActivateMemberAccountMutation,
  ActivateMemberAccountMutationVariables,
  CancelDeleteMemberAccountMutation,
  CancelDeleteMemberAccountMutationVariables,
  CloseMemberAccountMutation,
  CloseMemberAccountMutationVariables,
  CurrentUser,
  DeactivatedInfo,
  DeactivateMemberAccountMutation,
  DeactivateMemberAccountMutationVariables,
  MemberAccess,
  MemberAccessQuery,
  MemberAccessQueryVariables,
  MemberDetailsCanCloseQuery,
  MemberDetailsCanCloseQueryVariables,
  MemberDetailsQuery,
  MemberDetailsQueryVariables,
  PendingInfo,
  ResetMemberPasswordMutation,
  ResetMemberPasswordMutationVariables,
  UpdateMemberEnabledMutation,
  UpdateMemberEnabledMutationVariables,
  UpdateMemberMutation,
  UpdateMemberMutationVariables,
  UpdateMfaEnrollmentMutation,
  UpdateMfaEnrollmentMutationVariables,
  UpdateMfaExemptionMutation,
  UpdateMfaExemptionMutationVariables,
} from '../../schema/schema-types';

import getMemberTotal from '../member-page/member-page.query.graphql';
import { createOrgAnalyticsData } from '../organizations/organization-analytics';
import { EnforcementState } from '../organizations/organization.prop-types';
import { getEnforcementState } from '../organizations/organizations.util';
import memberAccessQuery from './member-access.query.graphql';
import getCanClose from './member-details-can-close.query.graphql';
import getMember from './member-details.query.graphql';
import { MemberDisableModal } from './member-disable-modal';
import { defaultMessages, labelMessages, otherDetailsMessages, statusMessages } from './member-messages';
import { ProductAccessTable } from './product-access-table';
import resetMemberPassword from './reset-member-password.mutation.graphql';
import { SiteAdminTable } from './site-admin-table';
import updateMemberEnabled from './update-member-enabled.mutation.graphql';
import updateMember from './update-member.mutation.graphql';
import updateMfaEnrollment from './update-mfa-enrollment.mutation.graphql';
import updateMfaExemption from './update-mfa-exemption.mutation.graphql';

import { PendingAndDeactivatedInfoFromIOMFeatureFlag, withPendingAndDeactivatedInfoFromIOMFeatureFlag } from '../feature-flags/with-id-org-manager-pending-deactivated-info-feautre.flag';
import { MemberApiTokenFeatureFlag, withMemberApiTokenFeatureFlag } from '../feature-flags/with-member-api-tokens-feature-flag';
import { MemberDeactivationViaFamFeatureFlag, withMemberDeactivationViaFamFeatureFlag } from '../feature-flags/with-member-deactivation-via-fam-feature-flag';
import { MemberDeletionFeatureFlag, withMemberDeletionFeatureFlag } from '../feature-flags/with-member-deletion-cancel-feature-flag';
import activateMemberAccount from './activate.mutation.graphql';
import cancelDeleteMemberAccount from './cancel-deletion.mutation.graphql';
import deactivateMemberAccount from './deactivate.mutation.graphql';
import { DeletionPendingWarning } from './deletion-pending-warning';
import { InactiveMemberWarning } from './inactive-member-warning';
import { ManagedByIdentityProviderBanner } from './managed-by-identity-provider-banner';
import { MemberApiTokenTable } from './member-api-token-table';
import { MemberDeletionModal } from './member-deletion-modal';
import closeMemberAccount from './member-deletion.mutation.graphql';
import { MemberDetailsPageInfo } from './member-detail-page-info';
import { ReactivateMemberModal } from './reactivate-member-modal';
import { ValidationMessage } from './validation-message';

const BannerWrapper = styled.div`
  margin-bottom: ${akGridSize() * 5}px;
`;

const ButtonBar = styled.div`
  padding-top: ${akGridSizeUnitless * 2}px;
`;

const Warning = props => <AkWarningIcon {...props} primaryColor={akColorY300} />;

const InlineContent = styled.p`
  display: inline-flex;
  align-items: center;

  span {
    padding-right: ${akGridSizeUnitless}px;
  }
`;

const Subtitle = styled.div`
  position: absolute;
`;

interface MemberActivationState {
  orgId: string;
  memberId: string;
  active: boolean;
}

type MemberActivationMutator = (state: MemberActivationState) => Promise<any>;

interface OwnState {
  isUpdating: boolean;
  isCancelDeletionDialogOpen: boolean;
  isDeleteFocusedTaskOpen: boolean;
  isDeactivateFocusedTaskOpen: boolean;
  isMemberDeletionModalDialogOpen: boolean;
  isMemberDeactivateModalDialogOpen: boolean;
  hasEmailValidationError: boolean;
  isReactivateModalOpen: boolean;
}

interface MemberDetailParams {
  orgId: string;
  memberId: string;
  memberApiTokenFeatureFlag: boolean;
}

interface MemberDetailPageProps {
  currentUser: Partial<CurrentUser>;
  memberAccess: MemberAccessQuery['member']['memberDetails']['memberAccess'];
  memberAccessError: boolean;
  data: QueryResult & MemberDetailsQuery;
  canClose: QueryResult & MemberDetailsCanCloseQuery;
  mutate(opts: MutationOpts & { variables: UpdateMemberMutationVariables }): Promise<ApolloQueryResult<UpdateMemberMutation>>;
  mutateMfaEnforcement(opts: MutationOpts & { variables: UpdateMfaExemptionMutationVariables }): Promise<ApolloQueryResult<UpdateMfaExemptionMutation>>;
  mutateMfaEnrollment(opts: MutationOpts & { variables: UpdateMfaEnrollmentMutationVariables }): Promise<ApolloQueryResult<UpdateMfaEnrollmentMutation>>;
  mutateEnabled(opts: MutationOpts & { variables: UpdateMemberEnabledMutationVariables }): Promise<ApolloQueryResult<UpdateMemberEnabledMutation>>;
  mutateResetPassword(opts: MutationOpts & { variables: ResetMemberPasswordMutationVariables }): Promise<ApolloQueryResult<ResetMemberPasswordMutation>>;
  mutateCloseMemberAccount(opts: MutationOpts & { variables: CloseMemberAccountMutationVariables }): Promise<ApolloQueryResult<CloseMemberAccountMutation>>;
  mutateCancelDeleteMemberAccount(opts: MutationOpts & { variables: CancelDeleteMemberAccountMutationVariables; }): Promise<ApolloQueryResult<CancelDeleteMemberAccountMutation>>;
  mutateDeactivateMemberAccount(opts: MutationOpts & { variables: DeactivateMemberAccountMutationVariables }): Promise<ApolloQueryResult<DeactivateMemberAccountMutation>>;
  mutateActivateMemberAccount(opts: MutationOpts & { variables: ActivateMemberAccountMutationVariables }): Promise<ApolloQueryResult<ActivateMemberAccountMutation>>;
}

type AllProps =
  MemberDetailPageProps
  & FlagProps
  & InjectedIntlProps
  & MemberDeletionFeatureFlag
  & MemberApiTokenFeatureFlag
  & MemberDeactivationViaFamFeatureFlag
  & RouteComponentProps<MemberDetailParams>
  & AnalyticsClientProps
  & PendingAndDeactivatedInfoFromIOMFeatureFlag;

export class MemberDetailPageImpl extends React.Component<AllProps, OwnState> {
  public state: OwnState = {
    isUpdating: false,
    isCancelDeletionDialogOpen: false,
    isDeleteFocusedTaskOpen: false,
    isDeactivateFocusedTaskOpen: false,
    isMemberDeletionModalDialogOpen: false,
    isMemberDeactivateModalDialogOpen: false,
    hasEmailValidationError: false,
    isReactivateModalOpen: false,
  };

  public componentWillReceiveProps(nextProps: MemberDetailPageProps) {
    if (this.props.data.loading !== true || nextProps.data.loading !== false || nextProps.data.error) {
      return;
    }

    analyticsClient.eventHandler(createOrgAnalyticsData({
      orgId: this.props.match.params.orgId,
      action: 'pageLoad',
      actionSubject: 'organizationPageLoad',
      actionSubjectId: 'memberDetailsPage',
    }));
  }

  public render() {
    const { intl: { formatMessage }, memberAccess, memberAccessError, currentUser } = this.props;

    if (this.props.data.error) {
      return <PageLayout><span>{formatMessage(defaultMessages.error)}</span></PageLayout>;
    } else if (!this.props.data.loading && !this.props.data.member) {
      return (<PageLayout><span>{formatMessage(defaultMessages.notFound)}</span></PageLayout>);
    } else if (!this.props.data.loading && !this.props.data.organization) {
      return (<PageLayout><span>{formatMessage(defaultMessages.organizationNotFound)}</span></PageLayout>);
    } else if (this.props.data.loading || !this.props.data.organization.security || !this.props.data.organization.users) {
      return (<PageLayout><AkSpinner /></PageLayout>);
    } else {
      const { memberDetails } = this.props.data.member;
      const isInactive = memberDetails.active !== 'ENABLED';
      const isMfaExempt = this.userIsMfaExempt();
      const isDeletionFeatureEnabled = userDeletionFFEnabled(this.props);
      const pendingInfo = isDeletionFeatureEnabled ? this.userPendingDeletionInfo() : null;
      const saveDetailsIsPrimary = !(isDeletionFeatureEnabled && isInactive);

      const action = (
        <AkButtonGroup>
          {isDeletionFeatureEnabled && pendingInfo &&
            <AkButton
              onClick={this.showCancelDeletionDialog}
              appearance="primary"
            >
              {formatMessage(defaultMessages.cancelDelete)}
            </AkButton>
          }
          {isDeletionFeatureEnabled && memberDetails.active === 'DISABLED' && !pendingInfo &&
            <AkButton
              onClick={this.showReactivateMemberDialog}
              appearance="primary"
            >
              {formatMessage(defaultMessages.activate)}
            </AkButton>
          }
          <AkDropdownMenu
            items={[{ items: this.getDropdownItems() }]}
            onItemActivated={this.executeOptionItem}
            triggerType="button"
            position="bottom right"
            triggerButtonProps={{ isDisabled: this.state.isUpdating }}
          >
            {formatMessage(defaultMessages.actions)}
          </AkDropdownMenu>
        </AkButtonGroup>
      );

      const mfaExemptSidePanel = (
        <PageSidebar>
          <h4>{formatMessage(otherDetailsMessages.otherDetails)}</h4>
          <InlineContent>
            <Warning label={formatMessage(otherDetailsMessages.mfaExempt)} size="medium" />
            <span>{formatMessage(otherDetailsMessages.mfaExempt)}</span>
          </InlineContent>
        </PageSidebar>
      );

      const updateAnalytics = createOrgAnalyticsData({
        orgId: this.props.match.params.orgId,
        actionSubject: 'updateOrgMemberDetailsButton',
        actionSubjectId: 'update',
        action: 'click',
      });

      const isUserViewingThemselves = currentUser && memberDetails.id === currentUser.id;
      const memberDeletionGracePeriod = 14;
      const isProvisionedByGsync = this.isProvisionedByGsync();
      const isProvisionedByScim = this.isProvisionedByScim();
      const isDisplayNameFieldDisabled = isProvisionedByGsync || isProvisionedByScim || this.state.isUpdating;
      const isEmailFieldDisabled = isProvisionedByGsync || isProvisionedByScim || this.state.isUpdating;
      const isJobTitleFieldDisabled = isProvisionedByScim || this.state.isUpdating;
      const areAllFormFieldsDisabled = isDisplayNameFieldDisabled && isEmailFieldDisabled && isJobTitleFieldDisabled;
      const banner = this.getBanner();
      const deleteFocusedTaskScreenEventData = deleteAccountFocusedTaskScreenEvent();
      const deactivateFocusedTaskScreenEventData = deactivateAccountFocusedTaskScreenEvent();
      const tokensCount = this.props.data.member.memberTokens ? this.props.data.member.memberTokens.length : undefined;

      return (
        <ScreenEventSender
          event={memberDetailsScreenEvent({ tokensCount })}
          isDataLoading={this.props.memberApiTokenFeatureFlag.isLoading || this.props.data.loading}
        >
          <PageLayout
            isDisabled={!isDeletionFeatureEnabled && memberDetails.active !== 'ENABLED'}
            subtitle={!isDeletionFeatureEnabled ? this.generateSubtitle(memberDetails.active) : undefined}
            icon={getAvatarUrlByUserId(memberDetails.id)}
            title={memberDetails.displayName}
            action={action}
            isFullWidth={true}
            breadcrumbs={
              <OrgBreadcrumbs>
                <ManagedAccountsBreadcrumb />
              </OrgBreadcrumbs>
            }
          >

            {!!banner && <BannerWrapper>{banner}</BannerWrapper>}
            <AkGrid>
              <AkGridColumn medium={6}>
                <h2>{formatMessage(defaultMessages.subtitle)}</h2>
                <p><FormattedHTMLMessage {...defaultMessages.description} /></p>
                <form onSubmit={this.updateMember}>
                  {this.generateFieldText('displayName', formatMessage(labelMessages.displayName), memberDetails.displayName, isDisplayNameFieldDisabled, 255)}
                  {this.generateFieldText('email', formatMessage(labelMessages.emailAddress), memberDetails.emails[0].value, isEmailFieldDisabled)}
                  { this.state.hasEmailValidationError && <ValidationMessage
                    invalidMessage={this.getEmailErrorMessage()}
                    hasValidationError={true}
                  /> }
                  {this.generateFieldText('jobTitle', formatMessage(labelMessages.jobTitle), memberDetails.jobTitle, isJobTitleFieldDisabled)}
                  <ButtonBar>
                    <AnalyticsButton
                      isLoading={this.state.isUpdating}
                      type="submit"
                      appearance={saveDetailsIsPrimary ? 'primary' : 'default'}
                      analyticsData={updateAnalytics}
                      isDisabled={areAllFormFieldsDisabled}
                    >
                      {formatMessage(defaultMessages.submit)}
                    </AnalyticsButton>
                  </ButtonBar>
                </form>
              </AkGridColumn>
              <AkGridColumn medium={1} />
              <AkGridColumn medium={5}>
                {isMfaExempt && mfaExemptSidePanel}
                {isDeletionFeatureEnabled && <MemberDetailsPageInfo status={(isDeletionFeatureEnabled && pendingInfo) ? 'PENDING-DELETION' : memberDetails.active} />}
              </AkGridColumn>
            </AkGrid>
            {this.generateMemberAccessTables()}
            {this.generateMemberApiTokenTable()}
            {
              this.props.data.member.canDeactivateAccount &&
              <MemberDisableModal
                isOpen={this.state.isMemberDeactivateModalDialogOpen}
                isDisabling={this.state.isUpdating}
                displayName={memberDetails.displayName}
                canDeactivateResult={this.props.data.member.canDeactivateAccount}
                disableMember={this.disableMember}
                toggleDialog={this.toggleDisableMemberDialog}
                orgId={this.props.match.params.orgId}
                memberId={this.props.match.params.memberId}
                products={getMemberProductAccess(memberAccess, memberAccessError)}
              />
            }
            <AkFocusedTaskCloseAccount
              onClose={this.closeDeactivateFocusedTask}
              isOpen={this.state.isDeactivateFocusedTaskOpen}
              screens={[
                <ScreenEventSender event={deactivateFocusedTaskScreenEventData} key="deactivateFocusedTaskScreenEventData">
                  <AkDeactivateUserOverviewScreen
                    key="DeactivateUserOverviewScreen"
                    user={{
                      avatarUrl: getAvatarUrlByUserId(memberDetails.id),
                      fullName: memberDetails.displayName,
                      email: memberDetails.emails[0].value,
                    }}
                    accessibleSites={getMemberAccessibleSites(memberAccess, memberAccessError)}
                    isCurrentUser={isUserViewingThemselves}
                  />
                </ScreenEventSender>,
              ]}
              learnMoreLink={links.external.accountDeactivation}
              submitButton={
                <AkButton appearance="primary" onClick={this.showDeactivateConfirmation} id="deactivateAccountFocusedTask" >
                  {formatMessage(defaultMessages.deactivate)}
                </AkButton>}
            />
            {
              isDeletionFeatureEnabled &&
              <CancelDeletionModal
                isOpen={this.state.isCancelDeletionDialogOpen}
                cancelDeletion={this.cancelDeletion}
                hideDialog={this.hideCancelDeletionDialog}
                displayName={memberDetails.displayName}
                memberAccess={this.props.memberAccess}
                orgId={this.props.match.params.orgId}
                memberId={this.props.match.params.memberId}
              />
            }
            <AkFocusedTaskCloseAccount
              onClose={this.closeDeleteFocusedTask}
              isOpen={this.state.isDeleteFocusedTaskOpen}
              screens={[
                <ScreenEventSender event={deleteFocusedTaskScreenEventData} key="deleteFocusedTaskScreenEventData">
                  <AkDeleteUserOverviewScreen
                    key="DeleteUserOverviewScreen"
                    user={{
                      avatarUrl: getAvatarUrlByUserId(memberDetails.id),
                      fullName: memberDetails.displayName,
                      email: memberDetails.emails[0].value,
                    }}
                    accessibleSites={getMemberAccessibleSites(memberAccess, memberAccessError)}
                    isCurrentUser={isUserViewingThemselves}
                    isUserDeactivated={isInactive}
                    deactivateUserHandler={(isInactive ? () => null : this.deactivateFromDeleteHandler)}
                  />
                </ScreenEventSender>,
              ]}
              learnMoreLink={links.external.rightToBeForgotten}
              submitButton={
                <AkButton appearance="primary" onClick={this.showDeletionConfirmation} >
                  {formatMessage(defaultMessages.delete)}
                </AkButton>}
            />
            {
              isDeletionFeatureEnabled &&
              this.props.canClose &&
              this.props.canClose.member &&
              this.props.canClose.member.canCloseAccount &&
              <MemberDeletionModal
                isOpen={this.state.isMemberDeletionModalDialogOpen}
                hideDialog={this.hideMemberDeletionDialog}
                confirmDelete={this.confirmDeleteMember}
                canCloseResult={this.props.canClose.member.canCloseAccount}
                displayName={memberDetails.displayName}
                orgId={this.props.match.params.orgId}
                memberId={this.props.match.params.memberId}
                gracePeriod={memberDeletionGracePeriod}
                products={getMemberProductAccess(memberAccess, memberAccessError)}
              />
            }
            <ReactivateMemberModal
              isOpen={this.state.isReactivateModalOpen}
              displayName={memberDetails.displayName}
              memberAccess={this.props.memberAccess}
              orgId={this.props.match.params.orgId}
              memberId={this.props.match.params.memberId}
              reactivateMember={this.onEnableMember}
              hideDialog={this.hideReactivateMemberDialog}
            />
          </PageLayout>
        </ScreenEventSender>
      );
    }
  }

  private getEmailErrorMessage = () => {
    const { intl: { formatMessage } } = this.props;
    // try to pass in link in a better way
    const reactRoute = `/o/${this.props.match.params.orgId}/domains`;
    const domainsPageLink = <Link to={reactRoute}> {formatMessage(defaultMessages.verifiedDomain)}</Link>;

    return (
      <FormattedMessage {...defaultMessages.unverifiedEmailDomain} values={{ domainMessage: domainsPageLink }} />
    );

  }

  private getBanner() {
    const {
      data: {
        member: {
          memberDetails,
        },
      },
    } = this.props;

    const isDeletionFeatureEnabled = userDeletionFFEnabled(this.props);
    const pendingInfo = this.userPendingDeletionInfo();
    const deactivatedInfo = this.userDeactivatedInfo();

    if (isDeletionFeatureEnabled && pendingInfo) {
      return (
        <DeletionPendingWarning
          memberFullName={memberDetails.displayName}
          deleteDate={this.props.intl.formatDate(moment(pendingInfo.startedOn).add(14, 'days').toDate(), { day: '2-digit', month: 'long', year: 'numeric' })}
          deletedBy={pendingInfo.initiatorName ? pendingInfo.initiatorName : ''}
        />
      );
    } else if (isDeletionFeatureEnabled && memberDetails.active !== 'ENABLED') {
      return (
        <InactiveMemberWarning
          memberFullName={memberDetails.displayName}
          active={memberDetails.active}
          deactivatedInfo={deactivatedInfo}
        />
      );
    }

    if (memberDetails.provisionedBy) {
      return <ManagedByIdentityProviderBanner idpType={memberDetails.provisionedBy} />;
    }

    return null;
  }

  private isProvisionedByScim() {
    return this.props.data.member.memberDetails.provisionedBy === 'SCIM';
  }

  private isProvisionedByGsync() {
    return this.props.data.member.memberDetails.provisionedBy === 'GSYNC';
  }

  private generateMemberAccessTables() {
    const { memberAccessError, memberAccess } = this.props;

    if (memberAccessError) {
      return this.generateMemberAccessError();
    }

    return (
      <React.Fragment>
        <SiteAdminTable memberAccess={memberAccess} />
        <ProductAccessTable memberAccess={memberAccess} />
      </React.Fragment>
    );
  }

  private generateMemberAccessError = () => {
    return <GenericError header={<FormattedMessage {...defaultMessages.memberAccessErrorTitle} />} />;
  }

  private executeOptionItem = (e) => {
    e.item.onItemActivated(e);
  }

  private generateSubtitle(activeState) {
    const { formatMessage } = this.props.intl;

    return activeState === 'ENABLED' ? undefined : <Subtitle>{formatMessage(defaultMessages.deactivatedState)}</Subtitle>;
  }

  // tslint:disable-next-line:prefer-function-over-method
  private generateFieldText(name, label, value, isDisabled, maxLength?) {
    return (
      <AkFieldTextStateless
        disabled={isDisabled}
        shouldFitContainer={true}
        name={name}
        label={label}
        value={value || ''}
        maxLength={maxLength}
      />
    );
  }

  private showSuccessFlag = (message: FormattedMessage.MessageDescriptor) => {
    this.props.showFlag({
      autoDismiss: true,
      icon: createSuccessIcon(),
      id: `organisation.member.detail.success.flag.${Date.now()}`,
      title: this.props.intl.formatMessage(message),
    });
  }

  private toggleMfaExemption = () => {
    const { orgId, memberId } = this.props.match.params;
    const isMfaEnforced = this.orgHasMfaEnforced();
    const isMfaExempt = this.userIsMfaExempt();
    const isMfaEnrolled = !!this.props.data.member.memberDetails.useMfa;

    this.setState({ isUpdating: true });

    const promises = [this.updateMfaExemption(orgId, memberId, isMfaExempt, isMfaEnrolled, isMfaEnforced)];

    // If the user is being exempted from mfa, delete their existing enrollments
    if (!isMfaExempt) {
      promises.push(this.deleteMfaEnrollment(orgId, memberId, isMfaEnforced));
    }

    Promise.all(promises).then(() => {
      this.setState({ isUpdating: false });
      this.showSuccessFlag(isMfaExempt ? statusMessages.enforceMfa : statusMessages.excludeMfa);
    }).catch((error) => {
      this.setState({ isUpdating: false });
      analyticsClient.onError(error);
    });
  }

  private updateMfaExemption = async (orgId: string, memberId: string, isMfaExempt: boolean, isMfaEnrolled: boolean, isMfaEnforced: boolean): Promise<ApolloQueryResult<any>> => {
    analyticsClient.eventHandler(createOrgAnalyticsData({
      orgId,
      actionSubject: 'twoStepVerificationMemberToggle',
      actionSubjectId: isMfaExempt ? 'enforce' : 'exclude',
      action: 'click',
      attributes: {
        wasMfaEnrolled: isMfaEnrolled,
        isMfaEnforced,
      },
    }));

    return this.props.mutateMfaEnforcement({
      variables: {
        input: {
          id: orgId,
          member: memberId,
          exempt: !isMfaExempt,
        },
      },
      optimisticResponse: {
        updateMfaExemption: true,
      },
      refetchQueries: [{
        query: getMember,
        variables: {
          id: orgId,
          memberId,
          memberTokenFeatureFlagDisabled: !memberApiTokenFFEnabled(this.props),
          pendingAndDeactivatedInfoReadFromIOM: pendingAndDeactivatedInfoFromIomFFEnabled(this.props),
        },
      }],
    });
  }

  private disableMfa = () => {
    const { orgId, memberId } = this.props.match.params;
    const isMfaEnforced = this.orgHasMfaEnforced();

    this.setState({ isUpdating: true });

    this.deleteMfaEnrollment(orgId, memberId, isMfaEnforced).then(() => {
      this.setState({ isUpdating: false });
      this.showSuccessFlag(statusMessages.disabledMfa);
    }).catch((error) => {
      this.setState({ isUpdating: false });
      analyticsClient.onError(error);
    });
  }

  private deleteMfaEnrollment = async (orgId: string, memberId: string, isMfaEnforced: boolean): Promise<ApolloQueryResult<any>> => {
    analyticsClient.eventHandler(createOrgAnalyticsData({
      orgId,
      actionSubject: 'twoStepVerificationMemberDeleteEnrolment',
      actionSubjectId: 'update',
      action: 'click',
      attributes: {
        isMfaEnforced,
      },
    }));

    return this.props.mutateMfaEnrollment({
      variables: {
        input: {
          id: orgId,
          member: memberId,
        },
      },
      optimisticResponse: {
        updateMfaEnrollment: true,
      }, refetchQueries: [{
        query: getMember,
        variables: {
          id: orgId,
          memberId,
          memberTokenFeatureFlagDisabled: !memberApiTokenFFEnabled(this.props),
          pendingAndDeactivatedInfoReadFromIOM: pendingAndDeactivatedInfoFromIomFFEnabled(this.props),
        },
      }],
    });
  }

  private resetPassword = () => {
    const { orgId, memberId } = this.props.match.params;
    this.setState({ isUpdating: true });

    analyticsClient.eventHandler(createOrgAnalyticsData({
      orgId,
      actionSubject: 'resetOrgMemberPasswordButton',
      actionSubjectId: 'reset',
      action: 'click',
    }));

    this.props.mutateResetPassword({
      variables: {
        id: orgId,
        member: memberId,
      },
      optimisticResponse: {
        resetMemberPassword: true,
      },
      refetchQueries: [{
        query: getMember,
        variables: {
          id: orgId,
          memberId,
          memberTokenFeatureFlagDisabled: !memberApiTokenFFEnabled(this.props),
          pendingAndDeactivatedInfoReadFromIOM: pendingAndDeactivatedInfoFromIomFFEnabled(this.props),
        },
      }],
    }).then(() => {
      this.setState({ isUpdating: false });
      this.showSuccessFlag(statusMessages.resetPassword);
    }).catch((error) => {
      this.setState({ isUpdating: false });
      analyticsClient.onError(error);
    });
  }

  private updateMember = (e) => {
    e.preventDefault();
    this.setState({ isUpdating: true, hasEmailValidationError: false });
    this.props.mutate({
      variables: {
        id: this.props.match.params.orgId,
        member: this.props.match.params.memberId,
        displayName: e.currentTarget.displayName.value,
        emailAddress: e.currentTarget.email.value,
        jobTitle: e.currentTarget.jobTitle.value,
      },
      optimisticResponse: {
        updateMember: true,
      },
      refetchQueries: [
        {
          query: getMember,
          variables: {
            id: this.props.match.params.orgId,
            memberId: this.props.match.params.memberId,
            memberTokenFeatureFlagDisabled: !memberApiTokenFFEnabled(this.props),
            pendingAndDeactivatedInfoReadFromIOM: pendingAndDeactivatedInfoFromIomFFEnabled(this.props),
          },
        },
        {
          query: getMemberTotal,
          variables: {
            id: this.props.match.params.orgId,
          },
        },
      ]})
      .then(() => {
        this.setState({ isUpdating: false });
        this.showSuccessFlag(statusMessages.details);
      }).catch((error) => {
        this.setState({ isUpdating: false });
        this.handleMemberUpdateErrors(error);
      });
  }

  private handleMemberUpdateErrors = (error: ApolloError): void => {
    const preconditionFailed = PreconditionFailedError.findIn(error);

    // current the only validation error, would have to include future validation error to run within this if block
    if (preconditionFailed.length) {
      this.showValidationErrorMessages();
    } else {
      // by default it should run the "something went wrong" flag if there is no specific error.
      this.showMemberUpdateErrorFlag(error);
    }
  }

  private showMemberUpdateErrorFlag = (error: ApolloError) => {
    const { intl: { formatMessage }, showFlag } = this.props;
    let errorDescription = formatMessage(errorFlagMessages.description);
    const providedEmailAlreadyTakenRegex = /Unable to update email for the user (.+) as provided email is already taken./;
    const conflictErrors = ConflictError.findIn(error);

    conflictErrors.find((conflictError) => {
      if (providedEmailAlreadyTakenRegex.test(conflictError.message)) {
        const match = conflictError.message.match(providedEmailAlreadyTakenRegex);
        if (match) {
          errorDescription = formatMessage(defaultMessages.duplicateEmailError);

          return true;
        }
      }

      return false;
    });

    showFlag({
      autoDismiss: true,
      icon: createErrorIcon(),
      id: `organisation.member.detail.success.flag.${Date.now()}`,
      title: formatMessage(errorFlagMessages.title),
      description: errorDescription,
    });
  }

  private showValidationErrorMessages = () => {
    this.setState({
      hasEmailValidationError: true,
    });
  }

  private disableMember = () => {
    this.updateEnabled(true);
  }

  private showCancelDeletionDialog = () => {
    this.setState({ isCancelDeletionDialogOpen: true });

    this.props.analyticsClient.sendUIEvent({
      data: openCancelMemberDeletionModalButton({
        orgId: this.props.match.params.orgId,
        memberId: this.props.match.params.memberId,
      }),
    });
  }

  private hideCancelDeletionDialog = () => {
    this.setState({ isCancelDeletionDialogOpen: false });
  }

  private hideMemberDeletionDialog = () => {
    this.setState({ isMemberDeletionModalDialogOpen: false });
  }

  private confirmDeleteMember = () => {
    const { orgId, memberId } = this.props.match.params;
    const { formatMessage } = this.props.intl;

    this.setState({ isUpdating: true });
    this.hideMemberDeletionDialog();

    this.props.mutateCloseMemberAccount({
      variables: {
        memberId,
      },
      optimisticResponse: {
        closeMemberAccount: true,
      },
      refetchQueries: [{
        query: getMember,
        variables: {
          id: orgId,
          memberId,
          memberTokenFeatureFlagDisabled: !memberApiTokenFFEnabled(this.props),
          pendingAndDeactivatedInfoReadFromIOM: pendingAndDeactivatedInfoFromIomFFEnabled(this.props),
        },
      }],
    }).then(() => {
      this.setState({ isUpdating: false });
      this.showFlagWithMessage(createSuccessIcon(),
        formatMessage(statusMessages.deleteAccountTitle),
        formatMessage(statusMessages.deleteAccount,
          { memberName: this.props.data.member.memberDetails.displayName }),
        [{
          content: formatMessage(defaultMessages.cancelDelete),
          onClick: this.showCancelDeletionDialog,
        }]);
      this.closeDeleteFocusedTaskWithoutAnalytics();
    }).catch((error) => {
      this.setState({ isUpdating: false });
      this.showFlagWithMessage(createErrorIcon(),
        formatMessage(errorFlagMessages.title),
        formatMessage(statusMessages.deleteAccountFailed,
          { memberName: this.props.data.member.memberDetails.displayName }),
        []);
      analyticsClient.onError(error);
    });

  }

  private showFlagWithMessage = (icon, title, message, actions: FlagAction[]) => {

    this.props.showFlag({
      autoDismiss: true,
      icon,
      id: `organisation.member.detail.success.flag.${Date.now()}`,
      title,
      description: message,
      actions,
    });
  }

  private cancelDeletion = () => {
    const { orgId, memberId } = this.props.match.params;
    const { formatMessage } = this.props.intl;

    this.setState({ isUpdating: true });

    this.props.mutateCancelDeleteMemberAccount({
      variables: {
        memberId,
      },
      optimisticResponse: {
        cancelDeleteMemberAccount: true,
      },
      refetchQueries: [{
        query: getMember,
        variables: {
          id: orgId,
          memberId,
          memberTokenFeatureFlagDisabled: !memberApiTokenFFEnabled(this.props),
          pendingAndDeactivatedInfoReadFromIOM: pendingAndDeactivatedInfoFromIomFFEnabled(this.props),
        },
      }],
    }).then(() => {
      this.setState({ isUpdating: false });
      this.showFlagWithMessage(createSuccessIcon(),
        formatMessage(statusMessages.cancelDeleteAccountTitle),
        formatMessage(statusMessages.cancelDeleteAccount,
          { memberName: this.props.data.member.memberDetails.displayName }),
        []);
    }).catch((error: RequestError) => {
      this.setState({ isUpdating: false });
      this.showFlagWithMessage(createErrorIcon(),
        formatMessage(errorFlagMessages.title),
        formatMessage(statusMessages.cancelDeleteAccountFailed,
          { memberName: this.props.data.member.memberDetails.displayName }),
        []);
      analyticsClient.onError(error);
    });

    this.hideCancelDeletionDialog();
  };

  private toggleDisableMemberDialog = () => {
    this.setState({ isMemberDeactivateModalDialogOpen: !this.state.isMemberDeactivateModalDialogOpen });
  }

  private toggleDeactivateFocusedTask = () => {
    this.setState({ isDeactivateFocusedTaskOpen: !this.state.isDeactivateFocusedTaskOpen });
  }

  private showReactivateMemberDialog = () => {
    this.setState({ isReactivateModalOpen: true });

    this.props.analyticsClient.sendUIEvent({
      data: openReactivateMemberModalButton({
        orgId: this.props.match.params.orgId,
        memberId: this.props.match.params.memberId,
      }),
    });
  }

  private hideReactivateMemberDialog = () => {
    this.setState({ isReactivateModalOpen: false });
  };

  private onEnableMember = () => {
    this.updateEnabled(false);
    analyticsClient.eventHandler(createOrgAnalyticsData({
      orgId: this.props.match.params.orgId,
      actionSubject: 'enableOrgMemberButton',
      actionSubjectId: 'update',
      action: 'click',
    }));
  }

  private onDisableMemberDrawer = () => {
    const { canDeactivateAccount } = this.props.data.member;

    if (canDeactivateAccount!.errors && canDeactivateAccount!.errors.length > 0) {
      this.setState({ isMemberDeactivateModalDialogOpen: true });
    } else {
      this.toggleDeactivateFocusedTask();
    }
  };

  private mutateMemberDeactivate: MemberActivationMutator = async ({ orgId, memberId }) => {
    return this.props.mutateDeactivateMemberAccount({
      variables: {
        memberId,
      },
      optimisticResponse: {
        deactivateMemberAccount: true,
      },
      refetchQueries: [
        {
          query: getMember,
          variables: {
            id: orgId,
            memberId,
            memberTokenFeatureFlagDisabled: !memberApiTokenFFEnabled(this.props),
            pendingAndDeactivatedInfoReadFromIOM: pendingAndDeactivatedInfoFromIomFFEnabled(this.props),
          },
        },
      ],
    });
  };

  private mutateMemberActivate: MemberActivationMutator = async ({ orgId, memberId }) => {
    return this.props.mutateActivateMemberAccount({
      variables: {
        memberId,
      },
      optimisticResponse: {
        activateMemberAccount: true,
      },
      refetchQueries: [
        {
          query: getMember,
          variables: {
            id: orgId,
            memberId,
            memberTokenFeatureFlagDisabled: !memberApiTokenFFEnabled(this.props),
            pendingAndDeactivatedInfoReadFromIOM: pendingAndDeactivatedInfoFromIomFFEnabled(this.props),
          },
        },
      ],
    });
  };

  private mutateMemberActive: MemberActivationMutator = async ({ orgId, memberId, active }) => {
    return this.props.mutateEnabled({
      variables: {
        id: orgId,
        member: memberId,
        enabled: !active,
      },
      optimisticResponse: {
        updateMemberEnabled: true,
      },
      refetchQueries: [{
        query: getMember,
        variables: {
          id: orgId,
          memberId,
          memberTokenFeatureFlagDisabled: !memberApiTokenFFEnabled(this.props),
          pendingAndDeactivatedInfoReadFromIOM: pendingAndDeactivatedInfoFromIomFFEnabled(this.props),
        },
      }],
    });
  };

  private updateEnabled = (active) => {
    const { formatMessage } = this.props.intl;
    const { orgId, memberId } = this.props.match.params;
    this.setState({ isUpdating: true });

    const isDeactivationViaFamEnabled = userDeactivationViaFamFFEnabled(this.props);

    // This feature flag ensures that the existing flow is identical, until FAM path is integrated successfully.
    const updateFunction: MemberActivationMutator = isDeactivationViaFamEnabled
      ? (active ? this.mutateMemberDeactivate : this.mutateMemberActivate)
      : this.mutateMemberActive;

    updateFunction({ orgId, memberId, active }).then(() => {
      this.setState({ isUpdating: false, isDeactivateFocusedTaskOpen: false, isMemberDeactivateModalDialogOpen: false });
      if (active) {
        this.showFlagWithMessage(createSuccessIcon(),
          formatMessage(statusMessages.deactivatedAccountTitle),
          formatMessage(statusMessages.deactivatedAccountDescription, { memberName: this.props.data.member.memberDetails.displayName }),
          [{
            content: formatMessage(defaultMessages.activate),
            onClick: this.showReactivateMemberDialog,
          }],
        );

        return;
      }

      this.hideReactivateMemberDialog();
      this.showSuccessFlag(statusMessages.enableAccount);
    }).catch((error) => {
      this.setState({ isUpdating: false });
      if (active) {
        this.showFlagWithMessage(createErrorIcon(),
          formatMessage(errorFlagMessages.title),
          formatMessage(statusMessages.deactivateAccountFailed,
          { memberName: this.props.data.member.memberDetails.displayName }),
        []);
      }
      analyticsClient.onError(error);
    });
  };

  private getDropdownItems() {
    const { formatMessage } = this.props.intl;
    const items: any[] = [];

    const { memberDetails } = this.props.data.member;

    const isMfaEnforced = this.orgHasMfaEnforced();
    const isMfaExempt = this.userIsMfaExempt();
    const isDeletionFeatureEnabled = userDeletionFFEnabled(this.props);
    const isPendingDeletion = this.userPendingDeletionInfo() || false;
    const isProvisionedByGsync = this.isProvisionedByGsync();

    items.push({
      content: formatMessage(defaultMessages.resetPassword),
      isDisabled: isProvisionedByGsync,
      onItemActivated: this.resetPassword,
    });

    if (!(isDeletionFeatureEnabled && isPendingDeletion)) {
      if (isMfaEnforced) {
        items.push({
          content: isMfaExempt ? formatMessage(defaultMessages.enforce) : formatMessage(defaultMessages.exclude),
          isDisabled: isProvisionedByGsync,
          onItemActivated: this.toggleMfaExemption,
        });
      } else if (memberDetails.useMfa) {
        items.push({
          content: formatMessage(defaultMessages.disableMfa),
          isDisabled: isProvisionedByGsync,
          onItemActivated: this.disableMfa,
        });
      }

      const isMemberActive = memberDetails.active === 'ENABLED';

      if (!isDeletionFeatureEnabled || isMemberActive) {
        items.push({
          content: isMemberActive ? formatMessage(defaultMessages.deactivate) : formatMessage(defaultMessages.activate),
          isDisabled: isProvisionedByGsync || memberDetails.active === 'BLOCKED',
          onItemActivated: isMemberActive ? this.onDisableMemberDrawer : this.showReactivateMemberDialog,
        });
      }
    }

    if (isDeletionFeatureEnabled &&
      this.props.canClose &&
      !(this.props.canClose.loading || this.props.canClose.error) &&
      this.props.canClose.member &&
      this.props.canClose.member.canCloseAccount &&
      !isPendingDeletion
    ) {
      items.push({
        content: formatMessage(defaultMessages.delete),
        onItemActivated: this.openDeleteFocusedTask,
      });
    }

    return items;
  }

  private orgHasMfaEnforced = (): boolean => {
    const { organization } = this.props.data;
    const enforcementState = getEnforcementState({ twoStepVerification: { dateEnforced: (organization.security.twoStepVerification!.dateEnforced as any) } });

    return enforcementState.state === EnforcementState.Enforced;
  }

  private userIsMfaExempt = (): boolean => {
    const { mfaExemptMembers } = this.props.data.organization.users;
    const { memberDetails } = this.props.data.member;

    return !!(mfaExemptMembers || []).find(u => u.userId === memberDetails.id);
  }

  private userPendingDeletionInfo = (): PendingInfo | null => {
    return this.props.data.member.memberDetails.pendingInfo;
  }

  private userDeactivatedInfo = (): DeactivatedInfo | null => {
    return this.props.data.member.memberDetails.deactivatedInfo;
  }

  private showDeletionConfirmation = (): void => {
    this.setState({
      isMemberDeletionModalDialogOpen: true,
    });

    this.props.analyticsClient.sendUIEvent({
      data: deleteAccountButton(),
    });
  }

  private showDeactivateConfirmation = (): void => {
    this.toggleDisableMemberDialog();
    analyticsClient.eventHandler(createOrgAnalyticsData({
      orgId: this.props.match.params.orgId,
      actionSubject: 'disableOrgMemberButton',
      actionSubjectId: 'openModal',
      action: 'click',
    }));

    this.props.analyticsClient.sendUIEvent({
      data: deactivateAccountButton(),
    });
  }

  private openDeleteFocusedTask = async () => {
    const { canCloseAccount } = this.props.canClose.member;

    if (canCloseAccount && canCloseAccount.errors && canCloseAccount.errors.length) {
      this.setState({
        isMemberDeletionModalDialogOpen: true,
      });
    } else {
      this.setState({
        isDeleteFocusedTaskOpen: true,
      });
    }
  }

  private closeDeleteFocusedTask = (): void => {
    this.setState({
      isDeleteFocusedTaskOpen: false,
    });

    this.props.analyticsClient.sendUIEvent({
      data: cancelDeleteButton(),
    });
  }

  private closeDeactivateFocusedTask = (): void => {
    this.setState({
      isDeactivateFocusedTaskOpen: false,
    });

    this.props.analyticsClient.sendUIEvent({
      data: cancelDeactivateButton(),
    });

  }

  private closeDeleteFocusedTaskWithoutAnalytics = (): void => {
    this.setState({
      isDeleteFocusedTaskOpen: false,
    });
  }

  private deactivateFromDeleteHandler = () => {
    this.props.analyticsClient.sendUIEvent({
      data: deactivateAccountLink(),
    });

    this.closeDeleteFocusedTaskWithoutAnalytics();
    this.onDisableMemberDrawer();
  }

  private generateMemberApiTokenTable() {
    const hasIdentity = this.props.data.products && this.props.data.products.identityManager;

    return this.props.memberApiTokenFeatureFlag.value ? (
      <MemberApiTokenTable tokens={this.props.data.member.memberTokens} hasIdentity={hasIdentity} orgId={this.props.match.params.orgId}/>
    ) : null;
  }
}

export const getMemberAccessibleSites = (memberAccess: MemberAccess | null, memberAccessError: boolean): string[] => {
  const siteDetails = new Set<string>();

  if (!memberAccessError && memberAccess && memberAccess.products) {
    memberAccess.products.map(product => siteDetails.add(product.siteUrl));
  }

  return Array.from(siteDetails);
};

export const getMemberProductAccess = (memberAccess: MemberAccess | null, memberAccessError: boolean): string[] => {
  const products = new Set<string>();

  if (!memberAccessError && memberAccess && memberAccess.products) {
    memberAccess.products.map(product => products.add(product.productKey));
  }

  return Array.from(products);
};

const query = graphql<RouteComponentProps<MemberDetailParams>, MemberDetailsQuery, MemberDetailsQueryVariables, MemberDetailPageProps>(getMember, {
  options: (props) => ({
    variables: {
      id: props.match.params.orgId,
      memberId: props.match.params.memberId,
      memberTokenFeatureFlagDisabled: !memberApiTokenFFEnabled(props),
      pendingAndDeactivatedInfoReadFromIOM: pendingAndDeactivatedInfoFromIomFFEnabled(props),
    },
  }),
});
const mutation = graphql<UpdateMemberMutation, any>(updateMember);
const mfaExemptionMutation = graphql<UpdateMfaExemptionMutation, any>(updateMfaExemption, { name: 'mutateMfaEnforcement' });
const mfaEnrollmentMutation = graphql<UpdateMfaEnrollmentMutation, any>(updateMfaEnrollment, { name: 'mutateMfaEnrollment' });
const updateEnabledMutation = graphql<UpdateMemberEnabledMutation, any>(updateMemberEnabled, { name: 'mutateEnabled' });
const resetPasswordMutation = graphql<ResetMemberPasswordMutation, any>(resetMemberPassword, { name: 'mutateResetPassword' });
const closeMemberAccountMutation = graphql<CloseMemberAccountMutation, any>(closeMemberAccount, { name: 'mutateCloseMemberAccount' });
const cancelDeleteMemberAccountMutation = graphql<CancelDeleteMemberAccountMutation, any>(cancelDeleteMemberAccount, { name: 'mutateCancelDeleteMemberAccount' });
const deactivateMemberAccountMutation = graphql<DeactivateMemberAccountMutation, any>(deactivateMemberAccount, { name: 'mutateDeactivateMemberAccount' });
const activateMemberAccountMutation = graphql<ActivateMemberAccountMutation, any>(activateMemberAccount, { name: 'mutateActivateMemberAccount' });
const withCanCloseAccount = graphql<RouteComponentProps<MemberDetailParams> & MemberDeletionFeatureFlag, MemberDetailsCanCloseQuery, MemberDetailsCanCloseQueryVariables>(
  getCanClose,
  {
    name: 'canClose',
    options: (props) => ({ variables: { id: props.match.params.orgId, memberId: props.match.params.memberId } }),
    skip: (props) => !userDeletionFFEnabled(props),
  },
);

const userDeletionFFEnabled = (props): boolean => {
  return props.memberDeletionFeatureFlag.value;
};

const memberApiTokenFFEnabled = (props): boolean => {
  return props.memberApiTokenFeatureFlag.value;
};

const pendingAndDeactivatedInfoFromIomFFEnabled = (props): boolean => {
  return props.pendingAndDeactivatedInfoFromIOMFeatureFlag.value;
};

const userDeactivationViaFamFFEnabled = (props): boolean => {
  return props && props.memberDeactivationViaFamFeatureFlag && props.memberDeactivationViaFamFeatureFlag.value;
};

const getMemberProps = (props: OptionProps<{}, MemberAccessQuery>): Partial<MemberDetailPageProps> => {
  const access = getMemberAccess(props);

  const hasLoadError = !!(props.data && props.data.error);
  const hasAccessLoadError = !!(access && access.errors && access.errors.length > 0);

  return {
    memberAccess: access,
    memberAccessError: hasLoadError || hasAccessLoadError,
  };
};

const withMemberAccess = graphql<RouteComponentProps<MemberDetailParams>, MemberAccessQuery, MemberAccessQueryVariables, Partial<MemberDetailPageProps>>(
  memberAccessQuery,
  {
    options: (props) => ({ variables: { id: props.match.params.orgId, memberId: props.match.params.memberId } }),
    props: getMemberProps,
  },
);

const getMemberAccess = (props: OptionProps<{}, MemberAccessQuery>) => {
  if (!props || !props.data || props.data.error || !props.data.member || !props.data.member.memberDetails) {
    return null;
  }

  return props.data.member.memberDetails.memberAccess;
};

export const MemberDetailPage = compose(
  withPendingAndDeactivatedInfoFromIOMFeatureFlag,
  withMemberApiTokenFeatureFlag,
  query,
  mutation,
  mfaExemptionMutation,
  mfaEnrollmentMutation,
  updateEnabledMutation,
  resetPasswordMutation,
  activateMemberAccountMutation,
  deactivateMemberAccountMutation,
  closeMemberAccountMutation,
  cancelDeleteMemberAccountMutation,
  withMemberDeletionFeatureFlag,
  withMemberDeactivationViaFamFeatureFlag,
  withMemberAccess,
  withCanCloseAccount,
)(withFlag(withAnalyticsClient(injectIntl(MemberDetailPageImpl))));
