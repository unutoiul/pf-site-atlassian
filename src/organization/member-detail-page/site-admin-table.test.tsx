import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import { DynamicTableStateless as AkDynamicTableStateless } from '@atlaskit/dynamic-table';

import { MemberAccessQuery } from '../../schema/schema-types';
import { createMockIntlContext } from '../../utilities/testing';
import { SiteAdminTable } from './site-admin-table';

describe('Site Admin Table', () => {
  const mockMemberAccessData: MemberAccessQuery['member']['memberDetails']['memberAccess'] = {
    errors: [],
    sites: [
      {
        __typename: 'MemberSiteAccess',
        siteUrl: 'hamburger.atlassian.net',
        isSiteAdmin: true,
      },
      {
        __typename: 'MemberSiteAccess',
        siteUrl: 'test.atlassian.net',
        isSiteAdmin: true,
      },
      {
        __typename: 'MemberSiteAccess',
        siteUrl: 'empty.com',
        isSiteAdmin: false,
      },
      {
        __typename: 'MemberSiteAccess',
        siteUrl: 'acme.atlassian.com',
        isSiteAdmin: true,
      },
      {
        __typename: 'MemberSiteAccess',
        siteUrl: 'fun.atlassian.com',
        isSiteAdmin: true,
      },
    ],
    products: [],
    __typename: 'MemberAccess',
  };

  const sortedSitesWithAdmin = [
    'acme.atlassian.com',
    'fun.atlassian.com',
    'hamburger.atlassian.net',
    'test.atlassian.net',
  ];

  const shallowSiteAdminTable = ({ mockData = mockMemberAccessData } = {}) => {
    return shallow(
      <SiteAdminTable
        memberAccess={mockData}
      />,
      createMockIntlContext(),
    );
  };

  it('should render sites', () => {
    const wrapper = shallowSiteAdminTable();

    const table = wrapper.find(AkDynamicTableStateless);
    expect(table.exists()).to.equal(true);

    const rows = table.props().rows;
    expect(rows).to.have.length(4);

    rows!.forEach((row, index) => {
      const content = row.cells[0].content as any;
      expect(content.props.children).to.contain(sortedSitesWithAdmin[index]);
    });

  });

  it('should render nothing if memberAccess is null', () => {
    const wrapper = shallowSiteAdminTable({ mockData: null! });

    const table = wrapper.find(AkDynamicTableStateless);
    expect(table.exists()).to.equal(false);
  });

  it('should render nothing if sites is null', () => {
    const mockMemberAccessNullSitesData: MemberAccessQuery['member']['memberDetails']['memberAccess'] = {
      errors: [],
      sites: null!,
      products: [],
      __typename: 'MemberAccess',
    };
    const wrapper = shallowSiteAdminTable({ mockData: mockMemberAccessNullSitesData });

    const table = wrapper.find(AkDynamicTableStateless);
    expect(table.exists()).to.equal(false);
  });

  it('should render nothing if sites is empty', () => {
    const mockMemberAccessEmptySitesData: MemberAccessQuery['member']['memberDetails']['memberAccess'] = {
      errors: [],
      sites: [],
      products: [],
      __typename: 'MemberAccess',
    };
    const wrapper = shallowSiteAdminTable({ mockData: mockMemberAccessEmptySitesData });

    const table = wrapper.find(AkDynamicTableStateless);
    expect(table.exists()).to.equal(false);
  });

});
