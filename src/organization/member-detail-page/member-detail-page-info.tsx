import * as React from 'react';
import { defineMessages, FormattedHTMLMessage, FormattedMessage } from 'react-intl';

import { PageSidebar } from 'common/page-layout';

/* ROCKET-1631: use wired up status for PENDING-DELETION */
interface AllProps {
  status: string;
}

export class MemberDetailsPageInfo extends React.Component<AllProps> {

  public render() {
    const message = this.renderMessage();
    if (!message) {
      return null;
    }

    return (
      <PageSidebar>
        <FormattedMessage {...messages.title} tagName="h4" />
        {message}
      </PageSidebar>
    );
  }

  private renderMessage() {

    const { status } = this.props;

    if (status === 'ENABLED') {
      return <FormattedHTMLMessage {...messages.textCommon} tagName="p" />;
    }

    if (status === 'PENDING-DELETION') {
      return <FormattedHTMLMessage {...messages.textPendingDeletion} tagName="p" />;
    }

    if (status === 'DISABLED') {
      return <FormattedHTMLMessage {...messages.textDisabled} tagName="p" />;
    }

    if (status === 'BLOCKED') {
      return <FormattedHTMLMessage {...messages.textBlocked} tagName="p" />;
    }

    return null;
  }
}

const messages = defineMessages({
  title: {
    id: 'organization.member.detail.info.title',
    description: 'Title of an iformational block on member detail page',
    defaultMessage: 'Account actions',
  },

  textCommon: {
    id: 'organization.member.detail.info.common',
    defaultMessage: `<ul>
      <li>Reset a user's password to log the user out and make them set a new password.</li>
      <li>Deactivate the account to revoke the user's access to Atlassian account services and stop getting billed for them.</li>
      <li>Delete the account to revoke the user's access to Atlassian account services, stop getting billed for them, and delete their personal data.</li>
    </ul>`,
  },

  textPendingDeletion: {
    id: 'organization.member.detail.info.pending.deletion',
    defaultMessage: `<ul>
      <li>Cancel the deletion of the account to give the user access to all Atlassian account services again. We'll also start billing you for them again.</li>
      <li>Reset a user's password to make them set a new password if you cancel the deletion of their account.</li>
    </ul>`,
  },

  textDisabled: {
    id: 'organization.member.detail.info.disabled',
    defaultMessage: `<ul>
      <li>Reset a user's password to make them set a new password if you reactivate their account.</li>
      <li>Reactivate the account to give the user access to Atlassian account services and start getting billed for them again.</li>
      <li>Delete the account to revoke the user's access to Atlassian account services and delete their personal data.</li>
    </ul>`,
  },

  textBlocked: {
    id: 'organization.member.detail.info.blocked',
    defaultMessage: `<ul>
      <li>Reset a user's password to make them set a new password if you reactivate their account.</li>
      <li>Delete the account to revoke the user's access to Atlassian account services and delete their personal data.</li>
    </ul>`,
  },
});
