// tslint:disable jsx-use-translation-function
import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import { PageSidebar } from 'common/page-layout';

import { createMockIntlContext } from '../../utilities/testing';

import { MemberDetailsPageInfo } from './member-detail-page-info';

describe('Cancel Deletion Info', () => {
  const defaultProps = {
    status: 'ENABLED',
  };
  const shallowRender = (props = {}) => shallow(
    <MemberDetailsPageInfo
        {...defaultProps}
        {...props}
    />,
    createMockIntlContext(),
  );

  it('wrapped in a <PageSidebar>', () => {
    const wrapper = shallowRender();
    expect(wrapper.is(PageSidebar)).to.equal(true);
  });

  it('has correct title', () => {
    const wrapper = shallowRender();
    expect(wrapper.childAt(0).props()).to.contain({
      tagName: 'h4',
      id: 'organization.member.detail.info.title',
    });
  });

  it('has correct text for ENABLED user status', () => {
    const wrapper = shallowRender();
    expect(wrapper.childAt(1).props()).to.contain({
      tagName: 'p',
      id: 'organization.member.detail.info.common',
    });
  });

  it('has correct text for PENDING-DELITION user status', () => {
    const wrapper = shallowRender({ status: 'PENDING-DELETION' });
    expect(wrapper.childAt(1).props()).to.contain({
      tagName: 'p',
      id: 'organization.member.detail.info.pending.deletion',
    });
  });

  it('has correct text for DISABLED user status', () => {
    const wrapper = shallowRender({ status: 'DISABLED' });
    expect(wrapper.childAt(1).props()).to.contain({
      tagName: 'p',
      id: 'organization.member.detail.info.disabled',
    });
  });

  it('has correct text for BLOCKED user status', () => {
    const wrapper = shallowRender({ status: 'BLOCKED' });
    expect(wrapper.childAt(1).props()).to.contain({
      tagName: 'p',
      id: 'organization.member.detail.info.blocked',
    });
  });

  it('has correct text for unknown status', () => {
    const wrapper = shallowRender({ status: 'unknown' });
    expect(wrapper.get(0)).to.equal(null);
  });

});
