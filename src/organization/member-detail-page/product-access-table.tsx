import * as React from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';
import styled from 'styled-components';

import { DynamicTableStateless as AkDynamicTableStateless, Row as AkRow } from '@atlaskit/dynamic-table';
import { gridSize as akGridSize } from '@atlaskit/theme';

import { ProductIcon, ProductName } from 'common/icons';
import { EmptyTableView } from 'common/table';

const messages = defineMessages({
  title: {
    id: 'organization.member.detail.product.accessTable.title',
    defaultMessage: 'Product access',
  },
  product: {
    id: 'organization.member.detail.accessTable.product',
    defaultMessage: 'Product',
  },
  empty: {
    id: 'organization.member.detail.accessTable.empty',
    defaultMessage: 'No product access',
  },
});

const Title = styled.h2`
  margin-top: ${akGridSize() * 6}px;
  margin-bottom: ${akGridSize() * 2}px;
`;

const ProductNameCell = styled.div`
  vertical-align: top;
`;

const ProductName = styled.span`
  font-weight: 600;
  letter-spacing: -0.003em;
  margin-top: ${akGridSize() * 2}px;
  margin-left: ${akGridSize() * 4.5}px;
`;

const IconWrapper = styled.span`
  position: absolute;
  margin-top: -${akGridSize() / 2}px;
`;

const AlignTop = styled.div`
  td {
    vertical-align: top;
    padding-top: ${akGridSize}px;
    padding-bottom: ${akGridSize}px;
  }
`;

interface MemberAccessProps {
  memberAccess: {
    products: Array<{
      productName: string,
      siteUrl: string,
    }>,
  } | null;
}

type ProductsAndSites = {
  [k in ProductName]?: string[];
};

export class ProductAccessTable extends React.Component<MemberAccessProps> {

  public render() {
    return (
      <React.Fragment>
        <Title><FormattedMessage {...messages.title} /></Title>
        {this.getContent()}
      </React.Fragment>
    );
  }

  private getContent() {
    const productsAndSites = this.getProductsAndSites(this.props.memberAccess);

    return (
      <AlignTop>
        <AkDynamicTableStateless
          isFixedSize={true}
          isLoading={!productsAndSites}
          head={this.getHead()}
          rows={this.getRows(productsAndSites)}
          emptyView={<EmptyTableView><FormattedMessage {...messages.empty} /></EmptyTableView>}
        />
      </AlignTop>
    );
  }

  private getHead = () => {
    return {
      cells: [
        {
          key: 'product',
          content: <FormattedMessage {...messages.product} />,
          width: 50,
        },
        {
          key: 'site',
          content: '',
          width: 50,
        },
      ],
    };
  }

  private getRows(productsAndSites: ProductsAndSites): AkRow[] | undefined {

    if (!productsAndSites) {
      return undefined;
    }

    const rows: AkRow[] = [];
    Object.keys(productsAndSites).sort().forEach((productName: string) => {
      const product = productsAndSites[productName];

      rows.push(
        {
          cells: [
            {
              key: `product-${productName}`,
              content: (
                <ProductNameCell>
                  <IconWrapper>
                    <ProductIcon productName={productName.toLowerCase() as ProductName} />
                  </IconWrapper>
                  <ProductName>
                    {productName}
                  </ProductName>
                </ProductNameCell>
              ),

            },
            {
              key: `product-${productName}-sites`,
              content: this.getProductSitesContent(product.sort()),
            },
          ],
        },
      );
    });

    return rows;
  }

  private getProductSitesContent = (sites: string[]) => {
    return sites.map(site => (
      // tslint:disable-next-line:react-anchor-blank-noopener
      <div key={site}><a href={`https://${site}`} target="_blank" rel="noopener">{site}</a></div>
    ));
  }

  private getProductsAndSites = (memberAccess: MemberAccessProps['memberAccess']): ProductsAndSites => {
    const productsAndSites = {};

    if (!memberAccess || !memberAccess.products) {
      return productsAndSites;
    }

    memberAccess.products.forEach((product) => {
      if (!productsAndSites[product.productName]) {
        productsAndSites[product.productName] = [];
      }
      if (product.siteUrl) {
        productsAndSites[product.productName].push(product.siteUrl);
      }
    });

    return productsAndSites;
  };

}
