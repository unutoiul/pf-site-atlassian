import * as React from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';
import styled from 'styled-components';

import { DynamicTableStateless as AkDynamicTableStateless, Row as AkRow } from '@atlaskit/dynamic-table';

import { gridSize as akGridSize } from '@atlaskit/theme';

const messages = defineMessages({
  title: {
    id: 'organization.member.detail.accessTable.site.title',
    defaultMessage: 'Site admin',
  },
  site: {
    id: 'organization.member.detail.accessTable.site',
    defaultMessage: 'Site',
  },
});

const Title = styled.h2`
  margin-top: ${akGridSize() * 6}px;
  margin-bottom: ${akGridSize() * 2}px;
`;

interface MemberAccessProps {
  memberAccess: {
    sites: Array<{
      siteUrl: string,
      isSiteAdmin: boolean,
    }>,
  } | null;
}

export class SiteAdminTable extends React.Component<MemberAccessProps> {

  public render() {
    const sitesWithAdmin = this.getSitesWithAdmin(this.props.memberAccess);

    if (!sitesWithAdmin || sitesWithAdmin.length === 0) {
      return null;
    }

    return (
      <React.Fragment>
        <Title><FormattedMessage {...messages.title} /></Title>
        {this.getContent(sitesWithAdmin)}
      </React.Fragment>
    );
  }

  private getContent(sitesWithAdmin: string[]) {
    return (
      <AkDynamicTableStateless
        head={this.getHead()}
        rows={this.getRows(sitesWithAdmin)}
      />
    );
  }

  private getHead = () => {
    return {
      cells: [
        {
          key: 'site',
          content: <FormattedMessage {...messages.site} />,
        },
      ],
    };
  }

  private getRows = (sitesWithAdmin: string[]): AkRow[] | undefined => {

    if (!sitesWithAdmin) {
      return undefined;
    }

    return sitesWithAdmin.sort().map(site => ({
      cells: [
        {
          key: site,
          // tslint:disable-next-line:react-anchor-blank-noopener
          content: <a key={site} href={`https://${site}`} target="_blank" rel="noopener">{site}</a>,
        },
      ],
    }));
  }

  private getSitesWithAdmin = (memberAccess: MemberAccessProps['memberAccess']): string[] => {

    const sitesWithAdmin: string[] = [];

    if (!memberAccess || !memberAccess.sites) {
      return sitesWithAdmin;
    }

    memberAccess.sites.forEach((site) => {
      if (site.isSiteAdmin) {
        sitesWithAdmin.push(site.siteUrl);
      }
    });

    return sitesWithAdmin;
  };

}
