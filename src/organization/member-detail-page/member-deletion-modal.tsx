import * as React from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';

import AkButton from '@atlaskit/button';

import {
  AnalyticsClientProps,
  closeMemberDeletion,
  confirmMemberDeletion,
  memberDeletionErrorsScreenEvent,
  memberDeletionScreenEvent,
  ScreenEventSender,
  withAnalyticsClient,
} from 'common/analytics';
import { ButtonGroup } from 'common/button-group';
import { createDangerIcon } from 'common/error';
import { IconWrapper, ListItems, ModalDialog, StyledModalDialog } from 'common/modal';

import { MemberDetailsCanCloseQuery } from '../../schema/schema-types';

interface MemberDeletionModalProps {
  isOpen: boolean;
  displayName: string;
  orgId: string;
  memberId: string;
  gracePeriod: number;
  canCloseResult: MemberDetailsCanCloseQuery['member']['canCloseAccount'];
  products: string[];
  hideDialog(): void;
  confirmDelete(): void;
}

const messages = defineMessages({
  titleErrors: {
    id: 'organization.member.deletion.modal.errors.title',
    description: 'Dialog title for delete modal dialog in case of errors',
    defaultMessage: `You can't delete this account`,
  },
  titleConfirmation: {
    id: 'organization.member.deletion.modal.confirmation.title',
    description: 'Dialog title for delete confirmation modal dialog',
    defaultMessage: 'Delete account?',
  },
  bodyErrors: {
    id: 'organization.member.deletion.modal.errors.body',
    description: 'Body of delete dialog in case of errors',
    defaultMessage: `The account for {displayName} can't be deleted because:`,
  },
  bodyWarningsPre: {
    id: 'organization.member.deletion.modal.warnings.body.pre',
    description: 'non-bold part of delete dialog body in case of warnings',
    defaultMessage: `{memberName} will`,
  },
  bodyWarningsBold: {
    id: 'organization.member.deletion.modal.warnings.body.bold',
    description: 'Bold part of body of delete dialog in case of warnings',
    defaultMessage: `immediately lose access`,
  },
  bodyWarningsPost: {
    id: 'organization.member.deletion.modal.warnings.body.post',
    description: 'Post-bold part of body of delete dialog in case of warnings',
    defaultMessage: `to all Atlassian account services.`,
  },
  bodyWarningsP2: {
    id: 'organization.member.deletion.modal.warnings.body.p2',
    description: 'Second paragraph on the deletion modal in case of warnings',
    defaultMessage: `Before you delete their account, you should know that:`,
  },
  bodyWarningsDeactivation: {
    id: 'organization.member.deletion.modal.warnings.body.deactivation',
    description: 'Deletion warning for body of delete dialog',
    defaultMessage: `We'll deactivate their account for {gracePeriod} days, during which you can cancel the deletion.`,
  },
  cancel: {
    id: 'organization.member.deletion.modal.cancel',
    description: 'Label for cancel button',
    defaultMessage: 'Cancel',
  },
  ok: {
    id: 'organization.member.deletion.modal.ok',
    description: 'Label for OK button',
    defaultMessage: 'OK',
  },
  delete: {
    id: 'organization.member.deletion.modal.delete',
    description: 'Label for delete button',
    defaultMessage: 'Delete account',
  },
});

export class MemberDeletionModalImpl extends React.PureComponent<MemberDeletionModalProps & AnalyticsClientProps> {
  public render() {
    const { isOpen, hideDialog, canCloseResult, orgId, memberId } = this.props;
    const hasError = canCloseResult && canCloseResult.errors && canCloseResult.errors.length;
    const modalFooter = hasError ? this.modalDialogFooterErrors : this.modalDialogFooterConfirmation;

    const screenEventData = hasError ?
      memberDeletionErrorsScreenEvent(orgId, memberId) : memberDeletionScreenEvent(orgId, memberId);

    return (
      <StyledModalDialog>
        <ModalDialog
          width="small"
          header={this.modalHeader(hasError)}
          isOpen={isOpen}
          onClose={hideDialog}
          footer={modalFooter()}
        >
          <ScreenEventSender event={screenEventData} >
            {
              hasError ? this.modalErrors(canCloseResult!.errors) : this.modalWarnings(canCloseResult!.warnings)
            }
          </ScreenEventSender>
        </ModalDialog>
      </StyledModalDialog>
    );
  }

  private modalDialogFooterErrors = () => {
    return (
      <ButtonGroup alignment="right">
        <AkButton
          onClick={this.props.hideDialog}
          appearance="primary"
        >
          <FormattedMessage {...messages.ok} />
        </AkButton>
      </ButtonGroup>
    );
  }

  private modalDialogFooterConfirmation = () => {
    return (
      <ButtonGroup alignment="right">
        <AkButton
          onClick={this.hideDialogWithAnalytics}
          appearance="default"
        >
          <FormattedMessage {...messages.cancel} />
        </AkButton>
        <AkButton
          onClick={this.confirmDeletionWithAnalytics}
          appearance="danger"
        >
          <FormattedMessage {...messages.delete} />
        </AkButton>
      </ButtonGroup>
    );
  }

  private modalErrors = (errors) => {
    return (
      <React.Fragment>
        <FormattedMessage
          tagName="p"
          {...messages.bodyErrors}
          values={{
            displayName: (this.props.displayName),
          }}
        />
        {this.messagesList(errors)}
      </React.Fragment>
    );
  }

  private modalWarnings = (warnings) => {
    return (
      <React.Fragment>
        <FormattedMessage
          {...messages.bodyWarningsPre}
          values={{
            memberName: (this.props.displayName),
          }}
        /> <FormattedMessage
          tagName="strong"
          {...messages.bodyWarningsBold}
        /> <FormattedMessage
          {...messages.bodyWarningsPost}
        />
        {warnings && warnings.length > 0 &&
        <FormattedMessage
          {...messages.bodyWarningsP2}
          tagName="p"
        />
        }
        {this.messagesList(warnings)}

        <FormattedMessage
          tagName="p"
          {...messages.bodyWarningsDeactivation}
          values={{
            gracePeriod: (this.props.gracePeriod),
          }}
        />

      </React.Fragment>
    );
  }

  private modalHeader = (hasErrors) => {
    return hasErrors ?
      <FormattedMessage {...messages.titleErrors} /> :
      <span><IconWrapper>{createDangerIcon()}</IconWrapper><FormattedMessage {...messages.titleConfirmation} /></span>;
  }

  private messagesList = (items) => {
    if (items && items.length > 0) {
      return (
        <ListItems>
          {items.map(({ message }) => (
            <li key={message}>
              {message}
            </li>
          ))}
        </ListItems>
      );
    }

    return null;

  }

  private confirmDeletionWithAnalytics = () => {
    this.props.confirmDelete();

    this.props.analyticsClient.sendUIEvent({
      data: confirmMemberDeletion({
        orgId: this.props.orgId,
        memberId: this.props.memberId,
        products: this.props.products,
      }),
    });
  };

  private hideDialogWithAnalytics = () => {
    this.props.hideDialog();

    this.props.analyticsClient.sendUIEvent({
      data: closeMemberDeletion({
        orgId: this.props.orgId,
        memberId: this.props.memberId,
      }),
    });
  };

}

export const MemberDeletionModal = withAnalyticsClient(
  MemberDeletionModalImpl,
);
