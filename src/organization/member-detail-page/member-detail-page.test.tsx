// tslint:disable jsx-use-translation-function
import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as moment from 'moment';
import * as React from 'react';
import { FormattedHTMLMessage } from 'react-intl';
import * as sinon from 'sinon';

import AkButton, { ButtonGroup as AkButtonGroup } from '@atlaskit/button';
import AkDropdownMenu from '@atlaskit/dropdown-menu';
import AkFieldTextStateless from '@atlaskit/field-text';
import { FocusedTaskCloseAccount as AkFocusedTaskCloseAccount } from '@atlaskit/focused-task-close-account';
import AkSpinner from '@atlaskit/spinner';

import { analyticsClient, Button as AnalyticsButton } from 'common/analytics';
import { errorFlagMessages, PreconditionFailedError, RequestError } from 'common/error';
import { PageLayout, PageSidebar } from 'common/page-layout';

import {
  MemberAccess,
  MemberDetailsCanCloseQuery,
  MemberDetailsQuery,
  MemberProvisioningSource,
  MemberStatus,
} from '../../schema/schema-types';
import { createMockAnalyticsClient, createMockIntlContext, createMockIntlProp, MockAnalyticsClient, waitUntil } from '../../utilities/testing';
import { CancelDeletionModal } from './cancel-deletion-modal';
import { ManagedByIdentityProviderBanner } from './managed-by-identity-provider-banner';
import { MemberDeletionModal } from './member-deletion-modal';
import { getMemberAccessibleSites, MemberDetailPageImpl } from './member-detail-page';
import { MemberDisableModal } from './member-disable-modal';
import { ReactivateMemberModal } from './reactivate-member-modal';
import { ValidationMessage } from './validation-message';

describe('Member Detail Page', () => {

  const params = {
    params: {
      orgId: 'DUMMY-TEST-ORG-ID',
      memberId: 'DUMMY-MEMBER-ID',
    },
  };

  const memberDetails = (active: MemberStatus, useMfa: boolean = false, pendingInfo: MemberDetailsQuery['member']['memberDetails']['pendingInfo'] = null): MemberDetailsQuery['member']['memberDetails'] => {
    return {
      __typename: '',
      id: '655362:30cb73ac-628c-4c2a-c586c07ae68d',
      active,
      displayName: 'Trash Turkey',
      jobTitle: 'Garbage Collector',
      useMfa,
      provisionedBy: null,
      emails: [
        {
          __typename: '',
          primary: true,
          value: 'chicken@binjuice.com',
        },
      ],
      pendingInfo,
      deactivatedInfo: null,
    };
  };

  const orgData: MemberDetailsQuery['organization'] = {
    __typename: '',
    id: 'test-org',
    security: {
      __typename: '',
      twoStepVerification: {
        __typename: '',
        dateEnforced: null,
      },
    },
    users: {
      __typename: '',
      mfaExemptMembers: [],
    },
  };

  const currentUserData = {
    currentUser: {
      __typename: '',
      id: '',
    },
    products: {
      __typename: '',
      identityManager: true,
    },
  };

  const canDeactivateErrors = {
    __typename: '',
    errors: [{ __typename: '', message: 'Hello', link: 'nothing', code: 'code' }],
    warnings: [],
  };

  const canDeactivateDefault = {
    __typename: '',
    errors: [],
    warnings: [],
  };
  const memberTokenData = {
    memberTokens: [
      {
        __typename: '',
        id: 'api-token-id-1',
        label: 'api-token',
        createdAt: '2019-01-03T00:11:46.608Z',
        lastAccess: '2019-01-05T00:10:46.768Z',
      },
    ],
  };

  const userData = (useMfa: boolean = false): MemberDetailsQuery => {
    return {
      ...currentUserData,
      member: {
        __typename: '',
        memberDetails: {
          ...memberDetails('ENABLED', useMfa),
        },
        canDeactivateAccount: canDeactivateDefault,
        ...memberTokenData,
      },
      organization: orgData,
    };
  };

  const disabledUserData: MemberDetailsQuery = {
    ...currentUserData,
    member: {
      __typename: '',
      memberDetails: memberDetails('DISABLED'),
      canDeactivateAccount: canDeactivateDefault,
      ...memberTokenData,
    },
    organization: orgData,
  };

  const pendingDeletionUserData: MemberDetailsQuery = {
    ...disabledUserData,
    member: {
      __typename: '',
      memberDetails: memberDetails(
        'DISABLED',
        false,
        { __typename: '', startedOn: new Date().toISOString(), startedBy: '655362:30cb73ac-628c-4c2a-c586c07ae68d', initiatorName: 'Alex Simkin' },
      ),
      canDeactivateAccount: canDeactivateDefault,
      ...memberTokenData,
    },
  };

  const eveDisabledUserData: MemberDetailsQuery = {
    ...currentUserData,
    member: {
      __typename: '',
      memberDetails: memberDetails('BLOCKED'),
      canDeactivateAccount: canDeactivateDefault,
      ...memberTokenData,
    },
    organization: orgData,
  };

  const mfaEnforcedUserData = (isMfaEnrolled: boolean = true): MemberDetailsQuery => {
    return {
      ...currentUserData,
      member: {
        __typename: '',
        memberDetails: {
          useMfa: isMfaEnrolled,
          ...memberDetails('ENABLED'),
        },
        canDeactivateAccount: canDeactivateDefault,
        ...memberTokenData,
      },
      organization: {
        __typename: '',
        id: params.params.orgId,
        security: {
          __typename: '',
          twoStepVerification: {
            __typename: '',
            dateEnforced: moment().subtract(1, 'days').unix().toString(),
          },
        },
        users: {
          __typename: '',
          mfaExemptMembers: [],
        },
      },
    };
  };

  const mfaExemptUserData: MemberDetailsQuery = {
    ...currentUserData,
    member: {
      __typename: '',
      memberDetails: memberDetails('ENABLED'),
      canDeactivateAccount: canDeactivateDefault,
      ...memberTokenData,
    },
    organization: {
      __typename: '',
      id: params.params.orgId,
      security: {
        __typename: '',
        twoStepVerification: {
          __typename: '',
          dateEnforced: moment().subtract(1, 'days').unix().toString(),
        },
      },
      users: {
        __typename: '',
        mfaExemptMembers: [{
          __typename: '',
          userId: '655362:30cb73ac-628c-4c2a-c586c07ae68d',
        }],
      },
    },
  };

  const canCloseErrors = {
    member: {
      __typename: '',
      canCloseAccount: {
        __typename: '',
        errors: [
          { __typename: '', message: 'Hello', link: 'nothing', code: 'code' },
        ],
        warnings: [],
      },
    },
  };

  const canCloseDefault = {
    member: {
      __typename: '',
      canCloseAccount: {
        __typename: '',
        errors: [],
        warnings: [],
      },
    },
  };

  const userDataWithCanDeactivate = (useMfa: boolean = false, canDeactivate): MemberDetailsQuery => {
    return {
      ...currentUserData,
      member: {
        __typename: '',
        memberDetails: {
          ...memberDetails('ENABLED', useMfa),
        },
        canDeactivateAccount: canDeactivate,
        ...memberTokenData,
      },
      organization: orgData,
    };
  };

  const disabledMemberDeletionFeatureFlag = {
    isLoading: false,
    value: false,
  };

  const enabledMemberDeletionFeatureFlag = {
    isLoading: false,
    value: true,
  };

  const disabledMemberApiTokenFeatureFlag = {
    isLoading: false,
    value: false,
  };

  const pendingAndDeactivatedInfoFromIOMFeatureFlag = {
    isLoading: false,
    value: false,
  };

  const enableMemberApiTokenFeatureFlag = {
    isLoading: false,
    value: true,
  };

  const disabledMemberDeactivationViaFamFeatureFlag = {
    isLoading: false,
    value: false,
  };

  const enabledMemberDeactivationViaFamFeatureFlag = {
    isLoading: false,
    value: true,
  };

  let mutateEnforcement;
  let mutateEnrollment;
  let mutateDeleteMember;
  let mutateCancelDeletion;
  let mutateActivation;
  let mutateDeactivation;
  let updateMemberEnabled;
  let memberAccess;
  let eventHandlerSpy;
  let showFlagSpy;
  let mutateStub;

  const sandbox = sinon.sandbox.create();

  function setProvisionedBy(data: MemberDetailsQuery, provisionedBy: MemberProvisioningSource) {
    data.member.memberDetails.provisionedBy = provisionedBy;

    return data;
  }

  type PageAction = 'Reset password' | 'Deactivate account' | 'Reactivate account' | 'Delete account' | 'Cancel deletion' | 'Exclude from two-step verification' | 'Enforce two-step verification' | 'Disable two-step verification';

  interface PageActionState {
    action: PageAction;
    isDisabled: boolean;
  }

  interface TextFieldState {
    value: string | undefined;
    isDisabled: boolean;
  }

  interface ActionDescriptor {
    content: PageAction;
    isDisabled: boolean;
    execute(...args: any[]): void;
  }

  class Helper {
    private _wrapper: ShallowWrapper<MemberDetailPageImpl['props'], MemberDetailPageImpl['state']>;
    private mockAnalyticsClient: MockAnalyticsClient;

    constructor(data: Partial<MemberDetailPageImpl['props']['data']>,
      canClose: MemberDetailsCanCloseQuery = canCloseDefault,
      customProps = {}) {

      this.mockAnalyticsClient = createMockAnalyticsClient();
      this._wrapper = shallow(
        <MemberDetailPageImpl
          memberDeletionFeatureFlag={disabledMemberDeletionFeatureFlag}
          memberDeactivationViaFamFeatureFlag={disabledMemberDeactivationViaFamFeatureFlag}
          match={params as any}
          data={data as any}
          mutateMfaEnforcement={mutateEnforcement}
          mutateMfaEnrollment={mutateEnrollment}
          mutateEnabled={updateMemberEnabled}
          mutateCloseMemberAccount={mutateDeleteMember}
          mutateCancelDeleteMemberAccount={mutateCancelDeletion}
          mutateDeactivateMemberAccount={mutateDeactivation}
          mutateActivateMemberAccount={mutateActivation}
          memberAccess={memberAccess}
          intl={createMockIntlProp()}
          analyticsClient={this.mockAnalyticsClient}
          canClose={canClose as any}
          currentUser={{} as any}
          history={null as any}
          location={null as any}
          hideFlag={null as any}
          mutate={mutateStub}
          memberAccessError={false}
          mutateResetPassword={null as any}
          showFlag={showFlagSpy}
          memberApiTokenFeatureFlag={disabledMemberApiTokenFeatureFlag}
          pendingAndDeactivatedInfoFromIOMFeatureFlag={pendingAndDeactivatedInfoFromIOMFeatureFlag}
          {...customProps}
        />,
        createMockIntlContext(),
      );
    }

    public get publicWrapper() {
      return this.wrapper;
    }

    public async cancelDeletion() {
      const cancelDeletionModal = this.pageLayoutWrapper.find(CancelDeletionModal);

      cancelDeletionModal.props().cancelDeletion();
    }

    public async reactivateMember() {
      const reactivateMemberModal = this.pageLayoutWrapper.find(ReactivateMemberModal);

      reactivateMemberModal.props().reactivateMember();
    }

    public startUpdating() {
      this.wrapper.setState({ isUpdating: true });
    }

    public expectFormSubmitButtonToBeDisabled() {
      expect(this.formSubmitButtonWrapper.props().isDisabled).to.equal(true);
    }

    public expectFormSubmitButtonToHaveDefaultAppearance() {
      expect(this.formSubmitButtonWrapper.props().appearance).to.equal('default');
    }

    public expectAllActionsToBeDisabled() {
      expect(this.actionDropdownWrapper.props().triggerButtonProps!.isDisabled).to.equal(true);
      expect(this.formWrapper).to.have.length(1);
      expect(this.formSubmitButtonWrapper.props().isLoading).to.equal(true);
    }

    public async confirmDelete() {
      const memberDeletionModal = this.pageLayoutWrapper.find(MemberDeletionModal);

      memberDeletionModal.props().confirmDelete();
    }

    public async confirmDeactivate() {
      const memberDisableModal = this.pageLayoutWrapper.find(MemberDisableModal);

      memberDisableModal.props().disableMember();
    }

    public expectSpinnerToBe(state: 'absent' | 'present') {
      if (state === 'present') {
        expect(this.spinnerWrapper).to.have.length(1, 'spinner was expected to be present, but it was not');
      } else if (state === 'absent') {
        expect(this.spinnerWrapper).to.have.length(0, 'spinner was expected to be absent, but it was found');
      } else {
        throw new Error(`unknown state: ${state}`);
      }
    }

    public expectGenericErrorMessageToBePresent(variation: 'cannot load details' | 'member not found' | 'org not found') {
      if (variation === 'cannot load details') {
        expect(this.pageLayoutWrapper.contains(<span>Cannot load user details</span>)).to.equal(true);
      } else if (variation === 'member not found') {
        expect(this.pageLayoutWrapper.contains(<span>Members cannot be found</span>)).to.equal(true);
      } else if (variation === 'org not found') {
        expect(this.pageLayoutWrapper.contains(<span>Organization cannot be found</span>)).to.equal(true);
      } else {
        throw new Error(`unknown variation: ${variation}`);
      }
    }

    public expectPageActionsToBe(actions: PageActionState[] | undefined) {
      if (actions === undefined) {
        expect(this.actions).to.be.undefined();
      } else {
        expect(this.actions!.map<PageActionState>(({ content, isDisabled }) => ({ action: content, isDisabled }))).to.deep.equal(actions);
      }
    }

    public expectPageActionsToInclude(action: PageActionState) {
      expect(this.actions!.map<PageActionState>(({ content, isDisabled }) => ({ action: content, isDisabled }))).to.deep.include(action, `Action not found. available actions: ${this.actions!.map(a => a.content).join(', ')}`);
    }

    public expectPageActionsToNotInclude(action: PageAction) {
      expect(this.actions!.map<PageAction>(({ content }) => content)).to.not.include(action);
    }

    public expectPageIconToBe(icon: string) {
      expect(this.pageLayoutWrapper.props().icon).to.equal(icon);
    }

    public expectPageTitleToBe(title: string) {
      expect(this.pageLayoutWrapper.props().title).to.equal(title);
    }

    public expectCloseAccountFocusedTaskToBe(state: { isOpen: boolean; }) {
      expect(this.wrapper.find(AkFocusedTaskCloseAccount).last().props().isOpen).to.equal(state.isOpen);
    }

    public expectDeactivateAccountFocusedTaskToBe(state: { isOpen: boolean; }) {
      expect(this.wrapper.find(AkFocusedTaskCloseAccount).first().props().isOpen).to.equal(state.isOpen);
    }

    public expectAccountDetailsSectionToBePresent() {
      expect(this.wrapper.contains(<h2>Account details</h2>)).to.equal(true);
      expect(this.wrapper.find(FormattedHTMLMessage).props().defaultMessage).to.deep.include(`Changes will apply across all Atlassian account services. Changing the email domain of this user's email`);
    }

    public expectFormToBePresent() {
      expect(this.formWrapper).to.have.length(1);
      expect(this.formSubmitButtonWrapper.props().type).to.deep.equal('submit');
    }

    public expectJobTitleFieldStateToBe(state: TextFieldState) {
      expect(this.jobTitleFieldDescriptor).to.deep.equal(state);
    }

    public expectEmailFieldStateToBe(state: TextFieldState) {
      expect(this.emailFieldDescriptor).to.deep.equal(state);
    }

    public expectDisplayNameFieldStateToBe(state: TextFieldState) {
      expect(this.displayNameFieldDescriptor).to.deep.equal(state);
    }

    public expectMfaExclusionInfoToBePresent() {
      expect(this.wrapper.find(PageSidebar).find('h4').text()).to.equal('Other details');
      expect(this.wrapper.find(PageSidebar).html()).to.include('Excluded from two-step verification');
    }

    public expectBannerToBe(bannerType: 'idp-scim' | 'idp-gsync') {
      if (bannerType === 'idp-scim') {
        const banner = this.wrapper.find(ManagedByIdentityProviderBanner);
        expect(banner).to.have.length(1, 'banner was not found');
        const idpBannerType = banner.prop('idpType');
        expect(idpBannerType).to.equal('SCIM', `banner was found, but had the wrong type (saw ${JSON.stringify(idpBannerType)})`);
      } else if (bannerType === 'idp-gsync') {
        const banner = this.wrapper.find(ManagedByIdentityProviderBanner);
        expect(banner).to.have.length(1, 'banner was not found');
        const idpBannerType = banner.prop('idpType');
        expect(idpBannerType).to.equal('GSYNC', `banner was found, but had the wrong type (saw ${JSON.stringify(idpBannerType)})`);
      } else {
        throw new Error(`unknown banner type: ${bannerType}`);
      }
    }

    public expectAnalyticsEventToHaveBeenFired(eventData: any, eventType?: 'ui') {
      if (eventType === 'ui') {
        expect(this.mockAnalyticsClient.sendUIEvent.callCount).to.equal(1);
        expect(this.mockAnalyticsClient.sendUIEvent.lastCall.args[0]).to.deep.equal(eventData);
      } else if (eventType === undefined) {
        // old analytics
        expect(eventHandlerSpy.lastCall.args[0]).to.deep.equal(eventData);
      }
    }

    public expectFocusedTaskScreenEventToHaveBeenFired(eventData: any) {
      const focusedTask = this.wrapper.find(AkFocusedTaskCloseAccount).last();
      const screenEventSender: any = focusedTask.props().screens[0];
      expect(screenEventSender.props.event).to.deep.equal(eventData);
    }

    public expectDeactivateFocusedTaskScreenEventToHaveBeenFired(eventData: any) {
      const focusedTask = this.wrapper.find(AkFocusedTaskCloseAccount).first();
      const screenEventSender: any = focusedTask.props().screens[0];
      expect(screenEventSender.props.event).to.deep.equal(eventData);
    }

    public expectMemberUpdateApiToNotHaveBeenCalled() {
      expect(updateMemberEnabled.callCount).to.equal(0);
    }

    public expectCancelDeletionModalToBe(state: { isOpen: boolean; }) {
      const modal = this.wrapper.find(CancelDeletionModal);

      expect(modal.props().isOpen).to.equal(state.isOpen);
    }

    public expectReactivateMemberModalToBe(state: { isOpen: boolean; }) {
      const modal = this.wrapper.find(ReactivateMemberModal);

      expect(modal.props().isOpen).to.equal(state.isOpen);
    }

    public expectMemberUpdateApiToHaveBeenCalledWith(options: any) {
      expect(updateMemberEnabled.callCount).to.equal(1);
      expect(updateMemberEnabled.firstCall.args[0]).to.deep.include({ variables: options });
    }

    public expectApiTokensToBePresent() {
      expect(this.wrapper.find('MemberApiTokenTable')).to.have.length(1);
    }

    public expectMemberActivateApiNotToHaveBeenCalled() {
      expect(mutateActivation.callCount).to.equal(0);
    }

    public expectMemberActivateApiToHaveBeenCalledWith(options: any) {
      expect(mutateActivation.callCount).to.equal(1);
      expect(mutateActivation.firstCall.args[0]).to.deep.include({ variables: options });
    }

    public deleteAccount() {
      const buttonWrapper = shallow(this.wrapper.find(AkFocusedTaskCloseAccount).last().props().submitButton as React.ReactElement<any>);
      expect(buttonWrapper.children()).to.be.lengthOf(1);

      const submitButton = buttonWrapper.childAt(0);
      submitButton.simulate('click');
      this.wrapper.update();
    }

    public deactivateAccount() {
      const buttonWrapper = shallow(this.wrapper.find(AkFocusedTaskCloseAccount).first().props().submitButton as React.ReactElement<any>);
      expect(buttonWrapper.children()).to.be.lengthOf(1);

      const submitButton = buttonWrapper.childAt(0);
      submitButton.simulate('click');
      this.wrapper.update();
    }

    public closeFocusedTask() {
      this.wrapper.find(AkFocusedTaskCloseAccount).last().props().onClose();
    }

    public closeDeactivateFocusedTask() {
      this.wrapper.find(AkFocusedTaskCloseAccount).first().props().onClose();
    }

    public updateAccount(options: {email: string; displayName: string; jobTitle: string}) {
      const submitFunction = this.formWrapper.prop('onSubmit')!;
      submitFunction({
        preventDefault: () => null,
        currentTarget: {
          displayName: { value: options.displayName },
          email: { value: options.email },
          jobTitle: { value: options.jobTitle },
        },
      } as any);

      const variables = {
        id: params.params.orgId,
        member: params.params.memberId,
        displayName: options.displayName,
        emailAddress: options.email,
        jobTitle: options.jobTitle,
      };

      expect(mutateStub.callCount).to.equal(1);
      expect(mutateStub.firstCall.args[0]).to.deep.include({ variables });
    }

    public setupMutateToFail(error) {
      mutateStub = sinon.stub().rejects({
        graphQLErrors: [{
          originalError: error,
        }],
      });

      this.wrapper.setProps({
        mutate: mutateStub,
      });
    }

    public expectMemberDeletionModalToBe(state: { isOpen: boolean; }) {
      expect(this.wrapper.find(MemberDeletionModal).props().isOpen).to.equal(state.isOpen);
    }

    public executeAction(action: PageAction, ...args: any[]) {
      this.expectPageActionsToInclude({ action, isDisabled: false });
      const targetAction = this.actions!.find(a => a.content === action);

      targetAction!.execute(...args);

      this.wrapper.update();
    }

    public expectMemberDisableModalToBe(state: { isOpen: boolean; isDisabling: boolean; }) {
      const modal = this.wrapper.find(MemberDisableModal);

      expect(modal.props().isOpen).to.equal(state.isOpen);
      expect(modal.props().isDisabling).to.equal(state.isDisabling);
    }

    private get displayNameFieldDescriptor(): TextFieldState {
      return this.getTextFieldState(0);
    }

    private get emailFieldDescriptor(): TextFieldState {
      return this.getTextFieldState(1);
    }

    private get jobTitleFieldDescriptor(): TextFieldState {
      return this.getTextFieldState(2);
    }

    private get actions(): ActionDescriptor[] | undefined {
      if (this.pageLayoutActionProp === undefined) {
        return undefined;
      }

      expect(this.pageLayoutActionProp.type).to.equal(AkButtonGroup);

      const directActions: ActionDescriptor[] = [];
      if (this.actionWrapper.find(AkButton).length > 0) {
        directActions.push(...this.actionWrapper
          .find(AkButton)
          .map<ActionDescriptor>(b => ({
            content: b.text() as any,
            isDisabled: !!b.prop('isDisabled'),
            execute: b.prop('onClick')!,
          })),
        );
      }

      const dropdownActions: ActionDescriptor[] = this.actionDropdownWrapper.props().items![0].items.map(item => ({
        content: item.content,
        isDisabled: !!item.isDisabled,
        execute: item.onItemActivated,
      } as ActionDescriptor));

      return [
        ...directActions,
        ...dropdownActions,
      ];
    }

    private get formWrapper() {
      return this.wrapper.find('form');
    }

    private get formSubmitButtonWrapper() {
      return this.formWrapper.find(AnalyticsButton);
    }

    private get actionDropdownWrapper() {
      return this.actionWrapper.find(AkDropdownMenu);
    }

    private get pageLayoutActionProp() {
      return this.pageLayoutWrapper.props().action;
    }

    private get actionWrapper() {
      return shallow(this.pageLayoutActionProp!);
    }

    private get pageLayoutWrapper() {
      return this.wrapper.find(PageLayout);
    }

    private get spinnerWrapper() {
      return this.wrapper.find(AkSpinner);
    }

    private getTextFieldState(index: number): TextFieldState {
      const field = this.wrapper.find(AkFieldTextStateless).at(index);
      const { disabled, value } = field.props();

      return {
        value,
        isDisabled: !!disabled,
      };
    }

    private get wrapper() {
      this._wrapper.update();

      return this._wrapper;
    }
  }

  beforeEach(() => {
    mutateEnforcement = sandbox.stub().resolves({});
    mutateEnrollment = sandbox.stub().resolves({});
    mutateDeleteMember = sandbox.stub().resolves({});
    mutateCancelDeletion = sandbox.stub().resolves({});
    mutateDeactivation = sandbox.stub().resolves({});
    mutateActivation = sandbox.stub().resolves({});
    updateMemberEnabled = sandbox.stub().resolves({});
    mutateStub = sandbox.stub().resolves({});
    eventHandlerSpy = sandbox.spy(analyticsClient, 'onEvent');
    showFlagSpy = sandbox.spy();

    memberAccess = { sites: [] };
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('should have a spinner while page is still loading', () => {
    const helper = new Helper({ loading: true });

    helper.expectSpinnerToBe('present');
  });

  it('should display a generic error message if page loading error occurs', () => {
    const helper = new Helper({ loading: false, error: {} as any });

    helper.expectGenericErrorMessageToBePresent('cannot load details');
    helper.expectPageActionsToBe(undefined);
  });

  describe('if the member cannot be found', () => {
    let helper: Helper;

    beforeEach(() => {
      helper = new Helper({ loading: false });
    });

    it('generic error message is displayed', () => {
      helper.expectGenericErrorMessageToBePresent('member not found');
      helper.expectPageActionsToBe(undefined);
    });

    it('should not have a spinner', () => {
      helper.expectSpinnerToBe('absent');
    });
  });

  describe('if the organization cannot be found', () => {
    it('generic error message is displayed', () => {
      const helper = new Helper({ loading: false, member: {} as any, organization: undefined });

      helper.expectGenericErrorMessageToBePresent('org not found');
      helper.expectPageActionsToBe(undefined);
    });
  });

  describe('if the member is found', () => {

    describe('and the form is not in an updating state', () => {
      it('page layout components and text are rendered', () => {
        const user = userData();
        const helper = new Helper({ loading: false, ...user });

        helper.expectPageTitleToBe(user.member.memberDetails.displayName);
        helper.expectPageIconToBe(`https://avatar-cdn.atlassian.com/${user.member.memberDetails.id}?by=id&s=48`);
        helper.expectPageActionsToBe([
          { action: 'Reset password', isDisabled: false },
          { action: 'Deactivate account', isDisabled: false },
        ]);
        helper.expectAccountDetailsSectionToBePresent();
      });

      it('all the form components are rendered', () => {
        const user = userData();
        const helper = new Helper({ loading: false, ...user });

        helper.expectFormToBePresent();
        helper.expectDisplayNameFieldStateToBe({ value: 'Trash Turkey', isDisabled: false });
        helper.expectEmailFieldStateToBe({ value: 'chicken@binjuice.com', isDisabled: false });
        helper.expectJobTitleFieldStateToBe({ value: 'Garbage Collector', isDisabled: false });
      });

      it('should display a banner if the user was provisioned by scim', () => {
        const user = setProvisionedBy(userData(), 'SCIM');
        const helper = new Helper({ loading: false, ...user });

        helper.expectBannerToBe('idp-scim');
      });

      it('should display a banner if the user was provisioned by gsync', () => {
        const user = setProvisionedBy(userData(), 'GSYNC');
        const helper = new Helper({ loading: false, ...user });

        helper.expectBannerToBe('idp-gsync');
      });

      it('if the user is disabled then the dropdown item is "Reactivate account"', () => {
        const helper = new Helper({ loading: false, ...disabledUserData });

        helper.expectPageActionsToInclude({ action: 'Reactivate account', isDisabled: false });
      });

      it('clicking "Reactivate account" should open the modal dialog', () => {
        const helper = new Helper({ loading: false, ...disabledUserData });

        helper.expectReactivateMemberModalToBe({ isOpen: false });
        helper.executeAction('Reactivate account');

        helper.expectReactivateMemberModalToBe({ isOpen: true });
      });

      it('"Reactivate account" action should be disabled if the user was provisioned by gsync', () => {
        const user = setProvisionedBy({ ...disabledUserData }, 'GSYNC');
        const helper = new Helper({ loading: false, ...user });

        helper.expectPageActionsToInclude({ action: 'Reactivate account', isDisabled: true });
      });

      it('clicking "Deactivate account" should open focused task', () => {
        const helper = new Helper({ loading: false, ...userData() });

        helper.expectPageActionsToInclude({ action: 'Deactivate account', isDisabled: false });
        helper.expectDeactivateAccountFocusedTaskToBe({ isOpen: false });

        helper.executeAction('Deactivate account');

        helper.expectMemberUpdateApiToNotHaveBeenCalled();
        helper.expectDeactivateAccountFocusedTaskToBe({ isOpen: true });
      });

      it('clicking "Deactivate account" should open error dialog', () => {
        const helper = new Helper({ loading: false, ...userDataWithCanDeactivate(false, canDeactivateErrors) }, canCloseDefault);

        helper.expectPageActionsToInclude({ action: 'Deactivate account', isDisabled: false });
        helper.expectDeactivateAccountFocusedTaskToBe({ isOpen: false });

        helper.executeAction('Deactivate account');

        helper.expectMemberUpdateApiToNotHaveBeenCalled();

        helper.expectMemberDisableModalToBe({ isOpen: true, isDisabling: false });

      });

      it('opens the modal when `Deactivate account` is pressed on focused task', () => {
        const helper = new Helper({ loading: false, ...userData() });

        helper.expectMemberDisableModalToBe({ isOpen: false, isDisabling: false });

        helper.executeAction('Deactivate account');
        helper.deactivateAccount();

        helper.expectMemberDisableModalToBe({ isOpen: true, isDisabling: false });
      });

      it('sends analytics event when `Deactivate account` is clicked on focused task', () => {
        const helper = new Helper({ loading: false, ...userData() });

        helper.executeAction('Deactivate account');
        helper.deactivateAccount();

        helper.expectAnalyticsEventToHaveBeenFired({
          data: {
            action: 'clicked',
            actionSubject: 'button',
            actionSubjectId: 'deactivateAccount',
            source: 'deactivateAccountFocusedTaskScreen',
          },
        }, 'ui');
      });

      it('sends analytics event when focused task closed', () => {
        const helper = new Helper({ loading: false, ...userData() });

        helper.executeAction('Deactivate account');
        helper.expectDeactivateFocusedTaskScreenEventToHaveBeenFired({ data: { name: 'deactivateAccountFocusedTaskScreen' } });

        helper.closeDeactivateFocusedTask();

        helper.expectDeactivateAccountFocusedTaskToBe({ isOpen: false });
        helper.expectAnalyticsEventToHaveBeenFired({
          data: {
            action: 'clicked',
            actionSubject: 'button',
            actionSubjectId: 'cancelDeactivate',
            source: 'deactivateAccountFocusedTaskScreen',
          },
        }, 'ui');
      });

      it('clicking Deactivate account in modal invokes showFlag with correct parameters', async () => {
        const helper = new Helper({ loading: false, ...userData() });

        helper.executeAction('Deactivate account');

        await helper.confirmDeactivate();

        expect(showFlagSpy.callCount).to.equal(1);

        const arg = showFlagSpy.firstCall.args[0];
        expect(arg.autoDismiss).to.be.true();
        expect(arg.title).to.equals('You deactivated the account.');
        expect(arg.description).to.contain('Trash Turkey no longer has access to Atlassian account services.');
      });

      describe('when API returns error', () => {
        it('handles the error correctly', async () => {
          const helper = new Helper({ loading: false, ...userData() },
            canCloseDefault,
            {
              memberDeletionFeatureFlag: enabledMemberDeletionFeatureFlag,
              memberDeactivationViaFamFeatureFlag: enabledMemberDeactivationViaFamFeatureFlag,
            },
          );

          const error = new RequestError({
            status: 500,
            message: 'test error',
          });
          mutateDeactivation.returns(Promise.reject(error));

          expect(showFlagSpy.callCount).to.equal(0);

          await helper.confirmDeactivate();

          await waitUntil(() => showFlagSpy.callCount === 1);

          const arg = showFlagSpy.firstCall.args[0];
          expect(arg.autoDismiss).to.be.true();
          expect(arg.title).to.equals(errorFlagMessages.title.defaultMessage);
          expect(arg.description).to.equal(`We weren’t able to deactivate Trash Turkey\'s account. Please try again.`);
        });
      });

      it('"Deactivate account" action should be disabled if the user was provisioned by gsync', () => {
        const user = setProvisionedBy(userData(), 'GSYNC');
        const helper = new Helper({ loading: false, ...user });

        helper.expectPageActionsToInclude({ action: 'Deactivate account', isDisabled: true });
      });

      it('if the user is disabled by EVE then the dropdown is disabled', () => {
        const helper = new Helper({ loading: false, ...eveDisabledUserData });

        helper.expectPageActionsToInclude({ action: 'Reactivate account', isDisabled: true });
      });

      it('if the user is mfa exempted page contains warning message', () => {
        const helper = new Helper({ loading: false, ...mfaExemptUserData });

        helper.expectMfaExclusionInfoToBePresent();
      });

      describe('when memberDeletionFeatureFlag is enabled and the user is not pending deletion', () => {

        let helper: Helper;

        beforeEach(() => {
          helper = new Helper(
            { loading: false, ...eveDisabledUserData },
            canCloseDefault,
            {
              memberDeletionFeatureFlag: enabledMemberDeletionFeatureFlag,
            },
          );
        });

        it('does not render cancel delete button', () => {
          helper.expectPageActionsToNotInclude('Cancel deletion');
        });

        it('delete button should be shown', () => {
          helper.expectPageActionsToInclude({ action: 'Delete account', isDisabled: false });
        });

        it('focused task is launched when delete button is pressed', () => {
          helper.expectCloseAccountFocusedTaskToBe({ isOpen: false });

          helper.executeAction('Delete account');

          helper.expectCloseAccountFocusedTaskToBe({ isOpen: true });
        });

        it('sends correct screen event to analytics when focused task is launched', () => {
          helper.executeAction('Delete account');
          helper.expectFocusedTaskScreenEventToHaveBeenFired({ data: { name: 'deleteAccountFocusedTaskScreen' } });
        });

        it('opens the modal when `Delete account` is pressed on focused task', () => {
          helper.expectMemberDeletionModalToBe({ isOpen: false });

          helper.executeAction('Delete account');
          helper.deleteAccount();

          helper.expectMemberDeletionModalToBe({ isOpen: true });
        });

        it('sends analytics event when `Delete account` is pressed on focused task', () => {
          helper.executeAction('Delete account');
          helper.deleteAccount();

          helper.expectAnalyticsEventToHaveBeenFired({
            data: {
              action: 'clicked',
              actionSubject: 'button',
              actionSubjectId: 'deleteAccount',
              source: 'deleteAccountFocusedTaskScreen',
            },
          }, 'ui');
        });

        it('sends analytics event when focused task closed', () => {
          helper.executeAction('Delete account');
          helper.expectFocusedTaskScreenEventToHaveBeenFired({ data: { name: 'deleteAccountFocusedTaskScreen' } });

          helper.closeFocusedTask();

          helper.expectCloseAccountFocusedTaskToBe({ isOpen: false });
          helper.expectAnalyticsEventToHaveBeenFired({
            data: {
              action: 'clicked',
              actionSubject: 'button',
              actionSubjectId: 'cancelDelete',
              source: 'deleteAccountFocusedTaskScreen',
            },
          }, 'ui');
        });

        describe('when Delete Account button is clicked on Modal Dialog', () => {
          it('hides the modal dialog', async () => {
            helper.executeAction('Delete account');
            helper.deleteAccount();
            helper.expectMemberDeletionModalToBe({ isOpen: true });

            await helper.confirmDelete();

            helper.expectMemberDeletionModalToBe({ isOpen: false });
          });

          it('calls the mutateCloseMemberAccount', async () => {
            helper.executeAction('Delete account');
            helper.deleteAccount();
            expect(mutateDeleteMember.callCount).to.be.eqls(0);

            await helper.confirmDelete();

            expect(mutateDeleteMember.callCount).to.be.eqls(1);
            expect(mutateDeleteMember.firstCall.args[0]).to.deep.include({
              variables: {
                memberId: params.params.memberId,
              },
              optimisticResponse: {
                closeMemberAccount: true,
              },
            });
          });

          it('invokes showFlag with correct parameters', async () => {
            helper.executeAction('Delete account');
            helper.deleteAccount();
            expect(showFlagSpy.callCount).to.equal(0);

            await helper.confirmDelete();

            expect(showFlagSpy.callCount).to.equal(1);

            const arg = showFlagSpy.firstCall.args[0];
            expect(arg.autoDismiss).to.be.true();
            expect(arg.title).to.equals('You scheduled the account for deletion');
            expect(arg.description).to.contain('Trash Turkey no longer has access to Atlassian account services');
          });

          describe('when success flag is shown', () => {
            let links;
            beforeEach(async () => {
              helper.executeAction('Delete account');
              helper.deleteAccount();
              await helper.confirmDelete();

              links = showFlagSpy.firstCall.args[0].actions;
            });

            it('renders cancel delete button', () => {
              expect(links).to.have.lengthOf(1);
              expect(links[0].content).to.equals('Cancel deletion');
              expect(links[0]).to.not.null();
            });

            it('clicking the cancel deletion link opens the dialog', () => {
              helper.expectCancelDeletionModalToBe({ isOpen: false });
              expect(links).to.have.lengthOf(1);

              links[0].onClick();
              helper.expectCancelDeletionModalToBe({ isOpen: true });
            });
          });

          it('closes the focused task', async () => {
            helper.executeAction('Delete account');
            helper.deleteAccount();
            helper.expectCloseAccountFocusedTaskToBe({ isOpen: true });

            await helper.confirmDelete();

            helper.expectCloseAccountFocusedTaskToBe({ isOpen: false });
          });

          describe('when API returns error', () => {
            it('handles the error correctly', async () => {
              const error = new RequestError({
                status: 500,
                message: 'test error',
              });
              mutateDeleteMember.returns(Promise.reject(error));
              expect(showFlagSpy.callCount).to.equal(0);

              await helper.confirmDelete();

              expect(showFlagSpy.callCount).to.equal(1);

              const arg = showFlagSpy.firstCall.args[0];
              expect(arg.autoDismiss).to.be.true();
              expect(arg.title).to.equals(errorFlagMessages.title.defaultMessage);
              expect(arg.description).to.equal(`We weren’t able to delete Trash Turkey\'s account. Please try again.`);
            });
          });
        });

        describe('when canClose returns errors', () => {
          beforeEach(() => {
            helper = new Helper(
              { loading: false, ...eveDisabledUserData },
              canCloseErrors,
              { memberDeletionFeatureFlag: enabledMemberDeletionFeatureFlag },
            );
          });

          it('opens the modal dialog', () => {
            helper.expectMemberDeletionModalToBe({ isOpen: false });

            helper.executeAction('Delete account');

            helper.expectMemberDeletionModalToBe({ isOpen: true });
          });
        });
      });

      describe('when memberDeletionFeatureFlag is enabled and the user is pending deletion', () => {

        let helper: Helper;

        beforeEach(() => {
          helper = new Helper(
            { loading: false, ...pendingDeletionUserData },
            canCloseDefault,
            {
              memberDeletionFeatureFlag: enabledMemberDeletionFeatureFlag,
            },
          );
        });

        it('renders cancel delete button', () => {
          helper.expectPageActionsToInclude({ action: 'Cancel deletion', isDisabled: false });
        });

        it('reactivate button should not be shown', () => {
          helper.expectPageActionsToNotInclude('Reactivate account');
        });

        describe('when Cancel deletion button is clicked on Modal Dialog', () => {
          it('hides the modal dialog', async () => {
            helper.executeAction('Cancel deletion');
            helper.expectCancelDeletionModalToBe({ isOpen: true });

            await helper.cancelDeletion();
            helper.expectCancelDeletionModalToBe({ isOpen: false });
          });

          it('calls the mutateCancelDeleteMemberAccount', async () => {
            expect(mutateCancelDeletion.callCount).to.equal(0);

            await helper.cancelDeletion();
            expect(mutateCancelDeletion.callCount).to.be.eqls(1);
            expect(mutateCancelDeletion.firstCall.args[0]).to.deep.include({
              variables: {
                memberId: params.params.memberId,
              },
              optimisticResponse: {
                cancelDeleteMemberAccount: true,
              },
            });
          });

          it('invokes showFlag with correct parameters', async () => {
            expect(showFlagSpy.callCount).to.equal(0);

            await helper.cancelDeletion();
            expect(showFlagSpy.callCount).to.equal(1);

            const arg = showFlagSpy.firstCall.args[0];
            expect(arg.autoDismiss).to.be.true();
            expect(arg.title).to.equals('You canceled the account deletion');
            expect(arg.description).to.contain('Trash Turkey has access to Atlassian account services again. We\'ll let them know by email.');
          });

          describe('when API returns error', () => {
            it('handles the error correctly', async () => {
              const error = new RequestError({
                status: 500,
                message: 'test error',
              });
              mutateCancelDeletion.returns(Promise.reject(error));
              expect(showFlagSpy.callCount).to.equal(0);

              await helper.cancelDeletion();

              expect(showFlagSpy.callCount).to.equal(1);

              const arg = showFlagSpy.firstCall.args[0];
              expect(arg.autoDismiss).to.be.true();
              expect(arg.title).to.equals(errorFlagMessages.title.defaultMessage);
              expect(arg.description).to.equal(`We weren’t able to cancel the deletion of Trash Turkey\'s account. Please try again.`);
            });
          });
        });

        it('delete button should not be shown', () => {
          helper.expectPageActionsToNotInclude('Delete account');
        });

        it('deactivate button should not be shown', () => {
          helper.expectPageActionsToNotInclude('Deactivate account');
        });

        it('reactivate button should not be shown in dropdown', () => {
          helper.expectPageActionsToNotInclude('Reactivate account');
        });

        it('user details submit button should not be primary', () => {
          helper.expectFormSubmitButtonToHaveDefaultAppearance();
        });
      });

      describe('when memberDeletionFeatureFlag is enabled and the user is inactive', () => {

        let helper;

        beforeEach(() => {
          helper = new Helper(
            { loading: false, ...disabledUserData },
            canCloseDefault,
            {
              memberDeletionFeatureFlag: enabledMemberDeletionFeatureFlag,
            },
          );
        });

        it('renders Reactivate account button', () => {
          helper.expectPageActionsToInclude({ action: 'Reactivate account', isDisabled: false });
        });

        it('clicking the "Reactivate account" opens the dialog', () => {
          helper.expectReactivateMemberModalToBe({ isOpen: false });
          helper.executeAction('Reactivate account');

          helper.expectReactivateMemberModalToBe({ isOpen: true });
        });

        describe('clicking "Reactivate account" in reactivate member modal should enable the account and close modal', () => {
          it('By calling member update, if deactivationViaFamFeatureFlag disabled', async () => {
            helper.expectMemberUpdateApiToNotHaveBeenCalled();
            await helper.reactivateMember();

            helper.expectMemberUpdateApiToHaveBeenCalledWith({
              enabled: true,
              id: 'DUMMY-TEST-ORG-ID',
              member: 'DUMMY-MEMBER-ID',
            });
            helper.expectAnalyticsEventToHaveBeenFired({
              subproduct: 'organization',
              tenantType: 'organizationId',
              tenantId: 'DUMMY-TEST-ORG-ID',
              actionSubject: 'enableOrgMemberButton',
              actionSubjectId: 'update',
              action: 'click',
            });

            helper.expectReactivateMemberModalToBe({ isOpen: false });
          });

          it('By calling member activate, if deactivationViaFamFeatureFlag enabled', async () => {
            helper = new Helper(
              { loading: false, ...disabledUserData },
              canCloseDefault,
              {
                memberDeletionFeatureFlag: enabledMemberDeletionFeatureFlag,
                memberDeactivationViaFamFeatureFlag: enabledMemberDeactivationViaFamFeatureFlag,
              },
            );

            helper.expectMemberUpdateApiToNotHaveBeenCalled();
            helper.expectMemberActivateApiNotToHaveBeenCalled();
            await helper.reactivateMember();

            helper.expectMemberUpdateApiToNotHaveBeenCalled();
            helper.expectMemberActivateApiToHaveBeenCalledWith({
              memberId: 'DUMMY-MEMBER-ID',
            });
            helper.expectAnalyticsEventToHaveBeenFired({
              subproduct: 'organization',
              tenantType: 'organizationId',
              tenantId: 'DUMMY-TEST-ORG-ID',
              actionSubject: 'enableOrgMemberButton',
              actionSubjectId: 'update',
              action: 'click',
            });

            helper.expectReactivateMemberModalToBe({ isOpen: false });
          });
        });

        it('delete button should be present in dropdown', () => {
          helper.expectPageActionsToInclude({ action: 'Delete account', isDisabled: false });
        });

        it('deactivate button should not be shown in dropdown', () => {
          helper.expectPageActionsToNotInclude('Deactivate account');
        });

        it('user details submit button should not be primary', () => {
          helper.expectFormSubmitButtonToHaveDefaultAppearance();
        });
      });

      describe('when memberDeletionFeatureFlag is enabled and the user is blocked by export controls', () => {

        let helper;

        beforeEach(() => {
          helper = new Helper(
            { loading: false, ...eveDisabledUserData },
            canCloseDefault,
            {
              memberDeletionFeatureFlag: enabledMemberDeletionFeatureFlag,
            },
          );
        });

        it('does not render Reactivate account button', () => {
          helper.expectPageActionsToNotInclude('Reactivate account');
        });

        it('delete button should be present in dropdown', () => {
          helper.expectPageActionsToInclude({ action: 'Delete account', isDisabled: false });
        });

        it('deactivate button should not be shown in dropdown', () => {
          helper.expectPageActionsToNotInclude('Deactivate account');
        });

        it('user details submit button should not be primary', () => {
          helper.expectFormSubmitButtonToHaveDefaultAppearance();
        });
      });
    });

    it('delete button should not be shown', () => {
      const helper = new Helper({ loading: false, ...userData(true) });

      helper.expectPageActionsToNotInclude('Delete account');
    });

    describe('if MFA is enforced', () => {
      it('dropdown has item  "Exclude from two-step-verification"', () => {
        const helper = new Helper({ loading: false, ...mfaEnforcedUserData() });

        helper.expectPageActionsToInclude({ action: 'Exclude from two-step verification', isDisabled: false });
      });

      it('if user is exempt then the dropdown has item is "Enforce two-step verification"', () => {
        const helper = new Helper({ loading: false, ...mfaExemptUserData });

        helper.expectPageActionsToInclude({ action: 'Enforce two-step verification', isDisabled: false });
      });

      it('pressing "Exclude from two-step verification" should remove enrollment and add user to whitelist', () => {
        const helper = new Helper({ loading: false, ...mfaEnforcedUserData() });

        helper.executeAction('Exclude from two-step verification');

        expect(mutateEnforcement.callCount).to.equal(1);
        expect(mutateEnrollment.callCount).to.equal(1);
      });

      it('pressing "Enforce two-step verification" should remove user from whitelist', () => {
        const helper = new Helper({ loading: false, ...mfaExemptUserData });

        helper.executeAction('Enforce two-step verification');

        expect(mutateEnforcement.callCount).to.equal(1);
        expect(mutateEnrollment.callCount).to.equal(0);
      });

      it('"Exclude from two-step verification" should be disabled if the user was provisioned by gsync', () => {
        const user = setProvisionedBy({ ...mfaExemptUserData }, 'GSYNC');
        const helper = new Helper({ loading: false, ...user });

        helper.expectPageActionsToInclude({ action: 'Enforce two-step verification', isDisabled: true });
      });

      it('"Enforce two-step verification" should be disabled if the user was provisioned by gsync', () => {
        const user = setProvisionedBy(mfaEnforcedUserData(), 'GSYNC');
        const helper = new Helper({ loading: false, ...user });

        helper.expectPageActionsToInclude({ action: 'Exclude from two-step verification', isDisabled: true });
      });
    });

    describe('if MFA is not enforced', () => {
      it('dropdown has item "Disable two-step verification" when user is enrolled to MFA', () => {
        const helper = new Helper({ loading: false, ...userData(true) });

        helper.expectPageActionsToInclude({ action: 'Disable two-step verification', isDisabled: false });
      });

      it('pressing "Disable two-step verification" should remove enrollment', () => {
        const helper = new Helper({ loading: false, ...userData(true) });

        helper.executeAction('Disable two-step verification');

        expect(mutateEnforcement.callCount).to.equal(0);
        expect(mutateEnrollment.callCount).to.equal(1);
      });

      it('action should be disabled if the user was provisioned by gsync', () => {
        const user = setProvisionedBy(userData(true), 'GSYNC');
        const helper = new Helper({ loading: false, ...user });

        helper.expectPageActionsToInclude({ action: 'Disable two-step verification', isDisabled: true });
      });
    });

    it('should disable form fields and options dropdown while updating', () => {
      const helper = new Helper({ loading: false, ...userData() });

      helper.startUpdating();

      helper.expectDisplayNameFieldStateToBe({ value: 'Trash Turkey', isDisabled: true });
      helper.expectEmailFieldStateToBe({ value: 'chicken@binjuice.com', isDisabled: true });
      helper.expectJobTitleFieldStateToBe({ value: 'Garbage Collector', isDisabled: true });
      helper.expectAllActionsToBeDisabled();
    });

    describe('field mutability constraints', () => {
      describe('for scim-provisioned users', () => {
        it('display name field should be disabled accordingly', () => {
          const user = setProvisionedBy(userData(), 'SCIM');
          const helper = new Helper({ loading: false, ...user });

          helper.expectDisplayNameFieldStateToBe({ value: 'Trash Turkey', isDisabled: true });
        });

        it('email field should be disabled accordingly', () => {
          const user = setProvisionedBy(userData(), 'SCIM');
          const helper = new Helper({ loading: false, ...user });

          helper.expectEmailFieldStateToBe({ value: 'chicken@binjuice.com', isDisabled: true });
        });

        it('job title field should be disabled accordingly', () => {
          const user = setProvisionedBy(userData(), 'SCIM');
          const helper = new Helper({ loading: false, ...user });

          helper.expectJobTitleFieldStateToBe({ value: 'Garbage Collector', isDisabled: true });
        });

        it('submit button should be disabled if all form fields are', () => {
          const user = setProvisionedBy(userData(), 'SCIM');
          const helper = new Helper({ loading: false, ...user });

          helper.expectFormSubmitButtonToBeDisabled();
        });
      });

      describe('for gsync-provisioned users', () => {
        it('display name field should be disabled accordingly', () => {
          const user = setProvisionedBy(userData(), 'GSYNC');
          const helper = new Helper({ loading: false, ...user });

          helper.expectDisplayNameFieldStateToBe({ value: 'Trash Turkey', isDisabled: true });
        });

        it('email field should be disabled accordingly', () => {
          const user = setProvisionedBy(userData(), 'GSYNC');
          const helper = new Helper({ loading: false, ...user });

          helper.expectEmailFieldStateToBe({ value: 'chicken@binjuice.com', isDisabled: true });
        });
      });
    });

    describe('and has API tokens', () => {
      let helper: Helper;

      beforeEach(() => {
        helper = new Helper(
          { loading: false, ...eveDisabledUserData },
          canCloseDefault,
          {
            memberApiTokenFeatureFlag: enableMemberApiTokenFeatureFlag,
          },
        );
      });

      it('should show the table', () => {
        helper.expectApiTokensToBePresent();
      });
    });
  });

  describe('if deleting a member', () => {
    it('deactivate handler is set for active users', () => {
      const user = userData();
      const helper = new Helper({ loading: false, ...user });
      const focusedCloseAccountWrapper = helper.publicWrapper.find(AkFocusedTaskCloseAccount).last();
      const screenEventSender: any = focusedCloseAccountWrapper.props().screens[0];
      const deleteUserOverviewScreen = screenEventSender.props.children;
      expect(deleteUserOverviewScreen.props.deactivateUserHandler).not.to.be.undefined();
      expect(deleteUserOverviewScreen.props.isUserDeactivated).equals(false);
    });

    it('sends analytics event when deactivate handler executed', () => {
      const user = userData();
      const helper = new Helper({ loading: false, ...user });
      const focusedCloseAccountWrapper = helper.publicWrapper.find(AkFocusedTaskCloseAccount).last();
      const screenEventSender: any = focusedCloseAccountWrapper.props().screens[0];
      const deleteUserOverviewScreen = screenEventSender.props.children;

      deleteUserOverviewScreen.props.deactivateUserHandler();

      helper.expectAnalyticsEventToHaveBeenFired({
        data: {
          action: 'clicked',
          actionSubject: 'link',
          actionSubjectId: 'deactivateAccountLink',
          source: 'deleteAccountFocusedTaskScreen',
        },
      }, 'ui');
    });

    it('deactivate handler is not set for inactive users', () => {
      const helper = new Helper({ loading: false, ...disabledUserData });
      const focusedCloseAccountWrapper = helper.publicWrapper.find(AkFocusedTaskCloseAccount).last();
      const screenEventSender: any = focusedCloseAccountWrapper.props().screens[0];
      const deleteUserOverviewScreen = screenEventSender.props.children;
      expect(deleteUserOverviewScreen.props.deactivateUserHandler).not.to.be.undefined();
      expect(deleteUserOverviewScreen.props.isUserDeactivated).equals(true);

    });

    it('then deactivate chosen, the deactivate dialog should appear', () => {
      const user = userData();
      const helper = new Helper({ loading: false, ...user });
      const focusedCloseAccountWrapper = helper.publicWrapper.find(AkFocusedTaskCloseAccount).last();
      const screenEventSender: any = focusedCloseAccountWrapper.props().screens[0];
      const deleteUserOverviewScreen = screenEventSender.props.children;

      expect(deleteUserOverviewScreen.props.deactivateUserHandler).not.to.be.undefined();

      deleteUserOverviewScreen.props.deactivateUserHandler();

      helper.expectCloseAccountFocusedTaskToBe({ isOpen: false });
      helper.expectDeactivateAccountFocusedTaskToBe({ isOpen: true });
    });
  });

  describe('if updating a member', () => {
    it('sends the correct data when updating', () => {
      const user = userData();
      const helper = new Helper({ loading: false, ...user });

      helper.updateAccount({ email: 'joe@cupof.com', displayName: 'joe', jobTitle: '' });
    });

    it('does enforce a 255 limit on displayName', () => {
      const helper = new Helper({ loading: false, ...userData() });
      const nameInput = helper.publicWrapper.find(AkFieldTextStateless).at(0);

      expect(nameInput.prop('maxLength')).to.equal(255);
    });

    describe('when email update is successful', () => {
      it('shows a success flag', async () => {
        const user = userData();
        const helper = new Helper({ loading: false, ...user });

        helper.updateAccount({ email: 'joe@cupof.com', displayName: 'joe', jobTitle: '' });

        await waitUntil(() => showFlagSpy.called);

        expect(showFlagSpy.callCount).to.equal(1);
      });

      it('displays the correct information on the flag', async () => {
        const user = userData();
        const helper = new Helper({ loading: false, ...user });

        helper.updateAccount({ email: 'joe@cupof.com', displayName: 'joe', jobTitle: '' });

        await waitUntil(() => showFlagSpy.called);

        const arg = showFlagSpy.firstCall.args[0];
        expect(arg.autoDismiss).to.be.true();
        expect(arg.title).to.equals('You\'ve updated this account\'s details.');
      });
    });

    describe('when email update is unsuccessful', () => {
      it(('correctly resolves promise rejection'), async () => {
        const user = userData();
        const helper = new Helper({ loading: false, ...user });
        const error = new RequestError({ status: 500, title: 'test error' });

        helper.setupMutateToFail(error);
        helper.updateAccount({ email: 'joe@cupof.com', displayName: 'joe', jobTitle: '' });

        await waitUntil(() => showFlagSpy.called);
        const arg = showFlagSpy.firstCall.args[0];

        expect(showFlagSpy.callCount).to.equal(1);
        expect(arg.autoDismiss).to.be.true();
        expect(arg.title).to.equals(errorFlagMessages.title.defaultMessage);
      });

      it('opens a validation error message with a status 412 error', async () => {
        const user = userData();
        const helper = new Helper({ loading: false, ...user });
        const error = new PreconditionFailedError({ status: 412 });

        helper.setupMutateToFail(error);

        helper.updateAccount({ email: 'joe@cupof.com', displayName: 'joe', jobTitle: '' });

        await waitUntil(() => mutateStub.called);

        helper.publicWrapper.update();

        const validationError = helper.publicWrapper.find(ValidationMessage);

        expect(validationError.props().hasValidationError).to.equal(true);
        expect(showFlagSpy.callCount).to.equal(0);
      });
    });

    describe('Member access sites', () => {
      it('Should return an empty list of sites when there is a member access error', () => {
        const memberAccessInfo: MemberAccess = {
          sites: [
            {
              isSiteAdmin: false,
              siteUrl: 'rachel2.atlassian.net',
            },
          ],
          products: [
            {
              siteUrl: 'rachel2.atlassian.net',
              productName: 'Jira',
              productKey: 'jira-software.ondemand',
            },
          ],
          errors: ['error'],
        };

        const result = getMemberAccessibleSites(memberAccessInfo, true);
        expect(result.length).to.equal(0);
      });

      it('Should return an empty list of sites if memberAccess does not exist', () => {
        const result = getMemberAccessibleSites(null, false);
        expect(result).to.deep.equal([]);
      });

      it('Should return a list of unique sites per product', () => {
        const memberAccessInfo: MemberAccess = {
          sites: [
            {
              isSiteAdmin: false,
              siteUrl: 'rachel2.atlassian.net',
            },
            {
              isSiteAdmin: false,
              siteUrl: 'rachel3.atlassian.net',
            },
          ],
          products: [
            {
              siteUrl: 'rachel2.atlassian.net',
              productName: 'Jira',
              productKey: 'jira-core.ondemand',
            },
            {
              siteUrl: 'rachel2.atlassian.net',
              productName: 'Confluence',
              productKey: 'jira-core.ondemand',
            },
            {
              siteUrl: 'rachel3.atlassian.net',
              productName: 'Jira Core',
              productKey: 'jira-core.ondemand',
            },
            {
              siteUrl: 'bitbucket.org',
              productName: 'Bitbucket',
              productKey: 'bitbucket.cloud',
            },
          ],
          errors: [],
        };

        const result = getMemberAccessibleSites(memberAccessInfo, false);
        expect(result).to.deep.equal(['rachel2.atlassian.net', 'rachel3.atlassian.net', 'bitbucket.org']);
      });
    });
  });
});
