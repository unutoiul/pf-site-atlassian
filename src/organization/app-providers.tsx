import * as React from 'react';
import { Provider } from 'react-redux';
import { Store } from 'redux';

import { AnalyticsListener as AkAnalyticsListener } from '@atlaskit/analytics';
import { SpotlightManager as AkSpotlightManager } from '@atlaskit/onboarding';

import { analyticsClient, AnalyticsWebClient, NewAnalyticsContext } from 'common/analytics';
import { FlagProvider, FlagRenderer } from 'common/flag/flag-provider';
import { ComponentTimingRoot, newRelicReporter } from 'common/performance-metrics';
import { MediaProvider } from 'common/responsive/Matcher';

import { RedirectFunc } from '../apollo-client';
import { ApolloProviderWrapper } from '../apollo-client/apollo-provider-wrapper';
import { I18nProvider } from '../i18n/i18n-provider/i18n-provider';
import { createStore, RootState } from '../store';

interface Props {
  redirectToLogin?: RedirectFunc;
}

export class AppProviders extends React.Component<Props, {}> {
  private analyticsClient = analyticsClient;
  private newAnalyticsClient = AnalyticsWebClient.create();
  public store: Store<RootState> = createStore();

  public componentDidMount() {
    this.newAnalyticsClient.init();
    analyticsClient.onPageLoad();
    analyticsClient.onPageAction({
      name: 'page.version',
      attributes: {
        version: window.__build_version__ || 'UNKNOWN',
      },
    });
  }

  public render() {
    return (
      <FlagProvider>
        <Provider store={this.store}>
          <ApolloProviderWrapper redirectToLogin={this.props.redirectToLogin}>
            <ComponentTimingRoot reporter={newRelicReporter}>
              <AkAnalyticsListener onEvent={this.onAnalyticsEvent}>
                <NewAnalyticsContext.Provider value={this.newAnalyticsClient}>
                  <I18nProvider>
                    <MediaProvider>
                      <AkSpotlightManager>
                        {this.props.children}
                      </AkSpotlightManager>
                      <FlagRenderer/>
                    </MediaProvider>
                  </I18nProvider>
                </NewAnalyticsContext.Provider>
              </AkAnalyticsListener>
            </ComponentTimingRoot>
          </ApolloProviderWrapper>
        </Provider>
      </FlagProvider>
    );
  }

  private onAnalyticsEvent = (_, data) => {
    this.analyticsClient.onEvent(data);
  };
}
