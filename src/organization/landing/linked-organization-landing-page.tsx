import * as React from 'react';
import { graphql, OptionProps, QueryOpts, QueryResult } from 'react-apollo';
import { defineMessages, FormattedMessage } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';
import styled from 'styled-components';

import AkSpinner from '@atlaskit/spinner';
import { gridSize as akGridSize } from '@atlaskit/theme';

import { orgOverviewScreen } from 'common/analytics';
import { GenericError } from 'common/error';
import { PageLayout } from 'common/page-layout';

import { LinkedOrgLandingPageQuery, LinkedOrgLandingPageQueryVariables } from '../../schema/schema-types';
import { OrgBreadcrumbs } from '../breadcrumbs';
import { MemberTotal } from '../member-page/member-total';
import { OrgNameForm } from '../organization-name-form/organization-name-form';
import linkedOrganizationLandingPageQuery from './linked-organization-landing-page.query.graphql';
import { LinkedSites } from './linked-sites';

const messages = defineMessages({
  description: {
    id: 'linked.organization.landing.description',
    defaultMessage: 'Centrally manage authentication, authorization and security policies for your organization.',
    description: 'Description for overview page of an organization.',
  },
  managedAccounts: {
    id: 'linked.organization.landing.managed.accounts',
    defaultMessage: 'Managed Accounts',
    description: 'Title of the section showing information about the accounts managed via domain claim.',
  },
});

const ManagedAccountsTitle = styled.div`
  padding-bottom: ${akGridSize}px;
`;

const Header = styled.div`
  padding-bottom: ${akGridSize() * 5}px;
`;

interface DerivedProps {
  loading: boolean;
  error?: QueryResult['error'];
  data?: {
    name: string,
    totalMembers: number | undefined;
    totalDisabledMembers: number | undefined;
  };
}

type Props = RouteComponentProps<{ orgId: string }> & DerivedProps;

export class LinkedOrganizationLandingPageImpl extends React.Component<Props> {
  public render() {
    if (this.props.loading || this.props.error || !this.props.data) {
      return (
        <PageLayout
          breadcrumbs={<OrgBreadcrumbs />}
        >
          {this.props.error ? (
            <GenericError />
          ) : (
            <AkSpinner />
          )}
        </PageLayout>
      );
    }

    return (
      <PageLayout
        title={<OrgNameForm name={this.props.data.name} orgId={this.props.match.params.orgId} analyticsSource={orgOverviewScreen} />}
        description={<FormattedMessage {...messages.description} />}
        breadcrumbs={<OrgBreadcrumbs />}
        isFullWidth={true}
      >
        <Header>
          <ManagedAccountsTitle>
            <FormattedMessage {...messages.managedAccounts} tagName="h4" />
          </ManagedAccountsTitle>
          <MemberTotal total={this.props.data.totalMembers} disabledTotal={this.props.data.totalDisabledMembers} />
        </Header>
        <LinkedSites />
      </PageLayout>
    );
  }
}

function getDataProps(componentProps: OptionProps<{}, LinkedOrgLandingPageQuery>): DerivedProps {
  if (!componentProps || !componentProps.data || componentProps.data.loading) {
    return { loading: true };
  }

  if (componentProps.data.error || !componentProps.data.organization) {
    return { loading: false, error: componentProps.data.error };
  }

  const {
    name,
    users: {
      memberTotal,
    },
  } = componentProps.data.organization;

  return {
    loading: false,
    data: {
      name,
      totalMembers: !memberTotal || memberTotal.total < 0 ? undefined : memberTotal.total,
      totalDisabledMembers: !memberTotal || memberTotal.disabledTotal < 0 ? undefined : memberTotal.disabledTotal,
    },
  };
}

const withQuery = graphql<RouteComponentProps<any>, LinkedOrgLandingPageQuery, LinkedOrgLandingPageQueryVariables, DerivedProps>(linkedOrganizationLandingPageQuery, {
  options: ({ match: { params: { orgId } } }): QueryOpts<LinkedOrgLandingPageQueryVariables> => ({
    variables: { id: orgId },
  }),
  props: getDataProps,
  skip: ({ match: { params: { orgId } } }) => !orgId,
});

export const LinkedOrganizationLandingPage = withRouter<{}>(withQuery(LinkedOrganizationLandingPageImpl));
