// tslint:disable jsx-use-translation-function
import { expect } from 'chai';
import { mount, shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import AkSpinner from '@atlaskit/spinner';

import { analyticsClient, AnalyticsData } from 'common/analytics';
import { Button as AnalyticsButton } from 'common/analytics/components';
import { PageLayout } from 'common/page-layout';
import { CompletedIcon, DeclarativeProgressTracker, IncompleteIcon, ProgressState, SelectedIcon } from 'common/progress-tracker';

import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import { OrganizationLandingPageImpl, OrganizationLandingPageProps } from './organization-landing-page';

declare interface ProgressAndLoadingState {
  state: ProgressState;
  loading?: boolean;
}

describe('Organization Landing Page', () => {
  let currentOrgs;
  let currentDomains;
  let currentProducts;
  let analyticsSpy: sinon.SinonSpy;
  let sandbox: sinon.SinonSandbox;

  class OrganizationLandingPageHelper {
    private wrapper: ShallowWrapper<OrganizationLandingPageProps, {}>;

    constructor(currentOrgsData, currentDomainsData, currentProductsData) {
      this.wrapper = organizationLandingPage(currentOrgsData, currentDomainsData, currentProductsData);
    }

    public expectToHaveSpinner(): void {
      expect(this.spinner).to.have.length(1);
    }

    public expectPageTitleToBe(title: string): any {
      expect(this.pageTitle).to.equal(title);
    }

    public expectPageDescriptionToContain(text: string): any {
      expect(this.pageDescription.text()).to.include(text);
    }

    public updateProps<K extends keyof OrganizationLandingPageProps>(props: Pick<OrganizationLandingPageProps, K>) {
      this.wrapper.setProps(props);
    }

    public expectProgressItemsToBeInState(itemState1: ProgressState, itemState2: ProgressState, itemState3: ProgressState): void {
      function toLoadingState(s: ProgressState) {
        return { state: s, loading: s === ProgressState.Incomplete };
      }
      this.expectProgressItemsToBe(toLoadingState(itemState1), toLoadingState(itemState2), toLoadingState(itemState3));
    }

    public expectProgressItemsToBe(itemState1: ProgressAndLoadingState, itemState2: ProgressAndLoadingState, itemState3: ProgressAndLoadingState): void {
      this.expectProgressItemToBe(this.progressItems.at(0), itemState1, 'OrgProgress');
      this.expectProgressItemToBe(this.progressItems.at(1), itemState2, 'DomainProgress');
      this.expectProgressItemToBe(this.progressItems.at(2), itemState3, 'IdentityManagerProgress');
    }

    public expectPageDescriptionToContainLearnMoreButtonWithAnalytics(analyticsData: AnalyticsData): any {
      expect(this.analyticsButton.props().analyticsData).to.deep.equal(analyticsData);
    }

    // tslint:disable-next-line:prefer-function-over-method
    private expectProgressItemToBe(item: ShallowWrapper<{}, {}>, state: ProgressAndLoadingState, itemName: string) {
      switch (state.state) {
        case ProgressState.Incomplete: {
          expect(item.find(IncompleteIcon)).to.have.length(1, `expected an item "${itemName}" to be in "Incomplete" state`);
          if (state.loading === true) {
            expect(item.find(AkSpinner)).to.have.length(1, `expected an incomplete item "${itemName}" to have a loading spinner`);
          }
          break;
        }
        case ProgressState.Completed: {
          expect(item.find(CompletedIcon)).to.have.length(1, `expected an item "${itemName}" to be in "Completed" state`);
          break;
        }
        case ProgressState.Selected: {
          expect(item.find(SelectedIcon)).to.have.length(1, `expected an item "${itemName}" to be in "Selected" state`);
          break;
        }
        default: {
          throw new Error('did you forget to add a case?');
        }
      }
    }

    private get progressItems() {
      return this.wrapper.find(DeclarativeProgressTracker).children();
    }

    private get spinner(): ShallowWrapper<{}, {}> {
      return this.wrapper.find(AkSpinner);
    }

    private get pageTitle() {
      return this.wrapper.find(PageLayout).props().title;
    }

    private get analyticsButton() {
      return this.pageDescription.find(AnalyticsButton);
    }

    private get pageDescription() {
      const description = this.wrapper.find(PageLayout).props().description;

      return mount(description, createMockIntlContext());
    }
  }

  beforeEach(() => {
    sandbox = sinon.sandbox.create();
  });

  afterEach(() => {
    sandbox.restore();
  });

  function organizationLandingPage(currentOrgsData, currentDomainsData, currentProductsData) {
    return shallow(
      <OrganizationLandingPageImpl
        currentOrgs={{ ...currentOrgsData }}
        currentDomains={{ ...currentDomainsData }}
        currentProducts={{ ...currentProductsData }}
        match={{
          params: { orgId: 'DUMMY_ORG_ID' },
        } as any}
        location={{} as any}
        history={{} as any}
        intl={createMockIntlProp()}
      />,
      createMockIntlContext(),
    );
  }

  beforeEach(() => {
    currentOrgs = {
      loading: false,
      currentUser: {
        organizations: [{
          id: 'DUMMY_ORG_ID',
          name: 'Org Name',
        }],
      },
    };
    currentDomains = { loading: true };
    currentProducts = { loading: true };
  });

  describe('Check Organization Progress', () => {
    it('Should show Spinner if still loading Org Data', () => {
      currentOrgs.loading = true;
      const helper = new OrganizationLandingPageHelper(currentOrgs, currentDomains, currentProducts);

      helper.expectToHaveSpinner();
    });

    it('Should send analytics after finished loading page', () => {
      analyticsSpy = sandbox.spy(analyticsClient, 'onEvent');
      currentOrgs.loading = true;
      const helper = new OrganizationLandingPageHelper(currentOrgs, currentDomains, currentProducts);
      expect(analyticsSpy.called).to.equal(false);
      helper.expectToHaveSpinner();

      currentOrgs.loading = false;

      helper.updateProps({ currentOrgs: { ...currentOrgs } });

      expect(analyticsSpy.called).to.equal(true);
      expect(analyticsSpy.firstCall.args[0]).to.deep.equal({
        subproduct: 'organization',
        tenantId: 'DUMMY_ORG_ID',
        tenantType: 'organizationId',
        action: 'pageLoad',
        actionSubject: 'organizationPageLoad',
        actionSubjectId: 'organizationLandingPage',
      });
    });

    it('Should show title and description if finished loading Org Data', () => {
      const helper = new OrganizationLandingPageHelper(currentOrgs, currentDomains, currentProducts);

      helper.expectPageTitleToBe('Org Name');
      helper.expectPageDescriptionToContain('Create an organization and verify your domains so that');
      helper.expectPageDescriptionToContain('Learn about organizations and Atlassian Access.');
      helper.expectPageDescriptionToContainLearnMoreButtonWithAnalytics({
        subproduct: 'organization',
        tenantId: 'DUMMY_ORG_ID',
        tenantType: 'organizationId',
        action: 'click',
        actionSubject: 'orgDetailsLearnMoreButton',
        actionSubjectId: 'openLink',
      });
    });

    it('Items should show spinner if still loading domains and products', () => {
      const helper = new OrganizationLandingPageHelper(currentOrgs, currentDomains, currentProducts);

      helper.expectProgressItemsToBeInState(ProgressState.Completed, ProgressState.Incomplete, ProgressState.Incomplete);
    });

    it('Should show domain claim progress as Selected if there are no domains', () => {
      currentDomains = {
        loading: false,
        organization: {
          domainClaim: {
            domains: [],
          },
        },
      };

      const helper = new OrganizationLandingPageHelper(currentOrgs, currentDomains, currentProducts);

      helper.expectProgressItemsToBeInState(ProgressState.Completed, ProgressState.Selected, ProgressState.Incomplete);
    });

    it('Should show domain claim progress as Selected if there are no verified domains', () => {
      currentDomains = {
        loading: false,
        organization: {
          domainClaim: {
            domains: [{
              domain: 'cool.com',
              verified: false,
              status: 'UNVERIFIED',
              verificationType: 'DNS',
            }],
          },
        },
      };
      const helper = new OrganizationLandingPageHelper(currentOrgs, currentDomains, currentProducts);

      helper.expectProgressItemsToBeInState(ProgressState.Completed, ProgressState.Selected, ProgressState.Incomplete);
    });

    it('Should show domain claim progress as Selected if there are no verified domain and identity-manager', () => {
      currentDomains = {
        loading: false,
        organization: {
          domainClaim: {
            domains: [{
              domain: 'cool.com',
              verified: false,
              status: 'UNVERIFIED',
              verificationType: 'DNS',
            }],
          },
        },
      };
      currentProducts = {
        loading: false,
        products: {
          identityManager: true,
        },
      };
      const helper = new OrganizationLandingPageHelper(currentOrgs, currentDomains, currentProducts);

      helper.expectProgressItemsToBeInState(ProgressState.Completed, ProgressState.Selected, ProgressState.Completed);
    });

    it('Should show domain claim progress as Selected if there are no verified domain and NO identity-manager', () => {
      currentDomains = {
        loading: false,
        organization: {
          domainClaim: {
            domains: [{
              domain: 'cool.com',
              verified: false,
              status: 'UNVERIFIED',
              verificationType: 'DNS',
            }],
          },
        },
      };
      currentProducts = {
        loading: false,
        products: {
          identityManager: false,
        },
      };

      const helper = new OrganizationLandingPageHelper(currentOrgs, currentDomains, currentProducts);

      helper.expectProgressItemsToBe({ state: ProgressState.Completed }, { state: ProgressState.Selected }, { state: ProgressState.Incomplete, loading: false });
    });

    it('Should show idManager progress as Selected if there are verified domains and NO identity-manager', () => {
      currentDomains = {
        loading: false,
        organization: {
          domainClaim: {
            domains: [{
              domain: 'cool.com',
              verified: true,
              status: 'VERIFIED',
              verificationType: 'DNS',
            }],
          },
        },
      };
      currentProducts = {
        loading: false,
        products: {
          identityManager: false,
        },
      };

      const helper = new OrganizationLandingPageHelper(currentOrgs, currentDomains, currentProducts);

      helper.expectProgressItemsToBeInState(ProgressState.Completed, ProgressState.Completed, ProgressState.Selected);
    });

    it('Should show all completed when verified domains and purchased identity manager', () => {
      currentDomains = {
        loading: false,
        organization: {
          domainClaim: {
            domains: [{
              domain: 'cool.com',
              verified: true,
              status: 'VERIFIED',
              verificationType: 'DNS',
            }],
          },
        },
      };
      currentProducts = {
        loading: false,
        products: {
          identityManager: true,
        },
      };

      const helper = new OrganizationLandingPageHelper(currentOrgs, currentDomains, currentProducts);

      helper.expectProgressItemsToBeInState(ProgressState.Completed, ProgressState.Completed, ProgressState.Completed);
    });
  });
});
