import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';
import { MemoryRouter as Router } from 'react-router-dom';

import { createApolloClient } from '../../apollo-client';
import { OrgSite } from '../../schema/schema-types';
import { LinkedSitesImpl } from './linked-sites';

const site1: OrgSite = {
  id: 'site-1-id',
  displayName: 'acme-usa',
  siteUrl: 'acme-usa.atlassian.net',
  avatar: '',
  products: [
    {
      key: 'jira-software',
      name: 'Jira Software',
    },
    {
      key: 'confluence',
      name: 'Confluence',
    },
  ],
};

const site2: OrgSite = {
  id: 'site-2-id',
  displayName: 'acme-asia',
  siteUrl: 'acme-asia.atlassian.net',
  avatar: '',
  products: [
    {
      key: 'jira-service-desk',
      name: 'Jira Service Desk',
    },
    {
      key: 'confluence',
      name: 'Confluence',
    },
    {
      key: 'stride',
      name: 'Stride',
    },
  ],
};

const dummyProps = {
  match: { params: { orgId: 'my-org' } } as any,
  location: {} as any,
  history: {} as any,
  analyticsClient: { sendUIEvent: (args) => action('analytics')(args) } as any,
  showDrawer: action('Open Drawer'),
};

const client = createApolloClient();

storiesOf('Organization|Landing Page/Linked Sites', module)
  .add('Loading', () => (
    <IntlProvider locale="en">
      <Router>
        <LinkedSitesImpl
          linkedSitesData={{ loading: true }}
          {...dummyProps}
        />
      </Router>
    </IntlProvider>
  ))
  .add('Default', () => (
    <ApolloProvider client={client}>
      <IntlProvider locale="en">
        <Router>
          <LinkedSitesImpl
            linkedSitesData={{ loading: false, sites: [site1, site2] } as any}
            {...dummyProps}
          />
        </Router>
      </IntlProvider>
    </ApolloProvider>
  ))
  .add('Error', () => (
    <IntlProvider locale="en">
      <Router>
        <LinkedSitesImpl
          linkedSitesData={{ loading: false, error: 'Something went wrong' as any }}
          {...dummyProps}
        />
      </Router>
    </IntlProvider>
  ));
