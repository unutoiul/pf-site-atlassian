import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import AkSpinner from '@atlaskit/spinner';

import { LinkedOrganizationLandingPage } from './linked-organization-landing-page';
import { OrgMasterLandingPageImpl } from './organization-landing-master-page';
import { OrganizationLandingPage } from './organization-landing-page';

describe('Organization Master Landing Page', () => {
  describe('master page logic', () => {
    function createMasterPageWrapper(hasError: boolean, loading: boolean, hasSites: boolean | undefined) {
      return shallow(
        (
          <OrgMasterLandingPageImpl
            history={null as any}
            location={null as any}
            match={null as any}
            linkedSitesData={{
              error: hasError ? {} as any : undefined,
              loading,
              sites: hasSites === undefined
                ? undefined
                : hasSites === true
                  ? ['some-site'] as any
                  : [],
            }}
          />
        ),
      );
    }

    it('should render a spinner while sites data is still loading', () => {
      const wrapper = createMasterPageWrapper(false, true, undefined);

      expect(wrapper.find(AkSpinner)).to.have.lengthOf(1);
    });

    it('should fall back to "regular" org landing page in case of an error', () => {
      const wrapper = createMasterPageWrapper(true, false, undefined);

      expect(wrapper.find(OrganizationLandingPage)).to.have.lengthOf(1);
    });

    it('should render "regular" org landing page if sites are unavailable', () => {
      const wrapper = createMasterPageWrapper(false, false, undefined);

      expect(wrapper.find(OrganizationLandingPage)).to.have.lengthOf(1);
    });

    it('should render "regular" org landing page if there are no linked sites', () => {
      const wrapper = createMasterPageWrapper(false, false, false);

      expect(wrapper.find(OrganizationLandingPage)).to.have.lengthOf(1);
    });

    it('should render org landing page with linked sites info if an org has site linked to it', () => {
      const wrapper = createMasterPageWrapper(false, false, true);

      expect(wrapper.find(LinkedOrganizationLandingPage)).to.have.lengthOf(1);
    });
  });
});
