import * as React from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';
import styled from 'styled-components';

import AkButton from '@atlaskit/button';
import AkSpinner from '@atlaskit/spinner';

import { gridSize as akGridSize } from '@atlaskit/theme';

import { AnalyticsClientProps, orgOverviewAddSiteEventData, orgOverviewScreen, orgOverviewScreenEvent, ScreenEventSender, withAnalyticsClient } from 'common/analytics';
import { ShowDrawerProps, withShowDrawer } from 'common/drawer';
import { GenericErrorMessage } from 'common/error';
import { SiteCard } from 'common/site-card';

import { OrgRouteProps } from '../routes';
import { LinkedSitesProps, withLinkedSites } from '../with-linked-sites';

const messages = defineMessages({
  title: {
    id: 'overview.linked.sites.title',
    defaultMessage: 'Products',
    description: 'This will be title of the section showing sites linked to an organization and the products on those sites.',
  },
  addSite: {
    id: 'overview.add.site',
    defaultMessage: 'Add a site',
    description: 'This is a button used to add a site to an organization.',
  },
});

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-bottom: ${akGridSize() * 2}px;
`;

export class LinkedSitesImpl extends React.Component<LinkedSitesProps & RouteComponentProps<OrgRouteProps> & ShowDrawerProps & AnalyticsClientProps> {
  public render() {
    const { linkedSitesData, match: { params: { orgId } } } = this.props;
    if (linkedSitesData.loading) {
      return <AkSpinner />;
    }

    if (linkedSitesData.error || !linkedSitesData.sites) {
      return <GenericErrorMessage />;
    }

    return (
      <ScreenEventSender event={orgOverviewScreenEvent(orgId, 'withSite', linkedSitesData.sites.length)}>
        <Header>
          <FormattedMessage {...messages.title} tagName="h3" />
          <AkButton onClick={this.openOrgSiteLinkingDrawer} appearance="primary">
            <FormattedMessage {...messages.addSite} />
          </AkButton>
        </Header>
        {
          linkedSitesData.sites.map(site => (
            <SiteCard key={site.id} site={site} orgId={orgId} screen={orgOverviewScreen} />
          ))
        }
      </ScreenEventSender>
    );
  }

  private openOrgSiteLinkingDrawer = () => {
    const { analyticsClient, match: { params: { orgId } } } = this.props;

    analyticsClient.sendUIEvent({
      orgId,
      data: orgOverviewAddSiteEventData(),
    });
    this.props.showDrawer('ORG_SITE_LINKING');
  }
}

export const LinkedSites: React.ComponentClass = withRouter(
  withLinkedSites(
    withShowDrawer(
      withAnalyticsClient(
        LinkedSitesImpl,
      ),
    ),
  ),
);
