// tslint:disable jsx-use-translation-function react-this-binding-issue
import { storiesOf } from '@storybook/react';
import { ApolloError } from 'apollo-client';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';
import { MemoryRouter as Router, Route } from 'react-router-dom';

import AkPage from '@atlaskit/page';

import { Organization, OrgSite } from '../../schema/schema-types';
import { createApolloClientWithOverriddenResolvers } from '../../utilities/testing';
import { LinkedOrganizationLandingPageImpl } from './linked-organization-landing-page';

const testOrgId = 'DUMMY_ORG_ID';
const site1: OrgSite = {
  id: 'site-1-id',
  displayName: 'acme-usa',
  siteUrl: 'acme-usa.atlassian.net',
  avatar: '',
  products: [
    {
      key: 'jira-software',
      name: 'Jira Software',
    },
    {
      key: 'confluence',
      name: 'Confluence',
    },
  ],
};

const site2: OrgSite = {
  id: 'site-2-id',
  displayName: 'acme-asia',
  siteUrl: 'acme-asia.atlassian.net',
  avatar: '',
  products: [
    {
      key: 'jira-service-desk',
      name: 'Jira Service Desk',
    },
    {
      key: 'confluence',
      name: 'Confluence',
    },
    {
      key: 'stride',
      name: 'Stride',
    },
  ],
};

const resolverOrgData: { organization: Partial<Organization> } = { organization: {} };
const client = createApolloClientWithOverriddenResolvers({
  Query: {
    organization: async () => resolverOrgData.organization,
  },
  Organization: {
    id: async () => resolverOrgData.organization.id,
    name: async () => resolverOrgData.organization.name,
    users: async () => resolverOrgData.organization.users,
    sites: async () => resolverOrgData.organization.sites,
  },
});

const StorybookLinkedOrganizationLandingPage = ({
  data,
  error,
  loading,
}: {
} & Pick<LinkedOrganizationLandingPageImpl['props'], 'loading' | 'data' | 'error'>) => {
  class TestComp extends React.Component<{}, { configured: boolean }> {
    public state = { configured: false };

    public async componentDidMount() {
      await client.resetStore();
      await client.cache.reset();

      resolverOrgData.organization = {
        id: testOrgId,
        name: data && data.name,
        users: {
          members: {
            total: data && data.totalMembers,
            users: data && data.totalDisabledMembers,
          },
        },
        sites: [
          site1,
          site2,
        ],
      } as any;

      this.setState({ configured: true });
    }

    public render() {
      if (!this.state.configured) {
        return <h1>Please wait, configuring Apollo</h1>;
      }

      return (
        <ApolloProvider client={client}>
          <IntlProvider locale="en">
            <Router
              initialEntries={[{
                pathname: `/${testOrgId}`,
              }]}
            >
              <AkPage>
                <Route
                  path="/:orgId"
                  render={(props) =>
                    <LinkedOrganizationLandingPageImpl
                      {...props}
                      error={error}
                      loading={loading}
                      data={data}
                    />
                  }
                />
              </AkPage>
            </Router>
          </IntlProvider>
        </ApolloProvider>
      );
    }
  }

  return <TestComp />;
};

storiesOf('Organization|Landing Page/Linked Landing Page', module)
  .add('Loading', () => (
    <StorybookLinkedOrganizationLandingPage
      loading={true}
    />
  ))
  .add('Default', () => (
    <StorybookLinkedOrganizationLandingPage
      loading={false}
      data={{
        name: 'My Awesome Org!',
        totalDisabledMembers: 12,
        totalMembers: 1001,
      }}
    />
  ))
  .add('Error', () => (
    <StorybookLinkedOrganizationLandingPage
      loading={false}
      error={new ApolloError({})}
    />
  ))
  .add('Data undefined', () => (
    <StorybookLinkedOrganizationLandingPage
      loading={false}
    />
  ));
