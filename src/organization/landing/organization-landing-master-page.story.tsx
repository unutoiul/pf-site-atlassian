// tslint:disable jsx-use-translation-function react-this-binding-issue
import { boolean } from '@storybook/addon-knobs';
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';
import { MemoryRouter as Router, Route } from 'react-router-dom';
import { withoutWhaleShot } from 'whales-story-shots/storybook/decorator';

import AkPage from '@atlaskit/page';

import { FlagProvider } from 'common/flag';

import { Organization, OrgProducts, OrgSite, RenameOrgMutationVariables } from '../../schema/schema-types';
import { ApolloClientStorybookWrapper, StorybookAnalyticsClientProvider } from '../../utilities/storybooks';
import { createApolloClientWithOverriddenResolvers } from '../../utilities/testing';
import { MasterOrgLandingPage } from './organization-landing-master-page';

const testCurrentUserId = 'DUMMY_USER_ID';
const testOrgId = 'DUMMY_ORG_ID';
const site1: OrgSite = {
  id: 'site-1-id',
  displayName: 'acme-usa',
  siteUrl: 'acme-usa.atlassian.net',
  avatar: '',
  products: [
    {
      key: 'jira-software',
      name: 'Jira Software',
    },
    {
      key: 'confluence',
      name: 'Confluence',
    },
  ],
};

const site2: OrgSite = {
  id: 'site-2-id',
  displayName: 'acme-asia',
  siteUrl: 'acme-asia.atlassian.net',
  avatar: '',
  products: [
    {
      key: 'jira-service-desk',
      name: 'Jira Service Desk',
    },
    {
      key: 'confluence',
      name: 'Confluence',
    },
    {
      key: 'stride',
      name: 'Stride',
    },
  ],
};

const resolverData: {
  organization: Partial<Organization>,
  products: Partial<OrgProducts>,
} = {
  organization: {},
  products: {},
};
const client = createApolloClientWithOverriddenResolvers({
  Query: {
    products: async () => resolverData.products,
    organization: async () => resolverData.organization,
    currentUser: async () => ({ id: testCurrentUserId }),
  },
  CurrentUser: {
    id: async ({ id }) => ({ id }),
    organizations: async () => [resolverData.organization],
  },
  Organization: {
    id: async () => resolverData.organization.id,
    name: async () => resolverData.organization.name,
    users: async () => resolverData.organization.users,
    sites: async () => resolverData.organization.sites,
    domainClaim: async () => resolverData.organization.domainClaim,
  },
  OrganizationUsers: {
    memberTotal: async () => resolverData.organization.users!.memberTotal,
  },
  DomainClaim: {
    domains: async () => resolverData.organization.domainClaim!.domains,
  },
  Mutation: {
    renameOrganization: async (_, { id, name }: RenameOrgMutationVariables) => {
      resolverData.organization.name = name;

      return { id, name };
    },
  },
});

const StorybookComponent = ({
  hasAtlassianAccess,
  hasLinkedSites,
  hasVerifiedDomains,
}: {
  hasAtlassianAccess: boolean,
  hasLinkedSites: boolean,
  hasVerifiedDomains: boolean,
}) => {
  class TestComp extends React.Component {
    public state = { configured: false };

    public configureApolloClient = () => {
      resolverData.products = {
        id: testOrgId,
        identityManager: hasAtlassianAccess,
      };
      resolverData.organization = {
        id: testOrgId,
        name: 'Storybook Org Inc.',
        domainClaim: {
          domains: [
            {
              id: 'dummy-domain.org',
              domain: 'dummy-domain.org',
              verified: hasVerifiedDomains,
              status: '',
            },
          ],
        },
        users: {
          memberTotal: {
            total: 186,
            disabledTotal: 4,
          },
        } as any,
        sites: hasLinkedSites
          ? [site1, site2]
          : [],
      };
    }

    public render() {
      return (
        <StorybookAnalyticsClientProvider>
          <ApolloClientStorybookWrapper client={client} configureClient={this.configureApolloClient}>
            <ApolloProvider client={client}>
              <IntlProvider locale="en">
                <FlagProvider>
                  <Router
                    initialEntries={[{
                      pathname: `/${testOrgId}`,
                    }]}
                  >
                    <AkPage>
                      <Route
                        path="/:orgId"
                        render={() => <MasterOrgLandingPage />}
                      />
                    </AkPage>
                  </Router>
                </FlagProvider>
              </IntlProvider>
            </ApolloProvider>
          </ApolloClientStorybookWrapper>
        </StorybookAnalyticsClientProvider>
      );
    }
  }

  return <TestComp />;
};

storiesOf('Organization|Landing Page', module)
  .add('Default', withoutWhaleShot(() => {
    const hasAccess = boolean('Has Atlassian Access', true);
    const hasLinkedSites = boolean('Has linked sites', false);
    const hasVerifiedDomains = boolean('Has verified domains', true);

    return (
      <StorybookComponent
        hasAtlassianAccess={hasAccess}
        hasLinkedSites={hasLinkedSites}
        hasVerifiedDomains={hasVerifiedDomains}
      />
    );
  }));
