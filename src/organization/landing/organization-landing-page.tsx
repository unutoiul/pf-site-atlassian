import * as React from 'react';
import { compose, graphql, QueryResult } from 'react-apollo';
import { defineMessages, FormattedHTMLMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import AkSpinner from '@atlaskit/spinner';
import { akGridSizeUnitless } from '@atlaskit/util-shared-styles';

import {
  analyticsClient,
  atlassianAccessLearnMoreEventData,
  Button as AnalyticsButton,
  orgOverviewScreen,
  orgOverviewScreenEvent,
  OrgOverviewScreenVariation,
  Referrer,
  ScreenEventSender,
  UIEvent,
} from 'common/analytics';
import { GenericError } from 'common/error';
import { IdentityManagerLearnMoreButton } from 'common/identity-manager-learn-more-button';
import { PageLayout } from 'common/page-layout';
import { DeclarativeProgressTracker, ProgressItem, ProgressState, renderProgressItem } from 'common/progress-tracker';

import { OrgBreadcrumbs } from '../breadcrumbs';

import { CurrentUserQuery, DomainClaimQuery, ProductsQuery } from '../../schema/schema-types';
import { Domains } from '../domain-claim/domain-claim.prop-types';
import { OrgNameForm } from '../organization-name-form/organization-name-form';
import { createOrgAnalyticsData } from '../organizations/organization-analytics';
import currentUserQuery from '../organizations/queries/current-user.query.graphql';
import domainClaimQuery from '../organizations/queries/domain-claim.query.graphql';
import identityManagerPageQuery from '../organizations/queries/identity-manager-page.query.graphql';

export interface OrganizationLandingPageProps {
  currentOrgs: QueryResult & CurrentUserQuery;
  currentDomains: QueryResult & DomainClaimQuery;
  currentProducts: QueryResult & ProductsQuery;
}

export const defaultMessages = defineMessages({
  description: {
    id: 'organization.landing.description',
    defaultMessage: `Create an organization and verify your domains so that you can easily <b>manage all your users.</b>
    Then you can subscribe to <b>Atlassian Access</b> for <b>SAML single sign-on, two-step verification</b>
    and <b>password policy.</b>`,
  },
  learnMore: {
    id: 'organization.landing.learn.more',
    defaultMessage: `Learn about organizations and Atlassian Access.`,
  },
  verifyDomain: {
    id: 'organization.landing.verify.domain',
    defaultMessage: 'Verify your domains to manage accounts',
  },
  domainHasBeenVerified: {
    id: 'organization.landing.verified.domain',
    defaultMessage: 'Your domains have been verified',
  },
  verifyDomains: {
    id: 'organization.landing.get.started.domains',
    defaultMessage: 'Verify domains',
  },
  activateIdentityManager: {
    id: 'organization.landing.activate.identity.manager',
    defaultMessage: 'Subscribe to Atlassian Access for SAML single sign-on, <br />two-step verification and password policy',
  },
  identityManagerHasBeenActivated: {
    id: 'organization.landing.activated.identity.manager',
    defaultMessage: `You\'re subscribed to Atlassian Access`,
  },
});

// tslint:disable-next-line:no-var-requires no-require-imports
const OrgHeader = require('./org-header.svg');

const HeaderImage = styled.div`
  background: url(${OrgHeader}) no-repeat;
  height: 210px;
`;

const OrgDescription = styled.div`
  display: block;
  padding-top:  ${akGridSizeUnitless * 2}px;
  margin-bottom: ${akGridSizeUnitless * 4}px;
  margin-right: ${akGridSizeUnitless * 8}px;
`;

const ListItem = styled.div`
  margin-right: ${akGridSizeUnitless * 0.5}px;
`;

const ActionButton = styled.div`
  position: absolute;
`;

const OrgPageDescription = (orgId?: string) => (
  <div>
    <HeaderImage />
    <OrgDescription>
      <FormattedHTMLMessage {...defaultMessages.description} />
      <AnalyticsButton
        appearance={'link'}
        href={'https://confluence.atlassian.com/cloud/organization-administration-938859734.html'}
        target={'_blank'}
        rel={'noopener noreferrer'}
        spacing={'none'}
        analyticsData={createOrgAnalyticsData({
          orgId,
          action: 'click',
          actionSubject: 'orgDetailsLearnMoreButton',
          actionSubjectId: 'openLink',
        })}
      >
        <FormattedHTMLMessage {...defaultMessages.learnMore} />
      </AnalyticsButton>
    </OrgDescription>
  </div>
);

const SpinnerItem: ProgressItem = { state: ProgressState.Incomplete, item: <AkSpinner /> };

declare type Props = OrganizationLandingPageProps & RouteComponentProps<{ orgId: string }>;

export class OrganizationLandingPageImpl extends React.Component<Props & InjectedIntlProps> {
  public componentWillReceiveProps(nextProps: Props) {
    if (nextProps.currentOrgs.loading) {
      return;
    }

    // tslint:disable-next-line:early-exit
    if (this.props.currentOrgs.loading === true && nextProps.currentOrgs.loading === false && !nextProps.currentOrgs.error) {
      analyticsClient.eventHandler(createOrgAnalyticsData({
        orgId: this.props.match.params.orgId,
        action: 'pageLoad',
        actionSubject: 'organizationPageLoad',
        actionSubjectId: 'organizationLandingPage',
      }));
    }
  }

  public render() {
    const {
      currentOrgs,
      currentDomains,
      currentProducts,
      match: { params: { orgId } },
    } = this.props;

    if (currentOrgs.error) {
      return (<PageLayout><GenericError /></PageLayout>);
    }

    if (currentOrgs.loading) {
      return (<PageLayout><AkSpinner /></PageLayout>);
    }

    const orgProgress = this.buildOrgProgress(currentOrgs);
    const domainProgress = this.buildDomainProgress(currentDomains);
    const accessProgress = this.buildAtlassianAccessProgress(currentDomains, currentProducts);

    const analyticsScreenVariation: OrgOverviewScreenVariation = accessProgress.state === ProgressState.Completed
      ? 'withAccess'
      : domainProgress.state === ProgressState.Completed
        ? 'domainNoAccess'
        : 'noDomainClaim';

    return (
      <ScreenEventSender event={orgOverviewScreenEvent(orgId, analyticsScreenVariation)} isDataLoading={currentDomains.loading || currentProducts.loading}>
        <PageLayout
          title={this.getOrgName(this.props)}
          description={OrgPageDescription(orgId)}
          breadcrumbs={<OrgBreadcrumbs />}
        >
          <DeclarativeProgressTracker>
            {renderProgressItem(orgProgress)}
            {renderProgressItem(domainProgress)}
            {renderProgressItem(accessProgress)}
          </DeclarativeProgressTracker>
        </PageLayout>
      </ScreenEventSender>
    );
  }

  private getOrgName = (currentProps: Props): string => {
    const currentOrg = currentProps.currentOrgs.currentUser && (currentProps.currentOrgs.currentUser.organizations || []).find(org => org.id === currentProps.match.params.orgId);

    return currentOrg ? currentOrg.name : '';
  };

  private hasAnyDomainVerified = (domains: Domains[] | null): boolean => {
    return (domains || []).some((domain) => domain.verified);
  };

  private buildOrgProgress = (currentOrgs: QueryResult & CurrentUserQuery): ProgressItem => {
    if (currentOrgs.loading) {
      return SpinnerItem;
    }

    return {
      item: (
        <ListItem>
          <OrgNameForm orgId={this.props.match.params.orgId} name={this.getOrgName(this.props)} analyticsSource={orgOverviewScreen} />
        </ListItem>
      ),
      state: ProgressState.Completed,
    };
  };

  private buildDomainProgress = (currentDomains: OrganizationLandingPageProps['currentDomains']): ProgressItem => {
    if (currentDomains.loading || currentDomains.error) {
      return SpinnerItem;
    }

    const { intl: { formatMessage } } = this.props;

    if (this.hasAnyDomainVerified(currentDomains.organization.domainClaim.domains)) {
      const verifyDomainItem = (
        <div>
          <ListItem>
            {formatMessage(defaultMessages.domainHasBeenVerified)}
          </ListItem>
        </div>
      );

      return { item: verifyDomainItem, state: ProgressState.Completed };
    } else {
      const verifyDomainItem = (
        <div>
          <FormattedHTMLMessage {...defaultMessages.verifyDomain} />
          <p />
          <Link to={`domains`}>
            <ActionButton>
              <AnalyticsButton
                appearance={'primary'}
                analyticsData={createOrgAnalyticsData({
                  orgId: this.props.match.params.orgId,
                  action: 'click',
                  actionSubject: 'landingPageVerifyDomainsButton',
                  actionSubjectId: 'openLink',
                })}
              >
                {formatMessage(defaultMessages.verifyDomains)}
              </AnalyticsButton>
            </ActionButton>
          </Link>
        </div>
      );

      return { item: verifyDomainItem, state: ProgressState.Selected };
    }
  };

  private buildAtlassianAccessProgress = (currentDomains: QueryResult & DomainClaimQuery, currentProducts: QueryResult & ProductsQuery): ProgressItem => {
    if (currentDomains.loading || currentDomains.error || currentProducts.loading || currentProducts.error) {
      return SpinnerItem;
    }

    if (currentProducts.products.identityManager) {
      return {
        state: ProgressState.Completed,
        item: <ListItem><FormattedHTMLMessage {...defaultMessages.identityManagerHasBeenActivated} /></ListItem>,
      };
    } else {
      const domains = currentDomains.organization.domainClaim.domains;

      return {
        state: this.hasAnyDomainVerified(domains) ? ProgressState.Selected : ProgressState.Incomplete,
        item: (
          <div>
            <FormattedHTMLMessage {...defaultMessages.activateIdentityManager} />
            <p />
            <ActionButton>
              <Referrer value="orgOverview">
                <IdentityManagerLearnMoreButton
                  isPrimary={this.hasAnyDomainVerified(domains)}
                  analyticsUIEvent={this.createIdentityManagerLearnMoreAnalyticsEvent()}
                />
              </Referrer>
            </ActionButton>
          </div>
        ),
      };
    }
  };

  private createIdentityManagerLearnMoreAnalyticsEvent = (): UIEvent => {
    return {
      orgId: this.props.match.params.orgId,
      data: atlassianAccessLearnMoreEventData(orgOverviewScreen),
    };
  }
}

const currentUserOrganization = graphql<{}, CurrentUserQuery>(currentUserQuery, { name: 'currentOrgs' });
const currentDomainClaims = graphql<any, DomainClaimQuery>(domainClaimQuery, {
  name: 'currentDomains',
  options: (props) => ({ variables: { id: props.match.params.orgId } }),
});
const currentOrgProducts = graphql<any, ProductsQuery>(identityManagerPageQuery, {
  name: 'currentProducts',
  options: (props) => ({ variables: { id: props.match.params.orgId } }),
});

export const OrganizationLandingPage = compose(
  withRouter,
  currentDomainClaims,
  currentUserOrganization,
  currentOrgProducts,
  injectIntl,
)(OrganizationLandingPageImpl);
