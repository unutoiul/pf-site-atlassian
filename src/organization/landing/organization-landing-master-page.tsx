import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';

import AkSpinner from '@atlaskit/spinner';

import { PageLayout } from 'common/page-layout';

import { OrgRouteProps } from '../routes';
import { LinkedSitesProps, withLinkedSites } from '../with-linked-sites';
import { LinkedOrganizationLandingPage } from './linked-organization-landing-page';
import { OrganizationLandingPage } from './organization-landing-page';

export class OrgMasterLandingPageImpl extends React.Component<LinkedSitesProps & RouteComponentProps<OrgRouteProps>> {
  public render() {
    const {
      linkedSitesData: {
        error,
        loading,
        sites,
      },
    } = this.props;

    if (loading) {
      return <PageLayout><AkSpinner /></PageLayout>;
    }

    if (error) {
      // If we failed to load the linked sites, we can render the other overview page (e.g. a graceful fallback)
      return <OrganizationLandingPage />;
    }

    if (sites && sites.length > 0) {
      return <LinkedOrganizationLandingPage />;
    }

    return <OrganizationLandingPage />;
  }
}

export const MasterOrgLandingPage = withRouter<{}>(withLinkedSites(OrgMasterLandingPageImpl));
