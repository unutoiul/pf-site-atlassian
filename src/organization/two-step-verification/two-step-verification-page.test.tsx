import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import AkSpinner from '@atlaskit/spinner';

import { PageLayoutProps } from 'common/page-layout';

import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import { IdentityManagerPageLayout } from '../identity-manager/identity-manager-page-layout';
import { TwoStepVerificationPageImpl } from './two-step-verification-page';

describe('TwoStepVerificationPage', () => {
  function shallowTwoStepVerificationPage(data = {}) {
    return shallow(
      <TwoStepVerificationPageImpl
        data={data}
        match={{ params: { orgId: 'DUMMY_ORG_ID' } }}
        intl={createMockIntlProp()}
        atlassianAccessEnabled={true}
        {...{} as any}
      />,
      createMockIntlContext(),
    );
  }

  it('page layout should always be rendered with title and description', () => {
    const wrapper = shallowTwoStepVerificationPage();
    const pageLayout = wrapper.find(IdentityManagerPageLayout);
    const pageLayoutProps = pageLayout.props() as PageLayoutProps;
    expect(pageLayoutProps.title).to.deep.equal('Two-step verification');
    expect(pageLayoutProps.description).to.not.equal(undefined);
  });

  it('shows spinner when loading', () => {
    const wrapper = shallowTwoStepVerificationPage({ loading: true });
    expect(wrapper.find(AkSpinner)).to.have.length(1);
  });

  describe('determines the enforcement state correctly', () => {
    function pageWithEnforcementDate(date: Date, data = {}) {
      const dateProperty = {
        organization: {
          security: {
            twoStepVerification: {
              dateEnforced: date,
            },
          },
        },
      };

      return shallowTwoStepVerificationPage({ ...data, ...dateProperty });
    }
    it('returns Unenforced when there is no enforcement date', () => {
      const wrapper = pageWithEnforcementDate(null as any);
      expect(wrapper).to.not.be.equal(undefined);
    });
  });

  it('displays an error when data cannot be retrieved', () => {
    const wrapper = shallowTwoStepVerificationPage({ loading: false });
    const errorMessage = wrapper.find('span');
    expect(errorMessage).to.have.length(1);
    expect(errorMessage.text()).to.equal('Two-step verification data could not be retrieved.');
  });

  it('displays an error when data cannot be retrieved', () => {
    const wrapper = shallowTwoStepVerificationPage({ loading: false });
    const errorMessage = wrapper.find('span');
    expect(errorMessage).to.have.length(1);
    expect(errorMessage.text()).to.equal('Two-step verification data could not be retrieved.');
  });

  it('should not display sidecar when loading', () => {
    const wrapper = shallowTwoStepVerificationPage({ loading: true });
    const pageLayout = wrapper.find(IdentityManagerPageLayout);
    const pageLayoutProps = pageLayout.props() as PageLayoutProps;
    expect(pageLayoutProps.side).to.equal(null);
  });

  it('should not display sidecar when error occurred', () => {
    const wrapper = shallowTwoStepVerificationPage({ loading: false, error: true });
    const pageLayout = wrapper.find(IdentityManagerPageLayout);
    const pageLayoutProps = pageLayout.props() as PageLayoutProps;
    expect(pageLayoutProps.side).to.equal(null);
  });

  it('should display sidecar when loaded successfully', () => {
    const wrapper = shallowTwoStepVerificationPage({ loading: false, error: false, organization: { security: { twoStepVerification: { dateEnforced: null } } } });
    const pageLayout = wrapper.find(IdentityManagerPageLayout);
    const pageLayoutProps = pageLayout.props() as PageLayoutProps;
    expect(pageLayoutProps.side).to.not.equal(null);
  });
});
