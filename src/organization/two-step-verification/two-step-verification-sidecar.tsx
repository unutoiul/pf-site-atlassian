import * as React from 'react';
import { defineMessages, FormattedHTMLMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import { akGridSizeUnitless } from '@atlaskit/util-shared-styles';

import { PageSidebar } from 'common/page-layout';

import { Sidecar } from './two-step-verification.prop-types';

const messages = defineMessages({
  heading: {
    id: 'organization.two.step.verification.sidecar.heading',
    defaultMessage: 'What you need to know',
  },
  verifiedDomainsDisabled: {
    id: 'organization.two.step.verification.sidecar.verified.domains.disabled',
    defaultMessage: '<a href="https://confluence.atlassian.com/x/2tz1Nw" target="_blank" rel="noopener noreferrer">Enforced two-step verification</a> only applies to accounts from your verified domains.',
  },
  verifiedDomainsEnabled: {
    id: 'organization.two.step.verification.sidecar.verified.domains.enabled',
    defaultMessage: 'Two-step verification can only be enforced on user accounts from your verified domains.',
  },
  beforeEnforcement: {
    id: 'organization.two.step.verification.sidecar.before.enforcement',
    defaultMessage: 'You should enable two-step verification for yourself before enforcing it for your users.',
  },
  stopEnforcement: {
    id: 'organization.two.step.verification.sidecar.stop.enforcement',
    defaultMessage: `If you
    <a href="https://confluence.atlassian.com/x/2tz1Nw#Enforcedtwo-stepverification-stop_enforcing" target="_blank" rel="noopener noreferrer">stop enforcing two-step verification</a>,
    it will remain enabled on your users' accounts but they can choose to disable it. New users won't be asked to enable two-step verification.`,
  },
  idpManagedDisabled: {
    id: 'organization.two.step.verification.sidecar.idp.managed.disabled',
    defaultMessage: `Users who log in with Google or SAML SSO won't see Atlassian two-step verification.
      Use <a href="https://confluence.atlassian.com/x/2tz1Nw#Enforcedtwo-stepverification-no_saml_gsuite" target="_blank" rel="noopener noreferrer">Google's or your identity provider's equivalent</a> instead.`,
  },
  idpManagedEnabled: {
    id: 'organization.two.step.verification.sidecar.idp.managed.enabled',
    defaultMessage: `Users who log in with Google or SAML single sign-on don't see Atlassian two-step verification.
       We recommend that you <a href="https://confluence.atlassian.com/x/2tz1Nw#Enforcedtwo-stepverification-no_saml_gsuite" target="_blank" rel="noopener noreferrer">use Google's or your identity provider's equivalent</a>.`,
  },
  apiTokensDisabled: {
    id: 'organization.two.step.verification.sidecar.api.tokens.disabled',
    defaultMessage: `Scripts and services must <a href="https://confluence.atlassian.com/x/2tz1Nw#Enforcedtwo-stepverification-api_tokens" target="_blank" rel="noopener noreferrer">use an API token</a> to authenticate with Atlassian Cloud apps when two-step verification is enforced.`,
  },
  apiTokensEnabled: {
    id: 'organization.two.step.verification.sidecar.api.tokens.enabled',
    defaultMessage: `When two-step verification is enforced,
      <a href="https://confluence.atlassian.com/x/2tz1Nw#Enforcedtwo-stepverification-api_tokens" target="_blank" rel="noopener noreferrer">scripts and services need to use an API token</a>
      to authenticate with your Atlassian Cloud applications.`,
  },
  whitelistUsers: {
    id: 'organization.two.step.verification.sidecar.whitelist.users',
    defaultMessage: `You can temporarily  <a href="https://confluence.atlassian.com/x/2tz1Nw#Enforcedtwo-stepverification-temp_exclude" target="_blank" rel="noopener noreferrer">exclude users from two-step verification</a>, so they can log in without it enabled.`,
  },
  learnMore: {
    id: 'organization.two.step.verification.sidecar.learn.more',
    defaultMessage: '<a href="https://confluence.atlassian.com/x/2tz1Nw" target="_blank" rel="noopener noreferrer">Learn more about enforced two-step verification</a>.',
  },
});

const enabledMessages = [
  messages.verifiedDomainsEnabled,
  messages.stopEnforcement,
  messages.idpManagedEnabled,
  messages.apiTokensEnabled,
  messages.whitelistUsers,
  messages.learnMore,
];

const disabledMessages = [
  messages.verifiedDomainsDisabled,
  messages.beforeEnforcement,
  messages.idpManagedDisabled,
  messages.apiTokensDisabled,
  messages.whitelistUsers,
  messages.learnMore,
];

const ListItem = styled.li`
  padding: ${akGridSizeUnitless}px;
`;

export class TwoStepVerificationSidecarImpl extends React.Component<Sidecar & InjectedIntlProps> {

  public render() {
    const { formatMessage } = this.props.intl;

    return (
      <PageSidebar>
        <h4>{formatMessage(messages.heading)}</h4>
        <ul>{this.items()}</ul>
      </PageSidebar>
    );
  }

  private items = () => (this.props.enforced ? enabledMessages : disabledMessages).
    map(message => <ListItem key={message.id}><FormattedHTMLMessage {...message} /></ListItem>)
}

export const TwoStepVerificationSidecar = injectIntl<Sidecar>(TwoStepVerificationSidecarImpl);
