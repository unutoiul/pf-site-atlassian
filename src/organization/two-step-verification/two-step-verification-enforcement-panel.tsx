import * as moment from 'moment';
import * as React from 'react';
import { defineMessages, FormattedHTMLMessage, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import AkButton, { ButtonGroup as AkButtonGroup } from '@atlaskit/button';
import { default as AkLozenge } from '@atlaskit/lozenge';
import AkSpinner from '@atlaskit/spinner';
import { akGridSizeUnitless } from '@atlaskit/util-shared-styles';

import { Button as AnalyticsButton, OrgAnalyticsActionSubject } from 'common/analytics';
import { DateFormats } from 'common/date';

import { EnforcementState, TwoStepVerificationEnforcementState } from '../organizations/organization.prop-types';
import { DisableEnforcementModal } from './two-step-verification-enforce-disable-modal';
import { EnableEnforcementModal } from './two-step-verification-enforce-modal';

import { createOrgAnalyticsData, OrgAnalyticsData } from '../organizations/organization-analytics';
import { TwoStepVerificationError } from './two-step-verification.prop-types';

const enforcementMessages = defineMessages({
  title: {
    id: 'organization.two.step.verification.enforcement.title',
    defaultMessage: 'Enforce two-step verification',
  },
  deadline: {
    id: 'organization.two.step.verification.enforcement.deadline',
    defaultMessage: 'Enablement deadline',
  },
});

const buttonMessages = defineMessages({
  enforce: {
    id: 'organization.two.step.verification.enforcement.button.enforce',
    defaultMessage: 'Enforce two-step verification',
  },
  update: {
    id: 'organization.two.step.verification.enforcement.button.update',
    defaultMessage: 'Update deadline',
  },
  deactivate: {
    id: 'organization.two.step.verification.enforcement.button.deactivate',
    defaultMessage: 'Stop enforcing two-step verification',
  },
});

const lozengeMessages = defineMessages({
  inactive: {
    id: 'organization.two.step.verification.enforcement.lozenge.inactive',
    defaultMessage: 'inactive',
  },
  active: {
    id: 'organization.two.step.verification.enforcement.lozenge.active',
    defaultMessage: 'active',
  },
});

const descriptionMessages = defineMessages({
  unenforced: {
    id: 'organization.two.step.verification.enforcement.description.unenforced',
    defaultMessage: `We'll email existing users with instructions on how to enable two-step verification. They'll have to do that when they next log in. New users will enable two-step verification as part of signup.`,
  },
  onboarding: {
    id: 'organization.two.step.verification.enforcement.description.onboarding',
    defaultMessage: 'We are onboarding your existing users to two-step verification. New users will enable the feature on signup.',
  },
  enforcedHtml: {
    id: 'organization.two.step.verification.enforcement.description.enforced',
    defaultMessage: 'Two-step verification has been enforced since <time datetime={timestamp}>{dateText}</time>. New users must enable two-step verification as part of signup.',
  },
});

const ButtonGroupContainer = styled.div`
  margin-top: ${akGridSizeUnitless * 3}px;
`;

const Description = styled.p`
  margin-top: ${akGridSizeUnitless * 2}px;
  time {
    font-weight: bold;
  }
`;

interface Props {
  enforcement: TwoStepVerificationEnforcementState;
  orgId: string;
  onEnforceTwoStepVerification(d: Date): Promise<any>;
  onDisableTwoStepVerificationEnforcement(): Promise<any>;
  onError(err: TwoStepVerificationError): void;
}

export const enum ModalState {
  None,
  EnforceTwoStep,
  DisableTwoStep,
}

interface State {
  performingAsyncOperation: boolean;
  modalState: ModalState;
}

const ButtonContent = styled.div`
  position: relative;
`;

const SpinnerContainer = styled.div`
  width: 100%;
  position: absolute;
  top: 4px;
`;

const Header = styled.h3`
  display: inline-flex;
`;

const LozengeContainer = styled.span`
  align-self: center;
  margin-left: ${akGridSizeUnitless / 2}px;
  display: inline-flex;
`;

export class TwoStepVerificationEnforcementPanelImpl extends React.Component<Props & InjectedIntlProps, State> {
  public state: State = {
    performingAsyncOperation: false,
    modalState: ModalState.None,
  };

  public render() {
    const { formatMessage } = this.props.intl;
    const { enforcement } = this.props;

    return (
      <div>
        <Header>{formatMessage(enforcementMessages.title)} <LozengeContainer>{this.getLozenge(enforcement)}</LozengeContainer></Header>
        <Description>{getDescriptionMessage(enforcement)}</Description>
        <ButtonGroupContainer>
          <AkButtonGroup children={this.getEnrollmentButtons(enforcement)} />
        </ButtonGroupContainer>
        <EnableEnforcementModal
          isOpen={this.state.modalState === ModalState.EnforceTwoStep}
          buttonAnalyticsData={this.enforceButtonAnalyticsData}
          enableTwoStepVerificationEnforcement={this.hideModalAndEnableEnforcement}
          hideModal={this.hideModal}
        />
        <DisableEnforcementModal
          isOpen={this.state.modalState === ModalState.DisableTwoStep}
          buttonAnalyticsData={this.disableButtonAnalyticsData}
          disableTwoStepVerificationEnforcement={this.hideModalAndDisableEnforcement}
          hideModal={this.hideModal}
        />
      </div>
    );
  }

  private showDisableModal = () => {
    this.setState({ modalState: ModalState.DisableTwoStep });
  }

  private hideModalAndDisableEnforcement = async () => {
    this.hideModal();

    return this.handleDisable();
  }

  private showEnableModal = () => {
    this.setState({ modalState: ModalState.EnforceTwoStep });
  }

  private hideModal = () => {
    this.setState({ modalState: ModalState.None });
  }

  private hideModalAndEnableEnforcement = async () => {
    this.hideModal();

    return this.handleEnforcement();
  }

  private handleEnforcement = async () => {
    this.setState({ performingAsyncOperation: true });

    return this.props.onEnforceTwoStepVerification(new Date())
      .catch(e => this.props.onError({
        subject: 'enforceTwoStepVerification',
        err: e,
      }))
      .then(_ => this.setState({ performingAsyncOperation: false }));
  }

  private handleDisable = async () => {
    this.setState({ performingAsyncOperation: true });

    return this.props.onDisableTwoStepVerificationEnforcement()
      .catch(e => this.props.onError({
        subject: 'disableTwoStepVerificationEnforcement',
        err: e,
      }))
      .then(_ => this.setState({ performingAsyncOperation: false }));
  }

  private spinnerOrMessage = (m: FormattedMessage.MessageDescriptor): JSX.Element => {
    const shouldShowSpinner = this.state.performingAsyncOperation;

    return (
      <ButtonContent>
        <SpinnerContainer style={{ opacity: (shouldShowSpinner ? 1 : 0) }}>
          <AkSpinner size={'small'} />
        </SpinnerContainer>
        <span style={{ opacity: (shouldShowSpinner ? 0 : 1) }}><FormattedMessage {...m} /></span>
      </ButtonContent>
    );
  }

  private enforceButtonAnalyticsData = (actionSubjectId: string) => {
    return this.buttonAnalyticsData('enforceTwoStepVerificationButton', actionSubjectId);
  }

  private disableButtonAnalyticsData = (actionSubjectId: string) => {
    return this.buttonAnalyticsData('deactivateTwoStepVerificationEnforcementButton', actionSubjectId);
  }

  private buttonAnalyticsData = (actionSubject: OrgAnalyticsActionSubject, actionSubjectId: string): OrgAnalyticsData => {
    return createOrgAnalyticsData({
      action: 'click',
      actionSubject,
      actionSubjectId,
      orgId: this.props.orgId,
    });
  }

  private getEnrollmentButtons(enf: TwoStepVerificationEnforcementState): JSX.Element[] {
    const isDisabled = this.state.performingAsyncOperation;
    switch (enf.state) {
      default:
      case EnforcementState.Unenforced: return [(
        <AnalyticsButton
          appearance="primary"
          key="enforce"
          isDisabled={isDisabled}
          onClick={this.showEnableModal}
          analyticsData={this.enforceButtonAnalyticsData('openModal')}
        >
          {this.spinnerOrMessage(buttonMessages.enforce)}
        </AnalyticsButton>
      )];
      case EnforcementState.Onboarding: return [(
        <AkButton
          appearance="default"
          key="update"
          isDisabled={isDisabled}
        >
          {this.spinnerOrMessage(buttonMessages.update)}
        </AkButton>
      ), (
        <AkButton
          appearance="link"
          key="deactivate"
          isDisabled={isDisabled}
        >
          {this.spinnerOrMessage(buttonMessages.deactivate)}
        </AkButton>
      )];
      case EnforcementState.Enforced: return [(
        <AnalyticsButton
          appearance="default"
          key="deactivate"
          isDisabled={isDisabled}
          onClick={this.showDisableModal}
          analyticsData={this.disableButtonAnalyticsData('openModal')}
        >
          {this.spinnerOrMessage(buttonMessages.deactivate)}
        </AnalyticsButton>
      )];
    }
  }

  private getLozenge(enf: TwoStepVerificationEnforcementState): AkLozenge {
    const { formatMessage } = this.props.intl;

    switch (enf.state) {
      default:
      case EnforcementState.Unenforced: return <AkLozenge appearance="default" isBold={true}>{formatMessage(lozengeMessages.inactive)}</AkLozenge>;
      case EnforcementState.Onboarding:
      case EnforcementState.Enforced: return <AkLozenge appearance="success" isBold={true}>{formatMessage(lozengeMessages.active)}</AkLozenge>;
    }
  }
}

function getDescriptionMessage(enf: TwoStepVerificationEnforcementState): JSX.Element {
  switch (enf.state) {
    default:
    case EnforcementState.Unenforced: return <FormattedMessage {...descriptionMessages.unenforced} />;
    case EnforcementState.Onboarding: return <FormattedMessage {...descriptionMessages.onboarding} />;
    case EnforcementState.Enforced: return <FormattedHTMLMessage {...descriptionMessages.enforcedHtml} values={{ timestamp: enf.dateEnforced.toISOString(), dateText: moment(enf.dateEnforced).format(DateFormats.SHORT) }} />;
  }
}

export const TwoStepVerificationEnforcementPanel = injectIntl<Props>(TwoStepVerificationEnforcementPanelImpl);
