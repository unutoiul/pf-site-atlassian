import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import { spy } from 'sinon';

import AkButton from '@atlaskit/button';

import { Button as AnalyticsButton } from 'common/analytics';

import { createMockIntlContext } from '../../utilities/testing';
import { TwoStepVerificationModal } from './two-step-verification-modal';

describe('TwoStepVerificationModalTest', () => {

  let submit;
  let cancel;
  let analytics;

  beforeEach(() => {
    submit = spy();
    cancel = spy();
    analytics = spy(u => u);
  });

  function mountModal() {
    const component = (
      <TwoStepVerificationModal
        isOpen={true}
        header={<div id="header"/>}
        content={<div id="content"/>}
        submitButtonContent={<div id="submitButton" />}
        cancelButtonContent={<div id="cancelButton" />}
        buttonAnalyticsData={analytics}
        onSubmit={submit}
        hideModal={cancel}
      />
    );

    return mount(component, createMockIntlContext());
  }

  it('renders header', () => {
    const modal = mountModal();
    const header = modal.find('#header');

    expect(header).to.have.length(1);
  });

  it('renders content', () => {
    const modal = mountModal();
    const header = modal.find('#content');

    expect(header).to.have.length(1);
  });

  it('renders submit button', () => {
    const modal = mountModal();
    const submitButtonContent = modal.find('#submitButton');
    const submitButton = modal.find(AnalyticsButton).at(0);

    expect(submitButtonContent).to.have.length(1);
    expect(submitButton.props().analyticsData).to.deep.equal('update');
  });

  it('renders cancel button', () => {
    const modal = mountModal();
    const cancelButtonContent = modal.find('#cancelButton');
    const cancelButton = modal.find(AnalyticsButton).at(1);

    expect(cancelButtonContent).to.have.length(1);
    expect(cancelButton.props().analyticsData).to.deep.equal('cancel');
  });

  it('submit button calls submit handler', () => {
    const modal = mountModal();
    const submitButton = modal.find(AkButton).at(0);

    submitButton.simulate('click');
    expect(submit.callCount).to.equal(1);
  });

  it('cancel button calls cancel handler', () => {
    const modal = mountModal();
    const cancelButton = modal.find(AkButton).at(1);

    cancelButton.simulate('click');
    expect(cancel.callCount).to.equal(1);
  });
});
