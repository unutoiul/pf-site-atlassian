import * as React from 'react';
import { compose, graphql } from 'react-apollo';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import AkSpinner from '@atlaskit/spinner';

import { ActionsBarCheckCircleIcon } from 'common/actions-bar';
import { analyticsClient, Referrer, twoStepVerificationScreen, twoStepVerificationScreenEvent } from 'common/analytics';
import { FlagProps, withFlag } from 'common/flag';

import { OrgBreadcrumbs } from '../breadcrumbs';

import { DisableTwoStepVerificationEnforcementMutation, EnforceTwoStepVerificationMutation, TwoStepVerificationEnforcementQuery } from '../../schema/schema-types';
import { IdentityManagerPageLayout } from '../identity-manager/identity-manager-page-layout';
import { EnforcementState } from '../organizations/organization.prop-types';
import { getEnforcementState } from '../organizations/organizations.util';
import twoStepVerificationEnforcementQuery from '../organizations/queries/two-step-verification-page.query.graphql';
import disableTwoStepVerificationEnforcement from './disable-two-step-verification-enforcement.mutation.graphql';
import enableTwoStepVerificationEnforcement from './enforce-two-step-verification.mutation.graphql';
import { TwoStepVerificationEnforcementPanel } from './two-step-verification-enforcement-panel';
import { TwoStepVerificationSidecar } from './two-step-verification-sidecar';
import { Page, TwoStepVerificationError } from './two-step-verification.prop-types';

const messages = defineMessages({
  title: {
    id: 'organization.two.step.verification.title',
    defaultMessage: 'Two-step verification',
  },
  description: {
    id: 'organization.two.step.verification.description',
    defaultMessage: `Two-step verification uses a second login step to keep your users' accounts more secure.`,
  },
  error: {
    id: 'organization.two.step.verification.error',
    defaultMessage: 'Two-step verification data could not be retrieved.',
  },
  enforcementSuccessTitle: {
    id: 'organization.two.step.verification.success.title',
    defaultMessage: `You've enforced two-step verification`,
  },
  enforcementSuccessDescription: {
    id: 'organization.two.step.verification.success.description',
    defaultMessage: 'Each user will need to enable it on their account before they can log in again.',
  },
  stopEnforcementTitle: {
    id: 'organization.two.step.verification.stop.title',
    defaultMessage: `You've stopped enforcing two-step verification`,
  },
  stopEnforcementDescription: {
    id: 'organization.two.step.verification.stop.description',
    defaultMessage: 'Your users are no longer required to have it enabled.',
  },
  identityManagerEvaluation: {
    id: 'organization.two.step.verification.identityManagerEvaluation',
    defaultMessage: 'Two-step verification is an Atlassian Access feature. Before you can enforce two-step verification you need to have verified a domain and have an Atlassian Access subscription.',
  },
  domainPrompt: {
    id: 'organization.two.step.verification.domain.prompt',
    defaultMessage: `To enforce two-step verification on your managed accounts, you need to verify a domain.`,
  },
});

export class TwoStepVerificationPageImpl extends React.Component<Page & FlagProps & InjectedIntlProps> {

  public render() {
    const {
      intl: { formatMessage },
      data: {
        loading,
        organization,
        error,
      },
    } = this.props;
    const shouldShowComponent = !loading && !error && organization && organization.security;

    return (
      <Referrer value="twoStep">
        <IdentityManagerPageLayout
          title={formatMessage(messages.title)}
          description={formatMessage(messages.description)}
          domainPrompt={formatMessage(messages.domainPrompt)}
          side={this.sidePanel(shouldShowComponent)}
          orgId={this.props.match.params.orgId}
          analyticsScreenForEvalButton={twoStepVerificationScreen}
          cardDescription={formatMessage(messages.identityManagerEvaluation)}
          breadcrumbs={<OrgBreadcrumbs />}
          screenEvent={twoStepVerificationScreenEvent}
        >
          {loading && <AkSpinner />}

          {!loading && (error || !organization || !organization.security) &&
            <span>{formatMessage(messages.error)}</span>}

          {shouldShowComponent &&
            <TwoStepVerificationEnforcementPanel
              enforcement={getEnforcementState(organization.security)}
              onEnforceTwoStepVerification={this.enforceTwoStepVerification}
              onDisableTwoStepVerificationEnforcement={this.disableTwoStepVerificationEnforcement}
              onError={this.handleError}
              orgId={this.props.match.params.orgId}
            />
          }
        </IdentityManagerPageLayout>
      </Referrer>
    );
  }

  private sidePanel(shouldShowComponent): React.ReactNode {
    const { organization } = this.props.data;

    return shouldShowComponent ? <TwoStepVerificationSidecar enforced={getEnforcementState(organization.security).state === EnforcementState.Enforced} /> : null;
  }

  private enforceTwoStepVerification = async () => {
    const { formatMessage } = this.props.intl;

    return this.props.enableTwoStepVerificationEnforcement({
      variables: {
        id: this.props.match.params.orgId,
        date: new Date(),
      },
      optimisticResponse: {
        enableTwoStepVerificationEnforcement: true,
      },
    }).then(async _ => {
      return this.props.data.refetch(); // Calling refetch this way prevents the page's loading state from being triggered
    }).then(_ => {
      this.props.showFlag({
        id: 'two-step-verficiation-enforcement-success',
        title: formatMessage(messages.enforcementSuccessTitle),
        description: formatMessage(messages.enforcementSuccessDescription),
        autoDismiss: true,
        icon: <ActionsBarCheckCircleIcon label="" />,
      });
    });
  }

  private disableTwoStepVerificationEnforcement = async () => {
    const { formatMessage } = this.props.intl;

    return this.props.disableTwoStepVerificationEnforcement({
      variables: {
        id: this.props.match.params.orgId,
      },
      optimisticResponse: {
        disableTwoStepVerificationEnforcement: true,
      },
    }).then(async _ => {
      return this.props.data.refetch();
    }).then(_ => {
      this.props.showFlag({
        id: 'two-step-verficiation-stop-enforcement-success',
        title: formatMessage(messages.stopEnforcementTitle),
        description: formatMessage(messages.stopEnforcementDescription),
        autoDismiss: true,
        icon: <ActionsBarCheckCircleIcon label="" />,
      });
    });
  }

  private handleError = (err: TwoStepVerificationError) => {
    analyticsClient.onError(new Error(`Error on two-step verification page. Subject: ${err.subject}; Error: ${err.err}`));
  };

}

const disableEnforcement = graphql<any, DisableTwoStepVerificationEnforcementMutation>(disableTwoStepVerificationEnforcement, {
  name: 'disableTwoStepVerificationEnforcement',
});
const enableEnforcement = graphql<any, EnforceTwoStepVerificationMutation>(enableTwoStepVerificationEnforcement, {
  name: 'enableTwoStepVerificationEnforcement',
});
const query = graphql<any, TwoStepVerificationEnforcementQuery>(twoStepVerificationEnforcementQuery, {
  options: (props) => ({ variables: { id: props.match.params.orgId } }),
});

export const TwoStepVerificationPage =
  compose(
    query,
    enableEnforcement,
    disableEnforcement,
    withFlag,
    injectIntl)(TwoStepVerificationPageImpl);
