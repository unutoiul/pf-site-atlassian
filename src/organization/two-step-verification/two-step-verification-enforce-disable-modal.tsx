import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import { TwoStepVerificationModal } from './two-step-verification-modal';
import { DisableEnforcementModalProps } from './two-step-verification.prop-types';

const messages = defineMessages({
  title: {
    id: 'organization.two.step.verification.enforce.disable.modal.title',
    defaultMessage: 'Stop enforcing two-step verification?',
  },
  descriptionLineOne: {
    id: 'organization.two.step.verification.enforce.disable.modal.line.one',
    defaultMessage: 'Two-step verification will remain enabled for your users, but they can choose to disable it.',
  },
  descriptionLineTwo: {
    id: 'organization.two.step.verification.enforce.disable.modal.line.two',
    defaultMessage: `New users won't be asked to enable two-step verification.`,
  },
  disable: {
    id: 'organization.two.step.verification.enforce.disable.modal.disable',
    defaultMessage: 'Stop enforcing two-step verification',
  },
  cancel: {
    id: 'organization.two.step.verification.enforce.disable.modal.cancel',
    defaultMessage: 'Cancel',
  },
});

export class DisableEnforcementModalImpl extends React.PureComponent<DisableEnforcementModalProps & InjectedIntlProps> {
  public render() {
    const { formatMessage } = this.props.intl;

    const dialogContent = (
      <div>
        <p>
          {formatMessage(messages.descriptionLineOne)}
        </p>
        <p>
          {formatMessage(messages.descriptionLineTwo)}
        </p>
      </div>
    );

    return (
      <TwoStepVerificationModal
        isOpen={this.props.isOpen}
        header={formatMessage(messages.title)}
        content={dialogContent}
        onSubmit={this.props.disableTwoStepVerificationEnforcement}
        hideModal={this.props.hideModal}
        submitButtonContent={formatMessage(messages.disable)}
        cancelButtonContent={formatMessage(messages.cancel)}
        buttonAnalyticsData={this.props.buttonAnalyticsData}
      />
    );
  }
}

export const DisableEnforcementModal = injectIntl<DisableEnforcementModalProps>(DisableEnforcementModalImpl);
