import { expect } from 'chai';
import { mount } from 'enzyme';
import * as moment from 'moment';
import * as React from 'react';
import { FormattedHTMLMessage, FormattedMessage } from 'react-intl';
import { spy, stub } from 'sinon';

import AkButton from '@atlaskit/button';
import AkLozenge from '@atlaskit/lozenge';

import { Button as AnalyticsButton } from 'common/analytics';

import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import { Enforced, EnforcementState, Onboarding, TwoStepVerificationEnforcementState, Unenforced } from '../organizations/organization.prop-types';
import { DisableEnforcementModal } from './two-step-verification-enforce-disable-modal';
import { EnableEnforcementModal } from './two-step-verification-enforce-modal';
import { ModalState, TwoStepVerificationEnforcementPanelImpl } from './two-step-verification-enforcement-panel';

describe('TwoStepVerificationEnforcementPanel', () => {

  let enforce;
  let disable;
  let error;

  beforeEach(() => {
    enforce = stub();
    enforce.resolves(spy());
    disable = stub();
    disable.resolves(spy());
    error = spy();
  });

  function mountPanel(state: TwoStepVerificationEnforcementState, eventHandlers = {}) {
    const handlers = { enforce, disable, error, ...eventHandlers };
    const component = (
      <TwoStepVerificationEnforcementPanelImpl
        enforcement={state}
        onEnforceTwoStepVerification={handlers.enforce}
        onDisableTwoStepVerificationEnforcement={handlers.disable}
        onError={handlers.error}
        orgId={'DUMMY_ORG_ID'}
        intl={createMockIntlProp()}
      />
    );

    return mount(component, createMockIntlContext());
  }

  const tomorrow = moment().add(1, 'days').toDate();
  const yesterday = moment().subtract(1, 'days').toDate();

  const unenforced: Unenforced = { state: EnforcementState.Unenforced };
  const onboarding: Onboarding = { state: EnforcementState.Onboarding, dateEnforced: tomorrow };
  const enforced: Enforced = { state: EnforcementState.Enforced, dateEnforced: yesterday };

  describe('Renders correct information for enforcement state', () => {
    describe('for Unenforced', () => {
      const wrapper = mountPanel(unenforced);

      it('displays the lozenge with "INACTIVE"', () => {
        const lozenge = wrapper.find(AkLozenge) as typeof AkLozenge;
        expect(lozenge).to.have.length(1);
        expect(lozenge.props().children).to.equal('inactive');
        expect(lozenge.props().appearance).to.equal('default');
      });
      it('provides a button to enforce two-step verification', () => {
        const button = wrapper.find(AnalyticsButton);
        expect(button).to.have.length(1);
        expect(button.at(0).children().text()).to.deep.equal('Enforce two-step verification');
        expect(button.props().analyticsData).to.deep.equal({
          action: 'click',
          actionSubject: 'enforceTwoStepVerificationButton',
          actionSubjectId: 'openModal',
          subproduct: 'organization',
          tenantId: 'DUMMY_ORG_ID',
          tenantType: 'organizationId',
        });
      });
      it('displays the correct description copy', () => {
        const descriptions = wrapper.find(FormattedMessage).map(x => x.props()).filter(x => x.id === 'organization.two.step.verification.enforcement.description.unenforced');
        expect(descriptions).to.have.length(1);
      });
    });

    describe('for Onboarding', () => {
      const wrapper = mountPanel(onboarding);

      it('displays the lozenge with "ACTIVE"', () => {
        const lozenge = wrapper.find(AkLozenge) as typeof AkLozenge;
        expect(lozenge).to.have.length(1);
        expect(lozenge.props().children).to.equal('active');
        expect(lozenge.props().appearance).to.equal('success');
      });
      it('provides buttons to deactivate and update two-step verification enforcement', () => {
        const buttons = wrapper.find(AkButton);
        expect(buttons).to.have.length(2);
        expect(buttons.at(0).children().text()).to.deep.equal('Update deadline');
        expect(buttons.at(1).children().text()).to.deep.equal('Stop enforcing two-step verification');
      });
      it('displays the correct description copy', () => {
        const descriptions = wrapper.find(FormattedMessage).map(x => x.props()).filter(x => x.id === 'organization.two.step.verification.enforcement.description.onboarding');
        expect(descriptions).to.have.length(1);
      });
    });

    describe('for Enforced', () => {
      const wrapper = mountPanel(enforced);

      it('displays the lozenge with "ACTIVE"', () => {
        const lozenge = wrapper.find(AkLozenge) as typeof AkLozenge;
        expect(lozenge).to.have.length(1);
        expect(lozenge.props().children).to.equal('active');
        expect(lozenge.props().appearance).to.equal('success');
      });
      it('provides a button to deactivate two-step verification', () => {
        const button = wrapper.find(AnalyticsButton);
        expect(button).to.have.length(1);
        expect(button.at(0).children().text()).to.deep.equal('Stop enforcing two-step verification');
        expect(button.props().analyticsData).to.deep.equal({
          action: 'click',
          actionSubject: 'deactivateTwoStepVerificationEnforcementButton',
          actionSubjectId: 'openModal',
          subproduct: 'organization',
          tenantId: 'DUMMY_ORG_ID',
          tenantType: 'organizationId',
        });
      });
      it('displays the correct description copy', () => {
        const descriptions = wrapper.find(FormattedHTMLMessage).map(x => x.props()).filter(x => x.id === 'organization.two.step.verification.enforcement.description.enforced');
        expect(descriptions).to.have.length(1);
      });
    });
  });

  describe('button behaviors are hooked up correctly', () => {
    it('unenforced -> onboarding', () => {
      const wrapper = mountPanel(unenforced);
      const enforceButton = wrapper.find(AkButton).at(0);
      enforceButton.simulate('click');

      const modal = wrapper.find(EnableEnforcementModal);
      expect(modal.props().isOpen).to.equal(true);
    });
    // TODO: Uncomment when onboarding is implemented
    // it('onboarding -> onboarding (updated enforcement date)', () => {
    //   const wrapper = mountPanel(onboarding);
    //   const updateButton = wrapper.find(Button).at(0);
    //   updateButton.simulate('click');
    //   expect(enforce.callCount).to.equal(1);
    // });
    // it('onboarding -> unenforced', () => {
    //   const wrapper = mountPanel(onboarding);
    //   const disableButton = wrapper.find(Button).at(1);
    //   disableButton.simulate('click');
    //   expect(disable.callCount).to.equal(1);
    // });
    it('enforced -> unenforced', () => {
      const wrapper = mountPanel(enforced);
      const disableButton = wrapper.find(AkButton).at(0);
      disableButton.simulate('click');

      const modal = wrapper.find(DisableEnforcementModal);
      expect(modal.props().isOpen).to.equal(true);
    });
  });

  describe('error handler is called', () => {
    it('when onEnforceTwoStepVerification fails', async () => {
      const enforceFails = stub();
      enforceFails.rejects('fail');
      const component = mountPanel(unenforced, { enforce: enforceFails }).find(TwoStepVerificationEnforcementPanelImpl).instance() as any;
      await component.handleEnforcement();

      expect(enforceFails.callCount).to.equal(1);
      expect(error.callCount).to.equal(1);
    });
    it('when onDisableTwoStepVerification fails', async () => {
      const disableFails = stub();
      disableFails.rejects();
      const component = mountPanel(unenforced, { enforce: disableFails }).find(TwoStepVerificationEnforcementPanelImpl).instance() as any;
      await component.handleEnforcement();

      expect(disableFails.callCount).to.equal(1);
      expect(error.callCount).to.equal(1);
    });
  });

  it('should display modal when modalState is DisableTwoStep', () => {
    const wrapper = mountPanel(enforced);
    wrapper.find(TwoStepVerificationEnforcementPanelImpl).instance().setState({ modalState: ModalState.DisableTwoStep });

    const modal = wrapper.find(DisableEnforcementModal).instance();

    expect((modal.props as any).isOpen).to.equal(true);
  });

  it('should not display modal by default', () => {
    const wrapper = mountPanel(enforced);

    const modal = wrapper.find(DisableEnforcementModal).instance();

    expect((modal.props as any).isOpen).to.equal(false);
  });
});
