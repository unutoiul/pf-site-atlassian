import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import { spy } from 'sinon';

import AkButton from '@atlaskit/button';

import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import { EnableEnforcementModalImpl } from './two-step-verification-enforce-modal';

describe('TwoStepVerificationEnableEnforcementModal', () => {

  let enable;
  let hide;
  let analytics;

  beforeEach(() => {
    enable = spy();
    hide = spy();
    analytics = spy();
  });

  function mountModal() {
    const component = (
      <EnableEnforcementModalImpl
        isOpen={true}
        enableTwoStepVerificationEnforcement={enable}
        hideModal={hide}
        buttonAnalyticsData={analytics}
        intl={createMockIntlProp()}
      />
    );

    return mount(component, createMockIntlContext());
  }

  it('deactivate button should invoke enableTwoStepVerificationEnforcement on click', () => {
    const modal = mountModal();
    const enableButton = modal.find(AkButton).at(0);

    enableButton.simulate('click');
    expect(enable.callCount).to.equal(1);
  });

  it('hide button should invoke hideModal on click', () => {
    const modal = mountModal();
    const cancelButton = modal.find(AkButton).at(1);

    cancelButton.simulate('click');
    expect(hide.callCount).to.equal(1);
  });
});
