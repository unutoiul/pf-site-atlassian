import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import { spy } from 'sinon';

import AkButton from '@atlaskit/button';

import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import { DisableEnforcementModalImpl } from './two-step-verification-enforce-disable-modal';

describe('TwoStepVerificationDisableEnforcementModal', () => {

  let disable;
  let hide;
  let analytics;

  beforeEach(() => {
    disable = spy();
    hide = spy();
    analytics = spy();
  });

  function mountModal() {
    const handlers = { disable, hide };
    const component = (
      <DisableEnforcementModalImpl
        isOpen={true}
        disableTwoStepVerificationEnforcement={handlers.disable}
        hideModal={handlers.hide}
        buttonAnalyticsData={analytics}
        intl={createMockIntlProp()}
      />
    );

    return mount(component, createMockIntlContext());
  }

  it('has two paragraphs', () => {
    const modal = mountModal();
    const paragraphs = modal.find('p');

    expect(paragraphs).to.have.length(2);
  });

  it('deactivate button should invoke disableTwoStepVerificationEnforcement on click', () => {
    const modal = mountModal();
    const disableButton = modal.find(AkButton).at(0);

    disableButton.simulate('click');
    expect(disable.callCount).to.equal(1);
  });

  it('hide button should invoke hideModal on click', () => {
    const modal = mountModal();
    const cancelButton = modal.find(AkButton).at(1);

    cancelButton.simulate('click');
    expect(hide.callCount).to.equal(1);
  });
});
