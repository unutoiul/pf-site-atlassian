import { ApolloQueryResult } from 'apollo-client';
import { MutationOpts, QueryResult } from 'react-apollo';
import { RouteComponentProps } from 'react-router-dom';

import { DisableTwoStepVerificationEnforcementMutation, DisableTwoStepVerificationEnforcementMutationVariables, EnforceTwoStepVerificationMutation } from '../../schema/schema-types';
import { OrgAnalyticsData } from '../organizations/organization-analytics';

export interface Sidecar {
  enforced: boolean;
}

export interface Modal {
  isOpen: boolean;
  header: React.ReactNode;
  content: React.ReactNode;
  submitButtonContent: React.ReactNode;
  cancelButtonContent: React.ReactNode;
  buttonAnalyticsData(subject: string): OrgAnalyticsData;
  onSubmit(): void;
  hideModal(): void;
}

export interface DisableEnforcementModalProps {
  isOpen: boolean;
  buttonAnalyticsData(subject: string): OrgAnalyticsData;
  disableTwoStepVerificationEnforcement(): void;
  hideModal(): void;
}

export interface EnableEnforcementModalProps {
  isOpen: boolean;
  buttonAnalyticsData(subject: string): OrgAnalyticsData;
  enableTwoStepVerificationEnforcement(): void;
  hideModal(): void;
}

export interface TwoStepVerificationError {
  subject: string;
  err: any;
}

interface OrgParams {
  orgId: string;
}

// This interface should be using proper types for mutations & ChildProps.
// Unfortunately Apollo Client ignores custom scalar types and makes dateEnforced a String instead of a Date.
// Related issue https://github.com/apollographql/apollo-client/issues/585.
export interface Page extends RouteComponentProps<OrgParams> {
  data: QueryResult & {
    organization: {
      security: {
        twoStepVerification: {
          dateEnforced?: Date,
        },
      },
    },
  };
  enableTwoStepVerificationEnforcement(opts: MutationOpts & { variables: {id: string, date: Date} }): Promise<ApolloQueryResult<EnforceTwoStepVerificationMutation>>;
  disableTwoStepVerificationEnforcement(opts: MutationOpts & { variables: DisableTwoStepVerificationEnforcementMutationVariables }): Promise<ApolloQueryResult<DisableTwoStepVerificationEnforcementMutation>>;
}
