import * as React from 'react';

import { Button as AnalyticsButton } from 'common/analytics/components';

import { ModalDialog } from 'common/modal';

import { Modal } from './two-step-verification.prop-types';

export class TwoStepVerificationModal extends React.PureComponent<Modal> {

  public render() {
    const dialogFooter = (
      <div>
        <AnalyticsButton onClick={this.props.onSubmit} appearance="primary" analyticsData={this.props.buttonAnalyticsData('update')}>
          {this.props.submitButtonContent}
        </AnalyticsButton>
        <AnalyticsButton onClick={this.props.hideModal} appearance="subtle-link" analyticsData={this.props.buttonAnalyticsData('cancel')}>
          {this.props.cancelButtonContent}
        </AnalyticsButton>
      </div>
    );

    return (
      <ModalDialog
        width="small"
        header={this.props.header}
        footer={dialogFooter}
        onClose={this.props.hideModal}
        isOpen={this.props.isOpen}
        children={this.props.content}
      />
    );
  }
}
