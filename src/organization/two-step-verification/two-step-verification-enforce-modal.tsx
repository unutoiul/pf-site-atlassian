import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import { TwoStepVerificationModal } from './two-step-verification-modal';
import { EnableEnforcementModalProps } from './two-step-verification.prop-types';

const messages = defineMessages({
  title: {
    id: 'organization.two.step.verification.enforce.modal.title',
    defaultMessage: 'Enforce two-step verification?',
  },
  descriptionLineOne: {
    id: 'organization.two.step.verification.enforce.modal.line.one',
    defaultMessage: 'All your existing users will be required to enable two-step verification when they next log in. We\'ll send them an email explaining how to do that.',
  },
  descriptionLineTwo: {
    id: 'organization.two.step.verification.enforce.modal.line.two',
    defaultMessage: 'All new users will need to enable two-step verification when they sign up.',
  },
  enforce: {
    id: 'organization.two.step.verification.enforce.modal.enforce',
    defaultMessage: 'Enforce',
  },
  cancel: {
    id: 'organization.two.step.verification.enforce.modal.cancel',
    defaultMessage: 'Cancel',
  },
});

export class EnableEnforcementModalImpl extends React.PureComponent<EnableEnforcementModalProps & InjectedIntlProps> {

  public render() {
    const { formatMessage } = this.props.intl;

    const dialogContent = (
      <div>
        <p>
          {formatMessage(messages.descriptionLineOne)}
        </p>
        <p>
          {formatMessage(messages.descriptionLineTwo)}
        </p>
      </div>
    );

    return (
      <TwoStepVerificationModal
        isOpen={this.props.isOpen}
        header={formatMessage(messages.title)}
        content={dialogContent}
        onSubmit={this.props.enableTwoStepVerificationEnforcement}
        hideModal={this.props.hideModal}
        submitButtonContent={formatMessage(messages.enforce)}
        cancelButtonContent={formatMessage(messages.cancel)}
        buttonAnalyticsData={this.props.buttonAnalyticsData}
      />
    );
  }
}

export const EnableEnforcementModal = injectIntl<EnableEnforcementModalProps>(EnableEnforcementModalImpl);
