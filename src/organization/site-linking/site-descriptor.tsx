import * as React from 'react';
import styled from 'styled-components';

import { AkRadio, AkRadioProps } from '@atlaskit/field-radio-group';
import { gridSize as akGridSize } from '@atlaskit/theme';

import { Product } from '../../schema/schema-types';

interface Props {
  site: string;
  products: Product[];
  withRadio?: AkRadioProps;
}

const Bold = styled.span`
  font-weight: 600;
`;
const RadioPadding = styled.div`
  padding-left: ${akGridSize() * 3}px;
`;

const productNames = (products): string => (
  products.map(product => product.name).join(', ')
);

export class SiteDescriptor extends React.Component<Props> {
  public render() {
    const {
      site,
      products,
      withRadio,
    } = this.props;

    if (withRadio) {
      if (products && products.length > 0) {
        return (
          <React.Fragment>
            <AkRadio {...withRadio}>
              <Bold>{productNames(products)}</Bold>
            </AkRadio>
            <RadioPadding>{site}</RadioPadding>
          </React.Fragment>
        );
      }

      return (
        <AkRadio {...withRadio}>
          {site}
        </AkRadio>
      );
    }

    if (products && products.length > 0) {
      return (
        <React.Fragment>
          <Bold>{productNames(products)}</Bold>
          <div>{site}</div>
        </React.Fragment>
      );
    }

    return site;
  }
}
