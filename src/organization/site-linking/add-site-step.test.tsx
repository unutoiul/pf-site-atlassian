import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { spy, stub } from 'sinon';

import AkEmptyState from '@atlaskit/empty-state';
import AkSpinner from '@atlaskit/spinner';

import { FlagDescriptor } from 'common/flag';

import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import { AddSiteStepImpl } from './add-site-step';
import { DrawerButtons } from './drawer-buttons';
import { SiteData } from './org-site-linking-types';
import { SiteDescriptor } from './site-descriptor';

const products = [
  {
    key: 'jira-software',
    name: 'Jira Software',
  },
  {
    key: 'confluence',
    name: 'Confluence',
  },
];

describe('AddSiteStep', () => {
  class Helper {
    private _wrapper: ShallowWrapper<AddSiteStepImpl['props']>;

    private getOrgSiteAdminsPromiseResolveCallback!: (...args: any[]) => any;
    private getOrgSiteAdminsPromiseRejectCallback!: (...args: any[]) => any;
    private getOrgSiteAdminsPromise = new Promise((resolve, reject) => {
      this.getOrgSiteAdminsPromiseResolveCallback = resolve;
      this.getOrgSiteAdminsPromiseRejectCallback = reject;
    });

    private getOrgSiteAdminsStub = stub().returns(this.getOrgSiteAdminsPromise);
    private onSiteSelectedSpy = spy();
    private hideFlagSpy = spy();
    private showFlagSpy = spy();
    private sendUIEventSpy = spy();

    constructor() {
      const analyticsClient = { sendUIEvent: this.sendUIEventSpy } as any;
      // TODO react-apollo upgrade: figure out how to remove RouteComponentProps from component props
      this._wrapper = shallow(
        (
          <AddSiteStepImpl
            intl={createMockIntlProp()}
            getOrgSiteAdmins={this.getOrgSiteAdminsStub}
            hideFlag={this.hideFlagSpy}
            showFlag={this.showFlagSpy}
            onSiteSelected={this.onSiteSelectedSpy}
            loading={true}
            sites={[]}
            analyticsClient={analyticsClient}
            match={{} as any}
            location={{} as any}
            history={{} as any}
            totalOrganizations={1}
            totalOrganizationsLoadError={undefined}
          />
        ),
        createMockIntlContext(),
      );
    }

    public loadingFinished(sitesOrError: AddSiteStepImpl['props']['sites'] | 'withError'): void {
      if (sitesOrError === 'withError') {
        this.wrapper.setProps({
          loading: false,
          error: {} as any,
        });
      } else {
        this.wrapper.setProps({
          loading: false,
          sites: sitesOrError,
        });
      }
    }

    public withPreLoadedSiteData(data: SiteData): void {
      this.wrapper.setProps({ defaultSiteData: data });
    }

    public clickNext(): void {
      this.expectNextButtonToBe('enabled');
      this.nextButton.onClick!();
    }

    public selectSite(siteId: string): void {
      const siteButton = this.radioButtons.find(({ value }) => value === siteId)!;
      siteButton.select();
    }

    public expectSitesToBe(state: Array<{ selected: boolean; name: string; }> | 'loading' | 'inError'): void {
      if (state === 'loading') {
        expect(this.wrapper.find(AkSpinner)).to.have.lengthOf(1, 'the sites were expected to be loading');
      } else if (state === 'inError') {
        expect(this.wrapper.find(AkEmptyState)).to.have.lengthOf(1, 'error element was expected to be found in table');
      } else {
        const actual = this.radioButtons.map(({ text, isSelected }) => ({ selected: isSelected, name: text }));
        expect(actual).to.deep.equal(state);
      }
    }

    public expectNextButtonToBe(state: 'disabled' | 'enabled'): void {
      expect(this.nextButton.isDisabled ? 'disabled' : 'enabled').to.equal(state, 'next button is in wrong state');
    }

    public expectSiteAdminsRequestToBeFired(): void {
      expect(this.getOrgSiteAdminsStub.callCount).to.equal(1, 'site admins request was expected to be fired');
    }

    public expectSiteAdminsRequestToNotBeFired(): void {
      expect(this.getOrgSiteAdminsStub.callCount).to.equal(0, 'site admins request was expected to *not* be fired');
    }

    public expectErrorFlagToBeShownWithSiteName(siteName: string): void {
      expect(this.showFlagSpy.callCount).to.equal(1, 'error flag was expected to be shown');
      const flag: FlagDescriptor = this.showFlagSpy.lastCall.args[0];

      expect(flag.description).to.include(siteName);
    }

    public expectOnSiteSelectedToBeFiredWith(data: SiteData): void {
      expect(this.onSiteSelectedSpy.callCount).to.equal(1, '"onSiteSelected" was expected to be fired');

      expect(this.onSiteSelectedSpy.lastCall.args).to.deep.equal([data]);
    }

    public async loadingSiteAdminsFinished(dataOrError: object[] | 'error') {
      if (dataOrError === 'error') {
        this.getOrgSiteAdminsPromiseRejectCallback('some error');
      } else {
        this.getOrgSiteAdminsPromiseResolveCallback(dataOrError);
      }

      try {
        await this.getOrgSiteAdminsPromise;
      } catch (_) {
        // no handling is necessary here
      }
    }

    public expectRadioButtonsToBe(state: 'disabled' | 'enabled'): void {
      const radioButtonStates = this.radioButtons.map(({ isDisabled }) => isDisabled);

      const expectedDisabledStates = Array(radioButtonStates.length).fill(state === 'disabled');
      expect(radioButtonStates).to.deep.equal(expectedDisabledStates);
    }

    public expectAnalyticsToBeSent(): void {
      expect(this.sendUIEventSpy.called).to.equal(true);
    }

    private get wrapper() {
      this._wrapper.update();

      return this._wrapper;
    }

    private get nextButton() {
      return this.wrapper.find(DrawerButtons).prop('next');
    }

    private get radioButtons(): Array<{
      value: string;
      isSelected: boolean;
      isDisabled: boolean;
      text: string;
      select(): void;
    }> {
      const rows = this.wrapper.find(SiteDescriptor).map(sd => sd.props());

      return rows.map((row) => ({
        isSelected: !!row.withRadio!.isSelected,
        text: row.site,
        value: row.withRadio!.value,
        isDisabled: !!row.withRadio!.isDisabled,
        select: () => {
          row.withRadio!.onChange({ target: { value: row.withRadio!.value } } as any);
        },
      }));
    }
  }

  let helper: Helper;

  beforeEach(() => {
    helper = new Helper();
  });

  describe('sites table', () => {
    it('should have a loading state', () => {
      helper.expectSitesToBe('loading');
    });

    it('should have an error state', () => {
      helper.loadingFinished('withError');

      helper.expectSitesToBe('inError');
    });

    it('should have a row for every site when loaded', () => {
      helper.loadingFinished([
        { id: 'site-1', siteUrl: 'Site #1', products: [] },
        { id: 'site-2', siteUrl: 'Site #2', products: [] },
      ]);

      helper.expectSitesToBe([
        { name: 'Site #1', selected: false },
        { name: 'Site #2', selected: false },
      ]);
    });

    it('should have a site pre-selected if there is only one site available for linking', () => {
      helper.loadingFinished([
        { id: 'site-1', siteUrl: 'Site #1', products: [] },
      ]);

      helper.expectSitesToBe([
        { name: 'Site #1', selected: true },
      ]);
    });
  });

  describe('"next" button', () => {
    it('should be disabled when loading', () => {
      helper.expectNextButtonToBe('disabled');
    });

    it('should be disabled when loading sites fails', () => {
      helper.loadingFinished('withError');

      helper.expectNextButtonToBe('disabled');
    });

    it('should be disabled if the user has not selected a site yet', () => {
      helper.loadingFinished([
        { id: 'site-1', siteUrl: 'Site #1', products: [] },
        { id: 'site-2', siteUrl: 'Site #2', products: [] },
      ]);

      helper.expectNextButtonToBe('disabled');
    });

    it('should be enabled when the user selects a site', () => {
      helper.loadingFinished([
        { id: 'site-1', siteUrl: 'Site #1', products: [] },
        { id: 'site-2', siteUrl: 'Site #2', products: [] },
      ]);
      helper.selectSite('site-1');

      helper.expectNextButtonToBe('enabled');
    });

    it('should be enabled if there is only one preselected site', () => {
      helper.loadingFinished([
        { id: 'site-1', siteUrl: 'Site #1', products: [] },
      ]);

      helper.expectNextButtonToBe('enabled');
    });
  });

  describe('when "next" button is clicked', () => {
    let selectedSite: Pick<SiteData, 'id' | 'siteUrl' | 'products'>;
    beforeEach(() => {
      helper.loadingFinished([
        { id: 'site-1', siteUrl: 'Site #1', products: [] },
        { id: 'site-2', siteUrl: 'Site #2', products: [] },
      ]);
      helper.selectSite('site-1');
      selectedSite = {
        id: 'site-1',
        siteUrl: 'Site #1',
        products: [],
      };
      helper.clickNext();
    });

    it('should send analytics UI event', async () => {
      helper.expectAnalyticsToBeSent();
    });

    it('should fire a request for site admins', () => {
      helper.expectSiteAdminsRequestToBeFired();
    });

    it('should disable "next" button and all the radio buttons', () => {
      helper.expectNextButtonToBe('disabled');
      helper.expectRadioButtonsToBe('disabled');
    });

    it('should fire "onSiteSelected" event when data has loaded successfully', async () => {
      const admins: any = [{}];
      await helper.loadingSiteAdminsFinished(admins);

      helper.expectOnSiteSelectedToBeFiredWith({ ...selectedSite, admins });
    });

    it('should show an error flag if site admins loading fails', async () => {
      await helper.loadingSiteAdminsFinished('error');

      helper.expectErrorFlagToBeShownWithSiteName(selectedSite.siteUrl);
    });

    it('should re-enable controls if site admins loading fails', async () => {
      await helper.loadingSiteAdminsFinished('error');

      helper.expectNextButtonToBe('enabled');
      helper.expectRadioButtonsToBe('enabled');
    });
  });

  describe('when site data has been loaded previously', () => {
    it('should pre-select the previously loaded site', () => {
      helper.withPreLoadedSiteData({ id: 'site-1', siteUrl: 'Site #1', admins: [{ some: 'admins-data' }] as any, products: [] });
      helper.loadingFinished([
        { id: 'site-1', siteUrl: 'Site #1', products: [] },
        { id: 'site-2', siteUrl: 'Site #2', products: [] },
      ]);

      helper.expectSitesToBe([
        { name: 'Site #1', selected: true },
        { name: 'Site #2', selected: false },
      ]);
      helper.expectNextButtonToBe('enabled');
    });

    it('should not load site admins again if the data for selected site has been fetched already', () => {
      const preLoadedSiteData: SiteData = { id: 'site-1', siteUrl: 'Site #1', admins: [{ some: 'admin-data' }] as any, products };
      helper.withPreLoadedSiteData(preLoadedSiteData);
      helper.loadingFinished([
        { id: 'site-1', siteUrl: 'Site #1', products: [] },
        { id: 'site-2', siteUrl: 'Site #2', products: [] },
      ]);

      helper.selectSite('site-2');
      helper.selectSite('site-1');
      helper.clickNext();

      helper.expectOnSiteSelectedToBeFiredWith(preLoadedSiteData);
      helper.expectSiteAdminsRequestToNotBeFired();
    });

    it('should load site admins if the user has chosen a different site', async () => {
      const preLoadedSiteData: SiteData = { id: 'site-1', siteUrl: 'Site #1', admins: [{ some: 'admin-data' }] as any, products };
      helper.withPreLoadedSiteData(preLoadedSiteData);
      helper.loadingFinished([
        { id: 'site-1', siteUrl: 'Site #1', products: [] },
        { id: 'site-2', siteUrl: 'Site #2', products: [] },
      ]);

      helper.selectSite('site-2');
      helper.clickNext();

      helper.expectSiteAdminsRequestToBeFired();

      const loadedData = [{ some: 'new-data' }];
      await helper.loadingSiteAdminsFinished(loadedData);

      helper.expectOnSiteSelectedToBeFiredWith({
        id: 'site-2',
        siteUrl: 'Site #2',
        admins: [{ some: 'new-data' }] as any,
        products: [],
      });
    });
  });
});
