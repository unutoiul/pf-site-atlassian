import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';

import { FlagProvider } from 'common/flag';

import { createApolloClient } from '../../apollo-client';
import { getRandomUser } from '../../utilities/testing';
import { SiteData } from './org-site-linking-types';
import { SummaryStep } from './summary-step';

function getRandomAdminData(index: number) {
  const { id, displayName, email } = getRandomUser();

  return { id: id + index.toString(10), displayName, email };
}

storiesOf('Organization|Org-site linking', module)
  .add('Step 3. Summary', () => {
    const site: SiteData = {
      id: 'some-site-id',
      siteUrl: 'acme-usa.atlassian.net',
      products: [{
        name: 'Confluence',
        key: 'confluence',
      }],
      admins: [
        { ...getRandomAdminData(1), isOrgAdmin: true },
        { ...getRandomAdminData(2), isCurrentUser: true, isOrgAdmin: true },
        { ...getRandomAdminData(3), isOrgAdmin: false, newRole: 'org-admin' },
        { ...getRandomAdminData(4), isOrgAdmin: false, newRole: 'product-admin' },
        { ...getRandomAdminData(5), isOrgAdmin: false, newRole: 'org-admin' },
        { ...getRandomAdminData(6), isOrgAdmin: false, newRole: 'product-admin' },
      ],
    };

    return (
      // tslint:disable-next-line no-console
      <ApolloProvider client={createApolloClient(console.error.bind(console))}>
        <IntlProvider locale="en">
          <FlagProvider>
            <SummaryStep
              orgId="test-org-id"
              orgName="Acme Inc."
              goBack={action('goBack')}
              onDone={action('onDone')}
              site={site}
            />
          </FlagProvider>
        </IntlProvider>
      </ApolloProvider>
    );
  },
  );
