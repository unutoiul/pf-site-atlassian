import { action } from '@storybook/addon-actions';
import { boolean } from '@storybook/addon-knobs';
import { storiesOf } from '@storybook/react';
import * as React from 'react';

import { SiteDescriptor } from './site-descriptor';

storiesOf('Organization|Org-site linking', module)
  .add('Site descriptor', () => {
    const withRadio = boolean('with radio', true);
    const withProducts = boolean('with products', true);

    return (
      <SiteDescriptor
        withRadio={!withRadio
          ? undefined
          : { onChange: action('onRadioChange'), name: 'radio-name', value: 'something' }
        }
        site="product fabric"
        products={withProducts ? [
          {
            key: 'confluence',
            name: 'Confluence',
          },
          {
            key: 'jira-software',
            name: 'Jira Software',
          },
        ] : []}
      />
    );
  },
);
