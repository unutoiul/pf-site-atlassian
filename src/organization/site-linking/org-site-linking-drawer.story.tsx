// tslint:disable jsx-use-translation-function
import { boolean } from '@storybook/addon-knobs';
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';
import { MemoryRouter as Router, Route } from 'react-router-dom';
import { withoutWhaleShot } from 'whales-story-shots/storybook/decorator';

import AkButton from '@atlaskit/button';
import AkPage from '@atlaskit/page';

import { FlagProvider } from 'common/flag';

import { CurrentUser, FeatureFlag, LinkSiteToOrgMutation, Organization, OrgSite, Product, SiteAdmin, SiteAdminsOrganizationArgs } from '../../schema/schema-types';
import { ApolloClientStorybookWrapper, StorybookAnalyticsClientProvider } from '../../utilities/storybooks';
import { createApolloClientWithOverriddenResolvers } from '../../utilities/testing';
import { OrgSiteLinkingDrawer } from './org-site-linking-drawer';

const resolverData: {
  organization: Partial<Organization>,
  currentUser: Partial<CurrentUser>,
  siteProducts: { [siteId: string]: Product[] },
  siteAdmins: { [siteId: string]: SiteAdmin[] },
  flags: { [flagKey: string]: FeatureFlag },
  mutationResponse: LinkSiteToOrgMutation['linkSiteToOrganization'],
} = {
  organization: {},
  currentUser: {},
  siteProducts: {},
  siteAdmins: {},
  flags: {},
  mutationResponse: {
    isSuccess: true,
    errors: null,
  } as any,
};
const currentUserId = '557057:80abfbd6-e1b5-4f23-99d9-077658050e25';
const client = createApolloClientWithOverriddenResolvers({
  Query: {
    organization: async () => resolverData.organization,
    currentUser: async () => resolverData.currentUser,
  },
  CurrentUser: {
    id: async () => resolverData.currentUser.id,
    unlinkedSites: async () => resolverData.currentUser.unlinkedSites,
    flag: async (_, { flagKey }) => resolverData.flags[flagKey],
  },
  Site: {
    // products: async ({ id }) => resolverData.siteProducts[id],
  },
  Mutation: {
    linkSiteToOrganization: async () => resolverData.mutationResponse,
  },
  Organization: {
    id: async () => resolverData.organization.id,
    name: async () => resolverData.organization.name,
    siteAdmins: async (_, { cloudId }: SiteAdminsOrganizationArgs) => resolverData.siteAdmins[cloudId],
    flag: async (_, { flagKey }) => resolverData.flags[flagKey],
  },
});

const testOrgId = 'DUMMY_ORG_ID';

interface StorybookExampleProps {
  sites: Array<{
    id: string,
    siteUrl: string,
    products: Product[],
    orgSiteAdmins: SiteAdmin[],
  }>;
  isAssignRolesScreenActive: boolean;
}

const StorybookExample = (props: StorybookExampleProps) => {
  class TestComp extends React.Component<StorybookExampleProps, { isDrawerOpen: boolean }> {
    public state = { isDrawerOpen: false };

    public configureApolloClient = () => {
      const {
        sites,
        isAssignRolesScreenActive,
      } = this.props;

      resolverData.siteProducts = sites.reduce<typeof resolverData['siteProducts']>((prev, cur) => ({ ...prev, [cur.id]: cur.products }), {});
      resolverData.siteAdmins = sites.reduce<typeof resolverData['siteAdmins']>((prev, cur) => ({ ...prev, [cur.id]: cur.orgSiteAdmins }), {});

      resolverData.flags = {
        'site-linking.assign-roles.enabled': {
          id: 'site-linking.assign-roles.enabled',
          value: isAssignRolesScreenActive,
        },
      };
      resolverData.currentUser = {
        id: currentUserId,
        unlinkedSites: sites.map<Pick<OrgSite, 'id' | 'siteUrl' | 'products'>>(s => ({
          id: s.id,
          siteUrl: s.siteUrl,
          products: s.products,
        })) as any,
      };
      resolverData.organization = {
        id: testOrgId,
        name: '_Dummy org name_',
      };
    }

    public render() {
      return (
        <StorybookAnalyticsClientProvider>
          <ApolloClientStorybookWrapper client={client} configureClient={this.configureApolloClient}>
            <ApolloProvider client={client}>
              <IntlProvider locale="en">
                <FlagProvider>
                  <Router initialEntries={[{ pathname: `/${testOrgId}` }]} >
                    <AkPage>
                      <Route path="/:orgId" render={this.renderRoute} />
                    </AkPage>
                  </Router>
                </FlagProvider>
              </IntlProvider>
            </ApolloProvider>
          </ApolloClientStorybookWrapper>
        </StorybookAnalyticsClientProvider>
      );
    }

    private renderRoute = () => {
      return (
        <React.Fragment>
          <AkButton onClick={this.openDrawer} appearance="primary">
            Add site
          </AkButton>

          <OrgSiteLinkingDrawer
            isOpen={this.state.isDrawerOpen}
            onClose={this.closeDrawer}
          />
        </React.Fragment>
      );
    }

    private openDrawer = () => this.setState({ isDrawerOpen: true });
    private closeDrawer = () => this.setState({ isDrawerOpen: false });
  }

  return <TestComp {...props} />;
};

function shuffleArray<T>(array: T[]) {
  const copy = [...array];

  for (let i = copy.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [copy[i], copy[j]] = [copy[j], copy[i]];
  }

  return copy;
}

storiesOf('Organization|Org-site linking', module)
  .add('Full flow', withoutWhaleShot(() => {
    const isAssignRolesScreenActive = boolean('Assign roles screen active', false);
    const isProductsDataAvailable = boolean('With data about site products', true);

    const allProducts = {
      jira: {
        key: 'jira-software',
        name: 'Jira Software',
      },
      jsd: {
        key: 'jira-service-desk',
        name: 'Jira Service Desk',
      },
      confluence: {
        key: 'confluence',
        name: 'Confluence',
      },
      stride: {
        key: 'stride',
        name: 'Stride',
      },
    };
    const allAdmins = {
      currentUser: {
        id: currentUserId,
        displayName: 'Jose Reyes',
        email: 'jreyes@acme-usa.com',
        isOrgAdmin: true,
      } as SiteAdmin,
      will: {
        id: '557057:4a24f93a-04b0-44ab-93f4-424b02d8dcb7',
        displayName: 'Will Jones',
        email: 'wjones@acme-usa.com',
        isOrgAdmin: false,
      } as SiteAdmin,
      amy: {
        id: '557057:e095c181-83c3-4f79-acfb-d947e6ebca33',
        displayName: 'Amy Whitmore',
        email: 'awhitmore@acme-usa.com',
        isOrgAdmin: false,
      } as SiteAdmin,
      james: {
        id: '557057:5bc5f1a2-ba3f-472f-aeef-7eb41a9fba97',
        displayName: 'James Reed',
        email: 'jreed@acme-usa.com',
        isOrgAdmin: true,
      } as SiteAdmin,
    };
    const allAdminsArray = [allAdmins.amy, allAdmins.currentUser, allAdmins.james, allAdmins.will];
    const sites: StorybookExampleProps['sites'] = [
      {
        id: 'site-1',
        siteUrl: 'acme-usa.atlassian.net',
        products: isProductsDataAvailable ? [allProducts.confluence, allProducts.jira] : [],
        orgSiteAdmins: shuffleArray(allAdminsArray),
      },
      {
        id: 'site-2',
        siteUrl: 'acme-eu.atlassian.net',
        products: isProductsDataAvailable ? [allProducts.jira, allProducts.jsd] : [],
        orgSiteAdmins: shuffleArray(allAdminsArray),
      },
      {
        id: 'site-3',
        siteUrl: 'acme-asia.atlassian.net',
        products: isProductsDataAvailable ? [allProducts.confluence, allProducts.jira, allProducts.stride] : [],
        orgSiteAdmins: shuffleArray(allAdminsArray),
      },
    ];

    return (
      <StorybookExample
        isAssignRolesScreenActive={isAssignRolesScreenActive}
        sites={sites}
      />
    );
  },
  ))
;
