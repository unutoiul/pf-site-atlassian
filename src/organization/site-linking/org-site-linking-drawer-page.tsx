import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';

import { ShowDrawerProps, withDrawerCloseSubscriber, WithDrawerCloseSubscriberProps, withShowDrawer } from 'common/drawer';

import { OrgRouteProps } from '../routes';

class OrgSiteLinkingDrawerPageImpl extends React.Component<ShowDrawerProps & WithDrawerCloseSubscriberProps & RouteComponentProps<OrgRouteProps>> {

  public render() {
    return null;
  }

  public componentDidMount() {
    const { showDrawer, whenDrawerClosed } = this.props;
    showDrawer('ORG_SITE_LINKING');
    whenDrawerClosed(this.onDrawerClosed, 'ORG_SITE_LINKING');
  }

  private onDrawerClosed = () => {
    // Currently this page goes back to launchpad, this would need to be updated if anyone else needs to use it.
    this.props.history.push('/');
  }
}

export const OrgSiteLinkingDrawerPage =
withDrawerCloseSubscriber(
  withShowDrawer(
    withRouter(
      OrgSiteLinkingDrawerPageImpl,
    ),
  ),
);
