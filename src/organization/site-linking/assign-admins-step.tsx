import * as React from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';
import styled from 'styled-components';

import { DynamicTableStateless as AkDynamicTableStateless } from '@atlaskit/dynamic-table';
import AkLozenge from '@atlaskit/lozenge';
import AkSelect from '@atlaskit/select';
import { gridSize as akGridSize } from '@atlaskit/theme';

import { Account } from 'common/account';

import { DrawerButtons } from './drawer-buttons';
import { StepInfo, StepTitle } from './layout-components';
import { SiteAdminData, SiteAdminRole, SiteData } from './org-site-linking-types';

const messages = defineMessages({
  title: {
    id: 'organization.site-linking.drawer.assign-admins-step.title',
    defaultMessage: 'Admin permissions',
    description: 'This text is displayed as a heading when an admin is linking site to an organization. At this step the user is going over the site admins and choosing who of them will be org admin, and who will remain product admin',
  },
  info: {
    id: 'organization.site-linking.drawer.assign-admins-step.info',
    defaultMessage: 'Assign existing site admins new roles to manage access to settings, billing and security features.',
    description: '"Assign" is a call to action. This sentence is description of the step the user will be taking.',
  },
  orgAdminHeading: {
    id: 'organization.site-linking.drawer.assign-admins-step.org-admin-role.heading',
    defaultMessage: 'Organization admins',
    description: 'Used as the heading of a section.',
  },
  orgAdminInfo: {
    id: 'organization.site-linking.drawer.assign-admins-step.org-admin-role.info',
    defaultMessage: 'Access to all managed accounts, sites and features in the organization.',
    description: 'Description of what privileges does an "Organization admin" have.',
  },
  productAdminHeading: {
    id: 'organization.site-linking.drawer.assign-admins-step.product-admin-role.heading',
    defaultMessage: 'Product admin',
    description: 'Used as the heading of a section.',
  },
  productAdminInfo: {
    id: 'organization.site-linking.drawer.assign-admins-step.product-admin-role.info',
    defaultMessage: 'No access to billing, and user management feature.',
    description: 'Description of what privileges will user XYZ have after the current user decides to make them product admin.',
  },
  column1Heading: {
    id: 'organization.site-linking.drawer.assign-admins-step.column1-heading',
    defaultMessage: 'Current {site} admins',
    description: 'A header for a table column. This column will display the names of users (i.e. "First Last"). All of these users are site admins for "site". The "site" variable will be the url of a site (e.g. acme.atlassian.net)',
  },
  column2Heading: {
    id: 'organization.site-linking.drawer.assign-admins-step.column2-heading',
    defaultMessage: 'Assign admin roles',
    description: 'A header for a table column. This column will display selectors with possible admin roles (i.e. "organization admin" or "product admin"). The user will be going over the displayed data and choosing roles for every table row.',
  },
  youLozenge: {
    id: 'organization.site-linking.drawer.assign-admins-step.you-lozenge',
    defaultMessage: 'You',
    description: 'This text is displayed in a lozenge to give the user a visual indication that this is them.',
  },
  productAdminSelectValue: {
    id: 'organization.site-linking.drawer.assign-admins-step.role-select.product-admin',
    defaultMessage: 'Product admin',
    description: 'This text is displayed as an option in admin role selector',
  },
  orgAdminSelectValue: {
    id: 'organization.site-linking.drawer.assign-admins-step.role-select.org-admin',
    defaultMessage: 'Org admin',
    description: 'This text is displayed as an option in admin role selector',
  },
});

const SplitView = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: ${akGridSize() * 4}px;

  > div {
    flex: 1;
  }

  > :nth-child(1) {
    padding-right: ${akGridSize() * 2}px;
  }

  > :nth-child(2) {
    padding-left: ${akGridSize() * 2}px;
  }
`;

interface Props {
  site: SiteData;
  onRolesAssigned(site: SiteData): void;
  goBack(): void;
}

interface AdminRoles {
  [adminId: string]: SiteAdminRole;
}

interface State {
  adminRoles: AdminRoles;
  // tslint:disable-next-line:react-unused-props-and-state It is actually used in `getDerivedStateFromProps` function
  prevAdmins: SiteAdminData[] | undefined;
}

export class AssignAdminsStep extends React.Component<Props, State> {
  public readonly state: Readonly<State> = {
    adminRoles: {},
    prevAdmins: undefined,
  };

  public static getDerivedStateFromProps(nextProps: Readonly<Props>, prevState: State): Partial<State> | null {
    if (nextProps.site.admins !== prevState.prevAdmins) {
      return {
        adminRoles: AssignAdminsStep.calculateInitialRoles(nextProps.site.admins),
        prevAdmins: nextProps.site.admins,
      };
    }

    return null;
  }

  private static calculateInitialRoles(admins: SiteAdminData[]): AdminRoles {
    return admins.reduce<AdminRoles>((roles, admin) => {
      roles[admin.id] = admin.isOrgAdmin || admin.isCurrentUser
        ? 'org-admin'
        : admin.newRole || 'product-admin';

      return roles;
    }, {});
  }

  public render() {
    return (
      <React.Fragment>
        <StepTitle>
          <FormattedMessage {...messages.title} />
        </StepTitle>
        <StepInfo>
          <FormattedMessage {...messages.info} tagName="p" />
        </StepInfo>

        <SplitView>
          <div>
            <FormattedMessage {...messages.orgAdminHeading} tagName="h3" />
            <FormattedMessage {...messages.orgAdminInfo} tagName="p" />
          </div>
          <div>
            <FormattedMessage {...messages.productAdminHeading} tagName="h3" />
            <FormattedMessage {...messages.productAdminInfo} tagName="p" />
          </div>
        </SplitView>

        {this.renderAdminsTable()}
        <DrawerButtons
          back={{ onClick: this.onBackClick }}
          next={{ onClick: this.onNextClick }}
        />
      </React.Fragment>
    );
  }

  private onBackClick = () => {
    this.props.goBack();
  }

  private renderAdminsTable() {
    const {
      site: {
        siteUrl,
        admins,
      },
    } = this.props;

    const head = {
      cells: [
        {
          key: 'current-admins',
          content: (
            <FormattedMessage
              {...messages.column1Heading}
              values={{
                site: (
                  <strong>{siteUrl}</strong>
                ),
              }}
              tagName="h4"
            />
          ),
          width: 70,
        },
        {
          key: 'assign-roles',
          content: <FormattedMessage {...messages.column2Heading} tagName="h4" />,
          width: 30,
        },
      ],
    };

    const rows = admins.map(admin => ({
      cells: [
        {
          key: 'current-admins',
          content: (
            <Account
              id={admin.id}
              displayName={admin.displayName}
              email={admin.email}
              lozenges={admin.isCurrentUser
                ? [(
                  <AkLozenge key="you">
                    <FormattedMessage {...messages.youLozenge} />
                  </AkLozenge>
                )]
                : []}
            />
          ),
        },
        {
          key: 'assign-roles',
          content: this.generateRoleSelector(admin),
        },
      ],
    }));

    return (
      <AkDynamicTableStateless
        head={head}
        rows={rows}
      />
    );
  }

  private onNextClick = () => {
    const {
      site: {
        id,
        siteUrl,
        products,
        admins,
      },
      onRolesAssigned,
    } = this.props;

    onRolesAssigned({
      id,
      siteUrl,
      products,
      admins: admins.map(admin => ({
        ...admin,
        newRole: admin.isOrgAdmin || admin.isCurrentUser
          ? 'org-admin'
          : this.state.adminRoles[admin.id],
      })),
    });
  }

  private generateRoleSelector(admin: SiteAdminData) {
    const options = [
      {
        label: <FormattedMessage {...messages.productAdminSelectValue} />,
        value: `product-admin|${admin.id}`,
      },
      {
        label: <FormattedMessage {...messages.orgAdminSelectValue} />,
        value: `org-admin|${admin.id}`,
      },
    ];

    const value = this.state.adminRoles[admin.id] === 'product-admin'
      ? options[0]
      : options[1];

    return (
      <AkSelect
        isSearchable={false}
        isDisabled={admin.isOrgAdmin || admin.isCurrentUser}
        value={value}
        onChange={this.onSelectChange}
        options={options}
      />
    );
  }

  private onSelectChange = ({ value }: { value: string }) => {
    const separatorIndex = value.indexOf('|');
    const role = value.substring(0, separatorIndex);
    const adminId = value.substring(separatorIndex + 1);

    this.setState(prevState => ({
      adminRoles: {
        ...prevState.adminRoles,
        [adminId]: role as SiteAdminRole,
      },
    }));
  }
}
