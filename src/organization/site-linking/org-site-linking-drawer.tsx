import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import { defineMessages, FormattedMessage } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';

import { ProgressTracker as AkProgressTracker } from '@atlaskit/progress-tracker';

import { AnalyticsClientProps, closeIcon, withAnalyticsClient } from 'common/analytics';
import { FocusedTask } from 'common/focused-task';
import { withOrganizationsCount, WithOrgsCountProps } from 'common/user';
import { withSitesCount, WithSitesCountProps } from 'common/user/with-sites-count';

import { OrgDataForLinkingQuery, OrgDataForLinkingQueryVariables } from '../../schema/schema-types';
import { AddSiteStep } from './add-site-step';
import { AssignAdminsStep } from './assign-admins-step';
import { DrawerContainer, DrawerContent } from './layout-components';
import { LinkingSuccessfulModal } from './linking-successful-modal';
import orgDataQuery from './org-data.query.graphql';
import { SiteAdminData, SiteData } from './org-site-linking-types';
import { SummaryStep } from './summary-step';

const messages = defineMessages({
  step1: {
    id: 'organization.site-linking.drawer.step1',
    defaultMessage: 'Add a site',
    description: 'This text is displayed in the progress tracker when an admin is linking site to an organization. "Site" refers to an Atlassian site (e.g. example-company.atlassian.net)',
  },
  step2: {
    id: 'organization.site-linking.drawer.step2',
    defaultMessage: 'Admin permissions',
    description: 'This text is displayed in the progress tracker when an admin is linking site to an organization. At this step the user is going over the site admins and choosing who of them will be org admin, and who will remain product admin',
  },
  step3: {
    id: 'organization.site-linking.drawer.step3',
    defaultMessage: 'Summary',
    description: 'This text is displayed in the progress tracker when an admin is linking site to an organization. This is the last step where a user is confirming the operation.',
  },
  orgDataFetchError: {
    id: 'organization.site-linking.drawer.org-data-load-failed',
    defaultMessage: 'Failed to load organization data at this time. Please try again later.',
    description: 'This text is displayed when we fail to load the data about organization from the server.',
  },
});

interface State {
  step: 1 | 2 | 3 | 4;
  siteData?: SiteData;
  previousIsOpen?: boolean;
}

interface OwnProps {
  isOpen: boolean;
  onClose(): void;
}

// TODO react-apollo upgrade: figure out how to remove RouteComponentProps from component props
type AllProps = RouteComponentProps<any> & OwnProps & DerivedProps & AnalyticsClientProps & WithSitesCountProps & WithOrgsCountProps;
export class OrgSiteLinkingDrawerImpl extends React.Component<AllProps, State> {
  public readonly state: Readonly<State> = {
    step: 1,
  };

  public static getDerivedStateFromProps(props: OrgSiteLinkingDrawerImpl['props'], state: State): Partial<State> | null {
    if (!state.previousIsOpen && props.isOpen || props.isOpen !== state.previousIsOpen) {
      return { step: 1, previousIsOpen: props.isOpen, siteData: undefined };
    }

    return null;
  }

  public render() {
    const { isOpen } = this.props;
    const { step, siteData } = this.state;

    return (
      <React.Fragment>
        {isOpen && (
          <LinkingSuccessfulModal
            isOpen={step === 4}
            onClose={this.close}
            site={siteData ? siteData : { id: '', siteUrl: '', products: [], admins: [] }}
          />
        )}
        {step < 4 && (
          <FocusedTask
            isOpen={isOpen}
            onClose={this.close}
          >
            {this.renderContent()}
          </FocusedTask>
        )}
      </React.Fragment>
    );
  }

  private renderContent() {
    const {
      isOpen,
      data,
    } = this.props;
    if (!isOpen) {
      // we are not mounting the content until the drawer gets opened to prevent redundant data fetching
      return null;
    }

    if (!data || data.loading) {
      return null;
    }

    if (data.error || !data.organization) {
      return (
        <DrawerContainer>
          <DrawerContent>
            <FormattedMessage {...messages.orgDataFetchError} />
          </DrawerContent>
        </DrawerContainer>
      );
    }

    const { step } = this.state;

    if (step === 4) {
      return null;
    }

    const progressTrackerItems = [
      {
        id: 'sites',
        label: <FormattedMessage {...messages.step1} />,
        ...this.getStepState(1),
      },
      ...(
        this.isAssignRoleScreenEnabled
          ? [{
            id: 'admins',
            label: <FormattedMessage {...messages.step2} />,
            ...this.getStepState(2),
          }]
          : []
      ),
      {
        id: 'summary',
        label: <FormattedMessage {...messages.step3} />,
        ...this.getStepState(3),
      },
    ];

    return (
      <DrawerContainer>
        <AkProgressTracker items={progressTrackerItems} />
        <DrawerContent>
          {this.getStepContent(data.organization.id, data.organization.name)}
        </DrawerContent>
      </DrawerContainer>
    );
  }

  private close = () => {
    const { onClose, analyticsClient, totalSites, totalOrganizations } = this.props;
    const { step } = this.state;
    if (step !== 4) {
      analyticsClient.sendUIEvent({
        data: closeIcon({ currentSiteCount: totalSites, currentOrgCount: totalOrganizations }),
      });
    }
    onClose();
  }

  private getStepContent(orgId: string, orgName: string) {
    const {
      step,
      siteData,
    } = this.state;

    switch (step) {
      case 1: return (
        <AddSiteStep
          onSiteSelected={this.onSiteSelected}
          defaultSiteData={siteData}
        />
      );
      case 2: return (
        <AssignAdminsStep
          onRolesAssigned={this.onRolesAssigned}
          goBack={this.goBack}
          site={siteData!}
        />
      );
      case 3: return (
        <SummaryStep
          orgId={orgId}
          orgName={orgName}
          onDone={this.linkingDone}
          goBack={this.goBack}
          site={siteData!}
        />
      );
      default:
        return null;
    }
  }

  private linkingDone = () => {
    this.setState({ step: 4 });
  }

  private goBack = () => {
    const { step } = this.state;
    if (step === 1) {
      this.close();

      return;
    }

    if (step === 3 && !this.isAssignRoleScreenEnabled) {
      this.setState({ step: 1 });

      return;
    }

    this.setState({ step: (step - 1) as (1 | 2) });
  }

  private get isAssignRoleScreenEnabled(): boolean {
    const { data } = this.props;

    if (!data || data.loading || data.error) {
      return false;
    }

    return !!(data.currentUser && data.currentUser.flag && data.currentUser.flag.value);
  }

  private onSiteSelected = (siteData: SiteData): void => {
    if (this.isAssignRoleScreenEnabled) {
      this.setState({
        step: 2,
        siteData,
      });
    } else {
      const siteDataWithAllRolesSetToOrgAdmins: SiteData = {
        ...siteData,
        admins: siteData.admins.map<SiteAdminData>(admin => ({
          ...admin,
          newRole: 'org-admin',
        })),
      };

      this.onRolesAssigned(siteDataWithAllRolesSetToOrgAdmins);
    }
  }

  private onRolesAssigned = (siteData: SiteData): void => {
    this.setState({
      step: 3,
      siteData,
    });
  }

  private getStepState(step: number): {
    percentageComplete: number,
    status: 'current' | 'visited' | 'unvisited',
  } {
    const currentStep = this.state.step;

    const status = step === currentStep
      ? 'current'
      : step > currentStep
        ? 'unvisited'
        : 'visited';
    const percentageComplete = step >= currentStep ? 0 : 100;

    return { status, percentageComplete };
  }
}

type DerivedProps = ChildProps<{}, OrgDataForLinkingQuery>;

const withOrgData = graphql<OwnProps & RouteComponentProps<any>, OrgDataForLinkingQuery, OrgDataForLinkingQueryVariables, DerivedProps>(
  orgDataQuery,
  {
    options: (p) => ({
      variables: { orgId: p.match.params.orgId } as OrgDataForLinkingQueryVariables,
    }),
    skip: (p) => !p.match.params.orgId || !p.isOpen,
  },
);

export const OrgSiteLinkingDrawer = withRouter(
  withOrgData(
    withAnalyticsClient(
      withSitesCount(
        withOrganizationsCount(
          OrgSiteLinkingDrawerImpl,
        ),
      ),
    ),
  ),
);
