import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import { ScreenEventSender } from 'common/analytics';

import { createMockAnalyticsClient } from '../../utilities/testing';
import { LinkingSuccessfulModalImpl } from './linking-successful-modal';

const noop = () => null;

describe('linking successful modal', () => {
  it('should not render anything if not open', () => {
    const wrapper = shallow(
      <LinkingSuccessfulModalImpl
        isOpen={false}
        site={{ id: 'my-site-id', siteUrl: 'my-site-url', products: [], admins: [] }}
        totalSites={1}
        totalSitesLoadError={undefined}
        totalOrganizations={1}
        totalOrganizationsLoadError={undefined}
        onClose={noop}
        analyticsClient={createMockAnalyticsClient()}
      />,
    );

    expect(wrapper.type()).to.equal(null);
  });

  it('should send analytics if the modal is open', () => {
    const wrapper = shallow(
      <LinkingSuccessfulModalImpl
        isOpen={true}
        site={{ id: 'my-site-id', siteUrl: 'my-site-url', products: [], admins: [] }}
        totalSites={1}
        totalSitesLoadError={undefined}
        totalOrganizations={1}
        totalOrganizationsLoadError={undefined}
        onClose={noop}
        analyticsClient={createMockAnalyticsClient()}
      />,
    );
    expect(wrapper.find(ScreenEventSender).prop('event')).to.deep.equal({
      data: {
        name: 'siteAddedSuccessModal',
        attributes: { siteId: 'my-site-id' },
      },
    });
  });
});
