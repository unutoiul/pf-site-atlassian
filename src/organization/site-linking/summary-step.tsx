import * as React from 'react';
import { graphql, OptionProps } from 'react-apollo';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import { DynamicTableStateless as AkDynamicTableStateless } from '@atlaskit/dynamic-table';
import { colors as akColors, fontSize as akFontSize, gridSize as akGridSize } from '@atlaskit/theme';

import { Account } from 'common/account';
import { addSiteToOrgSummaryModalScreenEvent, analyticsClient as oldAnalyticsClient, AnalyticsClientProps, backButton, completeButton, ScreenEventSender, withAnalyticsClient } from 'common/analytics';
import { createErrorIcon } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';
import { EmptyTableView } from 'common/table';
import { withOrganizationsCount, WithOrgsCountProps, withSitesCount, WithSitesCountProps } from 'common/user';

import { LinkSiteToOrgMutation, LinkSiteToOrgMutationVariables, RefetchOrgSitesQueryVariables } from '../../schema/schema-types';
import { DrawerButtons } from './drawer-buttons';
import { StepSubTitle, StepTitle } from './layout-components';
import { SiteData } from './org-site-linking-types';
import orgSiteLinkingMutation from './org-site-linking.mutation.graphql';
import refetchOrgSitesQuery from './refetch-org-sites.query.graphql';
import { SiteDescriptor } from './site-descriptor';

const messages = defineMessages({
  title: {
    id: 'organization.site-linking.drawer.summary-step.title',
    defaultMessage: 'Summary',
    description: 'This text is displayed as a heading when an admin is linking site to an organization. This is the last step of the flow, where the user sees the summary of the configuration they have made on previous screens',
  },
  siteSummaryTitle: {
    id: 'organization.site-linking.drawer.summary-step.site-summary-title',
    defaultMessage: 'Site added to {organizationName}',
    description: 'A heading for the section that tells the user which site they are about to add to organization named "organizationName". The user is yet to submit the action',
  },
  adminsSummaryTitle: {
    id: 'organization.site-linking.drawer.summary-step.admins-summary-title',
    defaultMessage: 'Assign site admin new roles',
    description: 'A heading for the section that tells the user which roles they have assigned to site admins.. The user is yet to submit the action',
  },
  orgAdminsSummaryTitle: {
    id: 'organization.site-linking.drawer.summary-step.org-admins-summary-title',
    defaultMessage: 'Organization admins',
    description: 'Used as the heading of a section.',
  },
  orgAdminsSummaryInfo: {
    id: 'organization.site-linking.drawer.summary-step.org-admins-summary-info',
    defaultMessage: 'Existing and future admins will have access to all managed accounts, sites and features in the organization',
    description: 'Description of what privileges does an "Organization admin" have.',
  },
  unknownErrorTitle: {
    id: 'organization.site-linking.drawer.summary-step.error.unknown.title',
    defaultMessage: 'Something went wrong.',
    description: 'The title of a generic error. It is shown when we encountered an unknown problem',
  },
  unknownErrorDescription: {
    id: 'organization.site-linking.drawer.summary-step.error.unknown.description',
    defaultMessage: 'Could not perform the operation at this time. Please try again later. Contact support.atlassian.com if the problem persists.',
    description: 'The description of a generic error. It is shown when we encountered an unknown problem',
  },
  noNewAdminRoles: {
    id: 'organization.site-linking.drawer.summary-step.no-new-admin-roles',
    defaultMessage: 'No new admin roles',
    description: 'Shown to the user when the configuration they chose did not result in any changes to the roles of other admins',
  },
});

interface OwnProps {
  orgId: string;
  orgName: string;
  site: SiteData;
  onDone(): void;
  goBack(): void;
}

const AdminsTableHeadingTitle = styled.div`
  color: ${akColors.heading};
  font-size: ${akFontSize}px;
`;
const AdminsTableHeadingInfo = styled.div`
  color: ${akColors.subtleText};
  font-size: ${akFontSize() * 0.85}px;
  font-weight: 400;
  margin-bottom: ${akGridSize}px;
`;

const TableWithContentSpacing = styled.div`
  tr:first-child > td {
    padding-top: ${akGridSize}px;
  }

  tr:last-child > td {
    padding-bottom: ${akGridSize}px;
  }

  tbody {
    border-bottom: none;
  }
`;

const AdminsTableHeading = ({ title, info }: { title: FormattedMessage.MessageDescriptor, info: FormattedMessage.MessageDescriptor }) => (
  <React.Fragment>
    <AdminsTableHeadingTitle>
      <FormattedMessage {...title} />
    </AdminsTableHeadingTitle>
    <AdminsTableHeadingInfo>
      <FormattedMessage {...info} />
    </AdminsTableHeadingInfo>
  </React.Fragment>
);

const SiteDescriptorContainer = styled.div`
  margin-bottom: ${akGridSize() * 4}px;
`;

interface State {
  linkingInProgress: boolean;
  linkingCompletedWithErrors: boolean;
}

type AllProps = OwnProps & FlagProps & DerivedProps & InjectedIntlProps & AnalyticsClientProps & WithSitesCountProps & WithOrgsCountProps;

export class SummaryStepImpl extends React.Component<AllProps, State> {
  public state: Readonly<State> = {
    linkingInProgress: false,
    linkingCompletedWithErrors: false,
  };

  public render() {
    const {
      orgName,
      site: {
        siteUrl,
        products,
      },
    } = this.props;
    const {
      linkingInProgress,
      linkingCompletedWithErrors,
    } = this.state;

    const eventAttributes = {
      siteId: this.props.site.id,
      orgAdminCount: this.props.site.admins && this.props.site.admins.length,
    };

    return (
      <ScreenEventSender event={addSiteToOrgSummaryModalScreenEvent(eventAttributes)}>
        <StepTitle>
          <FormattedMessage {...messages.title} />
        </StepTitle>

        <StepSubTitle>
          <FormattedMessage {...messages.siteSummaryTitle} values={{ organizationName: orgName }} />
        </StepSubTitle>
        <SiteDescriptorContainer>
          <SiteDescriptor site={siteUrl} products={products} />
        </SiteDescriptorContainer>

        <StepSubTitle>
          <FormattedMessage {...messages.adminsSummaryTitle} />
        </StepSubTitle>
        {this.renderOrgAdminsTable()}

        <DrawerButtons
          back={{
            onClick: this.onBackClick,
            isDisabled: linkingInProgress || linkingCompletedWithErrors,
          }}
          next={{
            onClick: this.onNextClick,
            isDisabled: linkingInProgress || linkingCompletedWithErrors,
            isCompleteAction: true,
          }}
        />
      </ScreenEventSender>
    );
  }

  private renderOrgAdminsTable() {
    const {
      site: {
        admins,
      },
    } = this.props;

    const head = {
      cells: [
        {
          key: 'org-admins',
          content: (
            <AdminsTableHeading
              title={messages.orgAdminsSummaryTitle}
              info={messages.orgAdminsSummaryInfo}
            />
          ),
          width: 100,
        },
      ],
    };

    const rows = admins
      .filter(admin => !admin.isCurrentUser && !admin.isOrgAdmin && admin.newRole === 'org-admin')
      .map(admin => ({
        cells: [
          {
            key: 'user-info',
            content: (
              <Account
                id={admin.id}
                displayName={admin.displayName}
                email={admin.email}
              />
            ),
          },
        ],
      }));

    return (
      <TableWithContentSpacing>
        <AkDynamicTableStateless
          head={head}
          rows={rows}
          emptyView={(
            <EmptyTableView>
              <FormattedMessage {...messages.noNewAdminRoles} />
            </EmptyTableView>
          )}
        />
      </TableWithContentSpacing>
    );
  }

  private getOrgAdminCount = () => this.props.site.admins.filter(admin => admin.isOrgAdmin).length;

  private onBackClick = () => {
    const { analyticsClient, totalSites, site } = this.props;
    analyticsClient.sendUIEvent({
      data: backButton({ currentSiteCount: totalSites, siteId: site.id, siteAdminCount: site.admins.length, currentOrgAdminCount: this.getOrgAdminCount() }),
    });
    this.props.goBack();
  }

  private onNextClick = async () => {
    const {
      analyticsClient,
      onDone,
      linkSiteToOrg,
      orgId,
      totalSites,
      totalOrganizations,
      site: { id: cloudId },
    } = this.props;

    analyticsClient.sendUIEvent({
      data: completeButton({ currentSiteCount: totalSites, currentOrgAdminCount: this.getOrgAdminCount(), currentOrgCount: totalOrganizations }),
    });

    this.setState({ linkingInProgress: true });

    try {
      const result = await linkSiteToOrg(orgId, cloudId);
      if (result.isSuccess) {
        if (!result.errors || result.errors.length === 0) {
          onDone();

          return;
        }

        this.setState({ linkingCompletedWithErrors: true });
      } else {
        this.showErrorFlag();
      }
    } catch (_) {
      this.showErrorFlag();
    }

    this.setState({ linkingInProgress: false });
  }

  private showErrorFlag() {
    const {
      showFlag,
      intl: { formatMessage },
    } = this.props;

    showFlag({
      autoDismiss: false,
      icon: createErrorIcon(),
      id: `org-site-linking-error.${new Date().getTime()}`,
      title: formatMessage(messages.unknownErrorTitle),
      description: formatMessage(messages.unknownErrorDescription),
    });
  }
}

interface DerivedProps {
  linkSiteToOrg(orgId: string, cloudId: string): Promise<LinkSiteToOrgMutation['linkSiteToOrganization']>;
}

const withOrgSiteLinkingMutation = graphql<OwnProps, LinkSiteToOrgMutation, LinkSiteToOrgMutationVariables, DerivedProps>(
  orgSiteLinkingMutation,
  {
    props: (p: OptionProps<OwnProps, LinkSiteToOrgMutation>): DerivedProps => ({
      linkSiteToOrg: async (orgId: string, cloudId: string) => {
        if (!(p.mutate instanceof Function)) {
          const error = new Error('"mutate" was expected to be a function');
          oldAnalyticsClient.onError(error);

          throw error;
        }

        const variables: LinkSiteToOrgMutationVariables = { cloudId, orgId };

        const result = await p.mutate({
          variables,
          refetchQueries: [{
            query: refetchOrgSitesQuery,
            variables: { id: orgId } as RefetchOrgSitesQueryVariables,
          }],
        });

        if (result.errors) {
          return {
            __typename: 'OrgSiteLinkingResult',
            isSuccess: false,
            errors: ['unknown'],
          };
        }

        return result.data.linkSiteToOrganization;
      },
    }),
  },
);

export const SummaryStep = withOrgSiteLinkingMutation(
  withFlag(
    injectIntl(
      withAnalyticsClient(
        withSitesCount(
          withOrganizationsCount(
            SummaryStepImpl,
          ),
        ),
      ),
    ),
  ),
);
