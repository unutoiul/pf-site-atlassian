import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { spy } from 'sinon';

import { DynamicTableStateless as AkDynamicTableStateless } from '@atlaskit/dynamic-table';

import { AccountProps } from 'common/account';

import { createMockIntlContext } from '../../utilities/testing';
import { AssignAdminsStep } from './assign-admins-step';
import { DrawerButtons } from './drawer-buttons';
import { SiteAdminRole, SiteData } from './org-site-linking-types';

describe('AssignAdminsStep', () => {
  class Helper {
    private _wrapper!: ShallowWrapper<AssignAdminsStep['props']>;

    private goBackSpy = spy();
    private onRolesAssignedSpy = spy();

    public init(siteData: SiteData) {
      this._wrapper = shallow(
        (
          <AssignAdminsStep
            goBack={this.goBackSpy}
            onRolesAssigned={this.onRolesAssignedSpy}
            site={siteData}
          />
        ),
        createMockIntlContext(),
      );
    }

    public expectGoBackToBeFired(): void {
      expect(this.goBackSpy.callCount).to.equal(1, 'expected "goBack" callback to be fired, but it was not');
    }

    public clickBack(): void {
      this.backButton.onClick!();
    }

    public expectCurrentUserIndicatorToBePresentFor(displayName: string): void {
      expect(this.getRowDescriptor(displayName).hasYouLozenge).to.be.true(`user with display name "${displayName}" does not have "Current User" indicator`);
    }

    public expectTableToShowUsers(displayNames: string[]): void {
      expect(this.rowDescriptors.map(rd => rd.displayName)).to.deep.equal(displayNames);
    }

    public toggleUserRole(displayName: string): void {
      this.getRowDescriptor(displayName).toggleRole();
    }

    public expectNewUserRolesToBe(newRoles: SiteAdminRole[]): void {
      expect(this.onRolesAssignedSpy.callCount).to.equal(1, 'expected "onRolesAssigned" to have been called');
      const siteData: SiteData = this.onRolesAssignedSpy.lastCall.args[0];

      const actualRoles = siteData.admins.map(a => a.newRole);
      expect(actualRoles).to.deep.equal(newRoles);
    }

    public clickNext(): void {
      this.nextButton.onClick!();
    }

    public expectRoleSelectorsToBe(selectorStates: Array<{ value: SiteAdminRole, disabled: boolean }>): void {
      const actual = this.rowDescriptors.map(rd => ({
        value: rd.roleSelectorValue,
        disabled: !rd.roleSelectorEnabled,
      }));

      expect(actual).to.deep.equal(selectorStates);
    }

    private getRowDescriptor(displayName: string) {
      const rowDescriptor = this.rowDescriptors.find(rd => rd.displayName === displayName);
      expect(rowDescriptor).to.not.be.undefined(`could not find row with display name "${displayName}"`);

      return rowDescriptor!;
    }

    private get wrapper() {
      this._wrapper.update();

      return this._wrapper;
    }

    private get backButton() {
      return this.wrapper.find(DrawerButtons).prop('back')!;
    }

    private get nextButton() {
      return this.wrapper.find(DrawerButtons).prop('next');
    }

    private get rowDescriptors() {
      return this.table.prop('rows')!.map(row => {
        const firstCell: React.ReactElement<AccountProps> = row.cells[0].content as any;
        const secondCell: React.ReactElement<any> = row.cells[1].content as any;

        const isRoleSelectorEnabled = !secondCell.props.isDisabled;
        const roleSelectorCurrentValue = secondCell.props.value.value;

        return {
          displayName: firstCell.props.displayName,
          hasYouLozenge: firstCell.props.lozenges && firstCell.props.lozenges.length > 0,
          roleSelectorEnabled: isRoleSelectorEnabled,
          roleSelectorValue: roleSelectorCurrentValue.startsWith('org-admin|')
            ? 'org-admin'
            : roleSelectorCurrentValue.startsWith('product-admin|')
              ? 'product-admin'
              : undefined,
          toggleRole: () => {
            expect(isRoleSelectorEnabled).to.be.true('expected role selector to be enabled, but it was not');
            const options = secondCell.props.options;
            const otherValue = options.find(o => o.value !== roleSelectorCurrentValue);
            secondCell.props.onChange(otherValue);
          },
        };
      });
    }

    private get table() {
      return this.wrapper.find(AkDynamicTableStateless);
    }
  }

  let helper: Helper;

  beforeEach(() => {
    helper = new Helper();
  });

  it('should invoke "goBack" prop when the back button is clicked', () => {
    helper.init({ admins: [] } as any);
    helper.clickBack();

    helper.expectGoBackToBeFired();
  });

  describe('admin roles table', () => {
    it('should have a row with every admin display name', () => {
      helper.init({
        id: 'site',
        siteUrl: 'Site',
        products: [],
        admins: [
          { displayName: 'admin 1', email: 'admin1@acme.com', id: 'admin-1', isOrgAdmin: false },
          { displayName: 'admin 2', email: 'admin2@acme.com', id: 'admin-2', isOrgAdmin: false },
        ],
      });

      helper.expectTableToShowUsers([
        'admin 1',
        'admin 2',
      ]);
    });

    it('should show a lozenge next to an admin which is current user', () => {
      helper.init({
        id: 'site',
        siteUrl: 'Site',
        products: [],
        admins: [
          { displayName: 'admin 1', email: 'admin1@acme.com', id: 'admin-1', isOrgAdmin: false },
          { displayName: 'admin 2', email: 'admin2@acme.com', id: 'admin-2', isOrgAdmin: false, isCurrentUser: true },
        ],
      });

      helper.expectCurrentUserIndicatorToBePresentFor('admin 2');
    });

    it('should have role selectors disabled for current org admins and current user', () => {
      helper.init({
        id: 'site',
        siteUrl: 'Site',
        products: [],
        admins: [
          { displayName: 'admin 1', email: 'admin1@acme.com', id: 'admin-1', isOrgAdmin: true },
          { displayName: 'admin 2', email: 'admin2@acme.com', id: 'admin-2', isOrgAdmin: false, isCurrentUser: true },
          { displayName: 'admin 3', email: 'admin3@acme.com', id: 'admin-3', isOrgAdmin: true, isCurrentUser: true },
        ],
      });

      helper.expectRoleSelectorsToBe([
        { value: 'org-admin', disabled: true },
        { value: 'org-admin', disabled: true },
        { value: 'org-admin', disabled: true },
      ]);
    });

    it('should set role selectors to "Product admin" initially', () => {
      helper.init({
        id: 'site',
        siteUrl: 'Site',
        products: [],
        admins: [
          { displayName: 'admin 1', email: 'admin1@acme.com', id: 'admin-1', isOrgAdmin: false },
          { displayName: 'admin 2', email: 'admin2@acme.com', id: 'admin-2', isOrgAdmin: false },
          { displayName: 'admin 3', email: 'admin3@acme.com', id: 'admin-3', isOrgAdmin: false },
        ],
      });

      helper.expectRoleSelectorsToBe([
        { value: 'product-admin', disabled: false },
        { value: 'product-admin', disabled: false },
        { value: 'product-admin', disabled: false },
      ]);
    });

    it('should preserve the previously selected roles', () => {
      helper.init({
        id: 'site',
        siteUrl: 'Site',
        products: [],
        admins: [
          { displayName: 'admin 1', email: 'admin1@acme.com', id: 'admin-1', isOrgAdmin: false, newRole: 'org-admin' },
          { displayName: 'admin 2', email: 'admin2@acme.com', id: 'admin-2', isOrgAdmin: false, newRole: 'product-admin' },
          { displayName: 'admin 3', email: 'admin3@acme.com', id: 'admin-3', isOrgAdmin: false, newRole: 'org-admin' },
        ],
      });

      helper.expectRoleSelectorsToBe([
        { value: 'org-admin', disabled: false },
        { value: 'product-admin', disabled: false },
        { value: 'org-admin', disabled: false },
      ]);
    });
  });

  describe('when "next" button is clicked', () => {
    it('should report the role for all modify-able admins as "Product admin" if user has not made any changes', () => {
      helper.init({
        id: 'site',
        siteUrl: 'Site',
        products: [],
        admins: [
          { displayName: 'admin 1', email: 'admin1@acme.com', id: 'admin-1', isOrgAdmin: false },
          { displayName: 'admin 2', email: 'admin2@acme.com', id: 'admin-2', isOrgAdmin: true },
          { displayName: 'admin 3', email: 'admin3@acme.com', id: 'admin-3', isOrgAdmin: true, isCurrentUser: true },
          { displayName: 'admin 4', email: 'admin4@acme.com', id: 'admin-4', isOrgAdmin: false },
        ],
      });
      helper.clickNext();

      helper.expectNewUserRolesToBe([
        'product-admin',
        'org-admin',
        'org-admin',
        'product-admin',
      ]);
    });

    it('should report the roles taking user-made changes into account', () => {
      helper.init({
        id: 'site',
        siteUrl: 'Site',
        products: [],
        admins: [
          { displayName: 'admin 1', email: 'admin1@acme.com', id: 'admin-1', isOrgAdmin: false },
          { displayName: 'admin 2', email: 'admin2@acme.com', id: 'admin-2', isOrgAdmin: true },
          { displayName: 'admin 3', email: 'admin3@acme.com', id: 'admin-3', isOrgAdmin: true, isCurrentUser: true },
          { displayName: 'admin 4', email: 'admin4@acme.com', id: 'admin-4', isOrgAdmin: false },
        ],
      });
      helper.toggleUserRole('admin 1');
      helper.clickNext();

      helper.expectNewUserRolesToBe([
        'org-admin',
        'org-admin',
        'org-admin',
        'product-admin',
      ]);
    });

    it('should report the roles taking user-made changes into account which override the previously selected roles', () => {
      helper.init({
        id: 'site',
        siteUrl: 'Site',
        products: [],
        admins: [
          { displayName: 'admin 1', email: 'admin1@acme.com', id: 'admin-1', isOrgAdmin: false, newRole: 'org-admin' },
          { displayName: 'admin 2', email: 'admin2@acme.com', id: 'admin-2', isOrgAdmin: false, newRole: 'product-admin' },
          { displayName: 'admin 3', email: 'admin3@acme.com', id: 'admin-3', isOrgAdmin: false, newRole: 'org-admin' },
          { displayName: 'admin 4', email: 'admin4@acme.com', id: 'admin-4', isOrgAdmin: false, newRole: 'product-admin' },
        ],
      });
      helper.toggleUserRole('admin 1');
      helper.toggleUserRole('admin 2');
      helper.clickNext();

      helper.expectNewUserRolesToBe([
        'product-admin',
        'org-admin',
        'org-admin',
        'product-admin',
      ]);
    });
  });
});
