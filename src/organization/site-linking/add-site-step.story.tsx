// tslint:disable jsx-use-translation-function
import { action } from '@storybook/addon-actions';
import { boolean, number } from '@storybook/addon-knobs';
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';

import { FlagProvider } from 'common/flag';

import { createApolloClient } from '../../apollo-client';
import { createMockIntlProp } from '../../utilities/testing';
import { AddSiteStepImpl } from './add-site-step';

storiesOf('Organization|Org-site linking', module)
  .add('Step 1. Add site', () => {
    const isSiteAdminsFetchSuccessful = boolean('Loading site admins successful', true);
    const isSitesFetchSuccessful = boolean('Loading sites successful', true);
    const getSitesApiDelay = number('Get Sites delay (ms)', 1000);
    const getSiteAdminsApiDelay = number('Get Site admins delay (ms)', 1000);

    interface State { loading: boolean; error?: any; sites: AddSiteStepImpl['props']['sites']; }

    class Example extends React.Component<{}, State> {
      public readonly state: Readonly<State> = {
        loading: true,
        sites: [],
      };

      public componentDidMount() {
        setTimeout(() => {
          this.setState({
            loading: false,
            error: isSitesFetchSuccessful ? undefined : {},
            sites: isSitesFetchSuccessful
              ? [
                { id: 'site-1',
                  siteUrl: 'Site #1',
                  products: [
                    {
                      key: 'stride',
                      name: 'Stride',
                    },
                  ],
                },
                {
                  id: 'site-2',
                  siteUrl: 'Site #2',
                  products: [
                    {
                      key: 'confluence',
                      name: 'Confluence',
                    },
                  ],
                },
                {
                  id: 'site-3',
                  siteUrl: 'Site #3',
                  products: [
                    {
                      key: 'jira-software',
                      name: 'Jira Software',
                    },
                    {
                      key: 'jira-service-desk',
                      name: 'Jira Service Desk',
                    },
                  ],
                },
              ]
              : [],
          });
        }, getSitesApiDelay);
      }

      public render() {
        // TODO react-apollo upgrade: figure out how to remove RouteComponentProps from component props

        return (
          // tslint:disable-next-line no-console
          <ApolloProvider client={createApolloClient(console.error.bind(console))}>
            <IntlProvider locale="en">
              <FlagProvider>
                <AddSiteStepImpl
                  getOrgSiteAdmins={this.getOrgSiteAdmins}
                  loading={this.state.loading}
                  sites={this.state.sites}
                  error={this.state.error}
                  analyticsClient={{
                    sendUIEvent: () => null,
                    sendScreenEvent: () => null} as any}
                  onSiteSelected={action('onSiteSelected')}
                  hideFlag={action('hideFlag')}
                  showFlag={action('showFlag')}
                  intl={createMockIntlProp()}
                  match={{} as any}
                  location={{} as any}
                  history={{} as any}
                  totalOrganizations={1}
                  totalOrganizationsLoadError={undefined}
                />
              </FlagProvider>
            </IntlProvider>
          </ApolloProvider>
        );
      }

      private getOrgSiteAdmins = async () => {
        await new Promise(resolve => setTimeout(resolve, getSiteAdminsApiDelay));

        if (isSiteAdminsFetchSuccessful) {
          return 'some site admins list' as any;
        } else {
          throw new Error('requested to fail');
        }
      }
    }

    return <Example />;
  },
  );
