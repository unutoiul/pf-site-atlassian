import { Product } from '../../schema/schema-types';

export type SiteAdminRole = 'product-admin' | 'org-admin';

export interface SiteAdminData {
  id: string;
  displayName: string;
  email: string;
  isOrgAdmin: boolean;
  isCurrentUser?: boolean;
  newRole?: SiteAdminRole;
}

export interface SiteData {
  id: string;
  siteUrl: string;
  products: Product[];
  admins: SiteAdminData[];
}
