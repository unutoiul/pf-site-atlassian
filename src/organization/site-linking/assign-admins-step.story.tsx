import { action } from '@storybook/addon-actions';
import { boolean } from '@storybook/addon-knobs';
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';

import { getRandomUser } from '../../utilities/testing';
import { AssignAdminsStep } from './assign-admins-step';
import { SiteData } from './org-site-linking-types';

function getRandomAdminData(index: number) {
  const { id, displayName, email } = getRandomUser();

  return { id: id + index.toString(10), displayName, email };
}

storiesOf('Organization|Org-site linking', module)
  .add('Step 2. Assign admins', () => {
    const isPreviousSelectionDone = boolean('Roles have previously been selected', false);

    const site: SiteData = {
      id: 'some-site-id',
      siteUrl: 'acme-usa.atlassian.net',
      products: [],
      admins: [
        { ...getRandomAdminData(1), isOrgAdmin: true },
        { ...getRandomAdminData(2), isCurrentUser: true, isOrgAdmin: true },
        { ...getRandomAdminData(3), isOrgAdmin: false, newRole: isPreviousSelectionDone ? 'org-admin' : undefined },
        { ...getRandomAdminData(4), isOrgAdmin: false, newRole: isPreviousSelectionDone ? 'product-admin' : undefined },
        { ...getRandomAdminData(5), isOrgAdmin: false, newRole: isPreviousSelectionDone ? 'org-admin' : undefined },
        { ...getRandomAdminData(6), isOrgAdmin: false, newRole: isPreviousSelectionDone ? 'product-admin' : undefined },
      ],
    };

    return (
      <IntlProvider locale="en">
        <AssignAdminsStep
          goBack={action('goBack')}
          onRolesAssigned={action('onRolesAssigned')}
          site={site}
        />
      </IntlProvider>
    );
  },
);
