import * as React from 'react';
import { graphql, QueryResult } from 'react-apollo';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';
import styled from 'styled-components';

import AkEmptyState from '@atlaskit/empty-state';
import AkSpinner from '@atlaskit/spinner';
import { gridSize as akGridSize } from '@atlaskit/theme';

import {
  addSiteToOrgModalScreenEvent,
  AnalyticsClientProps,
  nextButton,
  ScreenEventSender,
  withAnalyticsClient,
} from 'common/analytics';

import { createErrorIcon } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';
import { errorWindowImage } from 'common/images';

import { withOrganizationsCount, WithOrgsCountProps } from 'common/user';

import {
  OrgSite,
  OrgSiteAdminsQuery,
  OrgSiteAdminsQueryVariables,
  UserSitesQuery,
} from '../../schema/schema-types';

import { DrawerButtons } from './drawer-buttons';
import { StepInfo, StepTitle } from './layout-components';
import getOrgSiteAdminsQuery from './org-site-admins.query.graphql';
import { SiteAdminData, SiteData } from './org-site-linking-types';
import { SiteDescriptor } from './site-descriptor';
import getUserSitesQuery from './user-sites.query.graphql';

const messages = defineMessages({
  title: {
    id: 'organization.site-linking.drawer.add-site-step.title',
    defaultMessage: 'Add a Jira or Confluence site to your organization',
    description: '"Site" refers to an Atlassian site (e.g. example-company.atlassian.net)',
  },
  info: {
    id: 'organization.site-linking.drawer.add-site-step.info',
    defaultMessage: 'Add Jira and Confluence sites to your organization so that users will get default product access when they\'re synced. You must be an admin of the site you want to add. When a site gets added:',
    description: 'Description of the actions that the user would take. "Site" refers to Atlassian site (e.g. example-company.atlassian.net). After the last colon, there are two bulleted list items (translated separately with ids "organization.site-linking.drawer.add-site-step.info.explanation1" and "organization.site-linking.drawer.add-site-step.info.explanation2"), which say: a) Its site admins become organization admins and get access to all sites in the organization. b) All organization admins become admins of the added site.',
  },
  infoExplanation1: {
    id: 'organization.site-linking.drawer.add-site-step.info.explanation1',
    defaultMessage: 'Its site admins become organization admins and get access to all sites in the organization.',
    description: 'This is an explanation of what happens when a site gets linked to an organization. "Its" refers to a site, e.g. "Its site admins" means "the admins of a site". This is a bullet point list item that follows the string with id "organization.site-linking.drawer.add-site-step.info".',
  },
  infoExplanation2: {
    id: 'organization.site-linking.drawer.add-site-step.info.explanation2',
    defaultMessage: 'All organization admins become admins of the added site.',
    description: 'This is an explanation of what happens when a site gets linked ("added") to an organization.  This is a bullet point list item that follows the string with id "organization.site-linking.drawer.add-site-step.info".',
  },
  fetchingSiteAdminsFailedTitle: {
    id: 'organization.site-linking.drawer.add-site-step.fetching-site-admins-failed.title',
    defaultMessage: 'Could not load site admins',
    description: 'Summary of an error which happens during loading the list of site admins from the server',
  },
  fetchingSiteAdminsFailedDescription: {
    id: 'organization.site-linking.drawer.add-site-step.fetching-site-admins-failed.description',
    defaultMessage: 'Something went wrong when getting the site admins of {site}. Please try again later.',
    description: 'Description of an error which happens during loading the list of site admins from the server. "{site}" will be the display name of Atlassian site (e.g. acme.atlassian.net)',
  },
  fetchingSitesFailedTitle: {
    id: 'organization.site-linking.drawer.add-site-step.fetching-sites-failed.title',
    defaultMessage: 'Could not load sites',
    description: 'Summary of an error which happens during loading the list of sites that the current user is site admin of.',
  },
  fetchingSitesFailedDescription: {
    id: 'organization.site-linking.drawer.add-site-step.fetching-sites-failed.description',
    defaultMessage: 'Something went wrong when getting the list of your sites. Please try again later.',
    description: 'Description of an error which happens during loading the list of sites that the current user is site admin of.',
  },
  noSitesToAdd: {
    id: 'organization.site-linking.drawer.add-site-step.no-site-to-add',
    defaultMessage: 'There are no more sites available for adding',
    description: 'This text is shown in the table when the user started the "Add sites" operation, but there are no sites available for adding.',
  },
});

const SiteDescriptorContainer = styled.div`
  margin-bottom: ${akGridSize() * 2.5}px;
`;

export interface AddSiteStepProps {
  defaultSiteData?: SiteData;
  onSiteSelected(siteData: SiteData): void;
}

interface State {
  cloudId: string | undefined;
  loadingSiteAdmins: boolean;
}

interface GetOrgSiteAdminsProps {
  getOrgSiteAdmins(cloudId: string): Promise<SiteData['admins']>;
}

interface UserSitesProps {
  loading: boolean;
  error?: QueryResult['error'];
  sites: OrgSite[];
}

type AllProps = RouteComponentProps<any> & AddSiteStepProps & GetOrgSiteAdminsProps & UserSitesProps & FlagProps & InjectedIntlProps & AnalyticsClientProps & WithOrgsCountProps;

export class AddSiteStepImpl extends React.Component<AllProps, State> {
  public readonly state: Readonly<State> = {
    cloudId: undefined,
    loadingSiteAdmins: false,
  };

  public static getDerivedStateFromProps(nextProps: Readonly<AddSiteStepImpl['props']>, prevState: State): Partial<State> | null {
    if (prevState.cloudId === undefined) {
      if (nextProps.sites.length === 1) {
        // if only one site is available - preselect it
        return { cloudId: nextProps.sites[0].id };
      }

      if (nextProps.defaultSiteData) {
        const preLoadedSiteId = nextProps.defaultSiteData.id;

        if (nextProps.sites.some(site => site.id === preLoadedSiteId)) {
          return { cloudId: preLoadedSiteId };
        }
      }
    }

    return null;
  }

  public render() {
    const { cloudId, loadingSiteAdmins } = this.state;

    return (
      <React.Fragment>
        <StepTitle>
          <FormattedMessage {...messages.title} />
        </StepTitle>
        <StepInfo>
          <FormattedMessage {...messages.info} tagName="p" />
          <ul>
            <FormattedMessage {...messages.infoExplanation1} tagName="li" />
            <FormattedMessage {...messages.infoExplanation2} tagName="li" />
          </ul>
        </StepInfo>
        {this.renderSites()}
        <DrawerButtons
          next={{
            onClick: this.onNextClick,
            isLoading: loadingSiteAdmins,
            isDisabled: !cloudId || loadingSiteAdmins,
          }}
        />
      </React.Fragment>
    );
  }

  private renderSites() {
    const {
      loading,
      error,
      sites,
    } = this.props;

    if (loading) {
      return (
        <AkSpinner size="medium" />
      );
    }

    if (error) {
      return (
        <AkEmptyState
          imageUrl={errorWindowImage}
          header={<FormattedMessage {...messages.fetchingSitesFailedTitle} />}
          description={<FormattedMessage {...messages.fetchingSitesFailedDescription} />}
          size="wide"
        />
      );
    }

    if (sites.length === 0) {
      return (
        <FormattedMessage {...messages.noSitesToAdd} tagName="em" />
      );
    }
    const eventAttributes = {
      siteCount: sites && sites.length,
    };

    return (
      <ScreenEventSender
        event={addSiteToOrgModalScreenEvent(eventAttributes)}
        isDataLoading={this.props.loading}
      >
        {sites.map(site => (
          <SiteDescriptorContainer key={site.id}>
            <SiteDescriptor
              site={site.siteUrl}
              products={site.products}
              withRadio={{
                name: 'cloud-id',
                value: site.id,
                isSelected: site.id === this.state.cloudId,
                isDisabled: this.state.loadingSiteAdmins,
                onChange: this.onSiteSelect,
              }}
            />
          </SiteDescriptorContainer>
        ))}
      </ScreenEventSender>
    );
  }

  private onSiteSelect = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ cloudId: event.target.value });
  }

  private onNextClick = async () => {
    const { cloudId } = this.state;
    if (!cloudId) {
      return;
    }

    const {
      analyticsClient,
      getOrgSiteAdmins,
      onSiteSelected,
      showFlag,
      defaultSiteData,
      intl: { formatMessage },
      sites,
      totalOrganizations,
    } = this.props;

    analyticsClient.sendUIEvent({
      cloudId,
      data: nextButton({ currentSiteCount: sites.length, currentOrgCount: totalOrganizations }),
    });

    if (defaultSiteData && defaultSiteData.id === cloudId) {
      // the user most likely went back from "Assign admins" screen, which means that we have already loaded the data for the given cloudId
      onSiteSelected(defaultSiteData);

      return;
    }

    this.setState({ loadingSiteAdmins: true });

    try {
      const siteAdmins = await getOrgSiteAdmins(cloudId);
      const site = sites.find(s => s.id === cloudId);
      onSiteSelected({
        id: cloudId,
        siteUrl: (site && site.siteUrl) || cloudId,
        products: (site && site.products) || [],
        admins: siteAdmins,
      });
    } catch (_) {
      const site = sites.find(s => s.id === cloudId);
      const siteMessageData = site ? site.siteUrl : cloudId;
      this.setState({ loadingSiteAdmins: false });

      showFlag({
        id: `fetchingSiteAdminsFailed.${new Date().getTime()}`,
        title: formatMessage(messages.fetchingSiteAdminsFailedTitle),
        description: formatMessage(messages.fetchingSiteAdminsFailedDescription, { site: siteMessageData }),
        icon: createErrorIcon(),
      });
    }
  }
}

const withOrgSiteAdmins = graphql<RouteComponentProps<any> & AddSiteStepProps & UserSitesProps, OrgSiteAdminsQuery, OrgSiteAdminsQueryVariables, GetOrgSiteAdminsProps>(
  getOrgSiteAdminsQuery,
  {
    props: (allProps): GetOrgSiteAdminsProps => ({
      getOrgSiteAdmins: async (cloudId: string) => {
        const variables: OrgSiteAdminsQueryVariables = {
          orgId: allProps.ownProps.match.params.orgId,
          cloudId,
          skip: false,
        };
        const result = await allProps.data!.refetch(variables);

        const data: OrgSiteAdminsQuery = result.data;
        const currentUserId = data.currentUser && data.currentUser.id;

        return data.organization.siteAdmins.map<SiteAdminData>(({ id, displayName, email, isOrgAdmin }) => {
          const admin: SiteAdminData = { id, displayName, email, isOrgAdmin };
          if (id === currentUserId) {
            admin.isCurrentUser = true;
          }

          return admin;
        });
      },
    }),
    options: () => ({
      variables: { orgId: '', cloudId: '', skip: true } as OrgSiteAdminsQueryVariables,
    }),
    skip: (props) => !props.match.params.orgId,
  },
);

const withUserSites = graphql<AddSiteStepProps, UserSitesQuery, {}, UserSitesProps>(
  getUserSitesQuery,
  {
    props: ({ data }): UserSitesProps => ({
      error: data && data.error,
      loading: !!(data && data.loading),
      sites: (data && data.currentUser && data.currentUser.unlinkedSites && data.currentUser.unlinkedSites) || [],
    }),
  },
);

export const AddSiteStep =
withUserSites(
  withRouter(
    withOrgSiteAdmins(
      withFlag(
        injectIntl(
          withAnalyticsClient(
            withOrganizationsCount(
              AddSiteStepImpl,
            ),
          ),
        ),
      ),
    ),
  ),
);
