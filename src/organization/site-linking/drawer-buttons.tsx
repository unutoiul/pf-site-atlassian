import * as React from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';

import AkButton, { AkButtonProps } from '@atlaskit/button';

import { ButtonGroup } from 'common/button-group';

import { DrawerButtonContainer } from './layout-components';

const messages = defineMessages({
  nextButton: {
    id: 'organization.site-linking.drawer.buttons.next-button',
    defaultMessage: 'Next',
    description: 'Text for a button that is used to proceed to the next step in configuration flow.',
  },
  completeButton: {
    id: 'organization.site-linking.drawer.buttons.complete-button',
    defaultMessage: 'Complete',
    description: 'Text for a button that is used to complete in configuration flow.',
  },
  backButton: {
    id: 'organization.site-linking.drawer.buttons.back-button',
    defaultMessage: 'Back',
    description: 'Text for a button that is used to proceed to the previous step in configuration flow.',
  },
});

export interface DrawerButtonsProps {
  next: Pick<AkButtonProps, 'onClick' | 'isDisabled' | 'isLoading'> & { isCompleteAction?: boolean };
  back?: Pick<AkButtonProps, 'onClick' | 'isDisabled'>;
}

export class DrawerButtons extends React.Component<DrawerButtonsProps> {
  public render() {
    const { back, next } = this.props;

    return (
      <DrawerButtonContainer>
        <ButtonGroup alignment="right">
          {back && (
            <AkButton
              onClick={back.onClick}
              isDisabled={back.isDisabled}
            >
              <FormattedMessage {...messages.backButton} />
            </AkButton>
          )}
          <AkButton
            appearance="primary"
            onClick={next.onClick}
            isDisabled={next.isDisabled}
            isLoading={next.isLoading}
          >
            <FormattedMessage {...(next.isCompleteAction ? messages.completeButton : messages.nextButton)} />
          </AkButton>
        </ButtonGroup>
      </DrawerButtonContainer>
    );
  }
}
