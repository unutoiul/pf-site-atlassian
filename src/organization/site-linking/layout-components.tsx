import styled from 'styled-components';

import { colors as akColors, fontSize as akFontSize, gridSize as akGridSize } from '@atlaskit/theme';

export const StepTitle = styled.h2`
  font-size: ${akFontSize() * 1.42}px;
  margin-bottom: ${akGridSize() * 2}px;
`;

export const StepSubTitle = styled.div`
  color: ${akColors.heading};
  font-size: ${akFontSize() * 1.14}px;
  font-weight: 600;
  margin: ${akGridSize() * 2.5}px 0;
`;

export const StepInfo = styled.div`
  margin-bottom: ${akGridSize() * 4}px;
`;

export const DrawerButtonContainer = styled.div`
  margin-top: ${akGridSize() * 3}px;
`;

export const DrawerContainer = styled.div`
  margin: 0 auto;
  min-width: ${akGridSize() * 50}px;
  width: 80%;
  max-width: ${akGridSize() * 100}px;
`;

export const DrawerContent = styled.div`
  padding-top: ${akGridSize() * 6}px;
`;
