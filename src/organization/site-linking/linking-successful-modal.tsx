import * as React from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';

import AkButton from '@atlaskit/button';

import {
  AnalyticsClientProps,
  linkSiteToOrgTrackData,
  ScreenEventSender,
  siteAddedSuccessCloseButton,
  siteAddedSuccessModalScreenEvent,
  withAnalyticsClient,
} from 'common/analytics';
import { ModalDialog } from 'common/modal';
import { withOrganizationsCount, WithOrgsCountProps, withSitesCount, WithSitesCountProps } from 'common/user';

import { SiteData } from './org-site-linking-types';

export const messages = defineMessages({
  title: {
    id: 'organization.site-linking.drawer.successful.title',
    defaultMessage: 'Site successfully added to your organization',
    description: 'The title of the modal dialog which appears after the user has successfully linked site to an organization',
  },
  description: {
    id: 'organization.site-linking.drawer.successful.description',
    defaultMessage: 'Syncing groups to your site may take several minutes. Once the groups have synced, you will be able to set up product access to {site}.',
    description: 'The content of the modal dialog that is shown to the user once they have successfully linked site to an organization. "groups" means "user groups" in this context. "site" will be replaced by the linked Atlassian site (e.g. acme.atlassian.net)',
  },
  buttonClose: {
    id: 'organization.site-linking.drawer.successful.button-close',
    defaultMessage: 'Close',
    description: 'The button that closes the modal dialog.',
  },
});

export interface LinkingSuccessModalProps {
  isOpen: boolean;
  site: SiteData;
  onClose(): void;
}

type AllProps = LinkingSuccessModalProps & AnalyticsClientProps & WithSitesCountProps & WithOrgsCountProps;

export class LinkingSuccessfulModalImpl extends React.Component<AllProps> {
  public render() {
    const {
      site,
    } = this.props;

    if (!this.props.isOpen) {
      return null;
    }

    const eventAttributes = {
      siteId: site.id,
    };

    return (
      <ScreenEventSender event={siteAddedSuccessModalScreenEvent(eventAttributes)}>
        <ModalDialog
          width="medium"
          isOpen={true}
          onClose={this.onCloseDialog}
          header={(
            <FormattedMessage {...messages.title} />
          )}
          footer={(
            <AkButton
              appearance="primary"
              onClick={this.onCloseDialog}
            >
              <FormattedMessage {...messages.buttonClose} />
            </AkButton>
          )}
        >
          <FormattedMessage
            {...messages.description}
            values={{ site: site.siteUrl ? site.siteUrl : '' }}
            tagName="p"
          />
        </ModalDialog>
      </ScreenEventSender>
    );
  }
  private onCloseDialog = () => {
    const{ analyticsClient, totalSites, totalOrganizations, onClose, site } = this.props;
    analyticsClient.sendUIEvent({
      data: siteAddedSuccessCloseButton({ currentSiteCount: totalSites, siteId: site.id, currentOrgCount: totalOrganizations }),
    });
    analyticsClient.sendTrackEvent({
      data: linkSiteToOrgTrackData({ currentSiteCount: totalSites, siteId: site.id, currentOrgCount: totalOrganizations }),
    });
    onClose();
  }
}

export const LinkingSuccessfulModal = withAnalyticsClient(
  withSitesCount(
    withOrganizationsCount(
      LinkingSuccessfulModalImpl,
    ),
  ),
);
