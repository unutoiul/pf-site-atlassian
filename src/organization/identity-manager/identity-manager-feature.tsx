import * as React from 'react';
import styled from 'styled-components';

import { akGridSizeUnitless } from '@atlaskit/util-shared-styles';

interface Props {
  title: string;
  description: string;
  icon: any;
}

const Container = styled.div`
  padding: ${akGridSizeUnitless * 2}px;
  width: ${akGridSizeUnitless * 30}px;
`;

export class IdentityManagerFeature extends React.PureComponent<Props> {
  public render() {

    return (
      <Container>
        <p>{this.props.icon}</p>
        <h5>{this.props.title}</h5>
        <p>{this.props.description}</p>
      </Container>
    );
  }

}
