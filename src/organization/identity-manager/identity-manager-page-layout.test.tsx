
// tslint:disable:jsx-use-translation-function
import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { sandbox as sinonSandbox } from 'sinon';

import AkSpinner from '@atlaskit/spinner';

import { IdentityManagerEvaluationCard } from 'common/card/identity-manager-evaluation-card';
import { PageLayout } from 'common/page-layout';

import { ScreenEventSender } from 'common/analytics';

import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import { VerifyDomainSectionMessage } from '../domain-claim/verify-domain-section-message';
import { IdentityManagerPageLayoutImpl } from './identity-manager-page-layout';

describe('Identity Manager Page Layout', () => {
  const sandbox = sinonSandbox.create();
  let eventHandlerSpy;

  beforeEach(() => {
    eventHandlerSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  function identityManagerPageLayout(product) {
    return shallow(
      <IdentityManagerPageLayoutImpl
        orgId="DUMMY-TEST-ORG-ID"
        analyticsScreenForEvalButton="dummyIMEval"
        cardDescription="The ladies always say DJ Khaled you smell good, I use no cologne. Cocoa butter is the key"
        additionalChildren={<p>I am the always shown child</p>}
        data={product}
        intl={createMockIntlProp()}
        {...{} as any}
        screenEvent={eventHandlerSpy}
      >
        <p>Of course they don’t want us to eat our breakfast, so we are going to enjoy our breakfast</p>
      </IdentityManagerPageLayoutImpl>,
      createMockIntlContext(),
    );
  }

  it('Renders a spinner if product data is loading', () => {
    const productData = { loading: true };
    const wrapper = identityManagerPageLayout(productData);
    const pageLayout = wrapper.find(PageLayout);
    expect(pageLayout).to.have.length(1);
    expect(pageLayout.find(AkSpinner)).to.have.length(1);
  });

  it('Renders an Identity Manager Evaluation Card if IM is not a product', () => {
    const productData = { loading: false, products: { identityManager: false } };
    const wrapper = identityManagerPageLayout(productData);
    const pageLayout = wrapper.find(PageLayout);
    expect(pageLayout).to.have.length(1);
    expect(pageLayout.contains(<p>Of course they don’t want us to eat our breakfast, so we are going to enjoy our breakfast</p>)).to.equal(false);

    const identityManagerEvaluationCard = pageLayout.find(IdentityManagerEvaluationCard);
    expect(identityManagerEvaluationCard).to.have.length(1);
    expect(identityManagerEvaluationCard.props().orgId).to.equal('DUMMY-TEST-ORG-ID');
    expect(identityManagerEvaluationCard.props().analyticsScreenForEvalButton).to.deep.equal('dummyIMEval');
    expect(identityManagerEvaluationCard.props().cardDescription).to.deep.equal('The ladies always say DJ Khaled you smell good, I use no cologne. Cocoa butter is the key');
    expect(pageLayout.children().contains(<p>I am the always shown child</p>)).to.equal(true);
  });

  it('Renders domain claim panel if there are no domain claims and IM is a product', () => {
    const productData = { loading: false, products: { identityManager: true }, organization: { domainClaim: { domains: [] } } };
    const wrapper = identityManagerPageLayout(productData);
    const pageLayout = wrapper.find(PageLayout);
    expect(pageLayout).to.have.length(1);
    expect(pageLayout.find(VerifyDomainSectionMessage)).to.have.length(1);
    expect(pageLayout.contains(<p>Of course they don’t want us to eat our breakfast, so we are going to enjoy our breakfast</p>)).to.equal(false);
    expect(pageLayout.children().contains(<p>I am the always shown child</p>)).to.equal(true);
  });

  it('Renders domain claim panel if there are no verified domain claims and IM is a product', () => {
    const productData = { loading: false, products: { identityManager: true } };
    const wrapper = identityManagerPageLayout(productData);
    const pageLayout = wrapper.find(PageLayout);
    expect(pageLayout).to.have.length(1);
    expect(pageLayout.find(VerifyDomainSectionMessage)).to.have.length(1);
    expect(pageLayout.contains(<p>Of course they don’t want us to eat our breakfast, so we are going to enjoy our breakfast</p>)).to.equal(false);
    expect(pageLayout.children().contains(<p>I am the always shown child</p>)).to.equal(true);
  });

  it('Renders children if IM is a product and there are verified domains', () => {
    const productData = { loading: false, products: { identityManager: true }, organization: { domainClaim: { domains: [{ verified: true }] } } };
    const wrapper = identityManagerPageLayout(productData);
    const pageLayout = wrapper.find(PageLayout);
    expect(pageLayout).to.have.length(1);
    expect(pageLayout.find(IdentityManagerEvaluationCard)).to.have.length(0);
    expect(pageLayout.contains(<p>Of course they don’t want us to eat our breakfast, so we are going to enjoy our breakfast</p>)).to.equal(true);
    expect(pageLayout.children().contains(<p>I am the always shown child</p>)).to.equal(true);
  });

  it('Passes the screenEvent function to the ScreenEventSender child', () => {
    // this productData sets hasIdentityManager and hasDomainsVerified to true and true
    const productData = { loading: false, products: { identityManager: true }, organization: { domainClaim: { domains: [{ verified: true }] } } };
    const wrapper = identityManagerPageLayout(productData);
    const pageLayout = wrapper.find(PageLayout);
    const screenEventSender = pageLayout.find(ScreenEventSender);

    expect(screenEventSender).to.have.length(1);

    expect(eventHandlerSpy.called).to.equal(true);
    expect(eventHandlerSpy.calledWith(true, true)).to.equal(true);
  });
});
