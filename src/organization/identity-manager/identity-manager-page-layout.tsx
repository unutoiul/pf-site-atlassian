
import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';

import AkSpinner from '@atlaskit/spinner';

import { ScreenEvent, ScreenEventSender } from 'common/analytics';
import { IdentityManagerEvaluationCard, IdentityManagerEvaluationCardProps } from 'common/card/identity-manager-evaluation-card';
import { PageLayout, PageLayoutProps } from 'common/page-layout';

import { IdentityManagerPageQuery } from '../../schema/schema-types';
import { VerifyDomainSectionMessage } from '../domain-claim/verify-domain-section-message';
import { hasAnyDomainVerified } from '../organizations/organizations.util';
import identityManagerPageQuery from '../organizations/queries/identity-manager-page.query.graphql';

export interface AnalyticsProps {
  screenEvent(hasIdentityManager: boolean, hasDomainsVerified: boolean): ScreenEvent;
}

interface DomainProps {
  domainPrompt: string;
}

// This content is shown beneath the normal child props, regardless of the state of IM
interface AlwaysShownChildProps {
  additionalChildren?: JSX.Element;
}

type OwnProps = DomainProps & AnalyticsProps & IdentityManagerEvaluationCardProps & PageLayoutProps & AlwaysShownChildProps;

export type IdentityManagerPageLayoutProps = ChildProps<OwnProps, IdentityManagerPageQuery>;

export class IdentityManagerPageLayoutImpl extends React.Component<IdentityManagerPageLayoutProps> {

  public render() {
    return (
        <PageLayout
          title={this.props.title}
          isDisabled={this.props.isDisabled}
          subtitle={this.props.subtitle}
          children={
            <div>
              {this.determineChildren()}
              {this.props.additionalChildren}
            </div>
          }
          description={this.props.description}
          side={this.props.side}
          icon={this.props.icon}
          isFullWidth={this.props.isFullWidth}
          breadcrumbs={this.props.breadcrumbs}
        />
    );
  }

  private determineChildren = (): React.ReactNode => {
    if (!this.props.data) {
      return null;
    }

    const { loading, products, organization } = this.props.data;

    if (loading) {
      return <AkSpinner />;
    }

    let child: React.ReactNode;
    const hasIdentityManager = products && products.identityManager;
    const hasDomainsVerified = organization && hasAnyDomainVerified(organization.domainClaim.domains);

    if (!hasIdentityManager) {
      child = this.evaluateIdentityManagerCard();
    } else if (!hasDomainsVerified) {
      child = <VerifyDomainSectionMessage domainPrompt={this.props.domainPrompt} />;
    } else {
      child = this.props.children;
    }

    return (
      <ScreenEventSender event={this.props.screenEvent(hasIdentityManager || false, hasDomainsVerified || false)}>
        {child}
      </ScreenEventSender>
    );
  };

  private evaluateIdentityManagerCard = (): JSX.Element => {
    return (
      <IdentityManagerEvaluationCard
        orgId={this.props.orgId}
        analyticsScreenForEvalButton={this.props.analyticsScreenForEvalButton}
        cardDescription={this.props.cardDescription}
      />
    );
  };

}

const withIdentityManagerPageQuery = graphql<OwnProps, IdentityManagerPageQuery>(identityManagerPageQuery, {
  options: (props) => ({ variables: { id: props.orgId } }),
});

export const IdentityManagerPageLayout =
  withIdentityManagerPageQuery(
    IdentityManagerPageLayoutImpl,
  );
