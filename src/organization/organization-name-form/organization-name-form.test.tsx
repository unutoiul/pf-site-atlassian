// tslint:disable jsx-use-translation-function react-this-binding-issue prefer-function-over-method
import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import { createMockAnalyticsClient, createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import { EditFilledIconButton, OrgNameFormImpl, OrgNameFormProps } from './organization-name-form';

describe('OrgNameForm', () => {
  class Helper {
    public static defaultOrgName = 'Some test org name';

    private _wrapper: ShallowWrapper<OrgNameFormProps>;
    private renamePromise: Promise<void>;
    private renameOrgSpy: sinon.SinonSpy;
    private showFlagSpy: sinon.SinonSpy = sinon.spy();
    private renamePromiseResolveCallback!: () => void;
    private renamePromiseRejectCallback!: () => void;

    constructor() {
      this.renamePromise = new Promise((resolve, reject) => {
        this.renamePromiseResolveCallback = resolve;
        this.renamePromiseRejectCallback = reject;
      });
      this.renameOrgSpy = sinon.stub().returns(this.renamePromise);

      this._wrapper = shallow(
        <OrgNameFormImpl
          showFlag={this.showFlagSpy}
          hideFlag={() => null}
          renameOrg={this.renameOrgSpy}
          intl={createMockIntlProp()}
          name={Helper.defaultOrgName}
          orgId="test-org-id"
          analyticsSource="orgOverviewScreen"
          analyticsClient={createMockAnalyticsClient()}
        />,
        createMockIntlContext(),
      );
    }

    public clickOutOfForm(orgNameValue: string): void {
      this.editForm.simulate('blur', this.createFormEvent(orgNameValue));
    }

    public expectErrorFlagToBeShown(): void {
      expect(this.showFlagSpy.callCount).to.equal(1);
      expect(this.showFlagSpy.getCalls()[0].args[0].title).to.equal('Organization could not be renamed.');
    }

    public expectSuccessFlagToBeShown(): void {
      expect(this.showFlagSpy.callCount).to.equal(1);
      expect(this.showFlagSpy.getCalls()[0].args[0].title).to.equal('Organization has been successfully renamed');
    }

    public async waitForRenameToFail() {
      this.renamePromiseRejectCallback();
      try {
        await this.renamePromise;
      } catch {
        // we expect it to fail, so we don't do anything
      }
    }

    public async waitForRenameToSucceed() {
      this.renamePromiseResolveCallback();
      await this.renamePromise;
    }

    public expectRenameToNotBeCalled(): void {
      expect(this.renameOrgSpy.callCount).to.equal(0);
    }

    public expectRenameToBeCalledWith(orgName: string): void {
      expect(this.renameOrgSpy.callCount).to.equal(1);
      expect(this.renameOrgSpy.getCalls()[0].args[0]).to.equal(orgName);
    }

    public submitRenameForm(orgName: string): void {
      this.editForm.simulate('submit', this.createFormEvent(orgName));
    }

    public clickOnEdit(): void {
      this.editButton.simulate('click');
    }

    public expectModeToBe(mode: 'read' | 'edit'): void {
      if (mode === 'read') {
        expect(this.editButton).to.have.lengthOf(1);
      } else {
        expect(this.editButton).to.have.lengthOf(0);
      }
    }

    private createFormEvent(orgNameValue: string) {
      return {
        preventDefault: () => null,
        currentTarget: { elements: { name: { value: orgNameValue } } },
      };
    }

    private get wrapper() {
      this._wrapper.update();

      return this._wrapper;
    }

    private get editForm() {
      return this.wrapper.find('form');
    }

    private get editButton() {
      return this.wrapper.find(EditFilledIconButton);
    }
  }

  it('should initially be in "read" mode', () => {
    const helper = new Helper();

    helper.expectModeToBe('read');
  });

  it('should go into "edit" mode when user clicks on "rename" button', () => {
    const helper = new Helper();

    helper.clickOnEdit();

    helper.expectModeToBe('edit');
  });

  it('should call rename org with correct args when user submits the form', () => {
    const helper = new Helper();

    helper.clickOnEdit();
    helper.submitRenameForm('new org name');

    helper.expectRenameToBeCalledWith('new org name');
  });

  it('should show success flag when rename succeeds', async () => {
    const helper = new Helper();

    helper.clickOnEdit();
    helper.submitRenameForm('new org name');
    await helper.waitForRenameToSucceed();

    helper.expectSuccessFlagToBeShown();
  });

  it('should show error flag when rename fails', async () => {
    const helper = new Helper();

    helper.clickOnEdit();
    helper.submitRenameForm('new org name');
    await helper.waitForRenameToFail();

    helper.expectErrorFlagToBeShown();
  });

  it('should switch back to "read" mode if user clicks out of form and the name has not changed', () => {
    const helper = new Helper();

    helper.clickOnEdit();
    helper.clickOutOfForm(Helper.defaultOrgName);

    helper.expectModeToBe('read');
    helper.expectRenameToNotBeCalled();
  });
});
