import * as React from 'react';
import { graphql } from 'react-apollo';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import AkTextField from '@atlaskit/field-text';
import AkEditFilledIcon from '@atlaskit/icon/glyph/edit-filled';
import { fontSize as akFontSize, gridSize as akGridSize } from '@atlaskit/theme';

import { ActionsBarCheckCircleIcon } from 'common/actions-bar';
import {
  AnalyticsClientProps,
  Button,
  OrgNameFormSource,
  renameOrgButtonEventData, renameOrgConfirmButtonEventData,
  withAnalyticsClient,
  withClickAnalytics,
} from 'common/analytics';
import { createErrorIcon } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';

import { RenameOrgMutation, RenameOrgMutationVariables } from '../../schema/schema-types';
import { createOrgAnalyticsData } from '../organizations/organization-analytics';
import renameOrgMutation from './organization-name-form.mutation.graphql';

export interface OrgNameFormProps {
  orgId: string;
  name: string;
  analyticsSource: OrgNameFormSource;
}

const CreateOrgContent = styled.div`
  margin-top: -${akGridSize() * 2}px;
  align-items: center;
  display: flex;
`;

const CreateNameField = styled.div`
  flex-grow: 7;
`;

const CreateNameButton = styled.div`
  /* We are explicitly setting font-size because otherwise the button might inherit a gigantic font-size from <h1> tag through PageLayout.title in some usages */
  font-size: ${akFontSize}px;
  padding-left: ${akGridSize() * 2}px;
  padding-top: ${akGridSize() * 5}px;
`;

const messages = defineMessages({
  label: {
    id: 'organization.name.form.label',
    defaultMessage: 'Organization name',
    description: 'Serves as a label in a form for the input that will hold organization name',
  },
  submitButton: {
    id: 'organization.name.form.submit-button',
    defaultMessage: 'Rename',
    description: 'Button text. Clicking on this button will result in firing a request to rename organization',
  },
  renameSuccess: {
    id: 'organization.name.form.rename.success',
    defaultMessage: 'Organization has been successfully renamed',
    description: 'The text that is shown to the user if renaming an organization succeeds',
  },
  renameFailureTitle: {
    id: 'organization.name.form.rename.failure.title',
    defaultMessage: 'Organization could not be renamed.',
    description: 'The title of an error screen that is shown to the user if renaming an organization fails',
  },
  renameFailureDescription: {
    id: 'organization.name.form.rename.failure.description',
    defaultMessage: 'Please try again later.',
    description: 'The body of an error screen that is shown to the user if renaming an organization fails',
  },
  editOrgNameLabel: {
    id: 'organization.name.form.edit.org.name',
    defaultMessage: 'Edit organization details',
    description: 'The alternative text that is read to screen readers. Performing this action clicking will result in the ability to edit organization name',
  },
});

interface State {
  isRenameRequestInProgress: boolean;
  isEditMode: boolean;
}

type Props = OrgNameFormProps & InjectedIntlProps & FlagProps & MutationDerivedProps & AnalyticsClientProps;

const EditButtonWrapper = styled.span`
  cursor: pointer;
  margin-left: ${akGridSize() * 1.5}px;
  height: ${akGridSize() * 2}px;

  &&& > span {
    line-height: 0;
  }
`;

const AlignmentDiv = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const EditIconWithCursorPointer = props => <EditButtonWrapper><AkEditFilledIcon {...props} /></EditButtonWrapper>;

export const EditFilledIconButton = withClickAnalytics<AkEditFilledIcon['props']>()(EditIconWithCursorPointer);

export class OrgNameFormImpl extends React.Component<Props, State> {
  public readonly state: Readonly<State> = {
    isRenameRequestInProgress: false,
    isEditMode: false,
  };

  public render() {
    return this.state.isEditMode
      ? this.renderEditMode()
      : this.renderViewMode();
  }

  private renderViewMode() {
    const {
      intl: { formatMessage },
      orgId,
      name,
    } = this.props;

    return (
      <AlignmentDiv>
        <span>{name}</span>
          <EditFilledIconButton
            label={formatMessage(messages.editOrgNameLabel)}
            onClick={this.enableEditMode}
            size="small"
            analyticsData={createOrgAnalyticsData({
              orgId,
              action: 'click',
              actionSubject: 'renameOrgButton',
              actionSubjectId: 'openEdit',
            })}
          />
      </AlignmentDiv>
    );
  }

  private renderEditMode() {
    const {
      intl: { formatMessage },
      orgId,
      name,
    } = this.props;

    const { isRenameRequestInProgress } = this.state;

    return (
      <form onSubmit={this.onFormSubmit} onBlur={this.onFormBlur}>
        <CreateOrgContent>
          <CreateNameField>
            <AkTextField
              value={name}
              name="name"
              disabled={isRenameRequestInProgress}
              label={formatMessage(messages.label)}
              required={true}
              shouldFitContainer={true}
              onChange={undefined}
              maxLength={70}
              autoFocus={true}
            />
          </CreateNameField>
          <CreateNameButton>
            <Button
              type="submit"
              shouldFitContainer={true}
              isDisabled={isRenameRequestInProgress}
              isLoading={isRenameRequestInProgress}
              appearance="primary"
              analyticsData={createOrgAnalyticsData({
                orgId,
                action: 'click',
                actionSubject: 'renameOrgButton',
                actionSubjectId: 'update',
              })}
            >
              {formatMessage(messages.submitButton)}
            </Button>
          </CreateNameButton>
        </CreateOrgContent>
      </form>
    );
  }

  private onFormBlur = (e: React.FormEvent<HTMLFormElement>) => {
    const { name } = (e.currentTarget as any).elements;

    if (name.value === this.props.name) {
      this.disableEditMode();
    }
  }

  private enableEditMode = () => {
    this.setState({ isEditMode: true });

    const {
      orgId,
      analyticsSource,
      analyticsClient: { sendUIEvent },
    } = this.props;

    sendUIEvent({
      orgId,
      data: renameOrgButtonEventData(analyticsSource),
    });
  }

  private disableEditMode = () => {
    this.setState({ isEditMode: false });
  }

  private onFormSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    const { name } = (e.currentTarget as any).elements;
    this.rename(name.value);
  }

  private rename = (newName: string) => {
    const {
      renameOrg,
      showFlag,
      intl: { formatMessage },
      orgId,
      analyticsSource,
      analyticsClient: { sendUIEvent },
    } = this.props;

    sendUIEvent({
      orgId,
      data: renameOrgConfirmButtonEventData(analyticsSource),
    });

    this.setState({ isRenameRequestInProgress: true });

    renameOrg(newName)
      .then(() => {
        this.setState({ isEditMode: false, isRenameRequestInProgress: false });

        showFlag({
          autoDismiss: true,
          icon: <ActionsBarCheckCircleIcon label="" />,
          id: `organization.landing.success.flag.${Date.now()}`,
          title: formatMessage(messages.renameSuccess),
        });
      }).catch(() => {
        this.setState({ isRenameRequestInProgress: false });

        showFlag({
          icon: createErrorIcon(),
          id: `organization.landing.failure.flag.${Date.now()}`,
          title: formatMessage(messages.renameFailureTitle),
          description: formatMessage(messages.renameFailureDescription),
        });
      });
  }
}

interface MutationDerivedProps {
  renameOrg(newName: string): Promise<void>;
}

const mutation = graphql<OrgNameFormProps, RenameOrgMutation, RenameOrgMutationVariables, MutationDerivedProps>(
  renameOrgMutation,
  {
    props: ({ mutate, ownProps: { orgId } }): MutationDerivedProps => ({
      renameOrg: async (newName: string) => {
        if (!(mutate instanceof Function)) {
          throw new Error('"renameOrg" mutation function missing');
        }

        await mutate({
          variables: {
            id: orgId,
            name: newName,
          } as RenameOrgMutationVariables,
          optimisticResponse: {
            renameOrganization: {
              id: orgId,
              name: newName,
              __typename: 'Organization',
            },
          },
        });
      },
    }),
  },
);

export const OrgNameForm = mutation(
  withFlag(
    injectIntl(
      withAnalyticsClient(
        OrgNameFormImpl,
      ),
    ),
  ),
);
