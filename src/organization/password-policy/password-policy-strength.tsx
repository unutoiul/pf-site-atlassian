export enum PasswordPolicyStrength {
  Weak = 'weak',
  Fair = 'fair',
  Good = 'good',
  Strong = 'strong',
  VeryStrong = 'very strong',
}
