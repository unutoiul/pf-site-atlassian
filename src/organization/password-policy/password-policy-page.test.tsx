import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import AkSpinner from '@atlaskit/spinner';

import { PageLayoutProps } from 'common/page-layout';

import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import {
  IdentityManagerPageLayout,
  IdentityManagerPageLayoutProps,
} from '../identity-manager/identity-manager-page-layout';
import { PasswordPolicyForm } from './password-policy-form';
import { ListItem, PasswordPolicyPageImpl } from './password-policy-page';
import { PasswordResetSection } from './password-policy-reset-section';

describe('PasswordPolicyPage', () => {
  const orgJsonWithDomains = {
    organization: {
      security : {
        passwordPolicy: {
          minimumStrength: 'very strong',
          expiryDays: '5',
        },
      },
      domainClaim: {
        domains: [{
          domain: 'cool.com',
          verified: true,
          status: 'VERIFIED',
        }],
      },
    },
  };

  const orgJsonNoDomains = {
    organization: {
      security : {
        passwordPolicy: {
          minimumStrength: 'very strong',
          expiryDays: '5',
        },
      },
      domainClaim: {
        domains: [],
      },
    },
  };

  function passwordPolicyPageWithData(data, products = {}) {
    return shallow(
      <PasswordPolicyPageImpl
        data={data}
        match={{ params: { orgId: 'DUMMY_ORG_ID' } }}
        products={products}
        intl={createMockIntlProp()}
        atlassianAccessEnabled={true}
        {...{} as any}
      />,
      createMockIntlContext(),
    );
  }
  it('page layout should always be rendered with title and description and sidebar', () => {
    const title = 'Password management';
    const wrapper = passwordPolicyPageWithData({});
    const pageLayout = wrapper.find(IdentityManagerPageLayout);
    const pageLayoutProps = pageLayout.props() as PageLayoutProps;
    expect(pageLayoutProps.title).to.deep.equal(title);
    expect(pageLayoutProps.side).to.exist();
  });

  describe('without organization data', () => {
    const wrapper = passwordPolicyPageWithData({ loading: false });

    it('shows an error message', () => {
      expect(wrapper.find('span')).to.have.length(1);
      expect(wrapper.find('span').text()).to.equal('Password policy data could not be retrieved.');
    });

    it('does not show spinner', () => {
      const spinner = wrapper.find(AkSpinner) as typeof AkSpinner;
      expect(spinner).to.have.length(0);
    });

    it('no password policy form or reset passwords section should be rendered', () => {
      const passwordPolicyForm = wrapper.find(PasswordPolicyForm);
      expect(passwordPolicyForm).to.have.length(0);
      expect(wrapper.find(PasswordResetSection)).to.have.length(0);
    });
  });

  describe('with organization data, with domains, and with Identity Manager', () => {

    const wrapper = passwordPolicyPageWithData({ loading: false, ...orgJsonWithDomains }, { products: { loading: false, identityManager: true } });

    it('doesn\'t show an error message', () => {
      expect(wrapper.find('span')).to.have.length(0);
    });

    it('doesn\'t show a spinner', () => {
      const spinner = wrapper.find(AkSpinner) as typeof AkSpinner;
      expect(spinner).to.have.length(0);
    });

    it('password policy form and reset section should be rendered, and there should be 4 side items', () => {
      const passwordPolicyForm = wrapper.find(PasswordPolicyForm);
      expect(passwordPolicyForm).to.have.length(1);
      expect(passwordPolicyForm.props().minimumStrength).to.deep.equal(orgJsonWithDomains.organization.security.passwordPolicy.minimumStrength);
      expect(passwordPolicyForm.props().expiryDays).to.deep.equal(orgJsonWithDomains.organization.security.passwordPolicy.expiryDays);

      const pageLayoutProps = wrapper.find(IdentityManagerPageLayout).props() as IdentityManagerPageLayoutProps;
      expect(pageLayoutProps.additionalChildren!.type).equals(PasswordResetSection);

      const sideWrapper = shallow(pageLayoutProps.side, createMockIntlContext());
      expect(sideWrapper.find(ListItem)).to.have.length(4);
    });

  });

  describe('With domains, and without Identity Manager', () => {

    const wrapper = passwordPolicyPageWithData({ loading: false, ...orgJsonWithDomains }, { products: { loading: false, identityManager: false } });

    it('doesn\'t show an error message', () => {
      expect(wrapper.find('span')).to.have.length(0);
    });

    it('doesn\'t show a spinner', () => {
      const spinner = wrapper.find(AkSpinner) as typeof AkSpinner;
      expect(spinner).to.have.length(0);
    });

    it('Reset section should be rendered, and there should be 2 side items', () => {
      const pageLayoutProps = wrapper.find(IdentityManagerPageLayout).props() as IdentityManagerPageLayoutProps;
      expect(pageLayoutProps.additionalChildren!.type).to.eq(PasswordResetSection);

      const sideWrapper = shallow(pageLayoutProps.side, createMockIntlContext());
      expect(sideWrapper.find(ListItem)).to.have.length(2);
    });
  });

  describe('Without domains, and without Identity Manager', () => {

    const products = { loading: false, products: { id: 'id', identityManager: false } };
    const wrapper = passwordPolicyPageWithData({ loading: false, ...orgJsonNoDomains }, products);

    it('doesn\'t show an error message', () => {
      expect(wrapper.find('span')).to.have.length(0);
    });

    it('doesn\'t show a spinner', () => {
      const spinner = wrapper.find(AkSpinner) as typeof AkSpinner;
      expect(spinner).to.have.length(0);
    });

    it('Reset section should NOT be rendered, and there should be 2 side items', () => {
      const pageLayoutProps = wrapper.find(IdentityManagerPageLayout).props() as IdentityManagerPageLayoutProps;
      expect(pageLayoutProps.additionalChildren).to.be.undefined();

      const sideWrapper = shallow(pageLayoutProps.side, createMockIntlContext());
      expect(sideWrapper.find(ListItem)).to.have.length(2);
    });
  });

  describe('Without domains, and with Identity Manager', () => {

    const wrapper = passwordPolicyPageWithData({ loading: false, ...orgJsonNoDomains }, { products: { loading: false, identityManager: true } });

    it('doesn\'t show an error message', () => {
      expect(wrapper.find('span')).to.have.length(0);
    });

    it('doesn\'t show a spinner', () => {
      const spinner = wrapper.find(AkSpinner) as typeof AkSpinner;
      expect(spinner).to.have.length(0);
    });

    it('Reset section should NOT be rendered, and there should be 4 side items', () => {
      const pageLayoutProps = wrapper.find(IdentityManagerPageLayout).props() as IdentityManagerPageLayoutProps;
      expect(pageLayoutProps.additionalChildren).to.be.undefined();

      const sideWrapper = shallow(pageLayoutProps.side, createMockIntlContext());
      expect(sideWrapper.find(ListItem)).to.have.length(4);
    });
  });

  describe('with old organization data', () => {

    // To test the case where we may have organization data but the loading prop is set to true.
    // We can end up in this state because we have organization data from a previous request, but it is
    // fetching new data. See: http://dev.apollodata.com/react/api-queries.html#graphql-query-data-loading
    const wrapper = passwordPolicyPageWithData({ loading: true, ... orgJsonWithDomains });

    it('shows the spinner if loading', () => {
      const spinner = wrapper.find(AkSpinner) as typeof AkSpinner;

      expect(spinner).to.have.length(1);
      expect(spinner.props().isCompleting).to.be.false();
    });

    it('no password policy form or reset passwords section should be rendered', () => {
      const passwordPolicyForm = wrapper.find(PasswordPolicyForm);
      expect(passwordPolicyForm).to.have.length(0);
      expect(wrapper.find(PasswordResetSection)).to.have.length(0);
    });
  });
});
