import { PasswordPolicyStrength } from './password-policy-strength';

export interface PasswordPolicy {
  minimumStrength: PasswordPolicyStrength;
  expiryDays: number;
}

export interface FormPropType {
  minimumStrength: PasswordPolicyStrength;
  expiryDays: number | null;
  resetPasswords: boolean;
}
