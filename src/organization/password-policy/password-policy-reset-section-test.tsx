import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import AkButton from '@atlaskit/button';

import { Button as AnalyticsButton } from 'common/analytics';
import { ModalDialog } from 'common/modal';

import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import { PasswordResetSectionImpl } from './password-policy-reset-section';

describe('Password policy reset section', () => {

  const resetPasswords = sinon.spy();

  function resetSection({ resettingPasswords }) {
    return mount(
      <PasswordResetSectionImpl
        resettingPasswords={resettingPasswords}
        resetPasswords={resetPasswords}
        cancelResetPasswords={sinon.spy() as any}
        hasIM={false}
        intl={createMockIntlProp()}
      />,
      createMockIntlContext(),
    );
  }

  beforeEach(() => {
    resetPasswords.reset();
  });

  it('Renders the section normally when not resetting passwords', () => {
    const wrapper = resetSection({
      resettingPasswords: false,
    });
    expect(wrapper.find('h3').at(0).text()).to.equal('Password reset');
    expect(wrapper.find(AkButton).at(0).text()).to.equal('Reset passwords');
  });

  it('Pressing reset passwords brings up modal and then reset passwords', () => {
    const wrapper = resetSection({
      resettingPasswords: false,
    });
    const button = wrapper.find(AkButton).at(0);
    button.simulate('click');
    const modal = wrapper.find(ModalDialog);
    expect(
      (wrapper.find(PasswordResetSectionImpl).instance() as PasswordResetSectionImpl).state.isModalOpen,
    ).to.equal(true);
    expect(modal).to.have.length(1);
    const resetConfirmButton = modal.find(AkButton).at(0);
    expect(resetConfirmButton.text()).to.equal('Reset passwords');

    resetConfirmButton.simulate('click');
    expect(resetPasswords.calledOnce).to.equal(true);
    expect(
      (wrapper.find(PasswordResetSectionImpl).instance() as PasswordResetSectionImpl).state.isModalOpen,
    ).to.equal(false);
  });

  it('Pressing cancel hides modal and does not reset passwords', () => {
    const wrapper = resetSection({
      resettingPasswords: false,
    });
    const button = wrapper.find(AkButton).at(0);
    button.simulate('click');

    const modal = wrapper.find(ModalDialog);
    expect(
      (wrapper.find(PasswordResetSectionImpl).instance() as PasswordResetSectionImpl).state.isModalOpen,
    ).to.equal(true);
    expect(modal).to.have.length(1);
    const resetCancelButton = modal.find(AkButton).at(1);
    expect(resetCancelButton.text()).to.equal('Cancel');

    resetCancelButton.simulate('click');
    expect(resetPasswords.calledOnce).to.equal(false);
    expect(
      (wrapper.find(PasswordResetSectionImpl).instance() as PasswordResetSectionImpl).state.isModalOpen,
    ).to.equal(false);
  });

  it('while resetting passwords modal is visible and button is disabled', () => {
    const wrapper = resetSection({
      resettingPasswords: true,
    });
    const modal = wrapper.find(ModalDialog);
    expect(modal).to.have.length(1);
    const resetButton = modal.find(AnalyticsButton).first();
    expect(resetButton.prop('isLoading')).to.equal(true);

    resetButton.at(0).simulate('click');
    expect(wrapper.find(ModalDialog)).to.have.length(1);
  });

  it('mentions Atlassian Acess', () => {
    const wrapper = resetSection({
      resettingPasswords: false,
    });
    expect(wrapper.find('p').at(0).text()).to.contain('If you are subscribed to Atlassian Access, new passwords will have to comply with your existing password policy.');
  });

});
