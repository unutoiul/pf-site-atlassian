import { expect } from 'chai';
import { mount, ReactWrapper } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import AkFieldRadioGroup from '@atlaskit/field-radio-group';
import AkTextField from '@atlaskit/field-text';

import { Button } from 'common/analytics';
import { ModalDialog } from 'common/modal';

import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import { PasswordPolicyFormImpl, PasswordPolicyStrength } from '../password-policy';

describe('PasswordPolicyForm', () => {
  const setPasswordPolicy = sinon.spy();

  class PasswordPolicyFormHelper {
    private wrapper: ReactWrapper<any, any>;

    constructor(minimumStrength, expiryDays) {
      this.wrapper = passwordPolicyForm(minimumStrength, expiryDays);
    }

    public submitForm(): void {
      this.form.simulate('submit', { preventDefault: () => null });
    }

    public expectDialogStateToBe(state: 'open' | 'closed'): void {
      expect(this.dialog).to.have.length(1);
      expect(!!this.dialog.props().isOpen).to.equal(state === 'open', `dialog was expected to be ${state}`);
    }

    public expectDialogToHaveButtons(expectedButtonsCount: number): void {
      const buttons = this.dialog.find(Button);
      expect(buttons).to.have.length(expectedButtonsCount);
    }

    public clickDialogButton(button: 'submit' | 'cancel'): void {
      const buttons = this.dialog.find(Button);
      expect(buttons).to.have.length(2);
      buttons.at(button === 'submit' ? 0 : 1).simulate('click');
    }

    private get dialog(): ReactWrapper<any, any> {
      return this.wrapper.find(ModalDialog);
    }

    private get form(): ReactWrapper<any, any> {
      return this.wrapper.find('form');
    }
  }

  function passwordPolicyForm(minimumStrength, expiryDays) {
    return mount(
      <PasswordPolicyFormImpl
        minimumStrength={minimumStrength}
        expiryDays={expiryDays}
        setPasswordPolicy={setPasswordPolicy}
        cancelSetPasswordPolicy={() => {
          return;
        }}
        intl={createMockIntlProp()}
        settingPasswordPolicy={undefined as any}
      />,
      createMockIntlContext(),
    );
  }

  afterEach(() => {
    setPasswordPolicy.reset();
  });

  it('should contain correct content', () => {
    const wrapper = passwordPolicyForm(PasswordPolicyStrength.VeryStrong, 5);
    const form = wrapper.find('form');

    const description = form.find('h3');
    const radioGroups = form.find(AkFieldRadioGroup);
    const textField = form.find(AkTextField);
    const buttons = form.find(Button);

    expect(description).to.have.length(1);
    expect(radioGroups).to.have.length(2);
    expect(buttons).to.have.length(1);

    expect(description.text()).to.contain('Password policy');
    expect(textField.props().value).to.equal('5');
    expect(radioGroups.at(0).prop('items')[4].defaultSelected).to.equal(true);
    expect(radioGroups.at(1).prop('items')[0].defaultSelected).to.equal(true);
    expect(buttons.children().text()).to.deep.equal('Update password policy');
    expect(buttons.props().appearance).to.deep.equal('primary');
  });

  describe('button actions', () => {
    it('submitting form should trigger setPasswordPolicy', () => {
      const helper = new PasswordPolicyFormHelper(PasswordPolicyStrength.VeryStrong, 5);

      expect(setPasswordPolicy.callCount).to.equal(0);
      helper.submitForm();
      expect(setPasswordPolicy.callCount).to.equal(0);

      helper.expectDialogStateToBe('open');
      helper.expectDialogToHaveButtons(2);

      helper.clickDialogButton('submit');

      expect(setPasswordPolicy.callCount).to.equal(1);
      expect(setPasswordPolicy.getCall(0).args[0]).to.deep.equal({
        applyImmediatelyDialogOpen: true,
        minimumStrength: PasswordPolicyStrength.VeryStrong,
        expiryDays: 5,
        expiryChecked: true,
        resetPasswords: true,
      });
      helper.expectDialogStateToBe('closed');
    });

    it('clicking on the cancel button should trigger cancel action', () => {
      const helper = new PasswordPolicyFormHelper(PasswordPolicyStrength.VeryStrong, 5);

      expect(setPasswordPolicy.callCount).to.equal(0);
      helper.submitForm();
      expect(setPasswordPolicy.callCount).to.equal(0);

      helper.expectDialogStateToBe('open');
      helper.expectDialogToHaveButtons(2);

      helper.clickDialogButton('cancel');

      expect(setPasswordPolicy.callCount).to.equal(0);
      helper.expectDialogStateToBe('closed');
    });
  });

  it('apply changes on next login should not open dialog', () => {
    const wrapper = passwordPolicyForm(PasswordPolicyStrength.Good, 3);
    const formWrapper = wrapper.find(PasswordPolicyFormImpl).instance() as PasswordPolicyFormImpl;
    formWrapper.setState({ resetPasswords: false });
    const form = wrapper.find('form');
    expect(setPasswordPolicy.callCount).to.equal(0);

    form.simulate('submit', { preventDefault: () => null });
    expect(setPasswordPolicy.callCount).to.equal(1);
    expect(setPasswordPolicy.getCall(0).args[0]).to.deep.equal({
      applyImmediatelyDialogOpen: false,
      minimumStrength: PasswordPolicyStrength.Good,
      expiryDays: 3,
      expiryChecked: true,
      resetPasswords: false,
    });
    expect(formWrapper.state.applyImmediatelyDialogOpen).to.equal(false);
  });

  it('submitting form without password expiry should set password expiry days to null', () => {
    const wrapper = passwordPolicyForm(PasswordPolicyStrength.Good, 3);
    const formWrapper = wrapper.find(PasswordPolicyFormImpl).instance() as PasswordPolicyFormImpl;
    const form = wrapper.find('form');
    expect(setPasswordPolicy.callCount).to.equal(0);
    const checkbox = wrapper.find('input').at(5);
    (checkbox.getDOMNode() as HTMLInputElement).checked = false;
    checkbox.simulate('change');
    formWrapper.setState({ resetPasswords: false });

    form.simulate('submit', { preventDefault: () => null });
    expect(setPasswordPolicy.callCount).to.equal(1);
    expect(setPasswordPolicy.getCall(0).args[0].expiryChecked).to.equal(false);
    expect(setPasswordPolicy.getCall(0).args[0].expiryDays).to.equal(null);
    expect(formWrapper.state.applyImmediatelyDialogOpen).to.equal(false);
  });

});
