import * as React from 'react';
import { defineMessages, FormattedHTMLMessage, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import { ButtonGroup as AkButtonGroup } from '@atlaskit/button';
import { Checkbox as AkCheckboxStateless } from '@atlaskit/checkbox';
import { Label as AkLabel } from '@atlaskit/field-base';
import AkFieldRadioGroup from '@atlaskit/field-radio-group';
import AkTextField from '@atlaskit/field-text';
import AkErrorIcon from '@atlaskit/icon/glyph/error';
import { akGridSizeUnitless } from '@atlaskit/util-shared-styles';

import { ActionsBar, ActionsBarLabel, ActionsBarSmallButton } from 'common/actions-bar';
import { Button as AnalyticsButton } from 'common/analytics';
import { InfoHover } from 'common/info-hover/info-hover';
import { ModalDialog } from 'common/modal';

import { createOrgAnalyticsData } from '../organizations/organization-analytics';
import { PasswordPolicyStrength } from './password-policy-strength';
import { FormPropType } from './password-policy.prop-types';

interface OwnProps {
  minimumStrength: PasswordPolicyStrength;
  expiryDays: number;
  settingPasswordPolicy: boolean;
  orgId?: string;
  setPasswordPolicy(e: FormPropType): void;
  cancelSetPasswordPolicy(): void;
}

interface OwnState {
  applyImmediatelyDialogOpen: boolean;
  minimumStrength: PasswordPolicyStrength;
  expiryDays: number | null;
  expiryChecked: boolean;
  resetPasswords: boolean;
}

const PasswordPolicyHeader = styled.div`
  line-height:  ${akGridSizeUnitless * 5}px;
  margin-bottom: ${akGridSizeUnitless * 2}px;
`;

const PasswordStrengthTooltip = styled.div`
  margin: ${akGridSizeUnitless * 0.75}px;
`;

const PasswordStrengthDialogContent = styled.div`
  font-weight: normal;
  width: ${akGridSizeUnitless * 42}px;
`;

const ApplyChangesFieldGroup = styled.div`
  padding-bottom: ${akGridSizeUnitless * 2}px;
`;

const PasswordStrengthLabel = styled.div`
  display: inline-block;
`;

const StrengthFieldGroup = styled.div`
  display: block;
  margin-top: ${akGridSizeUnitless * 4}px;
`;

const messages = defineMessages({
  passwordPolicyTitle: {
    id: 'organization.password.policy.form.passwordPolicyTitle',
    defaultMessage: 'Password policy',
  },
  description: {
    id: 'organization.password.policy.form.description',
    defaultMessage: 'Use a password policy to help your users to create secure passwords.',
  },
  error: {
    id: 'organization.password.policy.form.error',
    defaultMessage: 'Password policy data could not be retrieved.',
  },
  passwordStrengthTitle: {
    id: 'organization.password.policy.form.passwordStrengthTitle',
    defaultMessage: 'Minimum password strength',
  },
  weak: {
    id: 'organization.password.policy.form.weak',
    defaultMessage: 'Weak',
  },
  good: {
    id: 'organization.password.policy.form.good',
    defaultMessage: 'Good',
  },
  fair: {
    id: 'organization.password.policy.form.fair',
    defaultMessage: 'Fair',
  },
  strong: {
    id: 'organization.password.policy.form.strong',
    defaultMessage: 'Strong',
  },
  veryStrong: {
    id: 'organization.password.policy.form.veryStrong',
    defaultMessage: 'Very strong',
  },
  passwordExpiryLabel: {
    id: 'organization.password.policy.form.passwordExpiryLabel',
    defaultMessage: 'Password expiry',
  },
  passwordExpiry1: {
    id: 'organization.password.policy.form.passwordExpiry1',
    defaultMessage: 'Passwords expire every',
    description: `Full sentence will be "Passwords expire every [#] days", which is organization.password.policy.form.passwordExpiry1 and organization.password.policy.form.passwordExpiry2 combined.
     Please translate it as what would come before the #`,
  },
  passwordExpiry2: {
    id: 'organization.password.policy.form.passwordExpiry2',
    defaultMessage: 'days',
    description: `Full sentence will be "Passwords expire every [#] days", which is organization.password.policy.form.passwordExpiry1 and organization.password.policy.form.passwordExpiry2 combined.
     Please translate it as what would come after the #`,
  },
  submitButton: {
    id: 'organization.password.policy.form.submitButton',
    defaultMessage: 'Update password policy',
  },
  applyChangesLabel: {
    id: 'organization.password.policy.form.applyChangesLabel',
    defaultMessage: 'Apply changes',
  },
  applyImmediatelyTitle: {
    id: 'organization.password.policy.form.applyImmediatelyTitle',
    defaultMessage: 'Immediately',
  },
  applyImmediatelyDescription: {
    id: 'organization.password.policy.form.applyImmediatelyDescription',
    defaultMessage: 'Users will be logged out and prompted to set a new password when they next log in.',
  },
  applyNextTimeTitle: {
    id: 'organization.password.policy.form.applyNextTimeTitle',
    defaultMessage: 'Next time user changes their password',
  },
  info: {
    id: 'organization.password.policy.form.info',
    defaultMessage: `If your team uses a password policy, we recommend choosing the highest strength.
    <a href="https://confluence.atlassian.com/x/4YxjL" target="_blank" rel="noopener noreferrer">
    Learn more about setting requirements for passwords.</a>`,
  },
  applyImmediatelyDialogTitle: {
    id: 'organization.password.policy.form.applyImmediatelyDialogTitle',
    defaultMessage: 'Enforce password policy immediately?',
  },
  applyImmediatelyConfirmation: {
    id: 'organization.password.policy.form.applyImmediatelyConfirmation',
    defaultMessage: 'User passwords will now need to satisfy the updated password policy.',
  },
  passwordsExpireEveryXDays: {
    id: 'organization.password.policy.form.passwordsExpireEveryXDays',
    defaultMessage: ` Their passwords will expire every {expiryDays, plural,
      one {day}
      other {# days}}.`,
  },
  usersWillBeLoggedOut: {
    id: 'organization.password.policy.form.usersWillBeLoggedOut',
    defaultMessage: 'Users will be logged out and will have to set a new password when they next log in.',
  },
  enforceButton: {
    id: 'organization.password.policy.form.enforceButton',
    defaultMessage: 'Enforce',
  },
  cancelButton: {
    id: 'organization.password.policy.form.cancelButton',
    defaultMessage: 'Cancel',
  },
});

const defaultExpiryDays: number = 180;

export class PasswordPolicyFormImpl extends React.Component<OwnProps & InjectedIntlProps, OwnState> {
  private formElement?: HTMLFormElement;

  public readonly state: Readonly<OwnState> = {
    applyImmediatelyDialogOpen: false,
    minimumStrength: this.props.minimumStrength,
    expiryDays: this.props.expiryDays,
    expiryChecked: !!this.props.expiryDays,
    resetPasswords: true,
  };

  public render() {

    const { formatMessage } = this.props.intl;

    const strengthItems = [
      { name: 'passwordstrength', value: PasswordPolicyStrength.Weak, label: formatMessage(messages.weak), defaultSelected: this.props.minimumStrength === PasswordPolicyStrength.Weak },
      { name: 'passwordstrength', value: PasswordPolicyStrength.Fair, label: formatMessage(messages.fair), defaultSelected: this.props.minimumStrength === PasswordPolicyStrength.Fair },
      { name: 'passwordstrength', value: PasswordPolicyStrength.Good, label: formatMessage(messages.good), defaultSelected: this.props.minimumStrength === PasswordPolicyStrength.Good },
      { name: 'passwordstrength', value: PasswordPolicyStrength.Strong, label: formatMessage(messages.strong), defaultSelected: this.props.minimumStrength === PasswordPolicyStrength.Strong },
      { name: 'passwordstrength', value: PasswordPolicyStrength.VeryStrong, label: formatMessage(messages.veryStrong), defaultSelected: this.props.minimumStrength === PasswordPolicyStrength.VeryStrong },
    ];

    const passwordApplyChangesItems = [
      {
        name: 'resetpasswords', value: 'true', label: (
          <span>
            {formatMessage(messages.applyImmediatelyTitle)}
            <InfoHover
              dialogContent={<span>{formatMessage(messages.applyImmediatelyDescription)}</span>}
              persistTime={0}
            />
          </span>
        ), defaultSelected: true,
      },
      { name: 'resetpasswords', value: 'false', label: formatMessage(messages.applyNextTimeTitle) },
    ];

    const dialogHeader = formatMessage(messages.applyImmediatelyDialogTitle);

    const dialogFooter = (
      <AkButtonGroup>
        <AnalyticsButton
          appearance="primary"
          isLoading={!!this.props.settingPasswordPolicy}
          analyticsData={createOrgAnalyticsData({
            orgId: this.props.orgId,
            action: 'click',
            actionSubject: 'passwordPolicyButton',
            actionSubjectId: 'update',
          })}
          onClick={this.submitFromDialog}
        >
          {formatMessage(messages.enforceButton)}
        </AnalyticsButton>
        <AnalyticsButton
          appearance="subtle-link"
          onClick={this.cancelApplyImmediately}
          analyticsData={createOrgAnalyticsData({
            orgId: this.props.orgId,
            action: 'click',
            actionSubject: 'passwordPolicyButton',
            actionSubjectId: 'cancel',
          })}
        >
          {formatMessage(messages.cancelButton)}
        </AnalyticsButton>
      </AkButtonGroup>
    );

    if (!this.props.minimumStrength) {
      return <div><AkErrorIcon label={formatMessage(messages.error)} />{formatMessage(messages.error)}</div>;
    }

    return (
      <form onSubmit={this.submitForm} ref={this.setFormElement}>
        <PasswordPolicyHeader>
          <h3>{formatMessage(messages.passwordPolicyTitle)}</h3>
          <div>{formatMessage(messages.description)}</div>
        </PasswordPolicyHeader>
        <ActionsBarLabel>
          <PasswordStrengthLabel>
            <AkLabel label={formatMessage(messages.passwordStrengthTitle)} /></PasswordStrengthLabel>
          <PasswordStrengthTooltip>
            <InfoHover
              dialogContent={
                <PasswordStrengthDialogContent>
                  <FormattedHTMLMessage {...messages.info} />
                </PasswordStrengthDialogContent>}
            />
          </PasswordStrengthTooltip>
        </ActionsBarLabel>
        <StrengthFieldGroup><AkFieldRadioGroup items={strengthItems} onRadioChange={this.updateFormState} /></StrengthFieldGroup>
        <ActionsBarLabel><AkLabel label={formatMessage(messages.passwordExpiryLabel)} /></ActionsBarLabel>
        <ActionsBar>
          <AkCheckboxStateless
            defaultChecked={!!this.props.expiryDays}
            isChecked={this.state.expiryChecked}
            label={formatMessage(messages.passwordExpiry1)}
            name="expirydayscheck"
            onChange={this.updateFormState}
            value={''}
          />
          <ActionsBarSmallButton>
            <AkTextField
              disabled={!this.state.expiryChecked}
              min={1}
              required={this.state.expiryChecked}
              isLabelHidden={true}
              label={formatMessage(messages.passwordExpiry2)}
              value={this.props.expiryDays ? this.props.expiryDays.toString() : defaultExpiryDays.toString()}
              onChange={this.updateFormState}
              compact={true}
              name="passwordexpiry"
              type="number"
            />
          </ActionsBarSmallButton>
          {formatMessage(messages.passwordExpiry2)}
        </ActionsBar>
        <AkLabel label={formatMessage(messages.applyChangesLabel)} />
        <ApplyChangesFieldGroup><AkFieldRadioGroup onRadioChange={this.updateFormState} items={passwordApplyChangesItems} /></ApplyChangesFieldGroup>
        <AnalyticsButton
          appearance="primary"
          isLoading={(this.props.settingPasswordPolicy && !this.state.resetPasswords)}
          type="submit"
          analyticsData={createOrgAnalyticsData({
            orgId: this.props.orgId,
            action: 'click',
            actionSubject: 'passwordPolicyButton',
            actionSubjectId: 'openModal',
          })}
        >
          {formatMessage(messages.submitButton)}
        </AnalyticsButton>
        <ModalDialog
          header={dialogHeader}
          footer={dialogFooter}
          onClose={this.closeApplyImmediatelyDialog}
          isOpen={this.state.applyImmediatelyDialogOpen || (this.props.settingPasswordPolicy && this.state.resetPasswords)}
          width={'small'}
        >
          <p>
            <FormattedMessage {...messages.applyImmediatelyConfirmation} />
            {this.state.expiryChecked &&
              <FormattedMessage values={{ expiryDays: this.state.expiryDays! }} {...messages.passwordsExpireEveryXDays} />
            }
          </p>
          <p>
            <FormattedMessage {...messages.usersWillBeLoggedOut} />
          </p>
        </ModalDialog>
      </form>
    );
  }

  private updateFormState = () => {
    if (!this.formElement) {
      return;
    }
    const { expirydayscheck, resetpasswords, passwordstrength, passwordexpiry } = this.formElement.elements as any;

    this.setState({
      expiryChecked: expirydayscheck.checked,
      expiryDays: expirydayscheck.checked ? parseInt(passwordexpiry.value, 10) : null,
      resetPasswords: this.getRadioValue(resetpasswords) === 'true',
      minimumStrength: this.getRadioValue(passwordstrength),
    });
  };

  // IE11 doesn't fully support HTMLFormControlsCollection API, so we need to get the value of radio group manually
  private getRadioValue = (radioGroup) => {
    return radioGroup.value || Array.from<HTMLInputElement>(radioGroup).find(radio => radio.checked)!.value;
  }

  private cancelApplyImmediately = () => {
    if (this.props.settingPasswordPolicy) {
      this.props.cancelSetPasswordPolicy();
    }
    this.closeApplyImmediatelyDialog();
  };

  private closeApplyImmediatelyDialog = () => {
    this.setState({ applyImmediatelyDialogOpen: false });
  };

  private submitForm = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    if (this.state.resetPasswords) {
      this.setState({ applyImmediatelyDialogOpen: true });
    } else {
      this.props.setPasswordPolicy(this.state);
    }
  };

  private submitFromDialog = () => {
    this.props.setPasswordPolicy(this.state);
    this.closeApplyImmediatelyDialog();
  };

  private setFormElement = (formElement: HTMLFormElement) => {
    if (formElement) {
      this.formElement = formElement;
    }
  };

}

export const PasswordPolicyForm = injectIntl<OwnProps>(PasswordPolicyFormImpl);
