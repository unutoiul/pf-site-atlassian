import { ApolloQueryResult } from 'apollo-client';
import * as React from 'react';
import { compose, graphql, MutationOpts, QueryResult } from 'react-apollo';
import { defineMessages, FormattedHTMLMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps } from 'react-router';
import styled from 'styled-components';

import AkCheckCircleIcon from '@atlaskit/icon/glyph/check-circle';
import AkSpinner from '@atlaskit/spinner';
import { akColorG300, akGridSizeUnitless } from '@atlaskit/util-shared-styles';

import { FlagProps, withFlag } from 'common/flag';
import { PageSidebar } from 'common/page-layout';

import { analyticsClient, passwordMgmtScreen, passwordMgmtScreenEvent, Referrer } from 'common/analytics';

import { OrgBreadcrumbs } from '../breadcrumbs';

import {
  PasswordPolicyQuery,
  PasswordPolicyQueryVariables, ProductsQuery, ProductsQueryVariables,
  ResetPasswordsMutation,
  ResetPasswordsMutationVariables,
  UpdatePasswordPolicyMutation,
  UpdatePasswordPolicyMutationVariables,
} from '../../schema/schema-types';
import { IdentityManagerPageLayout } from '../identity-manager/identity-manager-page-layout';
import { hasAnyDomainVerified } from '../organizations/organizations.util';
import { PasswordPolicyForm } from './password-policy-form';
import { PasswordResetSection } from './password-policy-reset-section';
import { PasswordPolicyStrength } from './password-policy-strength';
import { FormPropType } from './password-policy.prop-types';
import getPasswordPolicyQuery from './password-policy.query.graphql';
import hasIdentityManagerQuery from './products.query.graphql';
import resetPasswordsMutation from './reset-passwords.mutation.graphql';
import updatePasswordPolicyMutation from './update-password-policy.mutation.graphql';

interface OrgParams {
  orgId: string;
}

interface OwnProps extends RouteComponentProps<OrgParams> {
  data: QueryResult & PasswordPolicyQuery;
  products: QueryResult & ProductsQuery;
  updatePasswordPolicy(opts: MutationOpts & { variables: UpdatePasswordPolicyMutationVariables }): Promise<ApolloQueryResult<UpdatePasswordPolicyMutation>>;
  resetPasswords(opts: MutationOpts & { variables: ResetPasswordsMutationVariables }): Promise<ApolloQueryResult<ResetPasswordsMutation>>;
}

interface OwnState {
  settingPasswordPolicy: boolean;
  resettingPasswords: boolean;
}

// tslint:disable-next-line:no-var-requires no-require-imports
const PasswordSafeImage = require('./password-policy-safe.svg');

// exported for testing
export const ListItem = styled.li`
  padding: ${akGridSizeUnitless}px;
`;

const GreenTick = props => <AkCheckCircleIcon {...props} primaryColor={akColorG300} />;

const messages = defineMessages({
  title: {
    id: 'organization.password.policy.page.title',
    defaultMessage: 'Password management',
  },
  error: {
    id: 'organization.password.policy.page.error',
    defaultMessage: 'Password policy data could not be retrieved.',
  },
  sidebarTitle: {
    id: 'organization.password.policy.page.sidebarTitle',
    defaultMessage: 'What you need to know',
  },
  sidebarPoint1: {
    id: 'organization.password.policy.page.sidebarPoint1',
    defaultMessage: 'New users will be subject to your password policy when they sign up.',
  },
  sidebarPoint2: {
    id: 'organization.password.policy.page.sidebarTitle2',
    defaultMessage: 'Users will be subject to your password policy the next time they change their password.',
  },
  sidebarPoint3: {
    id: 'organization.password.policy.page.sidebarTitle3',
    defaultMessage: 'You can only manage passwords of users from your <a href="./domains">verified domains</a>.',
  },
  sidebarPoint4: {
    id: 'organization.password.policy.page.sidebarTitle4',
    defaultMessage: '<a href="https://confluence.atlassian.com/x/29z1Nw" target="_blank" rel="noopener noreferrer">Learn more about password policy.</a>',
  },
  successFlagTitle: {
    id: 'organization.password.policy.page.successFlagTitle',
    defaultMessage: 'You\'ve updated your password policy',
  },
  successFlagDesc: {
    id: 'organization.password.policy.page.successFlagDesc',
    defaultMessage: 'Your new policy will be enforced the next time users change their password.',
  },
  successImmediateFlagDesc: {
    id: 'organization.password.policy.page.successImmediateFlagDesc',
    defaultMessage: 'Users will be logged out and will have to set a new password when they next log in.',
  },
  passwordResetSuccessTitle: {
    id: 'organization.password.policy.page.passwordResetSuccessTitle',
    defaultMessage: 'You\'ve reset your users\' passwords.',
  },
  passwordResetSuccessDesc: {
    id: 'organization.password.policy.page.passwordResetSuccessDesc',
    defaultMessage: 'Users were logged out and prompted to set a new password.',
  },
  passwordResetSuccessLink: {
    id: 'organization.password.policy.page.passwordResetSuccessLink',
    defaultMessage: 'Set a new password for yourself.',
  },
  identityManagerEvaluation: {
    id: 'organization.password.policy.page.identityManagerEvaluation',
    defaultMessage: `Password policy is an Atlassian Access feature. Before you can apply password policies you need to have
    verified a domain and have an Atlassian Access subscription.`,
  },
  domainPrompt: {
    id: 'organization.password.policy.page.domainPrompt',
    defaultMessage: 'To enforce password policies on your managed accounts, you need to verify a domain.',
  },
});

export class PasswordPolicyPageImpl extends React.Component<OwnProps & FlagProps & InjectedIntlProps, OwnState> {
  public state: OwnState = { settingPasswordPolicy: false, resettingPasswords: false };

  public render() {
    const {
      products,
      intl: { formatMessage },
      data: { error, loading, organization },
    } = this.props;

    const sidePanel = (
      <PageSidebar imgAfterSrc={PasswordSafeImage}>
        <h4>{formatMessage(messages.sidebarTitle)}</h4>
        <ul>
          {!products.loading && products.products && products.products.identityManager && <ListItem>{formatMessage(messages.sidebarPoint1)}</ListItem>}
          {!products.loading && products.products && products.products.identityManager && <ListItem>{formatMessage(messages.sidebarPoint2)}</ListItem>}
          <ListItem onClick={this.handleSidebarClick}><FormattedHTMLMessage {...messages.sidebarPoint3} /></ListItem>
          <ListItem><FormattedHTMLMessage {...messages.sidebarPoint4} /></ListItem>
        </ul>
      </PageSidebar>
    );

    return (
      <Referrer value="password">
        <IdentityManagerPageLayout
          title={formatMessage(messages.title)}
          side={sidePanel}
          domainPrompt={formatMessage(messages.domainPrompt)}
          orgId={this.props.match.params.orgId}
          analyticsScreenForEvalButton={passwordMgmtScreen}
          cardDescription={formatMessage(messages.identityManagerEvaluation)}
          additionalChildren={!loading && organization && organization.domainClaim && !products.loading &&
            hasAnyDomainVerified(organization.domainClaim.domains) ?
              <PasswordResetSection
                resetPasswords={this.resetPasswords}
                cancelResetPasswords={this.cancelResetPasswords}
                resettingPasswords={this.state.resettingPasswords}
                hasIM={!products.products ? false : products.products.identityManager}
              />
              : undefined
          }
          breadcrumbs={<OrgBreadcrumbs />}
          screenEvent={passwordMgmtScreenEvent}
        >
          {(error || (!loading && (!organization || !organization.security))) && <span>{formatMessage(messages.error)}</span>}
          {loading && <AkSpinner />}
          {!loading && organization && organization.security && organization.security.passwordPolicy &&
            <div>
              <PasswordPolicyForm
                minimumStrength={organization.security.passwordPolicy.minimumStrength as PasswordPolicyStrength}
                expiryDays={organization.security.passwordPolicy.expiryDays!}
                setPasswordPolicy={this.setPasswordPolicy}
                cancelSetPasswordPolicy={this.cancelSetPasswordPolicy}
                settingPasswordPolicy={this.state.settingPasswordPolicy}
              />
            </div>
          }
        </IdentityManagerPageLayout>
      </Referrer>
    );
  }

  private handleSidebarClick = (e) => {
    const el = e.target;

    const isSimpleLinkClick = el instanceof HTMLAnchorElement && !(e.metaKey || e.shiftKey);
    if (!isSimpleLinkClick) {
      return;
    }

    const href = el.getAttribute('href') || '';
    const isInAppHref = href.startsWith('/') || href.startsWith('.');

    if (!isInAppHref) {
      return;
    }

    e.preventDefault();
    if (!!this.props.history) {
      this.props.history.push(href);
    }
  };

  // Doesn't actually cancel the request
  private cancelSetPasswordPolicy = (): void => {
    this.setState({ settingPasswordPolicy: false });
  };

  // Doesn't actually cancel the request
  private cancelResetPasswords = (): void => {
    this.setState({ resettingPasswords: false });
  };

  private setPasswordPolicy = async (formState: FormPropType) => {
    this.setState({ settingPasswordPolicy: true });

    return this.props.updatePasswordPolicy({
      variables: {
        input: {
          id: this.props.match.params.orgId,
          minimumStrength: formState.minimumStrength,
          expiryDays: formState.expiryDays,
          resetPasswords: formState.resetPasswords,
        },
      },
      refetchQueries: [{
        query: getPasswordPolicyQuery,
        variables: { id: this.props.match.params.orgId },
      }],
    }).then(() => {
      this.setState({ settingPasswordPolicy: false });
      const flagDesc = formState.resetPasswords ? messages.successImmediateFlagDesc : messages.successFlagDesc;
      this.props.showFlag({
        appearance: 'normal',
        autoDismiss: true,
        description: this.props.intl.formatMessage(flagDesc),
        icon: <GreenTick label="" />,
        id: 'set-password-policy-success',
        title: this.props.intl.formatMessage(messages.successFlagTitle),
      });
    }).catch((error) => {
      if (this.state.settingPasswordPolicy === false) {
        // User hit 'cancel' button, don't show an error flag for a request that they 'cancelled'
        return;
      }
      analyticsClient.onError(error);
      this.setState({ settingPasswordPolicy: false });
    });
  };

  private resetPasswords = async () => {
    const { formatMessage } = this.props.intl;

    const args = {
      variables: {
        id: this.props.match.params.orgId,
      },
    };

    this.setState({ resettingPasswords: true });

    return this.props.resetPasswords(args).then(() => {
      this.setState({ resettingPasswords: false });
      this.props.showFlag({
        appearance: 'normal',
        autoDismiss: false, // Shouldn't be autodismissed as the admin needs to hover over the flag and click a link.
        description: (
          <div>
            {formatMessage(messages.passwordResetSuccessDesc)}<br />
            <a href="https://id.atlassian.com/manage/change-password" target="_blank" rel="noopener noreferrer">
              {formatMessage(messages.passwordResetSuccessLink)}
            </a>
          </div>
        ),
        icon: <GreenTick label="" />,
        id: 'reset-passwords-success',
        title: formatMessage(messages.passwordResetSuccessTitle),
      });
    }).catch((error) => {
      if (this.state.resettingPasswords === false) {
        // User hit 'cancel' button, don't show an error flag for a request that they 'canceled'
        return;
      }
      analyticsClient.onError(error);
      this.setState({ resettingPasswords: false });
    });
  };
}

const updatePasswordPolicy = graphql<UpdatePasswordPolicyMutationVariables, UpdatePasswordPolicyMutation, null>(updatePasswordPolicyMutation, { name: 'updatePasswordPolicy' });
const resetPasswords = graphql<ResetPasswordsMutationVariables, ResetPasswordsMutation>(resetPasswordsMutation, { name: 'resetPasswords' });

const getPasswordPolicyAndDomains = graphql<RouteComponentProps<OrgParams>, PasswordPolicyQuery, PasswordPolicyQueryVariables>(
  getPasswordPolicyQuery,
  { options: (props) => ({ variables: { id: props.match.params.orgId } }) },
);

const hasIdentityManager = graphql<RouteComponentProps<OrgParams>, ProductsQuery, ProductsQueryVariables, ProductsQueryVariables>(
  hasIdentityManagerQuery,
  { name: 'products', options: (props) => ({ variables: { id: props.match.params.orgId } }) },
);

export const PasswordPolicyPage = compose(
  getPasswordPolicyAndDomains,
  updatePasswordPolicy,
  resetPasswords,
  hasIdentityManager,
  withFlag,
  injectIntl,
)(PasswordPolicyPageImpl);
