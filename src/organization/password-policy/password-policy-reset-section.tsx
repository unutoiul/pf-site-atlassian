import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import AkButton, { ButtonGroup as AkButtonGroup } from '@atlaskit/button';
import { akGridSizeUnitless } from '@atlaskit/util-shared-styles';

import { Button as AnalyticsButton } from 'common/analytics';
import { ModalDialog } from 'common/modal';

interface OwnProps {
  resettingPasswords: boolean;
  hasIM: boolean;
  resetPasswords(): void;
  cancelResetPasswords(): void;
}

interface OwnState {
  isModalOpen: boolean;
}

export const messages = defineMessages({
  title: {
    id: 'organization.password.policy.reset.title',
    description: '',
    defaultMessage: 'Password reset',
  },
  description: {
    id: 'organization.password.policy.reset.description',
    description: '',
    defaultMessage: 'All users will be logged out and prompted to change their passwords. New passwords will have to be compliant with your existing password policy.',
  },
  descriptionNoIM: {
    id: 'organization.password.policy.reset.descriptionNoIM',
    description: '',
    defaultMessage: 'All users will be logged out and prompted to change their passwords. If you are subscribed to Atlassian Access, new passwords will have to comply with your existing password policy.',
  },
  modalTitle: {
    id: 'organization.password.policy.reset.modalTitle',
    description: '',
    defaultMessage: 'Reset users\' passwords?',
  },
  modalDescription: {
    id: 'organization.password.policy.reset.modalDescription',
    description: '',
    defaultMessage: 'All users will be logged out and prompted to set a new password.',
  },
  modalCancelButtonText: {
    id: 'organization.password.policy.reset.modalCancelButtonText',
    description: '',
    defaultMessage: 'Cancel',
  },
  resetButtonText: {
    id: 'organization.password.policy.reset.resetButtonText',
    description: '',
    defaultMessage: 'Reset passwords',
  },
});

const PasswordResetDiv = styled.div`
  margin-top: ${akGridSizeUnitless * 8}px;
`;

const ResetButtonDiv = styled.div`
  margin-top: ${akGridSizeUnitless * 3}px;
`;

// tslint:disable-next-line:no-empty
const noop = () => {};

export class PasswordResetSectionImpl extends React.Component<OwnProps & InjectedIntlProps, OwnState> {

  public state: OwnState = { isModalOpen: false };

  public render() {

    const { intl: { formatMessage } } = this.props;

    const modalFooter = (
      <AkButtonGroup>
        <AnalyticsButton appearance="primary" onClick={this.resetPasswords} isLoading={this.props.resettingPasswords}>
          {formatMessage(messages.resetButtonText)}
        </AnalyticsButton>
        <AkButton onClick={this.cancelResetPasswords} appearance="subtle-link">
          {formatMessage(messages.modalCancelButtonText)}
        </AkButton>
      </AkButtonGroup>
    );

    return (
      <PasswordResetDiv>
        <h3>{formatMessage(messages.title)}</h3>
        <p>
          {this.props.hasIM ? formatMessage(messages.description) : formatMessage(messages.descriptionNoIM)}
        </p>
        <ResetButtonDiv>
          <AkButton appearance="default" onClick={this.toggleModal} isDisabled={this.props.resettingPasswords}>
            {formatMessage(messages.resetButtonText)}
          </AkButton>
        </ResetButtonDiv>

        <ModalDialog
          header={formatMessage(messages.modalTitle)}
          footer={modalFooter}
          isOpen={this.state.isModalOpen || this.props.resettingPasswords}
          width="small"
          onClose={noop}
        >
          {formatMessage(messages.modalDescription)}
        </ModalDialog>
      </PasswordResetDiv>);
  }

  private toggleModal = () => {
    this.setState({ isModalOpen: !this.state.isModalOpen });
  };

  private resetPasswords = () => {
    this.props.resetPasswords();
    this.setState({ isModalOpen: false });
  };

  private cancelResetPasswords = () => {
    this.props.cancelResetPasswords();
    this.setState({ isModalOpen: false });
  };

}

export const PasswordResetSection = injectIntl<OwnProps>(PasswordResetSectionImpl);
