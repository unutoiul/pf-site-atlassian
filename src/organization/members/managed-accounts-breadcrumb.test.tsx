import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import { MemoryRouter } from 'react-router-dom';

import { Breadcrumb } from 'common/breadcrumb';

import { createMockContext } from '../../utilities/testing';
import { ManagedAccountsBreadcrumbImpl } from './managed-accounts-breadcrumb';

describe('ManagedAccountsBreadcrumb', () => {
  it('should render a Breadcrumb with valid props', () => {
    const wrapper = mount((
      <MemoryRouter>
        <ManagedAccountsBreadcrumbImpl
          match={{
            isExact: false,
            path: '',
            url: '',
            params: {
              orgId: '1',
            },
          }}
          location={null as any}
          history={null as any}
        />
      </MemoryRouter>
    ), createMockContext({ intl: true, apollo: false }));

    const theBreadcrumb = wrapper.find(Breadcrumb);

    expect(theBreadcrumb).to.have.lengthOf(1);
    expect(theBreadcrumb.prop('href')).to.equal('/o/1/members');
    expect(theBreadcrumb.text()).to.equal('Managed accounts');
  });

  it('should render null if the org id is null', () => {
    const wrapper = mount((
      <MemoryRouter>
        <ManagedAccountsBreadcrumbImpl
          match={{
            isExact: false,
            path: '',
            url: '',
            params: {
              orgId: null,
            },
          } as any}
          location={null as any}
          history={null as any}
        />
      </MemoryRouter>
    ), createMockContext({ intl: true, apollo: false }));

    expect(wrapper.find(ManagedAccountsBreadcrumbImpl).isEmptyRender()).to.equal(true);
  });
});
