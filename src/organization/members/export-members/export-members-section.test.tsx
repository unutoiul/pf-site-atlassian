import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';

import AkButton from '@atlaskit/button';

import { createMockIntlContext } from '../../../utilities/testing';

import { ExportMembersModal } from './export-members-modal';
import { ExportMembersSection } from './export-members-section';

describe('Export Members Email Section', () => {

  function clickExportButton(section: ShallowWrapper): void {
    section.find(AkButton).simulate('click');
  }

  function isExportButtonVisible(section: ShallowWrapper): boolean {
    return !!section.find(AkButton).length;
  }

  function isExportModalRendered(section: ShallowWrapper): boolean {
    return !!section.find(ExportMembersModal).length;
  }

  function isExportModalOpen(section: ShallowWrapper): boolean {
    return !!section.find(ExportMembersModal).props().isOpen;
  }

  function createExportShallowWrapper(): ShallowWrapper {
    return shallow(
      <ExportMembersSection orgId={'coffee-org'} />,
      createMockIntlContext(),
    );
  }

  it('should render button', () => {
    const wrapper = createExportShallowWrapper();
    expect(isExportButtonVisible(wrapper)).to.equal(true);
    expect(isExportModalRendered(wrapper)).to.equal(true);
    expect(isExportModalOpen(wrapper)).to.equal(false);
  });

  it('should open modal on click', () => {
    const wrapper = createExportShallowWrapper();
    expect(isExportButtonVisible(wrapper)).to.equal(true);
    expect(isExportModalRendered(wrapper)).to.equal(true);
    expect(isExportModalOpen(wrapper)).to.equal(false);

    clickExportButton(wrapper);

    expect(isExportModalOpen(wrapper)).to.equal(true);
  });
});
