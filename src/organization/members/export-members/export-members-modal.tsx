import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';

import AkButton, { ButtonGroup as AkButtonGroup } from '@atlaskit/button';

import { ActionsBarModalFooter } from 'common/actions-bar';
import { analyticsClient as oldAnalyticsClient, AnalyticsClientProps, initiateManagedAcountsExportData, withAnalyticsClient } from 'common/analytics';
import { createErrorIcon, createSuccessIcon, errorFlagMessages } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';
import { ModalDialog } from 'common/modal';

import { ExportMembersModalQuery, ExportMembersMutation, ExportMembersMutationVariables } from '../../../schema/schema-types';
import exportMembersModalQuery from './export-members-modal.query.graphql';
import exportMembersMutation from './export-members.mutation.graphql';

const messages = defineMessages({
  title: {
    id: 'organization.export.members.email.modal.title',
    defaultMessage: 'Export all managed accounts',
    description: 'Title of a modal dialog that exports member account information to a CSV file',
  },
  description: {
    id: 'organization.export.members.email.modal.description',
    defaultMessage: 'An email will be sent to {emailAddress} once the export is ready to download.',
    description: 'Description of a modal dialog that exports member account information to a CSV file',
  },
  button: {
    id: 'organization.export.members.email.modal.button',
    defaultMessage: 'Export',
    description: 'Button of a modal dialog that exports member account information to a CSV file when clicked',
  },
  cancel: {
    id: 'organization.export.members.email.modal.cancel',
    defaultMessage: 'Cancel',
    description: 'Cancel button that will close a modal dialog and cancel CSV file export',
  },
  successFlagTitle: {
    id: 'organization.export.members.email.modal.flag.success.title',
    defaultMessage: 'Exporting accounts',
    description: 'Success notification title for exporting member accounts to a CSV file',
  },
  successFlagDescription: {
    id: 'organization.export.members.email.modal.flag.success.description',
    defaultMessage: `You'll be emailed a download link to the CSV file. This can take a few minutes if you have a large number of accounts.`,
    description: 'Success notification description for exporting member accounts to a CSV file',
  },
  errorFlagDescription: {
    id: 'organization.export.members.email.modal.flag.error.description',
    defaultMessage: 'Managed accounts could not be exported. Please try again later.',
    description: 'Failure notification description for when exporting member accounts to a CSV file fails',
  },
});

interface ExportMembersModalProp {
  orgId: string;
  isOpen: boolean;
  setModalOpenState(isOpen: boolean): void;
}

interface State {
  isLoading: boolean;
}

type AllProps = ChildProps<ExportMembersModalProp, ExportMembersModalQuery> & InjectedIntlProps & FlagProps & AnalyticsClientProps;

export class ExportMembersModalImpl extends React.Component<AllProps, State> {

  public readonly state: Readonly<State> = {
    isLoading: false,
  };

  public render() {
    if (!this.props.data || this.props.data.loading || this.props.data.error || !this.props.data.currentUser) {
      return null;
    }
    const { isOpen, data: { currentUser: { email } } } = this.props;

    return (
      <ModalDialog
        width="small"
        header={this.props.intl.formatMessage(messages.title)}
        isOpen={isOpen}
        footer={this.generateModalFooter()}
        onClose={this.closeModal}
      >
        <FormattedMessage {...messages.description} values={{ emailAddress: email }} />
      </ModalDialog>
    );
  }

  private generateModalFooter = (): JSX.Element => {
    return (
      <ActionsBarModalFooter alignment="right">
        <AkButtonGroup>
          <AkButton
            isLoading={this.state.isLoading}
            appearance="primary"
            onClick={this.startExport}
          >
            <FormattedMessage {...messages.button} />
          </AkButton>
          <AkButton onClick={this.closeModal} appearance="subtle-link">
            <FormattedMessage {...messages.cancel} />
          </AkButton>
        </AkButtonGroup>
      </ActionsBarModalFooter>
    );
  }

  private startExport = () => {
    const { mutate, orgId, analyticsClient } = this.props;
    if (!mutate || !orgId) {
      return;
    }

    this.setState({ isLoading: true });

    analyticsClient.sendUIEvent({ data: initiateManagedAcountsExportData() });

    mutate({ variables: { orgId } })
      .then(() => {
        this.generateSuccessFlag();
        this.closeModal();
      })
      .catch((error) => {
        oldAnalyticsClient.onError(error);
        this.generateErrorFlag();
        this.closeModal();
      })
    ;
  }

  private closeModal = () => {
    this.setState({ isLoading: false });
    this.props.setModalOpenState(false);
  }

  private generateSuccessFlag = (): void => {
    const { formatMessage } = this.props.intl;
    this.props.showFlag({
      autoDismiss: true,
      icon: createSuccessIcon(),
      id: `organization.export.members.email.success.flag.${Date.now()}`,
      title: formatMessage(messages.successFlagTitle),
      description: formatMessage(messages.successFlagDescription),
    });
  }

  private generateErrorFlag = (): void => {
    const { formatMessage } = this.props.intl;
    this.props.showFlag({
      autoDismiss: true,
      icon: createErrorIcon(),
      id: `organization.export.members.email.error.flag.${Date.now()}`,
      title: formatMessage(errorFlagMessages.title),
      description: formatMessage(messages.errorFlagDescription),
    });
  }
}

const withExportMembersModalQuery = graphql<ExportMembersModalProp, ExportMembersModalQuery>(exportMembersModalQuery);

const withExportMembersMutation = graphql<ExportMembersModalProp, ExportMembersMutation, ExportMembersMutationVariables>(exportMembersMutation);

export const ExportMembersModal =
  withExportMembersMutation(
    withExportMembersModalQuery(
      withFlag(
        injectIntl(
          withAnalyticsClient(
            ExportMembersModalImpl,
          ),
        ),
      ),
    ),
  )
;
