import * as React from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';

import AkButton from '@atlaskit/button';

import { ExportMembersModal } from './export-members-modal';

const messages = defineMessages({
  exportButton: {
    id: 'organization.export.members.email.button',
    defaultMessage: 'Export accounts',
    description: 'text in a button that will open a dialog to export member account information to a CSV file',
  },
});

interface State {
  isModalOpen: boolean;
}

interface ExportMembersSectionProps {
  orgId: string;
}

export class ExportMembersSection extends React.Component<ExportMembersSectionProps, State> {

  public readonly state: Readonly<State> = {
    isModalOpen: false,
  };

  public render() {

    return (
      <div>
        <AkButton onClick={this.openModal}>
          <FormattedMessage {...messages.exportButton} />
        </AkButton>
        <ExportMembersModal
          orgId={this.props.orgId}
          isOpen={this.state.isModalOpen}
          setModalOpenState={this.setExportModalOpenState}
        />
      </div>
    );
  }

  private openModal = () => {
    this.setExportModalOpenState(true);
  }

  private setExportModalOpenState = (isOpen: boolean): void => {
    this.setState({ isModalOpen: isOpen });
  }
}
