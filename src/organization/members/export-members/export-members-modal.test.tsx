import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import AkButton from '@atlaskit/button';

import { ModalDialog } from 'common/modal';

import { createMockIntlContext, createMockIntlProp } from '../../../utilities/testing';
import { ExportMembersModalImpl } from './export-members-modal';

describe('Export Members Email Modal', () => {

  let setModalOpenState;
  let showFlag;
  let mutateStub;
  let sendUIEventSpy;
  const mockData = {
    loading: false,
    currentUser: {
      email: 'test@atlassian.com',
    },
  };
  const successfulMutation = Promise.resolve({
    data: { startMemberCsvExport: true },
  });
  // tslint:disable-next-line:no-empty
  const noop = () => {};

  const sandbox = sinon.sandbox.create();

  beforeEach(() => {
    setModalOpenState = sandbox.spy();
    showFlag = sandbox.spy();
    sendUIEventSpy = sandbox.spy();
    mutateStub = sandbox.stub().returns(successfulMutation);
  });

  afterEach(() => {
    sandbox.restore();
  });

  function clickCancelButton(section: ShallowWrapper): void {
    section.find(AkButton).last().simulate('click');
  }

  function clickExportButton(section: ShallowWrapper): void {
    section.find(AkButton).first().simulate('click');
  }

  function areButtonsVisible(section: ShallowWrapper): boolean {
    return section.find(AkButton).length === 2;
  }

  function createExportModal(section: ShallowWrapper): ShallowWrapper {
    return shallow((section.find(ModalDialog).props()).footer as any);
  }

  function createExportShallowWrapper(data): ShallowWrapper {
    return shallow(
      <ExportMembersModalImpl
        orgId={'coffee-org'}
        setModalOpenState={setModalOpenState}
        isOpen={true}
        hideFlag={null as any}
        showFlag={showFlag}
        intl={createMockIntlProp()}
        data={data}
        mutate={mutateStub}
        analyticsClient={{ init: noop, sendScreenEvent: noop, sendUIEvent: sendUIEventSpy, sendTrackEvent: noop }}
      />,
      createMockIntlContext(),
    );
  }

  it('Should contain two buttons', () => {
    const wrapper = createExportShallowWrapper(mockData);
    const modal = createExportModal(wrapper);

    expect(areButtonsVisible(modal)).to.equal(true);
  });

  it('Cancel should toggle modal', () => {
    const wrapper = createExportShallowWrapper(mockData);
    const modal = createExportModal(wrapper);

    clickCancelButton(modal);

    expect(setModalOpenState.called).to.equal(true);
  });

  it('Clicking export should perform action', async () => {
    const wrapper = createExportShallowWrapper(mockData);
    const modal = createExportModal(wrapper);

    clickExportButton(modal);
    await successfulMutation;

    expect(setModalOpenState.called).to.equal(true);
    expect(showFlag.called).to.equal(true);
  });

  it('Clicking export should send UI event', async () => {
    const wrapper = createExportShallowWrapper(mockData);
    const modal = createExportModal(wrapper);

    clickExportButton(modal);
    expect(sendUIEventSpy.called).to.equal(true);
  });
});
