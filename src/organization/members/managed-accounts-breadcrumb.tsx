import * as React from 'react';
import {
  defineMessages,
  FormattedMessage,
} from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';

import { Breadcrumb } from 'common/breadcrumb';

import { OrgRouteProps } from '../../organization/routes';
import { createOrgAnalyticsData } from '../organizations/organization-analytics';

const messages = defineMessages({
  text: {
    id: 'managed.accounts.breadcrumb.text',
    defaultMessage: 'Managed accounts',
  },
});

interface OwnProps {
  hasSeparator?: boolean;
}

type ManagedAccountsBreadcrumbProps = OwnProps & RouteComponentProps<OrgRouteProps>;

export class ManagedAccountsBreadcrumbImpl extends React.Component<ManagedAccountsBreadcrumbProps> {
  public render() {
    const {
      match: {
        params: {
          orgId = null,
        } = {},
      } = {},
      hasSeparator,
    } = this.props;

    if (!orgId) {
      return null;
    }

    return (
      <Breadcrumb
        href={`/o/${orgId}/members`}
        text={<FormattedMessage {...messages.text} />}
        hasSeparator={hasSeparator}
        analyticsData={createOrgAnalyticsData({
          action: 'click',
          actionSubject: 'orgBreadcrumb',
          actionSubjectId: 'managedAccounts',
        })}
      />
    );
  }
}

export const ManagedAccountsBreadcrumb = withRouter<OwnProps>(ManagedAccountsBreadcrumbImpl);
