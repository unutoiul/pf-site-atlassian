// tslint:disable:restrict-plus-operands
import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import { defineMessages, FormattedMessage } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';
import styled from 'styled-components';

import {
  colors as akColors,
  fontSize as akFontSize,
  gridSize as akGridSize,
} from '@atlaskit/theme';

import {
  AnalyticsClientProps,
  downloadManagedAccountsCsvData,
  managedAccountsDownloadErrorScreenData,
  managedAccountsDownloadExpiredScreenData,
  managedAccountsDownloadScreenData,
  ScreenEventSender,
  withAnalyticsClient,
} from 'common/analytics';
import { GenericErrorMessage, GoneError } from 'common/error';
import { downloadImage, errorImage } from 'common/images';
import { CenteredPageLayout } from 'common/page-layout';

import { MemberExportDownloadPageQuery } from '../../schema/schema-types/document-types';
import memberExportDownloadPageQuery from './member-export-download-page.query.graphql';

const messages = defineMessages({
  titlePreparing: {
    id: 'organization.memberExportDownload.title.preparing',
    defaultMessage: 'Preparing your download...',
    description: 'Title for a page that is preparing a file to downloads',
  },
  titleReady: {
    id: 'organization.memberExportDownload.title.ready',
    defaultMessage: 'Download ready',
    description: 'Title for a page that downloads a file',
  },
  descriptionLink: {
    id: 'organization.memberExportDownload.description.link',
    defaultMessage: 'Click here',
    description: `Link text that will be inserted into a organization.memberExportDownload.description, full text is "Click here if your file doesn't start downloading"`,
  },
  description: {
    id: 'organization.memberExportDownload.description',
    defaultMessage: `{link} if your file doesn't start downloading.`,
    description: 'Description for a sentance about downloading a file, {link} variable is replaced with organization.memberExportDownload.description.link',
  },
  downloadExpiredTitle: {
    id: 'organization.memberExportDownload.downloadExpiredTitle',
    defaultMessage: 'The link to your download has expired',
    description: 'Explanation title telling user that a file is no downloadable because it has expired',
  },
  downloadExpiredDescription: {
    id: 'organization.memberExportDownload.downloadExpiredDescription',
    defaultMessage: 'Create a new export to download the file. ',
    description: 'Explanation description telling user that a file is no downloadable because it has expired',
  },
});

const Container = styled.div`
  text-align: center;
  margin-top: ${akGridSize() * 3}px;
  img {
    height: ${akGridSize() * 20}px;
  }
`;

const Title = styled.div`
  margin-top: ${akGridSize() * 4}px;
  margin-bottom: ${akGridSize() * 2}px;
  color: ${akColors.N800};
  h1 {
    font-size: ${akFontSize() + 6}px;
  }
`;

const Description = styled.div`
  color: ${akColors.N600};
  font-size: ${akFontSize}px;
`;

export interface MemberExportDownloadRouteProps {
  orgId: string;
  exportId: string;
}

const getMemberDownloadPageContent = (
  image: string,
  TitleMessage: React.ReactElement<FormattedMessage>,
  DescriptionMessage?: React.ReactElement<FormattedMessage>,
) => {
  return (
    <Container>
      <img src={image} role="presentation" alt={undefined} />
      <Title>
        {TitleMessage}
      </Title>
      <Description>
        {DescriptionMessage}
      </Description>
    </Container>
  );
};

interface State {
  autoDownloadStarted: boolean;
  downloadedData?: Blob;
}

export class MemberExportDownloadPageImpl extends React.Component<ChildProps<RouteComponentProps<MemberExportDownloadRouteProps> & AnalyticsClientProps, MemberExportDownloadPageQuery>, State> {

  public readonly state: Readonly<State> = {
    autoDownloadStarted: false,
  };

  private ref?: HTMLAnchorElement;

  public componentDidMount() {
    this.updateDownloadedDataState();
  }

  public componentDidUpdate() {
    if (this.ref && !this.state.autoDownloadStarted) {
      this.setState({ autoDownloadStarted: true });
      this.ref.click();
    }
    this.updateDownloadedDataState();
  }

  public render() {
    const { data } = this.props;

    if (!data) {
      return null;
    }

    return (
      <CenteredPageLayout collapsedNav={true}>
        {data.loading ?
          getMemberDownloadPageContent(downloadImage, <FormattedMessage {...messages.titlePreparing} tagName="h1" />) :
          data.error ?
          this.getErrorContent() :
          this.getContent()
        }
      </CenteredPageLayout>
    );
  }

  public getContent() {
    const { downloadedData } = this.state;

    if (!downloadedData) {
      return null;
    }

    const link = (
      <a
        ref={this.setRef}
        onClick={this.onDownloadClick}
        href={window.URL.createObjectURL(downloadedData)}
        download="export.csv"
      >
        <FormattedMessage {...messages.descriptionLink} />
      </a>
    );

    return (
      <ScreenEventSender event={managedAccountsDownloadScreenData()} >
        {getMemberDownloadPageContent(
          downloadImage,
          <FormattedMessage {...messages.titleReady} tagName="h1" />,
          <FormattedMessage {...messages.description} values={{ link }} tagName="p" />,
        )}
      </ScreenEventSender>
    );
  }

  public getErrorContent() {
    if (GoneError.findIn(this.props.data!.error!).length > 0) {
      return (
        <ScreenEventSender event={managedAccountsDownloadExpiredScreenData()} >
          {getMemberDownloadPageContent(
            errorImage,
            <FormattedMessage {...messages.downloadExpiredTitle} tagName="h1" />,
            <FormattedMessage {...messages.downloadExpiredDescription} tagName="p" />,
          )}
        </ScreenEventSender>
      );
    } else {
      return (
        <ScreenEventSender event={managedAccountsDownloadErrorScreenData()} >
          {getMemberDownloadPageContent(
            errorImage,
            <h1><GenericErrorMessage /></h1>,
          )}
        </ScreenEventSender>
      );
    }
  }

  private updateDownloadedDataState = () => {
    const { data } = this.props;

    if (data && data.organization && data.organization.memberExport && data.organization.memberExport.content && !this.state.downloadedData) {
      const downloadedData = new Blob([this.props.data!.organization!.memberExport.content], { type: 'text/csv' });
      this.setState({ downloadedData });
    }
  }

  private setRef = (anchor) => {
    if (anchor) {
      this.ref = anchor;
    }
  }

  private onDownloadClick = () => {
    this.props.analyticsClient.sendUIEvent({ data: downloadManagedAccountsCsvData() });

    // hack for IE11 since a download is not supported
    if (window.navigator.msSaveOrOpenBlob && this.state.downloadedData) {
      window.navigator.msSaveOrOpenBlob(this.state.downloadedData, 'export.csv');
    }
  }
}

const withMemberExportDownloadPageQuery = graphql<RouteComponentProps<MemberExportDownloadRouteProps>, MemberExportDownloadPageQuery>(memberExportDownloadPageQuery, {
  options: (props) => ({ variables: { id: props.match.params.orgId, exportId: props.match.params.exportId } }),
});

export const MemberExportDownloadPage =
  withRouter(
    withMemberExportDownloadPageQuery(
      withAnalyticsClient(
        MemberExportDownloadPageImpl,
      ),
    ),
  )
;
