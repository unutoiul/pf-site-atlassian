import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import { ScreenEventSender } from 'common/analytics';
import { GoneError, RequestError } from 'common/error';

import { createMockContext } from '../../utilities/testing';
import { MemberExportDownloadPageImpl } from './member-export-download-page';

describe('MemberExportDownloadPage', () => {
  const sandbox = sinon.sandbox.create();
  // tslint:disable-next-line:no-empty
  const noop = () => {};
  let sendScreenEventSpy;
  let sendUIEventSpy;
  const org = { organization: { memberExport: { content: 'test1234,test5432' } }, loading: false };

  beforeEach(() => {
    sendScreenEventSpy = sandbox.spy();
    sendUIEventSpy = sandbox.spy();
    sandbox.stub(window.URL, 'createObjectURL');
    window.URL.createObjectURL = () => 'test.com';
  });

  afterEach(() => {
    sandbox.restore();
  });

  const getWrapper = (data?) => {
    return mount((
      <MemberExportDownloadPageImpl
        data={data}
        analyticsClient={{ init: noop, sendScreenEvent: sendScreenEventSpy, sendUIEvent: sendUIEventSpy, sendTrackEvent: noop }}
        location={null as any}
        history={null as any}
        match={{ params: { orgId: 'xyz123', exportId: '321zyx' }, isExact: true, path: '', url: '' }}
      />
    ), createMockContext({ intl: true, apollo: true }));
  };

  it('should render link', () => {
    const wrapper = getWrapper();
    wrapper.setProps({ data: org });
    wrapper.update();

    const downloadLink = wrapper.find('a');
    expect(downloadLink.exists()).to.equal(true);
    expect(downloadLink.prop('href')).to.contain('test.com');
  });

  it('should send correct props to screen event sender', () => {
    const wrapper = getWrapper();
    wrapper.setProps({ data: org });
    wrapper.update();

    expect(wrapper.find(ScreenEventSender).prop('event')).to.deep.equal({
      data: {
        name: 'managedAccountsDownloadScreen',
      },
    });
  });

  it('should send ui event on link click', () => {
    const wrapper = getWrapper();
    wrapper.setProps({ data: org });
    wrapper.update();

    const downloadLink = wrapper.find('a');
    expect(downloadLink.exists()).to.equal(true);
    downloadLink.simulate('click');
    expect(sendUIEventSpy.called).to.equal(true);
  });

  describe('download expired', () => {
    it('should send expired screen event', () => {
      const wrapper = getWrapper();
      wrapper.setProps({ data: { error: { graphQLErrors: [{ originalError: new GoneError({}) }] } } });
      wrapper.update();

      expect(wrapper.find(ScreenEventSender).prop('event')).to.deep.equal({
        data: {
          name: 'managedAccountsDownloadExpiredScreen',
        },
      });
    });
  });

  describe('download error', () => {
    it('should send expired screen event', () => {
      const wrapper = getWrapper();
      wrapper.setProps({ data: { error: { graphQLErrors: [{ originalError: new RequestError({}) }] } } });
      wrapper.update();

      expect(wrapper.find(ScreenEventSender).prop('event')).to.deep.equal({
        data: {
          name: 'managedAccountsDownloadErrorScreen',
        },
      });
    });
  });
});
