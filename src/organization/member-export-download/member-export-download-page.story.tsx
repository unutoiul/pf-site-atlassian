import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';
import { MemoryRouter } from 'react-router-dom';

import AkPage from '@atlaskit/page';

import { GoneError } from 'common/error';

import { createApolloClient } from '../../apollo-client';
import { MemberExportDownloadPageImpl } from './member-export-download-page';

const client = createApolloClient();

const getMemberExportDownloadPage = ({ loading, error }) => {

  return (
    <ApolloProvider client={client}>
      <IntlProvider locale="en">
        <MemoryRouter>
          <AkPage>
            <MemberExportDownloadPageImpl
              data={{ organization: { id: 'Hamburger', memberExport: { id: '12354', content: 'lalalala' } }, error, loading } as any}
              analyticsClient={{ sendUIEvent: () => undefined } as any}
              match={{ params: { orgId: 'Hamburger', exportId: '54321' } } as any}
              history={{} as any}
              location={{} as any}
            />
          </AkPage>
        </MemoryRouter>
      </IntlProvider>
    </ApolloProvider>
  );
};

storiesOf('Organization|Member Export Download', module)
  .add('Default', () => {
    return getMemberExportDownloadPage({ loading: false, error: false });
  })
  .add('Loading', () => {
    return getMemberExportDownloadPage({ loading: true, error: false });
  })
  .add('Expired link', () => {
    return getMemberExportDownloadPage({ loading: false, error: { graphQLErrors: [{ originalError: new GoneError({ message: 'Link expired' }) }] } });
  })
  .add('Error', () => {
    return getMemberExportDownloadPage({ loading: false, error: true });
  })
;
