import * as React from 'react';
import styled from 'styled-components';

import AkTextField from '@atlaskit/field-text';

import AkSpinner from '@atlaskit/spinner';
import { gridSize as akGridSize } from '@atlaskit/theme';

import { Button as AnalyticsButton } from 'common/analytics';
import { ButtonGroup as ButtonGroupBase } from 'common/button-group';

const ButtonGroup = styled(ButtonGroupBase)`
  margin-top: ${akGridSize() * 5}px;
`;

const LoadingStatusWrapper = styled.div`
  margin-top: ${akGridSize() * 5}px;
  display: inline-flex;
  align-items: center;
`;

const LoadingText = styled.div`
  margin-left: ${akGridSize() * 1}px;
`;

interface CreateOrganizationFormProps {
  isLoading: boolean;
  nameLabel: string;
  createButtonText: string;
  cancelButtonText: string;
  loadingMessageText: string;
  placeholderText: string;
  onSubmit(orgName: string): void;
  onCancel(): void;
}

interface CreateOrganizationFormState {
  orgName: string;
}

export class CreateOrganizationForm extends React.Component<CreateOrganizationFormProps, CreateOrganizationFormState> {

  public state: Readonly<CreateOrganizationFormState> = {
    orgName: '',
  };

  public render() {
    const {
      isLoading,
      nameLabel,
      onCancel,
      createButtonText,
      cancelButtonText,
      loadingMessageText,
      placeholderText,
    } = this.props;

    return (
      <form onSubmit={this.onSubmit}>
        <AkTextField
          placeholder={placeholderText}
          onChange={this.onChange}
          label={nameLabel}
          required={true}
          disabled={isLoading}
          shouldFitContainer={true}
          id={'create-organization-name'}
        />
        {isLoading
          ? renderLoadingMessage({ loadingMessageText })
          : (
            <ButtonGroup alignment="left">
              <AnalyticsButton
                appearance="primary"
                type="submit"
                isDisabled={!this.canSubmitForm()}
              >
                {createButtonText}
              </AnalyticsButton>
              <AnalyticsButton
                appearance="subtle-link"
                onClick={onCancel}
              >
                {cancelButtonText}
              </AnalyticsButton>
            </ButtonGroup>
          )
        }
      </form>
    );
  }

  private onChange = (e: React.SyntheticEvent<HTMLInputElement>) => {
    const orgName = e.currentTarget.value;
    this.setState({
      orgName,
    });
  }

  private onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (!this.canSubmitForm()) {
      return;
    }
    this.props.onSubmit(this.state.orgName);
  }

  private canSubmitForm() {
    return this.state.orgName.length > 0;
  }
}

const renderLoadingMessage = ({ loadingMessageText }) => {
  return (
    <LoadingStatusWrapper>
      <AkSpinner size={'small'} />
      <LoadingText>
        {loadingMessageText}
      </LoadingText>
    </LoadingStatusWrapper>
  );
};
