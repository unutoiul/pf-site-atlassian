import * as React from 'react';
import { ChildProps, compose, graphql } from 'react-apollo';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';
import styled from 'styled-components';

import AkSpinner from '@atlaskit/spinner';
import { gridSize as akGridSize } from '@atlaskit/theme';

import {
  AnalyticsClientProps,
  cancelOrgCreationButtonClickedEvent,
  confirmOrgCreationButtonClickedEvent,
  createOrgScreen,
  createOrgScreenEvent,
  FocusedTaskUIEventMeta,
  orgCreatedTrackEvent,
  ScreenEventSender,
  withAnalyticsClient,
} from 'common/analytics';
import { createErrorIcon, createSuccessIcon, GenericError } from 'common/error';
import { FlagDescriptor, FlagProps, withFlag } from 'common/flag';
import { FocusedTask } from 'common/focused-task';
import { PermissionErrorPage } from 'common/permission-error-page';
import { withOrganizationsCount, WithOrgsCountProps, withSitesCount, WithSitesCountProps } from 'common/user';

import { CreateOrgMutation, CurrentUserQuery, PollCreateOrgQuery, PollCreateOrgQueryVariables } from '../../schema/schema-types';
import { CreateOrgSectionProps } from '../organizations/organization.prop-types';
import currentUserQuery from '../organizations/queries/current-user.query.graphql';
import pollCreateOrgQuery from '../organizations/queries/poll-create-org.query.graphql';
import createOrgMutation from './create-org.mutation.graphql';
import { CreateOrganizationForm } from './create-organization-form';
import { CreateOrganizationPersistedState, withCreateOrganizationPersistedState } from './with-create-organization-persisted-state';

export const messages = defineMessages({
  button: {
    id: 'organization.create.button',
    defaultMessage: 'Create',
  },
  errorTitle: {
    id: 'organization.create.error.title',
    defaultMessage: 'Create organization unsuccessful',
  },
  errorDescription: {
    id: 'organization.create.error.description',
    defaultMessage: 'There was an error trying to create the organization. Please check back in a few minutes.',
  },
  successDescription: {
    id: 'organization.create.success.description',
    defaultMessage: 'Now you\'ve got more options in the sidebar for configuring your organization.',
  },
  successTitle: {
    id: 'organization.create.success.title',
    defaultMessage: '{orgName} has been created',
  },
  createTitle: {
    id: 'organization.create.title',
    defaultMessage: 'Create organization',
  },
  createDescription: {
    id: 'organization.create.description',
    defaultMessage: 'Your organization represents your company and lets you manage users, security settings across multiple sites and products.',
  },
  permissionDenied: {
    id: 'organization.create.permission.denied',
    defaultMessage: 'This feature is not yet available.',
  },
  cancelLabel: {
    id: 'organization.create.cancel.label',
    defaultMessage: 'Cancel',
  },
  createLabel: {
    id: 'organization.create.create.label',
    defaultMessage: 'Create',
  },
  orgNameLabel: {
    id: 'organization.create.name.label',
    defaultMessage: 'Organization name',
  },
  createOrgLoadingMessage: {
    id: 'organization.create.loading.message',
    defaultMessage: 'Creating your organization',
  },
  placeholderText: {
    id: 'organization.create.placeholder',
    defaultMessage: 'Acme, Inc',
  },
});

const Page = styled.div`
  margin: 0 auto;
  padding-top: ${akGridSize() * 5}px;
  padding-bottom: ${akGridSize() * 2}px;
  width: ${akGridSize() * 50}px;
`;

const Description = styled.div`
  margin-top: ${akGridSize() * 1}px;
`;

interface CreateOrganizationPageState {
  isLoading: boolean;
  orgId: string | null;
  orgName: string;
}

const enum PageReadyState {
  Unknown,
  PermissionDenied,
  RedirectToLaunchpad,
  OK,
  Error,
}

// reusing one object here to prevent tree reconciliation
const uiEventMeta: FocusedTaskUIEventMeta = {
  data: {
    source: createOrgScreen,
  },
};

type OwnProps = CreateOrgSectionProps & FlagProps;
type AllProps = ChildProps<OwnProps, CreateOrgMutation & PollCreateOrgQuery> & InjectedIntlProps & RouteComponentProps<any> & WithSitesCountProps & WithOrgsCountProps & CreateOrganizationPersistedState & AnalyticsClientProps;

export class CreateOrganizationPageImpl extends React.Component<AllProps, CreateOrganizationPageState> {
  public state: CreateOrganizationPageState = {
    isLoading: false,
    orgId: null,
    orgName: '',
  };

  public render() {
    const { intl: { formatMessage } } = this.props;
    let pageContent;

    const pageReadyState = this.getReadyState(this.props);
    if (pageReadyState === PageReadyState.PermissionDenied) {
      pageContent = <PermissionErrorPage />;
    } else if (pageReadyState === PageReadyState.Unknown) {
      pageContent = <AkSpinner />;
    } else if (pageReadyState === PageReadyState.Error) {
      pageContent = <GenericError />;
    } else {
      pageContent = (
        <ScreenEventSender event={createOrgScreenEvent()}>
            <h1>{formatMessage(messages.createTitle)}</h1>
            <Description>
              {formatMessage(messages.createDescription)}
            </Description>
            <CreateOrganizationForm
              cancelButtonText={formatMessage(messages.cancelLabel)}
              createButtonText={formatMessage(messages.createLabel)}
              loadingMessageText={formatMessage(messages.createOrgLoadingMessage)}
              placeholderText={this.state.orgName || formatMessage(messages.placeholderText)}
              nameLabel={formatMessage(messages.orgNameLabel)}
              onCancel={this.onCancel}
              onSubmit={this.onSubmitForm}
              isLoading={this.state.isLoading}
            />
        </ScreenEventSender>
      );
    }

    return (
      <FocusedTask
        onClose={this.redirectToLaunchpad}
        isOpen={true}
        uiEvent={uiEventMeta}
      >
        <Page>
          {pageContent}
        </Page>
      </FocusedTask>
    );
  }

  public componentWillMount() {
    const pageReadyState = this.getReadyState(this.props);
    if (pageReadyState === PageReadyState.RedirectToLaunchpad) {
      this.redirectToLaunchpad();
    }
  }

  public componentDidMount() {
    const {
      progressUri,
      orgId,
      orgName,
    } = this.props;

    if (!progressUri || !orgName || !orgId) {
      return;
    }

    this.startCreateOrganization();
    this.setState({ orgName, orgId });
    this.pollProgressUri(progressUri);
  }

  public componentWillReceiveProps(nextProps: CreateOrganizationPageImpl['props']) {
    const pageReadyState = this.getReadyState(nextProps);
    if (pageReadyState === PageReadyState.RedirectToLaunchpad) {
      this.redirectToLaunchpad();
    }

    if (nextProps.data && nextProps.data.error) {
      nextProps.data.stopPolling();
      this.stopCreateOrganization();
    }

    if (this.pollingIsNotComplete(nextProps)) {
      return;
    }

    if (nextProps.data) {
      nextProps.data.stopPolling();
    }
    nextProps.currentOrgs.refetch().then(() => {
      this.completeCreateOrganization();
    }).catch(() => {
      this.createOrganizationFailure();
    });
  }

  private onCancel = () => {
    const {
      analyticsClient: { sendUIEvent },
    } = this.props;

    sendUIEvent(cancelOrgCreationButtonClickedEvent());

    this.redirectToLaunchpad();
  }

  private getReadyState = (props: CreateOrganizationPageImpl['props']) => {
    const { totalSites, totalSitesLoadError, totalOrganizations, totalOrganizationsLoadError, currentOrgs } = props;

    if (currentOrgs.error || totalSitesLoadError || totalOrganizationsLoadError) {
      return PageReadyState.Error;
    }

    if ((totalSites === undefined && totalOrganizations === undefined) ||
      (totalOrganizations === undefined)) {
      return PageReadyState.Unknown;
    }

    if (totalSites === 0 && totalOrganizations === 0) {
      return PageReadyState.PermissionDenied;
    }

    if (totalSites != null && totalSites > 0 && totalOrganizations === 0) {
      return PageReadyState.OK;
    }

    if (totalOrganizations > 0) {
      return PageReadyState.RedirectToLaunchpad;
    }

    return PageReadyState.Unknown;
  }

  private onSubmitForm = (orgName) => {
    const {
      analyticsClient: { sendUIEvent },
    } = this.props;

    sendUIEvent(confirmOrgCreationButtonClickedEvent());

    this.startCreateOrganization();
    this.createOrganization(orgName);
  }

  private startCreateOrganization = () => {
    this.setState({
      isLoading: true,
    });
  }

  private completeCreateOrganization() {
    const {
      clearOrgCreationState,
      showFlag,
      analyticsClient: { sendTrackEvent },
    } = this.props;

    clearOrgCreationState();
    sendTrackEvent(orgCreatedTrackEvent(this.state.orgId!));
    showFlag(this.createOrgSuccessFlag());
    this.redirectToOrgOverview();
  }

  private stopCreateOrganization() {
    this.setState({
      isLoading: false,
    });
    this.props.clearOrgCreationState();
  }

  private createOrganizationFailure() {
    this.setState({
      isLoading: false,
    });
    this.props.showFlag(this.createOrgFailureFlag());
    this.props.clearOrgCreationState();
  }

  private createOrganization(orgName) {
    if (!this.props.mutate) {
      return;
    }

    this.props.mutate({
      variables: { name: orgName },
    }).then((response) => {
      const { id, progressUri } = response.data.createOrganization;
      this.setState({
        orgId: id,
        orgName,
      });
      this.props.setOrgCreationState(progressUri, id, orgName);
      this.pollProgressUri(progressUri);
    }).catch(() => {
      this.createOrganizationFailure();
    });
  }

  private pollProgressUri(progressUri) {
    if (!this.props.data) {
      return;
    }

    const { refetch, startPolling } = this.props.data;

    refetch({ progressUri }).then(() => {
      // Used to query the COFS endpoint to determine whether our organization has been created.
      // 4000ms has been chosen as an arbitrary number. The query should return a completed state in
      // less than 20 seconds.
      startPolling(4000);
    }).catch(() => {
      this.createOrganizationFailure();
    });
  }

  private pollingIsNotComplete(nextProps: CreateOrganizationPageImpl['props']) {
    // We stop polling when the endpoint returns "completed" as true. We're 90% sure that the COFS endpoint
    // will always resolve this variable to true regardless of network errors. Investigate in KT-686
    // to determine if this is 100% the case.
    if (!this.props.data || !this.props.data.pollCreateOrg) {
      return true;
    }

    return (
      !nextProps.data ||
      !nextProps.data.pollCreateOrg ||
      !nextProps.data.pollCreateOrg.completed ||
      nextProps.data.pollCreateOrg.completed === this.props.data.pollCreateOrg.completed
    );
  }

  private redirectToLaunchpad = () => {
    this.props.history.replace('/');
  }

  private redirectToOrgOverview() {
    const { orgId } = this.state;
    this.props.history.replace(`/o/${orgId}/overview`);
  }

  private createOrgSuccessFlag = (): FlagDescriptor => {
    const { formatMessage } = this.props.intl;

    return {
      appearance: 'normal',
      autoDismiss: true,
      description: formatMessage(messages.successDescription),
      icon: createSuccessIcon(),
      id: 'organization.create.success.flag',
      title: formatMessage(messages.successTitle, { orgName: this.state.orgName }),
    };
  };

  private createOrgFailureFlag = (): FlagDescriptor => {
    const { formatMessage } = this.props.intl;

    return {
      appearance: 'normal',
      autoDismiss: true,
      description: formatMessage(messages.errorDescription),
      icon: createErrorIcon(),
      id: 'organization.create.failure.flag',
      title: formatMessage(messages.errorTitle),
    };
  };
}

interface ProgressUriProps {
  progressUri: string;
}
const currentUserOrganization = graphql<{}, CurrentUserQuery, {}, CreateOrgSectionProps>(currentUserQuery, { name: 'currentOrgs' });
const query = graphql<CreateOrgSectionProps, PollCreateOrgQuery, PollCreateOrgQueryVariables, ProgressUriProps & CreateOrgSectionProps>(
  pollCreateOrgQuery, {
    options: (props: CreateOrgSectionProps) => ({
      variables: { progressUri: props.progressUri },
    }),
  },
);
const mutation = graphql<{}, CreateOrgMutation, CreateOrgSectionProps>(createOrgMutation);

export const CreateOrganizationPage = compose(
  withRouter,
  query,
  currentUserOrganization,
  mutation,
)(
  withFlag(
    injectIntl(
      withOrganizationsCount(
        withSitesCount(
          withCreateOrganizationPersistedState(
            withAnalyticsClient(
              CreateOrganizationPageImpl,
            ),
          ),
        ),
      ),
    ),
  ),
);
