// tslint:disable react-this-binding-issue
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';
import { MemoryRouter } from 'react-router-dom';

import { FlagProvider } from 'common/flag';
import { sitesCountQuery } from 'common/user';

import { createApolloClient } from '../../apollo-client';
import currentUserQuery from '../organizations/queries/current-user.query.graphql';
import pollCreateOrgQuery from '../organizations/queries/poll-create-org.query.graphql';

import { CreateOrganizationPage } from './create-organization-page';

const client = createApolloClient();

client.writeQuery({
  query: currentUserQuery,
  data: {
    currentUser: {
      id: '1234',
      // __typename seems to be required for writeQuery to work correctly
      __typename: 'CurrentUser',
      organizations: [],
    },
  },
});

client.writeQuery({
  query: pollCreateOrgQuery,
  data: {
    pollCreateOrg: {
      __typename: 'PollCreateOrgState',
      completed: false,
      successful: false,
    },
  },
});

client.writeQuery({
  query: sitesCountQuery,
  data: {
    currentUser: {
      id: '1234',
      __typename: 'CurrentUser',
      sites: [
        {
          __typename: 'CurrentSite',
          id: '2',
        },
      ],
    },
  },
});

const CreateOrganizationPageWithProviders = () => (
  <MemoryRouter>
    <ApolloProvider client={client}>
      <IntlProvider locale="en">
        <FlagProvider>
          <CreateOrganizationPage />
        </FlagProvider>
      </IntlProvider>
    </ApolloProvider>
  </MemoryRouter>
);

storiesOf('Organization|Create Organization Page', module)
  .add('Default', () => (
    <CreateOrganizationPageWithProviders />
  ));
