import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { sandbox as sinonSandbox, SinonSandbox } from 'sinon';

import { sessionStorageUtils } from '../../utilities/sessionStorage';
import { CreateOrganizationPersistedState, withCreateOrganizationPersistedState } from './with-create-organization-persisted-state';

describe('withCreateOrganizationPersistedState', () => {
  class TestComponent extends React.Component<CreateOrganizationPersistedState> {
    public render() {
      return null;
    }
  }
  const Decorated = withCreateOrganizationPersistedState<{}>(TestComponent);

  let sandbox: SinonSandbox;

  beforeEach(() => {
    sandbox = sinonSandbox.create();
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('should pass the interface members down to the wrapped component', () => {
    sandbox.stub(sessionStorageUtils, 'getItem');

    const result = shallow(<Decorated />);

    const testObj: CreateOrganizationPersistedState = {
      clearOrgCreationState: () => null,
      orgId: undefined,
      orgName: undefined,
      progressUri: undefined,
      setOrgCreationState: () => null,
    };

    const expectedKeys = Object.keys(testObj);
    const actualKeys = Object.keys(result.props());

    expect(expectedKeys.length).to.equal(5, 'just ensuring test sanity');
    expectedKeys.forEach(expectedKey => {
      expect(actualKeys).to.include(expectedKey);
    });
  });

  it('should get the state from session storage item with "org-creation-state" key', () => {
    const stub = sandbox.stub(sessionStorageUtils, 'getItem');

    shallow(<Decorated />);

    expect(stub.callCount).to.equal(1);
    expect(stub.getCalls()[0].args[0]).to.equal('org-creation-state');
  });

  it('should get the state from session storage', () => {
    sandbox.stub(sessionStorageUtils, 'getItem').returns(JSON.stringify({
      progressUri: 'test-uri',
      orgId: 'test-org-id',
      orgName: 'test-org-name',
    }));

    const result = shallow(<Decorated />).find(TestComponent);

    expect(result.prop('orgId')).to.equal('test-org-id');
    expect(result.prop('orgName')).to.equal('test-org-name');
    expect(result.prop('progressUri')).to.equal('test-uri');
  });

  it('should handle absent state correctly', () => {
    sandbox.stub(sessionStorageUtils, 'getItem').returns(null);

    const result = shallow(<Decorated />).find(TestComponent);

    expect(result.prop('orgId')).to.be.undefined();
    expect(result.prop('orgName')).to.be.undefined();
    expect(result.prop('progressUri')).to.be.undefined();
  });

  it('should persist state to session storage when "setOrgCreationState" is invoked', () => {
    sandbox.stub(sessionStorageUtils, 'getItem').returns(null);
    const stub = sandbox.stub(sessionStorageUtils, 'setItem');

    const setOrgCreationState = shallow(<Decorated />).find(TestComponent).prop('setOrgCreationState');
    setOrgCreationState('test-uri', 'test-id', 'test-name');

    expect(stub.callCount).to.equal(1);
    expect(stub.getCalls()[0].args).to.deep.equal([
      'org-creation-state',
      JSON.stringify({
        orgId: 'test-id',
        orgName: 'test-name',
        progressUri: 'test-uri',
      }),
    ]);
  });

  it('should clear state session storage when "setOrgCreationState" is invoked', () => {
    sandbox.stub(sessionStorageUtils, 'getItem').returns(null);
    const stub = sandbox.stub(sessionStorageUtils, 'removeItem');

    const clearOrgCreationState = shallow(<Decorated />).find(TestComponent).prop('clearOrgCreationState');
    clearOrgCreationState();

    expect(stub.callCount).to.equal(1);
    expect(stub.getCalls()[0].args[0]).to.equal('org-creation-state');
  });
});
