import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import * as React from 'react';
import { MemoryRouter } from 'react-router-dom';
import * as sinon from 'sinon';

import { GenericError } from 'common/error';

import { PermissionErrorPage } from 'common/permission-error-page';

import { createMockAnalyticsClient, createMockContext, createMockIntlProp, waitUntil } from '../../utilities/testing';
import { CreateOrganizationPageImpl } from './create-organization-page';

describe('Create Organization Page', () => {
  let stopPollingSpy: sinon.SinonSpy;
  let showFlagSpy: sinon.SinonSpy;
  let mutateMock;
  let historyMock;
  let stateUpdateSpy: sinon.SinonSpy;
  let refetchSpy: sinon.SinonSpy;

  const sandbox = sinon.sandbox.create();

  beforeEach(() => {
    sandbox.stub(console, 'error');
    stopPollingSpy = sandbox.spy();
    mutateMock = () => ({
      then: () => ({
        catch: () => ({}),
      }),
    });
    stateUpdateSpy = sandbox.spy();
    showFlagSpy = sandbox.spy();
    historyMock = {
      replace: sandbox.spy(),
      location: {
        search: '',
      },
    };
    refetchSpy = sandbox.spy(() => ({
      then: (cb) => {
        cb();

        return {
          catch: () => null,
        };
      }, catch: () => ({}),
    }));
  });

  afterEach(() => {
    sandbox.restore();
  });

  const defaultCurrentOrgs = () => ({
    loading: false, currentUser: { organizations: [] },
    refetch: refetchSpy,
  });

  const createOrganizationPage = ({ data = {}, currentOrgs = null } = {}) => {
    const wrapper = shallow(
      <CreateOrganizationPageImpl
        currentOrgs={currentOrgs ? { ...currentOrgs as any } : { ...defaultCurrentOrgs() }}
        match={{ params: {} } as any}
        location={{} as any}
        showFlag={showFlagSpy}
        data={data as any}
        history={historyMock}
        mutate={mutateMock}
        intl={createMockIntlProp()}
        totalSites={1}
        totalOrganizations={0}
        clearOrgCreationState={sinon.spy()}
        totalOrganizationsLoadError={undefined}
        totalSitesLoadError={undefined}
        hideFlag={sinon.spy()}
        setOrgCreationState={sinon.spy()}
        analyticsClient={createMockAnalyticsClient()}
      />,
      createMockContext({ intl: true, apollo: true }),
    );

    return {
      getWrapper: () => wrapper,
      historyMock,
    };
  };

  const createOrganizationPageMount = ({
    data = {} as any,
    currentOrgs = null as any,
    totalSites = 1,
    totalOrganizations = 0,
    mutate = mutateMock,
    history = historyMock,
    orgId,
    orgName,
    progressUri,
  }: Partial<CreateOrganizationPageImpl['props']> = {}) => {
    const clearOrgCreationStateSpy = sinon.spy();
    const wrapper = mount(
      <MemoryRouter>
        <CreateOrganizationPageImpl
          currentOrgs={currentOrgs ? currentOrgs : { ...defaultCurrentOrgs() as any }}
          showFlag={showFlagSpy}
          data={data}
          mutate={mutate}
          history={history}
          intl={createMockIntlProp()}
          totalSites={totalSites === -1 ? undefined : totalSites}
          totalOrganizations={totalOrganizations === -1 ? undefined : totalOrganizations}
          hideFlag={null as any}
          location={{} as any}
          match={{} as any}
          totalOrganizationsLoadError={undefined}
          totalSitesLoadError={undefined}
          setOrgCreationState={stateUpdateSpy}
          orgId={orgId}
          orgName={orgName}
          progressUri={progressUri}
          clearOrgCreationState={clearOrgCreationStateSpy}
          analyticsClient={createMockAnalyticsClient()}
        />
      </MemoryRouter>,
      createMockContext({ intl: true, apollo: true }),
    );
    const createOrgPage = wrapper.find(CreateOrganizationPageImpl).at(0).instance() as any;
    const startCreateOrganizationSpy = sinon.spy(createOrgPage, 'startCreateOrganization');
    const completeCreateOrganizationSpy = sinon.spy(createOrgPage, 'completeCreateOrganization');
    const submitFormSpy = sinon.spy(createOrgPage, 'onSubmitForm');

    return {
      clearOrgCreationStateSpy,
      historyMock,
      getOrgPage: () => {
        return {
          createOrgPage,
          startCreateOrganizationSpy,
          completeCreateOrganizationSpy,
          submitFormSpy,
          startCreateOrganization: () => {
            createOrgPage.startCreateOrganization();
            wrapper.update();
          },
          stopCreateOrganization: () => {
            createOrgPage.stopCreateOrganization();
            wrapper.update();
          },
          completeCreateOrganization: () => {
            createOrgPage.completeCreateOrganization();
            wrapper.update();
          },
          createOrganizationFailure: () => {
            createOrgPage.createOrganizationFailure();
            wrapper.update();
          },
          submitForm: (orgNameValue = 'test') => {
            createOrgPage.onSubmitForm(orgNameValue);
            wrapper.update();
          },
        };
      },
      getOrgNameInput: () => wrapper.find('FieldText').at(0).instance() as any,
      getWrapper: () => wrapper,
      getForm: () => ({
        simulateSubmit: () => {
          const form = wrapper.find('form').at(0) as any;
          wrapper.instance().forceUpdate();

          const organizationForm = wrapper.find('CreateOrganizationForm').at(0);
          organizationForm.instance().setState({
            orgName: 'test',
          });
          organizationForm.instance().forceUpdate();

          form.simulate('submit', {
            preventDefault: () => ({}),
            target: { name: {} },
          });
        },
      }),
    };
  };

  describe('Render', () => {
    it('should render the create organization page', () => {
      const wrapper = createOrganizationPageMount();

      expect(wrapper.getOrgPage()).to.not.equal(null);
    });

    it('should render spinner if sites and orgs counts not loaded', () => {
      const sitesAndOrgs = {
        totalSites: -1,
        totalOrganizations: -1,
      };
      const wrapper = createOrganizationPageMount(sitesAndOrgs);

      const spinner = wrapper.getWrapper().find('Spinner');
      expect(spinner).to.have.length(1);
    });

    it('should render spinner if sites counts not loaded', () => {
      const sitesAndOrgs = {
        totalSites: -1,
        totalOrganizations: 0,
      };
      const wrapper = createOrganizationPageMount(sitesAndOrgs);

      const spinner = wrapper.getWrapper().find('Spinner');
      expect(spinner).to.have.length(1);
    });

    it('should render permission denied when user has no orgs or sites', () => {
      const sitesAndOrgs = {
        totalSites: 0,
        totalOrganizations: 0,
      };
      const wrapper = createOrganizationPageMount(sitesAndOrgs);

      const spinner = wrapper.getWrapper().find('Spinner');
      expect(spinner).to.have.length(0);

      const permissionError = wrapper.getWrapper().find<any>(PermissionErrorPage);
      expect(permissionError).to.have.length(1);
    });

    it('should render error when user has error', () => {
      const sitesAndOrgs = {
        currentOrgs: {
          error: true,
        },
      };
      const wrapper = createOrganizationPageMount(sitesAndOrgs as any);

      const spinner = wrapper.getWrapper().find('Spinner');
      expect(spinner).to.have.length(0);

      const genericError = wrapper.getWrapper().find<any>(GenericError);
      expect(genericError).to.have.length(1);
    });

    it('should redirect to landing page when user has an org but no site', async () => {
      const sitesAndOrgs = {
        totalSites: 0,
        totalOrganizations: 1,
      };
      const wrapper = createOrganizationPageMount(sitesAndOrgs);

      const spinner = wrapper.getWrapper().find('Spinner');
      expect(spinner).to.have.length(0);

      const permissionError = wrapper.getWrapper().find<any>(PermissionErrorPage);
      expect(permissionError).to.have.length(0);

      await waitUntil(() => wrapper.historyMock.replace.called);
      expect(wrapper.historyMock.replace.calledWithExactly('/')).to.equal(true);
    });

    it('should redirect to landing page when user has an org but site not yet loaded', async () => {
      const sitesAndOrgs = {
        totalSites: -1,
        totalOrganizations: 1,
      };
      const wrapper = createOrganizationPageMount(sitesAndOrgs);

      const spinner = wrapper.getWrapper().find('Spinner');
      expect(spinner).to.have.length(0);

      const permissionError = wrapper.getWrapper().find<any>(PermissionErrorPage);
      expect(permissionError).to.have.length(0);

      await waitUntil(() => wrapper.historyMock.replace.called);
      expect(wrapper.historyMock.replace.calledWithExactly('/')).to.equal(true);
    });

    it('should redirect to landing page when user has an org and site', async () => {
      const sitesAndOrgs = {
        totalSites: 1,
        totalOrganizations: 1,
      };
      const wrapper = createOrganizationPageMount(sitesAndOrgs);

      const spinner = wrapper.getWrapper().find('Spinner');
      expect(spinner).to.have.length(0);

      const permissionError = wrapper.getWrapper().find<any>(PermissionErrorPage);
      expect(permissionError).to.have.length(0);

      await waitUntil(() => wrapper.historyMock.replace.called);
      expect(wrapper.historyMock.replace.calledWithExactly('/')).to.equal(true);
    });

  });

  describe('Loading state', () => {
    it('should display loading state when startCreateOrganization is called', async () => {
      const wrapper = createOrganizationPageMount();

      wrapper.getOrgPage().startCreateOrganization();

      await waitUntil(() => wrapper.getWrapper().find('Spinner').length === 1);
      expect(wrapper.getWrapper().find('Spinner')).length(1);
    });

    it('should not display loading spinner when stopCreateOrganization is called', async () => {
      const wrapper = createOrganizationPageMount();
      wrapper.getOrgPage().startCreateOrganization();
      await waitUntil(() => wrapper.getWrapper().find('Spinner').length === 1);

      wrapper.getOrgPage().stopCreateOrganization();

      await waitUntil(() => wrapper.getWrapper().find('Spinner').length === 0);
      expect(wrapper.getWrapper().find('Spinner')).length(0);
    });

    it('should clear org creation state after stopCreateOrganization is called', async () => {
      const wrapper = createOrganizationPageMount();
      wrapper.getOrgPage().startCreateOrganization();
      await waitUntil(() => wrapper.getWrapper().find('Spinner').length === 1);

      wrapper.getOrgPage().stopCreateOrganization();

      await waitUntil(() => wrapper.clearOrgCreationStateSpy.callCount === 1);
      expect(wrapper.clearOrgCreationStateSpy.callCount).to.equal(1);
    });
  });

  describe('Successful org creation', () => {
    it('should show a success flag and redirect to org overview on completeCreateOrganization', async () => {
      const wrapper = createOrganizationPageMount();

      wrapper.getOrgPage().createOrgPage.setState({
        orgId: 'test-org-id',
        orgName: 'test-org-name',
      });

      wrapper.getOrgPage().completeCreateOrganization();

      await waitUntil(() => wrapper.historyMock.replace.called);
      expect(wrapper.historyMock.replace.calledWithExactly('/o/test-org-id/overview')).to.equal(true);
      expect(showFlagSpy.called).to.equal(true);
      const args = showFlagSpy.lastCall.args[0];
      expect(args.title).to.include('test-org-name');
    });

    it('should clear org creation state on completeCreateOrganization', async () => {
      const wrapper = createOrganizationPageMount();

      wrapper.getOrgPage().createOrgPage.setState({
        orgId: 'test-org-id',
        orgName: 'test-org-name',
      });

      expect(wrapper.clearOrgCreationStateSpy.callCount).to.equal(0);
      wrapper.getOrgPage().completeCreateOrganization();

      await waitUntil(() => wrapper.historyMock.replace.called);
      expect(wrapper.clearOrgCreationStateSpy.callCount).to.equal(1);
    });
  });

  describe('Failed org creation', () => {
    it('should show an error flag', async () => {
      const wrapper = createOrganizationPageMount();
      wrapper.getOrgPage().createOrganizationFailure();

      expect(wrapper.historyMock.replace.called).to.equal(false);
      expect(showFlagSpy.called).to.equal(true);
    });

    it('should clear org creation state', async () => {
      const wrapper = createOrganizationPageMount();
      wrapper.getOrgPage().createOrganizationFailure();

      expect(wrapper.clearOrgCreationStateSpy.callCount).to.equal(1);
    });
  });

  describe('Form', () => {
    it('Calls submitForm when a form submit event is fired', () => {
      const wrapper = createOrganizationPageMount();

      expect(wrapper.getOrgPage().submitFormSpy.callCount).to.equal(0);

      wrapper.getForm().simulateSubmit();

      expect(wrapper.getOrgPage().submitFormSpy.callCount).to.equal(1);
    });

    it('Stops polling when poll response has completed=true status', () => {
      const wrapper = createOrganizationPage();

      wrapper.getWrapper().setProps({ data: { pollCreateOrg: { completed: false, successful: false }, stopPolling: stopPollingSpy } });
      wrapper.getWrapper().setProps({ data: { pollCreateOrg: { completed: true, successful: false }, stopPolling: stopPollingSpy } });

      expect(stopPollingSpy.callCount).to.equal(1);
    });

    it('Redirects when response is successful and completed and refetch is done', async () => {
      const wrapper = createOrganizationPage();

      wrapper.getWrapper().setProps({ data: { pollCreateOrg: { completed: false, successful: false }, stopPolling: stopPollingSpy } });
      wrapper.getWrapper().setProps({ data: { pollCreateOrg: { completed: true, successful: true }, stopPolling: stopPollingSpy } });

      await waitUntil(() => wrapper.historyMock.replace.called);
      expect(wrapper.historyMock.replace.called);
    });

    it('Should disable input after form is submitted', async () => {
      const wrapper = createOrganizationPageMount();

      wrapper.getOrgPage().submitForm();

      const orgNameInput = wrapper.getOrgNameInput();
      expect(orgNameInput.props.disabled).to.equal(true);
    });
  });

  describe('Progress state', () => {
    function setupCreateOrganizationPageWithProgressState(params: {
      stateOrgId?: string,
      stateOrgName?: string,
      stateProgressUri?: string,
    } = {}) {
      const mutate: any = () => ({
        then: (cb) => {
          cb({
            data: {
              createOrganization: {
                id: 'some-org-id',
                progressUri: 'some-progress-uri',
              },
            },
          });

          return {
            catch: (ecb) => {
              ecb();
            },
          };
        },
      });
      const startPolling = () => null;

      return {
        ...createOrganizationPageMount({
          data: { refetch: refetchSpy, startPolling } as any,
          mutate,
          orgId: params.stateOrgId,
          orgName: params.stateOrgName,
          progressUri: params.stateProgressUri,
        }),
        refetchSpy,
      };
    }

    it('should set progress state upon receiving initial create org response', () => {
      const { getOrgPage } = setupCreateOrganizationPageWithProgressState();

      getOrgPage().submitForm('some-org-name');

      expect(stateUpdateSpy.callCount).to.equal(1);
      expect(stateUpdateSpy.getCalls()[0].args).to.deep.equal(['some-progress-uri', 'some-org-id', 'some-org-name']);
    });

    it('should query progress upon render if state is present', () => {
      const pollingRefetchSpy = setupCreateOrganizationPageWithProgressState({
        stateOrgId: 'test',
        stateOrgName: 'Test org name',
        stateProgressUri: 'some-progress-uri',
      }).refetchSpy;

      expect(pollingRefetchSpy.calledWith({
        progressUri: 'some-progress-uri',
      })).to.equal(true);
    });

    it('should not query progress upon render if progress state not is not present', () => {
      const pollingRefetchSpy = setupCreateOrganizationPageWithProgressState().refetchSpy;

      expect(pollingRefetchSpy.calledWith({
        progressUri: 'some-progress-uri',
      })).to.equal(false);
    });

    it('should rehydrate orgId and orgName state on polling resumption', () => {
      const { getOrgPage } = setupCreateOrganizationPageWithProgressState({
        stateOrgId: 'some-org-id',
        stateOrgName: 'some-org-name',
        stateProgressUri: 'some-progress-uri',
      });

      expect(getOrgPage().createOrgPage.state).includes({
        orgId: 'some-org-id',
        orgName: 'some-org-name',
      });
    });
  });
});
