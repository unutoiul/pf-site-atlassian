import * as React from 'react';

import { sessionStorageUtils } from '../../utilities/sessionStorage';

export interface CreateOrganizationPersistedState {
  progressUri?: string;
  orgId?: string;
  orgName?: string;
  setOrgCreationState(progressUri: string, orgId: string, orgName: string): void;
  clearOrgCreationState(): void;
}

interface ProgressUriParams {
  progressUri: string;
  orgName: string;
  orgId: string;
}

export function withCreateOrganizationPersistedState<TInProps>(
  Component: React.ComponentType<TInProps & CreateOrganizationPersistedState>,
): React.ComponentClass<TInProps> {
  type State = Partial<ProgressUriParams>;

  const storageKey = 'org-creation-state';

  return class extends React.Component<TInProps, State> {
    public readonly state: Readonly<State> = {};

    public componentWillMount() {
      const storedStateString = sessionStorageUtils.getItem(storageKey);
      if (!storedStateString) {
        return;
      }

      try {
        const { orgId, orgName, progressUri } = JSON.parse(storedStateString);
        this.setState({
          orgId,
          orgName,
          progressUri,
        });
      } catch (_) {
        // most likely JSON parsing failed, so we ignore the error
      }
    }

    public render() {
      const {
        orgId,
        orgName,
        progressUri,
      } = this.state;

      return (
        <Component
          orgId={orgId}
          orgName={orgName}
          progressUri={progressUri}
          setOrgCreationState={this.update}
          clearOrgCreationState={this.clear}
          {...this.props}
        />
      );
    }

    private update = (progressUri: string, orgId: string, orgName: string) => {
      sessionStorageUtils.setItem(storageKey, JSON.stringify({
        orgId,
        orgName,
        progressUri,
      }));
    }

    private clear = () => {
      sessionStorageUtils.removeItem(storageKey);
    }
  };
}
