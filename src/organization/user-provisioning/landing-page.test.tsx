// tslint:disable jsx-use-translation-function
import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import AkButton from '@atlaskit/button';

import { GenericError } from 'common/error';
import { IdentityManagerLearnMoreButton } from 'common/identity-manager-learn-more-button';

import { createMockContext, createMockIntlProp } from '../../utilities/testing';
import { VerifyDomainSectionMessage } from '../domain-claim/verify-domain-section-message';
import { LandingPageImpl } from './landing-page';

describe('User Provisioning Landing Page', () => {
  const showDrawerMock = () => undefined;

  const shallowUserProvisioningLandingPage = ({ atlassianAccess, error, verified = true }) => {
    const organization = {
      organization: {
        domainClaim: {
          domains: [{
            verified,
          }],
        },
      },
    };

    return shallow(
      <LandingPageImpl
        data={{ products: { identityManager: atlassianAccess }, error, ...organization } as any}
        showDrawer={showDrawerMock as any}
        analyticsClient={{} as any}
        match={{ params: { orgId: '12345' } } as any}
        history={{} as any}
        location={{} as any}
        intl={createMockIntlProp()}
      />,
      createMockContext({ intl: true, apollo: true }),
    );
  };

  it('should render create directory button', () => {
    const wrapper = shallowUserProvisioningLandingPage({ atlassianAccess: true, error: false });
    const button = wrapper.find(AkButton);
    expect(button.exists()).to.equal(true);
    const learnMoreButton = wrapper.find(IdentityManagerLearnMoreButton);
    expect(learnMoreButton.exists()).to.equal(false);
  });

  it('should render verify domain if no domain claims', () => {
    const wrapper = shallowUserProvisioningLandingPage({ atlassianAccess: true, error: false, verified: false });
    const verifyDomainSection = wrapper.find(VerifyDomainSectionMessage);
    expect(verifyDomainSection.exists()).to.equal(true);
  });

  it('should render learn more button if not signed up for access', () => {
    const wrapper = shallowUserProvisioningLandingPage({ atlassianAccess: false, error: false });
    const learnMoreButton = wrapper.find(IdentityManagerLearnMoreButton);
    expect(learnMoreButton.exists()).to.equal(true);
  });

  it('should render learn more button if not signed up for access and no domain verfied', () => {
    const wrapper = shallowUserProvisioningLandingPage({ atlassianAccess: false, error: false, verified: false });
    const learnMoreButton = wrapper.find(IdentityManagerLearnMoreButton);
    expect(learnMoreButton.exists()).to.equal(true);
  });

  it('should render error message if there is an error', () => {
    const wrapper = shallowUserProvisioningLandingPage({ atlassianAccess: true, error: true });
    const error = wrapper.find(GenericError);
    expect(error.exists()).to.equal(true);
  });
});
