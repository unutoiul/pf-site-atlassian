import * as React from 'react';
import { ChildProps, graphql, OptionProps, QueryResult } from 'react-apollo';
import { defineMessages, FormattedDate, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';
import styled from 'styled-components';

import AkButton from '@atlaskit/button';
import {
  colors as akColors,
  fontSize as akFontSize,
} from '@atlaskit/theme';

import { analyticsClient as oldAnalyticsClient, AnalyticsClientProps, scimDirectoryRemovedTrackData, userProvisioningRemoveDirectoryButton, withAnalyticsClient } from 'common/analytics';
import { ShowDrawerProps, withShowDrawer } from 'common/drawer';
import { createErrorIcon, createSuccessIcon, errorFlagMessages, GenericError } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';

import { DirectoryQuery, DirectoryQueryVariables, RemoveDirectoryMutation } from '../../schema/schema-types';
import { OrgRouteProps } from '../routes';
import directoryQuery from './directory.query.graphql';
import { RemoveDirectoryDialog } from './remove-directory-dialog';
import removeDirectoryMutation from './remove-directory.mutation.graphql';

const messages = defineMessages({
  created: {
    id: 'organization.userProvisioning.directory.created',
    defaultMessage: 'Created {creationDate}',
    description: 'Text that prepends a creation date, in English it will be something like "Created 5 Jan 2018". Variable creationDate will be automatically localized',
  },
  remove: {
    id: 'organization.userProvisioning.directory.remove',
    defaultMessage: 'Remove',
    description: 'Text for a button that removes a SCIM directory when clicked',
  },
  regenerate: {
    id: 'organization.userProvisioning.directory.regenerate',
    defaultMessage: 'Regenerate',
    description: 'Text for a button that regenerates/makes a new SCIM API key when clicked',
  },
  removeSuccess: {
    id: 'organization.userProvisioning.directory.remove.success',
    defaultMessage: '{directoryName} removed',
    description: 'Dialog message shown to user when their SCIM api key & user directory is successfully removed',
  },
  removeFailure: {
    id: 'organization.userProvisioning.directory.remove.failure',
    defaultMessage: 'There was an issue removing {directoryName}',
    description: 'Dialog message shown to user when revoking their directory fails',
  },
  errorTitle: {
    id: 'organization.userProvisioning.directory.error.title',
    defaultMessage: 'No information found',
    description: 'Error message shown when there is an error getting directory data.',
  },
});

const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
`;

const DirectoryName = styled.div`
  font-weight: 500;
`;

const CreatedDate = styled.div`
  color: ${akColors.subtleHeading};
  font-size: ${akFontSize() - 2}px;
`;

interface State {
  removeDirectoryDialog: {
    isOpen: boolean;
    isMutationLoading: boolean;
  };
}

export type DirectoryProps = DerivedProps & FlagProps & InjectedIntlProps & AnalyticsClientProps;

export class DirectoryImpl extends React.Component<ChildProps<DirectoryProps, RemoveDirectoryMutation> & RouteComponentProps<OrgRouteProps> & ShowDrawerProps, State> {
  public readonly state: Readonly<State> = {
    removeDirectoryDialog: {
      isOpen: false,
      isMutationLoading: false,
    },
  };

  public render() {
    const { error, directories } = this.props;
    const { removeDirectoryDialog } = this.state;

    if (error) {
      return <GenericError header={<FormattedMessage {...messages.errorTitle} />} />;
    }

    if (!directories || directories.length === 0) {
      return null;
    }

    return (
      <React.Fragment>
        <Wrapper>
          <div>
            <DirectoryName>{directories[0].name}</DirectoryName>
            <CreatedDate>
              <FormattedMessage
                {...messages.created}
                values={{ creationDate: <FormattedDate value={directories[0].creationDate} day="numeric" month="short" year="numeric" /> }}
              />
            </CreatedDate>
          </div>
          <div>
            <AkButton
              id="scim-regenerate-api-key-button"
              appearance="link"
              onClick={this.showRegenerateApiKeyFocusTask}
            >
              <FormattedMessage {...messages.regenerate} />
            </AkButton>
            <AkButton
              id="scim-remove-directory-button"
              appearance="link"
              onClick={this.showRemoveDirectoryDialog}
            >
              <FormattedMessage {...messages.remove} />
            </AkButton>
          </div>
        </Wrapper>
        <RemoveDirectoryDialog
          isOpen={removeDirectoryDialog.isOpen}
          isRemoveDisabled={removeDirectoryDialog.isMutationLoading}
          onDismiss={this.hideRemoveDirectoryDialog}
          onRemove={this.removeDirectory}
          directoryName={directories[0].name}
        />
      </React.Fragment>
    );
  }

  public showRegenerateApiKeyFocusTask = () => {
    this.props.showDrawer('USER_PROVISIONING_REGEN_API_KEY');
  }

  public showRemoveDirectoryDialog = () => {
    this.setRemoveDirectoryDialogOpenState(true);
  }

  public hideRemoveDirectoryDialog = () => {
    this.setRemoveDirectoryDialogOpenState(false);
  }

  public removeDirectory = async () => {
    const { analyticsClient, directories, mutate, match: { params: { orgId } } } = this.props;

    if (!directories || directories.length === 0 || !mutate) {
      return;
    }

    this.setRemoveDirectoryLoadingState(true);
    analyticsClient.sendUIEvent({
      orgId,
      data: userProvisioningRemoveDirectoryButton(),
    });

    try {
      const result = await mutate({
        variables: { directoryId: directories[0].id },
        refetchQueries: [{
          query: directoryQuery,
          variables: {
            id: orgId,
          },
        }],
      });

      if (result && result.data && result.data.removeDirectory) {
        analyticsClient.sendTrackEvent({
          orgId,
          data: scimDirectoryRemovedTrackData(),
        });
        this.showSuccessFlag(directories[0].name);
      } else {
        throw new Error('Failed to remove SCIM directory');
      }
    } catch (error) {
      oldAnalyticsClient.onError(error);
      this.showErrorFlag(directories[0].name);
      this.hideRemoveDirectoryDialog();
      this.setRemoveDirectoryLoadingState(false);
    }
  }

  private showSuccessFlag = (directoryName: string) => {
    const { showFlag, intl: { formatMessage } } = this.props;
    showFlag({
      autoDismiss: true,
      icon: createSuccessIcon(),
      id: `user.provisioning.remove.directory.success.${Date.now()}`,
      title: formatMessage(messages.removeSuccess, { directoryName }),
    });
  }

  private showErrorFlag = (directoryName: string) => {
    const { showFlag, intl: { formatMessage } } = this.props;
    showFlag({
      autoDismiss: true,
      icon: createErrorIcon(),
      id: `user.provisioning.remove.directory.failure.${Date.now()}`,
      title: formatMessage(errorFlagMessages.title),
      description: formatMessage(messages.removeFailure, { directoryName }),
    });
  }

  private setRemoveDirectoryDialogOpenState = (isOpen: boolean) => {
    this.setState(prevState => ({
      removeDirectoryDialog: {
        ...prevState.removeDirectoryDialog,
        isOpen,
      },
    }));
  };

  private setRemoveDirectoryLoadingState = (isMutationLoading: boolean) => {
    this.setState(prevState => ({
      removeDirectoryDialog: {
        ...prevState.removeDirectoryDialog,
        isMutationLoading,
      },
    }));
  };
}

interface DerivedProps {
  loading: boolean;
  error?: QueryResult['error'];
  directories?: DirectoryQuery['organization']['externalDirectories'];
}

const getDirectories = (componentProps: OptionProps<RouteComponentProps<OrgRouteProps>, DirectoryQuery>): DerivedProps => {
  if (!componentProps || !componentProps.data || componentProps.data.loading) {
    return {
      loading: true,
    };
  }

  if (componentProps.data.error || !componentProps.data.organization) {
    return {
      loading: false,
      error: componentProps.data.error,
    };
  }

  return {
    loading: false,
    directories: componentProps.data.organization.externalDirectories,
  };
};

const withDirectoriesQuery = graphql<RouteComponentProps<OrgRouteProps>, DirectoryQuery, DirectoryQueryVariables, DerivedProps>(
  directoryQuery,
  {
    options: (props) => ({ variables: { id: props.match.params.orgId } }),
    props: getDirectories,
  },
);

const withRemoveDirectoryMutation = graphql<DirectoryProps & RouteComponentProps<OrgRouteProps>, RemoveDirectoryMutation>(
  removeDirectoryMutation,
);

export const Directory =
  withRouter<{}>(
    withDirectoriesQuery(
      injectIntl(
        withFlag(
          withAnalyticsClient(
            withRemoveDirectoryMutation(
              withShowDrawer(
                DirectoryImpl,
              ),
            ),
          ),
        ),
      ),
    ),
  )
;
