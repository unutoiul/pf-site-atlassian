import { ApolloQueryResult } from 'apollo-client';
import { parse, stringify } from 'query-string';
import * as React from 'react';
import { graphql, OptionProps, QueryResult } from 'react-apollo';
import { defineMessages, FormattedMessage, FormattedNumber, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';
import styled from 'styled-components';

import AkDynamicTable from '@atlaskit/dynamic-table';
import { gridSize as akGridSize } from '@atlaskit/theme';

import { GenericError } from 'common/error';
import { GroupName } from 'common/group-name';
import { getStart, sanitizePage } from 'common/pagination';
import { EmptyTableView } from 'common/table';

import { SyncedGroup, SyncedGroupsCollection, SyncedGroupsQuery, SyncedGroupsQueryVariables } from '../../schema/schema-types';
import { OrgRouteProps } from '../routes';
import syncedGroupsQuery from './synced-groups.query.graphql';

const messages = defineMessages({
  description: {
    id: 'organization.userProvisioning.groups.description',
    defaultMessage: 'Nested groups from your external directory will be flattened. Users in nested groups will also be members of any parent group.',
    description: 'Refers to synced groups of users from an identity provider such as active directory',
  },
  groupNameHeader: {
    id: 'organization.userProvisioning.groups.header.name',
    defaultMessage: 'Name',
    description: 'Heading for a table column of groups names. A group is a group of users synced from an identity provider such as active directory',
  },
  groupUserTotalHeader: {
    id: 'organization.userProvisioning.groups.header.userTotal',
    defaultMessage: 'Users',
    description: 'Heading for a table column containg the number or users in a group. Users are synced from an identity provider like active directory',
  },
  emptyGroups: {
    id: 'organization.userProvisioning.groups.empty',
    defaultMessage: 'No groups synced',
    description: 'Message shown when no synced groups are found. A group is a group of users synced from an identity provider such as active directory',
  },
  errorTitle: {
    id: 'organization.userProvisioning.groups.error.title',
    defaultMessage: 'No information found',
    description: 'An error message title shown when the synced groups were not able to be loaded',
  },
});

const rowsPerPage = 20;

const Description = styled.div`
  margin-bottom: ${akGridSize() * 3}px;
`;
const nameCellKey = '1';
const userTotalCellKey = '2';

export type SyncedGroupsProps = InjectedIntlProps & DerivedProps & RouteComponentProps<OrgRouteProps>;
export class SyncedGroupsImpl extends React.Component<SyncedGroupsProps> {
  public render() {
    const { loading, error, location: { search } } = this.props;
    const { page } = parse(search);

    return (
      <React.Fragment>
        <Description>
          <FormattedMessage {...messages.description} />
        </Description>
        <AkDynamicTable
          isLoading={loading}
          head={this.getHead()}
          rows={this.getRows()}
          emptyView={
            error ? (
                <GenericError header={<FormattedMessage {...messages.errorTitle} />} />
              ) : (
                <EmptyTableView>
                  <FormattedMessage {...messages.emptyGroups} />
                </EmptyTableView>
              )
          }
          rowsPerPage={rowsPerPage}
          onSetPage={this.setPage}
          page={sanitizePage(page)}
        />
      </React.Fragment>
    );
  }

  private getHead = () => {
    return {
      cells: [
        {
          key: nameCellKey,
          content: <FormattedMessage {...messages.groupNameHeader} />,
          width: 70,
        },
        {
          key: userTotalCellKey,
          content: <FormattedMessage {...messages.groupUserTotalHeader} />,
          width: 30,
        },
      ],
    };
  }

  private getRows() {
    const { groups } = this.props;

    if (!groups) {
      return null;
    }

    return groups.groups.map((group, index: number) => {
      if (!group) {
        return {
          key: `row-${index}`,
          cells: [
            {
              key: nameCellKey,
              content: null,
            },
            {
              key: userTotalCellKey,
              content: null,
            },
          ],
        };
      }

      const { name, membershipCount } = group;

      return {
        key: `row-${index}`,
        cells: [
          {
            key: nameCellKey,
            content: (
              <GroupName name={name} ownerType="EXT_SCIM" managementAccess="READ_ONLY" />
            ),
          },
          {
            key: userTotalCellKey,
            content: <FormattedNumber value={membershipCount} />,
          },
        ],
      };
    });
  }

  private setPage = async (page: number) => {
    const { fetchMoreGroups, location } = this.props;
    if (typeof fetchMoreGroups !== 'function') {
      throw new Error('Problem fetching groups page');
    }

    this.updateQueryParams({
      ...parse(location.search),
      page,
    });

    return fetchMoreGroups(page);
  };

  private updateQueryParams = (params: object) => {
    const { history, location } = this.props;
    history.replace(`${location.pathname}?${stringify(params)}`);
  };

}

interface DerivedProps {
  loading: boolean;
  error?: QueryResult['error'];
  groups?: SyncedGroupsCollection;
  fetchMoreGroups?(page: number): Promise<ApolloQueryResult<SyncedGroupsQuery>>;
}

const getGroups = (componentProps: OptionProps<RouteComponentProps<OrgRouteProps>, SyncedGroupsQuery>): DerivedProps => {
  if (!componentProps || !componentProps.data || componentProps.data.loading) {
    return {
      loading: true,
    };
  }

  const { error, fetchMore } = componentProps.data;

  if (error || !componentProps.data.organization) {
    return {
      loading: false,
      error,
    };
  }

  // AkDynamicTable requires an array with length equal to total results for pagination to work, so we pad our results here
  const groups = componentProps.data.organization.externalDirectories[0].groups;
  const paddedGroups: SyncedGroup[] = Array(groups.totalResults).fill(null);
  paddedGroups.splice(groups.startIndex, groups.groups.length, ...groups.groups);
  const groupsWithPadding = {
    ...groups,
    groups: paddedGroups,
  };

  return {
    fetchMoreGroups: async (page: number) => fetchMore({
      variables: {
        start: getStart(page, rowsPerPage),
      },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        return {
          ...previousResult,
          ...fetchMoreResult,
        };
      },
    }),
    loading: false,
    groups: groupsWithPadding,
  };
};

const withSyncedGroupsQuery = graphql<RouteComponentProps<OrgRouteProps>, SyncedGroupsQuery, SyncedGroupsQueryVariables, DerivedProps>(
  syncedGroupsQuery,
  {
    options: ({ location, match: { params: { orgId } } }) => {
      const { page } = parse(location.search);

      return {
        variables: {
          id: orgId,
          count: rowsPerPage,
          start: getStart(sanitizePage(page), rowsPerPage),
        },
      };
    },
    props: getGroups,
  },
);

export const SyncedGroups =
  withRouter<{}>(
    withSyncedGroupsQuery(
      injectIntl(
        SyncedGroupsImpl,
      ),
    ),
  )
;
