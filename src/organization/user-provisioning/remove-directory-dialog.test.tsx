import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import AkButton from '@atlaskit/button';

import { createMockIntlContext } from '../../utilities/testing';
import { RemoveDirectoryDialog, RemoveDirectoryDialogProps } from './remove-directory-dialog';

const mountRemoveDirectoryDialog = (props: RemoveDirectoryDialogProps) => {
  return mount<RemoveDirectoryDialogProps, never>((
    <RemoveDirectoryDialog
      {...props}
    />
  ), createMockIntlContext());
};

describe('Remove Directory Dialog', () => {
  const sandbox = sinon.sandbox.create();
  let onDismissSpy: sinon.SinonSpy;
  let onRemoveSpy: sinon.SinonSpy;

  beforeEach(() => {
    onDismissSpy = sandbox.spy();
    onRemoveSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('should render an action button which calls the onRemove prop', () => {
    const wrapper = mountRemoveDirectoryDialog({
      isOpen: true,
      isRemoveDisabled: false,
      directoryName: 'My directory',
      onDismiss: onDismissSpy,
      onRemove: onRemoveSpy,
    });

    const removeButton = wrapper.find(AkButton).first();

    expect(onRemoveSpy.callCount).to.equal(0);
    removeButton.simulate('click');
    expect(onRemoveSpy.callCount).to.equal(1);
  });

  it('should disable action button', () => {
    const wrapper = mountRemoveDirectoryDialog({
      isOpen: true,
      isRemoveDisabled: true,
      directoryName: 'My directory',
      onDismiss: onDismissSpy,
      onRemove: onRemoveSpy,
    });

    const removeButton = wrapper.find(AkButton).first();

    expect(removeButton.props().isDisabled).to.equal(true);
  });

  it('should render a cancel button which calls the onDismiss prop', () => {
    const wrapper = mountRemoveDirectoryDialog({
      isOpen: true,
      isRemoveDisabled: false,
      directoryName: 'My directory',
      onDismiss: onDismissSpy,
      onRemove: onRemoveSpy,
    });

    const cancelButton = wrapper.find(AkButton).last();

    expect(onDismissSpy.callCount).to.equal(0);
    cancelButton.simulate('click');
    expect(onDismissSpy.callCount).to.equal(1);
  });

});
