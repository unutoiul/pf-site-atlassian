import { parse, stringify } from 'query-string';
import * as React from 'react';
import { graphql, OptionProps, QueryResult } from 'react-apollo';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';
import styled from 'styled-components';

import AkSectionMessage from '@atlaskit/section-message';
import AkTabs from '@atlaskit/tabs';
import { gridSize as akGridSize } from '@atlaskit/theme';

import { CountIndicator } from 'common/count-indicator';
import { links } from 'common/link-constants';
import { ExternalLink } from 'common/navigation';
import { PageLayout } from 'common/page-layout';

import {
  AnalyticsClientProps,
  Referrer,
  ScreenEventSender,
  userProvisioningAddSiteEventData,
  userProvisioningAuditLogLink,
  userProvisioningDirectoryLink,
  userProvisioningGroupsLink,
  userProvisioningProductAccessLink,
  userProvisioningScreenEvent,
  withAnalyticsClient,
} from 'common/analytics';
import { ShowDrawerProps, withShowDrawer } from 'common/drawer';
import { GenericError } from 'common/error';

import { UserProvisioningQuery, UserProvisioningQueryVariables } from '../../schema/schema-types';
import { OrgBreadcrumbs } from '../breadcrumbs';
import { OrgRouteProps } from '../routes';
import { LinkedSitesProps, withLinkedSites } from '../with-linked-sites';
import { AuditLog } from './audit-log';
import { Directory } from './directory';
import { ProductAccess } from './product-access';
import { SyncedGroups } from './synced-groups';
import userProvisioningQuery from './user-provisioning.query.graphql';
import { withDirectoriesRedirect } from './with-directories-redirect';

const CountContainer = styled.div`
  display: flex;
`;

const TabContainer = styled.div`
  padding-top: ${akGridSize() * 3}px;
`;

const Tab = styled.div`
  width: 100%;
  padding-top: ${akGridSize() * 3}px;
`;

const OrgSiteLinkingBannerContainer = styled.div`
  margin-bottom: ${akGridSize() * 3}px;
`;

const messages = defineMessages({
  title: {
    id: 'organization.userProvisioning.title',
    defaultMessage: 'User provisioning',
    description: 'User provisioning is connecting to an identity provider like active directory and then automatically syncing groups and users from there using SCIM: https://en.wikipedia.org/wiki/System_for_Cross-domain_Identity_Management. It is not exactly user creation, it is using an external source to provision users in your own system. The translation of this should match admin.nav.user.provisioning',
  },
  description: {
    id: 'organization.userProvisioning.description',
    defaultMessage: 'Automatically provision users and groups from your identity provider. Users from verified domains will be synced from your identity provider. {learnMoreLink}',
    description: 'Description for what user provisioning is and where you can find more details.',
  },
  syncedUsers: {
    id: 'organization.userProvisioning.synced.users',
    defaultMessage: 'Synced users',
    description: 'Title for showing the total number of users synced.',
  },
  syncedGroups: {
    id: 'organization.userProvisioning.synced.groups',
    defaultMessage: 'Synced groups',
    description: 'Title for showing the total number of groups synced.',
  },
  auditLog: {
    id: 'organization.userProvisioning.audit.log',
    defaultMessage: 'Audit log',
    description: 'Title for the tab showing the logs created by SCIM.',
  },
  groups: {
    id: 'organization.userProvisioning.groups',
    defaultMessage: 'Groups',
    description: 'Title for the tab showing the list of groups synced by SCIM.',
  },
  apiKey: {
    id: 'organization.userProvisioning.apiKey',
    defaultMessage: 'API key',
    description: 'Title for the tab showing the SCIM API key.',
  },
  productAccess: {
    id: 'organization.userProvisioning.productAccess',
    defaultMessage: 'Product access',
    description: 'Title for a tab showing access to different products',
  },
  learnMore: {
    id: 'organization.userProvisioning.learnMore',
    defaultMessage: 'Learn more',
    description: 'Link to know more about SCIM.',
  },
  addSiteBannerTitle: {
    id: 'organization.userProvisioning.addSiteBannerTitle',
    defaultMessage: 'Set up default access',
    description: 'This message serves as a title for a banner. "default access" is about privileges that SCIM-provisioned users will receive when the syncing process is completed.',
  },
  addSiteBannerBody: {
    id: 'organization.userProvisioning.addSiteBannerBody',
    defaultMessage: 'Add Jira and Confluence sites to your organization so that users will get default product access when they\'re synced.',
    description: 'This message serves as a body for a banner. "default access" is about privileges that SCIM-provisioned users will receive when the syncing process is completed.',
  },
  addSiteBannerAction: {
    id: 'organization.userProvisioning.addSiteBannerAction',
    defaultMessage: 'Add a site',
    description: 'This message serves as a call to action link for a banner. "site" as in Atlassian site (e.g. company-name.atlassian.net)',
  },
});

const tabs = [
  { label: <FormattedMessage {...messages.auditLog} />, content: <Tab><AuditLog /></Tab>, uiData: userProvisioningAuditLogLink },
  { label: <FormattedMessage {...messages.groups} />, content: <Tab><SyncedGroups /></Tab>, uiData: userProvisioningGroupsLink },
  { label: <FormattedMessage {...messages.productAccess} />, content: <Tab><ProductAccess /></Tab>, uiData: userProvisioningProductAccessLink },
  { label: <FormattedMessage {...messages.apiKey} />, content: <Tab><Directory /></Tab>, uiData: userProvisioningDirectoryLink },
];

export class UserProvisioningPageImpl extends React.Component<GroupsDerivedProps & ShowDrawerProps & AnalyticsClientProps & LinkedSitesProps & RouteComponentProps<OrgRouteProps> & InjectedIntlProps> {
  public render() {
    const { loading, error, syncedGroups, syncedUsers, linkedSitesData, intl: { formatMessage }, match: { params: { orgId } } } = this.props;

    const eventData = {
      orgId,
      syncedGroupCount: syncedGroups,
      syncedUserCount: syncedUsers,
      siteCount: linkedSitesData.sites ? linkedSitesData.sites.length : undefined,
    };

    return (
      <ScreenEventSender event={userProvisioningScreenEvent(eventData)} isDataLoading={loading || linkedSitesData.loading}>
        <Referrer value="userProvisioningScreen">
          <PageLayout
            breadcrumbs={<OrgBreadcrumbs />}
            title={<FormattedMessage {...messages.title} />}
            description={
              <FormattedMessage
                {...messages.description}
                values={{
                  learnMoreLink: (
                    <ExternalLink
                      href={links.external.userProvisioningGeneral}
                      hideReferrer={false}
                    >
                      <FormattedMessage {...messages.learnMore} />
                    </ExternalLink>
                  ),
                }}
              />
            }
            isFullWidth={true}
          >
            {this.shouldShowSiteLinkingBanner() && (
              <OrgSiteLinkingBannerContainer>
                <AkSectionMessage
                  appearance="warning"
                  title={formatMessage(messages.addSiteBannerTitle)}
                  actions={[
                    {
                      text: formatMessage(messages.addSiteBannerAction),
                      onClick: this.openOrgSiteLinkingDrawer,
                    },
                  ]}
                >
                  <FormattedMessage {...messages.addSiteBannerBody} tagName="p" />
                </AkSectionMessage>
              </OrgSiteLinkingBannerContainer>
            )}
            <CountContainer>
              <CountIndicator
                header={<FormattedMessage {...messages.syncedUsers} />}
                count={loading || error ? undefined : syncedUsers}
              />
              <CountIndicator
                header={<FormattedMessage {...messages.syncedGroups} />}
                count={loading || error ? undefined : syncedGroups}
              />
            </CountContainer>
            {error ? (
                <GenericError />
              ) : (
                <TabContainer>
                  <AkTabs tabs={tabs} onSelect={this.tabChanged} />
                </TabContainer>
              )
            }
          </PageLayout>
        </Referrer>
      </ScreenEventSender>
    );
  }

  private shouldShowSiteLinkingBanner(): boolean {
    const {
      linkedSitesData: {
        loading,
        error,
        sites,
      },
    } = this.props;

    return !loading && !error && !!sites && sites.length === 0;
  }

  private openOrgSiteLinkingDrawer = () => {
    const { analyticsClient, match: { params: { orgId } } } = this.props;

    analyticsClient.sendUIEvent({
      orgId,
      data: userProvisioningAddSiteEventData(),
    });
    this.props.showDrawer('ORG_SITE_LINKING');
  }

  private tabChanged = (tab) => {
    this.sendTabEvent(tab);
    this.removePageFromQueryParams();
  }

  private sendTabEvent = (tab) => {
    const { analyticsClient, match: { params: { orgId } } } = this.props;
    analyticsClient.sendUIEvent({
      orgId,
      data: tab.uiData(),
    });
  }

  private removePageFromQueryParams = () => {
    const { history, location } = this.props;
    const params = parse(location.search);
    // tslint:disable-next-line:no-unused
    const { page, ...rest } = params;
    history.replace(`${location.pathname}?${stringify(rest)}`);
  }
}

interface GroupsDerivedProps {
  loading: boolean;
  error?: QueryResult['error'];
  syncedUsers?: number;
  syncedGroups?: number;
}

const getDirectorySummary = (componentProps: OptionProps<RouteComponentProps<OrgRouteProps>, UserProvisioningQuery>): GroupsDerivedProps => {
  if (!componentProps || !componentProps.data || componentProps.data.loading) {
    return {
      loading: true,
    };
  }

  const { error } = componentProps.data;

  if (error || !componentProps.data.organization) {
    return {
      loading: false,
      error,
    };
  }

  return {
    loading: false,
    syncedUsers: componentProps.data.organization.externalDirectories[0].syncedUsers.countOfUsers,
    syncedGroups: componentProps.data.organization.externalDirectories[0].groups.totalResults,
  };
};

const withGroupsSummaryQuery = graphql<ShowDrawerProps & RouteComponentProps<OrgRouteProps>, UserProvisioningQuery, UserProvisioningQueryVariables, GroupsDerivedProps>(
  userProvisioningQuery,
  {
    options: (props) => ({ variables: { id: props.match.params.orgId } }),
    props: getDirectorySummary,
  },
);

export const UserProvisioningPage =
  withDirectoriesRedirect(
    withRouter(
      withShowDrawer(
        withGroupsSummaryQuery(
          withLinkedSites(
            withAnalyticsClient(
              injectIntl(
                UserProvisioningPageImpl,
              ),
            ),
          ),
        ),
      ),
    ),
  )
  ;
