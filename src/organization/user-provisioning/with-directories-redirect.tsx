import * as React from 'react';
import { graphql, OptionProps, QueryResult } from 'react-apollo';
import { RouteComponentProps, withRouter } from 'react-router';

import AkSpinner from '@atlaskit/spinner';

import { GenericError } from 'common/error';
import { PageLayout } from 'common/page-layout';

import { DirectoryQuery, DirectoryQueryVariables } from '../../schema/schema-types';
import { OrgRouteProps } from '../routes';
import directoryQuery from './directory.query.graphql';
import { LandingPage } from './landing-page';

export interface DerivedProps {
  loading: boolean;
  error?: QueryResult['error'];
  hasDirectories?: boolean;
}

export function withDirectoriesRedirectImpl(Component: React.ComponentType) {

  return class extends React.Component<DerivedProps & RouteComponentProps<OrgRouteProps>> {

    public render() {
      const { loading, error, hasDirectories } = this.props;

      if (loading) {
        return <PageLayout description={<AkSpinner />} isFullWidth={true} />;
      }

      if (error) {
        return <GenericError />;
      }

      if (hasDirectories === false) {
        return <LandingPage {...this.props} />;
      }

      return <Component {...this.props} />;
    }

  };
}

const getDirectories = (componentProps: OptionProps<RouteComponentProps<OrgRouteProps>, DirectoryQuery>): DerivedProps => {
  if (!componentProps || !componentProps.data || componentProps.data.loading) {
    return {
      loading: true,
    };
  }

  if (componentProps.data.error) {
    return {
      loading: false,
      error: componentProps.data.error,
    };
  }

  return {
    loading: false,
    hasDirectories: componentProps.data && componentProps.data.organization &&
      componentProps.data.organization.externalDirectories &&
      componentProps.data.organization.externalDirectories.length > 0,
  };
};

const withDirectoriesQuery = graphql<RouteComponentProps<OrgRouteProps>, DirectoryQuery, DirectoryQueryVariables, DerivedProps>(
  directoryQuery,
  {
    options: (props) => ({ variables: { id: props.match.params.orgId } }),
    props: getDirectories,
  },
);

export function withDirectoriesRedirect(Component: React.ComponentType) {
  return withRouter(withDirectoriesQuery(withDirectoriesRedirectImpl(Component)));
}
