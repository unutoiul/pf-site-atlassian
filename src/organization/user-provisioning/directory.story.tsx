
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';
import { MemoryRouter } from 'react-router-dom';

import AkPage from '@atlaskit/page';

import { createApolloClient } from '../../apollo-client';
import { DirectoryQuery } from '../../schema/schema-types';
import { createMockIntlProp } from '../../utilities/testing';
import { DirectoryImpl } from './directory';

const directoriesMock: DirectoryQuery['organization']['externalDirectories'] = [
  { id: '1', name: 'Okta', creationDate: '2018-08-02T20:07:30.610117Z' } as any,
];

const auditLogOrgId = 'myorg1234';

const getPage = (loading, error, directories?) => {
  const client = createApolloClient();

  return (
    <ApolloProvider client={client}>
      <IntlProvider locale="en">
        <MemoryRouter>
          <AkPage>
            <DirectoryImpl
              analyticsClient={{ sendUIEvent: () => undefined } as any}
              directories={directories}
              loading={loading}
              error={error}
              showFlag={null as any}
              hideFlag={null as any}
              intl={createMockIntlProp()}
              match={{ params: { orgId: auditLogOrgId }, path: '', url: '', isExact: true }}
              location={{ search: '', pathname: '', state: '', hash: '' }}
              history={null as any}
              showDrawer={() => undefined as any}
            />
          </AkPage>
        </MemoryRouter>
      </IntlProvider>
    </ApolloProvider>
  );
};

storiesOf('Organization|User Provisioning/Tab - Directory', module)
  .add('Default', () => getPage(false, false, directoriesMock))
  .add('Empty', () => getPage(false, false, []))
  .add('Loading', () => getPage(true, false))
  .add('Error', () => getPage(false, true))
;
