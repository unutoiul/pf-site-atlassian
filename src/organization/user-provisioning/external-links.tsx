import * as React from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';

import { links } from 'common/link-constants';
import { ExternalLink } from 'common/navigation';

const messages = defineMessages({
  okta: {
    id: 'organization.userProvisioning.links.okta',
    defaultMessage: 'Okta',
  },
  generalInstructions: {
    id: 'organization.userProvisioning.links.general',
    defaultMessage: 'general instructions',
    description: 'General/non-specific instructions for link text',
  },
});

export const OktaLink = (
  <ExternalLink
    href={links.external.userProvisioningOkta}
    hideReferrer={false}
  >
    <FormattedMessage {...messages.okta} />
  </ExternalLink>
);

export const GeneralInstructionsLink = (
  <ExternalLink
    href={links.external.userProvisioningGeneral}
    hideReferrer={false}
  >
    <FormattedMessage {...messages.generalInstructions} />
  </ExternalLink>
);
