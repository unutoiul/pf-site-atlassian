import * as React from 'react';
import {
  defineMessages,
  FormattedMessage,
} from 'react-intl';

import AkButton, { ButtonGroup as AkButtonGroup } from '@atlaskit/button';

import { ModalDialog } from 'common/modal';

export const messages = defineMessages({
  title: {
    id: 'organization.userProvisioning.directory.remove.dialog.title',
    defaultMessage: 'Remove {directoryName}',
    description: 'The title of the dialog to delete a SCIM directory.',
  },
  description: {
    id: 'organization.userProvisioning.directory.remove.dialog.description',
    defaultMessage: 'Removing the directory will:',
    description: 'Modal description telling the user what happens when a SCIM directory is removed. This is followed by bullets containing organization.userProvisioning.directory.remove.dialog.description.item1 and organization.userProvisioning.directory.remove.dialog.description.item2 and organization.userProvisioning.directory.remove.dialog.description.item3',
  },
  descriptionItem1: {
    id: 'organization.userProvisioning.directory.remove.dialog.description.item1',
    defaultMessage: 'Revoke the directory API key',
    description: 'Modal description list item telling the user what happens when SCIM user directory is removed. This list is preceeded by organization.userProvisioning.directory.remove.dialog.description',
  },
  descriptionItem2: {
    id: 'organization.userProvisioning.directory.remove.dialog.description.item2',
    defaultMessage: 'Stop provisioning users and groups from your identity provider',
    description: 'Modal description list item telling the user what happens when SCIM user directory is removed. This list is preceeded by organization.userProvisioning.directory.remove.dialog.description',
  },
  descriptionItem3: {
    id: 'organization.userProvisioning.directory.remove.dialog.description.item3',
    defaultMessage: 'Allow synced groups to be editable',
    description: 'Modal description list item telling the user what happens when SCIM user directory is removed. This list is preceeded by organization.userProvisioning.directory.remove.dialog.description',
  },
  userAccountsAlreadySynced: {
    id: 'organization.userProvisioning.directory.remove.dialog.description.userAccountsAlreadySynced',
    defaultMessage: 'User accounts already synced will not be affected and will retain access to products.',
    description: 'Message about how user accounts synced by SCIM will not be deleted',
  },
  buttonRemove: {
    id: 'organization.userProvisioning.directory.remove.dialog.button.remove',
    defaultMessage: 'Confirm',
    description: 'The button that confirms the removing of the user directory.',
  },
  buttonCancel: {
    id: 'organization.userProvisioning.directory.remove.dialog.button.cancel',
    defaultMessage: 'Cancel',
    description: 'The button that cancels and closes the dialog.',
  },
});

export interface RemoveDirectoryDialogProps {
  isOpen: boolean;
  isRemoveDisabled: boolean;
  directoryName: string;
  onDismiss(): void;
  onRemove(): void;
}

export class RemoveDirectoryDialog extends React.Component<RemoveDirectoryDialogProps> {
  public render() {
    const {
      isOpen,
      isRemoveDisabled,
      onDismiss,
      onRemove,
      directoryName,
    } = this.props;

    return (
      <ModalDialog
        isOpen={isOpen}
        width="small"
        onClose={onDismiss}
        header={(
          <FormattedMessage {...messages.title} values={{ directoryName }} />
        )}
        footer={(
          <AkButtonGroup>
            <AkButton
              id="scim-confirm-remove-directory-button"
              appearance="danger"
              onClick={onRemove}
              isDisabled={isRemoveDisabled}
            >
              <FormattedMessage {...messages.buttonRemove} />
            </AkButton>
            <AkButton
              appearance="subtle-link"
              onClick={onDismiss}
            >
              <FormattedMessage {...messages.buttonCancel} />
            </AkButton>
          </AkButtonGroup>
        )}
      >
        <FormattedMessage
          {...messages.description}
          tagName="p"
        />
        <ul>
          <FormattedMessage {...messages.descriptionItem1} tagName="li" />
          <FormattedMessage {...messages.descriptionItem2} tagName="li" />
          <FormattedMessage {...messages.descriptionItem3} tagName="li" />
        </ul>
        <FormattedMessage
          {...messages.userAccountsAlreadySynced}
          tagName="p"
        />
      </ModalDialog>
    );
  }
}
