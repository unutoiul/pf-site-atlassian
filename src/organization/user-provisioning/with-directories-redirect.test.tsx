import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import { GenericError } from 'common/error';
import { PageLayout } from 'common/page-layout';

import { ApolloError } from '../../../node_modules/apollo-client';
import { createMockContext } from '../../utilities/testing';
import { LandingPage } from './landing-page';
import { DerivedProps, withDirectoriesRedirectImpl } from './with-directories-redirect';

describe('Directories redirect', () => {

  const mountTestComponent = (mockData: DerivedProps) => {
    class TestComponentImpl extends React.Component {
      public render() {
        return <div id="test" />;
      }
    }

    const TestComponent = withDirectoriesRedirectImpl(TestComponentImpl);

    return shallow((
      <TestComponent {...mockData} history={null as any} location={null as any} match={{ isExact: false, params: { orgId: '1234' }, path: '', url: '' }} />
    ), createMockContext({ intl: false, apollo: true }));
  };

  it('should render child if there are directories', () => {
    const wrapper = mountTestComponent({ loading: false, hasDirectories: true });
    expect(wrapper.html().includes('<div id="test">')).to.equal(true);
  });

  it('should render LandingPage if there are no directories', () => {
    const wrapper = mountTestComponent({ loading: false, hasDirectories: false });
    const landingPage = wrapper.find(LandingPage);
    expect(landingPage.exists()).to.equal(true);
  });

  it('should render page layout with spinner when loading', () => {
    const wrapper = mountTestComponent({ loading: true, hasDirectories: false });
    const spinnerPageLayout = wrapper.find(PageLayout);
    expect(spinnerPageLayout.exists()).to.equal(true);
  });

  it('shoud render error', () => {
    const apolloError = new ApolloError({});
    const wrapper = mountTestComponent({ loading: false, error: apolloError });
    const error = wrapper.find(GenericError);
    expect(error.exists()).to.equal(true);
  });

});
