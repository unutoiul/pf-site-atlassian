import { ApolloQueryResult } from 'apollo-client';
import { parse, stringify } from 'query-string';
import * as React from 'react';
import { graphql, OptionProps, QueryResult } from 'react-apollo';
import { defineMessages, FormattedDate, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';

import { DynamicTableStateless as AkDynamicTableStateless, Row as AkRow } from '@atlaskit/dynamic-table';

import { GenericError } from 'common/error';
import { getStart, sanitizePage } from 'common/pagination';
import { EmptyTableView, getPaginationMessages } from 'common/table';

import { AuditLogQuery, AuditLogQueryVariables, LogEntries, LogEntryCollection } from '../../schema/schema-types';
import { OrgRouteProps } from '../routes';
import auditLogQuery from './audit-log.query.graphql';

const messages = defineMessages({
  date: {
    id: 'organization.userProvisioning.auditLog.date',
    defaultMessage: 'Date',
    description: 'Date column heading for a table of log entries.',
  },
  description: {
    id: 'organization.userProvisioning.auditLog.description',
    defaultMessage: 'Description',
    description: 'Description column heading for a table of log entries.',
  },
  empty: {
    id: 'organization.userProvisioning.auditLog.empty',
    defaultMessage: 'No errors',
    description: 'Description when log table is empty, errors referrs to any kind of issue or problem with syncing data from the identity provider.',
  },
  errorTitle: {
    id: 'organization.userProvisioning.auditLog.error.title',
    defaultMessage: 'No information found',
    description: 'Error message shown when there is an error getting log data.',
  },
});

const rowsPerPage = 50;

export type AuditLogProps = DerivedProps & RouteComponentProps<OrgRouteProps> & InjectedIntlProps;

export class AuditLogImpl extends React.Component<AuditLogProps> {

  public render() {
    const { auditLog, error, loading, location: { search } } = this.props;
    const { page } = parse(search);

    return (
      <AkDynamicTableStateless
        isFixedSize={true}
        isLoading={loading}
        head={this.getHead()}
        rows={this.getRows(auditLog)}
        emptyView={
          error ? (
              <GenericError header={<FormattedMessage {...messages.errorTitle} />} />
            ) : (
              <EmptyTableView>
                <FormattedMessage {...messages.empty} />
              </EmptyTableView>
            )
        }
        rowsPerPage={rowsPerPage}
        onSetPage={this.onSetAuditLogPage}
        page={sanitizePage(page)}
        paginationi18n={this.getPaginationMessages()}
      />
    );
  }

  private getHead = () => {
    return {
      cells: [
        {
          key: 'date',
          content: <FormattedMessage {...messages.date} />,
          width: 20,
        },
        {
          key: 'description',
          content: <FormattedMessage {...messages.description} />,
          width: 80,
        },
      ],
    };
  }

  private getRows = (auditLog: LogEntryCollection | undefined): AkRow[] | undefined => {
    if (!auditLog || !auditLog.logEntries || auditLog.logEntries.length === 0) {
      return undefined;
    }

    return auditLog.logEntries.map(this.formatRow);
  }

  private formatRow = (logEntry: LogEntries, index: number): AkRow => {
    if (!logEntry) {
      return {
        key: `row-${index}`,
        cells: [
          {
            key: 'date',
            content: null,
          },
          {
            key: 'description',
            content: null,
          },
        ],
      };
    }

    return {
      key: `row-${logEntry.createdOn}`,
      cells: [
        {
          key: 'date',
          content: <FormattedDate value={logEntry.createdOn} day="numeric" month="short" hour12={false} year="numeric" hour="numeric" minute="numeric" second="numeric" />,
        },
        {
          key: 'description',
          content: logEntry.error,
        },
      ],
    };
  }

  private onSetAuditLogPage = async (page: number) => {
    const { fetchMoreLogs, location } = this.props;
    if (typeof fetchMoreLogs !== 'function') {
      throw new Error('Problem fetching audit log page');
    }

    this.updateQueryParams({
      ...parse(location.search),
      page,
    });

    return fetchMoreLogs(page);
  };

  private updateQueryParams = (params: object) => {
    const { history, location } = this.props;
    history.replace(`${location.pathname}?${stringify(params)}`);
  };

  private getPaginationMessages = () => {
    return getPaginationMessages(this.props.intl.formatMessage.bind(this.props.intl));
  }
}

interface DerivedProps {
  loading: boolean;
  error?: QueryResult['error'];
  auditLog?: LogEntryCollection;
  fetchMoreLogs?(page: number): Promise<ApolloQueryResult<AuditLogQuery>>;
}

const getAuditLog = (componentProps: OptionProps<RouteComponentProps<OrgRouteProps>, AuditLogQuery>): DerivedProps => {
  if (!componentProps || !componentProps.data || componentProps.data.loading) {
    return {
      loading: true,
    };
  }

  const { error, fetchMore } = componentProps.data;

  if (error || !componentProps.data.organization) {
    return {
      loading: false,
      error,
    };
  }

  // AkDynamicTable requires an array with length equal to total results for pagination to work, so we pad our results here
  const auditLog = componentProps.data.organization.externalDirectories[0].logs;
  const paddedLogs: LogEntries[] = Array(auditLog.totalResults).fill(null);
  paddedLogs.splice(auditLog.startIndex, auditLog.logEntries.length, ...auditLog.logEntries);
  const auditLogWithPadding = {
    ...auditLog,
    logEntries: paddedLogs,
  };

  return {
    fetchMoreLogs: async (page: number) => fetchMore({
      variables: {
        start: getStart(page, rowsPerPage),
      },
      updateQuery: (previousResult, { fetchMoreResult }) => {
        return {
          ...previousResult,
          ...fetchMoreResult,
        };
      },
    }),
    loading: false,
    auditLog: auditLogWithPadding,
  };
};

const withAuditLogQuery = graphql<RouteComponentProps<OrgRouteProps>, AuditLogQuery, AuditLogQueryVariables, DerivedProps>(
  auditLogQuery,
  {
    options: ({ location, match: { params: { orgId } } }) => {
      const { page } = parse(location.search);

      return {
        variables: {
          id: orgId,
          count: rowsPerPage,
          start: getStart(sanitizePage(page), rowsPerPage),
        },
      };
    },
    props: getAuditLog,
  },
);

export const AuditLog =
  withRouter<{}>(
    withAuditLogQuery(
      injectIntl(
        AuditLogImpl,
      ),
    ),
  )
;
