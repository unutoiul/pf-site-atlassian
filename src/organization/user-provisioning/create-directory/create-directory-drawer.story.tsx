
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';
import { MemoryRouter } from 'react-router-dom';

import AkPage from '@atlaskit/page';

import { FlagProvider } from 'common/flag';

import { createApolloClient } from '../../../apollo-client';

import { ApiKeyDetails } from './api-key-details';
import { CreateDirectoryDrawer, Page } from './create-directory-drawer';

const client = createApolloClient();

storiesOf('Organization|User Provisioning/Create Directory Drawer', module)
  .add('Default', () => {
    return (
      <ApolloProvider client={client}>
        <IntlProvider locale="en">
          <MemoryRouter>
            <FlagProvider>
              <AkPage>
                <CreateDirectoryDrawer onCloseDrawer={() => null} />
              </AkPage>
            </FlagProvider>
          </MemoryRouter>
        </IntlProvider>
      </ApolloProvider>
    );
  })
  .add('Second page', () => {
    return (
      <ApolloProvider client={client}>
        <IntlProvider locale="en">
          <MemoryRouter>
            <FlagProvider>
              <AkPage>
                <Page>
                  <ApiKeyDetails
                    directoryName="Acme test"
                    apiKey="X123-Y5"
                    baseUrl="hamburger.com"
                    orgId="12354"
                    onCloseDrawer={() => null}
                  />
                </Page>
              </AkPage>
            </FlagProvider>
          </MemoryRouter>
        </IntlProvider>
      </ApolloProvider>
    );
  })
;
