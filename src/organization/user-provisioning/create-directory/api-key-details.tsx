import * as React from 'react';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import AkButton from '@atlaskit/button';
import AkWarningIcon from '@atlaskit/icon/glyph/warning';
import { gridSize as akGridSize } from '@atlaskit/theme';
import { akColorN300, akColorY300 } from '@atlaskit/util-shared-styles';

import {
  AnalyticsClientProps,
  ScreenEventSender,
  userProvisioningApiKeyDetailsScreenEvent,
  userProvisioningCopyApiKeyEventData,
  userProvisioningCopyBaseUrlEventData,
  userProvisioningFinishCreateDirectoryEventData,
  withAnalyticsClient,
} from 'common/analytics';
import { ButtonGroup as ButtonGroupBase } from 'common/button-group';
import { TextCopy } from 'common/text-copy';

import { OktaLink } from '../external-links';

const messages = defineMessages({
  title: {
    id: 'organization.userProvisioning.createDirectoryDrawer.keyDetails.title',
    defaultMessage: '{directoryName} API key',
    description: 'Title of the API key details page, directoryName is a variable that will be replaced with the name of an API key',
  },
  description: {
    id: 'organization.userProvisioning.createDirectoryDrawer.keyDetails.description',
    defaultMessage: 'Use the base URL and API key to set up the integration in your identity provider. See step-by-step instructions for {oktaLink}.',
    description: 'Instructions for how to set up SCIM integration with Okta and other Identity Providers',
  },
  directoryBaseUrlLabel: {
    id: 'organization.userProvisioning.createDirectoryDrawer.keyDetails.directoryBaseUrlLabel',
    defaultMessage: 'Directory base URL',
    description: 'Label of a field that contains a directory base URL',
  },
  apiKeyValueLabel: {
    id: 'organization.userProvisioning.createDirectoryDrawer.keyDetails.apiKeyValueLabel',
    defaultMessage: 'API key',
    description: 'Label of a field that contains an API key',
  },
  copyButtonLabel: {
    id: 'organization.userProvisioning.createDirectoryDrawer.keyDetails.copyButtonLabel',
    defaultMessage: 'Copy',
    description: 'text of a button that will copy the value of a field when clicked',
  },
  warningIconLabel: {
    id: 'organization.userProvisioning.createDirectoryDrawer.keyDetails.warning.label',
    defaultMessage: 'warning',
    description: 'a text label for a warning icon',
  },
  warningDescription: {
    id: 'organization.userProvisioning.createDirectoryDrawer.keyDetails.warning.description',
    defaultMessage: 'Store this key somewhere safe. It will not be visible again once you close this page.',
  },
  doneButton: {
    id: 'organization.userProvisioning.createDirectoryDrawer.keyDetails.doneButton',
    defaultMessage: 'Done',
    description: 'text of a button that will close this page when clicked',
  },
});

const Description = styled.div`
  margin-top: ${akGridSize() * 2}px;
  margin-bottom: ${akGridSize() - 4}px;
`;

const WarningMessage = styled.div`
  display: flex;
  margin-top: ${akGridSize}px;
  color: ${akColorN300};

  span:first-child {
    margin-right: ${akGridSize}px;
  }
`;

const ButtonGroup = styled(ButtonGroupBase) `
  margin-top: ${akGridSize() * 4}px;
`;

interface OwnProps {
  directoryName: string;
  apiKey: string;
  baseUrl: string;
  orgId: string;
  onCloseDrawer(): void;
}

export class ApiKeyDetailsImpl extends React.Component<OwnProps & InjectedIntlProps & AnalyticsClientProps> {

  public render() {
    const { intl: { formatMessage }, baseUrl, apiKey, directoryName, orgId } = this.props;

    return (
      <ScreenEventSender event={userProvisioningApiKeyDetailsScreenEvent()} >
        <FormattedMessage {...messages.title} tagName="h1" values={{ directoryName }} />
        <Description>
          <FormattedMessage {...messages.description} values={{ oktaLink: OktaLink }} />
        </Description>
        <TextCopy
          label={formatMessage(messages.directoryBaseUrlLabel)}
          value={baseUrl}
          analyticsUIEvent={{ orgId, data: userProvisioningCopyBaseUrlEventData() }}
        />
        <TextCopy
          label={formatMessage(messages.apiKeyValueLabel)}
          value={apiKey}
          analyticsUIEvent={{ orgId, data: userProvisioningCopyApiKeyEventData() }}
        />
        <WarningMessage>
          <AkWarningIcon size="medium" primaryColor={akColorY300} label={formatMessage(messages.warningIconLabel)} />
          <FormattedMessage {...messages.warningDescription} />
        </WarningMessage>
        <ButtonGroup alignment="right">
          <AkButton
            id="scim-api-key-details-done-button"
            appearance="primary"
            type="button"
            onClick={this.onDone}
          >
            <FormattedMessage {...messages.doneButton} />
          </AkButton>
        </ButtonGroup>
      </ScreenEventSender>
    );
  }

  private onDone = () => {
    const { onCloseDrawer, orgId, analyticsClient } = this.props;

    analyticsClient.sendUIEvent({
      orgId,
      data: userProvisioningFinishCreateDirectoryEventData(),
    });

    onCloseDrawer();
  }
}

export const ApiKeyDetails =
  injectIntl<OwnProps>(
    withAnalyticsClient(
      ApiKeyDetailsImpl,
    ),
  )
;
