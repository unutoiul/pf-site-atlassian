import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';
import styled from 'styled-components';

import { ProgressTracker as AkProgressTracker } from '@atlaskit/progress-tracker';
import { gridSize as akGridSize } from '@atlaskit/theme';

import { OrgRouteProps } from '../../routes';
import { ApiKeyDetails } from './api-key-details';
import { CreateDirectory } from './create-directory';

const messages = defineMessages({
  step1: {
    id: 'organization.userProvisioning.createDirectoryDrawer.step1',
    defaultMessage: 'Create a directory',
    description: 'Directory is a directory of users that will be synced with SCIM (http://www.simplecloud.info/)',
  },
  step2: {
    id: 'organization.userProvisioning.createDirectoryDrawer.step2',
    defaultMessage: 'API key',
    description: 'An API key for SCIM integration (http://www.simplecloud.info/)',
  },
});

export const Page = styled.div`
  margin: 0 auto;
  width:  ${akGridSize() * 50}px;
`;

interface State {
  page: 'create-directory' | 'api-key-details';
  name?: string;
  apiKey?: string;
  baseUrl?: string;
}

interface OwnProps {
  onCloseDrawer(): void;
}

type InProps = InjectedIntlProps & RouteComponentProps<OrgRouteProps> & OwnProps;

export class CreateDirectoryDrawerImpl extends React.Component<InProps, State> {
  public state: Readonly<State> = {
    page: 'create-directory',
  };

  public render() {
    const { intl: { formatMessage }, onCloseDrawer, match: { params: { orgId } } } = this.props;

    const progressTrackerItems = [
      {
        id: 'create-directory',
        label: formatMessage(messages.step1),
        percentageComplete: this.state.page === 'create-directory' ? 0 : 100,
        status: this.state.page === 'create-directory' ? 'current' : 'visited',
      },
      {
        id: 'api-key-details',
        label: formatMessage(messages.step2),
        percentageComplete: 0,
        status: this.state.page === 'create-directory' ? 'unvisited' : 'current',
      },
    ];

    return (
      <Page>
        <AkProgressTracker items={progressTrackerItems} />
        {this.state.page === 'create-directory' ?
          <CreateDirectory
            onCloseDrawer={onCloseDrawer}
            goToDetailsPage={this.goToDetailsPage}
            orgId={orgId}
          /> :
          <ApiKeyDetails
            onCloseDrawer={onCloseDrawer}
            directoryName={this.state.name!}
            apiKey={this.state.apiKey!}
            baseUrl={this.state.baseUrl!}
            orgId={orgId}
          />
        }
      </Page>
    );
  }

  private goToDetailsPage = (name: string, apiKey: string, baseUrl: string) => {
    if (name && apiKey && baseUrl) {
      this.setState({ page: 'api-key-details', name, apiKey, baseUrl });
    }
  }

}

export const CreateDirectoryDrawer = withRouter<OwnProps>(injectIntl<InProps>(CreateDirectoryDrawerImpl));
