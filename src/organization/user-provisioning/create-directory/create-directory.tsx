import { ApolloQueryResult } from 'apollo-client';
import * as React from 'react';
import { graphql } from 'react-apollo';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import AkButton from '@atlaskit/button';
import { FieldTextStateless as AkTextFieldStateless } from '@atlaskit/field-text';
import { gridSize as akGridSize } from '@atlaskit/theme';

import { AnalyticsClientProps, scimDirectoryCreatedTrackData, ScreenEventSender, userProvisioningCancelCreateDirectoryEventData, userProvisioningCreateDirectoryEventData, userProvisioningCreateDirectoryScreenEvent, withAnalyticsClient } from 'common/analytics';
import { ButtonGroup as ButtonGroupBase } from 'common/button-group';
import { createErrorIcon, errorFlagMessages } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';

import { CreateDirectoryMutation, CreateDirectoryMutationVariables } from '../../../schema/schema-types';
import directoryQuery from '../directory.query.graphql';
import createDirectoryMutation from './create-directory.mutation.graphql';

const messages = defineMessages({
  title: {
    id: 'organization.userProvisioning.createDirectoryDrawer.createDirectoryPage.title',
    defaultMessage: 'Create a directory',
    description: 'The title of a page for creating a directory that will contain users synced via SCIM (http://www.simplecloud.info/)',
  },
  inputLabel: {
    id: 'organization.userProvisioning.createDirectoryDrawer.createDirectoryPage.inputLabel',
    defaultMessage: 'Name',
    description: 'The label for an field for inputting a name for a directory',
  },
  inputPrompt: {
    id: 'organization.userProvisioning.createDirectoryDrawer.createDirectoryPage.inputPrompt',
    defaultMessage: 'Name of your directory',
    description: 'Placeholder text prompting users to enter the name of their identity manager user directory name',
  },
  createButton: {
    id: 'organization.userProvisioning.createDirectoryDrawer.createDirectoryPage.createButton',
    defaultMessage: 'Create',
    description: 'Text for a button that creates a SCIM user directory',
  },
  backButton: {
    id: 'organization.userProvisioning.createDirectoryDrawer.createDirectoryPage.backButton',
    defaultMessage: 'Back',
    description: 'Text for a button that cancels and exits this dialog',
  },
  createDirectoryErrorDescription: {
    id: 'organization.userProvisioning.createDirectoryDrawer.createDirectoryPage.error.description',
    defaultMessage: 'There was an issue creating your directory. Please try again later.',
    description: 'Description for an error dialog that shows when there was a problem creating a SCIM user directory',
  },
});

const ButtonGroup = styled(ButtonGroupBase) `
  margin-top: ${akGridSize() * 4}px;
`;

interface State {
  isLoading: boolean;
  directoryName: string;
}

interface OwnProps {
  orgId: string;
  goToDetailsPage(name: string, apiKey: string, baseUrl: string): void;
  onCloseDrawer(): void;
}

interface CreateDirectoryMutationProps {
  createDirectory(orgId: string, name: string): Promise<false | ApolloQueryResult<CreateDirectoryMutation>>;
}

type CreateDirectoryProps = CreateDirectoryMutationProps & OwnProps & InjectedIntlProps & FlagProps & AnalyticsClientProps;

export class CreateDirectoryImpl extends React.Component<CreateDirectoryProps, State> {
  public state: Readonly<State> = { isLoading: false, directoryName: '' };

  public render() {
    const { intl: { formatMessage } } = this.props;
    const { isLoading, directoryName } = this.state;

    return (
      <ScreenEventSender event={userProvisioningCreateDirectoryScreenEvent()} >
        <FormattedMessage {...messages.title} tagName="h1" />
        <form onSubmit={this.onSubmit}>
          <AkTextFieldStateless
            placeholder={formatMessage(messages.inputPrompt)}
            onChange={this.setTextFieldState}
            label={formatMessage(messages.inputLabel)}
            required={true}
            disabled={isLoading}
            shouldFitContainer={true}
            id="scim-create-directory-name"
            autoFocus={true}
            autoComplete="off"
            value={directoryName}
          />
          <ButtonGroup alignment="right">
            <AkButton
              appearance="default"
              onClick={this.onCancel}
            >
              <FormattedMessage {...messages.backButton} />
            </AkButton>
            <AkButton
              id="scim-create-directory-button"
              appearance="primary"
              type="submit"
              isDisabled={directoryName === ''}
              isLoading={isLoading}
            >
              <FormattedMessage {...messages.createButton} />
            </AkButton>
          </ButtonGroup>
        </form>
      </ScreenEventSender>
    );
  }

  private setTextFieldState = (event: React.FormEvent<HTMLInputElement>): void => {
    this.setState({ directoryName: (event.target as HTMLInputElement).value });
  };

  private onCancel = () => {
    const { onCloseDrawer, orgId, analyticsClient } = this.props;

    analyticsClient.sendUIEvent({
      orgId,
      data: userProvisioningCancelCreateDirectoryEventData(),
    });

    onCloseDrawer();
  }

  private onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (!this.canSubmitForm()) {
      return;
    }

    const { analyticsClient, orgId, goToDetailsPage, createDirectory, showFlag, intl: { formatMessage } } = this.props;
    const { directoryName } = this.state;
    this.setState({ isLoading: true });

    analyticsClient.sendUIEvent({
      orgId,
      data: userProvisioningCreateDirectoryEventData(),
    });

    try {
      const result = await createDirectory(orgId, directoryName);
      if (!result) {
        throw new Error('createDirectory returned bad result');
      }
      analyticsClient.sendTrackEvent({
        orgId,
        data: scimDirectoryCreatedTrackData(),
      });
      const { name, apiKey, baseUrl } = result.data.createDirectory;
      goToDetailsPage(name, apiKey, baseUrl);
    } catch (error) {
      this.setState({ isLoading: false });
      showFlag({
        autoDismiss: true,
        icon: createErrorIcon(),
        id: `common.copy.text.field.error.${Date.now()}`,
        title: formatMessage(errorFlagMessages.title),
        description: formatMessage(messages.createDirectoryErrorDescription),
      });
    }

  }

  private canSubmitForm() {
    return this.state.directoryName.length > 0;
  }
}

const withCreateDirectoryMutation = graphql<OwnProps, CreateDirectoryMutation, CreateDirectoryMutationVariables, CreateDirectoryMutationProps>(
  createDirectoryMutation,
  {
    props: ({ mutate }) => ({
      createDirectory: async (orgId: string, name: string) => mutate instanceof Function && mutate({
        variables: { orgId, name },
        refetchQueries: [{
          query: directoryQuery,
          variables: {
            id: orgId,
          },
        }],
      }),
    }),
  },
);

export const CreateDirectory = withCreateDirectoryMutation(
  injectIntl(
    withFlag(
      withAnalyticsClient(
        CreateDirectoryImpl,
      ),
    ),
  ),
);
