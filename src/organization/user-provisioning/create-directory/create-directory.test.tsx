// tslint:disable jsx-use-translation-function
import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import * as sinon from 'sinon';

import AkButton from '@atlaskit/button';
import { FieldTextStateless as AkTextFieldStateless } from '@atlaskit/field-text';

import { createMockIntlContext, createMockIntlProp } from '../../../utilities/testing';

import { CreateDirectoryImpl } from './create-directory';

describe('User Provisioning create directory', () => {

  const sandbox = sinon.sandbox.create();
  let closeDrawerSpy;
  let createDirectoryStub;
  let goToDetailsPageSpy;
  let showFlagSpy;
  let sendUIEventSpy;
  let sendTrackEventSpy;

  beforeEach(() => {
    closeDrawerSpy = sandbox.spy();
    createDirectoryStub = sandbox.stub();
    goToDetailsPageSpy = sandbox.spy();
    showFlagSpy = sandbox.spy();
    sendUIEventSpy = sandbox.spy();
    sendTrackEventSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  const shallowUserProvisioningCreateDirectoryPage = () => {
    return shallow(
      <CreateDirectoryImpl
        analyticsClient={{ sendUIEvent: sendUIEventSpy, sendTrackEvent: sendTrackEventSpy } as any}
        orgId="1234"
        onCloseDrawer={closeDrawerSpy}
        createDirectory={createDirectoryStub}
        goToDetailsPage={goToDetailsPageSpy}
        showFlag={showFlagSpy}
        hideFlag={() => null}
        intl={createMockIntlProp()}
      />,
      createMockIntlContext(),
    );
  };

  it('should render UI elements correctly', () => {
    const wrapper = shallowUserProvisioningCreateDirectoryPage();
    const text = wrapper.find(FormattedMessage) as any;
    expect(text).to.have.length(3);
    expect(text.at(0).props().defaultMessage).to.contain('Create a directory');
  });

  it('should close drawer', () => {
    const wrapper = shallowUserProvisioningCreateDirectoryPage();
    const button = wrapper.find(AkButton);
    expect(button).to.have.length(2);
    button.at(0).simulate('click');
    expect(closeDrawerSpy.called).to.equal(true);
  });

  it('should send UI event on close drawer', () => {
    const wrapper = shallowUserProvisioningCreateDirectoryPage();
    const button = wrapper.find(AkButton);
    expect(button).to.have.length(2);
    button.at(0).simulate('click');
    expect(sendUIEventSpy.called).to.equal(true);
  });

  it('should go to next page', async () => {
    const expectedItems = {
      data: {
        createDirectory: {
          name: 'directoryName',
          apiKey: '123456',
          baseUrl: 'www.hamburger.com',
        },
      },
    };
    const promise = Promise.resolve(expectedItems);
    createDirectoryStub.onFirstCall().returns(promise);

    const wrapper = shallowUserProvisioningCreateDirectoryPage();
    let buttons = wrapper.find(AkButton);
    expect(buttons).to.have.length(2);
    expect(buttons.at(1).props().isDisabled).to.equal(true);

    wrapper.find(AkTextFieldStateless).simulate('change', { target: { value: 'xyz' } });
    wrapper.update();

    buttons = wrapper.find(AkButton);
    expect(buttons.at(1).props().isDisabled).to.equal(false);

    const form = wrapper.find('form').at(0);
    form.simulate('submit', { preventDefault: () => null });

    await promise;

    expect(goToDetailsPageSpy.called).to.equal(true);
    expect(sendTrackEventSpy.called).to.equal(true);
  });

});
