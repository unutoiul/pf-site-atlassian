// tslint:disable jsx-use-translation-function
import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import * as sinon from 'sinon';

import AkButton from '@atlaskit/button';

import { createMockIntlContext, createMockIntlProp } from '../../../utilities/testing';

import { ApiKeyDetailsImpl } from './api-key-details';

describe('User Provisioning api key details', () => {

  let closeDrawerSpy;
  let sendUIEventSpy;
  const sandbox = sinon.sandbox.create();

  beforeEach(() => {
    closeDrawerSpy = sandbox.spy();
    sendUIEventSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  const shallowUserProvisioningDetailsPage = () => {
    return shallow(
      <ApiKeyDetailsImpl
        analyticsClient={{ sendUIEvent: sendUIEventSpy } as any}
        directoryName="myKey"
        apiKey="1234-1234"
        baseUrl="www.hamburger.com"
        orgId="1234"
        onCloseDrawer={closeDrawerSpy}
        intl={createMockIntlProp()}
      />,
      createMockIntlContext(),
    );
  };

  it('should render UI elements correctly', () => {
    const wrapper = shallowUserProvisioningDetailsPage();
    const text = wrapper.find(FormattedMessage) as any;
    expect(text).to.have.length(4);
    expect(text.at(0).props().defaultMessage).to.contain('{directoryName} API key');
    expect(text.at(0).props().values.directoryName).to.equal('myKey');
  });

  it('should close drawer', () => {
    const wrapper = shallowUserProvisioningDetailsPage();
    const button = wrapper.find(AkButton);
    expect(button).to.have.length(1);
    button.simulate('click');
    expect(closeDrawerSpy.called).to.equal(true);
  });

  it('should send UI event on close', () => {
    const wrapper = shallowUserProvisioningDetailsPage();
    const button = wrapper.find(AkButton);
    expect(button).to.have.length(1);
    button.simulate('click');
    expect(sendUIEventSpy.called).to.equal(true);
  });

});
