// tslint:disable jsx-use-translation-function
import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import { createMockIntlContext, createMockIntlProp } from '../../../utilities/testing';

import { ApiKeyDetails } from './api-key-details';
import { CreateDirectory } from './create-directory';
import { CreateDirectoryDrawerImpl } from './create-directory-drawer';

describe('User Provisioning create directory drawer', () => {

  const shallowUserProvisioningDrawer = () => {
    return shallow(
      <CreateDirectoryDrawerImpl
        match={{ url: '', path: '', isExact: true, params: { orgId: '1234' } }}
        onCloseDrawer={sinon.spy()}
        location={{} as any}
        history={{} as any}
        intl={createMockIntlProp()}
      />,
      createMockIntlContext(),
    );
  };

  it('should render create directory page correctly', () => {
    const wrapper = shallowUserProvisioningDrawer();
    const createPage = wrapper.find(CreateDirectory) as any;
    expect(createPage.exists()).to.equal(true);
  });

  it('should render api key details page correctly', () => {
    const wrapper = shallowUserProvisioningDrawer();
    wrapper.setState({ page: 'api-key-details' });
    wrapper.update();
    const createPage = wrapper.find(CreateDirectory) as any;
    expect(createPage.exists()).to.equal(false);
    const detailsPage = wrapper.find(ApiKeyDetails) as any;
    expect(detailsPage.exists()).to.equal(true);
  });

});
