import { ApolloQueryResult } from 'apollo-client';
import * as React from 'react';
import { graphql, OptionProps, QueryResult } from 'react-apollo';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import AkButton from '@atlaskit/button';
import { gridSize as akGridSize } from '@atlaskit/theme';

import {
  AnalyticsClientProps,
  scimApiKeyRegeneratedTrackData,
  ScreenEventSender,
  userProvisioningCancelRegenerateApiKeyEventData,
  userProvisioningRegenerateApiKeyEventData,
  userProvisioningRegenerateApiKeyScreenEvent,
  withAnalyticsClient,
} from 'common/analytics';
import { ButtonGroup as ButtonGroupBase } from 'common/button-group';
import { createErrorIcon, errorFlagMessages, GenericError } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';

import { RegenerateApiKeyQuery, RegenerateDirectoryApiKeyMutation, RegenerateDirectoryApiKeyMutationVariables } from '../../../schema/schema-types';
import directoryQuery from '../directory.query.graphql';
import regenerateDirectoryApiKeyMutation from './regenerate-api-key.mutation.graphql';
import regenerateApiKeyQuery from './regenerate-api-key.query.graphql';

const messages = defineMessages({
  title: {
    id: 'organization.userProvisioning.regenerateKeyDrawer.regenerateKeyPage.title',
    defaultMessage: 'Regenerate directory API key',
    description: 'The title of a page for regenerating a SCIM (http://www.simplecloud.info/) API key',
  },
  description: {
    id: 'organization.userProvisioning.regenerateKeyDrawer.regenerateKeyPage.description',
    defaultMessage: 'Regenerating a new API key will disable the existing key. Update the credentials in your identity provider with the new details to continue syncing with your identity provider.',
    description: 'A paragraph that explains what happens when a SCIM (http://www.simplecloud.info/) API key is regenerated',
  },
  regenerateButton: {
    id: 'organization.userProvisioning.regenerateKeyDrawer.regenerateKeyPage.regenerateButton',
    defaultMessage: 'Regenerate key',
    description: 'Text for a button that regenerates a SCIM API key and takes people to the next step in a flow',
  },
  backButton: {
    id: 'organization.userProvisioning.regenerateKeyDrawer.regenerateKeyPage.backButton',
    defaultMessage: 'Back',
    description: 'Text for a button that cancels and exits the regenerate API key flow',
  },
  regenerateApiKeyErrorDescription: {
    id: 'organization.userProvisioning.regenerateKeyDrawer.regenerateKeyPage.error.description',
    defaultMessage: 'There was an issue regenerating your API key. Please try again later.',
    description: 'Description for an error dialog that shows when there was a problem regenerating an API key',
  },
});

const Description = styled.div`
  margin-top: ${akGridSize() * 2}px;
  margin-bottom: ${akGridSize() - 4}px;
`;

const ButtonGroup = styled(ButtonGroupBase) `
  margin-top: ${akGridSize() * 4}px;
`;

interface State {
  isLoading: boolean;
}

interface OwnProps {
  orgId: string;
  goToDetailsPage(name: string, apiKey: string, baseUrl: string): void;
  onCloseDrawer(): void;
}

interface DerivedProps {
  loading: boolean;
  error?: QueryResult['error'];
  directories?: RegenerateApiKeyQuery['organization']['externalDirectories'];
}

interface RegenerateApiKeyMutationProps {
  regenerateDirectoryApiKey(orgId: string, directoryId: string): Promise<false | ApolloQueryResult<RegenerateDirectoryApiKeyMutation>>;
}

type RegenerateApiKeyProps = RegenerateApiKeyMutationProps & OwnProps & InjectedIntlProps & FlagProps & AnalyticsClientProps & DerivedProps;

export class RegenerateApiKeyImpl extends React.Component<RegenerateApiKeyProps, State> {
  public state: Readonly<State> = { isLoading: false };

  public render() {
    if (this.props.loading) {
      return null;
    }

    if (this.props.error) {
      return <GenericError />;
    }

    const { isLoading } = this.state;

    return (
      <ScreenEventSender event={userProvisioningRegenerateApiKeyScreenEvent()} >
        <FormattedMessage {...messages.title} tagName="h1" />
        <Description>
          <FormattedMessage {...messages.description} />
        </Description>
        <form onSubmit={this.onSubmit}>
          <ButtonGroup alignment="right">
            <AkButton
              appearance="default"
              onClick={this.onCancel}
            >
              <FormattedMessage {...messages.backButton} />
            </AkButton>
            <AkButton
              appearance="primary"
              type="submit"
              isLoading={isLoading}
            >
              <FormattedMessage {...messages.regenerateButton} />
            </AkButton>
          </ButtonGroup>
        </form>
      </ScreenEventSender>
    );
  }

  private onCancel = () => {
    const { onCloseDrawer, orgId, analyticsClient } = this.props;

    analyticsClient.sendUIEvent({
      orgId,
      data: userProvisioningCancelRegenerateApiKeyEventData(),
    });

    onCloseDrawer();
  }

  private onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    const { analyticsClient, orgId, goToDetailsPage, regenerateDirectoryApiKey, showFlag, intl: { formatMessage } } = this.props;
    this.setState({ isLoading: true });

    analyticsClient.sendUIEvent({
      orgId,
      data: userProvisioningRegenerateApiKeyEventData(),
    });

    const { name, baseUrl, id: directoryId } = this.props.directories![0];

    try {
      const result = await regenerateDirectoryApiKey(orgId, directoryId);
      if (!result) {
        throw new Error('regenerateDirectoryApiKey returned bad result');
      }
      analyticsClient.sendTrackEvent({
        orgId,
        data: scimApiKeyRegeneratedTrackData(),
      });
      const { apiKey } = result.data.regenerateDirectoryApiKey;
      goToDetailsPage(name, apiKey, baseUrl);
    } catch (error) {
      this.setState({ isLoading: false });
      showFlag({
        autoDismiss: true,
        icon: createErrorIcon(),
        id: `common.copy.text.field.error.${Date.now()}`,
        title: formatMessage(errorFlagMessages.title),
        description: formatMessage(messages.regenerateApiKeyErrorDescription),
      });
    }

  }
}

const getDirectories = (componentProps: OptionProps<OwnProps, RegenerateApiKeyQuery>): DerivedProps => {
  if (!componentProps || !componentProps.data || componentProps.data.loading) {
    return {
      loading: true,
    };
  }

  if (componentProps.data.error || !componentProps.data.organization) {
    return {
      loading: false,
      error: componentProps.data.error,
    };
  }

  return {
    loading: false,
    directories: componentProps.data.organization.externalDirectories,
  };
};

const withDirectoriesQuery = graphql<OwnProps, RegenerateApiKeyQuery, {}, DerivedProps>(
  regenerateApiKeyQuery,
  {
    options: (props) => ({ variables: { id: props.orgId } }),
    props: getDirectories,
  },
);

const withRegenerateApiKeyMutation = graphql<DerivedProps & OwnProps, RegenerateDirectoryApiKeyMutation, RegenerateDirectoryApiKeyMutationVariables, RegenerateApiKeyMutationProps>(
  regenerateDirectoryApiKeyMutation,
  {
    props: ({ mutate }) => ({
      regenerateDirectoryApiKey: async (orgId: string, directoryId: string) => mutate instanceof Function && mutate({
        variables: { directoryId },
        refetchQueries: [{
          query: directoryQuery,
          variables: {
            id: orgId,
          },
        }],
      }),
    }),
  },
);

export const RegenerateApiKey =
  withDirectoriesQuery(
    withRegenerateApiKeyMutation(
      injectIntl(
        withFlag(
          withAnalyticsClient(
            RegenerateApiKeyImpl,
          ),
        ),
      ),
    ),
  )
;
