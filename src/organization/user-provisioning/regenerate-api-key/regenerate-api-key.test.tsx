// tslint:disable jsx-use-translation-function
import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import * as sinon from 'sinon';

import AkButton from '@atlaskit/button';

import { GenericError } from 'common/error';

import { createMockIntlContext, createMockIntlProp } from '../../../utilities/testing';
import { RegenerateApiKeyImpl } from './regenerate-api-key';

describe('User Provisioning regenerate API key', () => {

  const sandbox = sinon.sandbox.create();
  let closeDrawerSpy;
  let regenerateApiKeyStub;
  let goToDetailsPageSpy;
  let showFlagSpy;
  let sendUIEventSpy;
  let sendTrackEventSpy;

  beforeEach(() => {
    closeDrawerSpy = sandbox.spy();
    regenerateApiKeyStub = sandbox.stub();
    goToDetailsPageSpy = sandbox.spy();
    showFlagSpy = sandbox.spy();
    sendUIEventSpy = sandbox.spy();
    sendTrackEventSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  const shallowUserProvisioningRegenerateApiKeyPage = (error?) => {
    const directories = [
      {
        id: '1234',
        name: 'My key',
        baseUrl: 'https://test.atlassian.net',
        creationDate: '2018-08-02T20:07:30.610117Z',
      },
    ];

    return shallow(
      <RegenerateApiKeyImpl
        analyticsClient={{ sendUIEvent: sendUIEventSpy, sendTrackEvent: sendTrackEventSpy } as any}
        orgId="1234"
        loading={false}
        error={error}
        directories={directories as any}
        onCloseDrawer={closeDrawerSpy}
        regenerateDirectoryApiKey={regenerateApiKeyStub}
        goToDetailsPage={goToDetailsPageSpy}
        showFlag={showFlagSpy}
        hideFlag={() => null}
        intl={createMockIntlProp()}
      />,
      createMockIntlContext(),
    );
  };

  it('should render UI elements correctly', () => {
    const wrapper = shallowUserProvisioningRegenerateApiKeyPage();
    const text = wrapper.find(FormattedMessage) as any;
    expect(text).to.have.length(4);
    expect(text.at(0).props().defaultMessage).to.contain('Regenerate directory API key');
  });

  it('should render error', () => {
    const wrapper = shallowUserProvisioningRegenerateApiKeyPage({ message: 'yeaowzers' });
    const emptyState = wrapper.find(GenericError) as any;
    expect(emptyState.exists()).to.equal(true);
  });

  it('should close drawer', () => {
    const wrapper = shallowUserProvisioningRegenerateApiKeyPage();
    const button = wrapper.find(AkButton);
    expect(button).to.have.length(2);
    button.at(0).simulate('click');
    expect(closeDrawerSpy.called).to.equal(true);
  });

  it('should send UI event on close drawer', () => {
    const wrapper = shallowUserProvisioningRegenerateApiKeyPage();
    const button = wrapper.find(AkButton);
    expect(button).to.have.length(2);
    button.at(0).simulate('click');
    expect(sendUIEventSpy.called).to.equal(true);
  });

  it('should go to next page', async () => {
    const expectedItems = {
      data: {
        regenerateDirectoryApiKey: {
          apiKey: '123456',
        },
      },
    };
    const promise = Promise.resolve(expectedItems);
    regenerateApiKeyStub.onFirstCall().returns(promise);

    const wrapper = shallowUserProvisioningRegenerateApiKeyPage();

    const form = wrapper.find('form').at(0);
    form.simulate('submit', { preventDefault: () => null });

    await promise;

    expect(goToDetailsPageSpy.called).to.equal(true);
    expect(sendTrackEventSpy.called).to.equal(true);
  });

});
