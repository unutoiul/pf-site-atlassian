
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';
import { BrowserRouter, MemoryRouter, Route, Switch } from 'react-router-dom';

import AkPage from '@atlaskit/page';

import { FlagProvider } from 'common/flag';

import { createApolloClient } from '../../../apollo-client';

import { ApiKeyDetails } from '../create-directory/api-key-details';
import { Page, RegenerateApiKeyDrawer } from './regenerate-api-key-drawer';
import regenerateApiKeyQuery from './regenerate-api-key.query.graphql';

const client = createApolloClient();
const regenApiKeyOrgId = 'orgid';

storiesOf('Organization|User Provisioning/Regenerate API Key Drawer', module)
  .add('Default', () => {
    const organization = {
      organization: {
        __typename: 'Organization',
        id: regenApiKeyOrgId,
        externalDirectories: [
          {
            __typename: 'ExternalDirectory',
            id: '1234',
            name: 'Acme test',
            baseUrl: 'https://test.atlassian.net',
            creationDate: '2018-08-02T20:07:30.610117Z',
          },
        ],
      },
    };

    client.writeQuery({
      query: regenerateApiKeyQuery,
      variables: {
        id: regenApiKeyOrgId,
      },
      data: organization,
    });

    return (
      <ApolloProvider client={client}>
        <IntlProvider locale="en">
          <BrowserRouter>
            <Switch location={{ pathname: `/o/${regenApiKeyOrgId}/user-provisioning`, search: '', state: undefined, hash: '' }}>
              <Route path="/o/:orgId/user-provisioning">
                <FlagProvider>
                  <AkPage>
                    <RegenerateApiKeyDrawer onCloseDrawer={() => null} />
                  </AkPage>
                </FlagProvider>
              </Route>
            </Switch>
          </BrowserRouter>
        </IntlProvider>
      </ApolloProvider>
    );
  })
  .add('Second page', () => {
    return (
      <ApolloProvider client={client}>
        <IntlProvider locale="en">
          <MemoryRouter>
            <FlagProvider>
              <AkPage>
                <Page>
                  <ApiKeyDetails
                    directoryName="Acme test"
                    apiKey="X123-Y5"
                    baseUrl="hamburger.com"
                    orgId="12354"
                    onCloseDrawer={() => null}
                  />
                </Page>
              </AkPage>
            </FlagProvider>
          </MemoryRouter>
        </IntlProvider>
      </ApolloProvider>
    );
  })
;
