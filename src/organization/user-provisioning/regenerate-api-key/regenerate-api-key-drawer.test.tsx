// tslint:disable jsx-use-translation-function
import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import { createMockIntlContext, createMockIntlProp } from '../../../utilities/testing';

import { ApiKeyDetails } from '../create-directory/api-key-details';
import { RegenerateApiKey } from './regenerate-api-key';
import { RegenerateApiKeyDrawerImpl } from './regenerate-api-key-drawer';

describe('User Provisioning regenerate API key drawer', () => {

  const shallowUserProvisioningDrawer = () => {
    return shallow(
      <RegenerateApiKeyDrawerImpl
        match={{ url: '', path: '', isExact: true, params: { orgId: '1234' } }}
        onCloseDrawer={sinon.spy()}
        location={{} as any}
        history={{} as any}
        intl={createMockIntlProp()}
      />,
      createMockIntlContext(),
    );
  };

  it('should render regenerate API key page correctly', () => {
    const wrapper = shallowUserProvisioningDrawer();
    const createPage = wrapper.find(RegenerateApiKey) as any;
    expect(createPage.exists()).to.equal(true);
  });

  it('should render API key details page correctly', () => {
    const wrapper = shallowUserProvisioningDrawer();
    wrapper.setState({ page: 'api-key-details' });
    wrapper.update();
    const createPage = wrapper.find(RegenerateApiKey) as any;
    expect(createPage.exists()).to.equal(false);
    const detailsPage = wrapper.find(ApiKeyDetails) as any;
    expect(detailsPage.exists()).to.equal(true);
  });

});
