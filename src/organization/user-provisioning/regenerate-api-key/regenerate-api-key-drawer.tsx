import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';
import styled from 'styled-components';

import { ProgressTracker as AkProgressTracker } from '@atlaskit/progress-tracker';
import { gridSize as akGridSize } from '@atlaskit/theme';

import { OrgRouteProps } from '../../routes';
import { ApiKeyDetails } from '../create-directory/api-key-details';
import { RegenerateApiKey } from './regenerate-api-key';

const messages = defineMessages({
  step1: {
    id: 'organization.userProvisioning.regenerateKeyDrawer.step1',
    defaultMessage: 'Regenerate key',
    description: 'Key refers to a SCIM (http://www.simplecloud.info/) API key',
  },
  step2: {
    id: 'organization.userProvisioning.regenerateKeyDrawer.step2',
    defaultMessage: 'API key',
    description: 'Key refers to a SCIM (http://www.simplecloud.info/) API key',
  },
});

export const Page = styled.div`
  margin: 0 auto;
  width:  ${akGridSize() * 50}px;
`;

interface State {
  page: 'regenerate-api-key' | 'api-key-details';
  name?: string;
  apiKey?: string;
  baseUrl?: string;
}

interface OwnProps {
  onCloseDrawer(): void;
}

type InProps = InjectedIntlProps & RouteComponentProps<OrgRouteProps> & OwnProps;

export class RegenerateApiKeyDrawerImpl extends React.Component<InProps, State> {
  public state: Readonly<State> = {
    page: 'regenerate-api-key',
  };

  public render() {
    const { intl: { formatMessage }, onCloseDrawer, match: { params: { orgId } } } = this.props;

    const progressTrackerItems = [
      {
        id: 'regenerate-api-key',
        label: formatMessage(messages.step1),
        percentageComplete: this.state.page === 'regenerate-api-key' ? 0 : 100,
        status: this.state.page === 'regenerate-api-key' ? 'current' : 'visited',
      },
      {
        id: 'api-key-details',
        label: formatMessage(messages.step2),
        percentageComplete: 0,
        status: this.state.page === 'regenerate-api-key' ? 'unvisited' : 'current',
      },
    ];

    return (
      <Page>
        <AkProgressTracker items={progressTrackerItems} />
        {this.state.page === 'regenerate-api-key' ?
          <RegenerateApiKey
            onCloseDrawer={onCloseDrawer}
            goToDetailsPage={this.goToDetailsPage}
            orgId={orgId}
          /> :
          <ApiKeyDetails
            onCloseDrawer={onCloseDrawer}
            directoryName={this.state.name!}
            apiKey={this.state.apiKey!}
            baseUrl={this.state.baseUrl!}
            orgId={orgId}
          />
        }
      </Page>
    );
  }

  private goToDetailsPage = (name: string, apiKey: string, baseUrl: string) => {
    if (name && apiKey && baseUrl) {
      this.setState({ page: 'api-key-details', name, apiKey, baseUrl });
    }
  }

}

export const RegenerateApiKeyDrawer = withRouter<OwnProps>(injectIntl<InProps>(RegenerateApiKeyDrawerImpl));
