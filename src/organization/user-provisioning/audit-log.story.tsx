
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';
import { MemoryRouter } from 'react-router-dom';

import AkPage from '@atlaskit/page';

import { createApolloClient } from '../../apollo-client';
import { createMockIntlProp } from '../../utilities/testing';
import { AuditLogImpl } from './audit-log';

const auditLog = {
  startIndex: 0,
  itemsPerPage: 5,
  totalResults: 10,
  logEntries: [
    { __typename: 'LogEntries', createdOn: '2018-08-02T18:07:30.610117Z', error: 'Something went wrong with a very long message what will happen zomg. But it\'s not long enough better add more text' },
    { __typename: 'LogEntries', createdOn: '2018-08-02T18:07:30.610117Z', error: 'Something went wrong' },
    { __typename: 'LogEntries', createdOn: '2018-08-02T18:07:30.610117Z', error: 'Something went wrong' },
    { __typename: 'LogEntries', createdOn: '2018-08-02T18:07:30.610117Z', error: 'Something went wrong' },
    { __typename: 'LogEntries', createdOn: '2018-08-02T18:07:30.610117Z', error: 'Something went wrong' },
    { __typename: 'LogEntries', createdOn: '2018-08-02T18:07:30.610117Z', error: 'Something went wrong' },
    { __typename: 'LogEntries', createdOn: '2018-08-02T18:07:30.610117Z', error: 'Something went wrong' },
    { __typename: 'LogEntries', createdOn: '2018-08-02T19:07:30.610117Z', error: 'Something went wrong' },
    { __typename: 'LogEntries', createdOn: '2018-08-02T20:07:30.610117Z', error: 'Something went wrong' },
    { __typename: 'LogEntries', createdOn: '2018-08-02T21:07:30.610117Z', error: 'Something went wrong' },
  ],
};

const auditLogOrgId = 'myorg1234';

const getPage = (loading, error, logs?) => {
  const client = createApolloClient();
  // tslint:disable-next-line:no-empty
  const noop = () => {};

  return (
    <ApolloProvider client={client}>
      <IntlProvider locale="en">
        <MemoryRouter>
          <AkPage>
            <AuditLogImpl
              loading={loading}
              error={error}
              auditLog={logs}
              match={{ params: { orgId: auditLogOrgId }, path: '', url: '', isExact: true }}
              location={{ search: '', pathname: '', state: '', hash: '' }}
              history={{ createHref: null as any, listen: null as any, block: null as any, goForward: null as any, goBack: null as any, go: null as any, push: null as any, replace: noop, length: 0, action: null as any, location: null as any }}
              intl={createMockIntlProp()}
            />
          </AkPage>
        </MemoryRouter>
      </IntlProvider>
    </ApolloProvider>
  );
};

storiesOf('Organization|User Provisioning/Tab - Audit Log', module)
  .add('Default', () => getPage(false, false, auditLog))
  .add('Empty', () => getPage(false, false, {}))
  .add('Loading', () => getPage(true, false))
  .add('Error', () => getPage(false, true))
;
