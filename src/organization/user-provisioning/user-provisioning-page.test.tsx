import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { spy } from 'sinon';

import AkSectionMessage from '@atlaskit/section-message';

import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import { UserProvisioningPageImpl } from './user-provisioning-page';

describe('UserProvisioningPage', () => {
  const shallowWrapper = ({ showDrawer, linkedSitesData }: {
    showDrawer?: UserProvisioningPageImpl['props']['showDrawer'],
    linkedSitesData?: UserProvisioningPageImpl['props']['linkedSitesData'],
  } = {}) => shallow(
    (
      <UserProvisioningPageImpl
        analyticsClient={{ sendUIEvent: () => null } as any}
        history={{} as any}
        location={{} as any}
        match={{ params: { orgId: 'test' } } as any}
        showDrawer={showDrawer || spy()}
        intl={createMockIntlProp()}
        loading={false}
        linkedSitesData={linkedSitesData || { loading: false, sites: [] }}
      />
    ),
    createMockIntlContext(),
  );

  describe('add site banner', () => {
    function expectBannerToBePresent(wrapper: ShallowWrapper) {
      expect(wrapper.find(AkSectionMessage)).to.have.lengthOf(1);
      expect(wrapper.find(AkSectionMessage).prop('title')).to.equal('Set up default access');
    }

    function expectBannerToNotBePresent(wrapper: ShallowWrapper) {
      expect(wrapper.find(AkSectionMessage)).to.have.lengthOf(0);
    }

    function invokeAddSiteBannerAction(wrapper: ShallowWrapper) {
      const callback = wrapper.find(AkSectionMessage).prop('actions')[0].onClick;
      callback();
      wrapper.update();

    }

    it('should not have a banner while linked sites data is still loading', () => {
      const wrapper = shallowWrapper({ linkedSitesData: { loading: true } });

      expectBannerToNotBePresent(wrapper);
    });

    it('should not have a banner if linked sites data loading erred', () => {
      const wrapper = shallowWrapper({ linkedSitesData: { loading: false, error: {} as any } });

      expectBannerToNotBePresent(wrapper);
    });

    it('should not have a banner if org has at least one linked site', () => {
      const wrapper = shallowWrapper({ linkedSitesData: { loading: false, sites: [{} as any] } });

      expectBannerToNotBePresent(wrapper);
    });

    it('should have a banner if org has no linked sites', () => {
      const wrapper = shallowWrapper({ linkedSitesData: { loading: false, sites: [] } });

      expectBannerToBePresent(wrapper);
    });

    it('should have an action to show org/site linking drawer', () => {
      const showSpy = spy();
      const wrapper = shallowWrapper({
        linkedSitesData: { loading: false, sites: [] },
        showDrawer: showSpy,
      });

      expectBannerToBePresent(wrapper);
      invokeAddSiteBannerAction(wrapper);

      expect(showSpy.callCount).to.equal(1);
    });
  });
});
