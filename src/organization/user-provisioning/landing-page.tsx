import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';
import styled from 'styled-components';

import AkButton from '@atlaskit/button';
import { gridSize as akGridSize } from '@atlaskit/theme';

import { AnalyticsClientProps, atlassianAccessLearnMoreEventData, ScreenEventSender, UIEvent, userProvisioningIntroCreateDirectoryEventData, userProvisioningIntroScreen, userProvisioningIntroScreenEvent, withAnalyticsClient } from 'common/analytics';
import { ShowDrawerProps, withShowDrawer } from 'common/drawer';
import { GenericError } from 'common/error';
import { IdentityManagerLearnMoreButton } from 'common/identity-manager-learn-more-button';
import { plugImage } from 'common/images';
import { CenteredPageLayout } from 'common/page-layout';

import { UserProvisioningLandingPageQuery } from '../../schema/schema-types/document-types';
import { OrgBreadcrumbs } from '../breadcrumbs';
import { VerifyDomainSectionMessage } from '../domain-claim/verify-domain-section-message';
import { hasAnyDomainVerified } from '../organizations/organizations.util';
import { OrgRouteProps } from '../routes';
import { GeneralInstructionsLink, OktaLink } from './external-links';
import userProvisioningLandingPageQuery from './user-provisioning-landing-page.query.graphql';

const messages = defineMessages({
  title: {
    id: 'organization.userProvisioning.landing.title',
    defaultMessage: 'Sync users from your external directory',
  },
  description: {
    id: 'organization.userProvisioning.landing.description',
    defaultMessage: 'Automatically provision users and groups from your identity provider. Users from verified domains will be synced from your identity provider. Synced groups will be available in all your sites to manage access to different products.',
  },
  createDirectory: {
    id: 'organization.userProvisioning.landing.createDirectory',
    defaultMessage: 'Create a directory',
    description: 'button for creating a user directory to sync with an identity provider',
  },
  instructions: {
    id: 'organization.userProvisioning.landing.instructions',
    defaultMessage: 'See step-by-step instructions for {oktaLink}. Follow {generalInstructionsLink} for other identity providers.',
    description: 'Instructions for configuring SCIM with different identity providers',
  },
  atlassianAccessMessage: {
    id: 'organization.userProvisioning.landing.atlassianAccessMessage',
    defaultMessage: 'User provisioning is an Atlassian Access feature. Before you can set it up, you need to have verified a domain and subscribed to Atlassian Access.',
    description: 'User provisioning should be translated like organization.userProvisioning.title. Atlassian Access is a product name and should not be translated. Verified domain refers to the act of verifying that a domain such as www.atlassian.com belongs to you.',
  },
  verifyDomain: {
    id: 'organization.userProvisioning.landing.verifyDomainMessage',
    defaultMessage: 'To provision users from your identity provider, you need to verify a domain.',
    description: 'Provisioning is creating and syncing users from an identity provider like Active Directory. Domain is a web domain like www.atlassian.com',
  },
});

const Container = styled.div`
  text-align: center;

  img {
    margin: ${akGridSize() * 3}px 0 ${akGridSize() * 4}px;
    max-width: ${akGridSize() * 48}px;
  }
`;

const DescriptionWrapper = styled.div`
  p {
    margin-top: 0;
  }
`;

const AtlassianAccessMessage = styled.div`
  p {
    margin-top: ${akGridSize() * 2}px;
  }
`;

const InstructionsWrapper = styled.div`
  margin-top: ${akGridSize() * 2}px;
`;

const CtaWrapper = styled.div`
  margin-top: ${akGridSize() * 4}px;
`;

const LearnMoreButtonWrapper = styled.div`
  margin-top: ${akGridSize() * 2}px;
`;

type AllProps = ChildProps<AnalyticsClientProps & RouteComponentProps<OrgRouteProps> & InjectedIntlProps & ShowDrawerProps, UserProvisioningLandingPageQuery>;

export class LandingPageImpl extends React.Component<AllProps> {
  public render() {
    const { data } = this.props;
    if (!data || data.loading) {
      return null;
    }

    return (
      <ScreenEventSender event={userProvisioningIntroScreenEvent()} >
        <CenteredPageLayout breadcrumbs={<OrgBreadcrumbs />}>
          {this.getContent()}
        </CenteredPageLayout>
      </ScreenEventSender>
    );
  }

  private getContent = () => {
    const { data } = this.props;
    if (!data) {
      return null;
    }

    if (data.error) {
      return <GenericError />;
    }

    return (
      <Container>
        <FormattedMessage {...messages.title} tagName="h1" />
        <img src={plugImage} role="presentation" alt={undefined} />
        <DescriptionWrapper>
          <FormattedMessage {...messages.description} tagName="p" />
        </DescriptionWrapper>
        {data.products && !data.products.identityManager && (
          <AtlassianAccessMessage>
            <FormattedMessage {...messages.atlassianAccessMessage} tagName="p" />
          </AtlassianAccessMessage>
        )}
        {this.getCtaSection()}
      </Container>
    );
  }

  private getCtaSection = () => {
    const { data, intl: { formatMessage } } = this.props;

    const hasAtlassianAccess = !!(data && data.products && data.products.identityManager);
    const hasVerifiedDomains = !!(data && data.organization && hasAnyDomainVerified(data.organization.domainClaim.domains));

    return hasAtlassianAccess ?
      hasVerifiedDomains ? (
        <React.Fragment>
          <InstructionsWrapper>
            <FormattedMessage {...messages.instructions} values={{ oktaLink: OktaLink, generalInstructionsLink: GeneralInstructionsLink }} />
          </InstructionsWrapper>
          <CtaWrapper>
            <AkButton appearance="primary" onClick={this.openCreateDirectoryDrawer}>
              <FormattedMessage {...messages.createDirectory} />
            </AkButton>
          </CtaWrapper>
        </React.Fragment>
      ) : (
          <CtaWrapper>
            <VerifyDomainSectionMessage domainPrompt={formatMessage(messages.verifyDomain)} />
          </CtaWrapper>
        ) : this.getAtlassianAccessLearnMoreButton();
  }

  private getAtlassianAccessLearnMoreButton = () => {
    const { match: { params: { orgId } } } = this.props;
    const event: UIEvent = {
      orgId,
      data: atlassianAccessLearnMoreEventData(userProvisioningIntroScreen),
    };

    return (
      <LearnMoreButtonWrapper>
        <IdentityManagerLearnMoreButton
          isPrimary={true}
          analyticsUIEvent={event}
        />
      </LearnMoreButtonWrapper>
    );
  }

  private openCreateDirectoryDrawer = () => {
    const { analyticsClient, showDrawer, match: { params: { orgId } } } = this.props;

    analyticsClient.sendUIEvent({
      orgId,
      data: userProvisioningIntroCreateDirectoryEventData(),
    });

    showDrawer('USER_PROVISIONING_CREATE_DIRECTORY');
  }
}

const withCurrentOrgProducts = graphql<RouteComponentProps<OrgRouteProps>, UserProvisioningLandingPageQuery>(userProvisioningLandingPageQuery, {
  options: (props) => ({ variables: { id: props.match.params.orgId } }),
});

export const LandingPage =
  withRouter(
    withCurrentOrgProducts(
      withShowDrawer(
        withAnalyticsClient(
          injectIntl(
            LandingPageImpl,
          ),
        ),
      ),
    ),
  )
  ;
