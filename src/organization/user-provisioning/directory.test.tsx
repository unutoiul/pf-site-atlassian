import { ApolloError } from 'apollo-client';
import { assert, expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import * as sinon from 'sinon';

import AkButton from '@atlaskit/button';

import { DirectoryQuery } from '../../schema/schema-types';
import { createMockIntlProp } from '../../utilities/testing';
import { DirectoryImpl, DirectoryProps } from './directory';
import { RemoveDirectoryDialog } from './remove-directory-dialog';

describe('User provisioning Directory', () => {

  const sandbox = sinon.sandbox.create();
  const directoriesMock = [{ id: '1234', name: 'Acme test', creationDate: 'Jan 7, 1980' } as any];
  const successfulMutation = Promise.resolve({
    data: { removeDirectory: true },
  });

  let mutateMock;
  let showFlagSpy;
  let hideFlagSpy;
  let historyReplaceSpy;
  let sendUIEventSpy;
  let sendTrackEventSpy;
  let showRegenApiKeyDrawerMock;

  beforeEach(() => {
    mutateMock = sandbox.stub().returns(successfulMutation);
    showFlagSpy = sandbox.spy();
    hideFlagSpy = sandbox.spy();
    historyReplaceSpy = sandbox.spy();
    sendUIEventSpy = sandbox.spy();
    sendTrackEventSpy = sandbox.spy();
    showRegenApiKeyDrawerMock = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  const shallowDirectory = (directories?: DirectoryQuery['organization']['externalDirectories'], error?: ApolloError) => {
    return shallow<DirectoryProps>((
      <DirectoryImpl
        analyticsClient={{ sendUIEvent: sendUIEventSpy, sendTrackEvent: sendTrackEventSpy } as any}
        loading={false}
        error={error}
        match={{ params: { orgId: '1234-12324' }, isExact: true, path: '', url: '' }}
        history={{ replace: historyReplaceSpy, length: 0, action: null as any, location: null as any, push: null as any, go: null as any, goBack: null as any, goForward: null as any, block: null as any, listen: null as any, createHref: null as any }}
        location={null as any}
        directories={directories}
        showFlag={showFlagSpy}
        hideFlag={hideFlagSpy}
        mutate={mutateMock}
        showDrawer={showRegenApiKeyDrawerMock}
        intl={createMockIntlProp()}
      />
    ));
  };

  it('should render with directory', () => {
    const wrapper = shallowDirectory(directoriesMock);
    const text = wrapper.find(FormattedMessage);
    expect(text.exists()).to.equal(true);
    const revokeButton = wrapper.find(AkButton);
    expect(revokeButton.exists()).to.equal(true);
  });

  it('should render error state', () => {
    const error = new ApolloError({});
    const wrapper = shallowDirectory(undefined, error);
    const text = wrapper.find(FormattedMessage);
    expect(text.exists()).to.equal(false);
  });

  it('should not render when directory is undefined', () => {
    const wrapper = shallowDirectory();
    const text = wrapper.find(FormattedMessage);
    expect(text.exists()).to.equal(false);
  });

  it('should call open drawer mutation when regenerate is clicked', () => {
    const wrapper = shallowDirectory(directoriesMock);
    const revokeButton = wrapper.find(AkButton);
    expect(revokeButton).to.have.lengthOf(2);
    revokeButton.first().simulate('click');
    expect(showRegenApiKeyDrawerMock.called).to.equal(true);
  });

  it('should open remove directory dialog when revoke is clicked', () => {
    const wrapper = shallowDirectory(directoriesMock);
    const revokeButton = wrapper.find(AkButton);
    expect(revokeButton).to.have.lengthOf(2);
    revokeButton.last().simulate('click');
    wrapper.update();
    expect(wrapper.find(RemoveDirectoryDialog).props().isOpen).to.equal(true);
  });

  describe('onRemoveDirectory', () => {
    it('should set the correct loading state', async (done) => {
      mutateMock = () => ({ then: () => ({ catch: () => ({}) }) });
      // tslint:disable-next-line:no-empty
      const doNothing = () => {};

      const wrapper = shallowDirectory(directoriesMock);
      const instance = wrapper.instance() as DirectoryImpl;

      instance.showRemoveDirectoryDialog();
      instance.removeDirectory()
        .then(doNothing)
        .catch(doNothing);

      expect(wrapper.state('removeDirectoryDialog').isMutationLoading).to.equal(true);
      done();
    });

    it('should send UI event', async (done) => {
      mutateMock = () => ({ then: () => ({ catch: () => ({}) }) });
      // tslint:disable-next-line:no-empty
      const doNothing = () => {};

      const wrapper = shallowDirectory(directoriesMock);
      const instance = wrapper.instance() as DirectoryImpl;

      instance.showRemoveDirectoryDialog();
      instance.removeDirectory()
        .then(doNothing)
        .catch(doNothing);

      expect(sendUIEventSpy.called).to.equal(true);
      done();
    });
  });

  describe('on success', () => {
    it('should show a flag', async () => {
      const wrapper = shallowDirectory(directoriesMock);
      const instance = wrapper.instance() as DirectoryImpl;

      instance.showRemoveDirectoryDialog();
      await instance.removeDirectory();

      await successfulMutation;
      expect(showFlagSpy.called).to.equal(true);
    });

    it('should call mutate', async () => {
      const wrapper = shallowDirectory(directoriesMock);
      const instance = wrapper.instance() as DirectoryImpl;

      instance.showRemoveDirectoryDialog();
      await instance.removeDirectory();

      await successfulMutation;
      expect(mutateMock.called).to.equal(true);
    });

    it('should send track event', async () => {
      const wrapper = shallowDirectory(directoriesMock);
      const instance = wrapper.instance() as DirectoryImpl;

      instance.showRemoveDirectoryDialog();
      await instance.removeDirectory();

      await successfulMutation;
      expect(sendTrackEventSpy.called).to.equal(true);
    });
  });

  describe('on failure', () => {
    it('should reset the loading state', async () => {
      const failedMutation = Promise.reject(new Error('Something bad'));
      mutateMock = sandbox.stub().returns(failedMutation);

      const wrapper = shallowDirectory(directoriesMock);
      const instance = wrapper.instance() as DirectoryImpl;

      instance.showRemoveDirectoryDialog();
      await instance.removeDirectory();

      await failedMutation
        .then(() => assert.fail())
        .catch(e => {
          expect(e instanceof Error).to.equal(true);
        });

      expect(wrapper.state('removeDirectoryDialog').isMutationLoading).to.equal(false);
    });

    it('should show a flag', async () => {
      const failedMutation = Promise.reject(new Error('Something bad'));
      const wrapper = shallowDirectory(directoriesMock);
      const instance = wrapper.instance() as DirectoryImpl;

      instance.showRemoveDirectoryDialog();
      await instance.removeDirectory();

      await failedMutation
        .then(() => assert.fail())
        .catch(e => {
          expect(e instanceof Error).to.equal(true);
        });

      expect(showFlagSpy.called).to.equal(true);
    });
  });

});
