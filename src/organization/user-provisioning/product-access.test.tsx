import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import AkEmptyState from '@atlaskit/empty-state';
import AkSpinner from '@atlaskit/spinner';

import { EmptyTableView } from 'common/table';

import { ProductAccessQuery } from '../../schema/schema-types';
import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import { ProductAccessImpl, ProductAccessProps, Products } from './product-access';

describe('User provisioning product access', () => {

  const sandbox = sinon.sandbox.create();
  const linkedSitesMock: ProductAccessQuery = {
    organization: {
      id: 'orgid',
      sites: [
        {
          id: 'siteid',
          siteUrl: '',
          products: [
            {
              name: 'p2',
            },
            {
              name: 'p1',
            },
          ],
        },
      ],
    },
  } as any;

  let showAddSiteDrawerSpy;

  beforeEach(() => {
    showAddSiteDrawerSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  const mountProductAccess = (loading, error, organization?: ProductAccessQuery) => {

    return mount<ProductAccessProps>((
      <ProductAccessImpl
        data={{ loading, error, ...organization } as any}
        match={{ params: { orgId: '1234-12324' }, isExact: true, path: '', url: '' }}
        history={null as any}
        location={{ search: '', pathname: '', state: '', hash: '' }}
        intl={createMockIntlProp()}
        analyticsClient={{} as any}
        showDrawer={showAddSiteDrawerSpy}
      />
    ), createMockIntlContext());
  };

  it('should render with org data', () => {
    const wrapper = mountProductAccess(false, false, linkedSitesMock);

    const products = wrapper.find(Products);
    expect(products.exists()).to.equal(true);
    expect(products.text()).to.equal('p1, p2');
  });

  it('should render empty state', () => {
    const emptySitesMock: ProductAccessQuery = {
      organization: {
        id: 'orgid',
        sites: [],
      },
    } as any;

    const wrapper = mountProductAccess(false, false, emptySitesMock);

    const emptyView = wrapper.find(EmptyTableView);
    expect(emptyView.exists()).to.equal(true);
  });

  it('should render loading state', () => {
    const wrapper = mountProductAccess(true, false);

    const spinner = wrapper.find(AkSpinner);
    expect(spinner.exists()).to.equal(true);
  });

  it('should render error state', () => {
    const wrapper = mountProductAccess(false, true);

    const emptyState = wrapper.find(AkEmptyState);
    expect(emptyState.exists()).to.equal(true);
  });

});
