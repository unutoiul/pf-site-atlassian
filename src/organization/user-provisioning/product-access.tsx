import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';
import styled from 'styled-components';

import AkButton from '@atlaskit/button';
import AkShortcutIcon from '@atlaskit/icon/glyph/shortcut';
import AkSpinner from '@atlaskit/spinner';
import {
  colors as akColors,
  fontSize as akFontSize,
  gridSize as akGridSize,
} from '@atlaskit/theme';

import { AnalyticsClientProps, userProvisioningProductAccessAddSiteEventData, withAnalyticsClient } from 'common/analytics';
import { ShowDrawerProps, withShowDrawer } from 'common/drawer';
import { GenericError } from 'common/error';
import { links } from 'common/link-constants';
import { ExternalLink } from 'common/navigation';
import { Centered } from 'common/styled';
import { EmptyTableView } from 'common/table';

import { ProductAccessQuery } from '../../schema/schema-types';
import { OrgRouteProps } from '../routes';
import productAccessQuery from './product-access.query.graphql';

const messages = defineMessages({
  description: {
    id: 'organization.userProvisioning.productAccess.created',
    defaultMessage: 'Users and groups will automatically be provisioned to Jira and Confluence sites linked to your organization. Set which synced groups automatically get access to products via site access configuration. {learnMoreLink}',
    description: 'Provisioned means the users will be created and synced in Jira and Confluence. Sites are Jira and Confluence sites with their own url like myjirasite.atlassian.net',
  },
  learnMore: {
    id: 'organization.userProvisioning.productAccess.learnMore',
    defaultMessage: 'Learn more',
    description: 'Link that takes user to a page with more details',
  },
  addSite: {
    id: 'organization.userProvisioning.productAccess.addSite',
    defaultMessage: 'Add a site',
    description: 'Adds a Jira, Confluence, or other product site to the users organization',
  },
  noSites: {
    id: 'organization.userProvisioning.productAccess.noSites',
    defaultMessage: 'No sites',
    description: 'Text that shows when no sites are found in the users organization',
  },
  noActiveProducts: {
    id: 'organization.userProvisioning.productAccess.noActiveProducts',
    defaultMessage: 'No active products',
    description: 'Text that shows when no active products are found',
  },
  editProductAccess: {
    id: 'organization.userProvisioning.productAccess.editProductAccess',
    defaultMessage: 'Edit product access {icon}',
    description: 'Link that takes users to a page to change the access users have to that product',
  },
  editProductAccessLabel: {
    id: 'organization.userProvisioning.productAccess.editProductAccessLabel',
    defaultMessage: 'Edit product access',
    description: 'Label for the product access link',
  },
});

const EmptyWrapper = styled(Centered)`
  margin-top: ${akGridSize() * 3}px;
`;

const FlexRow = styled.div`
  display: flex;
  justify-content: space-between;
`;

const DescriptionFlexRow = styled(FlexRow)`
  padding-bottom: ${akGridSize() * 1}px;
`;

const SiteFlexRow = styled(FlexRow)`
  margin-top: ${akGridSize() * 3}px;
`;

const AddSiteContainer = styled.div`
  margin-left: ${akGridSize() * 4}px;
`;

export const Products = styled.div`
  font-weight: 500;
`;

const Url = styled.div`
  color: ${akColors.subtleHeading};
  font-size: ${akFontSize() - 2}px;
`;

const LearnMoreLink = (
  <ExternalLink href={links.external.manageApplicationAccess}>
    <FormattedMessage {...messages.learnMore} />
  </ExternalLink>
);

const EmtpyView = (
  <EmptyWrapper>
    <EmptyTableView>
      <FormattedMessage {...messages.noSites} />
    </EmptyTableView>
  </EmptyWrapper>
);

export type ProductAccessProps = ChildProps<InjectedIntlProps, ProductAccessQuery> & AnalyticsClientProps & RouteComponentProps<OrgRouteProps> & ShowDrawerProps;

export class ProductAccessImpl extends React.Component<ProductAccessProps> {

  public render() {

    if (!this.props.data || (this.props.data && this.props.data.error)) {
      return <GenericError />;
    }

    if (this.props.data.loading) {
      return <AkSpinner />;
    }

    return (
      <React.Fragment>
        {this.getDescription()}
        {this.getSites()}
      </React.Fragment>
    );
  }

  private getDescription = () => {
    return (
      <DescriptionFlexRow>
        <FormattedMessage {...messages.description} values={{ learnMoreLink: LearnMoreLink }} />
        <AddSiteContainer>
          <AkButton appearance="default" onClick={this.openOrgSiteLinkingDrawer}>
            <FormattedMessage {...messages.addSite} />
          </AkButton>
        </AddSiteContainer>
      </DescriptionFlexRow>
    );
  }

  private getSites = () => {
    const { data } = this.props;

    if (!data || !data.organization) {
      return null;
    }

    if (!data.organization.sites || data.organization.sites.length === 0) {
      return EmtpyView;
    }

    const icon = <AkShortcutIcon label={this.props.intl.formatMessage(messages.editProductAccessLabel)} size="small" />;
    const sortedSites = data.organization.sites.slice().sort((a, b) => a.siteUrl.localeCompare(b.siteUrl));

    return sortedSites.map((site) => (
      <SiteFlexRow key={site.id}>
        <div>
          <Products>
            {site.products.length === 0 ?
              <FormattedMessage {...messages.noActiveProducts} />
              : site.products.map(product => product.name).slice().sort().join(', ')}
          </Products>
          <Url>
            {site.siteUrl}
          </Url>
        </div>
        <ExternalLink href={`https://${site.siteUrl}/admin/accessconfig`}>
          <FormattedMessage {...messages.editProductAccess} values={{ icon }}/>
        </ExternalLink>
      </SiteFlexRow>
    ));
  }

  private openOrgSiteLinkingDrawer = () => {
    const { analyticsClient, match: { params: { orgId } } } = this.props;

    analyticsClient.sendUIEvent({
      orgId,
      data: userProvisioningProductAccessAddSiteEventData(),
    });
    this.props.showDrawer('ORG_SITE_LINKING');
  }

}

const withProductAccessQuery = graphql<RouteComponentProps<OrgRouteProps>, ProductAccessQuery>(
  productAccessQuery,
  {
    options: (props) => ({ variables: { id: props.match.params.orgId } }),
  },
);

export const ProductAccess =
  withRouter<{}>(
    withProductAccessQuery(
      withShowDrawer(
        withAnalyticsClient(
          injectIntl(
            ProductAccessImpl,
          ),
        ),
      ),
    ),
  )
;
