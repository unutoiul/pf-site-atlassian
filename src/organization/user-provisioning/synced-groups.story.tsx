
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';

import { createMockIntlProp } from '../../utilities/testing';
import { SyncedGroupsImpl } from './synced-groups';

const getSyncedGroupsPage = (loading, error, groups?) => {
  const noop = () => undefined;

  return (
    <IntlProvider locale="en">
      <SyncedGroupsImpl
        error={error}
        loading={loading}
        groups={groups}
        intl={createMockIntlProp()}
        match={{ params: { orgId: '1234' }, path: '', url: '', isExact: true }}
        location={{ search: '', pathname: '', state: '', hash: '' }}
        history={{ createHref: null as any, listen: null as any, block: null as any, goForward: null as any, goBack: null as any, go: null as any, push: null as any, replace: noop, length: 0, action: null as any, location: null as any }}
      />
    </IntlProvider>
  );
};

storiesOf('Organization|User Provisioning/Tab - Groups', module)
  .add('Default', () => {
    const syncedGroups = {
      groups: [
        { name: 'Koala', membershipCount: 101 },
        { name: 'Wallaby', membershipCount: 2263 },
        { name: 'Quokka', membershipCount: 1 },
      ],
    };

    return getSyncedGroupsPage(false, false, syncedGroups);
  })
  .add('Empty', () => {
    const syncedGroups = {
      groups: [
      ],
    };

    return getSyncedGroupsPage(false, false, syncedGroups);
  })
  .add('Loading', () => {
    return getSyncedGroupsPage(true, false);
  })
  .add('Error', () => {
    return getSyncedGroupsPage(false, true);
  });
