
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import AkPage from '@atlaskit/page';

import { FlagProvider } from 'common/flag';

import { createApolloClient } from '../../apollo-client';
import { ApolloClientStorybookWrapper } from '../../utilities/storybooks';
import { createMockIntlProp } from '../../utilities/testing';
import auditLogQuery from './audit-log.query.graphql';
import directoryQuery from './directory.query.graphql';
import productAccessQuery from './product-access.query.graphql';
import syncedGroupsQuery from './synced-groups.query.graphql';
import { UserProvisioningPageImpl } from './user-provisioning-page';

const logs = {
  __typename: 'LogEntryCollection',
  startIndex: 0,
  itemsPerPage: 50,
  totalResults: 10,
  logEntries: [
    { __typename: 'LogEntries', createdOn: '2018-08-02T18:07:30.610117Z', error: 'Something went wrong with a very long message what will happen zomg. But it\'s not long enough better add more text' },
    { __typename: 'LogEntries', createdOn: '2018-08-02T18:07:30.610117Z', error: 'Something went wrong' },
    { __typename: 'LogEntries', createdOn: '2018-08-02T18:07:30.610117Z', error: 'Something went wrong' },
    { __typename: 'LogEntries', createdOn: '2018-08-02T18:07:30.610117Z', error: 'Something went wrong' },
    { __typename: 'LogEntries', createdOn: '2018-08-02T18:07:30.610117Z', error: 'Something went wrong' },
    { __typename: 'LogEntries', createdOn: '2018-08-02T18:07:30.610117Z', error: 'Something went wrong' },
    { __typename: 'LogEntries', createdOn: '2018-08-02T18:07:30.610117Z', error: 'Something went wrong' },
    { __typename: 'LogEntries', createdOn: '2018-08-02T19:07:30.610117Z', error: 'Something went wrong' },
    { __typename: 'LogEntries', createdOn: '2018-08-02T20:07:30.610117Z', error: 'Something went wrong' },
    { __typename: 'LogEntries', createdOn: '2018-08-02T21:07:30.610117Z', error: 'Something went wrong' },
  ],
};

const groups = {
  __typename: 'SyncedGroupsCollection',
  startIndex: 0,
  itemsPerPage: 20,
  totalResults: 3,
  groups: [
    { __typename: 'SyncedGroup', name: 'Group 1', membershipCount: 4 },
    { __typename: 'SyncedGroup', name: 'Group 1', membershipCount: 255 },
    { __typename: 'SyncedGroup', name: 'Group 1', membershipCount: 12 },
  ],
};

const sites = [
  {
    __typename: 'OrgSite',
    id: 'siteid',
    siteUrl: 'test.atlassian.net',
    products: [
      'Jira Core',
      'Confluence',
    ],
  },
  {
    __typename: 'OrgSite',
    id: 'siteid2',
    siteUrl: 'test2.atlassian.net',
    products: [
      'Jira Core',
      'Bitbucket',
    ],
  },
];

const userProvisioningPageOrgId = 'myorg1234';

const client = createApolloClient();

interface StorybookProps {
  configureApolloClient?: ApolloClientStorybookWrapper['props']['configureClient'];
  loading: boolean;
  error: boolean;
  hasSites: boolean;
}

const UserProvisioningStorybookPage = ({ configureApolloClient, error, hasSites, loading }: StorybookProps) => (
  <ApolloClientStorybookWrapper client={client} configureClient={configureApolloClient}>
    <FlagProvider>
      <ApolloProvider client={client}>
        <IntlProvider locale="en">
          <BrowserRouter>
            <Switch location={{ pathname: `/o/${userProvisioningPageOrgId}/user-provisioning`, search: '', state: undefined, hash: '' }}>
              <Route path="/o/:orgId/user-provisioning">
                <AkPage>
                  <UserProvisioningPageImpl
                    analyticsClient={{ sendUIEvent: () => undefined } as any}
                    loading={loading}
                    error={error ? {} as any : undefined}
                    syncedUsers={1234}
                    syncedGroups={11}
                    match={{ params: { orgId: userProvisioningPageOrgId } } as any}
                    history={{ replace: () => undefined } as any}
                    location={{} as any}
                    showDrawer={action('showOrgSiteLinkingDrawer')}
                    intl={createMockIntlProp()}
                    linkedSitesData={{ loading: false, sites: hasSites ? [{} as any] : [] }}
                  />
                </AkPage>
              </Route>
            </Switch>
          </BrowserRouter>
        </IntlProvider>
      </ApolloProvider>
    </FlagProvider>
  </ApolloClientStorybookWrapper>
);

const configureClientForUserProvisioning = () => {
  const organization = {
    organization: {
      __typename: 'Organization',
      id: userProvisioningPageOrgId,
      externalDirectories: [
        {
          __typename: 'ExternalDirectory',
          id: '1234',
          name: 'Acme test',
          creationDate: '2018-08-02T20:07:30.610117Z',
          logs,
          groups,
        },
      ],
      sites,
    },
  };

  client.writeQuery({
    query: auditLogQuery,
    variables: {
      id: userProvisioningPageOrgId,
      count: 50,
      start: 1,
    },
    data: organization,
  });

  client.writeQuery({
    query: directoryQuery,
    variables: {
      id: userProvisioningPageOrgId,
    },
    data: organization,
  });

  client.writeQuery({
    query: syncedGroupsQuery,
    variables: {
      id: userProvisioningPageOrgId,
      count: 20,
      start: 1,
    },
    data: organization,
  });

  client.writeQuery({
    query: productAccessQuery,
    variables: {
      id: userProvisioningPageOrgId,
      count: 20,
      start: 1,
    },
    data: organization,
  });
};

storiesOf('Organization|User Provisioning/User Provisioning Page', module)
  .add('Default', () => (
    <UserProvisioningStorybookPage
      configureApolloClient={configureClientForUserProvisioning}
      error={false}
      hasSites={false}
      loading={false}
    />
    ))
  .add('Loading', () => (
    <UserProvisioningStorybookPage
      error={false}
      hasSites={false}
      loading={true}
    />
  ))
  .add('Error', () => (
    <UserProvisioningStorybookPage
      error={true}
      hasSites={false}
      loading={false}
    />
  ))
  .add('With linked sites', () => (
    <UserProvisioningStorybookPage
      configureApolloClient={configureClientForUserProvisioning}
      error={false}
      hasSites={true}
      loading={false}
    />
  ))
  ;
