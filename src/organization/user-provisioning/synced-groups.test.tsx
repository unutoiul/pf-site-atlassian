import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import { DynamicTableStateless as AkDynamicTableStateless } from '@atlaskit/dynamic-table';
import AkEmptyState from '@atlaskit/empty-state';

import { SyncedGroupsCollection } from '../../schema/schema-types';
import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import { SyncedGroupsImpl, SyncedGroupsProps } from './synced-groups';

describe('User provisioning synced groups', () => {

  const sandbox = sinon.sandbox.create();
  const groupsMock: SyncedGroupsCollection = {
    startIndex: 0,
    itemsPerPage: 3,
    totalResults: 3,
    groups: [
      { name: 'Group 1', membershipCount: 4 },
      { name: 'Group 1', membershipCount: 255 },
      { name: 'Group 1', membershipCount: 12 },
    ],
  };

  let historyReplaceSpy;

  beforeEach(() => {
    historyReplaceSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  const mountAuditLog = (loading, error, groups?: SyncedGroupsCollection) => {

    return mount<SyncedGroupsProps>((
      <SyncedGroupsImpl
        loading={loading}
        error={error}
        match={{ params: { orgId: '1234-12324' }, isExact: true, path: '', url: '' }}
        history={{ replace: historyReplaceSpy, length: 0, action: null as any, location: null as any, push: null as any, go: null as any, goBack: null as any, goForward: null as any, block: null as any, listen: null as any, createHref: null as any }}
        location={{ search: '', pathname: '', state: '', hash: '' }}
        groups={groups}
        intl={createMockIntlProp()}
      />
    ), createMockIntlContext());
  };

  it('should render with directory', () => {
    const wrapper = mountAuditLog(false, false, groupsMock);
    const table = wrapper.find(AkDynamicTableStateless);
    expect(table.exists()).to.equal(true);
    expect(table.props().rows).to.have.length(3);
  });

  it('should render empty state', () => {
    const wrapper = mountAuditLog(false, false);
    const emptyText = wrapper.find({ id: 'organization.userProvisioning.groups.empty' });
    expect(emptyText.exists()).to.equal(true);
  });

  it('should render loading state', () => {
    const wrapper = mountAuditLog(true, false);
    const spinner = wrapper.find('svg');
    expect(spinner.exists()).to.equal(true);
  });

  it('should render error state', () => {
    const wrapper = mountAuditLog(false, true);
    const emptyState = wrapper.find(AkEmptyState);
    expect(emptyState.exists()).to.equal(true);
  });

});
