
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';

import AkPage from '@atlaskit/page';

import { ProductAccessQuery } from '../../schema/schema-types';
import { createMockIntlProp } from '../../utilities/testing';
import { ProductAccessImpl } from './product-access';

const getSyncedGroupsPage = (loading, error, organization?) => {
  const noop = () => undefined;

  return (
    <IntlProvider locale="en">
      <AkPage>
        <ProductAccessImpl
          data={{ error, loading, ...organization }}
          intl={createMockIntlProp()}
          match={{ params: { orgId: '1234' } } as any}
          location={{} as any}
          history={{} as any}
          showDrawer={noop}
          analyticsClient={{} as any}
        />
      </AkPage>
    </IntlProvider>
  );
};

storiesOf('Organization|User Provisioning/Tab - Product Access', module)
  .add('Default', () => {
    const linkedSitesMock: ProductAccessQuery = {
      organization: {
        id: 'orgid',
        sites: [
          {
            id: 'siteid',
            siteUrl: 'test.atlassian.net',
            products: [
              'Jira Core',
              'Confluence',
            ],
          },
          {
            id: 'siteid2',
            siteUrl: 'atest.atlassian.net',
            products: [
              'Jira Core',
              'Bitbucket',
            ],
          },
          {
            id: 'siteid3',
            siteUrl: 'btest.atlassian.net',
            products: [],
          },
        ],
      },
    } as any;

    return getSyncedGroupsPage(false, false, linkedSitesMock);
  })
  .add('Empty', () => {
    const linkedSitesMock: ProductAccessQuery = {
      organization: {
        id: 'orgid',
        sites: [],
      },
    } as any;

    return getSyncedGroupsPage(false, false, linkedSitesMock);
  })
  .add('Loading', () => {
    return getSyncedGroupsPage(true, false);
  })
  .add('Error', () => {
    return getSyncedGroupsPage(false, true);
  });
