
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';
import { MemoryRouter } from 'react-router-dom';

import AkPage from '@atlaskit/page';

import { createApolloClient } from '../../apollo-client';
import { createMockIntlProp } from '../../utilities/testing';
import { LandingPageImpl } from './landing-page';

const client = createApolloClient();

const getScimLandingPage = ({ atlassianAccess, error, verified = true }) => {

  const organization = {
    organization: {
      domainClaim: {
        domains: [{
          verified,
        }],
      },
    },
  };

  return (
    <ApolloProvider client={client}>
      <IntlProvider locale="en">
        <MemoryRouter>
          <AkPage>
            <LandingPageImpl
              data={{ products: { identityManager: atlassianAccess }, error, ...organization } as any}
              showDrawer={() => undefined as any}
              analyticsClient={{ sendUIEvent: () => undefined } as any}
              match={{ params: { orgId: 'Hamburger' } } as any}
              history={{ replace: () => undefined } as any}
              location={{} as any}
              intl={createMockIntlProp()}
            />
          </AkPage>
        </MemoryRouter>
      </IntlProvider>
    </ApolloProvider>
  );
};

storiesOf('Organization|User Provisioning/Landing Page', module)
  .add('Default', () => {
    return getScimLandingPage({ atlassianAccess: true, error: false });
  })
  .add('No Atlassian Access', () => {
    return getScimLandingPage({ atlassianAccess: false, error: false });
  })
  .add('Access but no verified domain', () => {
    return getScimLandingPage({ atlassianAccess: true, error: false, verified: false });
  })
  .add('Error', () => {
    return getScimLandingPage({ atlassianAccess: false, error: true });
  })
;
