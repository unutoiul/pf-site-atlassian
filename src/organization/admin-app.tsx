import * as React from 'react';
import { connect, DispatchProp } from 'react-redux';
import { Route, Switch } from 'react-router-dom';

import { EyeballAnalytics } from 'common/eyeball';
import { FeedbackDialog } from 'common/feedback';
import { Page } from 'common/responsive/Page';

import { Dashboard } from '../dashboard/dashboard';
import { Navigation, NavigationDrawers, NavType } from '../navigation/navigation';
import { CreateOrganizationPage } from '../organization/create-page';
import { SiteRoutes } from '../site/routes';
import { RootState } from '../store';
import { CreateOrgOnboardingPage } from './onboarding/create-org-onboarding-page';
import { OrgRoutes } from './routes';

interface AppProps extends DispatchProp<any> {
  drawerOpen: boolean;
}

function withNavigation(Component, type: NavType) {
  return (props) => (
    <Page
      navigation={<Navigation navType={type} />}
      drawers={<NavigationDrawers />}
    >
      <Component {...props} />
    </Page>
  );
}

const CreateOrgOnboardingPageWithNavigation = withNavigation(CreateOrgOnboardingPage, 'default');
const SiteRoutesWithNavigation = withNavigation(SiteRoutes, 'site');
const OrgRoutesWithNavigation = withNavigation(OrgRoutes, 'org');
const DashboardWithNavigation = withNavigation(Dashboard, 'default');

class AdminAppImpl extends React.Component<AppProps> {
  public render() {
    const style: React.CSSProperties = this.props.drawerOpen ? { height: '100vh', overflow: 'hidden' } : {};

    return (
      <div style={style}>
        <Switch>
          <Route path="/atlassian-access" exact={true} component={CreateOrgOnboardingPageWithNavigation} />
          <Route path="/o/create" exact={true} component={CreateOrganizationPage} />
          <Route path="/o/:orgId" component={OrgRoutesWithNavigation} />
          <Route path="/s/:cloudId" component={SiteRoutesWithNavigation} />
          {/* It's fine to mount a component on `/` while site admin is being shipped, since `index.html` will never get served from anything but `/admin` there. */}
          <Route path="/" exact={true} component={DashboardWithNavigation} />
        </Switch>

        <EyeballAnalytics />
        <FeedbackDialog />
      </div>
    );
  }
}

const mapStateToProps = (state: RootState) => ({
  drawerOpen: state.chrome.navigationDrawers.openDrawer !== undefined,
});

export const AdminApp = connect(mapStateToProps)(AdminAppImpl);
