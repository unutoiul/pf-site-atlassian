import { ApolloError } from 'apollo-client';
import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import AkButton from '@atlaskit/button';
import AkFieldText from '@atlaskit/field-text';
import AkForm, { Field as AkField, ValidatorMessage as AkValidatorMessage } from '@atlaskit/form';
import { gridSize as akGridSize } from '@atlaskit/theme';

import { analyticsClient, AnalyticsData, Button as AnalyticsButton } from 'common/analytics';
import { ButtonGroup as AkButtonGroup } from 'common/button-group';
import { BadRequestError, createSuccessIcon } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';
import { ModalDialog } from 'common/modal';

import { AddAdminMutation } from '../../schema/schema-types';
import { createOrgAnalyticsData } from '../organizations/organization-analytics';
import AddAdmin from './add-admin.mutation.graphql';
import Admin from './admin.query.graphql';

export const messages = defineMessages({
  title: {
    id: 'org.admin.add.title',
    defaultMessage: 'Grant organization admin access',
  },
  description: {
    id: 'org.admin.add.description',
    defaultMessage: 'Enter an email below to grant admin access for the user to your organization.',
  },
  createAnother: {
    id: 'org.admin.add.create.another',
    defaultMessage: 'Add another',
  },
  textareaLabel: {
    id: 'org.admin.add.email',
    defaultMessage: 'Email address',
  },
  confirm: {
    id: 'org.admin.add.grant.access',
    defaultMessage: 'Grant access',
  },
  cancel: {
    id: 'org.admin.add.cancel',
    defaultMessage: 'Cancel',
  },
  success: {
    id: 'org.admin.add.success',
    defaultMessage: 'Success!',
  },
  successDescription: {
    id: 'org.admin.add.success.description',
    defaultMessage: 'The user with email {email} has been added as an administrator.',
  },
  textFieldLabel: {
    id: 'org.admin.add.text.field.label',
    defaultMessage: 'Email address',
  },
  invalidEmail: {
    id: 'org.admin.add.invalid.email',
    defaultMessage: 'Not a valid Atlassian account email address. Make sure an account exists and the email address is correct.',
    description: 'When a user tries to add a new admin, but the email is not a valid Atlassian account, meaning, no sites have that email',
  },
  genericError: {
    id: 'org.admin.add.generic.error',
    defaultMessage: 'Unable to grant admin access. Try again later.',
    description: 'When a user tries to add an admin and it fails for any reason other than being an invalid Atlassian account email',
  },
});

interface OwnProps {
  orgId: string;
  toggleModal(): void;
}

interface State {
  isLoading: boolean;
  text: string;
  hasError: boolean;
  hasInvalidEmailError: boolean;
}

const FieldWrapper = styled.div`
  min-height: ${akGridSize() * 15}px;
`;

export class AdminAddModalImpl extends React.Component<ChildProps<OwnProps & FlagProps, AddAdminMutation> & InjectedIntlProps, State> {
  public readonly state: Readonly<State> = {
    isLoading: false,
    text: '',
    hasError: false,
    hasInvalidEmailError: false,
  };

  private inputText?: HTMLFormElement;

  public render() {
    return (
      <ModalDialog
        width="small"
        header={<FormattedMessage {...messages.title} />}
        isOpen={true}
        onClose={this.props.toggleModal}
        footer={this.generateFooter()}
      >
        {this.generateDialogContent()}
      </ModalDialog>
    );
  }

  private generateDialogContent(): JSX.Element {
    const { intl: { formatMessage } } = this.props;

    return (
      <React.Fragment>
        <FormattedMessage {...messages.description} tagName="p" />
        <AkForm
          onSubmit={this.handleSubmit}
        >
          <FieldWrapper>
            <AkField
              name="email-address"
            >
              <React.Fragment>
                <AkFieldText
                  label={formatMessage(messages.textFieldLabel)}
                  type={'email'}
                  ref={this.changeInput}
                  name="adminEmailAddress"
                  onChange={this.setTextFieldState}
                  shouldFitContainer={true}
                  value={''}
                  isInvalid={this.state.hasInvalidEmailError || this.state.hasError}
                />
                {this.state.hasInvalidEmailError && <AkValidatorMessage isInvalid={true} invalidMessage={formatMessage(messages.invalidEmail)} />}
                {this.state.hasError && <AkValidatorMessage isInvalid={true} invalidMessage={formatMessage(messages.genericError)} />}
              </React.Fragment>
            </AkField>
          </FieldWrapper>
        </AkForm>
      </React.Fragment>
    );
  }

  private generateFooter = (): JSX.Element => {
    return (
      <AkButtonGroup>
        <AnalyticsButton
          appearance="primary"
          isDisabled={this.state.text === ''}
          isLoading={this.state.isLoading}
          analyticsData={this.generateAddAdminAnalytics()}
          onClick={this.addAdministrator}
        >
          <FormattedMessage {...messages.confirm} />
        </AnalyticsButton>
        <AkButton onClick={this.props.toggleModal} appearance="subtle-link">
          <FormattedMessage {...messages.cancel} />
        </AkButton>
      </AkButtonGroup>
    );
  }

  private generateAddAdminAnalytics = (): AnalyticsData => {
    return createOrgAnalyticsData({
      orgId: this.props.orgId,
      action: 'click',
      actionSubject: 'addOrgAdminButton',
      actionSubjectId: 'add',
    });
  }

  private addAdministrator = (): void => {
    this.setState({ isLoading: true, hasError: false, hasInvalidEmailError: false });

    const email = this.state.text;

    if (!this.props.mutate) {
      return;
    }

    this.props.mutate({
      variables: {
        orgId: this.props.orgId,
        email,
      },
      optimisticResponse: {
        addOrganizationAdmin: true,
      },
      refetchQueries: [{
        query: Admin,
        variables: {
          id: this.props.orgId,
        },
      }],
    }).then(() => {
      this.props.showFlag({
        autoDismiss: true,
        icon: createSuccessIcon(),
        id: `org.admin.add.success.flag.${Date.now()}`,
        title: this.props.intl.formatMessage(messages.success),
        description: this.props.intl.formatMessage(messages.successDescription, { email }),
      });
      this.resetText();
      this.props.toggleModal();
    }).catch((error) => {
      this.handleError(error);
      analyticsClient.onError(error);
    });
  }

  private handleSubmit = (e: React.FormEvent<HTMLElement>): void => {
    e.preventDefault();

    this.addAdministrator();
  }

  private handleError(error: ApolloError) {
    if (BadRequestError.findIn(error).length) {
      this.setState({ isLoading: false, hasInvalidEmailError: true });

      return;
    }

    this.setState({ isLoading: false, hasError: true });
  }

  private changeInput = (inputField: any): void => {
    if (inputField) {
      this.inputText = inputField;
    }
  }

  private resetText = (): void => {
    if (this.inputText) {
      this.inputText.setState({ value: '' });
    }
    this.setState({ text: '' });
  };

  private setTextFieldState = (e): void => {
    this.setState({ text: e.target.value });
  };
}

const mutation = graphql<OwnProps, AddAdminMutation>(AddAdmin);

export const AdminAddModal = mutation(
  withFlag(
    injectIntl(
      AdminAddModalImpl,
    ),
  ),
);
