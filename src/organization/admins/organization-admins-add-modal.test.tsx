// tslint:disable:jsx-use-translation-function
import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import { IntlProvider } from 'react-intl';
import * as sinon from 'sinon';

import AkButton from '@atlaskit/button';
import AkTextField from '@atlaskit/field-text';

import { Button as AnalyticsButton } from 'common/analytics';
import { createSuccessIcon } from 'common/error';

import { ModalDialog } from 'common/modal';

import { createMockIntlProp } from '../../utilities/testing';
import { AdminAddModalImpl, messages } from './organization-admins-add-modal';

describe('Add admin modal', () => {

  let toggleModalSpy;
  let mutateStub;
  let showFlagSpy;
  let promise;

  const sandbox = sinon.sandbox.create();

  beforeEach(() => {
    promise = Promise.resolve(true);
    toggleModalSpy = sandbox.spy();
    showFlagSpy = sandbox.spy();
    mutateStub = sandbox.stub().returns(promise);
  });

  afterEach(() => {
    sandbox.reset();
  });

  function checkSpiesAndStubsAreInOriginalState() {
    expect(mutateStub.called).to.equal(false);
    expect(showFlagSpy.called).to.equal(false);
    expect(toggleModalSpy.called).to.equal(false);
  }

  function mountedAdminModal() {
    return mount(
      <IntlProvider
        locale={'en'}
        children={(
          <AdminAddModalImpl
            orgId={'DUMMY-TEST-ORG-ID'}
            toggleModal={toggleModalSpy}
            mutate={mutateStub}
            showFlag={showFlagSpy}
            hideFlag={() => null}
            intl={createMockIntlProp()}
          />
        )}
      />,
    );
  }

  it('Should render all the UI elements correctly', () => {
    const wrapper = mountedAdminModal();

    const modalDialog = wrapper.find(ModalDialog);
    expect(modalDialog.length).to.equal(1);
    expect(modalDialog.find('h4').text()).to.equal(messages.title.defaultMessage);

    const loadingButton = wrapper.find(AnalyticsButton);
    expect(loadingButton).to.have.length(1);

    const button = wrapper.find(AkButton).at(1);
    expect(button).to.have.length(1);

    expect(loadingButton.props().appearance).to.equal('primary');
    expect(loadingButton.props().analyticsData).to.deep.equal({
      action: 'click',
      actionSubject: 'addOrgAdminButton',
      actionSubjectId: 'add',
      subproduct: 'organization',
      tenantId: 'DUMMY-TEST-ORG-ID',
      tenantType: 'organizationId',
    });

    expect(button.props().appearance).to.deep.equal('subtle-link');
    expect(button.find('span').at(0).text()).to.deep.equal('Cancel');
  });

  it('Grant access should perform correct mutation and show a success flag', async () => {
    const wrapper = mountedAdminModal();

    checkSpiesAndStubsAreInOriginalState();

    const button1 = wrapper.find(AnalyticsButton);
    expect(button1.length).to.equal(1);

    const input = wrapper.find(AkTextField).find('input');
    (input.getDOMNode() as HTMLInputElement).value = 'cheese@cheese.com';
    input.simulate('change', input);
    wrapper.update();

    const button2 = wrapper.find(AnalyticsButton);
    button2.simulate('click');
    wrapper.update();

    await promise;

    expect(mutateStub.called).to.equal(true);
    expect(showFlagSpy.called).to.equal(true);

    // Mutate is called with the correct arguments
    expect(mutateStub.firstCall.args[0].variables.orgId).to.deep.equal('DUMMY-TEST-ORG-ID');
    expect(mutateStub.firstCall.args[0].variables.email).to.deep.equal('cheese@cheese.com');
    expect(mutateStub.firstCall.args[0].optimisticResponse).to.deep.equal({ addOrganizationAdmin: true });
    expect(mutateStub.firstCall.args[0].refetchQueries).to.have.length(1);

    // A success flag is shown
    expect(showFlagSpy.lastCall.args[0]).to.deep.include({
      autoDismiss: true,
      icon: createSuccessIcon(),
      title: 'Success!',
      description: 'The user with email cheese@cheese.com has been added as an administrator.',
    });

    // Toggle modal should be called when the checkbox is unchecked
    expect(toggleModalSpy.called).to.deep.equal(true);
  });
});
