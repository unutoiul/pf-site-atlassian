import * as React from 'react';
import { QueryResult } from 'react-apollo';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import AkAvatar from '@atlaskit/avatar';
import AkButton from '@atlaskit/button';
import { DynamicTableStateless as AkDynamicTableStateless } from '@atlaskit/dynamic-table';
import { akColorN300, akGridSizeUnitless } from '@atlaskit/util-shared-styles';

import { TruncatedField } from 'common/truncated-field';

import { getAvatarUrlByUserId } from 'common/avatar';

import { AdminQuery } from '../../schema/schema-types';

const headerKey = 'table.head.key.';
const bodyKey = 'table.body.key.';

const tableMessages = defineMessages({
  name: {
    id: 'organization.admins.table.name',
    defaultMessage: 'Name',
  },
  email: {
    id: 'organization.admins.table.email',
    defaultMessage: 'Email',
  },
  actions: {
    id: 'organization.admins.table.actions',
    defaultMessage: 'Actions',
  },
  avatar: {
    id: 'organization.admins.table.avatar',
    defaultMessage: 'Avatar',
  },
});

const tableActions = defineMessages({
  remove: {
    id: 'organization.admins.table.remove',
    defaultMessage: 'Remove',
  },
});

const ButtonWrapper = styled.div`
  margin-left: -${akGridSizeUnitless * 1.5}px;
  padding-left: 0;
`;

interface AdminPageProps {
  admins: AdminQuery & Partial<QueryResult>;
  currentUserId: string;
  removeAdmin(adminId: string, adminName: string): () => void;
}

export class AdminTableImpl extends React.Component<AdminPageProps & InjectedIntlProps> {
  public render() {

    const { formatMessage } = this.props.intl;

    const avatarCellStyle = {
      fontSize: '0',
      paddingRight: '0',
      width: '1px',
    };

    const displayNameStyle = {
      width: '30%',
    };

    const emailStyle = {
      color: akColorN300,
    };

    const actionStyle = {
      whiteSpace: 'nowrap',
      width: '1px',
    };

    const header = {
      cells: [
        { key: headerKey + tableMessages.name.defaultMessage, content: formatMessage(tableMessages.name), colSpan: 2 },
        { key: headerKey + tableMessages.email.defaultMessage, content: formatMessage(tableMessages.email) },
        { key: headerKey + tableMessages.actions.defaultMessage, content: formatMessage(tableMessages.actions) },
      ],
    };

    const adminList = this.props.admins.organization.users.admins.users;
    const rows = (adminList || []).map(admin => ({
      cells: [
        { key: bodyKey + tableMessages.avatar.id + admin.id, content: (<AkAvatar src={getAvatarUrlByUserId(admin.id)} size="medium" />), style: avatarCellStyle },
        { key: bodyKey + tableMessages.name.id + admin.id, content: <TruncatedField title={admin.displayName} maxWidth={`${akGridSizeUnitless * 30}px`}>{admin.displayName}</TruncatedField>, style: displayNameStyle },
        { key: bodyKey + tableMessages.email.id + admin.id, content: <TruncatedField title={admin.emails[0].value} maxWidth={`${akGridSizeUnitless * 30}px`}>{admin.emails[0].value}</TruncatedField>, style: emailStyle },
        { key: bodyKey + tableMessages.actions.id + admin.id, content: this.generateActions(admin.id, admin.displayName), style: actionStyle },
      ],
    }));

    return (
      <AkDynamicTableStateless
        head={header}
        rows={rows.length ? rows : undefined}
      />
    );
  }

  private generateActions(adminId: string, adminName: string): JSX.Element {
    const { formatMessage } = this.props.intl;

    return (
      <ButtonWrapper>
        <AkButton isDisabled={this.isUserCurrentAdmin(adminId)} id={adminId} onClick={this.props.removeAdmin(adminId, adminName)} appearance="link">
          {formatMessage(tableActions.remove)}
        </AkButton>
      </ButtonWrapper>
    );
  }

  private isUserCurrentAdmin = (adminId: string): boolean => this.props.currentUserId === adminId;
}

export const AdminTable: React.ComponentClass<AdminPageProps> = injectIntl(AdminTableImpl);
