import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import AkSpinner from '@atlaskit/spinner';

import { analyticsClient } from 'common/analytics';

import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import { AdminPageImpl } from './organization-admins-page';
import { AdminTable } from './organization-admins-table';

describe('Organization Admins Page', () => {
  let eventHandlerSpy;

  const sandbox = sinon.sandbox.create();

  beforeEach(() => {
    eventHandlerSpy = sandbox.spy(analyticsClient, 'onEvent');
  });

  afterEach(() => {
    sandbox.restore();
  });

  function organizationAdminPage(data) {
    const currentUser = {
      loading: false,
      error: undefined,
      currentUser: {
        id: 'cheese-account-id',
      },
    };

    const match = {
      params: {
        orgId: 'DUMMY-TEST-ORG-ID',
      },
    };

    return shallow(
      <AdminPageImpl match={match} admins={{ ...currentUser, ...data }} intl={createMockIntlProp()} {...{} as any} />,
      createMockIntlContext(),
    );
  }

  it('Spinner should display when loading', () => {
    const data = { loading: true };
    const wrapper = organizationAdminPage(data);
    expect(wrapper.find(AkSpinner)).to.have.length(1);
  });

  it('Generic error should display on GraphQL error', () => {
    const data = { loading: false, error: {} };
    const wrapper = organizationAdminPage(data);
    // tslint:disable-next-line:jsx-use-translation-function
    expect(wrapper.contains(<p>There was a problem accessing the page. Please check back in a few minutes.</p>)).to.equal(true);
    expect(eventHandlerSpy.called).to.equal(false);
  });

  it('Organization error should display when the organization does not exist', () => {
    const data = { loading: false };
    const wrapper = organizationAdminPage(data);
    // tslint:disable-next-line:jsx-use-translation-function
    expect(wrapper.contains(<p>The organization you have tried to access does not exist, or you do not have permission to access it.</p>)).to.equal(true);
    expect(eventHandlerSpy.called).to.equal(false);
  });

  it('Admin error should display when the admins do not exist', () => {
    const data = { loading: false, organization: { users: { admins: { total: 0, users: [] } } } };
    const wrapper = organizationAdminPage(data);
    // tslint:disable-next-line:jsx-use-translation-function
    expect(wrapper.contains(<p>Administrator information could not be retrieved for this organization.</p>)).to.equal(true);
  });

  it('Table should display when the admins do exist', () => {
    const user = { id: 'Cheese User 1', emails: [{ primary: true, value: 'cheese@cheese.com' }], displayName: 'Cheese to the Supreme', userName: 'cheese@cheese.com' };
    const data = { loading: false, organization: { users: { admins: { total: 1, users: [user] } } } };
    const wrapper = organizationAdminPage(data);
    expect(wrapper.find(AdminTable)).to.have.length(1);
  });

  it('should fire analytics on initial page load finishing', () => {
    const data = { loading: true };
    const wrapper = organizationAdminPage(data);
    wrapper.setProps({ admins: { loading: false, organization: { users: { admins: { total: 1, users: [] } } } } });
    wrapper.update();

    expect(eventHandlerSpy.called).to.equal(true);
    expect(eventHandlerSpy.firstCall.args[0]).to.deep.equal({
      subproduct: 'organization',
      tenantType: 'organizationId',
      tenantId: 'DUMMY-TEST-ORG-ID',
      action: 'pageLoad',
      actionSubject: 'organizationPageLoad',
      actionSubjectId: 'adminDetailsPage',
    });
  });
});
