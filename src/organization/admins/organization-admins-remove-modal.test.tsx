import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import AkButton from '@atlaskit/button';

import { Button as AnalyticsButton } from 'common/analytics';
import { ModalDialog } from 'common/modal';

import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import { AdminRemoveModalImpl } from './organization-admins-remove-modal';

describe('Remove admin modal', () => {
  let toggleModalSpy;
  let mutateStub;
  let promise;

  const sandbox = sinon.sandbox.create();

  beforeEach(() => {
    promise = Promise.resolve(true);
    toggleModalSpy = sandbox.spy();
    mutateStub = sandbox.stub().resolves({});
  });

  afterEach(() => {
    sandbox.reset();
  });

  function shallowAdminRemoveModal() {
    return shallow(
      <AdminRemoveModalImpl
        orgId={'DUMMY-TEST-ORG-ID'}
        isModalOpen={true}
        adminNameToRemove={'Boop'}
        adminIdToRemove={'420'}
        toggleModal={toggleModalSpy}
        mutate={mutateStub}
        intl={createMockIntlProp()}
        {...{} as any}
      />,
      createMockIntlContext(),
    );
  }

  it('Should render all the UI elements correctly', () => {
    const wrapper = shallowAdminRemoveModal();

    // Modal component is rendered
    const modalDialog = wrapper.find(ModalDialog);
    expect(modalDialog).to.have.length(1);

    // Footer exists
    const footer = shallow(modalDialog.props().footer as React.ReactElement<any>);

    // Remove and cancel button exists
    const loadingButton = footer.find(AnalyticsButton);
    const button = footer.find(AkButton);

    expect(loadingButton).to.have.length(1);
    expect(button).to.have.length(1);

    expect(loadingButton.props().appearance).to.equal('danger');
    expect(loadingButton.children().text()).to.equal('Remove');
    expect(loadingButton.props().analyticsData).to.deep.equal({
      action: 'click',
      actionSubject: 'removeOrgAdminButton',
      actionSubjectId: 'remove',
      subproduct: 'organization',
      tenantId: 'DUMMY-TEST-ORG-ID',
      tenantType: 'organizationId',
    });

    expect(button.props().appearance).to.equal('subtle-link');
    expect(button.children().text()).to.equal('Cancel');
  });

  it('Remove button behaves as expected', async () => {
    const wrapper = shallowAdminRemoveModal();
    const modalDialog = wrapper.find(ModalDialog);
    const footer = shallow(modalDialog.props().footer as React.ReactElement<any>);
    const loadingButton = footer.find(AnalyticsButton);
    expect(mutateStub.called).to.deep.equal(false);
    expect(toggleModalSpy.called).to.deep.equal(false);
    loadingButton.simulate('click');
    await promise;
    expect(toggleModalSpy.called).to.deep.equal(true);
    expect(mutateStub.called).to.deep.equal(true);
  });
});
