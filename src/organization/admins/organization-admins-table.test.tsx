import { expect } from 'chai';
import { mount, ReactWrapper } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import AkAvatar from '@atlaskit/avatar';
import AkButton from '@atlaskit/button';
import { DynamicTableStateless as AkDynamicTableStateless } from '@atlaskit/dynamic-table';

import { AdminQuery } from '../../schema/schema-types';
import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import { AdminTableImpl } from './organization-admins-table';

describe('Admin table', () => {

  let removeAdminSpy;

  const sandbox = sinon.sandbox.create();

  beforeEach(() => {
    removeAdminSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.reset();
  });

  const admins: AdminQuery = {
    currentUser: {
      __typename: '',
      id: 'cheese-account-id',
    },
    organization: {
      __typename: '',
      id: 'DUMMY-ORG-ID',
      users: {
        __typename: '',
        admins: {
          __typename: '',
          id: 'DUMMY-ORG-ID',
          total: 2,
          users: [
            { __typename: '', active: 'ENABLED', id: '1', displayName: 'Cheese Factory', emails: [{ __typename: '', primary: true, value: 'cheese@atlassian.com' }] },
            { __typename: '', active: 'ENABLED', id: '2', displayName: 'Cheese Eater', emails: [{ __typename: '', primary: true, value: 'brie@atlassian.com' }] },
          ],
        },
      },
    },
  };

  function mountedAdminTable() {
    return mount(<AdminTableImpl admins={admins} currentUserId={'2'} removeAdmin={removeAdminSpy} intl={createMockIntlProp()} />, createMockIntlContext());
  }

  function findCellsByText(element: ReactWrapper<any, any>, text: string): ReactWrapper<any, any> {
    return element.find('td').filterWhere(e => e.text() === text);
  }

  it('Admin row should display', () => {
    const wrapper = mountedAdminTable();
    expect(wrapper.find(AkDynamicTableStateless)).to.have.length(1);
    const table = wrapper.find(AkDynamicTableStateless);

    expect(findCellsByText(table, 'Cheese Factory')).to.have.length(1);
    expect(findCellsByText(table, 'cheese@atlassian.com')).to.have.length(1);
    expect(findCellsByText(table, 'Cheese Eater')).to.have.length(1);
    expect(findCellsByText(table, 'brie@atlassian.com')).to.have.length(1);

    expect(table.find(AkAvatar)).to.have.length(2);
    expect(table.find(AkButton)).to.have.length(2);
  });

  it('Current admin remove button should be greyed out, all others should be clickable', () => {
    const wrapper = mountedAdminTable();
    expect(wrapper.find(AkDynamicTableStateless)).to.have.length(1);
    const table = wrapper.find(AkDynamicTableStateless);

    const button1 = table.find(AkButton).at(0);
    const button2 = table.find(AkButton).at(1);

    expect(button1.props().appearance).to.equal('link');
    expect(button2.props().appearance).to.equal('link');

    expect(button1.at(0).props().isDisabled).to.equal(false);
    expect(button2.at(0).props().isDisabled).to.equal(true);
  });
});
