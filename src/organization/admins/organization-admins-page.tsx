import * as React from 'react';
import { graphql, QueryResult } from 'react-apollo';
import { defineMessages, FormattedHTMLMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps } from 'react-router';

import AkButton from '@atlaskit/button';
import AkSpinner from '@atlaskit/spinner';

import { analyticsClient, orgAdminScreenEvent, ScreenEventSender } from 'common/analytics';
import { PageLayout } from 'common/page-layout';

import { OrgBreadcrumbs } from '../breadcrumbs';

import { AdminQuery } from '../../schema/schema-types';
import { createOrgAnalyticsData } from '../organizations/organization-analytics';
import Admin from './admin.query.graphql';
import { AdminAddModal } from './organization-admins-add-modal';
import { AdminRemoveModal } from './organization-admins-remove-modal';
import { AdminTable } from './organization-admins-table';

export const pageMessages = defineMessages({
  title: {
    id: 'organization.admins.page.title',
    defaultMessage: 'Administrators',
  },
  description: {
    id: 'organization.admins.page.description',
    defaultMessage: `Add users here who you want to be admins for your organization. Organization admins can verify domains,
    manage user accounts and configure security policies.
    <a href="https://confluence.atlassian.com/x/1tz1Nw" target="_blank" rel="noopener noreferrer"> Learn more about organization administration</a>.`,
  },
  permissionDeniedError: {
    id: 'organization.admins.page.permission.denied.error',
    defaultMessage: 'The organization you have tried to access does not exist, or you do not have permission to access it.',
  },
  noAdminsError: {
    id: 'organization.admins.page.no.admins.error',
    defaultMessage: 'Administrator information could not be retrieved for this organization.',
  },
  genericError: {
    id: 'organization.admins.page.generic.error',
    defaultMessage: 'There was a problem accessing the page. Please check back in a few minutes.',
  },
  add: {
    id: 'organization.admins.page.add',
    defaultMessage: 'Add administrators',
  },
});

interface State {
  isRemoveAdminModalOpen: boolean;
  isAddAdminModalOpen: boolean;
  adminIdToRemove: string | undefined;
  adminNameToRemove: string | undefined;
}

// interface AdminPageProps extends RouteComponentProps<{ orgId: string }> {
//   admins: AdminQuery & QueryProps & QueryResult;
interface AdminPageProps {
  admins: AdminQuery & QueryResult;
}

export class AdminPageImpl extends React.Component<AdminPageProps & RouteComponentProps<{ orgId: string }> & InjectedIntlProps, State> {
  public state: State = {
    isRemoveAdminModalOpen: false,
    isAddAdminModalOpen: false,
    adminIdToRemove: undefined,
    adminNameToRemove: undefined,
  };

  public componentWillReceiveProps(nextProps: AdminPageImpl['props']) {
    if (this.props.admins.loading !== true || nextProps.admins.loading !== false || nextProps.admins.error) {
      return;
    }

    analyticsClient.eventHandler(createOrgAnalyticsData({
      orgId: nextProps.match.params.orgId,
      action: 'pageLoad',
      actionSubject: 'organizationPageLoad',
      actionSubjectId: 'adminDetailsPage',
    }));
  }

  public render() {
    const { formatMessage } = this.props.intl;

    return (
      <ScreenEventSender event={orgAdminScreenEvent()}>
        <PageLayout
          isFullWidth={true}
          title={formatMessage(pageMessages.title)}
          description={<FormattedHTMLMessage {...pageMessages.description} />}
          action={this.generateAction()}
          breadcrumbs={<OrgBreadcrumbs/>}
        >
          {this.generatePageState()}
        </PageLayout>
      </ScreenEventSender>
    );
  }

  private generateAction(): JSX.Element {

    const { formatMessage } = this.props.intl;
    const loadingState = this.props.admins.loading;
    const errorState = !!this.props.admins.error;

    return (
      <AkButton isDisabled={loadingState || errorState} onClick={this.addAdmin} appearance={'primary'}>
        {formatMessage(pageMessages.add)}
      </AkButton>
    );
  }

  private generatePageState(): React.ReactNode {

    const { formatMessage } = this.props.intl;

    if (this.props.admins.loading) {
      return (<AkSpinner />);
    } else if (this.props.admins.error) {
      return (<p>{formatMessage(pageMessages.genericError)}</p>);
    } else if (!this.props.admins.organization) {
      return (<p>{formatMessage(pageMessages.permissionDeniedError)}</p>);
    } else if (this.props.admins.organization.users.admins.users && this.props.admins.organization.users.admins.users.length === 0) {
      return (<p>{formatMessage(pageMessages.noAdminsError)}</p>);
    }

    return this.generateAdminPageContent();
  }

  private addAdmin = (): void => {
    this.toggleAddModal();
  }

  private removeAdmin = (adminId: string, adminName: string) => (): void => {
    this.setAdmin(adminId, adminName);
    this.toggleRemoveModal();
  }

  private generateAdminPageContent(): React.ReactNode {
    if (!this.props.admins.currentUser) {
      return null;
    }

    return (
      <div>
        <AdminTable
          admins={this.props.admins}
          currentUserId={this.props.admins.currentUser.id}
          removeAdmin={this.removeAdmin}
        />
        <AdminRemoveModal
          orgId={this.props.match.params.orgId}
          adminNameToRemove={this.state.adminNameToRemove!}
          adminIdToRemove={this.state.adminIdToRemove!}
          isModalOpen={this.state.isRemoveAdminModalOpen}
          toggleModal={this.toggleRemoveModal}
        />
        {this.state.isAddAdminModalOpen && (
          <AdminAddModal
            orgId={this.props.match.params.orgId}
            toggleModal={this.toggleAddModal}
          />
        )}
      </div>
    );
  }

  private setAdmin = (adminIdToRemove: string, adminNameToRemove: string): void => this.setState({ adminIdToRemove, adminNameToRemove });
  private toggleRemoveModal = (): void => this.setState({ isRemoveAdminModalOpen: !this.state.isRemoveAdminModalOpen });
  private toggleAddModal = (): void => this.setState({ isAddAdminModalOpen: !this.state.isAddAdminModalOpen });
}

const getAdmins = graphql<RouteComponentProps<{ orgId: string }>, AdminQuery>(Admin, {
  name: 'admins',
  options: (props) => ({ variables: { id: props.match.params.orgId } }),
});

export const AdminPage = getAdmins(
  injectIntl(
    AdminPageImpl,
  ),
);
