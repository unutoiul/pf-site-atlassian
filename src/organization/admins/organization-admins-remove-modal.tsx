import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import AkButton from '@atlaskit/button';

import { analyticsClient, AnalyticsData, Button as AnalyticsButton } from 'common/analytics';
import { createSuccessIcon } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';
import { ModalDialog } from 'common/modal';

import { RemoveAdminMutation } from '../../schema/schema-types';
import { createOrgAnalyticsData } from '../organizations/organization-analytics';
import Admin from './admin.query.graphql';
import RemoveAdmin from './remove-admin.mutation.graphql';

const removeModal = defineMessages({
  remove: {
    id: 'org.admin.remove.remove.domain',
    defaultMessage: 'Remove {admin} as an administrator?',
  },
  removeButton: {
    id: 'org.admin.remove.confirm',
    defaultMessage: 'Remove',
  },
  removeSuccess: {
    id: 'org.admin.remove.remove.success',
    defaultMessage: 'Successfully removed as an administrator.',
  },
  cancel: {
    id: 'org.admin.remove.cancel',
    defaultMessage: 'Cancel',
  },
});

interface OwnProps {
  orgId: string;
  isModalOpen: boolean;
  adminNameToRemove: string;
  adminIdToRemove: string;
  toggleModal(): void;
}

interface State {
  isLoading: boolean;
}

export class AdminRemoveModalImpl extends React.Component<ChildProps<OwnProps & FlagProps, RemoveAdminMutation> & InjectedIntlProps, State> {

  public state: State = { isLoading: false };

  public render() {
    const { formatMessage } = this.props.intl;

    return (
      <ModalDialog
        width="small"
        header={formatMessage(removeModal.remove, { admin: this.props.adminNameToRemove })}
        isOpen={this.props.isModalOpen}
        onClose={this.props.toggleModal}
        footer={this.generateRemoveModalFooter()}
      />
    );
  }

  private generateRemoveModalFooter(): JSX.Element {
    const { formatMessage } = this.props.intl;

    return (
      <div>
        <AnalyticsButton
          appearance="danger"
          isLoading={this.state.isLoading}
          onClick={this.removeAdministrator}
          analyticsData={this.generateRemoveAdminAnalytics()}
        >
          {formatMessage(removeModal.removeButton)}
        </AnalyticsButton>
        <AkButton onClick={this.props.toggleModal} appearance="subtle-link">
          {formatMessage(removeModal.cancel)}
        </AkButton>
      </div>
    );
  }

  private generateRemoveAdminAnalytics = (): AnalyticsData => {
    return createOrgAnalyticsData({
      orgId: this.props.orgId,
      action: 'click',
      actionSubject: 'removeOrgAdminButton',
      actionSubjectId: 'remove',
    });
  }

  private removeAdministrator = (): void => {
    this.setState({ isLoading: true });

    if (!this.props.mutate) {
      return;
    }

    this.props.mutate({
      variables: {
        orgId: this.props.orgId,
        adminId: this.props.adminIdToRemove,
      },
      optimisticResponse: {
        removeOrganizationAdmin: true,
      },
      refetchQueries: [{
        query: Admin,
        variables: {
          id: this.props.orgId,
        },
      }],
    }).then(() => {
      this.props.toggleModal();
      this.setState({ isLoading: false });
      this.props.showFlag({
        autoDismiss: true,
        icon: createSuccessIcon(),
        id: `org.admin.remove.success.flag.${(Date.now().valueOf())}`,
        title: this.props.intl.formatMessage(removeModal.removeSuccess),
      });

    }).catch((error) => {
      this.props.toggleModal();
      this.setState({ isLoading: false });
      analyticsClient.onError(error);
    });

  }
}

const mutation = graphql<OwnProps, RemoveAdminMutation>(RemoveAdmin);

export const AdminRemoveModal = mutation(
  withFlag(
    injectIntl(
      AdminRemoveModalImpl,
    ),
  ),
);
