import * as React from 'react';
import { InjectedIntlProps, injectIntl } from 'react-intl';

import { Button as AnalyticsButton } from 'common/analytics/components';
import { ModalDialog } from 'common/modal';

import { createOrgAnalyticsData } from '../organizations/organization-analytics';
import { deleteMessages } from './saml-messages';

interface SamlRemovalModalProps {
  isOpen: boolean;
  isDeleting: boolean;
  orgId: string;
  onCancel(): void;
  onDelete(): void;
}

export class SamlRemovalModalImpl extends React.Component<SamlRemovalModalProps & InjectedIntlProps, any> {
  public render() {
    const { formatMessage } = this.props.intl;

    return (
      <ModalDialog
        width="small"
        isOpen={this.props.isOpen}
        onClose={this.props.onCancel}
        header={formatMessage(deleteMessages.heading)}
        footer={
          <div>
            <AnalyticsButton
              appearance="danger"
              onClick={this.props.onDelete}
              isLoading={this.props.isDeleting}
              analyticsData={createOrgAnalyticsData({
                orgId: this.props.orgId,
                action: 'click',
                actionSubject: 'deleteSamlButton',
                actionSubjectId: 'delete',
              })}
            >
              {formatMessage(deleteMessages.confirm)}
            </AnalyticsButton>
            <AnalyticsButton
              appearance="subtle-link"
              onClick={this.props.onCancel}
              analyticsData={createOrgAnalyticsData({
                orgId: this.props.orgId,
                action: 'click',
                actionSubject: 'deleteSamlButton',
                actionSubjectId: 'cancel',
              })}
            >
              {formatMessage(deleteMessages.cancel)}
            </AnalyticsButton>
          </div>
        }
      >
        <p>{formatMessage(deleteMessages.description)}</p>
      </ModalDialog>
    );
  }
}

export const SamlRemovalModal = injectIntl(SamlRemovalModalImpl);
