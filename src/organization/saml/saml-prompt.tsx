import * as React from 'react';
import { InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import AkButton from '@atlaskit/button';
import AkCrossIcon from '@atlaskit/icon/glyph/cross';
import AkInlineDialog from '@atlaskit/inline-dialog';
import { akGridSizeUnitless } from '@atlaskit/util-shared-styles';

import { completePromptMessages } from './saml-messages';

export const Content = styled.div`
  max-width: ${akGridSizeUnitless * 40}px;
  position: relative;
`;

const CloseButton = styled(AkButton)`
  float: right;

  > span {
    height: ${akGridSizeUnitless * 3}px;
    width: ${akGridSizeUnitless * 3}px;
  }
`;

interface SamlPromptProps {
  isOpen: boolean;
  body: React.ReactNode;
  icon: React.ReactNode;
  placement?: string;
  onClose(): void;
  onOpen(): void;
}

export class SamlPromptImpl extends React.Component<SamlPromptProps & InjectedIntlProps> {
  public render() {
    return (
      <AkInlineDialog isOpen={this.props.isOpen} placement={this.props.placement || 'right'} content={this.content()}>
        <AkButton onClick={this.props.onOpen} appearance="link" spacing="none">
          {this.props.icon}
        </AkButton>
      </AkInlineDialog>
    );
  }

  private content() {
    const { formatMessage } = this.props.intl;

    return (
      <Content>
        <CloseButton appearance="subtle" spacing="none" onClick={this.props.onClose}>
          <AkCrossIcon label={formatMessage(completePromptMessages.close)} />
        </CloseButton>
        {this.props.body}
      </Content>
    );
  }
}

export const SamlPrompt = injectIntl<SamlPromptProps>(SamlPromptImpl);
