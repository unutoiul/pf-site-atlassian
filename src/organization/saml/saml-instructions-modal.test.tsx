import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { sandbox as sinonSandbox } from 'sinon';

import AkButton from '@atlaskit/button';

import { ModalDialog } from 'common/modal';

import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import { SamlInstructionsModalImpl as SamlInstructionsModal } from './saml-instructions-modal';

describe('SamlInstructionsModal', () => {
  const sandbox = sinonSandbox.create();
  let onDismissSpy;

  beforeEach(() => {
    onDismissSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  function shallowSamlInstructionsModal() {
    return shallow(
      <SamlInstructionsModal
        isOpen={true}
        onDismiss={onDismissSpy}
        intl={createMockIntlProp()}
      />,
      createMockIntlContext(),
    );
  }

  it('passes isOpen and onDialogDismissed', () => {
    const wrapper = shallowSamlInstructionsModal();
    expect(wrapper.find(ModalDialog).prop('isOpen')).to.be.true();
    expect(wrapper.find(ModalDialog).prop('onClose')).to.equal(onDismissSpy);
  });

  it('contains a button that triggers onDismiss when clicked', () => {
    const wrapper = shallowSamlInstructionsModal();
    const footer: any = wrapper.find(ModalDialog).prop('footer');
    const button = shallow(footer).find(AkButton);

    expect(button).to.have.length(1);
    button.simulate('click');
    expect(onDismissSpy.called).to.be.true();
  });
});
