import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { FormattedHTMLMessage, FormattedMessage } from 'react-intl';
import { sandbox as sinonSandbox } from 'sinon';

import { ButtonGroup as AkButtonGroup } from '@atlaskit/button';
import AkSectionMessage from '@atlaskit/section-message';
import AkSpinner from '@atlaskit/spinner';

import { Button as AnalyticsButton } from 'common/analytics';
import { PageLayout, PageLayoutProps } from 'common/page-layout';

import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import { IdentityManagerPageLayout } from '../identity-manager/identity-manager-page-layout';
import { defaultMessages, pageMessages, updateMessages } from './saml-messages';
import { AddButton, ExperiencingProblemsText, MigrateButton, SamlPageImpl } from './saml-page';
import { SamlPrompt } from './saml-prompt';

describe('SamlPage', () => {
  const issuer = 'Fake issuer';
  const ssoUrl = 'fake sso url';
  const publicCertificate = 'fake public certificate';

  const sandbox = sinonSandbox.create();
  let deleteSpy;
  let updateSpy;
  let showDrawerSpy;

  beforeEach(() => {
    deleteSpy = sandbox.spy();
    updateSpy = sandbox.spy();
    showDrawerSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  function shallowSamlPageWithData(data = {}) {
    return shallow<SamlPageImpl>(
      <SamlPageImpl
        data={data as any}
        delete={deleteSpy}
        update={updateSpy}
        match={{ params: { orgId: 'FAKE' } } as any}
        intl={createMockIntlProp()}
        showDrawer={showDrawerSpy}
        location={null as any}
        history={null as any}
        showFlag={null as any}
        hideFlag={null as any}
      />,
      createMockIntlContext(),
    );
  }

  describe('before SAML configuration has been fetched', () => {
    it('renders with no title, description or side', () => {
      const wrapper = shallowSamlPageWithData({ loading: true });
      const pageLayout = wrapper.find(PageLayout);
      expect(pageLayout.props().title).to.be.undefined();
      expect(pageLayout.props().description).to.be.undefined();
      expect(pageLayout.props().side).to.be.undefined();
    });

    it('renders a spinner', () => {
      const wrapper = shallowSamlPageWithData({ loading: true });
      expect(wrapper.find(AkSpinner)).to.have.length(1);
    });
  });

  describe('after SAML configuration has been fetched', () => {
    it('renders with a title, description and side', () => {
      const wrapper = shallowSamlPageWithData({ organization: {} });
      const pageLayout = wrapper.find(IdentityManagerPageLayout);
      const pageLayoutProps = pageLayout.props() as PageLayoutProps;
      expect(pageLayoutProps.title).to.equal(pageMessages.title.defaultMessage);
      expect(pageLayoutProps.description).to.equal(pageMessages.description.defaultMessage);
      expect(pageLayoutProps.side).to.exist();
    });

    it('renders without a spinner', () => {
      const wrapper = shallowSamlPageWithData({ organization: {} });
      expect(wrapper.find(AkSpinner)).to.have.length(0);
    });

    describe('without a SAML configuration', () => {
      it('contains a prompt to add one', () => {
        const wrapper = shallowSamlPageWithData({ organization: { security: {} } });
        expect(wrapper.find('h4').text()).to.equal(updateMessages.addHeading.defaultMessage);

        const addButton = wrapper.find(AddButton);
        expect(addButton).to.have.length(1);
        expect(addButton.children().text()).to.equal(updateMessages.addButton.defaultMessage);

        addButton.simulate('click');
        expect(showDrawerSpy.getCalls()).to.have.length(1);
      });

      it('correct analytics are passed to the button', () => {
        const wrapper = shallowSamlPageWithData({ organization: { security: {} } });

        const addButton: any = wrapper.find(AddButton);
        expect(addButton.props().analyticsData).to.deep.equal({
          action: 'click',
          actionSubject: 'addSamlButton',
          actionSubjectId: 'openFocusedState',
          subproduct: 'organization',
          tenantId: 'FAKE',
          tenantType: 'organizationId',
        });
      });
    });

    describe('with a SAML configuration', () => {
      it('contains edit and delete buttons', () => {
        const wrapper = shallowSamlPageWithData({ organization: { security: { saml: { serviceProvider: {} } } } });
        const buttons = wrapper.find(AkButtonGroup).find(AnalyticsButton);
        expect(buttons).to.have.length(2);

        const editButton = buttons.first();
        const deleteButton = buttons.last();

        expect(editButton.children().text()).to.equal(defaultMessages.edit.defaultMessage);
        expect(deleteButton.children().text()).to.equal(defaultMessages.delete.defaultMessage);

        editButton.simulate('click');
        expect(showDrawerSpy.getCalls()).to.have.length(1);

        deleteButton.simulate('click');
        expect(wrapper.state().isDeleteDialogOpen).to.be.true();
      });

      it('edit and delete buttons sets correct analytics data', () => {
        const wrapper = shallowSamlPageWithData({ organization: { security: { saml: { serviceProvider: {} } } } });
        const buttons = wrapper.find(AkButtonGroup).find(AnalyticsButton);
        expect(buttons).to.have.length(2);

        const editButton = buttons.first();
        const deleteButton = buttons.last();

        expect(editButton.props().analyticsData).to.deep.equal({
          action: 'click',
          actionSubject: 'editSamlButton',
          actionSubjectId: 'openFocusedState',
          subproduct: 'organization',
          tenantId: 'FAKE',
          tenantType: 'organizationId',
        });
        expect(deleteButton.props().analyticsData).to.deep.equal({
          action: 'click',
          actionSubject: 'deleteSamlButton',
          actionSubjectId: 'openModal',
          subproduct: 'organization',
          tenantId: 'FAKE',
          tenantType: 'organizationId',
        });
      });

      it('shows correct info with an AUTH0FORCED configuration', () => {
        const wrapper = shallowSamlPageWithData({ organization: { security: { saml: { serviceProvider: {}, auth0MigrationState: 'AUTH0FORCED' } } } });

        const dialog = wrapper.find(AkSectionMessage);
        expect(dialog).to.have.length(0);

        const text = wrapper.find(ExperiencingProblemsText);
        expect(text).to.have.length(0);

        const prompt = wrapper.find(SamlPrompt);
        expect(prompt).to.have.length(1);

        const configPrompt = shallow(prompt.first().props().body as any);
        expect(configPrompt.find('h5').text()).to.deep.equal('Just one more step to go!');
      });

      it('shows restore info with an AUTH0 configuration', () => {
        const wrapper = shallowSamlPageWithData({
          organization: { security: { saml: { serviceProvider: {}, auth0MigrationState: 'AUTH0' } } },
        });

        const dialog = wrapper.find(AkSectionMessage);
        expect(dialog).to.have.length(0);

        const text = wrapper.find(ExperiencingProblemsText);
        expect(text).to.have.length(1);

        const prompts = wrapper.find(SamlPrompt);
        expect(prompts).to.have.length(2);
        const configPrompt = shallow(prompts.first().props().body as any);
        const restorePrompt = shallow(prompts.last().props().body as any);

        expect(configPrompt.find('h5').text()).to.deep.equal('Just one more step to go!');
        expect(restorePrompt.find(FormattedMessage).props().defaultMessage).to.contain('save them to your identity provider');

        const restoreButton = restorePrompt.find(FormattedMessage).props().values!.restoreConfig as any;
        expect(restoreButton.props.appearance).to.deep.equal('link');
        expect(restoreButton.props.children.props.children).to.deep.equal('Restore original SAML configuration');
      });

      it('shows update info with a LEGACY configuration', () => {
        const wrapper = shallowSamlPageWithData({
          organization: { security: { saml: { serviceProvider: {}, auth0MigrationState: 'LEGACY' } } },
        });

        const prompts = wrapper.find(SamlPrompt);
        expect(prompts).to.have.length(0);

        const dialog = wrapper.find(AkSectionMessage);
        expect(dialog).to.have.length(1);

        const message = dialog.find(FormattedHTMLMessage);
        expect(message.props().defaultMessage).to.contain('Have you copied the new values to your identity provider?');

        const button: any = dialog.find(MigrateButton);
        expect(button.props().appearance).to.deep.equal('link');
        expect(button.find(FormattedMessage).props().defaultMessage).to.equal('Yes, update my configuration');
      });

      it('calls update with correct arguments with an AUTH0 configuration', async () => {
        const promise = Promise.resolve(true);
        updateSpy = sandbox.stub().returns(promise);
        const wrapper = shallowSamlPageWithData({
          organization: { security: { saml: { serviceProvider: {}, auth0MigrationState: 'AUTH0', issuer, ssoUrl, publicCertificate } } },
        });
        const prompts = wrapper.find(SamlPrompt);
        const restorePrompt = shallow(prompts.last().props().body as any);

        expect(restorePrompt.find(FormattedMessage).props().defaultMessage).to.contain('save them to your identity provider');

        const restoreButton = shallow(restorePrompt.find(FormattedMessage).props().values!.restoreConfig as any);
        expect(updateSpy.callCount).to.equal(0);
        restoreButton.simulate('click');

        await promise;
        expect(updateSpy.callCount).to.equal(1);
        expect(updateSpy.lastCall.args[0]).to.deep.include({
          variables: {
            input: { id: 'FAKE', issuer, ssoUrl, publicCertificate, auth0MigrationState: 'LEGACY' },
          },
        });
      });

      it('calls update with correct arguments with a LEGACY configuration', async () => {
        const promise = Promise.resolve(true);
        updateSpy = sandbox.stub().returns(promise);
        const wrapper = shallowSamlPageWithData({
          organization: { security: { saml: { serviceProvider: {}, auth0MigrationState: 'LEGACY', issuer, ssoUrl, publicCertificate } } },
        });

        const dialog = wrapper.find(AkSectionMessage);
        const button = dialog.find(MigrateButton);

        expect(updateSpy.callCount).to.equal(0);
        button.simulate('click');

        await promise;
        expect(updateSpy.callCount).to.equal(1);
        expect(updateSpy.lastCall.args[0]).to.deep.include({
          variables: {
            input: { id: 'FAKE', issuer, ssoUrl, publicCertificate, auth0MigrationState: 'AUTH0' },
          },
        });
      });
    });
  });

  describe('after SAML configuration has been changed', () => {
    it('opens complete prompt when new configuration is saved', () => {
      const wrapper = shallowSamlPageWithData({ organization: { security: {} } });
      expect(wrapper.find(SamlPrompt)).to.have.length(0);

      wrapper.setProps({ data: { organization: { security: { saml: { serviceProvider: {}, issuer, ssoUrl, publicCertificate } } } } } as any);
      expect(wrapper.find(SamlPrompt).props().isOpen).to.equal(true);
    });

    it('opens complete prompt when a configuration change is saved', () => {
      const wrapper = shallowSamlPageWithData({ organization: { security: { saml: { serviceProvider: {}, issuer, ssoUrl, publicCertificate } } } });
      expect(wrapper.find(SamlPrompt).props().isOpen).to.equal(false);

      wrapper.setProps({ data: { organization: { security: { saml: { serviceProvider: {}, issuer: 'different issuer', ssoUrl, publicCertificate } } } } } as any);
      expect(wrapper.find(SamlPrompt).props().isOpen).to.equal(true);
    });

    it('does not open complete prompt when configuration has not changed', () => {
      const wrapper = shallowSamlPageWithData({ loading: true });
      expect(wrapper.find(SamlPrompt)).to.have.length(0);
      wrapper.setProps({ data: { organization: { security: { saml: { serviceProvider: {}, issuer, ssoUrl, publicCertificate } } } } } as any);
      expect(wrapper.find(SamlPrompt).props().isOpen).to.equal(false);
      wrapper.setProps({ data: { organization: { security: { saml: { serviceProvider: {}, issuer, ssoUrl, publicCertificate } } } } } as any);
      expect(wrapper.find(SamlPrompt).props().isOpen).to.equal(false);
    });
  });
});
