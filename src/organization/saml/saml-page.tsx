import { ApolloQueryResult } from 'apollo-client';
import * as React from 'react';
import { ChildProps, compose, graphql, MutationOpts } from 'react-apollo';
import { FormattedHTMLMessage, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import { Link, RouteComponentProps } from 'react-router-dom';
import styled from 'styled-components';

import AkButton, { ButtonGroup as AkButtonGroup } from '@atlaskit/button';
import AkInfoIcon from '@atlaskit/icon/glyph/info';
import AkWarningIcon from '@atlaskit/icon/glyph/warning';
import AkSectionMessage from '@atlaskit/section-message';
import AkSpinner from '@atlaskit/spinner';
import {
  akCodeFontFamily,
  akColorN10, akColorN20, akColorN300, akColorP300, akColorY300,
  akFontFamily, akFontSizeDefault, akGridSizeUnitless,
} from '@atlaskit/util-shared-styles';

import { Button as AnalyticsButton } from 'common/analytics/components';
import { createErrorIcon, createSuccessIcon } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';
import { PageLayout, PageSidebar } from 'common/page-layout';

import { analyticsClient, Referrer, samlScreen, samlScreenEvent } from 'common/analytics';
import { ShowDrawerProps, withShowDrawer } from 'common/drawer';

import { OrgBreadcrumbs } from '../breadcrumbs';

import {
  Auth0State,
  DeleteSamlMutation,
  DeleteSamlMutationVariables,
  SamlConfigurationQuery,
  UpdateSamlMutation,
  UpdateSamlMutationVariables,
} from '../../schema/schema-types';
import { IdentityManagerPageLayout } from '../identity-manager/identity-manager-page-layout';
import { createOrgAnalyticsData } from '../organizations/organization-analytics';
import deleteSaml from './delete-saml.mutation.graphql';
import getSamlConfiguration from './saml-configuration.query.graphql';
import { SamlInstructionsModal } from './saml-instructions-modal';
import { completePromptMessages, copyMessages, defaultMessages, migrationMessages, pageMessages, sidebarMessages, updateMessages } from './saml-messages';
import { SamlPrompt } from './saml-prompt';
import { SamlRemovalModal } from './saml-removal-modal';
import updateSaml from './update-saml.mutation.graphql';

export const SamlConfigDescription = styled.div`
  margin-bottom: ${akGridSizeUnitless * 5}px;
`;

const SamlConfigDl = styled.dl`
  margin: ${akGridSizeUnitless * 2}px 0 ${akGridSizeUnitless * 4}px;
  padding-left: 0;

  dt {
    color: ${akColorN300};
    font-size: 12px;
    font-weight: 600;
    margin: 0 0 ${akGridSizeUnitless}px;
  }
  dd {
    margin: 0;

    + dt {
      margin-top: ${akGridSizeUnitless * 2}px;
    }
  }
`;

const PublicCertificateDd = styled.dd`
  display: flex;

  > pre {
    font-family: ${akFontFamily};
    margin-right: 0.5em;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  }
  &.revealed {
    display: block;

    > pre {
      font-family: ${akCodeFontFamily};
      text-overflow: clip;
      white-space: pre;
    }
  }
`;

const CopyTextBox = styled.input`
  background: ${akColorN10};
  border: 1px solid ${akColorN20};
  border-radius: 5px;
  box-sizing: border-box;
  font-family: ${akFontFamily};
  font-size: ${akFontSizeDefault};
  height: ${akGridSizeUnitless * 4}px;
  margin: 0 3px 0 0;
  padding: 9px;
  vertical-align: middle;
  width: ${akGridSizeUnitless * 43}px;
`;

export const AddButton: any = styled(AnalyticsButton) `
  margin-top: ${akGridSizeUnitless * 2}px !important;
`;

const TextBeforeIcon = styled.div`
  display: inline-block;

  + div {
    display: inline-block;
    margin-left: ${akGridSizeUnitless / 2}px;
    margin-top: ${akGridSizeUnitless / -2}px;
    vertical-align: text-top;
  }
`;

export const ExperiencingProblemsText = styled(TextBeforeIcon) `
  padding-top: ${akGridSizeUnitless}px;
  padding-bottom: ${akGridSizeUnitless * 2}px;
`;

const ListItem = styled.li`
  padding: ${akGridSizeUnitless}px;
`;

const InlineButton = styled(AkButton) `
  vertical-align: bottom !important;
`;

export const MigrateButton: any = styled(AnalyticsButton) `
  h5 {
    margin-top: ${akGridSizeUnitless * 1.5}px;
  }
`;

const Warning = props => <AkWarningIcon {...props} primaryColor={akColorY300} />;

const PurpleInfo = props => <AkInfoIcon {...props} primaryColor={akColorP300} />;

const SectionWrapper = styled.div`
  margin: ${akGridSizeUnitless * 2}px 0 !important;
`;

// tslint:disable-next-line:no-var-requires no-require-imports
const Keys = require('./saml.svg');

interface OrgParams {
  orgId: string;
}
interface OwnProps extends RouteComponentProps<OrgParams> {
  delete(opts: MutationOpts & { variables: DeleteSamlMutationVariables }): Promise<ApolloQueryResult<DeleteSamlMutation>>;
  update(opts: MutationOpts & { variables: UpdateSamlMutationVariables }): Promise<ApolloQueryResult<UpdateSamlMutation>>;
}
type SamlPageProps = ChildProps<OwnProps & FlagProps & ShowDrawerProps, SamlConfigurationQuery>;

interface State {
  isCompletePromptOpen: boolean;
  isRestoreLegacyPromptOpen: boolean;
  isMigrateButtonDisabled: boolean;
  isDeleteDialogOpen: boolean;
  isDeleting: boolean;
  isInstructionsDialogOpen: boolean;
  publicCertificateRevealed: boolean;
}

export class SamlPageImpl extends React.Component<SamlPageProps & InjectedIntlProps, State> {
  public entityIdField: HTMLInputElement | undefined;
  public acsUrlField: HTMLInputElement | undefined;

  public state: State = {
    isCompletePromptOpen: false,
    isRestoreLegacyPromptOpen: false,
    isMigrateButtonDisabled: false,
    isDeleteDialogOpen: false,
    isDeleting: false,
    isInstructionsDialogOpen: false,
    publicCertificateRevealed: false,
  };

  public componentWillReceiveProps(nextProps: SamlPageProps) {
    if (!this.props.data || this.props.data.loading || this.props.data.error) {
      return;
    }
    if (this.props.match.params.orgId !== nextProps.match.params.orgId) {
      return;
    }
    if (!this.props.data.organization || !nextProps.data || !nextProps.data.organization) {
      return;
    }

    const prevConfig = this.props.data.organization.security.saml;
    const newConfig = nextProps.data.organization.security.saml;
    // Open complete prompt if SAML has been newly configured or updated for an org
    if ((!prevConfig && newConfig) || (prevConfig && newConfig &&
      (prevConfig.ssoUrl !== newConfig.ssoUrl || prevConfig.issuer !== newConfig.issuer || prevConfig.publicCertificate !== newConfig.publicCertificate))) {
      this.setState({ isCompletePromptOpen: true });
    }
  }

  public componentWillUpdate(nextProps: SamlPageProps) {
    const defaultData = { error: undefined };
    const {
      data: { error } = defaultData,
    } = this.props;
    const nextError = (nextProps.data || defaultData).error;

    if (error === nextError || !nextError) {
      return;
    }

    this.props.showFlag({
      icon: createErrorIcon(),
      id: `SAML_ERROR.${Date.now()}`,
      title: nextError.graphQLErrors[0].message,
    });
  }

  public render() {
    const {
      intl: { formatMessage },
      data: {
        loading = false,
        organization = null,
      } = {},
    } = this.props;

    const sidePanel = (
      <PageSidebar imgAfterSrc={Keys}>
        <h4>{formatMessage(sidebarMessages.heading)}</h4>
        <ul>
          <ListItem>
            <FormattedMessage
              {...sidebarMessages.point1}
              values={{
                domainslink: (<Link to="./domains">{formatMessage(sidebarMessages.point1Link)}</Link>),
              }}
            />
          </ListItem>
          <ListItem>{formatMessage(sidebarMessages.point2)}</ListItem>
          <ListItem>{formatMessage(sidebarMessages.point3)}</ListItem>
          <ListItem>
            <FormattedMessage
              {...sidebarMessages.point4}
              values={{
                equivalentsLink: (<a href="https://confluence.atlassian.com/x/gPYCO#SAMLsinglesign-on-SAMLsinglesign-onwithtwo-stepverificationandpasswordpolicy" target="_blank" rel="noopener noreferrer">
                  {formatMessage(sidebarMessages.point4Link)}
                </a>),
              }}
            />
          </ListItem>
          <ListItem>{formatMessage(sidebarMessages.point5)}</ListItem>
          <ListItem>
            <a href="https://confluence.atlassian.com/x/gPYCO" target="_blank" rel="noopener noreferrer">
              {formatMessage(sidebarMessages.point6)}
            </a>
          </ListItem>
        </ul>
      </PageSidebar>
    );

    const prompt = (
      <SamlPrompt
        isOpen={this.state.isCompletePromptOpen}
        body={(
          <div>
            <h5>{formatMessage(completePromptMessages.heading)}</h5>
            <p>{formatMessage(completePromptMessages.description)}</p>
          </div>
        )}
        icon={(<Warning label="" />)}
        onClose={this.onCloseCompletePrompt}
        onOpen={this.onOpenCompletePrompt}
      />
    );

    const legacyToAuth0TogglePrompt = (
      <SectionWrapper>
        <AkSectionMessage appearance="warning">
          <FormattedHTMLMessage {...migrationMessages.migrateToAuth0} />
          <MigrateButton
            appearance="link"
            spacing="none"
            onClick={this.getUpdateHandler('AUTH0')}
            isDisabled={this.state.isMigrateButtonDisabled}
            analyticsData={createOrgAnalyticsData({
              orgId: this.props.match.params.orgId,
              action: 'click',
              actionSubject: 'migrateSamlToAuth0Button',
              actionSubjectId: 'migrateSamlToAuth0Button',
            })}
          >
            <FormattedMessage {...migrationMessages.updateConfig} tagName="h5" />
          </MigrateButton>
        </AkSectionMessage>
      </SectionWrapper>
    );

    const auth0ToLegacyTogglePrompt = (
      <span>
        <ExperiencingProblemsText>{formatMessage(migrationMessages.experiencingProblems)}</ExperiencingProblemsText>
        <SamlPrompt
          isOpen={this.state.isRestoreLegacyPromptOpen}
          body={(
            <div>
              <FormattedMessage
                {...migrationMessages.migrateToLegacy}
                values={{
                  troubleshootingLink: (<a href="https://confluence.atlassian.com/x/gPYCO" target="_blank" rel="noopener noreferrer">{formatMessage(migrationMessages.troubleshootingLink)}</a>),
                  retrieveOriginalUrls: (<a href="https://confluence.atlassian.com/x/gPYCO#SAMLsinglesign-on-UpdateyourSAMLconfiguration" target="_blank" rel="noopener noreferrer">{formatMessage(migrationMessages.retrieveOriginalUrls)}</a>),
                  restoreConfigButton: (<b>{formatMessage(migrationMessages.restoreOriginalButton)}</b>),
                  supportLink: (<a href="https://support.atlassian.com/" target="_blank" rel="noopener noreferrer">{formatMessage(migrationMessages.supportLink)}</a>),
                  restoreConfig: (
                    <MigrateButton
                      appearance="link"
                      spacing="none"
                      onClick={this.getUpdateHandler('LEGACY')}
                      isDisabled={this.state.isMigrateButtonDisabled}
                      analyticsData={createOrgAnalyticsData({
                        orgId: this.props.match.params.orgId,
                        action: 'click',
                        actionSubject: 'migrateSamlToLegacyButton',
                        actionSubjectId: 'migrateSamlToLegacyButton',
                      })}
                    >
                      <h5>{formatMessage(migrationMessages.restore)}</h5>
                    </MigrateButton>
                  ),
                }}
              />
            </div>
          )}
          icon={(<PurpleInfo label="" />)}
          placement="right"
          onClose={this.onCloseRestoreLegacyPrompt}
          onOpen={this.onOpenRestoreLegacyPrompt}
        />
      </span>
    );

    if (loading) {
      return (<PageLayout><AkSpinner /></PageLayout>);
    }

    return (
        <Referrer value="saml">
          <IdentityManagerPageLayout
            title={formatMessage(pageMessages.title)}
            description={formatMessage(pageMessages.description)}
            domainPrompt={formatMessage(defaultMessages.domainPrompt)}
            side={sidePanel}
            orgId={this.props.match.params.orgId}
            analyticsScreenForEvalButton={samlScreen}
            cardDescription={formatMessage(defaultMessages.identityManagerEvaluation)}
            breadcrumbs={<OrgBreadcrumbs/>}
            screenEvent={samlScreenEvent}
          >
            <SamlConfigDescription>
              <h2>{formatMessage(defaultMessages.heading)}</h2>
              <FormattedMessage
                {...defaultMessages.description}
                values={{
                  instructionsLink: (
                    <InlineButton appearance="link" spacing="none" onClick={this.onOpenInstructions}>
                      {formatMessage(defaultMessages.descriptionLink)}
                    </InlineButton>
                  ),
                }}
                tagName="p"
              />
            </SamlConfigDescription>
            <SamlInstructionsModal
              isOpen={this.state.isInstructionsDialogOpen}
              onDismiss={this.onCloseInstructions}
            />
            {organization && organization.security && (
              organization.security.saml ? (
                <div>
                  <TextBeforeIcon><h4>{formatMessage(defaultMessages.requiredHeading)}</h4></TextBeforeIcon>
                  {(organization.security.saml.auth0MigrationState !== 'LEGACY') && prompt}
                  {organization.security.saml.auth0MigrationState === 'LEGACY' ? legacyToAuth0TogglePrompt
                    : organization.security.saml.auth0MigrationState === 'AUTH0' ? auth0ToLegacyTogglePrompt
                      : null
                  }
                  <p>{formatMessage(defaultMessages.requiredDescription)}</p>
                  <SamlConfigDl>
                    <dt>{formatMessage(defaultMessages.entityId)}</dt>
                    <dd>
                      {/* tslint:disable-next-line:react-this-binding-issue */}
                      <CopyTextBox value={organization.security.saml.serviceProvider.entityId} readOnly={true} innerRef={i => this.entityIdField = i} />
                      <AkButton onClick={this.onCopyEntityId}>
                        {formatMessage(defaultMessages.copy)}
                      </AkButton>
                    </dd>
                    <dt>{formatMessage(defaultMessages.acsUrl)}</dt>
                    <dd>
                      {/* tslint:disable-next-line:react-this-binding-issue */}
                      <CopyTextBox value={organization.security.saml.serviceProvider.acsUrl} readOnly={true} innerRef={i => this.acsUrlField = i} />
                      <AkButton onClick={this.onCopyAcsUrl}>
                        {formatMessage(defaultMessages.copy)}
                      </AkButton>
                    </dd>
                  </SamlConfigDl>
                  <h4>{formatMessage(defaultMessages.currentHeading)}</h4>
                  <SamlConfigDl>
                    <dt>{formatMessage(defaultMessages.issuer)}</dt>
                    <dd>{organization.security.saml.issuer}</dd>
                    <dt>{formatMessage(defaultMessages.ssoUrl)}</dt>
                    <dd>{organization.security.saml.ssoUrl}</dd>
                    <dt>{formatMessage(defaultMessages.publicCertificate)}</dt>
                    <PublicCertificateDd className={this.state.publicCertificateRevealed ? 'revealed' : ''}>
                      <pre>{organization.security.saml.publicCertificate}</pre>
                      <AkButton appearance="link" spacing="none" onClick={this.onToggleCertificate}>
                        {formatMessage(this.state.publicCertificateRevealed ? defaultMessages.currentCertificateHide : defaultMessages.currentCertificateShow)}
                      </AkButton>
                    </PublicCertificateDd>
                  </SamlConfigDl>
                  <AkButtonGroup>
                    <AnalyticsButton
                      onClick={this.openSamlDrawer}
                      analyticsData={createOrgAnalyticsData({
                        orgId: this.props.match.params.orgId,
                        action: 'click',
                        actionSubject: 'editSamlButton',
                        actionSubjectId: 'openFocusedState',
                      })}
                    >
                      {formatMessage(defaultMessages.edit)}
                    </AnalyticsButton>
                    <AnalyticsButton
                      appearance="link"
                      onClick={this.onShowDeleteConfirmation}
                      analyticsData={createOrgAnalyticsData({
                        orgId: this.props.match.params.orgId,
                        action: 'click',
                        actionSubject: 'deleteSamlButton',
                        actionSubjectId: 'openModal',
                      })}
                    >
                      {formatMessage(defaultMessages.delete)}
                    </AnalyticsButton>
                  </AkButtonGroup>
                  <SamlRemovalModal
                    isOpen={this.state.isDeleteDialogOpen}
                    isDeleting={this.state.isDeleting}
                    orgId={this.props.match.params.orgId}
                    onCancel={this.onCancel}
                    onDelete={this.onDelete}
                  />
                </div>
              ) : (
                  <div>
                    <h4>{formatMessage(updateMessages.addHeading)}</h4>
                    <p>{formatMessage(updateMessages.description)}</p>
                    <AddButton
                      appearance="primary"
                      onClick={this.openSamlDrawer}
                      analyticsData={createOrgAnalyticsData({
                        orgId: this.props.match.params.orgId,
                        action: 'click',
                        actionSubject: 'addSamlButton',
                        actionSubjectId: 'openFocusedState',
                      })}
                    >
                      {formatMessage(updateMessages.addButton)}
                    </AddButton>
                  </div>
                )
            )}
          </IdentityManagerPageLayout>
        </Referrer>
    );
  }

  private onCopyEntityId = () => {
    if (this.entityIdField) {
      this.entityIdField.select();
      this.copySelected('entity-id-copy', copyMessages.copyEntityIdSuccess, copyMessages.copyEntityIdFail);
    }
  }

  private onCopyAcsUrl = () => {
    if (this.acsUrlField) {
      this.acsUrlField.select();
      this.copySelected('acs-url-copy', copyMessages.copyAcsUrlSuccess, copyMessages.copyAcsUrlFail);
    }
  }

  private copySelected = (idPrefix: string, onCopySuccess: FormattedMessage.MessageDescriptor, onCopyFail: FormattedMessage.MessageDescriptor) => {
    const result = document.execCommand('copy');
    this.props.showFlag({
      autoDismiss: true,
      icon: result ? createSuccessIcon() : createErrorIcon(),
      id: `${idPrefix}-${result}.${Date.now()}`,
      title: this.props.intl.formatMessage(result ? onCopySuccess : onCopyFail),
    });
  }

  private onToggleCertificate = () => {
    this.setState({
      publicCertificateRevealed: !this.state.publicCertificateRevealed,
    });
  }

  private onShowDeleteConfirmation = () => {
    this.setState({
      isDeleteDialogOpen: true,
    });
  }

  private onCancel = () => {
    this.setState({
      isDeleteDialogOpen: false,
    });
  }

  private onOpenCompletePrompt = () => {
    this.setState({
      isCompletePromptOpen: true,
    });
  }

  private onCloseCompletePrompt = () => {
    this.setState({
      isCompletePromptOpen: false,
    });
  }

  private onOpenRestoreLegacyPrompt = () => {
    this.setState({
      isRestoreLegacyPromptOpen: true,
    });
  }

  private onCloseRestoreLegacyPrompt = () => {
    this.setState({
      isRestoreLegacyPromptOpen: false,
    });
  }

  private onOpenInstructions = () => {
    this.setState({
      isInstructionsDialogOpen: true,
    });
  }

  private onCloseInstructions = () => {
    this.setState({
      isInstructionsDialogOpen: false,
    });
  }

  private getUpdateHandler = (newState: Auth0State) => {
    return () => {
      this.setState({ isMigrateButtonDisabled: true });
      this.onUpdate(newState);
    };
  }

  private onUpdate = (newState: Auth0State) => {
    if (!this.props.data || !this.props.data.organization || !this.props.data.organization.security.saml) {
      return;
    }

    const { formatMessage } = this.props.intl;
    const {
      update,
      match: { params: { orgId } },
    } = this.props;
    const {
      issuer,
      ssoUrl,
      publicCertificate,
    } = this.props.data.organization.security.saml;

    update({
      variables: { input: { id: orgId, issuer, ssoUrl, publicCertificate, auth0MigrationState: newState } },
      optimisticResponse: {
        updateSaml: true,
      },
      refetchQueries: [{
        query: getSamlConfiguration,
        variables: { id: orgId },
      }],
    }).then(() => {
      this.setState({ isMigrateButtonDisabled: false });
      this.props.showFlag({
        autoDismiss: true,
        icon: createSuccessIcon(),
        id: `SAML_UPDATE_SUCCESS.${Date.now()}`,
        title: formatMessage(migrationMessages.updateSuccess),
        description: formatMessage(migrationMessages.updateSuccessDescription),
      });
    }).catch((error) => {
      this.setState({ isMigrateButtonDisabled: false });
      analyticsClient.onError(error);
    });
  };

  private onDelete = () => {
    this.setState({ isDeleting: true });
    const { formatMessage } = this.props.intl;
    const {
      match: { params: { orgId } },
    } = this.props;

    this.props.delete({
      variables: { id: orgId },
      optimisticResponse: {
        deleteSaml: true,
      },
      refetchQueries: [{
        query: getSamlConfiguration,
        variables: { id: orgId },
      }],
    }).then(() => {
      this.setState({
        isDeleteDialogOpen: false,
        isDeleting: false,
      });
      this.props.showFlag({
        autoDismiss: true,
        icon: createSuccessIcon(),
        id: `SAML_DELETE_SUCCESS.${Date.now()}`,
        title: formatMessage(defaultMessages.deleteSuccess),
      });
    }).catch((error) => {
      this.setState({ isDeleting: false });
      analyticsClient.onError(error);
    });
  }

  private openSamlDrawer = () => {
    this.props.showDrawer('SAML');
  }
}

export const SamlPage = compose(
  withShowDrawer,
  graphql<SamlPageProps, SamlConfigurationQuery>(getSamlConfiguration, {
    options: ({ match: { params: { orgId } } }) => ({ variables: { id: orgId } }),
  }),
  graphql<any, UpdateSamlMutation>(updateSaml, { name: 'update' }),
  graphql<any, DeleteSamlMutation>(deleteSaml, { name: 'delete' }),
  withFlag,
  injectIntl,
)(SamlPageImpl);
