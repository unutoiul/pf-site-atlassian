import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { sandbox as sinonSandbox } from 'sinon';

import { Button as AnalyticsButton } from 'common/analytics';
import { ModalDialog } from 'common/modal';

import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import { SamlRemovalModalImpl as SamlRemovalModal } from './saml-removal-modal';

describe('SamlRemovalModal', () => {
  const sandbox = sinonSandbox.create();
  let onCancelSpy;
  let onDeleteSpy;

  beforeEach(() => {
    onCancelSpy = sandbox.spy();
    onDeleteSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  function shallowSamlRemovalModal(isDeleting = false) {
    return shallow(
      <SamlRemovalModal
        isOpen={true}
        orgId="ORG_ID"
        isDeleting={isDeleting}
        onCancel={onCancelSpy}
        onDelete={onDeleteSpy}
        intl={createMockIntlProp()}
      />,
      createMockIntlContext(),
    );
  }

  it('passes isOpen and onClose', () => {
    const wrapper = shallowSamlRemovalModal();
    expect(wrapper.find(ModalDialog).prop('isOpen')).to.be.true();
    expect(wrapper.find(ModalDialog).prop('onClose')).to.equal(onCancelSpy);
  });

  it('contains a confirm button that triggers onDelete when clicked', () => {
    const wrapper = shallowSamlRemovalModal();
    const modal = wrapper.find(ModalDialog);
    const footer = shallow(modal.props().footer as React.ReactElement<any>);
    const button = footer.find(AnalyticsButton).first();

    expect(button).to.have.length(1);
    button.simulate('click');
    expect(onDeleteSpy.called).to.be.true();
  });

  it('confirm button is disabled while loading', () => {
    const wrapper = shallowSamlRemovalModal(true);
    const modal = wrapper.find(ModalDialog);
    const footer = shallow(modal.props().footer as React.ReactElement<any>);
    const button = footer.find(AnalyticsButton).first();

    expect(button).to.have.length(1);
    expect(button.props().isLoading).to.equal(true);
  });

  it('contains a cancel button that triggers onCancel when clicked', () => {
    const wrapper = shallowSamlRemovalModal();
    const modal = wrapper.find(ModalDialog);
    const footer = shallow(modal.props().footer as React.ReactElement<any>);
    const button = footer.find(AnalyticsButton).filterWhere(b => b.prop('appearance') === 'subtle-link');

    expect(button).to.have.length(1);
    button.simulate('click');
    expect(onCancelSpy.called).to.be.true();
  });

  it('correct analytics are passed to the delete button', () => {
    const wrapper = shallowSamlRemovalModal();
    const modal = wrapper.find(ModalDialog);
    const footer = shallow(modal.props().footer as React.ReactElement<any>);
    const button = footer.find(AnalyticsButton).first();

    expect(button).to.have.length(1);
    expect(button.props().analyticsData).to.deep.equal({
      action: 'click',
      actionSubject: 'deleteSamlButton',
      actionSubjectId: 'delete',
      subproduct: 'organization',
      tenantId: 'ORG_ID',
      tenantType: 'organizationId',
    });
  });

  it('correct analytics are passed to the cancel button', () => {
    const wrapper = shallowSamlRemovalModal();
    const modal = wrapper.find(ModalDialog);
    const footer = shallow(modal.props().footer as React.ReactElement<any>);
    const button = footer.find(AnalyticsButton).filterWhere(b => b.prop('appearance') === 'subtle-link');

    expect(button).to.have.length(1);
    expect(button.props().analyticsData).to.deep.equal({
      action: 'click',
      actionSubject: 'deleteSamlButton',
      actionSubjectId: 'cancel',
      subproduct: 'organization',
      tenantId: 'ORG_ID',
      tenantType: 'organizationId',
    });
  });
});
