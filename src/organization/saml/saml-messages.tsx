import { defineMessages } from 'react-intl';

export const pageMessages = defineMessages({
  title: {
    id: 'organizations.saml.heading',
    defaultMessage: 'SAML single sign-on',
  },
  description: {
    id: 'organizations.saml.description',
    defaultMessage: `Single sign-on with SAML allows your users to log in using your organization's identity provider to access all your Atlassian Cloud applications.`,
  },
});

export const defaultMessages = defineMessages({
  heading: {
    id: `organizations.saml.configuration.heading`,
    defaultMessage: `SAML configuration`,
  },
  description: {
    id: `organizations.saml.configuration.description`,
    defaultMessage: `How you configure SAML depends on which identity provider you use. See our SAML configuration {instructionsLink}.`,
  },
  descriptionLink: {
    id: `organizations.saml.configuration.description.link`,
    defaultMessage: `instructions for different identity providers`,
  },
  requiredHeading: {
    id: `organizations.saml.configuration.required.heading`,
    defaultMessage: `Information required by your identity provider`,
  },
  requiredDescription: {
    id: `organizations.saml.configuration.required.description`,
    defaultMessage: `To complete your SAML configuration, move the values for SP Entity ID and SP Assertion Consumer Service URL below to your identity provider:`,
  },
  currentHeading: {
    id: `organizations.saml.configuration.current.heading`,
    defaultMessage: `Your current SAML configuration`,
  },
  currentCertificateHide: {
    id: `organizations.saml.configuration.current.certificate.hide`,
    defaultMessage: `Show less`,
  },
  currentCertificateShow: {
    id: `organizations.saml.configuration.current.certificate.show`,
    defaultMessage: `Show more`,
  },
  issuer: {
    id: `organizations.saml.configuration.issuer`,
    defaultMessage: `Identity provider Entity ID`,
  },
  issuerDescription: {
    id: `organizations.saml.configuration.issuer.description`,
    defaultMessage: `The URL your identity provider uses for SAML 2.0.`,
  },
  ssoUrl: {
    id: `organizations.saml.configuration.ssoUrl`,
    defaultMessage: `Identity provider SSO URL`,
  },
  ssoUrlDescription: {
    id: `organizations.saml.configuration.ssoUrl.description`,
    defaultMessage: `The SAML endpoint URL given to you by your identity provider.`,
  },
  publicCertificate: {
    id: `organizations.saml.configuration.publicCertificate`,
    defaultMessage: `Public x509 certificate`,
  },
  publicCertificateDescription: {
    id: `organizations.saml.configuration.publicCertificate.description`,
    defaultMessage: `Copy and paste the entire certificate.`,
  },
  entityId: {
    id: `organizations.saml.configuration.entityId`,
    defaultMessage: `SP Entity ID`,
  },
  acsUrl: {
    id: `organizations.saml.configuration.acsUrl`,
    defaultMessage: `SP Assertion Consumer Service URL`,
  },
  copy: {
    id: `organizations.saml.configuration.copy.button`,
    defaultMessage: `Copy`,
  },
  edit: {
    id: `organizations.saml.configuration.edit.button`,
    defaultMessage: `Edit configuration`,
  },
  delete: {
    id: `organizations.saml.configuration.delete.button`,
    defaultMessage: `Delete configuration`,
  },
  deleteSuccess: {
    id: `organizations.saml.configuration.delete.success`,
    defaultMessage: `SAML configuration deleted`,
  },
  identityManagerEvaluation: {
    id: `organizations.saml.configuration.identityManagerEvaluation`,
    defaultMessage: `SAML is an Atlassian Access feature. Before you can apply a SAML configuration you need to have verified a domain and have an Atlassian Access subscription.`,
  },
  domainPrompt: {
    id: `organizations.saml.configuration.domain.prompt`,
    defaultMessage: `To enforce SAML on your managed accounts, you need to verify a domain.`,
  },
});

export const updateMessages = defineMessages({
  addHeading: {
    id: `organizations.saml.configuration.add.heading`,
    defaultMessage: `Add SAML configuration`,
  },
  updateHeading: {
    id: `organizations.saml.configuration.update.heading`,
    defaultMessage: `Edit SAML configuration`,
  },
  description: {
    id: `organizations.saml.configuration.update.description`,
    defaultMessage: `After initial setup with the identity provider, bring the values for Entity ID, SSO URL and Public x509 certificate across and add them to the SAML configuration here.`,
  },
  addButton: {
    id: `organizations.saml.configuration.add.button`,
    defaultMessage: `Add SAML configuration`,
  },
  saveButton: {
    id: `organizations.saml.configuration.save.button`,
    defaultMessage: `Save configuration`,
  },
  cancelButton: {
    id: `organizations.saml.configuration.cancel.button`,
    defaultMessage: `Cancel`,
  },
});

export const copyMessages = defineMessages({
  copyEntityIdSuccess: {
    id: `organizations.saml.configuration.copy.entityId.success`,
    defaultMessage: `Entity ID was copied to the clipboard.`,
  },
  copyEntityIdFail: {
    id: `organizations.saml.configuration.copy.entityId.fail`,
    defaultMessage: `Problem copying the Entity ID.`,
  },
  copyAcsUrlSuccess: {
    id: `organizations.saml.configuration.copy.acsUrl.success`,
    defaultMessage: `ACS URL was copied to the clipboard.`,
  },
  copyAcsUrlFail: {
    id: `organizations.saml.configuration.copy.acsUrl.fail`,
    defaultMessage: `Problem copying the ACS URL.`,
  },
});

export const sidebarMessages = defineMessages({
  heading: {
    id: `organizations.saml.configuration.sidebar.heading`,
    defaultMessage: `What you need to know`,
  },
  point1: {
    id: `organizations.saml.configuration.sidebar.point1`,
    defaultMessage: `SAML is only available for users from your {domainslink}.`,
  },
  point1Link: {
    id: `organizations.saml.configuration.sidebar.point1.link`,
    defaultMessage: `verified domains`,
  },
  point2: {
    id: `organizations.saml.configuration.sidebar.point2`,
    defaultMessage: `You can only have one SAML configuration - if you have multiple domains, your SAML provider will need to be configured to handle them.`,
  },
  point3: {
    id: `organizations.saml.configuration.sidebar.point3`,
    defaultMessage: `To test your SAML configuration, open a new incognito window, go to your Atlassian login and sign in with an email address on one of your verified domains.`,
  },
  point4: {
    id: `organizations.saml.configuration.sidebar.point4`,
    defaultMessage: `When SAML single sign-on is configured, users won't be subject to Atlassian password policy and two-step verification. {equivalentsLink} instead.`,
  },
  point4Link: {
    id: `organizations.saml.configuration.sidebar.point4.link`,
    defaultMessage: `Use your identity provider's equivalents`,
  },
  point5: {
    id: `organizations.saml.configuration.sidebar.point5`,
    defaultMessage: `During the time it takes to configure SAML single sign-on, users won't be able to log in to your Atlassian Cloud applications.
      Consider scheduling a day and time for the changeover to SAML, and alerting your users in advance.`,
  },
  point6: {
    id: `organizations.saml.configuration.sidebar.point6`,
    defaultMessage: `Learn more about SAML single sign-on.`,
  },
});

export const completePromptMessages = defineMessages({
  heading: {
    id: `organizations.saml.configuration.complete.prompt.heading`,
    defaultMessage: `Just one more step to go!`,
  },
  description: {
    id: `organizations.saml.configuration.complete.prompt.description`,
    defaultMessage: `Your users won't be able to log in with SAML single sign-on until you've copied the values for SP Entity ID and SP Assertion Consumer Service URL to your identity provider.`,
  },
  close: {
    id: `organizations.saml.configuration.complete.prompt.close`,
    defaultMessage: `Close`,
  },
});

export const migrationMessages = defineMessages({
  migrateToAuth0: {
    id: `organizations.saml.configuration.migration.migrate.to.auth0`,
    defaultMessage: `<p>We've made some changes to the way SAML single sign-on works on our end, which required changing
    the <b>SP Entity ID</b> and <b>SP Assertion Consumer Service URL</b> values below.</p> <p>You need to copy the new values from the fields
    below to your identity provider by April 6th, 2018. If you're using Azure, Okta, Onelogin, Bitium or Centrify,
    <a href="https://confluence.atlassian.com/x/gPYCO#SAMLsinglesign-on-UpdateyourSAMLconfiguration" target="_blank"
    rel="noopener noreferrer">follow our specific instructions.</a> Then return here and click <b>Yes, update my configuration</b> below.</p>
    <p>Have you copied the new values to your identity provider?</p>`,
  },
  updateConfig: {
    id: `organizations.saml.configuration.migration.update`,
    defaultMessage: `Yes, update my configuration`,
  },
  experiencingProblems: {
    id: `organizations.saml.configuration.migration.experiencing.problems`,
    defaultMessage: `Experiencing problems after updating your SAML single sign-on configuration?`,
  },
  migrateToLegacy: {
    id: `organizations.saml.configuration.migration.migrate.to.legacy`,
    defaultMessage: `Check out our {troubleshootingLink}. If that doesn't help - {retrieveOriginalUrls}, save them to your identity provider,
    then click the {restoreConfigButton} button and contact {supportLink}. {restoreConfig}`,
  },
  retrieveOriginalUrls: {
    id: `organizations.saml.configuration.migration.retrieve.original.urls`,
    defaultMessage: `retrieve the original URLs`,
  },
  restoreOriginalButton: {
    id: `organizations.saml.configuration.migration.restore.original.button`,
    defaultMessage: `Restore my original configuration`,
  },
  troubleshootingLink: {
    id: `organizations.saml.configuration.migration.troubleshooting.link`,
    defaultMessage: `troubleshooting guide`,
  },
  supportLink: {
    id: `organizations.saml.configuration.migration.support.link`,
    defaultMessage: `Atlassian Support`,
  },
  restore: {
    id: `organizations.saml.configuration.migration.restore`,
    defaultMessage: `Restore original SAML configuration`,
  },
  updateSuccess: {
    id: `organizations.saml.configuration.migration.update.success`,
    defaultMessage: `Your SAML configuration has updated`,
  },
  updateSuccessDescription: {
    id: `organizations.saml.configuration.migration.update.success.description`,
    defaultMessage: `Great, your SAML single sign-on is now up-to-date and ready to reap the benefits of future updates we make!`,
  },
});

export const deleteMessages = defineMessages({
  heading: {
    id: `organizations.saml.configurationdelete.heading`,
    defaultMessage: `Delete SAML configuration?`,
  },
  description: {
    id: `organizations.saml.configurationdelete.description`,
    defaultMessage: `Your users will get back to logging in with their credentials.`,
  },
  confirm: {
    id: `organizations.saml.configurationdelete.confirm`,
    defaultMessage: `Yes, delete it`,
  },
  cancel: {
    id: `organizations.saml.configurationdelete.cancel`,
    defaultMessage: `No, keep it`,
  },
});

export const instructionsMessages = defineMessages({
  heading: {
    id: `organizations.saml.configurationinstructions.heading`,
    defaultMessage: `SAML configuration instructions`,
  },
  description: {
    id: `organizations.saml.configurationinstructions.description`,
    defaultMessage: `Some identity providers have set up connectors for configuring SAML single sign-on with Atlassian. If your identity provider is listed below, follow the instructions from their website.`,
  },
  close: {
    id: `organizations.saml.configurationinstructions.close`,
    defaultMessage: `Got it`,
  },
  providerAzure: {
    id: `organizations.saml.configurationinstructions.provider.azure`,
    defaultMessage: `For Azure:`,
  },
  providerBitium: {
    id: `organizations.saml.configurationinstructions.provider.bitium`,
    defaultMessage: `For Bitium:`,
  },
  providerCentrify: {
    id: `organizations.saml.configurationinstructions.provider.centrify`,
    defaultMessage: `For Centrify:`,
  },
  providerOkta: {
    id: `organizations.saml.configurationinstructions.provider.okta`,
    defaultMessage: `For Okta:`,
  },
  providerOneLogin: {
    id: `organizations.saml.configurationinstructions.provider.onelogin`,
    defaultMessage: `For OneLogin:`,
  },
  providerAzureDescription: {
    id: `organizations.saml.configurationinstructions.provider.azure.description`,
    defaultMessage: `See <a href="https://docs.microsoft.com/en-us/azure/active-directory/active-directory-saas-atlassian-cloud-tutorial" target="_blank" rel="noopener noreferrer">
      the Azure help page</a> and the <a href="https://azure.microsoft.com/en-us/marketplace/partners/atlassian/atlassiancloud/" target="_blank" rel="noopener noreferrer">Atlassian Cloud app</a> to set up SAML.
      We don't officially support ADFS, so we recommend using Azure Active Directory instead.`,
  },
  providerBitiumDescription: {
    id: `organizations.saml.configurationinstructions.provider.bitium.description`,
    defaultMessage: `See <a href="https://support.bitium.com/administration/saml-atlassian/" target="_blank" rel="noopener noreferrer">Configuring SAML for Atlassian Cloud</a> to set up SAML.`,
  },
  providerCentrifyDescription: {
    id: `organizations.saml.configurationinstructions.provider.centrify.description`,
    defaultMessage: `See <a href="https://docs.centrify.com/en/centrify/appref/?_ga=2.143391413.1277756220.1508797934-1918052083.1508349314#page/cloudhelp%2Fg-n%2Fsaas_appref_JIRA_Cloud_specifications.html%23ww1221337" target="_blank" rel="noopener noreferrer">
      Centrify's help page</a> to set up SAML.`,
  },
  providerOktaDescription: {
    id: `organizations.saml.configurationinstructions.provider.okta.description`,
    defaultMessage: `See <a href="http://saml-doc.okta.com/SAML_Docs/How-to-Configure-SAML-2.0-for-Atlassian-Cloud.html" target="_blank" rel="noopener noreferrer">
      How to Configure SAML 2.0 for Atlassian Cloud</a> to set up SAML.`,
  },
  providerOneLoginDescription: {
    id: `organizations.saml.configurationinstructions.provider.onelogin.description`,
    defaultMessage: `See <a href="https://support.onelogin.com/hc/en-us/articles/217040946" target="_blank" rel="noopener noreferrer">the OneLogin help page</a>
      and <a href="https://admin.us.onelogin.com/apps/new/80915" target="_blank" rel="noopener noreferrer">the OneLogin app</a> to set up SAML. Make sure you're logged in to see these pages.`,
  },
  providerUnsupported: {
    id: `organizations.saml.configurationinstructions.provider.unsupported`,
    defaultMessage: `If your identity provider isn't supported, you'll need to add the Atlassian application to it manually. When doing so, make sure that your identity provider can send email using the NameId attribute and add the following SAML attribute mappings:`,
  },
  mappingsAttribute: {
    id: `organizations.saml.configurationinstructions.mappings.attribute`,
    defaultMessage: `SAML attribute name`,
  },
  mappingsMapped: {
    id: `organizations.saml.configurationinstructions.mappings.mapped`,
    defaultMessage: `What it should map to in your identity provider`,
  },
  mappingsMappedFirstname: {
    id: `organizations.saml.configurationinstructions.mappings.mapped.firstname`,
    defaultMessage: `User's first name`,
  },
  mappingsMappedLastname: {
    id: `organizations.saml.configurationinstructions.mappings.mapped.lastname`,
    defaultMessage: `User's last name`,
  },
  mappingsOriginalInternalid: {
    id: `organizations.saml.configurationinstructions.mappings.original.internalid`,
    defaultMessage: `{link1} or {link2} or {link3}`,
  },
  mappingsMappedInternalid: {
    id: `organizations.saml.configurationinstructions.mappings.mapped.internalid`,
    defaultMessage: `Internal Id for user that will not change`,
  },
  readMore: {
    id: `organizations.saml.configurationinstructions.read.more`,
    defaultMessage: `Read the Atlassian documentation for <a href="https://confluence.atlassian.com/x/gPYCO" target="_blank" rel="noopener noreferrer">SAML single sign-on</a>.`,
  },
});
