import * as React from 'react';
import { FormattedHTMLMessage, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import AkButton from '@atlaskit/button';
import { akColorN10A, akColorN30, akColorN300, akGridSizeUnitless } from '@atlaskit/util-shared-styles';

import { ModalDialog } from 'common/modal';

import { instructionsMessages } from './saml-messages';

const MappingTable = styled.table`
  font-size: 12px;
  margin-bottom: ${akGridSizeUnitless}px;

  thead,
  tbody {
    border: 0;
  }
  thead {
    background: ${akColorN10A};
  }
  th,
  td {
    border: 1px solid ${akColorN30};
    padding: ${akGridSizeUnitless * 1.5}px;
  }
  th {
    color: ${akColorN300};
    font-weight: 600;
  }
`;
const MappingValue = styled.td`
  white-space: nowrap;
`;
const InternalIdMappingKey = styled.td`
  font-weight: 600;

  p {
    display: inline-block;
    font-weight: normal;
    margin-right: ${akGridSizeUnitless * 1.5}px;
  }
`;

const ProviderList = styled.ul`
  list-style-type: none;
  padding-left: 0;

  li {
    margin-bottom: ${akGridSizeUnitless}px;
  }
`;

interface IdentityProvider {
  title: FormattedMessage.MessageDescriptor;
  description: FormattedMessage.MessageDescriptor;
}

const identityProviders: IdentityProvider[] = [
  {
    title: instructionsMessages.providerAzure,
    description: instructionsMessages.providerAzureDescription,
  },
  {
    title: instructionsMessages.providerOkta,
    description: instructionsMessages.providerOktaDescription,
  },
  {
    title: instructionsMessages.providerOneLogin,
    description: instructionsMessages.providerOneLoginDescription,
  },
  {
    title: instructionsMessages.providerCentrify,
    description: instructionsMessages.providerCentrifyDescription,
  },
  {
    title: instructionsMessages.providerBitium,
    description: instructionsMessages.providerBitiumDescription,
  },
];

interface SamlInstructionsModalProps {
  isOpen: boolean;
  onDismiss(): void;
}

export class SamlInstructionsModalImpl extends React.Component<SamlInstructionsModalProps & InjectedIntlProps, any> {
  public render() {
    const { formatMessage } = this.props.intl;

    return (
      <ModalDialog
        width="large"
        isOpen={this.props.isOpen}
        onClose={this.props.onDismiss}
        header={formatMessage(instructionsMessages.heading)}
        footer={
          <div>
            <AkButton appearance="primary" onClick={this.props.onDismiss}>
              {formatMessage(instructionsMessages.close)}
            </AkButton>
          </div>
        }
      >
        <FormattedMessage
          {...instructionsMessages.description}
          tagName="p"
        />
        <ProviderList>
          {identityProviders.map(idp => (
            <li key={idp.title.id}>
              <b>{formatMessage(idp.title)}</b> <FormattedHTMLMessage {...idp.description} />
            </li>
          ))}
        </ProviderList>
        <FormattedMessage
          {...instructionsMessages.providerUnsupported}
          tagName="p"
        />
        <MappingTable>
          <thead>
            <tr>
              <th>
                {formatMessage(instructionsMessages.mappingsAttribute)}
              </th>
              <th>
                {formatMessage(instructionsMessages.mappingsMapped)}
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                {/* tslint:disable-next-line:jsx-use-translation-function */}
                http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname
              </td>
                <MappingValue>
                  {formatMessage(instructionsMessages.mappingsMappedFirstname)}
                </MappingValue>
              </tr>
              <tr>
                <td>
                  {/* tslint:disable-next-line:jsx-use-translation-function */}
                  http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname
              </td>
              <MappingValue>
                {formatMessage(instructionsMessages.mappingsMappedLastname)}
              </MappingValue>
            </tr>
            <tr>
              <InternalIdMappingKey>
                <FormattedMessage
                  tagName="div"
                  {...instructionsMessages.mappingsOriginalInternalid}
                  values={{
                    // tslint:disable-next-line:jsx-use-translation-function
                    link1: <p>http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name</p>,
                    // tslint:disable-next-line:jsx-use-translation-function
                    link2: <p>http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier</p>,
                    // tslint:disable-next-line:jsx-use-translation-function
                    link3: <p>http://schemas.xmlsoap.org/ws/2005/05/identity/claims/upn</p>,
                  }}
                />
              </InternalIdMappingKey>
              <MappingValue>
                {formatMessage(instructionsMessages.mappingsMappedInternalid)}
              </MappingValue>
            </tr>
          </tbody>
        </MappingTable>
        <FormattedHTMLMessage {...instructionsMessages.readMore} />
      </ModalDialog>
    );
  }
}

export const SamlInstructionsModal = injectIntl(SamlInstructionsModalImpl);
