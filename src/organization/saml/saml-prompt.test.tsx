import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import { sandbox as sinonSandbox } from 'sinon';

import AkButton from '@atlaskit/button';
import AkInlineDialog from '@atlaskit/inline-dialog';

import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import { Content, SamlPromptImpl } from './saml-prompt';

describe('SamlCompletePrompt', () => {
  const body = (<div id="body" />);
  const icon = (<div id="icon" />);
  const sandbox = sinonSandbox.create();
  let onCloseSpy;
  let onOpenSpy;

  beforeEach(() => {
    onCloseSpy = sandbox.spy();
    onOpenSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  function mountSamlCompletePrompt(isOpen: boolean) {
    return mount(
      <SamlPromptImpl
        body={body}
        icon={icon}
        isOpen={isOpen}
        onClose={onCloseSpy}
        onOpen={onOpenSpy}
        intl={createMockIntlProp()}
      />,
      createMockIntlContext(),
    );
  }

  it('contains correct content', () => {
    const wrapper = mountSamlCompletePrompt(true);
    expect(wrapper.find('#body')).to.have.length(1);
    expect(wrapper.find('#icon')).to.have.length(1);
  });

  it('passes isOpen', () => {
    const wrapper = mountSamlCompletePrompt(true);
    expect(wrapper.find(AkInlineDialog).prop('isOpen')).to.be.true();
  });

  it('contains a button that triggers onOpen when clicked', () => {
    const wrapper = mountSamlCompletePrompt(false);
    const button = wrapper.find(AkButton);

    expect(button).to.have.length(1);
    button.simulate('click');
    expect(onOpenSpy.called).to.be.true();
  });

  it('content contains a button that triggers onClose when clicked', () => {
    const wrapper = mountSamlCompletePrompt(true);
    const button = wrapper.find(Content).find(AkButton);

    expect(button).to.have.length(1);
    button.simulate('click');
    expect(onCloseSpy.called).to.be.true();
  });
});
