import { combineReducers } from 'redux';

import { organizationReducer, OrganizationState } from './organizations/organization.reducer';

export interface AppState {
  readonly organization: OrganizationState;
}

export const appsReducer = combineReducers({
  organization: organizationReducer,
});
