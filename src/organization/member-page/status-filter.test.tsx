import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { spy } from 'sinon';

import { DropdownItem as AkDropdownItem } from '@atlaskit/dropdown-menu';

import { FeatureFlagProps } from 'common/feature-flags';

import { MemberStateFilter } from '../../schema/schema-types';

import { StatusFilterImpl, StatusFilterProps } from './status-filter';

describe('StatusFilter', () => {
  const mockTrueFFProps: FeatureFlagProps = {
    isLoading: false,
    value: true,
  };

  const mockFalseFFProps: FeatureFlagProps = {
    isLoading: false,
    value: false,
  };

  class Helper {
    private readonly wrapper: ShallowWrapper<StatusFilterProps>;
    public readonly statusFilterItemActivatedSpy = spy();

    constructor(params: {
      memberState?: MemberStateFilter;
      isMemberStateEnumEnabled?: boolean;
      billableStatusFeatureFlag?: {
        isLoading: boolean,
        value: boolean,
      }
    }) {
      this.wrapper = shallow(
        <StatusFilterImpl
          statusFilterItemActivated={this.statusFilterItemActivatedSpy}
          isMemberStateEnumEnabled={params.isMemberStateEnumEnabled || false}
          billableStatusFeatureFlag={params.billableStatusFeatureFlag || mockFalseFFProps}
          {...params}
        />,
      );
    }

    public simulateAllClick = () => {
      this.allSelector.at(0).simulate('click');
      expect(this.statusFilterItemActivatedSpy.callCount).to.equal(1);
    }

    public simulateActiveClick = () => {
      this.allSelector.at(1).simulate('click');
      expect(this.statusFilterItemActivatedSpy.callCount).to.equal(1);
    }

    public simulateDeactivatedClick = () => {
      this.allSelector.at(2).simulate('click');
      expect(this.statusFilterItemActivatedSpy.callCount).to.equal(1);
    }

    public simulateBillableClick = () => {
      this.allSelector.at(3).simulate('click');
      expect(this.statusFilterItemActivatedSpy.callCount).to.equal(1);
    }

    public simulateNonBillableClick = () => {
      this.allSelector.at(4).simulate('click');
      expect(this.statusFilterItemActivatedSpy.callCount).to.equal(1);
    }

    public verifyItemsLength = (length: number) => {
      expect(this.allSelector.length).to.equal(length);
    }

    private get allSelector() {
      return this.wrapper.dive().find(AkDropdownItem);
    }
  }

  describe('with memberState being enabled or disabled', () => {
    it('should activate statusFilterItemActivated callback on filter selection', () => {
      const helper = new Helper({ memberState: undefined });

      helper.simulateActiveClick();

    });

    it('should pass in the correct filter on active click', () => {
      const helper = new Helper({ memberState: undefined });

      helper.simulateActiveClick();

      expect(helper.statusFilterItemActivatedSpy.args[0][0]).to.deep.equal({ item: { memberState: 'ENABLED' } });
    });

    it('should pass in the correct filter on deactivated click', () => {
      const helper = new Helper({ memberState: undefined });

      helper.simulateDeactivatedClick();

      expect(helper.statusFilterItemActivatedSpy.args[0][0]).to.deep.equal({ item: { memberState: 'DISABLED' } });
    });

    it('should pass in the correct filter on all click', () => {
      const helper = new Helper({ memberState: 'ENABLED' });

      helper.simulateAllClick();

      expect(helper.statusFilterItemActivatedSpy.args[0][0]).to.deep.equal({ item: { memberState: undefined } });
    });

  });
  describe('with feature flag enabled', () => {
    it('should render five filter items', () => {
      const helper = new Helper({ memberState: 'BILLABLE', isMemberStateEnumEnabled: true, billableStatusFeatureFlag: mockTrueFFProps });

      helper.verifyItemsLength(5);
    });

    it('should activate the correct filter on billable click', () => {
      const helper = new Helper({ memberState: 'BILLABLE', isMemberStateEnumEnabled: true, billableStatusFeatureFlag: mockTrueFFProps });

      helper.simulateBillableClick();

      expect(helper.statusFilterItemActivatedSpy.args[0][0]).to.deep.equal({ item: { memberState: 'BILLABLE' } });
    });

    it('should activate the correct filter on billable click', () => {
      const helper = new Helper({ memberState: 'BILLABLE', isMemberStateEnumEnabled: true, billableStatusFeatureFlag: mockTrueFFProps });

      helper.simulateNonBillableClick();

      expect(helper.statusFilterItemActivatedSpy.args[0][0]).to.deep.equal({ item: { memberState: 'NON_BILLABLE' } });
    });
  });

  describe('with feature flag disabled', () => {
    it('should render only three filter items', () => {
      const helper = new Helper({ memberState: 'BILLABLE' , isMemberStateEnumEnabled: false, billableStatusFeatureFlag: mockFalseFFProps });

      helper.verifyItemsLength(3);
    });
  });
});
