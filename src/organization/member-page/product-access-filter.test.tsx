import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { spy } from 'sinon';

import { MultiSelectDropdown, MultiSelectDropdownChangedEvent, MultiSelectDropdownItem, MultiSelectDropdownProps } from 'common/multi-select-dropdown';

import { Product } from '../../schema/schema-types';
import { createMockIntlContext } from '../../utilities/testing';
import { ProductAccessFilter, ProductAccessFilterProps } from './product-access-filter';

describe('ProductAccessFilter', () => {
  class Helper {
    private readonly wrapper: ShallowWrapper<ProductAccessFilterProps>;
    private readonly selectionChangedSpy = spy();

    constructor(props: Pick<ProductAccessFilterProps, 'allProducts' | 'selectedProducts'>) {
      this.wrapper = shallow(
        <ProductAccessFilter {...props} onProductSelectionChanged={this.selectionChangedSpy} />,
        createMockIntlContext(),
      );
    }

    public expectDropdownItemsToBe(items: MultiSelectDropdownItem[]): void {
      expect(this.dropdown.prop('allItems')).to.deep.equal(items);
    }

    public expectSelectionCallbackToHaveBeenInvokedWith(newSelection: Product[]): void {
      expect(this.selectionChangedSpy.callCount).to.equal(1);
      expect(this.selectionChangedSpy.getCall(0).args[0]).to.deep.equal(newSelection);
    }

    public onChange(event: MultiSelectDropdownChangedEvent): void {
      this.dropdown.prop('onChanged')!({} as any, event);
    }

    private get dropdown(): ShallowWrapper<MultiSelectDropdownProps> {
      return this.wrapper.dive().find(MultiSelectDropdown);
    }
  }

  it('should provide correct selection info to MultiSelectDropdown', () => {
    const helper = new Helper({
      allProducts: [
        { key: 'pr-1', name: 'Product 1' },
        { key: 'pr-2', name: 'Product 2' },
        { key: 'pr-3', name: 'Product 3' },
        { key: 'pr-4', name: 'Product 4' },
      ],
      selectedProducts: [
        { key: 'pr-1', name: 'Product 1' },
        { key: 'pr-3', name: 'Product 3' },
      ],
    });

    helper.expectDropdownItemsToBe([
      { id: 'pr-1', label: 'Product 1', isSelected: true },
      { id: 'pr-2', label: 'Product 2', isSelected: false },
      { id: 'pr-3', label: 'Product 3', isSelected: true },
      { id: 'pr-4', label: 'Product 4', isSelected: false },
    ]);
  });

  it('should ignore "selectedproducts" that have not been defined in "allProducts"', () => {
    const helper = new Helper({
      allProducts: [
        { key: 'pr-1', name: 'Product 1' },
        { key: 'pr-2', name: 'Product 2' },
      ],
      selectedProducts: [
        { key: 'pr-unknown', name: 'Product unknown' },
      ],
    });

    helper.expectDropdownItemsToBe([
      { id: 'pr-1', label: 'Product 1', isSelected: false },
      { id: 'pr-2', label: 'Product 2', isSelected: false },
    ]);
  });

  it('should fire "onProductSelectionChanged" with item removed when item is unselected', () => {
    const helper = new Helper({
      allProducts: [
        { key: 'pr-1', name: 'Product 1' },
        { key: 'pr-2', name: 'Product 2' },
      ],
      selectedProducts: [
        { key: 'pr-1', name: 'Product 1' },
        { key: 'pr-2', name: 'Product 2' },
      ],
    });

    helper.onChange({
      id: 'pr-1',
      newSelected: false,
      oldSelected: true,
    });

    helper.expectSelectionCallbackToHaveBeenInvokedWith([
      { key: 'pr-2', name: 'Product 2' },
    ]);
  });

  it('should fire "onProductSelectionChanged" with item added when item is selected', () => {
    const helper = new Helper({
      allProducts: [
        { key: 'pr-1', name: 'Product 1' },
        { key: 'pr-2', name: 'Product 2' },
      ],
      selectedProducts: [
        { key: 'pr-2', name: 'Product 2' },
      ],
    });

    helper.onChange({
      id: 'pr-1',
      newSelected: true,
      oldSelected: false,
    });

    helper.expectSelectionCallbackToHaveBeenInvokedWith([
      { key: 'pr-1', name: 'Product 1' },
      { key: 'pr-2', name: 'Product 2' },
    ]);
  });
});
