import groupBy from 'lodash.groupby';
import * as React from 'react';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import AkAvatar from '@atlaskit/avatar';
import AkBadge from '@atlaskit/badge';
import { DynamicTableStateless as AkDynamicTableStateless, HeaderRow as AkHeaderRow, Row as AkRow } from '@atlaskit/dynamic-table';
import AKWarningIcon from '@atlaskit/icon/glyph/warning';
import AkLozenge from '@atlaskit/lozenge';
import { akColorN20, akColorN30, akColorN300, akColorY300, akGridSizeUnitless } from '@atlaskit/util-shared-styles';

import { getAvatarUrlByUserId } from 'common/avatar';
import { InlineIcon } from 'common/inline-icon';
import { EmptyTableView, getPaginationMessages } from 'common/table';
import { TruncatedField } from 'common/truncated-field';

import { MemberProduct } from '../../schema/schema-types';
import { nonNullOrUndefined } from '../../utilities/arrays';
import { Member, MfaExemptUser } from './member.prop-types';

const messages = defineMessages({
  headingMember: {
    id: 'organizations.members.table.heading.member',
    defaultMessage: 'User',
  },
  headingEmail: {
    id: 'organizations.members.table.heading.email',
    defaultMessage: 'Email address',
  },
  headingProductAccess: {
    id: 'organizations.table.heading.product-access',
    defaultMessage: 'Products',
  },
  headingOptions: {
    id: 'organizations.members.table.heading.options',
    defaultMessage: 'Options',
  },
  editAccount: {
    id: 'organizations.members.table.edit.account',
    defaultMessage: 'Edit account',
  },
  deactivated: {
    id: 'organizations.members.table.deactivated',
    defaultMessage: 'Deactivated',
  },
  resetFilter: {
    id: 'organizations.members.table.reset.filter',
    defaultMessage: 'Reset filter',
  },
  mfaExemptTitle: {
    id: 'organizations.members.table.mfaexempt.title',
    defaultMessage: 'Excluded from two-step verification',
  },
  mfaExemptText: {
    id: 'organizations.members.table.mfaexempt.text',
    defaultMessage: 'This user was manually excluded from two-step verification by an org admin.',
  },
  noProductsText: {
    id: 'organizations.members.table.no.products.text',
    defaultMessage: 'No product access',
  },
  loadingProductsFailed: {
    id: 'organizations.members.table.loading-products-failed',
    defaultMessage: 'Could not load product access',
    description: 'This message is shown when the loading of the list of products that the user has access to failed',
  },
  commaSeparator: {
    id: 'organizations.members.table.product.list.separator',
    defaultMessage: ', ',
  },
  noOrgMembersFound: {
    id: 'organizations.members.table.none',
    defaultMessage: 'No organization members found.',
  },
});

interface LoadingBlockProps {
  borderWidth?: string;
  className?: string;
  height?: string;
  rounded?: boolean;
  width?: string;
}

const LoadingBlockBase: React.StatelessComponent<LoadingBlockProps> = props => (
  <span className={props.className}>
    {props.children}
  </span>
);
export const LoadingBlock = styled(LoadingBlockBase)`
  animation: pulsate 0.8s ease-in-out infinite;
  background: ${akColorN20};
  border: ${props => props.borderWidth || '0'} solid #fff;
  border-radius: ${props => props.rounded ? '100%' : '0'};
  display: inline-block;
  height: ${props => props.height || '20px'};
  vertical-align: middle;
  width: ${props => props.width || '80%'};

  @keyframes pulsate {
    0% { background: ${akColorN20}; }
    50% { background: ${akColorN30}; }
    100% { background: ${akColorN20}; }
  }
`;

const WarningIcon = props => <AKWarningIcon {...props} primaryColor={akColorY300} />;

const WarningTooltip = styled.div`
  max-width: ${akGridSizeUnitless * 40}px;
`;

const WarningIconWrapper = styled.span`
  margin-top: -6px;
`;

const ProductListBadgeWrapper = styled.span`
  padding-left: ${akGridSizeUnitless / 2}px;
`;

interface DisplayNameProps {
  className?: string;
  isDisabled?: boolean;
}
const DisplayNameBase: React.StatelessComponent<DisplayNameProps> = props => (
  <div className={props.className}>
    {props.children}
  </div>
);
const DisplayName = styled(DisplayNameBase)`
  align-items: center;
  color: ${props => props.isDisabled ? akColorN300 : 'inherit'};
  display: flex;

  > span {
    margin-left: ${akGridSizeUnitless}px;
  }
`;

export interface MemberTableProps {
  members: Member[];
  mfaExemptMembers: MfaExemptUser[];
  organizationId: string;
  page: number;
  rowsPerPage: number;
  onSetPage?(page: number): void;
  onMemberDetailsLinkClick?(memberId: string, rowPosition: number): void;
}

interface GroupedProduct {
  productName: string;
  count: number;
}

const groupProducts = (products: MemberProduct[]): GroupedProduct[] => {
  const groupedProducts = groupBy(products, 'productName');

  return Object.entries(groupedProducts).map((entry) => {
    const [productName, productEntries] = entry;

    return {
      productName,
      count: (productEntries as MemberProduct[]).length,
    } as GroupedProduct;
  });
};

export class MemberTableImpl extends React.Component<MemberTableProps & InjectedIntlProps> {
  public render() {
    const { members, intl: { formatMessage } } = this.props;

    const head: AkHeaderRow = {
      cells: [
        {
          key: 'member',
          content: formatMessage(messages.headingMember),
          colSpan: 2,
        },
        {
          key: 'email',
          content: formatMessage(messages.headingEmail),
        },
        {
          key: 'product-access',
          content: formatMessage(messages.headingProductAccess),
        },
        {
          key: 'options',
          content: formatMessage(messages.headingOptions),
        },
      ].filter(nonNullOrUndefined),
    };
    const rows: AkRow[] = members.map((member, index) => ({
      cells: [
        {
          key: 'avatar',
          content: !member.isPlaceholder ? (
            <AkAvatar
              src={getAvatarUrlByUserId(member.id)}
              size="medium"
            />
          ) : (
              <LoadingBlock width="32px" height="32px" rounded={true} borderWidth="2px" />
            ),
          style: {
            fontSize: '0',
            paddingRight: '0',
            width: '1px',
          },
        },
        {
          key: 'display-name',
          content: !member.isPlaceholder ? this.getDisplayNameWithStatus(member) : (
            <LoadingBlock />
          ),
          style: {
            width: '30%',
          },
        },
        {
          key: 'email',
          content: !member.isPlaceholder ? (
            <TruncatedField title={member.emails[0].value} maxWidth={`${akGridSizeUnitless * 30}px`}>{member.emails[0].value}</TruncatedField>
          ) : (
              <LoadingBlock />
            ),
          style: {
            color: akColorN300,
          },
        },
        (!member.isPlaceholder)
          ? {
            key: 'products',
            content: this.renderProductAccessColumn(member),
            style: {
              color: akColorN300,
            },
          }
          : {
            key: 'products',
            content: '',
            style: {
              whiteSpace: 'nowrap',
              width: '1px',
            },
          },
        {
          key: 'actions',
          content: (
            <Link
              to={{ pathname: `./members/${member.id}` }}
              style={{ visibility: member.isPlaceholder ? 'hidden' : 'visible' }}
              onClick={this.onMemberLinkClick}
              data-member-id={member.id}
              data-row-position={(index + 1).toString(10)}
            >
              {formatMessage(messages.editAccount)}
            </Link>
          ),
          style: {
            whiteSpace: 'nowrap',
            width: '1px',
          },
        },
      ].filter(nonNullOrUndefined),
      style: member.isPlaceholder ? {
        background: 'transparent',
      } : {},
    }));

    return (
      <AkDynamicTableStateless
        head={head}
        rows={rows.length ? rows : undefined}
        rowsPerPage={this.props.rowsPerPage}
        onSetPage={this.props.onSetPage}
        page={this.props.page}
        paginationi18n={this.getPaginationMessages()}
        emptyView={
          <EmptyTableView>
            <FormattedMessage {...messages.noOrgMembersFound} />
            {
              // tslint:disable-next-line:jsx-use-translation-function
              ' '
            }
            <Link to="./members">
              {formatMessage(messages.resetFilter)}
            </Link>
          </EmptyTableView>
        }
      />
    );
  }

  private onMemberLinkClick = (e: React.MouseEvent<HTMLElement>): void => {
    const { onMemberDetailsLinkClick } = this.props;

    if (!(onMemberDetailsLinkClick instanceof Function)) {
      return;
    }

    const memberId = e.currentTarget.dataset.memberId || '';
    const rowPosition = parseInt(e.currentTarget.dataset.rowPosition || '', 10);

    onMemberDetailsLinkClick(memberId, rowPosition);
  }

  private getMfaExemptIcon() {
    const { formatMessage } = this.props.intl;
    const warningText = formatMessage(messages.mfaExemptText);
    const warningContent = (
      <WarningTooltip>
        <h5>{formatMessage(messages.mfaExemptTitle)}</h5>
        <p>{warningText}</p>
      </WarningTooltip>
    );

    const icon = (
      <WarningIconWrapper>
        <WarningIcon label="" />
      </WarningIconWrapper>);

    return (
      <InlineIcon
        tooltipContent={warningContent}
        tooltipPosition="right-start"
        icon={icon}
      />
    );
  }

  private getDisplayNameWithStatus(member: Member) {
    const isMfaExempt = !!this.props.mfaExemptMembers.find(u => u.userId === member.id);

    return (
      <DisplayName isDisabled={member.active !== 'ENABLED'}>
        <TruncatedField title={member.displayName} maxWidth={`${akGridSizeUnitless * 30}px`}>{member.displayName}</TruncatedField>
        {member.active !== 'ENABLED' && <AkLozenge>{this.props.intl.formatMessage(messages.deactivated)}</AkLozenge>}
        {isMfaExempt && this.getMfaExemptIcon()}
      </DisplayName>
    );
  }

  private renderProductAccessColumn(member: Member) {
    const { formatMessage } = this.props.intl;

    if (member.memberAccess && member.memberAccess.products && member.memberAccess.products.length > 0) {
      return this.renderProductList(member.memberAccess.products);
    }

    const failedToLoadMemberAccess = (member.memberAccess && member.memberAccess.errors || []).length > 0;

    return (
      <TruncatedField
        title={formatMessage(messages.noProductsText)}
        maxWidth={`${akGridSizeUnitless * 30}px`}
      >
        {
          failedToLoadMemberAccess
            ? <FormattedMessage {...messages.loadingProductsFailed} tagName="em" />
            : <FormattedMessage {...messages.noProductsText} tagName="em" />
        }
      </TruncatedField>
    );

  }

  private renderProductList = (products: MemberProduct[]) => {
    const groupedProducts = groupProducts(products);
    const productList = this.buildProductList(groupedProducts);

    return (
      <TruncatedField maxWidth={`${akGridSizeUnitless * 30}px`}>{productList}</TruncatedField>
    );
  }

  private buildProductList(groupedProducts: GroupedProduct[]) {
    const { formatMessage } = this.props.intl;

    return groupedProducts.map((groupedProduct, i) => {
      let separator = '';
      let badge: React.ReactNode = null;

      if (groupedProduct.count > 1) {
        badge = <ProductListBadgeWrapper><AkBadge value={groupedProduct.count} /></ProductListBadgeWrapper>;
      }

      if (i < groupedProducts.length - 1) {
        separator = formatMessage(messages.commaSeparator);
      }

      return (
        <span key={groupedProduct.productName}>
          {groupedProduct.productName}{badge}{separator}
        </span>
      );
    });
  }

  private getPaginationMessages = () => {
    return getPaginationMessages(this.props.intl.formatMessage.bind(this.props.intl));
  }
}

export const MemberTable = injectIntl(
  MemberTableImpl,
);
