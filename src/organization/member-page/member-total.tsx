import * as React from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';
import styled from 'styled-components';

import { CountIndicator } from 'common/count-indicator';

const Container = styled.div`
  display: flex;
`;

const messages = defineMessages({
  total: {
    id: 'organization.member.total.header.total',
    defaultMessage: 'Total',
  },
  deactivated: {
    id: 'organization.member.total.header.deactivated',
    defaultMessage: 'Deactivated',
  },
});

export interface MemberTotalProps {
  total: number | undefined;
  disabledTotal: number | undefined;
}

export class MemberTotal extends React.Component<MemberTotalProps> {
  public render() {
    const { total, disabledTotal } = this.props;

    return (
      <Container>
        <CountIndicator
          header={<FormattedMessage {...messages.total} />}
          count={total}
        />
        <CountIndicator
          header={<FormattedMessage {...messages.deactivated} />}
          count={disabledTotal}
        />
      </Container>
    );
  }
}
