import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';

import AkButton from '@atlaskit/button';
import AkSpinner from '@atlaskit/spinner';

import { GenericError } from 'common/error';
import { PageLayout } from 'common/page-layout';

import { createMockIntlContext } from '../../utilities/testing';
import { MemberPageImpl, NoMembersPrompt } from './member-page';
import { MemberTableWithFilters } from './member-table-with-filters';
import { Domain } from './member.prop-types';

interface WrapperProps {
  data?: {
    verifiedDomains: Domain[];
    totalMembers: number | undefined;
    totalDisabledMembers: number | undefined;
  };
  loading?: boolean;
  location?: Location;
  error?: boolean;
}

function shallowMemberPageWithData({ data, loading, error }: WrapperProps) {
  return shallow(
    <MemberPageImpl
      error={error as any}
      history={{} as any}
      loading={!!loading}
      location={{} as any}
      match={{ params: { orgId: 'DUMMY_ORG_ID' } } as any}
      data={data}
    />,
    createMockIntlContext(),
  );
}

describe('MemberPage', () => {
  describe('before members have been fetched', () => {
    it('renders with no title', () => {
      const wrapper = shallowMemberPageWithData({ loading: true });
      expect(wrapper.find(PageLayout).props().title).to.be.undefined();
    });

    it('renders a spinner', () => {
      const wrapper = shallowMemberPageWithData({ loading: true });
      expect(wrapper.find(AkSpinner)).to.have.length(1);
    });

    it('renders an error', () => {
      const wrapper = shallowMemberPageWithData({ error: true });
      expect(wrapper.find(GenericError)).to.have.length(1);
    });
  });

  describe('after members have been fetched', () => {
    it('renders with title', () => {
      const wrapper = shallowMemberPageWithData({
        data: {
          totalDisabledMembers: 0,
          totalMembers: 1,
          verifiedDomains: [{ domain: 'test-domain', verified: true }],
        },
      });

      const title = wrapper.find(PageLayout).prop('title') as React.ReactElement<any>;
      expect(title.props.defaultMessage).to.equal('Managed accounts');
    });

    describe('no members prompt', () => {
      it('when no verified domains', () => {
        const wrapper = shallowMemberPageWithData({
          data: {
            totalDisabledMembers: 0,
            totalMembers: 0,
            verifiedDomains: [],
          },
        });

        expect(wrapper.find(NoMembersPrompt)).to.have.lengthOf(1);
        expect(wrapper.find(NoMembersPrompt).prop('hasVerifiedDomains')).to.be.false();
      });

      it('when has verified domains', () => {
        const wrapper = shallowMemberPageWithData({
          data: {
            totalDisabledMembers: 0,
            totalMembers: 0,
            verifiedDomains: [{ domain: 'test-domain', verified: true }],
          },
        });

        expect(wrapper.find(NoMembersPrompt)).to.have.lengthOf(1);
        expect(wrapper.find(NoMembersPrompt).prop('hasVerifiedDomains')).to.be.true();
      });

      it('has a prompt to verify domains when there are no verified domains and no members', () => {
        const wrapper = shallow(<NoMembersPrompt hasVerifiedDomains={false} />);

        const messages = wrapper.find(FormattedMessage);
        expect(messages.findWhere(m => m.prop('id') === 'organizations.members.nomembers.verify.no.domains')).to.have.lengthOf(1);
        expect(wrapper.find(AkButton).props().appearance).to.equal('primary');
      });

      it('has a prompt to verify domains when there are verified domains and no members', () => {
        const wrapper = shallow(<NoMembersPrompt hasVerifiedDomains={true} />);

        const messages = wrapper.find(FormattedMessage);
        expect(messages.findWhere(m => m.prop('id') === 'organizations.members.nomembers.verify.has.domains')).to.have.lengthOf(1);
        expect(wrapper.find(AkButton).props().appearance).to.equal('default');
      });
    });

    it('renders member table', () => {
      const wrapper = shallowMemberPageWithData({
        data: {
          totalDisabledMembers: 0,
          totalMembers: 1,
          verifiedDomains: [{ domain: 'test-domain', verified: true }],
        },
      });
      expect(wrapper.find(MemberTableWithFilters)).to.have.length(1);
    });
  });
});
