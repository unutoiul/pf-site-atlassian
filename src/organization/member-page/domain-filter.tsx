import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import AkInlineDialog from '@atlaskit/inline-dialog';
import AkSelect, { AkSelectOption } from '@atlaskit/select';
import { gridSize as akGridSize } from '@atlaskit/theme';

import { Domain } from './member.prop-types';

const DomainFilterInstructionsWrapper = styled.div`
  max-width: ${akGridSize() * 41}px;
`;

const SelectWrapper = styled.div`
  min-width: ${akGridSize() * 15}px;
`;

export const messages = defineMessages({
  domainFilterAll: {
    id: 'organizations.members.domain.filter.all',
    defaultMessage: 'All domains',
    description: 'A label for an item in domain selector. This item represents all domains. E.g. instead of choosing "domain1", "domain2", etc. a user could just choose "All domains".',
  },
  domainFilterPlaceholder: {
    id: 'organizations.members.domain.filter.placeholder',
    defaultMessage: 'Choose a domain',
    description: 'A label for a selector that is used to choose a domain.',
  },
  domainFilterInstructions: {
    id: 'organizations.members.domain.filter.instructions',
    defaultMessage: 'Start typing to see matching domain names, then choose a domain to see its managed accounts.',
    description: 'This text is shown to the user to prompt them to choose a domain. They can narrow the list down by typing the first few characters. "its" is referring to "domain", as in "to see managed accounts under this domain". ',
  },
  domainNotFound: {
    id: 'organizations.members.domain.filter.not-found',
    defaultMessage: 'Unable to find {searchTerm}',
    description: 'The text that is shown to the user when they search for a domain in the list, and their search term does not yield any results. "searchTerm" is what the user has entered. The space for this string is quite limited, so try to keep it short.',
  },
});

interface Props {
  verifiedDomains: Domain[];
  domainFilterLimit: number;
  value: string | undefined;
  onDomainSelected(domain: string): void;
}

interface State {
  startedTyping: boolean;
}

export class DomainFilterImpl extends React.Component<Props & InjectedIntlProps, State> {
  public state: State = {
    startedTyping: false,
  };

  public render() {
    const {
      domainFilterLimit,
      value,
      verifiedDomains,
      intl: { formatMessage },
    } = this.props;

    const exceedsMaximumDomainLimit = verifiedDomains.length > domainFilterLimit;

    const isDialogOpen = !this.state.startedTyping && !value && exceedsMaximumDomainLimit;

    return (
      <AkInlineDialog
        content={<DomainFilterInstructionsWrapper>{formatMessage(messages.domainFilterInstructions)}</DomainFilterInstructionsWrapper>}
        isOpen={isDialogOpen}
        placement="top"
      >
        <SelectWrapper>
          <AkSelect
            onInputChange={this.startTyping}
            placeholder={formatMessage(messages.domainFilterPlaceholder)}
            noOptionsMessage={this.formatNoOptionsMessage}
            menuIsOpen={(!value && exceedsMaximumDomainLimit) || undefined}
            openMenuOnFocus={true}
            onChange={this.onSelectChange}
            id="domain-filter-selector" // this id is used in a pollinator check. Make sure to update it if you change this
            {...this.getSelectOption(exceedsMaximumDomainLimit)}
          />
        </SelectWrapper>
      </AkInlineDialog >
    );
  }

  private formatNoOptionsMessage = (searchTerm: string) => {
    const {
      intl: { formatMessage },
    } = this.props;

    return formatMessage(messages.domainNotFound, { searchTerm });
  }

  private onSelectChange = ({ value }) => {
    const { onDomainSelected } = this.props;

    onDomainSelected(value);
  }

  private startTyping = (_, { action }): void => {
    if (!this.state.startedTyping && action === 'input-change') {
      this.setState({ startedTyping: true });
    }
  }

  private getSelectOption(exceedsMaximumDomainLimit: boolean): { options: AkSelectOption[], value: AkSelectOption | null } {
    const {
      value,
      verifiedDomains,
      intl: { formatMessage },
    } = this.props;

    const allDomainsItem = { label: formatMessage(messages.domainFilterAll), value: '' };
    const allDomainsSelectItem = exceedsMaximumDomainLimit ? [] : [allDomainsItem];
    const allVerifiedDomainsSelectItem = verifiedDomains.map(d => ({ label: d.domain, value: d.domain }));
    const defaultSelectedItem = allVerifiedDomainsSelectItem.find(d => d.value === value) || (!exceedsMaximumDomainLimit ? allDomainsItem : null);

    return {
      options: [
        ...allDomainsSelectItem,
        ...allVerifiedDomainsSelectItem,
      ],
      value: defaultSelectedItem,
    };
  }
}

export const DomainFilter: React.ComponentClass<Props> =
  injectIntl(
    DomainFilterImpl,
  );
