import { expect } from 'chai';
import { mount, ReactWrapper } from 'enzyme'; // "react-intl" needs to mount in order for it to translate messages correclt
import * as React from 'react';
import { IntlProvider } from 'react-intl';

import { MemberTotal } from './member-total';

import { createMockIntlContext } from '../../utilities/testing';

describe('MemberTotal', () => {
  function mountMemberTotal({ total, disabledTotal }): ReactWrapper {
    return mount(
      <IntlProvider locale="en">
        <MemberTotal
          total={total}
          disabledTotal={disabledTotal}
        />
      </IntlProvider>,
      createMockIntlContext(),
    );
  }

  it('should display headers and numbers', () => {
    const wrapper = mountMemberTotal({ total: 10, disabledTotal: 8 });
    expect(wrapper.length).to.equal(1);

    const html = wrapper.html();
    expect(html.includes('Total')).to.equal(true);
    expect(html.includes('Deactivated')).to.equal(true);
    expect(html.match(/>-</g)).to.equal(null);
    expect(html.includes('10')).to.equal(true);
    expect(html.includes('8')).to.equal(true);
  });
});
