import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import AkInlineDialog from '@atlaskit/inline-dialog';
import AkSelect from '@atlaskit/select';

import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import { DomainFilterImpl, messages } from './domain-filter';

interface Domain {
  domain: string;
  verified: boolean;
}

describe('DomainFilter', () => {
  const domainFilterItemSelected = () => {
    return;
  };

  function domainFilterWithDomains(domains: Domain[], domainFilterLimit = 30) {
    return shallow(
      <DomainFilterImpl
        onDomainSelected={domainFilterItemSelected}
        value=""
        domainFilterLimit={domainFilterLimit}
        verifiedDomains={domains}
        intl={createMockIntlProp()}
      />,
      createMockIntlContext(),
    );
  }

  describe('when there are less verified domains than the limit', () => {
    const wrapper = domainFilterWithDomains([
      { domain: 'foo.com', verified: true },
      { domain: 'foo1.com', verified: true },
      { domain: 'foo2.com', verified: true },
      { domain: 'foo3.com', verified: true },
      { domain: 'foo4.com', verified: true },
      { domain: 'foo5.com', verified: true },
    ], 30);
    const select = wrapper.find(AkSelect);

    it('is not open by default', () => {
      expect(select.prop('menuIsOpen')).to.be.undefined();
    });
    it('has "All domains" item', () => {
      expect(select.prop('options')).to.deep.include({ label: messages.domainFilterAll.defaultMessage, value: '' });
    });
    it('has "All domains" as the default selected item', () => {
      expect(select.prop('value')).to.have.property('label', messages.domainFilterAll.defaultMessage);
    });
  });

  describe('when there are more verified domains than the limit', () => {
    const wrapper = domainFilterWithDomains([
      { domain: 'foo.com', verified: true },
      { domain: 'foo1.com', verified: true },
      { domain: 'foo2.com', verified: true },
      { domain: 'foo3.com', verified: true },
      { domain: 'foo4.com', verified: true },
      { domain: 'foo5.com', verified: true },
    ], 3);
    const select = wrapper.find(AkSelect);

    it('is open by default', () => {
      expect(select.prop('menuIsOpen')).to.be.true();
    });
    it('has no "All domains" item', () => {
      expect(select.prop('options')).to.not.deep.include({ label: messages.domainFilterAll.defaultMessage });
    });
    it('has no default selected item', () => {
      expect(select.prop('value')).to.be.null();
    });

  });

  describe('with feature flag', () => {
    it('should display the dialog with the feature flag disabled', () => {
      const wrapper = domainFilterWithDomains([
        { domain: 'foo.com', verified: true },
        { domain: 'foo1.com', verified: true },
        { domain: 'foo2.com', verified: true },
        { domain: 'foo3.com', verified: true },
        { domain: 'foo4.com', verified: true },
        { domain: 'foo5.com', verified: true },
      ], 3);

      const dialog = wrapper.find(AkInlineDialog) as any;

      expect(dialog.length).to.equal(1);
      expect(dialog.props().isOpen).to.equal(true);
    });

    it('should not display the dialog with the feature flag enabled', () => {
      const wrapper = domainFilterWithDomains([
        { domain: 'foo.com', verified: true },
        { domain: 'foo1.com', verified: true },
        { domain: 'foo2.com', verified: true },
        { domain: 'foo3.com', verified: true },
        { domain: 'foo4.com', verified: true },
        { domain: 'foo5.com', verified: true },
      ], Infinity);

      const dialog = wrapper.find(AkInlineDialog) as any;

      expect(dialog.length).to.equal(1);
      expect(dialog.props().isOpen).to.equal(false);
    });
  });
});
