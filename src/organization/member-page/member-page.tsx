import * as React from 'react';
import { graphql, OptionProps, QueryOpts, QueryResult } from 'react-apollo';
import { defineMessages, FormattedMessage } from 'react-intl';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import AkButton from '@atlaskit/button';
import AkSpinner from '@atlaskit/spinner';
import { akGridSizeUnitless } from '@atlaskit/util-shared-styles';

import { analyticsClient, managedAccountsScreenEvent, ScreenEventSender } from 'common/analytics';
import { GenericError } from 'common/error/generic-error';
import { PageLayout } from 'common/page-layout';

import { makeComponentTimingHoc } from 'common/performance-metrics';

import { OrgBreadcrumbs } from '../breadcrumbs';

import { MemberPageDataQuery, MemberPageDataQueryVariables } from '../../schema/schema-types';
import { ExportMembersSection } from '../members/export-members/export-members-section';
import { createOrgAnalyticsData } from '../organizations/organization-analytics';
import memberPageQuery from './member-page.query.graphql';
import { MemberTableWithFilters } from './member-table-with-filters';
import { MemberTotal } from './member-total';
import { Domain } from './member.prop-types';

const CenteredPrompt = styled.div`
  text-align: center;

  h1 {
    margin-top: ${akGridSizeUnitless * 2}px;
  }
  p {
    margin-bottom: ${akGridSizeUnitless * 2}px;
  }
`;

// tslint:disable-next-line:no-var-requires no-require-imports
const IdBadgeSvg = require('./id-badge.svg');
const IdBadgeImage = styled.div`
  background: url(${IdBadgeSvg}) no-repeat 50%/contain;
  height: 118px;
  margin: 0 auto;
  width: 152px;
`;

export const NoMembersPrompt = ({ hasVerifiedDomains }: { hasVerifiedDomains: boolean }) => (
  <CenteredPrompt>
    <IdBadgeImage />
    <h1><FormattedMessage {...messages.noMembersHeading} /></h1>
    <p>{
      hasVerifiedDomains
        ? <FormattedMessage {...messages.noMembersHasDomains} />
        : <FormattedMessage {...messages.noMembersNoDomains} />
    }
    </p>
    <Link to="./domains" style={{ textDecoration: 'none' }}>
      <AkButton appearance={hasVerifiedDomains ? 'default' : 'primary'}>
        <FormattedMessage {...messages.verifyDomainButton} />
      </AkButton>
    </Link>
  </CenteredPrompt>
);

const messages = defineMessages({
  verifyDomainButton: {
    id: 'organizations.members.nomembers.verify.button',
    defaultMessage: 'Verify domains',
  },
  noMembersHeading: {
    id: 'organizations.members.nomembers.heading',
    defaultMessage: 'You have no managed users',
  },
  noMembersHasDomains: {
    id: 'organizations.members.nomembers.verify.has.domains',
    defaultMessage: `Add users to your domain and we'll populate managed accounts, or verify another domain.`,
  },
  noMembersNoDomains: {
    id: 'organizations.members.nomembers.verify.no.domains',
    defaultMessage: `Your organization does not have any verified domains. Complete the domain verification process to manage accounts in this organization.`,
  },
  membersHeading: {
    id: 'organizations.members.heading',
    defaultMessage: 'Managed accounts',
  },
  membersDescription: {
    id: 'organizations.members.description',
    defaultMessage: 'These are the managed accounts from the verified domains in your organization. {link}.',
  },
  membersDescriptionLink: {
    id: 'organizations.members.description.link',
    defaultMessage: 'Learn more about managed accounts',
  },
});

interface DerivedProps {
  loading: boolean;
  error?: QueryResult['error'];
  data?: {
    verifiedDomains: Domain[];
    totalMembers: number | undefined;
    totalDisabledMembers: number | undefined;
  };
}

type AllProps = DerivedProps & RouteComponentProps<any>;

export class MemberPageImpl extends React.Component<AllProps> {
  public componentWillReceiveProps(nextProps: AllProps) {
    const hasJustFinishedLoading = this.props.loading === true && nextProps.loading === false;
    const hasError = !!this.props.error;

    // tslint:disable-next-line:early-exit
    if (hasJustFinishedLoading && !hasError) {
      analyticsClient.eventHandler(createOrgAnalyticsData({
        orgId: this.props.match.params.orgId,
        action: 'pageLoad',
        actionSubject: 'organizationPageLoad',
        actionSubjectId: 'membersPage',
      }));
    }
  }

  public render() {
    const {
      match: { params: { orgId } },
      data,
    } = this.props;

    const checkData = data && data.totalMembers !== 0;

    return (
      <ScreenEventSender event={managedAccountsScreenEvent()}>
        <PageLayout
          title={checkData ? <FormattedMessage {...messages.membersHeading} /> : undefined}
          description={checkData ?
            <FormattedMessage
              {...messages.membersDescription}
              values={{
                link: (
                  <a href="https://confluence.atlassian.com/x/YzcWN" target="_blank" rel="noopener noreferrer">
                    <FormattedMessage {...messages.membersDescriptionLink} />
                  </a>
                ),
              }}
            /> : undefined
          }
          isFullWidth={true}
          breadcrumbs={<OrgBreadcrumbs />}
          action={checkData ? <ExportMembersSection orgId={orgId} /> : undefined}
        >
          {this.getPageContent()}
          <MemberTableWithFilters totalMembers={data ? data.totalMembers : undefined} />
        </PageLayout>
      </ScreenEventSender>
    );
  }

  private getPageContent = () => {
    const { loading, error } = this.props;

    if (loading) {
      return (<AkSpinner />);
    }

    if (error || !this.props.data) {
      return (<GenericError />);
    }

    const {
      data: {
        totalMembers,
        verifiedDomains,
        totalDisabledMembers,
      },
    } = this.props;

    if (totalMembers === 0) {
      return (
        <NoMembersPrompt hasVerifiedDomains={verifiedDomains.length > 0} />
      );
    } else {
      return (
        <MemberTotal total={totalMembers} disabledTotal={totalDisabledMembers} />
      );
    }
  };
}

function getMemberPageDataProps(componentProps: OptionProps<{}, MemberPageDataQuery>): DerivedProps {
  if (!componentProps || !componentProps.data || componentProps.data.loading || !componentProps.data.organization) {
    return { loading: true };
  }

  if (componentProps.data.error) {
    return { loading: false, error: componentProps.data.error };
  }

  const {
    domainClaim: {
      domains,
    },
    users: {
      memberTotal,
    },
  } = componentProps.data.organization;

  return {
    loading: false,
    data: {
      verifiedDomains: (domains || []).filter(d => d.verified),
      totalMembers: !memberTotal || memberTotal.total < 0 ? undefined : memberTotal.total,
      totalDisabledMembers: !memberTotal || memberTotal.disabledTotal < 0 ? undefined : memberTotal.disabledTotal,
    },
  };
}

const withMemberPageData = graphql<RouteComponentProps<any>, MemberPageDataQuery, MemberPageDataQueryVariables, DerivedProps>(memberPageQuery, {
  options: ({ match: { params: { orgId } } }): QueryOpts<MemberPageDataQueryVariables> => ({
    variables: { id: orgId },
  }),
  props: getMemberPageDataProps,
  skip: ({ match: { params: { orgId } } }) => !orgId,
});

const withComponentTiming = makeComponentTimingHoc<AllProps>({
  id: 'members-page',
  category: 'page',
  isMeaningful: true,
  isLoaded: (props => !props.loading),
});

export const MemberPage = withMemberPageData(withComponentTiming(MemberPageImpl));
