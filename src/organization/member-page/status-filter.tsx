import * as React from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';

import { DropdownItem as AkDropdownItem, DropdownItemGroup as AkDropdownItemGroup } from '@atlaskit/dropdown-menu';

import { SingleSelectDropdownMenu } from 'common/dropdown';

import { MemberStateFilter } from '../../schema/schema-types';
import { BillableStatusFeatureFlag, withBillableStatusFeatureFlag } from '../feature-flags/with-billable-status-filter-feature-flag';

export const messages = defineMessages({
  statusAll: {
    id: 'organizations.members.status.filter.all',
    defaultMessage: 'All accounts',
  },
  statusActive: {
    id: 'organizations.members.status.filter.active',
    defaultMessage: 'Active accounts',
  },
  statusInactive: {
    id: 'organizations.members.status.filter.inactive',
    defaultMessage: 'Deactivated accounts',
  },
  statusBillable: {
    id: 'organization.members.status.filter.billable',
    defaultMessage: 'Billable',
    description: 'When an account is deemed billable, this filter will show them in a member table',
  },
  statusNonBillable: {
    id: 'organization.members.status.filter.nonbillable',
    defaultMessage: 'Non-billable',
    description: 'When an account is deemed nonbillable, this filter will show them in a member table',
  },
});

export interface StatusFilterProps {
  memberState?: MemberStateFilter;
  isMemberStateEnumEnabled?: boolean;
  statusFilterItemActivated({ }: { item: { memberState?: MemberStateFilter } }): void;
}

export class StatusFilterImpl extends React.Component<StatusFilterProps & BillableStatusFeatureFlag> {
  public render() {
    const { isMemberStateEnumEnabled, billableStatusFeatureFlag } = this.props;

    return (
      <SingleSelectDropdownMenu
        triggerType="button"
        trigger={this.getTitle()}
        withDivider={true}
      >
          <AkDropdownItemGroup id="all-group">
            <AkDropdownItem onClick={this.onAllClick}>
              <FormattedMessage {...messages.statusAll} />
            </AkDropdownItem>
          </AkDropdownItemGroup>
          <AkDropdownItemGroup id="active-group">
            <AkDropdownItem onClick={this.onActiveClick}>
              <FormattedMessage {...messages.statusActive} />
            </AkDropdownItem>
            <AkDropdownItem onClick={this.onInactiveClick}>
              <FormattedMessage {...messages.statusInactive} />
            </AkDropdownItem>
          </AkDropdownItemGroup>
          {isMemberStateEnumEnabled && billableStatusFeatureFlag.value && (
            <AkDropdownItemGroup id="billable-group">
              <AkDropdownItem onClick={this.onBillableClick}>
                <FormattedMessage {...messages.statusBillable} />
              </AkDropdownItem>
              <AkDropdownItem onClick={this.onNonBillableClick}>
                <FormattedMessage {...messages.statusNonBillable} />
              </AkDropdownItem>
            </AkDropdownItemGroup>
          )}
      </SingleSelectDropdownMenu>
    );
  }

  private getTitle = (): React.ReactNode => {
    const { memberState, isMemberStateEnumEnabled, billableStatusFeatureFlag } = this.props;

    const mapMemberStateToReactNode: {
      [key in MemberStateFilter]: React.ReactNode
    } = {
      ALL: <FormattedMessage {...messages.statusAll} />,
      ENABLED: <FormattedMessage {...messages.statusActive} />,
      DISABLED: <FormattedMessage {...messages.statusInactive} />,
      BILLABLE: <FormattedMessage {...messages.statusBillable} />,
      NON_BILLABLE: <FormattedMessage {...messages.statusNonBillable} />,
    };

    const memberStateLookUpValue: MemberStateFilter | undefined = !isMemberStateEnumEnabled || !billableStatusFeatureFlag.value ?
      (memberState === 'BILLABLE' || memberState === 'NON_BILLABLE' ? 'ALL' : memberState) :
      memberState;

    return (
      <React.Fragment>
        {memberStateLookUpValue !== undefined
          ? mapMemberStateToReactNode[memberStateLookUpValue]
          : <FormattedMessage {...messages.statusAll} />}
      </React.Fragment>
    );
  }

  private onAllClick = () => {
    this.props.statusFilterItemActivated({ item: { memberState: undefined } });
  }

  private onActiveClick = () => {
    this.props.statusFilterItemActivated({ item: { memberState: 'ENABLED' } });
  }

  private onInactiveClick = () => {
    this.props.statusFilterItemActivated({ item: { memberState: 'DISABLED' } });
  }

  private onBillableClick = () => {
    if (!this.props.isMemberStateEnumEnabled || !this.props.billableStatusFeatureFlag.value) {
      return;
    }
    this.props.statusFilterItemActivated({ item: { memberState: 'BILLABLE' } });
  }

  private onNonBillableClick = () => {
    if (!this.props.isMemberStateEnumEnabled || !this.props.billableStatusFeatureFlag.value) {
      return;
    }
    this.props.statusFilterItemActivated({ item: { memberState: 'NON_BILLABLE' } });
  }
}

export const StatusFilter = withBillableStatusFeatureFlag<StatusFilterProps>((componentProps) => {
  if (componentProps.billableStatusFeatureFlag.isLoading) {
    return null;
  }

  return (
    <StatusFilterImpl {...componentProps} />
  );
});
