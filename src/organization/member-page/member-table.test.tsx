// tslint:disable jsx-use-translation-function
// import AkNavigation from '@atlaskit/navigation';
import { expect } from 'chai';
import { mount, ReactWrapper, shallow } from 'enzyme';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { MemoryRouter } from 'react-router-dom';

import AkAvatar from '@atlaskit/avatar';
import AkBadge from '@atlaskit/badge';
import { DynamicTableStateless as AkDynamicTableStateless } from '@atlaskit/dynamic-table';

import { InlineIcon } from 'common/inline-icon';

import { getAvatarUrlByUserId } from 'common/avatar';

import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import { LoadingBlock, MemberTableImpl } from './member-table';
import { Member } from './member.prop-types';

function mountWithRoutes(jsx: JSX.Element, initialEntries = [{ pathname: '/' }]): ReactWrapper<any, any> {
  return mount(
    <MemoryRouter initialEntries={initialEntries}>
      {jsx}
    </MemoryRouter>, createMockIntlContext(),
  );
}

describe('MemberTable', () => {
  it('displays headings', () => {
    const wrapper = shallow(
      <MemberTableImpl
        members={[]}
        organizationId="1"
        mfaExemptMembers={[]}
        page={1}
        rowsPerPage={10}
        intl={createMockIntlProp()}
      />,
      createMockIntlContext(),
    );
    const headings = [
      {
        key: 'member',
        content: 'User',
        colSpan: 2,
      },
      {
        key: 'email',
        content: 'Email address',
      },
      {
        key: 'product-access',
        content: 'Products',
      },
      {
        key: 'options',
        content: 'Options',
      },
    ];
    const table = wrapper.find(AkDynamicTableStateless);
    expect(table).to.have.length(1);
    expect(table.props().head!.cells).to.deep.equal(headings);
  });

  describe('with no members', () => {
    const wrapper = shallow(
      <MemberTableImpl
        members={[]}
        organizationId="1"
        mfaExemptMembers={[]}
        page={1}
        rowsPerPage={10}
        intl={createMockIntlProp()}
      />,
      createMockIntlContext(),
    );
    const table = wrapper.find(AkDynamicTableStateless);

    it('contains no rows', () => {
      expect(table).to.have.length(1);
      expect(table.props().rows).to.equal(undefined);
    });

    it('should pass correct message for empty view', () => {
      expect(table).to.have.length(1);

      const emptyViewWrapper = shallow(table.props().emptyView as any);
      expect(emptyViewWrapper.find(FormattedMessage)).to.have.length(1);
      expect(emptyViewWrapper.find(FormattedMessage).props().defaultMessage).to.contain('No organization members found');
    });
  });

  describe('with members', () => {
    const member1Email = 'foo@bar.com';
    const members: Member[] = [
      {
        id: '1',
        active: 'ENABLED',
        displayName: 'Foo Bar',
        emails: [{
          primary: true,
          value: member1Email,
        }],
        memberAccess: {
          errors: [],
          products: [
            {
              productName: 'Jira',
              siteUrl: 'some-site',
            },
            {
              productName: 'Jira',
              siteUrl: 'some-other-site',
            },
            {
              productName: 'Confluence',
              siteUrl: 'some-site',
            },
          ],
        },
      },
      {
        id: '2',
        active: 'ENABLED',
        displayName: '',
        emails: [{
          primary: true,
          value: '',
        }],
        isPlaceholder: true,
        memberAccess: {
          errors: [],
          products: [],
        },
      },
      {
        id: '3',
        active: 'ENABLED',
        displayName: '',
        emails: [{
          primary: true,
          value: '',
        }],
        memberAccess: null,
      },
      {
        id: '4',
        active: 'ENABLED',
        displayName: '',
        emails: [{
          primary: true,
          value: '',
        }],
        memberAccess: {
          errors: [],
          products: [],
        },
      },
      {
        id: '5',
        active: 'ENABLED',
        displayName: '',
        emails: [{
          primary: true,
          value: '',
        }],
        memberAccess: {
          errors: ['some-error'],
          products: [],
        },
      },
    ];
    const wrapper = mountWithRoutes(
      <MemberTableImpl
        members={members}
        mfaExemptMembers={[]}
        organizationId="1"
        page={1}
        rowsPerPage={10}
        intl={createMockIntlProp()}
      />,
    );
    const table = wrapper.find(AkDynamicTableStateless);

    describe('member information', () => {
      it('shows rows with member information', () => {
        expect(table).to.have.length(1);

        const rows = table.find('tbody').find('tr');
        expect(rows).to.have.length(5);

        const member1Cells = rows.first().find('td');
        expect(member1Cells.at(0).find(AkAvatar)).to.have.length(1);
        expect(member1Cells.at(0).find<any>(AkAvatar).props().src).to.equal(getAvatarUrlByUserId('1'));
        expect(member1Cells.at(1).text()).to.equal(members[0].displayName);
        expect(member1Cells.at(2).text()).to.equal(member1Email);
        expect(member1Cells.at(4).find('a')).to.have.length(1);
        expect(member1Cells.at(4).find('a').props().style!.visibility).to.equal('visible');
        expect(member1Cells.at(4).text()).to.equal('Edit account');

        const member2Cells = rows.at(1).find('td');
        expect(member2Cells.at(0).find(LoadingBlock)).to.have.length(1);
        expect(member2Cells.at(1).find(LoadingBlock)).to.have.length(1);
        expect(member2Cells.at(2).find(LoadingBlock)).to.have.length(1);
        expect(member2Cells.at(4).find('a').props().style!.visibility).to.equal('hidden');
      });
    });

    describe('product access information', () => {
      it('should show product list' , () => {
        const rows = table.find('tbody').find('tr');
        const member1Cells = rows.first().find('td');
        expect(member1Cells.at(3).text()).to.equal('Jira2, Confluence');
      });

      it('should show badges for duplicate products across sites', () => {
        const rows = table.find('tbody').find('tr');
        const member1Cells = rows.first().find('td');
        expect(member1Cells.at(3).find(AkBadge)).to.have.length(1);
      });

      it('should display no product access when product list is empty', () => {
        const rows = table.find('tbody').find('tr');
        const member2Cells = rows.at(3).find('td');
        expect(member2Cells.at(3).text()).to.equal('No product access');
      });

      it('should display no product access when memberAccess is null', () => {
        const rows = table.find('tbody').find('tr');
        const member3Cells = rows.at(2).find('td');
        expect(member3Cells.at(3).text()).to.equal('No product access');
      });

      it('should display error message when memberAccess has loaded with errors', () => {
        const rows = table.find('tbody').find('tr');
        const memberCells = rows.at(4).find('td');
        expect(memberCells.at(3).text()).to.equal('Could not load product access');
      });

      it('should render an empty string if the member is a placeholder', () => {
        const rows = table.find('tbody').find('tr');
        const member3Cells = rows.at(1).find('td');
        expect(member3Cells.at(3).text()).to.equal('');
      });
    });
  });

  describe('with mfa exempt members', () => {
    const member1Email = 'foo@bar.com';
    const members: Member[] = [
      {
        id: '1',
        active: 'ENABLED',
        displayName: 'Foo Bar',
        emails: [{
          primary: true,
          value: member1Email,
        }],
        memberAccess: {
          errors: [],
          products: [],
        },
      },
    ];
    const wrapper = mountWithRoutes(
      <MemberTableImpl
        members={members}
        mfaExemptMembers={[{ userId: '1' }]}
        organizationId="1"
        page={1}
        rowsPerPage={10}
        intl={createMockIntlProp()}
      />,
    );
    const table = wrapper.find(AkDynamicTableStateless);

    it('shows rows with member information and mfa exempt information', () => {
      const rows = table.find('tbody').find('tr');
      const member1Cells = rows.first().find('td');
      expect(member1Cells.at(1).find(InlineIcon)).to.have.length(1);
    });
  });
});
