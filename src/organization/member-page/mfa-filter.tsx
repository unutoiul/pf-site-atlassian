import * as React from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';

import { DropdownItem as AkDropdownItem } from '@atlaskit/dropdown-menu';

import { SingleSelectDropdownMenu } from 'common/dropdown';

export const messages = defineMessages({
  mfaFilterHeading: {
    id: 'organizations.members.mfa.filter.heading',
    defaultMessage: 'Two-step verification',
  },
  mfaFilterAll: {
    id: 'organizations.members.mfa.filter.all',
    defaultMessage: 'All accounts',
  },
  mfaFilterNoMfa: {
    id: 'organizations.members.mfa.filter.no.mfa',
    defaultMessage: 'Accounts without two-step verification',
  },
});

interface Props {
  mfa?: boolean;
  mfaFilterItemActivated(e: { item: { mfa?: boolean } }): void;
}

export class MfaFilter extends React.Component<Props> {
  public render() {
    const { mfa } = this.props;

    return (
      <SingleSelectDropdownMenu
        triggerType="button"
        trigger={mfa === false
          ? <FormattedMessage {...messages.mfaFilterNoMfa} />
          : <FormattedMessage {...messages.mfaFilterHeading} />
        }
      >
        <AkDropdownItem onClick={this.onAllAccountsClick}>
          <FormattedMessage {...messages.mfaFilterAll} />
        </AkDropdownItem>
        <AkDropdownItem onClick={this.onNo2SVAccountsClick}>
          <FormattedMessage {...messages.mfaFilterNoMfa} />
        </AkDropdownItem>
      </SingleSelectDropdownMenu>
    );
  }

  private onAllAccountsClick = () => {
    this.props.mfaFilterItemActivated({ item: { mfa: undefined } });
  }

  private onNo2SVAccountsClick = () => {
    this.props.mfaFilterItemActivated({ item: { mfa: false } });
  }
}
