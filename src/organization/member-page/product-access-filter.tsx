import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import { MultiSelectDropdown, MultiSelectDropdownChangedEvent, MultiSelectDropdownItem } from 'common/multi-select-dropdown';

import { Product } from '../../schema/schema-types';

export interface ProductAccessFilterProps {
  allProducts: Product[];
  selectedProducts: Product[];
  onProductSelectionChanged(products: Product[]): void;
}

const messages = defineMessages({
  placeholder: {
    id: 'organizations.members.product-access-filter.placeholder',
    defaultMessage: 'Product access',
    description: 'A placeholder for the filter. This filter allows admins to choose products (e.g. "Jira Software", "Confluence", etc.). The user list will then be filtered down to only show users who have access to this product type.',
  },
});

class ProductAccessFilterImpl extends React.Component<ProductAccessFilterProps & InjectedIntlProps> {
  public render() {
    const {
      allProducts,
      selectedProducts,
      intl: { formatMessage },
    } = this.props;

    const items = allProducts.map<MultiSelectDropdownItem>(({ key, name }) => ({
      label: name,
      id: key,
      isSelected: !!selectedProducts.find(p => p.key === key),
    }));

    return (
      <MultiSelectDropdown
        placeholder={formatMessage(messages.placeholder)}
        allItems={items}
        onChanged={this.onChanged}
      />
    );
  }

  private onChanged = (_, e: MultiSelectDropdownChangedEvent): void => {
    const {
      allProducts,
      selectedProducts,
      onProductSelectionChanged,
    } = this.props;

    const currentSelection = allProducts.filter(({ key }) => e.id === key
      ? e.newSelected
      : !!selectedProducts.find(p => p.key === key));

    onProductSelectionChanged(currentSelection);
  }
}

export const ProductAccessFilter = injectIntl(ProductAccessFilterImpl);
