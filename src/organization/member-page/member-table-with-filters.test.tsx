// tslint:disable prefer-function-over-method early-exit
import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import { stringify } from 'query-string';
import * as React from 'react';

import { UIEvent } from 'common/analytics';
import { FieldTextFixed } from 'common/field-text';

import { Product } from '../../schema/schema-types';
import { createMockAnalyticsClient, createMockContext, createMockIntlProp, MockAnalyticsClient, wait } from '../../utilities/testing';
import { DomainFilter } from './domain-filter';
import { MemberTable } from './member-table';
import { MemberTableWithFiltersImpl, Props, ROWS_PER_PAGE } from './member-table-with-filters';
import { Domain, Member } from './member.prop-types';
import { MfaFilter } from './mfa-filter';
import { ProductAccessFilter } from './product-access-filter';
import { StatusFilter } from './status-filter';

describe('MemberTableWithFilters', () => {
  class Helper {
    private wrapper: ShallowWrapper<Props, {}>;
    private currentSearchQuery = '';
    private total: number | undefined;
    private analyticsClientMock: MockAnalyticsClient;

    constructor(params: {
      data: { loading: true } | { error: true } | {
        /**
         * Represents the total amount of members that can be retrieved using current filters
         */
        total: number,
        /**
         * Represents the total amount of members in an org (i.e. without any filters applied)
         */
        globalTotal?: number,
      },
      page?: number,
      displayName?: string,
      filter?: string,
      productAccessQuery?: string[],
      isMfaEnforced?: boolean,
      mfaStatusFilter?: 'enabled' | 'disabled',
      verifiedDomains?: string[],
      stringFilterEnabled?: boolean,
      products?: Product[]
      productAccessFilterEnabled?: boolean,
      memberStateEnumEnabled?: boolean,
      isDataLoading?: boolean;
      isAllDomainMembersEnabled?: boolean;
      active?: 'true' | 'false',
    }) {
      const searchParams = {
        ...(params.page ? { page: params.page } : {}),
        ...(params.displayName ? { displayName: params.displayName } : {}),
        ...(params.filter ? { filter: params.filter } : {}),
        ...(params.mfaStatusFilter === 'disabled' ? { mfa: false } : {}),
        ...(params.productAccessQuery ? { products: params.productAccessQuery } : {}),
        ...(params.active ? { active: params.active } : {}),
      };

      this.total = (params.data as any).total;
      this.analyticsClientMock = createMockAnalyticsClient();

      const totalMembers = typeof (params.data as any).globalTotal === 'number'
        ? (params.data as any).globalTotal
        : this.total || 1234;

      this.wrapper = shallow(
        <MemberTableWithFiltersImpl
          isDataLoading={params.isDataLoading || false}
          totalMembers={totalMembers}
          products={params.products || []}
          mfaExemptMembers={[]}
          error={(params.data as any).error}
          loading={!!(params.data as any).loading}
          isMfaEnforced={!!params.isMfaEnforced}
          history={{ replace: this.historyReplace.bind(this) } as any}
          location={{ search: stringify(searchParams), pathname: '/base/path' } as any}
          intl={createMockIntlProp()}
          match={{ params: { orgId: this.testOrgId } } as any}
          verifiedDomains={(params.verifiedDomains || ['test-domain.com']).map<Domain>(domain => ({ verified: true, domain }))}
          data={this.generateData(this.total, params.page || 1)}
          isFilteringByProductAccessEnabled={!!params.productAccessFilterEnabled}
          stringFilterEnabled={!!params.stringFilterEnabled}
          analyticsClient={this.analyticsClientMock}
          isMemberStateEnumEnabled={!!params.memberStateEnumEnabled}
          isAllDomainMembersEnabled={params.isAllDomainMembersEnabled || false}
        />,
        createMockContext({ apollo: true, intl: true }),
      );
    }

    public get testOrgId(): string {
      return 'test-org';
    }

    public selectPage(page: number, loadImmediately: boolean): void {
      const changeCallback = this.memberTable.prop('onSetPage');
      changeCallback!(page);

      if (loadImmediately) {
        this.wrapper.setProps({
          data: this.generateData(this.total, page),
        });
      }
    }

    public clickEditAccount(memberId: string, rowIndex: number): void {
      const callback = this.memberTable.prop('onMemberDetailsLinkClick');

      callback!(memberId, rowIndex);
    }

    public focusStringFilter(): void {
      const callback = this.stringFilter.prop('onFocus');
      callback!({} as any);
    }

    public setStringFilter(filterValue: string): void {
      this.focusStringFilter();
      const changeCallback = this.stringFilter.prop('onChange');
      changeCallback!({ currentTarget: { value: filterValue } } as any);
    }

    public searchForMembersWithMfaStatus(mfaStatus: 'enabled' | 'disabled'): void {
      const changeCallback = this.mfaFilter.prop('mfaFilterItemActivated');
      changeCallback({ item: { mfa: mfaStatus === 'disabled' ? false : undefined } });
    }

    public searchForMembersWithDomain(domain: string): void {
      const changeCallback = this.domainFilter.prop('onDomainSelected');
      changeCallback(domain);
    }

    public expectDomainFilterToBePresent(): void {
      expect(this.domainFilter).to.have.lengthOf(1, 'Expected domain filter to be present, but it was not');
    }

    public expectDomainFilterToBeAbsent(): void {
      expect(this.domainFilter).to.have.lengthOf(0, 'Expected domain filter to be absent, but it was present');
    }

    public expectProductAccessFilterToBeAbsent(): void {
      expect(this.productAccessFilter).to.have.lengthOf(0, 'Expected product access filter to be absent, but it was present');
    }

    public expectProductAccessFilterToBePresent(): void {
      expect(this.productAccessFilter).to.have.lengthOf(1, 'Expected product access filter to be present, but it was not');
    }

    public expectProductAccessSelectionToBe(products: Product[]): void {
      this.expectProductAccessFilterToBePresent();
      const selectedProducts = this.productAccessFilter.prop('selectedProducts');
      expect(selectedProducts).to.deep.equal(products);
    }

    public searchForMembersWithProductAccess(products: Product[]): void {
      const changeCallback = this.productAccessFilter.prop('onProductSelectionChanged');
      changeCallback(products);
    }

    public expectMfaStatusFilterToBeAbsent(): void {
      expect(this.mfaFilter).to.have.lengthOf(0, 'Expected mfa filter to be absent, but it was present');
    }

    public expectMfaStatusFilterToBePresent(): void {
      expect(this.mfaFilter).to.have.lengthOf(1, 'Expected mfa filter to be present, but it was not');
    }

    public expectSearchQueryToBe(searchQuery: string): void {
      expect(this.currentSearchQuery).to.equal(searchQuery);
    }

    public expectMembersTableRowCountToBe(rowCount: number): void {
      const members = this.memberTable.prop('members');

      expect(members.length).to.equal(rowCount);
    }

    public expectPlaceholders({ from, to }: { from: number, to: number }): void {
      const members = this.memberTable.prop('members');

      const count = to - from + 1;
      const actual = members.slice(from - 1, to).map(({ isPlaceholder }) => ({ isPlaceholder }));

      expect(actual).to.deep.equal(Array(count).fill({ isPlaceholder: true }));
    }

    public expectAnalyticsUIEventToBeSubmitted(withArgs: UIEvent): void {
      const spy = this.analyticsClientMock.sendUIEvent;
      expect(spy.callCount).to.equal(1);
      expect(spy.getCall(0).args[0]).to.deep.equal(withArgs);
    }

    public expectNullToBeRendered(): void {
      expect(this.wrapper.children().length).to.equal(0);
    }

    public expectNullToNotBeRendered(): void {
      expect(this.wrapper.children().length).to.not.equal(0);
    }

    public expectAllStatusFilter(): void {
      const statusFilter = this.wrapper.find(StatusFilter);

      expect(statusFilter.length).to.equal(1);
      expect(statusFilter.props().memberState).to.equal(undefined);
    }

    public expectActiveStatusFilter(): void {
      const statusFilter = this.wrapper.find(StatusFilter);

      expect(statusFilter.length).to.equal(1);
      expect(statusFilter.props().memberState).to.equal('ENABLED');
    }

    public expectDeactivatedStatusFilter(): void {
      const statusFilter = this.wrapper.find(StatusFilter);

      expect(statusFilter.length).to.equal(1);
      expect(statusFilter.props().memberState).to.equal('DISABLED');
    }

    private get memberTable() {
      return this.wrapper.find(MemberTable);
    }

    private get stringFilter() {
      return this.wrapper.find(FieldTextFixed);
    }

    private get mfaFilter() {
      return this.wrapper.find(MfaFilter);
    }

    private get domainFilter() {
      return this.wrapper.find(DomainFilter);
    }

    private get productAccessFilter() {
      return this.wrapper.find(ProductAccessFilter);
    }

    private historyReplace = (path: string): void => {
      this.currentSearchQuery = path.includes('?') ? path.substr(path.indexOf('?')) : '';
    }

    private generateData(total: number | undefined, page: number): Props['data'] {
      if (typeof total === 'undefined') {
        return undefined;
      }
      if (total === 0) {
        return { members: [], total: 0 };
      }

      const count = page * ROWS_PER_PAGE < total
        ? ROWS_PER_PAGE
        : (page * ROWS_PER_PAGE - total);
      const startIndex = (page - 1) * ROWS_PER_PAGE + 1;

      return {
        members: Array(count).fill(0).map((_, i) => i + startIndex).map<Member>((num) => ({
          id: `test-user-${num}`,
          active: 'DISABLED',
          displayName: `Test User ${num}`,
          emails: [{
            primary: false,
            value: `test_user_${num}@example.com`,
          }],
          memberAccess: {
            errors: [],
            products: [],
          },
        })),
        total,
      };
    }
  }

  describe('paging', () => {
    it('should update the query string when changing page', () => {
      const helper = new Helper({
        data: { total: 42 },
      });

      helper.selectPage(3, true);
      helper.expectSearchQueryToBe(`?page=3`);
    });

    it('should preserve the number of items in table when changing pages', () => {
      const helper = new Helper({
        data: { total: 42 },
      });

      helper.selectPage(2, false);
      helper.expectMembersTableRowCountToBe(42);
      helper.expectPlaceholders({ from: 21, to: 40 });

      helper.selectPage(3, false);
      helper.expectMembersTableRowCountToBe(42);
      helper.expectPlaceholders({ from: 41, to: 42 });
    });
  });

  describe('filtering', () => {
    describe('by display name', () => {
      it('should update the query string when filter changes', async () => {
        const helper = new Helper({
          data: { total: 42 },
        });

        helper.setStringFilter('test name');

        await wait(550); // accounting for "debounce"

        helper.expectSearchQueryToBe('?displayName=test%20name');
      });
      it('should clear the query string parameter when input becomes empty', () => {
        const helper = new Helper({
          data: { total: 42 },
          displayName: 'test name',
        });

        helper.setStringFilter('');
        helper.expectSearchQueryToBe('');
      });

      it('should submit an analytics event when string filter is focused', () => {
        const helper = new Helper({
          data: { total: 42 },
        });

        helper.focusStringFilter();
        helper.expectAnalyticsUIEventToBeSubmitted({
          orgId: helper.testOrgId,
          data: {
            action: 'clicked' as any,
            actionSubject: 'field' as any,
            actionSubjectId: 'searchField',
            source: 'managedAccountsScreen',
            attributes: {
              managedUsersCountOnScreen: 20,
              totalManagedUsersCount: 42,
            },
          },
        });
      });
    });

    describe('by string filter', () => {
      it('should update the query string when filter changes', async () => {
        const helper = new Helper({
          data: { total: 42 },
          stringFilterEnabled: true,
        });

        helper.setStringFilter('test name');

        await wait(550); // accounting for "debounce"

        helper.expectSearchQueryToBe('?filter=test%20name');
      });
      it('should clear the query string parameter when input becomes empty', () => {
        const helper = new Helper({
          data: { total: 42 },
          stringFilterEnabled: true,
          filter: 'test name',
        });

        helper.setStringFilter('');
        helper.expectSearchQueryToBe('');
      });
    });

    describe('by mfa status', () => {
      it('should update the query string when filter is applied', () => {
        const helper = new Helper({
          data: { total: 42 },
          isMfaEnforced: true,
        });

        helper.expectMfaStatusFilterToBePresent();
        helper.searchForMembersWithMfaStatus('disabled');
        helper.expectSearchQueryToBe('?mfa=false');
      });
      it('should clear the query string parameter when filter is unapplied', () => {
        const helper = new Helper({
          data: { total: 42 },
          isMfaEnforced: true,
          mfaStatusFilter: 'disabled',
        });

        helper.expectMfaStatusFilterToBePresent();
        helper.searchForMembersWithMfaStatus('enabled');
        helper.expectSearchQueryToBe('');
      });
      it('should not render mfa filter when mfa is not enforced', () => {
        const helper = new Helper({
          data: { total: 42 },
          isMfaEnforced: false,
        });

        helper.expectMfaStatusFilterToBeAbsent();
      });
    });

    describe('by domain', () => {
      it('should not render domain filter when there is 1 domain', () => {
        const helper = new Helper({
          data: { total: 42 },
          verifiedDomains: ['test-domain.com'],
        });

        helper.expectDomainFilterToBeAbsent();
      });
      it('should update the query string when filter changes', () => {
        const helper = new Helper({
          data: { total: 42 },
          verifiedDomains: ['test-domain1.com', 'test-domain2.com'],
        });

        helper.expectDomainFilterToBePresent();
        helper.searchForMembersWithDomain('test-domain2.com');
        helper.expectSearchQueryToBe('?domain=test-domain2.com');
      });

      describe('when all domains feature flag is on', () => {
        it('should search for an empty domain with feature flag disabled', () => {
          const helper = new Helper({ data: { total: 42 }, active: undefined, verifiedDomains: ['test-domain1.com', 'test-domain2.com'], isDataLoading: false, isAllDomainMembersEnabled: false });

          helper.expectDomainFilterToBePresent();
          helper.searchForMembersWithDomain('');
          helper.expectSearchQueryToBe('?domain=');
        });

        it('should search for an empty domain with feature flag enabled', () => {
          const helper = new Helper({ data: { total: 42 }, active: undefined, verifiedDomains: ['test-domain1.com', 'test-domain2.com'], isDataLoading: false, isAllDomainMembersEnabled: true });

          helper.expectDomainFilterToBePresent();
          helper.searchForMembersWithDomain('');
          helper.expectSearchQueryToBe('');
        });
      });
    });

    describe('by product access', () => {
      const sampleProducts = {
        product1: { key: 'test-product-1', name: 'Test Product one' } as Product,
        product2: { key: 'test-product-2', name: 'Test Product two' } as Product,
      };
      const allProducts = [sampleProducts.product1, sampleProducts.product2];

      describe('when feature flag is on', () => {
        it('should not render product access filter when the org has no products', () => {
          const helper = new Helper({
            data: { total: 42 },
            productAccessFilterEnabled: true,
            products: [],
          });

          helper.expectProductAccessFilterToBeAbsent();
        });

        it('should update the query string correctly when searching for one product', () => {
          const helper = new Helper({
            data: { total: 42 },
            productAccessFilterEnabled: true,
            products: allProducts,
          });

          helper.expectProductAccessFilterToBePresent();
          helper.searchForMembersWithProductAccess([sampleProducts.product1]);

          helper.expectSearchQueryToBe(`?products=${sampleProducts.product1.key}`);
        });

        it('should update the query string correctly when searching for multiple products', () => {
          const helper = new Helper({
            data: { total: 42 },
            productAccessFilterEnabled: true,
            products: allProducts,
          });

          helper.expectProductAccessFilterToBePresent();
          helper.searchForMembersWithProductAccess([sampleProducts.product1, sampleProducts.product2]);

          helper.expectSearchQueryToBe(`?products=${sampleProducts.product1.key}&products=${sampleProducts.product2.key}`);
        });

        it('should initiate product access selection correctly when no query string params are present', () => {
          const helper = new Helper({
            data: { total: 42 },
            productAccessFilterEnabled: true,
            products: allProducts,
            productAccessQuery: undefined,
          });

          helper.expectProductAccessFilterToBePresent();
          helper.expectProductAccessSelectionToBe([]);
        });

        it('should initiate product access selection correctly when one query string param was present', () => {
          const helper = new Helper({
            data: { total: 42 },
            productAccessFilterEnabled: true,
            products: allProducts,
            productAccessQuery: [sampleProducts.product2.key],
          });

          helper.expectProductAccessFilterToBePresent();
          helper.expectProductAccessSelectionToBe([sampleProducts.product2]);
        });

        it('should initiate product access selection correctly when multiple query string params were present', () => {
          const helper = new Helper({
            data: { total: 42 },
            productAccessFilterEnabled: true,
            products: allProducts,
            productAccessQuery: [sampleProducts.product1.key, sampleProducts.product2.key],
          });

          helper.expectProductAccessFilterToBePresent();
          helper.expectProductAccessSelectionToBe([sampleProducts.product1, sampleProducts.product2]);
        });
      });

      it('should not render product access filter when feature flag is off', () => {
        const helper = new Helper({
          data: { total: 42 },
          products: allProducts,
          productAccessFilterEnabled: false,
        });

        helper.expectProductAccessFilterToBeAbsent();
      });
    });

    it('handles empty results correctly', () => {
      const helper = new Helper({
        data: { total: 0 },
        verifiedDomains: ['test-domain.com'],
      });

      helper.expectMembersTableRowCountToBe(0);
    });
  });

  describe('"Edit account"', () => {
    it('should submit an analytics event when "Edit account" link is clicked', () => {
      const helper = new Helper({
        data: { total: 42 },
      });

      helper.clickEditAccount('test-user-14', 14);
      helper.expectAnalyticsUIEventToBeSubmitted({
        orgId: helper.testOrgId,
        data: {
          action: 'clicked' as any,
          actionSubject: 'link' as any,
          actionSubjectId: 'editAccountLink',
          source: 'managedAccountsScreen',
          attributes: {
            managedUsersCountOnScreen: 20,
            totalManagedUsersCount: 42,
            page: 1,
            numberOnPage: 14,
            fuzzyResults: false,
          },
        },
      });
    });

    describe('fuzzyResults analytics attribute', () => {
      const testMember = {
        id: 'test-user-14',
        positionOnPage: 14,
        displayName: `Test User 14`,
        email: 'test_user_14@example.com',
        nonMatchingFilter: 'something',
        displayNameMatchingFilter: 'user 1',
        emailMatchingFilter: 'user_14',
      };

      function createHelper(filterEnabled: boolean, filter: string | undefined) {
        return new Helper({
          data: { total: 42 },
          stringFilterEnabled: filterEnabled,
          filter,
        });
      }

      function clickOnTestUser(helper: Helper) {
        helper.clickEditAccount('test-user-14', 14);
      }

      function expectFuzzyResultAnalyticsAttributeToBe(helper: Helper, expected: boolean) {
        helper.expectAnalyticsUIEventToBeSubmitted({
          orgId: helper.testOrgId,
          data: {
            action: 'clicked' as any,
            actionSubject: 'link' as any,
            actionSubjectId: 'editAccountLink',
            source: 'managedAccountsScreen',
            attributes: {
              managedUsersCountOnScreen: 20,
              totalManagedUsersCount: 42,
              page: 1,
              numberOnPage: testMember.positionOnPage,
              fuzzyResults: expected,
            },
          },
        });
      }

      describe('when string filter feature flag is on', () => {
        it('should be true when no match is present in neither email nor display name', () => {
          const helper = createHelper(true, testMember.nonMatchingFilter);

          clickOnTestUser(helper);

          expectFuzzyResultAnalyticsAttributeToBe(helper, true);
        });

        it('should be false when match is present in email', () => {
          const helper = createHelper(true, testMember.emailMatchingFilter);

          clickOnTestUser(helper);

          expectFuzzyResultAnalyticsAttributeToBe(helper, false);
        });

        it('should be false when match is present in email regardless of casing', () => {
          const helper = createHelper(true, testMember.emailMatchingFilter.toUpperCase());

          clickOnTestUser(helper);

          expectFuzzyResultAnalyticsAttributeToBe(helper, false);
        });

        it('should be false when match is present in display name', () => {
          const helper = createHelper(true, testMember.displayNameMatchingFilter);

          clickOnTestUser(helper);

          expectFuzzyResultAnalyticsAttributeToBe(helper, false);
        });

        it('should be false when match is present in display name regardless of casing', () => {
          const helper = createHelper(true, testMember.displayNameMatchingFilter.toUpperCase());

          clickOnTestUser(helper);

          expectFuzzyResultAnalyticsAttributeToBe(helper, false);
        });
      });

      describe('when string filter feature flag is off', () => {
        it('should be false when no match is present in neither email nor display name', () => {
          const helper = createHelper(false, testMember.nonMatchingFilter);

          clickOnTestUser(helper);

          expectFuzzyResultAnalyticsAttributeToBe(helper, false);
        });

        it('should be false when match is present in email', () => {
          const helper = createHelper(false, testMember.emailMatchingFilter);

          clickOnTestUser(helper);

          expectFuzzyResultAnalyticsAttributeToBe(helper, false);
        });

        it('should be false when match is present in display name', () => {
          const helper = createHelper(false, testMember.displayNameMatchingFilter);

          clickOnTestUser(helper);

          expectFuzzyResultAnalyticsAttributeToBe(helper, false);
        });
      });
    });
  });

  describe('when updating member status filters', () => {
    it('should correctly display when filtering by all', () => {
      const helper = new Helper({ data: { total: 42 }, active: undefined, isDataLoading: false });

      helper.expectAllStatusFilter();
    });

    it('should correctly display when filtering by active', () => {
      const helper = new Helper({ data: { total: 42 }, active: 'true', isDataLoading: false });

      helper.expectActiveStatusFilter();
    });

    it('should correctly display when filtering by deactivated', () => {
      const helper = new Helper({ data: { total: 42 }, active: 'false', isDataLoading: false });

      helper.expectDeactivatedStatusFilter();
    });
  });

  describe('with parallelized queries', () => {
    it('should not render anything if data is still loading', () => {
      const helper = new Helper({ data: { total: 42 }, isDataLoading: true });

      helper.expectNullToBeRendered();
    });

    it('should not render anything if org has no members', () => {
      const helper = new Helper({ data: { total: 0, globalTotal: 0 } });

      helper.expectNullToBeRendered();
    });

    it('should not render null if data is done loading', () => {
      const helper = new Helper({ data: { total: 42 }, isDataLoading: false });

      helper.expectNullToNotBeRendered();
    });
  });
});
