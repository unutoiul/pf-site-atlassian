import memoizeOne from 'memoize-one';
import { parse, stringify } from 'query-string';
import * as React from 'react';
import { graphql, OptionProps, QueryOpts, QueryResult } from 'react-apollo';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';
import styled from 'styled-components';

import { gridSize as akGridSize } from '@atlaskit/theme';

import {
  analyticsClient,
  AnalyticsClientProps,
  editAccountLinkClickEventData,
  ManagedAccountsScreenCommonAttributes,
  managedAccountsSearchFieldClickEventData,
  withAnalyticsClient,
} from 'common/analytics';
import { GenericErrorMessage } from 'common/error';
import { FieldTextFixed } from 'common/field-text';
import { getStart, sanitizePage } from 'common/pagination';

import { MemberPageTableQuery, MemberPageTableQueryVariables, MemberStateFilter, MemberStatus, MemberTableStableDataQuery, MemberTableStableDataQueryVariables, Product } from '../../schema/schema-types';
import { debounce } from '../../utilities/decorators';
import { AllDomainMembersFeatureFlag, withAllDomainMembersFeatureFlag } from '../feature-flags/with-all-domain-members-feature-flag';
import { MemberStateEnumFeatureFlag, withMemberStateEnumFeatureFlag } from '../feature-flags/with-member-state-enum-feature-flag';
import { MemberTextFilteringFeatureFlag, withMemberTextFilteringFeatureFlag } from '../feature-flags/with-member-text-filtering-feature-flag';
import { ProductAccessFeatureFlag, withProductAccessFeatureFlag } from '../feature-flags/with-product-access-filter-feature-flag';
import { createOrgAnalyticsData } from '../organizations/organization-analytics';
import { DomainFilter } from './domain-filter';
import memberPageTableQuery from './member-page-table.query.graphql';
import { MemberTable } from './member-table';
import memberPageTableStableDataQuery from './member-table-stable-props.query.graphql';
import { Domain, Member, MfaExemptUser } from './member.prop-types';
import { MfaFilter } from './mfa-filter';
import { ProductAccessFilter } from './product-access-filter';
import { StatusFilter } from './status-filter';

export const ROWS_PER_PAGE = 20;

/*
  DOMAIN_FILTER_LIMIT should be the same as `search.domains.limit` here: https://stash.atlassian.com/projects/AID/repos/aid-account/browse/src/main/resources/aid.conf#113
  Due to consistent timeouts in Auth0 (see KT-1036) we have to temporarily reduce this from 30 to 4.
  Due to even more unknown timeouts, we have had to temporarily reduce this number from 4 to just 1.
  https://sdog.jira-dev.com/browse/KT-1134
*/
const DOMAIN_FILTER_LIMIT = 1;

const FilterBar = styled.div`
  align-items: baseline;
  display: flex;
  flex-wrap: wrap;
  align-content: start;
  margin-top: ${akGridSize() * 3}px;

  > * {
    margin-bottom: ${akGridSize() * 2}px;
  }

  > span {
    margin: 0 ${akGridSize() * 3}px;

    &:first-child {
      margin-left: 0;
    }
  }
`;

const TextFieldWrapper = styled.div`
  padding-right: ${akGridSize() * 3}px;
  width: ${akGridSize() * 24}px;
`;

const OptionsWrapper = styled.div`
  align-self: baseline;
  padding: 0 ${akGridSize() * 2}px 0 ${akGridSize()}px;
`;

function generatePlaceholders(placeholders: number): Member[] {
  const disabled: MemberStatus = 'DISABLED';

  return Array(placeholders).fill({}).map((_, i) => ({
    id: `${i}`,
    active: disabled,
    displayName: '',
    emails: [{
      primary: false,
      value: '',
    }],
    isPlaceholder: true,
    memberAccess: {
      errors: [],
      products: [],
    },
  }));
}

export const messages = defineMessages({
  domainFilterChoose: {
    id: 'organizations.members.domain.filter.choose.instructions',
    defaultMessage: 'Choose a domain in the filter above to get started.',
  },
  displayNameSearchPlaceholder: {
    id: 'organizations.members.search.by.display.name',
    defaultMessage: 'Search by name',
  },
  searchPlaceholder: {
    id: 'organizations.members.search-placeholder',
    defaultMessage: 'Search by name and email',
    description: 'This is a placeholder for an input field. Admins use this field to search for users who have certain characters in either their display name, or their email address.',
  },
});

interface SearchFilters {
  active: string | undefined;
  mfa: string | undefined;
  displayName: string | undefined;
  filter: string | undefined;
  domain: string | undefined;
  products: string[] | undefined;
  memberState: MemberStateFilter | undefined;
}

interface OwnProps {
  isDataLoading: boolean; // this boolean is the master boolean that determines if anything in the component is still loading, if true, will always cause the component to return null until all data has finished loading and `isDataLoading` is false. That day, the data will be fetched immediately and the appropriate data will be cached.
  totalMembers: number | undefined;
  verifiedDomains: Domain[];
  isMfaEnforced: boolean;
  mfaExemptMembers: MfaExemptUser[];
  stringFilterEnabled: boolean;
  isFilteringByProductAccessEnabled: boolean;
  isMemberStateEnumEnabled: boolean;
  isAllDomainMembersEnabled: boolean;
  products: Product[];
}

interface DerivedProps {
  loading: boolean;
  error?: QueryResult['error'];
  data?: {
    total: number;
    members: Member[];
  };
}

export type Props = OwnProps & DerivedProps & RouteComponentProps<any> & InjectedIntlProps & AnalyticsClientProps;

interface State {
  previousTotal: number | undefined;
}

export class MemberTableWithFiltersImpl extends React.Component<Props, State> {
  public state: Readonly<State> = {
    previousTotal: undefined,
  };

  public render() {
    const { isDataLoading, totalMembers } = this.props;

    if (isDataLoading || totalMembers === 0) {
      // if totalMembers is 0, then the top-level component will show the appropriate UI
      return null;
    }

    const {
      intl: { formatMessage },
      isMfaEnforced = false,
      verifiedDomains = [],
      mfaExemptMembers = [],
      stringFilterEnabled,
      isFilteringByProductAccessEnabled,
      products: allProducts,
      isAllDomainMembersEnabled,
    } = this.props;

    const { page, ...searchFilters } = this.getParams();
    const {
      filter,
      active,
      domain,
      mfa,
      displayName,
      products: productsFromQueryString,
      memberState,
    } = searchFilters;

    const selectedProducts = isFilteringByProductAccessEnabled
      ? allProducts.filter(p => (productsFromQueryString || []).includes(p.key))
      : [];

    const searchPlaceholder = stringFilterEnabled
      ? formatMessage(messages.searchPlaceholder)
      : formatMessage(messages.displayNameSearchPlaceholder);
    const searchValue = (stringFilterEnabled ? filter : displayName) || '';
    const memberStateFilterValue = this.getMemberStateValue({ active, memberState });

    return (
      <React.Fragment>
        <FilterBar>
          <TextFieldWrapper>
            <FieldTextFixed
              id="member-table-filter-field"
              value={searchValue}
              label={searchPlaceholder}
              placeholder={searchPlaceholder}
              isLabelHidden={true}
              shouldFitContainer={true}
              onChange={this.onFilterStringChange}
              onFocus={this.onFilterFieldFocus}
            />
          </TextFieldWrapper>
          <OptionsWrapper>
            <StatusFilter
              memberState={memberStateFilterValue}
              statusFilterItemActivated={this.statusFilterItemActivated}
              isMemberStateEnumEnabled={this.props.isMemberStateEnumEnabled}
            />
          </OptionsWrapper>
          {isMfaEnforced &&
            <OptionsWrapper>
              <MfaFilter
                mfa={mfa === undefined ? undefined : mfa === 'true'}
                mfaFilterItemActivated={this.mfaFilterItemActivated}
              />
            </OptionsWrapper>
          }
          {verifiedDomains.length > 1 &&
            <OptionsWrapper>
              <DomainFilter
                value={domain}
                onDomainSelected={this.domainFilterItemSelected}
                domainFilterLimit={isAllDomainMembersEnabled ? Infinity : DOMAIN_FILTER_LIMIT}
                verifiedDomains={verifiedDomains}
              />
            </OptionsWrapper>
          }
          {isFilteringByProductAccessEnabled && allProducts.length > 0 && (
            <OptionsWrapper>
              <ProductAccessFilter
                allProducts={allProducts}
                selectedProducts={selectedProducts}
                onProductSelectionChanged={this.onProductSelectionChanged}
              />
            </OptionsWrapper>
          )}
        </FilterBar>
        {this.showTable(page, verifiedDomains, searchFilters.domain, mfaExemptMembers)}
      </React.Fragment>
    );
  }

  private getMemberStateValue = ({ active, memberState }) => {
    const { isMemberStateEnumEnabled } = this.props;

    if (isMemberStateEnumEnabled) {
      return memberState;
    } else {
      return active === undefined ? undefined : active === 'true' ? 'ENABLED' : 'DISABLED';
    }
  }

  private onFilterFieldFocus = () => {
    const {
      match: { params: { orgId } },
      analyticsClient: { sendUIEvent },
    } = this.props;

    sendUIEvent({
      orgId,
      data: managedAccountsSearchFieldClickEventData(this.getCommonAnalyticsAttributes()),
    });
  }

  private getCommonAnalyticsAttributes(): ManagedAccountsScreenCommonAttributes {
    const {
      totalMembers,
    } = this.props;

    const totalManagedUsersCount = totalMembers == null ? -1 : totalMembers;
    const { page } = this.getParams();
    const members = this.getMembers(page);
    if (!members) {
      analyticsClient.onError(new Error('members not found'));

      return {
        managedUsersCountOnScreen: 0,
        totalManagedUsersCount,
      };
    }

    return {
      managedUsersCountOnScreen: Math.min(members.length, ROWS_PER_PAGE),
      totalManagedUsersCount,
    };
  }

  private onProductSelectionChanged = (products: Product[]) => {
    const selectedProductKeys = products.map(p => p.key);
    this.updateFilters({
      products: selectedProductKeys.length > 0
        ? selectedProductKeys
        : undefined,
    });
  }

  private statusFilterItemActivated = ({ item: { memberState } }) => {
    const { isMemberStateEnumEnabled } = this.props;

    if (isMemberStateEnumEnabled) {
      this.updateFilters({
        memberState,
        active: undefined,
      });
    } else {
      this.updateFilters({
        active: memberState === undefined ? undefined : memberState === 'ENABLED' ? 'true' : 'false',
        memberState: undefined,
      });
    }
  };

  private mfaFilterItemActivated = ({ item: { mfa } }) => {
    analyticsClient.eventHandler(createOrgAnalyticsData({
      orgId: this.props.match.params.orgId,
      action: 'filter',
      actionSubject: 'mfaUnenrolledFilterToggle',
      actionSubjectId: `filterMfaUnenrolled${(mfa === false)}`,
    }));

    this.updateFilters({
      mfa,
    });
  }

  private domainFilterItemSelected = (domain: string): void => {
    const domainFilter = !domain.length && this.props.isAllDomainMembersEnabled ? undefined : domain;

    this.updateFilters({
      domain: domainFilter,
    });
  };

  private showTable = (
    page: number,
    verifiedDomains: Domain[],
    domainFilter: string | undefined,
    mfaExemptMembers: MfaExemptUser[],
  ): React.ReactNode => {
    const members = this.getMembers(page);

    const isValidDomain = verifiedDomains.find(verified => verified.domain === domainFilter);

    if (verifiedDomains.length > DOMAIN_FILTER_LIMIT && !domainFilter && !isValidDomain && !this.props.isAllDomainMembersEnabled) {
      return <i><FormattedMessage {...messages.domainFilterChoose} /></i>;
    }

    return (
      <MemberTable
        members={members || []}
        organizationId={this.props.match.params.orgId}
        mfaExemptMembers={mfaExemptMembers}
        onSetPage={this.updatePage}
        page={page}
        rowsPerPage={ROWS_PER_PAGE}
        onMemberDetailsLinkClick={this.onMemberDetailsLinkClick}
      />
    );
  }

  private onMemberDetailsLinkClick = (memberId: string, rowPosition: number): void => {
    const {
      match: { params: { orgId } },
      analyticsClient: { sendUIEvent },
      stringFilterEnabled,
    } = this.props;
    const { page, filter } = this.getParams();

    const members = this.getMembers(page);
    if (!members) {
      analyticsClient.onError(new Error('members not found'));

      return;
    }

    const clickedMember = members.find(m => m.id === memberId);
    if (!clickedMember) {
      // member not found, and that's weird
      analyticsClient.onError(new Error('details of a clicked member not found'));

      return;
    }

    let isFuzzyResult = false;
    if (stringFilterEnabled && filter !== undefined) {
      const lowercaseFilter = filter.toLowerCase();
      const lowercaseDisplayName = clickedMember.displayName.toLowerCase();
      const lowercaseEmail = clickedMember.emails.length > 0
        ? clickedMember.emails[0].value.toLowerCase()
        : '';
      isFuzzyResult = !lowercaseDisplayName.includes(lowercaseFilter) && !lowercaseEmail.includes(lowercaseFilter);
    }

    sendUIEvent({
      orgId,
      data: editAccountLinkClickEventData({
        ...this.getCommonAnalyticsAttributes(),
        page,
        numberOnPage: rowPosition,
        fuzzyResults: isFuzzyResult,
      }),
    });
  }

  @debounce('onInputChange')
  private searchByFilterString(filterString: string): void {
    const value = filterString || undefined;

    if (this.props.stringFilterEnabled) {
      this.updateFilters({ filter: value, displayName: undefined });
    } else {
      this.updateFilters({ displayName: value, filter: undefined });
    }
  }

  private onFilterStringChange = (e: React.FormEvent<HTMLInputElement>) => {
    const filterString = e.currentTarget.value;
    this.searchByFilterString(filterString);
  }

  private getParams(): SearchFilters & { page: number } {
    const {
      location: { search },
    } = this.props;

    const queryString = parse(search);
    const { page: pageFromQueryString, ...searchFilters } = queryString;

    const {
      active,
      domain,
      mfa,
      displayName,
      filter,
      products: productsFromQueryString,
      memberState,
    } = searchFilters;

    const page = sanitizePage(pageFromQueryString);

    const products: string[] | undefined = productsFromQueryString == null
      ? undefined
      : typeof productsFromQueryString === 'string' // in case there's only one product
        ? [productsFromQueryString]
        : productsFromQueryString;

    return {
      active,
      domain,
      mfa,
      displayName,
      filter,
      page,
      products,
      memberState,
    };
  }

  private updatePage = (page: number) => {
    // Since we're only changing the page, the number of items stays the same in a general case (i.e. without taking concurrency into account)
    this.setState({ previousTotal: this.props.data && this.props.data.total });
    this.updateQueryParams({ page });
  }

  private updateFilters(params: Partial<SearchFilters>) {
    // we don't know how many items will there be in an updates dataset
    this.setState({ previousTotal: undefined });

    // reset the page in case it was set
    this.updateQueryParams({ ...params, page: undefined });
  }

  private updateQueryParams(params: object) {
    const { history, location: { search, pathname } } = this.props;
    const newParams = {
      ...parse(search),
      ...params,
    };

    const searchString = stringify(newParams);
    history.replace(`${pathname}${searchString.length > 0 ? `?${searchString}` : ''}`);
  }

  private getMembers(page: number): Member[] | null {
    const {
      data: {
        members = null,
        total = this.state.previousTotal,
      } = {},
    } = this.props;

    const start = getStart(page, ROWS_PER_PAGE) - 1;

    return this.getMembersMemoized(start, total === undefined ? ROWS_PER_PAGE : total, members);
  }

  // tslint:disable-next-line:member-ordering TSLint thinks this is a field, but it's really a method
  private getMembersMemoized = memoizeOne((start: number, total: number, members: Member[] | null) => {
    const users = generatePlaceholders(total);
    if (members) {
      users.splice(start, members.length, ...members);
    }

    return users;
  });
}

function getMemberPageTableProps(componentProps: OptionProps<RouteComponentProps<any>, MemberPageTableQuery>): DerivedProps {
  if (!componentProps || !componentProps.data || componentProps.data.loading || !componentProps.data.organization || !componentProps.data.organization.users.members) {
    return { loading: true };
  }

  if (componentProps.data.error) {
    return { loading: false, error: componentProps.data.error };
  }

  const {
    users: {
      members: {
        total,
        users,
      },
    },
  } = componentProps.data.organization;

  return {
    loading: false,
    data: {
      total,
      members: users || [],
    },
  };
}

const withMemberPageTableData = graphql<OwnProps & RouteComponentProps<any>, MemberPageTableQuery, MemberPageTableQueryVariables, DerivedProps>(memberPageTableQuery, {
  options: ({ location: { search }, match: { params: { orgId } } }): QueryOpts<MemberPageTableQueryVariables> => {
    const { active, mfa, page, displayName, domain, filter, products, memberState } = parse(search);

    return {
      variables: {
        id: orgId,
        count: ROWS_PER_PAGE,
        start: getStart(sanitizePage(page), ROWS_PER_PAGE),
        active: active && active === 'true',
        mfa: mfa && mfa === 'true',
        displayName,
        domain,
        filter,
        products,
        memberState,
      },
      // As Atlassian Account is so dynamic, and there are user updates happening outside of
      // the organization context, we do not want to fetch from the cache.
      fetchPolicy: 'network-only',
    };
  },
  props: getMemberPageTableProps,
  skip: ({ match: { params: { orgId } } }) => !orgId,
});

type WrapperComponentProps = RouteComponentProps<any>
  & MemberTextFilteringFeatureFlag
  & MemberTableWithFiltersProps
  & MemberStateEnumFeatureFlag
  & ProductAccessFeatureFlag
  & AllDomainMembersFeatureFlag
  ;

const withStableData = graphql<WrapperComponentProps, MemberTableStableDataQuery>(memberPageTableStableDataQuery, {
  options: ({
    match: { params: { orgId } },
    productAccessFeatureFlag: { value },
  }): QueryOpts<MemberTableStableDataQueryVariables> => ({
    variables: { id: orgId, productFilteringFeatureDisabled: !value },
  }),
  skip: ({ match: { params: { orgId } } }) => !orgId,
});

const Wrapped = withRouter(withMemberPageTableData(withAnalyticsClient(injectIntl(MemberTableWithFiltersImpl))));

export interface MemberTableWithFiltersProps {
  totalMembers: number | undefined;
}

export const MemberTableWithFilters = withRouter(
  withMemberTextFilteringFeatureFlag(
    withProductAccessFeatureFlag(
      withMemberStateEnumFeatureFlag(
        withAllDomainMembersFeatureFlag(
          withStableData(({
            data,
            totalMembers,
            productAccessFeatureFlag,
            memberTextFilteringFeatureFlag,
            memberStateEnumFeatureFlag,
            allDomainMembersFeatureFlag,
          }) => {
            if (!data || data.loading || memberTextFilteringFeatureFlag.isLoading || productAccessFeatureFlag.isLoading || memberStateEnumFeatureFlag.isLoading || allDomainMembersFeatureFlag.isLoading) {
              return (
                <Wrapped
                  isDataLoading={true}
                  totalMembers={totalMembers}
                  isMfaEnforced={false}
                  mfaExemptMembers={[]}
                  verifiedDomains={[]}
                  stringFilterEnabled={false}
                  isFilteringByProductAccessEnabled={false}
                  isMemberStateEnumEnabled={false}
                  isAllDomainMembersEnabled={false}
                  products={[]}
                />
              );
            }

            const org = data.organization;

            if (data.error || !org) {
              return <GenericErrorMessage />;
            }

            const {
              domainClaim: {
                domains,
              },
              security: {
                twoStepVerification,
              },
              users: {
                mfaExemptMembers,
              },
              products,
            } = org;

            return (
              <Wrapped
                isDataLoading={false}
                totalMembers={totalMembers}
                isMfaEnforced={!!(twoStepVerification && twoStepVerification.dateEnforced)}
                mfaExemptMembers={mfaExemptMembers || []}
                verifiedDomains={(domains || []).filter(d => d.verified)}
                stringFilterEnabled={!!memberTextFilteringFeatureFlag.value}
                isFilteringByProductAccessEnabled={!!productAccessFeatureFlag.value}
                isMemberStateEnumEnabled={!!memberStateEnumFeatureFlag.value}
                isAllDomainMembersEnabled={!!allDomainMembersFeatureFlag.value}
                products={products || []} // `products` from GraphQL will be null due to `@skip` when feature flag is off
              />
            );
          }),
        ),
      ),
    ),
  ),
);
