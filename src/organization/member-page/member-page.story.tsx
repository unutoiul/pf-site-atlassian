// tslint:disable jsx-use-translation-function react-this-binding-issue
import { storiesOf } from '@storybook/react';
import { ApolloError } from 'apollo-client';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';
import { MemoryRouter as Router, Route } from 'react-router-dom';

import AkPage from '@atlaskit/page';

import { Member, MemberStatus, Organization } from '../../schema/schema-types';
import { createApolloClientWithOverriddenResolvers } from '../../utilities/testing';
import { MemberPageImpl as StatelessMemberPage } from './member-page';

const baseProducts = [
  {
    productName: 'Confluence',
    productKey: 'confluence.ondemand',
    siteUrl: 'site-1',
  },
  {
    productName: 'Jira Core',
    productKey: 'jira-core.ondemand',
    siteUrl: 'site-1',
  },
  {
    productName: 'Jira Software',
    productKey: 'jira-software.ondemand',
    siteUrl: 'site-1',
  },
];

const duplicateProducts = [
  {
    productName: 'Confluence',
    productKey: 'confluence.ondemand',
    siteUrl: 'site-1',
  },
  {
    productName: 'Confluence',
    productKey: 'confluence.ondemand',
    siteUrl: 'site-2',
  },
  {
    productName: 'Jira Core',
    productKey: 'jira-core.ondemand',
    siteUrl: 'site-1',
  },
  {
    productName: 'Jira Core',
    productKey: 'jira-core.ondemand',
    siteUrl: 'site-2',
  },
  {
    productName: 'Jira Software',
    productKey: 'jira-software.ondemand',
    siteUrl: 'site-1',
  },
];

const membersAccess = [
  { products: baseProducts },
  { products: duplicateProducts },
  { products: [] },
  { errors: ['some-error'] },
  { products: baseProducts },
  { products: [] },
  {
    products: [
      {
        productName: 'Sourcetree',
        productKey: 'sourcetree',
        siteUrl: 'site-1',
      },
    ],
  },
  { products: baseProducts },
  { products: [] },
].map(ma => ({ products: [], sites: [], errors: [], ...ma }));

const usersWithProducts = (users: Member[]): Member[] => users.map((user, i) => ({
  ...user,
  memberAccess: membersAccess[i],
}));

const defaultMembers = usersWithProducts([
  { id: '557057:80abfbd6-e1b5-4f23-99d9-077658050e25', displayName: 'Alex Simkin', jobTitle: '', active: 'ENABLED' as MemberStatus, useMfa: false, emails: [{ value: 'asimkin@atlassian.com', primary: true }] },
  { id: '557057:4a24f93a-04b0-44ab-93f4-424b02d8dcb7', displayName: 'Alexander Minozhenko', jobTitle: 'Security Engineer', active: 'ENABLED' as MemberStatus, useMfa: false, emails: [{ value: 'aminozhenko@atlassian.com', primary: true }] },
  { id: '557057:57e7be63-e1cc-4a6f-b597-d61171415df6', displayName: 'Angel Bartomeu Bonillo', jobTitle: 'Quality Engineer', active: 'ENABLED' as MemberStatus, useMfa: false, emails: [{ value: 'abartomeubonillo@atlassian.com', primary: true }] },
  { id: '557057:5bc5f1a2-ba3f-472f-aeef-7eb41a9fba97', displayName: 'Dipanjan Laha', jobTitle: 'Senior Developer', active: 'ENABLED' as MemberStatus, useMfa: false, emails: [{ value: 'dlaha@atlassian.com', primary: true }] },
  { id: '557057:e095c181-83c3-4f79-acfb-d947e6ebca33', displayName: 'Helen Xue', jobTitle: 'Graduate Product Manager', active: 'ENABLED' as MemberStatus, useMfa: false, emails: [{ value: 'hxue@atlassian.com', primary: true }] },
  { id: '557057:708a28cd-b518-427a-a4c7-821d467feb3b', displayName: ' I told you all this before, when you have a swimming pool do not use chlorine, use salt water, the healing, salt water is the healing.', jobTitle: '', active: 'ENABLED' as MemberStatus, useMfa: false, emails: [{ value: 'reallyreallyreallyreallyreallyreallyreallyreallyreallyreallyreallyreallylongemail@atlassian.com', primary: true }] },
  { id: '557057:ce58bfaa-23be-4ac7-bb69-ac24aca0711e', displayName: 'Rachel De Paula Cavalcanti', jobTitle: '', active: 'ENABLED' as MemberStatus, useMfa: false, emails: [{ value: 'rdepaulacavalcanti@atlassian.com', primary: true }] },
  { id: '557057:b03d7440-663b-4c78-850d-5fc0016a69df', displayName: 'Saxon Bruce', jobTitle: 'Developer', active: 'ENABLED' as MemberStatus, useMfa: false, emails: [{ value: 'sbruce@atlassian.com', primary: true }] },
  { id: '557058:7ee82aa9-e4f9-49a3-9673-f03ad05c7cd0', displayName: 'test test', jobTitle: null, active: 'ENABLED' as MemberStatus, useMfa: false, emails: [{ value: 'test@example.com', primary: true }] },
]);

const defaultDomains = [{
  domain: 'atlassian.com',
  verified: true,
  status: 'VERIFIED',
}];

const testOrgId = 'DUMMY_ORG_ID';

const resolverOrgData: { organization: Partial<Organization> } = { organization: {} };
const client = createApolloClientWithOverriddenResolvers({
  Query: {
    organization: async () => resolverOrgData.organization,
  },
  Organization: {
    id: async () => resolverOrgData.organization.id,
    name: async () => resolverOrgData.organization.name,
    users: async () => resolverOrgData.organization.users,
    security: async () => resolverOrgData.organization.security,
    domainClaim: async () => resolverOrgData.organization.domainClaim,
    flag: async (_, { flagKey }) => ({ id: flagKey, value: false }),
  },
  OrganizationSecurity: {
    twoStepVerification: async () => resolverOrgData.organization.security!.twoStepVerification,
  },
  OrganizationUsers: {
    members: async () => resolverOrgData.organization.users!.members,
    mfaExemptMembers: async () => resolverOrgData.organization.users!.mfaExemptMembers,
  },
  DomainClaim: {
    domains: async () => resolverOrgData.organization.domainClaim!.domains,
  },
});

const StorybookMemberPage = ({
  data,
  error,
  loading,
  displayNameFilter,
  mfaEnforced,
  members,
}: {
  displayNameFilter?: string,
  mfaEnforced?: boolean,
  members?: any[],
} & Pick<StatelessMemberPage['props'], 'loading' | 'data' | 'error'>) => {
  class TestComp extends React.Component<{}, { configured: boolean }> {
    public state = { configured: false };

    public async componentDidMount() {
      await client.resetStore();
      await client.cache.reset();

      resolverOrgData.organization = {
        id: testOrgId,
        name: 'Dummy org name',
        domainClaim: {
          domains: data && data.verifiedDomains,
        },
        security: {
          twoStepVerification: {
            dateEnforced: mfaEnforced ? '1234567890' : null,
          },
        },
        users: {
          members: {
            total: members && members.length,
            users: members,
          },
        },
        flags: {
        },
      } as any;

      this.setState({ configured: true });
    }

    public render() {
      if (!this.state.configured) {
        return <h1>Please wait, configuring Apollo</h1>;
      }

      return (
        <ApolloProvider client={client}>
          <IntlProvider locale="en">
            <Router
              initialEntries={[{
                pathname: `/${testOrgId}`,
                search: displayNameFilter ? `?displayName=${displayNameFilter}` : undefined,
              }]}
            >
              <AkPage>
                <Route
                  path="/:orgId"
                  render={(props) =>
                    <StatelessMemberPage
                      {...props}
                      error={error}
                      loading={loading}
                      data={data}
                    />
                  }
                />
              </AkPage>
            </Router>
          </IntlProvider>
        </ApolloProvider>
      );
    }
  }

  return <TestComp />;
};

storiesOf('Organization|Member Page', module)
  .add('While loading', () => (
    <StorybookMemberPage
      loading={true}
    />
  ))
  .add('With error', () => (
    <StorybookMemberPage
      loading={false}
      error={new ApolloError({})}
    />
  ))
  .add('With no verified domains', () => (
    <StorybookMemberPage
      loading={false}
      data={{
        totalDisabledMembers: 0,
        totalMembers: 0,
        verifiedDomains: [],
      }}
    />
  ))
  .add('With a verified domain and no members', () => (
    <StorybookMemberPage
      loading={false}
      data={{
        totalDisabledMembers: 0,
        totalMembers: 0,
        verifiedDomains: defaultDomains,
      }}
    />
  ))
  .add('With a huge number of verified domains and no members', () => (
    <StorybookMemberPage
      loading={false}
      data={{
        totalDisabledMembers: 0,
        totalMembers: 0,
        verifiedDomains: Array(50).fill({}).map((_, i) => ({
          domain: `example${i}.com`,
          verified: true,
          status: 'VERIFIED',
        })),
      }}
    />
  ))
  .add('With a verified domain and a filter set resulting in no members', () => (
    <StorybookMemberPage
      loading={false}
      members={[]}
      displayNameFilter="idontexist"
      data={{
        totalDisabledMembers: 0,
        totalMembers: defaultMembers.length,
        verifiedDomains: defaultDomains,
      }}
    />
  ))
  .add('With multiple verified domains and a filter set resulting in no members', () => (
    <StorybookMemberPage
      loading={false}
      members={[]}
      data={{
        totalDisabledMembers: 0,
        totalMembers: defaultMembers.length,
        verifiedDomains: Array(3).fill({}).map((_, i) => ({
          domain: `example${i}.com`,
          verified: true,
          status: 'VERIFIED',
        })),
      }}
    />
  ))
  .add('With a verified domain and members', () => (
    <StorybookMemberPage
      loading={false}
      data={{
        totalDisabledMembers: 0,
        totalMembers: defaultMembers.length,
        verifiedDomains: defaultDomains,
      }}
      members={defaultMembers}
    />
  ))
  .add('With a verified domain, members and MFA enforced', () => (
    <StorybookMemberPage
      loading={false}
      mfaEnforced={true}
      members={defaultMembers}
      data={{
        totalDisabledMembers: 0,
        totalMembers: defaultMembers.length,
        verifiedDomains: defaultDomains,
      }}
    />
  ))
  .add('With multiple verified domains, members and MFA enforced', () => (
    <StorybookMemberPage
      loading={false}
      members={defaultMembers}
      mfaEnforced={true}
      data={{
        totalDisabledMembers: 0,
        totalMembers: defaultMembers.length,
        verifiedDomains: [
          {
            domain: 'atlassian.com',
            verified: true,
          },
          {
            domain: 'example.com',
            verified: true,
          },
        ],
      }}
    />
  ))
  .add('With a huge number of verified domains, members and MFA enforced', () => (
    <StorybookMemberPage
      loading={false}
      members={defaultMembers}
      mfaEnforced={true}
      data={{
        totalDisabledMembers: 0,
        totalMembers: defaultMembers.length,
        verifiedDomains: Array(50).fill({}).map((_, i) => ({
          domain: `example${i}.com`,
          verified: true,
        })),
      }}
    />
  ));
