import {
  MemberProduct,
  MemberStatus,
} from '../../schema/schema-types';

interface MemberEmail {
  primary: boolean;
  value: string;
}

export interface Member {
  id: string;
  active: MemberStatus;
  displayName: string;
  emails: MemberEmail[];
  isPlaceholder?: boolean;
  memberAccess: {
    products: MemberProduct[];
    errors: string[];
  } | null;
}

export interface MfaExemptUser {
  userId: string;
}

export interface Domain {
  verified: boolean;
  domain: string;
}
