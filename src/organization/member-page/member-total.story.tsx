import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';

import { createMockIntlProp } from '../../utilities/testing';
import { MemberTotal } from './member-total';

export const StatelessMemberTotal = (MemberTotal as any).WrappedComponent;

const data = {
  loading: false,
  error: false,
  organization: {
    users: {
      memberTotal: {
        total: 200,
        enabledTotal: 100,
        disabledTotal: 100,
      },
    },
  },
};

const loading = {
  loading: true,
};

storiesOf('Organization|Members Total', module)
  .add('Members total', () => (
    <IntlProvider locale="en">
      <StatelessMemberTotal intl={createMockIntlProp()} data={data} orgId="orgId"/>
    </IntlProvider>
  ))
  .add('Members total loading / error state', () => (
    <IntlProvider locale="en">
      <StatelessMemberTotal intl={createMockIntlProp()} data={loading} orgId="orgId"/>
    </IntlProvider>
  ));
