import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import { MemoryRouter } from 'react-router';
import * as sinon from 'sinon';
import { ThemeProvider } from 'styled-components';

import AkButton from '@atlaskit/button';

import { FlagProvider } from 'common/flag';
import { PageLayout } from 'common/page-layout';

import { createMockContext, noop } from '../../../src/utilities/testing';
import { AdminApiPageImpl } from './admin-api-page';

describe('Admin API page', () => {

  const sandbox = sinon.sandbox.create();
  let showDrawerSpy: sinon.SinonSpy;

  beforeEach(() => {
    showDrawerSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });
  const shallowAdminApiPage = () => {
    return mount((
      <ThemeProvider theme={{}}>
          <MemoryRouter>
            <FlagProvider>
              <AdminApiPageImpl
                match={{ params: { orgId: 'test' } } as any}
                location={{} as any}
                history={{} as any}
                analyticsClient={{ sendUIEvent: noop } as any}
                showDrawer={showDrawerSpy}
              />
            </FlagProvider>
          </MemoryRouter>
        </ThemeProvider>),
      createMockContext({ intl: true, apollo: true }),
    );
  };

  it('should render', () => {
    const wrapper = shallowAdminApiPage();
    const pageLayout = wrapper.find(PageLayout);

    expect(pageLayout.exists()).to.equal(true);
  });

  it('should open drawer', () => {
    const wrapper = shallowAdminApiPage();
    const button = wrapper.find(AkButton).filter('[appearance="primary"]').first();

    button.simulate('click');

    expect(showDrawerSpy.called).to.equal(true);
  });
});
