import ApolloClient, { ApolloQueryResult } from 'apollo-client';
import * as React from 'react';
import { withApollo } from 'react-apollo';
import { defineMessages, FormattedDate, FormattedMessage } from 'react-intl';
import styled from 'styled-components';

import AkDynamicTableStateless from '@atlaskit/dynamic-table';
import {
  colors as akColors,
  fontSize as akFontSize,
  gridSize as akGridSize,
} from '@atlaskit/theme';

import { GenericError } from 'common/error';
import { EmptyTableView } from 'common/table';

import { withDrawerCloseSubscriber, WithDrawerCloseSubscriberProps } from 'common/drawer/with-drawer-close-subscriber';

import { AdminApiKeysQuery } from '../../schema/schema-types';
import { rowsPerPage } from './admin-api-constants';
import adminApiKeysQuery from './admin-api-keys.query.graphql';
import { RevokeAdminApiKeyButton } from './revoke-admin-api-key-button';

const messages = defineMessages({
  errorTitle: {
    id: 'organization.admin.api.error',
    defaultMessage: 'No information found',
    description: 'Error message that displays when the page could not load',
  },
  noApiKeys: {
    id: 'organization.admin.api.noApiKeys',
    defaultMessage: `You don't have any API keys`,
    description: 'Message that shows when the user does not have any API keys',
  },
  nameHeader: {
    id: 'organization.admin.api.nameHeader',
    defaultMessage: 'Name',
    description: 'Table header for a column containing API key names',
  },
  createdByHeader: {
    id: 'organization.admin.api.createdByHeader',
    defaultMessage: 'Created by',
    description: 'Table header for a column containing names of users who created API key',
  },
  createdOnHeader: {
    id: 'organization.admin.api.createdOnHeader',
    defaultMessage: 'Created on',
    description: 'Table header for a column containing API key creation dates',
  },
  expiresHeader: {
    id: 'organization.admin.api.expiresHeader',
    defaultMessage: 'Expires',
    description: 'Table header for a column containing API key expiration dates',
  },
});

const TableWrapper = styled.div`
  margin-top: ${akGridSize() * 2}px;

  tbody {
    vertical-align: top;
  }
`;

const EmailWrapper = styled.div`
  color: ${akColors.subtleHeading};
  font-size: ${akFontSize() - 2}px;
`;

const fillerTokenId = '__filler_token__';

const fillerToken: AdminApiKeysQuery['organization']['adminApiKeys']['tokens'][0] = {
  __typename: 'AdminApiKey',
  id: fillerTokenId,
  name: '',
  createdBy: {
    __typename: 'Member',
    id: '',
    displayName: '',
    primaryEmail: '',
  },
  creationDate: '',
  expirationDate: '',
};

interface ApolloClientProps {
  client: ApolloClient<any>;
}

interface OwnProps {
  orgId: string;
}

type AdminApiTableProps = OwnProps & ApolloClientProps & WithDrawerCloseSubscriberProps;

interface State {
  loading: boolean;
  error: boolean;
  page: number;
  scrollId: string | null;
  apiKeys: AdminApiKeysQuery['organization']['adminApiKeys']['tokens'];
}

export class AdminApiTableImpl extends React.Component<AdminApiTableProps, State> {
  public readonly state: Readonly<State> = {
    loading: true,
    error: false,
    page: 1,
    scrollId: null,
    apiKeys: [],
  };

  public componentDidMount() {
    void this.loadApiKeys(1, this.state.scrollId, this.state.apiKeys, false);
    this.props.whenDrawerClosed(this.reloadApiKeys, 'CREATE_ADMIN_API_KEY_DRAWER');
  }

  public render() {
    const { loading, error } = this.state;

    return (
      <TableWrapper>
        <AkDynamicTableStateless
          isLoading={loading}
          head={this.getHead()}
          rows={this.getRows()}
          page={this.state.page}
          rowsPerPage={rowsPerPage}
          onSetPage={this.onSetPage}
          emptyView={
            error ? (
                <GenericError header={<FormattedMessage {...messages.errorTitle} />} />
              ) : (
                <EmptyTableView>
                  <FormattedMessage {...messages.noApiKeys} />
                </EmptyTableView>
              )
          }
        />
      </TableWrapper>
    );
  }

  private onSetPage = async (page: number) => {
    const { apiKeys, scrollId } = this.state;

    if (!scrollId || page < apiKeys.length / rowsPerPage) {
      this.setState({ page });

      return;
    }

    this.setState({ loading: true });
    void this.loadApiKeys(page, scrollId, apiKeys, false);
  }

  /**
   * This table has some unique pagination. The API returns a scrollId if there are more items, so in that case we add
   * an extra item to the local state array so that AkTable is tricked into thinking there's another page. When the user
   * tries to load the next page, we make a new request with the most recent scrollId, and then add the returned items
   * into our local state array.
   */
  private loadApiKeys = async (page: number, scrollId: State['scrollId'], apiKeys: State['apiKeys'], refetch: boolean) => {
    try {
      const result: ApolloQueryResult<AdminApiKeysQuery> = await this.props.client.query<AdminApiKeysQuery>({
        query: adminApiKeysQuery,
        variables: {
          id: this.props.orgId,
          scrollId,
        },
        fetchPolicy: refetch ? 'network-only' : 'cache-first',
      });
      if (result.errors) {
        throw new Error('Problem loading API keys');
      }
      // if we have a scroll ID, then add a filler row to trick AtlasKit table into thinking there's another page
      const { scrollId: newScrollId } = result.data.organization.adminApiKeys;
      const filler = newScrollId ? [fillerToken] : [];
      const newApiKeys = [
        ...apiKeys.slice(0, -1),
        ...result.data.organization.adminApiKeys.tokens,
        ...filler,
      ];
      this.setState({ apiKeys: newApiKeys, scrollId: newScrollId, loading: false, page });
    } catch (_) {
      this.setState({ error: true, loading: false });
    }
  }

  /**
   * Reload keys after creation or deletion
   */
  private reloadApiKeys = () => {
    void this.loadApiKeys(1, null, [], true);
  }

  private getHead = () => {
    return {
      cells: [
        {
          key: 'name',
          content: <FormattedMessage {...messages.nameHeader} />,
          width: 25,
        },
        {
          key: 'createdBy',
          content: <FormattedMessage {...messages.createdByHeader} />,
          width: 25,
        },
        {
          key: 'createdOn',
          content: <FormattedMessage {...messages.createdOnHeader} />,
          width: 20,
        },
        {
          key: 'expires',
          content: <FormattedMessage {...messages.expiresHeader} />,
          width: 20,
        },
        {
          key: 'revoke',
          width: 10,
        },
      ],
    };
  }

  private getRows = () => {
    const { apiKeys } = this.state;
    const { orgId } = this.props;

    return apiKeys.map((key) => ({
      key: `row-${key.id}`,
      cells: key.id === fillerTokenId ? [] : [
        {
          key: 'name',
          content: key.name,
        },
        {
          key: 'created-by',
          content: (
            <React.Fragment>
              <div>{key.createdBy.displayName}</div>
              <EmailWrapper>{key.createdBy.primaryEmail}</EmailWrapper>
            </React.Fragment>
          ),
        },
        {
          key: 'creation-date',
          content: <FormattedDate value={key.creationDate} day="numeric" month="short" year="numeric" />,
        },
        {
          key: 'expiration',
          content: <FormattedDate value={key.expirationDate} day="numeric" month="short" year="numeric" />,
        },
        {
          key: 'revoke',
          content: (
            <RevokeAdminApiKeyButton
              apiKey={key}
              orgId={orgId}
              onRevoke={this.reloadApiKeys}
            />
          ),
        },
      ],
    }));
  }
}

export const AdminApiTable =
  withDrawerCloseSubscriber(
    withApollo(
      AdminApiTableImpl,
    ),
  )
;
