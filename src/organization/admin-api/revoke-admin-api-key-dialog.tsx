import * as React from 'react';
import {
  defineMessages,
  FormattedMessage,
} from 'react-intl';

import AkButton, { ButtonGroup as AkButtonGroup } from '@atlaskit/button';

import { ModalDialog } from 'common/modal';

export const messages = defineMessages({
  title: {
    id: 'organization.adminApi.token.revoke.dialog.title',
    defaultMessage: 'Revoke {apiKeyName}',
    description: 'The title of the dialog to delete an API key.',
  },
  description: {
    id: 'organization.adminApi.token.revoke.dialog.description',
    defaultMessage: 'Revoking an API key is permanent. It may take up to 10 minutes for a revoked key to stop working.',
    description: 'Modal description telling the user what happens when an API key is revoked.',
  },
  buttonRevoke: {
    id: 'organization.adminApi.token.revoke.dialog.button.revoke',
    defaultMessage: 'Revoke',
    description: 'The button that confirms the revoking of the user API key.',
  },
  buttonCancel: {
    id: 'organization.adminApi.token.revoke.dialog.button.cancel',
    defaultMessage: 'Cancel',
    description: 'The button that cancels and closes the dialog.',
  },
});

export interface RevokeAdminApiKeyDialogProps {
  isOpen: boolean;
  isRevokeLoading: boolean;
  apiKeyName: string;
  onDismiss(): void;
  onRevoke(): Promise<void>;
}

export class RevokeAdminApiKeyDialog extends React.Component<RevokeAdminApiKeyDialogProps> {
  public render() {
    const {
      isOpen,
      isRevokeLoading,
      onDismiss,
      onRevoke,
      apiKeyName,
    } = this.props;

    return (
      <ModalDialog
        isOpen={isOpen}
        width="small"
        onClose={onDismiss}
        header={(
          <FormattedMessage {...messages.title} values={{ apiKeyName }} />
        )}
        footer={(
          <AkButtonGroup>
            <AkButton
              id="confirm-revoke-admin-api-token-button" // might be used by pollinator, check before modifying
              appearance="danger"
              onClick={onRevoke}
              isLoading={isRevokeLoading}
            >
              <FormattedMessage {...messages.buttonRevoke} />
            </AkButton>
            <AkButton
              appearance="subtle-link"
              onClick={onDismiss}
            >
              <FormattedMessage {...messages.buttonCancel} />
            </AkButton>
          </AkButtonGroup>
        )}
      >
        <FormattedMessage
          {...messages.description}
          tagName="p"
        />
      </ModalDialog>
    );
  }
}
