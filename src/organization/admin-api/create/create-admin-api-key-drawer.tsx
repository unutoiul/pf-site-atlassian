import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';
import styled from 'styled-components';

import { ProgressTracker as AkProgressTracker } from '@atlaskit/progress-tracker';
import { gridSize as akGridSize } from '@atlaskit/theme';

import { OrgRouteProps } from '../../routes';
import { AdminApiKeyDetails } from './admin-api-key-details';
import { CreateAdminApiKey } from './create-admin-api-key';

const messages = defineMessages({
  step1: {
    id: 'organization.adminApi.createDrawer.step1',
    defaultMessage: 'Name',
    description: 'A heading for a page to enter the name of an API key',
  },
  step2: {
    id: 'organization.adminApi.createDrawer.step2',
    defaultMessage: 'API key',
    description: 'A heading for a page to view an API key',
  },
});

export const Page = styled.div`
  margin: 0 auto;
  width: ${akGridSize() * 50}px;
`;

interface State {
  page: 'create-admin-api-key' | 'admin-api-key-details';
  name?: string;
  apiKey?: string;
}

interface OwnProps {
  onCloseDrawer(): void;
}

type InProps = InjectedIntlProps & RouteComponentProps<OrgRouteProps> & OwnProps;

export class CreateAdminApiKeyDrawerImpl extends React.Component<InProps, State> {
  public state: Readonly<State> = {
    page: 'create-admin-api-key',
  };

  public render() {
    const { intl: { formatMessage }, onCloseDrawer, match: { params: { orgId } } } = this.props;

    const progressTrackerItems = [
      {
        id: 'create-admin-api-key',
        label: formatMessage(messages.step1),
        percentageComplete: this.state.page === 'create-admin-api-key' ? 0 : 100,
        status: this.state.page === 'create-admin-api-key' ? 'current' : 'visited',
      },
      {
        id: 'api-key-details',
        label: formatMessage(messages.step2),
        percentageComplete: 0,
        status: this.state.page === 'create-admin-api-key' ? 'unvisited' : 'current',
      },
    ];

    return (
      <Page>
        <AkProgressTracker items={progressTrackerItems} />
        {this.state.page === 'create-admin-api-key' ?
          <CreateAdminApiKey
            onCloseDrawer={onCloseDrawer}
            goToDetailsPage={this.goToDetailsPage}
            orgId={orgId}
          /> :
          <AdminApiKeyDetails
            onCloseDrawer={onCloseDrawer}
            apiKeyName={this.state.name!}
            apiKey={this.state.apiKey!}
            orgId={orgId}
          />
        }
      </Page>
    );
  }

  private goToDetailsPage = (name: string, apiKey: string) => {
    if (name && apiKey) {
      this.setState({ page: 'admin-api-key-details', name, apiKey });
    }
  }

}

export const CreateAdminApiKeyDrawer = withRouter<OwnProps>(injectIntl<InProps>(CreateAdminApiKeyDrawerImpl));
