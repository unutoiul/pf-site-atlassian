// tslint:disable jsx-use-translation-function
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';
import { MemoryRouter as Router, Route } from 'react-router-dom';
import styled from 'styled-components';
import { withoutWhaleShot } from 'whales-story-shots/storybook/decorator';

import AkButton from '@atlaskit/button';
import AkPage from '@atlaskit/page';

import { FlagProvider } from 'common/flag';
import { FocusedTask } from 'common/focused-task';

import { CreateAdminApiKeyMutation } from '../../../schema/schema-types';
import { ApolloClientStorybookWrapper, StorybookAnalyticsClientProvider } from '../../../utilities/storybooks';
import { createApolloClientWithOverriddenResolvers } from '../../../utilities/testing';
import { CreateAdminApiKeyDrawer } from './create-admin-api-key-drawer';

const ContentWrapper = styled.div`
  margin-top: 100px;
  text-align: center;
`;

const TextWrapper = styled.div`
  animation:blinkingText 0.8s infinite;

  @keyframes blinkingText{
    0%{
      color: #000;
      padding-right: 10px;
    }
    25%{ color: transparent; }
    50%{
      color: #000;
      padding-left: 10px;
    }
    75%{ color: transparent; }
    100%{
      color: #000;
      padding-right: 10px;
    }
  }
`;

const resolverData: {
  mutationResponse: CreateAdminApiKeyMutation['createAdminApiKey'],
} = {
  mutationResponse: {
    id: '12345',
    name: 'Test API key',
    token: 'asdgasg-12lkkja-asdgklkjg-1lkja',
    errors: null,
  } as any,
};

const client = createApolloClientWithOverriddenResolvers({
  Mutation: {
    createAdminApiKey: async () => resolverData.mutationResponse,
  },
});

const testOrgId = 'DUMMY_ORG_ID';

const StorybookExample = () => {
  class TestComp extends React.Component<{}, { isDrawerOpen: boolean }> {
    public state = { isDrawerOpen: false };

    public render() {
      return (
        <StorybookAnalyticsClientProvider>
          <ApolloClientStorybookWrapper client={client}>
            <ApolloProvider client={client}>
              <IntlProvider locale="en">
                <FlagProvider>
                  <Router initialEntries={[{ pathname: `/${testOrgId}` }]} >
                    <AkPage>
                      <Route path="/:orgId" render={this.renderRoute} />
                    </AkPage>
                  </Router>
                </FlagProvider>
              </IntlProvider>
            </ApolloProvider>
          </ApolloClientStorybookWrapper>
        </StorybookAnalyticsClientProvider>
      );
    }

    private renderRoute = () => {
      return (
        <React.Fragment>
          <ContentWrapper>
            <TextWrapper>
              Click Here ⤵
            </TextWrapper>
            <AkButton onClick={this.openDrawer} appearance="primary">
              Create API token
            </AkButton>
          </ContentWrapper>

          <FocusedTask
            isOpen={this.state.isDrawerOpen}
            onClose={this.closeDrawer}
          >
            <CreateAdminApiKeyDrawer onCloseDrawer={this.closeDrawer} />
          </FocusedTask>
        </React.Fragment>
      );
    }

    private openDrawer = () => this.setState({ isDrawerOpen: true });
    private closeDrawer = () => this.setState({ isDrawerOpen: false });
  }

  return <TestComp />;
};

storiesOf('Organization|Admin API/Create API Key Drawer', module)
  .add('Default', withoutWhaleShot(() => {
    return (
      <StorybookExample />
    );
  },
  ))
;
