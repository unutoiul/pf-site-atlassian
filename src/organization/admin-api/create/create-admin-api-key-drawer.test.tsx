// tslint:disable jsx-use-translation-function
import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import { createMockIntlContext, createMockIntlProp } from '../../../utilities/testing';
import { AdminApiKeyDetails } from './admin-api-key-details';
import { CreateAdminApiKey } from './create-admin-api-key';
import { CreateAdminApiKeyDrawerImpl } from './create-admin-api-key-drawer';

describe('Admin API create API key drawer', () => {

  const shallowCreateAdminApiKeyDrawer = () => {
    return shallow(
      <CreateAdminApiKeyDrawerImpl
        match={{ url: '', path: '', isExact: true, params: { orgId: '1234' } }}
        onCloseDrawer={sinon.spy()}
        location={{} as any}
        history={{} as any}
        intl={createMockIntlProp()}
      />,
      createMockIntlContext(),
    );
  };

  it('should render create API key page correctly', () => {
    const wrapper = shallowCreateAdminApiKeyDrawer();
    const createPage = wrapper.find(CreateAdminApiKey) as any;
    expect(createPage.exists()).to.equal(true);
  });

  it('should render api key details page correctly', () => {
    const wrapper = shallowCreateAdminApiKeyDrawer();
    wrapper.setState({ page: 'admin-api-key-details' });
    wrapper.update();
    const createPage = wrapper.find(CreateAdminApiKey) as any;
    expect(createPage.exists()).to.equal(false);
    const detailsPage = wrapper.find(AdminApiKeyDetails) as any;
    expect(detailsPage.exists()).to.equal(true);
  });

});
