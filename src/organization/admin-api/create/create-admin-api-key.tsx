import { ApolloQueryResult } from 'apollo-client';
import * as React from 'react';
import { graphql } from 'react-apollo';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import AkButton from '@atlaskit/button';
import { FieldTextStateless as AkTextFieldStateless } from '@atlaskit/field-text';
import { gridSize as akGridSize } from '@atlaskit/theme';

import { adminApiCancelCreateAdminApiKeyEventData, adminApiCreateAdminApiKeyEventData, adminApiCreateKeyScreenEvent, adminApiKeyCreatedTrackData, AnalyticsClientProps, ScreenEventSender, withAnalyticsClient } from 'common/analytics';
import { ButtonGroup as ButtonGroupBase } from 'common/button-group';
import { createErrorIcon, errorFlagMessages } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';

import { CreateAdminApiKeyMutation, CreateAdminApiKeyMutationVariables } from '../../../schema/schema-types';
import createAdminApiKeyMutation from './create-admin-api-key.mutation.graphql';

const messages = defineMessages({
  title: {
    id: 'organization.adminApi.createAdminApiKeyDrawer.createAdminApiKeyPage.title',
    defaultMessage: 'Create an API key',
    description: 'The title of a page for creating an API key',
  },
  inputLabel: {
    id: 'organization.adminApi.createAdminApiKeyDrawer.createAdminApiKeyPage.inputLabel',
    defaultMessage: 'Name',
    description: 'The label for an field for inputting a name for an API key',
  },
  inputPrompt: {
    id: 'organization.adminApi.createAdminApiKeyDrawer.createAdminApiKeyPage.inputPrompt',
    defaultMessage: 'Name of your key',
    description: 'Placeholder text prompting users to enter the name of their API key',
  },
  createButton: {
    id: 'organization.adminApi.createAdminApiKeyDrawer.createAdminApiKeyPage.createButton',
    defaultMessage: 'Create',
    description: 'Text for a button that creates an API key',
  },
  backButton: {
    id: 'organization.adminApi.createAdminApiKeyDrawer.createAdminApiKeyPage.backButton',
    defaultMessage: 'Back',
    description: 'Text for a button that cancels and exits this dialog',
  },
  createAdminApiKeyErrorDescription: {
    id: 'organization.adminApi.createAdminApiKeyDrawer.createAdminApiKeyPage.error.description',
    defaultMessage: 'There was an issue creating your API key. Try again later.',
    description: 'Description for an error dialog that shows when there was a problem creating an API key',
  },
});

const ButtonGroup = styled(ButtonGroupBase) `
  margin-top: ${akGridSize() * 4}px;
`;

interface State {
  isLoading: boolean;
  apiKeyName: string;
}

interface OwnProps {
  orgId: string;
  goToDetailsPage(name: string, apiKey: string): void;
  onCloseDrawer(): void;
}

interface CreateAdminApiKeyMutationProps {
  createAdminApiKey(orgId: string, name: string): Promise<false | ApolloQueryResult<CreateAdminApiKeyMutation>>;
}

type CreateAdminApiKeyProps = CreateAdminApiKeyMutationProps & OwnProps & InjectedIntlProps & FlagProps & AnalyticsClientProps;

export class CreateAdminApiKeyImpl extends React.Component<CreateAdminApiKeyProps, State> {
  public state: Readonly<State> = { isLoading: false, apiKeyName: '' };

  public render() {
    const { intl: { formatMessage } } = this.props;
    const { isLoading, apiKeyName } = this.state;

    return (
      <ScreenEventSender event={adminApiCreateKeyScreenEvent()} >
        <FormattedMessage {...messages.title} tagName="h1" />
        <form onSubmit={this.onSubmit}>
          <AkTextFieldStateless
            placeholder={formatMessage(messages.inputPrompt)}
            onChange={this.setTextFieldState}
            label={formatMessage(messages.inputLabel)}
            required={true}
            disabled={isLoading}
            shouldFitContainer={true}
            id="create-admin-api-key-name"
            autoFocus={true}
            autoComplete="off"
            value={apiKeyName}
          />
          <ButtonGroup alignment="right">
            <AkButton
              appearance="default"
              onClick={this.onCancel}
            >
              <FormattedMessage {...messages.backButton} />
            </AkButton>
            <AkButton
              id="create-admin-api-key-button"
              appearance="primary"
              type="submit"
              isDisabled={apiKeyName === ''}
              isLoading={isLoading}
            >
              <FormattedMessage {...messages.createButton} />
            </AkButton>
          </ButtonGroup>
        </form>
      </ScreenEventSender>
    );
  }

  private setTextFieldState = (event: React.FormEvent<HTMLInputElement>): void => {
    this.setState({ apiKeyName: (event.target as HTMLInputElement).value });
  };

  private onCancel = () => {
    const { onCloseDrawer, orgId, analyticsClient } = this.props;

    analyticsClient.sendUIEvent({
      orgId,
      data: adminApiCancelCreateAdminApiKeyEventData(),
    });

    onCloseDrawer();
  }

  private onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (!this.isFormValid()) {
      return;
    }

    const { analyticsClient, orgId, goToDetailsPage, createAdminApiKey, showFlag, intl: { formatMessage } } = this.props;
    const { apiKeyName } = this.state;
    this.setState({ isLoading: true });

    analyticsClient.sendUIEvent({
      orgId,
      data: adminApiCreateAdminApiKeyEventData(),
    });

    try {
      const result = await createAdminApiKey(orgId, apiKeyName);
      if (!result) {
        throw new Error('createAdminApiKey returned bad result');
      }
      analyticsClient.sendTrackEvent({
        orgId,
        data: adminApiKeyCreatedTrackData(),
      });
      const { name, token: apiKey } = result.data.createAdminApiKey;
      goToDetailsPage(name, apiKey);
    } catch (error) {
      this.setState({ isLoading: false });
      showFlag({
        autoDismiss: true,
        icon: createErrorIcon(),
        id: `organization.adminApi.createAdminApiKeyDrawer.createAdminApiKeyPage.error.${Date.now()}`,
        title: formatMessage(errorFlagMessages.title),
        description: formatMessage(messages.createAdminApiKeyErrorDescription),
      });
    }

  }

  private isFormValid() {
    return this.state.apiKeyName.length > 0;
  }
}

const withCreateAdminApiKeyMutation = graphql<OwnProps, CreateAdminApiKeyMutation, CreateAdminApiKeyMutationVariables, CreateAdminApiKeyMutationProps>(
  createAdminApiKeyMutation,
  {
    props: ({ mutate }) => ({
      createAdminApiKey: async (orgId: string, name: string) => mutate instanceof Function && mutate({
        variables: { orgId, name },
      }),
    }),
  },
);

export const CreateAdminApiKey = withCreateAdminApiKeyMutation(
  injectIntl(
    withFlag(
      withAnalyticsClient(
        CreateAdminApiKeyImpl,
      ),
    ),
  ),
);
