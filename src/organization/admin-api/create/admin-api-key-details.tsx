import * as React from 'react';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import AkButton from '@atlaskit/button';
import AkWarningIcon from '@atlaskit/icon/glyph/warning';
import { gridSize as akGridSize } from '@atlaskit/theme';
import { akColorN300, akColorY300 } from '@atlaskit/util-shared-styles';

import {
  adminApiApiKeyDetailsScreenEvent,
  adminApiCopyApiKeyEventData,
  adminApiCopyOrgIdEventData,
  adminApiFinishCreateApiKeyEventData,
  AnalyticsClientProps,
  ScreenEventSender,
  withAnalyticsClient,
} from 'common/analytics';
import { ButtonGroup as ButtonGroupBase } from 'common/button-group';
import { links } from 'common/link-constants';
import { TextCopy } from 'common/text-copy';

const messages = defineMessages({
  title: {
    id: 'organization.adminApi.createAdminApiKeyDrawer.keyDetails.title',
    defaultMessage: '{apiKeyName}',
    description: 'Title of the API key details page, apiKeyName is a variable that will be replaced with the name of an API key',
  },
  description: {
    id: 'organization.adminApi.createAdminApiKeyDrawer.keyDetails.description',
    defaultMessage: 'Use this key to access your organization via the Atlassian Admin API. See the {developerDocumentationLink} to learn more.',
    description: 'Instructions for how to use an API key, developerDocumentationLink will be replaced with text from organization.adminApi.createAdminApiKeyDrawer.keyDetails.developerDocumentation',
  },
  developerDocumentation: {
    id: 'organization.adminApi.createAdminApiKeyDrawer.keyDetails.developerDocumentation',
    defaultMessage: 'developer documentation',
    description: 'link for developer documentation, will be inserted into organization.adminApi.createAdminApiKeyDrawer.keyDetails.description',
  },
  orgIdLabel: {
    id: 'organization.adminApi.createAdminApiKeyDrawer.keyDetails.orgIdLabel',
    defaultMessage: 'Organization ID',
    description: 'Label of a field that contains an ID for and organization/company',
  },
  apiKeyValueLabel: {
    id: 'organization.adminApi.createAdminApiKeyDrawer.keyDetails.apiKeyValueLabel',
    defaultMessage: 'API key',
    description: 'Label of a field that contains an API key',
  },
  copyButtonLabel: {
    id: 'organization.adminApi.createAdminApiKeyDrawer.keyDetails.copyButtonLabel',
    defaultMessage: 'Copy',
    description: 'Text of a button that will copy the value of a field when clicked',
  },
  warningIconLabel: {
    id: 'organization.adminApi.createAdminApiKeyDrawer.keyDetails.warning.label',
    defaultMessage: 'warning',
    description: 'A text label for a warning icon',
  },
  warningDescription: {
    id: 'organization.adminApi.createAdminApiKeyDrawer.keyDetails.warning.description',
    defaultMessage: 'Store this key somewhere safe. It will not be visible again once you close this page.',
  },
  doneButton: {
    id: 'organization.adminApi.createAdminApiKeyDrawer.keyDetails.doneButton',
    defaultMessage: 'Done',
    description: 'Text of a button that will close this page when clicked',
  },
});

const Description = styled.div`
  margin-top: ${akGridSize() * 2}px;
  margin-bottom: ${akGridSize() - 4}px;
`;

const WarningMessage = styled.div`
  display: flex;
  margin-top: ${akGridSize}px;
  color: ${akColorN300};

  span:first-child {
    margin-right: ${akGridSize}px;
  }
`;

const ButtonGroup = styled(ButtonGroupBase) `
  margin-top: ${akGridSize() * 4}px;
`;

interface OwnProps {
  apiKeyName: string;
  apiKey: string;
  orgId: string;
  onCloseDrawer(): void;
}

const DeveloperDocumentationLink = (
  <AkButton
    appearance="link"
    spacing="none"
    href={links.external.adminApiDocumentation}
  >
    <FormattedMessage {...messages.developerDocumentation} />
  </AkButton>
);

export class AdminApiKeyDetailsImpl extends React.Component<OwnProps & InjectedIntlProps & AnalyticsClientProps> {

  public render() {
    const { intl: { formatMessage }, apiKey, apiKeyName, orgId } = this.props;

    return (
      <ScreenEventSender event={adminApiApiKeyDetailsScreenEvent()} >
        <FormattedMessage {...messages.title} tagName="h1" values={{ apiKeyName }} />
        <Description>
          <FormattedMessage {...messages.description} values={{ developerDocumentationLink: DeveloperDocumentationLink }} />
        </Description>
        <TextCopy
          label={formatMessage(messages.orgIdLabel)}
          value={orgId}
          analyticsUIEvent={{ orgId, data: adminApiCopyOrgIdEventData() }}
        />
        <TextCopy
          label={formatMessage(messages.apiKeyValueLabel)}
          value={apiKey}
          analyticsUIEvent={{ orgId, data: adminApiCopyApiKeyEventData() }}
        />
        <WarningMessage>
          <AkWarningIcon size="medium" primaryColor={akColorY300} label={formatMessage(messages.warningIconLabel)} />
          <FormattedMessage {...messages.warningDescription} />
        </WarningMessage>
        <ButtonGroup alignment="right">
          <AkButton
            id="admin-api-key-details-done-button"
            appearance="primary"
            type="button"
            onClick={this.onDone}
          >
            <FormattedMessage {...messages.doneButton} />
          </AkButton>
        </ButtonGroup>
      </ScreenEventSender>
    );
  }

  private onDone = () => {
    const { onCloseDrawer, orgId, analyticsClient } = this.props;

    analyticsClient.sendUIEvent({
      orgId,
      data: adminApiFinishCreateApiKeyEventData(),
    });

    onCloseDrawer();
  }
}

export const AdminApiKeyDetails =
  injectIntl<OwnProps>(
    withAnalyticsClient(
      AdminApiKeyDetailsImpl,
    ),
  )
;
