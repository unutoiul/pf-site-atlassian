import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import { AdminApiKeysQuery, RevokeAdminApiKeyMutation } from 'src/schema/schema-types';

import AkButton from '@atlaskit/button';

import {
  adminApiKeyRevokedTrackData,
  adminApiRevokeApiKeyEventData,
  AnalyticsClientProps,
  withAnalyticsClient,
} from 'common/analytics';

import { createErrorIcon, createSuccessIcon, errorFlagMessages } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';

import { RevokeAdminApiKeyDialog } from './revoke-admin-api-key-dialog';
import revokeAdminApiKeyMutation from './revoke-admin-api-key.mutation.graphql';

const messages = defineMessages({
  revoke: {
    id: 'organization.admin.api.revoke',
    defaultMessage: 'Revoke',
    description: 'Link to revoke an API key',
  },
  revokeSuccess: {
    id: 'organization.admin.api.revoke.success',
    defaultMessage: '{apiKeyName} revoked',
    description: 'Success message that is shown when an API key is revoked',
  },
  revokeFailure: {
    id: 'organization.admin.api.revoke.failure',
    defaultMessage: 'There was an issue revoking {apiKeyName}. Try again later.',
    description: 'An error message that is shown when there is a failure revoking an API key',
  },
});

type AdminApiKey = AdminApiKeysQuery['organization']['adminApiKeys']['tokens'][0];

interface OwnProps {
  apiKey: AdminApiKey;
  orgId: string;
  onRevoke(): void;
}

interface State {
  isDialogOpen: boolean;
  isMutationLoading: boolean;
}

type RevokeAdminApiKeyButtonProps = ChildProps<OwnProps, RevokeAdminApiKeyMutation> & FlagProps & AnalyticsClientProps & InjectedIntlProps;

export class RevokeAdminApiKeyButtonImpl extends React.Component<RevokeAdminApiKeyButtonProps, State> {
  public readonly state: Readonly<State> = {
    isDialogOpen: false,
    isMutationLoading: false,
  };

  public render() {
    const { isMutationLoading, isDialogOpen } = this.state;
    const { apiKey } = this.props;

    return (
      <React.Fragment>
        <AkButton
          id={`api-key-revoke-button-${apiKey.id}`} // might be used by pollinator, check before modifying
          appearance="link"
          spacing="none"
          onClick={this.showRevokeApiKeyDialog}
        >
          <FormattedMessage {...messages.revoke} />
        </AkButton>
        <RevokeAdminApiKeyDialog
          isOpen={isDialogOpen}
          isRevokeLoading={isMutationLoading}
          apiKeyName={apiKey.name}
          onDismiss={this.hideRevokeApiKeyDialog}
          onRevoke={this.revokeApiKey}
        />
      </React.Fragment>
    );
  }

  private showRevokeApiKeyDialog = () => {
    this.setRevokeApiKeyDialogOpenState(true);
  }

  private hideRevokeApiKeyDialog = () => {
    this.setRevokeApiKeyDialogOpenState(false);
  }

  private setRevokeApiKeyDialogOpenState = (isDialogOpen: boolean) => {
    this.setState({ isDialogOpen });
  }

  private revokeApiKey = async () => {
    const { analyticsClient, apiKey, mutate, orgId } = this.props;

    analyticsClient.sendUIEvent({
      orgId,
      data: adminApiRevokeApiKeyEventData(),
    });

    if (typeof mutate !== 'function') {
      return;
    }

    this.setState({ isMutationLoading: true });

    try {
      await mutate({
        variables: {
          orgId,
          apiKeyId: apiKey.id,
        },
      });
      analyticsClient.sendTrackEvent({
        orgId,
        data: adminApiKeyRevokedTrackData(),
      });
      this.showSuccessFlag(apiKey.name);
    } catch (error) {
      this.showErrorFlag(apiKey.name);
    } finally {
      this.setState({ isMutationLoading: false });
      this.props.onRevoke();
      this.hideRevokeApiKeyDialog();
    }
  }

  private showSuccessFlag = (apiKeyName: string) => {
    const { showFlag, intl: { formatMessage } } = this.props;
    showFlag({
      autoDismiss: true,
      icon: createSuccessIcon(),
      id: `admin.api.revoke.key.success.${Date.now()}`,
      title: formatMessage(messages.revokeSuccess, { apiKeyName }),
    });
  }

  private showErrorFlag = (apiKeyName: string) => {
    const { showFlag, intl: { formatMessage } } = this.props;
    showFlag({
      autoDismiss: true,
      icon: createErrorIcon(),
      id: `admin.api.revoke.key.failure.${Date.now()}`,
      title: formatMessage(errorFlagMessages.title),
      description: formatMessage(messages.revokeFailure, { apiKeyName }),
    });
  }
}

const withRevokeAdminApiKeyMutation = graphql<OwnProps, RevokeAdminApiKeyMutation>(
  revokeAdminApiKeyMutation,
);

export const RevokeAdminApiKeyButton =
withRevokeAdminApiKeyMutation(
  withFlag(
    injectIntl(
      withAnalyticsClient(
        RevokeAdminApiKeyButtonImpl,
      ),
    ),
  ),
);
