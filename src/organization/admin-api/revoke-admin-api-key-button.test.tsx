import { expect } from 'chai';
import { shallow } from 'enzyme';
import { Exception } from 'handlebars';
import * as React from 'react';
import * as sinon from 'sinon';

import AkButton from '@atlaskit/button';

import { createMockIntlProp } from '../../utilities/testing';
import { RevokeAdminApiKeyButtonImpl } from './revoke-admin-api-key-button';
import { RevokeAdminApiKeyDialog } from './revoke-admin-api-key-dialog';

describe('RevokeAdminApiKeyButton', () => {

  const sandbox = sinon.sandbox.create();
  const successfulMutation = Promise.resolve(true);

  let mutateMock;
  let showFlagSpy;
  let revokeKeySpy;

  beforeEach(() => {
    mutateMock = sandbox.stub().returns(successfulMutation);
    showFlagSpy = sandbox.spy();
    revokeKeySpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  const shallowRevokeAdminApiKeyButton = () => {
    const apiKey = {
      __typename: 'AdminApiKey',
      id: '12345',
      name: 'Scripts',
      createdBy: {
        __typename: 'Member',
        id: '12345',
        displayName: 'Happy Corgi',
        primaryEmail: 'happy@corgi.com',
      },
      creationDate: '2018-08-02T18:07:30.610117Z',
      expirationDate: '2018-09-02T18:07:30.610117Z',
    };

    return shallow<RevokeAdminApiKeyButtonImpl>((
      <RevokeAdminApiKeyButtonImpl
        apiKey={apiKey}
        orgId="test_org"
        showFlag={showFlagSpy}
        hideFlag={() => undefined}
        analyticsClient={{
          sendUIEvent: () => undefined,
          sendTrackEvent: () => undefined,
        } as any}
        intl={createMockIntlProp()}
        mutate={mutateMock}
        onRevoke={revokeKeySpy}
      />
    ));
  };

  it('should render', () => {
    const wrapper = shallowRevokeAdminApiKeyButton();
    const button = wrapper.find(AkButton);

    expect(button.exists()).to.equal(true);
  });

  it('should open dialog on button click', () => {
    const wrapper = shallowRevokeAdminApiKeyButton();
    const button = wrapper.find(AkButton);

    button.simulate('click');
    wrapper.update();

    expect(wrapper.state().isDialogOpen).to.equal(true);
  });

  it('should display success flag after mutation', async () => {
    const wrapper = shallowRevokeAdminApiKeyButton();
    const dialog = wrapper.find(RevokeAdminApiKeyDialog);

    await dialog.props().onRevoke();
    const showFlagArgs = showFlagSpy.getCall(0).args[0];

    expect(showFlagSpy.called).to.equal(true);
    expect(showFlagArgs.id).to.contain('success');
  });

  it('should display failure flag after failed mutation', async () => {
    mutateMock.throws(new Exception(''));
    const wrapper = shallowRevokeAdminApiKeyButton();
    const dialog = wrapper.find(RevokeAdminApiKeyDialog);

    await dialog.props().onRevoke();
    const showFlagArgs = showFlagSpy.getCall(0).args[0];

    expect(showFlagSpy.called).to.equal(true);
    expect(showFlagArgs.id).to.contain('failure');
  });
});
