import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import * as React from 'react';
import { MemoryRouter } from 'react-router';
import * as sinon from 'sinon';
import { AdminApiKeysQuery } from 'src/schema/schema-types';
import { ThemeProvider } from 'styled-components';

import AkDynamicTableStateless from '@atlaskit/dynamic-table';
import AkSpinner from '@atlaskit/spinner';

import { FlagProvider } from 'common/flag';

import { EmptyTableView } from 'common/table';

import { GenericError } from 'common/error';

import { createMockContext, waitUntil } from '../../utilities/testing';
import { rowsPerPage } from './admin-api-constants';
import { AdminApiTableImpl } from './admin-api-table';
import { RevokeAdminApiKeyButton } from './revoke-admin-api-key-button';

type Token = AdminApiKeysQuery['organization']['adminApiKeys']['tokens'][0];

describe('Admin API table', () => {

  const sandbox = sinon.sandbox.create();
  let onQueryMock: sinon.SinonStub;
  let whenDrawerClosedCallback: () => void;
  const whenDrawerClosedMock = (callback) => {
    whenDrawerClosedCallback = callback;
  };

  beforeEach(() => {
    onQueryMock = sandbox.stub();
  });

  afterEach(() => {
    sandbox.restore();
  });

  const mockToken = (id: string = '12345'): Token => ({
    __typename: 'AdminApiKey',
    id,
    name: 'Happy Corgi token',
    createdBy: {
      __typename: 'Member',
      id: '1234',
      displayName: 'Happy Corgi',
      primaryEmail: 'happy@corgi.com',
    },
    creationDate: '2018-08-02T18:07:30.610117Z',
    expirationDate: '2018-09-02T18:07:30.610117Z',
  });

  const mockOrgData = ({ tokens = [mockToken()], scrollId = null }: { tokens?: Token[], scrollId?: string | null } = {}): AdminApiKeysQuery => ({
    organization: {
      __typename: 'Organization',
      id: 'AdminApiPageOrgId',
      adminApiKeys: {
        __typename: 'AdminApiKeyCollection',
        scrollId,
        tokens,
      },
    },
  });

  const mockOrgDataPage = ({ start, stop, scrollId = null }: { start: number, stop: number, scrollId: string | null }) => {
    const tokens: Token[] = [];
    for (let i = start; i <= stop; ++i) {
      tokens.push(mockToken(i.toString()));
    }

    return mockOrgData({ tokens, scrollId });
  };

  const mountAdminApiTableTest = () => {
    const client = {
      query: onQueryMock,
    };

    return mount((
      <ThemeProvider theme={{}}>
        <MemoryRouter>
          <FlagProvider>
            <AdminApiTableImpl
              orgId="AdminApiPageOrgId"
              client={client as any}
              whenDrawerClosed={whenDrawerClosedMock as any}
            />
          </FlagProvider>
        </MemoryRouter>
      </ThemeProvider>),
      createMockContext({ intl: true, apollo: true }),
    );
  };

  const shallowAdminApiTableTest = () => {
    const client = {
      query: onQueryMock,
    };

    return shallow<AdminApiTableImpl>((
      <AdminApiTableImpl
        orgId="AdminApiPageOrgId"
        client={client as any}
        whenDrawerClosed={whenDrawerClosedMock as any}
      />),
      createMockContext({ intl: true, apollo: true }),
    );
  };

  const waitForTableLoad = async (wrapper) => {
    let table;
    await waitUntil(() => {
      table = wrapper.update().find(AkDynamicTableStateless);

      return table.props().isLoading === false;
    });

    return table;
  };

  it('should render loaded table', async () => {
    onQueryMock.resolves({ data: mockOrgData() });
    const wrapper = mountAdminApiTableTest();

    const table = await waitForTableLoad(wrapper);

    expect(table.exists()).to.equal(true);
    expect(table.props().rows).to.have.length(1);
  });

  it('should render empty state', async () => {
    onQueryMock.resolves({ data: mockOrgData({ tokens: [] }) });
    const wrapper = mountAdminApiTableTest();
    await waitForTableLoad(wrapper);

    const emptyTableView = wrapper.find(EmptyTableView);

    expect(emptyTableView.exists()).to.equal(true);
  });

  it('should render loading', () => {
    onQueryMock.returns(new Promise(() => undefined));
    const wrapper = mountAdminApiTableTest();

    const spinner = wrapper.find(AkSpinner);

    expect(spinner.exists()).to.equal(true);
  });

  it('should render error', async () => {
    onQueryMock.rejects(new Error(''));
    const wrapper = mountAdminApiTableTest();
    await waitForTableLoad(wrapper);

    const genericError = wrapper.find(GenericError);

    expect(genericError.exists()).to.equal(true);
  });

  it('should load second page', async () => {
    onQueryMock.onCall(0).resolves({ data: mockOrgDataPage({ start: 1, stop: rowsPerPage, scrollId: 'abcd' }) });
    onQueryMock.onCall(1).resolves({ data: mockOrgDataPage({ start: rowsPerPage + 1, stop: 25, scrollId: null }) });
    const wrapper = shallowAdminApiTableTest();
    let table = await waitForTableLoad(wrapper);

    table.props().onSetPage(2);
    table = await waitForTableLoad(wrapper);

    expect(onQueryMock.callCount).to.equal(2);
    expect(onQueryMock.firstCall.args[0].variables.scrollId).to.equal(null);
    expect(onQueryMock.lastCall.args[0].variables.scrollId).to.equal('abcd');
    expect(table.props().rows).to.have.length(25);
  });

  it('should not refetch data if going back to a previously loaded page', async () => {
    onQueryMock.onCall(0).resolves({ data: mockOrgDataPage({ start: 1, stop: rowsPerPage, scrollId: 'abcd' }) });
    onQueryMock.onCall(1).resolves({ data: mockOrgDataPage({ start: rowsPerPage + 1, stop: 25, scrollId: null }) });
    const wrapper = shallowAdminApiTableTest();
    let table = await waitForTableLoad(wrapper);

    table.props().onSetPage(2);
    table = await waitForTableLoad(wrapper);
    table.props().onSetPage(1);
    table = await waitForTableLoad(wrapper);

    expect(onQueryMock.callCount).to.equal(2);
  });

  it('should show add extra item to table if there are more results', async () => {
    onQueryMock.onCall(0).resolves({ data: mockOrgDataPage({ start: 1, stop: rowsPerPage, scrollId: 'abcd' }) });
    const wrapper = shallowAdminApiTableTest();
    const table = await waitForTableLoad(wrapper);

    expect(table.props().rows).to.have.length(21);
  });

  it('should not add extra item if no more results', async () => {
    onQueryMock.onCall(0).resolves({ data: mockOrgDataPage({ start: 1, stop: rowsPerPage, scrollId: null }) });
    const wrapper = mountAdminApiTableTest();
    const table = await waitForTableLoad(wrapper);

    expect(table.props().rows).to.have.length(20);
  });

  it('should reload after revoke', async () => {
    onQueryMock.resolves({ data: mockOrgDataPage({ start: 1, stop: 20, scrollId: null }) });
    const wrapper = mountAdminApiTableTest();
    const table = await waitForTableLoad(wrapper);

    wrapper.find(RevokeAdminApiKeyButton).first().props().onRevoke();
    await waitForTableLoad(wrapper);

    expect(onQueryMock.callCount).to.equal(2);
    expect(table.props().rows).to.have.length(20);
  });

  it('should reload after create', async () => {
    onQueryMock.resolves({ data: mockOrgDataPage({ start: 1, stop: rowsPerPage, scrollId: null }) });
    const wrapper = shallowAdminApiTableTest();
    let table = await waitForTableLoad(wrapper);

    whenDrawerClosedCallback();
    table = await waitForTableLoad(wrapper);

    expect(onQueryMock.callCount).to.equal(2);
    expect(table.props().rows).to.have.length(20);
  });

});
