import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { AdminApiKeysQuery } from 'src/schema/schema-types';

import AkPage from '@atlaskit/page';

import withDrawerCloseSubscriberQuery from 'common/drawer/with-drawer-close-subscriber.query.graphql';
import { FlagProvider } from 'common/flag';

import { createApolloClient } from '../../apollo-client';
import { ApolloClientStorybookWrapper } from '../../utilities/storybooks';
import adminApiKeysQuery from './admin-api-keys.query.graphql';
import { AdminApiPageImpl } from './admin-api-page';

const drawerWriteQuery = {
  query: withDrawerCloseSubscriberQuery,
  data: {
    ui: {
      __typename: 'UI',
      navigation: {
        __typename: 'Navigation',
        activeDrawer: 'NONE',
      },
    },
  },
};

const client = createApolloClient();
const adminApiPageOrgId = 'test-org';

interface AdminApiStorybookProps {
  configureApolloClient?: ApolloClientStorybookWrapper['props']['configureClient'];
}

const StorybookAdminApiPage: React.SFC<AdminApiStorybookProps> = ({ configureApolloClient }) => (
  <ApolloClientStorybookWrapper client={client} configureClient={configureApolloClient}>
    <ApolloProvider client={client}>
      <IntlProvider locale="en">
        <FlagProvider>
          <BrowserRouter>
            <Switch location={{ pathname: `/o/${adminApiPageOrgId}/api`, search: '', state: undefined, hash: '' }}>
              <Route path="/o/:orgId/api">
                <AkPage>
                  <AdminApiPageImpl
                    match={{ params: { orgId: adminApiPageOrgId } } as any}
                    location={{} as any}
                    history={{} as any}
                    showDrawer={() => undefined}
                    analyticsClient={{} as any}
                  />
                </AkPage>
              </Route>
            </Switch>
          </BrowserRouter>
        </FlagProvider>
      </IntlProvider>
    </ApolloProvider>
  </ApolloClientStorybookWrapper>
);

storiesOf('Organization|Admin API/Admin API Page', module)
  .add('Default', () => {
    const configureClientForAdminApiPage = () => {
      const organization: AdminApiKeysQuery = {
        organization: {
          __typename: 'Organization',
          id: adminApiPageOrgId,
          adminApiKeys: {
            __typename: 'AdminApiKeyCollection',
            scrollId: null,
            tokens: [
              {
                __typename: 'AdminApiKey',
                id: '12345',
                name: 'Scripts',
                createdBy: {
                  __typename: 'Member',
                  id: '12345',
                  displayName: 'Happy Corgi',
                  primaryEmail: 'happy@corgi.com',
                },
                creationDate: '2018-08-02T18:07:30.610117Z',
                expirationDate: '2018-09-02T18:07:30.610117Z',
              },
              {
                __typename: 'AdminApiKey',
                id: '56789',
                name: 'User management',
                createdBy: {
                  __typename: 'Member',
                  id: '56789',
                  displayName: 'Sad Corgi',
                  primaryEmail: 'sad@corgi.com',
                },
                creationDate: '2018-08-02T18:07:30.610117Z',
                expirationDate: '2018-09-02T18:07:30.610117Z',
              },
            ],
          },
        },
      };

      client.writeQuery({
        query: adminApiKeysQuery,
        variables: {
          id: adminApiPageOrgId,
          scrollId: null,
        },
        data: organization,
      });

      client.writeQuery(drawerWriteQuery);
    };

    return <StorybookAdminApiPage configureApolloClient={configureClientForAdminApiPage} />;
  })
  .add('Empty', () => {
    const configureClientForAdminApiPage = () => {
      const organization: AdminApiKeysQuery = {
        organization: {
          __typename: 'Organization',
          id: adminApiPageOrgId,
          adminApiKeys: {
            __typename: 'AdminApiKeyCollection',
            scrollId: null,
            tokens: [],
          },
        },
      };

      client.writeQuery({
        query: adminApiKeysQuery,
        variables: {
          id: adminApiPageOrgId,
          scrollId: null,
        },
        data: organization,
      });

      client.writeQuery(drawerWriteQuery);
    };

    return <StorybookAdminApiPage configureApolloClient={configureClientForAdminApiPage} />;
  })
  .add('Error', () => {
    const configureClientForAdminApiPage = () => {
      // do nothing to cause an apollo error
    };

    return <StorybookAdminApiPage configureApolloClient={configureClientForAdminApiPage} />;
  });
