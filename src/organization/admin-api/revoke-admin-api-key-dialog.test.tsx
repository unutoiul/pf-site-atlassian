import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import AkButton from '@atlaskit/button';

import { createMockIntlContext } from '../../utilities/testing';
import { RevokeAdminApiKeyDialog, RevokeAdminApiKeyDialogProps } from './revoke-admin-api-key-dialog';

const mountRevokeAdminApiKeyDialog = (props: RevokeAdminApiKeyDialogProps) => {
  return mount<RevokeAdminApiKeyDialogProps, never>((
    <RevokeAdminApiKeyDialog
      {...props}
    />
  ), createMockIntlContext());
};

describe('Revoke Admin Api Key Dialog', () => {
  const sandbox = sinon.sandbox.create();
  let onDismissSpy: sinon.SinonSpy;
  let onRevokeSpy: sinon.SinonSpy;

  beforeEach(() => {
    onDismissSpy = sandbox.spy();
    onRevokeSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('should render an action button which calls the onRevoke prop', () => {
    const wrapper = mountRevokeAdminApiKeyDialog({
      isOpen: true,
      isRevokeLoading: false,
      apiKeyName: 'My key',
      onDismiss: onDismissSpy,
      onRevoke: onRevokeSpy,
    });

    const revokeButton = wrapper.find(AkButton).first();

    expect(onRevokeSpy.callCount).to.equal(0);
    revokeButton.simulate('click');
    expect(onRevokeSpy.callCount).to.equal(1);
  });

  it('should disable action button when isRevokeDisabled is true', () => {
    const wrapper = mountRevokeAdminApiKeyDialog({
      isOpen: true,
      isRevokeLoading: true,
      apiKeyName: 'My key',
      onDismiss: onDismissSpy,
      onRevoke: onRevokeSpy,
    });

    const revokeButton = wrapper.find(AkButton).first();

    expect(revokeButton.props().isLoading).to.equal(true);
  });

  it('should render a cancel button which calls the onDismiss prop', () => {
    const wrapper = mountRevokeAdminApiKeyDialog({
      isOpen: true,
      isRevokeLoading: false,
      apiKeyName: 'My key',
      onDismiss: onDismissSpy,
      onRevoke: onRevokeSpy,
    });

    const cancelButton = wrapper.find(AkButton).last();

    expect(onDismissSpy.callCount).to.equal(0);
    cancelButton.simulate('click');
    expect(onDismissSpy.callCount).to.equal(1);
  });

});
