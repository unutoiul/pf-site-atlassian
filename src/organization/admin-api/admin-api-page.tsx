import * as React from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';

import AkButton from '@atlaskit/button';

import {
  adminApiOpenCreateAdminApiKeyDrawerEventData,
  AnalyticsClientProps,
  apiKeysScreenEvent,
  ScreenEventSender,
  withAnalyticsClient,
} from 'common/analytics';
import { ShowDrawerProps, withShowDrawer } from 'common/drawer';
import { links } from 'common/link-constants';
import { ExternalLink } from 'common/navigation';
import { PageLayout } from 'common/page-layout';

import { OrgBreadcrumbs } from '../breadcrumbs';
import { OrgRouteProps } from '../routes';
import { AdminApiTable } from './admin-api-table';

const messages = defineMessages({
  title: {
    id: 'organization.admin.api.title',
    defaultMessage: 'API keys',
    description: 'Title for a page containing API key information',
  },
  description: {
    id: 'organization.admin.api.description',
    defaultMessage: `API keys allow you to manage your organization via the Atlassian Admin APIs. You can update organization settings and manage user accounts by making requests to HTTP endpoints. See the {learnMoreLink} to learn more.`,
    description: 'Description for how API keys work and where you can find more details. {learnMoreLink} is a variable name that should not be translated, it will be replaced with organization.admin.api.learnMore',
  },
  learnMore: {
    id: 'organization.admin.api.learnMore',
    defaultMessage: 'Admin API developer documentation',
    description: 'Name of administration API documentation link, it is surrounded by organization.admin.api.description',
  },
  createApiKey: {
    id: 'organization.admin.api.createApiKey',
    defaultMessage: 'Create API key',
    description: 'Text for a button that creates an API key',
  },
});

type AdminApiPageProps = AnalyticsClientProps & ShowDrawerProps & RouteComponentProps<OrgRouteProps>;

export class AdminApiPageImpl extends React.Component<AdminApiPageProps> {

  public render() {

    return (
      <ScreenEventSender event={apiKeysScreenEvent()}>
        <PageLayout
          breadcrumbs={<OrgBreadcrumbs />}
          title={<FormattedMessage {...messages.title} />}
          description={
            <FormattedMessage
              {...messages.description}
              values={{
                learnMoreLink: (
                  <ExternalLink
                    href={links.external.adminApiDocumentation}
                    hideReferrer={false}
                  >
                    <FormattedMessage {...messages.learnMore} />
                  </ExternalLink>
                ),
              }}
            />
          }
          isFullWidth={true}
          action={(
            <AkButton appearance="primary" onClick={this.showCreateApiKeyDrawer}>
              <FormattedMessage {...messages.createApiKey} />
            </AkButton>
          )}
        >
          <AdminApiTable
            orgId={this.props.match.params.orgId}
          />
        </PageLayout>
      </ScreenEventSender>
    );
  }

  private showCreateApiKeyDrawer = () => {
    const { match: { params: { orgId } }, analyticsClient } = this.props;
    analyticsClient.sendUIEvent({
      orgId,
      data: adminApiOpenCreateAdminApiKeyDrawerEventData(),
    });

    this.props.showDrawer('CREATE_ADMIN_API_KEY_DRAWER');
  }

}

export const AdminApiPage =
  withRouter(
    withShowDrawer(
      withAnalyticsClient(
        AdminApiPageImpl,
      ),
    ),
  )
;
