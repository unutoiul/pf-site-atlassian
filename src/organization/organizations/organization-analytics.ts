import { AnalyticsData, AnalyticsSubproduct, TenantType } from 'common/analytics/with-analytics';

const orgTenantType: TenantType = 'organizationId';
const orgSubProduct: AnalyticsSubproduct = 'organization';

export interface OrgAnalyticsData extends AnalyticsData {
  /**
   * Org ID
   */
  orgId?: string;
}

export function createOrgAnalyticsData(data: Partial<OrgAnalyticsData> & Pick<AnalyticsData, 'actionSubject' | 'actionSubjectId' | 'action'>): AnalyticsData {
  const { orgId, actionSubject, actionSubjectId, action, ...restProps } = data;

  return {
    subproduct: orgSubProduct,
    tenantType: orgTenantType,
    tenantId: orgId,
    actionSubject,
    action,
    actionSubjectId,
    ...restProps,
  };
}
