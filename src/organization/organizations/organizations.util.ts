import * as React from 'react';
import styled from 'styled-components';

import akLock from '@atlaskit/icon/glyph/lock-filled';
import { akColorB500 } from '@atlaskit/util-shared-styles';

import { EnforcementState, TwoStepVerificationEnforcementState } from './organization.prop-types';

const LockIcon = styled(akLock)`
  color: ${akColorB500};
`;

export function getEnforcementState(organization: OrganizationTwoStepVerificationEnforcementDate, currentDate?: Date): TwoStepVerificationEnforcementState {
  const { twoStepVerification: { dateEnforced } } = organization;

  const resolvedDate = currentDate === undefined ? new Date() : currentDate;

  if (dateEnforced == null) {
    return { state: EnforcementState.Unenforced };
  }
  if (resolvedDate < dateEnforced) {
    return { state: EnforcementState.Onboarding, dateEnforced };
  }
  if (resolvedDate >= dateEnforced) {
    return { state: EnforcementState.Enforced, dateEnforced };
  }

  return { state: EnforcementState.Unenforced };
}

interface OrganizationTwoStepVerificationEnforcementDate {
  twoStepVerification: {
    dateEnforced?: Date,
  };
}

export function hasAnyDomainVerified(domains): boolean {
  return domains.some((domain) => domain.verified);
}

export const createLockIcon = () => React.createElement(LockIcon, { label: '' });
