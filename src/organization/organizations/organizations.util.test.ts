import { expect } from 'chai';
import * as moment from 'moment';

import { EnforcementState } from './organization.prop-types';
import { getEnforcementState, hasAnyDomainVerified } from './organizations.util';

describe('getEnforcementState util', () => {
  const tomorrow = moment().add(1, 'days').toDate();
  const yesterday = moment().subtract(1, 'days').toDate();

  const organization = (date) => {
    return {
      twoStepVerification: {
        dateEnforced: date,
      },
    };
  };

  it('returns Uneforced when the enforcement date is not set', () => {
    const org = organization(null);
    const state = getEnforcementState(org);
    expect(state).to.deep.equal({ state: EnforcementState.Unenforced });
  });

  it('returns Onboarding when the enforcement date is in the future', () => {
    const org = organization(tomorrow);
    const state = getEnforcementState(org);
    expect(state).to.deep.equal({ state: EnforcementState.Onboarding, dateEnforced: tomorrow });
  });

  it('returns Enforced when the enforcement date is in the past', () => {
    const org = organization(yesterday);
    const state = getEnforcementState(org);
    expect(state).to.deep.equal({ state: EnforcementState.Enforced, dateEnforced: yesterday });
  });

  it('returns Enforced when the enforcement date and current date are equal', () => {
    const org = organization(tomorrow);
    const state = getEnforcementState(org, tomorrow);
    expect(state).to.deep.equal({ state: EnforcementState.Enforced, dateEnforced: tomorrow });
  });
});

describe('verified domains util', () => {
  it('should return true if there are verified domains', () => {
    const domains = [{ domain: 'acme.com', verified: true }, { domain: 'acme.net', verified: false }, { domain: 'acme.org', verified: false }];
    expect(hasAnyDomainVerified(domains)).to.equal(true);
  });

  it('should return false if there are no verified domains', () => {
    const domains = [{ domain: 'acme.com', verified: false }, { domain: 'acme.net', verified: false }, { domain: 'acme.org', verified: false }];
    expect(hasAnyDomainVerified(domains)).to.equal(false);
  });

  it('should return false if there are no domains', () => {
    const domains = [];
    expect(hasAnyDomainVerified(domains)).to.equal(false);
  });
});
