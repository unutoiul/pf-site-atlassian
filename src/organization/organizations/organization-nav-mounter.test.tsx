import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { sandbox as sinonSandbox, SinonSandbox, SinonSpy } from 'sinon';

import { NavigationMounterProps, NavItem } from 'common/navigation';

import { OrganizationNavMounterImpl } from './organization-nav-mounter';
import { OrgNavMounterProps } from './organization.prop-types';

describe('OrganizationNavMounter', () => {
  let registerRoutes: SinonSpy;
  let updateNavigationSection: SinonSpy;
  let removeNavigationSection: SinonSpy;
  let unregisterRoutes: SinonSpy;
  let sandbox: SinonSandbox;

  beforeEach(() => {
    sandbox = sinonSandbox.create();
    registerRoutes = sandbox.spy();
    updateNavigationSection = sandbox.spy();
    removeNavigationSection = sandbox.spy();
    unregisterRoutes = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  const genericNavMounterProps: NavigationMounterProps = {} as any;

  const movedOrgOnboardingDismissed = { loading: false, onboarding: { id: '1234', dismissed: true, __typename: '' } } as Partial<OrgNavMounterProps['movedOrgOnboarding']>;
  const movedOrgOnboardingNotDismissed = { loading: false, onboarding: { id: '1234', dismissed: false, __typename: '' } } as Partial<OrgNavMounterProps['movedOrgOnboarding']>;

  // Organization props passed by the GraphQL Schema
  const withOrgs = { currentOrgs: { loading: false, currentUser: { __typename: '', id: 'test-user', organizations: [{  __typename: '', id: 'foo', name: 'name', migrationState: null }] } } as Partial<OrgNavMounterProps['currentOrgs']> };
  const withNoOrgs = { currentOrgs: { loading: false, currentUser: { __typename: '', id: 'test-user', organizations: [] as any } } as Partial<OrgNavMounterProps['currentOrgs']> };

  const navSection = [{ id: 'example', title: { id: '1', defaultMessage: 'Example' }, links: [{ id: 'gsync' }, { id: 'passwordpolicy' }] } as NavItem];

  it('renders no children', () => {
    const component = shallow(
      <OrganizationNavMounterImpl {...{} as any}>
        <div className="child" />
      </OrganizationNavMounterImpl>,
      { disableLifecycleMethods: false },
    );
    expect(component.find('.child').length).to.equal(0);
  });

  const createDefaultOrganizationData = (overrideData?: Partial<OrganizationNavMounterImpl['props']>) => {
    return shallow(
      <OrganizationNavMounterImpl
        currentOrgs={withOrgs.currentOrgs as any}
        movedOrgOnboarding={movedOrgOnboardingNotDismissed as any}
        navigationSections={navSection}
        registerRoutes={registerRoutes}
        unregisterRoutes={unregisterRoutes}
        updateNavigationSection={updateNavigationSection}
        removeNavigationSection={removeNavigationSection}
        {...genericNavMounterProps}
        {...(overrideData || {})}
      />,
      { disableLifecycleMethods: false },
    );
  };

  describe('Does not update navigation or register routes', () => {

    it('when still loading organization data', () => {
      createDefaultOrganizationData({ currentOrgs: { loading: true } } as any);
      expect(registerRoutes.notCalled).to.equal(true);
      expect(updateNavigationSection.notCalled).to.equal(true);
    });

    it('when still bootstrapping nav sections', () => {
      createDefaultOrganizationData({ navigationSections: undefined } as any);
      expect(registerRoutes.notCalled).to.equal(true);
      expect(updateNavigationSection.notCalled).to.equal(true);
    });
  });

  describe('Admin hub onboarding', () => {
    it('should not throw while the flags are still loading', () => {
      const mutateSpy = sandbox.stub().returns(Promise.resolve());

      const wrapper = shallow(
        <OrganizationNavMounterImpl
          currentOrgs={withOrgs.currentOrgs as any}
          movedOrgOnboarding={movedOrgOnboardingNotDismissed as any}
          navigationSections={navSection}
          registerRoutes={registerRoutes}
          unregisterRoutes={unregisterRoutes}
          updateNavigationSection={updateNavigationSection}
          removeNavigationSection={removeNavigationSection}
          mutate={mutateSpy}
          {...genericNavMounterProps}
        />,
        { disableLifecycleMethods: false },
      );

      expect(() => wrapper.setProps({
      })).not.to.throw();
    });

    describe('With orgs', () => {
      let mutateSpy;

      beforeEach(() => {
        mutateSpy = sandbox.stub().returns(Promise.resolve());
      });

      it('should show a security link', () => {
        shallow(
          <OrganizationNavMounterImpl
            currentOrgs={withOrgs.currentOrgs as any}
            movedOrgOnboarding={movedOrgOnboardingNotDismissed as any}
            navigationSections={navSection}
            updateNavigationSection={updateNavigationSection}
            removeNavigationSection={removeNavigationSection}
            mutate={mutateSpy}
            {...genericNavMounterProps}
          />,
          { disableLifecycleMethods: false },
        );

        expect(updateNavigationSection.firstCall.args[0].id).to.equal('organizations');
        expect(updateNavigationSection.firstCall.args[0].links[0].id).to.equal('security');
      });

      it('should render a link to the onboarding page when it hasn\'t been seen yet', () => {
        shallow(
          <OrganizationNavMounterImpl
            currentOrgs={withOrgs.currentOrgs as any}
            movedOrgOnboarding={movedOrgOnboardingNotDismissed as any}
            navigationSections={navSection}
            registerRoutes={registerRoutes}
            unregisterRoutes={unregisterRoutes}
            updateNavigationSection={updateNavigationSection}
            removeNavigationSection={removeNavigationSection}
            mutate={mutateSpy}
            {...genericNavMounterProps}
          />,
          { disableLifecycleMethods: false },
        );

        expect(updateNavigationSection.firstCall.args[0].links[0].isExternal).to.equal(false);
        expect(mutateSpy.called).to.equal(true);
      });

      it('should render the external link to admin.atlassian.com if the onboarding has been seen', () => {
        shallow(
          <OrganizationNavMounterImpl
            currentOrgs={withOrgs.currentOrgs as any}
            movedOrgOnboarding={movedOrgOnboardingDismissed as any}
            navigationSections={navSection}
            registerRoutes={registerRoutes}
            unregisterRoutes={unregisterRoutes}
            updateNavigationSection={updateNavigationSection}
            removeNavigationSection={removeNavigationSection}
            mutate={mutateSpy}
            {...genericNavMounterProps}
          />,
          { disableLifecycleMethods: false },
        );

        expect(updateNavigationSection.firstCall.args[0].links[0].isExternal).to.equal(true);
        expect(updateNavigationSection.firstCall.args[0].links[0].path).to.equal('http://localhost:3001');
        expect(mutateSpy.called).to.equal(false);
      });
    });

    describe('Without orgs', () => {
      let mutateSpy;

      beforeEach(() => {
        mutateSpy = sandbox.stub().returns(Promise.resolve());
      });

      it('should show a security link', () => {
        shallow(
          <OrganizationNavMounterImpl
            currentOrgs={withNoOrgs.currentOrgs as any}
            movedOrgOnboarding={movedOrgOnboardingNotDismissed as any}
            navigationSections={navSection}
            registerRoutes={registerRoutes}
            unregisterRoutes={unregisterRoutes}
            updateNavigationSection={updateNavigationSection}
            removeNavigationSection={removeNavigationSection}
            mutate={mutateSpy}
            {...genericNavMounterProps}
          />,
          { disableLifecycleMethods: false },
        );

        expect(mutateSpy.called).to.equal(false);
        expect(updateNavigationSection.firstCall.args[0].id).to.equal('organizations');
        expect(updateNavigationSection.firstCall.args[0].links[0].id).to.equal('security');
        expect(updateNavigationSection.firstCall.args[0].links[0].lozenge).to.be.undefined();
      });

      it('it should render the value prop page', () => {
        shallow(
          <OrganizationNavMounterImpl
            currentOrgs={withNoOrgs.currentOrgs as any}
            movedOrgOnboarding={movedOrgOnboardingDismissed as any}
            navigationSections={navSection}
            registerRoutes={registerRoutes}
            unregisterRoutes={unregisterRoutes}
            updateNavigationSection={updateNavigationSection}
            removeNavigationSection={removeNavigationSection}
            mutate={mutateSpy}
            {...genericNavMounterProps}
          />,
          { disableLifecycleMethods: false },
        );

        expect(updateNavigationSection.firstCall.args[0].links[0].isExternal).to.equal(false);
        expect(updateNavigationSection.firstCall.args[0].links[0].path).to.equal('/admin/security');
        expect(mutateSpy.called).to.equal(false);
      });
    });
  });
});
