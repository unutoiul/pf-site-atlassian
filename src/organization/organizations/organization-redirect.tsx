import * as React from 'react';
import { graphql, OptionProps } from 'react-apollo';
import { Redirect, RouteComponentProps } from 'react-router';

import AkSpinner from '@atlaskit/spinner';

import { getConfig } from 'common/config';
import { OnboardingIdType, onboardingQuery } from 'common/onboarding';

import { OnboardingQuery, OnboardingQueryVariables } from '../../schema/schema-types';
import { util } from '../../utilities/redirect';

export interface DerivedProps {
  movedOrgOnboardingDismissed?: boolean;
}

const oldPath = '/admin';
const orgPath = '/o/';
const oldOrgPath = oldPath + orgPath;

function withOrganizationRedirectImpl(Component: React.ComponentType<DerivedProps>): React.ComponentClass<DerivedProps & RouteComponentProps<any>> {

  return class OrganizationRedirectImpl extends React.Component<DerivedProps & RouteComponentProps<any>> {

    public render() {
      const { movedOrgOnboardingDismissed, location: { pathname } } = this.props;

      const isOldOrgPath = pathname.startsWith(oldOrgPath);

      if (!isOldOrgPath) {
        return <Component {...this.props} />;
      }

      // when redirect happens outside of render method, render still completes, so we need to render the spinner until the redirect finishes
      if (this.isLoading(this.props) || this.shouldRedirectToNewOrgPage(this.props)) {
        return <AkSpinner />;
      }

      if (isOldOrgPath && !movedOrgOnboardingDismissed) {
        return <Redirect to="/admin/security/moved" />;
      }

      return <Component {...this.props} />;
    }

    public componentWillMount() {
      this.redirectToNewOrgPageIfNeeded(this.props);
    }

    public componentWillReceiveProps(nextProps: OrganizationRedirectImpl['props']) {
      this.redirectToNewOrgPageIfNeeded(nextProps);
    }

    private isLoading = (props: OrganizationRedirectImpl['props']) => {
      const { movedOrgOnboardingDismissed } = props;

      return movedOrgOnboardingDismissed === undefined;
    }

    private redirectToNewOrgPageIfNeeded = (props: OrganizationRedirectImpl['props']) => {
      if (!this.shouldRedirectToNewOrgPage(props)) {
        return;
      }

      const { pathname, search } = props.location;
      const adminHubUrl: string = pathname.slice(oldPath.length);

      util.redirect(getConfig().goToAdminLinkUrl + adminHubUrl + search);
    }

    private shouldRedirectToNewOrgPage = (props: OrganizationRedirectImpl['props']): boolean => {
      const { movedOrgOnboardingDismissed, location: { pathname } } = props;

      if (this.isLoading(props)) {
        return false;
      }

      if (pathname.startsWith(oldOrgPath) && movedOrgOnboardingDismissed) {
        return true;
      }

      return false;
    }
  };
}

const withMovedOrgOnboarding = graphql<RouteComponentProps<any>, OnboardingQuery, OnboardingQueryVariables, DerivedProps>(
  onboardingQuery,
  {
    options: {
      variables: {
        id: OnboardingIdType.MOVED_ORG,
      },
    },
    props: (props): DerivedProps => (
      // tslint:disable-next-line:prefer-object-spread Typescript fails to spread a generic type, this is a workaround mentioned here: https://github.com/Microsoft/TypeScript/pull/13288#issuecomment-367396818
      Object.assign(
        {},
        props.ownProps,
        {
          movedOrgOnboardingDismissed: getMovedOrgOnboardingDismissed(props),
        },
      )
    ),
  },
);

function getMovedOrgOnboardingDismissed<TInProps>(props: OptionProps<TInProps, OnboardingQuery>) {
  if (!props.data || props.data.loading || props.data.error || !props.data.onboarding) {
    return undefined;
  }

  return props.data.onboarding.dismissed;
}

export function withOrganizationRedirect(Component: React.ComponentType<{}>): React.ComponentClass<DerivedProps & RouteComponentProps<any>> {
  return withMovedOrgOnboarding(withOrganizationRedirectImpl(Component));
}
