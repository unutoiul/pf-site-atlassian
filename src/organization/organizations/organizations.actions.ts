export enum OrganizationKeys {
  START_CREATE_ORGANIZATION = 'START_CREATE_ORGANIZATION',
  STOP_CREATE_ORGANIZATION = 'STOP_CREATE_ORGANIZATION',
  START_POLLING_ORGANIZATION = 'START_POLLING_ORGANIZATION',
}

export interface CreateOrganization {
  type: OrganizationKeys.START_CREATE_ORGANIZATION;
  name: string;
}

export interface StopCreateOrganization {
  type: OrganizationKeys.STOP_CREATE_ORGANIZATION;
  success: boolean;
}

export interface StartPollingOrganization {
  type: OrganizationKeys.START_POLLING_ORGANIZATION;
  progressUri: string;
  orgId: string;
}

export function startPollingOrganization(progressUri, orgId): StartPollingOrganization {
  return {
    type: OrganizationKeys.START_POLLING_ORGANIZATION,
    progressUri,
    orgId,
  };
}

export function createOrganization(name): CreateOrganization {
  return {
    type: OrganizationKeys.START_CREATE_ORGANIZATION,
    name,
  };
}

export function stopCreateOrganization(success = false): StopCreateOrganization {
  return {
    type: OrganizationKeys.STOP_CREATE_ORGANIZATION,
    success,
  };
}
