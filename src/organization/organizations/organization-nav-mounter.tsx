import * as React from 'react';
import { ChildProps, compose, graphql, QueryResult } from 'react-apollo';
import { defineMessages } from 'react-intl';
import { connect } from 'react-redux';

import { analyticsClient } from 'common/analytics';
import { getConfig } from 'common/config';
import {
  NavigationLinkIconGlyph,
  NavigationMounterProps,
  NavigationSectionId,
} from 'common/navigation';
import { OnboardingIdType, onboardingMutation, onboardingQuery } from 'common/onboarding';

import { CurrentUserQuery, OnboardingQuery, OnboardingQueryVariables } from '../../schema/schema-types';
import { RootState } from '../../store';
import { NoOrgOnboardingPage } from '../onboarding/no-org-onboarding-page';
import { OrgNavMounterProps } from './organization.prop-types';
import currentUserQuery from './queries/current-user.query.graphql';

export const messages = defineMessages({
  security: {
    id: 'organization.nav.security',
    defaultMessage: 'Security',
  },
});

const mapStateToProps = (state: RootState) => ({
  navigationSections: state.chrome.navigationSections,
});

interface QueryProps {
  movedOrgOnboarding: QueryResult & Partial<OnboardingQuery>;
  currentOrgs: QueryResult & Partial<CurrentUserQuery>;
}

export class OrganizationNavMounterImpl extends React.Component<ChildProps<NavigationMounterProps & OrgNavMounterProps> & QueryProps> {
  private adminHubRouteRegistered = false;

  public shouldComponentUpdate() {
    return !this.adminHubRouteRegistered;
  }

  public componentDidMount() {
    this.registerRoutesIfPossible();
  }

  public componentDidUpdate() {
    this.registerRoutesIfPossible();
  }

  public render() {
    return null;
  }

  private registerRoutesIfPossible() {
    const {
      movedOrgOnboarding: { loading: movedOrgOnboardingLoading } = { loading: true },
      currentOrgs: { loading: currentOrgsLoading } = { loading: true },
      navigationSections,
    } = this.props;

    const isAnythingStillLoading = !navigationSections || movedOrgOnboardingLoading || currentOrgsLoading;

    if (isAnythingStillLoading) {
      return;
    }

    const {
      currentOrgs: {
        currentUser,
      },
    } = this.props;

    if (currentUser && currentUser.organizations && currentUser.organizations.length > 0) {
      this.registerMovedOrgPage();
    } else {
      this.registerAtlassianAccessValuePropPage();
    }
  }

  private registerMovedOrgPage() {
    const {
      updateNavigationSection,
      movedOrgOnboarding: {
        onboarding: {
          dismissed = false,
        } = {},
      } = {},
      mutate,
    } = this.props;

    if (!dismissed && mutate) {
      mutate({
        variables: {
          id: OnboardingIdType.MOVED_ORG,
        },
      }).catch((error) => {
        analyticsClient.onError(error);
      });
    }

    if (this.adminHubRouteRegistered) {
      return;
    }
    updateNavigationSection({
      id: NavigationSectionId.Organizations,
      links: [{
        id: 'security',
        title: messages.security,
        analyticsSubjectId: 'security',
        priority: 10,
        isExternal: dismissed ? true : false,
        path: dismissed ? getConfig().goToAdminLinkUrl : '/admin/security/moved',
        icon: NavigationLinkIconGlyph.OfficeBuilding,
      }],
    });

    this.adminHubRouteRegistered = true;
  }

  private registerAtlassianAccessValuePropPage() {
    const {
      updateNavigationSection,
      registerRoutes,
    } = this.props;

    const path = '/admin/security';

    if (this.adminHubRouteRegistered) {
      return;
    }

    updateNavigationSection({
      id: NavigationSectionId.Organizations,
      links: [{
        id: 'security',
        title: messages.security,
        analyticsSubjectId: 'security',
        priority: 10,
        isExternal: false,
        path,
        icon: NavigationLinkIconGlyph.OfficeBuilding,
      }],
    });

    registerRoutes({
      switchRoutes: [
        {
          path,
          component: NoOrgOnboardingPage,
        },
      ],
    });

    this.adminHubRouteRegistered = true;
  }
}

const currentUserOrganization = graphql<NavigationMounterProps, CurrentUserQuery>(currentUserQuery, {
  name: 'currentOrgs',
});

const movedOrgOnboarding = graphql<NavigationMounterProps, OnboardingQuery, OnboardingQueryVariables>(onboardingQuery, {
  name: 'movedOrgOnboarding',
  options: {
    variables: {
      id: OnboardingIdType.MOVED_ORG,
    },
  },
});

export const OrganizationNavMounter = compose(
  currentUserOrganization,
  movedOrgOnboarding,
  graphql(onboardingMutation),
  connect(mapStateToProps),
)(OrganizationNavMounterImpl);
