import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import { ChildProps } from 'react-apollo';
import { MockedProvider } from 'react-apollo/test-utils';
import { MemoryRouter, Redirect } from 'react-router';
import { sandbox as sinonSandbox, SinonSandbox, SinonSpy } from 'sinon';

import AkSpinner from '@atlaskit/spinner';

import { getConfig } from 'common/config';
import { onboardingQuery } from 'common/onboarding';

import { OnboardingQuery } from '../../schema/schema-types';
import { util } from '../../utilities/redirect';
import { DerivedProps, withOrganizationRedirect } from './organization-redirect';

describe('OrganizationRedirect', () => {
  class TestComponentImpl extends React.Component<DerivedProps> {
    public render() {
      return <div id="test" />;
    }
  }

  type MockMovedOrgOnboardingData = Partial<ChildProps<{}, OnboardingQuery>['data']>;

  let sandbox: SinonSandbox;
  let redirectSpy: SinonSpy;

  beforeEach(() => {
    sandbox = sinonSandbox.create();
    redirectSpy = sandbox.stub(util, 'redirect');
  });

  afterEach(() => {
    sandbox.restore();
  });

  const mountTestComponent = (
    mockMovedOrgOnboardingData: MockMovedOrgOnboardingData,
    pathname = '/admin/o/1363002c-4b37-303f-b9e3-06734d54989a/members',
    search = '',
  ) => {

    const TestComponent = withOrganizationRedirect(TestComponentImpl);

    const variables = { id: 'MovedOrg' };
    const mocks = [
      { request: { query: onboardingQuery, variables }, result: { data: mockMovedOrgOnboardingData } },
    ];

    return mount((
      <MemoryRouter>
        <MockedProvider mocks={mocks} addTypename={false}>
          <TestComponent location={{ pathname, search, state: null, hash: null } as any} match={null as any} history={null as any} />
        </MockedProvider>
      </MemoryRouter>
    ));
  };

  // MockedProvider is async so we have to wait for it to complete with this hack
  // see https://github.com/apollographql/react-apollo/issues/1711
  const waitForMockedProvider = async (wrapper) => {
    // tslint:disable-next-line:no-unnecessary-callback-wrapper
    await new Promise(resolve => setTimeout(resolve));
    wrapper.update();
  };

  it('should immediately render component for non org paths even when the data is loading', async () => {
    const mockMovedOrgOnboardingData = { loading: true, onboarding: null };

    const wrapper = mountTestComponent(mockMovedOrgOnboardingData as any, '/admin/emoji');
    await waitForMockedProvider(wrapper);

    const spinner = wrapper.find<any>(AkSpinner);
    expect(spinner).to.have.length(0);

    const testChildDiv = wrapper.find('#test');
    expect(testChildDiv).to.have.length(1);
  });

  it('should render spinner when loading', async () => {
    const mockMovedOrgOnboardingData = { loading: true, onboarding: null };

    const wrapper = mountTestComponent(mockMovedOrgOnboardingData as any);
    await waitForMockedProvider(wrapper);

    const spinner = wrapper.find<any>(AkSpinner);
    expect(spinner).to.have.length(1);

    const testChildDiv = wrapper.find('#test');
    expect(testChildDiv).to.have.length(0);
  });

  it('should redirect to orgs moved page if onboarding is not dismissed', async () => {
    // TODO: console warning for redirecting again to the same page.
    sandbox.stub(console, 'error');
    const mockMovedOrgOnboardingData = { onboarding: { id: 'test', dismissed: false } };

    const wrapper = mountTestComponent(mockMovedOrgOnboardingData as any);
    await waitForMockedProvider(wrapper);

    const spinner = wrapper.find<any>(AkSpinner);
    expect(spinner).to.have.length(0);

    const testChildDiv = wrapper.find('#test');
    expect(testChildDiv).to.have.length(0);

    const redirect = wrapper.find<any>(Redirect);
    expect(redirect).to.have.length(1);
    expect(redirect.props().to).to.equal('/admin/security/moved');
  });

  it('should redirect to admin hub overview', async () => {
    const mockMovedOrgOnboardingData = { onboarding: { id: 'test', dismissed: true } };

    const wrapper = mountTestComponent(mockMovedOrgOnboardingData as any, '/admin/o/1234/overview');
    await waitForMockedProvider(wrapper);

    const spinner = wrapper.find<any>(AkSpinner);
    expect(spinner).to.have.length(1);

    const testChildDiv = wrapper.find('#test');
    expect(testChildDiv).to.have.length(0);

    expect(redirectSpy.callCount).to.equal(1);
    expect(redirectSpy.calledWith(getConfig().goToAdminLinkUrl + '/o/1234/overview')).to.equal(true);
  });

  it('should redirect to admin hub if admin hub members', async () => {
    const mockMovedOrgOnboardingData = { onboarding: { id: 'test', dismissed: true } };

    const wrapper = mountTestComponent(mockMovedOrgOnboardingData as any, '/admin/o/1234/members');
    await waitForMockedProvider(wrapper);

    const spinner = wrapper.find<any>(AkSpinner);
    expect(spinner).to.have.length(1);

    const testChildDiv = wrapper.find('#test');
    expect(testChildDiv).to.have.length(0);

    expect(redirectSpy.callCount).to.equal(1);
    expect(redirectSpy.calledWith(getConfig().goToAdminLinkUrl + '/o/1234/members')).to.equal(true);
  });

  it('should redirect to admin hub and preserve query parameters', async () => {
    const mockMovedOrgOnboardingData = { onboarding: { id: 'test', dismissed: true } };

    const wrapper = mountTestComponent(mockMovedOrgOnboardingData as any, '/admin/o/1234/members', '?id=xyz&test=true');
    await waitForMockedProvider(wrapper);

    const spinner = wrapper.find<any>(AkSpinner);
    expect(spinner).to.have.length(1);

    const testChildDiv = wrapper.find('#test');
    expect(testChildDiv).to.have.length(0);

    expect(redirectSpy.callCount).to.equal(1);
    expect(redirectSpy.calledWith(getConfig().goToAdminLinkUrl + '/o/1234/members?id=xyz&test=true')).to.equal(true);
  });

  it('should not redirect for new admin hub org pages', async () => {
    const mockMovedOrgOnboardingData = { onboarding: { id: 'test', dismissed: true } };

    const wrapper = mountTestComponent(mockMovedOrgOnboardingData as any, '/o/1234/members');
    await waitForMockedProvider(wrapper);

    const spinner = wrapper.find<any>(AkSpinner);
    expect(spinner).to.have.length(0);

    const testChildDiv = wrapper.find('#test');
    expect(testChildDiv).to.have.length(1);

    expect(redirectSpy.callCount).to.equal(0);
  });

});
