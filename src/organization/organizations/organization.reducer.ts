import {
  Action, OrganizationKeys,
} from '../../store/action-types';

export interface OrganizationState {
  progressUri: string;
  orgId: string;
  completed: boolean;
  successful: boolean;
  isLoading: boolean;
  shouldRedirect: boolean;
  name: string;
}

const initialState = {
  orgId: '',
  name: '',
  progressUri: '',
  isLoading: false,
  completed: false,
  shouldRedirect: false,
  successful: false,
};

const organizationReducer = (state: OrganizationState = initialState, action: Action) => {
  switch (action.type) {
    case OrganizationKeys.START_CREATE_ORGANIZATION:
      return {
        ...state,
        isLoading: true,
        name: action.name,
      };
    case OrganizationKeys.START_POLLING_ORGANIZATION:
      return {
        ...state,
        isLoading: true,
        progressUri: action.progressUri,
        orgId: action.orgId,
      };
    case OrganizationKeys.STOP_CREATE_ORGANIZATION:
      return {
        ...state,
        isLoading: false,
        shouldRedirect: action.success,
      };
    default:
      return state;
  }
};

export { organizationReducer };
