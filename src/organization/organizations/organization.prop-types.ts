import { QueryProps, QueryResult } from 'react-apollo';
import { Dispatch } from 'redux';

import { NavItem } from 'common/navigation';

import {
  CurrentUserQuery,
  OnboardingQuery,
} from '../../schema/schema-types';

export interface Organization {
  id: string;
}

export interface CreateOrgSectionProps {
  dispatch?: Dispatch<any>;
  progressUri?: string;
  isLoading?: boolean;
  shouldRedirect?: boolean;
  completed?: boolean;
  successful?: boolean;
  orgId?: string;
  name?: string;
  currentOrgs: QueryResult & CurrentUserQuery;
}

export interface OrgNavMounterProps {
  navigationSections: NavItem[];
  currentOrgs: CurrentUserQuery & QueryProps;
  movedOrgOnboarding: OnboardingQuery & QueryProps;
}

export interface Enforced {
  state: EnforcementState.Enforced;
  dateEnforced: Date;
}

export interface Unenforced {
  state: EnforcementState.Unenforced;
}

export interface Onboarding {
  state: EnforcementState.Onboarding;
  dateEnforced: Date;
}

export enum EnforcementState {
  Unenforced,
  Onboarding,
  Enforced,
}

export type TwoStepVerificationEnforcementState = Enforced | Unenforced | Onboarding;
