import { FeatureFlagProps, withUserFeatureFlag } from 'common/feature-flags';

import { memberDeletionFeatureFlagKey } from './flag-keys';

export interface MemberDeletionFeatureFlag {
  memberDeletionFeatureFlag: FeatureFlagProps;
}

export function withMemberDeletionFeatureFlag<TOwnProps>(Component: React.ComponentType<TOwnProps & MemberDeletionFeatureFlag>): React.ComponentClass<TOwnProps> {
  return withUserFeatureFlag<TOwnProps, MemberDeletionFeatureFlag>({
    flagKey: memberDeletionFeatureFlagKey,
    defaultValue: false,
    name: 'memberDeletionFeatureFlag',
  })(Component);
}
