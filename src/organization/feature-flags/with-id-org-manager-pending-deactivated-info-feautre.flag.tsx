import { FeatureFlagProps, withUserFeatureFlag } from 'common/feature-flags';

export interface PendingAndDeactivatedInfoFromIOMFeatureFlag {
  pendingAndDeactivatedInfoFromIOMFeatureFlag: FeatureFlagProps;
}

export function withPendingAndDeactivatedInfoFromIOMFeatureFlag<TOwnProps>(Component: React.ComponentType<TOwnProps & PendingAndDeactivatedInfoFromIOMFeatureFlag>): React.ComponentClass<TOwnProps> {
  return withUserFeatureFlag<TOwnProps, PendingAndDeactivatedInfoFromIOMFeatureFlag>({
    flagKey: 'admin.pending-info-and-deactivated-info-from-iom-instead-fo-fam',
    defaultValue: false,
    name: 'pendingAndDeactivatedInfoFromIOMFeatureFlag',
  })(Component);
}
