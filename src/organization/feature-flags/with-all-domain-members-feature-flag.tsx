import { FeatureFlagProps, withOrgFeatureFlag } from 'common/feature-flags';

export interface AllDomainMembersFeatureFlag {
  allDomainMembersFeatureFlag: FeatureFlagProps;
}

// https://app.launchdarkly.com/cloud-admin/production/features/member-page.all-domains
export function withAllDomainMembersFeatureFlag<TOwnProps>(Component: React.ComponentType<TOwnProps & AllDomainMembersFeatureFlag>): React.ComponentClass<TOwnProps> {
  return withOrgFeatureFlag<TOwnProps, AllDomainMembersFeatureFlag>({
    flagKey: 'member-page.all-domains',
    defaultValue: false,
    name: 'allDomainMembersFeatureFlag',
  })(Component);
}
