export const memberDeletionFeatureFlagKey = 'admin.member-cancel-deletion';
export const memberDeactivationViaFamFeatureFlagKey = 'admin.member-deactivation-via-fam';

// https://app.launchdarkly.com/cloud-admin/production/features/admin.api
export const adminApiFeatureFlagKey = 'admin.api';
