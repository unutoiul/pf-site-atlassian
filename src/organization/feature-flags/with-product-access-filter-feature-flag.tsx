import { FeatureFlagProps, withOrgFeatureFlag } from 'common/feature-flags';

export interface ProductAccessFeatureFlag {
  productAccessFeatureFlag: FeatureFlagProps;
}

// https://app.launchdarkly.com/cloud-admin/production/features/product-access-filter
export function withProductAccessFeatureFlag<TOwnProps>(Component: React.ComponentType<TOwnProps & ProductAccessFeatureFlag>): React.ComponentClass<TOwnProps> {
  return withOrgFeatureFlag<TOwnProps, ProductAccessFeatureFlag>({
    flagKey: 'product-access-filter',
    defaultValue: false,
    name: 'productAccessFeatureFlag',
  })(Component);
}
