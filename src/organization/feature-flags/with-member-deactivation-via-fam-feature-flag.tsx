import { FeatureFlagProps, withUserFeatureFlag } from 'common/feature-flags';

import { memberDeactivationViaFamFeatureFlagKey } from './flag-keys';

export interface MemberDeactivationViaFamFeatureFlag {
  memberDeactivationViaFamFeatureFlag: FeatureFlagProps;
}

export function withMemberDeactivationViaFamFeatureFlag<TOwnProps>(Component: React.ComponentType<TOwnProps & MemberDeactivationViaFamFeatureFlag>): React.ComponentClass<TOwnProps> {
  return withUserFeatureFlag<TOwnProps, MemberDeactivationViaFamFeatureFlag>({
    flagKey: memberDeactivationViaFamFeatureFlagKey,
    defaultValue: false,
    name: 'memberDeactivationViaFamFeatureFlag',
  })(Component);
}
