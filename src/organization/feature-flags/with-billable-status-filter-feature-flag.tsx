import { FeatureFlagProps, withOrgFeatureFlag } from 'common/feature-flags';

export interface BillableStatusFeatureFlag {
  billableStatusFeatureFlag: FeatureFlagProps;
}

// https://app.launchdarkly.com/cloud-admin/production/features/member-table.billable-status
export function withBillableStatusFeatureFlag<TOwnProps>(Component: React.ComponentType<TOwnProps & BillableStatusFeatureFlag>): React.ComponentClass<TOwnProps> {
  return withOrgFeatureFlag<TOwnProps, BillableStatusFeatureFlag>({
    flagKey: 'member-table.billable-status',
    defaultValue: false,
    name: 'billableStatusFeatureFlag',
  })(Component);
}
