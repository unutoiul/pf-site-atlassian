import { FeatureFlagProps, withOrgFeatureFlag } from 'common/feature-flags';

export interface DomainClaimCheckFeatureFlag {
  domainClaimCheckFeatureFlag: FeatureFlagProps;
}

// https://app.launchdarkly.com/cloud-admin/production/features/domain-claim.check-claimed
export function withDomainClaimCheckFeatureFlag<TOwnProps>(Component: React.ComponentType<TOwnProps & DomainClaimCheckFeatureFlag>): React.ComponentClass<TOwnProps> {
  return withOrgFeatureFlag<TOwnProps, DomainClaimCheckFeatureFlag>({
    flagKey: 'domain-claim.check-claimed',
    defaultValue: false,
    name: 'domainClaimCheckFeatureFlag',
  })(Component);
}
