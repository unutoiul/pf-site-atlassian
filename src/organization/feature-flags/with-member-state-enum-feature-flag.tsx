import { FeatureFlagProps, withOrgFeatureFlag } from 'common/feature-flags';

export interface MemberStateEnumFeatureFlag {
  memberStateEnumFeatureFlag: FeatureFlagProps;
}

// https://app.launchdarkly.com/cloud-admin/production/features/member-state-enum
export function withMemberStateEnumFeatureFlag<TOwnProps>(Component: React.ComponentType<TOwnProps & MemberStateEnumFeatureFlag>): React.ComponentClass<TOwnProps> {
  return withOrgFeatureFlag<TOwnProps, MemberStateEnumFeatureFlag>({
    flagKey: 'member-state-enum',
    defaultValue: false,
    name: 'memberStateEnumFeatureFlag',
  })(Component);
}
