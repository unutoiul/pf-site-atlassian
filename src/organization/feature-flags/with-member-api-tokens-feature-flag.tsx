/**
 * Feature flag defined at https://app.launchdarkly.com/cloud-admin/test/features/admin.member.api.token/targeting
 */

import { FeatureFlagProps, withOrgFeatureFlag } from 'common/feature-flags';

export interface MemberApiTokenFeatureFlag {
  memberApiTokenFeatureFlag: FeatureFlagProps;
}

export function withMemberApiTokenFeatureFlag<TOwnProps>(Component: React.ComponentType<TOwnProps & MemberApiTokenFeatureFlag>): React.ComponentClass<TOwnProps> {
  return withOrgFeatureFlag<TOwnProps, MemberApiTokenFeatureFlag>({
    flagKey: 'admin.member.api.token',
    defaultValue: false,
    name: 'memberApiTokenFeatureFlag',
  })(Component);
}
