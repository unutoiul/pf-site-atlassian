import { FeatureFlagProps, withOrgFeatureFlag } from 'common/feature-flags';

import { adminApiFeatureFlagKey } from './flag-keys';

export interface AdminApiFeatureFlag {
  adminApiFeatureFlag: FeatureFlagProps;
}

export function withAdminApiFeatureFlag<TOwnProps>(Component: React.ComponentType<TOwnProps & AdminApiFeatureFlag>): React.ComponentClass<TOwnProps> {
  return withOrgFeatureFlag<TOwnProps, AdminApiFeatureFlag>({
    flagKey: adminApiFeatureFlagKey,
    defaultValue: false,
    name: 'adminApiFeatureFlag',
  })(Component);
}
