import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import { MemoryRouter, Route } from 'react-router';

import { MockOrgFeatureFlagApolloClient } from 'common/feature-flags/test-utils';

import { adminApiFeatureFlagKey } from './flag-keys';
import { withAdminApiFeatureFlag } from './with-admin-api-feature-flag';

describe('withAdminApiFeatureFlag', () => {

  const TestComponent = ({ adminApiFeatureFlag }) => <div>{JSON.stringify(adminApiFeatureFlag)}</div>;
  const TestWrappedComponent = withAdminApiFeatureFlag<{}>(TestComponent);

  const mountWrappedComponentWithAdminApiFeatureFlag = () => {
    return mount(
      <MemoryRouter initialEntries={[{ pathname: '/o/my-org-id/overview' }]}>
        <Route path="/o/:orgId/overview">
          <MockOrgFeatureFlagApolloClient
            flagKey={adminApiFeatureFlagKey}
            flagValue={true}
            orgId="my-org-id"
          >
            <TestWrappedComponent />
          </MockOrgFeatureFlagApolloClient>
        </Route>
      </MemoryRouter>,
    );
  };

  it('should pass user provisioning feature flag result to component', () => {
    const wrapper = mountWrappedComponentWithAdminApiFeatureFlag();

    const testComponent = wrapper.find(TestComponent);
    expect(testComponent.props()).to.deep.equal({
      adminApiFeatureFlag: {
        isLoading: false,
        value: true,
        defaultValue: false,
      },
    });
  });
});
