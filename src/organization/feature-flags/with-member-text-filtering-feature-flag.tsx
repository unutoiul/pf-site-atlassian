import { FeatureFlagProps, withOrgFeatureFlag } from 'common/feature-flags';

export interface MemberTextFilteringFeatureFlag {
  memberTextFilteringFeatureFlag: FeatureFlagProps;
}

// https://app.launchdarkly.com/cloud-admin/production/features/member-table.filter-string
export function withMemberTextFilteringFeatureFlag<TOwnProps>(Component: React.ComponentType<TOwnProps & MemberTextFilteringFeatureFlag>): React.ComponentClass<TOwnProps> {
  return withOrgFeatureFlag<TOwnProps, MemberTextFilteringFeatureFlag>({
    flagKey: 'member-table.filter-string',
    defaultValue: false,
    name: 'memberTextFilteringFeatureFlag',
  })(Component);
}
