// tslint:disable jsx-use-translation-function
import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { spy } from 'sinon';

import AkTabs from '@atlaskit/tabs';

import { createMockIntlProp } from '../../utilities/testing';
import { DomainClaimTabImpl } from '../domain-claim';

describe('DomainClaimTab', () => {
  const toggleDialogHandlerSpy = spy((arg) => arg);

  beforeEach(() => {
    toggleDialogHandlerSpy.reset();
  });

  const wrapper = shallow(
    <DomainClaimTabImpl orgId={'DUMMY-TEST-ORG-ID'} toggleDialogHandler={toggleDialogHandlerSpy} intl={createMockIntlProp()} />,
  );
  const tabs = wrapper.find(AkTabs) as typeof AkTabs;

  it('there should be two tabs', () => {
    expect(tabs.props().tabs).to.have.length(2, 'There should be two tabs!');
    expect(tabs.props().tabs[0].label).to.deep.equal('DNS');
    expect(tabs.props().tabs[1].label).to.deep.equal('HTTPS');
    expect(tabs.props().tabs[0].defaultSelected).to.equal(true);
    expect(tabs.props().tabs[1].defaultSelected).to.equal(false);
  });

  describe('token tab', () => {

    it('should have correct values', () => {
      expect(tabs.props().tabs[0].label).to.deep.equal('DNS');
      expect(tabs.props().tabs[0].defaultSelected).to.equal(true);
    });

    it('should pass the orgId down to the child component', () => {
      const tabContent = tabs.props().tabs[0].content;
      expect(tabContent.props.orgId).to.deep.equal('DUMMY-TEST-ORG-ID');
      expect(tabContent.props.toggleDialog).to.deep.equal('dns');
    });
  });

  describe('http tab', () => {
    it('should have correct values', () => {
      expect(tabs.props().tabs[1].label).to.deep.equal('HTTPS');
      expect(tabs.props().tabs[1].defaultSelected).to.equal(false);
    });

    it('should pass the orgId down to the child component', () => {
      const tabContent = tabs.props().tabs[1].content;
      expect(tabContent.props.orgId).to.deep.equal('DUMMY-TEST-ORG-ID');
      expect(tabContent.props.toggleDialog).to.deep.equal('http');
    });
  });
});
