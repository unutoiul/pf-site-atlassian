import * as React from 'react';
import { defineMessages, FormattedHTMLMessage, FormattedMessage } from 'react-intl';

import { catchInNetImage } from 'common/images';
import { PageSidebar } from 'common/page-layout';

const messages = defineMessages({
  title: {
    id: 'organization.domain.claim.sidebar.title',
    defaultMessage: 'What you need to know',
    description: 'Heading for instructions on the processes related to domain claims',
  },
  pointOne: {
    id: 'organization.domain.claim.sidebar.bullet.one',
    defaultMessage: 'Manage any user account with the domain across sites and products.',
    description: 'First bullet point in the list of domain claim instructions.  Instructs the user on what domain claim is for.',
  },
  pointTwo: {
    id: 'organization.domain.claim.sidebar.bullet.two',
    defaultMessage: 'We\'ll email users with this domain to let them know what it means to have a managed account.',
    description: 'Second bullet point in the list of domain claim instructions',
  },
  pointThree: {
    id: 'organization.domain.claim.sidebar.bullet.three',
    defaultMessage: '<strong>Don’t delete the DNS record or HTTPS file from your server at any time</strong> as we need to verify it on a regular basis.',
    description: 'Third bullet point in the list of domain claim instructions',
  },
});

export const DomainClaimPageSideBar = () => {
  return (
    <PageSidebar
      imgBeforeSrc={catchInNetImage}
    >
      <FormattedMessage {...messages.title} tagName="h4"/>
      <ul>
        <FormattedMessage {...messages.pointOne} tagName="li"/>
        <FormattedMessage {...messages.pointTwo} tagName="li"/>
        <FormattedHTMLMessage {...messages.pointThree} tagName="li"/>
      </ul>
    </PageSidebar >
  );
};
