import * as React from 'react';
import { graphql } from 'react-apollo';
import { defineMessages, FormattedHTMLMessage, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import { withRouter } from 'react-router-dom';
import styled from 'styled-components';

import AkButton, { ButtonGroup as AkButtonGroup } from '@atlaskit/button';
import AkFieldRadioGroup from '@atlaskit/field-radio-group';
import AkTextField from '@atlaskit/field-text';
import { colors as akColors, gridSize as akGridSize } from '@atlaskit/theme';

import { ActionsBarModalFooter } from 'common/actions-bar';
import { AnalyticsData, Button as AnalyticsButton } from 'common/analytics';
import { ModalDialog } from 'common/modal';

import { DomainCheckQuery, DomainCheckQueryVariables, VerificationType } from '../../schema/schema-types';
import domainCheck from './domain-check.query.graphql';

import { createOrgAnalyticsData } from '../organizations/organization-analytics';

import { DomainClaimCheckFeatureFlag, withDomainClaimCheckFeatureFlag } from '../feature-flags/with-domain-claim-check-feature-flag';
import { Domains } from './domain-claim.prop-types';

const ButtonChildren = styled.div`
  padding: ${akGridSize() * 2}px 0 ${akGridSize() * 2}px 0;
`;

const Description = styled.div`
  color: ${akColors.N200};
`;

const RedDescription = styled.div`
  color: ${akColors.R500};
`;

export interface DomainClaimModalProps {
  orgId: string;
  isClaimingDomain: boolean;
  reverificationDomain?: string;
  verificationType: string | undefined;
  domains: Domains[];
  verifyDomain(domain: string, method: VerificationType): void;
  onClose(): void;
}

export interface DomainClaimModalState {
  hasDomain: boolean;
  domainConflict?: string;
  verificationType?: VerificationType;
}

const messages = defineMessages({
  verifyDomain: {
    id: 'organization.domain.claim.tab.dns.modal.verify.domain',
    defaultMessage: 'Verify domain',
  },
  modalText: {
    id: 'organization.domain.claim.tab.dns.modal.text',
    defaultMessage: 'Complete the following details. We\'ll check for the record to verify your ownership of the domain.',
  },
  cancel: {
    id: 'organization.domain.claim.tab.dns.modal.cancel',
    defaultMessage: 'Cancel',
  },
  label: {
    id: 'organization.domain.claim.tab.dns.modal.label',
    defaultMessage: 'Domain',
  },
  example: {
    id: 'organization.domain.claim.tab.dns.modal.example',
    defaultMessage: 'Example: acme.com',
  },
  method: {
    id: 'organization.domain.claim.tab.dns.modal.method',
    defaultMessage: 'Method',
  },
  txtRecord: {
    id: 'organization.domain.claim.tab.dns.modal.dns.method',
    defaultMessage: 'TXT Record',
  },
  httpRecord: {
    id: 'organization.domain.claim.tab.dns.modal.http.method',
    defaultMessage: 'HTTPS',
  },
  domainAlreadyVerified: {
    id: 'organization.domain.claim.warning.modal.domain.already.verified',
    defaultMessage: 'Domain already verified',
    description: 'Error message when someone tries to claim a domain that has already been claimed',
  },
  domainAlreadyVerifiedBody: {
    id: 'organization.domain.claim.warning.modal.domain.already.verified.body',
    defaultMessage: 'If your domain verification is successful, the other organization will lose ownership. This means that any security policies currently configured for the domain, such as SAML single sign-on, will no longer apply.',
    description: 'Warns users what will happen if they proceed with the domain claim, since another org has claimed the domain they are already trying to claim',
  },
  anotherOrg: {
    id: 'organization.domain.claim.warning.modal.domain.already.verified.domain',
    defaultMessage: 'An organization has already verified <strong>{domainName}</strong>.',
    description: 'Warns users that the domain they are currently trying to claim has already been claimed.',
  },
});

interface QueryProps {
  checkIfDomainHasBeenClaimed(domain: string): Promise<boolean>;
}

type AllProps = DomainClaimModalProps & InjectedIntlProps & DomainClaimCheckFeatureFlag & QueryProps;

export class DomainClaimModalImpl extends React.PureComponent<AllProps, DomainClaimModalState> {
  public state: DomainClaimModalState = { hasDomain: false, domainConflict: undefined, verificationType: undefined };

  public componentWillReceiveProps() {
    this.setState({ hasDomain: false });
  }

  public render() {
    const {
      intl: { formatMessage },
      onClose,
      domainClaimCheckFeatureFlag,
    } = this.props;

    if (domainClaimCheckFeatureFlag.isLoading) {
      return null;
    }

    if (this.state.domainConflict) {
      return (
        <ModalDialog
          width={'small'}
          header={<FormattedMessage {...messages.domainAlreadyVerified} />}
          isOpen={true}
          footer={this.getWarningFooter()}
          onClose={onClose}
        >
          <RedDescription>
            <FormattedHTMLMessage {...messages.anotherOrg} values={{ domainName: this.state.domainConflict }} />
          </RedDescription>
          <FormattedMessage {...messages.domainAlreadyVerifiedBody} tagName="p" />
        </ModalDialog>
      );
    }

    return (
      <ModalDialog
        width={'small'}
        header={formatMessage(messages.verifyDomain)}
        isOpen={true}
        onClose={this.props.onClose}
        children={this.modalDialogContent()}
      />
    );
  }

  private handleTextFieldChange = (event: React.FormEvent<HTMLInputElement>) => {
    this.setState({ hasDomain: !!event.currentTarget.value });
  }

  private getWarningFooter = () => {
    const { onClose } = this.props;

    return (
      <AkButtonGroup>
        <AkButton
          appearance="danger"
          onClick={this.handleConfirmVerify}
          isLoading={this.props.isClaimingDomain}
        >
          <FormattedMessage {...messages.verifyDomain} />
        </AkButton>
        <AkButton onClick={onClose} appearance="subtle-link">
          <FormattedMessage {...messages.cancel} />
        </AkButton>
      </AkButtonGroup>
    );
  }

  private modalDialogContent = () => {
    const {
      intl: { formatMessage },
    } = this.props;

    return (
      <form onSubmit={this.handleSubmit}>
        {this.getInnerModalContent()}
        <ActionsBarModalFooter alignment="right">
          <ButtonChildren>
            <AnalyticsButton
              type="submit"
              appearance="primary"
              isDisabled={(!this.state.hasDomain && !this.props.reverificationDomain)}
              isLoading={this.props.isClaimingDomain}
              analyticsData={this.generateVerifyDomainAnalytics()}
              id="modal-submit-verify-domain-claim-button"
            >
              {formatMessage(messages.verifyDomain)}
            </AnalyticsButton>
            <AkButton onClick={this.props.onClose} appearance="subtle-link">
              {formatMessage(messages.cancel)}
            </AkButton>
          </ButtonChildren>
        </ActionsBarModalFooter>
      </form>
    );
  }

  private handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    const { domain, method } = (e.currentTarget as any).elements;
    const { domainClaimCheckFeatureFlag } = this.props;

    let methodValue: string;
    if (!('value' in method)) {
      // IE11 doesn't fully support HTMLFormControlsCollection API, so we need to get the value of radio group manually
      const selectedItem = Array.from<HTMLInputElement>(method).find(radio => radio.checked);
      methodValue = selectedItem!.value;
    } else {
      methodValue = method.value;
    }

    if (domainClaimCheckFeatureFlag.value) {
      if (await this.shouldShowDomainConflictWarning(domain.value)) {
        this.setState({
          domainConflict: domain.value,
          verificationType: methodValue as VerificationType,
        });

        return;
      }

    }

    this.props.verifyDomain(domain.value, methodValue as VerificationType);
  }

  private shouldShowDomainConflictWarning = async (newDomain) => {
    if (this.props.domains.some((domainObj) => domainObj.domain === newDomain && domainObj.status === 'VERIFIED')) {
      return false;
    }

    const isClaimed = await this.props.checkIfDomainHasBeenClaimed(newDomain);

    if (isClaimed) {
      return true;
    }

    return false;
  }

  private handleConfirmVerify = (): void => {
    const { domainConflict, verificationType } = this.state;
    const { verifyDomain } = this.props;

    if (domainConflict && verificationType) {
      verifyDomain(domainConflict, verificationType);
    }
  }

  private getInnerModalContent = () => {
    const { formatMessage } = this.props.intl;

    const prefilled = !!this.props.reverificationDomain && !!this.props.verificationType;
    const radioGroupValues = [
      { name: 'method', value: 'dns', label: formatMessage(messages.txtRecord), defaultSelected: this.props.verificationType !== 'http', isDisabled: prefilled },
      { name: 'method', value: 'http', label: formatMessage(messages.httpRecord), defaultSelected: this.props.verificationType === 'http', isDisabled: prefilled },
    ];

    return (
      <Description>
        <p>{formatMessage(messages.modalText)}</p>
        <AkFieldRadioGroup
          items={radioGroupValues}
          label={formatMessage(messages.method)}
        />
        <AkTextField
          name="domain"
          placeholder={formatMessage(messages.example)}
          label={formatMessage(messages.label)}
          onChange={this.handleTextFieldChange}
          shouldFitContainer={true}
          disabled={this.props.isClaimingDomain || this.props.reverificationDomain != null}
          value={this.props.reverificationDomain || ''}
          id="modal-domain-url-text-field"
        />
      </Description>
    );
  }

  private generateVerifyDomainAnalytics = (): AnalyticsData => {
    return createOrgAnalyticsData({
      orgId: this.props.orgId,
      action: 'click',
      actionSubject: 'verifyDomainButton',
      actionSubjectId: 'verify',
      attributes: {
        isReverifying: !!this.props.reverificationDomain,
        verificationType: this.props.verificationType,
      },
    });
  }
}

const query = graphql<DomainClaimModalProps, DomainCheckQuery, DomainCheckQueryVariables, QueryProps>(domainCheck, {
  props: (componentProps): QueryProps => ({
    checkIfDomainHasBeenClaimed: async (domain: string): Promise<boolean> => {
      const { data } = componentProps;
      if (!data) {
        return false;
      }

      const variables: DomainCheckQueryVariables = {
        domain,
        skip: false,
      };

      const result = await data.refetch(variables);

      return result.data.checkDomainClaimed.claimed;
    },
  }),
  options: () => {
    return {
      variables: { domain: '', skip: true },
    };
  },
  skip: ({ orgId }) => !orgId,
});

export const DomainClaimModal =
  query(
    withRouter(
      injectIntl(
        withDomainClaimCheckFeatureFlag(
          DomainClaimModalImpl,
        ),
      ),
    ),
  );
