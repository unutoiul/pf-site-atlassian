import { VerificationType } from '../../schema/schema-types';

export interface Domains {
  domain: string;
  verified: boolean;
  status: string;
  verificationType?: VerificationType;
}

export interface Organization {
  id: string;
  name: string;
}
