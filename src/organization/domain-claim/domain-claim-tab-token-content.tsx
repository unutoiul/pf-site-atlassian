import * as React from 'react';
import { compose, graphql } from 'react-apollo';
import { defineMessages, FormattedHTMLMessage, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import AkButton from '@atlaskit/button';
import AkTextField from '@atlaskit/field-text';
import AkSpinner from '@atlaskit/spinner';
import { gridSize as akGridSize } from '@atlaskit/theme';

import { ActionsBarCheckCircleIcon } from 'common/actions-bar';
import { createErrorIcon, errorFlagMessages } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';
import { Description } from 'common/tab-content';

import domainClaimQuery from './domain-claim.query.graphql';

import {
  InstructionsList,
  SpacedListItem,
  TabButtonFooter,
  TokenActions,
  TokenArea,
  TokenButton,
  TokenComment,
  TokenField,
} from './domain-claim-tab-components';

export interface DomainClaimTabTokenContentProps {
  orgId: string;
  data?: any;
  toggleDialog(): void;
}

const Padding = styled.div`
  padding-top: ${akGridSize() * 3}px;
`;

const messages = defineMessages({
  description1: {
    id: 'organization.domain.claim.tab.dns.description1',
    defaultMessage: 'Add the TXT record below to the DNS host of the domains you want to verify.',
    description: 'Specific instructions on how to use TXT records to claim a domain.  Part 1.',
  },
  description2: {
    id: 'organization.domain.claim.tab.dns.description2',
    defaultMessage: `DNS changes <strong>can take up to 72 hours to update</strong> depending on your DNS host. You may need to wait before adding and verifying your domains.`,
    description: 'Specific instructions on how to use TXT records to claim a domain.  Part 2.',
  },
  description3: {
    id: 'organization.domain.claim.tab.dns.description3',
    defaultMessage: `After adding the TXT record, click <strong>Verify domain</strong> below.`,
    description: 'Specific instructions on how to use TXT records to claim a domain.  Part 3.',
  },
  description4: {
    id: 'organization.domain.claim.tab.dns.description4',
    defaultMessage: 'If you have difficulties with this process, ask your IT department for help.',
    description: 'Specific instructions on how to use TXT records to claim a domain.  Part 4.',
  },
  fieldUnderLabel: {
    id: 'organization.domain.claim.tab.dns.field.under.label',
    defaultMessage: 'The same record can be used for multiple domains.',
  },
  tokenLabel: {
    id: 'organization.domain.claim.tab.dns.token.label',
    defaultMessage: 'Token',
  },
  verifyDomain: {
    id: 'organization.domain.claim.tab.dns.verify.domain',
    defaultMessage: 'Verify domain',
  },
  error: {
    id: 'organization.domain.claim.tab.dns.error',
    defaultMessage: 'There was an error retrieving the DNS token.',
  },
  copy: {
    id: 'organization.domain.claim.tab.dns.copy',
    defaultMessage: 'Copy',
  },
  copySuccess: {
    id: 'organization.domain.claim.tab.dns.copy.success',
    defaultMessage: 'Success!',
  },
  copySuccessDescription: {
    id: 'organization.domain.claim.tab.dns.copy.success.description',
    defaultMessage: 'The DNS token was copied to the clipboard.',
  },
  copyFailDescription: {
    id: 'organization.domain.claim.tab.dns.copy.fail.description',
    defaultMessage: 'There was an issue copying the DNS token to the clipboard.',
  },
});

export class DomainClaimTabTokenContentImpl extends React.Component<DomainClaimTabTokenContentProps & FlagProps & InjectedIntlProps> {
  private textInput: HTMLFormElement | undefined;

  public render() {
    const { formatMessage } = this.props.intl;
    const { organization, loading, error } = this.props.data;

    if (loading) {
      return <Padding><AkSpinner /></Padding>;
    } else if (error || !(organization && organization.domainClaim)) {
      return <span>{formatMessage(messages.error)}</span>;
    }

    return (
      <Description>
        <InstructionsList>
          <SpacedListItem>
            <span>{formatMessage(messages.description1)}</span>
            <TokenArea>
              <TokenActions>
                <TokenField>
                  <AkTextField
                    ref={this.setInput}
                    label={formatMessage(messages.tokenLabel)}
                    isLabelHidden={true}
                    value={this.props.data.organization.domainClaim.token}
                    isReadOnly={true}
                    onChange={undefined}
                    shouldFitContainer={true}
                    id="dns-text-field"
                  />
                </TokenField>
                <TokenButton>
                  <AkButton id={'copy'} onClick={this.onClick} appearance={'default'}>
                    {formatMessage(messages.copy)}
                  </AkButton>
                </TokenButton>
              </TokenActions>
              <TokenComment>
                {formatMessage(messages.fieldUnderLabel)}
              </TokenComment>
            </TokenArea>
            <FormattedHTMLMessage {...messages.description2} />
          </SpacedListItem>
          <SpacedListItem>
            <FormattedHTMLMessage {...messages.description3} />
            <FormattedMessage {...messages.description4} tagName="p" />
          </SpacedListItem>
        </InstructionsList>
        <TabButtonFooter>
          <AkButton onClick={this.props.toggleDialog} appearance="primary" id="verify-domain-button">
            {formatMessage(messages.verifyDomain)}
          </AkButton>
        </TabButtonFooter>
      </Description>
    );
  }

  private onClick = () => {
    const { formatMessage } = this.props.intl;
    this.textInput!.input.select();
    if (document.execCommand('copy')) {
      this.props.showFlag({
        autoDismiss: true,
        icon: <ActionsBarCheckCircleIcon label="" />,
        id: `organisation.domain.claim.tab.dns.success.flag.${Date.now()}`,
        title: formatMessage(messages.copySuccess),
        description: formatMessage(messages.copySuccessDescription),
      });
    } else {
      this.props.showFlag({
        autoDismiss: true,
        icon: createErrorIcon(),
        id: `organisation.domain.claim.tab.dns.error.flag.${Date.now()}`,
        title: formatMessage(errorFlagMessages.title),
        description: formatMessage(messages.copyFailDescription),
      });
    }
  }

  private setInput = (input: any) => {
    if (input) {
      this.textInput = input;
    }
  }
}

const query = graphql<any, DomainClaimTabTokenContentProps>(domainClaimQuery, { options: (props) => ({ variables: { id: props.orgId } }) });

export const DomainClaimTabTokenContent = compose(
  query,
  withFlag,
  injectIntl,
)(DomainClaimTabTokenContentImpl);
