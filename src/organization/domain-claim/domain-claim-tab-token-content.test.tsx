// tslint:disable jsx-use-translation-function
import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import AkButton from '@atlaskit/button';
import AkTextField from '@atlaskit/field-text';
import AkSpinner from '@atlaskit/spinner';

import { createErrorIcon } from 'common/error';

import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import { TokenArea } from './domain-claim-tab-components';
import { DomainClaimTabTokenContentImpl } from './domain-claim-tab-token-content';

describe('DomainClaimTabTokenContent', () => {

  const organizationDataWithToken = {
    organization: {
      domainClaim: {
        token: 'atlassiantoken123',
      },
    },
  };

  function statelessDomainClaimTabTokenContent(data) {
    return shallow(
      <DomainClaimTabTokenContentImpl
        orgId={'DUMMY-TEST-ORG-ID'}
        data={data}
        toggleDialog={() => void 0}
        showFlag={() => void 0}
        hideFlag={() => null}
        intl={createMockIntlProp()}
      />,
      createMockIntlContext(),
    );
  }

  function statelessMountedDomainClaimTabTokenContent(data, showFlag, toggleDialog?) {
    return mount(
      <DomainClaimTabTokenContentImpl
        orgId={'DUMMY-TEST-ORG-ID'}
        data={data}
        toggleDialog={toggleDialog}
        showFlag={showFlag}
        hideFlag={() => null}
        intl={createMockIntlProp()}
      />,
      createMockIntlContext(),
    );
  }

  describe('with no organization data', () => {

    describe('when the request has finished loading', () => {
      it('only the error should display', () => {
        const wrapper = statelessDomainClaimTabTokenContent({ loading: false });
        expect(wrapper.contains(<span>There was an error retrieving the DNS token.</span>)).to.equal(true);

        expect(wrapper.find(AkSpinner)).to.have.length(0);
        expect(wrapper.find(AkTextField)).to.have.length(0);
        expect(wrapper.find(AkButton)).to.have.length(0);
      });
    });

    describe('when the request has not finished loading', () => {
      it('the token area should not display', () => {
        const wrapper = statelessDomainClaimTabTokenContent({ loading: true });
        expect(wrapper.find(TokenArea)).to.have.length(0);
      });
    });
  });

  describe('with organization token data', () => {

    let showFlagSpy;
    let toggleDialogSpy;
    const sandbox = sinon.sandbox.create();

    beforeEach(() => {
      toggleDialogSpy = sandbox.spy();
      showFlagSpy = sandbox.spy();
    });

    afterEach(() => {
      sandbox.restore();
    });

    const wrapper = statelessDomainClaimTabTokenContent({ loading: false, ...organizationDataWithToken });

    it('spinner should not display', () => {
      const spinner = wrapper.find(AkSpinner);
      expect(spinner).to.have.length(0);
    });

    it('token area should display', () => {
      expect(wrapper.find(TokenArea)).to.have.length(1);
    });

    it('token description should display', () => {
      const text = wrapper.find('span');
      expect(text).to.have.length(1);
      expect(text.props().children).to.contain('Add the TXT record');
    });

    it('token copy button should display', () => {
      const button = wrapper.find(AkButton).at(0);
      expect(button).to.have.length(1);
      expect((button as any).props().children).to.equal('Copy');
    });

    it('token text field should display', () => {
      const textField = wrapper.find(AkTextField);
      expect(textField).to.have.length(1);
      expect(textField.props().isLabelHidden).to.equal(true);
      expect(textField.props().isReadOnly).to.equal(true);
      expect(textField.props().value).to.equal('atlassiantoken123');
    });

    it('verify domain button should display', () => {
      const button = wrapper.find(AkButton).at(1);
      expect(button.props().appearance).to.equal('primary');
      expect((button as any).props().children).to.equal('Verify domain');
    });

    it('failed copy should show an error flag', () => {
      const mountedWrapper = statelessMountedDomainClaimTabTokenContent({
        loading: false,
        ...organizationDataWithToken,
      }, showFlagSpy);
      const button = mountedWrapper.find(AkButton).at(0);
      expect(showFlagSpy.callCount).to.equal(0);
      button.simulate('click');
      expect(showFlagSpy.calledOnce).to.equal(true);
      expect(showFlagSpy.lastCall.args[0]).to.deep.include({
        autoDismiss: true,
        icon: createErrorIcon(),
        description: 'There was an issue copying the DNS token to the clipboard.',
      });
    });

    it('clicking verify domain button should open dialog', () => {
      const mountedWrapper = statelessMountedDomainClaimTabTokenContent({
        loading: false,
        ...organizationDataWithToken,
      }, showFlagSpy, toggleDialogSpy);
      const button = mountedWrapper.find(AkButton);
      expect(toggleDialogSpy.callCount).to.equal(0);
      button.at(1).simulate('click');
      expect(toggleDialogSpy.calledOnce).to.equal(true);
    });
  });

  describe('with old organization data', () => {

    // To test the case where we may have organization data but the loading prop is set to true.
    // We can end up in this state because we have organization data from a previous request, but it is
    // fetching new data. See: http://dev.apollodata.com/react/api-queries.html#graphql-query-data-loading
    const wrapper = statelessDomainClaimTabTokenContent({ loading: true, ...organizationDataWithToken });

    it('only the spinner should display', () => {
      expect(wrapper.find(AkSpinner)).to.have.length(1);
      expect(wrapper.find('span')).to.have.length(0);
      expect(wrapper.find(AkTextField)).to.have.length(0);
      expect(wrapper.find(AkButton)).to.have.length(0);
      expect(wrapper.find(TokenArea)).to.have.length(0);
    });
  });
});
