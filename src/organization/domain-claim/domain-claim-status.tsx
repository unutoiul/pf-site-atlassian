export enum DomainClaimStatus {
  Verified = 'VERIFIED',
  Deleted = 'DELETED',
  Unverified = 'UNVERIFIED',
  Superseded = 'SUPERSEDED',
  MissingToken = 'MISSING_TOKEN',
}
