import { ApolloError } from 'apollo-client';
import * as React from 'react';
import { ChildProps, graphql, MutationFunc } from 'react-apollo';
import { defineMessages, FormattedHTMLMessage, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps } from 'react-router';
import styled from 'styled-components';

import AkSpinner from '@atlaskit/spinner';
import { gridSize as akGridSize } from '@atlaskit/theme';

import {
  analyticsClient,
  AnalyticsClientProps,
  domainScreenEvent,
  ScreenEventSender,
  verifyDomainTrackEventData,
  withAnalyticsClient,
} from 'common/analytics';
import { BadRequestError, ConflictError, createErrorIcon, createSuccessIcon, errorFlagMessages } from 'common/error';
import { FlagProps, withFlag } from 'common/flag';
import { PageLayout } from 'common/page-layout';

import { links } from 'common/link-constants';

import { ClaimDomainMutation, DeleteDomainMutation, DeleteDomainMutationVariables, DomainClaimQuery, DomainClaimQueryVariables, VerificationType } from '../../schema/schema-types';
import { OrgBreadcrumbs } from '../breadcrumbs';
import { createOrgAnalyticsData } from '../organizations/organization-analytics';
import { OrgRouteProps } from '../routes';
import claimDomain from './claim-domain.mutation.graphql';
import deleteDomain from './delete-domain.mutation.graphql';
import { DomainClaimModal } from './domain-claim-modal';
import { DomainClaimPageSideBar } from './domain-claim-page-sidebar';
import { DomainClaimRemovalModal } from './domain-claim-removal-modal';
import { DomainClaimSuccessModal } from './domain-claim-success-modal';
import { DomainClaimTab } from './domain-claim-tab';
import { DomainClaimTable } from './domain-claim-table';
import getDomains from './get-domains.query.graphql';

// @atlaskit/tabs indents everything by 8px.
// This is a disgusting hack to move it back.
const StyledTab = styled.div`
  margin: -${akGridSize}px;
`;

interface State {
  activeDomainClaimPoll?: string;
  isClaimModalOpen: boolean;
  isClaimingDomain: boolean;
  isRemovalModalOpen: boolean;
  reverificationDomain?: string;
  verificationType?: VerificationType;
  isRemovingDomain: boolean;
  domainToRemove?: string;
  isDomainToRemoveVerified?: boolean;
  verifiedDomain?: string;
}

export const messages = defineMessages({
  title: {
    id: 'organization.domain.claim.list.title',
    defaultMessage: 'Domains',
  },
  description: {
    id: 'organization.domain.claim.list.description',
    defaultMessage: `Domains are used to determine which user accounts can be managed by your organization.
    You need to verify that you own a domain to be able to manage the accounts that use that domain in their email addresses.
    <a href="https://confluence.atlassian.com/x/gjcWN" target="_blank" rel="noopener noreferrer"> Learn more about domain verification.</a>`,
  },
  removedSuccessTitle: {
    id: 'organization.domain.claim.list.removed.success.title',
    defaultMessage: 'Successfully removed domain',
  },
  removedSuccess: {
    id: 'organization.domain.claim.list.removed.success',
    defaultMessage: '{domain} has been successfully removed.',
  },
  error: {
    id: 'organization.domain.claim.list.error',
    defaultMessage: 'Domain claim data could not be retrieved.',
  },
  claimFailedFormat: {
    id: 'organization.domain.claim.list.claim.failed.format',
    defaultMessage: 'The domain must be in a valid format.',
    description: 'Error message when a web domain name is not formatted correctly',
  },
  claimFailedTitle: {
    id: 'organization.domain.claim.list.claim.failed.title',
    defaultMessage: 'The domain could not be verified',
    description: 'Header for when the domain claim fails. Verifying a domain is the process of trying to determine that a user owns a specific internet domain name.',
  },
  claimFailed: {
    id: 'organization.domain.claim.list.claim.failed.dns',
    defaultMessage: 'DNS changes can take up to 72 hours to update depending on your DNS host. Please try again later.',
    description: 'Verifying a domain is the process of trying to determine that a user owns a specific internet domain name. The user chose to verify using DNS and it failed.  https://confluence.atlassian.com/x/gjcWN',
  },
  claimFailedHTTP: {
    id: 'organization.domain.claim.list.claim.failed.http',
    defaultMessage: 'There was an error trying to verify your domain via HTTPS. Check if your domain has a valid SSL certificate.',
    description: 'Verifying a domain is the process of trying to determine that a user owns a specific internet domain name. The user chose to verify using HTTPS and it failed.  https://confluence.atlassian.com/x/gjcWN',
  },
  learnMore: {
    id: 'organization.domain.claim.list.learn.more.message',
    defaultMessage: 'Learn more',
    description: 'It is a link that redirects to a confluence page about domain claim.',
  },
});

type Props = RemoveMutationProps & ChildProps<RouteComponentProps<OrgRouteProps> & FlagProps & InjectedIntlProps, DomainClaimQuery & ClaimDomainMutation> & AnalyticsClientProps;

export class DomainClaimPageImpl extends React.Component<Props, State> {

  public readonly state: Readonly<State> = {
    isClaimModalOpen: false,
    isClaimingDomain: false,
    isRemovalModalOpen: false,
    isRemovingDomain: false,
  };

  public componentWillReceiveProps(nextProps: Props) {
    const { data } = this.props;

    if (!data || data.loading !== true || !nextProps.data || nextProps.data.loading !== false || nextProps.data.error) {
      return;
    }

    analyticsClient.eventHandler(createOrgAnalyticsData({
      orgId: nextProps.match.params.orgId,
      action: 'pageLoad',
      actionSubject: 'organizationPageLoad',
      actionSubjectId: 'domainClaimPage',
    }));
  }

  public render() {
    const {
      intl: { formatMessage },
      data,
    } = this.props;

    return (
      <ScreenEventSender event={domainScreenEvent()}>
        <PageLayout title={formatMessage(messages.title)} description={<FormattedHTMLMessage {...messages.description} />} side={<DomainClaimPageSideBar />} breadcrumbs={<OrgBreadcrumbs />}>
          {!data || !data.loading && (data.error || !data.organization || !data.organization.domainClaim) && <span>{formatMessage(messages.error)}</span>}
          {data && data.loading && <AkSpinner />}
          {data && !data.loading && !data.error && data.organization &&
          <div>
            <StyledTab>
              <DomainClaimTab
                orgId={this.props.match.params.orgId}
                toggleDialogHandler={this.toggleDialogHandler}
              />
            </StyledTab>
            <DomainClaimTable
              domains={data.organization.domainClaim.domains || []}
              reverifyHandler={this.toggleDialogHandler}
              removeHandler={this.removeDomainHandler}
            />
            {this.state.isClaimModalOpen && (
              <DomainClaimModal
                orgId={this.props.match.params.orgId}
                isClaimingDomain={this.state.isClaimingDomain}
                verifyDomain={this.claimDomain}
                onClose={this.closeDomainClaimModal}
                reverificationDomain={this.state.reverificationDomain}
                verificationType={this.state.verificationType}
                domains={data.organization.domainClaim.domains || []}
              />
            )}
            <DomainClaimRemovalModal
              orgId={this.props.match.params.orgId}
              isOpen={this.state.isRemovalModalOpen}
              domain={this.state.domainToRemove!}
              verified={!!this.state.isDomainToRemoveVerified}
              removeDomain={this.removeDomain}
              isRemovingDomain={this.state.isRemovingDomain}
              toggleDialog={this.toggleRemovalDialog}
            />
            {this.state.verifiedDomain && (
              <DomainClaimSuccessModal
                orgId={this.props.match.params.orgId}
                onClose={this.closeDomainClaimSuccessModal}
                domain={this.state.verifiedDomain}
              />
            )}
          </div>
          }
        </PageLayout>
      </ScreenEventSender>
    );
  }

  private closeDomainClaimModal = () => {
    this.setState({ isClaimModalOpen: false });
  }

  private toggleRemovalDialog = () => {
    this.setState((prevState) => ({ isRemovalModalOpen: !prevState.isRemovalModalOpen }));
  }

  private closeDomainClaimSuccessModal = () => {
    this.setState({ verifiedDomain: undefined });
  }

  private claimDomain = (domain: string, method: VerificationType) => {
    if (!this.props.mutate) {
      this.claimDomainFailure();

      return;
    }

    this.setState({ activeDomainClaimPoll: domain, isClaimingDomain: true, verificationType: method });
    const {
      match: {
        params: {
          orgId,
        },
      },
      analyticsClient : { sendTrackEvent },
    } = this.props;
    this.props.mutate({
      variables: {
        id: orgId,
        domain,
        method,
      },
      refetchQueries: [{
        query: getDomains,
        variables: { id: orgId },
      }],
    }).then((response) => {
      sendTrackEvent({ data: verifyDomainTrackEventData(orgId, method) });
      this.completeDomainClaim(response.data.claimDomain);
    }).catch((error) => {
      this.claimDomainFailure(error);
    });
  }

  private completeDomainClaim(claimDomainData: ClaimDomainMutation['claimDomain']) {
    if (claimDomainData &&
      claimDomainData.status === 'FAILED') {
      this.claimDomainFailure();

      return;
    }

    // Need to set an intermediate variable, verifiedDomain if the claim is successful, because claimDomainCleanup clears the active domain poll.
    this.setState({
      verifiedDomain: this.state.activeDomainClaimPoll,
    });

    this.claimDomainCleanup();
  }

  private claimDomainCleanup() {
    this.setState({
      isClaimModalOpen: false,
      activeDomainClaimPoll: undefined,
    });
  }

  private claimDomainFailure(error?: ApolloError) {
    if (this.props.data) {
      this.props.data.refetch().catch(this.showRefetchErrorFlag);
    }
    this.props.showFlag({
      autoDismiss: true,
      icon: createErrorIcon(),
      id: `organization.domain.claim.list.failure.flag.${Date.now()}`,
      title: this.props.intl.formatMessage(messages.claimFailedTitle),
      description: this.getClaimDomainErrorMessage(error),
      actions: [
        {
          content: (
            <a href={links.external.domainClaim} target="_blank" rel="noopener noreferrer">
              <FormattedMessage {...messages.learnMore} />
            </a>
          ),
        },
      ],
    });
    this.claimDomainCleanup();
  }

  private getClaimDomainErrorMessage(error?: ApolloError) {
    const { formatMessage } = this.props.intl;

    if (error) {
      if (ConflictError.findIn(error).length > 0) {
        return this.getDomainClaimTypeFailureMessage();
      }
      if (BadRequestError.findIn(error).length > 0) {
        return formatMessage(messages.claimFailedFormat);
      }
    }

    return this.getDomainClaimTypeFailureMessage();
  }

  private getDomainClaimTypeFailureMessage() {
    const { verificationType } = this.state;

    return verificationType === 'dns' ? <FormattedMessage {...messages.claimFailed}/> : <FormattedMessage {...messages.claimFailedHTTP} />;
  }

  private toggleDialogHandler = (verificationType?: VerificationType, reverificationDomain?: string) => () => {
    this.setState((prevState) => ({
      isClaimModalOpen: !prevState.isClaimModalOpen,
      isClaimingDomain: false,
      verificationType,
      reverificationDomain,
    }));
  };

  private removeDomainHandler = (domainToRemove: string, isDomainToRemoveVerified: boolean) => () => {
    this.setState({ isRemovalModalOpen: true, domainToRemove, isDomainToRemoveVerified });
  }

  private removeDomain = () => {
    if (!this.state.domainToRemove) {
      return;
    }

    this.setState({ isRemovingDomain: true });
    this.props.remove({
      variables: {
        id: this.props.match.params.orgId,
        domain: this.state.domainToRemove,
      },
      refetchQueries: [{
        query: getDomains,
        variables: { id: this.props.match.params.orgId },
      }],
    }).then(() => {
      this.props.showFlag({
        autoDismiss: true,
        icon: createSuccessIcon(),
        id: `organization.domain.claim.list.success.flag.${Date.now()}`,
        title: this.props.intl.formatMessage(messages.removedSuccessTitle),
        description: this.props.intl.formatMessage(messages.removedSuccess, { domain: this.state.domainToRemove || '' }),
      });
      this.setState({ isRemovingDomain: false });
      this.toggleRemovalDialog();
    }).catch((error) => {
      this.setState({ isRemovingDomain: false });
      this.toggleRemovalDialog();
      if (this.props.data) {
        this.props.data.refetch().catch(this.showRefetchErrorFlag);
      }
      analyticsClient.onError(error);
    });
  };

  private showRefetchErrorFlag = () => {
    this.props.showFlag({
      autoDismiss: true,
      icon: createErrorIcon(),
      id: `organization.domain.claim.refetch.failure.flag.${Date.now()}`,
      title: this.props.intl.formatMessage(errorFlagMessages.title),
      description: this.props.intl.formatMessage(errorFlagMessages.description),
    });
  }
}

const mutation = graphql<RouteComponentProps<OrgRouteProps>, ClaimDomainMutation>(claimDomain);
interface RemoveMutationProps {
  remove: MutationFunc<DeleteDomainMutation, DeleteDomainMutationVariables>;
}
const removeMutation = graphql<RouteComponentProps<OrgRouteProps>, DeleteDomainMutation, DeleteDomainMutationVariables, RemoveMutationProps>(deleteDomain, { name: 'remove' });
const query = graphql<RouteComponentProps<OrgRouteProps>, DomainClaimQuery, DomainClaimQueryVariables>(getDomains, {
  options: (props) => ({ variables: { id: props.match.params.orgId } }),
});

export const DomainClaimPage =
  query(
    mutation(
      removeMutation(
        withFlag(
          injectIntl(
            withAnalyticsClient(
              DomainClaimPageImpl,
            ),
          ),
        ),
      ),
    ),
  );
