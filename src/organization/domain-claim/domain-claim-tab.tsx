import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import AkTabs from '@atlaskit/tabs';

import { VerificationType } from '../../schema/schema-types';
import { DomainClaimTabHttpContent } from './domain-claim-tab-http-content';
import { DomainClaimTabTokenContent } from './domain-claim-tab-token-content';

export interface DomainClaimTabProps {
  orgId: string;
  toggleDialogHandler(method: VerificationType): () => void;
}

const messages = defineMessages({
  tokenLabel: {
    id: 'organization.domain.claim.tab.token.label',
    defaultMessage: 'DNS',
  },
  httpLabel: {
    id: 'organization.domain.claim.tab.http.label',
    defaultMessage: 'HTTPS',
  },
  error: {
    id: 'organization.domain.claim.tab.error',
    defaultMessage: 'No domain claim record information found.',
  },
});

export class DomainClaimTabImpl extends React.PureComponent<DomainClaimTabProps & InjectedIntlProps> {
  public render() {
    return (
        <AkTabs tabs={this.fetchTabs()} />
    );
  }

  private fetchTabs() {
    return [
      {
        label: this.props.intl.formatMessage(messages.tokenLabel),
        content: <DomainClaimTabTokenContent orgId={this.props.orgId} toggleDialog={this.props.toggleDialogHandler('dns')} />,
        defaultSelected: true,
      },
      {
        label: this.props.intl.formatMessage(messages.httpLabel),
        content: <DomainClaimTabHttpContent orgId={this.props.orgId} toggleDialog={this.props.toggleDialogHandler('http')} />,
        defaultSelected: false,
      },
    ];
  }
}

export const DomainClaimTab: React.ComponentClass<DomainClaimTabProps> = injectIntl(DomainClaimTabImpl);
