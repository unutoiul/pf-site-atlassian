// tslint:disable jsx-use-translation-function
import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import * as React from 'react';
import { FormattedHTMLMessage } from 'react-intl';
import * as sinon from 'sinon';

import AkButton from '@atlaskit/button';

import { ActionsBarCheckCircleIcon } from 'common/actions-bar';

import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';

import { DomainClaimTabHttpContentImpl } from '../domain-claim';
import { TokenArea } from './domain-claim-tab-components';

describe('DomainClaimTabHttpContent', () => {

  let toggleDialogSpy;
  let showFlagSpy;
  const sandbox = sinon.sandbox.create();

  beforeEach(() => {
    toggleDialogSpy = sandbox.spy();
    showFlagSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  function statelessDomainClaimTabHttpContent() {
    return shallow(
      <DomainClaimTabHttpContentImpl
        orgId={'DUMMY-TEST-ORG-ID'}
        toggleDialog={toggleDialogSpy}
        showFlag={showFlagSpy}
        hideFlag={() => null}
        intl={createMockIntlProp()}
      />,
      createMockIntlContext(),
    );
  }

  function statelessMountedDomainClaimTabHttpContent() {
    return mount(
      <DomainClaimTabHttpContentImpl
        orgId={'DUMMY-TEST-ORG-ID'}
        toggleDialog={toggleDialogSpy}
        showFlag={showFlagSpy}
        hideFlag={() => null}
        intl={createMockIntlProp()}
      />,
      createMockIntlContext(),
    );
  }

  const wrapper = statelessDomainClaimTabHttpContent();

  it('token area should display', () => {
    expect(wrapper.find(TokenArea)).to.have.length(1);
  });

  it('token description and file text should display', () => {
    const text = wrapper.find(FormattedHTMLMessage) as any;
    expect(text).to.have.length(3);
    expect(text.at(0).props().defaultMessage).to.contain('Download the verification file');
    expect(text.at(1).props().defaultMessage).to.contain('<b>atlassian-domain-verification.html</b>');
    expect(text.at(2).props().defaultMessage).to.contain('Go to your server and upload the file');
  });

  it('download file button should display', () => {
    const button = wrapper.find(AkButton);
    expect(button).to.have.length(2);
    expect((button.at(0) as any).props().children).to.equal('Download file');
  });

  it('verify domain button should display', () => {
    const button = wrapper.find(AkButton).at(1);
    expect(button.props().appearance).to.equal('primary');
    expect((button as any).props().children).to.equal('Verify domain');
  });

  it('clicking download button should show a success flag', () => {
    const mountedWrapper = statelessMountedDomainClaimTabHttpContent();
    const button = mountedWrapper.find(AkButton).at(0);
    expect(showFlagSpy.callCount).to.equal(0);
    button.simulate('click');
    expect(showFlagSpy.calledOnce).to.equal(true);
    expect(showFlagSpy.lastCall.args[0]).to.deep.include({
      autoDismiss: true,
      icon: <ActionsBarCheckCircleIcon label="" />,
      title: 'Verification file is being downloaded',
      description: 'Upload the verification file to your web server.',
    });
  });

  it('clicking verify domain button should open dialog', () => {
    const mountedWrapper = statelessMountedDomainClaimTabHttpContent();
    const button = mountedWrapper.find(AkButton);
    expect(toggleDialogSpy.callCount).to.equal(0);
    button.at(1).simulate('click');
    expect(toggleDialogSpy.calledOnce).to.equal(true);
  });
});
