import styled from 'styled-components';

import { akColorN200 } from '@atlaskit/util-shared-styles';

import { gridSize as akGridSize } from '@atlaskit/theme';

export const InstructionsList = styled.ol`
  list-style-position: inside;
  padding-left: 0;
  padding-bottom: ${akGridSize() * 2}px;
`;

export const TokenComment = styled.div`
  font-size: 12px;
  padding-top: ${akGridSize}px;
  color: ${akColorN200};
`;

export const TokenActions = styled.div`
  align-items: center;
  display: flex;
`;

export const TokenArea = styled.div`
  width: ${akGridSize() * 45}px;
  padding-bottom: ${akGridSize() * 2}px;
  padding-top: ${akGridSize}px;
`;

export const TokenField = styled.div`
  flex-grow: 2;
`;

export const TokenButton = styled.div`
  padding: 0 0 0 ${akGridSize() * 0.5}px;
  button, a:link, a:visited, a:hover, a:active  {
    height: ${akGridSize() * 5}px;
  }
`;

export const TabButtonFooter = styled.div`
  margin-bottom: auto;
`;

export const SpacedListItem = styled.li`
  margin-bottom: ${akGridSize() * 2}px;
`;
