import * as React from 'react';
import { graphql, OptionProps, QueryResult } from 'react-apollo';
import { defineMessages, FormattedHTMLMessage, FormattedMessage } from 'react-intl';
import { withRouter } from 'react-router-dom';

import AkButton, { ButtonGroup as AkButtonGroup } from '@atlaskit/button';

import { ModalDialog } from 'common/modal';
import { RouterLink } from 'common/navigation';

import { DomainClaimSuccessQuery, DomainClaimSuccessQueryVariables } from '../../schema/schema-types';
import domainClaimSuccessQuery from './domain-claim-success.query.graphql';

export interface DomainClaimSuccessModalProps {
  orgId: string;
  domain: string;
  onClose(): void;
}

export const messages = defineMessages({
  verifiedDomain: {
    id: 'organization.domain.claim.success.modal.verified.title',
    defaultMessage: 'We\'ve verified your domain',
    description: 'After the user successfully verifies a domain, we will show this success message',
  },
  modalBody: {
    id: 'organization.domain.claim.success.modal.modal.body',
    defaultMessage: `{totalCount, plural,
      one {<strong>{totalCount} account</strong> uses the <strong>{domainName}</strong> domain.}
      other {<strong>{totalCount} accounts</strong> use the <strong>{domainName}</strong> domain.}
    }`,
    description: 'Tells the user how many users are using the domain.  We want to make sure the plural translations are taken into consideration across multiple languages.  Refer to the plural format and select the translation categories applicable for your target language. See https://formatjs.io/guides/message-syntax/#plural-format',
  },
  cancel: {
    id: 'organization.domain.claim.success.modal.close',
    defaultMessage: 'Close',
    description: 'Closes the modal',
  },
  managedAccounts: {
    id: 'organization.domain.claim.success.modal.managed.accounts',
    defaultMessage: 'View managed accounts',
    description: 'A button that upon clicking, will redirect the user to their managed accounts',
  },
  body: {
    id: 'organization.domain.claim.success.modal.main.body',
    defaultMessage: 'View the accounts to see which sites and products they can access, and to manage them globally.',
    description: 'When an admin verifies a domain, they will see this, prompting them that they can now manage users in one place',
  },
  errorBody: {
    id: 'organization.domain.claim.success.modal.error.body',
    defaultMessage: 'We\'ve verified the <strong>{domainName}</strong> domain.',
    description: 'In the event that the user count fails, we still want to notify the user that the domain claim was successful',
  },
});

interface DerivedProps {
  loading: boolean;
  error?: QueryResult['error'];
  totalMembers?: number;
}

export class DomainClaimSuccessModalImpl extends React.Component<DomainClaimSuccessModalProps & DerivedProps> {

  public render() {
    const {
      onClose,
      loading,
    } = this.props;

    if (loading) {
      return null;
    }

    return (
      <ModalDialog
        width={'small'}
        header={<FormattedMessage {...messages.verifiedDomain} />}
        footer={this.getFooter()}
        isOpen={true}
        onClose={onClose}
      >
        {this.modalDialogContent()}
      </ModalDialog>
    );

  }
  private modalDialogContent = () => {
    return (
      <React.Fragment>
          {this.getUserCountAndDomain()}
          {<FormattedMessage {...messages.body} tagName="p"/>}
      </React.Fragment>
    );
  };

  private getFooter = () => {
    const {
      onClose,
      orgId,
      domain,
    } = this.props;

    return (
      <AkButtonGroup>
        <AkButton
          appearance="primary"
          href={`/o/${orgId}/members?domain=${domain}`}
          component={RouterLink}
        >
          <FormattedMessage {...messages.managedAccounts} />
        </AkButton>
        <AkButton onClick={onClose} appearance="subtle-link">
          <FormattedMessage {...messages.cancel} />
        </AkButton>
      </AkButtonGroup>
    );
  }

  private getUserCountAndDomain = () => {
    const {
      domain,
      totalMembers,
      error,
    } = this.props;

    if (error) {
      return <FormattedHTMLMessage {...messages.errorBody} values={{ domainName: domain }} />;
    }

    if (totalMembers === undefined || totalMembers === null) {
      return null;
    }

    return <FormattedHTMLMessage {...messages.modalBody} values={{ totalCount: totalMembers, domainName: domain }}/>;
  };
}

function getDomainClaimSuccessProps(componentProps: OptionProps<{}, DomainClaimSuccessQuery>): DerivedProps {
  if (!componentProps || !componentProps.data || componentProps.data.loading || !componentProps.data.organization) {
    return { loading: true };
  }

  if (componentProps.data.error) {
    return { loading: false, error: componentProps.data.error };
  }

  const {
    users: {
      members,
    },
  } = componentProps.data.organization;

  return {
    loading: false,
    totalMembers: !members ? 0 : members.total,
  };
}

const query = graphql<DomainClaimSuccessModalProps, DomainClaimSuccessQuery, DomainClaimSuccessQueryVariables, DerivedProps>(domainClaimSuccessQuery, {
  options: (componentProps) => ({
    variables: { id: componentProps.orgId, domain: componentProps.domain },
  }),
  props: getDomainClaimSuccessProps,
  skip: ({ orgId }) => !orgId,
});

export const DomainClaimSuccessModal =
  withRouter(
    query(DomainClaimSuccessModalImpl),
  );
