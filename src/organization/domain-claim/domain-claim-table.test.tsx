// tslint:disable jsx-use-translation-function
import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import AkButton from '@atlaskit/button';
import AkDynamicTable from '@atlaskit/dynamic-table';

import { createMockIntlProp } from '../../utilities/testing';
import { DomainClaimTableImpl, DomainClaimTableProps } from './domain-claim-table';

describe('DomainClaimTable', () => {
  const reverifyHandler = sinon.spy((_type, _domain) => sinon.spy());
  const removeHandler = sinon.spy((_domain, _type) => sinon.spy());

  const domainJSON = {
    domains: [
      {
        domain: 'cheese.com',
        verified: true,
        status: 'VERIFIED',
        verificationType: 'dns',
      },
      {
        domain: 'brie.com',
        verified: false,
        status: 'UNVERIFIED',
      },
      {
        domain: 'cheddar.com',
        verified: false,
        status: 'MISSING_TOKEN',
        verificationType: 'http',
      },
    ],
    reverifyHandler,
    removeHandler,
  };

  beforeEach(() => {
    reverifyHandler.reset();
    removeHandler.reset();
  });

  it('does not render any children', () => {
    const page = shallow(
      <DomainClaimTableImpl
        domains={[]}
        reverifyHandler={reverifyHandler}
        removeHandler={removeHandler}
        intl={createMockIntlProp()}
      >
        <div className="child" />
      </DomainClaimTableImpl>,
    );
    expect(page.find('.child').length).to.equal(0);
    expect(reverifyHandler.callCount).to.equal(0);
    expect(removeHandler.callCount).to.equal(0);
  });

  it('headings and caption should always display', () => {
    const wrapper = shallow(
      <DomainClaimTableImpl
        domains={domainJSON.domains as any}
        reverifyHandler={reverifyHandler}
        removeHandler={removeHandler}
        intl={createMockIntlProp()}
      />,
    );
    const headings = [
      {
        content: 'Domain',
        key: 'domain',
      },
      {
        content: 'Status',
        key: 'status',
      },
      {
        content: 'Verification type',
        key: 'verificationType',
      },
      {
        content: 'Actions',
        key: 'actions',
        style: {
          paddingLeft: '28px',
        },
      },
    ];
    const table = wrapper.find(AkDynamicTable);
    expect(table).to.have.length(1);
    expect(table.props().caption).to.deep.equal('Verified domains');
    expect(table.props().head.cells).to.deep.equal(headings);
  });

  describe('with no domain claims', () => {

    it('no rows should be in the table', () => {
      const wrapper = shallow(
        <DomainClaimTableImpl
          domains={[]}
          reverifyHandler={reverifyHandler}
          removeHandler={removeHandler}
          intl={createMockIntlProp()}
        />,
      );
      const table = wrapper.find(AkDynamicTable);
      expect(table).to.have.length(0);
    });
  });

  describe('with domain claims', () => {

    function shallowWrap(props?: DomainClaimTableProps) {
      return shallow(
        <DomainClaimTableImpl
          intl={createMockIntlProp()}
          {...props as any}
        />,
      );
    }

    function mountWrap(props?: DomainClaimTableProps) {
      return mount(
        <DomainClaimTableImpl
          intl={createMockIntlProp()}
          {...props as any}
        />,
      );
    }

    it('row should contain domain claim info with domains', () => {
      const wrapper = shallowWrap(domainJSON as any);
      const table = wrapper.find(AkDynamicTable);
      expect(table).to.have.length(1);

      const row = table.props().rows;
      expect(row).to.have.length(3);
      expect(reverifyHandler.calledTwice).to.equal(true);
      expect(removeHandler.calledThrice).to.equal(true);

      expect(removeHandler.firstCall.args[0]).to.deep.equal('cheese.com');
      expect(removeHandler.secondCall.args[0]).to.deep.equal('brie.com');
      expect(removeHandler.thirdCall.args[0]).to.deep.equal('cheddar.com');
      expect(removeHandler.firstCall.args[1]).to.deep.equal(true);
      expect(removeHandler.secondCall.args[1]).to.deep.equal(false);
      expect(removeHandler.thirdCall.args[1]).to.deep.equal(false);

      expect(row[0].cells[0].content.props.children).to.deep.equal('cheese.com');
      expect(row[0].cells[1].content.props.appearance).to.deep.equal('success');
      expect(row[0].cells[1].content.props.children).to.deep.equal('VERIFIED');
      expect(row[0].cells[2].content).to.deep.equal('DNS');
      expect(row[0].cells[3].content.props.children[1].props.appearance).to.deep.equal('subtle-link');
      expect(row[0].cells[3].content.props.children[1].props.children).to.deep.equal('Remove');

      expect(row[1].cells[0].content.props.children).to.deep.equal('brie.com');
      expect(row[1].cells[1].content.props.appearance).to.deep.equal('removed');
      expect(row[1].cells[1].content.props.children).to.deep.equal('UNVERIFIED');
      expect(row[1].cells[2].content).to.equal(null);
      expect(row[1].cells[3].content.props.children[0].props.appearance).to.deep.equal('link');
      expect(row[1].cells[3].content.props.children[0].props.children).to.deep.equal('Verify');
      expect(row[1].cells[3].content.props.children[1].props.appearance).to.deep.equal('subtle-link');
      expect(row[1].cells[3].content.props.children[1].props.children).to.deep.equal('Remove');
      expect(reverifyHandler.firstCall.args[1]).to.deep.equal('brie.com');
      expect(reverifyHandler.firstCall.args[0]).to.equal(undefined);

      expect(row[2].cells[0].content.props.children).to.deep.equal('cheddar.com');
      expect(row[2].cells[1].content.props.appearance).to.deep.equal('removed');
      expect(row[2].cells[1].content.props.children).to.deep.equal('MISSING_TOKEN');
      expect(row[2].cells[2].content).to.deep.equal('HTTPS');
      expect(row[2].cells[3].content.props.children[0].props.appearance).to.deep.equal('link');
      expect(row[2].cells[3].content.props.children[0].props.children).to.deep.equal('Verify');
      expect(row[2].cells[3].content.props.children[1].props.appearance).to.deep.equal('subtle-link');
      expect(row[2].cells[3].content.props.children[1].props.children).to.deep.equal('Remove');
      expect(reverifyHandler.secondCall.args[1]).to.deep.equal('cheddar.com');
      expect(reverifyHandler.secondCall.args[0]).to.deep.equal('http');
    });

    it('clicking on Verify should call supplied reverify function handler', () => {
      const wrapper = mountWrap(domainJSON as any);
      const table = wrapper.find(AkDynamicTable);
      expect(table).to.have.length(1);

      const row = table.props().rows;
      expect(row).to.have.length(3);
      expect(reverifyHandler.calledTwice).to.equal(true);

      expect(row[2].cells[0].content.props.children).to.deep.equal('cheddar.com');
      expect(row[2].cells[1].content.props.appearance).to.deep.equal('removed');
      expect(row[2].cells[1].content.props.children).to.deep.equal('MISSING_TOKEN');
      expect(row[2].cells[2].content).to.deep.equal('HTTPS');
      expect(row[2].cells[3].content.props.children[0].props.appearance).to.deep.equal('link');
      expect(row[2].cells[3].content.props.children[0].props.children).to.deep.equal('Verify');
      expect(row[2].cells[3].content.props.children[1].props.children).to.deep.equal('Remove');
      expect(reverifyHandler.secondCall.args[1]).to.deep.equal('cheddar.com');
      expect(reverifyHandler.secondCall.args[0]).to.deep.equal('http');

      const handler = reverifyHandler.secondCall.returnValue;
      expect(handler.notCalled).to.equal(true);
      const buttons = table.find(AkButton);
      expect(buttons).to.have.length(5);
      buttons.at(3).simulate('click');
      expect(handler.calledOnce).to.equal(true);
    });

    it('clicking on Delete should call supplied delete function handler', () => {
      const wrapper = mountWrap(domainJSON as any);
      const table = wrapper.find(AkDynamicTable);
      expect(table).to.have.length(1);

      const row = table.props().rows;
      expect(row).to.have.length(3);
      expect(reverifyHandler.calledTwice).to.equal(true);

      expect(row[2].cells[0].content.props.children).to.deep.equal('cheddar.com');
      expect(row[2].cells[1].content.props.appearance).to.deep.equal('removed');
      expect(row[2].cells[1].content.props.children).to.deep.equal('MISSING_TOKEN');
      expect(row[2].cells[2].content).to.deep.equal('HTTPS');
      expect(row[2].cells[3].content.props.children[0].props.appearance).to.deep.equal('link');
      expect(row[2].cells[3].content.props.children[0].props.children).to.deep.equal('Verify');
      expect(row[2].cells[3].content.props.children[1].props.children).to.deep.equal('Remove');
      expect(reverifyHandler.secondCall.args[1]).to.deep.equal('cheddar.com');
      expect(reverifyHandler.secondCall.args[0]).to.deep.equal('http');

      expect(removeHandler.thirdCall.args[0]).to.deep.equal('cheddar.com');
      expect(removeHandler.thirdCall.args[1]).to.deep.equal(false);

      const handler = removeHandler.thirdCall.returnValue;
      expect(handler.notCalled).to.equal(true);
      const buttons = table.find(AkButton);
      expect(buttons).to.have.length(5);
      buttons.at(4).simulate('click');
      expect(handler.calledOnce).to.equal(true);
    });
  });
});
