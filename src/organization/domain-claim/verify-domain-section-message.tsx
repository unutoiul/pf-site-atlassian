import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';
import styled from 'styled-components';

import AkLock from '@atlaskit/icon/glyph/lock-filled';
import AkSectionMessage from '@atlaskit/section-message';

import { OrgRouteProps } from '../routes';

const messages = defineMessages({
  title: {
    id: 'organizations.page.evaluation.title',
    defaultMessage: 'You need to verify a domain',
    description: 'domain is a web domain like www.atlassian.com and verifiying is proving that you own it',
  },
  link: {
    id: 'organizations.page.evaluation.link',
    defaultMessage: 'Verify domain',
    description: 'domain is a web domain like www.atlassian.com and verifiying is proving that you own it',
  },
});

export const Container = styled.div`
  text-align: left;
`;

interface VarifyDomainSectionMessageProps {
  domainPrompt: string;
}

class VerifyDomainSectionMessageImpl extends React.Component<VarifyDomainSectionMessageProps & InjectedIntlProps & RouteComponentProps<OrgRouteProps>> {

  public render() {
    const { formatMessage } = this.props.intl;

    return (
      <Container>
        <AkSectionMessage
          title={formatMessage(messages.title)}
          actions={[{
            onClick: this.navigateToDomainPage,
            text: formatMessage(messages.link),
          }]}
          icon={AkLock}
        >
          {this.props.domainPrompt}
        </AkSectionMessage>
      </Container>
    );
  }

  private navigateToDomainPage = () => {
    const { history, match: { params: { orgId } } } = this.props;

    history.push(`/o/${orgId}/domains`);
  }
}

export const VerifyDomainSectionMessage = withRouter<VarifyDomainSectionMessageProps>(injectIntl(VerifyDomainSectionMessageImpl));
