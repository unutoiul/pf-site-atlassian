// tslint:disable jsx-use-translation-function
import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import AkButton from '@atlaskit/button';

import { Button as AnalyticsButton } from 'common/analytics';
import { ModalDialog } from 'common/modal';

import { createMockIntlProp } from '../../utilities/testing';
import { DomainClaimRemovalModalImpl } from './domain-claim-removal-modal';

describe('DomainClaimRemovalModal', () => {
  let removeDomain;
  let toggleDialog;
  const sandbox = sinon.sandbox.create();

  beforeEach(() => {
    removeDomain = sandbox.spy();
    toggleDialog = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  function shallowDomainClaimRemovalModal(isOpen, isRemovingDomain?, domain?, isVerified?) {
    return shallow(
      <DomainClaimRemovalModalImpl
        orgId={'DUMMY-TEST-ORG-ID'}
        isOpen={isOpen}
        isRemovingDomain={isRemovingDomain}
        domain={domain}
        removeDomain={removeDomain}
        toggleDialog={toggleDialog}
        verified={isVerified}
        intl={createMockIntlProp()}
        analyticsClient={{ sendUIEvent: () => undefined } as any}
      />,
    );
  }

  describe('should contain correct content', () => {

    it('Product mentioned should be atlassian access', () => {
      const wrapper = shallowDomainClaimRemovalModal(true, false, 'verified.domain.com', true);
      const modal = wrapper.find(ModalDialog);
      const description = modal.find('p');
      expect(description).to.have.length(1);
      expect(description.text()).to.contain('Atlassian Access');
    });

    it('if domain is verified then description should be shown', () => {
      const wrapper = shallowDomainClaimRemovalModal(true, false, 'verified.domain.com', true);
      const modal = wrapper.find(ModalDialog);

      expect(modal.props().isOpen).to.equal(true);
      expect(modal.props().header).to.equal('Remove verified.domain.com?');

      const description = modal.find('p');
      const modalFooter = shallow(modal.props().footer as React.ReactElement<any>);
      const loadingButton = modalFooter.find(AnalyticsButton);
      const button = modalFooter.find(AkButton);

      expect(description).to.have.length(1);
      expect(loadingButton).to.have.length(1);
      expect(button).to.have.length(1);

      expect(description.text()).to.contain('Once you remove this domain');
      expect(loadingButton.children().text()).to.deep.equal('Remove');
      expect(loadingButton.props().appearance).to.deep.equal('danger');
      expect(button.children().text()).to.deep.equal('Cancel');
      expect(button.props().appearance).to.deep.equal('subtle-link');

      expect(loadingButton.props().analyticsData).to.deep.equal({
        action: 'click',
        actionSubject: 'removeDomainButton',
        actionSubjectId: 'remove',
        subproduct: 'organization',
        tenantId: 'DUMMY-TEST-ORG-ID',
        tenantType: 'organizationId',
        attributes: {
          verified: true,
        },
      });
    });

    it('if domain is not verified then no description should be shown', () => {
      const wrapper = shallowDomainClaimRemovalModal(true, false, 'unverified.example.com', false);
      const modal = wrapper.find(ModalDialog);

      expect(modal.props().isOpen).to.equal(true);
      expect(modal.props().header).to.equal('Remove unverified.example.com?');

      const description = modal.find('p');
      const modalFooter = shallow(modal.props().footer as React.ReactElement<any>);
      const loadingButton = modalFooter.find(AnalyticsButton);
      const button = modalFooter.find(AkButton);

      expect(description).to.have.length(0);
      expect(loadingButton).to.have.length(1);
      expect(button).to.have.length(1);

      expect(loadingButton.children().text()).to.deep.equal('Remove');
      expect(loadingButton.props().appearance).to.deep.equal('danger');
      expect(button.children().text()).to.deep.equal('Cancel');
      expect(button.props().appearance).to.deep.equal('subtle-link');

      expect(loadingButton.props().analyticsData).to.deep.equal({
        action: 'click',
        actionSubject: 'removeDomainButton',
        actionSubjectId: 'remove',
        subproduct: 'organization',
        tenantId: 'DUMMY-TEST-ORG-ID',
        tenantType: 'organizationId',
        attributes: {
          verified: false,
        },
      });
    });
  });

  describe('button actions', () => {

    it('when sending a request to remove a domain the button should be disabled ', () => {
      const wrapper = shallowDomainClaimRemovalModal(true, true);
      const modal = wrapper.find(ModalDialog);
      const modalFooter = shallow(modal.props().footer as React.ReactElement<any>);
      const loadingButton = modalFooter.find(AnalyticsButton);
      expect(loadingButton.props().isDisabled).to.equal(true);
    });

    it('clicking remove should trigger verifyDomain', () => {
      const wrapper = shallowDomainClaimRemovalModal(true, false);
      const modal = wrapper.find(ModalDialog);
      const modalFooter = shallow(modal.props().footer as React.ReactElement<any>);
      const loadingButton = modalFooter.find(AnalyticsButton);
      expect(removeDomain.callCount).to.equal(0);
      loadingButton.simulate('click');
      expect(removeDomain.callCount).to.equal(1);
    });

    it('clicking on the cancel button should trigger cancel action', () => {
      const wrapper = shallowDomainClaimRemovalModal(true, false);
      const modal = wrapper.find(ModalDialog);
      const modalFooter = shallow(modal.props().footer as React.ReactElement<any>);
      const button = modalFooter.find(AkButton);
      expect(toggleDialog.callCount).to.equal(0);
      button.simulate('click');
      expect(toggleDialog.callCount).to.equal(1);
    });
  });
});
