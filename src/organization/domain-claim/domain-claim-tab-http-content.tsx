import * as React from 'react';
import { defineMessages, FormattedHTMLMessage, InjectedIntlProps, injectIntl } from 'react-intl';

import AkButton from '@atlaskit/button';

import { ActionsBarCheckCircleIcon } from 'common/actions-bar';
import { getConfig } from 'common/config';
import { FlagProps, withFlag } from 'common/flag';
import { Description } from 'common/tab-content';

import { InstructionsList, TabButtonFooter, TokenActions, TokenArea, TokenButton, TokenComment, TokenField } from './domain-claim-tab-components';

export interface DomainClaimTabHttpContentProps {
  orgId: string;
  toggleDialog(): void;
}

const messages = defineMessages({
  description1: {
    id: 'organization.domain.claim.tab.http.description1',
    defaultMessage: `Download the verification file.`,
  },
  description2: {
    id: 'organization.domain.claim.tab.http.description2',
    defaultMessage: `Go to your server and upload the file to the root folder of your domain's website. If you
    have difficulties with this process, ask your IT department for help.`,
  },
  fieldUnderLabel: {
    id: 'organization.domain.claim.tab.http.field.under.label',
    defaultMessage: `The same file can be used for multiple domains. It includes information that will allow us
    to verify that you own the domain.`,
  },
  tokenLabel: {
    id: 'organization.domain.claim.tab.http.token.label',
    defaultMessage: 'Token',
  },
  download: {
    id: 'organization.domain.claim.tab.http.download',
    defaultMessage: 'Download file',
  },
  verifyDomain: {
    id: 'organization.domain.claim.tab.http.verify.domain',
    defaultMessage: 'Verify domain',
  },
  downloadingTitle: {
    id: 'organization.domain.claim.tab.http.downloading.title',
    defaultMessage: 'Verification file is being downloaded',
  },
  downloadingMessage: {
    id: 'organization.domain.claim.tab.http.downloading.message',
    defaultMessage: 'Upload the verification file to your web server.',
  },
  verification: {
    id: 'organization.domain.claim.tab.http.verification.file',
    defaultMessage: '<b>atlassian-domain-verification.html</b>',
  },
});

export class DomainClaimTabHttpContentImpl extends React.Component<DomainClaimTabHttpContentProps & FlagProps & InjectedIntlProps> {
  public render() {
    const { formatMessage } = this.props.intl;

    return (
      <Description>
        <InstructionsList>
          <li>
            <FormattedHTMLMessage {...messages.description1} />
            <TokenArea>
              <TokenActions>
                <TokenField>
                  <FormattedHTMLMessage {...messages.verification} />
                </TokenField>
                <TokenButton>
                  <AkButton
                    href={`${getConfig().orgUrl}/${this.props.orgId}/domainClaims/tokenFile`}
                    appearance="default"
                    onClick={this.startDownloadFlag}
                    id="http-domain-claim-download-file-button"
                  >
                    {formatMessage(messages.download)}
                  </AkButton>
                </TokenButton>
              </TokenActions>
              <TokenComment>
                {formatMessage(messages.fieldUnderLabel)}
              </TokenComment>
            </TokenArea>
          </li>
          <li>
            <FormattedHTMLMessage {...messages.description2} />
          </li>
        </InstructionsList>
        <TabButtonFooter>
          <AkButton onClick={this.props.toggleDialog} appearance="primary" id="verify-domain-button">
            {formatMessage(messages.verifyDomain)}
          </AkButton>
        </TabButtonFooter>
      </Description>
    );
  }

  private startDownloadFlag = () => {
    const { formatMessage } = this.props.intl;
    this.props.showFlag({
      autoDismiss: true,
      icon: <ActionsBarCheckCircleIcon label="" />,
      id: `organisation.domain.claim.tab.http.success.flag.${Date.now()}`,
      title: formatMessage(messages.downloadingTitle),
      description: formatMessage(messages.downloadingMessage),
    });
  }
}

export const DomainClaimTabHttpContent = withFlag(injectIntl(DomainClaimTabHttpContentImpl));
