import { ApolloError } from 'apollo-client';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { FormattedHTMLMessage, FormattedMessage } from 'react-intl';
import * as sinon from 'sinon';

import { ConflictError } from 'common/error';
import { ModalDialog } from 'common/modal';

import { DomainClaimSuccessModalImpl, messages } from './domain-claim-success-modal';

describe('DomainClaimSuccessModal', () => {
  let toggleDialogSpy;

  const membersTotalDerivedResponse = 123;
  const sandbox = sinon.sandbox.create();

  beforeEach(() => {
    toggleDialogSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  const defaultProps = {
    orgId: '123',
    isOpen: true,
    domain: 'cheesecake.com',
    toggleDialog: toggleDialogSpy,
    loading: false,
  };

  const loadingProps = {
    orgId: '123',
    isOpen: true,
    domain: 'cheesecake.com',
    toggleDialog: toggleDialogSpy,
    loading: true,
  };

  const errorProps = {
    orgId: '123',
    isOpen: true,
    domain: 'cheesecake.com',
    toggleDialog: toggleDialogSpy,
    loading: false,
    error: new ApolloError({ graphQLErrors: [{ name: '', message: '', originalError: new ConflictError({ status: 409 }) }] }),
  };

  const domainClaimSuccessModalWithData = (claimClaimSuccessModalProps = defaultProps, dataProps = membersTotalDerivedResponse) => {
    const wrapper = shallow(
      <DomainClaimSuccessModalImpl
        onClose={toggleDialogSpy}
        totalMembers={dataProps}
        {...claimClaimSuccessModalProps}
      />,
    );

    const claimDomainSuccessModal = wrapper.instance();

    return {
      claimDomainSuccessModal,
      getWrapper: () => wrapper,
    };
  };

  it('should not render anything when data is loading', () => {
    const wrapper = domainClaimSuccessModalWithData(loadingProps).getWrapper();

    expect(wrapper.find(ModalDialog).length).to.equal(0);
  });

  it('should render a modal when not loading', () => {
    const wrapper = domainClaimSuccessModalWithData().getWrapper();
    const modal = wrapper.find(ModalDialog);
    expect(modal.length).to.equal(1);
  });

  it('should render the correct title', () => {
    const wrapper = domainClaimSuccessModalWithData().getWrapper();
    const modal = wrapper.find(ModalDialog);

    expect(modal.props().header).to.deep.equal(<FormattedMessage {...messages.verifiedDomain} />);
  });

  it('should render children', () => {
    const wrapper = domainClaimSuccessModalWithData().getWrapper();
    const modal = wrapper.find(ModalDialog);

    expect(modal.children.length).to.not.equal(0);
  });

  describe('domain claim success modal content', () => {
    it('should render the user count and domain', () => {
      const wrapper = domainClaimSuccessModalWithData().getWrapper();
      const html = wrapper.find(FormattedHTMLMessage);

      expect(html.props().defaultMessage).to.contain(messages.modalBody.defaultMessage);
      expect(html.props().values!.totalCount).to.equal(123);
      expect(html.props().values!.domainName).to.contain('cheesecake.com');
    });

    it('should render a user count of 0 if 0 members use the domain', () => {
      const memberTotalQueryResult = 0;

      const wrapper = domainClaimSuccessModalWithData(defaultProps, memberTotalQueryResult).getWrapper();
      const html = wrapper.find(FormattedHTMLMessage);

      expect(html.props().defaultMessage).to.contain(messages.modalBody.defaultMessage);
      expect(html.props().values!.totalCount).to.equal(0);
      expect(html.props().values!.domainName).to.contain('cheesecake.com');
    });

    it('should render a different message if there is an error', () => {
      const wrapper = domainClaimSuccessModalWithData(errorProps).getWrapper();
      const html = wrapper.find(FormattedHTMLMessage);

      expect(html.props().defaultMessage).to.contain(messages.errorBody.defaultMessage);
    });
  });
});
