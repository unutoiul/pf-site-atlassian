// tslint:disable jsx-use-translation-function
import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import * as React from 'react';
import { FormattedMessage, IntlProvider } from 'react-intl';
import * as sinon from 'sinon';

import AkButton from '@atlaskit/button';
import AkFieldRadioGroup from '@atlaskit/field-radio-group';
import AkTextField from '@atlaskit/field-text';

import { Button as AnalyticsButton } from 'common/analytics';
import { ModalDialog } from 'common/modal';

import { VerificationType } from '../../schema/schema-types';
import { createMockIntlProp, waitUntil } from '../../utilities/testing';
import { DomainClaimModalImpl } from './domain-claim-modal';

import { Domains } from './domain-claim.prop-types';

describe('DomainClaimModal', () => {

  let verifyDomain;
  let onClose;
  let checkDomainClaimFalseStub;
  let checkDomainClaimTrueStub;
  const sandbox = sinon.sandbox.create();

  const messages = {
    domainAlreadyVerified: {
      id: 'organization.domain.claim.warning.modal.domain.already.verified',
      defaultMessage: 'Domain already verified',
      description: 'Error message when someone tries to claim a domain that has already been claimed',
    },
  };

  beforeEach(() => {
    verifyDomain = sandbox.spy();
    onClose = sandbox.spy();
    checkDomainClaimFalseStub = sandbox.stub().returns(false);
    checkDomainClaimTrueStub = sandbox.stub().returns(true);
  });

  afterEach(() => {
    sandbox.restore();
  });

  const mockFeatureFlagProps = {
    isLoading: false,
    value: false,
  };

  const mockTrueFeatureFlagProps = {
    isLoading: false,
    value: true,
  };

  interface OptionProps {
    isClaimingDomain?: boolean;
    verificationType?: VerificationType;
    reverificationDomain?: string;
    domainClaimCheckFeatureFlag?: {
      isLoading: boolean;
      value: boolean;
    };
    domains?: Domains[];
    whatShouldIsClaimedReturn?: boolean;
  }

  const defaultOptions = {
    isClaimingDomain: false,
    verificationType: undefined,
    reverificationDomain: undefined,
    domainClaimCheckFeatureFlag: mockFeatureFlagProps,
    domains: [],
    whatShouldIsClaimedReturn: false,
  };

  function mountedDomainClaimModal(options: OptionProps) {
    return mount(
      <IntlProvider
        locale={'en'}
        children={(
          <DomainClaimModalImpl
            orgId={'DUMMY-TEST-ORG-ID'}
            isClaimingDomain={options.isClaimingDomain || defaultOptions.isClaimingDomain}
            verifyDomain={verifyDomain}
            onClose={onClose}
            reverificationDomain={options.reverificationDomain}
            verificationType={options.verificationType}
            domainClaimCheckFeatureFlag={options.domainClaimCheckFeatureFlag || defaultOptions.domainClaimCheckFeatureFlag}
            checkIfDomainHasBeenClaimed={options.whatShouldIsClaimedReturn ? checkDomainClaimTrueStub : checkDomainClaimFalseStub}
            intl={createMockIntlProp()}
            domains={options.domains || defaultOptions.domains}
          />
        )}
      />,
    );
  }

  function shallowDomainClaimModal(options: OptionProps) {
    return shallow(
      <DomainClaimModalImpl
        orgId={'DUMMY-TEST-ORG-ID'}
        isClaimingDomain={options.isClaimingDomain || defaultOptions.isClaimingDomain}
        verifyDomain={verifyDomain}
        onClose={onClose}
        reverificationDomain={options.reverificationDomain}
        verificationType={options.verificationType}
        domainClaimCheckFeatureFlag={options.domainClaimCheckFeatureFlag || defaultOptions.domainClaimCheckFeatureFlag}
        checkIfDomainHasBeenClaimed={options.whatShouldIsClaimedReturn ? checkDomainClaimTrueStub : checkDomainClaimFalseStub}
        intl={createMockIntlProp()}
        domains={options.domains || defaultOptions.domains}
      />,
    );
  }

  describe('should contain correct content', () => {
    it('if no reverify data is supplied fields should all be enabled', () => {
      const props = {
        isClaimingDomain: false,
      };

      const wrapper = shallowDomainClaimModal(props);
      const modal = wrapper.find(ModalDialog);

      const description = modal.find('p');
      const radioButtons = modal.find(AkFieldRadioGroup);
      const textField = modal.find(AkTextField);
      const button = modal.find(AkButton);
      const loadingButton = modal.find(AnalyticsButton);

      expect(description).to.have.length(1);
      expect(textField).to.have.length(1);
      expect(button).to.have.length(1);
      expect(loadingButton).to.have.length(1);

      expect(description.children().text()).to.contain('Complete the following');
      expect(radioButtons.props().label).to.equal('Method');
      expect(radioButtons.props().items[0].label).to.equal('TXT Record');
      expect(radioButtons.props().items[1].label).to.equal('HTTPS');
      expect(textField.props().value).to.equal('');
      expect(loadingButton.children().text()).to.deep.equal('Verify domain');
      expect(loadingButton.props().appearance).to.deep.equal('primary');
      expect(button.children().text()).to.deep.equal('Cancel');
    });

    it('if reverify data is supplied it should be shown and fields disabled', () => {
      const props = {
        isClaimingDomain: false,
        verificationType: 'http' as VerificationType,
        reverificationDomain: 'example.com',
      };

      const wrapper = shallowDomainClaimModal(props);
      const modal = wrapper.find(ModalDialog);

      const description = modal.find('p');
      const radioButtons = modal.find(AkFieldRadioGroup);
      const textField = modal.find(AkTextField);
      const button = modal.find(AkButton);
      const loadingButton = modal.find(AnalyticsButton);

      expect(description).to.have.length(1);
      expect(radioButtons).to.have.length(1);
      expect(textField).to.have.length(1);
      expect(button).to.have.length(1);
      expect(loadingButton).to.have.length(1);

      expect(description.children().text()).to.contain('Complete the following');
      expect(radioButtons.props().items[0].defaultSelected).to.equal(false);
      expect(radioButtons.props().items[0].isDisabled).to.equal(true);
      expect(radioButtons.props().items[1].defaultSelected).to.equal(true);
      expect(radioButtons.props().items[1].isDisabled).to.equal(true);
      expect(textField.props().value).to.equal('example.com');
      expect(textField.props().disabled).to.equal(true);
      expect(loadingButton.children().text()).to.deep.equal('Verify domain');
      expect(loadingButton.props().appearance).to.deep.equal('primary');
      expect(loadingButton.props().isDisabled).to.equal(false);
      expect(button.children().text()).to.deep.equal('Cancel');

      expect(loadingButton.props().analyticsData).to.deep.equal({
        action: 'click',
        actionSubject: 'verifyDomainButton',
        actionSubjectId: 'verify',
        subproduct: 'organization',
        tenantId: 'DUMMY-TEST-ORG-ID',
        tenantType: 'organizationId',
        attributes: {
          isReverifying: true,
          verificationType: 'http',
        },
      });
    });

    it('if reverify domain only is supplied it should be shown with only the textField disabled', () => {
      const props = {
        isClaimingDomain: false,
        reverificationDomain: 'example.com',
      };

      const wrapper = shallowDomainClaimModal(props);
      const modal = wrapper.find(ModalDialog);

      const description = modal.find('p');
      const radioButtons = modal.find(AkFieldRadioGroup);
      const textField = modal.find(AkTextField);
      const button = modal.find(AkButton);
      const loadingButton = modal.find(AnalyticsButton);

      expect(description).to.have.length(1);
      expect(textField).to.have.length(1);
      expect(button).to.have.length(1);
      expect(loadingButton).to.have.length(1);

      expect(description.children().text()).to.contain('Complete the following');
      expect(radioButtons.props().items[0].isDisabled).to.equal(false);
      expect(radioButtons.props().items[1].isDisabled).to.equal(false);
      expect(textField.props().value).to.equal('example.com');
      expect(textField.props().disabled).to.equal(true);
      expect(loadingButton.children().text()).to.deep.equal('Verify domain');
      expect(loadingButton.props().appearance).to.deep.equal('primary');
      expect(loadingButton.props().isDisabled).to.equal(false);
      expect(button.children().text()).to.deep.equal('Cancel');
    });

    it('if verify method only is supplied it should be shown with all fields enabled', () => {
      const props = {
        isClaimingDomain: false,
        verificationType: 'dns' as VerificationType,
      };
      const wrapper = shallowDomainClaimModal(props);
      const modal = wrapper.find(ModalDialog);

      const description = modal.find('p');
      const radioButtons = modal.find(AkFieldRadioGroup);
      const textField = modal.find(AkTextField);
      const button = modal.find(AkButton);
      const loadingButton = modal.find(AnalyticsButton);

      expect(description).to.have.length(1);
      expect(textField).to.have.length(1);
      expect(button).to.have.length(1);
      expect(loadingButton).to.have.length(1);

      expect(description.children().text()).to.contain('Complete the following');
      expect(radioButtons.props().items[0].isDisabled).to.equal(false);
      expect(radioButtons.props().items[0].defaultSelected).to.equal(true);
      expect(radioButtons.props().items[1].isDisabled).to.equal(false);
      expect(radioButtons.props().items[1].defaultSelected).to.equal(false);
      expect(textField.props().value).to.equal('');
      expect(textField.props().disabled).to.equal(false);
      expect(loadingButton.children().text()).to.deep.equal('Verify domain');
      expect(loadingButton.props().appearance).to.deep.equal('primary');
      expect(loadingButton.props().isDisabled).to.equal(true);
      expect(button.children().text()).to.deep.equal('Cancel');
    });
  });

  describe('button actions', () => {

    it('when sending a request to claim a domain the text field and button should be disabled ', () => {
      const props = {
        isClaimingDomain: true,
      };

      const wrapper = shallowDomainClaimModal(props);
      const button = wrapper.find(AnalyticsButton);
      const text = wrapper.find(AkTextField);
      expect(text.props().disabled).to.equal(true);
      expect(button.props().isDisabled).to.equal(true);
    });

    it('submitting form should trigger verifyDomain', () => {
      const props = {
        isClaimingDomain: false,
      };

      const wrapper = shallowDomainClaimModal(props);
      const form = wrapper.find('form');
      expect(verifyDomain.callCount).to.equal(0);
      form.simulate('submit', { preventDefault: () => null, currentTarget: { elements: { domain: { value: 'test.com' }, method: { value: 'dns' } } } });
      expect(verifyDomain.callCount).to.equal(1);
    });

    it('verify domain button is disabled until text is entered into the text field', () => {
      const props = {
        isClaimingDomain: false,
      };

      const wrapper = mountedDomainClaimModal(props);
      expect(wrapper.find(AnalyticsButton).props().isDisabled).to.equal(true);

      const input = wrapper.find(AkTextField).find('input');
      (input.getDOMNode() as HTMLInputElement).value = 'www.ducks.com';
      input.simulate('change', input);
      wrapper.update();

      expect(wrapper.find(AnalyticsButton).at(0).props().isDisabled).to.equal(false);
    });

    it('clicking on the cancel button should trigger cancel action', () => {
      const props = {
        isClaimingDomain: false,
      };

      const wrapper = shallowDomainClaimModal(props);
      const button = wrapper.find(AkButton);
      expect(onClose.callCount).to.equal(0);
      button.simulate('click');
      expect(onClose.callCount).to.equal(1);
    });
  });

  describe('with domain claim check feature flag enabled', () => {
    it('should call claim domain if the domain is not claimed', async () => {
      const props = {
        isClaimingDomain: false,
        domainClaimCheckFeatureFlag: mockTrueFeatureFlagProps,
      };

      const wrapper = shallowDomainClaimModal(props);
      const form = wrapper.find('form');
      expect(verifyDomain.callCount).to.equal(0);
      form.simulate('submit', { preventDefault: () => null, currentTarget: { elements: { domain: { value: 'test.com' }, method: { value: 'dns' } } } });

      await waitUntil(() => verifyDomain.callCount > 0);

      expect(checkDomainClaimFalseStub.callCount).to.equal(1);
      expect(verifyDomain.callCount).to.equal(1);
    });

    it('should call claim domain if the org owns the domain and it is verified, but is not claimed', async () => {
      const props = {
        isClaimingDomain: false,
        domainClaimCheckFeatureFlag: mockTrueFeatureFlagProps,
        domains: [{ domain: 'test.com', status: 'VERIFIED', verified: true }],
      };

      const wrapper = shallowDomainClaimModal(props);
      const form = wrapper.find('form');
      expect(verifyDomain.callCount).to.equal(0);
      form.simulate('submit', { preventDefault: () => null, currentTarget: { elements: { domain: { value: 'test.com' }, method: { value: 'dns' } } } });

      await waitUntil(() => verifyDomain.callCount > 0);

      // should not make a query to the backend
      expect(checkDomainClaimFalseStub.callCount).to.equal(0);
      expect(verifyDomain.callCount).to.equal(1);
    });

    it('should call claim domain if the org owns the domain and it is verified, even if the domain is claimed', async () => {
      const props = {
        isClaimingDomain: false,
        domainClaimCheckFeatureFlag: mockTrueFeatureFlagProps,
        whatShouldIsClaimedReturn: true,
        domains: [{ domain: 'test.com', status: 'VERIFIED', verified: true }],
      };

      const wrapper = shallowDomainClaimModal(props);
      const form = wrapper.find('form');
      expect(verifyDomain.callCount).to.equal(0);
      form.simulate('submit', { preventDefault: () => null, currentTarget: { elements: { domain: { value: 'test.com' }, method: { value: 'dns' } } } });

      await waitUntil(() => verifyDomain.callCount > 0);

      // doesn't need to check domain in back end
      expect(checkDomainClaimTrueStub.callCount).to.equal(0);
      expect(verifyDomain.callCount).to.equal(1);
    });

    it('shows a warning message if the org does not own the domain and there is a conflict', async () => {
      const props = {
        isClaimingDomain: false,
        domainClaimCheckFeatureFlag: mockTrueFeatureFlagProps,
        whatShouldIsClaimedReturn: true,
      };

      const wrapper = shallowDomainClaimModal(props);
      const form = wrapper.find('form');
      expect(verifyDomain.callCount).to.equal(0);
      form.simulate('submit', { preventDefault: () => null, currentTarget: { elements: { domain: { value: 'test.com' }, method: { value: 'dns' } } } });

      await waitUntil(() => checkDomainClaimTrueStub.callCount > 0);

      expect(verifyDomain.callCount).to.equal(0);

      const warningModal = wrapper.find(ModalDialog);

      expect(warningModal.length).to.equal(1);
      expect(warningModal.props().header).to.deep.equal(<FormattedMessage {...messages.domainAlreadyVerified} />);
    });

    it('should try to supercede the old domain claim on click', async () => {
      const props = {
        isClaimingDomain: false,
        domainClaimCheckFeatureFlag: mockTrueFeatureFlagProps,
        whatShouldIsClaimedReturn: true,
      };

      const wrapper = mountedDomainClaimModal(props);
      expect(verifyDomain.callCount).to.equal(0);

      const input = wrapper.find(AkTextField).find('input');
      (input.getDOMNode() as HTMLInputElement).value = 'www.test.com';
      input.simulate('change', input);
      wrapper.update();

      const form = wrapper.find('form');
      form.simulate('submit', { preventDefault: () => null });
      wrapper.update();

      await waitUntil(() => checkDomainClaimTrueStub.callCount > 0);
      wrapper.update();

      expect(verifyDomain.callCount).to.equal(0);

      const warningModal = wrapper.find(ModalDialog);

      expect(warningModal.length).to.equal(1);

      const button = warningModal.find(AkButton).at(0);
      button.simulate('click');
      wrapper.update();

      expect(verifyDomain.callCount).to.equal(1);
    });
  });
});
