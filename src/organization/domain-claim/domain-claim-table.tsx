import * as React from 'react';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import AkButton, { ButtonGroup as AkButtonGroup } from '@atlaskit/button';
import AkDynamicTable from '@atlaskit/dynamic-table';
import AkLozenge from '@atlaskit/lozenge';
import { akGridSizeUnitless } from '@atlaskit/util-shared-styles';

import { EmptyTableView } from 'common/table';
import { TruncatedField } from 'common/truncated-field';

import { VerificationType } from '../../schema/schema-types';
import { DomainClaimStatus } from './domain-claim-status';
import { Domains } from './domain-claim.prop-types';

export interface DomainClaimTableProps {
  domains: Domains[];
  reverifyHandler(domain: VerificationType | undefined, verificationType: string): () => void;
  removeHandler(domain: string, verified: boolean): () => void;
}

const DomainClaimDynamicTable = styled.div`
  .actions {
    text-align: right;
  }
`;

const messages = defineMessages({
  domain: {
    defaultMessage: 'Domain',
    id: 'organization.domain.claim.table.domain',
  },
  status: {
    defaultMessage: 'Status',
    id: 'organization.domain.claim.table.status',
  },
  verificationType: {
    defaultMessage: 'Verification type',
    id: 'organization.domain.claim.table.verificationType',
  },
  actions: {
    defaultMessage: 'Actions',
    id: 'organization.domain.claim.table.actions',
  },
  verify: {
    defaultMessage: 'Verify',
    id: 'organization.domain.claim.table.verify',
  },
  remove: {
    defaultMessage: 'Remove',
    id: 'organization.domain.claim.table.remove',
  },
  verifiedDomains: {
    defaultMessage: 'Verified domains',
    id: 'organization.domain.claim.table.verifiedDomains',
  },
});

export class DomainClaimTableImpl extends React.Component<DomainClaimTableProps & InjectedIntlProps> {
  public render() {
    const { formatMessage } = this.props.intl;

    const head = {
      cells: [
        {
          key: 'domain',
          content: formatMessage(messages.domain),
        },
        {
          key: 'status',
          content: formatMessage(messages.status),
        },
        {
          key: 'verificationType',
          content: formatMessage(messages.verificationType),
        },
        {
          key: 'actions',
          content: formatMessage(messages.actions),
          style: {
            paddingLeft: '28px',
          },
        },
      ],
    };

    const rows = this.props.domains.map(domain => ({
      cells: [
        {
          key: domain.domain + '.name',
          content: <TruncatedField title={domain.domain} maxWidth={`${akGridSizeUnitless * 30}px`}>{domain.domain}</TruncatedField>,
          style: {
            paddingTop: `${akGridSizeUnitless}px`,
            paddingBottom: `${akGridSizeUnitless}px`,
            width: `${akGridSizeUnitless * 30}px`,
          },
        },
        {
          key: domain.domain + '.status',
          content: (
            <AkLozenge appearance={domain.status === DomainClaimStatus.Verified ? 'success' : 'removed'}>
              {domain.status}
            </AkLozenge>
          ),
        },
        {
          key: domain.domain + '.verificationType',
          content: this.renderDomainVerificationType(domain.verificationType),
        },
        {
          key: domain.domain + '.actions',
          content: (
            <AkButtonGroup>
              {(domain.status !== DomainClaimStatus.Verified) && (
                <AkButton appearance="link" onClick={this.props.reverifyHandler(domain.verificationType, domain.domain)} id="domain-claim-list-verify-button">
                  {formatMessage(messages.verify)}
                </AkButton>
              )}
              <AkButton appearance="subtle-link" onClick={this.props.removeHandler(domain.domain, domain.status === DomainClaimStatus.Verified)} id="domain-claim-list-remove-button">
                {formatMessage(messages.remove)}
              </AkButton>
            </AkButtonGroup>
          ),
          className: 'actions',
        },
      ],
    }));

    if (this.props.domains.length === 0) {
      return null;
    }

    return (
      <DomainClaimDynamicTable>
        <AkDynamicTable
          caption={formatMessage(messages.verifiedDomains)}
          head={head}
          rows={rows.length ? rows : null}
          emptyView={
            <EmptyTableView>
              <FormattedMessage
                id="organizations.domains.table.none"
                defaultMessage="No verified domains."
              />
            </EmptyTableView>
          }
        />
      </DomainClaimDynamicTable>
    );
  }

  private renderDomainVerificationType = (verificationType: Domains['verificationType']) => {
    if (!verificationType) {
      return null;
    }

    if (verificationType === 'http') {
      return 'HTTPS';
    }

    return verificationType.toUpperCase();
  }
}

export const DomainClaimTable: React.ComponentClass<DomainClaimTableProps> = injectIntl(DomainClaimTableImpl);
