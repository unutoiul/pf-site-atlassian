// tslint:disable jsx-use-translation-function
import { ApolloError } from 'apollo-client';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import * as sinon from 'sinon';

import AkSpinner from '@atlaskit/spinner';

import { analyticsClient } from 'common/analytics';
import { PageLayout } from 'common/page-layout';

import { BadRequestError, ConflictError, RequestError } from 'common/error';

import { createMockContext, createMockIntlProp, waitUntil } from '../../utilities/testing';
import { DomainClaimPageImpl, messages } from './domain-claim-page';
import { DomainClaimSuccessModal } from './domain-claim-success-modal';
import { DomainClaimTable } from './domain-claim-table';

describe('DomainClaimPage', () => {
  const organizationJSON = {
    organization: {
      domainClaim: {
        domains: [
          {
            domain: 'cheese.com',
            verified: true,
            status: 'VERIFIED',
            verificationType: 'DNS',
          },
        ],
      },
    },
  };

  const organizationUnverifiedDomainJSON = {
    organization: {
      domainClaim: {
        domains: [
          {
            domain: 'corgi.com',
            verified: false,
            status: 'UNVERIFIED',
            verificationType: 'DNS',
          },
        ],
      },
    },
  };

  let eventHandlerSpy;
  let dataRefetchStub;
  let mutateStub;
  let showFlagSpy;
  let sendTrackEventSpy;

  const sandbox = sinon.sandbox.create();

  beforeEach(() => {
    eventHandlerSpy = sandbox.spy(analyticsClient, 'onEvent');
    dataRefetchStub = sandbox.stub().returns(Promise.resolve({}));
    mutateStub = sandbox.stub();
    showFlagSpy = sandbox.spy();
    sendTrackEventSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  const domainClaimPageWithData = (data, _claimDomainData = {}) => {

    data.refetch = dataRefetchStub;

    const wrapper = shallow(
      <DomainClaimPageImpl
        mutate={mutateStub}
        data={data}
        showFlag={showFlagSpy}
        hideFlag={() => undefined}
        match={{ params: { orgId: 'DUMMY_ORG_ID' } }}
        {...{} as any}
        intl={createMockIntlProp()}
        analyticsClient={{ sendTrackEvent: sendTrackEventSpy }}
      />,
      createMockContext({ intl: true, apollo: true }),
    );

    const domainClaimPage = wrapper.instance() as any;
    const claimDomainSpy = sandbox.spy(domainClaimPage, 'claimDomain');
    const completeDomainClaimSpy = sandbox.spy(domainClaimPage, 'completeDomainClaim');
    const claimDomainFailureSpy = sandbox.spy(domainClaimPage, 'claimDomainFailure');

    return {
      domainClaimPage,
      claimDomainSpy,
      completeDomainClaimSpy,
      claimDomainFailureSpy,
      claimDomain: (args) => {
        domainClaimPage.claimDomain(...args);
        wrapper.update();
      },
      completeDomainClaim: () => {
        domainClaimPage.completeDomainClaim();
        wrapper.update();
      },
      getWrapper: () => wrapper,
    };
  };

  describe('verify domain', () => {
    const formEventMock = ['test.com', 'dns'];
    const formEventMockHTTP = ['test.com', 'http'];

    it('should complete call when first claim response succeeds', async () => {
      mutateStub.onFirstCall().returns(Promise.resolve({ data: { claimDomain: { status: 'SUCCESS' } } }));
      const wrapper = domainClaimPageWithData({ loading: false, ...organizationUnverifiedDomainJSON });

      wrapper.claimDomain(formEventMock);
      await waitUntil(() => wrapper.completeDomainClaimSpy.called);

      expect(wrapper.completeDomainClaimSpy.called).to.equal(true);
    });

    it('should send a track event when claim response succeeds', async () => {
      mutateStub.onFirstCall().returns(Promise.resolve({ data: { claimDomain: { status: 'SUCCESS' } } }));
      const wrapper = domainClaimPageWithData({ loading: false, ...organizationUnverifiedDomainJSON });

      wrapper.claimDomain(formEventMock);
      await waitUntil(() => wrapper.completeDomainClaimSpy.called);

      expect(sendTrackEventSpy.called).to.equal(true);
    });

    it('should render a domain claim success modal', async () => {
      mutateStub.onFirstCall().returns(Promise.resolve({ data: { claimDomain: { status: 'SUCCESS' } } }));
      const wrapper = domainClaimPageWithData({ loading: false, ...organizationUnverifiedDomainJSON }, {});

      wrapper.claimDomain(formEventMock);
      await waitUntil(() => wrapper.completeDomainClaimSpy.called);

      expect(wrapper.completeDomainClaimSpy.called).to.equal(true);
      expect(wrapper.getWrapper().find(DomainClaimSuccessModal).length).to.equal(1);
    });

    it('should show conflict error flag when there is a conflict error via dns', async () => {
      const mockError: Partial<ApolloError> = {
        graphQLErrors: [
          { name: '', message: '', originalError: new ConflictError({ status: 409 }) },
        ],
      };
      mutateStub.onFirstCall().returns(Promise.reject(mockError));
      const wrapper = domainClaimPageWithData({ loading: false, ...organizationUnverifiedDomainJSON });

      wrapper.claimDomain(formEventMock);
      await waitUntil(() => showFlagSpy.called);

      expect(showFlagSpy.called).to.equal(true);
      expect(showFlagSpy.lastCall.args[0].description).to.deep.equal(<FormattedMessage {...messages.claimFailed}/>);
    });

    it('should show conflict error flag when there is a conflict error via https', async () => {
      const mockError: Partial<ApolloError> = {
        graphQLErrors: [
          { name: '', message: '', originalError: new ConflictError({ status: 409 }) },
        ],
      };
      mutateStub.onFirstCall().returns(Promise.reject(mockError));
      const wrapper = domainClaimPageWithData({ loading: false, ...organizationUnverifiedDomainJSON });

      wrapper.claimDomain(formEventMockHTTP);
      await waitUntil(() => showFlagSpy.called);

      expect(showFlagSpy.called).to.equal(true);
      expect(showFlagSpy.lastCall.args[0].description).to.deep.equal(<FormattedMessage {...messages.claimFailedHTTP}/>);
    });

    it('should show bad request error flag when there is a bad request', async () => {
      const mockError: Partial<ApolloError> = {
        graphQLErrors: [
          { name: '', message: '', originalError: new BadRequestError({ status: 400 }) },
        ],
      };
      mutateStub.onFirstCall().returns(Promise.reject(mockError));
      const wrapper = domainClaimPageWithData({ loading: false, ...organizationUnverifiedDomainJSON });

      wrapper.claimDomain(formEventMock);
      await waitUntil(() => showFlagSpy.called);

      expect(showFlagSpy.called).to.equal(true);
      expect(showFlagSpy.lastCall.args[0].description).to.equal('The domain must be in a valid format.');
    });

    it('should show request error flag when there is a request error', async () => {
      const mockError: Partial<ApolloError> = {
        graphQLErrors: [
          { name: '', message: '', originalError: new RequestError({ status: 500 }) },
        ],
      };
      mutateStub.onFirstCall().returns(Promise.reject(mockError));
      const wrapper = domainClaimPageWithData({ loading: false, ...organizationUnverifiedDomainJSON });

      wrapper.claimDomain(formEventMock);
      await waitUntil(() => showFlagSpy.called);

      expect(showFlagSpy.called).to.equal(true);
      expect(showFlagSpy.lastCall.args[0].title).to.contain('The domain could not be verified');
    });

    it('should show correct request error when claiming via dns', async () => {
      const mockError: Partial<ApolloError> = {
        graphQLErrors: [
          { name: '', message: '', originalError: new RequestError({ status: 500 }) },
        ],
      };
      mutateStub.onFirstCall().returns(Promise.reject(mockError));
      const wrapper = domainClaimPageWithData({ loading: false, ...organizationUnverifiedDomainJSON });

      wrapper.claimDomain(formEventMock);
      await waitUntil(() => showFlagSpy.called);

      expect(showFlagSpy.called).to.equal(true);
      expect(showFlagSpy.lastCall.args[0].description.props.defaultMessage).to.contain('DNS changes can take');
    });

    it('should show correct request error when claiming via https', async () => {
      const mockError: Partial<ApolloError> = {
        graphQLErrors: [
          { name: '', message: '', originalError: new RequestError({ status: 500 }) },
        ],
      };
      mutateStub.onFirstCall().returns(Promise.reject(mockError));
      const wrapper = domainClaimPageWithData({ loading: false, ...organizationUnverifiedDomainJSON });

      wrapper.claimDomain(formEventMockHTTP);
      await waitUntil(() => showFlagSpy.called);

      expect(showFlagSpy.called).to.equal(true);
      expect(showFlagSpy.lastCall.args[0].description.props.defaultMessage).to.contain('There was an error');
    });
  });

  it('should render page layout with title and description', () => {
    const title = 'Domains';
    const wrapper = domainClaimPageWithData({}).getWrapper();
    const pageLayout = wrapper.find(PageLayout);
    expect(pageLayout.props().title).to.deep.equal(title);
    expect(pageLayout.props().description).to.exist();
  });

  describe('without organization data', () => {

    const wrapper = domainClaimPageWithData({ loading: false }).getWrapper();

    it('should show an error message', () => {
      expect(wrapper.find('span')).to.have.length(1);
      expect(wrapper.find('span').text()).to.equal('Domain claim data could not be retrieved.');
    });

    it('should not show spinner', () => {
      const spinner = wrapper.find(AkSpinner) as typeof AkSpinner;
      expect(spinner).to.have.length(0);
    });

    it('should not render domain claim table', () => {
      const domainClaimTable = wrapper.find(DomainClaimTable);
      expect(domainClaimTable).to.have.length(0);
    });
  });

  describe('with organization data', () => {

    const wrapper = domainClaimPageWithData({ loading: false, ...organizationJSON }).getWrapper();

    it('should not show an error message', () => {
      expect(wrapper.find('span')).to.have.length(0);
    });

    it('should not show a spinner', () => {
      const spinner = wrapper.find(AkSpinner) as typeof AkSpinner;
      expect(spinner).to.have.length(0);
    });

    it('should render domain claim table', () => {
      const domainClaimTable = wrapper.find(DomainClaimTable);
      expect(domainClaimTable).to.have.length(1);
      expect(domainClaimTable.props().domains).to.deep.equal(organizationJSON.organization.domainClaim.domains);
    });
  });

  describe('analytics are fired', () => {
    it('should fire analytics on initial page load finishing', () => {
      const wrapper = domainClaimPageWithData({ loading: true, ...organizationJSON }).getWrapper();
      wrapper.setProps({ data: { loading: false, ...organizationJSON } });

      expect(eventHandlerSpy.called).to.equal(true);
      expect(eventHandlerSpy.firstCall.args[0]).to.deep.equal({
        subproduct: 'organization',
        tenantType: 'organizationId',
        tenantId: 'DUMMY_ORG_ID',
        action: 'pageLoad',
        actionSubject: 'organizationPageLoad',
        actionSubjectId: 'domainClaimPage',
      });
    });
  });

  describe('with old organization data', () => {

    // To test the case where we may have organization data but the loading prop is set to true.
    // We can end up in this state because we have organization data from a previous request, but it is
    // fetching new data. See: http://dev.apollodata.com/react/api-queries.html#graphql-query-data-loading

    const wrapper = domainClaimPageWithData({ loading: true, ...organizationJSON }).getWrapper();

    it('should show the spinner if loading', () => {
      const spinner = wrapper.find(AkSpinner) as typeof AkSpinner;

      expect(spinner).to.have.length(1);
      expect(spinner.props().isCompleting).to.be.false();
    });

    it('should not render domain claim table', () => {
      const domainClaimTable = wrapper.find(DomainClaimTable);
      expect(domainClaimTable).to.have.length(0);
    });
  });
});
