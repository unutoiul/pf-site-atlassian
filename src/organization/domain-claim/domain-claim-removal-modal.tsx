import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import AkButton, { ButtonGroup as AkButtonGroup } from '@atlaskit/button';

import {
  AnalyticsClientProps,
  AnalyticsData,
  Button as AnalyticsButton,
  removeDomainClickEventData,
  withAnalyticsClient,
} from 'common/analytics';
import { ModalDialog } from 'common/modal';

import { createOrgAnalyticsData } from '../organizations/organization-analytics';
export interface OwnProps {
  orgId: string;
  domain: string;
  isOpen: boolean;
  verified: boolean;
  isRemovingDomain: boolean;
  removeDomain(): void;
  toggleDialog(): void;
}

const messages = defineMessages({
  removeDomain: {
    id: 'organization.domain.claim.removal.modal.remove.domain',
    defaultMessage: 'Remove {domain}?',
  },
  verifiedModalText: {
    id: 'organization.domain.claim.removal.modal.verified.text',
    defaultMessage: `Once you remove this domain, you won't manage accounts from it any more.
    Atlassian Access features set up for this domain will stop working for those managed accounts.`,
  },
  cancel: {
    id: 'organization.domain.claim.removal.modal.cancel',
    defaultMessage: 'Cancel',
  },
  label: {
    id: 'organization.domain.claim.removal.modal.label',
    defaultMessage: 'Remove',
  },
});

type DomainClaimRemovalModalProps = OwnProps & InjectedIntlProps & AnalyticsClientProps;
export class DomainClaimRemovalModalImpl extends React.PureComponent<DomainClaimRemovalModalProps> {
  public render() {
    const { formatMessage } = this.props.intl;

    return (
      <ModalDialog
        width="small"
        header={formatMessage(messages.removeDomain, { domain: this.props.domain })}
        isOpen={this.props.isOpen}
        onClose={this.props.toggleDialog}
        children={this.modalDialogContent()}
        footer={this.modalDialogFooter()}
      />
    );
  }

  private modalDialogContent = () => {
    const { intl: { formatMessage } } = this.props;

    return (
      <div>
        {this.props.verified &&
          <p>
            {formatMessage(messages.verifiedModalText)}
          </p>
        }
      </div>
    );
  }

  private modalDialogFooter = () => {
    const { formatMessage } = this.props.intl;

    return (
      <AkButtonGroup>
        <AnalyticsButton
          analyticsData={this.generateRemoveDomainAnalytics()}
          onClick={this.onClickRemoveDomain}
          isLoading={this.props.isRemovingDomain}
          isDisabled={this.props.isRemovingDomain}
          appearance="danger"
          id="modal-domain-claim-remove-button"
        >
          {formatMessage(messages.label)}
        </AnalyticsButton>
        <AkButton onClick={this.props.toggleDialog} appearance="subtle-link">
          {formatMessage(messages.cancel)}
        </AkButton>
      </AkButtonGroup>
    );
  }

  private onClickRemoveDomain = () => {
    this.props.analyticsClient.sendUIEvent({
      orgId: this.props.orgId,
      data: removeDomainClickEventData({
        verified: this.props.verified,
      }),
    });
    this.props.removeDomain();
  }

  private generateRemoveDomainAnalytics = (): AnalyticsData => {
    return createOrgAnalyticsData({
      orgId: this.props.orgId,
      action: 'click',
      actionSubject: 'removeDomainButton',
      actionSubjectId: 'remove',
      attributes: {
        verified: this.props.verified,
      },
    });
  }
}

export const DomainClaimRemovalModal: React.ComponentClass<OwnProps> =
  injectIntl(
    withAnalyticsClient(
      DomainClaimRemovalModalImpl,
    ),
  );
