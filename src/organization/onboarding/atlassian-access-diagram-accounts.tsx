// tslint:disable:no-require-imports
// tslint:disable:no-var-requires

import * as React from 'react';
import { defineMessages, FormattedHTMLMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import {
  colors as akColors,
  gridSize as akGridSize,
} from '@atlaskit/theme';

const emptyAvatar = require('./images/empty-avatar.svg');
const person1Avatar = require('./images/person1-avatar.svg');
const person2Avatar = require('./images/person2-avatar.svg');
const person3Avatar = require('./images/person3-avatar.svg');
const person4Avatar = require('./images/person4-avatar.svg');
const lock = require('./images/lock.svg');
const check = require('./images/check.svg');

const diagramMessages = defineMessages({
  managedAccountsTitle: {
    id: 'atlassian.access.diagram.managedAccountsTitle',
    defaultMessage: 'Managed accounts',
  },
  claimedEmails: {
    id: 'atlassian.access.diagram.claimedEmails',
    defaultMessage: 'Claimed emails from @acme.com',
  },
  person1Email: {
    id: 'atlassian.access.diagram.person1Email',
    defaultMessage: 'liza@acme.com',
  },
  person2Email: {
    id: 'atlassian.access.diagram.person2Email',
    defaultMessage: 'john@acme.com',
  },
  person3Email: {
    id: 'atlassian.access.diagram.person3Email',
    defaultMessage: 'kelly@acme.com',
  },
  person4Email: {
    id: 'atlassian.access.diagram.person4Email',
    defaultMessage: 'ryan@acme.com',
  },
  nonManagedEmail1: {
    id: 'atlassian.access.diagram.nonManagedEmail1',
    defaultMessage: 'sharon@partner.com',
  },
  nonManagedEmail2: {
    id: 'atlassian.access.diagram.nonManagedEmail2',
    defaultMessage: 'tim@partner.com',
  },
  nonManagedEmail3: {
    id: 'atlassian.access.diagram.nonManagedEmail3',
    defaultMessage: 'josh@vendor.com',
  },
  nonManagedEmail4: {
    id: 'atlassian.access.diagram.nonManagedEmail4',
    defaultMessage: 'tina@vendor.com',
  },
  securityFeatureAtlassianAccess: {
    id: 'atlassian.access.diagram.securityFeatureAtlassianAccess',
    defaultMessage: 'with <strong>Atlassian Access</strong>,',
  },
  securityFeatures: {
    id: 'atlassian.access.diagram.securityFeatures',
    defaultMessage: 'SAML single sign-on, enforced two-step verification, password policies',
  },
  avatarAltText: {
    id: 'atlassian.access.diagram.avatarAltText',
    defaultMessage: 'Account avatar',
  },
});

const avatarVerticalOffset = akGridSize() * 10.5;
const lockSizeUnits = 3;
const checkSizeUnits = 2;

const AllAccounts = styled.div`
  display: flex;
  flex-direction: row;
`;

const ManagedAccounts = styled.div`
  background-color: ${akColors.N30};
  border-radius: ${akGridSize}px;
  min-width: ${akGridSize() * 49}px;
  padding-bottom: ${akGridSize() * 3}px;
`;

const NonManagedAccounts = styled.div`
  display: flex;
  opacity: 0.4;
  padding-top: ${avatarVerticalOffset}px;
`;

const AccountsSection = styled.div`
  align-items: center;
  background-color: ${akColors.N20};
  border-radius: ${akGridSize}px;
  display: flex;
  flex-direction: column;
  margin-bottom: ${akGridSize() * 4}px;
  padding-bottom: ${akGridSize() * 5}px;
  position: relative;

  &::before {
    background-image: url(${check});
    background-position: center;
    background-repeat: no-repeat;
    border-top: 3px solid ${akColors.N0};
    content: '';
    height: ${akGridSize() * checkSizeUnits}px;
    left: 50%;
    position: absolute;
    top: 0;
    transform: translateX(-50%) translateY(-50%);
    width: ${akGridSize() * checkSizeUnits}px;
  }

  &::after {
    background-image: url(${lock});
    background-position: center;
    background-repeat: no-repeat;
    bottom: 0;
    content: '';
    height: ${akGridSize() * lockSizeUnits}px;
    left: 50%;
    position: absolute;
    transform: translateX(-50%) translateY(50%);
    width: ${akGridSize() * lockSizeUnits}px;
  }

`;

const ManagedAccountsTitle = styled.h4`
  padding-top: ${akGridSize() * 2}px;
`;

const SecurityFeatures = styled.div`
  align-items: center;
  display: flex;
  flex-direction: column;
  padding: 0 ${akGridSize() * 2}px;
  text-align: center;
`;

const AvatarWrapper = styled.div`
  align-items: center;
  display: flex;
  flex-direction: column;
  margin: 0 ${akGridSize() * 2}px;
  max-width: ${akGridSize() * 9}px;
`;

const AvatarImage = styled.img`
  padding-bottom: ${akGridSize() * 1}px;
  width: ${akGridSize() * 8}px;
`;
const AvatarSubtitle = styled.span`
  font-size: 0.65em;
`;

const Avatar = ({ image, subtitle, alt }) => (
  <AvatarWrapper>
    <AvatarImage src={image} alt={alt} />
    <AvatarSubtitle>{subtitle}</AvatarSubtitle>
  </AvatarWrapper>
);

const Accounts = styled.div`
  display: flex;
  flex-direction: row;
  margin-top: ${akGridSize() * 3}px;
`;

// tslint:disable-next-line:no-empty-interface
interface OwnProps {}

export class AtlassianAccessDiagramAccountsImpl extends React.Component<InjectedIntlProps & OwnProps> {
  public render() {
    const { intl: { formatMessage } } = this.props;

    return (
      <AllAccounts>
        <NonManagedAccounts>
          <Avatar alt={formatMessage(diagramMessages.avatarAltText)} image={emptyAvatar} subtitle={formatMessage(diagramMessages.nonManagedEmail1)}/>
          <Avatar alt={formatMessage(diagramMessages.avatarAltText)} image={emptyAvatar} subtitle={formatMessage(diagramMessages.nonManagedEmail2)}/>
        </NonManagedAccounts>
        <ManagedAccounts>
          <AccountsSection>
            <ManagedAccountsTitle>{formatMessage(diagramMessages.managedAccountsTitle)}</ManagedAccountsTitle>
            <span>{formatMessage(diagramMessages.claimedEmails)}</span>
            <Accounts>
              <Avatar alt={formatMessage(diagramMessages.avatarAltText)} image={person1Avatar} subtitle={formatMessage(diagramMessages.person1Email)}/>
              <Avatar alt={formatMessage(diagramMessages.avatarAltText)} image={person2Avatar} subtitle={formatMessage(diagramMessages.person2Email)}/>
              <Avatar alt={formatMessage(diagramMessages.avatarAltText)} image={person3Avatar} subtitle={formatMessage(diagramMessages.person3Email)}/>
              <Avatar alt={formatMessage(diagramMessages.avatarAltText)} image={person4Avatar} subtitle={formatMessage(diagramMessages.person4Email)}/>
            </Accounts>
          </AccountsSection>
          <SecurityFeatures>
            <span><FormattedHTMLMessage {...diagramMessages.securityFeatureAtlassianAccess} /></span>
            <span>{formatMessage(diagramMessages.securityFeatures)}</span>
          </SecurityFeatures>
        </ManagedAccounts>
        <NonManagedAccounts>
          <Avatar alt={formatMessage(diagramMessages.avatarAltText)} image={emptyAvatar} subtitle={formatMessage(diagramMessages.nonManagedEmail3)}/>
          <Avatar alt={formatMessage(diagramMessages.avatarAltText)} image={emptyAvatar} subtitle={formatMessage(diagramMessages.nonManagedEmail4)}/>
        </NonManagedAccounts>
      </AllAccounts>
    );
  }
}

export const AtlassianAccessDiagramAccounts = injectIntl<OwnProps>(AtlassianAccessDiagramAccountsImpl);
