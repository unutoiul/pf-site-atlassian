import * as React from 'react';
import { graphql, OptionProps } from 'react-apollo';

import { accessNoOrgScreenEvent, atlassianAccessPricingUIEventData, continueToAccessUIEventData, learnAboutAccessUIEventData, ScreenEventSender } from 'common/analytics';
import { AtlassianAccessValuePropBody } from 'common/atlassian-access-value-prop/atlassian-access-value-prop-body';
import { AtlassianAccessValuePropHeader } from 'common/atlassian-access-value-prop/atlassian-access-value-prop-header';
import { FeatureFlagAddedProp, withUserFeatureFlag } from 'common/feature-flags';
import {
  ValuePropFooter,
  ValuePropPage,
} from 'common/value-prop';

import { NoOrgOnboardingPageQuery } from '../../schema/schema-types';
import { NoOrgOnboardingPageFooterButton } from './no-org-onboarding-page-footer-button';
import noOrgOnboardingPageQuery from './no-org-onboarding-page.query.graphql';

const analyticsActionSubject = 'noOrgOnboardingPage';

export class NoOrgOnboardingPageImpl extends React.Component<FeatureFlagAddedProp & DerivedProps> {
  public render() {
    const { featureFlag, currentSiteId, loading } = this.props;

    return (
      <ScreenEventSender
        event={accessNoOrgScreenEvent(featureFlag.value, currentSiteId)}
        isDataLoading={featureFlag.isLoading || loading}
      >
        <ValuePropPage
          renderHeader={this.renderHeader}
          renderBody={this.renderBody}
          renderFooter={this.renderFooter}
        />
      </ScreenEventSender>
    );
  }

  private renderHeader = () => {
    return (
      <AtlassianAccessValuePropHeader
        analyticsActionSubject={analyticsActionSubject}
        learnMoreAnalyticsEvent={{ data: learnAboutAccessUIEventData() }}
      />
    );
  };

  private renderBody = () => {
    return (
      <AtlassianAccessValuePropBody
        analyticsActionSubject={analyticsActionSubject}
        pricingAnalyticsEvent={{ data: atlassianAccessPricingUIEventData() }}
      />
    );
  };

  private renderFooter = () => {
    return (
      <ValuePropFooter>
        <NoOrgOnboardingPageFooterButton buttonAnalytics={{ data: continueToAccessUIEventData() }} />
      </ValuePropFooter>
    );
  };
}

interface DerivedProps {
  currentSiteId?: string;
  loading: boolean;
}

export const getNoOrgOnboardingProps = (props: OptionProps<{}, NoOrgOnboardingPageQuery>): DerivedProps => {
  if (!props || !props.data || props.data.loading) {
    return {
      loading: true,
    };
  }

  if (props.data.error || !props.data.currentSite || !props.data.currentSite.id) {
    return {
      loading: false,
    };
  }

  return {
    loading: false,
    currentSiteId: props.data.currentSite.id,
  };
};

const withNoOrgOnboardingPageQuery = graphql<FeatureFlagAddedProp, NoOrgOnboardingPageQuery, {}, DerivedProps>(
  noOrgOnboardingPageQuery,
  {
    props: getNoOrgOnboardingProps,
  },
);

export const NoOrgOnboardingPage =
  withUserFeatureFlag({ flagKey: 'new-lozenge-for-security-nav-item', defaultValue: false, name: 'featureFlag' })(
    withNoOrgOnboardingPageQuery(
      NoOrgOnboardingPageImpl,
  ),
);
