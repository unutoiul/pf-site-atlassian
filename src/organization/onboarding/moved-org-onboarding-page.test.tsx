import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import { ThemeProvider } from 'styled-components';

import AkButton from '@atlaskit/button';

import { createMockContext, createMockIntlProp } from '../../utilities/testing';
import {
  messages,
  MovedOrgOnboardingPageImpl,
} from './moved-org-onboarding-page';

describe('MovedOrgOnboardingPage', () => {
  let mounted ;
  const movedOrgWrapper = (
    props = {
      loading: false,
      hasError: false,
      atlassianAccessEnabled: false,
    },
  ) => {
    return mounted = mount(
      <ThemeProvider theme={{}}>
      <MovedOrgOnboardingPageImpl
        intl={createMockIntlProp()}
        {...props}
      /></ThemeProvider>,
      createMockContext({ intl: true, apollo: true }),
    );
  };

  afterEach(() => {
    if (mounted) {
      mounted.unmount();
    }
  });

  it('should render the atlassian access text', () => {
    const wrapper = movedOrgWrapper({
      loading: false,
      hasError: false,
      atlassianAccessEnabled: true,
    });

    expect(wrapper.text()).to.include(messages.descriptionAdminHub.defaultMessage);
    expect(wrapper.text()).to.include(messages.descriptionAtlassianAccess.defaultMessage);
  });

  it('should render a button which links to admin.atlassian.com in a new tab', () => {
    const wrapper = movedOrgWrapper();
    const button = wrapper.find(AkButton).at(0);

    expect(button.text()).equals('Go to admin.atlassian.com');
    expect(button.prop('href')).equals('http://localhost:3001');
    expect(button.prop('target')).equals('_blank');
  });

  it('should render a Learn More button which links to a CAC page in a new tab', () => {
    const wrapper = movedOrgWrapper();
    const button = wrapper.find(AkButton).at(1);

    expect(button.text()).equals('Learn more');
    expect(button.prop('href')).equals('https://confluence.atlassian.com/display/Cloud/2018/04/24/Atlassian+admin+hub');
    expect(button.prop('target')).equals('_blank');
  });
});
