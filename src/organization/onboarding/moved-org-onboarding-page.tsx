import * as React from 'react';
import { compose } from 'react-apollo';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';

import AkEmptyState from '@atlaskit/empty-state';
import AkSpinner from '@atlaskit/spinner';

import { accessOrgMovedScreenEvent, Button as AnalyticsButton, ScreenEventSender } from 'common/analytics';
import { getConfig } from 'common/config';
import { GenericError } from 'common/error';
import { PageLayout } from 'common/page-layout';

import { createOrgAnalyticsData } from '../../organization/organizations/organization-analytics';

export const messages = defineMessages({
  title: {
    id: 'moved.org.onboarding.title',
    defaultMessage: 'We’ve moved your organization',
  },
  descriptionAdminHub: {
    id: 'moved.org.onboarding.description.admin.hub',
    defaultMessage: 'We’ve created a new global home for your organization at admin.atlassian.com.',
  },
  descriptionAtlassianAccess: {
    id: 'moved.org.onboarding.description.atlassian.access',
    defaultMessage: 'As part of the move, Identity Manager is now called Atlassian Access. If you joined the Identity Manager Early Access Program, we’ll begin charging at the end of your evaluation period. You can find your bill estimate under the billing section of your organization.',
  },
  learnMore: {
    id: 'moved.org.onboarding.learn.more',
    defaultMessage: 'Learn more',
  },
  cta: {
    id: 'moved.org.onboarding.cta',
    defaultMessage: 'Go to admin.atlassian.com',
  },
});

export interface MovedOrgOnboardingPageOwnProps {
  loading: boolean;
  hasError: boolean;
}

export type MovedOrgOnboardingPageProps = MovedOrgOnboardingPageOwnProps & InjectedIntlProps;

export class MovedOrgOnboardingPageImpl extends React.Component<MovedOrgOnboardingPageProps> {
  public render() {
    const {
      intl: { formatMessage },
      loading,
      hasError,
    } = this.props;

    const analyticsActionSubject = 'movedOrgOnboardingPage';

    return (
      <ScreenEventSender event={accessOrgMovedScreenEvent()}>
        <PageLayout isFullWidth={true}>
          {loading && <AkSpinner />}
          {!loading && hasError && <GenericError />}
          {!loading && !hasError &&
            <AkEmptyState
              // tslint:disable-next-line:no-require-imports
              imageUrl={require('./images/hero-moved-org.svg')}
              header={formatMessage(messages.title)}
              description={this.getDescription()}
              primaryAction={(
                <AnalyticsButton
                  appearance="primary"
                  href={getConfig().goToAdminLinkUrl}
                  target="_blank"
                  analyticsData={createOrgAnalyticsData({
                    action: 'click',
                    actionSubject: analyticsActionSubject,
                    actionSubjectId: 'goToAdminButton',
                  })}
                >
                  {formatMessage(messages.cta)}
                </AnalyticsButton>
              )}
              secondaryAction={(
                <AnalyticsButton
                  appearance="subtle-link"
                  href="https://confluence.atlassian.com/display/Cloud/2018/04/24/Atlassian+admin+hub"
                  target="_blank"
                  analyticsData={createOrgAnalyticsData({
                    action: 'click',
                    actionSubject: analyticsActionSubject,
                    actionSubjectId: 'learnMoreButton',
                  })}
                >
                  {formatMessage(messages.learnMore)}
                </AnalyticsButton>
              )}
            />
          }
        </PageLayout>
      </ScreenEventSender>
    );
  }

  private getDescription = () => {
    // AkEmptyState currently wraps the description prop within a <p>,
    // requiring us to pass the below instead of something more semantically correct
    // since <p> tags cannot be descendants of <p>'s.
    // @see https://ecosystem.atlassian.net/browse/AK-4581
    return (
      <span>
        <FormattedMessage {...messages.descriptionAdminHub} />
        <br /><br />
        <FormattedMessage {...messages.descriptionAtlassianAccess} />
      </span>
    );
  }
}

export const MovedOrgOnboardingPage = compose(
  injectIntl,
)(MovedOrgOnboardingPageImpl);
