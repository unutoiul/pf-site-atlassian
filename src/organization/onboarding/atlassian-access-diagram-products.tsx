import * as React from 'react';
import styled from 'styled-components';

import {
  BitbucketIcon as AkBitbucketIcon,
  ConfluenceIcon as AkConfluenceIcon,
  JiraIcon as AkJiraIcon,
  StrideIcon as AkStrideIcon,
} from '@atlaskit/logo';
import {
  colors as akColors,
  gridSize as akGridSize,
} from '@atlaskit/theme';

const ProductsList = styled.div`
  align-items: center;
  background-color: ${akColors.N20};
  border-radius: ${akGridSize}px;
  display: flex;
  margin: ${akGridSize() * 1}px 0;
  padding: ${akGridSize() * 0.5}px 0 ${akGridSize() * 0.5}px 0;
`;

const IconWrapper = styled.div`
  display: flex;
  justify-content: center;
  padding-left: ${akGridSize() * 1}px;
  padding-right: ${akGridSize() * 1}px;
`;

// tslint:disable-next-line:no-empty-interface
interface OwnProps {}

export class AtlassianAccessDiagramProducts extends React.Component<OwnProps> {
  public render() {

    return (
      <ProductsList>
        <IconWrapper>
          <AkJiraIcon size="medium" iconColor={akColors.B200} iconGradientStart={akColors.B400} iconGradientStop={akColors.B200} />
        </IconWrapper>
        <IconWrapper>
          <AkConfluenceIcon size="medium" iconColor={akColors.B200} iconGradientStart={akColors.B400} iconGradientStop={akColors.B200} />
        </IconWrapper>
        <IconWrapper>
          <AkStrideIcon size="medium" iconColor={akColors.B200} iconGradientStart={akColors.B400} iconGradientStop={akColors.B200} />
        </IconWrapper>
        <IconWrapper>
          <AkBitbucketIcon size="medium" iconColor={akColors.B200} iconGradientStart={akColors.B400} iconGradientStop={akColors.B200} />
        </IconWrapper>
      </ProductsList>
    );
  }
}
