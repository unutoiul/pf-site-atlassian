import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import {
  colors as akColors,
  gridSize as akGridSize,
} from '@atlaskit/theme';

const diagramMessages = defineMessages({
  orgCreated: {
    id: 'atlassian.access.diagram.orgCreated',
    defaultMessage: 'Organization created',
  },
  domainVerified: {
    id: 'atlassian.access.diagram.domainVerified',
    defaultMessage: 'Domain verified: @acme.com',
  },
});
const Steps = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-evenly;
  min-height: ${akGridSize() * 18}px;
  position: relative;

  &::before {
    border: 1px solid ${akColors.N40};
    content: '';
    height: 100%;
    left: 50%;
    position: absolute;
    width: 0;
    z-index: -1;
  }
`;

const Step = styled.span`
  background-color: ${akColors.N0};
`;

// tslint:disable-next-line:no-empty-interface
interface OwnProps {}

export class AtlassianAccessDiagramStepsImpl extends React.Component<InjectedIntlProps & OwnProps> {
  public render() {
    const { intl: { formatMessage } } = this.props;

    return (
      <Steps>
        <Step>{formatMessage(diagramMessages.orgCreated)}</Step>
        <Step>{formatMessage(diagramMessages.domainVerified)}</Step>
      </Steps>
    );
  }
}

export const AtlassianAccessDiagramSteps = injectIntl<OwnProps>(AtlassianAccessDiagramStepsImpl);
