import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';
import { MemoryRouter } from 'react-router-dom';

import AkPage from '@atlaskit/page';

import { configureApolloClientWithUserFeatureFlag } from 'common/feature-flags/test-utils';

import { createApolloClient } from '../../apollo-client';
import { createMockIntlProp } from '../../utilities/testing';
import { CreateOrgOnboardingPageImpl } from './create-org-onboarding-page';

const CreateOrgOnboardingPageWithProviders = ({ isAtlassianAccessEnabled }) => (
  <MemoryRouter>
    <ApolloProvider
      client={
        configureApolloClientWithUserFeatureFlag({
          client: createApolloClient(),
          flagKey: 'atlassianaccess.enabled',
          flagValue: isAtlassianAccessEnabled,
        })}
    >
      <IntlProvider locale="en">
        <AkPage>
          <CreateOrgOnboardingPageImpl
            intl={createMockIntlProp()}
            match={null as any}
            location={null as any}
            history={null as any}
            totalOrganizations={undefined}
            totalOrganizationsLoadError={undefined}
            analyticsClient={null as any}
          />
        </AkPage>
      </IntlProvider>
    </ApolloProvider>
  </MemoryRouter >
);

storiesOf('Organization|Create Org Onboarding Page', module)
  .add('With Atlassian Access FF enabled', () => (
    <CreateOrgOnboardingPageWithProviders isAtlassianAccessEnabled={true} />
  ))
  .add('With Atlassian Access FF disabled', () => (
    <CreateOrgOnboardingPageWithProviders isAtlassianAccessEnabled={false} />
  ));
