import * as React from 'react';
import { defineMessages, FormattedHTMLMessage, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import AKButton from '@atlaskit/button';
import { gridSize as akGridSize } from '@atlaskit/theme';

import {
  accessIntroScreenEvent,
  AnalyticsClientProps,
  Button as AnalyticsButton,
  createOrgButtonClickedEvent,
  learnMoreAccessLinkClickedEvent,
  learnMoreDomainLinkClickedEvent,
  ScreenEventSender,
  withAnalyticsClient,
} from 'common/analytics';
import { ValuePropFooter, ValuePropHeader, ValuePropPage } from 'common/value-prop';

import { links } from 'common/link-constants';

import { withOrganizationsCount, WithOrgsCountProps } from 'common/user';

import { AtlassianAccessDiagram } from './atlassian-access-diagram';

const BodyWrapper = styled.div`
  margin-top: ${akGridSize() * 4}px;
`;

const DiagramWrapper = styled.div`
  margin: ${akGridSize() * 6}px 0;
`;

// tslint:disable-next-line:no-var-requires no-require-imports
const headerImageUrl = require('common/atlassian-access-value-prop/images/hero.svg');

const atlassianAccessMessages = defineMessages({
  title: {
    id: 'create.org.onboarding.page.atlassian.access.title',
    defaultMessage: 'Atlassian Access',
  },
  description: {
    id: 'create.org.onboarding.page.atlassian.access.description',
    defaultMessage: 'With Atlassian Access your users get the convenience of <strong>SAML single sign-on</strong>, and you can enforce <strong>two-step verification</strong> and set <strong>password policies</strong>.',
  },
  learnMore: {
    id: 'create.org.onboarding.page.atlassian.access.learnMore',
    defaultMessage: 'Learn more about Atlassian Access',
  },
  bodyNeedToKnow: {
    id: 'create.org.onboarding.page.atlassian.access.bodyNeedToKnow',
    defaultMessage: `
    <h4>What you need to know</h4>
    <ul>
      <li>When you verify a domain, Atlassian accounts that use email addresses from that domain become managed accounts.</li>
      <li>After you verify a domain, you may have more managed accounts than you expected.</li>
      <li>After you verify a domain, site admins will no longer have as much control over user accounts.</li>
      <li>Atlassian Access is free to try for 30 days.</li>
    </ul>
    `,
  },
  bodyDoNextHeader: {
    id: 'create.org.onboarding.page.atlassian.access.bodyDoNextHeader',
    defaultMessage: 'What to do next',
    description: 'The heading for the do next section',
  },
  createOrganization: {
    id: 'create.org.onboarding.page.atlassian.access.createOrganization',
    defaultMessage: 'To get started, create an organization by clicking below.',
    description: 'The first bullet in the list of things to do next',
  },
  verifyDomains: {
    id: 'create.org.onboarding.page.atlassian.access.verifyDomains',
    defaultMessage: 'Next, verify one or more domains, to prove that you own them. ',
    description: 'The second bullet in the list of things to do next list',
  },
  learnMoreAboutDomains: {
    id: 'create.org.onboarding.page.atlassian.access.learnMoreAboutDomains',
    defaultMessage: 'Learn more',
    description: 'The link to learn more about domains in the second bullet of things to do next list',
  },
  subscribe: {
    id: 'create.org.onboarding.page.atlassian.access.subscribe',
    defaultMessage: 'Then you\'ll be able to subscribe to Atlassian Access for security control over your managed accounts.',
    description: 'The last bullet in the things to next list.',
  },
  footerButton: {
    id: 'create.org.onboarding.page.atlassian.access.footerButtonText',
    defaultMessage: 'Create an organization',
    description: 'The button after the things to do next list',
  },
});

type AllProps = InjectedIntlProps & RouteComponentProps<any> & WithOrgsCountProps & AnalyticsClientProps;
export class CreateOrgOnboardingPageImpl extends React.Component<AllProps> {
  public render() {

    return (
      <ScreenEventSender event={accessIntroScreenEvent()}>
        <ValuePropPage
          renderHeader={this.renderHeader}
          renderBody={this.renderBody}
          renderFooter={this.renderFooter}
        />
      </ScreenEventSender>
    );
  }

  private renderHeader = (): React.ReactNode => {
    const { formatMessage } = this.props.intl;

    return (
      <ValuePropHeader
        title={formatMessage(atlassianAccessMessages.title)}
        description={<FormattedHTMLMessage {...atlassianAccessMessages.description} />}
        learnMoreLink={
          <AKButton
            appearance="link"
            spacing="none"
            href={links.external.atlassianAccess}
            target="_blank"
            onClick={this.learnMoreAccessClicked}
          >
            <FormattedHTMLMessage {...atlassianAccessMessages.learnMore} />
          </AKButton>}
        imagePath={headerImageUrl}
      />
    );
  };

  private renderBody = () => {
    return (
      <BodyWrapper>
        <FormattedHTMLMessage {...atlassianAccessMessages.bodyNeedToKnow} />
        <DiagramWrapper><AtlassianAccessDiagram /></DiagramWrapper>
        <FormattedMessage {...atlassianAccessMessages.bodyDoNextHeader} tagName="h4"/>
        <ul>
          <FormattedMessage {...atlassianAccessMessages.createOrganization} tagName="li"/>
          <li>
            <FormattedMessage {...atlassianAccessMessages.verifyDomains}/>
            <AKButton
              appearance="link"
              spacing="none"
              href={links.external.orgAdmin}
              target="_blank"
              onClick={this.learnMoreDomainLinkClicked}
            >
              <FormattedMessage {...atlassianAccessMessages.learnMoreAboutDomains}/>
            </AKButton>
          </li>
          <FormattedMessage {...atlassianAccessMessages.subscribe} tagName="li"/>
        </ul>
      </BodyWrapper>
    );
  };

  private renderFooter = () => {
    const {
      intl: { formatMessage },
    } = this.props;

    return (
      <ValuePropFooter>
        <Link
          to={links.internal.createOrg}
        >
          <AnalyticsButton
            appearance="primary"
            onClick={this.createButtonClicked}
          >
            {formatMessage(atlassianAccessMessages.footerButton)}
          </AnalyticsButton>
        </Link>
      </ValuePropFooter>
    );
  };

  private createButtonClicked = () => {
    this.sendAnalytics(createOrgButtonClickedEvent);
  };

  private learnMoreAccessClicked = () => {
    this.sendAnalytics(learnMoreAccessLinkClickedEvent);
  };

  private learnMoreDomainLinkClicked = () => {
    this.sendAnalytics(learnMoreDomainLinkClickedEvent);
  };

  private sendAnalytics = (event) => {
    this.props.analyticsClient.sendUIEvent(event());
  };
}

export const CreateOrgOnboardingPage =
  withOrganizationsCount<{}>(
    injectIntl(
      withRouter(
        withAnalyticsClient(
          CreateOrgOnboardingPageImpl,
        ),
      ),
    ),
  );
