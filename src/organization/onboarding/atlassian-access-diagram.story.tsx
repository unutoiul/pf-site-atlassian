import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { IntlProvider } from 'react-intl';

import { AtlassianAccessDiagram } from './atlassian-access-diagram';

storiesOf('Organization|Atlassian Access Diagram', module)
  .add('Atlassian Access Diagram', () => (
    <IntlProvider locale="en">
      <AtlassianAccessDiagram />
    </IntlProvider>
  ));
