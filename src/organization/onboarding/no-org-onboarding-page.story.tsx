import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';

import AkPage from '@atlaskit/page';

import { configureApolloClientWithUserFeatureFlag } from 'common/feature-flags/test-utils';

import { createApolloClient } from '../../apollo-client';
import { NoOrgOnboardingPage } from './no-org-onboarding-page';

const CreateAtlassianAccessPageWithProviders = ({ isAtlassianAccessEnabled }) => (
  <ApolloProvider
    client={
      configureApolloClientWithUserFeatureFlag({
        client: createApolloClient(),
        flagKey: 'atlassianaccess.enabled',
        flagValue: isAtlassianAccessEnabled,
      })}
  >
    <IntlProvider locale="en">
      <AkPage>
        <NoOrgOnboardingPage />
      </AkPage>
    </IntlProvider>
  </ApolloProvider>
);

storiesOf('Organization|No Org Onboarding Page', module)
  .add('With Atlassian Access FF enabled', () => (
    <CreateAtlassianAccessPageWithProviders isAtlassianAccessEnabled={true} />
  ))
  .add('With Atlassian Access FF disabled', () => (
    <CreateAtlassianAccessPageWithProviders isAtlassianAccessEnabled={false} />
  ));
