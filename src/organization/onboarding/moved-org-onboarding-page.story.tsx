import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';

import { createApolloClient } from '../../apollo-client';

import { createMockIntlProp } from '../../utilities/testing';
import { MovedOrgOnboardingPageImpl } from './moved-org-onboarding-page';

const graphqlDataProps = {
  networkStatus: null,
  variables: null,
  fetchMore: null,
  refetch: null,
  startPolling: null,
  stopPolling: null,
  subscribeToMore: null,
  updateQuery: null,
  loading: null,
};

const client = createApolloClient();

const CreatePageWithProviders = ({ ...props }) => (
  <ApolloProvider client={client}>
    <IntlProvider locale="en">
      <MovedOrgOnboardingPageImpl {...props as any} />
    </IntlProvider>
  </ApolloProvider>
);

storiesOf('Common|Moved Org Onboarding Page', module)
  .add('With Atlassian Access FF enabled', () => {
    return (
      <CreatePageWithProviders
        data={graphqlDataProps}
        intl={createMockIntlProp()}
        atlassianAccessEnabled={true}
        loading={false}
        hasError={false}
      />
    );
  })
  .add('With Atlassian Access FF disabled', () => {
    return (
      <CreatePageWithProviders
        data={graphqlDataProps}
        intl={createMockIntlProp()}
        atlassianAccessEnabled={false}
        loading={false}
        hasError={false}
      />
    );
  })
  .add('With FF loading', () => {
    return (
      <CreatePageWithProviders
        data={graphqlDataProps}
        intl={createMockIntlProp()}
        atlassianAccessEnabled={true}
        loading={true}
        hasError={false}
      />
    );
  })
  .add('With FF error', () => {
    return (
      <CreatePageWithProviders
        data={graphqlDataProps}
        intl={createMockIntlProp()}
        atlassianAccessEnabled={true}
        loading={false}
        hasError={true}
      />
    );
  });
