import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';

import AkPage from '@atlaskit/page';

import { Button as AnalyticsButton, ScreenEventSender } from 'common/analytics';
import { ValuePropPage } from 'common/value-prop';

import { configureApolloClientWithUserFeatureFlag } from 'common/feature-flags/test-utils';

import { createApolloClient } from '../../apollo-client';
import { getNoOrgOnboardingProps, NoOrgOnboardingPageImpl } from './no-org-onboarding-page';

const mountPage = ({ atlassianAccessFlagEnabled = false } = {}) => {
  const wrapper = mount(
    <ApolloProvider
      client={
        configureApolloClientWithUserFeatureFlag({
          client: createApolloClient(),
          flagKey: 'atlassianaccess.enabled',
          flagValue: atlassianAccessFlagEnabled,
        })}
    >
      <IntlProvider locale="en">
        <AkPage>
          <NoOrgOnboardingPageImpl featureFlag={{ isLoading: false, value: false }} loading={false} currentSiteId="12345" />
        </AkPage>
      </IntlProvider>
    </ApolloProvider>,
  );

  const getFooterButton = () => wrapper.find(AnalyticsButton).last();

  return {
    wrapper,
    getFooterButton,
  };
};

describe('NoOrgOnboardingPage', () => {
  describe('Render', () => {
    it('should render a value prop page instance', () => {
      const wrapper = mountPage().wrapper;

      expect(wrapper.find(ValuePropPage).length).to.equal(1);
    });

    it('should set ScreenEventSender attributes', () => {
      const wrapper = mountPage().wrapper;

      const screenEventSender = wrapper.find(ScreenEventSender);

      expect(screenEventSender.props().event).to.deep.equal({
        cloudId: '12345',
        data: {
          attributes: {
            isNewLozengeShown: false,
          },
          name: 'accessNoOrgScreen',
        },
      });
    });
  });

  describe('Content', () => {
    const getText = (wrapper) => wrapper.text().toLowerCase();

    it('should mention Atlassian Access', () => {
      const wrapper = mountPage({
        atlassianAccessFlagEnabled: false,
      }).wrapper;

      const wrapperText = getText(wrapper);

      expect(wrapperText.includes('Atlassian Access')).to.equal(false);
    });
  });

  describe('Footer button', () => {
    it('should have a button to admin.atlassian.com', () => {
      const { getFooterButton } = mountPage();

      expect(getFooterButton().prop('href')).to.equal('http://localhost:3001/atlassian-access');
    });
  });

  describe('query props', () => {
    it('should return loading when loading it true', () => {
      const props = getNoOrgOnboardingProps({ data: { loading: true } } as any);

      expect(props.loading).to.equal(true);
    });

    it('should return loading when data is undefined', () => {
      const props = getNoOrgOnboardingProps({} as any);

      expect(props.loading).to.equal(true);
    });

    it('should return undefined when data has error', () => {
      const props = getNoOrgOnboardingProps({ data: { loading: false, error: true } } as any);

      expect(props.loading).to.equal(false);
      expect(props.currentSiteId).to.equal(undefined);
    });

    it('should return currentSiteId', () => {
      const props = getNoOrgOnboardingProps({ data: { loading: false, currentSite: { id: '1234' } } } as any);

      expect(props.loading).to.equal(false);
      expect(props.currentSiteId).to.equal('1234');
    });
  });
});
