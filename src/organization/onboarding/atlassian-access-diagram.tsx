// tslint:disable:no-require-imports
// tslint:disable:no-var-requires

import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import styled from 'styled-components';

import { AtlassianAccessDiagramAccounts } from './atlassian-access-diagram-accounts';
import { AtlassianAccessDiagramProducts } from './atlassian-access-diagram-products';
import { AtlassianAccessDiagramSteps } from './atlassian-access-diagram-steps';

const diagramMessages = defineMessages({
  products: {
    id: 'atlassian.access.diagram.products',
    defaultMessage: 'Atlassian products',
  },
  diagramLabel: {
    id: 'atlassian.access.diagram.diagramLabel',
    defaultMessage: 'A diagram of Atlassian Access',
  },
});

const Wrapper = styled.div`
  align-items: center;
  display: flex;
  flex-direction: column;
  width: 100%;
`;

// tslint:disable-next-line:no-empty-interface
interface OwnProps {}

export class AtlassianAccessDiagramImpl extends React.Component<InjectedIntlProps & OwnProps> {
  public render() {
    const { intl: { formatMessage } } = this.props;

    return (
      <Wrapper aria-label={formatMessage(diagramMessages.diagramLabel)}>
        <h3>{formatMessage(diagramMessages.products)}</h3>
        <AtlassianAccessDiagramProducts />
        <AtlassianAccessDiagramSteps />
        <AtlassianAccessDiagramAccounts />
      </Wrapper>
    );
  }
}

export const AtlassianAccessDiagram = injectIntl<OwnProps>(AtlassianAccessDiagramImpl);
