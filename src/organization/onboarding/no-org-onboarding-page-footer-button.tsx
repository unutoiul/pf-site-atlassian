// tslint:disable:no-require-imports
import * as React from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';

import { AnalyticsClientProps, Button as AnalyticsButton, UIEvent, withAnalyticsClient } from 'common/analytics';
import { getConfig } from 'common/config';

import { links } from 'common/link-constants';

import { createOrgAnalyticsData } from '../../organization/organizations/organization-analytics';

const analyticsActionSubject = 'noOrgOnboardingPage';

const messages = defineMessages({
  sectionPricingButton: {
    id: 'security.value.prop.page.aa.section.pricing.button',
    defaultMessage: 'Continue',
  },
});

interface OwnProps {
  buttonAnalytics?: UIEvent;
}

export class NoOrgOnboardingPageFooterButtonImpl extends React.Component<OwnProps & AnalyticsClientProps> {
  public render() {
    return (
      <AnalyticsButton
        href={getConfig().goToAdminLinkUrl + links.internal.atlassianAccess}
        onClick={this.onButtonClick}
        appearance="primary"
        target="_blank"
        analyticsData={createOrgAnalyticsData({
          action: 'click',
          actionSubject: analyticsActionSubject,
          actionSubjectId: 'goToAdminButton',
        })}
      >
        <FormattedMessage {...messages.sectionPricingButton} />
      </AnalyticsButton>
    );
  }

  private onButtonClick = () => {
    if (!this.props.buttonAnalytics) {
      return;
    }

    this.props.analyticsClient.sendUIEvent(this.props.buttonAnalytics);
  }
}

export const NoOrgOnboardingPageFooterButton = withAnalyticsClient(NoOrgOnboardingPageFooterButtonImpl);
