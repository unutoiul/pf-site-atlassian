import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';
import { Link, MemoryRouter } from 'react-router-dom';

import AkPage from '@atlaskit/page';

import { ValuePropPage } from 'common/value-prop';

import { configureApolloClientWithUserFeatureFlag } from 'common/feature-flags/test-utils';

import { createApolloClient } from '../../apollo-client';
import { createMockAnalyticsClient, createMockIntlProp } from '../../utilities/testing';
import { CreateOrgOnboardingPageImpl } from './create-org-onboarding-page';

const mountCreateOrgOnboardingPage = ({ atlassianAccessFlagEnabled = false } = {}) => {
  const wrapper = mount(
    <MemoryRouter>
      <ApolloProvider
        client={
          configureApolloClientWithUserFeatureFlag({
            client: createApolloClient(),
            flagKey: 'atlassianaccess.enabled',
            flagValue: atlassianAccessFlagEnabled,
          })}
      >
        <IntlProvider locale="en">
          <AkPage>
            <CreateOrgOnboardingPageImpl
              intl={createMockIntlProp()}
              match={null as any}
              location={null as any}
              history={null as any}
              totalOrganizations={undefined}
              totalOrganizationsLoadError={undefined}
              analyticsClient={createMockAnalyticsClient()}
            />
          </AkPage>
        </IntlProvider>
      </ApolloProvider>
    </MemoryRouter>,
  );

  const getFooterButtonLink = () => wrapper.find(Link).at(0);

  return {
    wrapper,
    getFooterButtonLink,
  };
};

describe('CreateOrgOnboardingPage', () => {
  describe('Render', () => {
    it('should render a value prop page instance', () => {
      const wrapper = mountCreateOrgOnboardingPage().wrapper;

      expect(wrapper.find(ValuePropPage).length).to.equal(1);
    });
  });

  describe('Content', () => {
    const getText = (wrapper) => wrapper.text().toLowerCase();

    it('should mention Atlassian access', () => {
      const wrapper = mountCreateOrgOnboardingPage({
        atlassianAccessFlagEnabled: false,
      }).wrapper;

      const wrapperText = getText(wrapper);

      expect(wrapperText.includes('Atlassian Access')).to.equal(false);
    });
  });

  describe('Footer button link', () => {
    it('should have the create org page passed as the to prop', () => {
      const { getFooterButtonLink } = mountCreateOrgOnboardingPage();

      const footerButtonLink = getFooterButtonLink();

      expect(footerButtonLink.props().to).to.equal('/o/create');
    });
  });
});
