import { graphql, QueryResult } from 'react-apollo';
import { RouteComponentProps } from 'react-router';

import { OrgLinkedSitesQuery, OrgLinkedSitesQueryVariables } from '../../schema/schema-types';
import { OrgRouteProps } from '../routes';
import orgLinkedSitesQuery from './with-linked-sites.query.graphql';

const withLinkedSitesQuery = graphql<RouteComponentProps<OrgRouteProps>, OrgLinkedSitesQuery, OrgLinkedSitesQueryVariables, LinkedSitesProps>(
  orgLinkedSitesQuery,
  {
    options: (props) => ({
      variables: {
        id: props.match.params.orgId,
      },
    }),
    props: (props): LinkedSitesProps => ({
      linkedSitesData: {
        loading: !props.data || props.data.loading,
        error: props.data && props.data.error,
        sites: props.data && props.data.organization && props.data.organization.sites,
      },
    }),
    skip: (props) => !props.match.params.orgId,
  },
);

export interface LinkedSitesProps {
  linkedSitesData: {
    loading: boolean;
    error?: QueryResult['error'];
    sites?: OrgLinkedSitesQuery['organization']['sites'] | undefined;
  };
}

export function withLinkedSites<TOwnProps extends RouteComponentProps<OrgRouteProps>>(
  Component: React.ComponentType<TOwnProps & LinkedSitesProps>,
): React.ComponentClass<TOwnProps> {
  return withLinkedSitesQuery(Component) as any;
}
