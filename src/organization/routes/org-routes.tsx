import * as React from 'react';
import { graphql, OptionProps } from 'react-apollo';
import { Redirect, Route, RouteComponentProps, Switch } from 'react-router-dom';

import { AuthorizationError, GenericError } from 'common/error';
import { PickLoader } from 'common/loading';
import { PageLayout } from 'common/page-layout';
import { PermissionErrorPage } from 'common/permission-error-page';

import { AdminPage } from '../../organization/admins/organization-admins-page';
import { DomainClaimPage } from '../../organization/domain-claim';
import { MemberDetailPage } from '../../organization/member-detail-page';
import { MemberPage } from '../../organization/member-page';
import { PasswordPolicyPage } from '../../organization/password-policy';
import { SamlPage } from '../../organization/saml';
import { TwoStepVerificationPage } from '../../organization/two-step-verification';
import { OrgRoutesQuery, OrgRoutesQueryVariables } from '../../schema/schema-types';
import { MemberExportDownloadPage } from '../member-export-download/member-export-download-page';
import { OrgSiteLinkingDrawerPage } from '../site-linking/org-site-linking-drawer-page';
import orgRoutesQuery from './org-routes.query.graphql';

const loadBillingPages = async () => import(
  /* webpackChunkName: "billing-pages" */
  '../../apps/billing',
);

const BillHistoryPage = PickLoader(
  loadBillingPages,
  ({ BillHistory }) => BillHistory,
);

const BillEstimatePage = PickLoader(
  loadBillingPages,
  ({ BillEstimate }) => BillEstimate,
);

const BillOverviewPage = PickLoader(
  loadBillingPages,
  ({ BillOverview }) => BillOverview,
);

const ManageSubscriptionsPage = PickLoader(
  loadBillingPages,
  ({ ManageSubscriptions }) => ManageSubscriptions,
);

const PaymentDetailPage = PickLoader(
  loadBillingPages,
  ({ PaymentDetail }) => PaymentDetail,
);

const LoadableUserProvisioningPage = PickLoader(
  async () => import(
    /* webpackChunkName: "user-provisioning" */
    '../user-provisioning/user-provisioning-page',
  ),
  ({ UserProvisioningPage }) => UserProvisioningPage,
);

const LoadableMasterOrgLandingPage = PickLoader(
  async () => import(
    /* webpackChunkName: "org-landing-page" */
    '../../organization/landing',
  ),
  ({ MasterOrgLandingPage }) => MasterOrgLandingPage,
);

const LoadableAdminApiPage = PickLoader(
  async () => import(
    /* webpackChunkName: "admin-api-page" */
    '../admin-api/admin-api-page',
  ),
  ({ AdminApiPage }) => AdminApiPage,
);

export interface OrgRouteProps {
  orgId: string;
}

export interface OrgRoutesDerivedProps {
  hasPermission: boolean;
  loading: boolean;
  hasError: boolean;
}

export class OrgRoutesImpl extends React.Component<OrgRoutesDerivedProps & RouteComponentProps<OrgRouteProps>> {
  public render() {
    const {
      loading,
      hasError,
      hasPermission,
      match: { params: { orgId } },
    } = this.props;

    const routes = (
      <Switch>
        <Redirect exact={true} from="/o/:orgId" to={`/o/${orgId}/overview`} />
        <Route path="/o/:orgId/overview" component={LoadableMasterOrgLandingPage} />
        <Route path="/o/:orgId/org-sites" component={OrgSiteLinkingDrawerPage} />
        <Route path="/o/:orgId/admins" component={AdminPage} />
        <Route path="/o/:orgId/domains" component={DomainClaimPage} />
        <Route path="/o/:orgId/members" component={MemberPage} exact={true} />
        <Route path="/o/:orgId/members/:memberId" component={MemberDetailPage} exact={true} />
        <Route path="/o/:orgId/members/export/:exportId" component={MemberExportDownloadPage} />
        <Route path="/o/:orgId/saml" component={SamlPage} />
        <Route path="/o/:orgId/user-provisioning" component={LoadableUserProvisioningPage} />
        <Route path="/o/:orgId/passwordpolicy" component={PasswordPolicyPage} />
        <Route path="/o/:orgId/two-step-verification" component={TwoStepVerificationPage} />
        <Route path="/o/:orgId/admin-api" component={LoadableAdminApiPage} />
        <Redirect exact={true} from="/o/:orgId/billing" to={`/o/${orgId}/billing/overview`} />
        <Route path="/o/:orgId/billing/overview" component={BillOverviewPage} />
        <Route path="/o/:orgId/billing/estimate" component={BillEstimatePage} />
        <Route path="/o/:orgId/billing/paymentdetails" component={PaymentDetailPage} />
        <Route path="/o/:orgId/billing/applications" component={ManageSubscriptionsPage} />
        <Route path="/o/:orgId/billing/history" component={BillHistoryPage} />
      </Switch>
    );

    if (loading) {
      return routes;
    }

    // no permission will come from an error during loading organization data, so we need to check for that first
    if (!hasPermission) {
      return <PermissionErrorPage />;
    }

    if (hasError) {
      return <PageLayout><GenericError /></PageLayout>;
    }

    return routes;
  }
}

const getHasPermission = (data: OptionProps<RouteComponentProps<OrgRouteProps>, OrgRoutesQuery>['data']) => {
  if (data && data.error) {
    return AuthorizationError.findIn(data.error).length === 0;
  }

  return true;
};

const withPermissions = graphql<RouteComponentProps<OrgRouteProps>, OrgRoutesQuery, OrgRoutesQueryVariables, OrgRoutesDerivedProps>(
  orgRoutesQuery,
  {
    options: (componentProps) => ({
      variables: {
        id: componentProps.match.params.orgId,
      },
    }),
    props: ({ data }): OrgRoutesDerivedProps => ({
      hasPermission: getHasPermission(data),
      loading: !!(data && data.loading),
      hasError: !!(data && data.error),
    }),
  },
);

export const OrgRoutes = withPermissions(OrgRoutesImpl);
