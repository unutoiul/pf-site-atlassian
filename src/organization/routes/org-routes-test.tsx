import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { Route } from 'react-router-dom';

import { GenericError } from 'common/error';
import { PermissionErrorPage } from 'common/permission-error-page';

import { OrgRoutesDerivedProps, OrgRoutesImpl } from './org-routes';

describe('Org routes', () => {
  const orgRoutesWrapper = (
    props: OrgRoutesDerivedProps = {
      loading: false,
      hasError: false,
      hasPermission: true,
    },
  ) => {
    return shallow(
      <OrgRoutesImpl
        match={{ params: { orgId: 'my-org-id' } }}
        {...props}
        {...{} as any}
      />,
    );
  };

  it('should render routes if there\'s no errors and the user has permission', () => {
    const wrapper = orgRoutesWrapper({
      loading: false,
      hasError: false,
      hasPermission: true,
    });

    expect(wrapper.find(PermissionErrorPage).exists()).to.equal(false);
    expect(wrapper.find(Route).exists()).to.equal(true);
  });

  it('should render routes if it\'s loading', () => {
    const wrapper = orgRoutesWrapper({
      loading: true,
      hasError: false,
      hasPermission: true,
    });

    expect(wrapper.find(PermissionErrorPage).exists()).to.equal(false);
    expect(wrapper.find(Route).exists()).to.equal(true);
  });

  it('should render the error page on error', () => {
    const wrapper = orgRoutesWrapper({
      loading: false,
      hasError: true,
      hasPermission: true,
    });

    expect(wrapper.find(GenericError).exists()).to.equal(true);
    expect(wrapper.find(Route).exists()).to.equal(false);
  });

  it('should render the permission denied page if there was a regular and a permission error', () => {
    const wrapper = orgRoutesWrapper({
      loading: false,
      hasError: true,
      hasPermission: false,
    });

    expect(wrapper.find(PermissionErrorPage).exists()).to.equal(true);
    expect(wrapper.find(Route).exists()).to.equal(false);
  });

  it('should render the permission error page if the user doesn\'t have permission', () => {
    const wrapper = orgRoutesWrapper({
      loading: false,
      hasError: false,
      hasPermission: false,
    });

    expect(wrapper.find(PermissionErrorPage).exists()).to.equal(true);
    expect(wrapper.find(Route).exists()).to.equal(false);
  });
});
