import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import { MemoryRouter } from 'react-router-dom';

import { Breadcrumb } from 'common/breadcrumb';

import { createMockContext } from '../../utilities/testing';
import { AdminBreadcrumb } from './admin-breadcrumb';

describe('AdminBreadcrumb', () => {
  it('should render a Breadcrumb with valid props', () => {
    const wrapper = mount((
      <MemoryRouter>
        <AdminBreadcrumb />
      </MemoryRouter>
    ), createMockContext({ intl: true, apollo: false }));

    const theBreadcrumb = wrapper.find(Breadcrumb);

    expect(theBreadcrumb).to.have.lengthOf(1);
    expect(theBreadcrumb.prop('href')).to.equal('/');
    expect(theBreadcrumb.text()).to.equal('Admin');
  });
});
