import * as React from 'react';
import {
  defineMessages,
  FormattedMessage,
} from 'react-intl';

import { Breadcrumb } from 'common/breadcrumb';

import { createOrgAnalyticsData } from '../organizations/organization-analytics';

const messages = defineMessages({
  text: {
    id: 'admin.breadcrumb.text',
    defaultMessage: 'Admin',
  },
});

interface OwnProps {
  hasSeparator?: boolean;
}

type AdminBreadcrumbProps = OwnProps;

export class AdminBreadcrumb extends React.Component<AdminBreadcrumbProps> {
  public render() {
    return (
      <Breadcrumb
        href="/"
        text={<FormattedMessage {...messages.text} />}
        hasSeparator={this.props.hasSeparator}
        analyticsData={createOrgAnalyticsData({
          action: 'click',
          actionSubject: 'orgBreadcrumb',
          actionSubjectId: 'admin',
        })}
      />
    );
  }
}
