import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import { MemoryRouter } from 'react-router';

import { Breadcrumb } from 'common/breadcrumb';

import { createMockContext } from '../../utilities/testing';
import { OrgBreadcrumbImpl } from './org-breadcrumb';

describe('OrgBreadcrumb', () => {
  const getWrapper = ({ loading, hasError, organization }) => {
    return mount((
      <MemoryRouter>
        <OrgBreadcrumbImpl
          loading={loading}
          hasError={hasError}
          organization={organization}
          match={null as any}
          location={null as any}
          history={null as any}
        />
      </MemoryRouter>
    ), createMockContext({ intl: true, apollo: true }));
  };

  it('should not render anything if it is loading', () => {
    const wrapper = getWrapper({
      loading: true,
      hasError: false,
      organization: {},
    });

    expect(wrapper.find(OrgBreadcrumbImpl).isEmptyRender()).to.equal(true);
  });

  it('should render the default name and path if it has an error', () => {
    const wrapper = getWrapper({
      loading: false,
      hasError: true,
      organization: {},
    });

    const theBreadcrumb = wrapper.find(Breadcrumb);

    expect(theBreadcrumb.length).to.equal(1);
    expect(theBreadcrumb.prop('href')).to.equal('#');
    expect(theBreadcrumb.text()).to.equal('Organization');
  });

  it('should render the default name and path no org data exists', () => {
    const wrapper = getWrapper({
      loading: false,
      hasError: false,
      organization: {},
    });

    const theBreadcrumb = wrapper.find(Breadcrumb);

    expect(theBreadcrumb.length).to.equal(1);
    expect(theBreadcrumb.prop('href')).to.equal('#');
    expect(theBreadcrumb.text()).to.equal('Organization');
  });

  it('should render a breadcrumb with the correct path to the org', () => {
    const wrapper = getWrapper({
      loading: false,
      hasError: false,
      organization: {
        id: '1',
        name: 'Acme',
      },
    });

    const theBreadcrumb = wrapper.find(Breadcrumb);

    expect(theBreadcrumb.length).to.equal(1);
    expect(theBreadcrumb.prop('href')).to.equal('/o/1/overview');
    expect(theBreadcrumb.text()).to.equal('Acme');
  });
});
