import * as React from 'react';
import { graphql } from 'react-apollo';
import { defineMessages, FormattedMessage } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';

import { Breadcrumb } from 'common/breadcrumb';

import { OrgRouteProps } from '../../organization/routes';
import { OrgBreadcrumbQuery, OrgBreadcrumbQueryVariables } from '../../schema/schema-types';
import { createOrgAnalyticsData } from '../organizations/organization-analytics';
import orgBreadcrumbQuery from './org-breadcrumb.query.graphql';

interface OwnProps {
  hasSeparator?: boolean;
}

interface DerivedProps {
  organization: OrgBreadcrumbQuery['organization'] | undefined;
  loading: boolean;
  hasError: boolean;
}

type OrgBreadcrumbProps = OwnProps & DerivedProps & RouteComponentProps<OrgRouteProps>;

const messages = defineMessages({
  organization: {
    id: 'org.breadcrumb.name',
    defaultMessage: 'Organization',
  },
});

export class OrgBreadcrumbImpl extends React.Component<OrgBreadcrumbProps> {
  public render() {
    const {
      loading,
      hasError,
      organization,
      hasSeparator,
    } = this.props;

    if (loading) {
      return null;
    }

    let path: string;
    let text: string | JSX.Element;

    if (hasError || !organization || !organization.id) {
      path = '#';
      text = <FormattedMessage {...messages.organization} />;
    } else {
      path = `/o/${organization.id}/overview`;
      text = organization.name;
    }

    return (
      <Breadcrumb
        href={path}
        text={text}
        hasSeparator={hasSeparator}
        analyticsData={createOrgAnalyticsData({
          action: 'click',
          actionSubject: 'orgBreadcrumb',
          actionSubjectId: 'org',
        })}
      />
    );
  }
}

const withOrgData = graphql<RouteComponentProps<OrgRouteProps>, OrgBreadcrumbQuery, OrgBreadcrumbQueryVariables, DerivedProps>(orgBreadcrumbQuery, {
  options: (props) => ({
    variables: { id: props.match.params.orgId },
  }),
  skip: (props) => !props.match.params.orgId,
  props: ({ data }): DerivedProps => ({
    organization: data && data.organization,
    loading: !!(data && data.loading),
    hasError: !!(data && data.error),
  }),
});

export const OrgBreadcrumb = withRouter<{}>(
  withOrgData(OrgBreadcrumbImpl),
);
