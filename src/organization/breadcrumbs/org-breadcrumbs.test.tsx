import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import { AdminBreadcrumb, OrgBreadcrumb } from '.';
import { createMockContext } from '../../utilities/testing';
import { OrgBreadcrumbs } from './org-breadcrumbs';

describe('OrgBreadcrumbs', () => {
  const getWrapper = ({ children }) => {
    return shallow(
      <OrgBreadcrumbs children={children} />,
      createMockContext({ intl: true, apollo: true }),
    );
  };

  it('should render the default breadcrumbs: Admin > Org Name', () => {
    const wrapper = getWrapper({
      children: null,
    });

    expect(wrapper.find(AdminBreadcrumb)).to.have.lengthOf(1);
    expect(wrapper.find(OrgBreadcrumb)).to.have.lengthOf(1);
  });

  it('should render the default and any children', () => {
    const wrapper = getWrapper({
      children: [
        <OrgBreadcrumb key="test" />,
      ],
    });

    expect(wrapper.find(AdminBreadcrumb)).to.have.lengthOf(1);
    expect(wrapper.find(OrgBreadcrumb)).to.have.lengthOf(2);
  });
});
