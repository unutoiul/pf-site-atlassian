import * as React from 'react';

import {
  BreadcrumbsStateless as AkBreadcrumbsStateless,
} from '@atlaskit/breadcrumbs';

import { AdminBreadcrumb } from './admin-breadcrumb';
import { OrgBreadcrumb } from './org-breadcrumb';

/**
 * Reuse this component when you want to render everything up to
 * the organization name. Pass additional breadcrumbs as children.
 * @returns {string} "Admin / Organization"
 */
export class OrgBreadcrumbs extends React.Component {
  public render() {
    return (
      <AkBreadcrumbsStateless>
        <AdminBreadcrumb />
        <OrgBreadcrumb />
        {this.props.children}
      </AkBreadcrumbsStateless>
    );
  }
}
