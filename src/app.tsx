import * as React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import { RedirectFunc } from './apollo-client';
import { AdminApp } from './organization/admin-app';
import { AppProviders } from './organization/app-providers';
import { SiteAdminApp } from './site/site-admin-app';
import { redirectToLoginOrSignupOrNoop } from './utilities/auth';

interface Props {
  redirectToLogin?: RedirectFunc;
}

export class App extends React.Component<Props> {
  public static readonly defaultProps: Partial<Props> = {
    redirectToLogin: redirectToLoginOrSignupOrNoop,
  };

  public render() {
    return (
      <Router>
        <Switch>
          <Route
            path="/admin"
            render={this.withAppProviders(SiteAdminApp)}
          />
          {/* It's fine to mount a component on `/` while site admin is being shipped, since `index.html` will never get served from anything but `/admin` there. */}
          <Route
            path=""
            render={this.withAppProviders(AdminApp)}
          />
        </Switch>
      </Router>
    );
  }

  private withAppProviders(Component) {
    return (props) => (
      <AppProviders redirectToLogin={this.props.redirectToLogin}>
        <Component {...props} />
      </AppProviders>
    );
  }
}
