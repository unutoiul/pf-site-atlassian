import * as React from 'react';
import { defineMessages, FormattedMessage, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import styled from 'styled-components';

import AkShortcutIcon from '@atlaskit/icon/glyph/shortcut';
import AkLozenge from '@atlaskit/lozenge';
import { AkNavigationItem } from '@atlaskit/navigation';
import { gridSize as akGridSize } from '@atlaskit/theme';

import { withAnalyticsClient } from 'common/analytics';
import { navigationItemEvent } from 'common/analytics/analytics-event-data';
import { AnalyticsClientProps } from 'common/analytics/with-analytics-client';
import { ExternalLink, NavigationLinkIcon, NavigationLinkIconGlyph, NavItem, RouterLink } from 'common/navigation';

const messages = defineMessages({
  newTab: {
    id: 'chrome.org.navigation-link.open-in-new-tab',
    description: 'Used as an alternative accessible text to describe that an icon will open the text in a new tab',
    defaultMessage: 'Open in new tab',
  },
  betaLozengeText: {
    id: 'chrome.navigation-link.beta-lozenge-text',
    description: 'Used to indicate a navigation item as "beta" functionality',
    defaultMessage: 'Beta',
  },
  newLozengeText: {
    id: 'chrome.navigationv2-link.new-lozenge-text',
    description: 'Used to indicate a navigation item as "new" functionality',
    defaultMessage: 'New',
  },
});

const LozengeContainer = styled.div`
  display: inline-block;
  margin: 0 ${akGridSize}px;
`;

export interface NavigationLinkProps {
  path?: string;
  title?: string;
  isExternal?: boolean;
  icon?: NavigationLinkIconGlyph;
  iconRight?: React.ReactNode;
  exact?: boolean;
  lozenge?: NavItem['lozenge'];
  actionSubjectId?: string;
  onClick?(event: Event): void;
}
class NavigationLinkImpl extends React.Component<NavigationLinkProps & InjectedIntlProps & RouteComponentProps<any> & AnalyticsClientProps> {
  public static defaultProps = {
    onClick: () => null,
    path: '',
    title: '',
    isExternal: false,
  };

  public onClick = (e) => {
    if (this.props.onClick) {
      this.props.onClick(e);
    }
    if (!this.props.actionSubjectId) {
      return;
    }
    this.props.analyticsClient.sendUIEvent(navigationItemEvent('adminNavigation', this.props.actionSubjectId));
  }

  public render() {
    const {
      path,
      isExternal,
      icon,
      iconRight,
      intl,
    } = this.props;

    const iconNode = iconRight || (isExternal && <AkShortcutIcon label={intl.formatMessage(messages.newTab)} size="small" />);

    const textAfter = iconNode
      ? iconNode
      : null;
    const LinkComponent = isExternal ? ExternalLink : RouterLink;

    return (
      <AkNavigationItem
        icon={icon ? <NavigationLinkIcon icon={icon} /> : null}
        href={path}
        text={this.getContent()}
        onClick={this.onClick}
        linkComponent={path ? LinkComponent : null}
        isSelected={this.isSelected()}
        textAfter={textAfter}
      />
    );
  }

  private getContent(): React.ReactNode {
    const {
      title,
    } = this.props;

    return (
      <React.Fragment>
        {title}
        {this.getLozenge()}
      </React.Fragment>
    );
  }

  private getLozenge() {
    const {
      lozenge,
    } = this.props;

    if (lozenge && lozenge.type === 'beta') {
      return (
        <LozengeContainer>
          <AkLozenge
            appearance="new"
          >
            <FormattedMessage {...messages.betaLozengeText} />
          </AkLozenge>
        </LozengeContainer>
      );
    }

    if (lozenge && lozenge.type === 'new') {
      return (
        <LozengeContainer>
          <AkLozenge
            appearance="new"
          >
            <FormattedMessage {...messages.newLozengeText} />
          </AkLozenge>
        </LozengeContainer>
      );
    }

    return null;
  }

  private isSelected = () => {
    if (!this.props.path) {
      return false;
    }

    if (!this.props.exact) {
      return this.props.location.pathname.indexOf(this.props.path) === 0;
    }

    return this.props.location.pathname === this.props.path;
  }
}

export const NavigationLink = withRouter(injectIntl<NavigationLinkProps>(withAnalyticsClient(NavigationLinkImpl)));
