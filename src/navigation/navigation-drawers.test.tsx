import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import { UserInviteDrawer } from '../site/user-management/users/user-invite-drawer/user-invite-drawer';
import { noop } from '../utilities/testing';
import { SiteNavigationCreateDrawer } from './drawers/site-navigation-create-drawer';
import { NavigationDrawerImpl } from './navigation-drawers';

describe('NavigationDrawers', () => {
  it('renders the site create drawer when that drawer is active', () => {
    const wrapper = shallow(
      <NavigationDrawerImpl
        showDrawer={noop}
        activeDrawer={'SITE_CREATE'}
        match={{
          params: {
            cloudId: 'test-cloud-id',
          },
        } as any}
        location={{} as any}
        history={{} as any}
      />,
    );

    expect(wrapper.find(SiteNavigationCreateDrawer).props().isOpen).to.equal(true);
  });
  it('renders the invite drawer when that drawer is active', () => {
    const wrapper = shallow(
      <NavigationDrawerImpl
        showDrawer={noop}
        activeDrawer={'INVITE_USER'}
        match={{
          params: {
            cloudId: 'test-cloud-id',
          },
        } as any}
        location={{} as any}
        history={{} as any}
      />,
    );

    expect(wrapper.find(UserInviteDrawer).props().isOpen).to.equal(true);
  });
});
