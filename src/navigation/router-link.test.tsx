// tslint:disable:jsx-use-translation-function
import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { Link } from 'react-router-dom';

import { RouterLink } from './router-link';

describe('Router link', () => {
  const noop = () => null;
  const href = 'http://atlassian.com';
  const className = 'test';

  it('should return a link component if theres a href with a valid url', () => {
    const wrapper = shallow((
      <RouterLink
        href={href}
        className={className}
        onMouseDown={noop}
        onClick={noop}
      />
    ));

    const link = wrapper.find(Link);

    expect(link.prop('to')).to.equal(href);
    expect(link.prop('className')).to.equal(className);
  });

  it('should return the children if no href exist', () => {
    const wrapper = shallow((
      <RouterLink
        href=""
        className={className}
        onMouseDown={noop}
        onClick={noop}
      >
        <b>Child!</b>
      </RouterLink>
    ));

    expect(wrapper.find(Link).exists()).to.equal(false);
    expect(wrapper.children()).to.have.length(1);
  });
});
