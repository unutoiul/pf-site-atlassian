import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';
import { MemoryRouter, Route } from 'react-router';

import AkPage from '@atlaskit/page';

import navigationHeaderQuery from './navigation-header-org.query.graphql';

import { createApolloClient } from '../apollo-client';
import { noop } from '../utilities/testing';
import { NavigationImpl } from './navigation';

const client = createApolloClient();

const query = navigationHeaderQuery;

client.writeQuery({
  query,
  data: { organization: { id: 'my-org-id', name: 'Acme', __typename: 'Organization' } },
  variables: {
    id: 'my-org-id',
  },
});

client.writeQuery({
  query,
  data: { organization: { id: 'long-org-id', name: `Derek Zoolander's School for Kids Who Can't Read Good and Want to Learn to Do Other Stuff Good Too`, __typename: 'Organization' } },
  variables: {
    id: 'long-org-id',
  },
});

const NavigationWithMocks = (SampleNavigation) => () => (
  <ApolloProvider client={client}>
    <IntlProvider locale="en">
      <AkPage
        navigation={SampleNavigation}
      />
    </IntlProvider>
  </ApolloProvider>
);

storiesOf('Organization|Navigation V2', module)
  .add('Initial', () => {
    return (
      <MemoryRouter initialEntries={[{ pathname: '/o/my-org-id/overview' }]}>
        <Route path="/o/:orgId" component={NavigationWithMocks((<NavigationImpl showDrawer={noop} showContainer={true} navType="default" />))} />
      </MemoryRouter>
    );
  })
  .add('With global only', () => {
    return (
      <MemoryRouter initialEntries={[{ pathname: '/o/my-org-id/overview' }]}>
        <Route path="/o/:orgId" component={NavigationWithMocks((<NavigationImpl showDrawer={noop} showContainer={false} navType="default" />))} />
      </MemoryRouter>
    );
  })
  .add('With a long org name', () => {
    return (
      <MemoryRouter initialEntries={[{ pathname: '/o/long-org-id/overview' }]}>
        <Route path="/o/:orgId" component={NavigationWithMocks((<NavigationImpl showDrawer={noop} showContainer={true} navType="default" />))} />
      </MemoryRouter>
    );
  })
  .add('With nested org navigation', () => {
    return (
      <MemoryRouter initialEntries={[{ pathname: '/o/my-org-id/billing/details' }]}>
        <Route path="/o/:orgId" component={NavigationWithMocks((<NavigationImpl showDrawer={noop} showContainer={true} navType="default" />))} />
      </MemoryRouter>
    );
  });
