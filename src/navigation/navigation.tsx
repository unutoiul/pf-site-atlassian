import * as React from 'react';
import { graphql } from 'react-apollo';
import styled from 'styled-components';

import {
  AkGlobalNavigation,
  default as AkNavigation,
  presetThemes as akPresetThemes,
} from '@atlaskit/navigation';
import { gridSize as akGridSize } from '@atlaskit/theme';

import { HelpMenu } from 'common/help';
import { GlobalAtlassianIcon } from 'common/icons';
import { UserMenu } from 'common/user';

import { ShowDrawerProps, withShowDrawer } from 'common/drawer';

import { NavigationQuery } from '../schema/schema-types';
import { NavigationDrawers } from './navigation-drawers';
import { NavigationHeaderOrg } from './navigation-header-org';
import { NavigationHeaderSite } from './navigation-header-site';
import navigationQuery from './navigation.query.graphql';
import { NestedNavigation as OrgNestedNavigation } from './organization/nested-navigation';
import { RouterLink } from './router-link';
import { NestedNavigation as SiteNestedNavigation } from './site/nested-navigation';
import { SiteNavigationCreateIcon } from './site/site-navigation-create-icon';

export const GlobalNavigationSpacer = styled.div`
  width: ${akGridSize() * 8}px;
`;

export const GlobalNavigationWrapper = styled.div`
  position: fixed;
  left: 0;
  height: 100vh;
  display: flex;
`;

interface QueryDerivedProps {
  showContainer: boolean;
}

export type NavType = 'site' | 'org' | 'default';

interface OwnProps {
  navType: NavType;
  withoutDrawers?: boolean;
}

type AllProps = QueryDerivedProps & OwnProps & ShowDrawerProps;

export class NavigationImpl extends React.Component<AllProps> {
  public render() {
    const secondaryActions = [
      <HelpMenu key="help.menu" />,
      <UserMenu key="user.menu" />,
    ];

    if (!this.props.showContainer) {
      return (
        <GlobalNavigationSpacer>
          <GlobalNavigationWrapper>
            <AkGlobalNavigation
              theme={akPresetThemes.settings}
              primaryIcon={<GlobalAtlassianIcon />}
              secondaryActions={secondaryActions}
              primaryItemHref="/"
              linkComponent={RouterLink}
            />
          </GlobalNavigationWrapper>
        </GlobalNavigationSpacer>
      );
    }

    return (
      <AkNavigation
        containerTheme={akPresetThemes.settings}
        containerHeaderComponent={
          this.props.navType === 'site' ? NavigationHeaderSite : NavigationHeaderOrg
        }
        globalTheme={akPresetThemes.settings}
        globalPrimaryIcon={<GlobalAtlassianIcon />}
        globalPrimaryItemHref="/"
        globalCreateIcon={this.props.navType === 'site' ? <SiteNavigationCreateIcon /> : null}
        onCreateDrawerOpen={this.props.navType === 'site' ? this.onSiteCreateDrawerOpen : undefined}
        linkComponent={RouterLink}
        isCollapsible={false}
        isResizeable={false}
        globalSecondaryActions={secondaryActions}
        drawers={this.props.withoutDrawers ? [] : [<NavigationDrawers key="all" />]}
      >
        {this.props.navType === 'site' ? <SiteNestedNavigation /> : <OrgNestedNavigation />}
      </AkNavigation>
    );
  }

  private onSiteCreateDrawerOpen = () => {
    this.props.showDrawer('SITE_CREATE');
  }
}

const withNavigationState = graphql<OwnProps, NavigationQuery, {}, QueryDerivedProps>(
  navigationQuery,
  {
    props: ({ data }): QueryDerivedProps => {
      return {
        showContainer: !!(data && data.ui && data.ui.navigation.showContainer),
      };
    },
    options: {
      fetchPolicy: 'cache-only',
    },
  },
);

export const Navigation = withNavigationState(
  withShowDrawer(NavigationImpl),
);

export {
  NavigationDrawers,
};
