import * as React from 'react';
import { graphql } from 'react-apollo';
import { RouteComponentProps, withRouter } from 'react-router-dom';

import AkCrossIcon from '@atlaskit/icon/glyph/cross';
import {
  AkCustomDrawer,
} from '@atlaskit/navigation';

import { createAdminApiKeyDrawerEventMeta } from 'common/analytics';
import { ShowDrawerProps, withShowDrawer } from 'common/drawer';
import { FocusedTask } from 'common/focused-task';
import { GlobalAtlassianIcon } from 'common/icons';

import { BUXDrawer } from '../apps/billing/adapters/Drawer';
import { CreateAdminApiKeyDrawer } from '../organization/admin-api/create/create-admin-api-key-drawer';
import { OrgSiteLinkingDrawer } from '../organization/site-linking/org-site-linking-drawer';
import { CreateDirectoryDrawer } from '../organization/user-provisioning/create-directory/create-directory-drawer';
import { RegenerateApiKeyDrawer } from '../organization/user-provisioning/regenerate-api-key/regenerate-api-key-drawer';
import { Drawer, NavigationQuery } from '../schema/schema-types';
import { SiteRouteProps } from '../site/routes';
import { UserExportDrawer } from '../site/user-management/users/user-export-drawer/user-export-drawer';
import { UserInviteDrawer } from '../site/user-management/users/user-invite-drawer/user-invite-drawer';
import { IdentityManagerDrawer, SamlConfigDrawer } from './drawers';
import { SiteNavigationCreateDrawer } from './drawers/site-navigation-create-drawer';
import navigationQuery from './navigation.query.graphql';

interface QueryDerivedProps {
  activeDrawer: Drawer | undefined;
}

type AllProps = QueryDerivedProps & ShowDrawerProps & RouteComponentProps<Partial<SiteRouteProps>>;

export class NavigationDrawerImpl extends React.Component<AllProps> {
  public render() {
    return (
      <React.Fragment>
        <FocusedTask
          key="CREATE_ADMIN_API_KEY_DRAWER"
          isOpen={this.props.activeDrawer === 'CREATE_ADMIN_API_KEY_DRAWER'}
          onClose={this.closeDrawer}
          uiEvent={createAdminApiKeyDrawerEventMeta}
        >
          <CreateAdminApiKeyDrawer onCloseDrawer={this.closeDrawer} />
        </FocusedTask>
        <OrgSiteLinkingDrawer
          key="ORG_SITE_LINKING"
          isOpen={this.props.activeDrawer === 'ORG_SITE_LINKING'}
          onClose={this.closeDrawer}
        />
        <FocusedTask
          key="USER_PROVISIONING_CREATE_DIRECTORY"
          isOpen={this.props.activeDrawer === 'USER_PROVISIONING_CREATE_DIRECTORY'}
          onClose={this.closeDrawer}
        >
          <CreateDirectoryDrawer onCloseDrawer={this.closeDrawer} />
        </FocusedTask>
        <FocusedTask
          key="USER_PROVISIONING_REGEN_API_KEY"
          isOpen={this.props.activeDrawer === 'USER_PROVISIONING_REGEN_API_KEY'}
          onClose={this.closeDrawer}
        >
          <RegenerateApiKeyDrawer onCloseDrawer={this.closeDrawer} />
        </FocusedTask>
        <AkCustomDrawer
          key="IDM"
          width="full"
          isOpen={this.props.activeDrawer === 'IDM'}
          backIcon={<AkCrossIcon label="" />}
          primaryIcon={<GlobalAtlassianIcon />}
          onBackButton={this.closeDrawer}
        >
          <IdentityManagerDrawer onCloseDrawer={this.closeDrawer} />
        </AkCustomDrawer>
        <AkCustomDrawer
          key="SAML"
          width="full"
          isOpen={this.props.activeDrawer === 'SAML'}
          backIcon={<AkCrossIcon label="" />}
          primaryIcon={<GlobalAtlassianIcon />}
          onBackButton={this.closeDrawer}
        >
          <SamlConfigDrawer onCloseDrawer={this.closeDrawer} />
        </AkCustomDrawer>
        <BUXDrawer
          key="bux"
        />
        {this.props.match.params.cloudId ? (
          <React.Fragment>
            <UserInviteDrawer
              key="INVITE_USER"
              isOpen={this.props.activeDrawer === 'INVITE_USER'}
              onClose={this.closeDrawer}
              cloudId={this.props.match.params.cloudId}
            />
            <UserExportDrawer
              key="EXPORT_USERS"
              isOpen={this.props.activeDrawer === 'EXPORT_USERS'}
              onClose={this.closeDrawer}
              cloudId={this.props.match.params.cloudId}
            />
            <SiteNavigationCreateDrawer
              key="SITE_CREATE"
              onCloseDrawer={this.closeDrawer}
              onShowInviteDrawer={this.showInviteDrawer}
              isOpen={this.props.activeDrawer === 'SITE_CREATE'}
              cloudId={this.props.match.params.cloudId}
            />
          </React.Fragment>
        ) : null}
      </React.Fragment>
    );
  }

  private closeDrawer = () => {
    this.props.showDrawer('NONE');
  };

  private showInviteDrawer = () => {
    this.props.showDrawer('INVITE_USER');
  };
}

const withNavigationState = graphql<{}, NavigationQuery, {}, QueryDerivedProps>(
  navigationQuery,
  {
    props: ({ data }): QueryDerivedProps => {
      return {
        activeDrawer: data && data.ui && data.ui.navigation.activeDrawer,
      };
    },
    options: {
      fetchPolicy: 'cache-only',
    },
  },
);

export const NavigationDrawers = withNavigationState(
  withShowDrawer(
    withRouter(
      NavigationDrawerImpl,
    ),
  ),
);
