import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import { InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';
import styled from 'styled-components';

import AkTextField from '@atlaskit/field-text';
import AkSpinner from '@atlaskit/spinner';
import { akColorN300, akGridSizeUnitless } from '@atlaskit/util-shared-styles';

import { analyticsClient, Button as AnalyticsButton } from 'common/analytics';
import { ButtonGroup as ButtonGroupBase } from 'common/button-group';
import TextArea from 'common/field-textarea';

import { OrgRouteProps } from '../../organization/routes';

import { createOrgAnalyticsData } from '../../organization/organizations/organization-analytics';
import getSamlConfiguration from '../../organization/saml/saml-configuration.query.graphql';
import { defaultMessages, updateMessages } from '../../organization/saml/saml-messages';
import updateSaml from '../../organization/saml/update-saml.mutation.graphql';
import { Auth0State, SamlConfigurationQuery, SamlConfigurationQueryVariables, UpdateSamlMutation } from '../../schema/schema-types';

type SamlDrawerAction = 'ADD' | 'EDIT';

const Page = styled.div`
  margin: 0 auto;
  padding-top: ${akGridSizeUnitless * 5}px;
  padding-bottom: ${akGridSizeUnitless * 2}px;
  width: 350px;
`;

const ButtonGroup = styled(ButtonGroupBase)`
  margin-top: ${akGridSizeUnitless * 5}px;
`;

const FieldDescription = styled.div`
  color: ${akColorN300};
  font-size: 11px;
  margin-top: ${akGridSizeUnitless / 2}px;
`;

const SaveButton = styled.span `
  button span > div {
    min-width: ${akGridSizeUnitless * 15.5}px;
  }
`;

function pickSamlProperties(
  saml: {
    issuer: string,
    ssoUrl: string,
    publicCertificate: string,
    auth0MigrationState: Auth0State,
  } | null | undefined,
  ): SamlConfigDrawerState {

  const auth0MigrationState = saml ? saml.auth0MigrationState
    : 'AUTH0FORCED';

  return {
    issuer: saml ? saml.issuer : '',
    ssoUrl: saml ? saml.ssoUrl : '',
    publicCertificate: saml ? saml.publicCertificate : '',
    auth0MigrationState,
    isLoading: false,
    action: saml ? 'EDIT' : 'ADD',
  };
}

function isSamlEnabled(props) {
  return props.data.organization &&
    props.data.organization.security &&
    props.data.organization.security.saml;
}

interface OwnProps {
  onCloseDrawer(): void;
}
interface SamlConfigDrawerState {
  issuer: string;
  ssoUrl: string;
  publicCertificate: string;
  auth0MigrationState: Auth0State;
  isLoading: boolean;
  action: SamlDrawerAction;
}
type SamlConfigDrawerProps = ChildProps<OwnProps & RouteComponentProps<OrgRouteProps>, SamlConfigurationQuery>;
type UpdateSamlMutationProps = ChildProps<SamlConfigDrawerProps, UpdateSamlMutation>;

export class SamlConfigDrawerImpl extends React.Component<UpdateSamlMutationProps & InjectedIntlProps, SamlConfigDrawerState> {
  constructor(props: UpdateSamlMutationProps & InjectedIntlProps) {
    super(props);

    if (!props.data) {
      return;
    }

    const { organization } = props.data;

    this.state = pickSamlProperties(organization && organization.security && organization.security.saml);
  }

  public componentWillReceiveProps(nextProps: SamlConfigDrawerProps) {
    if (!this.props.data || !nextProps.data) {
      return;
    }

    const saml = isSamlEnabled(this.props);
    const nextSaml = isSamlEnabled(nextProps);
    if (JSON.stringify(saml) !== JSON.stringify(nextSaml)) {
      this.setState(pickSamlProperties(nextSaml));
    }
  }

  public render() {
    const { formatMessage } = this.props.intl;
    const {
      data: {
        loading = false,
        organization = null,
      } = {},
      onCloseDrawer,
      match: { params: { orgId } },
    } = this.props;

    const analyticsActionSubject = this.state.action === 'ADD' ? 'addSamlButton' : 'editSamlButton';

    return loading || !organization || !organization.security ?
        <Page><AkSpinner /></Page> :
        (
          <Page>
            <h3>{formatMessage(organization.security.saml ? updateMessages.updateHeading : updateMessages.addHeading)}</h3>
            <form onSubmit={this.onSubmit}>
              <AkTextField
                // tslint:disable-next-line:jsx-use-translation-function
                placeholder="https://yoursaml.com/app/acme"
                label={formatMessage(defaultMessages.issuer)}
                onChange={this.onIssuerChange}
                shouldFitContainer={true}
                id="update-saml-issuer"
                value={organization.security.saml ? organization.security.saml.issuer : ''}
              />
              <FieldDescription>{formatMessage(defaultMessages.issuerDescription)}</FieldDescription>
              <AkTextField
                // tslint:disable-next-line:jsx-use-translation-function
                placeholder="https://yoursaml.com/app/acme/saml/sso"
                label={formatMessage(defaultMessages.ssoUrl)}
                onChange={this.onSsoUrlChange}
                shouldFitContainer={true}
                id="update-saml-sso-url"
                value={organization.security.saml ? organization.security.saml.ssoUrl : ''}
              />
              <FieldDescription>{formatMessage(defaultMessages.ssoUrlDescription)}</FieldDescription>
              <TextArea
                label={formatMessage(defaultMessages.publicCertificate)}
                onChange={this.onPublicCertificateChange}
                shouldFitContainer={true}
                rows={4}
                id="update-saml-public-certificate"
                value={organization.security.saml ? organization.security.saml.publicCertificate : ''}
              />
              <FieldDescription>{formatMessage(defaultMessages.publicCertificateDescription)}</FieldDescription>
              <ButtonGroup alignment="right">
                <SaveButton>
                  <AnalyticsButton
                    appearance="primary"
                    type="submit"
                    isLoading={this.state.isLoading}
                    isDisabled={!this.state.issuer || !this.state.publicCertificate || !this.state.ssoUrl}
                    analyticsData={createOrgAnalyticsData({
                      orgId,
                      action: 'click',
                      actionSubject: analyticsActionSubject,
                      actionSubjectId: 'save',
                    })}
                  >
                    {formatMessage(updateMessages.saveButton)}
                  </AnalyticsButton>
                </SaveButton>
                <AnalyticsButton
                  appearance="subtle-link"
                  onClick={onCloseDrawer}
                  analyticsData={createOrgAnalyticsData({
                    orgId,
                    action: 'click',
                    actionSubject: analyticsActionSubject,
                    actionSubjectId: 'cancel',
                  })}
                >
                  {formatMessage(updateMessages.cancelButton)}
                </AnalyticsButton>
              </ButtonGroup>
            </form>
          </Page>
        );
  }

  private onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    this.setState({ isLoading: true });
    const {
      mutate,
      onCloseDrawer,
      match: { params: { orgId } },
    } = this.props;
    const {
      issuer,
      ssoUrl,
      publicCertificate,
      auth0MigrationState,
    } = this.state;

    if (!mutate) {
      return;
    }
    mutate({
      variables: { input: { id: orgId, issuer, ssoUrl, publicCertificate, auth0MigrationState } },
      optimisticResponse: {
        updateSaml: true,
      },
      refetchQueries: [{
        query: getSamlConfiguration,
        variables: { id: orgId },
      }],
    }).then(() => {
      onCloseDrawer();
      this.setState({ isLoading: false });
    }).catch((error) => {
      this.setState({ isLoading: false });
      analyticsClient.onError(error);
    });
  };

  private onIssuerChange = (e: React.ChangeEvent<HTMLInputElement>) => this.setState({ issuer: e.target.value });

  private onSsoUrlChange = (e: React.ChangeEvent<HTMLInputElement>) => this.setState({ ssoUrl: e.target.value });

  private onPublicCertificateChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => this.setState({ publicCertificate: e.target.value });
}

const withSamlData = graphql<OwnProps & RouteComponentProps<OrgRouteProps>, SamlConfigurationQuery, SamlConfigurationQueryVariables, SamlConfigDrawerProps>(getSamlConfiguration, {
  options: (ownProps) => ({ variables: { id: ownProps.match.params.orgId } }),
  skip: (componentProps) => !componentProps.match.params.orgId,
});

const withMutation = graphql<SamlConfigDrawerProps, UpdateSamlMutation, SamlConfigDrawerProps, UpdateSamlMutationProps>(updateSaml);

export const SamlConfigDrawer = withRouter<OwnProps>(
  withSamlData(
    withMutation(
      injectIntl(SamlConfigDrawerImpl),
    ),
  ),
);
