import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import { Button as AnalyticsButton } from 'common/analytics';
import { ValidationError } from 'common/error';

import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import { TryIdentityManagerButtonImpl } from './try-identity-manager-button';

describe('TryIdentityManagerButton', () => {
  let onEvaluateClickSpy;
  let doneSpy;
  let showFlagSpy;
  let sendUIEventSpy;

  const sandbox = sinon.sandbox.create();

  const match: any = {
    params: {
      orgId: 'DUMMY-TEST-ORG-ID',
    },
  };

  beforeEach(() => {
    doneSpy = sandbox.stub().resolves({});
    showFlagSpy = sandbox.stub().resolves({});
    sendUIEventSpy = sandbox.stub().resolves({});
    onEvaluateClickSpy = sandbox.stub().resolves({
      data: {
        evaluateIdentityManager: {
          progressUri: '/hello',
        },
      },
    });
  });

  afterEach(() => {
    sandbox.reset();
  });

  const identityManagerNavigationDrawer = () => {
    return shallow(
      <TryIdentityManagerButtonImpl
        done={doneSpy}
        onEvaluationClick={onEvaluateClickSpy}
        showFlag={showFlagSpy}
        hideFlag={() => null}
        match={match}
        intl={createMockIntlProp()}
        analyticsClient={{ sendUIEvent: sendUIEventSpy }}
        {...{} as any}
      />,
      createMockIntlContext(),
    );
  };

  it('should trigger evaluate action on clicking the evaluation of identity manager button', () => {
    const wrapper = identityManagerNavigationDrawer();
    const tryItNowButton = wrapper.find(AnalyticsButton);
    expect(tryItNowButton).to.have.length(1);
    expect(onEvaluateClickSpy.callCount).to.equal(0);

    tryItNowButton.simulate('click');

    expect(onEvaluateClickSpy.callCount).to.equal(1);
  });

  it('should trigger close drawer function once evaluation is enabled', async () => {
    const wrapper = identityManagerNavigationDrawer();
    const tryItNowButton = wrapper.find(AnalyticsButton);
    expect(tryItNowButton).to.have.length(1);

    tryItNowButton.simulate('click');

    await new Promise(resolve => setTimeout(resolve, 0));
    expect(doneSpy.callCount).to.equal(1);
  });

  it('should trigger show success flag once evaluation is enabled', async () => {
    const wrapper = identityManagerNavigationDrawer();
    const tryItNowButton = wrapper.find(AnalyticsButton);
    expect(tryItNowButton).to.have.length(1);

    tryItNowButton.simulate('click');

    await new Promise(resolve => setTimeout(resolve, 0));
    expect(showFlagSpy.callCount).to.equal(1);
    const showFlagArguments = showFlagSpy.firstCall.args[0];
    expect(showFlagArguments.id).to.equal('identity.manager.trial.completed.flag');
  });

  it('should show a generic error in case something happens', async () => {
    const onEvaluationFailedSpy = sandbox.stub().rejects({});
    const wrapper = shallow(
      <TryIdentityManagerButtonImpl
        doneSpyDrawer={doneSpy}
        onEvaluationClick={onEvaluationFailedSpy}
        showFlag={showFlagSpy}
        hideFlag={() => null}
        match={match}
        intl={createMockIntlProp()}
        analyticsClient={{ sendUIEvent: sendUIEventSpy }}
        {...{} as any}
      />,
      createMockIntlContext(),
    );
    const tryItNowButton = wrapper.find(AnalyticsButton);
    expect(tryItNowButton).to.have.length(1);

    tryItNowButton.simulate('click');

    await new Promise(resolve => setTimeout(resolve, 0));
    expect(showFlagSpy.callCount).to.equal(1);
    const showFlagArguments = showFlagSpy.firstCall.args[0];
    expect(showFlagArguments.id).to.equal('identity.manager.trial.generic.error.flag');
  });

  it('should trigger only one last name error flag for a validation error', async () => {
    const onEvaluationFailedSpy = sandbox.stub().rejects({
      graphQLErrors: [
        {
          originalError: new ValidationError({ code: 'lastNameRequired' }),
        },
        {
          originalError: new ValidationError({ code: 'lastNameRequired' }),
        },
      ],
    });
    const wrapper = shallow(
      <TryIdentityManagerButtonImpl
        doneSpyDrawer={doneSpy}
        onEvaluationClick={onEvaluationFailedSpy}
        showFlag={showFlagSpy}
        hideFlag={() => null}
        match={match}
        intl={createMockIntlProp()}
        analyticsClient={{ sendUIEvent: sendUIEventSpy }}
        {...{} as any}
      />,
      createMockIntlContext(),
    );
    const tryItNowButton = wrapper.find(AnalyticsButton);
    expect(tryItNowButton).to.have.length(1);

    tryItNowButton.simulate('click');

    await new Promise(resolve => setTimeout(resolve, 0));
    expect(showFlagSpy.callCount).to.equal(1);
    const showFlagArguments = showFlagSpy.firstCall.args[0];
    expect(showFlagArguments.id).to.equal('identity.manager.trial.lastname.error.flag');
  });

  it('should trigger sendUiEvent', () => {
    const wrapper = identityManagerNavigationDrawer();
    const tryItNowButton = wrapper.find(AnalyticsButton);

    tryItNowButton.simulate('click');

    expect(sendUIEventSpy.callCount).to.equal(1);
  });
});
