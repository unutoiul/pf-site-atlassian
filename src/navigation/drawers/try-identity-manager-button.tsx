import * as React from 'react';
import { graphql } from 'react-apollo';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';
import styled from 'styled-components';

import { gridSize as akGridSize } from '@atlaskit/theme';

import {
  analyticsClient,
  AnalyticsClientProps,
  Button as AnalyticsButton,
  tryFreeButtonClickedEvent,
  withAnalyticsClient } from 'common/analytics';
import { getConfig } from 'common/config';
import { createErrorIcon, createSuccessIcon, ValidationError } from 'common/error';
import { FlagDescriptor, FlagProps, withFlag } from 'common/flag';

import { OrgRouteProps } from '../../organization/routes';
import { TryIdentityManagerButtonMutation } from '../../schema/schema-types';
import { orgNavRefetchQueries } from '../organization/org-nav-updates';
import tryIdentityManagerButtonMutation from './try-identity-manager-button.mutation.graphql';

const messages = defineMessages({
  tryForFreeButton: {
    id: 'identity.manager.trial.try.for.free.button',
    defaultMessage: 'Try it free for 30 days',
  },
  evaluationSuccessFlagTitle: {
    id: 'identity.manager.trial.evaluation.success.flag.title',
    defaultMessage: 'Welcome to Atlassian Access!',
  },
  evaluationSuccessFlagDescription: {
    id: 'identity.manager.trial.evaluation.success.flag.description',
    defaultMessage: 'You\'ve started your Atlassian Access trial and can now apply security policy on managed accounts.',
  },
  genericErrorFlagTitle: {
    id: 'identity.manager.trial.generic.error.flag.title',
    defaultMessage: 'Uh oh! Something went wrong.',
  },
  genericErrorFlagDescription: {
    id: 'identity.manager.trial.generic.error.flag.description',
    defaultMessage: 'Could not evaluate Atlassian Access. Please try again in a few minutes.',
  },
  lastNameErrorFlagTitle: {
    id: 'identity.manager.trial.lastname.error.flag.title',
    defaultMessage: 'Last name required',
  },
  lastNameErrorFlagDescriptionPart1: {
    id: 'identity.manager.trial.lastname.error.flag.description.part1',
    defaultMessage: 'Please',
    description: `Part of the message, 'Please update your profile to include a last name before starting your Atlassian Access trial.'`,
  },
  lastNameErrorFlagDescriptionPart2: {
    id: 'identity.manager.trial.lastname.error.flag.description.part2',
    defaultMessage: 'to include a last name before starting your Atlassian Access trial.',
    description: `Part of the message, 'Please update your profile to include a last name before starting your Atlassian Access trial.'`,
  },
  lastNameErrorFlagDescriptionLink: {
    id: 'identity.manager.trial.lastname.error.flag.description.link',
    defaultMessage: 'update your profile',
    description: `Part of the message, 'Please update your profile to include a last name before starting your Atlassian Access trial.'`,
  },
});

const ProfileLink = styled.a`
  padding-right: ${akGridSize() / 2}px;
  padding-left: ${akGridSize() / 2}px;
`;

interface IdentityManagerDrawerState {
  isLoading: boolean;
}

interface OwnProps {
  done(): void;
}

interface DerivedProps {
  onEvaluationClick(): Promise<void>;
}

export class TryIdentityManagerButtonImpl extends React.Component<DerivedProps & InjectedIntlProps & OwnProps & FlagProps & RouteComponentProps<OrgRouteProps> & AnalyticsClientProps, IdentityManagerDrawerState> {
  public state: IdentityManagerDrawerState = {
    isLoading: false,
  };

  public render() {
    const { intl: { formatMessage } } = this.props;

    return (
      <AnalyticsButton
        isLoading={this.state.isLoading}
        appearance="primary"
        isDisabled={this.state.isLoading}
        onClick={this.evaluateIdentityManager}
      >
        {formatMessage(messages.tryForFreeButton)}
      </AnalyticsButton>
    );
  }

  private evaluateIdentityManager = async (): Promise<void> => {
    const {
      onEvaluationClick,
      showFlag,
      done,
      analyticsClient: { sendUIEvent },
    } = this.props;

    sendUIEvent(tryFreeButtonClickedEvent());

    this.setState({ isLoading: true });

    try {
      await onEvaluationClick();
      showFlag(this.evaluationCompletedFlag());
      done();
    } catch (e) {
      if (ValidationError.findIn(e).find(({ code }) => code === 'lastNameRequired')) {
        showFlag(this.lastNameErrorFlag());
      } else {
        analyticsClient.onError(e);
        showFlag(this.genericErrorFlag());
      }
    } finally {
      this.setState({ isLoading: false });
    }

  };

  private genericErrorFlag = (): FlagDescriptor => {
    return {
      appearance: 'normal',
      autoDismiss: false,
      icon: createErrorIcon(),
      id: 'identity.manager.trial.generic.error.flag',
      title: this.props.intl.formatMessage(messages.genericErrorFlagTitle),
      description: this.props.intl.formatMessage(messages.genericErrorFlagDescription),
    };
  };

  private evaluationCompletedFlag = (): FlagDescriptor => {
    return {
      appearance: 'normal',
      autoDismiss: true,
      icon: createSuccessIcon(),
      id: 'identity.manager.trial.completed.flag',
      title: this.props.intl.formatMessage(messages.evaluationSuccessFlagTitle),
      description: this.props.intl.formatMessage(messages.evaluationSuccessFlagDescription),
    };
  };

  private lastNameErrorFlag = (): FlagDescriptor => {
    const { intl: { formatMessage } } = this.props;

    return {
      appearance: 'normal',
      autoDismiss: false,
      icon: createErrorIcon(),
      id: 'identity.manager.trial.lastname.error.flag',
      title: formatMessage(messages.lastNameErrorFlagTitle),
      description: (
        <div>
          {formatMessage(messages.lastNameErrorFlagDescriptionPart1)}
          <ProfileLink href={getConfig().atlassianAccountUrl} target="_blank" rel="noopener noreferrer">
            {formatMessage(messages.lastNameErrorFlagDescriptionLink)}
          </ProfileLink>
          {formatMessage(messages.lastNameErrorFlagDescriptionPart2)}
        </div>
      ),
    };
  };
}

const withMutation = graphql< RouteComponentProps<OrgRouteProps> & OwnProps, TryIdentityManagerButtonMutation, {}, DerivedProps & OwnProps & RouteComponentProps<OrgRouteProps>>(
  tryIdentityManagerButtonMutation,
  {
    props: ({ ownProps, mutate }) => ({
      ...ownProps,
      onEvaluationClick: async (): Promise<void> => {
        await mutate!({
          variables: { orgId: ownProps.match.params.orgId },
          refetchQueries: orgNavRefetchQueries(ownProps.match.params.orgId),
        });
      },
    }),
    skip: (componentProps) => !componentProps.match.params.orgId,
  },
);

export const TryIdentityManagerButton = withRouter<OwnProps>(
  withMutation(
    withFlag(
      injectIntl(
        withAnalyticsClient(
          TryIdentityManagerButtonImpl,
        ),
      ),
    ),
  ),
);
