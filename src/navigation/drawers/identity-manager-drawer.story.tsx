import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';
import { MemoryRouter } from 'react-router';

import AkPage from '@atlaskit/page';

import { configureApolloClientWithUserFeatureFlag } from 'common/feature-flags/test-utils';

import { createApolloClient } from '../../apollo-client';
import { IdentityManagerDrawer } from './identity-manager-drawer';

const CreateAtlassianAccessPageWithProviders = ({ isAtlassianAccessEnabled }) => (
  <MemoryRouter>
    <ApolloProvider
      client={
        configureApolloClientWithUserFeatureFlag({
          client: createApolloClient(),
          flagKey: 'atlassianaccess.enabled',
          flagValue: isAtlassianAccessEnabled,
        })}
    >
      <IntlProvider locale="en">
        <AkPage>
          <IdentityManagerDrawer onCloseDrawer={action('Close Drawer')} />
        </AkPage>
      </IntlProvider>
    </ApolloProvider>
  </MemoryRouter>
);

storiesOf('Organization|Identity Manger Drawer', module)
  .add('With Atlassian Access FF enabled', () => (
    <CreateAtlassianAccessPageWithProviders isAtlassianAccessEnabled={true} />
  ))
  .add('With Atlassian Access FF disabled', () => (
    <CreateAtlassianAccessPageWithProviders isAtlassianAccessEnabled={false} />
  ));
