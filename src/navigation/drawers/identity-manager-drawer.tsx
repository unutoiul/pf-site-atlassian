import * as React from 'react';

import { AtlassianAccessValuePropBody } from 'common/atlassian-access-value-prop/atlassian-access-value-prop-body';
import { AtlassianAccessValuePropHeader } from 'common/atlassian-access-value-prop/atlassian-access-value-prop-header';
import {
  ValuePropFooter,
  ValuePropPage,
} from 'common/value-prop';

import { accessTrialModalScreenEvent, findOutMoreLinkClickedEvent, learnPricingLinkClickedEvent, ScreenEventSender } from 'common/analytics';

import { TryIdentityManagerButton } from './try-identity-manager-button';

const analyticsActionSubject = 'identityManagerEvaluationPage';

interface OwnProps {
  onCloseDrawer(): void;
}

export class IdentityManagerDrawer extends React.Component<OwnProps> {
  public render() {
    return (
      <ScreenEventSender event={accessTrialModalScreenEvent()}>
        <ValuePropPage
          renderHeader={this.renderHeader}
          renderBody={this.renderBody}
          renderFooter={this.renderFooter}
          isNavCollapsed={false}
        />
      </ScreenEventSender>
    );
  }

  private renderHeader = () => {
    return (
      <AtlassianAccessValuePropHeader
        analyticsActionSubject={analyticsActionSubject}
        learnMoreAnalyticsEvent={findOutMoreLinkClickedEvent()}
      />
    );
  };

  private renderBody = () => {
    return (
      <AtlassianAccessValuePropBody
        analyticsActionSubject={analyticsActionSubject}
        pricingAnalyticsEvent={learnPricingLinkClickedEvent()}
      />
    );
  };

  private renderFooter = () => {
    return (
      <ValuePropFooter>
        <TryIdentityManagerButton done={this.props.onCloseDrawer} />
      </ValuePropFooter>
    );
  };
}
