import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import { NavigationCreateDrawer } from '../../site/navigation/navigation-create-drawer';
import { NavigationLink } from '../navigation-link';
interface OwnProps {
  isOpen: boolean;
  cloudId: string;
  onCloseDrawer(): void;
  onShowInviteDrawer(): void;
}
type AllProps = OwnProps & InjectedIntlProps;

const messages = defineMessages({
  inviteUser: {
    id: 'nested.navigation.site.create.drawer.invite.user',
    defaultMessage: 'Invite a user',
    description: 'The title of a link that allows a user to invite a user to their site',
  },
  createGroup: {
    id: 'nested.navigation.site.create.drawer.create.group',
    defaultMessage: 'Create a group',
    description: 'The title of a link that allows a user to create a new group',
  },
});

export class SiteNavigationCreateDrawerImpl extends React.Component<AllProps> {
  public render() {
    return (
      <NavigationCreateDrawer
        key="SITE_CREATE"
        isOpen={this.props.isOpen}
        links={[]}
        onCloseDrawer={this.props.onCloseDrawer}
      >
        <NavigationLink title={this.props.intl.formatMessage(messages.inviteUser)} onClick={this.props.onShowInviteDrawer} />
        <NavigationLink title={this.props.intl.formatMessage(messages.createGroup)} onClick={this.props.onCloseDrawer} path={`/s/${this.props.cloudId}/groups`} />
      </NavigationCreateDrawer>
    );
  }
}

export const SiteNavigationCreateDrawer = injectIntl(SiteNavigationCreateDrawerImpl);
