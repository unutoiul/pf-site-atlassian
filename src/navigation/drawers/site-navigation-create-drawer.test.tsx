import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { sandbox as sinonSandbox } from 'sinon';

import { createMockIntlProp } from '../../utilities/testing';
import { NavigationLink } from '../navigation-link';
import { SiteNavigationCreateDrawerImpl } from './site-navigation-create-drawer';

describe('SiteNavigationCreateDrawer', () => {
  const sandbox = sinonSandbox.create();
  const onCloseDrawerSpy = sandbox.spy();
  const onShowInviteDrawer = sandbox.spy();

  afterEach(() => {
    sandbox.restore();
  });

  it('clicking on invite users closes the drawer and opens the invite drawer', () => {
    const wrapper = shallow(
      <SiteNavigationCreateDrawerImpl
        intl={createMockIntlProp()}
        isOpen={true}
        cloudId={'test-cloud-id'}
        onCloseDrawer={onCloseDrawerSpy}
        onShowInviteDrawer={onShowInviteDrawer}
      />,
    );

    wrapper.find(NavigationLink).at(0).props().onClick!(null as any);

    expect(onShowInviteDrawer.callCount).to.equal(1);
  });

  it('clicking on create group closes the drawer', () => {
    const wrapper = shallow(
      <SiteNavigationCreateDrawerImpl
        intl={createMockIntlProp()}
        isOpen={true}
        cloudId={'test-cloud-id'}
        onCloseDrawer={onCloseDrawerSpy}
        onShowInviteDrawer={onShowInviteDrawer}
      />,
    );

    wrapper.find(NavigationLink).at(1).props().onClick!(null as any);

    expect(onCloseDrawerSpy.callCount).to.equal(1);

  });
});
