import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';
import { MemoryRouter } from 'react-router';

import AkPage from '@atlaskit/page';

import { createApolloClient } from '../../apollo-client';
import { SiteNavigationCreateDrawer } from './site-navigation-create-drawer';

storiesOf('Site|Site navigation create drawer', module)
  .add('in the open state', () => (
    <MemoryRouter>
      <ApolloProvider
        client={createApolloClient()}
      >
        <IntlProvider locale="en">
          <AkPage>
            <SiteNavigationCreateDrawer
              isOpen={true}
              onShowInviteDrawer={action('Show invite drawer')}
              onCloseDrawer={action('Close Drawer')}
              cloudId="test-cloud-id"
            />
          </AkPage>
        </IntlProvider>
      </ApolloProvider>
    </MemoryRouter>
  ));
