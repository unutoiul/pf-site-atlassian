import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { sandbox as sinonSandbox } from 'sinon';

import AkTextField from '@atlaskit/field-text';
import AkSpinner from '@atlaskit/spinner';

import { Button as AnalyticsButton } from 'common/analytics';
import TextArea from 'common/field-textarea';

import { updateMessages } from '../../organization/saml/saml-messages';
import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import { SamlConfigDrawerImpl } from './saml-config-drawer';

const sampleSamlConfig = {
  issuer: 'https://example.com/issuer',
  ssoUrl: 'https://example.com/ssoUrl',
  publicCertificate: 'public certificate goes here',
  auth0MigrationState: 'LEGACY',
};

describe('SamlCreateDrawer V2', () => {
  const sandbox = sinonSandbox.create();
  let onCloseDrawerSpy;
  let dispatchSpy;

  beforeEach(() => {
    onCloseDrawerSpy = sandbox.spy();
    dispatchSpy = sandbox.spy();
  });

  afterEach(() => {
    sandbox.restore();
  });

  function shallowSamlCreateDrawerWithData(data = {}, mutate = null) {
    const match = {
      params: {
        orgId: 'FAKE',
      },
    };

    return shallow(
      <SamlConfigDrawerImpl
        data={data}
        dispatch={dispatchSpy}
        mutate={mutate}
        onCloseDrawer={onCloseDrawerSpy}
        intl={createMockIntlProp()}
        match={match}
        {...{} as any}
      />,
      createMockIntlContext(),
    );
  }

  it.skip('sets issuer, ssoUrl, publicCertificate, auth0MigrationState and action in state when provided in props', () => {
    const setInitiallyWrapper = shallowSamlCreateDrawerWithData({ organization: { security : { saml: sampleSamlConfig } } });
    expect(setInitiallyWrapper.state()).to.deep.include(sampleSamlConfig);

    const unsetInitiallyWrapper = shallowSamlCreateDrawerWithData();
    expect(unsetInitiallyWrapper.state()).to.be.deep.include({
      issuer: '',
      ssoUrl: '',
      publicCertificate: '',
      auth0MigrationState: 'LEGACY',
      action: 'ADD',
    });
    unsetInitiallyWrapper.setProps({ data: { organization: { security : { saml: sampleSamlConfig } } } });
    expect(unsetInitiallyWrapper.state()).to.deep.include(sampleSamlConfig);
  });

  it.skip('maintains auth0MigrationState and sets correct action of the saml provided in props', () => {
    const samlConfig = {
      issuer: 'https://example.com/issuer',
      ssoUrl: 'https://example.com/ssoUrl',
      publicCertificate: 'public certificate goes here',
      auth0MigrationState: 'AUTH0',
    };
    const state = {
      ...samlConfig,
      isLoading: false,
      action: 'EDIT',
    };
    const setInitiallyWrapper = shallowSamlCreateDrawerWithData({
      organization: {
        security : {
          saml: {
            issuer: 'https://example.com/issuer',
            ssoUrl: 'https://example.com/ssoUrl',
            publicCertificate: 'public certificate goes here',
            auth0MigrationState: 'AUTH0',
          },
        },
      },
    });
    expect(setInitiallyWrapper.state()).to.deep.equal(state);

    const unsetInitiallyWrapper = shallowSamlCreateDrawerWithData();
    unsetInitiallyWrapper.setProps({ data: { organization: { security : { saml: state } } } });
    expect(unsetInitiallyWrapper.state()).to.deep.equal(state);
  });

  describe('before SAML configuration has been fetched', () => {
    it('renders without a heading and form', () => {
      const wrapper = shallowSamlCreateDrawerWithData({ loading: true });
      expect(wrapper.find('h3')).to.have.length(0);
      expect(wrapper.find('form')).to.have.length(0);
    });

    it('renders a spinner', () => {
      const wrapper = shallowSamlCreateDrawerWithData({ loading: true });
      expect(wrapper.find(AkSpinner)).to.have.length(1);
    });
  });

  describe('after SAML configuration has been fetched', () => {
    it('renders with a heading and form', () => {
      const wrapper = shallowSamlCreateDrawerWithData({ organization: { security: {} } });
      expect(wrapper.find('h3')).to.have.length(1);
      expect(wrapper.find('form')).to.have.length(1);
    });

    it('renders without a spinner', () => {
      const wrapper = shallowSamlCreateDrawerWithData({ organization: { security: {} } });
      expect(wrapper.find(AkSpinner)).to.have.length(0);
    });

    it('renders the correct title', () => {
      const withConfigWrapper = shallowSamlCreateDrawerWithData({ organization: { security : { saml: sampleSamlConfig } } });
      const withoutConfigWrapper = shallowSamlCreateDrawerWithData({ organization: { security: {} } });

      expect(withConfigWrapper.find('h3').text()).to.equal(updateMessages.updateHeading.defaultMessage);
      expect(withoutConfigWrapper.find('h3').text()).to.equal(updateMessages.addHeading.defaultMessage);
    });

    it('renders buttons to save and cancel', () => {
      const wrapper = shallowSamlCreateDrawerWithData({ organization: { security : { saml: sampleSamlConfig } } });
      const cancel = wrapper.find(AnalyticsButton).last();
      expect(cancel).to.have.length(1);

      const submit = wrapper.find(AnalyticsButton).first();
      expect(submit).to.have.length(1);

      expect(submit).to.have.length(1);
      expect(submit.props().isDisabled).to.equal(false);

      expect(cancel).to.have.length(1);
      cancel.simulate('click');
      expect(onCloseDrawerSpy.getCalls()).to.have.length(1);
    });

    it('save and cancel button sets correct analytics data for adding saml', () => {
      const wrapper = shallowSamlCreateDrawerWithData({ organization: { security: {} } });
      const cancel = wrapper.find(AnalyticsButton).last();
      expect(cancel).to.have.length(1);

      const submit = wrapper.find(AnalyticsButton).first();
      expect(submit).to.have.length(1);

      expect(submit.props().analyticsData).to.deep.equal({
        action: 'click',
        actionSubject: 'addSamlButton',
        actionSubjectId: 'save',
        subproduct: 'organization',
        tenantId: 'FAKE',
        tenantType: 'organizationId',
      });
      expect(cancel.props().analyticsData).to.deep.equal({
        action: 'click',
        actionSubject: 'addSamlButton',
        actionSubjectId: 'cancel',
        subproduct: 'organization',
        tenantId: 'FAKE',
        tenantType: 'organizationId',
      });
    });

    it('save and cancel button sets correct analytics data for editing saml', () => {
      const wrapper = shallowSamlCreateDrawerWithData({ organization: { security : { saml: sampleSamlConfig } } });
      const cancel = wrapper.find(AnalyticsButton).last();
      expect(cancel).to.have.length(1);

      const submit = wrapper.find(AnalyticsButton).first();
      expect(submit).to.have.length(1);

      expect(submit.props().analyticsData).to.deep.equal({
        action: 'click',
        actionSubject: 'editSamlButton',
        actionSubjectId: 'save',
        subproduct: 'organization',
        tenantId: 'FAKE',
        tenantType: 'organizationId',
      });
      expect(cancel.props().analyticsData).to.deep.equal({
        action: 'click',
        actionSubject: 'editSamlButton',
        actionSubjectId: 'cancel',
        subproduct: 'organization',
        tenantId: 'FAKE',
        tenantType: 'organizationId',
      });
    });

    it('save button is enabled and disabled correctly when input is changed', () => {
      const wrapper = shallowSamlCreateDrawerWithData({ organization: { security: {} } });
      const submit = wrapper.find(AnalyticsButton).first();

      expect(submit.props().isDisabled).to.equal(true);

      const inputs = wrapper.find(AkTextField);
      const moreInput = wrapper.find(TextArea);
      inputs.forEach(input => input.simulate('change', { target: { value: 'text' } }));
      moreInput.simulate('change', { target: { value: 'text' } });

      const submitAfterInput = wrapper.find(AnalyticsButton).first();
      expect(wrapper.state()).to.deep.include({
        issuer: 'text',
        ssoUrl: 'text',
        publicCertificate: 'text',
        auth0MigrationState: 'AUTH0FORCED',
      });
      expect(submitAfterInput.props().isDisabled).to.equal(false);

      inputs.at(0).simulate('change', { target: { value: '' } });
      const submitAfterClear = wrapper.find(AnalyticsButton).first();
      expect(submitAfterClear.props().isDisabled).to.equal(true);
    });

    it('closes the drawer on successful save', async () => {
      const promise = Promise.resolve(true);
      const mutateStub = sandbox.stub().returns(promise);
      const preventDefaultSpy = sandbox.spy();
      const wrapper = shallowSamlCreateDrawerWithData({ organization: { security: {} } }, mutateStub as any);

      wrapper.find('form').simulate('submit', { preventDefault: preventDefaultSpy });
      expect(preventDefaultSpy.getCalls()).to.have.length(1);

      await promise;

      expect(mutateStub.getCalls()).to.have.length(1);
      expect(onCloseDrawerSpy.getCalls()).to.have.length(1);
    });
  });
});
