import * as React from 'react';
import styled from 'styled-components';

import { Skeleton as AkSkeleton } from '@atlaskit/avatar';
import { AtlassianIcon as AkAtlassianIcon } from '@atlaskit/logo';
import {
  AkGlobalItem,
  AkGlobalNavigation,
  presetThemes as akPresetThemes,
} from '@atlaskit/navigation';

import { GlobalNavigationSpacer, GlobalNavigationWrapper } from './navigation';

const AdjustedIcon = styled(AkAtlassianIcon)`
  margin-top: -2px;
`;

export class SkeletonNavigation extends React.Component {
  public render() {
    const secondaryActions = [
      (
        <AkGlobalItem>
          <AkSkeleton key="avatar" size="small" />
        </AkGlobalItem>
      ),
    ];

    return (
      <GlobalNavigationSpacer>
        <GlobalNavigationWrapper>
          <AkGlobalNavigation
            theme={akPresetThemes.settings}
            primaryIcon={<AdjustedIcon size="large" />}
            secondaryActions={secondaryActions}
          />
        </GlobalNavigationWrapper>
      </GlobalNavigationSpacer>
    );
  }
}
