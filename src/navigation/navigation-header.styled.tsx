import styled from 'styled-components';

import { gridSize as akGridSize } from '@atlaskit/theme';

export const NavigationHeader = styled.span`
  font-size: ${akGridSize() * 2.5}px;
  line-height: ${akGridSize() * 7.5}px;
  width: 100%;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
`;
