import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { sandbox as sinonSandbox } from 'sinon';

import AkNavigation from '@atlaskit/navigation';

import { noop } from '../utilities/testing';
import { NavigationImpl } from './navigation';

describe('Navigation', () => {
  const sandbox = sinonSandbox.create();

  const showDrawerSpy = sandbox.spy();

  afterEach(() => {
    sandbox.restore();
  });

  it('shows the global create icon when the context is site', () => {
    const wrapper = shallow(
      <NavigationImpl
        showContainer={true}
        showDrawer={noop}
        navType="site"
      />,
    );

    expect((wrapper.find(AkNavigation).props() as any).globalCreateIcon).to.not.equal(undefined);
  });

  it('does not show the global create icon when the context is org', () => {
    const wrapper = shallow(
      <NavigationImpl
        showContainer={true}
        showDrawer={noop}
        navType="org"
      />,
    );

    expect((wrapper.find(AkNavigation).props() as any).globalCreateIcon).to.equal(null);
  });

  it('opens the site create drawer when the global create icon is clicked', () => {
    const wrapper = shallow(
      <NavigationImpl
        showContainer={true}
        showDrawer={showDrawerSpy}
        navType="site"
      />,
    );

    (wrapper.find(AkNavigation).props() as any).onCreateDrawerOpen();

    expect(showDrawerSpy.calledWith('SITE_CREATE')).to.equal(true);
  });
});
