import * as React from 'react';
import { defineMessages } from 'react-intl';

import { withClickAnalytics } from 'common/analytics';

import { BackNavigationLink } from '../back-navigation-link';
import { NavigationLink } from '../navigation-link';

const messages = defineMessages({
  overview: {
    id: 'admin.site.nav.billing.overview',
    defaultMessage: 'Overview',
  },
  estimate: {
    id: 'admin.site.nav.billing.estimate',
    defaultMessage: 'Bill estimate',
  },
  paymentDetails: {
    id: 'admin.site.nav.billing.details',
    defaultMessage: 'Billing details',
  },
  history: {
    id: 'admin.site.nav.billing.history',
    defaultMessage: 'Billing history',
  },
  applications: {
    id: 'admin.site.nav.billing.applications',
    defaultMessage: 'Manage subscriptions',
  },
});

const overviewLink = (cloudId) => `/s/${cloudId}/billing/overview`;
const estimateLink = (cloudId) => `/s/${cloudId}/billing/estimate`;
const paymentDetailsLink = (cloudId) => `/s/${cloudId}/billing/paymentdetails`;
const applicationsLink = (cloudId) => `/s/${cloudId}/billing/applications`;
const historyLink = (cloudId) => `/s/${cloudId}/billing/history`;

export const NavigationLinkWithAnalytics = withClickAnalytics<any>()(NavigationLink);

// For any nested navigation, all the information required to for a list of links available is passed down.
// Example, if the passwordpolicy link is dynamic, ideally we would resolve it before rendering any of the
// navigation links to avoid the nav jumping around while changing loading states.
export const billingLinks = (cloudId: string, formatMessage, onBackLinkClick: () => void): React.ReactNode => (
  <React.Fragment>
    <BackNavigationLink onClick={onBackLinkClick} />
    <NavigationLinkWithAnalytics actionSubjectId="billingOverviewLink" path={overviewLink(cloudId)} title={formatMessage(messages.overview)} />
    <NavigationLinkWithAnalytics actionSubjectId="billingDetailsLink" path={paymentDetailsLink(cloudId)} title={formatMessage(messages.paymentDetails)} />
    <NavigationLinkWithAnalytics actionSubjectId="billingEstimateLink" path={estimateLink(cloudId)} title={formatMessage(messages.estimate)} />
    <NavigationLinkWithAnalytics actionSubjectId="billingHistoryLink" path={historyLink(cloudId)} title={formatMessage(messages.history)} />
    <NavigationLinkWithAnalytics actionSubjectId="manageSubscriptionsLink" path={applicationsLink(cloudId)} title={formatMessage(messages.applications)} />
  </React.Fragment>
);

const billingNavLinkPaths = (cloudId): string[] => {
  return [
    overviewLink(cloudId),
    estimateLink(cloudId),
    historyLink(cloudId),
    applicationsLink(cloudId),
    paymentDetailsLink(cloudId),
  ];
};

export const isBillingLinkActive = (cloudId: string, pathname: string): boolean => {
  return billingNavLinkPaths(cloudId).includes(pathname);
};
