import { expect } from 'chai';

import { isBillingLinkActive } from './billing-links';

const cloudId = 'cloud-id';

describe('Site billing links', () => {
  it('should return true for billing nested pages', () => {
    expect(isBillingLinkActive(cloudId, `/s/${cloudId}/billing/overview`)).to.equal(true);
  });

  it('should return false for non billing nested pages', () => {
    expect(isBillingLinkActive(cloudId, `/s/${cloudId}/non-billing-link`)).to.equal(false);
  });
});
