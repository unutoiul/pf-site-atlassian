import * as React from 'react';
import { graphql, OptionProps } from 'react-apollo';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';

import AkArrowRightIcon from '@atlaskit/icon/glyph/arrow-right';
import {
  AkContainerNavigationNested,
  AkNavigationItemGroup,
} from '@atlaskit/navigation';

import { withClickAnalytics } from 'common/analytics';
import { ErrorBoundary } from 'common/error-boundary';
import { FeedbackNavigationGroup } from 'common/feedback';

import { NestedNavigationQuery, NestedNavigationQueryVariables } from '../../schema/schema-types';
import { SiteAdminMigrationDialog } from '../../site/site-admin-migration';
import { NavigationLink } from '../navigation-link';
import { billingLinks, isBillingLinkActive } from './billing-links';
import nestedNavigationQuery from './nested-navigation.query.graphql';
import { SecurityNavigationLink } from './security-link';

const messages = defineMessages({
  billingSection: {
    defaultMessage: 'Subscriptions & Billing',
    description: '',
    id: 'nested.navigation.site.billing.title',
  },
  billing: {
    defaultMessage: 'Billing',
    description: '',
    id: 'nested.navigation.site.nav-sub-section.billing.title',
  },
  accessRequests: {
    id: 'nested.navigation.site.access-requests.title',
    description: 'The tile of the page',
    defaultMessage: 'Access Requests',
  },
  securitySection: {
    id: 'nested.navigation.site.organizations.title',
    defaultMessage: 'Organizations & security',
  },
  settingsSection: {
    defaultMessage: 'Site settings',
    description: 'The link in the navigation for the site settings page',
    id: 'nested.navigation.site.site-settings.title1',
  },
  groups: {
    defaultMessage: 'Groups',
    description: '',
    id: 'nested.navigation.site.um.groups',
  },
  users: {
    defaultMessage: 'Users',
    description: '',
    id: 'nested.navigation.site.um.users',
  },
  umSection: {
    defaultMessage: 'User management',
    description: '',
    id: 'nested.navigation.site.um.title',
  },
  serviceDesk: {
    defaultMessage: 'Jira Service Desk',
    description: '',
    id: 'nested.navigation.site.um.service-desk',
  },
  siteAccess: {
    id: 'nested.navigation.site.site-settings.site-access.title',
    defaultMessage: 'Site access',
  },
  discoverApplications: {
    defaultMessage: 'Discover applications',
    description: '',
    id: 'nested.navigation.site.billing.discover-applications',
  },
  security: {
    id: 'nested.navigation.site.security',
    defaultMessage: 'Security',
  },
  productAccess: {
    id: 'nested.navigation.site.userProvisioning.productAccess',
    defaultMessage: 'Product access',
    description: 'Title for a tab showing access to different products',
  },
  userConnectedApps: {
    id: 'nested.navigation.site.userProvisioning.userConnectedApps',
    defaultMessage: 'Connected apps',
    description: 'Title for a tab showing user connected apps',
  },
  gsuite: {
    id: 'nested.navigation.site.site-settings.g-suite.title1',
    defaultMessage: 'G Suite',
    description: 'Title of the page showing G Suite information.',
  },
  emoji: {
    defaultMessage: 'Emoji',
    id: 'nested.navigation.site.site-settings.emoji',
    description: 'Navigation link for the emoji settings page',
  },
  contact: {
    defaultMessage: 'Contact',
    id: 'nested.navigation.site.site-settings.contact',
    description: 'Navigation link for the contact settings page',
  },
});

type AllProps = InjectedIntlProps & RouteComponentProps<{ cloudId: string }> & DerivedProps;

const enum ActiveNav {
  TopLevel,
  Billing,
}

export const NavigationLinkWithAnalytics = withClickAnalytics<any>()(NavigationLink);

interface NavigationState {
  activeNavigation: ActiveNav;
}

export class NestedNavigationImpl extends React.Component<AllProps, NavigationState> {
  constructor(props) {
    super(props);

    const {
      match: { params: { cloudId } },
      location: { pathname },
    } = props;

    if (isBillingLinkActive(cloudId, pathname)) {
      this.state = {
        activeNavigation: ActiveNav.Billing,
      };
    } else {
      this.state = {
        activeNavigation: ActiveNav.TopLevel,
      };
    }
  }

  public render() {
    if (this.props.loading) {
      return null;
    }

    return (
      <ErrorBoundary>
        <AkContainerNavigationNested stack={this.getNavItems()} />
      </ErrorBoundary>
    );
  }

  private getNavItems = () => {
    const getNavItems = {
      [ActiveNav.TopLevel]: this.getTopLevelLinks,
      [ActiveNav.Billing]: this.getBillingLinks,
    };

    return getNavItems[this.state.activeNavigation]();
  }

  private getTopLevelLinks = () => {
    const {
      match: { params: { cloudId } },
      intl: { formatMessage },
      showJsdLink: hasJsd,
    } = this.props;

    return [
      (
        <React.Fragment>
          <SiteAdminMigrationDialog />
          <AkNavigationItemGroup title={formatMessage(messages.umSection)}>
            <NavigationLinkWithAnalytics path={`/s/${cloudId}/users`} title={formatMessage(messages.users)} />
            <NavigationLinkWithAnalytics path={`/s/${cloudId}/groups`} title={formatMessage(messages.groups)} />
            <NavigationLinkWithAnalytics path={`/s/${cloudId}/access-requests`} title={formatMessage(messages.accessRequests)} />
            {hasJsd && <NavigationLinkWithAnalytics path={`/s/${cloudId}/jira-service-desk/portal-only-customers`} title={formatMessage(messages.serviceDesk)} />}
          </AkNavigationItemGroup>
          <AkNavigationItemGroup title={formatMessage(messages.settingsSection)}>
            <NavigationLinkWithAnalytics path={`/s/${cloudId}/signup`} title={formatMessage(messages.siteAccess)} />
            <NavigationLinkWithAnalytics path={`/s/${cloudId}/apps`} title={formatMessage(messages.productAccess)} />
            <NavigationLinkWithAnalytics path={`/s/${cloudId}/gsuite`} title={formatMessage(messages.gsuite)} />
            <NavigationLinkWithAnalytics path={`/s/${cloudId}/emoji`} title={formatMessage(messages.emoji)} />
          </AkNavigationItemGroup>
          <AkNavigationItemGroup title={formatMessage(messages.securitySection)}>
            <SecurityNavigationLink />
            <NavigationLinkWithAnalytics path={`/s/${cloudId}/user-connected-apps`} title={formatMessage(messages.userConnectedApps)} />
          </AkNavigationItemGroup>
          <AkNavigationItemGroup title={formatMessage(messages.billingSection)}>
            <NavigationLinkWithAnalytics
              actionSubjectId="pricingLink"
              iconRight={<AkArrowRightIcon label="" />}
              onClick={this.setActiveNavigation(ActiveNav.Billing)}
              title={formatMessage(messages.billing)}
            />
            <NavigationLinkWithAnalytics actionSubjectId="discoverApplicationsLink" path={`/s/${cloudId}/billing/addapplication`} title={formatMessage(messages.discoverApplications)} />
          </AkNavigationItemGroup>
          <FeedbackNavigationGroup />
        </React.Fragment>
      ),
    ];
  };

  private getBillingLinks = () => {
    const { match: { params: { cloudId } }, intl: { formatMessage } } = this.props;

    return [
      [],
      billingLinks(cloudId, formatMessage, this.setActiveNavigation(ActiveNav.TopLevel)),
    ];
  };

  private setActiveNavigation = (activeNavigation: ActiveNav) => () => {
    this.setState({ activeNavigation });
  };
}

interface DerivedProps {
  loading: boolean;
  showJsdLink?: boolean;
}

const getDerivedProps = (componentProps: OptionProps<RouteComponentProps<{ cloudId }>, NestedNavigationQuery>): DerivedProps => {
  if (!componentProps || !componentProps.data || componentProps.data.loading) {
    return {
      loading: true,
    };
  }

  return {
    loading: false,
    showJsdLink: !!componentProps.data.site && !!componentProps.data.site.groupsUseAccessConfig.find((item) => item.product.productId === 'jira-servicedesk'),
  };
};

const withNestedNavigationQuery = graphql<RouteComponentProps<{ cloudId: string }>, NestedNavigationQuery, NestedNavigationQueryVariables, DerivedProps>(
  nestedNavigationQuery,
  {
    options: (props) => ({ variables: { cloudId: props.match.params.cloudId } }),
    props: getDerivedProps,
    skip: (componentProps: RouteComponentProps<{ cloudId: string }>) => !componentProps.match.params.cloudId,
  },
);

export const NestedNavigation =
  withRouter<{}>(
    withNestedNavigationQuery(
      injectIntl(
        NestedNavigationImpl,
      ),
    ),
  );
