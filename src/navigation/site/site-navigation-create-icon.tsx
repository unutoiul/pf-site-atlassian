import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import AkCreateIcon from '@atlaskit/icon/glyph/add';

import { NavigationTooltip } from 'common/navigation-tooltip';

const messages = defineMessages({
  globalButtonTooltip: {
    id: 'nested.navigation.site.global-create-icon.tooltip',
    defaultMessage: 'Create users and groups',
    description: 'The tooltip that shows when a user hovers the mouse of a button that lets them create groups and invite users',
  },
});

export class SiteNavigationCreateIconImpl extends React.Component<InjectedIntlProps> {
  public render() {
    const { formatMessage } = this.props.intl;
    const message = formatMessage(messages.globalButtonTooltip);

    return (
      <NavigationTooltip message={message}>
        <AkCreateIcon label={message} />
      </NavigationTooltip>
    );
  }

}

export const SiteNavigationCreateIcon = injectIntl<{}>(SiteNavigationCreateIconImpl);
