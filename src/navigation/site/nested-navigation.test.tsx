import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { match } from 'react-router';

import { AkContainerNavigationNested } from '@atlaskit/navigation';

import { createMockIntlContext, createMockIntlProp } from '../../utilities/testing';
import { NavigationLinkWithAnalytics as NavigationBillingLinkWithAnalytics } from './billing-links';
import { NavigationLinkWithAnalytics, NestedNavigationImpl } from './nested-navigation';

const cloudId = 'my-site';

const getMatchProp = (page: string): match<{cloudId: string}> => ({
  params: {
    cloudId,
  },
  isExact: true,
  path: `/s/${cloudId}/${page}`,
  url: `/s/${cloudId}/${page}`,
});

const getLocationProp = (page: string) => ({
  pathname: `/s/${cloudId}/${page}`,
  search : '',
  state : '',
  hash : '',
});

describe('Site nested org navigation', () => {
  it('should render top level links', () => {
    const wrapper = shallow((
      <NestedNavigationImpl
        intl={createMockIntlProp()}
        match={getMatchProp('users')}
        location={getLocationProp('users')}
        history={null as any}
        loading={false}
      />
    ), createMockIntlContext());

    const topLevelLinks = wrapper.find(AkContainerNavigationNested).prop('stack')[0];
    // billingLinks must be wrapped in a div so enzyme is able to shallow the fragment
    const linksWrapper = shallow(<div>{topLevelLinks}</div>);

    expect(linksWrapper.find(NavigationLinkWithAnalytics).length).to.be.greaterThan(0);
  });

  it('should render billing links', () => {
    const wrapper = shallow((
      <NestedNavigationImpl
        intl={createMockIntlProp()}
        match={getMatchProp('billing/overview')}
        location={getLocationProp('billing/overview')}
        history={null as any}
        loading={false}
      />
    ), createMockIntlContext());

    const billingLinks = wrapper.find(AkContainerNavigationNested).prop('stack')[1];
    // billingLinks must be wrapped in a div so enzyme is able to shallow the fragment
    const linksWrapper = shallow(<div>{billingLinks}</div>);

    expect(linksWrapper.find(NavigationBillingLinkWithAnalytics).length).to.be.greaterThan(0);
  });

  it('should render JSD link if enabled', () => {
    const wrapper = shallow((
      <NestedNavigationImpl
        intl={createMockIntlProp()}
        match={getMatchProp('users')}
        location={getLocationProp('users')}
        history={null as any}
        loading={false}
        showJsdLink={true}
      />
    ), createMockIntlContext());

    const topLevelLinks = wrapper.find(AkContainerNavigationNested).prop('stack')[0];
    // billingLinks must be wrapped in a div so enzyme is able to shallow the fragment
    const linksWrapper = shallow(<div>{topLevelLinks}</div>);

    expect(linksWrapper.find({ title: 'Jira Service Desk' }).length).to.equal(1);
  });

  it('should not render JSD link if it is not enabled', () => {
    const wrapper = shallow((
      <NestedNavigationImpl
        intl={createMockIntlProp()}
        match={getMatchProp('users')}
        location={getLocationProp('users')}
        history={null as any}
        loading={false}
        showJsdLink={false}
      />
    ), createMockIntlContext());

    const topLevelLinks = wrapper.find(AkContainerNavigationNested).prop('stack')[0];
    // billingLinks must be wrapped in a div so enzyme is able to shallow the fragment
    const linksWrapper = shallow(<div>{topLevelLinks}</div>);

    expect(linksWrapper.find({ title: 'Jira Service Desk' }).length).to.equal(0);
  });
});
