import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import { IntlProvider } from 'react-intl';
import { MemoryRouter } from 'react-router';
import { ThemeProvider } from 'styled-components';

import { ApolloProvider } from '../../../node_modules/react-apollo';
import { createMockApolloClient, createMockIntlContext, wait } from '../../utilities/testing';
import { NavigationLinkWithAnalytics, SecurityNavigationLink } from './security-link';

describe('Security link', () => {
  it('should link to /atlassian-access when the user has no orgs', async () => {
    const mockApolloClient = createMockApolloClient({
      Query: () => ({
        currentUser: () => ({
          id: 'user-id',
          organizations: [],
        }),
      }),
    });

    const wrapper = mount(
      <ThemeProvider theme={{}}>
        <MemoryRouter>
          <ApolloProvider client={mockApolloClient}>
            <IntlProvider locale="en">
              <SecurityNavigationLink />
            </IntlProvider>
          </ApolloProvider>
        </MemoryRouter>
      </ThemeProvider>,
      createMockIntlContext(),
    );

    await wait(1);
    wrapper.update();

    expect(wrapper.find(NavigationLinkWithAnalytics).props().path).to.equal('/atlassian-access');
  });

  it('should link to the org overview page when the user has one org', async () => {
    const mockApolloClient = createMockApolloClient({
      Query: () => ({
        currentUser: () => ({
          id: 'user-id',
          organizations: [{
            id: 'test-id',
          }],
        }),
      }),
    });

    const wrapper = mount(
      <ThemeProvider theme={{}}>
        <MemoryRouter>
          <ApolloProvider client={mockApolloClient}>
            <IntlProvider locale="en">
              <SecurityNavigationLink />
            </IntlProvider>
          </ApolloProvider>
        </MemoryRouter>
      </ThemeProvider>,
      createMockIntlContext(),
    );

    await wait(1);
    wrapper.update();

    expect(wrapper.find(NavigationLinkWithAnalytics).props().path).to.equal('/o/test-id/overview');
  });

  it('should link to / when the user has more than one org', async () => {
    const mockApolloClient = createMockApolloClient({
      Query: () => ({
        currentUser: () => ({
          id: 'user-id',
          organizations: [
            { id: 'test-id' },
            { id: 'test-id2' },
          ],
        }),
      }),
    });

    const wrapper = mount(
      <ThemeProvider theme={{}}>
        <MemoryRouter>
          <ApolloProvider client={mockApolloClient}>
            <IntlProvider locale="en">
              <SecurityNavigationLink />
            </IntlProvider>
          </ApolloProvider>
        </MemoryRouter>
      </ThemeProvider>,
      createMockIntlContext(),
    );

    await wait(1);
    wrapper.update();

    expect(wrapper.find(NavigationLinkWithAnalytics).props().path).to.equal('/');
  });

  it('should link to / in the error state', async () => {
    const mockApolloClient = createMockApolloClient({
      Query: () => ({
        currentUser: () => {
          throw new Error('test problem');
        },
      }),
    });

    const wrapper = mount(
      <ThemeProvider theme={{}}>
        <MemoryRouter>
          <ApolloProvider client={mockApolloClient}>
            <IntlProvider locale="en">
              <SecurityNavigationLink />
            </IntlProvider>
          </ApolloProvider>
        </MemoryRouter>
      </ThemeProvider>,
      createMockIntlContext(),
    );

    await wait(1);
    wrapper.update();

    expect(wrapper.find(NavigationLinkWithAnalytics).props().path).to.equal('/');
  });
});
