import { ApolloError } from 'apollo-client';
import * as React from 'react';
import { Query } from 'react-apollo';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import { withClickAnalytics } from 'common/analytics';

import { SecurityLinkQuery } from '../../schema/schema-types';
import { NavigationLink } from '../navigation-link';
import securityLinkQuery from './security-link.query.graphql';

export const NavigationLinkWithAnalytics = withClickAnalytics<any>()(NavigationLink);

const messages = defineMessages({
  security: {
    id: 'nested.navigation.site.site-settings.security',
    defaultMessage: 'Security',
    description: 'A navigation link that takes you to the place where you can manage your orgs, or sign up for one',
  },
});

type AllProps = InjectedIntlProps;

class SecurityLinkQueryComponent extends Query<SecurityLinkQuery> { }

// tslint:disable-next-line:max-classes-per-file
export class SecurityNavigationLinkImpl extends React.Component<AllProps> {
  public render() {
    return (
      <SecurityLinkQueryComponent query={securityLinkQuery}>
        {({ data, loading, error }) => (
          <NavigationLinkWithAnalytics path={this.getHref({ data, loading, error })} exact={true} title={this.props.intl.formatMessage(messages.security)} />
        )}
      </SecurityLinkQueryComponent >
    );
  }

  private getHref = ({ data, loading, error }: { data?: SecurityLinkQuery, loading: boolean, error?: ApolloError }) => {
    if (error || loading || !data || !data.currentUser || !data.currentUser.organizations) {
      return '/';
    }

    const orgCount = data.currentUser.organizations.length;

    if (orgCount === 0) {
      return '/atlassian-access';
    }

    if (orgCount === 1) {
      return `/o/${data.currentUser.organizations[0].id}/overview`;
    }

    return '/';

  }
}

export const SecurityNavigationLink = injectIntl<{}>(SecurityNavigationLinkImpl);
