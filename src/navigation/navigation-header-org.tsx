import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';
import styled from 'styled-components';

import { AkContainerTitle } from '@atlaskit/navigation';

import { AnalyticsData, Link } from 'common/analytics';
import { OrgIcon } from 'common/icons';

import { OrgRouteProps } from '../organization/routes';
import { NavigationHeaderQuery } from '../schema/schema-types';
import navigationHeaderQuery from './navigation-header-org.query.graphql';
import { NavigationHeader } from './navigation-header.styled';

const analyticsData: AnalyticsData = {
  action: 'click',
  actionSubject: 'sidebarContainerButton',
  actionSubjectId: 'org.header',
  subproduct: 'chrome',
};

const NonStyledLink = styled(Link)`
  text-decoration: none;

  &:focus, &:hover, &:visited, &:link, &:active {
    text-decoration: none;
  }
`;

type OwnProps = ChildProps<null, NavigationHeaderQuery>;
type OwnPropsWithRouter = RouteComponentProps<OrgRouteProps> & OwnProps;

const messages = defineMessages({
  organization: {
    id: 'navigation.header.org.title.subText',
    defaultMessage: 'Organization',
    description: 'Sub text for the container title',
  },
  loading: {
    id: 'navigation.header.org.title.loading',
    defaultMessage: 'Loading...',
    description: 'Placeholder text while the org name is loading',
  },
  admin: {
    id: 'navigation.header.org.title.admin',
    defaultMessage: 'Admin',
    description: 'Title text for the header on admin',
  },
});

class NavigationHeaderOrgImpl extends React.Component<OwnPropsWithRouter & InjectedIntlProps> {
  public render() {
    const {
      intl: { formatMessage },
      data: {
        loading = true,
        error = null,
        organization = null,
      } = {},
      match: {
        params: { orgId },
      },
    } = this.props;

    const text = loading ?
      formatMessage(messages.loading) :
      (error || !organization) ?
        '' :
        organization.name;

    return (
      <div>
        <NavigationHeader>
          {formatMessage(messages.admin)}
        </NavigationHeader>
        <NonStyledLink to={`/o/${orgId}/overview`} analyticsData={analyticsData}>
          <AkContainerTitle
            text={text}
            icon={<OrgIcon size="large" />}
            subText={formatMessage(messages.organization)}
          />
        </NonStyledLink>
      </div>
    );
  }
}

const withOrgName = graphql<RouteComponentProps<OrgRouteProps>, NavigationHeaderQuery>(
  navigationHeaderQuery,
  {
    options: (componentProps) => ({
      variables: {
        id: componentProps.match.params.orgId,
      },
    }),
    skip: (componentProps) => !componentProps.match.params.orgId,
  },
);

export const NavigationHeaderOrg = withRouter<OwnProps>(withOrgName(injectIntl(NavigationHeaderOrgImpl)));
