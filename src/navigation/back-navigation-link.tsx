import * as React from 'react';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';

import { NavigationLinkIconGlyph } from 'common/navigation';

import { NavigationLink } from './navigation-link';

const messages = defineMessages({
  back: {
    id: `admin.nav.back`,
    defaultMessage: 'Back',
  },
});

interface OwnProps {
  onClick(): void;
}

class BackNavigationLinkImpl extends React.Component<OwnProps & InjectedIntlProps> {
  public render() {
    const { onClick, intl: { formatMessage } } = this.props;

    return (<NavigationLink actionSubjectId="backLink" icon={NavigationLinkIconGlyph.ArrowLeft} onClick={onClick} title={formatMessage(messages.back)}/>);
  }
}

export const BackNavigationLink = injectIntl<OwnProps>(BackNavigationLinkImpl);
