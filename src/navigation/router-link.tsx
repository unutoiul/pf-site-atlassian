import * as React from 'react';
import { Link } from 'react-router-dom';

interface RouterLinkProps {
  href: string;
  className: string;
  onMouseDown(): void;
  onClick(): void;
}

export class RouterLink extends React.PureComponent<RouterLinkProps> {
  public render() {
    const {
      href,
      children,
      onMouseDown,
      onClick,
      className,
    } = this.props;

    const customStyles = { color: 'currentColor' };

    return (href ? (
      <Link
        className={className}
        onMouseDown={onMouseDown}
        onClick={onClick}
        to={href}
        style={customStyles}
      >
        {children}
      </Link>) : (
        children
      )
    );
  }
}
