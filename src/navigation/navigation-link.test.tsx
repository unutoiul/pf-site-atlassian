import { expect } from 'chai';
import { mount, ReactWrapper } from 'enzyme';
import * as React from 'react';
import { MemoryRouter } from 'react-router-dom';

import AkShortcutIcon from '@atlaskit/icon/glyph/shortcut';
import AkLozenge from '@atlaskit/lozenge';
import AkNavigation, { AkNavigationItem } from '@atlaskit/navigation';

import { RouterLink } from 'common/navigation';

import { createMockIntlContext } from '../utilities/testing';
import { NavigationLink } from './navigation-link';

function mountWithNavigationContext(node: React.ReactNode, initialEntries = [{ pathname: '/' }]): ReactWrapper<any, any> {
  return mount(
    <MemoryRouter initialEntries={initialEntries}>
      <AkNavigation>
        {node}
      </AkNavigation>
    </MemoryRouter>, createMockIntlContext(),
  );
}

describe('navigation-link (v2)', () => {

  it('should render with target=_blank only when links are external', () => {
    const internalLink = mountWithNavigationContext(<NavigationLink path="#foo"/>);
    expect(internalLink.find(NavigationLink).find('a').at(0).props().target).to.not.equal('_blank');

    const externalLink = mountWithNavigationContext(<NavigationLink isExternal={true} path="#foo"/>);
    expect(externalLink.find(NavigationLink).find('a').at(0).props().target).to.equal('_blank');
  });

  it('should render the path given', () => {
    const internalLink = mountWithNavigationContext(<NavigationLink path="#foo"/>);
    expect(internalLink.find(NavigationLink).find('a').at(0).props().href).to.equal('/#foo');
  });

  it('should pass null as the icon if it is not specified', () => {
    const internalLink = mountWithNavigationContext(<NavigationLink path="#foo"/>);

    const iconProp = internalLink.find(AkNavigationItem).prop('icon');

    expect(iconProp).to.equal(null);
  });

  it('should render lozenge if specified', () => {
    const internalLink = mountWithNavigationContext(<NavigationLink lozenge={{ type: 'beta' }} path="#foo"/>);
    const lozenge = internalLink.find(AkLozenge);

    expect(lozenge.exists()).to.equal(true);
  });

  it('should not render lozenge if not specified', () => {
    const internalLink = mountWithNavigationContext(<NavigationLink path="#foo"/>);
    const lozenge = internalLink.find(AkLozenge);

    expect(lozenge.exists()).to.equal(false);
  });

  it('should pass isSelected as true if current location matches path', () => {
    const currentPath = '/test/path';
    const internalLink = mountWithNavigationContext(
      <NavigationLink path={currentPath}/>,
        [{ pathname: currentPath }],
      );
    expect(internalLink.find(AkNavigationItem).prop('isSelected')).to.equal(true);
  });

  it('should pass isSelected as false if current location partially matches path and `exact={true}` is passed', () => {
    const internalLink = mountWithNavigationContext(
      <NavigationLink path={'/test/path'} exact={true}/>,
        [{ pathname: '/test/path/1' }],
      );
    expect(internalLink.find(AkNavigationItem).prop('isSelected')).to.equal(false);
  });

  it('should pass isSelected as true if current location exactly matches path and `exact={true}` is passed', () => {
    const internalLink = mountWithNavigationContext(
      <NavigationLink path={'/test/path'} exact={true}/>,
        [{ pathname: '/test/path' }],
      );
    expect(internalLink.find(AkNavigationItem).prop('isSelected')).to.equal(true);
  });

  it('should pass isSelected as true if current location is subpage of path', () => {
    const internalLink = mountWithNavigationContext(
      <NavigationLink path="/test/path"/>,
        [{ pathname: '/test/path/subpage' }],
      );
    expect(internalLink.find(AkNavigationItem).prop('isSelected')).to.equal(true);
  });

  it('should pass isSelected as false if current location does not match path', () => {
    const internalLink = mountWithNavigationContext(
      <NavigationLink path="/test/foo"/>,
        [{ pathname: '/test/bar' }],
      );
    expect(internalLink.find(AkNavigationItem).prop('isSelected')).to.equal(false);
  });

  it('should render a shortcut icon only when external', () => {
    const internalLink = mountWithNavigationContext(<NavigationLink path="#foo"/>);
    expect(internalLink.find(NavigationLink).find(AkShortcutIcon).length).to.equal(0);
    const externalLink = mountWithNavigationContext(<NavigationLink isExternal={true} path="#foo"/>);
    expect(externalLink.find(NavigationLink).find(AkShortcutIcon).length).to.equal(1);
  });

  it('should render a router link if there is a path', () => {
    const internalLink = mountWithNavigationContext(<NavigationLink path="#foo"/>);
    expect(internalLink.find(RouterLink).exists()).to.equal(true);
  });

  it('should not render a router link if there is no path', () => {
    const mock = () => {
      return;
    };
    const internalLink = mountWithNavigationContext(<NavigationLink onClick={mock}/>);
    expect(internalLink.find(RouterLink).exists()).to.equal(false);
  });
});
