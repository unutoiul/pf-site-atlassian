import * as React from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';

import { NavigationHeader } from './navigation-header.styled';

const messages = defineMessages({
  admin: {
    id: 'navigation.header.site.title',
    defaultMessage: 'Admin',
    description: 'Title text for the site header.',
  },
});

export const NavigationHeaderSite: React.SFC = () => (
  <NavigationHeader>
    <FormattedMessage {...messages.admin} />
  </NavigationHeader>
);
