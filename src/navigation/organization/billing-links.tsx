import * as React from 'react';
import { defineMessages } from 'react-intl';

import { withClickAnalytics } from 'common/analytics';

import { BackNavigationLink } from '../back-navigation-link';
import { NavigationLink } from '../navigation-link';

const messages = defineMessages({
  overview: {
    id: `admin.org.nav.billing.overview`,
    defaultMessage: 'Overview',
  },
  estimate: {
    id: `admin.org.nav.billing.estimate`,
    defaultMessage: 'Bill estimate',
  },
  paymentDetails: {
    id: `admin.org.nav.billing.details`,
    defaultMessage: 'Billing details',
  },
  history: {
    id: `admin.org.nav.billing.history`,
    defaultMessage: 'Billing history',
  },
  applications: {
    id: `admin.org.nav.billing.applications`,
    defaultMessage: 'Manage subscriptions',
  },
});

const overviewLink = (orgId) => `/o/${orgId}/billing/overview`;
const estimateLink = (orgId) => `/o/${orgId}/billing/estimate`;
const paymentDetailsLink = (orgId) => `/o/${orgId}/billing/paymentdetails`;
const applicationsLink = (orgId) => `/o/${orgId}/billing/applications`;
const historyLink = (orgId) => `/o/${orgId}/billing/history`;

export const NavigationLinkWithAnalytics = withClickAnalytics<any>()(NavigationLink);

// For any nested navigation, all the information required to for a list of links available is passed down.
// Example, if the passwordpolicy link is dynamic, the condition for it will be passed here and this will return only the links that will have to be rendered.
export const billingLinks = (orgId: string, formatMessage, onClick: () => void): JSX.Element => (
  <div>
    <BackNavigationLink onClick={onClick} />
    <NavigationLinkWithAnalytics actionSubjectId="billingOverviewLink" path={overviewLink(orgId)} title={formatMessage(messages.overview)} />
    <NavigationLinkWithAnalytics actionSubjectId="billingDetailsLink" path={paymentDetailsLink(orgId)} title={formatMessage(messages.paymentDetails)} />
    <NavigationLinkWithAnalytics actionSubjectId="billingEstimateLink" path={estimateLink(orgId)} title={formatMessage(messages.estimate)} />
    <NavigationLinkWithAnalytics actionSubjectId="billingHistoryLink" path={historyLink(orgId)} title={formatMessage(messages.history)} />
    <NavigationLinkWithAnalytics actionSubjectId="manageSubscriptionsLink" path={applicationsLink(orgId)} title={formatMessage(messages.applications)} />
  </div>
);

const billingNavLinkPaths = (orgId): string[] => {
  return [
    overviewLink(orgId),
    estimateLink(orgId),
    historyLink(orgId),
    applicationsLink(orgId),
    paymentDetailsLink(orgId),
  ];
};

export const isBillingLinkActive = (orgId: string, pathname: string): boolean => {
  return billingNavLinkPaths(orgId).includes(pathname);
};
