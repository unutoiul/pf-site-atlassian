import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { IntlProvider } from 'react-intl';
import { match } from 'react-router';
import { SinonStub, stub } from 'sinon';

import {
  AkContainerNavigationNested,
} from '@atlaskit/navigation';

import { OrgRouteProps } from '../../organization/routes';
import { BackNavigationLink } from '../back-navigation-link';
import { NavigationLinkWithAnalytics as NavigationBillingLinkWithAnalytics } from './billing-links';
import { NavigationLinkWithAnalytics, NestedNavigationImpl } from './nested-navigation';

const intlProvider = new IntlProvider({ locale: 'en' }, {});
const { intl } = intlProvider.getChildContext();

const orgId = 'my-org';

const getMatchProp = (page): match<OrgRouteProps> => ({
  params: {
    orgId,
  },
  isExact: true,
  path: `/o/${orgId}/${page}`,
  url: `/o/${orgId}/${page}`,
});

const getLocationProp = (page) => ({
  pathname: `/o/${orgId}/${page}`,
  search : '',
  state : '',
  hash : '',
});

describe('Nested org navigation', () => {
  it('should not render anything while data is loading', () => {
    const wrapper = shallow(
      <NestedNavigationImpl
        intl={intl}
        match={getMatchProp('billing/details')}
        location={getLocationProp('billing/details')}
        history={null as any}
        showBillingNav={true}
        loading={true}
        adminApiFeatureFlag={{ isLoading: false, value: false }}
      />,
    );

    const nestedLinks = wrapper.find(AkContainerNavigationNested);

    expect(nestedLinks.length).to.equal(0);
  });

  it('should not render anything while feature flag is loading', () => {
    const wrapper = shallow(
      <NestedNavigationImpl
        intl={intl}
        match={getMatchProp('billing/details')}
        location={getLocationProp('billing/details')}
        history={null as any}
        showBillingNav={true}
        loading={false}
        adminApiFeatureFlag={{ isLoading: true, value: false }}
      />,
    );

    const nestedLinks = wrapper.find(AkContainerNavigationNested);

    expect(nestedLinks.length).to.equal(0);
  });

  it('should render top level nav with billing link when identity manager billing link is enabled', () => {
    const wrapper = shallow(
      <NestedNavigationImpl
        intl={intl}
        match={getMatchProp('members')}
        location={getLocationProp('members')}
        history={null as any}
        showBillingNav={true}
        loading={false}
        adminApiFeatureFlag={{ isLoading: false, value: false }}
      />,
    );

    const topLevelLinks = wrapper.find(AkContainerNavigationNested).prop('stack')[0];
    const linksWrapper = shallow(topLevelLinks);

    expect(linksWrapper.find(NavigationLinkWithAnalytics).length).to.equal(9);
    expect(linksWrapper.find({ title: 'Billing' }).length).to.equal(1);
  });

  it('should render top level nav without billing link when identity manager billing link is disabled', () => {
    const wrapper = shallow(
      <NestedNavigationImpl
        intl={intl}
        match={getMatchProp('members')}
        location={getLocationProp('members')}
        history={null as any}
        showBillingNav={false}
        loading={false}
        adminApiFeatureFlag={{ isLoading: false, value: false }}
      />,
    );

    const topLevelLinks = wrapper.find(AkContainerNavigationNested).prop('stack')[0];
    const linksWrapper = shallow(topLevelLinks);

    expect(linksWrapper.find(NavigationLinkWithAnalytics).length).to.equal(8);
    expect(linksWrapper.find({ title: 'Billing' }).length).to.equal(0);
  });

  it('should render billing nav if that is the active route and billing links are to be displayed', () => {
    const wrapper = shallow(
      <NestedNavigationImpl
        intl={intl}
        match={getMatchProp('billing/overview')}
        location={getLocationProp('billing/overview')}
        history={null as any}
        showBillingNav={true}
        loading={false}
        adminApiFeatureFlag={{ isLoading: false, value: false }}
      />,
    );

    const nestedBillingLinks = wrapper.find(AkContainerNavigationNested).prop('stack')[1];
    const linksWrapper = shallow(nestedBillingLinks);

    expect(linksWrapper.find(NavigationBillingLinkWithAnalytics).length).to.equal(5);
    expect(linksWrapper.find(BackNavigationLink).length).to.equal(1);
  });

  it('should render top level nav with user provisioning link', () => {
    const wrapper = shallow(
      <NestedNavigationImpl
        intl={intl}
        match={getMatchProp('members')}
        location={getLocationProp('members')}
        history={null as any}
        showBillingNav={true}
        loading={false}
        adminApiFeatureFlag={{ isLoading: false, value: false }}
      />,
    );

    const topLevelLinks = wrapper.find(AkContainerNavigationNested).prop('stack')[0];
    const linksWrapper = shallow(topLevelLinks);

    expect(linksWrapper.find(NavigationLinkWithAnalytics).length).to.equal(9);
    expect(linksWrapper.find({ title: 'User provisioning' }).length).to.equal(1);
  });

  it('should render top level nav with admin API link when feature is enabled', () => {
    const wrapper = shallow(
      <NestedNavigationImpl
        intl={intl}
        match={getMatchProp('members')}
        location={getLocationProp('members')}
        history={null as any}
        showBillingNav={true}
        loading={false}
        adminApiFeatureFlag={{ isLoading: false, value: true }}
      />,
    );

    const topLevelLinks = wrapper.find(AkContainerNavigationNested).prop('stack')[0];
    const linksWrapper = shallow(topLevelLinks);

    expect(linksWrapper.find(NavigationLinkWithAnalytics).length).to.equal(10);
    expect(linksWrapper.find({ title: 'API keys' }).length).to.equal(1);
  });

  it('should render top level nav without admin API link when feature is disabled', () => {
    const wrapper = shallow(
      <NestedNavigationImpl
        intl={intl}
        match={getMatchProp('members')}
        location={getLocationProp('members')}
        history={null as any}
        showBillingNav={true}
        loading={false}
        adminApiFeatureFlag={{ isLoading: false, value: false }}
      />,
    );

    const topLevelLinks = wrapper.find(AkContainerNavigationNested).prop('stack')[0];
    const linksWrapper = shallow(topLevelLinks);

    expect(linksWrapper.find(NavigationLinkWithAnalytics).length).to.equal(9);
    expect(linksWrapper.find({ title: 'API keys' }).length).to.equal(0);
  });

  describe('long load times', () => {
    let setTimeoutStub: SinonStub;
    let clearTimeoutStub: SinonStub;
    let dummyTimeoutId: number;

    beforeEach(() => {
      dummyTimeoutId = Date.now();
      setTimeoutStub = stub(window, 'setTimeout').returns(dummyTimeoutId);
      clearTimeoutStub = stub(window, 'clearTimeout');
    });

    afterEach(() => {
      setTimeoutStub.restore();
      clearTimeoutStub.restore();
    });

    it('should render nav after predefined amount of time even if billing nav info is still loading', () => {
      const wrapper = shallow(
        <NestedNavigationImpl
          intl={intl}
          match={getMatchProp('members')}
          location={getLocationProp('members')}
          history={null as any}
          showBillingNav={false}
          loading={true}
          adminApiFeatureFlag={{ isLoading: false, value: false }}
        />,
      );

      expect(setTimeoutStub.callCount).to.equal(1);
      const [callback, timeout] = setTimeoutStub.getCall(0).args;
      expect(timeout).to.equal(2000);

      callback();
      wrapper.update();

      const topLevelLinks = wrapper.find(AkContainerNavigationNested).prop('stack')[0];
      const linksWrapper = shallow(topLevelLinks);

      expect(linksWrapper.find(NavigationLinkWithAnalytics).length).to.equal(8);
      expect(linksWrapper.find({ title: 'Billing' }).length).to.equal(0);
    });

    it('should render nav after predefined amount of time even if admin API feature flag is still loading', () => {
      const wrapper = shallow(
        <NestedNavigationImpl
          intl={intl}
          match={getMatchProp('members')}
          location={getLocationProp('members')}
          history={null as any}
          showBillingNav={false}
          loading={false}
          adminApiFeatureFlag={{ isLoading: true, value: false }}
        />,
      );

      expect(setTimeoutStub.callCount).to.equal(1);
      const [callback, timeout] = setTimeoutStub.getCall(0).args;
      expect(timeout).to.equal(2000);

      callback();
      wrapper.update();

      const topLevelLinks = wrapper.find(AkContainerNavigationNested).prop('stack')[0];
      const linksWrapper = shallow(topLevelLinks);

      expect(linksWrapper.find(NavigationLinkWithAnalytics).length).to.equal(8);
      expect(linksWrapper.find({ title: 'API keys' }).length).to.equal(0);
    });

    it('should render billing item once it has finished loading', () => {
      const wrapper = shallow(
        <NestedNavigationImpl
          intl={intl}
          match={getMatchProp('members')}
          location={getLocationProp('members')}
          history={null as any}
          showBillingNav={false}
          loading={true}
          adminApiFeatureFlag={{ isLoading: false, value: false }}
        />,
      );

      expect(setTimeoutStub.callCount).to.equal(1);
      const [callback, timeout] = setTimeoutStub.getCall(0).args;
      expect(timeout).to.equal(2000);

      callback();
      wrapper.update();

      const topLevelLinksBeforeLoadComplete = wrapper.find(AkContainerNavigationNested).prop('stack')[0];
      const linksWrapperBeforeLoadComplete = shallow(topLevelLinksBeforeLoadComplete);

      expect(linksWrapperBeforeLoadComplete.find(NavigationLinkWithAnalytics).length).to.equal(8);

      wrapper.setProps({ loading: false, showBillingNav: true });

      const topLevelLinksAfterLoadComplete = wrapper.find(AkContainerNavigationNested).prop('stack')[0];
      const linksWrapperAfterLoadComplete = shallow(topLevelLinksAfterLoadComplete);

      expect(linksWrapperAfterLoadComplete.find(NavigationLinkWithAnalytics).length).to.equal(9);
    });

    it('should render API keys item once it has finished loading', () => {
      const wrapper = shallow(
        <NestedNavigationImpl
          intl={intl}
          match={getMatchProp('members')}
          location={getLocationProp('members')}
          history={null as any}
          showBillingNav={false}
          loading={false}
          adminApiFeatureFlag={{ isLoading: true, value: false }}
        />,
      );

      expect(setTimeoutStub.callCount).to.equal(1);
      const [callback, timeout] = setTimeoutStub.getCall(0).args;
      expect(timeout).to.equal(2000);

      callback();
      wrapper.update();

      const topLevelLinksBeforeLoadComplete = wrapper.find(AkContainerNavigationNested).prop('stack')[0];
      const linksWrapperBeforeLoadComplete = shallow(topLevelLinksBeforeLoadComplete);

      expect(linksWrapperBeforeLoadComplete.find(NavigationLinkWithAnalytics).length).to.equal(8);

      wrapper.setProps({ adminApiFeatureFlag: { isLoading: false, value: true } });

      const topLevelLinksAfterLoadComplete = wrapper.find(AkContainerNavigationNested).prop('stack')[0];
      const linksWrapperAfterLoadComplete = shallow(topLevelLinksAfterLoadComplete);

      expect(linksWrapperAfterLoadComplete.find(NavigationLinkWithAnalytics).length).to.equal(9);
    });

    it('should unsubscribe from timeout upon being unmounted', () => {
      const wrapper = shallow(
        <NestedNavigationImpl
          intl={intl}
          match={getMatchProp('members')}
          location={getLocationProp('members')}
          history={null as any}
          showBillingNav={false}
          loading={false}
          adminApiFeatureFlag={{ isLoading: true, value: false }}
        />,
      );

      expect(clearTimeoutStub.callCount).to.equal(0);

      wrapper.unmount();

      expect(clearTimeoutStub.callCount).to.equal(1);
      expect(clearTimeoutStub.getCall(0).args[0]).to.equal(dummyTimeoutId);
    });
  });
});
