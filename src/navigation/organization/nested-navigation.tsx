import * as React from 'react';
import { graphql } from 'react-apollo';
import { defineMessages, InjectedIntlProps, injectIntl } from 'react-intl';
import { RouteComponentProps, withRouter } from 'react-router';

import AkArrowRightIcon from '@atlaskit/icon/glyph/arrow-right';
import {
  AkContainerNavigationNested,
  AkNavigationItemGroup,
} from '@atlaskit/navigation';

import { AnalyticsData, withClickAnalytics } from 'common/analytics';
import { ErrorBoundary } from 'common/error-boundary';
import { FeedbackNavigationGroup } from 'common/feedback';

import { AdminApiFeatureFlag, withAdminApiFeatureFlag } from '../../organization/feature-flags/with-admin-api-feature-flag';
import { OrgRouteProps } from '../../organization/routes';
import { OrgBillingNavFeatureFlagQuery, OrgBillingNavFeatureFlagQueryVariables } from '../../schema/schema-types';
import { NavigationLink } from '../navigation-link';
import { billingLinks, isBillingLinkActive } from './billing-links';
import orgBillingNavFeatureFlagQuery from './nested-navigation.query.graphql';

const messages = defineMessages({
  overview: {
    id: 'admin.nav.overview',
    defaultMessage: 'Overview',
  },
  admins: {
    id: 'admin.nav.admins',
    defaultMessage: 'Administrators',
  },
  domains: {
    id: 'admin.nav.domains',
    defaultMessage: 'Domains',
  },
  managed: {
    id: 'admin.nav.managed.accounts',
    defaultMessage: 'Managed accounts',
  },
  saml: {
    id: 'admin.nav.saml',
    defaultMessage: 'SAML single sign-on',
  },
  userprovisioning: {
    id: 'admin.nav.user.provisioning',
    defaultMessage: 'User provisioning',
    description: 'User provisioning is connecting to an identity provider like active directory and then automatically syncing groups and users from there using SCIM: https://en.wikipedia.org/wiki/System_for_Cross-domain_Identity_Management. It is not exactly user creation, it is using an external source to provision users in your own system. The translation of this should match organization.userProvisioning.title',
  },
  passwordManagement: {
    id: 'admin.nav.password.management',
    defaultMessage: 'Password management',
  },
  twoStepVerification: {
    id: 'admin.nav.two.step.verification',
    defaultMessage: 'Two-step verification',
  },
  billing: {
    id: 'admin.nav.billing',
    defaultMessage: 'Billing',
  },
  identityManager: {
    id: 'admin.nav.identity.manager',
    defaultMessage: 'Atlassian Access',
  },
  developers: {
    id: 'admin.nav.developers',
    defaultMessage: 'Developers',
    description: 'Header for a group of navigation links targeted to developers',
  },
  adminApi: {
    id: 'admin.nav.adminApi',
    defaultMessage: 'API keys',
    description: 'Navigation link for a page containing API key managementent',
  },
});

const getAnalyticsData = (analyticsSubjectId: string): AnalyticsData => {
  return {
    action: 'click',
    actionSubject: 'sidebarContainerButton',
    actionSubjectId: analyticsSubjectId,
    subproduct: 'chrome',
  };
};

interface OrgBillingNavProps {
  loading: boolean;
  showBillingNav: boolean;
}

type AllProps = InjectedIntlProps & RouteComponentProps<OrgRouteProps> & OrgBillingNavProps & AdminApiFeatureFlag;

const overviewLink = (orgId: string): string => `/o/${orgId}/overview`;
const adminsLink = (orgId: string): string => `/o/${orgId}/admins`;
const domainsLink = (orgId: string): string => `/o/${orgId}/domains`;
const managedLink = (orgId: string): string => `/o/${orgId}/members`;
const samlLink = (orgId: string): string => `/o/${orgId}/saml`;
const userProvisioningLink = (orgId: string): string => `/o/${orgId}/user-provisioning`;
const passwordManagementLink = (orgId: string): string => `/o/${orgId}/passwordpolicy`;
const twoStepVerificationLink = (orgId: string): string => `/o/${orgId}/two-step-verification`;
const adminApiLink = (orgId: string): string => `/o/${orgId}/admin-api`;

const enum ActiveNav {
  TopLevel,
  Billing,
}

export const NavigationLinkWithAnalytics = withClickAnalytics<any>()(NavigationLink);

interface NavigationState {
  activeNavigation: ActiveNav;
  ignoreLoading?: boolean;
}

const MAX_TIME_TO_WAIT_FOR_LOAD_MS = 2000;

export class NestedNavigationImpl extends React.Component<AllProps, NavigationState> {
  private timeoutId: number | undefined;

  constructor(props) {
    super(props);
    const { match: { params: { orgId } }, location: { pathname } } = props;

    if (isBillingLinkActive(orgId, pathname)) {
      this.state = {
        activeNavigation: ActiveNav.Billing,
      };
    } else {
      this.state = {
        activeNavigation: ActiveNav.TopLevel,
      };
    }
  }

  public componentDidMount() {
    // making sure that if the loading of certain things takes too long - we still render sth after a sensible timeout
    this.timeoutId = window.setTimeout(() => {
      this.setState({ ignoreLoading: true });
    }, MAX_TIME_TO_WAIT_FOR_LOAD_MS);
  }

  public componentWillUnmount() {
    window.clearTimeout(this.timeoutId);
  }

  public render() {
    const { loading, adminApiFeatureFlag } = this.props;
    const { ignoreLoading } = this.state;

    const isLoading = loading || adminApiFeatureFlag.isLoading;
    const isReadyToRender = ignoreLoading || !isLoading;

    return !isReadyToRender
      ? null
      : (
        <ErrorBoundary>
          <AkContainerNavigationNested stack={this.getNavItems()} />
        </ErrorBoundary>
      );
  }

  private getNavItems = () => {
    if (!this.props.showBillingNav) {
      return this.getTopLevelLinks();
    }

    const getNavItems = {
      [ActiveNav.TopLevel]: this.getTopLevelLinks,
      [ActiveNav.Billing]: this.getBillingLinks,
    };

    return getNavItems[this.state.activeNavigation]();
  }

  private getTopLevelLinks = () => {
    const {
      showBillingNav,
      match: { params: { orgId } },
      intl: { formatMessage },
      adminApiFeatureFlag,
    } = this.props;

    return [
      (
        <div>
          <NavigationLinkWithAnalytics exact={true} path={overviewLink(orgId)} title={formatMessage(messages.overview)} analyticsData={getAnalyticsData('org.overview')} />
          <NavigationLinkWithAnalytics path={adminsLink(orgId)} title={formatMessage(messages.admins)} analyticsData={getAnalyticsData('org.admins')} />
          <NavigationLinkWithAnalytics path={domainsLink(orgId)} title={formatMessage(messages.domains)} analyticsData={getAnalyticsData('org.domains')} />
          <NavigationLinkWithAnalytics path={managedLink(orgId)} title={formatMessage(messages.managed)} analyticsData={getAnalyticsData('org.members')} />
          <AkNavigationItemGroup title={formatMessage(messages.identityManager)}>
            <NavigationLinkWithAnalytics path={samlLink(orgId)} title={formatMessage(messages.saml)} analyticsData={getAnalyticsData('org.saml')} />
            <NavigationLinkWithAnalytics lozenge={{ type: 'new' }} path={userProvisioningLink(orgId)} title={formatMessage(messages.userprovisioning)} analyticsData={getAnalyticsData('org.userprovisioning')} />
            <NavigationLinkWithAnalytics path={passwordManagementLink(orgId)} title={formatMessage(messages.passwordManagement)} analyticsData={getAnalyticsData('org.passwordpolicy')} />
            <NavigationLinkWithAnalytics path={twoStepVerificationLink(orgId)} title={formatMessage(messages.twoStepVerification)} analyticsData={getAnalyticsData('org.two-step')} />
            {showBillingNav && (<NavigationLinkWithAnalytics
              actionSubjectId="pricingLink"
              iconRight={<AkArrowRightIcon label="" />}
              onClick={this.setActiveNavigation(ActiveNav.Billing)}
              title={formatMessage(messages.billing)}
              analyticsData={getAnalyticsData('org.billing')}
            />)}
          </AkNavigationItemGroup>
          {adminApiFeatureFlag.value && <AkNavigationItemGroup title={formatMessage(messages.developers)}>
            <NavigationLinkWithAnalytics actionSubjectId="discoverApplicationsLink" path={adminApiLink(orgId)} title={formatMessage(messages.adminApi)} analyticsData={getAnalyticsData('org.admin-api')} />
          </AkNavigationItemGroup>}
          <FeedbackNavigationGroup />
        </div >
      ),
    ];
  };

  private getBillingLinks = () => {
    const { match: { params: { orgId } }, intl: { formatMessage } } = this.props;

    return [
      [],
      billingLinks(orgId, formatMessage, this.setActiveNavigation(ActiveNav.TopLevel)),
    ];
  };

  private setActiveNavigation = (activeNavigation: ActiveNav) => () => {
    this.setState({ activeNavigation });
  };
}

const withBillingFeatureFlag = graphql<RouteComponentProps<OrgRouteProps>, OrgBillingNavFeatureFlagQuery, OrgBillingNavFeatureFlagQueryVariables, OrgBillingNavProps>(
  orgBillingNavFeatureFlagQuery,
  {
    options: (componentProps) => ({
      variables: {
        id: componentProps.match.params.orgId,
      },
    }),
    props: ({ data = { loading: false, error: null, organization: null } }) => {
      return {
        loading: data.loading,
        showBillingNav: !data.error && !!data.organization && data.organization.isBillable,
      };
    },
    skip: (componentProps: RouteComponentProps<OrgRouteProps>) => !componentProps.match.params.orgId,
  },
);

export const NestedNavigation = withRouter<{}>(
  withBillingFeatureFlag(
    withAdminApiFeatureFlag(
      injectIntl(
        NestedNavigationImpl,
      ),
    ),
  ),
);
