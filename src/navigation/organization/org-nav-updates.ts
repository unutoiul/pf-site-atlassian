import { PureQueryOptions } from 'apollo-client';

import orgBillingNavFeatureFlagQuery from './nested-navigation.query.graphql';

export const orgNavRefetchQueries = (orgId: string): PureQueryOptions[] => [
  {
    query: orgBillingNavFeatureFlagQuery,
    variables: { id: orgId },
  },
];
