import { expect } from 'chai';

import { isBillingLinkActive } from './billing-links';

const orgId = 'my-org';

describe('Billing Links', () => {
  it('should return true for billing nested pages', () => {
    expect(isBillingLinkActive(orgId, `/o/${orgId}/billing/overview`)).to.equal(true);
  });

  it('should return false for non billing nested pages', () => {
    expect(isBillingLinkActive(orgId, `/o/${orgId}/non-billing-link`)).to.equal(false);
  });
});
