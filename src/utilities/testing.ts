import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloClient } from 'apollo-client';
import { ApolloLink } from 'apollo-link';
import { onError } from 'apollo-link-error';
import { SchemaLink } from 'apollo-link-schema';
import { withClientState } from 'apollo-link-state';
import { ReactWrapper, ShallowWrapper } from 'enzyme';
import { addMockFunctionsToSchema, makeExecutableSchema } from 'graphql-tools';
import { IMocks } from 'graphql-tools/dist/Interfaces';
import merge from 'lodash.merge';
import { object } from 'prop-types';
import { ApolloProvider } from 'react-apollo';
import { IntlProvider } from 'react-intl';
import { sandbox, SinonSpy, spy } from 'sinon';

import { createApolloClient } from '../apollo-client';
import { combinedResolvers } from '../schema/resolvers';
import { uiState } from '../schema/resolvers/ui';
import { typeDefs } from '../schema/schema-defs';
import { random } from './random';

const intlProvider = new IntlProvider({ locale: 'en' }, {});
const { intl } = intlProvider.getChildContext();

export function createMockIntlContext() {
  return {
    context: {
      intl,
    },
    childContextTypes: {
      intl: object,
    },
  };
}

export interface MockContextSettings {
  intl?: boolean;
  apollo?: boolean;
}

export function createMockContext(options: MockContextSettings) {
  const apolloClient = createApolloClient((() => null));
  const apolloProvider = new ApolloProvider({ client: apolloClient, children: null }, {});
  const { client } = apolloProvider.getChildContext();

  return {
    context: {
      ...(options.intl ? { intl } : {}),
      ...(options.apollo ? { client } : {}),
    },
    childContextTypes: {
      ...(options.intl ? { intl: object } : {}),
      ...(options.apollo ? { client: object } : {}),
    },
  };
}

export function createMockIntlProp() {
  return new IntlProvider({ locale: 'en' }, {}).getChildContext().intl;
}

export interface MockAnalyticsClient {
  sendUIEvent: SinonSpy;
  sendTrackEvent: SinonSpy;
  sendScreenEvent: SinonSpy;
  init: SinonSpy;
  reset(): void;
}

export function createMockAnalyticsClient() {
  const sendUIEvent = spy();
  const sendTrackEvent = spy();
  const sendScreenEvent = spy();
  const init = spy();

  return {
    sendUIEvent,
    sendTrackEvent,
    sendScreenEvent,
    init,
    reset: () => {
      sendUIEvent.reset();
      sendTrackEvent.reset();
      sendScreenEvent.reset();
      init.reset();
    },
  } as MockAnalyticsClient;
}

// Simple promise-based timeouts with async/await
export const wait = async (ms) => {
  return new Promise(resolve => setTimeout(resolve, ms));
};

export const waitUntil = async (predicate, timeoutDuration = 1000) => {
  return new Promise((resolve, reject) => {
    const interval = setInterval(() => {
      if (predicate()) {
        clearInterval(interval);
        resolve();
      }
    }, 1);

    setTimeout(() => {
      if (!predicate()) {
        clearInterval(interval);
        reject(new Error(`waitUntil timed out after ${timeoutDuration}ms`));
      }
    }, timeoutDuration);
  });
};

// Helper to automatically wait for the supplied wrapper to update
export const waitUntilWithWrapperUpdate = async (
  predicate: () => boolean | number,
  wrapper: ReactWrapper,
  timeoutDuration?: number,
) => {
  try {
    await waitUntil(() => {
      wrapper.update();

      return predicate();
    }, timeoutDuration);
  } catch (e) {
    throw new Error(`${e.message}, original predicate: ${predicate.toString()}`);
  }
};

/*
 * Repeatedly render a component tree using enzyme.shallow() until
 * finding and rendering TargetComponent.
 */
export function shallowUntilTarget(root: ShallowWrapper<any>, TargetComponent: any) {
  function dive(node: any, depth: number) {
    if (node.is(TargetComponent)) {
      return node;
    }

    if (depth > 10) {
      return;
    }

    let result = null;
    node.find(':not(any)').forEach(child => {
      result = result || dive(
        typeof child.type() === 'string' ? child : child.dive(),
        depth + 1,
      );
    });

    return result;
  }

  return dive(root, 0);
}

/**
 * Useful utility for 'integration' style enzyme tests.
 * Walks down the react-tree looking for a text node and simulates a click on it.
 * @param {ReactWrapper} wrapper Wrapper you're looking in
 * @param {string} text Text you're looking for
 * @param {object} mockEventObject pass { button: 0 } when you click on links that need
 * the page to updated using a route. see: https://github.com/airbnb/enzyme/issues/516 for more.
 * @returns {null} Null
 */
export const clickOnText = (
  wrapper: ReactWrapper,
  text: string,
  mockEventObject: any = {},
) => {
  // returns 'true' if a node is a html tag ('div', 'span', 'h1', etc)
  const isHtmlTag = node => typeof node.type() === 'string';

  // find the lowest html tagged node in the tree to simulate the click on
  // this will ensure that any click handlers above the text are triggered.
  wrapper
    .findWhere(node => node.length > 0 && isHtmlTag(node) && node.text().includes(text))
    .last()
    .simulate('click', mockEventObject);
};

export interface MockLocalStorage {
  restore(): void;
}

export const mockLocalStorage = (): MockLocalStorage => {
  const localStorageSandbox = sandbox.create();
  localStorageSandbox.stub(window, 'localStorage');
  const restore = () => {
    localStorageSandbox.restore();
  };

  return { restore };
};

// This is for unit tests where you want to merge together fake and real resolvers
export const createApolloClientWithOverriddenResolvers = (overriddenResolvers: object): ApolloClient<any> => {
  const apolloCache: InMemoryCache = new InMemoryCache();

  const apolloSchema = makeExecutableSchema({
    typeDefs,
    resolvers: merge(combinedResolvers, overriddenResolvers),
  });

  return new ApolloClient({
    link: ApolloLink.from([
      // tslint:disable-next-line:no-console
      onError(console.error.bind(console)),
      withClientState({ ...uiState, cache: apolloCache }),
      new SchemaLink({ schema: apolloSchema }),
    ]),
    cache: apolloCache,
  });
};

// This is for unit tests where you do not want extra dependencies on our real resolvers
export function createMockApolloClient(mocks: IMocks) {
  const mockSchema = makeExecutableSchema({ typeDefs });
  addMockFunctionsToSchema({
    schema: mockSchema,
    mocks,
  });

  return createApolloClient(() => null, mockSchema);
}

export interface TestUserData {
  id: string;
  displayName: string;
  email: string;
  username: string;
}

export function getRandomUser(): TestUserData {
  const firstNames = [
    'Ruslan',
    'Ashwini',
    'Joshua',
    'Stephen',
    'Ian',
    'Tyler',
    'Swati',
    'Lingbo',
    'Helena',
  ];
  const lastNames = [
    'Arkhipau',
    'Rattihalli',
    'Nelson',
    'Bennett',
    'Tracey',
    'Van Hoomissen',
    'Raju',
    'Lu',
    'Lu',
  ];
  const ids = [
    '557058:43d09406-ff6e-41ed-9aae-fca16196e32b',
    '557057:26ead946-4955-46ef-a11e-b1f9ae987de7',
    '557057:03eeccea-d769-4c8b-b410-7619d2c7f640',
    '557057:c8534c8f-2190-4751-8c20-94a352b87440',
    '557058:d5096927-8106-4697-91f5-d7c86f3a8cb3',
    '557058:a26fd84b-c461-4c5c-ba46-a1d135eadbe0',
    '557058:a4e0cd3c-aabf-4aa3-ba57-07a40750c4af',
    '557057:d9e8d338-0f96-4ff6-9f81-41f566d9a113',
    '5a0a212739622c0fae911a0c',
  ];

  const id = random(ids);
  const first = random(firstNames);
  const last = random(lastNames);
  const username = `${first[0]}${last}`.replace(/ /g, '').toLowerCase();

  return {
    displayName: `${first} ${last}`,
    id,
    email: `${username}@storybalassian.com`,
    username,
  };
}

export const noop = () => undefined;
