import { getConfig } from 'common/config';

import { util as adminHubUtil } from './admin-hub';
import { util as redirectUtil } from './redirect';

const enum AuthRedirectMarker {
  AtlassianId = 'are=aid',
  Signup = 'are=tl', // "tl" stands for "Tenant Login"
}
const config = getConfig();

function getReturnUrl(marker: AuthRedirectMarker) {
  const hasQueryParams = window.location.search.indexOf('?') >= 0;
  const urlParamChar = hasQueryParams ? '&' : '?';

  return `${window.location.href}${urlParamChar}${marker}`;
}

function removeAuthRedirectMarkerFromUrl() {
  const location = window.location;
  const history = window.history;
  let newSearch = location.search.replace(AuthRedirectMarker.AtlassianId, '').replace(AuthRedirectMarker.Signup, '');
  if (newSearch === '?') {
    newSearch = '';
  }
  const replacement = location.origin + location.pathname + newSearch + location.hash;
  history.replaceState(history.state, document.title, replacement);
}

/**
 * Redirects the user to the login or signup page. This can be either Atlassian ID, or tenant login page, or a noop.
 * Please be sure to **not** rely on this function stopping the app execution (since it might be a noop).
 *
 * @export
 * @returns {void}
 */
export function redirectToLoginOrSignupOrNoop() {
  // if we came from Atlassian ID, and still receive 401s, most likely the user is not in the user base for current tenant
  if (window.__auth_redirect__ === AuthRedirectMarker.AtlassianId) {
    if (!adminHubUtil.isAdminHub()) {
      // only redirect to tenant signup page if we're not in the admin hub context
      const signupUrl = `${config.signup.url}?${config.signup.returnParamName}=${encodeURIComponent(getReturnUrl(AuthRedirectMarker.Signup))}`;
      redirectUtil.redirect(signupUrl);
    }

    return;
  } else if (window.__auth_redirect__ === AuthRedirectMarker.Signup) {
    // we are still receiving 401s even after going through both AID and tenant login page. Nothing to do here at this point, so noop
    return;
  } else {
    const redirectUrl = `${config.login.url}?${config.login.returnParamName}=${encodeURIComponent(getReturnUrl(AuthRedirectMarker.AtlassianId))}`;
    redirectUtil.redirect(redirectUrl);
  }
}

function getAuthRedirectSource(): string | false {
  if (window.location.search.indexOf(AuthRedirectMarker.AtlassianId) >= 0) {
    return AuthRedirectMarker.AtlassianId;
  }

  if (window.location.search.indexOf(AuthRedirectMarker.Signup) >= 0) {
    return AuthRedirectMarker.Signup;
  }

  return false;
}
/**
 * Saves the auth redirect status in app memory.
 *
 * @returns {void}
 */
export function preserveRedirectStatus(): void {
  const redirectSource = getAuthRedirectSource();
  if (!redirectSource) {
    return;
  }

  removeAuthRedirectMarkerFromUrl();
  window.__auth_redirect__ = redirectSource;
}
