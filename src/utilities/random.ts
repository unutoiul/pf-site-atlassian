export function randomBetween(min: number, max: number): number {
  return Math.floor(Math.random() * (max - min)) + min;
}

export function random<T>(array: T[]): T {
  return array[randomBetween(0, array.length)];
}
