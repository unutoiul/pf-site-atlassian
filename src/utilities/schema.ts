const isIE11 = () => !!window.navigator.userAgent.match(/Trident\/7\./);

const pragmaHeader: { [key: string]: string } = isIE11() ? { Pragma: 'no-cache' } : {};

export const fetchGetOptions = {
  credentials: 'same-origin' as RequestCredentials,
  headers: {
    ...pragmaHeader,
    'Content-Type': 'application/json',
  },
} as RequestInit;

export const fetchTextGetOptions = {
  credentials: 'same-origin' as RequestCredentials,
  headers: {
    ...pragmaHeader,
  },
} as RequestInit;

export const mutateOptions = {
  headers: {
    'Content-Type': 'application/json',
  },
} as RequestInit;
