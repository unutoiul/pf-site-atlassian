import { expect } from 'chai';

import { buildUserListBody, parseUserListResponse } from './directory-service';

describe('directory-service-utils', () => {
  describe('buildUserListBody', () => {
    it('throws error for empty userIds array', () => {
      expect(() => buildUserListBody('dummy', [])).to.throw();
    });

    it('creates graphQL for multiple userIds', () => {
      const cloudId = 'MONKEY_TROUSERS';
      const userIds = ['DavidDavidson', 'StevenStevenson'];

      const jsonBody = buildUserListBody(cloudId, userIds);
      const body = JSON.parse(jsonBody);
      expect(body.operationName).to.equal('UserList');
      expect(body.query !== undefined).to.equal(true);
      expect(body.query.indexOf(`user0:CloudUser(userId: "${userIds[0]}", cloudId: "${cloudId}")`)).to.not.equal(-1);
      expect(body.query.indexOf(`user1:CloudUser(userId: "${userIds[1]}", cloudId: "${cloudId}")`)).to.not.equal(-1);
    });
  });

  describe('parseUserListResponse', () => {

    it('returns empty list when no data property in response', () => {
      const json = {};
      expect(parseUserListResponse(json)).to.have.lengthOf(0);
    });

    it('returns array of user details for valid json response', async () => {
      const json = {
        data: {
          user0: {
            id: '123',
            fullName: 'Monkey Trousers',
            avatarUrl: 'http://example.org/user0',
          },
          user1: {
            id: '456',
            fullName: 'Simian Waistcoat',
            avatarUrl: 'http://example.org/user1',
          },
          user2: {
            id: '789',
            fullName: 'Gorrila Socks',
            avatarUrl: 'http://example.org/user2',
          },
        },
      };

      const users = parseUserListResponse(json);
      expect(users).to.have.lengthOf(3);

      expect(users[0].id).to.equal('123');
      expect(users[0].fullName).to.equal('Monkey Trousers');
      expect(users[0].avatarUrl).to.equal('http://example.org/user0');

      expect(users[1].id).to.equal('456');

      expect(users[2].id).to.equal('789');
    });
  });
});
