import * as URI from 'urijs';

export function getRelativeURI(location) {
  const uri = new URI(location.pathname);
  uri.search(location.search);
  uri.hash(location.hash);

  return uri;
}
