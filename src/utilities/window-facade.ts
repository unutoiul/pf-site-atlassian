export const reload: Location['reload'] = (...args) => window.location.reload(...args);
