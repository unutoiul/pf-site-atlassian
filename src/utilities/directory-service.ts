const buildUserListGraphQL = (cloudId: string, userIds: string[]): string => {
  if (userIds.length === 0) {
    throw new Error('Cannot build userList query for zero userIds');
  }
  let query = 'query UserList { \n';
  for (let i = 0; i < userIds.length; i++) {
    const userId = userIds[i];
    const label = `user${i}`;
    query = query + `${label}:CloudUser(userId: "${userId}", cloudId: "${cloudId}") { \n \
      id \n \
      fullName \n \
      avatarUrl(size: 200) \n \
    } \n`;
  }
  query = query + '} \n';

  return query;
};

interface UserListBody {
  query: string;
  operationName: string;
}

/**
 * Build the JSON formatted string representing the body of the POST to the directory service.
 *
 * @param {string} cloudId the id of the site to query for users against
 * @param {string[]} userIds the Atlassian Account Ids of the users to be retrieved.
 * @return {string} JSON formatted string representing a UserListBody
 */
export const buildUserListBody = (cloudId: string, userIds: string[]): string => {
  const payload: UserListBody = {
    query: buildUserListGraphQL(cloudId, userIds),
    operationName : 'UserList',
  };

  return JSON.stringify(payload);
};

export interface UserMinimalDisplayDetails {
  id: string;
  fullName: string;
  avatarUrl: string;
}

/**
 * Parse a json object response from the directory service into an array of user details. If the response
 * contains no results then an empty array will be returned.
 *
 * @param {any} json the response to be parsed.
 * @return {UserMinimalDisplayDetails[]} an array of the users described in the response.
 */
export const parseUserListResponse = (json: any): UserMinimalDisplayDetails[] => {

  const userList: UserMinimalDisplayDetails[] = [];
  if (json && json.data) {
    for (const key in json.data) {
      if (json.data.hasOwnProperty(key)) {
        const userDetails = json.data[key] as UserMinimalDisplayDetails;
        userList.push(userDetails);
      }
    }
  }

  return userList;
};
