
export const checkForDuplicateResolvers = (resolvers) => {
  const pathDuplicates = getDuplicates(
    concatAll(
      resolvers.map(getPaths),
    ),
  );
  if (pathDuplicates.length > 0) {
    throw new Error(`Resolver paths are not unique: ${pathDuplicates}`);
  }

};

const getPaths = (resolver) => {
  const generatePaths = (obj, context, coll) => {
    if (typeof obj !== 'object') {
      coll.push(context);

      return;
    }

    Object.keys(obj).forEach((key) => {
      const newContext = `${context}.${key}`;
      generatePaths(obj[key], newContext, coll);
    });
  };
  const paths = [];
  generatePaths(resolver, 'Root', paths);

  return paths;
};

const concatAll = (lists) =>
  lists.reduce((acc, elem) => {
    return acc.concat(elem);
  }, []);

const getDuplicates = (list) =>
  list.filter((value, index, arr) =>
    arr.indexOf(value) !== index);
