import { expect } from 'chai';

import { getCloudIdFromPath, getOrgIdFromPath } from './get-id-from-path';

describe('get-id-from-path', () => {
  describe('getCloudIdFromPath', () => {
    it('should return undefined when there is no id in the path', () => {
      expect(getCloudIdFromPath('/admin')).to.equal(undefined);
    });
    it('should return undefined when there is only an org id in the path', () => {
      expect(getCloudIdFromPath('/o/fake-org-id')).to.equal(undefined);
    });
    it('should return the cloud id for /s/fake-cloud-id', () => {
      expect(getCloudIdFromPath('/s/fake-cloud-id')).to.equal('fake-cloud-id');
    });
    it('should return the first cloud id for /s/fake-cloud-id/s/another-fake-cloud-id', () => {
      expect(getCloudIdFromPath('/s/fake-cloud-id')).to.equal('fake-cloud-id');
    });
    it('should return the cloud id for /s/fake-cloud-id/o/fake-org-id', () => {
      expect(getCloudIdFromPath('/s/fake-cloud-id/o/fake-org-id')).to.equal('fake-cloud-id');
    });
    it('should return the cloud id for /admin/s/fake-cloud-id', () => {
      expect(getCloudIdFromPath('/admin/s/fake-cloud-id')).to.equal('fake-cloud-id');
    });
    it('should return the cloud id for /admin/s/fake-cloud-id/users', () => {
      expect(getCloudIdFromPath('/admin/s/fake-cloud-id/users')).to.equal('fake-cloud-id');
    });
  });

  describe('getOrgIdFromPath', () => {
    it('should return undefined when there is no id in the path', () => {
      expect(getOrgIdFromPath('/admin')).to.equal(undefined);
    });
    it('should return undefined when there is only a cloud id in the path', () => {
      expect(getOrgIdFromPath('/s/fake-cloud-id')).to.equal(undefined);
    });
    it('should return the org id for /o/fake-org-id', () => {
      expect(getOrgIdFromPath('/o/fake-org-id')).to.equal('fake-org-id');
    });
    it('should return the first org id for /o/fake-org-id/o/another-fake-org-id', () => {
      expect(getOrgIdFromPath('/o/fake-org-id/o/another-fake-org-id')).to.equal('fake-org-id');
    });
    it('should return the org id for /o/fake-org-id/s/fake-cloud-id', () => {
      expect(getOrgIdFromPath('/o/fake-org-id/o/another-fake-org-id')).to.equal('fake-org-id');
    });
    it('should return the org id for /admin/o/fake-org-id', () => {
      expect(getOrgIdFromPath('/admin/o/fake-org-id')).to.equal('fake-org-id');
    });
    it('should return the org id for /admin/o/fake-org-id/members', () => {
      expect(getOrgIdFromPath('/admin/o/fake-org-id/members')).to.equal('fake-org-id');
    });
  });
});
