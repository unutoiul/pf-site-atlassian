import { expect } from 'chai';
import * as sinon from 'sinon';

import { wait } from '../utilities/testing';
import { PromiseQueue, PromiseQueueTask } from './promise-queue';

describe('PromiseQueue', () => {
  let queue: PromiseQueue;

  beforeEach(() => {
    queue = new PromiseQueue();
  });

  it('should push new tasks on the queue', async () => {
    const task: PromiseQueueTask = async () => {
      await wait(1);

      return 'Task result';
    };

    const first = queue.push(task);
    expect(queue.length()).to.equal(1);

    const second = queue.push(task);
    expect(queue.length()).to.equal(2);

    const firstResult = await first;
    const secondResult = await second;

    expect(firstResult).to.equal('Task result');
    expect(secondResult).to.equal('Task result');
  });

  it('should run a task when put on the queue', async () => {
    const taskSpy = sinon.spy();

    const task: PromiseQueueTask = async () => {
      await wait(1);
      taskSpy();
    };

    await queue.push(task);

    expect(taskSpy.calledOnce).to.equal(true);
  });

  it('should run all tasks on the queue', async () => {
    const taskSpy = sinon.spy();

    const task: PromiseQueueTask = async () => {
      taskSpy();

      return 'Task result';
    };

    await Promise.all([
      queue.push(task),
      queue.push(task),
    ]);

    await queue.push(task);

    expect(taskSpy.callCount).to.equal(3);
    expect(queue.length()).to.equal(0);
  });

  it('should run tasks in the correct FIFO order regardless of execution time', async () => {
    const firstTaskSpy = sinon.spy();
    const secondTaskSpy = sinon.spy();

    const firstTask: PromiseQueueTask = async () => {
      await wait(100);
      firstTaskSpy();

      return 'First task';
    };

    const secondTask: PromiseQueueTask = async () => {
      await wait(10);
      secondTaskSpy();

      return 'Second task';
    };

    await queue.push(firstTask);
    await queue.push(secondTask);

    expect(firstTaskSpy.calledOnce).to.equal(true);
    expect(secondTaskSpy.calledOnce).to.equal(true);
    expect(firstTaskSpy.calledBefore(secondTaskSpy)).to.equal(true);
  });
});
