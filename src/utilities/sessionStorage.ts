// a helper object for easier mocking of session storage (especially in Node environment)
export const sessionStorageUtils: Pick<Storage, 'getItem' | 'removeItem' | 'setItem'> = {
  getItem: (key: string) => {
    return sessionStorage.getItem(key);
  },
  setItem: (key: string, value: string) => {
    sessionStorage.setItem(key, value);
  },
  removeItem: (key: string) => {
    sessionStorage.removeItem(key);
  },
};
