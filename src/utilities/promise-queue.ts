interface InternalTask {
  task: PromiseQueueTask;
  resolve(value?: void | PromiseLike<void>): void;
}

export type PromiseQueueTask = () => any;

// FIFO queue for promises
export class PromiseQueue {
  private queue: InternalTask[] = [];

  public async push(task: PromiseQueueTask): Promise<any> {
    const promise = new Promise<any>(resolve => {
      this.queue.push({
        task,
        resolve,
      } as InternalTask);
    });

    if (this.queue.length === 1) {
      await this.run();
    }

    return promise;
  }

  public length(): number {
    return this.queue.length;
  }

  private async run(): Promise<void> {
    const topItem = this.queue[0];
    const result = await Promise.resolve(topItem.task());

    topItem.resolve(result);
    this.queue.shift();

    if (this.queue.length > 0) {
      await this.run();
    }
  }
}
