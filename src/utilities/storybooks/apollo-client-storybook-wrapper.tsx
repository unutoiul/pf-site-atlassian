// tslint:disable jsx-use-translation-function
import { ApolloClient } from 'apollo-client';
import * as React from 'react';

import { uiState } from '../../schema/resolvers/ui';

export interface ApolloClientStorybookWrapperProps {
  client: ApolloClient<any>;
  configureClient?(): void;
}

interface State {
  configured: boolean;
}

export class ApolloClientStorybookWrapper extends React.Component<ApolloClientStorybookWrapperProps, State> {
  public readonly state: Readonly<State> = { configured: false };

  public async componentDidMount() {
    const {
      client,
      configureClient,
    } = this.props;

    await client.resetStore();
    await client.cache.reset();
    client.writeData({ data: { ui: uiState.defaults.ui } });

    if (configureClient instanceof Function) {
      configureClient();
    }

    this.setState({ configured: true });
  }

  public render() {
    if (!this.state.configured) {
      return <h1>Please wait, configuring Apollo </h1>;
    }

    return this.props.children;
  }
}
