import { action } from '@storybook/addon-actions';
import * as React from 'react';

import { NewAnalyticsContext } from 'common/analytics';

export const createStorybookAnalyticsClient = () => ({
  sendUIEvent: action('[Analytics.sendUIEvent]'),
  sendScreenEvent: action('[Analytics.sendScreenEvent]'),
  sendTrackEvent: action('[Analytics.sendTrackEvent]'),
  init: () => null,
});

export class StorybookAnalyticsClientProvider extends React.Component {
  public render() {
    return (
      <NewAnalyticsContext.Provider value={createStorybookAnalyticsClient()}>
        {this.props.children}
      </NewAnalyticsContext.Provider>
    );
  }
}
