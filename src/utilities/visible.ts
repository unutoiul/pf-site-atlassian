const getOffsetTop = (elem: HTMLElement): number => {

  const boundingBoxTop = elem.getBoundingClientRect().top;
  const clientTop = document.documentElement.clientTop;

  return boundingBoxTop + window.pageYOffset - clientTop;
};

export const isElementVisible = (element: HTMLElement, container: HTMLElement): boolean => {
  const containerHeight = container.offsetHeight;
  const elemTop = getOffsetTop(element) - getOffsetTop(container);
  const elemBottom = elemTop + element.offsetHeight;

  return 0 <= elemTop && elemBottom < containerHeight;
};
