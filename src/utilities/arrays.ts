/**
 * Use this for filtering arrays with potentially null items, e.g.
 * const withoutNulls = arrayWithNulls.filter(nonNull);
 *
 * @param {(T | null | undefined)} item the item to check for null or undefined value
 * @returns {boolean} indicator of item actuality
 */
export function nonNullOrUndefined<T>(item: T | null | undefined): item is T {
  return item != null;
}
