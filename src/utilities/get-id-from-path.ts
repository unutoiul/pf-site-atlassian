export function getCloudIdFromPath(pathname: string): string | undefined {
  return (pathname.match(/\/s\/(.*?)(\/|$)/i) || [])[1];
}

export function getOrgIdFromPath(pathname: string): string | undefined {
  return (pathname.match(/\/o\/(.*?)(\/|$)/i) || [])[1];
}
