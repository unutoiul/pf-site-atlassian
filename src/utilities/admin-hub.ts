const adminHubContext: boolean = window.location.pathname.indexOf('/admin') !== 0;

export const util = {
  isAdminHub: (): boolean => adminHubContext,
  siteAdminBasePath: adminHubContext ? '' : '/admin',
};
