
export const util = {
  /**
   * A consistent way to perform redirects across the application
   *
   * @export
   * @param {string} path the path to redirect app to
   * @returns {void}
   */
  redirect: (path: string): void => {
    window.location.href = path;
  },
};
