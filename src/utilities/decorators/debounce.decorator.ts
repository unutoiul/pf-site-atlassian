import * as lodashDebounce from 'lodash.debounce';

export type Trigger = 'onInputChange';

const triggerToDelayMap: { [P in Trigger]: number; } = {
  onInputChange: 500,
};

export function debounce(trigger: Trigger): MethodDecorator {
  return (target: any, key: PropertyKey, descriptor: PropertyDescriptor): PropertyDescriptor => {
    if (descriptor === undefined) {
      descriptor = Object.getOwnPropertyDescriptor(target, key)!;
    }
    const originalMethod = descriptor.value;

    descriptor.value = lodashDebounce(originalMethod, triggerToDelayMap[trigger]);

    return descriptor;
  };
}
