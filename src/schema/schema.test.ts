import { assert, expect } from 'chai';
import * as fetchMock from 'fetch-mock';
import { sandbox as sinonSandbox, SinonSandbox } from 'sinon';

import { getConfig } from 'common/config';
import { ValidationError } from 'common/error';

import { apiFacade, CurrentUser } from '../schema/api';
import { mockLocalStorage, MockLocalStorage } from '../utilities/testing';
import { combinedResolvers as resolvers } from './resolvers';
import { Member } from './schema-types';

describe('GQL schema', () => {
  let sandbox: SinonSandbox;
  beforeEach(() => {
    sandbox = sinonSandbox.create();
  });
  afterEach(() => {
    fetchMock.restore();
    apiFacade.reset();
    sandbox.reset();
  });

  describe('resolvers', () => {
    describe('submitFeedback', () => {
      it('POSTs correct information', async () => {
        const spy = fetchMock.post('http://localhost:3002/api/feedback', {
          status: 201,
        });
        const comment = 'comment';
        const email = 'email';
        const webInfo = 'foo';
        await resolvers.Mutation.submitFeedback(null, { comment, email, webInfo });
        expect(spy.lastCall()[1].body).to.equal(JSON.stringify({
          collectorId: '3183179d',
          data: {
            description: comment,
            summary: comment,
            email,
            webInfo,
          },
        }));
      });

      it('rejects if request unsuccessful', async () => {
        fetchMock.post('http://localhost:3002/api/feedback', {
          status: 500,
        });
        const comment = 'comment';
        const email = 'email';
        const webInfo = 'foo';
        try {
          await resolvers.Mutation.submitFeedback(null, { comment, email, webInfo });
          assert.fail();
        } catch (e) {
          expect(e.message).to.equal('feedback submission failed: 500');
        }
      });
    });

    it('should delegate currentUser resolution to CurrentUser object', async () => {
      const testUserObj = { test: 123 };
      sandbox.stub(CurrentUser.prototype, 'getInfoOrThrow').returns(Promise.resolve(testUserObj));

      const response = await resolvers.Query.currentUser();
      expect(response).to.deep.equal(testUserObj);
    });

    describe('CurrentUser', () => {
      describe('isSiteAdmin', () => {
        it('returns true if the response is ok', async () => {
          fetchMock.get(`/admin/rest/um/1/apps/navigation`, {
            status: 200,
            body: [],
          });
          const response = await resolvers.CurrentUser.isSiteAdmin();
          expect(response).to.be.true();
        });

        it('returns false if the response is 401', async () => {
          fetchMock.get(`/admin/rest/um/1/apps/navigation`, {
            status: 401,
          });
          const response = await resolvers.CurrentUser.isSiteAdmin();
          expect(response).to.be.false();
        });

        it('returns false if the response is 403', async () => {
          fetchMock.get(`/admin/rest/um/1/apps/navigation`, {
            status: 403,
          });
          const response = await resolvers.CurrentUser.isSiteAdmin();
          expect(response).to.be.false();
        });
      });

      describe('organizations', () => {
        it('returns all orgs when flag is set', async () => {
          fetchMock.get(`${getConfig().orgUrl}/my`, {
            status: 200,
            body: [
              { id: 'foo' },
              { id: 'bar' },
            ],
          });

          const response = await resolvers.CurrentUser.organizations({});
          expect(response).to.deep.equal([{ id: 'foo' }, { id: 'bar' }]);
        });

      });

    });

    describe('Organization', () => {
      describe('admins', () => {
        it('returns default if the response is not ok', async () => {
          const orgId = 'FAKE';
          const body = {
            id: 'FAKE',
            total: 0,
            users: [],
          };
          fetchMock.get(`${getConfig().orgUrl}/${orgId}/admin`, { status: 500, body });
          const response = await resolvers.OrganizationUsers.admins({ id: orgId });
          expect(response).to.deep.equal(body);
        });
        it('returns response if response is ok', async () => {
          const orgId = 'FAKE';
          const body = {
            id: 'FAKE',
            total: 0,
            users: [{ displayName: 'Emma', emails: [{ primary: true, value: 'acme@acme.com' }] } as Member],
          };
          fetchMock.get(`${getConfig().orgUrl}/${orgId}/admin`, { status: 200, body });
          const response = await resolvers.OrganizationUsers.admins({ id: orgId });
          expect(response).to.deep.equal(body);
        });
      });
      describe('saml', () => {
        it('returns object if the response is ok', async () => {
          const orgId = 'FAKE';
          const body = {
            issuer: 'https://fake.com/issuer',
            ssoUrl: 'https://fake.com/sso-url',
            publicCertificate: '-----BEGIN CERTIFICATE-----\nfoobarbaz==\n-----END CERTIFICATE-----',
            serviceProvider: {
              entityId: 'https://id.atlassian.com/login',
              acsUrl: 'https://id.atlassian.com/login/saml/acs',
            },
          };
          fetchMock.get(`${getConfig().orgUrl}/${orgId}/saml`, {
            status: 200,
            body,
          });
          const response = await resolvers.OrganizationSecurity.saml({ id: orgId });
          expect(response).to.deep.equal(body);
        });
        it('returns null if the response is 404', async () => {
          const orgId = 'FAKE';
          fetchMock.get(`${getConfig().orgUrl}/${orgId}/saml`, {
            status: 404,
          });
          const response = await resolvers.OrganizationSecurity.saml({ id: orgId });
          expect(response).to.equal(null);
        });
        it('throw if the response is not ok for any other reason', async () => {
          const orgId = 'FAKE';
          fetchMock.get(`${getConfig().orgUrl}/${orgId}/saml`, {
            status: 500,
          });
          await resolvers.OrganizationSecurity.saml({ id: orgId }).catch(error => {
            expect(error.status).to.equal(500);
          });
        });
      });
    });
    describe('CurrentSite', () => {
      describe('Id', () => {
        it('should return correct cloud id if the cloud id fetch succeeds', async () => {
          fetchMock.get('/_edge/tenant_info', {
            status: 200,
            body: {
              cloudId: 'retrieved-cloud-id',
            },
          });

          const response = await resolvers.Query.currentSite();
          expect(response.id).to.equal('retrieved-cloud-id');
        });

        it('should throw error if the request fails', async () => {
          fetchMock.get('/_edge/tenant_info', {
            status: 500,
          });

          await resolvers.Query.currentSite().catch(error => {
            expect(error.status).to.equal(500);
            expect(error.message).to.equal('Fetch request failed with status 500 for hostname-site-id');
          });
        });
      });
      describe('SiteAccess', () => {
        const id = 'fake';
        it('returns empty object if the response is not ok', async () => {
          fetchMock.get(`${getConfig().umBasePath}/${id}/signup-options`, {
            status: 500,
          });
          const response = await resolvers.Site.siteAccess({ id });
          expect(response).to.deep.equal({});
        });

        it('returns site access configuration if the response is ok', async () => {
          const body = {
            signupEnabled: true,
            openInvite: false,
            domains: ['foo.com'],
            notifyAdmin: true,
          };
          fetchMock.get(`${getConfig().umBasePath}/${id}/signup-options`, {
            status: 200,
            body,
          });
          const response = await resolvers.Site.siteAccess({ id });
          expect(response).to.deep.equal(body);
        });
      });
      describe('DefaultApps', () => {
        const id = 'fake';
        it('returns empty array if the response is not ok', async () => {
          fetchMock.get(`${getConfig().umBasePath}/${id}/product/defaults`, {
            status: 500,
          });
          const response = await resolvers.Site.defaultApps({ id });
          expect(response).to.deep.equal([]);
        });
        it('returns default product access if the response is ok', async () => {
          const body = {
            products: [
              {
                productId: 'product:jira:jira-software',
                productName: 'Jira Software',
                canonicalProductKey: 'jira-software',
              },
            ],
          };
          fetchMock.get(`${getConfig().umBasePath}/${id}/product/defaults`, {
            status: 200,
            body,
          });
          const response = await resolvers.Site.defaultApps({ id });
          expect(response).to.deep.equal(body.products);
        });
      });
    });
    describe('Mutation', () => {
      describe('updateSaml', () => {
        const orgId = 'FAKE';
        const body = {
          id: orgId,
          issuer: 'https://fake.com/issuer',
          ssoUrl: 'https://fake.com/sso-url',
          publicCertificate: '-----BEGIN CERTIFICATE-----\nfoobarbaz==\n-----END CERTIFICATE-----',
          auth0MigrationState: 'AUTH0FORCED' as any,
        };

        it('returns object if the response is ok', async () => {
          fetchMock.put(`${getConfig().orgUrl}/${orgId}/saml`, {
            status: 204,
          });
          const response = await resolvers.Mutation.updateSaml(null, { input: body });
          expect(response).to.be.true();
        });
        it('throw if the response is not ok', async () => {
          fetchMock.put(`${getConfig().orgUrl}/${orgId}/saml`, {
            status: 500,
          });
          await resolvers.Mutation.updateSaml(null, { input: body }).catch(error => {
            expect(error.status).to.equal(500);
            expect(error.title).to.equal('Problem saving the SAML configuration');
          });
        });
        it('throw different error if the response is 400', async () => {
          fetchMock.put(`${getConfig().orgUrl}/${orgId}/saml`, {
            status: 400,
          });
          await resolvers.Mutation.updateSaml(null, { input: body }).catch(error => {
            expect(error.status).to.equal(400);
            expect(error.title).to.equal('Invalid SAML configuration');
            expect(error.description).to.equal('Please make sure the Entity ID, SSO URL and Certificate are correct.');
          });
        });
      });
      describe('deleteSaml', () => {
        const orgId = 'FAKE';

        it('returns true if the response is ok', async () => {
          fetchMock.delete(`${getConfig().orgUrl}/${orgId}/saml`, {
            status: 204,
          });
          const response = await resolvers.Mutation.deleteSaml(null, { id: orgId });
          expect(response).to.be.true();
        });
        it('throw if the response is not ok', async () => {
          fetchMock.delete(`${getConfig().orgUrl}/${orgId}/saml`, {
            status: 400,
          });
          await resolvers.Mutation.deleteSaml(null, { id: orgId }).catch(error => {
            expect(error.status).to.equal(400);
            expect(error.title).to.equal('Problem deleting the SAML configuration');
          });
        });
      });
      describe('updateEmojiSettings', () => {
        const id = 'FAKE';

        it('returns uploadEnabled as false for ok response to delete request', async () => {
          fetchMock.delete(`${getConfig().apiUrl}/emoji/${id}/site/settings/uploadEnabled`, {
            status: 204,
          });
          const response = await resolvers.Mutation.updateEmojiSettings(null, { id, input: { uploadEnabled: false } });
          expect(response).to.deep.equal({ id, uploadEnabled: false });
        });
        it('returns uploadEnabled as true for ok response to put request', async () => {
          fetchMock.put(`${getConfig().apiUrl}/emoji/${id}/site/settings/uploadEnabled`, {
            status: 204,
          });
          const response = await resolvers.Mutation.updateEmojiSettings(null, { id, input: { uploadEnabled: true } });
          expect(response).to.deep.equal({ id, uploadEnabled: true });
        });
        it('returns the previous uploadEnabled setting if response is not ok', async () => {
          fetchMock.delete(`${getConfig().apiUrl}/emoji/${id}/site/settings/uploadEnabled`, {
            status: 400,
          });
          const response = await resolvers.Mutation.updateEmojiSettings(null, { id, input: { uploadEnabled: false } });
          expect(response).to.deep.equal({ id, uploadEnabled: true });
        });
      });
      describe('evaluateIdentityManager', () => {
        const orgId = 'FAKE';

        it('throws an error with a skipped flag if the response is not ok', async () => {
          fetchMock.put(`${getConfig().orgUrl}/${orgId}/product/identity-manager`, {
            status: 500,
          });
          await resolvers.Mutation.evaluateIdentityManager(null, { orgId }).catch(error => {
            expect(error.status).to.equal(500);
            expect(error.legacySkipDefaultErrorFlag).to.be.true();
          });
        });
        it('throws a Validation error if the response returns a 400', async () => {
          fetchMock.put(`${getConfig().orgUrl}/${orgId}/product/identity-manager`, {
            status: 400,
            body: {},
          });
          await resolvers.Mutation.evaluateIdentityManager(null, { orgId }).catch(error => {
            expect(error).to.be.an.instanceof(ValidationError);
            expect(error.ignore).to.be.true();
            expect((error as ValidationError).code).to.equal('lastNameRequired');
          });
        });
      });
      describe('updateSiteAccess', () => {
        const id = 'fake';
        const input = { domains: ['foo.com'], notifyAdmin: true, openInvite: false, signupEnabled: true };

        it('returns true if the response is ok', async () => {
          fetchMock.put(`${getConfig().umBasePath}/${id}/signup-options`, {
            status: 204,
          });
          const response = await resolvers.Mutation.updateSiteAccess(null, { id, input });
          expect(response).to.be.true();
        });
        it('throws if the response is not ok', async () => {
          fetchMock.put(`${getConfig().umBasePath}/${id}/signup-options`, {
            status: 500,
          });
          await resolvers.Mutation.updateSiteAccess(null, { id, input }).catch(error => {
            expect(error.status).to.equal(500);
            expect(error.title).to.equal('Problem saving the site access configuration');
          });
        });
      });
    });
    describe('Onboarding', () => {

      describe('Query', () => {
        let mockedLocalStorage: MockLocalStorage;

        beforeEach(() => {
          mockedLocalStorage = mockLocalStorage();
        });

        afterEach(() => {
          mockedLocalStorage.restore();
        });

        it('should be true if service has the right value', async () => {
          fetchMock.get(`${getConfig().apiUrl}/flag/my?flagKey=admin.onboarding.test`, { status: 200, body: { status: true } });

          const result = await resolvers.Query.onboarding(null, { id: 'test' });
          expect(result.dismissed).to.be.true();
        });

        it('should be true if service returns 404', async () => {
          fetchMock.get(`${getConfig().apiUrl}/flag/my?flagKey=admin.onboarding.test`, { status: 404 });

          const result = await resolvers.Query.onboarding(null, { id: 'test' });
          expect(result.dismissed).to.be.true();
        });

        it('should be true if service returns 500', async () => {
          fetchMock.get(`${getConfig().apiUrl}/flag/my?flagKey=admin.onboarding.test`, { status: 500 });

          const result = await resolvers.Query.onboarding(null, { id: 'test' });
          expect(result.dismissed).to.be.true();
        });

        it('should be false if service returns false', async () => {
          fetchMock.get(`${getConfig().apiUrl}/flag/my?flagKey=admin.onboarding.test`, { status: 200, body: { status: false } });

          const result = await resolvers.Query.onboarding(null, { id: 'test' });
          expect(result.dismissed).to.be.false();
        });
      });

      describe('Mutation', () => {
        it('should call onboarding service when mutation is called', async () => {
          fetchMock.post(`${getConfig().apiUrl}/flag/my`, { status: 200, body: { status: true } }, { name: 'dismiss' });
          resolvers.Mutation.dismissOnboarding(null, { id: 'test' });
          expect(fetchMock.called('dismiss')).to.be.true();
        });
      });
    });
    describe('updateMfaExemption', () => {
      const id = 'FAKE';
      const member = 'AAID';
      const body = (exempt) => {
        return {
          id: 'FAKE',
          member: 'AAID',
          exempt,
        };
      };

      it('returns true when enabling exemption if the response is ok', async () => {
        fetchMock.put(`${getConfig().orgUrl}/${id}/mfa/${member}/exempt`, {
          status: 204,
        });
        const response = await resolvers.Mutation.updateMfaExemption(null, { input: body(true) });
        expect(response).to.be.true();
      });
      it('returns true when disabling exemption if response is ok', async () => {
        fetchMock.delete(`${getConfig().orgUrl}/${id}/mfa/${member}/exempt`, {
          status: 204,
        });
        const response = await resolvers.Mutation.updateMfaExemption(null, { input: body(false) });
        expect(response).to.be.true();
      });
      it('throw if the response is not ok', async () => {
        fetchMock.put(`${getConfig().orgUrl}/${id}/mfa/${member}/exempt`, {
          status: 400,
        });
        await resolvers.Mutation.updateMfaExemption(null, { input: body(true) }).catch(error => {
          expect(error.status).to.equal(400);
        });
      });
    });
    describe('updateMfaEnrollment', () => {
      const id = 'FAKE';
      const member = 'AAID';
      const body = {
        id: 'FAKE',
        member: 'AAID',
      };

      it('returns true if the response is ok', async () => {
        fetchMock.delete(`${getConfig().orgUrl}/${id}/members/${member}/mfa`, {
          status: 204,
        });
        const response = await resolvers.Mutation.updateMfaEnrollment(null, { input: body });
        expect(response).to.be.true();
      });
      it('throw if the response is not ok', async () => {
        fetchMock.delete(`${getConfig().orgUrl}/${id}/members/${member}/mfa`, {
          status: 400,
        });
        await resolvers.Mutation.updateMfaEnrollment(null, { input: body }).catch(error => {
          expect(error.status).to.equal(400);
        });
      });
    });
    describe('renameOrganization', () => {
      const orgId = 'FAKE';
      const name = 'New awesome Org name';

      it('returns id and name if the response is ok', async () => {
        fetchMock.put(`${getConfig().orgUrl}/${orgId}/rename`, {
          status: 204,
        });
        const response = await resolvers.Mutation.renameOrganization(null, { id: orgId, name });
        expect(response).to.deep.equal({ id: orgId, name });
      });
      it('throw if the response is not ok', async () => {
        fetchMock.put(`${getConfig().orgUrl}/${orgId}/rename`, {
          status: 400,
        });
        await resolvers.Mutation.renameOrganization(null, { id: orgId, name }).catch(error => {
          expect(error.status).to.equal(400);
        });
      });
    });
  });
  describe('removeOrganizationAdmin', () => {
    const orgId = 'FAKE';
    const adminId = 'FAKE_ADMIN_ID';

    it('returns true if the response is ok', async () => {
      fetchMock.delete(`${getConfig().orgUrl}/${orgId}/admin/${adminId}`, {
        status: 204,
      });
      const response = await resolvers.Mutation.removeOrganizationAdmin(null, { orgId, adminId });
      expect(response).to.be.true();
    });
    it('throw if the response is not ok', async () => {
      fetchMock.delete(`${getConfig().orgUrl}/${orgId}/admin/${adminId}`, {
        status: 400,
      });
      await resolvers.Mutation.removeOrganizationAdmin(null, { orgId, adminId }).catch(error => {
        expect(error.status).to.equal(400);
        expect(error.description).to.equal('The organization admin could not be removed. Please try again later.');
      });
    });
  });
  describe('membersList', () => {
    const id = 'FAKE';

    it('returns an empty list if no aaIds', async () => {
      const response = await resolvers.Query.membersList(null, { id, aaIds: [] });
      expect(response).to.deep.equal([]);
    });
    it('throws if response is not ok', async () => {
      fetchMock.post(`${getConfig().apiUrl}/directory/graphql`, {
        status: 400,
      });
      await resolvers.Query.membersList(null, { id, aaIds: [] }).catch(error => {
        expect(error.status).to.equal(400);
        expect(error.title).to.equal('Could not retrieve members list');
      });
    });
    it('returns an empty list if invalid json', async () => {
      fetchMock.post(`${getConfig().apiUrl}/directory/graphql`, {
        status: 200,
        body: {},
      });
      const response = await resolvers.Query.membersList(null, { id, aaIds: ['test'] });
      expect(response).to.deep.equal([]);
    });
  });

});
