import { GSuite as GSuiteModel } from '../api';
import { GetConnectionMutationArgs, GSuiteConnection } from '../schema-types/schema-types';

export const resolver = {
  GSuite: {
    state: async ({ id }) => {
      return GSuiteModel.create(id).getState();
    },
    groups: async ({ id }) => {
      return GSuiteModel.create(id).getGroups();
    },
  },
  Mutation: {
    getConnection: async (_, { id }: GetConnectionMutationArgs): Promise<GSuiteConnection> => {
      return GSuiteModel.create(id).getConnection();
    },
  },
};
