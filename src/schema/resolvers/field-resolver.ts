export type FieldResolver<TContainer, TSchemaType, TKeys extends keyof TSchemaType = keyof TSchemaType> = {
  [P in TKeys]: (
    container: TContainer,
    queryArguments: object,
  ) => Promise<TSchemaType[P]>;
};
