import { getConfig } from 'common/config';
import { RequestError } from 'common/error';

import { fetchGetOptions } from '../../utilities/schema';

export const resolver = {
  Mutation: {
    updateMfaExemption: async (_, { input: { id, member, exempt } }): Promise<boolean> => {
      const response = await fetch(`${getConfig().orgUrl}/${id}/mfa/${member}/exempt`, {
        method: exempt ? 'PUT' : 'DELETE',
        ...fetchGetOptions,
      });

      if (!response.ok) {
        throw new RequestError({
          status: response.status,
          title: 'Two-step verification exemption could not be updated.',
        });
      }

      return true;
    },
    updateMfaEnrollment: async (_, { input: { id, member } }): Promise<boolean> => {
      const response = await fetch(`${getConfig().orgUrl}/${id}/members/${member}/mfa`, {
        method: 'DELETE',
        ...fetchGetOptions,
      });

      if (!response.ok) {
        throw new RequestError({
          status: response.status,
          title: 'Two-step verification enrollment could not be updated.',
        });
      }

      return true;
    },
  },
};
