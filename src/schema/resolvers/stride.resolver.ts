import { getConfig } from 'common/config';
import { RequestError } from 'common/error';

import { fetchGetOptions, mutateOptions } from '../../utilities/schema';
import { CreateStrideExportInput, StrideExport, StrideExportStatus } from '../schema-types';

interface StrideExportResponse {
  job_id: string;
  status: StrideExportStatus;
  created_at: string;
  started_at?: string;
  expiration_date?: string;
  job_settings: {
    start_date?: string;
    end_date?: string;
    include_public_rooms: boolean;
    include_private_rooms: boolean;
    include_directs: boolean;
  };
  user_details: {
    emails: string[];
    name: string;
  };
  download_location?: string;
  integrations_location?: string;
}

interface StrideExportsResponse {
  jobs: StrideExportResponse[];
}

interface CreateStrideExportPayload {
  start_date?: string;
  end_date?: string;
  include_public_rooms: boolean;
  include_private_rooms: boolean;
  include_directs: boolean;
  passphrase: string;
}

function normalizeExportResponse(responseExport: StrideExportResponse): StrideExport {
  return {
    id: responseExport.job_id,
    status: responseExport.status,
    startedAt: responseExport.started_at || responseExport.created_at,
    ...(responseExport.expiration_date ? { expiresAt: responseExport.expiration_date } : {}),
    creatorName: responseExport.user_details.name,
    creatorEmail: responseExport.user_details.emails[0],
    settings: {
      ...(responseExport.job_settings.start_date ? {
        startDate: responseExport.job_settings.start_date,
      } : {}),
      ...(responseExport.job_settings.end_date ? {
        endDate: responseExport.job_settings.end_date,
      } : {}),
      includePublicRooms: responseExport.job_settings.include_public_rooms,
      includePrivateRooms: responseExport.job_settings.include_private_rooms,
      includeDirects: responseExport.job_settings.include_directs,
    },
    ...(responseExport.download_location ? {
      downloadLocation: responseExport.download_location,
    } : {}),
    ...(responseExport.integrations_location ? {
      integrationsLocation: responseExport.integrations_location,
    } : {}),
  };
}

function normalizeExportsResponse(responseExports: StrideExportResponse[]): StrideExport[] {
  return responseExports.map(normalizeExportResponse).sort((a, b) => {
    if (a.startedAt < b.startedAt) {
      return 1;
    }
    if (a.startedAt > b.startedAt) {
      return -1;
    }

    return 0;
  });
}

function normalizeCreateExportInput(input: CreateStrideExportInput): CreateStrideExportPayload {
  return {
    ...(input.startDate ? { start_date: input.startDate } : {}),
    ...(input.endDate ? { end_date: input.endDate } : {}),
    include_public_rooms: input.includePublicRooms,
    include_private_rooms: input.includePrivateRooms,
    include_directs: input.includeDirects,
    passphrase: input.passphrase,
  };
}

export const resolver = {
  Stride: {
    strideExports: async ({ cloudId }) => {
      const requestOptions: RequestInit = {
        ...fetchGetOptions,
        credentials: 'include',
      };
      const response = await fetch(`${getConfig().strideUrl}/${cloudId}/export`, requestOptions);

      if (!response.ok) {
        throw new RequestError({
          status: response.status,
          title: 'Could not fetch exports',
          description: 'There was a problem fetching the exports. Please try again in a few minutes.',
        });
      }

      const json: StrideExportsResponse = await response.json();

      return normalizeExportsResponse(json.jobs);
    },
    strideExport: async ({ cloudId }, { exportId }) => {
      const requestOptions: RequestInit = {
        ...fetchGetOptions,
        credentials: 'include',
      };
      const response = await fetch(`${getConfig().strideUrl}/${cloudId}/export/${exportId}`, requestOptions);

      if (!response.ok) {
        throw new RequestError({
          status: response.status,
          title: 'Could not fetch export',
          description: `There was a problem fetching export ${exportId}. Please try again in a few minutes.`,
        });
      }

      const json: StrideExportResponse = await response.json();

      return normalizeExportResponse(json);
    },
  },
  Query: {
    stride: async (_, { cloudId }) => {
      return { cloudId };
    },
  },
  Mutation: {
    deleteStrideExport: async (_, { cloudId, exportId }) => {
      const requestOptions: RequestInit = {
        method: 'DELETE',
        credentials: 'include',
      };

      const response = await fetch(`${getConfig().strideUrl}/${cloudId}/export/${exportId}`, requestOptions);

      if (!response.ok) {
        throw new RequestError({ status: response.status, title: 'Could not delete export' });
      }

      return true;
    },
    cancelStrideExport: async (_, { cloudId, exportId }) => {
      const requestOptions: RequestInit = {
        method: 'POST',
        credentials: 'include',
      };

      const response = await fetch(`${getConfig().strideUrl}/${cloudId}/export/${exportId}/cancel`, requestOptions);

      if (!response.ok) {
        throw new RequestError({ status: response.status, title: 'Could not cancel export' });
      }

      return true;
    },
    createStrideExport: async (_, { cloudId, input }: { cloudId: string, input: CreateStrideExportInput }): Promise<StrideExport> => {
      const requestOptions: RequestInit = {
        ...mutateOptions,
        method: 'POST',
        credentials: 'include',
        body: JSON.stringify(normalizeCreateExportInput(input)),
      };

      let response = await fetch(`${getConfig().strideUrl}/${cloudId}/export`, requestOptions);
      if (__NODE_ENV__ === 'development') {
        // The current mocking functionality does not support
        // mocking the same endpoint for multiple methods,
        // so this response is mocked here instead.
        response = {
          ok: true,
          json(): StrideExportResponse {
            return {
              job_id: 'abcdef-ghij-klmn-opqrst',
              created_at: new Date().toISOString(),
              started_at: new Date().toISOString(),
              expiration_date: new Date().toISOString(),
              status: 'pending',
              job_settings: {
                start_date: new Date().toISOString(),
                end_date: new Date().toISOString(),
                include_public_rooms: input.includePublicRooms,
                include_private_rooms: input.includePrivateRooms,
                include_directs: input.includeDirects,
              },
              user_details: {
                emails: ['johnny@las.co'],
                name: 'Johnny Atlas',
              },
            };
          },
          ...{} as any,
        };
      }

      if (!response.ok) {
        throw new RequestError({ status: response.status, title: 'Could not create export' });
      }

      return normalizeExportResponse(await response.json());
    },
  },
};
