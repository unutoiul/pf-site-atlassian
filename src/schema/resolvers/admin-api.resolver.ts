import { AdminApi as AdminApiModel } from '../api/model/admin-api';
import { CreateAdminApiKeyMutationVariables, RevokeAdminApiKeyMutationVariables } from '../schema-types';

export const resolver = {
  Mutation: {
    createAdminApiKey: async (_, args: CreateAdminApiKeyMutationVariables) => {
      return AdminApiModel.init().createAdminApiKey(args.orgId, args.name);
    },
    deleteAdminApiKey: async (_, args: RevokeAdminApiKeyMutationVariables) => {
      await AdminApiModel.init().deleteAdminApiKey(args.orgId, args.apiKeyId);

      return true;
    },
  },
};
