import { Site } from '../api';

export const resolver = {
  Mutation: {
    renameSite: async (_, { cloudId, input: { cloudName, cloudNamespace } }) => {
      const response = await Site.create().rename(cloudId, cloudName, cloudNamespace);

      if (response.progressUri) {
        // Update human-friendly display name stored in site admin service
        await Site.create().setDisplayName(cloudId, cloudName);

        // Swarm MVP shortcut: Sleep for arbitrary time to allow for eventual consistancy between Catalogue Service and TCS. 3 seconds should be sufficient for P99.
      // More permanent solution tracked here: https://product-fabric.atlassian.net/browse/PTC-1532
        await new Promise(resolve => setTimeout(resolve, 3000));
      }

      return response;
    },
  },
};
