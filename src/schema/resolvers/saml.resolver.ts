import { getConfig } from 'common/config';
import { RequestError } from 'common/error';

import { DeleteSamlMutationVariables, UpdateSamlMutationVariables } from '../../schema/schema-types';
import { fetchGetOptions } from '../../utilities/schema';

export const resolver = {
  Mutation: {
    updateSaml: async (_, { input: { id, issuer, ssoUrl, publicCertificate, auth0MigrationState } }: UpdateSamlMutationVariables) => {
      const response = await fetch(`${getConfig().orgUrl}/${id}/saml`, {
        ...fetchGetOptions,
        method: 'PUT',
        body: JSON.stringify({
          issuer,
          ssoUrl,
          publicCertificate,
          auth0MigrationState,
        }),
      });

      if (!response.ok) {
        if (response.status === 400) {
          throw new RequestError({
            status: response.status,
            title: 'Invalid SAML configuration',
            description: 'Please make sure the Entity ID, SSO URL and Certificate are correct.',
          });
        } else {
          throw new RequestError({ status: response.status, title: 'Problem saving the SAML configuration' });
        }
      }

      return true;
    },
    deleteSaml: async (_, { id }: DeleteSamlMutationVariables) => {
      const response = await fetch(`${getConfig().orgUrl}/${id}/saml`, {
        ...fetchGetOptions,
        method: 'DELETE',
      });

      if (!response.ok) {
        throw new RequestError({ status: response.status, title: 'Problem deleting the SAML configuration' });
      }

      return true;
    },
  },
};
