import { getConfig } from 'common/config';

import { fetchGetOptions } from '../../utilities/schema';
import { Organization } from '../api';

export const resolver = {
  Query: {
    products: async (_, args) => {
      const response = await fetch(`${getConfig().orgUrl}/${args.id}/product`, fetchGetOptions);
      if (!response.ok) {
        throw new Error('Products could not be evaluated');
      }

      const result = await response.json();

      return {
        id: args.id,
        identityManager: !!result.find(p => p === 'identity-manager'),
      };
    },
  },
  Mutation: {
    evaluateIdentityManager: async (_, { orgId }) => {
      await Organization.init().evaluateAtlassianAccess(orgId);

      return {
        id: orgId,
        identityManager: true,
      };
    },
  },
};
