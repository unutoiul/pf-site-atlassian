import { expect } from 'chai';
import * as fetchMock from 'fetch-mock';
import { sandbox as sinonSandbox, SinonSandbox } from 'sinon';

import { getConfig } from 'common/config';

import { trustedUsersFeatureFlagKey } from '../../site/feature-flags/flag-keys';
import { apiFacade, RolePermissions } from '../api';
import { combinedResolvers as resolvers } from '../resolvers';

describe('Role Permissions Resolver', () => {

  let sandbox: SinonSandbox;
  let mockApiFacade;
  let mockRolePermissions;

  beforeEach(() => {
    sandbox = sinonSandbox.create();
    mockApiFacade = {
      getFeatureFlag: sandbox.stub(),
    };
    sandbox.stub(apiFacade, 'identityFeatureFlagClient').value(mockApiFacade);
    mockRolePermissions = {
      queryRole: sandbox.stub(),
    };
  });

  afterEach(() => {
    sandbox.reset();
    sandbox.restore();
    afterEach(fetchMock.restore);
  });

  const roleId = 'trustworthy-people';
  const cloudId = 'acme-company-id';

  describe('rolePermissions', () => {
    describe('checking response values', () => {
      describe('queryRole', () => {
        it('should return empty permissionId array if the response is not ok', async () => {
          mockApiFacade.getFeatureFlag.resolves({
            [trustedUsersFeatureFlagKey]: false,
          });
          fetchMock.post(`${getConfig().xflowBasePath}/${cloudId}/query-role`, {
            status: 500,
          });
          const response = await resolvers.Query.rolePermissions(null, { cloudId, roleId });
          expect(response).to.deep.equal({
            permissionIds: [],
          });
        });

        it('should return permissionId array if response is ok', async () => {
          const url = `${getConfig().xflowBasePath}/${cloudId}/query-role`;
          mockApiFacade.getFeatureFlag.resolves({
            [trustedUsersFeatureFlagKey]: true,
          });
          fetchMock.post(url, {
            status: 200,
            body: {
              permissionIds: ['add-products', 'invite-users'],
            },
          });
          const response = await resolvers.Query.rolePermissions(null, { cloudId, roleId });

          expect(response).to.deep.equal({
            permissionIds: ['add-products', 'invite-users'],
          });
        });
      });
    });

    describe('queryRole method based on trusted users flag', () => {
      it('should call with correct params when feature flag is off', async () => {
        mockApiFacade.getFeatureFlag.resolves({
          [trustedUsersFeatureFlagKey]: false,
        });
        fetchMock.post(`${getConfig().xflowBasePath}/${cloudId}/query-role`, {
          status: 200,
        });
        sandbox.stub(RolePermissions, 'create').returns(mockRolePermissions);

        await resolvers.Query.rolePermissions(null, { cloudId, roleId });
        expect(mockRolePermissions.queryRole.calledOnce).equals(true);
        expect(mockRolePermissions.queryRole.calledWith(roleId, true)).to.equal(true);
      });

      it('should call with correct params when feature flag is on', async () => {
        mockApiFacade.getFeatureFlag.resolves({
          [trustedUsersFeatureFlagKey]: true,
        });
        fetchMock.post(`${getConfig().xflowBasePath}/${cloudId}/query-role`, {
          status: 200,
        });
        sandbox.stub(RolePermissions, 'create').returns(mockRolePermissions);

        await resolvers.Query.rolePermissions(null, { cloudId, roleId });
        expect(mockRolePermissions.queryRole.calledOnce).equals(true);
        expect(mockRolePermissions.queryRole.calledTwice).equals(false);
        expect(mockRolePermissions.queryRole.calledWith(roleId, false)).to.equal(true);
      });
    });
  });
});
