import { expect } from 'chai';
import { sandbox as sinonSandbox, SinonSandbox, SinonStub } from 'sinon';

import { Cads, Jira } from '../api';
import { ServiceDeskCustomerDirectoryShape } from '../api/client/cads-client';
import { JsdCustomer } from '../schema-types';
import { resolver } from './jsd.resolver';

describe('Jsd Resolver', () => {
  const cloudId = 'cloudId';

  const sandbox: SinonSandbox = sinonSandbox.create();
  let mockCads: {
    getDirectoryId: SinonStub,
    getCustomerExport: SinonStub,
    updateCustomerAccess: SinonStub,
    updateCustomerName: SinonStub,
    deleteCustomer: SinonStub,
  };
  let mockJira: {
    getJsdCustomers: SinonStub,
    migrateToAtlassianAccount: SinonStub,
  };

  beforeEach(() => {
    mockCads = {
      getDirectoryId: sandbox.stub(),
      getCustomerExport: sandbox.stub(),
      updateCustomerName: sandbox.stub(),
      deleteCustomer: sandbox.stub(),
      updateCustomerAccess: sandbox.stub(),
    };
    mockJira = {
      getJsdCustomers: sandbox.stub(),
      migrateToAtlassianAccount: sandbox.stub(),
    };
    sandbox.stub(Cads, 'create').returns(mockCads);
    sandbox.stub(Jira, 'create').returns(mockJira);
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('jsd resolver', () => {
    it('should return the list of customers for the customers field', async () => {
      const mockResponse: JsdCustomer[] = [{
        id: 'a',
        active: true,
        displayName: 'test',
        email: 'test@example.com',
        username: 'test,',
      }];
      mockJira.getJsdCustomers.resolves(mockResponse);

      const result = await resolver.Jsd.customers({ id: cloudId }, { start: 0, count: 1 });
      expect(result).to.equal(mockResponse);
    });

    it('should return the directory id for the directory field', async () => {
      const mockResponse: ServiceDeskCustomerDirectoryShape = {
        serviceDeskCustomerDirectoryId: 'test-directory-id',
      };
      mockCads.getDirectoryId.resolves(mockResponse);

      const result = await resolver.Jsd.directory({ id: cloudId });
      expect(result).to.deep.equal({
        id: 'test-directory-id',
      });
    });

    it('should return the customer export for the customerExport field', async () => {
      const mockResponse: string = `username,full_name,email,active,last_login`;
      mockCads.getCustomerExport.resolves(mockResponse);

      const result = await resolver.Directory.customerExport({ id: cloudId }, { errorRedirectUrl: 'https://example.com' });
      expect(result).to.equal(mockResponse);
    });

    it('should update the customer account on updateJsdCustomerAccess mutation', async () => {
      mockCads.updateCustomerAccess.resolves(undefined);

      await resolver.Mutation.updateJsdCustomerAccess({ id: cloudId }, {
        directoryId: 'dummy-directory',
        accountId: 'dummy-account',
        access: false,
      });

      expect(mockCads.updateCustomerAccess.getCall(0).args).to.deep.equal([
        'dummy-directory',
        'dummy-account',
        false,
      ]);
    });

    it('should update the customer account on updateJsdCustomerName mutation', async () => {
      mockCads.updateCustomerName.resolves(undefined);

      await resolver.Mutation.updateJsdCustomerName({ id: cloudId }, {
        directoryId: 'dummy-directory',
        accountId: 'dummy-account',
        name: 'Four Tet',
      });

      expect(mockCads.updateCustomerName.getCall(0).args).to.deep.equal([
        'dummy-directory',
        'dummy-account',
        'Four Tet',
      ]);
    });

    it('should migrate the customer account on migrateAccount mutation', async () => {
      mockJira.migrateToAtlassianAccount.resolves(undefined);

      await resolver.Mutation.migrateJsdCustomer({}, { cloudId: 'dummy-cloud-id', accountId: '123' });

      expect(mockJira.migrateToAtlassianAccount.getCall(0).args).to.deep.equal([
        'dummy-cloud-id',
        '123',
      ]);
    });

    it('should delete the customer account on deleteJsdCustomer mutation', async () => {
      mockCads.deleteCustomer.resolves(undefined);

      await resolver.Mutation.deleteJsdCustomer({ id: cloudId }, {
        directoryId: 'dummy-directory',
        accountId: 'dummy-account',
      });

      expect(mockCads.deleteCustomer.getCall(0).args).to.deep.equal([
        'dummy-directory',
        'dummy-account',
      ]);
    });
  });
});
