import { analyticsClient } from 'common/analytics';
import { getConfig } from 'common/config';

import { Organization } from '../../organization/organizations/organization.prop-types';
import { fetchGetOptions } from '../../utilities/schema';

export const resolver = {
  DomainClaim: {
    domains: async ({ id }: Organization) => {
      try {
        const response = await fetch(`${getConfig().orgUrl}/${id}/domainClaims`, fetchGetOptions);
        if (!response.ok) {
          return [];
        }

        const result = await response.json();

        return result.map(claim => ({ ...claim, id: claim.domain }));
      } catch (e) {
        analyticsClient.onError(e);

        return [];
      }
    },
    token: async ({ id }: Organization) => {
      try {
        const response = await fetch(`${getConfig().orgUrl}/${id}/domainClaims/tokenRecord`, fetchGetOptions);
        if (!response.ok) {
          return '';
        }
        const json = await response.json();

        return json.record;
      } catch (e) {
        analyticsClient.onError(e);

        return '';
      }
    },
  },
};
