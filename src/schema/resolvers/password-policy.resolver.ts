import { getConfig } from 'common/config';
import { RequestError } from 'common/error';

import { ResetPasswordsMutationVariables, UpdatePasswordPolicyMutationVariables } from '../../schema/schema-types';
import { fetchGetOptions } from '../../utilities/schema';

export const resolver = {
  Mutation: {
    updatePasswordPolicy: async (_, { input: { id, minimumStrength, expiryDays, resetPasswords } }: UpdatePasswordPolicyMutationVariables) => {
      const body = {
        minimumStrength,
        expiryDays,
        resetPasswords,
      };
      const response = await fetch(`${getConfig().orgUrl}/${id}/passwordpolicy`, {
        ...fetchGetOptions,
        method: 'PUT',
        body: JSON.stringify(body),
      });

      if (!response.ok) {
        throw new RequestError({ status: response.status, title: 'Could not set password policy' });
      }

      return true;
    },
    resetPasswords: async (_, { id }: ResetPasswordsMutationVariables) => {
      const response = await fetch(`${getConfig().orgUrl}/${id}/members/reset-passwords`, {
        ...fetchGetOptions,
        method: 'PUT',
      });

      if (!response.ok) {
        throw new RequestError({ status: response.status, title: 'Could not reset passwords' });
      }

      return true;
    },
  },
};
