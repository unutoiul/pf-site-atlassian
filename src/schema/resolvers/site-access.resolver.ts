import { getConfig } from 'common/config';
import { RequestError } from 'common/error';

import { UpdateSiteAccessMutationVariables } from '../../schema/schema-types';
import { fetchGetOptions } from '../../utilities/schema';

export const resolver = {
  Site: {
    siteAccess: async ({ id }) => {
      const response = await fetch(`${getConfig().umBasePath}/${id}/signup-options`, fetchGetOptions);
      if (!response.ok) {
        return {};
      }

      return response.json();
    },
  },
  Mutation: {
    updateSiteAccess: async (_, { id, input: { domains, notifyAdmin, openInvite, signupEnabled } }: UpdateSiteAccessMutationVariables) => {
      const response = await fetch(`${getConfig().umBasePath}/${id}/signup-options`, {
        ...fetchGetOptions,
        method: 'PUT',
        body: JSON.stringify({
          domains,
          notifyAdmin,
          openInvite,
          signupEnabled,
        }),
      });

      if (!response.ok) {
        throw new RequestError({
          status: response.status,
          title: response.status === 400 ? 'Invalid site access configuration' : 'Problem saving the site access configuration',
        });
      }

      return true;
    },
  },
};
