import { getConfig } from 'common/config';
import { RequestError } from 'common/error';

import { CheckDomainClaimed, CheckDomainClaimedQueryArgs, ClaimDomainMutationVariables, ClaimDomainStatus } from '../../schema/schema-types';
import { Domain } from '../api/model/domain';
import { Organization } from '../api/model/organization';

export const resolver = {
  Mutation: {
    claimDomain: async (_, args: ClaimDomainMutationVariables): Promise<ClaimDomainStatus> => {
      return Domain.init().claimDomain(args);
    },
    deleteDomain: async (_, args) => {
      const response = await fetch(`${getConfig().orgUrl}/${args.id}/domainClaims/${args.domain}`,
        { method: 'DELETE', credentials: 'same-origin' });

      if (!response.ok) {
        throw new RequestError({
          status: response.status,
          title: 'Domain could not be removed',
        });
      }

      return true;
    },
  },
  Query: {
    checkDomainClaimed: async (_, args: CheckDomainClaimedQueryArgs): Promise<CheckDomainClaimed> => {
      return Organization.init().checkDomainClaimed(args.domain);
    },
  },
};
