import { analyticsClient } from 'common/analytics';
import { getConfig } from 'common/config';
import { RequestError } from 'common/error';

import { Organization } from '../../organization/organizations/organization.prop-types';
import { fetchGetOptions } from '../../utilities/schema';

export const resolver = {
  OrganizationSecurity: {
    passwordPolicy: async ({ id }: Organization) => {
      try {
        const response = await fetch(`${getConfig().orgUrl}/${id}/passwordpolicy`, fetchGetOptions);
        if (!response.ok) {
          return [];
        }

        return await response.json();
      } catch (e) {
        analyticsClient.onError(e);

        return [];
      }
    },
    saml: async ({ id }: Organization) => {
      const response = await fetch(`${getConfig().orgUrl}/${id}/saml`, fetchGetOptions);

      if (!response.ok) {
        if (response.status === 404) {
          return null;
        }
        throw new RequestError({ status: response.status });
      }

      return response.json();
    },
    twoStepVerification: async ({ id }: Organization) => {
      try {
        const response = await fetch(`${getConfig().orgUrl}/${id}/mfa`, fetchGetOptions);
        if (!response.ok) {
          return null;
        }

        return await response.json();
      } catch (e) {
        analyticsClient.onError(e);

        return null;
      }
    },
  },
};
