import { Site } from '../api';

export const resolver = {
  Site: {
    siteRenameStatus: async ({ id }) => {
      const responseData = await Site.create().getRenameStatus(id);

      return {
        id,
        ...responseData,
      };
    },
  },
};
