import { ProductId } from '../../site/user-management/access-config/access-config-type';
import { Site } from '../api';
import { AddGroupsAccessToProductMutationVariables, ApproveImportedGroupMutationArgs, RejectImportedGroupMutationArgs, RemoveGroupAccessToProductMutationArgs, SetDefaultGroupMutationArgs, SetDefaultProductsMutationVariables } from '../schema-types';

export const resolver = {
  Site: {
    defaultApps: async ({ id }) => {
      return Site.create().getDefaultApps(id);
    },
    groupsAdminAccessConfig: async ({ id }) => {
      return Site.create().getGroupsAdminAccessConfig(id);
    },
    groupsUseAccessConfig: async ({ id }) => {
      return Site.create().getGroupsUseAccessConfig(id);
    },
  },
  Mutation: {
    setDefaultProducts: async (_, { id, input: { productId, productDefault } }: SetDefaultProductsMutationVariables): Promise<void> => {
      await Site.create().setDefaultProducts(id, productId as ProductId, productDefault);
    },
    addGroupsAccessToProduct: async (_, { id, input: { productId, groups, productType } }: AddGroupsAccessToProductMutationVariables): Promise<void> => {
      await Site.create().addGroupsAccessToProduct(id, productId as ProductId, groups, productType);
    },
    removeGroupAccessToProduct: async (_, { id, input: { productId, groupId, productType } }: RemoveGroupAccessToProductMutationArgs): Promise<void> => {
      await Site.create().removeGroupAccessToProduct(id, productId as ProductId, groupId, productType);
    },
    setDefaultGroup: async (_, { id, input: { productId, groupId, isDefault } }: SetDefaultGroupMutationArgs): Promise<void> => {
      if (isDefault) {
        await Site.create().removeDefaultGroup(id, productId as ProductId, groupId);
      } else {
        await Site.create().setDefaultGroup(id, productId as ProductId, groupId);
      }
    },
    approveImportedGroup: async (_, { id, input: { productId, groupId } }: ApproveImportedGroupMutationArgs): Promise<void> => {
      await Site.create().approveImportedGroup(id, productId as ProductId, groupId);
    },
    rejectImportedGroup: async (_, { id, input: { productId, groupId } }: RejectImportedGroupMutationArgs): Promise<void> => {
      await Site.create().rejectImportedGroup(id, productId as ProductId, groupId);
    },
  },
};
