import { UserConnectedApps as UserConnectedAppsModel } from '../api/model/user-connected-apps';
import { UserConnectedApp } from '../schema-types';

export const resolver = {
  Site: {
    userConnectedApps: async ({ id }): Promise<UserConnectedApp[]> =>
      UserConnectedAppsModel.init().getUserConnectedApps(id),
  },
};
