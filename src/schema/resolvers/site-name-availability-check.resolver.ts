import { Site } from '../api';

export const resolver = {
  Mutation: {
    checkSiteNameAvailability: async (_, { cloudId, siteName }) => {
      return Site.create().checkNameAvailability(cloudId, siteName);
    },
  },
};
