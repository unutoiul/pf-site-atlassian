import { getConfig } from 'common/config';

import { fetchGetOptions } from '../../utilities/schema';
import { Member } from '../api';
import { MembersOrganizationUsersArgs, Organization, OrganizationUsers } from '../schema-types';
import { FieldResolver } from './field-resolver';

export const resolver: { OrganizationUsers: Partial<FieldResolver<Organization, OrganizationUsers>> } = {
  OrganizationUsers: {
    memberTotal: async ({ id }: Organization) => {
      const response = await fetch(`${getConfig().orgUrl}/${id}/members/total`, fetchGetOptions);
      if (!response.ok) {
        return {
          total: -1,
          enabledTotal: -1,
          disabledTotal: -1,
        };
      }

      return response.json();
    },
    admins: async ({ id }: Organization) => {
      const response = await fetch(`${getConfig().orgUrl}/${id}/admin`, fetchGetOptions);
      if (!response.ok) {
        return {
          id,
          total: 0,
          users: [],
        };
      }

      const admins = await response.json();

      return {
        id,
        ...admins,
      };
    },
    members: async ({ id }: Organization, filter: MembersOrganizationUsersArgs) => Member.init().getMembers(id, filter),
    mfaExemptMembers: async ({ id }: Organization) => {
      const response = await fetch(`${getConfig().orgUrl}/${id}/mfa/exemptUsers`, fetchGetOptions);

      if (response.ok) {
        return response.json();
      } else {
        throw new Error();
      }
    },
  },
};
