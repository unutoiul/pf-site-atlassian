import { getConfig } from 'common/config';
import { RequestError } from 'common/error';

import { fetchGetOptions } from '../../utilities/schema';

export const resolver = {
  Mutation: {
    enableTwoStepVerificationEnforcement: async (_, args): Promise<boolean> => {
      const response = await fetch(`${getConfig().orgUrl}/${args.id}/mfa`, {
        method: 'PUT',
        body: JSON.stringify({ dateEnforced: (args.date) }),
        ...fetchGetOptions,
      });

      if (!response.ok) {
        throw new RequestError({
          status: response.status,
          title: 'Two-step verification could not be enabled.',
        });
      }

      return true;
    },
    disableTwoStepVerificationEnforcement: async (_, args): Promise<boolean> => {
      const response = await fetch(`${getConfig().orgUrl}/${args.id}/mfa`, {
        method: 'DELETE',
        ...fetchGetOptions,
      });

      if (!response.ok) {
        throw new RequestError({
          status: response.status,
          title: 'Two-step verification could not be disabled.',
        });
      }

      return true;
    },
  },
};
