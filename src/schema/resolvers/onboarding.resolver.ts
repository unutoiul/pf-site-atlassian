import { analyticsClient } from 'common/analytics';
import { getConfig } from 'common/config';

import { fetchGetOptions } from '../../utilities/schema';

function flagKeyForId(id: string): string {
  return `admin.onboarding.${id}`;
}

export const resolver = {
  Query: {
    onboarding: async (_, { id }) => {
      try {
        const response = await fetch(`${getConfig().apiUrl}/flag/my?flagKey=${flagKeyForId(id)}`, {
          method: 'GET',
          ...fetchGetOptions,
          credentials: 'include',
        });

        if (response.ok) {
          const json = await response.json();

          return {
            id,
            dismissed: json.status,
          };
        }
      } catch (e) {
        if (e.message) {
          e.message = `Retrieving onboarding flag ${id} failed: ${e.message}`;
        }

        analyticsClient.onError(e);
      }

      return {
        id,
        dismissed: true,
      };
    },
  },
  Mutation: {
    dismissOnboarding: async (_, { id }) => {
      const response = await fetch(`${getConfig().apiUrl}/flag/my`, {
        method: 'POST',
        ...fetchGetOptions,
        credentials: 'include',
        body: JSON.stringify({
          flagKey: flagKeyForId(id),
        }),
      });

      if (!response.ok) {
        analyticsClient.onError(new Error(`Setting onboarding flag ${id} failed with status ${response.status}`));
      }

      return { id, dismissed: true };
    },
  },
};
