import { Site, User } from '../api';
import {
  ActivateUserMutationArgs,
  DeactivateUserMutationArgs,
  GrantAccessToProductsMutationArgs,
  ImpersonateUserMutationArgs,
  PromptResetPasswordMutationArgs,
  RevokeAccessToProductsMutationArgs,
  SuggestChangesMutationArgs,
} from '../schema-types';

interface UserParams {
  cloudId: string;
  id: string;
}

export const resolver = {
  UserDetails: {
    userDetails: async ({ cloudId, id }: UserParams) => {
      return User.create(cloudId, id).getDetails();
    },
    groups: async ({ cloudId, id }: UserParams, { start, count }: { start: number, count: number }) => {
      return User.create(cloudId, id).getGroups(start, count);
    },
    productAccess: async ({ cloudId, id }: UserParams) => {
      return User.create(cloudId, id).getProductAccess();
    },
    managedStatus: async ({ cloudId, id }: UserParams) => {
      try {
        return await User.create(cloudId, id).getUserManagedStatus();
      } catch (e) {
        return null;
      }
    },
  },
  Query: {
    user: (_, { cloudId, id }: UserParams) => ({ cloudId, id }),
  },
  Mutation: {
    impersonateUser: async (_, { cloudId, userId }: ImpersonateUserMutationArgs): Promise<void> => {
      return Site.create().impersonateUser(cloudId, userId);
    },
    activateUser: async (_, { cloudId, id }: ActivateUserMutationArgs): Promise<void> => {
      return User.create(cloudId, id).activate();
    },
    deactivateUser: async (_, { cloudId, id }: DeactivateUserMutationArgs): Promise<void> => {
      return User.create(cloudId, id).deactivate();
    },
    reinviteUser: async (_, { cloudId, userId }): Promise<void> => {
      return User.create(cloudId, userId).reinvite();
    },
    grantAccessToProducts: async (_, { cloudId, users, productIds }: GrantAccessToProductsMutationArgs): Promise<void> => {
      return Site.create().grantUsersAccessToProducts(cloudId, users, productIds);
    },
    revokeAccessToProducts: async (_, { cloudId, users, productIds }: RevokeAccessToProductsMutationArgs): Promise<void> => {
      return Site.create().revokeUsersAccessToProducts(cloudId, users, productIds);
    },
    promptResetPassword: async (_, { cloudId, userId }: PromptResetPasswordMutationArgs): Promise<void> => {
      return Site.create().promptResetPassword(cloudId, userId);
    },
    suggestChanges: async (_, { cloudId, userId, name, email }: SuggestChangesMutationArgs): Promise<void> => {
      return Site.create().suggestChanges(cloudId, { userId, displayName: name, email });
    },
  },
};
