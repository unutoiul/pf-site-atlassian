import { getConfig } from 'common/config';
import { RequestError } from 'common/error';

import { fetchGetOptions } from '../../utilities/schema';
import { Site } from '../api';

export const resolver = {
  SiteSettings: {
    emoji: async ({ id }) => {
      const defaultState = { uploadEnabled: true };
      const settingsQueryOptions = {
        ...fetchGetOptions,
        credentials: 'include',
      } as RequestInit;
      const response = await fetch(`${getConfig().apiUrl}/emoji/${id}/site/settings`, settingsQueryOptions);
      const json = response.ok ? await response.json() : defaultState;

      return {
        id,
        uploadEnabled: json.uploadEnabled,
      };
    },
    serviceDesk: async () => {
      const sdFlagName = 'sd.user.management';
      const sdProductName = 'jira-servicedesk.ondemand';

      // tslint:disable check-fetch-result
      // This is disabled because the fetches below are an edge case where the concurrent fetches are important to performance.
      const tenantContextRequestPromise = fetch(`/admin/rest/um/1/context`, fetchGetOptions);
      const sdFeatureFlagRequestPromise = fetch(`/admin/rest/um/1/features?${sdFlagName}`, fetchGetOptions);

      const tenantContextRequest = await tenantContextRequestPromise;
      if (!tenantContextRequest.ok) {
        throw new RequestError({
          id: 'contextRequest',
          status: tenantContextRequest.status,
        });
      }

      const tenantContext: { isAaEnabled: boolean, products?: object } = await tenantContextRequest.json();
      const isServiceDeskLicensed = tenantContext.products && tenantContext.products[sdProductName] != null;

      if (!isServiceDeskLicensed) {
        return { enabled: false };
      }

      if (tenantContext.isAaEnabled) {
        return { enabled: true };
      }

      const sdFeatureFlagRequest = await sdFeatureFlagRequestPromise;
      if (!sdFeatureFlagRequest.ok) {
        throw new RequestError({
          id: 'serviceDeskFeatureFlagRequest',
          status: sdFeatureFlagRequest.status,
        });
      }

      const result = await sdFeatureFlagRequest.json();

      return { enabled: result[sdFlagName] };
      // tslint:enable check-fetch-result
    },
    xFlow: async ({ id }) => {
      const enabled = await Site.create().isXFlowEnabled(id);

      return { enabled };
    },
  },
};
