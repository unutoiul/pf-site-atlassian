import { analyticsClient } from 'common/analytics';
import { getConfig } from 'common/config';
import { FeatureFlagsClient } from 'common/feature-flags';

import { CurrentUser, Organization } from '../api';
import { OrgSite } from '../schema-types';

export const resolver = {
  CurrentUser: {
    organizations: async () => {
      return Organization.init().getAll();
    },
    sites: async () => {
      const sites = await CurrentUser.create().getSites();

      return sites.map(site => ({
        id: site.id,
        // This is temporary while we are in the site-admin world without display name
        // and have only the current site to worry about
        displayName: site.displayName || window.location.hostname,
        avatarUrl: site.avatarUrl || `${getConfig().siteAdminAvatarCdn}/avatars/240/invalid.png`,
      }));
    },
    unlinkedSites: async (): Promise<OrgSite[]> => CurrentUser.create().getUnlinkedSites(),
    isSiteAdmin: async () => {
      return CurrentUser.create().isSiteAdmin();
    },
    id: async () => {
      return CurrentUser.create().getAccountId();
    },
    email: async () => {
      return CurrentUser.create().getEmail();

    },
    flag: async ({ id }, { flagKey }) => {
      try {
        const cloudAdminFFClient = await FeatureFlagsClient.getInstance().allFlags(id);

        return {
          id: flagKey,
          value: cloudAdminFFClient[flagKey],
        };

      } catch (e) {
        analyticsClient.onError(e);

        return {
          id: flagKey,
          value: false,
        };
      }
    },
  },
  Query: {
    currentUser: async () => {
      return CurrentUser.create().getInfoOrThrow();
    },
  },
};
