import { expect } from 'chai';
import * as fetchMock from 'fetch-mock';
import { sandbox as sinonSandbox, SinonSandbox } from 'sinon';

import { getConfig } from 'common/config';

import { combinedResolvers as resolvers } from '../resolvers';
import { AddGroupsAccessToProductInput, RemoveGroupAccessToProductInput } from '../schema-types';

describe('AccessConfig', () => {

  let sandbox: SinonSandbox;
  beforeEach(() => {
    sandbox = sinonSandbox.create();
  });
  afterEach(() => {
    sandbox.reset();
    afterEach(fetchMock.restore);
  });

  const id = 'fake';
  describe('AdminAccessConfig', () => {
    it('returns empty array if the response is not ok', async () => {
      fetchMock.get(`${getConfig().umBasePath}/${id}/product/access-config/admin`, {
        status: 500,
      });
      const response = await resolvers.Site.groupsAdminAccessConfig({ id });
      expect(response).to.deep.equal([]);
    });
    it('returns response when ok', async () => {
      const body = {
        adminAccessConfig: [
          {
            product: {
              productName: 'Jira Core',
              productId: 'product:jira:jira-core',
            },
            groups: [
              {
                id: 'dummy-group-id',
                name: 'jira-core-users',
                requiresApproval: false,
                default: true,
              },
            ],
          },
        ],
      };
      fetchMock.get(`${getConfig().umBasePath}/${id}/product/access-config/admin`, {
        status: 200,
        body,
      });
      const response = await resolvers.Site.groupsAdminAccessConfig({ id });
      expect(response).to.deep.equal(body.adminAccessConfig);
    });
  });
  describe('UseAccessConfig', () => {
    it('returns empty array if the response is not ok', async () => {
      fetchMock.get(`${getConfig().umBasePath}/${id}/product/access-config/use`, {
        status: 500,
      });
      const response = await resolvers.Site.groupsUseAccessConfig({ id });
      expect(response).to.deep.equal([]);
    });
    it('returns response when ok', async () => {
      const body = {
        useAccessConfig: [
          {
            product: {
              productName: 'Jira Core',
              productId: 'product:jira:jira-core',
            },
            groups: [
              {
                id: 'dummy-group-id',
                name: 'jira-core-users',
                requiresApproval: false,
                default: true,
              },
            ],
          },
        ],
      };
      fetchMock.get(`${getConfig().umBasePath}/${id}/product/access-config/use`, {
        status: 200,
        body,
      });
      const response = await resolvers.Site.groupsUseAccessConfig({ id });
      expect(response).to.deep.equal(body.useAccessConfig);
    });
  });
  describe('setDefaultProducts', () => {
    const input = {
      productId: 'jira-core',
      productDefault: false,
    };

    it('returns true if the response is ok', async () => {
      fetchMock.put(`${getConfig().umBasePath}/${id}/product/defaults`, {
        status: 200,
        body: {},
      });
      await resolvers.Mutation.setDefaultProducts(null, { id, input });
    });
    it('throws error when response resolves in error', async () => {
      fetchMock.put(`${getConfig().umBasePath}/${id}/product/defaults`, {
        status: 500,
      });
      await resolvers.Mutation.setDefaultProducts(null, { id, input }).catch(error => {
        expect(error.status).to.equal(500);
      });
    });
  });

  describe('setDefaultGroup', () => {

    const input = {
      productId: 'jira-core',
      groupId: 'group-id',
      isDefault: false,
    };

    it('returns true if the response is ok', async () => {
      fetchMock.post(`${getConfig().umBasePath}/${id}/product/jira-core/access-config/default-groups/group-id`, {
        status: 204,
      });
      await resolvers.Mutation.setDefaultGroup(null, { id, input });
    });
    it('throws error when response resolves in error', async () => {
      fetchMock.post(`${getConfig().umBasePath}/${id}/product/jira-core/access-config/default-groups/group-id`, {
        status: 500,
      });
      await resolvers.Mutation.setDefaultGroup(null, { id, input }).catch(error => {

        expect(error.status).to.equal(500);
      });
    });
  });

  describe('approveImportedGroup', () => {
    const input = {
      productId: 'jira-core',
      groupId: '123456789',
    };

    it('returns true if the response is ok', async () => {
      fetchMock.post(`${getConfig().umBasePath}/${id}/product/jira-core/access-config/import/123456789`, {
        status: 200,
        body: {},
      });
      await resolvers.Mutation.approveImportedGroup(null, { id, input });
    });
    it('throws error when response resolves in error', async () => {
      fetchMock.post(`${getConfig().umBasePath}/${id}/product/jira-core/access-config/import/123456789`, {
        status: 500,
      });
      await resolvers.Mutation.approveImportedGroup(null, { id, input }).catch(error => {

        expect(error.status).to.equal(500);
      });
    });
  });

  describe('addGroup', () => {

    const input: AddGroupsAccessToProductInput = {
      productId: 'jira-core',
      groups: ['group-id'],
      productType: 'USE',
    };

    it('returns true if the response is ok', async () => {
      fetchMock.post(`${getConfig().umBasePath}/${id}/product/jira-core/access-config/use`, {
        status: 204,
      });
      await resolvers.Mutation.addGroupsAccessToProduct(null, { id, input });
    });
    it('throws error when response resolves in error', async () => {
      fetchMock.post(`${getConfig().umBasePath}/${id}/product/jira-core/access-config/use`, {
        status: 500,
      });
      await resolvers.Mutation.addGroupsAccessToProduct(null, { id, input }).catch(error => {
        expect(error.status).to.equal(500);
      });
    });
  });

  describe('removeGroup', () => {

    const input: RemoveGroupAccessToProductInput = {
      productId: 'jira-core',
      groupId: 'group-id',
      productType: 'USE',
    };

    it('returns true if the response is ok', async () => {
      fetchMock.delete(`${getConfig().umBasePath}/${id}/product/jira-core/access-config/use/group-id`, {
        status: 204,
      });
      await resolvers.Mutation.removeGroupAccessToProduct(null, { id, input });
    });
    it('throws error when response resolves in error', async () => {
      fetchMock.delete(`${getConfig().umBasePath}/${id}/product/jira-core/access-config/use/group-id`, {
        status: 500,
      });
      await resolvers.Mutation.removeGroupAccessToProduct(null, { id, input }).catch(error => {
        expect(error.status).to.equal(500);
      });
    });
  });

  describe('removeDefaultGroup', () => {

    const input = {
      productId: 'jira-core',
      groupId: 'group-id',
      isDefault: true,
    };

    it('returns true if the response is ok', async () => {
      fetchMock.delete(`${getConfig().umBasePath}/${id}/product/jira-core/access-config/default-groups/group-id`, {
        status: 204,
      });
      await resolvers.Mutation.setDefaultGroup(null, { id, input });
    });
    it('throws error when response resolves in error', async () => {
      fetchMock.delete(`${getConfig().umBasePath}/${id}/product/jira-core/access-config/default-groups/group-id`, {
        status: 500,
      });
      await resolvers.Mutation.setDefaultGroup(null, { id, input }).catch(error => {
        expect(error.status).to.equal(500);
      });
    });
  });
  describe('rejectImportedGroup', () => {

    const input = {
      productId: 'jira-core',
      groupId: '123456789',
    };

    it('returns true if the response is ok', async () => {
      fetchMock.delete(`${getConfig().umBasePath}/${id}/product/jira-core/access-config/import/123456789`, {
        status: 200,
        body: {},
      });
      await resolvers.Mutation.rejectImportedGroup(null, { id, input });
    });
    it('throws error when response resolves in error', async () => {
      fetchMock.delete(`${getConfig().umBasePath}/${id}/product/jira-core/access-config/import/123456789`, {
        status: 500,
      });
      await resolvers.Mutation.rejectImportedGroup(null, { id, input }).catch(error => {
        expect(error.status).to.equal(500);
      });
    });
  });
});
