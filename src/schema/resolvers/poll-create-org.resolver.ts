import { RequestError } from 'common/error';

import { PollCreateOrgQueryVariables } from '../../schema/schema-types';
import { fetchGetOptions } from '../../utilities/schema';

export const resolver = {
  Query: {
    pollCreateOrg: async (_, args: PollCreateOrgQueryVariables) => {

      if (!args.progressUri) {
        return {
          completed: false,
          successful: false,
        };
      }

      const response = await fetch(args.progressUri, fetchGetOptions);

      if (!response.ok) {
        throw new RequestError({
          status: response.status,
          title: 'Uh oh! Something went wrong',
          description: 'The organization could not be created. Please try again later.',
        });
      }

      return response.json();
    },
  },
};
