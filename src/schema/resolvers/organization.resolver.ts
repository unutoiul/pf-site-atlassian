import { getConfig } from 'common/config';
import { RequestError } from 'common/error';
import { FeatureFlagsClient } from 'common/feature-flags';

import { mutateOptions } from '../../utilities/schema';
import { AddOrganizationAdminArgs } from '../api';
import { Organization as OrganizationModel } from '../api/model';
import {
  AdminApiKeyCollection,
  AdminApiKeysOrganizationArgs,
  InitiateMemberCsvEmailExportMutationArgs,
  LinkSiteToOrganizationMutationArgs,
  MemberExport,
  MemberExportOrganizationArgs,
  OrganizationQueryArgs,
  OrgSite,
  OrgSiteLinkingResult,
  RenameOrgMutationVariables,
  SiteAdmin,
  SiteAdminsOrganizationArgs,
} from '../schema-types';

export const resolver = {
  Organization: {
    domainClaim: ({ id }) => ({ id }),
    security: ({ id }) => ({ id }),
    users: ({ id }) => ({ id }),
    flag: async ({ id }, { flagKey }) => {
      try {
        const cloudAdminFFClient = await FeatureFlagsClient.getInstance().allFlags(`ORG:${id}`);

        return {
          id: flagKey,
          value: cloudAdminFFClient[flagKey],
        };
      } catch (e) {
        return {
          id: flagKey,
          value: false,
        };
      }
    },
    isBillable: async ({ id }) => OrganizationModel.init().isBillable(id),
    externalDirectories: async ({ id }) => OrganizationModel.init().getExternalDirectories(id),
    siteAdmins: async ({ id }, { cloudId }: SiteAdminsOrganizationArgs): Promise<SiteAdmin[]> => OrganizationModel.init().getSiteAdmins(id, cloudId),
    sites: async ({ id }): Promise<OrgSite[]> => OrganizationModel.init().getLinkedSites(id),
    memberExport: async ({ id }, { exportId }: MemberExportOrganizationArgs): Promise<MemberExport> => OrganizationModel.init().getMemberExport(id, exportId),
    adminApiKeys: async ({ id }, { scrollId }: AdminApiKeysOrganizationArgs = { scrollId: null }): Promise<AdminApiKeyCollection> =>
      OrganizationModel.init().getAdminApiKeys(id, scrollId),
    products: async ({ id }) => OrganizationModel.init().getProducts(id),
  },
  Query: {
    organization: async (_, { id }: OrganizationQueryArgs) => OrganizationModel.init().getSummary(id),
  },
  Mutation: {
    createOrganization: async (_, args) => OrganizationModel.init().initiateOrganizationCreation(args.name),
    addOrganizationAdmin: async (_, { orgId, email }: AddOrganizationAdminArgs): Promise<boolean> => {
      await OrganizationModel.init().addOrganizationAdmin(orgId, email);

      return true;
    },
    removeOrganizationAdmin: async (_, { orgId, adminId }): Promise<boolean> => {
      const method = {
        ...mutateOptions,
        method: 'DELETE',
        credentials: 'same-origin' as RequestCredentials,
      };

      const path = `${getConfig().orgUrl}/${orgId}/admin/${adminId}`;
      const response = await fetch(path, method);

      if (!response.ok) {
        throw new RequestError({
          status: response.status,
          description: 'The organization admin could not be removed. Please try again later.',
        });
      }

      return true;
    },
    renameOrganization: async (_, { id, name }: RenameOrgMutationVariables) => {
      await OrganizationModel.init().rename(id, name);

      return { id, name };
    },
    initiateMemberCsvEmailExport: async (_, { orgId }: InitiateMemberCsvEmailExportMutationArgs) => OrganizationModel.init().initiateMemberCsvEmailExport(orgId),
    linkSiteToOrganization: async (_, { orgId, cloudId }: LinkSiteToOrganizationMutationArgs): Promise<OrgSiteLinkingResult> => {
      return OrganizationModel.init().linkSite(orgId, cloudId);
    },
  },
};
