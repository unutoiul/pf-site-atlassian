import { getConfig } from 'common/config';
import { RequestError } from 'common/error';

import { DeleteInviteUrlMutationVariables, GenerateInviteUrlMutationVariables } from '../../schema/schema-types';
import { fetchGetOptions } from '../../utilities/schema';

const errorCodes = {
  403: 'Not authorized',
  400: 'Invalid request',
};

export const resolver = {
  Site: {
    inviteUrls: async ({ id }) => {
      const response = await fetch(`${getConfig().umBasePath}/${id}/invitation-urls`, fetchGetOptions);
      if (!response.ok) {
        return [];
      }

      return (await response.json()).urls;
    },
  },
  Mutation: {
    generateInviteUrl: async (_, { id, input: { productKey } }: GenerateInviteUrlMutationVariables) => {
      const response = await fetch(`${getConfig().umBasePath}/${id}/invitation-urls/${productKey}/generate`, {
        ...fetchGetOptions,
        method: 'POST',
      });

      if (!response.ok) {
        throw new RequestError({
          status: response.status,
          title: errorCodes[response.status] || 'Invalid request',
        });
      }

      return true;
    },
    deleteInviteUrl: async (_, { id, input: { productKey } }: DeleteInviteUrlMutationVariables) => {
      const response = await fetch(`${getConfig().umBasePath}/${id}/invitation-urls/${productKey}`, {
        ...fetchGetOptions,
        method: 'DELETE',
      });

      if (!response.ok) {
        throw new RequestError({
          status: response.status,
          title: errorCodes[response.status] || 'Invalid request',
        });
      }

      return true;
    },
  },
};
