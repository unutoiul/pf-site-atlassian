import { AccessRequest as AccessRequestModel } from '../api';
import { AccessRequest, AccessRequestListQueryArgs } from '../schema-types';

export const resolver = {
  AccessRequestList: {
    total: ({ total }): number => {
      return total;
    },
    accessRequests: ({ accessRequests }): AccessRequest[] => {
      return accessRequests;
    },
  },
  Query: {
    accessRequestList: async (_, { cloudId, input }: AccessRequestListQueryArgs) => {
      return AccessRequestModel.create().getRequests(cloudId, input);
    },
  },
  Mutation: {
    approveAccess: async (_, { cloudId, input }): Promise<void> => {
      await AccessRequestModel.create().approveAccess(cloudId, input);
    },
    denyAccess: async (_, { cloudId, input }): Promise<void> => {
      await AccessRequestModel.create().denyAccess(cloudId, input);
    },
  },
};
