import {
  AddUsersToGroupsMutationVariables,
  CreateGroupMutationVariables,
  DeleteGroupMutationVariables,
  Group,
  GroupsPageQueryVariables,
  UpdateGroupMutationVariables,
} from '../../schema/schema-types';

import { Site } from '../api';

interface GroupParams {
  cloudId: string;
  id: string;
}

export const resolver = {
  GroupDetails: {
    groupDetails: async ({ cloudId, id }: GroupParams) => {
      return Site.create().getGroup(cloudId, id);
    },
    members: async ({ cloudId, id }: GroupParams, paginationParams: { start?: number | null, count?: number | null }) => {
      return Site.create().getGroupMembers(cloudId, id, paginationParams);
    },
  },
  Query: {
    group: (_, { cloudId, id }: GroupParams) => {
      return ({ cloudId, id });
    },
    groupList: async (_, { cloudId, start, count, displayName }: GroupsPageQueryVariables) => {
      return Site.create().getGroups(cloudId, {
        start,
        count,
        displayName,
      });
    },
  },
  Mutation: {
    createGroup: async (_, { cloudId, input }: CreateGroupMutationVariables): Promise<Group> => {
      return Site.create().createGroup(cloudId, input);
    },
    updateGroup: async (_, { cloudId, id, input }: UpdateGroupMutationVariables): Promise<Group> => {
      return Site.create().updateGroup(cloudId, id, input);
    },
    deleteGroup: async (_, { cloudId, id }: DeleteGroupMutationVariables): Promise<boolean> => {
      return Site.create().deleteGroup(cloudId, id);
    },
    addUsersToGroups: async (_, { cloudId, input }: AddUsersToGroupsMutationVariables): Promise<boolean> => {
      return Site.create().addUsersToGroups(cloudId, input);
    },
    deleteUserFromGroup: async (_, { cloudId, userId, groupId }): Promise<boolean> => {
      return Site.create().deleteUserFromGroup(cloudId, userId, groupId);
    },
  },
};
