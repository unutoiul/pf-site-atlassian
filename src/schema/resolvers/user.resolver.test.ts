import { expect } from 'chai';
import { sandbox as sinonSandbox, SinonSandbox, SinonStub } from 'sinon';

import { RequestError } from 'common/error';

import { User } from '../api';
import { Group } from '../schema-types';
import { resolver } from './user.resolver';

describe('User Resolver', () => {
  const cloudId = 'cloudId';
  const id = 'id';

  let sandbox: SinonSandbox;
  let mockUser: {
    getDetails: SinonStub,
    getGroups: SinonStub,
    getProductAccess: SinonStub,
    getUserManagedStatus: SinonStub,
  };

  beforeEach(() => {
    sandbox = sinonSandbox.create();
    mockUser = {
      getDetails: sandbox.stub(),
      getGroups: sandbox.stub(),
      getProductAccess: sandbox.stub(),
      getUserManagedStatus: sandbox.stub(),
    };
    sandbox.stub(User, 'create').withArgs(cloudId, id).returns(mockUser);
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('userDetails', () => {
    it('returns user details', async () => {
      const response = {
        id: 'id',
        email: 'email',
        displayName: 'displayName',
        active: 'active',
        nickname: 'nickname',
        title: 'title',
        timezone: 'timezone',
        location: 'location',
        companyName: 'companyName',
        department: 'department',
        system: 'system',
      };
      mockUser.getDetails.resolves(response);

      const result = await resolver.UserDetails.userDetails({ cloudId, id });

      expect(result).to.equal(response);
    });

    it('throws error if unsuccessful', async () => {
      const error = new RequestError({ status: 500 });
      mockUser.getDetails.rejects(error);

      await resolver.UserDetails.userDetails({ cloudId, id }).then(() => {
        expect.fail();
      }, (e) => {
        expect(e).to.equal(error);
      });
    });
  });

  describe('groups', () => {
    const start = 0;
    const count = 1;

    it('returns groups', async () => {
      const group: Group = {
        id: 'id',
        name: 'name',
        description: 'description',
        productPermissions: [],
        defaultForProducts: [],
        sitePrivilege: 'NONE',
        userTotal: 1,
        unmodifiable: false,
        managementAccess: 'ALL',
        ownerType: null,
      };

      const response = {
        total: 2,
        groups: [group],
      };
      mockUser.getGroups.resolves(response);

      const result = await resolver.UserDetails.groups({ cloudId, id }, { start, count });

      expect(result).to.equal(response);
    });

    it('throws error if unsuccessful', async () => {
      const error = new RequestError({ status: 500 });
      mockUser.getGroups.rejects(error);

      await resolver.UserDetails.groups({ cloudId, id }, { start, count }).then(() => {
        expect.fail();
      }, (e) => {
        expect(e).to.equal(error);
      });
    });
  });

  describe('productAccess', () => {
    it('returns product access', async () => {
      const response = [
        {
          productName: 'Confluence',
          accessLevel: 'USE',
          presence: null,
        },
      ];
      mockUser.getProductAccess.resolves(response);

      const result = await resolver.UserDetails.productAccess({ cloudId, id });

      expect(result).to.equal(response);
    });

    it('throws error if unsuccessful', async () => {
      const error = new RequestError({ status: 500 });
      mockUser.getProductAccess.rejects(error);

      await resolver.UserDetails.productAccess({ cloudId, id }).then(() => {
        expect.fail();
      }, (e) => {
        expect(e).to.equal(error);
      });
    });
  });

  describe('managedStatus', () => {
    it('returns user managed status', async () => {
      const response = {
        managed: true,
        owner: {
          ownerType: 'Google',
          ownerId: 'Google012345678',
        },
      };

      mockUser.getUserManagedStatus.resolves(response);

      const result = await resolver.UserDetails.managedStatus({ cloudId, id });

      expect(result).to.equal(response);
    });

    it('returns null if unsuccessful', async () => {
      const error = new RequestError({ status: 500 });
      mockUser.getUserManagedStatus.rejects(error);

      await resolver.UserDetails.managedStatus({ cloudId, id }).then((result) => {
        expect(result).to.be.null();
      });
    });
  });
});
