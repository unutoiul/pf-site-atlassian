import { expect } from 'chai';
import { sandbox as sinonSandbox, SinonSandbox, SinonStub } from 'sinon';

import { RequestError } from 'common/error';

import { GSuite } from '../api';
import { resolver } from './g-suite.resolver';

describe('G Suite Resolver', () => {
  const cloudId = 'cloudId';

  const sandbox: SinonSandbox = sinonSandbox.create();
  let mockGSuite: {
    getState: SinonStub,
    getConnection: SinonStub,

  };

  beforeEach(() => {
    mockGSuite = {
      getState: sandbox.stub(),
      getConnection: sandbox.stub(),
    };
    sandbox.stub(GSuite, 'create').withArgs(cloudId).returns(mockGSuite);
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('connectionUrl', () => {
    it('should return a connection url', async () => {
      mockGSuite.getConnection.resolves({ connection_url: 'http://google.com' });
      const result = await resolver.Mutation.getConnection({}, { id: cloudId });
      expect(result).to.deep.equal({ connection_url: 'http://google.com' });
    });
  });

  describe('state', () => {
    it('returns disconnected state', async () => {
      mockGSuite.getState.resolves(undefined);

      const result = await resolver.GSuite.state({ id: cloudId });

      expect(result).to.equal(undefined);
    });

    it('returns connected state', async () => {
      const response = {
        syncConfig: [],
        syncStartTime: null,
        nextSyncStartTime: '17 Oct 2018 4:35AM GMT',
        lastSync: {
          startTime: '17 Oct 2018 3:35AM GMT',
          failed: 0,
          errors: [],
          updated: 0,
          hasReport: true,
          status: 'successful',
          finishTime: '17 Oct 2018 3:35AM GMT',
          numOfSyncedUsers: 1,
          deleted: 0,
          created: 1,
        },
        selectedGroups: ['01hmsyys3fnt05a'],
        noSyncs: 2,
        user: 'jberry@jberry-test.teamsinspace.com',
      };
      mockGSuite.getState.resolves(response);

      const result = await resolver.GSuite.state({ id: cloudId });

      expect(result).to.deep.equal(response);
    });

    it('throws error if unsuccessful', async () => {
      const error = new RequestError({ status: 500 });
      mockGSuite.getState.rejects(error);

      await resolver.GSuite.state({ id: cloudId }).then(() => {
        expect.fail();
      }, (e) => {
        expect(e).to.equal(error);
      });
    });
  });
});
