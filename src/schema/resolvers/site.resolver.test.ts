import { expect } from 'chai';
import * as fetchMock from 'fetch-mock';
import * as sinon from 'sinon';

import { getConfig } from 'common/config';
import { FeatureFlagsClient } from 'common/feature-flags';

import {
  csatEvalCohortFlagKey,
  trustedUsersCommunicationFeatureFlagKey,
  trustedUsersFeatureFlagKey,
} from '../../site/feature-flags/flag-keys';
import { util } from '../../utilities/admin-hub';
import { combinedResolvers as resolvers } from '../resolvers';
import { DefaultProduct } from '../schema-types';

describe('Site', () => {
  const sandbox = sinon.sandbox.create();
  let isAdminHubStub: sinon.SinonStub;
  let allFlagsSpy: sinon.SinonStub;
  let getInstanceStub: sinon.SinonStub;

  beforeEach(() => {
    getInstanceStub = sandbox.stub(FeatureFlagsClient, 'getInstance');
    allFlagsSpy = sandbox.stub();
    isAdminHubStub = sandbox.stub(util, 'isAdminHub');

    allFlagsSpy.returns(true);

    getInstanceStub.returns({
      allFlags: allFlagsSpy,
    });
  });

  afterEach(() => {
    sandbox.restore();
    sandbox.reset();
    fetchMock.restore();
  });

  describe('context ', () => {
    const body = {
      isUsingIdentityPlatform: true,
      isAaOptOutDisabled: true,
      isInVertigo: true,
      isAaEnabled: true,
      isAutoUpgradeMode: true,
      products: {
        'jira-software.ondemand': '',
      },
      userId: '655363:1a082af1-7d4d-406b-be89-9a95f0669f34',
      cloudId: '5934cbbd-cc22-4a71-bfc6-c2a239b7b55c',
      firstActivationDate: 1540175786309,
    };

    it('resolves the API returning the firstActivationDate', async () => {
      fetchMock.get(`/admin/rest/um/1/context`, {
        status: 200,
        body,
      });

      const response = await resolvers.Site.context();

      expect(response.firstActivationDate).to.equal('1540175786309');
    });
  });

  describe('flags', () => {
    beforeEach(() => {
      fetchMock.get(`${getConfig().umBasePath}/fake-cloud-id/feature-flags`, {
        status: 200,
        body: {
          'identity.test.flag.key': true,
        },
      });
    });

    it('uses the hostname in the non admin hub context', async () => {
      isAdminHubStub.returns(false);

      await resolvers.Site.flags({ id: 'fake-cloud-id' });

      expect(allFlagsSpy.firstCall.args[0]).to.equal(window.location.host);
    });

    it('uses the cloud id in the admin hub context', async () => {
      isAdminHubStub.returns(true);

      await resolvers.Site.flags({ id: 'fake-cloud-id' });

      expect(allFlagsSpy.firstCall.args[0]).to.equal('fake-cloud-id');
    });
  });

  describe('flag', () => {
    beforeEach(() => {
      fetchMock.get(`${getConfig().umBasePath}/fake-cloud-id/feature-flags`, {
        status: 200,
        body: {
          'identity.test.flag.key': true,
        },
      });
    });

    it('uses the hostname in the non admin hub context', async () => {
      isAdminHubStub.returns(false);

      await resolvers.Site.flag({ id: 'fake-cloud-id' }, { flagKey: 'test-flag' });

      expect(allFlagsSpy.firstCall.args[0]).to.equal(window.location.host);
    });

    it('uses the cloud id in the admin hub context', async () => {
      isAdminHubStub.returns(true);

      await resolvers.Site.flag({ id: 'fake-cloud-id' }, { flagKey: 'test-flag' });

      expect(allFlagsSpy.firstCall.args[0]).to.equal('fake-cloud-id');
    });
  });

  describe('getSiteUsersList', () => {
    const input = {
      start: 1,
      count: 20,
    };

    const body = {
      total: 1,
      users: [
        {
          id: '12',
          displayName: 'Hello',
          email: 'hello@acme.com',
          active: false,
          admin: false,
          presence: '2017-07-10T14:00:00Z',
        },
      ],
    };

    it('returns true if the response is ok', async () => {
      const cloudId = 'fake';
      fetchMock.get(`${getConfig().umBasePath}/${cloudId}/users?count=20&start-index=1`, {
        status: 200,
        body,
      });
      const response = await resolvers.Query.siteUsersList(null, { cloudId, input });
      expect(response).to.deep.equal(body);
    });

    it('throws error when response resolves in error', async () => {
      const cloudId = 'fake';
      fetchMock.get(`${getConfig().umBasePath}/${cloudId}/users?count=20&start-index=1`, {
        status: 500,
      });
      await resolvers.Query.siteUsersList(null, { cloudId, input }).catch((error) => {
        expect(error.status).to.equal(500);
      });
    });
  });
  describe('CSAT Cohort Feature Flags', () => {
    it('should get csat feature flag if csatCohort feature flag key is provided', async () => {

      fetchMock.get(`${getConfig().identityFeatureFlagUrl}?${csatEvalCohortFlagKey}`, {
        status: 200,
        body: {
          [csatEvalCohortFlagKey]: true,
        },
      });

      const response = await resolvers.Site.flag({ id: 'fake-cloud-id' }, { flagKey: csatEvalCohortFlagKey });
      expect(response).to.deep.equal({
        id: csatEvalCohortFlagKey,
        value: true,
      });
    });
  });
  describe('Trusted Users Feature Flags', () => {
    it('should get trusted users feature flag if trusted users feature flag key is provided', async () => {

      fetchMock.get(`${getConfig().identityFeatureFlagUrl}?${trustedUsersFeatureFlagKey}`, {
        status: 200,
        body: {
          [trustedUsersFeatureFlagKey]: false,
        },
      });

      const response = await resolvers.Site.flag({ id: 'fake-cloud-id' }, { flagKey: trustedUsersFeatureFlagKey });
      expect(response).to.deep.equal({
        id: trustedUsersFeatureFlagKey,
        value: false,
      });
    });
  });
  describe('Trusted Users Comms Feature Flags', () => {
    it('should get trusted users feature flag if trusted users feature flag key is provided', async () => {

      fetchMock.get(`${getConfig().identityFeatureFlagUrl}?${trustedUsersCommunicationFeatureFlagKey}`, {
        status: 200,
        body: {
          [trustedUsersCommunicationFeatureFlagKey]: false,
        },
      });

      const response = await resolvers.Site.flag({ id: 'fake-cloud-id' }, { flagKey: trustedUsersCommunicationFeatureFlagKey });
      expect(response).to.deep.equal({
        id: trustedUsersCommunicationFeatureFlagKey,
        value: false,
      });
    });
  });
  describe('Invite mutations', () => {
    it('should request the default product ids if none are provided in the mutation', async () => {
      const cloudId = 'fake';
      fetchMock.post(`${getConfig().umBasePath}/${cloudId}/users/invite`, {
        status: 204,
      });
      fetchMock.get(`${getConfig().umBasePath}/${cloudId}/product/defaults`, {
        status: 200,
        body: {
          products: [{
            productId: 'jira-software',
            productName: 'Jira Software',
            canonicalProductKey: 'jira-software',
          }] as DefaultProduct[],
        },
      });

      await resolvers.Mutation.inviteUsers(null, { id: cloudId, input: {} });

      expect(fetchMock.calls().length).to.equal(2);
      expect(JSON.parse(fetchMock.calls()[1][1].body).productIds).deep.equals(['jira-software']);
    });

    it('should make only one request and use provided product ids if some are provided', async () => {
      const cloudId = 'fake';
      fetchMock.post(`${getConfig().umBasePath}/${cloudId}/users/invite`, {
        status: 204,
      });
      fetchMock.get(`${getConfig().umBasePath}/${cloudId}/product/defaults`, {
        status: 200,
        body: {
          products: [{
            productId: 'jira-software',
            productName: 'Jira Software',
            canonicalProductKey: 'jira-software',
          }] as DefaultProduct[],
        },
      });

      await resolvers.Mutation.inviteUsers(null, { id: cloudId, input: { productIds: ['test-product'] } });

      expect(fetchMock.calls().length).to.equal(1);
      expect(JSON.parse(fetchMock.calls()[0][1].body).productIds).deep.equals(['test-product']);
    });

    it('should make only one request and use provided product ids if an empty array is provided', async () => {
      const cloudId = 'fake';
      fetchMock.post(`${getConfig().umBasePath}/${cloudId}/users/invite`, {
        status: 204,
      });
      fetchMock.get(`${getConfig().umBasePath}/${cloudId}/product/defaults`, {
        status: 200,
        body: {
          products: [{
            productId: 'jira-software',
            productName: 'Jira Software',
            canonicalProductKey: 'jira-software',
          }] as DefaultProduct[],
        },
      });

      await resolvers.Mutation.inviteUsers(null, { id: cloudId, input: { productIds: [] } });

      expect(fetchMock.calls().length).to.equal(1);
      expect(JSON.parse(fetchMock.calls()[0][1].body).productIds).deep.equals([]);
    });
  });

});
