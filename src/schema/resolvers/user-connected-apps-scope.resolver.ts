import { UserConnectedApps } from '../api/model/user-connected-apps';

interface ShallowScopeParent {
  scopes: Array<{ id: string }>;
}

const scopes = async ({ scopes: shallowScopes }: ShallowScopeParent) =>
  UserConnectedApps.init().getScopes(shallowScopes.map(scope => scope.id));

export const resolver = {
  UserConnectedApp: { scopes },
  UserGrant: { scopes },
};
