import { Cads as CadsModel, Jira as JiraModel } from '../api';
import {
  CustomerExportDirectoryArgs,
  CustomersJsdArgs,
  DeleteJsdCustomerMutationArgs,
  MigrateJsdCustomerMutationArgs,
  PaginatedJsdCustomers,
  UpdateJsdCustomerAccessMutationArgs,
  UpdateJsdCustomerNameMutationArgs,
  UpdateJsdCustomerPasswordMutationArgs,
} from '../schema-types';

export const resolver = {
  Jsd: {
    customers: async ({ id }, args: CustomersJsdArgs): Promise<PaginatedJsdCustomers> => {
      return JiraModel.create().getJsdCustomers(id, args);
    },

    directory: async ({ id }) => {
      const directoryId = await CadsModel.create().getDirectoryId(id);

      return {
        id: directoryId.serviceDeskCustomerDirectoryId,
      };
    },
  },

  Directory: {
    customerExport: async ({ id }, args: CustomerExportDirectoryArgs): Promise<string> => {
      return CadsModel.create().getCustomerExport(id, args.errorRedirectUrl);
    },
  },

  Mutation: {
    deleteJsdCustomer: async (_, { directoryId, accountId }: DeleteJsdCustomerMutationArgs): Promise<void> => {
      await CadsModel.create().deleteCustomer(directoryId, accountId);
    },

    updateJsdCustomerAccess: async (_, { directoryId, accountId, access }: UpdateJsdCustomerAccessMutationArgs): Promise<void> => {
      await CadsModel.create().updateCustomerAccess(directoryId, accountId, access);
    },

    updateJsdCustomerName: async (_, { directoryId, accountId, name }: UpdateJsdCustomerNameMutationArgs): Promise<void> => {
      await CadsModel.create().updateCustomerName(directoryId, accountId, name);
    },

    updateJsdCustomerPassword: async (_, { directoryId, accountId, password }: UpdateJsdCustomerPasswordMutationArgs): Promise<void> => {
      await CadsModel.create().updateCustomerPassword(directoryId, accountId, password);
    },

    migrateJsdCustomer: async (_, { cloudId, accountId }: MigrateJsdCustomerMutationArgs): Promise<void> => {
      await JiraModel.create().migrateToAtlassianAccount(cloudId, accountId);
    },
  },
};
