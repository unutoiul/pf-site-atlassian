import { ExternalDirectory as ExternalDirectoryModel } from '../api/model/external-directory';
import { CreateDirectoryMutationArgs, GroupsExternalDirectoryArgs, LogsExternalDirectoryArgs, RegenerateDirectoryApiKeyMutationArgs, RemoveDirectoryMutationArgs } from '../schema-types';

export const resolver = {
  Mutation: {
    removeDirectory: async (_, args: RemoveDirectoryMutationArgs) => {
      await ExternalDirectoryModel.init().revoke(args);

      return true;
    },
    regenerateDirectoryApiKey: async (_, args: RegenerateDirectoryApiKeyMutationArgs) => {
      return ExternalDirectoryModel.init().regenerate(args);
    },
    createDirectory: async (_, args: CreateDirectoryMutationArgs) =>
      ExternalDirectoryModel.init().create(args),
  },
  ExternalDirectory: {
    logs: async ({ id }, { startIndex, count }: LogsExternalDirectoryArgs) => {
      return ExternalDirectoryModel.init().logs({
        directoryId: id,
        startIndex,
        count,
      });
    },
    groups: async ({ id }, { startIndex, count }: GroupsExternalDirectoryArgs) => {
      return ExternalDirectoryModel.init().groups({
        directoryId: id,
        startIndex,
        count,
      });
    },
    syncedUsers: async ({ id }) => {
      return ExternalDirectoryModel.init().syncedUsers({
        directoryId: id,
      });
    },
  },
};
