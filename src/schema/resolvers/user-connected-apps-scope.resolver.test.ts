import { expect } from 'chai';
import * as fetchMock from 'fetch-mock';

import { getConfig } from 'common/config';

import { UserConnectedAppsClient } from '../api/client/user-connected-apps-client';
import { AppScope } from '../schema-types';
import { combinedResolvers as resolvers } from './';

describe('User Connected Scopes', async () => {

  afterEach(fetchMock.restore);

  const keys = ['participate:conversation'];
  const url = UserConnectedAppsClient.buildScopesUrl(keys);

  it('builds url with single & multiple scopes', async () => {
    const scopeKey = ['participate:conversation'];
    const singleScopeUrl = UserConnectedAppsClient.buildScopesUrl(scopeKey);
    expect(singleScopeUrl).to.equal(`${getConfig().caasUrl}/scope?scopeKey=participate%3Aconversation`);

    const multiScopeKeys = ['participate:conversation', 'read:jira'];
    const multiScopeUrl = UserConnectedAppsClient.buildScopesUrl(multiScopeKeys);
    expect(multiScopeUrl).to.equal(`${getConfig().caasUrl}/scope?scopeKey=participate%3Aconversation&scopeKey=read%3Ajira`);
  });

  it('fetches scope details for scope ids', async () => {
    fetchMock.get(url, {
      status: 200,
      body: {
        _links: {
          self: 'https://connect.staging.atlassian.io/scope?scopeKey=participate%3Aconversation',
        },
        values: [
          {
            _links: {
              self: 'https://connect.staging.atlassian.io/scope?scopeKey=participate%3Aconversation',
            },
            key: 'participate:conversation',
            name: 'Participate in conversations',
            description: 'Take actions in a conversation',
            serviceId: '62616e61-6e61-4d1f-954c-659dd5cd33ce',
          },
        ],
      },
    });

    const parentWithScope = { scopes: [{ id: keys[0] }] };
    const appResponse = await resolvers.UserConnectedApp.scopes(parentWithScope);
    const grantResponse = await resolvers.UserGrant.scopes(parentWithScope);
    const resolvedScope: AppScope[] = [{
      id: 'participate:conversation',
      untranslatedName: 'Participate in conversations',
      untranslatedDescription: 'Take actions in a conversation',
    }];
    expect(appResponse).to.deep.equal(resolvedScope);
    expect(grantResponse).to.deep.equal(resolvedScope);
  });

  it('caches scopes for same scope ids', async () => {
    const singleScopeKey = ['some:scope'];
    const singleScopeUrl = UserConnectedAppsClient.buildScopesUrl(singleScopeKey);
    const multiScopeKeys = ['participate:conversation', 'read:jira'];
    const multiScopeUrl = UserConnectedAppsClient.buildScopesUrl(multiScopeKeys);
    fetchMock.get('*', {
      status: 200,
      body: {
        values: [
          {
            key: 'participate:conversation',
            name: 'Participate in conversations',
            description: 'Take actions in a conversation',
          },
        ],
      },
    });

    const singleScopeParent = { scopes: [{ id: singleScopeKey[0] }] };
    const multiScopesParent = { scopes: [{ id: multiScopeKeys[0] }, { id: multiScopeKeys[1] }] };
    await resolvers.UserConnectedApp.scopes(singleScopeParent);
    await resolvers.UserConnectedApp.scopes(singleScopeParent);
    await resolvers.UserConnectedApp.scopes(multiScopesParent);
    await resolvers.UserConnectedApp.scopes(multiScopesParent);

    expect(fetchMock.calls(singleScopeUrl).length).to.equal(1);
    expect(fetchMock.calls(multiScopeUrl).length).to.equal(1);
  });
});
