import { expect } from 'chai';
import * as fetchMock from 'fetch-mock';
import { sandbox as sinonSandbox, SinonSandbox } from 'sinon';

import { getConfig } from 'common/config';

import { combinedResolvers as resolvers } from '../resolvers';

describe('Organization Uesrs Resolver', () => {

  let sandbox: SinonSandbox;

  beforeEach(() => {
    sandbox = sinonSandbox.create();
  });

  afterEach(() => {
    sandbox.reset();
    afterEach(fetchMock.restore);
  });

  const id = 'fake';

  describe('memberTotal', () => {
    it('returns empty array if the response is not ok', async () => {
      fetchMock.get(`${getConfig().orgUrl}/${id}/members/total`, {
        status: 500,
      });
      const response = await resolvers.OrganizationUsers.memberTotal({ id });
      expect(response).to.deep.equal({
        total: -1,
        enabledTotal: -1,
        disabledTotal: -1,
      });
    });
  });

  it('returns member total if response is ok', async () => {
    fetchMock.get(`${getConfig().orgUrl}/${id}/members/total`, {
      body: {
        total: 30,
        enabledTotal: 10,
        disabledTotal: 20,
      },
    });
    const response = await resolvers.OrganizationUsers.memberTotal({ id });
    expect(response).to.deep.equal({
      total: 30,
      enabledTotal: 10,
      disabledTotal: 20,
    });
  });
});
