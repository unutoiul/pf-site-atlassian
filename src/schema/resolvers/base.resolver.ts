import { GraphQLScalarType } from 'graphql';
import { Kind } from 'graphql/language';
import * as moment from 'moment';

export const resolver = {
  UnixTimestamp: new GraphQLScalarType({
    name: 'UnixTimestamp',
    description: 'The number of seconds that have passed since 1970-01-01 00:00:00 UTC',
    parseValue(value: Date) {
      return moment(value).unix();
    },
    serialize(value) {
      return moment.unix(value).toDate(); // value from the client
    },
    parseLiteral(ast) {
      if (ast.kind === Kind.INT) {
        return parseInt(ast.value, 10); // ast value is always in string format
      }

      return null;
    },
  }),

  Query: {
  },

  Mutation: {
  },
};
