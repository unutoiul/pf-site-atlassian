import * as merge from 'lodash.merge';

import { resolver as accessConfigurationResolver } from './access-configuration.resolver';
import { resolver as accessRequestsResolver } from './access-requests.resolver';
import { resolver as adminApiResolver } from './admin-api.resolver';
import { resolver as baseResolver } from './base.resolver';
import { resolver as currentUserResolver } from './current-user.resolver';
import { resolver as domainClaimResolver } from './domain-claim.resolver';
import { resolver as domainResolver } from './domain.resolver';
import { resolver as externalDirectoryResolver } from './external-directory.resolver';
import { resolver as feedbackResolver } from './feedback.resolver';
import { resolver as gSuiteResolver } from './g-suite.resolver';
import { resolver as groupResolver } from './group.resolver';
import { resolver as inviteUrlsResolver } from './invite-urls.resolver';
import { resolver as jsdResolver } from './jsd.resolver';
import { resolver as memberResolver } from './member.resolver';
import { resolver as mfaResolver } from './mfa.resolver';
import { resolver as onboardingResolver } from './onboarding.resolver';
import { resolver as organizationSecurityResolver } from './organization-security.resolver';
import { resolver as organizationUsersResolver } from './organization-users.resolver';
import { resolver as organizationResolver } from './organization.resolver';
import { resolver as passwordPolicyResolver } from './password-policy.resolver';
import { resolver as pollCreateOrgResolver } from './poll-create-org.resolver';
import { resolver as productResolver } from './products.resolver';
import { resolver as rolePermissionsResolver } from './role-permissions.resolver';
import { resolver as samlResolver } from './saml.resolver';
import { resolver as siteAccessResolver } from './site-access.resolver';
import { resolver as siteNameAvailabilityCheckResolver } from './site-name-availability-check.resolver';
import { resolver as siteRenameStatusResolver } from './site-rename-status.resolver';
import { resolver as siteRenameResolver } from './site-rename.resolver';
import { resolver as siteSettingsResolver } from './site-settings.resolver';
import { resolver as siteResolver } from './site.resolver';
import { resolver as strideResolver } from './stride.resolver';
import { resolver as twoStepVerificationResolver } from './two-step-verification.resolver';
import { resolver as userConnectedAppsScopeResolver } from './user-connected-apps-scope.resolver';
import { resolver as userConnectedAppsResolver } from './user-connected-apps.resolver';
import { resolver as userResolver } from './user.resolver';

const resolvers = [
  accessConfigurationResolver,
  accessRequestsResolver,
  adminApiResolver,
  baseResolver,
  currentUserResolver,
  domainClaimResolver,
  domainResolver,
  externalDirectoryResolver,
  feedbackResolver,
  groupResolver,
  gSuiteResolver,
  inviteUrlsResolver,
  jsdResolver,
  memberResolver,
  mfaResolver,
  onboardingResolver,
  organizationResolver,
  organizationSecurityResolver,
  organizationUsersResolver,
  passwordPolicyResolver,
  pollCreateOrgResolver,
  productResolver,
  rolePermissionsResolver,
  samlResolver,
  siteAccessResolver,
  siteNameAvailabilityCheckResolver,
  siteRenameResolver,
  siteRenameStatusResolver,
  siteResolver,
  siteSettingsResolver,
  strideResolver,
  twoStepVerificationResolver,
  userConnectedAppsResolver,
  userConnectedAppsScopeResolver,
  userResolver,
];

if (__NODE_ENV__ === 'development') {
  // tslint:disable-next-line:no-var-requires no-require-imports no-shadowed-variable
  const resolverUtils = require('../../utilities/resolvers');
  resolverUtils.checkForDuplicateResolvers(resolvers);
}

export const combinedResolvers = merge.apply(null, resolvers);
