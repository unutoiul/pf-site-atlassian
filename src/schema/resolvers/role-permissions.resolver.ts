import { apiFacade, RolePermissions } from '../api';
import { IdentityFeatureFlagApiShape } from '../api/client/identity-feature-flag-client';
import { RolePermissions as RolePermissionsDetails } from '../schema-types';

import { trustedUsersFeatureFlagKey } from '../../site/feature-flags/flag-keys';

interface RolePermissionsParams {
  cloudId: string;
  roleId: string;
}

export const resolver = {
  Query: {
    rolePermissions: async (_, { cloudId, roleId }: RolePermissionsParams): Promise<RolePermissionsDetails> => {
      const trustedUsersFeatureFlag: IdentityFeatureFlagApiShape = await apiFacade.identityFeatureFlagClient.getFeatureFlag(trustedUsersFeatureFlagKey);

      // Do not create role if the Identity trusted users feature flag is not enabled
      const shouldNotCreateRole = !trustedUsersFeatureFlag[trustedUsersFeatureFlagKey];

      return RolePermissions.create(cloudId).queryRole(roleId, shouldNotCreateRole);
    },
  },
};
