import { assert, expect } from 'chai';
import * as fetchMock from 'fetch-mock';

import { getConfig } from 'common/config';
import { RequestError } from 'common/error';

import { apiFacade } from '../../schema/api';
import { combinedResolvers as resolvers } from '../resolvers';

describe('CurrentUser Resolver', () => {
  beforeEach(() => {
    apiFacade.reset();
  });

  afterEach(() => {
    afterEach(fetchMock.restore);
    apiFacade.reset();
  });

  describe('CurrentUser organizations', () => {
    it('throws exception if the response is not ok', async () => {
      fetchMock.get(`${getConfig().orgUrl}/my`, {
        status: 500,
      });

      await resolvers.CurrentUser.organizations({})
        .then(() => assert.fail())
        .catch(e => {
          expect(e instanceof RequestError).to.equal(true);
        });
    });

    it('returns org', async () => {
      const org = {
        id: '1363002c-4b37-303f-b9e3-06734d54989a',
        name: 'Acme',
        migrationState: 'IM',
      };

      fetchMock.get(`${getConfig().orgUrl}/my`, {
        body: org,
      });

      const response = await resolvers.CurrentUser.organizations({});
      expect(response).to.deep.equal(org);
    });
  });
});
