import { getConfig } from 'common/config';
import { RequestError } from 'common/error';

import {
  ActivateMemberAccountMutationVariables,
  CancelDeleteMemberAccountMutationVariables,
  CloseMemberAccountMutationVariables,
  DeactivatedInfo,
  DeactivateMemberAccountMutationVariables,
  InitiatorNameDeactivatedInfoArgs,
  InitiatorNamePendingInfoArgs,
  LifecycleHookResult,
  Member as MemberType,
  MemberDetailsMemberDetailsArgs,
  MemberQueryArgs,
  MemberToken,
  PendingInfo,
  ResetMemberPasswordMutationVariables,
  UpdateMemberEnabledMutationVariables,
  UpdateMemberMutationVariables,
} from '../../schema/schema-types';
import { nonNullOrUndefined } from '../../utilities/arrays';
import { buildUserListBody, parseUserListResponse } from '../../utilities/directory-service';
import { fetchGetOptions } from '../../utilities/schema';
import { Member } from '../api';
import { FieldResolver } from './field-resolver';

export const resolver = {
  MemberDetails: {
    memberDetails: async (params: MemberQueryArgs, { pendingAndDeactivatedInfoReadFromIOM }: MemberDetailsMemberDetailsArgs): Promise<MemberType> => Member.init().getMember(params.id, params.memberId, pendingAndDeactivatedInfoReadFromIOM),
    canCloseAccount : async ({ memberId }): Promise<LifecycleHookResult> => {
      return Member.init().handleCanCloseMember({ accountId: memberId });
    },
    canDeactivateAccount : async ({ memberId }): Promise<LifecycleHookResult> => {
      return Member.init().handleCanDeactivateMember({ accountId: memberId });
    },
    memberTokens: async ({ memberId }): Promise<MemberToken[]> => Member.init().getMemberTokens(memberId),
  },
  Member: {
    primaryEmail: async ({ emails }) => {
      return emails.filter(nonNullOrUndefined).filter(email => email.primary).map(email => email.value)[0] || null;
    },
    provisionedBy: async ({ id }) => Member.init().getProvisioningSource(id),
  } as FieldResolver<MemberType, MemberType, 'primaryEmail' | 'provisionedBy'>,
  PendingInfo: {
    initiatorName: async ({ startedBy }, { orgId }: InitiatorNamePendingInfoArgs) => Member.init().getInitiatorName(orgId, startedBy),
  } as FieldResolver<PendingInfo, PendingInfo, 'initiatorName'>,
  DeactivatedInfo: {
    initiatorName: async ({ deactivatedBy }, { orgId }: InitiatorNameDeactivatedInfoArgs) => Member.init().getInitiatorName(orgId, deactivatedBy),
  } as FieldResolver<DeactivatedInfo, DeactivatedInfo, 'initiatorName'>,
  Query: {
    member: (_, args) => ({ id: args.id, memberId: args.memberId }),
    membersList: async (_, args) => {
      const { id, aaIds } = args;
      // Should not query if no aaIds
      if (!aaIds.length) {
        return [];
      }

      const directoryHeaders = new Headers();
      directoryHeaders.append('Content-Type', 'application/json');

      const membersListOptions: RequestInit = {
        body: buildUserListBody(id, aaIds),
        headers: directoryHeaders,
        method: 'POST',
        mode: 'cors',
        credentials: 'include',
      };

      const response = await fetch(`${getConfig().directoryUrl}/graphql`, membersListOptions);
      if (!response.ok) {
        throw new Error('Could not retrieve members list');
      }

      const json = await response.json();

      return parseUserListResponse(json);
    },
  },
  Mutation: {
    updateMember: async (_, args: UpdateMemberMutationVariables): Promise<boolean> => {
      await Member.init().updateMember(args);

      return true;
    },
    updateMemberEnabled: async (_, { id, member, enabled }: UpdateMemberEnabledMutationVariables) => {
      const settingsMutateOptions = {
        ...fetchGetOptions,
        method: enabled ? 'PUT' : 'DELETE',
      };

      const response = await fetch(`${getConfig().orgUrl}/${id}/members/${member}/active`, settingsMutateOptions);

      if (!response.ok) {
        throw new RequestError({ status: response.status });
      }

      return true;
    },
    resetMemberPassword: async (_, { id, member }: ResetMemberPasswordMutationVariables) => {
      const response = await fetch(`${getConfig().orgUrl}/${id}/members/${member}/reset-password`, { ...fetchGetOptions, method: 'PUT' });

      if (!response.ok) {
        throw new RequestError({ status: response.status });
      }

      return true;
    },
    closeMemberAccount: async (_, { memberId }: CloseMemberAccountMutationVariables) => {
      return Member.init().handleCloseMember({ accountId: memberId });
    },
    cancelDeleteMemberAccount: async (_, { memberId }: CancelDeleteMemberAccountMutationVariables) => {
      return Member.init().handleCancelDeleteMember({ accountId: memberId });
    },
    revokeMemberToken: async (_, { memberId, memberTokenId }) => {
      await Member.init().revokeMemberToken(memberId, memberTokenId);

      return true;
    },
    deactivateMemberAccount: async (_, { memberId }: DeactivateMemberAccountMutationVariables) => {
      return Member.init().handleDeactivateMember({ accountId: memberId });
    },
    activateMemberAccount: async (_, { memberId }: ActivateMemberAccountMutationVariables) => {
      return Member.init().handleActivateMember({ accountId: memberId });
    },
  },
};
