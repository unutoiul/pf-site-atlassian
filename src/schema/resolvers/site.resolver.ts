import { analyticsClient } from 'common/analytics';
import { getConfig } from 'common/config';
import { AuthenticationError, AuthorizationError } from 'common/error';
import { FeatureFlagsClient } from 'common/feature-flags';

import { csatEvalCohortFlagKey, trustedUsersCommunicationFeatureFlagKey, trustedUsersFeatureFlagKey, userConnectedAppsPageFeatureFlagKey } from '../../site/feature-flags/flag-keys';
import { util } from '../../utilities/admin-hub';
import { getCloudIdFromPath } from '../../utilities/get-id-from-path';
import { fetchGetOptions, mutateOptions } from '../../utilities/schema';
import { apiFacade } from '../api/client/api-facade';
import { IdentityFeatureFlagApiShape } from '../api/client/identity-feature-flag-client';
import { CurrentSite, Site as SiteModel } from '../api/model';
import { ChangeUserRoleMutationVariables, InviteUsersMutationVariables, RemoveUserFromSiteMutationVariables, Site, SiteQueryArgs, SiteUsersListQueryVariables } from '../schema-types';
import { PaginatedUsers, UserExportMutationArgs } from '../schema-types/schema-types';

interface Application {
  id: string;
  name: string;
  homeUrl: string;
  adminUrl: string;
  hostType: string;
}

export const resolver = {
  Site: {
    gSuite: ({ id }: Site): Pick<Site, 'id'> => ({ id }),
    jsd: ({ id }: Site): Pick<Site, 'id'> => ({ id }),
    context: async () => apiFacade.userManagementClient.getSiteContext(),
    settings: ({ id }: Site) => ({ id }),
    flags: async ({ id }: Site) => {
      const cloudAdminFFClientKey = util.isAdminHub() ? id : window.location.host;
      const cloudAdminFFClientRequest = FeatureFlagsClient.getInstance().allFlags(cloudAdminFFClientKey);

      // tslint:disable-next-line check-fetch-result - Requests issued in parallel, we're awaiting them later
      const umFFClientRequest = fetch(`${getConfig().umBasePath}/${id}/feature-flags`, fetchGetOptions);
      const cloudAdminFFClient = await cloudAdminFFClientRequest;
      const umFFClient = await umFFClientRequest;

      if (!umFFClient.ok) {
        return {
          selfSignupADG3Migration: false,
          accessConfigADG3Migration: false,
          groupsADG3Migration: cloudAdminFFClient['groups.page.adg3'] || false,
          gSuiteADG3Migration: cloudAdminFFClient['gsuite.page.adg3'] || false,
          userExportADG3Migration: false,
          jsdADG3Migration: cloudAdminFFClient['jsd.page.adg3'] || false,
          siteAdminPageMigrationRedirect: false,
          usersADG3Migration: false,
          accessRequestsPage: false,
          inviteUrls: false,
          userConnectedAppsPage: cloudAdminFFClient[userConnectedAppsPageFeatureFlagKey] || false,
          siteUrlPage: cloudAdminFFClient['site-url.page'] || false,
        };
      }

      const umFFResult = await umFFClient.json();

      return {
        selfSignupADG3Migration: !!umFFResult['identity.user_management.adg3.self.signup'],
        accessConfigADG3Migration: !!umFFResult['identity.user_management.adg3.access.config'],
        groupsADG3Migration: cloudAdminFFClient['groups.page.adg3'] || false,
        gSuiteADG3Migration: cloudAdminFFClient['gsuite.page.adg3'] || false,
        userExportADG3Migration: !!umFFResult['identity.user_management.user.export.adg3'],
        jsdADG3Migration: cloudAdminFFClient['jsd.page.adg3'] || false,
        siteAdminPageMigrationRedirect: !!umFFResult['identity.user_management.site.admin.page.migration.redirect'] || false,
        usersADG3Migration: !!umFFResult['identity.user_management.adg3.users'],
        accessRequestsPage: !!umFFResult['identity.user_management.request_access.enabled'],
        inviteUrls: !!umFFResult['identity.user_management.invite_urls.enabled'],
        userConnectedAppsPage: cloudAdminFFClient[userConnectedAppsPageFeatureFlagKey] || false,
        siteUrlPage: cloudAdminFFClient['site-url.page'] || false,
      };
    },
    flag: async ({ id }: Site, { flagKey }: { flagKey: string }) => {
      try {
        if (flagKey === csatEvalCohortFlagKey || flagKey === trustedUsersCommunicationFeatureFlagKey || flagKey === trustedUsersFeatureFlagKey) {

          const identityCohortFeatureFlag: IdentityFeatureFlagApiShape = await apiFacade.identityFeatureFlagClient.getFeatureFlag(flagKey);

          return {
            id: flagKey,
            value: identityCohortFeatureFlag[flagKey],
          };
        }

        const cloudAdminFFClientKey = util.isAdminHub() ? id : window.location.host;
        const cloudAdminFFClient = await FeatureFlagsClient.getInstance().allFlags(cloudAdminFFClientKey);

        return {
          id: flagKey,
          value: cloudAdminFFClient[flagKey],
        };
      } catch (e) {
        return {
          id: flagKey,
          value: false,
        };
      }
    },
    applications: async () => {
      try {
        const applications: Application[] = await apiFacade.userManagementClient.getApplications();

        return applications.map(({ id, name, homeUrl, adminUrl, hostType }) => ({
          id,
          name,
          adminUrl,
          url: homeUrl,
          product: hostType,
        }));
      } catch (e) {
        if (e instanceof AuthenticationError || e instanceof AuthorizationError) {
          return [];
        }

        throw e;
      }
    },
  },
  Query: {
    site: (_, { id }: SiteQueryArgs): Pick<Site, 'id'> => ({ id }),
    siteUsersList: async (_, { cloudId, input }: SiteUsersListQueryVariables): Promise<PaginatedUsers> => {
      return SiteModel.create().getUsers(cloudId, input);
    },
    currentSite: async () => {
      if (__NODE_ENV__ === 'development') {
         // tslint:disable-next-line:no-console
        console.warn('[Deprecation] currentSite is deprecated, and will be removed. Instead of using currentSite, read URL parameters to determine the cloud id, and pass that into site.',
          'See https://statlas.prod.atl-paas.net/pf-site-admin-ui-playbooks-master/playbooks-gitbook/DEPRECATIONS.html#currentSite for details',
        );
      }

      // Note: currentSite is deprecated so that we can move away from needing to do this, which would not work if we had server side graphql resolvers
      const cloudIdFromPath = getCloudIdFromPath(window.location.pathname);
      if (cloudIdFromPath) {
        return { id: cloudIdFromPath };
      }

      if ((__NODE_ENV__ === 'development' || __NODE_ENV__ === 'test') && util.isAdminHub()) {
        console.log('%cStop!', 'color: red; font-size: 30px; font-weight: bold;'); // tslint:disable-line:no-console
        console.log('%cYou are using `currentSite` GraphQL type in the AdminHub context. This will not work in prod, and will be generating nasty user-facing errors. Please, rely on URL params instead', 'color: red; font-size: 24px;'); // tslint:disable-line:no-console
        // This console.error is checked in an integration test to verify that this case is not happening
        console.error('[currentSite usage] You are using currentSite outside of any site context! Ignoring this message has caused real production issues before.'); // tslint:disable-line:no-console
      }
      const siteId: string = await CurrentSite.create().getSiteId();

      return { id: siteId };
    },
  },
  Mutation: {
    removeUserFromSite: async (_, { cloudId, id }: RemoveUserFromSiteMutationVariables): Promise<void> => {
      return SiteModel.create().removeUserFromSite(cloudId, id);
    },

    changeUserRole: async (_, { cloudId, userId, roleId }: ChangeUserRoleMutationVariables): Promise<void> => {
      await SiteModel.create().changeUserRole(cloudId, userId, roleId);
    },

    inviteUsers: async (_, { id, input }: InviteUsersMutationVariables) => {
      let productIds = input.productIds;
      if (!productIds) {
        const defaultApps = await SiteModel.create().getDefaultApps(id);
        productIds = defaultApps.map(app => app.productId);
      }

      await SiteModel.create().inviteUsers(id, {
        productIds,
        ...input,
      });
    },
    updateEmojiSettings: async (_, { id, input }) => {
      // If response fails, should revert to previous setting
      const defaultSettings = { id, uploadEnabled: !input.uploadEnabled };
      try {
        const settingsMutateOptions = {
          ...mutateOptions,
          credentials: 'include',
          method: input.uploadEnabled ? 'PUT' : 'DELETE',
        } as RequestInit;
        const response = await fetch(`${getConfig().apiUrl}/emoji/${id}/site/settings/uploadEnabled`, settingsMutateOptions);
        if (!response.ok) {
          return defaultSettings;
        }

        return {
          id,
          uploadEnabled: input.uploadEnabled,
        };
      } catch (err) {
        if (err.status !== 401 && err.status !== 403) {
          analyticsClient.onError(err);
        }

        return defaultSettings;
      }
    },

    userExport: async (_, { cloudId, input }: UserExportMutationArgs): Promise<string> => {
      return SiteModel.create().userExport(cloudId, input);
    },

  },
};
