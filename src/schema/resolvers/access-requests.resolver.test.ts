import { expect } from 'chai';
import * as fetchMock from 'fetch-mock';

import { getConfig } from 'common/config';

import { combinedResolvers as resolvers } from '../resolvers';
import { AccessRequestStatus } from '../schema-types/schema-types';

describe('Access Requests Resolver', () => {
  const cloudId = 'cloudId';
  const input = {
    status: ['PENDING'] as AccessRequestStatus[],
    start: 1,
    count: 20,
  };
  const sampleResponse = {
    total: 1,
    accessRequests: [{
      user: {
        id: '557057:80abfbd6-e1b5-4f23-99d9-077658050e25',
        displayName: 'Alex Sim',
        email: 'asimkin@atlassian.com',
        active: true,
        system: false,
      },
      requestedProducts: [{
        productId: 'confluence',
        productName: 'Confluence',
        status: 'PENDING',
        requestedTime: '2017-07-10T14:00:00Z',
      }],
    }],
  };

  describe('accessRequests', () => {
    it('returns access requests list and total', async () => {
      fetchMock.get(`${getConfig().umBasePath}/${cloudId}/access-requests?count=20&start-index=1&status=PENDING`, {
        status: 200,
        body: sampleResponse,
      });

      const response = await resolvers.Query.accessRequestList(null, { cloudId, input });
      expect(response).to.deep.equal(sampleResponse);
    });

    it('returns total from access request response', async () => {
      fetchMock.get(`${getConfig().umBasePath}/${cloudId}/access-requests?count=20&start-index=1&status=PENDING`, {
        status: 200,
        body: sampleResponse,
      });

      const total = resolvers.AccessRequestList.total(sampleResponse);
      expect(total).to.equal(sampleResponse.total);
    });

    it('returns accessRequests from access request response', async () => {
      fetchMock.get(`${getConfig().umBasePath}/${cloudId}/access-requests?count=20&start-index=1&status=PENDING`, {
        status: 200,
        body: sampleResponse,
      });

      const accessRequests = resolvers.AccessRequestList.accessRequests(sampleResponse);
      expect(accessRequests).to.equal(sampleResponse.accessRequests);
    });

    it('returns true  if approveAccess response is ok', async () => {
      fetchMock.post(`${getConfig().umBasePath}/${cloudId}/access-requests/approve`, {
        status: 200,
        body: {},
      });

      const approveAccessInput = { userId: 'userId', productId: 'productId' };

      await resolvers.Mutation.approveAccess(null, { cloudId, input: approveAccessInput });
    });

    it('returns true  if denyAccess response is ok', async () => {
      fetchMock.post(`${getConfig().umBasePath}/${cloudId}/access-requests/reject`, {
        status: 200,
        body: {},
      });
      const denyAccessInput = { userId: 'userId', productId: 'productId', denialReason: 'denied...' };

      await resolvers.Mutation.denyAccess(null, { cloudId, input: denyAccessInput });
    });
  });
});
