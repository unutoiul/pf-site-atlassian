import { expect } from 'chai';
import * as fetchMock from 'fetch-mock';
import { sandbox as sinonSandbox, SinonSandbox } from 'sinon';

import { combinedResolvers as resolvers } from '.';
import { UserConnectedAppsClient } from '../api/client/user-connected-apps-client';
import { ATLASSIAN_VENDOR_NAME } from '../api/model';

describe('UserConnectedApps', () => {

  let sandbox: SinonSandbox;
  beforeEach(() => {
    sandbox = sinonSandbox.create();
  });
  afterEach(() => {
    sandbox.reset();
    fetchMock.restore();
  });

  describe('getUserConnectedApps', () => {
    const cloudId = '00000000-0000-0000-0000-000000000000';
    const input = {
      id: cloudId,
    };
    const testJiraContext = `ari:cloud:jira:${cloudId}:project/`;
    const testJSDContext = `ari:cloud:jira-servicedesk::site/${cloudId}`;
    const testConfluenceContext = `ari:cloud:confluence::site/${cloudId}`;
    const testAccountId = '000000:00000000-0000-0000-0000-000000000000';
    const octoberGrantDate = '2018-10-25T13:00:00.000Z';
    const septemberGrantDate = '2018-09-25T13:00:00.000Z';
    const apps = [
      {
        oauthClientId: 'app1',
        name: 'App 1',
        description: 'First test app',
        avatarUrl: '/foo',
        scopes: [
          'participate:conversation',
        ],
        vendorType: 'internal',
        context: testJiraContext,
        grants: [
          {
            id: 'grant-id-01',
            accountId: testAccountId,
            createdAt: octoberGrantDate,
            oauthClientId: 'app1',
            scopes: [
              'grouped:scope:one',
              'grouped:scope:two',
            ],
            context: testJiraContext,
          },
        ],
      },
      {
        oauthClientId: 'app2',
        name: 'App 2',
        description: 'Second test app',
        avatarUrl: '/bar',
        scopes: [
          'participate:conversation',
          'read:jira-user',
        ],
        grants: [
          {
            id: 'grant-id-02',
            accountId: testAccountId,
            createdAt: octoberGrantDate,
            oauthClientId: 'app2',
            scopes: [
              'unique:scope:one',
              'duplicate:scope',
            ],
            context: testJiraContext,
          },
          {
            id: 'grant-id-03',
            accountId: testAccountId,
            createdAt: septemberGrantDate,
            oauthClientId: 'app2',
            scopes: [
              'unique:scope:two',
              'duplicate:scope',
            ],
            context: testJiraContext,
          },
        ],
        vendorType: 'thirdParty',
        vendorName: 'app1 vendor name',
        context: testJiraContext,
      },
    ];
    const expectedGrants = {
      app1: [{
        id: `grant:to:app1:by:${testAccountId}`,
        accountId: testAccountId,
        createdAt: octoberGrantDate,
        scopes: [
          { id: 'grouped:scope:one', untranslatedDescription: '', untranslatedName: '' },
          { id: 'grouped:scope:two', untranslatedDescription: '', untranslatedName: '' },
        ],
      }],
      app2: [{
        id: `grant:to:app2:by:${testAccountId}`,
        accountId: testAccountId,
        createdAt: octoberGrantDate,
        scopes: [
          { id: 'unique:scope:one', untranslatedDescription: '', untranslatedName: '' },
          { id: 'duplicate:scope', untranslatedDescription: '', untranslatedName: '' },
          { id: 'unique:scope:two', untranslatedDescription: '', untranslatedName: '' },
        ],
      }],
    };
    const url = UserConnectedAppsClient.buildAppsUrl([testJiraContext, testJSDContext, testConfluenceContext], 0, 20);

    it('sets app details, grant details, scopes and vendor details', async () => {
      fetchMock.get(url, {
        status: 200,
        body: {
          values: apps,
          size: apps.length,
        },
      });

      const response = await resolvers.Site.userConnectedApps(input);
      expect(response).to.deep.equal(apps.map(
        ({ oauthClientId, name, description, avatarUrl, vendorName, vendorType, scopes }) => {
          return {
            id: oauthClientId,
            name,
            description,
            avatarUrl,
            vendorName: vendorType === 'internal' ? ATLASSIAN_VENDOR_NAME : vendorName,
            scopes: scopes.map(id => ({ id, untranslatedDescription: '', untranslatedName: '' })),
            userGrants: expectedGrants[oauthClientId],
          };
        }));

    });

  });
});
