import * as merge from 'lodash.merge';

import { defaults as analyticsDefaults, resolvers as analyticsResolvers } from './analytics.resolver';
import { defaults as feedbackDefaults, resolvers as feedbackResolvers } from './feedback.resolver';
import { defaults as navigationDefaults, resolvers as navigationResolvers } from './navigation.resolver';
import { defaults as xflowDefaults, resolvers as xflowResolvers } from './xflow.resolver';

export const uiState = {
  defaults: {
    ui: {
      __typename: 'UI',
      ...feedbackDefaults,
      ...navigationDefaults,
      ...analyticsDefaults,
      ...xflowDefaults,
    },
  },
  resolvers: {
    ...merge(
      feedbackResolvers,
      navigationResolvers,
      xflowResolvers,
      analyticsResolvers,
    ),
  },
};
