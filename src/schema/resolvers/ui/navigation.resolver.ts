export const defaults = {
  navigation: {
    showContainer: false,
    activeDrawer: false,
    __typename: 'Navigation',
  },
};

export const resolvers = {
  Mutation: {
    updateNavigationContainer: (_, { showContainer }, { cache }) => {
      cache.writeData({
        data: {
          ui: {
            navigation: {
              showContainer,
              __typename: 'Navigation',
            },
            __typename: 'UI',
          },
        },
      });

      return null;
    },
    updateActiveDrawer: (_, { activeDrawer }, { cache }) => {
      cache.writeData({
        data: {
          ui: {
            navigation: {
              activeDrawer,
              __typename: 'Navigation',
            },
            __typename: 'UI',
          },
        },
      });

      return null;
    },
  },
};
