export const defaults = {
  xflow: {
    isDialogOpen: false,
    sourceComponent: null,
    targetProduct: null,
    __typename: 'XFlow',
  },
};

export const resolvers = {
  Mutation: {
    updateXFlowDialog: (_, { isDialogOpen, sourceComponent, targetProduct }, { cache }) => {
      cache.writeData({
        data: {
          ui: {
            xflow: {
              isDialogOpen,
              sourceComponent,
              targetProduct,
              __typename: 'XFlow',
            },
            __typename: 'UI',
          },
        },
      });

      return null;
    },
  },
};
