export const defaults = {
  isFeedbackOpen: false,
};

export const resolvers = {
  Mutation: {
    updateIsFeedbackOpen: (_, { isOpen: isFeedbackOpen }, { cache }) => {
      cache.writeData({
        data: {
          ui: {
            isFeedbackOpen,
            __typename: 'UI',
          },
        },
      });

      return null;
    },
  },
};
