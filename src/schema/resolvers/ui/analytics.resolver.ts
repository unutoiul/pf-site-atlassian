export const defaults = {
  analytics: {
    referrer: 'direct',
    __typename: 'Analytics',
  },
};

export const resolvers = {
  Mutation: {
    updateReferrer: (_, { referrer }, { cache }) => {
      cache.writeData({
        data: {
          ui: {
            analytics: {
              referrer,
              __typename: 'Analytics',
            },
            __typename: 'UI',
          },
        },
      });

      return null;
    },
  },
};
