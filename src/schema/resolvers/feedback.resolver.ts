import { analyticsClient } from 'common/analytics';
import { getConfig } from 'common/config';
import { RequestError } from 'common/error';

export const resolver = {
  Mutation: {
    submitFeedback: async (_, { comment, email, webInfo }) => {
      function truncate(text: string) {
        if (text.length < 50) {
          return text;
        }

        return text.substring(0, 49) + '...';
      }

      let response;

      try {
        // tslint:disable-next-line:check-fetch-result
        response = await fetch(`${getConfig().feedbackUrl}/api/feedback`, {
          method: 'POST',
          headers: new Headers({ 'content-type': 'application/json' }),
          credentials: 'omit',
          body: JSON.stringify({
            collectorId: '3183179d',
            data: {
              description: comment,
              summary: truncate(comment),
              email,
              webInfo,
            },
          }),
        });
      } catch (e) {
        analyticsClient.onError(e);

        throw new RequestError({
          message: `feedback submission failed: ${e.message}`,
          ignore: true,
        });
      }

      if (response.ok) {
        return;
      }

      const e = new RequestError({
        status: response.status,
        message: `feedback submission failed: ${response.status}`,
        ignore: true,
      });

      analyticsClient.onError(e);

      throw e;
    },
  },
};
