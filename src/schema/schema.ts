import { makeExecutableSchema } from 'graphql-tools';

import { typeDefs } from '../schema/schema-defs';
import { combinedResolvers } from './resolvers';

export const schema = makeExecutableSchema({
  typeDefs,
  resolvers: combinedResolvers,
});
