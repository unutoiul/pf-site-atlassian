import { stringify } from 'query-string';

import { getConfig } from 'common/config';
import { RequestError } from 'common/error';

import { util } from '../../../utilities/admin-hub';
import { MemberEmail, MemberStateFilter, MemberStatus } from '../../schema-types';
import { RequestHelper } from './request-helper';

export type OrganizationMigrationState = 'ORGS' | 'IM_GRANDFATHERED' | 'IM';

export interface OrganizationApiShape {
  id: string;
  name: string;
  migrationState: OrganizationMigrationState | null;
}

export interface MemberDetailsApiResponse {
  active: MemberStatus;
  displayName: string;
  emails: Array<MemberEmail | null>;
  id: string;
  jobTitle?: string | null;
  useMfa?: boolean | null;
  memberAccess: MemberAccessApiResponse | null;
  pendingInfo?: MemberPendingInfo | null;
  deactivatedInfo?: MemberDeactivatedInfo | null;
}

export interface MemberPendingInfo {
  startedBy: string;
  startedOn: string;
}

export interface MemberDeactivatedInfo {
  deactivatedBy: string;
  deactivatedOn: string;
}

export interface MemberAccessApiResponse {
  sites: Array<{
    siteUrl: string;
    siteAdmin: boolean;
  }>;
  products: Array<{
    productName: string;
    productKey: string;
    siteUrl: string;
  }>;
  errors?: string[];
}

export interface MembersApiResponse {
  total: number;
  users: MemberDetailsApiResponse[] | null;
}

export interface MembersFilterArgs {
  start?: number | null;
  count?: number | null;
  active?: boolean | null;
  mfa?: boolean | null;
  displayName?: string | null;
  domain?: string | null;
  filter?: string | null;
  products?: string[] | null;
  memberState?: MemberStateFilter | null;
}

export interface OrganizationCreationRequestParams {
  name: string;
}

export interface OrganizationRenameRequestParams {
  id: string;
  name: string;
}

export interface OrganizationCreationResponse {
  id: string;
  progressUri: string;
}

export interface UnlinkedSites {
  sites: UnlinkedSiteInfo[];
}

export interface UnlinkedSiteInfo {
  id: string;
  siteUrl: string;
  products: ProductShape[] | null;
}

export interface CheckDomainShape {
  claimed: boolean;
}

interface AtlassianAccessEvaluationResponse {
  id: string;
  progressUri: string;
}

interface EvaluationPollingResponse {
  completed: boolean;
  failureReason: any;
  orgId: string;
  requestId: number;
  requestType: string; // e.g. "CREATE_ORG_ENTITLEMENT"
  successful: boolean;
}

interface Email {
  value: string;
  primary: boolean;
}

type MemberStateFilterValues = 'enabled' | 'disabled' | 'billable' | 'nonBillable' | undefined;

export interface SiteAdminApiShape {
  id: string;
  displayName: string;
  emails: Email[];
  orgAdmin: boolean;
}

export interface LinkedSiteShape {
  id: string;
  displayName: string | null;
  avatar: string | null;
  siteUrl: string;
  products?: ProductShape[] | null;
}

export interface SiteAdminsResponse {
  users: SiteAdminApiShape[];
}

export interface ProductShape {
  productKey: string;
  productName: string;
}

export interface AddOrganizationAdminArgs {
  orgId: string;
  email: string;
}

const handleProducts = (response) => {
  return response.map(({ products, ...rest }) => {
    const compatibleProducts = products && products.map(product => {
      if (typeof product === 'string') {
        return {
          productKey: product,
          productName: product,
        };
      }

      return product;
    }) || null;

    return {
      ...rest,
      products: compatibleProducts,
    };
  });
};

const convertGraphQLFilterTypeToBackendFilterType = (graphqlType: MemberStateFilter): MemberStateFilterValues => {
  const graphQLToBackendValues: {
    [key in MemberStateFilter]: MemberStateFilterValues
  } = {
    ALL: undefined,
    ENABLED: 'enabled',
    DISABLED: 'disabled',
    BILLABLE: 'billable',
    NON_BILLABLE: 'nonBillable',
  };

  return graphQLToBackendValues[graphqlType];
};

export class OrganizationClient {
  private static allOrgsStoreKey = 'orgs-all';
  private static linkedSitesStoreKey = (orgId: string) => `linked-sites-for-${orgId}`;

  constructor(
    private requestHelper: RequestHelper,
  ) {
  }

  public async getAllOrgs(): Promise<OrganizationApiShape[]> {
    // this is the only request that we proxy through Stargate in site admin environment,
    const requestUrl = util.isAdminHub()
      ? `${getConfig().orgUrl}/my`
      : `${getConfig().apiUrl}/adminhub/site-admin-orgs/my`;

    return this.requestHelper.getJson({
      url: requestUrl,
      requestId: `organizations-all`,
      storeStrategy: 'cache',
      storeKey: OrganizationClient.allOrgsStoreKey,
    });
  }

  public async renameOrganization({ id, name }: OrganizationRenameRequestParams): Promise<void> {
    await this.requestHelper.putJson({
      url: `${getConfig().orgUrl}/${id}/rename`,
      requestId: 'org-rename',
      body: { name },
    });

    this.clearOrgsCache();
  }

  public async addOrganizationAdmin({ orgId, email }: AddOrganizationAdminArgs): Promise<void> {
    await this.requestHelper.postJson({
      url: `${getConfig().orgUrl}/${orgId}/admin/invite`,
      requestId: 'add-org-admin',
      body: { emails: [email] },
    });
  }

  public async getOrganizationAdmins(orgId: string): Promise<MembersApiResponse> {
    return this.requestHelper.getJson<MembersApiResponse>({
      url: `${getConfig().orgUrl}/${orgId}/admin`,
      requestId: 'get-admins',
      storeStrategy: 'none',
    });
  }

  public async evaluateAtlassianAccess(orgId: string): Promise<void> {
    const response = await this.requestHelper.putJson<AtlassianAccessEvaluationResponse, {}>({
      url: `${getConfig().orgUrl}/${orgId}/product/identity-manager`,
      requestId: 'atlassian-access-evaluation',
      body: JSON.stringify({}),
    });

    const pollFn = async () => {
      const evaluationResult = await this.requestHelper.getJson<EvaluationPollingResponse>({
        url: response.progressUri,
        requestId: 'atlassian-access-evaluation-polling',
      });

      return evaluationResult.completed && evaluationResult.successful;
    };

    await this.requestHelper.poll(pollFn, new RequestError({ ignore: true, message: 'polling for evaluation success did not finish in time' }));
  }

  public async createOrganization(params: OrganizationCreationRequestParams): Promise<OrganizationCreationResponse> {
    const result: OrganizationCreationResponse = await this.requestHelper.postJson<OrganizationCreationResponse, OrganizationCreationRequestParams>({
      url: `${getConfig().orgUrl}/create`,
      requestId: 'org-rename',
      body: params,
    });
    this.clearOrgsCache();

    return result;
  }

  public async initiateMemberCsvEmailExport(orgId: string): Promise<void> {
    await this.requestHelper.postJson({
      url: `${getConfig().orgUrl}/${orgId}/members/export`,
      storeStrategy: 'none',
      requestId: 'org-member-export',
    });
  }

  public async getMember(orgId: string, memberId: string): Promise<MemberDetailsApiResponse> {
    return this.requestHelper.getJson({
      url: `${getConfig().orgUrl}/${orgId}/members/${memberId}`,
      requestId: `get-member`,
    });
  }

  public async getMembers(orgId: string, { count, active, displayName, domain, mfa, start, filter, products, memberState }: MembersFilterArgs): Promise<MembersApiResponse> {
    const params = stringify({
      count,
      'start-index': start,
      active,
      mfa,
      'display-name': displayName,
      domains: domain,
      filter,
      products,
      'member-state': memberState ? convertGraphQLFilterTypeToBackendFilterType(memberState) : undefined,
    });

    return this.requestHelper.getJson<MembersApiResponse>({
      url: `${getConfig().orgUrl}/${orgId}/members${params !== '' ? '?' : ''}${params}`,
      requestId: 'get-members',
      storeStrategy: 'none',
    });
  }

  public async getMemberExport(orgId: string, exportId: string): Promise<string> {
    const url = `${getConfig().orgUrl}/${orgId}/members/export/${encodeURIComponent(exportId)}`;

    return this.requestHelper.getText(url);
  }

  public async getSiteAdmins(orgId: string, cloudId: string): Promise<SiteAdminsResponse> {
    return this.requestHelper.getJson<SiteAdminsResponse>({
      url: `${getConfig().orgUrl}/${orgId}/sites/${cloudId}/admins`,
      requestId: 'org-site-admins',
    });
  }

  public async linkSite(orgId: string, cloudId: string): Promise<void> {
    await this.requestHelper.postJson({
      url: `${getConfig().orgUrl}/${orgId}/sites`,
      requestId: 'link-org-site',
      body: { id: cloudId },
    });
    this.clearLinkedSitesCache(orgId);
  }

  public async getSitesNotLinkedToOrg(): Promise<UnlinkedSites> {
    const response = await this.requestHelper.getJson({
      url: `${getConfig().orgUrl}/sites/unlinked-sites`,
      requestId: 'unlinked-sites',
    });

    return {
      sites: handleProducts(response.sites),
    };
  }

  public async getSitesLinkedToOrg(orgId: string): Promise<LinkedSiteShape[]> {
    const response = await this.requestHelper.getJson({
      url: `${getConfig().orgUrl}/${orgId}/sites`,
      requestId: 'linked-sites',
      storeStrategy: 'cache',
      storeKey: OrganizationClient.linkedSitesStoreKey(orgId),
    });

    return handleProducts(response);
  }

  public async getProducts(orgId: string): Promise<ProductShape[]> {
    return this.requestHelper.getJson({
      url: `${getConfig().orgUrl}/${orgId}/product-definitions`,
      requestId: 'product-definitions',
    });
  }

  public async checkDomainClaimed(domain: string): Promise<CheckDomainShape> {
    return this.requestHelper.getJson({
      // https://id-org-manager.staging.atl-paas.net/webjars/swagger-ui/index.html?url=https%3A%2F%2Fid-org-manager.staging.atl-paas.net%2Fapi%2Fswagger.json#/domain/Check_domain_already_owned_by_an_org
      url: `${getConfig().orgUrl}/domains/${domain}/ownership-check`,
      requestId: 'check-domain',
    });
  }

  private clearOrgsCache(): void {
    this.requestHelper.invalidate(OrganizationClient.allOrgsStoreKey);
  }

  private clearLinkedSitesCache(orgId: string): void {
    this.requestHelper.invalidate(OrganizationClient.linkedSitesStoreKey(orgId));
  }
}
