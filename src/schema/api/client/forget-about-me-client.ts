import { getConfig } from 'common/config';

import { DeactivatedInfo, LifecycleHookResult, PendingInfo } from '../../schema-types/schema-types';
import { RequestHelper } from './request-helper';

export interface AccountDisabledStateResponse {
  pendingInfo?: PendingInfo | null;
  deactivatedInfo?: DeactivatedInfo | null;
}

export class ForgetAboutMeClient {
  constructor(private requestHelper: RequestHelper) {}

  public async canCloseAccount({ accountId }): Promise<LifecycleHookResult> {
    return this.requestHelper.getJson<LifecycleHookResult>({
      url: `${getConfig().apiUrl}/users/${accountId}/can-close`,
      requestId: `get-can-close-user-reasons`,
      storeStrategy: 'none',
      defaultValue: { warnings: [], errors: [] },
    });
  }

  public async canDeactivateAccount({ accountId }): Promise<LifecycleHookResult> {
    return this.requestHelper.getJson<LifecycleHookResult>({
      url: `${getConfig().apiUrl}/users/${accountId}/can-deactivate`,
      requestId: `get-can-deactivate-user-reasons`,
      storeStrategy: 'none',
      defaultValue: { warnings: [], errors: [] },
    });
  }

  public async getDisabledState({ accountId }): Promise<AccountDisabledStateResponse> {
    return this.requestHelper.getJson<AccountDisabledStateResponse>({
      url: `${getConfig().apiUrl}/users/${accountId}/disabled-states`,
      requestId: `get-user-disabled-states`,
      storeStrategy: 'none',
      defaultValue: {},
    });
  }

  public async closeAccount({ accountId }): Promise<any> {
    return this.requestHelper.deleteJson<any>({
      url: `${getConfig().apiUrl}/users/${accountId}`,
      requestId: `delete-account`,
      storeStrategy: 'none',
    });
  }

  public async cancelDeleteAccount({ accountId }): Promise<any> {
    return this.requestHelper.postJson<any, any>({
      url: `${getConfig().apiUrl}/users/${accountId}/cancel-deletion`,
      requestId: `cancel-delete-account`,
      storeStrategy: 'none',
    });
  }

  public async deactivateAccount({ accountId }): Promise<any> {
    return this.requestHelper.postJson<any, any>({
      url: `${getConfig().apiUrl}/users/${accountId}/deactivate`,
      requestId: `deactivate-account`,
      storeStrategy: 'none',
    });
  }

  public async activateAccount({ accountId }): Promise<any> {
    return this.requestHelper.postJson<any, any>({
      url: `${getConfig().apiUrl}/users/${accountId}/activate`,
      requestId: `activate-account`,
      storeStrategy: 'none',
    });
  }
}
