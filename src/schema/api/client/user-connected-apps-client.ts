import { getConfig } from 'common/config';

import { RequestHelper } from './request-helper';

export interface AppInstallationQueryResponse {
  _links: {
    self: string;
    prev?: string;
    next?: string;
  };
  values: AppInstallationSummary[];
  start: number;
  limit: number;
  size: number;
}

export interface AppInstallationSummary {
  _links: {
    self: string;
    definition: string;
    installedData?: string;
    installedUrl?: string;
  };
  id: string;
  context: string;
  enabled: boolean;
  installTime: string;
  installerAaid?: string;
  grants?: AppGrant[];
  definitionId: string;
  key: string;
  name?: string;
  description?: string;
  application?: string;
  version?: string;
  latest: boolean;
  oauthClientId?: string;
  oauthRedirectUrl?: string;
  principalId?: string;
  avatarUrl?: string;
  baseUrl?: string;
  origin: 'marketplace' | 'dac' | 'token' | 'other';
  scopes?: string[];
  token?: string;
  privacyPolicy?: string;
  termsOfService?: string;
  contactLink?: string;
  vendorType?: 'internal' | 'thirdParty';
  vendorName?: string;
}

export interface AppGrant {
  id?: string;
  accountId?: string;
  audience?: string;
  oauthClientId: string;
  context?: string;
  type: 'client' | 'user';
  createdAt: string;
  scopes: string[];
}

export interface ScopesResponse {
  _links: {
    self: string;
    [k: string]: any;
  };
  values: ScopeDetails[];
  [k: string]: any;
}

export interface ScopeDetails {
  _links: {
    self: string;
    [k: string]: any;
  };
  key: string;
  serviceId: string;
  name: string;
  description: string;
  [k: string]: any;
}

export class UserConnectedAppsClient {
  constructor(private requestHelper: RequestHelper) {
  }

  public static buildAppsUrl(contexts: string[], start: number, limit: number): string {
    return `${getConfig().apiUrl}/apps-platform/app/installation?${
      contexts.map(ari => `context=${encodeURIComponent(ari)}`).join('&')
      }&start=${start}&limit=${limit}&auth-type=3lo`;
  }

  public static buildScopesUrl(scopeKeys: string[]): string {
    const scopesList = [...new Set(scopeKeys)].map(encodeURIComponent).sort();

    return `${getConfig().caasUrl}/scope?${scopesList.map(scope => `scopeKey=${scope}`).join('&')}`;
  }

  /**
   * Returns the AddonInstallationSummary[] for all contexts applicable to the cloudId (site).
   * Note that a single app may have multiple installations in this response.
   *
   * @param {string} cloudId The site's cloudId.
   * @returns {Object[]} The app installations for the site's contexts.
   */
  public async getAppInstallations(cloudId: string): Promise<AppInstallationSummary[]> {
    const contexts = [
      `ari:cloud:jira:${cloudId}:project/`,
      `ari:cloud:jira-servicedesk::site/${cloudId}`,
      `ari:cloud:confluence::site/${cloudId}`,
    ];

    let total;
    let start = 0;
    const limit = 20;
    const url = UserConnectedAppsClient.buildAppsUrl(contexts, start, limit);

    let appInstallations: AppInstallationSummary[] = [];
    do {
      const installationsResponseBody = await this.requestHelper.getJson({
        url,
        requestId: `user-connected-app-installations`,
        storeStrategy: 'cache',
        storeKey: url,
      }) as AppInstallationQueryResponse;

      appInstallations = appInstallations.concat(installationsResponseBody.values);

      total = installationsResponseBody.size;
      start += limit;
    } while (start < total);

    return appInstallations;
  }

  public async fetchScopes(scopes: string[]) {
    const url = UserConnectedAppsClient.buildScopesUrl(scopes);

    return this.requestHelper.getJson<ScopesResponse>({
      url,
      requestId: 'user-connected-app-scopes',
      storeStrategy: 'cache',
      storeKey: url,
    });
  }
}
