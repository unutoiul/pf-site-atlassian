import { assert, expect } from 'chai';
import * as fetchMock from 'fetch-mock';

import { AuthenticationError, AuthorizationError, GoneError, PreconditionFailedError } from 'common/error';

import { ConflictError } from 'common/error/conflict-error';

import { RequestHelper } from './request-helper';
import { StoreStrategy } from './request-store';

describe('RequestHelper', () => {
  let client: RequestHelper;

  beforeEach(() => {
    client = new RequestHelper();
  });

  afterEach(() => {
    fetchMock.restore();
  });

  describe('getText', () => {

    it('should return text', async () => {
      const url = `/rest/my/resource`;
      fetchMock.get(url, {
        status: 200,
        body: 'hello text',
      });
      const response = await client.getText(url);
      expect(response).to.equal('hello text');
    });

    it('should throw AuthenticationError for requests which result in 401', async () => {
      const url = `/rest/my/resource`;
      fetchMock.get(url, { status: 401 }, { name: 'my-request' });

      await client.getText(url).then(() => assert.fail())
        .catch(e => expect(e instanceof AuthenticationError).to.equal(true));
    });

    it('should throw AuthorizationError for requests which result in 403', async () => {
      const url = `/rest/my/resource`;
      fetchMock.get(url, { status: 403 }, { name: 'my-request' });

      await client.getText(url).then(() => assert.fail())
        .catch(e => expect(e instanceof AuthorizationError).to.equal(true));
    });

    it('should throw GoneError for requests which result in 410', async () => {
      const url = `/rest/my/resource`;
      fetchMock.get(url, { status: 410 }, { name: 'my-request' });

      await client.getText(url).then(() => assert.fail())
        .catch(e => expect(e instanceof GoneError).to.equal(true));
    });

    it('should throw ConflictError for requests which result in 409', async () => {
      const url = `/rest/my/resource`;
      fetchMock.get(url, { status: 409 }, { name: 'my-request' });

      await client.getText(url).then(() => assert.fail())
        .catch(e => expect(e instanceof ConflictError).to.equal(true));
    });

  });

  describe('getJson', () => {
    describe('common', () => {
      const storeStrategies: Array<StoreStrategy | undefined> = ['cache', 'memoize', 'none', undefined];

      storeStrategies.forEach(ss => it(`should return json if the response is ok with storeStrategy=${JSON.stringify(ss)}`, async () => {
        const url = `/rest/my/resource`;
        fetchMock.get(url, {
          status: 200,
          body: [{ app: 1 }, { app: 2 }],
        });
        const response = await client.getJson({ url, requestId: 'stub-request', storeStrategy: ss as any, storeKey: 'sk' });
        expect(response).to.deep.equal([{ app: 1 }, { app: 2 }]);
      }));

      storeStrategies.forEach(ss => it(`should throw an AuthenticationError if the response is 401 with storeStrategy=${JSON.stringify(ss)}`, async () => {
        const url = `/rest/my/resource`;
        fetchMock.get(url, {
          status: 401,
        });

        await client.getJson({ url, requestId: 'stub-request', storeStrategy: ss as any, storeKey: 'sk' })
          .then(() => assert.fail())
          .catch(e => expect(e instanceof AuthenticationError).to.equal(true));
      }));

      storeStrategies.forEach(ss => it(`should throw an AuthorizationError if the response is 403 with storeStrategy=${JSON.stringify(ss)}`, async () => {
        const url = `/rest/my/resource`;
        fetchMock.get(url, {
          status: 403,
        });

        await client.getJson({ url, requestId: 'stub-request', storeStrategy: ss as any, storeKey: 'sk' })
          .then(() => assert.fail())
          .catch(e => expect(e instanceof AuthorizationError).to.equal(true));
      }));

      storeStrategies.forEach(ss => it(`should throw a GoneError if the response is 410 with storeStrategy=${JSON.stringify(ss)}`, async () => {
        const url = `/rest/my/resource`;
        fetchMock.get(url, {
          status: 410,
        });

        await client.getJson({ url, requestId: 'stub-request', storeStrategy: ss as any, storeKey: 'sk' })
          .then(() => assert.fail())
          .catch(e => expect(e instanceof GoneError).to.equal(true));
      }));

      storeStrategies.forEach(ss => it(`should return default value if reading from the json fails with storeStrategy=${JSON.stringify(ss)}`, async () => {
        const url = `/rest/my/resource`;
        fetchMock.get(url, {
          status: 200,
          body: 'Some invalid json response',
        });

        const response = await client.getJson({ url, requestId: 'stub-request', storeStrategy: ss as any, storeKey: 'sk', defaultValue: [] });

        expect(response).to.deep.equal([]);
      }));

      storeStrategies.forEach(ss => it(`should throw error if reading from the json fails and default value is not specified with storeStrategy=${JSON.stringify(ss)}`, async () => {
        const url = `/rest/my/resource`;
        fetchMock.get(url, {
          status: 200,
          body: 'Some invalid json response',
        });

        await client.getJson({ url, requestId: 'stub-request', storeStrategy: ss as any, storeKey: 'sk' })
          .then(() => assert.fail())
          .catch(e => expect(e instanceof Error).to.equal(true));
      }));
    });

    it('should not re-fetch even if new response would have updated data when storeStrategy is "cache"', async () => {
      const url = `/rest/my/resource`;
      fetchMock.get(url, {
        status: 200,
        body: [{ app: 1 }, { app: 2 }],
      }, { name: 'my-request' });
      await client.getJson({ url, requestId: 'stub-request', storeStrategy: 'cache', storeKey: 'sk' });
      expect(fetchMock.called('my-request')).to.equal(true);

      fetchMock.restore();
      const response2 = await client.getJson({ url, requestId: 'stub-request', storeStrategy: 'cache', storeKey: 'sk' });

      expect(response2).to.deep.equal([{ app: 1 }, { app: 2 }]);
      expect(fetchMock.called('my-request')).to.equal(false);
    });

    it('should re-fetch data after cache entry has been invalidated when storeStrategy is "cache"', async () => {
      const url = `/rest/my/resource`;
      fetchMock.get(url, {
        status: 200,
        body: [{ app: 1 }, { app: 2 }],
      }, { name: 'my-request' });

      await client.getJson({ url, requestId: 'stub-request', storeStrategy: 'cache', storeKey: 'sk' });
      expect(fetchMock.called('my-request')).to.equal(true);

      fetchMock.restore();
      fetchMock.get(url, { status: 200, body: { updated: true } }, { name: 'my-request' });
      client.invalidate('sk');
      const response2 = await client.getJson({ url, requestId: 'stub-request', storeStrategy: 'cache', storeKey: 'sk' });

      expect(response2).to.deep.equal({ updated: true });
      expect(fetchMock.called('my-request')).to.equal(true);
    });

    it('should not re-fetch until the first response has arrived when storeStrategy is "memoize"', async () => {
      const url = `/rest/my/resource`;
      fetchMock.get(url, {
        status: 200,
        body: [{ app: 1 }, { app: 2 }],
      }, { name: 'my-request' });

      const [result1, result2] = await Promise.all([
        client.getJson({ url, requestId: 'stub-request', storeStrategy: 'memoize', storeKey: 'sk' }),
        client.getJson({ url, requestId: 'stub-request', storeStrategy: 'memoize', storeKey: 'sk' }),
        client.getJson({ url, requestId: 'stub-request', storeStrategy: 'memoize', storeKey: 'sk' }),
        client.getJson({ url, requestId: 'stub-request', storeStrategy: 'memoize', storeKey: 'sk' }),
        client.getJson({ url, requestId: 'stub-request', storeStrategy: 'memoize', storeKey: 'sk' }),
      ]);

      expect(fetchMock.called('my-request')).to.equal(true);
      expect(fetchMock.calls('my-request')).to.have.lengthOf(1);
      expect(result1).to.deep.equal(result2);
      expect(result1).to.deep.equal([{ app: 1 }, { app: 2 }]);

      fetchMock.restore();
      fetchMock.get(url, {
        status: 200,
        body: { changed: true },
      }, { name: 'my-request' });
      const nextResponse = await client.getJson({ url, requestId: 'stub-request', storeStrategy: 'memoize', storeKey: 'sk' });

      expect(nextResponse).to.deep.equal({ changed: true });
      expect(fetchMock.called('my-request')).to.equal(true);
    });

    it('should re-fetch data after cache entry has been invalidated when storeStrategy is "memoize"', async () => {
      const url = `/rest/my/resource`;
      fetchMock.get(url, {
        status: 200,
        body: [{ app: 1 }, { app: 2 }],
      }, { name: 'my-request' });

      await client.getJson({ url, requestId: 'stub-request', storeStrategy: 'memoize', storeKey: 'sk' });
      expect(fetchMock.called('my-request')).to.equal(true);

      fetchMock.restore();
      fetchMock.get(url, { status: 200, body: { updated: true } }, { name: 'my-request' });
      client.invalidate('sk');
      const response2 = await client.getJson({ url, requestId: 'stub-request', storeStrategy: 'memoize', storeKey: 'sk' });

      expect(response2).to.deep.equal({ updated: true });
      expect(fetchMock.called('my-request')).to.equal(true);
    });

    it('should re-fetch after the first request has erred when storeStrategy is "memoize"', async () => {
      const url = `/rest/my/resource`;

      try {
        fetchMock.get(url, { throws: {} }, { name: 'my-request' });

        await client.getJson({ url, requestId: 'stub-request', storeStrategy: 'memoize', storeKey: 'sk' });

        expect.fail('Expected `client.getJson` call to fail, but it did not');
      } catch (_) {
        expect(fetchMock.called('my-request')).to.equal(true);
        expect(fetchMock.calls('my-request')).to.have.lengthOf(1);

        fetchMock.restore();
        fetchMock.get(url, {
          status: 200,
          body: { changed: true },
        }, { name: 'my-request' });

        const nextResponse = await client.getJson({ url, requestId: 'stub-request', storeStrategy: 'memoize', storeKey: 'sk' });
        expect(nextResponse).to.deep.equal({ changed: true });
        expect(fetchMock.called('my-request')).to.equal(true);
      }
    });
  });

  describe('204 response', () => {
    it('should return undefined for GET requests which result in 204', async () => {
      const url = `/rest/my/resource`;
      fetchMock.get(url, { status: 204 }, { name: 'my-request' });

      const result = await client.getJson({ url, requestId: 'stub-request' });

      expect(fetchMock.called('my-request')).to.equal(true);
      expect(fetchMock.calls('my-request')).to.have.lengthOf(1);
      expect(result).to.equal(undefined);
    });

    it('should return undefined for POST requests which result in 204', async () => {
      const url = `/rest/my/resource`;
      fetchMock.post(url, { status: 204 }, { name: 'my-request' });

      const result = await client.postJson({ url, requestId: 'stub-request', body: {} });

      expect(fetchMock.called('my-request')).to.equal(true);
      expect(fetchMock.calls('my-request')).to.have.lengthOf(1);
      expect(result).to.equal(undefined);
    });

    it('should return undefined for PUT requests which result in 204', async () => {
      const url = `/rest/my/resource`;
      fetchMock.put(url, { status: 204 }, { name: 'my-request' });

      const result = await client.putJson({ url, requestId: 'stub-request', body: {} });

      expect(fetchMock.called('my-request')).to.equal(true);
      expect(fetchMock.calls('my-request')).to.have.lengthOf(1);
      expect(result).to.equal(undefined);
    });

    it('should return undefined for PATCH requests which result in 204', async () => {
      const url = `/rest/my/resource`;
      fetchMock.patch(url, { status: 204 }, { name: 'my-request' });

      const result = await client.patchJson({ url, requestId: 'stub-request', body: {} });

      expect(fetchMock.called('my-request')).to.equal(true);
      expect(fetchMock.calls('my-request')).to.have.lengthOf(1);
      expect(result).to.equal(undefined);
    });

    it('should return undefined for DELETE requests which result in 204', async () => {
      const url = `/rest/my/resource`;
      fetchMock.delete(url, { status: 204 }, { name: 'my-request' });

      const result = await client.deleteJson({ url, requestId: 'stub-request' });

      expect(fetchMock.called('my-request')).to.equal(true);
      expect(fetchMock.calls('my-request')).to.have.lengthOf(1);
      expect(result).to.equal(undefined);
    });
  });

  describe('409 response', () => {
    it('should throw ConflictError for PUT requests', async () => {
      const url = `/rest/my/resource`;
      fetchMock.put(url, { status: 409, body: { message: 'Oh no...' } }, { name: 'my-request' });

      await client.putJson({ url, body: {}, requestId: 'stub-request' })
        .then(() => assert.fail())
        .catch(e => {
          expect(e instanceof ConflictError).to.equal(true);
          expect(e.message).to.equal('Oh no...');
        });
    });

    it('should throw ConflictError for PATCH requests', async () => {
      const url = `/rest/my/resource`;
      fetchMock.patch(url, { status: 409, body: { message: 'Oh no...' } }, { name: 'my-request' });

      await client.patchJson({ url, body: {}, requestId: 'stub-request' })
        .then(() => assert.fail())
        .catch(e => {
          expect(e instanceof ConflictError).to.equal(true);
          expect(e.message).to.equal('Oh no...');
        });
    });
  });

  describe('412 response', () => {
    it('should throw PreconditionFailedError', async () => {
      const url = `/rest/my/resource`;
      fetchMock.put(url, { status: 412 }, { name: 'my-request' });

      await client.putJson({ url, body: {}, requestId: 'stub-request' })
        .then(() => assert.fail())
        .catch(e => {
          expect(e instanceof PreconditionFailedError).to.equal(true);
        });
    });
  });

  describe('fetchOptions', () => {
    it('should merge headers object', async () => {
      const url = `/rest/my/resource`;
      fetchMock.post(url, { status: 204 }, { name: 'my-request' });

      await client.postJson({
        url,
        requestId: 'stub-request',
        body: {},
        headers: {
          'some-header': 'some-value',
        },
      });

      expect(fetchMock.lastOptions().headers).to.be.deep.equal({
        'Content-Type': 'application/json',
        'some-header': 'some-value',
      });
    });

    it('should pass default headers', async () => {
      const url = `/rest/my/resource`;
      fetchMock.get(url, { status: 204 }, { name: 'my-request' });

      await client.getJson({
        url,
        requestId: 'stub-request',
      });

      expect(fetchMock.lastOptions().headers).to.be.deep.equal({
        'Content-Type': 'application/json',
      });
    });
  });
});
