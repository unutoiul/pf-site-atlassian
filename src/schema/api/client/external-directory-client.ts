import { getConfig } from 'common/config';

import { CreateDirectoryMutationArgs, GroupsExternalDirectoryArgs, LogsExternalDirectoryArgs, RegenerateDirectoryApiKeyMutationArgs, RemoveDirectoryMutationArgs, SyncedUsers } from '../../schema-types';
import { RequestHelper } from './request-helper';

export interface CreateDirectoryResponse extends Directory {
  token: string;
}

export interface GetDirectoriesForOrgParams {
  orgId: string;
}

export interface Directory {
  id: string;
  baseUrl: string;
  name: string;
  creationDate: string;
}

export interface RegenerateDirectoryApiKeyResponseShape {
  token: string;
}

export interface GroupsRequestParams extends GroupsExternalDirectoryArgs {
  directoryId: string;
}

export interface LogsRequestParams extends LogsExternalDirectoryArgs {
  directoryId: string;
}

export interface SyncedUsersRequestParams {
  directoryId: string;
}

export interface PaginatedResponse {
  totalResults: number;
  startIndex: number;
  itemsPerPage: number;
}

export interface GetLogsResponse extends PaginatedResponse {
  Resources: LogEntries;
}
export type LogEntries = LogEntry[];

interface LogEntry {
  error: string;
  created_on: string;
}

export interface GetGroupsResponse extends PaginatedResponse {
  Resources: SyncedGroups;
}

export type SyncedGroups = SyncedGroup[];

interface SyncedGroup {
  name: string;
  membershipCount: number;
}

export type GetDirectoriesForOrgResponse = Directory[];

interface ExternalDirectoryEndpoints {
  createDirectory: string;
  regenerateDirectoryApiKey(directoryId: string): string;
  directory(directoryId: string): string;
  logs(directoryId: string): string;
  groups(directoryId: string): string;
  getAllDirectoriesForOrg(orgId: string): string;
  getSyncedUserCount(directoryId: string): string;
}

const requestGroupBase = 'external-directory';
const getExternalDirectoryEndpoints = (): ExternalDirectoryEndpoints => {
  const baseUrl = getConfig().externalDirectoryUrl;

  return {
    createDirectory: `${baseUrl}`,
    regenerateDirectoryApiKey: (directoryId) => `${baseUrl}/${directoryId}/rotateToken`,
    directory: (directoryId) => `${baseUrl}/${directoryId}`,
    logs: (directoryId) => `${baseUrl}/${directoryId}/logs`,
    groups: (directoryId) => `${baseUrl}/${directoryId}/synced-group-memberships`,
    getAllDirectoriesForOrg: (orgId) => `${baseUrl}?orgId=${orgId}`,
    getSyncedUserCount: (directoryId) => `${baseUrl}/${directoryId}/synced-users`,
  };
};

const buildQueryParamString = ({ startIndex, count }: LogsExternalDirectoryArgs | GroupsExternalDirectoryArgs): string => {
  const startIndexParam = startIndex ? `startIndex=${startIndex}` : '';
  const countParam = count ? `count=${count}` : '';
  const params = [startIndexParam, countParam].filter(p => p !== '');
  if (params.length === 0) {
    return '';
  }

  return `?${params.join('&')}`;
};

export class ExternalDirectoryClient {
  constructor(
    private requestHelper: RequestHelper,
  ) {
  }

  public async createDirectory(params: CreateDirectoryMutationArgs): Promise<CreateDirectoryResponse> {
    return this.requestHelper.postJson<CreateDirectoryResponse, CreateDirectoryMutationArgs>({
      url: getExternalDirectoryEndpoints().createDirectory,
      requestId: requestGroupBase + '.create',
      body: params,
    });
  }

  public async removeDirectory(params: RemoveDirectoryMutationArgs): Promise<Response> {
    return this.requestHelper.deleteJson<Response>({
      url: getExternalDirectoryEndpoints().directory(params.directoryId),
      requestId: requestGroupBase + '.remove',
    });
  }

  public async regenerateDirectoryApiKey(params: RegenerateDirectoryApiKeyMutationArgs): Promise<RegenerateDirectoryApiKeyResponseShape> {
    return this.requestHelper.postJson<RegenerateDirectoryApiKeyResponseShape, RegenerateDirectoryApiKeyMutationArgs>({
      url: getExternalDirectoryEndpoints().regenerateDirectoryApiKey(params.directoryId),
      requestId: requestGroupBase + '.regenerate',
    });
  }

  public async getDirectoriesForOrg(params: GetDirectoriesForOrgParams): Promise<GetDirectoriesForOrgResponse> {
    return this.requestHelper.getJson<GetDirectoriesForOrgResponse>({
      url: getExternalDirectoryEndpoints().getAllDirectoriesForOrg(params.orgId),
      requestId: requestGroupBase + '.get-directories-for-org',
    });
  }

  public async getSyncedUsers(params: SyncedUsersRequestParams): Promise<SyncedUsers> {
    const { directoryId } = params;
    const syncedUsersUrl = getExternalDirectoryEndpoints().getSyncedUserCount(directoryId);

    return this.requestHelper.getJson<SyncedUsers>({
      url: syncedUsersUrl,
      requestId: requestGroupBase + '.synced-users-count',
    });
  }

  public async getLogs(params: LogsRequestParams): Promise<GetLogsResponse> {
    const { directoryId, startIndex, count } = params;
    const logsUrl = getExternalDirectoryEndpoints().logs(directoryId);
    const queryParams = buildQueryParamString({ startIndex, count });

    return this.requestHelper.getJson<GetLogsResponse>({
      url: `${logsUrl}${queryParams}`,
      requestId: requestGroupBase + '.logs',
    });
  }

  public async getGroups(params: GroupsRequestParams): Promise<GetGroupsResponse> {
    const { directoryId, startIndex, count } = params;
    const groupsUrl = getExternalDirectoryEndpoints().groups(directoryId);
    const queryParams = buildQueryParamString({ startIndex, count });

    return this.requestHelper.getJson<GetGroupsResponse>({
      url: `${groupsUrl}${queryParams}`,
      requestId: requestGroupBase + '.groups',
    });
  }
}
