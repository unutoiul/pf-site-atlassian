import { getConfig } from 'common/config';

import { RequestHelper } from './request-helper';

export interface IdmOrgApiShape {
  id: string;
}

export class BillingClient {
  constructor(
    private requestHelper: RequestHelper,
  ) {
  }

  public async getIdmOrg(orgId): Promise<IdmOrgApiShape> {
    return this.requestHelper.getJson({
      url: `${getConfig().billingUrl}/api/organizations/${orgId}`,
      requestId: 'idm-organization',
      storeStrategy: 'memoize',
      storeKey: 'idm-org',
    });
  }
}
