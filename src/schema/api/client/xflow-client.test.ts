import { expect } from 'chai';
import { sandbox as sinonSandbox, SinonSandbox, SinonStub } from 'sinon';

import { RolePermissions } from '../../schema-types/schema-types';
import { BodyRequestOptions } from './request-store';
import { XFlowClient } from './xflow-client';

describe('UserManagementClient', () => {
  let sandbox: SinonSandbox = sinonSandbox.create();
  let xflowClient: XFlowClient;
  let mockRequestHelper: {
    postJson: SinonStub,
  };

  sandbox = sinonSandbox.create();

  beforeEach(() => {
    mockRequestHelper = {
      postJson: sandbox.stub(),
    };

    xflowClient = new XFlowClient(mockRequestHelper as any);
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('getRolePermissions', () => {
    it('should return the result of the endpoint', async () => {
      mockRequestHelper.postJson.resolves({
        permissionIds: ['add-products'],
      });

      const result = await xflowClient.getRolePermissions({ cloudId: 'cloudId', roleId: 'trustworthy-people', shouldNotCreateRole: false });

      expect(mockRequestHelper.postJson.calledOnce).to.equal(true);
      const args: BodyRequestOptions<RolePermissions, { roleId, shouldNotCreateRole }> = mockRequestHelper.postJson.lastCall.args[0];
      expect(args.url).to.include('/gateway/api/xflow/cloudId/query-role');
      expect(args.body && args.body.roleId).to.equal('trustworthy-people');
      expect(args.body && args.body.shouldNotCreateRole).to.equal(false);
      expect(args.requestId).to.equal('get-role-permissions');
      expect(result).to.deep.equal({
        permissionIds: ['add-products'],
      });
    });
  });
});
