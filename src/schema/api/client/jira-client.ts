import { stringify } from 'query-string';

import { getConfig } from 'common/config';

import { RequestHelper } from './request-helper';

type CustomerAttributes = 'login.currentFailedCount'
  // This is the legacy value before migration which is used as fallback
  | 'login.lastLoginMillis'
  | 'local.login.currentFailedCount'
  | 'local.login.locked'
  | 'customer.account.last.login';

export interface ClientJsdCustomer {
  accountId: string;
  active: boolean;
  attributes: Record<CustomerAttributes, string[]> | object;
  displayName: string;
  emailAddress: string;
  localOnlyMode: boolean;
  splitUserMode: boolean;
  username: string;
}

export interface JsdCustomersResponse {
  localCustomers: ClientJsdCustomer[];
}

export class JiraClient {
  constructor(private requestHelper: RequestHelper) {}

  public async getJsdCustomers(cloudId: string, startIndex: number, maxResults: number = 20, displayName?: string | null, activeStatus?: string | null): Promise<JsdCustomersResponse> {
    const params = stringify({
      filter: displayName,
      'active-filter': activeStatus === 'all' ? undefined : activeStatus,
      'start-index': startIndex,
      'max-results': maxResults,
    });

    return this.requestHelper.getJson<JsdCustomersResponse>({
      url: `${getConfig().jiraUrl}/${cloudId}/rest/servicedesk/customer-management/noeyeball/1/local-servicedesk-user?${params}`,
      requestId: `jira-jsd-customers`,
    });
  }

  public async migrateToAtlassianAccount(cloudId: string, accountId: string): Promise<void> {
    return this.requestHelper.putJson<void, void>({
      url: `${getConfig().jiraUrl}/${cloudId}/rest/servicedesk/customer-management/noeyeball/1/local-servicedesk-user/migrate-to-atlassian-account-user/${accountId}`,
      requestId: `jira-jsd-migrate-account`,
    });
  }
}
