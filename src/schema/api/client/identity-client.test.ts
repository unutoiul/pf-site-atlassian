import { expect } from 'chai';
import { sandbox as sinonSandbox, SinonSandbox, SinonStub } from 'sinon';

import { getConfig } from 'common/config';

import { MemberToken } from '../../schema-types';
import { IdentityClient } from './identity-client';
import { RequestOptions } from './request-store';

describe('IdentityClient', () => {
  const sandbox: SinonSandbox = sinonSandbox.create();
  let identityClient: IdentityClient;
  let mockRequestHelper: {
    getJson: SinonStub,
    deleteJson: SinonStub,
  };
  let baseUrl: string;

  beforeEach(() => {
    baseUrl = `${getConfig().apiUrl}`;

    mockRequestHelper = {
      getJson: sandbox.stub(),
      deleteJson: sandbox.stub(),
    };

    identityClient = new IdentityClient(mockRequestHelper as any);
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('getMemberTokens', () => {
    it('should return 2 tokens and have correct url', async () => {
      const expectedResult: MemberToken[] = [
        {
          id: 'api-token-id-1',
          label: 'api-token',
          createdAt: '2019-01-03T00:11:46.608Z',
          lastAccess: '2019-01-05T00:10:46.768Z',
        },
        {
          id: 'api-token-id-2',
          label: 'api-token',
          createdAt: '2019-01-02T00:13:46.712Z',
          lastAccess: '2019-01-06T00:15:23.608Z',
        },
      ];
      const memberId = 'foo';

      mockRequestHelper.getJson.resolves(expectedResult);

      const result = await identityClient.getMemberTokens(memberId);
      const args: RequestOptions = mockRequestHelper.getJson.lastCall.args[0];
      expect(result).to.deep.equal(expectedResult);
      expect(mockRequestHelper.getJson.calledOnce).to.equal(true);
      expect(args.url).to.equal(`${baseUrl}/users/${memberId}/manage/api-tokens`);
    });
  });

  describe('revokeMemberToken', () => {
    it('should hit the correct url', async () => {
      const memberId = 'foo';
      const memberTokenId = '1';

      mockRequestHelper.deleteJson.resolves({});

      await identityClient.revokeMemberToken(memberId, memberTokenId);
      const args: RequestOptions = mockRequestHelper.deleteJson.lastCall.args[0];
      expect(mockRequestHelper.deleteJson.calledOnce).to.equal(true);
      expect(args.url).to.equal(`${baseUrl}/users/${memberId}/manage/api-tokens/${memberTokenId}`);
    });
  });
});
