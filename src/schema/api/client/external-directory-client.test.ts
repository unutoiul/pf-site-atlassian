// tslint:disable check-fetch-result
import { expect } from 'chai';
import { sandbox as sinonSandbox, SinonSandbox } from 'sinon';

import { getConfig } from 'common/config';

import { BodyRequestOptions, RequestOptions } from './request-store';

import {
  CreateDirectoryResponse,
  ExternalDirectoryClient,
  GetDirectoriesForOrgResponse,
  LogEntries,
  SyncedGroups,
} from './external-directory-client';

import { CreateDirectoryMutationArgs } from '../../schema-types';

describe('ExternalDirectoryClient', () => {
  let sandbox: SinonSandbox;
  let externalDirectoryClient: ExternalDirectoryClient;
  let mockRequestHelper;
  let baseUrl;

  beforeEach(() => {
    sandbox = sinonSandbox.create();
    baseUrl = `${getConfig().externalDirectoryUrl}`;

    mockRequestHelper = {
      postJson: sandbox.stub(),
      getJson: sandbox.stub(),
      deleteJson: sandbox.stub(),
    };

    // tslint:disable-next-line no-unnecessary-type-assertion
    externalDirectoryClient = new ExternalDirectoryClient(mockRequestHelper as any);
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('createDirectory', () => {
    it('should return the result of the endpoint', async () => {
      const response = {
        id: 'FAKE',
        status: 201,
        baseUrl: 'some-url',
        name: 'test',
        creationDate: Date(),
        token: 'token',
      };

      mockRequestHelper.postJson.resolves(response);

      const params = {
        orgId: 'test',
        name: 'test',
      };

      const result = await externalDirectoryClient.createDirectory(params);

      const args: BodyRequestOptions<CreateDirectoryResponse, CreateDirectoryMutationArgs> = mockRequestHelper.postJson.lastCall.args[0];

      expect(result).to.equal(response);
      expect(mockRequestHelper.postJson.calledOnce).to.equal(true);
      expect(args.url).to.equal(baseUrl);
      expect(args.body).to.equal(params);
    });
  });

  describe('regenerateDirectory', () => {
    it('should return the result of the endpoint', async () => {

      const response = {
        status: 201,
        token: 'token',
      };

      mockRequestHelper.postJson.resolves(response);

      const result = await externalDirectoryClient.regenerateDirectoryApiKey({
        directoryId: 'testDirectoryId',
      });
      const args: RequestOptions = mockRequestHelper.postJson.lastCall.args[0];

      expect(result).to.equal(response);
      expect(mockRequestHelper.postJson.calledOnce).to.equal(true);
      expect(args.url).to.equal(`${baseUrl}/testDirectoryId/rotateToken`);
    });
  });

  describe('removeDirectory', () => {
    it('should return the result of the endpoint', async () => {

      const response = {
        status: 204,
      };

      mockRequestHelper.deleteJson.resolves(response);

      const result = await externalDirectoryClient.removeDirectory({
        directoryId: 'testDirectoryId',
      });
      const args: RequestOptions = mockRequestHelper.deleteJson.lastCall.args[0];

      expect(result).to.equal(response);
      expect(mockRequestHelper.deleteJson.calledOnce).to.equal(true);
      expect(args.url).to.equal(`${baseUrl}/testDirectoryId`);
    });
  });

  describe('getDirectoriesForOrg', () => {
    it('should return the result of the endpoint', async () => {
      const response: GetDirectoriesForOrgResponse = [
        {
          id: 'foobar',
          baseUrl: 'string',
          name: 'name',
          creationDate: 'date',
        },
      ];

      mockRequestHelper.getJson.resolves(response);

      const result = await externalDirectoryClient.getDirectoriesForOrg({
        orgId: 'test',
      });

      const args: RequestOptions = mockRequestHelper.getJson.lastCall.args[0];

      expect(result).to.equal(response);
      expect(mockRequestHelper.getJson.calledOnce).to.equal(true);
      expect(args.url).to.equal(`${baseUrl}?orgId=test`);
    });
  });

  describe('getLogs', () => {
    it('should return the result of the endpoint', async () => {
      const response = {
        totalResults: 1,
        startIndex: 0,
        count: 1,
        logEntries: [
          {
            error: 'foobar',
            created_on: 'date',
          },
        ] as LogEntries,
      };

      mockRequestHelper.getJson.resolves(response);

      const result = await externalDirectoryClient.getLogs({
        directoryId: 'testDirectoryId',
        startIndex: 44,
        count: 10,
      });

      const args: RequestOptions = mockRequestHelper.getJson.lastCall.args[0];

      expect(result).to.deep.equal(response);
      expect(mockRequestHelper.getJson.calledOnce).to.equal(true);
      expect(args.url).to.equal(`${baseUrl}/testDirectoryId/logs?startIndex=44&count=10`);
    });
  });

  describe('getGroups', () => {
    it('should return the result of the endpoint', async () => {
      const response = {
        totalResults: 1,
        startIndex: 0,
        itemsPerPage: 1,
        Resources: [
          {
            name: 'foobar',
            membershipCount: 4,
          },
        ] as SyncedGroups,
      };

      mockRequestHelper.getJson.resolves(response);

      const result = await externalDirectoryClient.getGroups({
        directoryId: 'testDirectoryId',
        startIndex: 44,
        count: 10,
      });

      const args: RequestOptions = mockRequestHelper.getJson.lastCall.args[0];

      expect(result).to.deep.equal(response);
      expect(mockRequestHelper.getJson.calledOnce).to.equal(true);
      expect(args.url).to.equal(`${baseUrl}/testDirectoryId/synced-group-memberships?startIndex=44&count=10`);
    });
  });
});
