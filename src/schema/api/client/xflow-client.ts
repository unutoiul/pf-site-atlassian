import { getConfig } from 'common/config';

import { RolePermissions } from '../../schema-types/schema-types';
import { RequestHelper } from './request-helper';

interface RolePermissionsParams {
  cloudId: string;
  roleId: string;
  shouldNotCreateRole?: boolean;
}

export class XFlowClient {
  constructor(private requestHelper: RequestHelper) {
  }

  public async getRolePermissions({ cloudId, roleId, shouldNotCreateRole }: RolePermissionsParams): Promise<RolePermissions> {
    return this.requestHelper.postJson<RolePermissions, { roleId }>({
      url: `${getConfig().xflowBasePath}/${cloudId}/query-role`,
      requestId: 'get-role-permissions',
      body: {
        roleId,
        shouldNotCreateRole,
      },
    });
  }
}
