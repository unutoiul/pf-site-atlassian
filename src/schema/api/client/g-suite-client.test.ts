import { fail } from 'assert';
import { expect } from 'chai';
import { sandbox as sinonSandbox } from 'sinon';

import { getConfig } from 'common/config';

import { RequestError } from 'common/error';

import { GSuiteClient } from './g-suite-client';
import { RequestOptions } from './request-store';

describe('GSuite Client', () => {
  const sandbox = sinonSandbox.create();
  const mockRequestHelper = {
    getJson: sandbox.stub(),
    postJson: sandbox.stub(),
    putJson: sandbox.stub(),
    deleteJson: sandbox.stub(),
    fetch: sandbox.stub(),
  };
  const gSuiteClient = new GSuiteClient(mockRequestHelper as any);

  afterEach(() => {
    sandbox.reset();
  });
  after(() => {
    sandbox.restore();
  });

  describe('getConnection', () => {
    it('Should return a url if the G Suite connection url is returned from the response', async () => {
      const response = { connect_url: 'http://fake-google.jira-dev.com/authenticate' };
      mockRequestHelper.postJson.resolves(response);
      const result = await gSuiteClient.getConnection('cloudId');
      expect(result).to.deep.equal({ url: 'http://fake-google.jira-dev.com/authenticate' });
    });

    it('Should return an empty url if the connection url is undefined', async () => {
      const response = { connect_url: undefined };
      mockRequestHelper.postJson.resolves(response);
      const result = await gSuiteClient.getConnection('cloudId');
      expect(result).to.deep.equal({ url: undefined });
    });

    it('Should return an empty url if the connection url does not exist in the response returned', async () => {
      const response = { bogus: 'http://fake-google.jira-dev.com/authenticate' };
      mockRequestHelper.postJson.resolves(response);
      const result = await gSuiteClient.getConnection('cloudId');
      expect(result).to.deep.equal({ url: undefined });
    });

    it('Should throw an error if the request throws an error', async () => {
      const response = new RequestError({ message: 'Bad Request', status: 404 });
      mockRequestHelper.postJson.rejects(response);
      try {
        await gSuiteClient.getConnection('cloudId');
        fail('Should have failed with an error');
      } catch (e) {
        expect(e).to.deep.equal(response);
      }
    });
  });

  describe('getState', () => {
    it('should return the disconnected state', async () => {
      const response = {};
      mockRequestHelper.getJson.resolves(response);

      const result = await gSuiteClient.getState('cloudId');

      expect(result).to.equal(undefined);

      expect(mockRequestHelper.getJson.calledOnce).to.equal(true);
      const args: RequestOptions = mockRequestHelper.getJson.lastCall.args[0];
      expect(args.url).to.equal(`${getConfig().gSuiteBasePath}/cloudId/state`);
      expect(args.requestId).to.equal('um-gsuite-state');
    });

    it('should return the connected state', async () => {
      const response = {
        syncConfig: [],
        syncStartTime: null,
        nextSyncStartTime: '17 Oct 2018 4:35AM GMT',
        lastSync: {
          startTime: '17 Oct 2018 3:35AM GMT',
          failed: 0,
          errors: [],
          updated: 0,
          hasReport: true,
          status: 'successful',
          finishTime: '17 Oct 2018 3:35AM GMT',
          numOfSyncedUsers: 1,
          deleted: 0,
          created: 1,
        },
        selectedGroups: ['01hmsyys3fnt05a'],
        noSyncs: 2,
        user: 'jberry@jberry-test.teamsinspace.com',
      };
      mockRequestHelper.getJson.resolves(response);

      const result = await gSuiteClient.getState('cloudId');

      expect(result).to.deep.equal(response);

      expect(mockRequestHelper.getJson.calledOnce).to.equal(true);
      const args: RequestOptions = mockRequestHelper.getJson.lastCall.args[0];
      expect(args.url).to.equal(`${getConfig().gSuiteBasePath}/cloudId/state`);
      expect(args.requestId).to.equal('um-gsuite-state');
    });
  });
});
