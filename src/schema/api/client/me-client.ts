import { getConfig } from 'common/config';

import { RequestHelper } from './request-helper';

export interface CurrentUserResponse {
  account_id: string;
  email: string;
}

export class MeClient {
  constructor(private requestHelper: RequestHelper) {
  }

  public async getCurrentUser(): Promise<CurrentUserResponse> {
    return this.requestHelper.getJson<CurrentUserResponse>({
      url: `${getConfig().apiUrl}/me`,
      requestId: `get-current-user`,
      storeStrategy: 'cache',
      storeKey: 'current-user-data',
      defaultValue: { account_id: '', email: '' },
    });
  }
}
