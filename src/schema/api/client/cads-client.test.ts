import { expect } from 'chai';
import { sandbox as sinonSandbox, SinonSandbox, SinonStub } from 'sinon';

import { CadsClient, ServiceDeskCustomerDirectoryShape } from './cads-client';
import { BodyRequestOptions } from './request-store';

describe('CadsClient', () => {
  const sandbox: SinonSandbox = sinonSandbox.create();
  let cadsClient: CadsClient;
  let mockRequestHelper: {
    deleteJson: SinonStub,
    getText: SinonStub,
    getJson: SinonStub,
    patchJson: SinonStub,
  };

  beforeEach(() => {
    mockRequestHelper = {
      deleteJson: sandbox.stub(),
      getText: sandbox.stub(),
      getJson: sandbox.stub(),
      patchJson: sandbox.stub(),
    };

    cadsClient = new CadsClient(mockRequestHelper as any);
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('getDirectoryId', () => {
    it('should return the directory id for a given site', async () => {
      const response = {
        serviceDeskCustomerDirectoryId: 'test-directory-id',
      };
      mockRequestHelper.getJson.resolves(response);

      const result = await cadsClient.getDirectoryId('test-cloud-id');
      expect(result).to.equal(response);

      expect(mockRequestHelper.getJson.calledOnce).to.equal(true);
      const options: BodyRequestOptions<ServiceDeskCustomerDirectoryShape, never> = mockRequestHelper.getJson.lastCall.args[0];
      expect(options.url).to.contain('test-cloud-id');
    });
  });

  describe('getCustomerExport', () => {
    it('should return the export for a given directory', async () => {
      const response = `username,full_name,email,active,last_login`;
      mockRequestHelper.getText.resolves(response);

      const result = await cadsClient.getCustomerExport('test-directory-id', 'https://example.com');
      expect(result).to.equal(response);

      expect(mockRequestHelper.getText.calledOnce).to.equal(true);
      const url: string = mockRequestHelper.getText.lastCall.args[0];
      expect(url).to.contain('test-directory-id');
      expect(url).to.contain(`?errorRedirectUrl=https://example.com`);
    });
  });

  describe('deleteCustomer', () => {
    it('should delete the specified customer', async () => {
      mockRequestHelper.deleteJson.resolves(undefined);

      await cadsClient.deleteCustomer('test-directory-id', 'test-account-id');

      expect(mockRequestHelper.deleteJson.calledOnce).to.equal(true);
      const url: string = mockRequestHelper.deleteJson.lastCall.args[0].url;
      expect(url).to.contain('test-directory-id');
      expect(url).to.contain('test-account-id');
    });
  });

  describe('updateCustomerAccount', () => {
    it('should send the correct params', async () => {
      mockRequestHelper.patchJson.resolves({});

      await cadsClient.updateCustomerAccount('directoryId', 'accountId', 'name', 'password', 'email', 'emailVerified');

      expect(mockRequestHelper.patchJson.calledOnce).to.equal(true);
      expect(mockRequestHelper.patchJson.lastCall.args[0]).to.deep.equal({
        headers: {
          'ATL-Accept-User-Version': '2',
        },
        url: `/gateway/api/adminhub/customer-directory/directory/directoryId/user/accountId`,
        requestId: `cads-service-update-customer-account`,
        body: {
          name: 'name',
          password: 'password',
          email: 'email',
          email_verified: 'emailVerified',
        },
      });
    });
  });

  describe('updateCustomerAccess', () => {
    it('should send the correct params', async () => {
      mockRequestHelper.patchJson.resolves({});

      await cadsClient.updateCustomerAccess('directoryId', 'accountId', true);

      expect(mockRequestHelper.patchJson.calledOnce).to.equal(true);
      expect(mockRequestHelper.patchJson.lastCall.args[0]).to.deep.equal({
        headers: {
          'ATL-Accept-User-Version': '2',
        },
        url: `/gateway/api/adminhub/customer-directory/directory/directoryId/user/accountId`,
        requestId: `cads-service-update-customer-account`,
        body: {
          account_status: 'active',
        },
      });

      await cadsClient.updateCustomerAccess('directoryId', 'accountId', false);
      expect(mockRequestHelper.patchJson.lastCall.args[0]).to.deep.equal({
        headers: {
          'ATL-Accept-User-Version': '2',
        },
        url: `/gateway/api/adminhub/customer-directory/directory/directoryId/user/accountId`,
        requestId: `cads-service-update-customer-account`,
        body: {
          account_status: 'inactive',
        },
      });
    });
  });
});
