import { expect } from 'chai';
import { sandbox as sinonSandbox, SinonSandbox, SinonStub } from 'sinon';

import { RequestOptions } from './request-store';
import { SiteClient } from './site-client';

describe('SiteClient', () => {
  let sandbox: SinonSandbox;
  let siteClient: SiteClient;
  let mockRequestHelper: {
    getJson: SinonStub,
    putJson: SinonStub,
  };

  beforeEach(() => {
    sandbox = sinonSandbox.create();

    mockRequestHelper = {
      getJson: sandbox.stub(),
      putJson: sandbox.stub(),
    };

    siteClient = new SiteClient(mockRequestHelper as any);
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('isXFlowEnabled', () => {
    it('should return the result of the endpoint', async () => {
      const xflowEnabled = {
        'product-suggestions-enabled': true,
      };
      mockRequestHelper.getJson.resolves(xflowEnabled);

      const result = await siteClient.isXFlowEnabled('mockCloudId');
      expect(result).to.equal(xflowEnabled);

      expect(mockRequestHelper.getJson.calledOnce).to.equal(true);
      const args: RequestOptions = mockRequestHelper.getJson.lastCall.args[0];
      expect(args.url).to.equal('http://localhost:3002/gateway/api/site/mockCloudId/setting/xflow/product-suggestions-enabled');
      expect(args.requestId).to.equal('xflow-product-suggestions-enabled');
    });
  });

  describe('setDisplayName', () => {
    it('should return the result of the endpoint', async () => {
      mockRequestHelper.putJson.resolves();

      await siteClient.setDisplayName('mockCloudId', 'UpDog');

      expect(mockRequestHelper.putJson.calledOnce).to.equal(true);
      const args = mockRequestHelper.putJson.lastCall.args[0];
      expect(args.url).to.equal('http://localhost:3002/gateway/api/site/mockCloudId/setting/site.metadata/display.name');
      expect(args.body).to.deep.equal({
        'display.name': 'UpDog',
      });
    });
  });
});
