import { getConfig } from 'common/config';

import { RequestHelper } from './request-helper';

export interface ProfileMutabilityResponse {
  not_editable: MemberMutabilityConstraint[] | null;
}

export interface MemberMutabilityConstraint {
  field: string;
  reason: ImmutabilityReason;
}

export type ImmutabilityReason = 'managed' | 'ext.dir.scim' | 'ext.dir.google';

export class ProfileMutabilityClient {
  constructor(
    private requestHelper: RequestHelper,
  ) {
  }

  public async getMemberMutability(id: string): Promise<ProfileMutabilityResponse> {
    return this.requestHelper.getJson<ProfileMutabilityResponse>({
      // refer to https://id-public-api-facade.us-west-1.prod.atl-paas.net/swagger.json for ednpoint docs
      url: `${getConfig().apiUrl}/users/manage/${encodeURIComponent(id)}/profile`,
      requestId: 'get-profile-mutability',
    });
  }
}
