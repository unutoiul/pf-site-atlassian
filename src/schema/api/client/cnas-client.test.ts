import { expect } from 'chai';
import { sandbox as sinonSandbox, SinonSandbox, SinonStub } from 'sinon';
import { SiteRenameStatus } from 'src/schema/schema-types';

import { CnasClient, SiteRenameStatusArgs } from './cnas-client';
import { BodyRequestOptions } from './request-store';

describe('CnasClient', () => {
  let sandbox: SinonSandbox;
  let cnasClient: CnasClient;
  let mockRequestHelper: {
    postJson: SinonStub,
  };

  beforeEach(() => {
    sandbox = sinonSandbox.create();

    mockRequestHelper = {
      postJson: sandbox.stub(),
    };

    cnasClient = new CnasClient(mockRequestHelper as any);
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('getRenameStatus', () => {
    it('should return the result of the endpoint', async () => {
      const siteRenameStatus = {
        cloudName: 'foo',
        cloudNamespace: 'jira.com',
        renamesUsed: 1,
        renameLimit: 5,
      };
      mockRequestHelper.postJson.resolves(siteRenameStatus);

      const result = await cnasClient.getRenameStatus('123');
      expect(result).to.equal(siteRenameStatus);

      expect(mockRequestHelper.postJson.calledOnce).to.equal(true);
      const options: BodyRequestOptions<SiteRenameStatus, SiteRenameStatusArgs> = mockRequestHelper.postJson.lastCall.args[0];
      expect(options.body).to.deep.equal({
        entityId: '123',
        entityType: 'CLOUD',
      });
    });
  });
});
