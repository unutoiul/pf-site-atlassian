import { expect } from 'chai';
import * as fetchMock from 'fetch-mock';

import { getConfig } from 'common/config';

import { ProfileMutabilityClient, ProfileMutabilityResponse } from './profile-mutability-client';
import { RequestHelper } from './request-helper';

describe('ProfileMutabilityClient', () => {
  let profileMutabilityClient: ProfileMutabilityClient;

  beforeEach(() => {
    profileMutabilityClient = new ProfileMutabilityClient(new RequestHelper());
  });

  afterEach(() => {
    fetchMock.restore();
  });

  describe('getMemberMutability', () => {
    const accountId = '1234';

    it('throws if the response is not ok', async () => {
      fetchMock.get(`${getConfig().apiUrl}/users/manage/${encodeURIComponent(accountId)}/profile`, {
        status: 500,
        body: '{"key": "some-failure"}',
      });

      try {
        await profileMutabilityClient.getMemberMutability(accountId);
        expect.fail('an error was expected to be thrown, but the request succeeded');
      } catch (e) {
        expect(e.message).to.equal('Fetch request failed with status 500 for get-profile-mutability');
      }
    });

    it('returns mutability constraints', async () => {
      const profileMutabilityResults: ProfileMutabilityResponse = {
        not_editable: [{ field: 'email', reason: 'ext.dir.scim' }],
      };

      fetchMock.get(`${getConfig().apiUrl}/users/manage/${encodeURIComponent(accountId)}/profile`, {
        status: 200,
        body: profileMutabilityResults,
      });

      const response = await profileMutabilityClient.getMemberMutability(accountId);
      expect(response).to.deep.equal(profileMutabilityResults);
    });
  });
});
