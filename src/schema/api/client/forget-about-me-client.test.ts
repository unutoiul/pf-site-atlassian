import { expect } from 'chai';
import * as fetchMock from 'fetch-mock';

import { getConfig } from 'common/config';

import { LifecycleHookResult } from '../../schema-types/schema-types';
import { ForgetAboutMeClient } from './forget-about-me-client';
import { RequestHelper } from './request-helper';

describe('ForgetAboutMeClient', () => {

  const accountId = 1234;
  let forgetAboutMeClient: ForgetAboutMeClient;

  beforeEach(() => {
    forgetAboutMeClient = new ForgetAboutMeClient(new RequestHelper());
  });

  afterEach(() => {
    fetchMock.restore();
  });

  describe('canCloseAccount', () => {

    it('calls canCloseAccount on forget-about-me through stargate correctly', async () => {
      fetchMock.get(`${getConfig().apiUrl}/users/${accountId}/can-close`, {
        status: 200,
        body: '{}',
      });

      await forgetAboutMeClient.canCloseAccount({ accountId });
      expect(fetchMock.lastUrl()).to.equal(`${getConfig().apiUrl}/users/${accountId}/can-close`);
    });

    it('throws if the response is not ok', async () => {
      fetchMock.get(`${getConfig().apiUrl}/users/${accountId}/can-close`, {
        status: 500,
        body: '{"key": "some-failure"}',
      });

      try {
        await forgetAboutMeClient.canCloseAccount({ accountId });
        expect.fail('an error was expected to be thrown, but the request succeeded');
      } catch (e) {
        expect(e.message).to.equal('Fetch request failed with status 500 for get-can-close-user-reasons');
      }
    });

    it('returns can close details', async () => {
      const canCloseResult: LifecycleHookResult = {
        warnings: [{
          code: 'WARNING_CODE',
          message: 'Warning Message',
          link: 'http://warnings.atlassian.com',
        }],
        errors: [{
          code: 'ERROR_CODE',
          message: 'Error Message',
          link: 'http://errors.atlassian.com',
        }],
      };

      fetchMock.get(`${getConfig().apiUrl}/users/${accountId}/can-close`, {
        status: 200,
        body: canCloseResult,
      });

      const response = await forgetAboutMeClient.canCloseAccount({ accountId });
      expect(response).to.deep.equal(canCloseResult);
    });
  });

  describe('closeAccount', () => {
    it('calls closeAccount on forget-about-me through stargate correctly', async () => {
      fetchMock.delete(`${getConfig().apiUrl}/users/${accountId}`, {
        status: 200,
        body: '{}',
      });

      await forgetAboutMeClient.closeAccount({ accountId });
      expect(fetchMock.lastUrl()).to.equal(`${getConfig().apiUrl}/users/${accountId}`);
    });

    it('throws if the response is not ok', async () => {
      fetchMock.delete(`${getConfig().apiUrl}/users/${accountId}`, {
        status: 500,
        body: '{}',
      });

      try {
        await forgetAboutMeClient.closeAccount({ accountId });
        expect.fail('an error was expected to be thrown, but the request succeeded');
      } catch (e) {
        expect(e.message).to.equal('Fetch request failed with status 500 for delete-account');
      }
    });
  });

  describe('deactivateAccount', () => {
    it('calls deactivateAccount on forget-about-me through stargate correctly', async () => {
      fetchMock.post(`${getConfig().apiUrl}/users/${accountId}/deactivate`, {
        status: 204,
        body: '{}',
      });

      await forgetAboutMeClient.deactivateAccount({ accountId });
      expect(fetchMock.lastUrl()).to.equal(`${getConfig().apiUrl}/users/${accountId}/deactivate`);
    });

    it('throws if the response is not ok', async () => {
      fetchMock.post(`${getConfig().apiUrl}/users/${accountId}/deactivate`, {
        status: 500,
        body: '{}',
      });

      try {
        await forgetAboutMeClient.deactivateAccount({ accountId });
        expect.fail('an error was expected to be thrown, but the request succeeded');
      } catch (e) {
        expect(e.message).to.equal('Fetch request failed with status 500 for deactivate-account');
      }
    });
  });

  describe('activateAccount', () => {
    it('calls activateAccount on forget-about-me through stargate correctly', async () => {
      fetchMock.post(`${getConfig().apiUrl}/users/${accountId}/activate`, {
        status: 204,
        body: '{}',
      });

      await forgetAboutMeClient.activateAccount({ accountId });
      expect(fetchMock.lastUrl()).to.equal(`${getConfig().apiUrl}/users/${accountId}/activate`);
    });

    it('throws if the response is not ok', async () => {
      fetchMock.post(`${getConfig().apiUrl}/users/${accountId}/activate`, {
        status: 500,
        body: '{}',
      });

      try {
        await forgetAboutMeClient.activateAccount({ accountId });
        expect.fail('an error was expected to be thrown, but the request succeeded');
      } catch (e) {
        expect(e.message).to.equal('Fetch request failed with status 500 for activate-account');
      }
    });
  });

  describe('cancelDeleteAccount', () => {
    it('calls cancelDeleteAccount on forget-about-me through stargate correctly', async () => {
      fetchMock.post(`${getConfig().apiUrl}/users/${accountId}/cancel-deletion`, {
        status: 200,
        body: '{}',
      });

      await forgetAboutMeClient.cancelDeleteAccount({ accountId });
      expect(fetchMock.lastUrl()).to.equal(`${getConfig().apiUrl}/users/${accountId}/cancel-deletion`);
    });

    it('throws if the response is not ok', async () => {
      fetchMock.post(`${getConfig().apiUrl}/users/${accountId}/cancel-deletion`, {
        status: 500,
        body: '{}',
      });

      try {
        await forgetAboutMeClient.cancelDeleteAccount({ accountId });
        expect.fail('an error was expected to be thrown, but the request succeeded');
      } catch (e) {
        expect(e.message).to.equal('Fetch request failed with status 500 for cancel-delete-account');
      }
    });
  });
  describe('canDeactivateAccount', () => {

    it('calls canDeactivateAccount on forget-about-me through stargate correctly', async () => {
      fetchMock.get(`${getConfig().apiUrl}/users/${accountId}/can-deactivate`, {
        status: 200,
        body: '{}',
      });

      await forgetAboutMeClient.canDeactivateAccount({ accountId });
      expect(fetchMock.lastUrl()).to.equal(`${getConfig().apiUrl}/users/${accountId}/can-deactivate`);
    });

    it('throws if the response is not ok', async () => {
      fetchMock.get(`${getConfig().apiUrl}/users/${accountId}/can-deactivate`, {
        status: 500,
        body: '{"key": "some-failure"}',
      });

      try {
        await forgetAboutMeClient.canDeactivateAccount({ accountId });
        expect.fail('an error was expected to be thrown, but the request succeeded');
      } catch (e) {
        expect(e.message).to.equal('Fetch request failed with status 500 for get-can-deactivate-user-reasons');
      }
    });

    it('returns can deactivate details', async () => {
      const canDeactivateResult: LifecycleHookResult = {
        warnings: [{
          code: 'WARNING_CODE',
          message: 'Warning Message',
          link: 'http://warnings.atlassian.com',
        }],
        errors: [{
          code: 'ERROR_CODE',
          message: 'Error Message',
          link: 'http://errors.atlassian.com',
        }],
      };

      fetchMock.get(`${getConfig().apiUrl}/users/${accountId}/can-deactivate`, {
        status: 200,
        body: canDeactivateResult,
      });

      const response = await forgetAboutMeClient.canDeactivateAccount({ accountId });
      expect(response).to.deep.equal(canDeactivateResult);
    });
  });
});
