import { stringify } from 'query-string';

import { analyticsClient } from 'common/analytics/analytics-client';
import { getConfig } from 'common/config';

import { ProductId } from '../../../site/user-management/access-config/access-config-type';
import { Permissions, permissionToRole } from '../../../site/user-management/users/user-permission-section/permissions';
import { fetchGetOptions } from '../../../utilities/schema';

import {
  AccessRequestList,
  AccessRequestListInput,
  AddUsersToGroupsInput,
  ApproveAccessInput,
  DefaultProduct,
  DenyAccessInput,
  GenerateUserExportInput,
  Group,
  PaginatedGroups,
  PaginatedMembers,
  PaginatedUsers,
  ProductAccess,
  ProductType,
  SiteInviteUsersInput,
  SiteUsersListInput,
  SuggestChangesInput,
  UpdateGroupInput,
  User,
} from '../../schema-types';

import { UserManagedStatus } from '../../schema-types/schema-types';
import { toRequestError } from './request-store';
import { UserManagementRequestHelper } from './um-request-helper';

export interface Application {
  id: string;
  name: string;
  homeUrl: string;
  adminUrl: string;
  hostType: string;
}

interface SiteProduct {
  productId: string;
  productName: string;
}

export interface SiteContextRaw {
  firstActivationDate: number;
}

export interface SiteContext {
  firstActivationDate: string;
}

export class UserManagementClient {
  constructor(private requestHelper: UserManagementRequestHelper) {
  }

  public async getApplications(): Promise<Application[]> {
    return this.requestHelper.getJson<Application[]>({
      url: `/admin/rest/um/1/apps/navigation`,
      requestId: 'nav-apps',
      defaultValue: [],
      storeStrategy: 'cache',
      storeKey: 'current-site-apps',
    });
  }

  public async getDefaultApps(cloudId: string): Promise<DefaultProduct[]> {
    const response = await fetch(`${getConfig().umBasePath}/${cloudId}/product/defaults`, fetchGetOptions);
    if (!response.ok) {
      return [];
    }
    const json = await response.json();

    return json.products;
  }

  public async getGroupsAdminAccessConfig(cloudId: string): Promise<any> {
    const response = await fetch(`${getConfig().umBasePath}/${cloudId}/product/access-config/admin`, fetchGetOptions);
    if (!response.ok) {
      return [];
    }

    const json = await response.json();

    return json.adminAccessConfig;
  }

  public async getGroupsUseAccessConfig(cloudId: string): Promise<any> {
    const response = await fetch(`${getConfig().umBasePath}/${cloudId}/product/access-config/use`, fetchGetOptions);
    if (!response.ok) {
      return [];
    }
    const json = await response.json();

    return json.useAccessConfig;
  }

  public async getSiteContext(): Promise<SiteContext> {
    const siteContextRaw: SiteContextRaw = await this.requestHelper.getJson<SiteContextRaw>({
      url: `/admin/rest/um/1/context`,
      requestId: 'um-context',
      defaultValue: { firstActivationDate: 0 },
      storeStrategy: 'cache',
      storeKey: 'um-site-context',
    });

    if (!siteContextRaw || !siteContextRaw.firstActivationDate) {
      return {
        firstActivationDate: '0',
      };
    } else {
      return {
        firstActivationDate: siteContextRaw.firstActivationDate.toString(),
      };
    }
  }

  public async getUsers(cloudId: string, { start, count, activeStatus, displayName, productUse, productAdmin, sitePrivilege, contains }: SiteUsersListInput): Promise<PaginatedUsers> {
    const params = stringify({
      count,
      'start-index': start,
      'active-status': activeStatus ? (activeStatus.includes('inactive') ? false : true) : undefined,
      'display-name': displayName,
      'product-use': productUse,
      'product-admin': productAdmin,
      'site-privilege': sitePrivilege,
      contains,
    });

    return this.requestHelper.getJson({
      url: `${getConfig().umBasePath}/${cloudId}/users${params !== '' ? '?' : ''}${params}`,
      requestId: `get-site-users`,
    });
  }

  public async inviteUsers(cloudId: string, { atlOrigin, role, ...rest }: SiteInviteUsersInput): Promise<void> {
    // so that it does not impact current user invite flow
    const body = role && role !== Permissions.BASIC
      ? { ...rest, role: permissionToRole[role] }
      : rest;

    try {
      await this.requestHelper.postJson({
        url: `${getConfig().umBasePath}/${cloudId}/users/invite`,
        requestId: `invite-site-users`,
        body,
        headers: atlOrigin ? { 'Origin-ID': atlOrigin } : undefined,
      });
    } catch (e) {
      // Specific handling to debug an issue where 500's are being returned from UM for invite-site-users
      analyticsClient.onPageAction({
        name: 'error.invite-site-users',
        attributes: {
          cloudId,
          message: e.message,
          productIdsCount: rest.productIds && rest.productIds.length,
          additionalGroupsCount: rest.additionalGroups && rest.additionalGroups.length,
          sendNotification: rest.sendNotification,
          notificationTextLength: rest.notificationText && rest.notificationText.length,
          role,
        },
      });
      throw (e);
    }
  }

  public async removeUserFromSite(cloudId: string, userId: string): Promise<void> {
    await this.requestHelper.deleteJson<void>({
      url: `${getConfig().umBasePath}/${cloudId}/users/${userId}`,
      requestId: 'site-user-remove',
    });
  }

  public async changeUserRole(cloudId: string, userId: string, roleId: string): Promise<void> {
    const body = { role: roleId };
    await this.requestHelper.postJson({
      url: `${getConfig().umBasePath}/${cloudId}/users/${userId}/permissions`,
      requestId: 'site-user-change-role',
      body,
    });
  }

  public async userExport(cloudId: string, { includeProductAccess, ...parameters }: GenerateUserExportInput): Promise<string> {
    const response = await this.requestHelper.fetch(`${getConfig().umBasePath}/${cloudId}/users/export`, {
      method: 'POST',
      body: JSON.stringify({
        includeApplicationAccess: includeProductAccess,
        ...parameters,
      }),
    });

    if (!response.ok) {
      throw await toRequestError(response, 'site-user-export');
    }

    const blob = await response.clone().blob();

    return window.URL.createObjectURL(blob);
  }

  public async getUser(cloudId: string, userId: string): Promise<User> {
    return this.requestHelper.getJson({
      url: `${getConfig().umBasePath}/${cloudId}/users/${userId}`,
      requestId: 'get-site-user',
    });
  }

  public async getUserManagedStatus(cloudId: string, userId: string): Promise<UserManagedStatus> {
    return this.requestHelper.getJson({
      url: `${getConfig().umBasePath}/${cloudId}/users/${userId}/managed-status`,
      requestId: 'get-site-user-managed-status',
    });
  }

  public async activateUser(cloudId: string, userId: string): Promise<void> {
    await this.requestHelper.postJson({
      url: `${getConfig().umBasePath}/${cloudId}/users/${userId}/activate`,
      requestId: 'site-user-activate',
    });
  }

  public async deactivateUser(cloudId: string, userId: string): Promise<void> {
    await this.requestHelper.postJson({
      url: `${getConfig().umBasePath}/${cloudId}/users/${userId}/deactivate`,
      requestId: 'site-user-deactivate',
    });
  }

  public async getDirectGroups(cloudId: string, userId: string, start: number, count: number): Promise<PaginatedGroups> {
    const params = stringify({
      'start-index': start,
      count,
    });

    return this.requestHelper.getJson({
      url: `${getConfig().umBasePath}/${cloudId}/users/${userId}/direct-groups${params !== '' ? '?' : ''}${params}`,
      requestId: 'site-user-direct-groups',
    });
  }

  public async getProductAccess(cloudId: string, userId: string): Promise<ProductAccess[]> {
    return this.requestHelper.getJson({
      url: `${getConfig().umBasePath}/${cloudId}/users/${userId}/product-access`,
      requestId: 'site-user-product-access',
      defaultValue: [],
    });
  }

  public async grantAccessToProducts(cloudId: string, users: string[], productIds: string[]): Promise<void> {
    await this.requestHelper.postJson({
      url: `${getConfig().umBasePath}/${cloudId}/users/grant-access`,
      requestId: 'site-user-grant-product-access',
      body: { users, productIds },
    });
  }

  public async revokeAccessToProducts(cloudId: string, users: string[], productIds: string[]): Promise<void> {
    await this.requestHelper.postJson({
      url: `${getConfig().umBasePath}/${cloudId}/users/revoke-access`,
      requestId: 'site-user-revoke-product-access',
      body: { users, productIds },
    });
  }

  public async impersonateUser(cloudId: string, userId: string): Promise<void> {
    await this.requestHelper.postJson({
      url: `${getConfig().umBasePathDirect}/${cloudId}/users/${userId}/impersonate`,
      requestId: `users-impersonate`,
    });
  }

  public async setDefaultProducts(cloudId: string, productId: ProductId, productDefault: boolean): Promise<void> {
    await this.requestHelper.putJson({
      url: `${getConfig().umBasePath}/${cloudId}/product/defaults`,
      requestId: `set-site-default-products`,
      body: {
        productId,
        default: productDefault,
      },
    });
  }

  public async reinviteUser(cloudId: string, userId: string): Promise<void> {
    await this.requestHelper.postJson({
      url: `${getConfig().umBasePath}/${cloudId}/users/${userId}/re-invite`,
      requestId: `users-re-invite`,
    });
  }

  public async promptResetPassword(cloudId: string, userId: string): Promise<void> {
    await this.requestHelper.postJson({
      url: `${getConfig().umBasePath}/${cloudId}/users/${userId}/reset-password`,
      requestId: `users-prompt-reset-password`,
    });
  }

  public async approveImportedGroup(cloudId: string, productId: ProductId, groupId: string): Promise<void> {
    await this.requestHelper.postJson({
      url: `${getConfig().umBasePath}/${cloudId}/product/${productId}/access-config/import/${groupId}`,
      requestId: `approve-site-group-import`,
      body: {},
    });
  }

  public async rejectImportedGroup(cloudId: string, productId: ProductId, groupId: string): Promise<void> {
    await this.requestHelper.deleteJson({
      url: `${getConfig().umBasePath}/${cloudId}/product/${productId}/access-config/import/${groupId}`,
      requestId: `reject-site-group-import`,
    });
  }

  public async addGroupsAccessToProduct(cloudId: string, productId: ProductId, groups: string[], productType: ProductType): Promise<void> {
    await this.requestHelper.postJson({
      url: `${getConfig().umBasePath}/${cloudId}/product/${productId}/access-config/${productType.toLowerCase()}`,
      requestId: `add-site-groups-product-access`,
      body: { groups },
    });
  }

  public async removeGroupAccessToProduct(cloudId: string, productId: ProductId, groupId: string, productType: ProductType): Promise<void> {
    await this.requestHelper.deleteJson({
      url: `${getConfig().umBasePath}/${cloudId}/product/${productId}/access-config/${productType.toLowerCase()}/${groupId}`,
      requestId: `remove-site-group-product-access`,
    });
  }

  public async setDefaultGroup(cloudId: string, productId: ProductId, groupId: string): Promise<void> {
    await this.requestHelper.postJson({
      url: `${getConfig().umBasePath}/${cloudId}/product/${productId}/access-config/default-groups/${groupId}`,
      requestId: `set-site-default-groups`,
    });
  }

  public async removeDefaultGroup(cloudId: string, productId: ProductId, groupId: string): Promise<void> {
    await this.requestHelper.deleteJson({
      url: `${getConfig().umBasePath}/${cloudId}/product/${productId}/access-config/default-groups/${groupId}`,
      requestId: `remove-site-default-groups`,
    });
  }

  public async getProducts(cloudId: string): Promise<SiteProduct[]> {
    return this.requestHelper.getJson<SiteProduct[]>({
      url: `${getConfig().umBasePath}/${cloudId}/products`,
      requestId: 'get-site-products',
    });
  }

  public async getGroups(cloudId: string, { start, count, displayName }: { start?: number | null, count?: number | null, displayName?: string | null }): Promise<PaginatedGroups> {
    const params = stringify({
      count,
      'start-index': start,
      'display-name': displayName,
    });

    return this.requestHelper.getJson<PaginatedGroups>({
      url: `${getConfig().umBasePath}/${cloudId}/groups${params !== '' ? '?' : ''}${params}`,
      requestId: 'get-groups',
    });
  }

  public async getGroup(cloudId: string, groupId: string): Promise<Group> {
    return this.requestHelper.getJson<Group>({
      url: `${getConfig().umBasePath}/${cloudId}/groups/${groupId}`,
      requestId: 'get-group',
    });
  }

  public async createGroup(cloudId: string, { description, name }: UpdateGroupInput): Promise<Group> {
    return this.requestHelper.postJson<Group, UpdateGroupInput>({
      url: `${getConfig().umBasePath}/${cloudId}/groups`,
      requestId: 'create-group',
      body: {
        description,
        name,
      },
    });
  }

  public async updateGroup(cloudId: string, groupId: string, { description, name }: UpdateGroupInput): Promise<Group> {
    return this.requestHelper.putJson<Group, UpdateGroupInput>({
      url: `${getConfig().umBasePath}/${cloudId}/groups/${groupId}`,
      requestId: 'update-group',
      body: {
        description,
        name,
      },
    });
  }

  public async deleteGroup(cloudId: string, groupId: string): Promise<void> {
    return this.requestHelper.deleteJson<any>({
      url: `${getConfig().umBasePath}/${cloudId}/groups/${groupId}`,
      requestId: 'delete-group',
    });
  }

  public async getGroupMembers(cloudId: string, groupId: string, { start, count }: { start?: number | null, count?: number | null }): Promise<PaginatedMembers> {
    const params = stringify({
      count,
      'start-index': start,
    });

    return this.requestHelper.getJson<PaginatedMembers>({
      url: `${getConfig().umBasePath}/${cloudId}/groups/${groupId}/members${params !== '' ? '?' : ''}${params}`,
      requestId: 'get-group-members',
    });
  }

  public async addUsersToGroups(cloudId: string, { users, groups }: AddUsersToGroupsInput): Promise<void> {
    await this.requestHelper.postJson<any, AddUsersToGroupsInput>({
      url: `${getConfig().umBasePath}/${cloudId}/users/add-to-groups`,
      requestId: 'add-group-users',
      body: {
        users,
        groups,
      },
    });
  }

  public async deleteUserFromGroup(cloudId: string, userId: string, groupId: string): Promise<void> {
    await this.requestHelper.deleteJson<any>({
      url: `${getConfig().umBasePath}/${cloudId}/groups/${groupId}/users/${userId}`,
      requestId: 'delete-group-user',
    });
  }

  public async getAccessRequests(cloudId: string, { status, start, count }: AccessRequestListInput): Promise<AccessRequestList> {
    const params = stringify({
      status,
      'start-index': start,
      count,
    });
    const url = `${getConfig().umBasePath}/${cloudId}/access-requests${params ? `?${params}` : ''}`;

    return this.requestHelper.getJson<AccessRequestList>({
      url,
      requestId: 'um-access-requests-list',
    });
  }

  public async approveAccess(cloudId: string, { userId, productId, atlOrigin }: ApproveAccessInput): Promise<any> {
    return this.requestHelper.postJson({
      url: `${getConfig().umBasePath}/${cloudId}/access-requests/approve`,
      requestId: 'um-access-requests-approve',
      body: {
        userId,
        productId,
        atlOrigin,
      },
    });
  }

  public async denyAccess(cloudId: string, { userId, productId, denialReason }: DenyAccessInput): Promise<any> {
    return this.requestHelper.postJson({
      url: `${getConfig().umBasePath}/${cloudId}/access-requests/reject`,
      requestId: 'um-access-requests-reject',
      body: {
        userId,
        productId,
        denialReason,
      },
    });
  }

  public async suggestChanges(cloudId: string, { userId, displayName, email }: SuggestChangesInput): Promise<any> {
    return this.requestHelper.postJson({
      url: `${getConfig().umBasePath}/${cloudId}/users/${userId}/suggest-change-details`,
      requestId: 'um-suggest-change-details',
      body: {
        email,
        displayName,
      },
    });
  }
}
