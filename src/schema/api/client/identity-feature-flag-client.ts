import { getConfig } from 'common/config';

import { RequestHelper } from './request-helper';

export interface IdentityFeatureFlagApiShape {
  [key: string]: boolean;
}

export class IdentityFeatureFlagClient {
  constructor(
    private requestHelper: RequestHelper,
  ) {}

  public async getFeatureFlag(flagKey: string): Promise<IdentityFeatureFlagApiShape> {
    return this.requestHelper.getJson({
      url: `${getConfig().identityFeatureFlagUrl}?${flagKey}`,
      requestId: 'identity-feature-flags',
      storeStrategy: 'cache',
      storeKey: flagKey,
    });
  }
}
