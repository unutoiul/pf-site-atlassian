import { fail } from 'assert';
import { expect } from 'chai';
import * as fetchMock from 'fetch-mock';
import { sandbox as sinonSandbox, SinonSandbox } from 'sinon';

import { FeatureFlagsClient } from 'common/feature-flags';

import { UserManagementRequestHelper } from './um-request-helper';

describe('User Management Request Helper', () => {

  const url = '/admin/s/024c4362-303c-4ea7-bda6-ec5b9a057b5e/groups';

  let umRequestHelper: UserManagementRequestHelper;
  const sandbox: SinonSandbox = sinonSandbox.create();
  let getInstanceStub;
  let consistencyStub;

  beforeEach(() => {
    getInstanceStub = sandbox.stub(FeatureFlagsClient, 'getInstance');
    consistencyStub = sandbox.stub();
    getInstanceStub.returns({
      allFlags: async () => ({
        'um.eventual.consistency': true,
      }),
    });
    umRequestHelper = new UserManagementRequestHelper();
    sandbox.stub(console, 'error');
  });

  afterEach(() => {
    fetchMock.restore();
    sandbox.restore();
  });

  describe('should send 204 requests correctly', () => {

    it('should GET', async () => {
      fetchMock.get(url, {
        status: 204,
      });

      const response = await umRequestHelper.getJson({ url, requestId: 'get-ball' });
      expect(fetchMock.calls().length).to.equal(1);
      expect(response).to.equal(undefined);
    });

    it('should POST', async () => {
      fetchMock.post(url, {
        status: 204,
      });

      const response = await umRequestHelper.postJson({ url, requestId: 'get-ball' }) as any;
      expect(fetchMock.calls().length).to.equal(1);
      expect(response).to.equal(undefined);
    });

    it('should DELETE', async () => {
      fetchMock.delete(url, {
        status: 204,
      });

      const response = await umRequestHelper.deleteJson({ url, requestId: 'get-ball' }) as any;
      expect(fetchMock.calls().length).to.equal(1);
      expect(response).to.equal(undefined);
    });

    it('should PUT', async () => {
      fetchMock.put(url, {
        status: 204,
      });

      const response = await umRequestHelper.putJson({ url, requestId: 'get-ball' }) as any;
      expect(fetchMock.calls().length).to.equal(1);
      expect(response).to.equal(undefined);
    });
  });

  describe('should send 2xx requests correctly', () => {

    it('should GET', async () => {
      fetchMock.get(url, {
        status: 200,
        body: { message: `This sweater is Ralph Lauren, and I have no pants.` },
      });

      const response = await umRequestHelper.getJson({ url, requestId: 'get-ball' });
      expect(fetchMock.calls().length).to.equal(1);
      expect(response.message).to.equal(`This sweater is Ralph Lauren, and I have no pants.`);
    });

    it('should POST', async () => {
      fetchMock.post(url, {
        status: 200,
        body: { message: `This sweater is Ralph Lauren, and I have no pants.` },
      });

      const response = await umRequestHelper.postJson({ url, requestId: 'get-ball' }) as any;
      expect(fetchMock.calls().length).to.equal(1);
      expect(response.message).to.equal(`This sweater is Ralph Lauren, and I have no pants.`);
    });

    it('should DELETE', async () => {
      fetchMock.delete(url, {
        status: 200,
        body: { message: `This sweater is Ralph Lauren, and I have no pants.` },
      });

      const response = await umRequestHelper.deleteJson({ url, requestId: 'get-ball' }) as any;
      expect(fetchMock.calls().length).to.equal(1);
      expect(response.message).to.equal(`This sweater is Ralph Lauren, and I have no pants.`);
    });

    it('should PUT', async () => {
      fetchMock.put(url, {
        status: 200,
        body: { message: `This sweater is Ralph Lauren, and I have no pants.` },
      });

      const response = await umRequestHelper.putJson({ url, requestId: 'get-ball' }) as any;
      expect(fetchMock.calls().length).to.equal(1);
      expect(response.message).equal(`This sweater is Ralph Lauren, and I have no pants.`);
    });

    it('should pass headers correctly', async () => {
      fetchMock.post(url, {
        status: 200,
        body: { message: `This sweater is Ralph Lauren, and I have no pants.` },
      });

      await umRequestHelper.postJson({ url, requestId: 'get-ball', headers: { 'some-header': 'some-value' } });
      expect(fetchMock.lastCall()[1].headers['some-header']).to.deep.equal('some-value');
    });

  });

  describe('should store and set consistency tokens correctly', () => {

    describe('With consistency feature flag off', () => {

      it('should not send token header but should still send additional headers, with feature flag undefined', async () => {

        getInstanceStub.returns({
          allFlags: async () => ({}),
        });

        umRequestHelper = new UserManagementRequestHelper();
        fetchMock.post(url, {
          status: 204,
          headers: {
            'IdPlat-Outstanding-Token': '1e3f9db5-055b',
          },
        });

        fetchMock.get(url, {
          status: 204,
        });

        await umRequestHelper.postJson({ url, requestId: 'fetch-ball' });
        await umRequestHelper.getJson({ url, requestId: 'get-ball' });

        expect(fetchMock.calls().length).to.equal(2);

        expect(fetchMock.lastCall()[1].headers['Content-Type']).to.deep.equal('application/json');
        expect(fetchMock.lastCall()[1].headers['Event-Stream-State']).to.deep.equal(undefined);
      });

      it('should not send token header but should still send additional headers, with feature flag explictly off', async () => {

        getInstanceStub.returns({
          allFlags: async () => ({
            'um.eventual.consistency': false,
          }),
        });

        umRequestHelper = new UserManagementRequestHelper();
        fetchMock.post(url, {
          status: 204,
          headers: {
            'IdPlat-Outstanding-Token': '1e3f9db5-055b',
          },
        });

        fetchMock.get(url, {
          status: 204,
        });

        await umRequestHelper.postJson({ url, requestId: 'fetch-ball' });
        await umRequestHelper.getJson({ url, requestId: 'get-ball' });

        expect(fetchMock.calls().length).to.equal(2);

        expect(fetchMock.lastCall()[1].headers['Content-Type']).to.deep.equal('application/json');
        expect(fetchMock.lastCall()[1].headers['Event-Stream-State']).to.deep.equal(undefined);
      });

    });

    describe('With consistency feature flag on', () => {

      it('Should send token header and any additional headers', async () => {
        fetchMock.post(url, {
          status: 204,
          headers: {
            'IdPlat-Outstanding-Token': '1e3f9db5-055b',
          },
        });

        fetchMock.get(url, {
          status: 204,
        });

        await umRequestHelper.postJson({ url, requestId: 'fetch-ball' });
        await umRequestHelper.getJson({ url, requestId: 'get-ball' });

        expect(fetchMock.calls().length).to.equal(2);

        expect(fetchMock.lastCall()[1].headers['Content-Type']).to.deep.equal('application/json');
        expect(fetchMock.lastCall()[1].headers['Event-Stream-State']).to.deep.equal('At-Least 1e3f9db5-055b');
      });

      it('Should send multiple tokens correctly', async () => {

        fetchMock.post(url, {
          status: 204,
          headers: {
            'IdPlat-Outstanding-Token': '1e3f9db5-055b,13848090-962c',
          },
        });

        fetchMock.get(url, {
          status: 204,
        });

        await umRequestHelper.postJson({ url, requestId: 'fetch-ball' });
        await umRequestHelper.getJson({ url, requestId: 'get-ball' });

        expect(fetchMock.calls().length).to.equal(2);

        expect(fetchMock.lastCall()[1].headers['Event-Stream-State']).to.deep.equal('At-Least 1e3f9db5-055b,At-Least 13848090-962c');
      });

      it('Should send only tokens which are not NONE correctly', async () => {

        fetchMock.post(url, {
          status: 204,
          headers: {
            'IdPlat-Outstanding-Token': 'none,1e3f9db5-055b,13848090-962c',
          },
        });

        fetchMock.get(url, {
          status: 204,
        });

        await umRequestHelper.postJson({ url, requestId: 'fetch-ball' });
        await umRequestHelper.getJson({ url, requestId: 'get-ball' });

        expect(fetchMock.calls().length).to.equal(2);

        expect(fetchMock.lastCall()[1].headers['Event-Stream-State']).to.deep.equal('At-Least 1e3f9db5-055b,At-Least 13848090-962c');
      });

      it('Should flush tokens after retrieval', async () => {
        fetchMock.post(url, {
          status: 204,
          headers: {
            'IdPlat-Outstanding-Token': '1e3f9db5-055b,13848090-962c',
          },
        });

        fetchMock.get(url, {
          status: 204,
        });

        await umRequestHelper.postJson({ url, requestId: 'fetch-ball' });
        await umRequestHelper.getJson({ url, requestId: 'get-ball' });

        expect(fetchMock.calls().length).to.equal(2);

        expect(fetchMock.lastCall()[1].headers).to.not.equal(undefined);
        expect(fetchMock.lastCall()[1].headers['Event-Stream-State']).to.deep.equal('At-Least 1e3f9db5-055b,At-Least 13848090-962c');

        await umRequestHelper.getJson({ url, requestId: 'get-ball' });
        expect(fetchMock.lastCall()[1].headers['Event-Stream-State']).to.deep.equal(undefined);
      });

      it('Should send only correct tokens', async () => {

        fetchMock.post(url, {
          status: 204,
          headers: {
            'IdPlat-Outstanding-incorrect-name-Token': 'none,1e3f9db5-055b,13848090-962c',
          },
        });

        fetchMock.get(url, {
          status: 204,
        });

        await umRequestHelper.postJson({ url, requestId: 'fetch-ball' });
        await umRequestHelper.getJson({ url, requestId: 'get-ball' });

        expect(fetchMock.calls().length).to.equal(2);

        expect(fetchMock.lastCall()[1].headers['Event-Stream-State']).to.deep.equal(undefined);
      });

      it('No tokens', async () => {

        fetchMock.get(url, {
          status: 204,
        });

        await umRequestHelper.getJson({ url, requestId: 'get-ball' });

        expect(fetchMock.lastCall()[1].headers['Event-Stream-State']).to.deep.equal(undefined);
      });

      it('When pre-condition failed, it should retry until a success is returned', async () => {
        consistencyStub.onCall(0).returns({ status: 412 });
        consistencyStub.onCall(1).returns({ status: 204 });

        fetchMock.mock(url, consistencyStub);
        expect(fetchMock.calls().length).to.equal(0);
        await umRequestHelper.getJson({ url, requestId: 'get-ball' });

        expect(fetchMock.calls().length).to.equal(2);
      }).timeout(5000);

      it('When pre-condition failed, it should retry until another error is returned', async () => {
        consistencyStub.onCall(0).returns({ status: 412 });
        consistencyStub.onCall(1).returns({ status: 404 });

        fetchMock.mock(url, consistencyStub);

        try {
          expect(fetchMock.calls().length).to.equal(0);
          await umRequestHelper.getJson({ url, requestId: 'get-ball' });
          fail('Should have failed with an error');
        } catch (e) {
          expect(fetchMock.calls().length).to.equal(2);
          expect(e.status).to.equal(404);
        }
      }).timeout(5000);

      it('When pre-conditioned failed, it should retry until timing out with pre-condition failed ', async () => {
        fetchMock.get(url, {
          status: 412,
        });

        try {
          expect(fetchMock.calls().length).to.equal(0);
          await umRequestHelper.getJson({ url, requestId: 'get-ball' });
        } catch (e) {
          expect(fetchMock.calls().length).to.equal(3);
          expect(e.status).to.equal(412);
        }
      }).timeout(5000);

    });
  });
});
