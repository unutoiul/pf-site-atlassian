import { RequestHelper } from './request-helper';

export interface SiteId {
  cloudId: string;
}

export class EdgeClient {
  constructor(private requestHelper: RequestHelper) {
  }

  public async getCurrentSiteId(): Promise<SiteId> {
    return this.requestHelper.getJson({
      url: '/_edge/tenant_info',
      requestId: `hostname-site-id`,
      storeStrategy: 'cache',
      storeKey: 'current-site-id',
    });
  }
}
