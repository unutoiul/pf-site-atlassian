import { getConfig } from 'common/config';

import { SiteNameAvailabilityCheckResult, SiteRenameStatus } from '../../../schema/schema-types';
import { RequestHelper } from './request-helper';

export interface SiteRenameStatusArgs {
  entityId: string;
  entityType: 'CLOUD';
}

export interface SiteNameAvailabilityCheckArgs {
  cloudName: string;
  entityId: string;
  entityType: 'CLOUD';
}

export class CnasClient {
  constructor(private requestHelper: RequestHelper) {
  }

  public async getRenameStatus(id): Promise<SiteRenameStatus> {
    return this.requestHelper.postJson<SiteRenameStatus, SiteRenameStatusArgs>({
      url: `${getConfig().cnasUrl}/rename/status`,
      requestId: `site-rename-status`,
      body: {
        entityId: id,
        entityType: 'CLOUD',
      },
    });
  }

  public async checkNameAvailability(id: string, name: string): Promise<SiteNameAvailabilityCheckResult> {
    return this.requestHelper.postJson<SiteNameAvailabilityCheckResult, SiteNameAvailabilityCheckArgs>({
      url: `${getConfig().cnasUrl}/rename/check`,
      requestId: `site-rename-status`,
      body: {
        cloudName: name,
        entityId: id,
        entityType: 'CLOUD',
      },
    });
  }
}
