import { getConfig } from 'common/config';

import { MemberToken } from '../../schema-types';
import { RequestHelper } from './request-helper';

/***
 * This client represents Identity Platform service.
 * The documentation for Identity Platform can be found at https://id.prod.atl-paas.net/swagger/#/
 */
export class IdentityClient {
  constructor(
    private requestHelper: RequestHelper,
  ) {}

  public async getMemberTokens(memberId: string): Promise<MemberToken[]> {
    return this.requestHelper.getJson({
      url: `${getConfig().apiUrl}/users/${memberId}/manage/api-tokens`,
      requestId: `identity-platform-service-get-member-tokens`,
    });
  }

  public async revokeMemberToken(memberId: string, memberTokenId: string): Promise<void> {
    await this.requestHelper.deleteJson({
      url: `${getConfig().apiUrl}/users/${memberId}/manage/api-tokens/${memberTokenId}`,
      requestId: `identity-platform-service-delete-member-token`,
    });
  }
}
