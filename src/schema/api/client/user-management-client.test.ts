import { expect } from 'chai';
import { sandbox as sinonSandbox, SinonSandbox, SinonStub } from 'sinon';

import { getConfig } from 'common/config';
import { BadRequestError } from 'common/error';

import { Permissions, permissionToRole } from '../../../site/user-management/users/user-permission-section/permissions';
import {
  AccessRequestStatus,
  AddGroupsAccessToProductInput,
  AddUsersToGroupsInput,
  ApproveAccessInput,
  DenyAccessInput,
  Group,
  PaginatedGroups,
  PaginatedMembers,
  SiteInviteUsersInput,
  SuggestChangesInput,
  UpdateGroupInput,
} from '../../schema-types';
import { BodyRequestOptions, RequestOptions } from './request-store';
import { UserManagementClient } from './user-management-client';

describe('UserManagementClient', () => {
  let sandbox: SinonSandbox;
  let userManagementClient: UserManagementClient;
  let mockRequestHelper: {
    getJson: SinonStub,
    postJson: SinonStub,
    putJson: SinonStub,
    deleteJson: SinonStub,
    fetch: SinonStub,
  };
  const blobUrl = 'blob:http://example.com/66d2b1b4-b7b5-457c-a9c0-f4aee87a9b6f';

  beforeEach(() => {
    sandbox = sinonSandbox.create();

    mockRequestHelper = {
      getJson: sandbox.stub(),
      postJson: sandbox.stub(),
      putJson: sandbox.stub(),
      deleteJson: sandbox.stub(),
      fetch: sandbox.stub(),
    };

    userManagementClient = new UserManagementClient(mockRequestHelper as any);
    const createObjectURLStub = sandbox.stub(window.URL, 'createObjectURL');
    createObjectURLStub.returns(blobUrl);
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('getSiteContext', () => {
    let apiResponse;
    const validFirstActivationDate = 1540175786309;

    beforeEach(() => {
      apiResponse = {
        isUsingIdentityPlatform: true,
        isAaOptOutDisabled: true,
        isInVertigo: true,
        isAaEnabled: true,
        isAutoUpgradeMode: true,
        products: {
          'jira-software.ondemand': '',
        },
        userId: '655363:1a082af1-7d4d-406b-be89-9a95f0669f34',
        cloudId: '5934cbbd-cc22-4a71-bfc6-c2a239b7b55c',
      };
    });

    it('makes a correct request', async () => {
      mockRequestHelper.getJson.resolves();

      await userManagementClient.getSiteContext();
      const args: RequestOptions = mockRequestHelper.getJson.lastCall.args[0];

      expect(mockRequestHelper.getJson.calledOnce).to.equal(true);
      expect(args.url).to.equal('/admin/rest/um/1/context');
      expect(args.requestId).to.equal('um-context');
    });

    it('returns context with the firstActivationDate as a string', async () => {
      apiResponse.firstActivationDate = validFirstActivationDate;
      mockRequestHelper.getJson.resolves(apiResponse);

      const response = await userManagementClient.getSiteContext();

      expect(response).to.contain({ firstActivationDate: validFirstActivationDate.toString() });
    });

    it('returns context with firstActivationDate set to \'0\' when the API doesn\'t return the field', async () => {
      mockRequestHelper.getJson.resolves(apiResponse);

      const response = await userManagementClient.getSiteContext();

      expect(response).to.contain({ firstActivationDate: '0' });
    });
  });

  describe('getProducts', () => {
    it('should return the result of the endpoint', async () => {
      mockRequestHelper.getJson.resolves();

      await userManagementClient.getProducts('cloudId');

      expect(mockRequestHelper.getJson.calledOnce).to.equal(true);
      const args: RequestOptions = mockRequestHelper.getJson.lastCall.args[0];
      expect(args.url).to.equal(`${getConfig().umBasePath}/cloudId/products`);
      expect(args.requestId).to.equal('get-site-products');
    });
  });

  describe('getUsers', () => {

    it('For inactive status; activeStatus query parameter should be true', async () => {
      mockRequestHelper.getJson.resolves({
        total: 0,
        users: [],
      });
      await userManagementClient.getUsers('cloudId', { start: 1, count: 20, activeStatus: ['inactive'] });

      expect(mockRequestHelper.getJson.calledOnce).to.equal(true);
      const args: RequestOptions = mockRequestHelper.getJson.lastCall.args[0];
      expect(args.url).to.equal(`${getConfig().umBasePath}/cloudId/users?active-status=false&count=20&start-index=1`);
      expect(args.requestId).to.equal('get-site-users');
    });

    it('For active status; activeStatus query parameter should be true', async () => {
      mockRequestHelper.getJson.resolves({
        total: 0,
        users: [],
      });
      await userManagementClient.getUsers('cloudId', { start: 1, count: 20, activeStatus: ['active'] });

      expect(mockRequestHelper.getJson.calledOnce).to.equal(true);
      const args: RequestOptions = mockRequestHelper.getJson.lastCall.args[0];
      expect(args.url).to.equal(`${getConfig().umBasePath}/cloudId/users?active-status=true&count=20&start-index=1`);
      expect(args.requestId).to.equal('get-site-users');
    });

    it('should return the result of the endpoint', async () => {
      mockRequestHelper.getJson.resolves({
        total: 0,
        users: [],
      });
      await userManagementClient.getUsers('cloudId', { start: 1, count: 20 });

      expect(mockRequestHelper.getJson.calledOnce).to.equal(true);
      const args: RequestOptions = mockRequestHelper.getJson.lastCall.args[0];
      expect(args.url).to.equal(`${getConfig().umBasePath}/cloudId/users?count=20&start-index=1`);
      expect(args.requestId).to.equal('get-site-users');
    });
  });

  describe('inviteUsers', () => {
    it('should return the result of the endpoint', async () => {
      mockRequestHelper.postJson.resolves();

      await userManagementClient.inviteUsers('cloudId', { emails: ['cyberdash@gmail.com'], atlOrigin: 'some-atl-origin' });

      expect(mockRequestHelper.postJson.calledOnce).to.equal(true);
      const args: BodyRequestOptions<void, SiteInviteUsersInput> = mockRequestHelper.postJson.lastCall.args[0];
      expect(args.url).to.equal(`${getConfig().umBasePath}/cloudId/users/invite`);
      expect(args.body && args.body.emails && args.body.emails[0]).to.equal('cyberdash@gmail.com');
      expect(args.requestId).to.equal('invite-site-users');
      expect(args.headers && args.headers['Origin-ID']).to.equal('some-atl-origin');
    });

    it('should not pass roles when basic role is passed', async () => {
      mockRequestHelper.postJson.resolves();

      await userManagementClient.inviteUsers('cloudId', { emails: ['cyberdash@gmail.com'], role: 'basic' });

      expect(mockRequestHelper.postJson.calledOnce).to.equal(true);
      const args: BodyRequestOptions<void, SiteInviteUsersInput> = mockRequestHelper.postJson.lastCall.args[0];
      expect(args.url).to.equal(`${getConfig().umBasePath}/cloudId/users/invite`);
      expect(args.body && args.body.emails && args.body.emails[0]).to.equal('cyberdash@gmail.com');
      expect(args.body && args.body.role).to.equal(undefined);
      expect(args.requestId).to.equal('invite-site-users');
    });

    it('should return site/admin when siteadmin role is passed', async () => {
      mockRequestHelper.postJson.resolves();

      await userManagementClient.inviteUsers('cloudId', { emails: ['cyberdash@gmail.com'], role: Permissions.SITEADMIN });

      expect(mockRequestHelper.postJson.calledOnce).to.equal(true);
      const args: BodyRequestOptions<void, SiteInviteUsersInput> = mockRequestHelper.postJson.lastCall.args[0];
      expect(args.url).to.equal(`${getConfig().umBasePath}/cloudId/users/invite`);
      expect(args.body && args.body.emails && args.body.emails[0]).to.equal('cyberdash@gmail.com');
      expect(args.body && args.body.role).to.equal(permissionToRole[Permissions.SITEADMIN]);
      expect(args.requestId).to.equal('invite-site-users');
    });

    it('should return site/trusted-user when trusted-user role is passed', async () => {
      mockRequestHelper.postJson.resolves();

      await userManagementClient.inviteUsers('cloudId', { emails: ['cyberdash@gmail.com'], role: Permissions.TRUSTED, atlOrigin: 'some-atl-origin' });

      expect(mockRequestHelper.postJson.calledOnce).to.equal(true);
      const args: BodyRequestOptions<void, SiteInviteUsersInput> = mockRequestHelper.postJson.lastCall.args[0];
      expect(args.url).to.equal(`${getConfig().umBasePath}/cloudId/users/invite`);
      expect(args.body && args.body.emails && args.body.emails[0]).to.equal('cyberdash@gmail.com');
      expect(args.body && args.body.role).to.equal(permissionToRole[Permissions.TRUSTED]);
      expect(args.requestId).to.equal('invite-site-users');
    });

  });

  describe('setDefaultProducts', () => {
    it('should return the result of the endpoint', async () => {
      mockRequestHelper.putJson.resolves();

      await userManagementClient.setDefaultProducts('cloudId', 'jira', false);

      expect(mockRequestHelper.putJson.calledOnce).to.equal(true);
      const args: RequestOptions = mockRequestHelper.putJson.lastCall.args[0];
      expect(args.url).to.equal(`${getConfig().umBasePath}/cloudId/product/defaults`);
      expect(args.requestId).to.equal('set-site-default-products');
    });
  });

  describe('rejectImportedGroup', () => {
    it('should return the result of the endpoint', async () => {
      mockRequestHelper.deleteJson.resolves();

      await userManagementClient.rejectImportedGroup('cloudId', 'jira', '8c5f8f76-78ce-11e8-adc0-fa7ae01bbebc');

      expect(mockRequestHelper.deleteJson.calledOnce).to.equal(true);
      const args: RequestOptions = mockRequestHelper.deleteJson.lastCall.args[0];
      expect(args.url).to.equal(`${getConfig().umBasePath}/cloudId/product/jira/access-config/import/8c5f8f76-78ce-11e8-adc0-fa7ae01bbebc`);
      expect(args.requestId).to.equal('reject-site-group-import');
    });
  });

  describe('approveImportedGroup', () => {
    it('should return the result of the endpoint', async () => {
      mockRequestHelper.postJson.resolves();

      await userManagementClient.approveImportedGroup('cloudId', 'jira', '8c5f8f76-78ce-11e8-adc0-fa7ae01bbebc');

      expect(mockRequestHelper.postJson.calledOnce).to.equal(true);
      const args: RequestOptions = mockRequestHelper.postJson.lastCall.args[0];
      expect(args.url).to.equal(`${getConfig().umBasePath}/cloudId/product/jira/access-config/import/8c5f8f76-78ce-11e8-adc0-fa7ae01bbebc`);
      expect(args.requestId).to.equal('approve-site-group-import');
    });
  });

  describe('addGroupsAccessToProduct', () => {
    it('should return the result of the endpoint', async () => {
      mockRequestHelper.postJson.resolves();

      await userManagementClient.addGroupsAccessToProduct('cloudId', 'jira', ['jira-users'], 'USE');

      expect(mockRequestHelper.postJson.calledOnce).to.equal(true);
      const args: BodyRequestOptions<void, AddGroupsAccessToProductInput> = mockRequestHelper.postJson.lastCall.args[0];
      expect(args.url).to.equal(`${getConfig().umBasePath}/cloudId/product/jira/access-config/use`);
      expect(args.body && args.body.groups && args.body.groups[0]).to.equal('jira-users');
      expect(args.requestId).to.equal('add-site-groups-product-access');
    });
  });

  describe('removeGroupAccessToProduct', () => {
    it('should return the result of the endpoint', async () => {
      mockRequestHelper.deleteJson.resolves();

      await userManagementClient.removeGroupAccessToProduct('cloudId', 'jira', 'jira-users', 'USE');

      expect(mockRequestHelper.deleteJson.calledOnce).to.equal(true);
      const args: RequestOptions = mockRequestHelper.deleteJson.lastCall.args[0];
      expect(args.url).to.equal(`${getConfig().umBasePath}/cloudId/product/jira/access-config/use/jira-users`);
      expect(args.requestId).to.equal('remove-site-group-product-access');
    });
  });

  describe('impersonateUser', () => {
    it('should call correct endpoint', async () => {
      mockRequestHelper.postJson.resolves();

      await userManagementClient.impersonateUser('cloudId', 'userId');

      expect(mockRequestHelper.postJson.calledOnce).to.equal(true);
      const args: RequestOptions = mockRequestHelper.postJson.lastCall.args[0];
      expect(args.url).to.equal(`${getConfig().umBasePathDirect}/cloudId/users/userId/impersonate`);
      expect(args.requestId).to.equal('users-impersonate');
    });
  });

  describe('setDefaultGroup', () => {
    it('should return the result of the endpoint', async () => {
      mockRequestHelper.postJson.resolves();

      await userManagementClient.setDefaultGroup('cloudId', 'jira', 'jira-users');

      expect(mockRequestHelper.postJson.calledOnce).to.equal(true);
      const args: RequestOptions = mockRequestHelper.postJson.lastCall.args[0];
      expect(args.url).to.equal(`${getConfig().umBasePath}/cloudId/product/jira/access-config/default-groups/jira-users`);
      expect(args.requestId).to.equal('set-site-default-groups');
    });
  });

  describe('reinviteUser', () => {
    it('should call correct endpoint', async () => {
      mockRequestHelper.postJson.resolves();

      await userManagementClient.reinviteUser('cloudId', 'userId');

      expect(mockRequestHelper.postJson.calledOnce).to.equal(true);
      const args: RequestOptions = mockRequestHelper.postJson.lastCall.args[0];
      expect(args.url).to.equal(`${getConfig().umBasePath}/cloudId/users/userId/re-invite`);
      expect(args.requestId).to.equal('users-re-invite');
    });
  });

  describe('promptResetPassword', () => {
    it('should call correct endpoint', async () => {
      mockRequestHelper.postJson.resolves();

      await userManagementClient.promptResetPassword('cloudId', 'userId');

      expect(mockRequestHelper.postJson.calledOnce).to.equal(true);
      const args: RequestOptions = mockRequestHelper.postJson.lastCall.args[0];
      expect(args.url).to.equal(`${getConfig().umBasePath}/cloudId/users/userId/reset-password`);
      expect(args.requestId).to.equal('users-prompt-reset-password');
    });
  });

  describe('removeUserFromSite', () => {
    it('should call correct endpoint', async () => {
      mockRequestHelper.deleteJson.resolves();

      await userManagementClient.removeUserFromSite('cloudId', 'userId');

      expect(mockRequestHelper.deleteJson.calledOnce).to.equal(true);
      const args: RequestOptions = mockRequestHelper.deleteJson.lastCall.args[0];
      expect(args.url).to.equal(`${getConfig().umBasePath}/cloudId/users/userId`);
      expect(args.requestId).to.equal('site-user-remove');
    });
  });

  describe('userExport', () => {
    it('should call endpoint with correct parameters', async () => {
      const response = {
        ok: true,
        blob: () => new Blob(['id,name,etc'], { type: 'text/csv' }),
        clone: () => response,
      };
      mockRequestHelper.fetch.resolves(response);

      const parameters = {
        includeProductAccess: false,
        includeGroups: false,
        includeInactiveUsers: false,
        selectedGroupIds: [],
      };
      const url = await userManagementClient.userExport('cloudId', parameters);

      expect(mockRequestHelper.fetch.calledOnce).to.equal(true);
      const args = mockRequestHelper.fetch.lastCall.args;
      expect(args[0]).to.equal(`${getConfig().umBasePath}/cloudId/users/export`);
      const requestOptions: RequestInit = args[1];
      expect(requestOptions.method).to.equal('POST');
      expect(JSON.parse(requestOptions.body as string)).to.deep.equal({
        includeApplicationAccess: false,
        includeGroups: false,
        includeInactiveUsers: false,
        selectedGroupIds: [],
      });
      expect(url).to.equal(blobUrl);
    });

    it('should throw on non-ok response', async () => {
      const response = {
        ok: false,
        status: 400,
      };
      mockRequestHelper.fetch.resolves(response);

      await userManagementClient.userExport('cloudId', {} as any).then(() => {
        expect.fail();
      }, e => {
        expect(e instanceof BadRequestError).to.equal(true);
        expect((e as BadRequestError).id).to.equal('site-user-export-fetch-error');
      });
    });
  });

  describe('removeDefaultGroup', () => {
    it('should return the result of the endpoint', async () => {
      mockRequestHelper.deleteJson.resolves();

      await userManagementClient.removeDefaultGroup('cloudId', 'jira', 'jira-users');

      expect(mockRequestHelper.deleteJson.calledOnce).to.equal(true);
      const args: RequestOptions = mockRequestHelper.deleteJson.lastCall.args[0];
      expect(args.url).to.equal(`${getConfig().umBasePath}/cloudId/product/jira/access-config/default-groups/jira-users`);
      expect(args.requestId).to.equal('remove-site-default-groups');
    });
  });

  describe('getGroups', () => {
    it('returns the result of the endpoint', async () => {
      const response: PaginatedGroups = {
        total: 0,
        groups: [],
      };

      mockRequestHelper.getJson.resolves(response);

      const result = await userManagementClient.getGroups('cloudId', {});
      const args: RequestOptions = mockRequestHelper.getJson.lastCall.args[0];

      expect(result).to.deep.equal(response);
      expect(mockRequestHelper.getJson.calledOnce).to.equal(true);
      expect(args.url).to.equal(`${getConfig().umBasePath}/cloudId/groups`);
    });
  });

  describe('getGroup', () => {
    it('returns the result of the endpoint', async () => {
      const response: Group = {
        id: '8c5f8f76-78ce-11e8-adc0-fa7ae01bbebc',
        name: 'my-group',
        description: 'My description.',
        unmodifiable: false,
        productPermissions: [],
        defaultForProducts: [],
        sitePrivilege: 'NONE',
        userTotal: 0,
        managementAccess: 'ALL',
        ownerType: null,
      };

      mockRequestHelper.getJson.resolves(response);

      const result = await userManagementClient.getGroup('cloudId', '8c5f8f76-78ce-11e8-adc0-fa7ae01bbebc');
      const args: RequestOptions = mockRequestHelper.getJson.lastCall.args[0];

      expect(result).to.deep.equal(response);
      expect(mockRequestHelper.getJson.calledOnce).to.equal(true);
      expect(args.url).to.equal(`${getConfig().umBasePath}/cloudId/groups/8c5f8f76-78ce-11e8-adc0-fa7ae01bbebc`);
    });
  });

  describe('createGroup', () => {
    it('returns the result of the endpoint', async () => {
      const response: Group = {
        id: '8c5f8f76-78ce-11e8-adc0-fa7ae01bbebc',
        name: 'my-group',
        description: 'My description.',
        unmodifiable: false,
        productPermissions: [],
        defaultForProducts: [],
        sitePrivilege: 'NONE',
        userTotal: 0,
        managementAccess: 'ALL',
        ownerType: null,
      };
      const input: UpdateGroupInput = {
        name: 'my-group',
        description: 'My description.',
      };

      mockRequestHelper.postJson.resolves(response);

      const result = await userManagementClient.createGroup('cloudId', input);
      const args: BodyRequestOptions<Group, UpdateGroupInput> = mockRequestHelper.postJson.lastCall.args[0];

      expect(result).to.deep.equal(response);
      expect(mockRequestHelper.postJson.calledOnce).to.equal(true);
      expect(args.url).to.equal(`${getConfig().umBasePath}/cloudId/groups`);
      expect(args.body).to.deep.equal(input);
    });
  });

  describe('updateGroup', () => {
    it('returns the result of the endpoint', async () => {
      const response: Group = {
        id: '8c5f8f76-78ce-11e8-adc0-fa7ae01bbebc',
        name: 'my-group',
        description: 'My description.',
        unmodifiable: false,
        productPermissions: [],
        defaultForProducts: [],
        sitePrivilege: 'NONE',
        userTotal: 0,
        managementAccess: 'ALL',
        ownerType: null,
      };
      const input: UpdateGroupInput = {
        name: 'my-group',
        description: 'My description.',
      };

      mockRequestHelper.putJson.resolves(response);

      const result = await userManagementClient.updateGroup('cloudId', '8c5f8f76-78ce-11e8-adc0-fa7ae01bbebc', input);
      const args: BodyRequestOptions<Group, UpdateGroupInput> = mockRequestHelper.putJson.lastCall.args[0];

      expect(result).to.deep.equal(response);
      expect(mockRequestHelper.putJson.calledOnce).to.equal(true);
      expect(args.url).to.equal(`${getConfig().umBasePath}/cloudId/groups/8c5f8f76-78ce-11e8-adc0-fa7ae01bbebc`);
      expect(args.body).to.deep.equal(input);
    });
  });

  describe('deleteGroup', () => {
    it('is called on the correct resource', async () => {
      mockRequestHelper.deleteJson.resolves();

      await userManagementClient.deleteGroup('cloudId', '8c5f8f76-78ce-11e8-adc0-fa7ae01bbebc');
      const args: RequestOptions = mockRequestHelper.deleteJson.lastCall.args[0];

      expect(mockRequestHelper.deleteJson.calledOnce).to.equal(true);
      expect(args.url).to.equal(`${getConfig().umBasePath}/cloudId/groups/8c5f8f76-78ce-11e8-adc0-fa7ae01bbebc`);
    });
  });

  describe('getGroupMembers', () => {
    it('returns the result of the endpoint', async () => {
      const response: PaginatedMembers = {
        total: 0,
        users: [],
      };

      mockRequestHelper.getJson.resolves(response);

      const result = await userManagementClient.getGroupMembers('cloudId', '8c5f8f76-78ce-11e8-adc0-fa7ae01bbebc', {});
      const args: RequestOptions = mockRequestHelper.getJson.lastCall.args[0];

      expect(result).to.deep.equal(response);
      expect(mockRequestHelper.getJson.calledOnce).to.equal(true);
      expect(args.url).to.equal(`${getConfig().umBasePath}/cloudId/groups/8c5f8f76-78ce-11e8-adc0-fa7ae01bbebc/members`);
    });
  });

  describe('addUsersToGroups', () => {
    it('is called with correct parameters', async () => {
      const input: AddUsersToGroupsInput = {
        groups: ['test-group-id'],
        users: ['test-user-id'],
      };

      mockRequestHelper.postJson.resolves();

      await userManagementClient.addUsersToGroups('cloudId', input);
      const args: BodyRequestOptions<void, AddUsersToGroupsInput> = mockRequestHelper.postJson.lastCall.args[0];

      expect(mockRequestHelper.postJson.calledOnce).to.equal(true);
      expect(args.url).to.equal(`${getConfig().umBasePath}/cloudId/users/add-to-groups`);
      expect(args.body).to.deep.equal(input);
    });
  });

  describe('deleteUserFromGroup', () => {
    it('is called on the correct resource', async () => {
      mockRequestHelper.deleteJson.resolves();

      await userManagementClient.deleteUserFromGroup('cloudId', '8c5f8f76-78ce-11e8-adc0-fa7ae01bbebc', '3b296b50-66bd-4d96-a8a2-5fe50b5599a5');
      const args: RequestOptions = mockRequestHelper.deleteJson.lastCall.args[0];

      expect(mockRequestHelper.deleteJson.calledOnce).to.equal(true);
      expect(args.url).to.equal(`${getConfig().umBasePath}/cloudId/groups/3b296b50-66bd-4d96-a8a2-5fe50b5599a5/users/8c5f8f76-78ce-11e8-adc0-fa7ae01bbebc`);
    });
  });

  describe('getUser', () => {
    it('should return the result of the endpoint', async () => {
      const response = {
        id: 'userId',
        email: 'test@example.com',
        displayName: 'test test',
        active: true,
      };
      mockRequestHelper.getJson.resolves(response);

      const result = await userManagementClient.getUser('cloudId', 'userId');

      expect(result).to.equal(response);

      expect(mockRequestHelper.getJson.calledOnce).to.equal(true);
      const args: RequestOptions = mockRequestHelper.getJson.lastCall.args[0];
      expect(args.url).to.equal(`${getConfig().umBasePath}/cloudId/users/userId`);
      expect(args.requestId).to.equal('get-site-user');
    });
  });

  describe('getUserManagedStatus', () => {
    it('should return the result of the endpoint', async () => {
      const response = {
        managed: true,
        owner: {
          ownerType: 'Google',
          ownerId: 'Google012345678',
        },
      };
      mockRequestHelper.getJson.resolves(response);

      const result = await userManagementClient.getUserManagedStatus('cloudId', 'userId');

      expect(result).to.equal(response);

      expect(mockRequestHelper.getJson.calledOnce).to.equal(true);
      const args: RequestOptions = mockRequestHelper.getJson.lastCall.args[0];
      expect(args.url).to.equal(`${getConfig().umBasePath}/cloudId/users/userId/managed-status`);
      expect(args.requestId).to.equal('get-site-user-managed-status');
    });
  });

  describe('getDirectGroups', () => {
    const start = 1;
    const count = 10;

    it('should return the result of the endpoint', async () => {
      const response = {
        total: 2,
        groups: [
          {
            id: '8bd28362-a82b-48d6-bc72-2fa203687de3',
            name: 'confluence-users',
            description: '',
            productPermissions: [],
            defaultForProducts: [],
            sitePrivilege: 'NONE',
            unmodifiable: false,
            userTotal: 0,
            managementAccess: 'NONE',
            ownerType: 'MANAGED',
          } as Group,
          {
            id: 'e55da5fa-7d49-4e36-b198-a9cac33b207b',
            name: 'jira-users',
            description: '',
            productPermissions: [],
            defaultForProducts: [],
            sitePrivilege: 'NONE',
            unmodifiable: false,
            userTotal: 0,
            managementAccess: 'NONE',
            ownerType: 'MANAGED',
          } as Group,
        ],
      };
      mockRequestHelper.getJson.resolves(response);

      const result = await userManagementClient.getDirectGroups('cloudId', 'userId', start, count);

      expect(result).to.equal(response);

      expect(mockRequestHelper.getJson.calledOnce).to.equal(true);
      const args: RequestOptions = mockRequestHelper.getJson.lastCall.args[0];
      expect(args.url).to.equal(`${getConfig().umBasePath}/cloudId/users/userId/direct-groups?count=${count}&start-index=${start}`);
      expect(args.requestId).to.equal('site-user-direct-groups');
    });
  });

  describe('getProductAccess', () => {
    it('should return the result of the endpoint', async () => {
      const response = [
        {
          productName: 'Confluence',
          accessLevel: 'USE',
        },
        {
          productName: 'Jira',
          accessLevel: 'USE',
        },
      ];
      mockRequestHelper.getJson.resolves(response);

      const result = await userManagementClient.getProductAccess('cloudId', 'userId');

      expect(result).to.equal(response);

      expect(mockRequestHelper.getJson.calledOnce).to.equal(true);
      const args: RequestOptions = mockRequestHelper.getJson.lastCall.args[0];
      expect(args.url).to.equal(`${getConfig().umBasePath}/cloudId/users/userId/product-access`);
      expect(args.requestId).to.equal('site-user-product-access');
    });
  });

  describe('getAccessRequests', () => {
    it('should return the result of pending access requests', async () => {
      const response = {
        total: 1,
        accessRequests: [
          {
            user: {
              id: '557057:5bc5f1a2-ba3f-472f-aeef-7eb41a9fba97',
              displayName: 'Dipanjan Laha',
              email: 'dlaha@atlassian.com',
            },
            requestedProducts: [{
              productId: 'confluence',
              productName: 'Confluence',
              status: 'PENDING',
              requestedTime: '2017-07-10T14:00:00Z',
            }],
          },
        ],
      };
      mockRequestHelper.getJson.resolves(response);

      const result = await userManagementClient.getAccessRequests('cloudId', {
        status: ['PENDING'] as AccessRequestStatus[],
        start: 1,
        count: 20,
      });

      expect(result).to.equal(response);

      expect(mockRequestHelper.getJson.calledOnce).to.equal(true);
      const args: RequestOptions = mockRequestHelper.getJson.lastCall.args[0];
      expect(args.url).to.equal(`${getConfig().umBasePath}/cloudId/access-requests?count=20&start-index=1&status=PENDING`);
      expect(args.requestId).to.equal('um-access-requests-list');
    });
  });

  describe('approveAccess', () => {
    it('should make approve access call', async () => {
      const response = {};
      const body = {
        userId: 'userId',
        productId: 'productId',
        atlOrigin: 'abc-xyz',
      };
      mockRequestHelper.postJson.resolves(response);

      const result = await userManagementClient.approveAccess('cloudId', body);

      expect(result).to.equal(response);

      expect(mockRequestHelper.postJson.calledOnce).to.equal(true);
      const args: BodyRequestOptions<void, ApproveAccessInput> = mockRequestHelper.postJson.lastCall.args[0];
      expect(args.url).to.equal(`${getConfig().umBasePath}/cloudId/access-requests/approve`);
      expect(args.body).to.deep.equal(body);
      expect(args.requestId).to.equal('um-access-requests-approve');
    });
  });

  describe('denyAccess', () => {
    it('should make deny access call', async () => {
      const response = {};
      const body = {
        userId: 'userId',
        productId: 'productId',
        denialReason: 'Hello world',
      };
      mockRequestHelper.postJson.resolves(response);

      const result = await userManagementClient.denyAccess('cloudId', body);

      expect(result).to.equal(response);

      expect(mockRequestHelper.postJson.calledOnce).to.equal(true);
      const args: BodyRequestOptions<void, DenyAccessInput> = mockRequestHelper.postJson.lastCall.args[0];
      expect(args.url).to.equal(`${getConfig().umBasePath}/cloudId/access-requests/reject`);
      expect(args.body).to.deep.equal(body);
      expect(args.requestId).to.equal('um-access-requests-reject');
    });
  });

  describe('suggestChanges', () => {
    it('should make the suggest changes call', async () => {
      const response = {};

      mockRequestHelper.postJson.resolves(response);

      const result = await userManagementClient.suggestChanges('cloudId', {
        userId: 'userId',
        displayName: 'Joe Black',
        email: 'test@example.com',
      });

      expect(result).to.equal(response);
      expect(mockRequestHelper.postJson.calledOnce).to.equal(true);
      const args: BodyRequestOptions<void, SuggestChangesInput> = mockRequestHelper.postJson.lastCall.args[0];

      expect(args.url).to.equal(`${getConfig().umBasePath}/cloudId/users/userId/suggest-change-details`);
      expect(args.body).to.deep.equal({
        displayName: 'Joe Black',
        email: 'test@example.com',
      });
      expect(args.requestId).to.equal('um-suggest-change-details');
    });
  });
});
