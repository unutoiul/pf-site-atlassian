import { getConfig } from 'common/config';

import { RequestHelper } from './request-helper';

export interface XFlowEnabled {
  'product-suggestions-enabled': boolean;
}

export interface SiteInfo {
  id: string;
  displayName: string;
  avatarUrl: string;
}

export interface SiteResponse {
  sites: SiteInfo[];
}

export class SiteClient {
  constructor(private requestHelper: RequestHelper) {
  }

  public async getSites(): Promise<SiteResponse> {
    return this.requestHelper.getJson({
      url: `${getConfig().apiUrl}/site/admin/cloud`,
      requestId: `admin-sites`,
      storeStrategy: 'cache',
      storeKey: 'admin-sites',
    });
  }

  public async isXFlowEnabled(cloudId: string): Promise<XFlowEnabled> {
    return this.requestHelper.getJson({
      url: `${getConfig().apiUrl}/site/${cloudId}/setting/xflow/product-suggestions-enabled`,
      requestId: 'xflow-product-suggestions-enabled',
      storeStrategy: 'cache',
      storeKey: 'xflow-settings',
    });
  }

  public async setDisplayName(cloudId: string, displayName: string) {
    return this.requestHelper.putJson({
      url: `${getConfig().apiUrl}/site/${cloudId}/setting/site.metadata/display.name`,
      requestId: 'set-display-name',
      body: {
        'display.name': displayName,
      },
    });
  }
}
