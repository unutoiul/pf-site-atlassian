import { expect } from 'chai';
import { sandbox as sinonSandbox, SinonSandbox } from 'sinon';

import { getConfig } from 'common/config';

import { OrganizationClient } from './organization-client';
import { RequestOptions } from './request-store';

describe('Organization Client', () => {
  let sandbox: SinonSandbox;
  let organizationClient: OrganizationClient;
  let mockRequestHelper;
  let baseUrl;

  beforeEach(() => {
    sandbox = sinonSandbox.create();
    baseUrl = `${getConfig().orgUrl}`;

    mockRequestHelper = {
      postJson: sandbox.stub(),
      getJson: sandbox.stub(),
      deleteJson: sandbox.stub(),
    };

    // tslint:disable-next-line no-unnecessary-type-assertion
    organizationClient = new OrganizationClient(mockRequestHelper as any);
  });

  afterEach(() => {
    sandbox.restore();
  });

  const response = [
    {
      id: 'site-1',
      siteUrl: 'http://site-1',
      products: [
        {
          productKey: 'jira-core',
          productName: 'Jira Core',
          siteUrl: 'http://site-1',
        },
        {
          productKey: 'confluence',
          productName: 'Confluence',
          siteUrl: 'http://site-1',
        },
      ],
    },
    {
      id: 'site-2',
      siteUrl: 'http://site-2',
      products: ['Jira Core', 'Confluence'],
    },
    {
      id: 'site-3',
      siteUrl: 'http://site-3',
    },
  ];
  const expectedResponse = [
    {
      id: 'site-1',
      siteUrl: 'http://site-1',
      products: [
        {
          productKey: 'jira-core',
          productName: 'Jira Core',
          siteUrl: 'http://site-1',
        },
        {
          productKey: 'confluence',
          productName: 'Confluence',
          siteUrl: 'http://site-1',
        },
      ],
    },
    {
      id: 'site-2',
      siteUrl: 'http://site-2',
      products: [
        {
          productKey: 'Jira Core',
          productName: 'Jira Core',
        },
        {
          productKey: 'Confluence',
          productName: 'Confluence',
        },
      ],
    },
    {
      id: 'site-3',
      siteUrl: 'http://site-3',
      products: null,
    },
  ];

  it('should return products for linked sites', async () => {
    mockRequestHelper.getJson.resolves(response);
    const orgId = 'org-id-1';

    const result = await organizationClient.getSitesLinkedToOrg(orgId);

    const args: RequestOptions = mockRequestHelper.getJson.lastCall.args[0];
    expect(result).to.deep.equal(expectedResponse);
    expect(mockRequestHelper.getJson.calledOnce).to.equal(true);
    expect(args.url).to.equal(`${baseUrl}/${orgId}/sites`);
  });

  it('should return products for unlinked sites', async () => {
    mockRequestHelper.getJson.resolves({ sites: response });

    const result = await organizationClient.getSitesNotLinkedToOrg();

    const args: RequestOptions = mockRequestHelper.getJson.lastCall.args[0];
    expect(result.sites).to.deep.equal(expectedResponse);
    expect(mockRequestHelper.getJson.calledOnce).to.equal(true);
    expect(args.url).to.equal(`${baseUrl}/sites/unlinked-sites`);
  });

  it('should return claimed boolean', async () => {
    mockRequestHelper.getJson.resolves({ claimed: true });

    const expectedResult = {
      claimed: true,
    };

    const result = await organizationClient.checkDomainClaimed('test.com');
    const args: RequestOptions = mockRequestHelper.getJson.lastCall.args[0];
    expect(result).to.deep.equal(expectedResult);
    expect(mockRequestHelper.getJson.calledOnce).to.equal(true);
    expect(args.url).to.equal(`${baseUrl}/domains/test.com/ownership-check`);
  });

  it('should call add admin successfully', async () => {
    mockRequestHelper.getJson.resolves({});
    const orgId = 'org-id-1';
    const email = 'test@test.com';

    await organizationClient.addOrganizationAdmin({ orgId, email });

    const args = mockRequestHelper.postJson.lastCall.args[0];
    expect(mockRequestHelper.postJson.calledOnce).to.equal(true);
    expect(args.url).to.equal(`${baseUrl}/${orgId}/admin/invite`);
  });

  it('should return organization admins list', async () => {
    mockRequestHelper.getJson.resolves({});
    const orgId = 'orgId';

    await organizationClient.getOrganizationAdmins(orgId);
    const args = mockRequestHelper.getJson.lastCall.args[0];

    expect(mockRequestHelper.getJson.calledOnce).to.equal(true);
    expect(args.url).to.equal(`${baseUrl}/${orgId}/admin`);
  });
});
