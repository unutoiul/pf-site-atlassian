import { LDFlagSet } from 'ldclient-js';

import { FeatureFlagsClient } from 'common/feature-flags';

import { UserManagementTokenStore } from './um-token-store';

import { BodyRequestOptions, BodyResponse, RequestOptions, RequestStore } from './request-store';

export class UserManagementRequestHelper extends RequestStore {

  private allFlags: Promise<LDFlagSet> = FeatureFlagsClient.getInstance().allFlags(window.location.host);
  private UMTokenStore: UserManagementTokenStore = new UserManagementTokenStore();

  public async getJson<T = any>(options: RequestOptions<T>): Promise<T> {
    const headers = await this.getHeaders();
    const request = async (): Promise<BodyResponse<T>> => {
      return this.makeRequest(options, options.storeStrategy || 'none', { method: 'GET', headers });
    };

    return this.getRequest(request).then(async data => this.getBody(data));
  }

  public async postJson<TResponse, TBody>(options: BodyRequestOptions<TResponse, TBody>): Promise<TResponse> {
    return this.makeRequest(options, 'none', {
      method: 'POST',
      body: JSON.stringify(options.body),
      headers: await this.getHeaders(options.headers),
    }).then(async data => this.getBody(data));
  }

  public async putJson<TResponse, TBody>(options: BodyRequestOptions<TResponse, TBody>): Promise<TResponse> {
    return this.makeRequest(options, 'none', { method: 'PUT', body: JSON.stringify(options.body), headers: await this.getHeaders() })
      .then(async data => this.getBody(data));
  }

  public async deleteJson<T>(options: RequestOptions<T>): Promise<T> {
    return this.makeRequest(options, 'none', { method: 'DELETE', headers: await this.getHeaders() })
      .then(async data => this.getBody(data));
  }

  private async getConsistencyFlag(): Promise<boolean> {
    const flags = await this.allFlags;

    return flags['um.eventual.consistency'] || false;
  }

  private async getRequest<T>(request: () => Promise<BodyResponse<T>>) {
    const withConsistency = await this.getConsistencyFlag();

    if (!withConsistency) {
      return request();
    }

    return this.UMTokenStore.pollFn(request);
  }

  private async getHeaders(headers?: HeadersInit): Promise<HeadersInit> {

    const withConsistency = await this.getConsistencyFlag();

    if (!withConsistency) {
      return headers || {};
    }

    return {
      ...headers,
      ...this.UMTokenStore.getHeaders(),
    };
  }

  private async getBody<T>(data: BodyResponse<T>): Promise<T> {

    const withConsistency = await this.getConsistencyFlag();

    if (withConsistency) {
      this.UMTokenStore.setTokens(data);
    }

    return data.bodyJson!;
  }
}
