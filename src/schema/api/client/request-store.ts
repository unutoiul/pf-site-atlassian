import merge from 'lodash.merge';

import { AnalyticsClient, analyticsClient } from 'common/analytics/analytics-client'; // keep this import like that, otherwise we get into a circular dependency
import { AuthenticationError, AuthorizationError, BadRequestError, ConflictError, GoneError, PreconditionFailedError, RequestError, RequestErrorInfo } from 'common/error';

import { fetchGetOptions } from '../../../utilities/schema';

interface ResponseStoreEntry {
  promise: Promise<Response>;
  bodyJson?: any;
}

export type StoreStrategy = 'cache' | 'memoize' | 'none';

export type RequestOptions<T = any> = {
  url: string;
  /**
   * This is used for error reporting if something goes wrong.
   *
   * @type {string}
   */
  requestId: string;
  defaultValue?: T;
  /**
   * - 'cache'. Caches the response for the whole app life cycle. Consider using 'memoize' unless you know what you are doing.
   * - 'memoize'. If there are multiple requests for the same endpoint going out at the same time - we'll memoize the first one, and issue only one request at the end. All subsequent requests will go to the server again (with memoize logic still in play).
   * - 'none'. Just issue a request, and don't bother with cache/memoize quirks.
   *
   * When the value is 'cache' or 'memoize', the `storeKey` **must** be provided.
   *
   * Defaults to 'none'.
   *
   * @type {StoreStrategy}
   */
  storeStrategy?: StoreStrategy;
  /**
   * The key that is used for response storing
   *
   * @type {string}
   */
  storeKey?: string;
  headers?: HeadersInit;
}
  & ({
    storeStrategy?: 'cache' | 'memoize';
    storeKey: string;
  }
    | {
      storeStrategy?: 'none';
      storeKey?: string;
    })
  ;

export type BodyRequestOptions<TResponse, TBody> = RequestOptions<TResponse> & {
  body?: TBody;
};

export interface BodyResponse<T> {
  headers?: Headers;
  bodyJson?: T;
}

export interface PollingOptions {
  interval: number;
  timeout: number;
}

export async function toRequestError(response: Response, requestId: string): Promise<RequestError> {
  const errorParams: RequestErrorInfo = {
    status: response.status,
    id: `${requestId}-fetch-error`,
    message: `Fetch request failed with status ${response.status} for ${requestId}`,
  };
  if (response.status === 400) {
    return new BadRequestError(errorParams);
  }
  if (response.status === 401) {
    return new AuthenticationError(errorParams);
  } else if (response.status === 409) {
    const conflictError = new ConflictError(errorParams);
    await conflictError.setMessageFromResponse(response);

    return conflictError;
  } else if (response.status === 403) {
    return new AuthorizationError(errorParams);
  } else if (response.status === 410) {
    return new GoneError(errorParams);
  } else if (response.status === 412) {
    return new PreconditionFailedError(errorParams);
  }

  return new RequestError(errorParams);
}

export class RequestStore {
  public store = new Map<string, ResponseStoreEntry>();

  constructor(
    private analytics: AnalyticsClient = analyticsClient,
  ) {
  }

  public invalidate(cacheKey: string): void {
    this.store.delete(cacheKey);
  }

  public async fetch(url: string, options: RequestInit | RequestOptions = {}): Promise<Response> {
    const finalOptions: RequestInit = merge({}, fetchGetOptions, options);

    try {
      return fetch(url, finalOptions); // tslint:disable-line check-fetch-result
    } catch (e) {
      this.analytics.onError(e);

      throw new RequestError({
        message: `unexpected fetch failure${e.message ? `: ${e.message}` : ''}`,
      });
    }
  }

  public async makeRequest<T>(options: RequestOptions<T>, storeStrategy: StoreStrategy, fetchOptions: RequestInit | RequestOptions = {}): Promise<BodyResponse<T>> {
    const storeEntry = this.getOrCreateStoreEntry(options.storeKey, storeStrategy, async () => {
      return this.fetch(options.url, fetchOptions);
    });

    const response = await storeEntry.promise;

    if (!response.ok) {
      throw await toRequestError(response, options.requestId);
    }

    if (response.status === 204) {
      return {
        headers: response.headers,
      };
    }

    if (!storeEntry.bodyJson) {
      try {
        storeEntry.bodyJson = await response.clone().json();
      } catch (e) {
        if (options.defaultValue) {
          storeEntry.bodyJson = options.defaultValue;
        } else if (response.status === 202) {
          return {
            headers: response.headers,
          };
        } else {
          throw e;
        }
      }
    }

    return {
      headers: response.headers,
      bodyJson: storeEntry.bodyJson,
    };
  }

  private getOrCreateStoreEntry(cacheKey: string | undefined, storeStrategy: StoreStrategy, factory: () => Promise<Response>): ResponseStoreEntry {
    // TODO: define the behavior when the first request was made with one `storeStrategy`, and the other was made with a different one
    if (storeStrategy === 'none') {
      return {
        promise: factory(),
      };
    }

    if (!this.store.has(cacheKey!)) {
      // if caching was requested, we just put the response in the store and leave it there forever.
      // Otherwise, we memoize the request until it gets resolved, and remove it from the store afterwards.
      const promise = storeStrategy === 'cache'
        ? factory()
        : new Promise<Response>(async (resolve, reject) => {
          try {
            resolve(await factory()); // tslint:disable-line check-fetch-result The result is actually checked by a different function
          } catch (e) {
            reject(e);
          } finally {
            this.store.delete(cacheKey!);
          }
        });

      this.store.set(cacheKey!, { promise });
    }

    return this.store.get(cacheKey!)!;
  }
}
