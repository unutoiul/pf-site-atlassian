import { expect } from 'chai';
import { sandbox as sinonSandbox, SinonSandbox, SinonStub } from 'sinon';

import { CofsClient } from './cofs-client';

describe('CofsClient', () => {
  const sandbox: SinonSandbox = sinonSandbox.create();
  let cofsClient: CofsClient;
  let mockRequestHelper: {
    putJson: SinonStub,
  };

  beforeEach(() => {
    mockRequestHelper = {
      putJson: sandbox.stub(),
    };

    cofsClient = new CofsClient(mockRequestHelper as any);
  });

  afterEach(() => {
    sandbox.resetHistory();
  });

  describe('renameSite', () => {
    it('should return the result of the endpoint', async () => {
      const testCloudId = 'cloud-id-123';
      const response = {
        progressUri: `/cloud/${testCloudId}/status/456`,
      };

      mockRequestHelper.putJson.resolves(response);

      const result = await cofsClient.renameSite(testCloudId, 'updog', 'atlassian.net');
      expect(result).to.equal(response);

      expect(mockRequestHelper.putJson.calledOnce).to.equal(true);
      const options = mockRequestHelper.putJson.lastCall.args[0];
      expect(options.url).to.contain(`/cloud/${testCloudId}/rename`);
      expect(options.body).to.deep.equal({
        cloudName: 'updog',
        cloudNamespace: 'atlassian.net',
      });
    });
  });
});
