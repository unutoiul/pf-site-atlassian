import { RequestError } from 'common/error';

import { fetchGetOptions, fetchTextGetOptions } from '../../../utilities/schema';
import { BodyRequestOptions, PollingOptions, RequestOptions, RequestStore, toRequestError } from './request-store';

export class RequestHelper extends RequestStore {
  public async getText(url: string, headers?: HeadersInit): Promise<string> {
    const mergedHeaders = !headers ? fetchTextGetOptions : {
      ...fetchTextGetOptions,
      headers: {
        ...fetchGetOptions.headers,
        ...headers,
      },
    };

    const response = await fetch(url, mergedHeaders);
    if (!response.ok) {
      throw await toRequestError(response, 'getText');
    }

    return response.text();
  }

  public async getJson<T = any>(options: RequestOptions<T>): Promise<T> {
    return this.makeRequest(options, options.storeStrategy || 'none', {
      method: 'GET',
      headers: options.headers,
    }).then(data => data.bodyJson!);
  }

  public async postJson<TResponse, TBody>(options: BodyRequestOptions<TResponse, TBody>): Promise<TResponse> {
    return this.makeRequest(options, 'none', {
      method: 'POST',
      body: JSON.stringify(options.body),
      headers: options.headers,
    }).then(data => data.bodyJson!);
  }

  public async putJson<TResponse, TBody>(options: BodyRequestOptions<TResponse, TBody>): Promise<TResponse> {
    return this.makeRequest(options, 'none', {
      method: 'PUT',
      body: JSON.stringify(options.body),
      headers: options.headers,
    }).then(data => data.bodyJson!);
  }

  public async patchJson<TResponse, TBody>(options: BodyRequestOptions<TResponse, TBody>): Promise<TResponse> {
    return this.makeRequest(options, 'none', {
      method: 'PATCH',
      body: JSON.stringify(options.body),
      headers: options.headers,
    }).then(data => data.bodyJson!);
  }

  public async deleteJson<T>(options: RequestOptions<T>): Promise<T> {
    return this.makeRequest(options, 'none', {
      method: 'DELETE',
      headers: options.headers,
    }).then(data => data.bodyJson!);
  }

  public async poll<T>(fn: () => Promise<T>, error: RequestError, options: PollingOptions = { interval: 4000, timeout: 20000 }): Promise<T> {
    const endTime = Date.now().valueOf() + options.timeout;

    const checkCondition = async (resolve, reject) => {
      const result = await fn();
      if (result) {
        return resolve(result);
      } else if (Date.now().valueOf() < endTime) {
        // If the condition isn't met but the timeout hasn't elapsed, go again
        setTimeout(checkCondition, options.interval, resolve, reject);
      } else {
        // Didn't match and too much time, reject!
        reject(error);
      }
    };

    return new Promise<T>(checkCondition);
  }
}
