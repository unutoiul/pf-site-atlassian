import { expect } from 'chai';
import { sandbox as sinonSandbox, SinonSandbox, SinonStub } from 'sinon';

import { EdgeClient } from './edge-client';
import { RequestOptions } from './request-store';

describe('EdgeClient', () => {
  let sandbox: SinonSandbox;
  let edgeClient: EdgeClient;
  let mockRequestHelper: {
    getJson: SinonStub,
  };

  beforeEach(() => {
    sandbox = sinonSandbox.create();

    mockRequestHelper = {
      getJson: sandbox.stub(),
    };

    edgeClient = new EdgeClient(mockRequestHelper as any);
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('getSiteId', () => {
    it('should return the result of the endpoint', async () => {
      const siteId = {
        cloudId: 'mockCloudId',
      };
      mockRequestHelper.getJson.resolves(siteId);

      const result = await edgeClient.getCurrentSiteId();
      expect(result).to.equal(siteId);

      expect(mockRequestHelper.getJson.calledOnce).to.equal(true);
      const args: RequestOptions = mockRequestHelper.getJson.lastCall.args[0];
      expect(args.url).to.equal('/_edge/tenant_info');
      expect(args.requestId).to.equal('hostname-site-id');
    });
  });
});
