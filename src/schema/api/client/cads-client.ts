import { getConfig } from 'common/config';

import { RequestHelper } from './request-helper';

export interface ServiceDeskCustomerDirectoryShape {
  serviceDeskCustomerDirectoryId: string;
}

export class CadsClient {
  private headers: HeadersInit = {
    'ATL-Accept-User-Version': '2',
  };

  constructor(private requestHelper: RequestHelper) {}

  public async getDirectoryId(cloudId: string): Promise<ServiceDeskCustomerDirectoryShape> {
    return this.requestHelper.getJson({
      headers: this.headers,
      url: `${getConfig().cadsUrl}/site/${cloudId}/service-desk-customer-directory`,
      requestId: 'cads-service-desk-customer-directory',
    });
  }

  public async getCustomerExport(directoryId: string, errorRedirectUrl: string): Promise<string> {
    return this.requestHelper.getText(`${getConfig().cadsUrl}/directory/${directoryId}/export?errorRedirectUrl=${errorRedirectUrl}`, this.headers);
  }

  public async deleteCustomer(directoryId: string, accountId: string): Promise<void> {
    await this.requestHelper.deleteJson({
      headers: this.headers,
      url: `${getConfig().cadsUrl}/directory/${directoryId}/user/${accountId}`,
      requestId: 'cads-service-delete-customer-account',
    });
  }

  public async updateCustomerAccess(directoryId: string, accountId: string, active?: boolean): Promise<void> {
    await this.requestHelper.patchJson({
      headers: this.headers,
      url: `${getConfig().cadsUrl}/directory/${directoryId}/user/${accountId}`,
      requestId: `cads-service-update-customer-account`,
      body: {
        account_status: active ? 'active' : 'inactive',
      },
    });
  }

  public async updateCustomerAccount(directoryId: string, accountId: string, name?: string, password?: string, email?: string, emailVerified?: string): Promise<void> {
    await this.requestHelper.patchJson({
      headers: this.headers,
      url: `${getConfig().cadsUrl}/directory/${directoryId}/user/${accountId}`,
      requestId: `cads-service-update-customer-account`,
      body: {
        name,
        password,
        email,
        email_verified: emailVerified,
      },
    });
  }
}
