import { expect } from 'chai';
import { sandbox as sinonSandbox, SinonSandbox, SinonStub } from 'sinon';

import { JiraClient, JsdCustomersResponse } from './jira-client';
import { RequestOptions } from './request-store';

describe('JiraClient', () => {
  let jiraClient: JiraClient;
  let sandbox: SinonSandbox;

  let mockRequestHelper: {
    getJson: SinonStub,
    putJson: SinonStub,
  };

  beforeEach(() => {
    sandbox = sinonSandbox.create();

    mockRequestHelper = {
      getJson: sandbox.stub(),
      putJson: sandbox.stub(),
    };

    jiraClient = new JiraClient(mockRequestHelper as any);
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('getJsdCustomers', () => {
    it('should return the default result of the endpoint', async () => {
      const response: JsdCustomersResponse = {
        localCustomers: [],
      };

      mockRequestHelper.getJson.resolves(response);

      await jiraClient.getJsdCustomers('cloudId', 0, 20);
      const args: RequestOptions = mockRequestHelper.getJson.lastCall.args[0];

      expect(mockRequestHelper.getJson.calledOnce).to.equal(true);
      expect(args.url).to.equal('/gateway/api/ex/jira/cloudId/rest/servicedesk/customer-management/noeyeball/1/local-servicedesk-user?max-results=20&start-index=0');
      expect(args.requestId).to.equal('jira-jsd-customers');
    });

    it('should send off query params for the filters', async () => {
      const response: JsdCustomersResponse = {
        localCustomers: [],
      };

      mockRequestHelper.getJson.resolves(response);

      await jiraClient.getJsdCustomers('cloudId', 0, 20, 'josh', 'active');
      const args: RequestOptions = mockRequestHelper.getJson.lastCall.args[0];

      expect(mockRequestHelper.getJson.calledOnce).to.equal(true);
      expect(args.url).to.equal('/gateway/api/ex/jira/cloudId/rest/servicedesk/customer-management/noeyeball/1/local-servicedesk-user?active-filter=active&filter=josh&max-results=20&start-index=0');
    });

    it('should not send the active-filter query param if activeStatus is "all"', async () => {
      const response: JsdCustomersResponse = {
        localCustomers: [],
      };

      mockRequestHelper.getJson.resolves(response);

      await jiraClient.getJsdCustomers('cloudId', 0, 20, 'josh', 'all');
      const args: RequestOptions = mockRequestHelper.getJson.lastCall.args[0];

      expect(mockRequestHelper.getJson.calledOnce).to.equal(true);
      expect(args.url).to.equal('/gateway/api/ex/jira/cloudId/rest/servicedesk/customer-management/noeyeball/1/local-servicedesk-user?filter=josh&max-results=20&start-index=0');
    });
  });

  describe('migrateToAtlassianAccount', () => {
    it('should return the default result of the endpoint', async () => {
      mockRequestHelper.putJson.resolves(undefined);

      await jiraClient.migrateToAtlassianAccount('cloudId', '123');
      const args: RequestOptions = mockRequestHelper.putJson.lastCall.args[0];

      expect(mockRequestHelper.putJson.calledOnce).to.equal(true);
      expect(args.url).to.equal('/gateway/api/ex/jira/cloudId/rest/servicedesk/customer-management/noeyeball/1/local-servicedesk-user/migrate-to-atlassian-account-user/123');
      expect(args.requestId).to.equal('jira-jsd-migrate-account');
    });
  });
});
