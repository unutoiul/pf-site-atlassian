import { fail } from 'assert';
import { expect } from 'chai';
import { sandbox as sinonSandbox, SinonSandbox, SinonStub } from 'sinon';

import { RequestError } from 'common/error';

import { UserManagementTokenStore } from './um-token-store';

describe('UM token store', () => {

  let umTokenStore: UserManagementTokenStore;
  let mockRequest: SinonStub;

  const sandbox: SinonSandbox = sinonSandbox.create();

  beforeEach(() => {
    umTokenStore = new UserManagementTokenStore();
    mockRequest = sandbox.stub();
  });

  afterEach(() => {
    sandbox.reset();
    sandbox.restore();
  });

  it('Should return empty header if there are no tokens in the token store', () => {
    const headers = umTokenStore.getHeaders();
    expect(headers).to.deep.equal({});
  });

  it('Should return single token in correct format', () => {
    const data = {
      headers: new Headers({ 'IdPlat-Outstanding-Token': '06b79948e111' }),
    };

    umTokenStore.setTokens(data);

    let headers = umTokenStore.getHeaders();
    expect(headers).to.deep.equal({ 'Event-Stream-State': 'At-Least 06b79948e111' });

    headers = umTokenStore.getHeaders();
    expect(headers).to.deep.equal({});

  });

  it('Should return multiple tokens in correct format', () => {
    const data = {
      headers: new Headers({ 'IdPlat-Outstanding-Token': '06b79948e111,4e08-bed6' }),
    };

    umTokenStore.setTokens(data);

    let headers = umTokenStore.getHeaders();
    expect(headers).to.deep.equal({ 'Event-Stream-State': 'At-Least 06b79948e111,At-Least 4e08-bed6' });

    headers = umTokenStore.getHeaders();
    expect(headers).to.deep.equal({});
  });

  it('Should return only correct headers', () => {
    const data = {
      headers: new Headers({ 'IdPlat-Outstanding-Token': '06b79948e111', 'content-type': 'application-json' }),
    };

    umTokenStore.setTokens(data);

    let headers = umTokenStore.getHeaders();
    expect(headers).to.deep.equal({ 'Event-Stream-State': 'At-Least 06b79948e111' });

    headers = umTokenStore.getHeaders();
    expect(headers).to.deep.equal({});
  });

  describe('when polling', () => {

    it('should only call the function once if 412 is not the error returned', async () => {
      try {
        mockRequest.throws(new RequestError({ message: 'bad_request', status: 400 }));
        await umTokenStore.pollFn(mockRequest);
        fail('Should have failed with bad_request');
      } catch (e) {
        expect(mockRequest.calledOnce).to.equal(true);
      }
    });

    it('should only call the function once if result is returned', async () => {
      mockRequest.returns('result');
      await umTokenStore.pollFn(mockRequest);
      expect(mockRequest.calledOnce).to.equal(true);
    });

    it('should fail with precondition failed if the retry limit is exceeded', async () => {
      try {
        mockRequest.throws(new RequestError({ message: 'precondition_failed', status: 412 }));
        await umTokenStore.pollFn(mockRequest);
        fail('should have failed with precondition_failed');
      } catch (e) {
        expect(mockRequest.calledThrice).to.equal(true);
      }
    });

    it('should return result if first call resolves in 412, but second result resolves OK', async () => {
      mockRequest.onCall(0).throws(new RequestError({ message: 'precondition_failed', status: 412 }));
      mockRequest.onCall(1).returns('result');
      const result = await umTokenStore.pollFn(mockRequest);

      expect(mockRequest.calledTwice).to.equal(true);
      expect(result).equals('result');
    });

  });

});
