import { getConfig } from 'common/config';

import { AdminApiKeyCollection, AdminApiKeysOrganizationArgs, AdminApiKeyWithToken } from '../../schema-types/schema-types';
import { RequestHelper } from './request-helper';

export class AdminApiClient {
  constructor(private requestHelper: RequestHelper) {
  }

  public async createAdminApiKey(orgId: string, name: string): Promise<AdminApiKeyWithToken> {
    return this.requestHelper.postJson<AdminApiKeyWithToken, {}>({
      url: `${getConfig().orgUrl}/${orgId}/apiTokens`,
      requestId: `create-admin-api-key`,
      body: { name },
    });
  }

  public async getAdminApiKeys(orgId: string, scrollId?: AdminApiKeysOrganizationArgs['scrollId']): Promise<AdminApiKeyCollection> {
    return this.requestHelper.getJson({
      url: `${getConfig().orgUrl}/${orgId}/apiTokens${scrollId ? `?scrollId=${scrollId}` : ''}`,
      requestId: `get-admin-api-keys`,
    });
  }

  public async deleteAdminApiKey(orgId: string, apiKeyId: string): Promise<void> {
    return this.requestHelper.deleteJson<void>({
      url: `${getConfig().orgUrl}/${orgId}/apiTokens/${apiKeyId}`,
      requestId: `delete-admin-api-key`,
    });
  }
}
