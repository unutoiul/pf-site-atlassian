import { getConfig } from 'common/config';
import { RequestError } from 'common/error';

import { ClaimDomainMutationVariables, ClaimDomainStatusType } from '../../schema-types';
import { RequestHelper } from './request-helper';

interface ClaimDomainShape {
  taskId: string;
}

type ClaimDomainResponse = ClaimDomainShape | undefined;

interface PollClaimDomainArgs {
  taskId: string;
  id: string;
  domain: string;
}

interface PollClaimDomainStatus {
  status: ClaimDomainStatusType;
  taskId: string | null;
}

export class DomainClient {
  constructor(private requestHelper: RequestHelper) {
  }

  public async claimDomain(args: ClaimDomainMutationVariables): Promise<ClaimDomainResponse> {
    return this.requestHelper.postJson<ClaimDomainResponse, undefined>({
      url: `${getConfig().orgUrl}/${args.id}/domainClaims/${args.domain}/verify/${args.method}Async`,
      requestId: 'claim-domain',
    });
  }

  public async pollClaimDomainStatus(args: PollClaimDomainArgs): Promise<PollClaimDomainStatus> {
    const pollFn = async (): Promise<PollClaimDomainStatus | undefined> => {
      const claimDomainResult = await this.requestHelper.getJson<PollClaimDomainStatus>({
        url: `${getConfig().orgUrl}/${args.id}/domainClaims/${args.domain}/task/${args.taskId}/status`,
        requestId: 'get-domain-claim-status',
      });

      return claimDomainResult.status !== 'IN_PROGRESS' ? claimDomainResult : undefined;
    };

    const error = new RequestError({ ignore: true, message: 'polling for domain claim verification did not finish' });

    const result = await this.requestHelper.poll(pollFn, error, { interval: 2000, timeout: 30000 });
    if (result) {
      return result;
    }

    throw error;
  }
}
