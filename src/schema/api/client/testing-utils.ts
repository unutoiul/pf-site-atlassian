import { stub } from 'sinon';

export const buildMockExternalDirectoryClient = (
  {
   createDirectory = stub(),
   removeDirectory = stub(),
   getLogs = stub(),
   getDirectoriesForOrg = stub(),
  } = {}) => {

  return {
    createDirectory,
    removeDirectory,
    getLogs,
    getDirectoriesForOrg,
  };
};
