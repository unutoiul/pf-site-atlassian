import { getConfig } from 'common/config';

import {
  GSuiteConnection, GSuiteState,
} from '../../schema-types';

import { RequestHelper } from './request-helper';

export class GSuiteClient {
  constructor(private requestHelper: RequestHelper) {
  }

  public async getState(cloudId: string): Promise<GSuiteState | undefined> {
    return this.requestHelper.getJson<GSuiteState | undefined>({
      url: `${getConfig().gSuiteBasePath}/${cloudId}/state`,
      requestId: 'um-gsuite-state',
    })
      // Workaround because there's no easy way to get json-server returning a 204 and empty body
      .then(async (data) => Promise.resolve((data && Object.keys(data).length) ? data : undefined));
  }

  public async getGroups(cloudId: string): Promise<GSuiteState | undefined> {
    return this.requestHelper.getJson<GSuiteState | undefined>({
      url: `${getConfig().gSuiteBasePath}/${cloudId}/groups`,
      requestId: 'um-gsuite-group',
    })
      // Workaround because there's no easy way to get json-server returning a 204 and empty body
      .then(async (data) => Promise.resolve((data && Object.keys(data).length) ? data : undefined));
  }

  public async getConnection(cloudId: string): Promise<GSuiteConnection> {
    return this.requestHelper.postJson<{ connect_url: string }, {}>({
      url: `${getConfig().gSuiteBasePath}/${cloudId}/connect`,
      requestId: 'um-gsuite-connect-url',
    }).then(data => ({ url: data.connect_url }));
  }
}
