import { assert, expect } from 'chai';
import * as fetchMock from 'fetch-mock';

import { getConfig } from 'common/config';

import { MeClient } from './me-client';
import { RequestHelper } from './request-helper';

describe('MeClient', () => {
  let meClient: MeClient;
  let currentUserSpy;

  beforeEach(() => {
    meClient = new MeClient(new RequestHelper());

    currentUserSpy = fetchMock.get(`${getConfig().apiUrl}/me`, {
      status: 200,
      body: {
        account_id: 'foo',
        email: 'bar@example.com',
      },
    });
  });

  afterEach(() => {
    fetchMock.restore();
  });

  describe('getCurrentUser', () => {
    it('should call current user endpoint and return data', async () => {
      const response = await meClient.getCurrentUser();
      expect(response).to.deep.equal({
        account_id: 'foo',
        email: 'bar@example.com',
      });
      expect(currentUserSpy.called()).to.equal(true);
    });

    it('should not call current user endpoint if already called', async () => {
      await meClient.getCurrentUser();
      expect(currentUserSpy.called()).to.equal(true);
      currentUserSpy.reset();
      await meClient.getCurrentUser();
      expect(currentUserSpy.called()).to.equal(false);
    });

    it('should throw an error if fails', async () => {
      fetchMock.restore();
      fetchMock.get(`${getConfig().apiUrl}/me`, {
        status: 500,
      });

      try {
        await meClient.getCurrentUser();
        assert.fail();
      } catch (e) {
        expect(e.message).to.equal('Fetch request failed with status 500 for get-current-user');
      }
    });
  });
});
