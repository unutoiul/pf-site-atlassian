import { getConfig } from 'common/config';

import { RequestHelper } from './request-helper';

export interface RenameSiteArgs {
  cloudName: string;
  cloudNamespace: string;
}

export interface RenameSiteResponse {
  progressUri: string;
}

export class CofsClient {
  constructor(private requestHelper: RequestHelper) {
  }

  public async renameSite(cloudId: string, cloudName: string, cloudNamespace: string): Promise<RenameSiteResponse> {
    return this.requestHelper.putJson<RenameSiteResponse, RenameSiteArgs>({
      url: `${getConfig().cofsUrl}/cloud/${cloudId}/rename`,
      requestId: `site-rename-status`,
      body: {
        cloudName,
        cloudNamespace,
      },
    });
  }
}
