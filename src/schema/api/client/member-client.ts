import { getConfig } from 'common/config';

import { UpdateMemberMutationVariables } from '../../schema-types';
import { RequestHelper } from './request-helper';

interface UpdateMemberBody {
  id: string;
  userName: string;
  displayName: string;
  jobTitle: string;
  email: string;
}

export class MemberClient {
  constructor(private requestHelper: RequestHelper) {
  }

  public async updateMember(args: UpdateMemberMutationVariables): Promise<void> {
    const memberVariables: UpdateMemberBody = {
      id: args.member,
      userName: args.emailAddress,
      displayName: args.displayName,
      jobTitle: args.jobTitle,
      email: args.emailAddress,
    };

    await this.requestHelper.putJson({
      url: `${getConfig().orgUrl}/${args.id}/members/${args.member}`,
      requestId: `update-member`,
      body: memberVariables,
    });
  }
}
