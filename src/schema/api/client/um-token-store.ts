import { BodyResponse } from './request-store';

export class UserManagementTokenStore {
  private tokenStore = new Array<string>();

  public getHeaders(): HeadersInit {
    if (!this.tokenStore || !this.tokenStore.length) {
      return {};
    }

    const headers = { 'Event-Stream-State': this.tokenStore.map(token => `At-Least ${token}`).join(',') };

    this.resetTokens();

    return headers;
  }

  public setTokens<T>(data: BodyResponse<T>): void {
    const consistencyToken = data && data.headers && data.headers.get('IdPlat-Outstanding-Token');

    if (!consistencyToken) {
      return;
    }

    // UM may explicitly set the "none" string as a value.
    consistencyToken.split(',')
      .filter(token => token.toLowerCase() !== 'none')
      .forEach(token => this.tokenStore.push(token));
  }

  public resetTokens(): void {
    this.tokenStore = new Array<string>();
  }

  public async pollFn<T>(fn: () => Promise<BodyResponse<T>>): Promise<BodyResponse<T>> {
    const endTime = Date.now() + 3000;

    const checkCondition = async (resolve, reject) => {
      try {
        const result = await fn();

        resolve(result);
      } catch (e) {

        const isUnderTimeout = Date.now().valueOf() < endTime;
        const isPreconditionFailed = e.status === 412;

        if (isPreconditionFailed && isUnderTimeout) {
          setTimeout(checkCondition, 1500, resolve, reject);
        } else {
          return reject(e);
        }
      }
    };

    return new Promise<T>(checkCondition);
  }
}
