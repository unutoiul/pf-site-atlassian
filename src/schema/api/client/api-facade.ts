import { AdminApiClient } from './admin-api-client';
import { BillingClient } from './billing-client';
import { CadsClient } from './cads-client';
import { CnasClient } from './cnas-client';
import { CofsClient } from './cofs-client';
import { DomainClient } from './domain-client';
import { EdgeClient } from './edge-client';
import { ExternalDirectoryClient } from './external-directory-client';
import { ForgetAboutMeClient } from './forget-about-me-client';
import { GSuiteClient } from './g-suite-client';
import { IdentityClient } from './identity-client';
import { IdentityFeatureFlagClient } from './identity-feature-flag-client';
import { JiraClient } from './jira-client';
import { MeClient } from './me-client';
import { MemberClient } from './member-client';
import { OrganizationClient } from './organization-client';
import { ProfileMutabilityClient } from './profile-mutability-client';
import { RequestHelper } from './request-helper';
import { SiteClient } from './site-client';
import { UserManagementRequestHelper } from './um-request-helper';
import { UserConnectedAppsClient } from './user-connected-apps-client';
import { UserManagementClient } from './user-management-client';
import { XFlowClient } from './xflow-client';

export class ApiFacade {
  private requestHelperInstance!: RequestHelper;
  private umRequestHelperInstance!: UserManagementRequestHelper;

  public get adminApiClient(): AdminApiClient {
    return new AdminApiClient(this.requestHelper);
  }

  public get billingClient(): BillingClient {
    return new BillingClient(this.requestHelper);
  }

  public get edgeClient(): EdgeClient {
    return new EdgeClient(this.requestHelper);
  }

  public get externalDirectoryClient(): ExternalDirectoryClient {
    return new ExternalDirectoryClient(this.requestHelper);
  }

  public get meClient(): MeClient {
    return new MeClient(this.requestHelper);
  }

  public get organizationClient(): OrganizationClient {
    return new OrganizationClient(this.requestHelper);
  }

  public get siteClient(): SiteClient {
    return new SiteClient(this.requestHelper);
  }

  public get userManagementClient(): UserManagementClient {
    return new UserManagementClient(this.umRequestHelper);
  }

  public get identityFeatureFlagClient(): IdentityFeatureFlagClient {
    return new IdentityFeatureFlagClient(this.requestHelper);
  }

  public get memberClient(): MemberClient {
    return new MemberClient(this.requestHelper);
  }

  public get domainClient(): DomainClient {
    return new DomainClient(this.requestHelper);
  }

  public get userConnectedAppsClient(): UserConnectedAppsClient {
    return new UserConnectedAppsClient(this.requestHelper);
  }

  public get cnasClient(): CnasClient {
    return new CnasClient(this.requestHelper);
  }

  public get cofsClient(): CofsClient {
    return new CofsClient(this.requestHelper);
  }

  public get cadsClient(): CadsClient {
    return new CadsClient(this.requestHelper);
  }

  public get xflowClient(): XFlowClient {
    return new XFlowClient(this.requestHelper);
  }

  public get forgetAboutMeClient(): ForgetAboutMeClient {
    return new ForgetAboutMeClient(this.requestHelper);
  }

  public get gSuiteClient(): GSuiteClient {
    return new GSuiteClient(this.requestHelper);
  }

  public get jiraClient(): JiraClient {
    return new JiraClient(this.requestHelper);
  }

  public get profileMutabilityClient(): ProfileMutabilityClient {
    return new ProfileMutabilityClient(this.requestHelper);
  }

  public get identityClient(): IdentityClient {
    return new IdentityClient(this.requestHelper);
  }

  public reset(): void {
    delete this.requestHelperInstance;
    delete this.umRequestHelperInstance;
  }

  private get requestHelper(): RequestHelper {
    return this.requestHelperInstance || (this.requestHelperInstance = new RequestHelper());
  }

  private get umRequestHelper(): UserManagementRequestHelper {
    return this.umRequestHelperInstance || (this.umRequestHelperInstance = new UserManagementRequestHelper());
  }
}

export const apiFacade = new ApiFacade();
