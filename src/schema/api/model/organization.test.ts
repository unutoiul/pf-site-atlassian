import { expect } from 'chai';
import { sandbox as sinonSandbox, SinonSandbox } from 'sinon';
import * as uuid from 'uuid/v4';

import { AuthenticationError, AuthorizationError, GoneError } from 'common/error';

import { AdminApiKey } from '../../schema-types/schema-types';
import { AdminApiClient, BillingClient, ExternalDirectoryClient, LinkedSiteShape, OrganizationClient } from '../client';
import { Organization } from './organization';

describe('Organization', () => {
  let sandbox: SinonSandbox;
  let orgClientMock: { [K in keyof OrganizationClient]: any };
  let billingClientMock: { [K in keyof BillingClient]: any };
  let externalDirectoryClientMock: { [K in keyof ExternalDirectoryClient]: any };
  let adminApiClientMock: { [K in keyof AdminApiClient]: any };

  beforeEach(() => {
    sandbox = sinonSandbox.create();
    const noop = () => undefined;
    orgClientMock = {
      createOrganization: noop,
      getAllOrgs: noop,
      renameOrganization: noop,
      evaluateAtlassianAccess: noop,
      initiateMemberCsvEmailExport: noop,
      getSiteAdmins: noop,
      linkSite: noop,
      getSitesNotLinkedToOrg: noop,
      getSitesLinkedToOrg: noop,
      getMember: noop,
      getMembers: noop,
      getMemberExport: noop,
      checkDomainClaimed: noop,
      addOrganizationAdmin: noop,
      getProducts: () => undefined,
      getOrganizationAdmins: noop,
    };
    billingClientMock = {
      getIdmOrg: noop,
    };
    externalDirectoryClientMock = {
      createDirectory: noop,
      regenerateDirectoryApiKey: noop,
      removeDirectory: noop,
      getDirectoriesForOrg: noop,
      getLogs: noop,
      getGroups: noop,
      getSyncedUsers: noop,
    };
    adminApiClientMock = {
      createAdminApiKey: noop,
      getAdminApiKeys: noop,
      deleteAdminApiKey: noop,
    };
  });

  afterEach(() => {
    sandbox.restore();
  });

  function createOrg() {
    return new Organization(
      orgClientMock as any,
      billingClientMock as any,
      externalDirectoryClientMock as any,
      adminApiClientMock as any,
    );
  }

  function stubGetAllOrgs(response: Promise<any>) {
    return sandbox.stub(orgClientMock, 'getAllOrgs').returns(response);
  }

  function stubCreateOrganization(response: Promise<any>) {
    return sandbox.stub(orgClientMock, 'createOrganization').returns(response);
  }

  function stubGetMemberExport(response: Promise<any>) {
    return sandbox.stub(orgClientMock, 'getMemberExport').returns(response);
  }

  function stubGetSitesLinkedToOrg(response: Promise<any>) {
    return sandbox.stub(orgClientMock, 'getSitesLinkedToOrg').returns(response);
  }

  function stubGetAdminApiKeys(response: Promise<any>) {
    return sandbox.stub(adminApiClientMock, 'getAdminApiKeys').returns(response);
  }

  function stubGetProducts(response: Promise<any>) {
    return sandbox.stub(orgClientMock, 'getProducts').returns(response);
  }

  function stubCheckDomainClaimed(response: Promise<any>) {
    return sandbox.stub(orgClientMock, 'checkDomainClaimed').returns(response);
  }

  function stubAddOrganizationAdmin(response: Promise<any>) {
    return sandbox.stub(orgClientMock, 'addOrganizationAdmin').returns(response);
  }

  describe('getSummary', () => {
    it('should search all orgs for the one with current id and return its name', async () => {
      stubGetAllOrgs(Promise.resolve([
        { id: 'test-id-1', name: 'org 1' },
        { id: 'test-id-2', name: 'org 2' },
      ]));

      const result = await createOrg().getSummary('test-id-2');
      expect(result).to.deep.equal({ id: 'test-id-2', name: 'org 2' });
    });

    it('should throw an AuthorizationError if no org with given id can be found', async () => {
      stubGetAllOrgs(Promise.resolve([
        { id: 'test-id-1', name: 'org 1' },
        { id: 'test-id-2', name: 'org 2' },
      ]));

      try {
        await createOrg().getSummary('test-id-non-existent');
        expect.fail('expected to see an error');
      } catch (e) {
        expect(e instanceof AuthorizationError).to.equal(true);
      }
    });

    it('should re-throw an AuthenticationError from api client', async () => {
      stubGetAllOrgs(Promise.reject(new AuthenticationError({})));

      try {
        await createOrg().getSummary('test-id');
        expect.fail('expected to see an error');
      } catch (e) {
        expect(e instanceof AuthenticationError).to.equal(true);
      }
    });

    it('should re-throw non-authentication errors from api client', async () => {
      stubGetAllOrgs(Promise.reject(new Error('test')));

      try {
        await createOrg().getSummary('test-id');
        expect.fail('expected to see an error');
      } catch (e) {
        expect(e instanceof Error).to.equal(true);
        expect(e.message).to.equal('test');
      }
    });
  });

  describe('rename', () => {
    it('should delegate renaming to organization client', async () => {
      const stub = sandbox.stub(orgClientMock, 'renameOrganization').returns(Promise.resolve());

      await createOrg().rename('test-id', 'new-name');
      expect(stub.callCount).to.equal(1);
      expect(stub.getCalls()[0].args).to.have.length(1);
      expect(stub.getCalls()[0].args[0]).to.deep.equal({ id: 'test-id', name: 'new-name' });
    });

    it('should re-throw an AuthenticationError from api client', async () => {
      sandbox.stub(orgClientMock, 'renameOrganization').returns(Promise.reject(new AuthenticationError({})));

      try {
        await createOrg().rename('test-id', 'new-name');
        expect.fail('expected to see an error');
      } catch (e) {
        expect(e instanceof AuthenticationError).to.equal(true);
      }
    });

    it('should re-throw non-authentication errors from api client', async () => {
      sandbox.stub(orgClientMock, 'renameOrganization').returns(Promise.reject(new Error('test')));

      try {
        await createOrg().rename('test-id', 'new-name');
        expect.fail('expected to see an error');
      } catch (e) {
        expect(e instanceof Error).to.equal(true);
        expect(e.message).to.equal('test');
      }
    });
  });

  describe('isBillable', () => {
    it('should delegate data fetching to billing client', async () => {
      const stub = sandbox.stub(billingClientMock, 'getIdmOrg').returns(Promise.resolve({}));

      await createOrg().isBillable('test-id');
      expect(stub.callCount).to.equal(1);
    });

    it('should base the resulting value on billing client response', async () => {
      sandbox.stub(billingClientMock, 'getIdmOrg').returns(Promise.resolve({ id: 'test-id-1' }));

      const result = await Promise.resolve(createOrg().isBillable('test-id-1'));

      expect(result).to.equal(true);
    });

    it('should base the resulting value on billing client response', async () => {
      sandbox.stub(billingClientMock, 'getIdmOrg').returns(Promise.resolve({ error: 'Not Found' }));

      const result = await Promise.resolve(createOrg().isBillable('test-id-1'));

      expect(result).to.equal(false);
    });

    it('should re-throw an AuthenticationError from api client', async () => {
      sandbox.stub(billingClientMock, 'getIdmOrg').returns(Promise.reject(new AuthenticationError({})));

      try {
        await createOrg().isBillable('test-id');
        expect.fail('expected to see an error');
      } catch (e) {
        expect(e instanceof AuthenticationError).to.equal(true);
      }
    });

    it('should re-throw non-authentication errors from api client', async () => {
      sandbox.stub(billingClientMock, 'getIdmOrg').returns(Promise.reject(new Error('test')));

      try {
        await createOrg().isBillable('test-id');
        expect.fail('expected to see an error');
      } catch (e) {
        expect(e instanceof Error).to.equal(true);
        expect(e.message).to.equal('test');
      }
    });
  });

  describe('getAll', () => {
    it('should return all orgs from org client', async () => {
      const orgs = [
        { id: 'test-id-1', name: 'org 1' },
        { id: 'test-id-2', name: 'org 2' },
      ];
      stubGetAllOrgs(Promise.resolve(orgs));

      const result = await createOrg().getAll();
      expect(result).to.deep.equal(orgs);
    });

    it('should re-throw an AuthenticationError from api client', async () => {
      stubGetAllOrgs(Promise.reject(new AuthenticationError({})));

      try {
        await createOrg().getAll();
        expect.fail('expected to see an error');
      } catch (e) {
        expect(e instanceof AuthenticationError).to.equal(true);
      }
    });

    it('should re-throw non-authentication errors from api client', async () => {
      stubGetAllOrgs(Promise.reject(new Error('test')));

      try {
        await createOrg().getAll();
        expect.fail('expected to see an error');
      } catch (e) {
        expect(e instanceof Error).to.equal(true);
        expect(e.message).to.equal('test');
      }
    });
  });

  describe('initiateOrganizationCreation', () => {
    it('should delegate to org client', async () => {
      const response = { id: 'test-id', progressUri: 'test-uri' };
      const stub = stubCreateOrganization(Promise.resolve(response));

      const result = await createOrg().initiateOrganizationCreation('test-org-name');
      expect(stub.callCount).to.equal(1);
      expect(stub.getCalls()[0].args).to.have.length(1);
      expect(stub.getCalls()[0].args[0]).to.deep.equal({ name: 'test-org-name' });
      expect(result).to.deep.equal(response);
    });

    it('should re-throw an AuthenticationError from api client', async () => {
      stubCreateOrganization(Promise.reject(new AuthenticationError({})));

      try {
        await createOrg().initiateOrganizationCreation('test-org-name');
        expect.fail('expected to see an error');
      } catch (e) {
        expect(e instanceof AuthenticationError).to.equal(true);
      }
    });

    it('should re-throw non-authentication errors from api client', async () => {
      stubCreateOrganization(Promise.reject(new Error('test')));

      try {
        await createOrg().initiateOrganizationCreation('test-org-name');
        expect.fail('expected to see an error');
      } catch (e) {
        expect(e instanceof Error).to.equal(true);
        expect(e.message).to.equal('test');
      }
    });
  });
  describe('getExternalDirectories', () => {
    it('should delegate to external directory client', async () => {
      const response = [
        {
          id: 'id',
          baseUrl: 'test-url',
          name: 'test-name',
          creationDate: new Date(),
        },
      ];
      const stub = sandbox.stub(externalDirectoryClientMock, 'getDirectoriesForOrg')
        .returns(Promise.resolve(response));

      const result = await createOrg().getExternalDirectories('test-orgId');
      expect(stub.callCount).to.equal(1);
      expect(stub.getCalls()[0].args).to.have.length(1);
      expect(stub.getCalls()[0].args[0]).to.deep.equal({ orgId: 'test-orgId' });
      expect(result).to.deep.equal(response);
    });

    it('should re-throw non-authentication errors from api client', async () => {
      sandbox.stub(externalDirectoryClientMock, 'getDirectoriesForOrg').returns(Promise.reject(new Error('test')));

      try {
        await createOrg().getExternalDirectories('test-orgid');
        expect.fail('expected to see an error');
      } catch (e) {
        expect(e instanceof Error).to.equal(true);
        expect(e.message).to.equal('test');
      }
    });
  });

  describe('getMemberExport', () => {
    it('should return all orgs from org client', async () => {
      const memberExport = 'test1,test2,\ntesta,testb,';
      stubGetMemberExport(Promise.resolve(memberExport));

      const result = await createOrg().getMemberExport('test-orgId', 'exportId');
      expect(result).to.deep.equal({ id: 'exportId', content: memberExport });
    });

    it('should re-throw an GoneError from api client', async () => {
      stubGetMemberExport(Promise.reject(new GoneError({})));

      try {
        await createOrg().getMemberExport('test-orgId', 'exportId');
        expect.fail('expected to see an error');
      } catch (e) {
        expect(e instanceof GoneError).to.equal(true);
      }
    });

    it('should re-throw non-authentication errors from api client', async () => {
      stubGetMemberExport(Promise.reject(new Error('test')));

      try {
        await createOrg().getMemberExport('test-orgId', 'exportId');
        expect.fail('expected to see an error');
      } catch (e) {
        expect(e instanceof Error).to.equal(true);
        expect(e.message).to.equal('test');
      }
    });
  });

  describe('getSitesLinkedToOrg', () => {
    function randomSite(partial: Partial<LinkedSiteShape> = {}): LinkedSiteShape {
      return {
        avatar: `test-avatar-${uuid()}`,
        displayName: `test-display-name-${uuid()}`,
        id: `test-id-${uuid()}`,
        products: [],
        siteUrl: `test-site-url-${uuid()}`,
        ...partial,
      };
    }

    it('should return all sites linked to org', async () => {
      const linkedSites: LinkedSiteShape[] = [
        randomSite(),
      ];
      stubGetSitesLinkedToOrg(Promise.resolve(linkedSites));

      const result = await createOrg().getLinkedSites('test-orgId');
      expect(result).to.deep.equal(linkedSites);
    });

    it('should return all sites linked to org with null products', async () => {
      const siteWithNullProducts = randomSite({ products: null });
      const linkedSites: LinkedSiteShape[] = [siteWithNullProducts];
      stubGetSitesLinkedToOrg(Promise.resolve(linkedSites));

      const expectedLinkedSites: LinkedSiteShape[] = [{
        ...siteWithNullProducts,
        products: [],
      }];

      const result = await createOrg().getLinkedSites('test-orgId');
      expect(result).to.deep.equal(expectedLinkedSites);
    });

    it('should return all sites linked to org with undefined products', async () => {
      const siteWithUndefinedProducts = randomSite();
      siteWithUndefinedProducts.products = undefined;
      const linkedSites: LinkedSiteShape[] = [siteWithUndefinedProducts];
      stubGetSitesLinkedToOrg(Promise.resolve(linkedSites));

      const expectedLinkedSites: LinkedSiteShape[] = [{
        ...siteWithUndefinedProducts,
        products: [],
      }];

      const result = await createOrg().getLinkedSites('test-orgId');
      expect(result).to.deep.equal(expectedLinkedSites);
    });

    it('should filter out sites linked to org with undefined display name', async () => {
      const validSite = randomSite();
      const siteWithUndefinedDisplayName = randomSite();
      siteWithUndefinedDisplayName.displayName = null;
      const linkedSites: LinkedSiteShape[] = [siteWithUndefinedDisplayName, validSite];
      stubGetSitesLinkedToOrg(Promise.resolve(linkedSites));

      const result = await createOrg().getLinkedSites('test-orgId');

      expect(result).to.deep.equal([validSite]);
    });

    it('should filter out sites linked to org with undefined avatar', async () => {
      const validSite = randomSite();
      const siteWithUndefinedAvatar = randomSite();
      siteWithUndefinedAvatar.avatar = null;
      const linkedSites: LinkedSiteShape[] = [validSite, siteWithUndefinedAvatar];
      stubGetSitesLinkedToOrg(Promise.resolve(linkedSites));

      const result = await createOrg().getLinkedSites('test-orgId');

      expect(result).to.deep.equal([validSite]);
    });

    it('should return sites sorted alphanumerically by siteUrl', async () => {
      const linkedSites: LinkedSiteShape[] = [
        randomSite({ siteUrl: 'c' }),
        randomSite({ siteUrl: 'b' }),
        randomSite({ siteUrl: 'a' }),
      ];
      stubGetSitesLinkedToOrg(Promise.resolve(linkedSites));

      const result = await createOrg().getLinkedSites('test-orgId');
      expect(result.map(r => r.siteUrl)).to.deep.equal(['a', 'b', 'c']);
    });

    it('should re-throw non-authentication errors from api client', async () => {
      stubGetSitesLinkedToOrg(Promise.reject(new Error('test')));

      try {
        await createOrg().getLinkedSites('test-orgId');
        expect.fail('expected to see an error');
      } catch (e) {
        expect(e instanceof Error).to.equal(true);
        expect(e.message).to.equal('test');
      }
    });
  });

  describe('getAdminApiKeys', () => {
    it('should return all admin api keys from admin api client', async () => {
      const keys: AdminApiKey[] = [
        { id: 'test-key-id-1', name: 'key1', createdBy: { id: 'member', displayName: 'member', emails: [{ value: 'test@email.com', primary: true }] } as any, creationDate: '000', expirationDate: 'rapture' },
        { id: 'test-key-id-2', name: 'key2', createdBy: { id: 'member', displayName: 'member', emails: [{ value: 'test@email.com', primary: true }] } as any, creationDate: '000', expirationDate: 'rapture' },
        { id: 'test-key-id-3', name: 'key3', createdBy: { id: 'member', displayName: 'member', emails: [{ value: 'test@email.com', primary: true }] } as any, creationDate: '000', expirationDate: 'rapture' },
      ];
      stubGetAdminApiKeys(Promise.resolve(keys));

      const result = await createOrg().getAdminApiKeys('foo');
      expect(result).to.deep.equal(keys);
    });
  });

  describe('getProducts', () => {
    it('should return product information correctly', async () => {
      const productsResponse = [
        {
          productKey: 'jira-core',
          productName: 'Jira Core',
        },
        {
          productKey: 'confluence',
          productName: 'Confluence',
        },
        {
          productKey: 'stride',
          productName: 'Stride',
        },
      ];
      const expectedProducts = [
        {
          key: 'jira-core',
          name: 'Jira Core',
        },
        {
          key: 'confluence',
          name: 'Confluence',
        },
        {
          key: 'stride',
          name: 'Stride',
        },
      ];
      stubGetProducts(Promise.resolve(productsResponse));

      const result = await createOrg().getProducts('test-orgId');

      expect(result).to.deep.equal(expectedProducts);
    });
  });

  describe('checkDomainClaimed', () => {
    it('should return a boolean describing if a domain has been claimed', async () => {
      const response = {
        claimed: true,
      };
      stubCheckDomainClaimed(Promise.resolve(response));

      const result = await createOrg().checkDomainClaimed('test.com');

      expect(result).to.deep.equal(response);
    });
  });

  describe('addOrganizationAdmin', () => {
    it('should return empty response when successful', async () => {
      const response = undefined;

      stubAddOrganizationAdmin(Promise.resolve(response));

      await createOrg().addOrganizationAdmin('MOCK-ORG-ID', 'admin@admin.com');

      expect(orgClientMock.addOrganizationAdmin.callCount).to.equal(1);
    });
  });
});
