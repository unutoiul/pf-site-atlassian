import { AnalyticsClient, analyticsClient } from 'common/analytics';

import { apiFacade, EdgeClient } from '../client';

export class CurrentSite {
  constructor(
    private edgeClient: EdgeClient,
    private analytics: AnalyticsClient = analyticsClient,
  ) {
  }

  public async getSiteId(): Promise<string> {
    const siteIdJson = await this.edgeClient.getCurrentSiteId();

    if (siteIdJson && siteIdJson.cloudId) {
      return siteIdJson.cloudId;
    }

    const err = new Error('Could not retrieve site id for the the provided hostname');
    this.analytics.onError(err);
    throw err;
  }

  public static create(): CurrentSite {
    return new CurrentSite(apiFacade.edgeClient, analyticsClient);
  }
}
