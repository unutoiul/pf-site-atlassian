import { AdminApiClient } from '../client/admin-api-client';
import { apiFacade } from '../client/api-facade';
import { handleErrors } from './utils/handle-errors.decorator';

export class AdminApi {
  constructor(private adminApiClient: AdminApiClient) {}

  @handleErrors()
  public async createAdminApiKey(orgId: string, name: string) {
    return this.adminApiClient.createAdminApiKey(orgId, name);
  }

  @handleErrors()
  public async deleteAdminApiKey(orgId: string, adminKeyId: string) {
    return this.adminApiClient.deleteAdminApiKey(orgId, adminKeyId);
  }

  public static init(): AdminApi {
    return new AdminApi(
      apiFacade.adminApiClient,
    );
  }
}
