import { RequestError, ValidationError } from 'common/error';

import { AnalyticsClient, analyticsClient } from 'common/analytics';

import { ProductId } from '../../../site/user-management/access-config/access-config-type';
import { GenerateUserExportInput, ProductType, SiteNameAvailabilityCheckResult, SiteRenameStatus } from '../../schema-types';
import {
  AddUsersToGroupsInput,
  DefaultProduct,
  Group,
  PaginatedGroups,
  PaginatedMembers,
  PaginatedUsers,
  SiteInviteUsersInput,
  SiteUsersListInput,
  SuggestChangesInput,
  UpdateGroupInput,
} from '../../schema-types/schema-types';
import {
  apiFacade,
  CnasClient,
  CofsClient,
  SiteClient,
  UserManagementClient,
} from '../client';
import { RenameSiteResponse } from '../client/cofs-client';
import { handleErrors, handleErrorsLegacy } from './utils/handle-errors.decorator';

export class Site {
  constructor(
    private userManagementClient: UserManagementClient,
    private siteClient: SiteClient,
    private cnasClient: CnasClient,
    private cofsClient: CofsClient,
    private analytics: AnalyticsClient = analyticsClient,
  ) {
  }

  @handleErrors()
  public async getUsers(cloudId: string, parameters: SiteUsersListInput): Promise<PaginatedUsers> {
    return this.userManagementClient.getUsers(cloudId, parameters);
  }

  @handleErrors()
  public async getDefaultApps(cloudId: string): Promise<DefaultProduct[]> {
    return this.userManagementClient.getDefaultApps(cloudId);
  }

  @handleErrors()
  public async getGroupsAdminAccessConfig(cloudId: string): Promise<any> {
    return this.userManagementClient.getGroupsAdminAccessConfig(cloudId);
  }

  @handleErrors()
  public async getGroupsUseAccessConfig(cloudId: string): Promise<any> {
    return this.userManagementClient.getGroupsUseAccessConfig(cloudId);
  }

  @handleErrors()
  public async inviteUsers(cloudId: string, parameters: SiteInviteUsersInput): Promise<void> {
    await this.userManagementClient.inviteUsers(cloudId, parameters);
  }

  @handleErrors()
  public async setDefaultProducts(cloudId: string, productId: ProductId, setDefault: boolean): Promise<void> {
    await this.userManagementClient.setDefaultProducts(cloudId, productId, setDefault);
  }

  @handleErrors()
  public async approveImportedGroup(cloudId: string, productId: ProductId, groupId: string): Promise<void> {
    await this.userManagementClient.approveImportedGroup(cloudId, productId, groupId);
  }

  @handleErrors()
  public async rejectImportedGroup(cloudId: string, productId: ProductId, groupId: string): Promise<void> {
    await this.userManagementClient.rejectImportedGroup(cloudId, productId, groupId);
  }

  @handleErrors()
  public async addGroupsAccessToProduct(cloudId: string, productId: ProductId, groups: string[], productType: ProductType): Promise<void> {
    await this.userManagementClient.addGroupsAccessToProduct(cloudId, productId, groups, productType);
  }

  @handleErrors()
  public async removeGroupAccessToProduct(cloudId: string, productId: ProductId, groupId: string, productType: ProductType): Promise<void> {
    await this.userManagementClient.removeGroupAccessToProduct(cloudId, productId, groupId, productType);
  }

  @handleErrors()
  public async setDefaultGroup(cloudId: string, productId: ProductId, groupId: string): Promise<void> {
    await this.userManagementClient.setDefaultGroup(cloudId, productId, groupId);
  }

  @handleErrors()
  public async removeDefaultGroup(cloudId: string, productId: ProductId, groupId: string): Promise<void> {
    await this.userManagementClient.removeDefaultGroup(cloudId, productId, groupId);
  }

  @handleErrors()
  public async removeUserFromSite(cloudId: string, userId: string): Promise<void> {
    return this.userManagementClient.removeUserFromSite(cloudId, userId);
  }

  @handleErrors()
  public async changeUserRole(cloudId: string, userId: string, roleId: string): Promise<void> {
    await this.userManagementClient.changeUserRole(cloudId, userId, roleId);
  }

  @handleErrors()
  public async userExport(cloudId: string, parameters: GenerateUserExportInput): Promise<string> {
    return this.userManagementClient.userExport(cloudId, parameters);
  }

  @handleErrorsLegacy({
    title: 'Uh oh! Something went wrong.',
    description: 'There was a problem fetching the groups. Please try again in a few minutes.',
  })
  public async getGroups(cloudId: string, { start, count, displayName }: { start?: number | null, count?: number | null, displayName?: string | null }): Promise<PaginatedGroups> {
    return this.userManagementClient.getGroups(cloudId, { start, count, displayName });
  }

  @handleErrorsLegacy({
    title: 'Uh oh! Something went wrong.',
    description: 'There was a problem fetching the group. Please try again in a few minutes.',
  })
  public async getGroup(cloudId: string, groupId: string): Promise<Group> {
    return this.userManagementClient.getGroup(cloudId, groupId);
  }

  @handleErrors()
  public async createGroup(cloudId: string, { description, name }: UpdateGroupInput): Promise<Group> {
    try {

      return await this.userManagementClient.createGroup(cloudId, { description, name });
    } catch (e) {
      if (e instanceof RequestError && e.status === 409) {
        throw new ValidationError({
          code: 'duplicateGroupName',
          title: 'A group with this name already exists',
          description: 'Please choose another name and try again.',
        });
      }

      throw new RequestError({
        status: e.status,
        title: 'Uh oh! Something went wrong.',
        description: 'There was a problem creating the group. Please try again in a few minutes.',
      });
    }
  }

  @handleErrorsLegacy({
    title: 'Uh oh! Something went wrong.',
    description: 'There was a problem updating the group. Please try again in a few minutes.',
  })
  public async updateGroup(cloudId: string, groupId: string, { description, name }: UpdateGroupInput): Promise<Group> {
    return this.userManagementClient.updateGroup(cloudId, groupId, { description, name });
  }

  @handleErrorsLegacy({
    title: 'Uh oh! Something went wrong.',
    description: 'There was a problem deleting the group. Please try again in a few minutes.',
  })
  public async deleteGroup(cloudId: string, groupId: string): Promise<boolean> {
    await this.userManagementClient.deleteGroup(cloudId, groupId);

    return true;
  }

  @handleErrorsLegacy({
    title: 'Uh oh! Something went wrong.',
    description: 'There was a problem fetching the group members. Please try again in a few minutes.',
  })
  public async getGroupMembers(cloudId: string, groupId: string, { start, count }: { start?: number | null, count?: number | null }): Promise <PaginatedMembers > {
    return this.userManagementClient.getGroupMembers(cloudId, groupId, { start, count });
  }

  @handleErrorsLegacy({
    title: 'Uh oh! Something went wrong.',
    description: 'There was a problem adding the user to groups. Please try again in a few minutes.',
  })
  public async addUsersToGroups(cloudId: string, { users, groups }: AddUsersToGroupsInput): Promise<boolean> {
    await this.userManagementClient.addUsersToGroups(cloudId, { groups, users });

    return true;
  }

  @handleErrors()
  public async deleteUserFromGroup(cloudId: string, userId: string, groupId: string): Promise<boolean> {
    await this.userManagementClient.deleteUserFromGroup(cloudId, userId, groupId);

    return true;
  }

  @handleErrors()
  public async grantUsersAccessToProducts(cloudId: string, users: string[], productIds: string[]): Promise<void> {
    return this.userManagementClient.grantAccessToProducts(cloudId, users, productIds);
  }

  @handleErrors()
  public async revokeUsersAccessToProducts(cloudId: string, users: string[], productIds: string[]): Promise<void> {
    return this.userManagementClient.revokeAccessToProducts(cloudId, users, productIds);
  }

  @handleErrors()
  public async promptResetPassword(cloudId: string, userId: string): Promise<void> {
    return this.userManagementClient.promptResetPassword(cloudId, userId);
  }

  @handleErrors()
  public async impersonateUser(cloudId: string, userId: string): Promise<void> {
    return this.userManagementClient.impersonateUser(cloudId, userId);
  }

  @handleErrors()
  public async setDisplayName(cloudId: string, displayName: string): Promise<{}> {
    return this.siteClient.setDisplayName(cloudId, displayName);
  }

  @handleErrors()
  public async suggestChanges(cloudId: string, input: SuggestChangesInput): Promise<void> {
    return this.userManagementClient.suggestChanges(cloudId, input);
  }

  public async isXFlowEnabled(cloudId: string): Promise<boolean> {
    try {
      const result = await this.siteClient.isXFlowEnabled(cloudId);

      return result['product-suggestions-enabled'];
    } catch (err) {
      if (err instanceof RequestError && err.status === 404) {
        // generally the service will 404 because we only PUT when an admin decides to opt-out of product promotions
        return true;
      }

      this.analytics.onError(err);
      throw err;
    }
  }

  @handleErrors()
  public async getRenameStatus(cloudId: string): Promise<SiteRenameStatus> {
    return this.cnasClient.getRenameStatus(cloudId);
  }

  @handleErrors()
  public async checkNameAvailability(cloudId: string, siteName: string): Promise<SiteNameAvailabilityCheckResult> {
    return this.cnasClient.checkNameAvailability(cloudId, siteName);
  }

  @handleErrors()
  public async rename(cloudId: string, cloudName: string, cloudNamespace: string): Promise<RenameSiteResponse> {
    return this.cofsClient.renameSite(cloudId, cloudName, cloudNamespace);
  }

  public static create(): Site {
    return new Site(
      apiFacade.userManagementClient,
      apiFacade.siteClient,
      apiFacade.cnasClient,
      apiFacade.cofsClient,
    );
  }
}
