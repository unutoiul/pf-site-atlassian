import { expect } from 'chai';
import { sandbox as sinonSandbox, SinonSandbox } from 'sinon';

import { JsdCustomer } from '../../schema-types/schema-types';
import { JsdCustomersResponse } from '../client/jira-client';
import { Jira } from './jira';

describe('Jira', () => {
  const sandbox: SinonSandbox = sinonSandbox.create();
  let mockJiraClient;
  let jira: Jira;

  beforeEach(() => {
    mockJiraClient = {
      getJsdCustomers: sandbox.stub(),
      migrateToAtlassianAccount: sandbox.stub(),
    };

    jira = new Jira(mockJiraClient);
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('getJsdCustomers', () => {
    it('should return an empty list if no customers exist', async () => {
      const response: JsdCustomersResponse = {
        localCustomers: [],
      };

      mockJiraClient.getJsdCustomers.resolves(response);

      const result = await jira.getJsdCustomers('cloudId', { count: 20, start: 1 });
      expect(result).to.deep.equal({
        hasMoreResults: false,
        customers: [],
      });
    });

    it('should return a single customer if that is all that exists', async () => {
      const response: JsdCustomersResponse = {
        localCustomers: [
          {
            accountId: '1',
            active: false,
            attributes: {},
            displayName: 'Thom Yorke',
            emailAddress: 'thom@radiohead.com',
            localOnlyMode: false,
            splitUserMode: false,
            username: 'tyorke',
          },
        ],
      };

      mockJiraClient.getJsdCustomers.resolves(response);

      const result = await jira.getJsdCustomers('cloudId', { count: 1, start: 1 });
      const customer: JsdCustomer = {
        id: '1',
        active: false,
        displayName: 'Thom Yorke',
        email: 'thom@radiohead.com',
        username: 'tyorke',
      };

      expect(result).to.deep.equal({
        hasMoreResults: false,
        customers: [customer],
      });
    });

    it('should return 2 customers and also set hasMoreResults to true if more than 2 exist', async () => {
      const response: JsdCustomersResponse = {
        localCustomers: [
          {
            accountId: '1',
            active: false,
            attributes: {},
            displayName: 'Thom Yorke',
            emailAddress: 'thom@radiohead.com',
            localOnlyMode: false,
            splitUserMode: false,
            username: 'tyorke',
          },
          {
            accountId: '2',
            active: false,
            attributes: {},
            displayName: 'Thom Yorke',
            emailAddress: 'thom@radiohead.com',
            localOnlyMode: false,
            splitUserMode: false,
            username: 'tyorke',
          },
          {
            accountId: '3',
            active: false,
            attributes: {},
            displayName: 'Thom Yorke',
            emailAddress: 'thom@radiohead.com',
            localOnlyMode: false,
            splitUserMode: false,
            username: 'tyorke',
          },
        ],
      };

      mockJiraClient.getJsdCustomers.resolves(response);

      const result = await jira.getJsdCustomers('cloudId', { count: 2, start: 1 });
      expect(result.hasMoreResults).to.equal(true);
      expect(result.customers.length).to.equal(2);
    });

    it('should pass on displayName and activeStatus to the client', async () => {
      const response: JsdCustomersResponse = {
        localCustomers: [
          {
            accountId: '1',
            active: false,
            attributes: {},
            displayName: 'Thom Yorke',
            emailAddress: 'thom@radiohead.com',
            localOnlyMode: false,
            splitUserMode: false,
            username: 'tyorke',
          },
        ],
      };

      mockJiraClient.getJsdCustomers.resolves(response);
      await jira.getJsdCustomers('cloudId', { count: 2, start: 1, displayName: 'josh', activeStatus: 'active' });

      expect(mockJiraClient.getJsdCustomers.getCalls()[0].args).to.deep.equal(['cloudId', 0, 3, 'josh', 'active']);
    });
  });

  describe('migrateToAtlassianAccount', () => {
    it('should call the client and return undefined', async () => {
      mockJiraClient.migrateToAtlassianAccount.resolves(undefined);

      await jira.migrateToAtlassianAccount('cloudId', '123');
      expect(mockJiraClient.migrateToAtlassianAccount.called).to.equal(true);
    });
  });
});
