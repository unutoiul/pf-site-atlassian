import { RequestError } from 'common/error';

interface ErrorInfo {
  title: string;
  description: string;
}

export interface ErrorHandlerOptions {
  skipDefaultErrorFlag?: boolean;
  legacy?: ErrorInfo;
}

export const handleErrorsLegacy = (errorInfo: ErrorInfo): MethodDecorator => {
  return handleErrorsInternal({ legacy: errorInfo });
};

export const handleErrors = (): MethodDecorator => {
  return handleErrorsInternal();
};

const handleErrorsInternal = (options: ErrorHandlerOptions = { skipDefaultErrorFlag: true }): MethodDecorator => {
  return (target: any, key: PropertyKey, descriptor: PropertyDescriptor): PropertyDescriptor => {
    if (descriptor === undefined) {
      descriptor = Object.getOwnPropertyDescriptor(target, key)!;
    }
    const originalMethod = descriptor.value;

    // tslint:disable-next-line promise-function-async We explicitly don't want this to be async to not modify the behavior of incorrectly-decorated non-async functions. Instead we will throw synchronously (which can't be achieved in an "async" function)
    descriptor.value = function(this: any, ...args: any[]) {
      const result = originalMethod.apply(this, args);

      if (!(result instanceof Promise)) {
        throw new Error(`"handleErrors" decorator must be applied to async functions. Check "${String(key)}" declaration`);
      }

      return result.catch(e => {
        if ((e instanceof RequestError) && options) {
          if (options.legacy) {
            e.updateDetails({
              title: options.legacy.title,
              description: options.legacy.description,
            });
          }

          // Once @handleErrorsLegacy() is removed this can be removed,
          // since @handleErrors() never displays a flag automatically
          e.legacySkipDefaultErrorFlag = options.skipDefaultErrorFlag;
        }

        throw e;
      });
    };

    return descriptor;
  };
};
