// tslint:disable max-classes-per-file
import { expect } from 'chai';

import { RequestError } from 'common/error';

import { handleErrors, handleErrorsLegacy } from './handle-errors.decorator';

describe('handleErrors', () => {
  it('should throw runtime error if applied to non-async method', () => {
    class Test {
      @handleErrors()
      public method() {
        return;
      }
    }

    expect(() => new Test().method()).to.throw(Error, `"handleErrors" decorator must be applied to async functions. Check "method" declaration`);
  });

  it('should rethrow errors from async methods', async () => {
    class Test {
      @handleErrors()
      public async method() {
        throw new Error('test-error');
      }
    }

    try {
      await new Test().method();
      expect.fail('no error was thrown from async method');
    } catch (e) {
      expect(e instanceof Error).to.equal(true);
      expect(e.message).to.equal('test-error');
    }
  });

  it('should have all arguments propagated to the original method', async () => {
    let calledWithArgs: any[] | undefined;

    class Test {
      @handleErrors()
      public async method(...args: any[]) {
        calledWithArgs = args;
      }
    }

    await new Test().method(1, 'test', []);
    expect(calledWithArgs).to.not.equal(undefined);
    expect(calledWithArgs).to.deep.equal([1, 'test', []]);
  });

  it('should update request error details based on provided props', async () => {
    class Test {
      @handleErrorsLegacy({ title: 'test-title', description: 'test-description' })
      public async method() {
        throw new RequestError({});
      }
    }

    try {
      await new Test().method();
      expect.fail('no error was thrown from async method');
    } catch (e) {
      expect(e instanceof RequestError).to.equal(true);
      expect(e.title).to.equal('test-title');
      expect(e.description).to.equal('test-description');
    }
  });

  it('should skip the error flag if no options are passed', async () => {
    class Test {
      @handleErrors()
      public async method() {
        throw new RequestError({});
      }
    }

    try {
      await new Test().method();
      expect.fail('no error was thrown from async method');
    } catch (e) {
      expect(e instanceof RequestError).to.equal(true);
      expect(e.legacySkipDefaultErrorFlag).to.equal(true);
    }
  });

  it('should return the value from original async method', async () => {
    class Test {
      @handleErrors()
      public async method() {
        return 'test-value';
      }
    }

    const result = await new Test().method();
    expect(result).to.equal('test-value');
  });
});
