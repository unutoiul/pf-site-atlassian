import { parse as parseAri } from '@atlassian/cs-ari';

import { ValidationError } from 'common/error';

import { AppScope, UserConnectedApp, UserGrant } from '../../schema-types';

import { handleErrors } from './utils/handle-errors.decorator';

import { apiFacade } from '../client';
import { UserConnectedAppsClient } from '../client/user-connected-apps-client';

export const ATLASSIAN_VENDOR_NAME = 'Atlassian';

export const extractCloudId = (ari?: string): string | undefined => {
  if (!ari) {
    return undefined;
  }

  try {
    return parseAri(ari).cloudId;
  } catch {
    return undefined;
  }
};

const ariIsForSite = (ari: string | undefined, cloudId: string): boolean => extractCloudId(ari) === cloudId;

export class UserConnectedApps {
  constructor(private userConnectedAppsClient: UserConnectedAppsClient) { }

  @handleErrors()
  public async getUserConnectedApps(cloudId: string): Promise<UserConnectedApp[]> {
    const appInstallations = await this.userConnectedAppsClient.getAppInstallations(cloudId);

    this.validate(appInstallations, cloudId);

    const oauthClientIds: Set<string> = new Set();

    const appsWithGrants = appInstallations
      .filter(({ oauthClientId, grants }) => {
        if (!grants || grants.length === 0 || !oauthClientId || oauthClientIds.has(oauthClientId)) {
          return false;
        }
        oauthClientIds.add(oauthClientId);

        return true;
      });

    const toPlaceholderScope = scopeKey => ({ id: scopeKey, untranslatedName: '', untranslatedDescription: '' });
    const addScopes = (grant: UserGrant, newScopeIds: string[]): void => {
      const uniqueScopeIds = [...new Set([...grant.scopes.map(({ id }) => id), ...newScopeIds])];
      grant.scopes = uniqueScopeIds.map(toPlaceholderScope);
    };

    return appsWithGrants.map(({ oauthClientId, name, description, avatarUrl, vendorType, vendorName, scopes, grants }) => {
      const grantsByUser: Record<string, UserGrant> = {};

      for (const grant of grants || []) {
        if (!grant.accountId) {
          continue;
        }
        const existingGrantForUser = grantsByUser[grant.accountId];
        if (existingGrantForUser) {
          addScopes(existingGrantForUser, grant.scopes);
          existingGrantForUser.createdAt = (grant.createdAt > existingGrantForUser.createdAt ? grant.createdAt : existingGrantForUser.createdAt);
        } else if (grant.accountId) {
          grantsByUser[grant.accountId] = {
            id: `grant:to:${oauthClientId}:by:${grant.accountId}`,
            accountId: grant.accountId,
            createdAt: grant.createdAt,
            scopes: grant.scopes.map(toPlaceholderScope),
          };
        }
      }

      return ({
        id: oauthClientId!,
        name: name!,
        description: description!,
        avatarUrl,
        vendorName: vendorType === 'internal' ? ATLASSIAN_VENDOR_NAME : vendorName,
        scopes: (scopes || []).map(toPlaceholderScope),
        userGrants: Object.values(grantsByUser).sort((a, b) => (new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime())),
      });
    }).sort((a, b) => a.name.toLocaleLowerCase().localeCompare(b.name.toLocaleLowerCase()));
  }

  @handleErrors()
  public async getScopes(scopeKeys: string[]): Promise<AppScope[]> {
    const scopesResponse = await this.userConnectedAppsClient.fetchScopes(scopeKeys);

    return scopesResponse.values.map(value => ({
      id: value.key,
      untranslatedName: value.name,
      untranslatedDescription: value.description,
    }));
  }

  public static init = (): UserConnectedApps => new UserConnectedApps(apiFacade.userConnectedAppsClient);

  private validate = (appInstallations, cloudId: string): void => {

    for (const installation of appInstallations) {
      if (!ariIsForSite(installation.context, cloudId)) {
        throw new ValidationError({ code: 'DACDEV-4-installation' });
      }
      for (const grant of installation.grants || []) {
        if (!ariIsForSite(grant.context, cloudId)) {
          throw new ValidationError({ code: 'DACDEV-4-grant' });
        }
        if (grant && grant.oauthClientId !== installation.oauthClientId) {
          throw new ValidationError({ code: 'DACDEV-21-oauthClientId' });
        }
      }
    }
  };
}
