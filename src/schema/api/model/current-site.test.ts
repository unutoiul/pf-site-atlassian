import { expect } from 'chai';
import { sandbox as sinonSandbox, SinonSandbox } from 'sinon';

import { CurrentSite } from './current-site';

describe('CurrentSite', () => {
  let sandbox: SinonSandbox;
  let mockAnalyticsClient;
  let mockEdgeClient;
  let currentSite: CurrentSite;

  beforeEach(() => {
    sandbox = sinonSandbox.create();

    mockAnalyticsClient = {
      onError: sandbox.stub(),
    };

    mockEdgeClient = {
      getCurrentSiteId: sandbox.stub(),
    };

    currentSite = new CurrentSite(mockEdgeClient, mockAnalyticsClient);
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('getSiteId', () => {
    it('should return the site id of given hostname', async () => {
      mockEdgeClient.getCurrentSiteId.resolves({ cloudId: 'stub-site-id' });

      const siteId = await currentSite.getSiteId();

      expect(siteId).to.equal('stub-site-id');
    });

    it('should throw error if cloudId is missing from response for the given hostname', async () => {
      mockEdgeClient.getCurrentSiteId.resolves({});

      await currentSite.getSiteId().catch(err => {
        expect(err.message).to.equal('Could not retrieve site id for the the provided hostname');

        expect(mockAnalyticsClient.onError.calledWith(err)).to.equal(true);
      });
    });
  });
});
