import { AnalyticsClient, analyticsClient } from 'common/analytics';

import { RolePermissions as RolePermissionsDetails } from '../../schema-types';
import { apiFacade, XFlowClient } from '../client';

export class RolePermissions {
  constructor(
    private analytics: AnalyticsClient = analyticsClient,
    private xflowClient: XFlowClient,
    private cloudId: string,
  ) {
  }

  public async queryRole(roleId: string, shouldNotCreateRole: boolean): Promise<RolePermissionsDetails> {
    try {
      return await this.xflowClient.getRolePermissions({
        cloudId: this.cloudId,
        roleId,
        shouldNotCreateRole,
      });
    } catch {
      this.analytics.onError(new Error(`The roleId ${roleId} permissions could not be retrieved`));

      // Should the request for role permissions fail,
      // we should return an empty array as to not block admins from inviting users
      return {
        permissionIds: [],
      };
    }
  }

  public static create(cloudId: string): RolePermissions {
    return new RolePermissions(analyticsClient, apiFacade.xflowClient, cloudId);
  }
}
