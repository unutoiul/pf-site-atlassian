import { assert, expect } from 'chai';
import { sandbox as sinonSandbox, SinonSandbox } from 'sinon';

import { XFlowClient } from '../client/xflow-client';
import { RolePermissions } from './role-permissions';

describe('Role Permissions', () => {
  const sandbox: SinonSandbox = sinonSandbox.create();
  let xflowClientMock: { [K in keyof XFlowClient]: any };
  let mockAnalyticsClient;

  beforeEach(() => {
    xflowClientMock = {
      getRolePermissions: () => undefined,
    };
    mockAnalyticsClient = {
      onError: sandbox.stub(),
    };
  });

  afterEach(() => {
    sandbox.restore();
  });

  const createRolePermissions = () => {
    return new RolePermissions(mockAnalyticsClient, xflowClientMock as any, 'cloudId');
  };

  const stubGetRolePermissions = (response: Promise<any>) => {
    sandbox.stub(xflowClientMock, 'getRolePermissions').returns(response);
  };

  describe('queryRole', () => {
    it('should return permissionsIds if successful', async () => {
      stubGetRolePermissions(Promise.resolve({ permissionIds: ['add-products'] }));

      try {
        const result = await createRolePermissions().queryRole('roleId', false);
        expect(result).to.deep.equal({ permissionIds: ['add-products'] });
      } catch (e) {
        assert.fail();
      }
    });

    it('should return empty permissionIds when error occurs', async () => {
      stubGetRolePermissions(Promise.reject({}));

      try {
        const result = await createRolePermissions().queryRole('roleId', false);
        expect(result).to.deep.equal({ permissionIds: [] });
      } catch (e) {
        assert.fail();
      }
    });
  });
});
