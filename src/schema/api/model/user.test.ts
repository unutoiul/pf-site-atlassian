import { expect } from 'chai';
import { sandbox as sinonSandbox, SinonSandbox, SinonStub } from 'sinon';

import { RequestError } from 'common/error';

import { Group } from '../../schema-types';
import { User } from './user';

describe('User', () => {
  const cloudId = 'cloudId';
  const userId = 'userId';

  let sandbox: SinonSandbox;
  let mockUserManagementClient: {
    getUser: SinonStub,
    getDirectGroups: SinonStub,
    getProductAccess: SinonStub,
    getManagedStatus: SinonStub,
  };
  let user: User;

  beforeEach(() => {
    sandbox = sinonSandbox.create();
    mockUserManagementClient = {
      getUser: sandbox.stub(),
      getDirectGroups: sandbox.stub(),
      getProductAccess: sandbox.stub(),
      getManagedStatus: sandbox.stub(),
    };
    user = new User(mockUserManagementClient as any, cloudId, userId);
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('getDetails', () => {
    it('should get details', async () => {
      const response = {
        id: 'id',
        email: 'email',
        displayName: 'displayName',
        active: 'active',
        nickname: 'nickname',
        title: 'title',
        timezone: 'timezone',
        location: 'location',
        companyName: 'companyName',
        department: 'department',
        system: 'system',
      };
      mockUserManagementClient.getUser.resolves(response);

      const result = await user.getDetails();

      expect(mockUserManagementClient.getUser.callCount).to.equal(1);
      expect(mockUserManagementClient.getUser.getCall(0).args[0]).to.equal(cloudId);
      expect(mockUserManagementClient.getUser.getCall(0).args[1]).to.equal(userId);
      expect(result).to.equal(response);
    });
    it('should throw error if unsuccessful', async () => {
      const error = new RequestError({ status: 500 });
      mockUserManagementClient.getUser.rejects(error);

      await user.getDetails().then(() => {
        expect.fail();
      }, (e) => {
        expect(e).to.equal(error);
      });
    });
  });

  describe('getGroups', () => {
    const start = 0;
    const count = 1;
    it('should get groups', async () => {
      const group: Group = {
        id: 'id',
        name: 'name',
        description: 'description',
        productPermissions: [],
        defaultForProducts: [],
        sitePrivilege: 'NONE',
        userTotal: 1,
        unmodifiable: false,
        managementAccess: 'ALL',
        ownerType: null,
      };

      const response = {
        total: 2,
        groups: [group],
      };
      mockUserManagementClient.getDirectGroups.resolves(response);

      const result = await user.getGroups(start, count);

      expect(mockUserManagementClient.getDirectGroups.callCount).to.equal(1);
      expect(mockUserManagementClient.getDirectGroups.getCall(0).args[0]).to.equal(cloudId);
      expect(mockUserManagementClient.getDirectGroups.getCall(0).args[1]).to.equal(userId);
      expect(mockUserManagementClient.getDirectGroups.getCall(0).args[2]).to.equal(start);
      expect(mockUserManagementClient.getDirectGroups.getCall(0).args[3]).to.equal(count);
      expect(result).to.equal(response);
    });
    it('should throw error if unsuccessful', async () => {
      const error = new RequestError({ status: 500 });
      mockUserManagementClient.getDirectGroups.rejects(error);

      await user.getGroups(start, count).then(() => {
        expect.fail();
      }, (e) => {
        expect(e).to.equal(error);
      });
    });
  });

  describe('getProductAccess', () => {
    it('should get product access', async () => {
      const response = [
        {
          productName: 'Confluence',
          accessLevel: 'USE',
          presence: null,
        },
      ];
      mockUserManagementClient.getProductAccess.resolves(response);

      const result = await user.getProductAccess();

      expect(mockUserManagementClient.getProductAccess.callCount).to.equal(1);
      expect(mockUserManagementClient.getProductAccess.getCall(0).args[0]).to.equal(cloudId);
      expect(mockUserManagementClient.getProductAccess.getCall(0).args[1]).to.equal(userId);
      expect(result).to.equal(response);
    });
    it('should throw error if unsuccessful', async () => {
      const error = new RequestError({ status: 500 });
      mockUserManagementClient.getProductAccess.rejects(error);

      await user.getProductAccess().then(() => {
        expect.fail();
      }, (e) => {
        expect(e).to.equal(error);
      });
    });
  });
});
