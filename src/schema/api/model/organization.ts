import { AuthorizationError, RequestError, ValidationError } from 'common/error';

import { AdminApiKeyCollection, AdminApiKeysOrganizationArgs, MemberExport, OrgSite, Product, SiteAdmin } from '../../schema-types';
import {
  AdminApiClient,
  apiFacade,
  BillingClient,
  ExternalDirectoryClient,
  OrganizationApiShape,
  OrganizationClient,
  OrganizationCreationResponse,
} from '../client';
import { GetDirectoriesForOrgResponse } from '../client/external-directory-client';
import { CheckDomainShape, LinkedSiteShape, ProductShape } from '../client/organization-client';
import { handleErrors, handleErrorsLegacy } from './utils/handle-errors.decorator';

export interface OrganizationSummary {
  id: string;
  name: string;
}

export interface LinkSiteResult {
  isSuccess: boolean;
  errors?: string[];
}

export class Organization {
  constructor(
    private organizationClient: OrganizationClient,
    private billingClient: BillingClient,
    private externalDirectoryClient: ExternalDirectoryClient,
    private adminApiClient: AdminApiClient,
  ) {
  }

  @handleErrorsLegacy({
    title: 'Uh oh! Something went wrong.',
    description: 'There was a problem fetching the organization name.',
  })
  public async getSummary(orgId: string): Promise<OrganizationSummary> {
    const allOrganizations = await this.organizationClient.getAllOrgs();

    const targetOrganization = allOrganizations.find(({ id }) => orgId === id);

    if (!targetOrganization) {
      // assume unauthorized if the org fetching went OK, but we can't find an org
      throw new AuthorizationError({
        ignore: true, // this error must be checked by the downstream component
        message: 'Could not fetch organization with the given id',
      });
    }

    return { id: orgId, name: targetOrganization.name };
  }

  @handleErrors()
  public async rename(orgId: string, name: string): Promise<void> {
    return this.organizationClient.renameOrganization({ id: orgId, name });
  }

  @handleErrors()
  public async isBillable(orgId: string): Promise<boolean> {
    let orgIdResponse;
    try {
      orgIdResponse = await this.billingClient.getIdmOrg(orgId);
    } catch (e) {
      if (e instanceof RequestError && e.status === 404) {
        throw new ValidationError({
          code: 'orgNotInSystem',
          ignore: true,
        });
      }

      throw e;
    }

    return orgIdResponse && orgIdResponse.id ? orgId === orgIdResponse.id : false;
  }

  @handleErrors()
  public async addOrganizationAdmin(orgId: string, email: string): Promise<void> {
    try {
      await this.organizationClient.addOrganizationAdmin({ orgId, email });
    } catch (error) {
      // Currently, if we do not mark the error.ignore as true, it will create a flag for the error. This is not the behavior that we want per the design, so we will for now set all errors to ignore.
      error.ignore = true;

      throw error;
    }
  }

  @handleErrorsLegacy({
    title: 'Uh oh! Something went wrong.',
    description: 'There was a problem fetching the organization.',
  })
  public async getAll(): Promise<OrganizationApiShape[]> {
    return this.organizationClient.getAllOrgs();
  }

  @handleErrorsLegacy({
    title: 'Uh oh! Something went wrong.',
    description: 'There was an error trying to create the organization. Please check back in a few minutes.',
  })
  public async initiateOrganizationCreation(name: string): Promise<OrganizationCreationResponse> {
    return this.organizationClient.createOrganization({ name });
  }

  @handleErrors()
  public async evaluateAtlassianAccess(orgId: string): Promise<void> {
    try {
      await this.organizationClient.evaluateAtlassianAccess(orgId);
    } catch (e) {
      if (e instanceof RequestError && e.status === 400) {
        throw new ValidationError({
          code: 'lastNameRequired',
          ignore: true,
        });
      }

      throw e;
    }
  }

  @handleErrors()
  public async checkDomainClaimed(domain: string): Promise<CheckDomainShape> {
    const result = await this.organizationClient.checkDomainClaimed(domain);

    return { claimed: result.claimed };
  }

  @handleErrors()
  public async getExternalDirectories(orgId: string): Promise<GetDirectoriesForOrgResponse> {
    return this.externalDirectoryClient.getDirectoriesForOrg({ orgId });
  }

  @handleErrors()
  public async initiateMemberCsvEmailExport(orgId: string): Promise<boolean> {
    await this.organizationClient.initiateMemberCsvEmailExport(orgId);

    return true;
  }

  @handleErrors()
  public async getMemberExport(orgId: string, exportId: string): Promise<MemberExport> {
    const content = await this.organizationClient.getMemberExport(orgId, exportId);

    return {
      id: exportId,
      content,
    };
  }

  @handleErrors()
  public async getSiteAdmins(orgId: string, cloudId: string): Promise<SiteAdmin[]> {
    const response = await this.organizationClient.getSiteAdmins(orgId, cloudId);

    return response.users.map(user => {
      const primaryEmail = user.emails.find(email => email.primary);
      const value = primaryEmail ? primaryEmail.value : '';

      return {
        ...user,
        email: value,
        isOrgAdmin: user.orgAdmin,
      };
    });
  }

  @handleErrors()
  public async linkSite(orgId: string, cloudId: string): Promise<LinkSiteResult> {
    try {
      await this.organizationClient.linkSite(orgId, cloudId);

      return { isSuccess: true };
    } catch (e) {
      // Error handling TBD
      return { isSuccess: false, errors: ['unknown'] };
    }
  }

  @handleErrors()
  public async getLinkedSites(orgId: string): Promise<OrgSite[]> {
    const response: LinkedSiteShape[] = await this.organizationClient.getSitesLinkedToOrg(orgId);

    const filterOutDeletedSites = (site: LinkedSiteShape): site is LinkedSiteShape & { displayName: string, avatar: string } => {
      return !!site.displayName && !!site.avatar;
    };

    return response.filter(filterOutDeletedSites)
      .map(({ products, ...rest }) => {
        const definedProducts: ProductShape[] = products || [];

        return {
          ...rest,
          products: definedProducts.map(product => ({
            key: product.productKey,
            name: product.productName,
          })),
        };
      })
      .sort((a, b) => a.siteUrl.localeCompare(b.siteUrl));
  }

  @handleErrors()
  public async getAdminApiKeys(orgId: string, scrollId?: AdminApiKeysOrganizationArgs['scrollId']): Promise<AdminApiKeyCollection> {
    return this.adminApiClient.getAdminApiKeys(orgId, scrollId);
  }

  @handleErrors()
  public async getProducts(orgId: string): Promise<Product[]> {
    const response = await this.organizationClient.getProducts(orgId);

    return response.map(product => ({
      key: product.productKey,
      name: product.productName,
    }));
  }

  public static init(): Organization {
    return new Organization(
      apiFacade.organizationClient,
      apiFacade.billingClient,
      apiFacade.externalDirectoryClient,
      apiFacade.adminApiClient,
    );
  }
}
