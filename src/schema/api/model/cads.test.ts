import { expect } from 'chai';
import { sandbox as sinonSandbox, SinonSandbox } from 'sinon';

import { ServiceDeskCustomerDirectoryShape } from '../client/cads-client';
import { Cads } from './cads';

describe('Cads', () => {
  const sandbox: SinonSandbox = sinonSandbox.create();
  let mockCadsClient;
  let cads: Cads;

  beforeEach(() => {
    mockCadsClient = {
      deleteCustomer: sandbox.stub(),
      getDirectoryId: sandbox.stub(),
      getCustomerExport: sandbox.stub(),
      updateCustomerAccount: sandbox.stub(),
      updateCustomerAccess: sandbox.stub(),
    };

    cads = new Cads(mockCadsClient);
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('getDirectoryId', () => {
    it('should return the directory id for a given cloud id', async () => {
      const response: ServiceDeskCustomerDirectoryShape = {
        serviceDeskCustomerDirectoryId: 'test-directory-id',
      };

      mockCadsClient.getDirectoryId.resolves(response);

      const result = await cads.getDirectoryId('test-cloud-id');
      expect(result).to.deep.equal({
        serviceDeskCustomerDirectoryId: 'test-directory-id',
      });
    });
  });

  describe('getCustomerExport', () => {
    it('should return a customer export for a given cloud id and error url', async () => {
      const response: string = `username,full_name,email,active,last_login`;

      mockCadsClient.getCustomerExport.resolves(response);

      const result = await cads.getCustomerExport('test-cloud-id', 'https://example.com');
      expect(result).to.deep.equal(response);
    });
  });

  describe('deleteCustomer', () => {
    it('should call the client and return undefined', async () => {
      mockCadsClient.deleteCustomer.resolves(undefined);

      await cads.deleteCustomer('mock-directory', 'mock-account-id');
      expect(mockCadsClient.deleteCustomer.called).to.equal(true);
    });
  });

  describe('updateCustomerName', () => {
    it('should call the client and return undefined', async () => {
      mockCadsClient.updateCustomerAccount.resolves(undefined);

      await cads.updateCustomerName('mock-directory', 'mock-account-id', 'Four Tet');
      expect(mockCadsClient.updateCustomerAccount.called).to.equal(true);
    });
  });

  describe('updateCustomerAccess', () => {
    it('should call the client and return undefined', async () => {
      mockCadsClient.updateCustomerAccess.resolves(undefined);

      await cads.updateCustomerAccess('mock-directory', 'mock-account-id', true);
      expect(mockCadsClient.updateCustomerAccess.called).to.equal(true);
    });
  });

  describe('updateCustomerPassword', () => {
    it('should call the client and return undefined', async () => {
      mockCadsClient.updateCustomerAccount.resolves(undefined);

      await cads.updateCustomerPassword('mock-directory', 'mock-account-id', 'Four Tet');
      expect(mockCadsClient.updateCustomerAccount.called).to.equal(true);
    });
  });
});
