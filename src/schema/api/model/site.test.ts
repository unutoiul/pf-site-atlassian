import { expect } from 'chai';
import { sandbox as sinonSandbox, SinonSandbox, SinonStub } from 'sinon';

import { RequestError, ValidationError } from 'common/error';

import { AddUsersToGroupsInput, Group, PaginatedGroups, PaginatedMembers, UpdateGroupInput } from '../../schema-types';
import { SiteClient, UserManagementClient } from '../client';
import { Site } from './site';

describe('Site', () => {
  let sandbox: SinonSandbox;
  let mockUserManagementClient: { [K in keyof UserManagementClient]: SinonStub };
  let site: Site;
  let mockSiteClient: { [K in keyof SiteClient]: SinonStub };
  let mockAnalyticsClient;
  let mockCnasClient;
  let mockCofsClient;

  beforeEach(() => {
    sandbox = sinonSandbox.create();
    mockUserManagementClient = {
      activateUser: sandbox.stub(),
      addUsersToGroups: sandbox.stub(),
      approveImportedGroup: sandbox.stub(),
      changeUserRole: sandbox.stub(),
      createGroup: sandbox.stub(),
      deactivateUser: sandbox.stub(),
      deleteGroup: sandbox.stub(),
      deleteUserFromGroup: sandbox.stub(),
      getApplications: sandbox.stub(),
      getDefaultApps: sandbox.stub(),
      getGroupsAdminAccessConfig: sandbox.stub(),
      getGroupsUseAccessConfig: sandbox.stub(),
      getSiteContext: sandbox.stub(),
      getDirectGroups: sandbox.stub(),
      getGroup: sandbox.stub(),
      getGroupMembers: sandbox.stub(),
      getUsers: sandbox.stub(),
      inviteUsers: sandbox.stub(),
      getGroups: sandbox.stub(),
      getProductAccess: sandbox.stub(),
      getUserManagedStatus: sandbox.stub(),
      getProducts: sandbox.stub(),
      getUser: sandbox.stub(),
      rejectImportedGroup: sandbox.stub(),
      removeDefaultGroup: sandbox.stub(),
      addGroupsAccessToProduct: sandbox.stub(),
      removeGroupAccessToProduct: sandbox.stub(),
      setDefaultGroup: sandbox.stub(),
      setDefaultProducts: sandbox.stub(),
      updateGroup: sandbox.stub(),
      removeUserFromSite: sandbox.stub(),
      getAccessRequests: sandbox.stub(),
      approveAccess: sandbox.stub(),
      denyAccess: sandbox.stub(),
      grantAccessToProducts: sandbox.stub(),
      revokeAccessToProducts: sandbox.stub(),
      promptResetPassword: sandbox.stub(),
      impersonateUser: sandbox.stub(),
      reinviteUser: sandbox.stub(),
      suggestChanges: sandbox.stub(),
      userExport: sandbox.stub(),
    };
    mockSiteClient = {
      setDisplayName: sandbox.stub(),
      isXFlowEnabled: sandbox.stub(),
      ...{} as any,
    };

    mockCnasClient = {
      getRenameStatus: sandbox.stub(),
    };

    mockCofsClient = {
      renameSite: sandbox.stub(),
    };

    mockAnalyticsClient = {
      onError: sandbox.stub(),
    };

    site = new Site(
      mockUserManagementClient as any,
      mockSiteClient as any,
      mockCnasClient,
      mockCofsClient,
      mockAnalyticsClient,
    );
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('getUsers', () => {
    it('should resolve successfully', async () => {
      mockUserManagementClient.getUsers.resolves({});
      await site.getUsers('cloudId', { start: 0, count: 20 });
      expect(mockUserManagementClient.getUsers.called).to.equal(true);
    });

    it('should throw error if unable to set successfully', async () => {
      const mockError = new RequestError({ status: 500 });
      mockUserManagementClient.getUsers.throws(mockError);

      await site.getUsers('cloudId', { start: 0, count: 20 }).catch((err) => {
        expect(err).to.equal(mockError);
      });
    });
  });

  describe('inviteUsers', () => {
    it('should resolve successfully', async () => {
      mockUserManagementClient.inviteUsers.resolves({});
      await site.inviteUsers('cloudId', { emails: ['cyberdash@gmail.com'], atlOrigin: 'some-atl-origin' });
      expect(mockUserManagementClient.inviteUsers.called).to.equal(true);
    });

    it('should throw error if unable to set successfully', async () => {
      const mockError = new RequestError({ status: 500 });
      mockUserManagementClient.inviteUsers.throws(mockError);

      await site.inviteUsers('cloudId', { emails: ['cyberdash@gmail.com'], atlOrigin: 'some-atl-origin' }).catch((err) => {
        expect(err).to.equal(mockError);
      });
    });
  });

  describe('changeUserRole', () => {
    it('should resolve successfully', async () => {
      mockUserManagementClient.changeUserRole.resolves({});
      await site.changeUserRole('cloudId', 'user-id', 'site/admin');
      expect(mockUserManagementClient.changeUserRole.called).to.equal(true);
    });

    it('should throw error if unable to change role successfully', async () => {
      const mockError = new RequestError({ status: 500 });
      mockUserManagementClient.changeUserRole.throws(mockError);

      await site.changeUserRole('cloudId', 'user-id', 'site/admin').catch((err) => {
        expect(err).to.equal(mockError);
      });
    });
  });

  describe('setDefaultProducts', () => {
    it('should resolve successfully', async () => {
      mockUserManagementClient.setDefaultProducts.resolves({});
      await site.setDefaultProducts('cloudid', 'jira', false);
      expect(mockUserManagementClient.setDefaultProducts.called).to.equal(true);
    });

    it('should throw error if unable to set successfully', async () => {
      const mockError = new RequestError({ status: 500 });
      mockUserManagementClient.setDefaultProducts.throws(mockError);

      await site.setDefaultProducts('cloudId', 'jira', false).catch((err) => {
        expect(err).to.equal(mockError);
      });
    });
  });

  describe('approveImportedGroup', () => {
    it('should resolve successfully', async () => {
      mockUserManagementClient.approveImportedGroup.resolves({});
      await site.approveImportedGroup('cloudid', 'jira', '8c5f8f76-78ce-11e8-adc0-fa7ae01bbebc');
      expect(mockUserManagementClient.approveImportedGroup.called).to.equal(true);
    });

    it('should throw error if unable to set successfully', async () => {
      const mockError = new RequestError({ status: 500 });
      mockUserManagementClient.approveImportedGroup.throws(mockError);

      await site.approveImportedGroup('cloudid', 'jira', '8c5f8f76-78ce-11e8-adc0-fa7ae01bbebc').catch((err) => {
        expect(err).to.equal(mockError);
      });
    });
  });

  describe('rejectImportedGroup', () => {
    it('should resolve successfully', async () => {
      mockUserManagementClient.rejectImportedGroup.resolves({});
      await site.rejectImportedGroup('cloudid', 'jira', '8c5f8f76-78ce-11e8-adc0-fa7ae01bbebc');
      expect(mockUserManagementClient.rejectImportedGroup.called).to.equal(true);
    });

    it('should throw error if unable to set successfully', async () => {
      const mockError = new RequestError({ status: 500 });
      mockUserManagementClient.rejectImportedGroup.throws(mockError);

      await site.rejectImportedGroup('cloudid', 'jira', '8c5f8f76-78ce-11e8-adc0-fa7ae01bbebc').catch((err) => {
        expect(err).to.equal(mockError);
      });
    });
  });

  describe('addGroupsAccessToProduct', () => {
    it('should resolve successfully', async () => {
      mockUserManagementClient.addGroupsAccessToProduct.resolves({});
      await site.addGroupsAccessToProduct('cloudid', 'jira', ['jira-users'], 'USE');
      expect(mockUserManagementClient.addGroupsAccessToProduct.called).to.equal(true);

    });

    it('should throw error if unable to set successfully', async () => {
      const mockError = new RequestError({ status: 500 });
      mockUserManagementClient.addGroupsAccessToProduct.throws(mockError);

      await site.addGroupsAccessToProduct('cloudid', 'jira', ['jira-users'], 'USE').catch((err) => {
        expect(err).to.equal(mockError);
      });
    });
  });

  describe('removeGroupAccessToProduct', () => {
    it('should resolve successfully', async () => {
      mockUserManagementClient.removeGroupAccessToProduct.resolves({});
      await site.removeGroupAccessToProduct('cloudid', 'jira', 'jira-users', 'USE');
      expect(mockUserManagementClient.removeGroupAccessToProduct.called).to.equal(true);

    });

    it('should throw error if unable to set successfully', async () => {
      const mockError = new RequestError({ status: 500 });
      mockUserManagementClient.removeGroupAccessToProduct.throws(mockError);

      await site.removeGroupAccessToProduct('cloudid', 'jira', 'jira-users', 'USE').catch((err) => {
        expect(err).to.equal(mockError);
      });
    });
  });

  describe('setDefaultGroup', () => {
    it('should resolve successfully', async () => {
      mockUserManagementClient.setDefaultGroup.resolves({});
      await site.setDefaultGroup('cloudid', 'jira', 'jira-users');
      expect(mockUserManagementClient.setDefaultGroup.called).to.equal(true);
    });

    it('should throw error if unable to set successfully', async () => {
      const mockError = new RequestError({ status: 500 });
      mockUserManagementClient.setDefaultGroup.throws(mockError);

      await site.setDefaultGroup('cloudid', 'jira', 'jira-users').catch((err) => {
        expect(err).to.equal(mockError);
      });
    });
  });

  describe('removeDefaultGroup', () => {
    it('should resolve successfully', async () => {
      mockUserManagementClient.removeDefaultGroup.resolves({});
      await site.removeDefaultGroup('cloudid', 'jira', 'jira-users');
      expect(mockUserManagementClient.removeDefaultGroup.called).to.equal(true);
    });

    it('should throw error if unable to set successfully', async () => {
      const mockError = new RequestError({ status: 500 });
      mockUserManagementClient.removeDefaultGroup.throws(mockError);

      await site.removeDefaultGroup('cloudid', 'jira', 'jira-users').catch((err) => {
        expect(err).to.equal(mockError);
      });
    });
  });

  describe('removeUserFromSite', () => {
    it('should resolve successfully', async () => {
      mockUserManagementClient.removeUserFromSite.resolves({});
      await site.removeUserFromSite('cloudid', 'user-id');
      expect(mockUserManagementClient.removeUserFromSite.called).to.equal(true);
    });

    it('should throw error if unable to set successfully', async () => {
      const mockError = new RequestError({ status: 500 });
      mockUserManagementClient.removeUserFromSite.throws(mockError);

      await site.removeUserFromSite('cloudid', 'user-id').catch((err) => {
        expect(err).to.equal(mockError);
      });
    });
  });

  describe('userExport', () => {
    it('should resolve successfully', async () => {
      mockUserManagementClient.userExport.resolves('');
      await site.userExport('cloudid', {} as any);
      expect(mockUserManagementClient.userExport.called).to.equal(true);
    });

    it('should throw error if unable to set successfully', async () => {
      const mockError = new RequestError({ status: 500 });
      mockUserManagementClient.userExport.throws(mockError);

      await site.userExport('cloudid', {} as any).catch((err) => {
        expect(err).to.equal(mockError);
      });
    });
  });

  describe('getGroups', () => {
    it('resolves successfully', async () => {
      const response: PaginatedGroups = {
        total: 0,
        groups: [],
      };

      mockUserManagementClient.getGroups.resolves(response);
      const result = await site.getGroups('cloudid', {});
      expect(result).to.deep.equal(response);
    });

    it('throws error if unable to fetch successfully', async () => {
      const mockError = new RequestError({ status: 500 });
      mockUserManagementClient.getGroups.throws(mockError);

      await site.getGroups('cloudid', {}).catch((err) => {
        expect(err).to.equal(mockError);
      });
    });
  });

  describe('getGroup', () => {
    it('resolves successfully', async () => {
      const response: Group = {
        id: '8c5f8f76-78ce-11e8-adc0-fa7ae01bbebc',
        name: 'my-group',
        description: 'My description.',
        unmodifiable: false,
        productPermissions: [],
        defaultForProducts: [],
        sitePrivilege: 'NONE',
        userTotal: 0,
        managementAccess: 'ALL',
        ownerType: null,
      };

      mockUserManagementClient.getGroup.resolves(response);
      const result = await site.getGroup('cloudid', '8c5f8f76-78ce-11e8-adc0-fa7ae01bbebc');
      expect(result).to.deep.equal(response);
    });

    it('throws error if unable to fetch successfully', async () => {
      const mockError = new RequestError({ status: 500 });
      mockUserManagementClient.getGroup.throws(mockError);

      await site.getGroup('cloudid', '8c5f8f76-78ce-11e8-adc0-fa7ae01bbebc').catch((err) => {
        expect(err).to.equal(mockError);
      });
    });
  });

  describe('createGroup', () => {
    it('resolves successfully', async () => {
      const response: Group = {
        id: '8c5f8f76-78ce-11e8-adc0-fa7ae01bbebc',
        name: 'my-group',
        description: 'My description.',
        unmodifiable: false,
        productPermissions: [],
        defaultForProducts: [],
        sitePrivilege: 'NONE',
        userTotal: 0,
        managementAccess: 'ALL',
        ownerType: null,
      };
      const input: UpdateGroupInput = {
        name: 'my-group',
        description: 'My description.',
      };

      mockUserManagementClient.createGroup.resolves(response);
      const result = await site.createGroup('cloudid', input);
      expect(result).to.deep.equal(response);
    });

    it('throws error if unable to create successfully', async () => {
      const mockError = new RequestError({ status: 500 });
      mockUserManagementClient.createGroup.throws(mockError);

      await site.createGroup('cloudid', {} as any).catch((err) => {
        expect(err instanceof RequestError).to.equal(true);
        expect(err.title).to.equal('Uh oh! Something went wrong.');
        expect(err.description).to.equal('There was a problem creating the group. Please try again in a few minutes.');
      });
    });

    it('throws error if unable to create successfully because a group with this name already exists', async () => {
      const mockError = new RequestError({ status: 409 });
      mockUserManagementClient.createGroup.throws(mockError);

      await site.createGroup('cloudid', {} as any).catch((err) => {
        expect(err instanceof ValidationError).to.equal(true);
        expect(err.title).to.equal('A group with this name already exists');
        expect(err.description).to.equal('Please choose another name and try again.');
      });
    });
  });

  describe('updateGroup', () => {
    it('resolves successfully', async () => {
      const response: Group = {
        id: '8c5f8f76-78ce-11e8-adc0-fa7ae01bbebc',
        name: 'my-group',
        description: 'My description.',
        unmodifiable: false,
        productPermissions: [],
        defaultForProducts: [],
        sitePrivilege: 'NONE',
        userTotal: 0,
        managementAccess: 'ALL',
        ownerType: null,
      };
      const input: UpdateGroupInput = {
        name: 'my-group',
        description: 'My description.',
      };

      mockUserManagementClient.updateGroup.resolves(response);
      const result = await site.updateGroup('cloudid', '8c5f8f76-78ce-11e8-adc0-fa7ae01bbebc', input);
      expect(result).to.deep.equal(response);
    });

    it('throws error if unable to update successfully', async () => {
      const mockError = new RequestError({ status: 500 });
      mockUserManagementClient.updateGroup.throws(mockError);

      await site.updateGroup('cloudid', '8c5f8f76-78ce-11e8-adc0-fa7ae01bbebc', {} as any).catch((err) => {
        expect(err).to.equal(mockError);
      });
    });
  });

  describe('deleteGroup', () => {
    it('resolves successfully', async () => {
      mockUserManagementClient.deleteGroup.resolves();
      const result = await site.deleteGroup('cloudid', '8c5f8f76-78ce-11e8-adc0-fa7ae01bbebc');
      expect(result).to.equal(true);
    });

    it('throws error if unable to delete successfully', async () => {
      const mockError = new RequestError({ status: 500 });
      mockUserManagementClient.deleteGroup.throws(mockError);

      await site.deleteGroup('cloudid', '8c5f8f76-78ce-11e8-adc0-fa7ae01bbebc').catch((err) => {
        expect(err).to.equal(mockError);
      });
    });
  });

  describe('getGroupMembers', () => {
    it('resolves successfully', async () => {
      const response: PaginatedMembers = {
        total: 0,
        users: [],
      };

      mockUserManagementClient.getGroupMembers.resolves(response);
      const result = await site.getGroupMembers('cloudid', '8c5f8f76-78ce-11e8-adc0-fa7ae01bbebc', {});
      expect(result).to.deep.equal(response);
    });

    it('throws error if unable to fetch successfully', async () => {
      const mockError = new RequestError({ status: 500 });
      mockUserManagementClient.getGroupMembers.throws(mockError);

      await site.getGroupMembers('cloudid', '8c5f8f76-78ce-11e8-adc0-fa7ae01bbebc', {}).catch((err) => {
        expect(err).to.equal(mockError);
      });
    });
  });

  describe('addUsersToGroups', () => {
    it('resolves successfully', async () => {
      const input: AddUsersToGroupsInput = {
        groups: ['test-group-id'],
        users: ['test-user-id'],
      };

      mockUserManagementClient.addUsersToGroups.resolves();
      const result = await site.addUsersToGroups('cloudId', input);
      expect(result).to.equal(true);
      expect(mockUserManagementClient.addUsersToGroups.calledOnce).to.equal(true);
    });

    it('throws error if unable to add successfully', async () => {
      const mockError = new RequestError({ status: 500 });
      mockUserManagementClient.addUsersToGroups.throws(mockError);

      await site.addUsersToGroups('cloudId', {} as any).catch((err) => {
        expect(err instanceof RequestError).to.equal(true);
        expect(err.title).to.equal('Uh oh! Something went wrong.');
        expect(err.description).to.equal('There was a problem adding the user to groups. Please try again in a few minutes.');
      });
    });
  });

  describe('deleteUserFromGroup', () => {
    const cloudId = 'cloudId';
    const userId = '51c08261-ffe3-4afd-90e7-abc2b5743a37';
    const groupId = '3b296b50-66bd-4d96-a8a2-5fe50b5599a5';

    it('resolves successfully', async () => {
      mockUserManagementClient.deleteUserFromGroup.resolves();
      const result = await site.deleteUserFromGroup(cloudId, userId, groupId);
      expect(result).to.equal(true);
    });

    it('throws error if unable to delete successfully', async () => {
      const mockError = new RequestError({ status: 500 });
      mockUserManagementClient.deleteUserFromGroup.throws(mockError);

      await site.deleteUserFromGroup(cloudId, userId, groupId).catch((err) => {
        expect(err).to.equal(mockError);
      });
    });
  });

  describe('setDisplayName', () => {
    it('should resolve successfully', async () => {
      mockSiteClient.setDisplayName.resolves();
      await site.setDisplayName('cloud-id-123', 'UpDog');
      expect(mockSiteClient.setDisplayName.calledWith('cloud-id-123', 'UpDog')).to.equal(true);
    });

    it('should throw error if unable to set successfully', async () => {
      const mockError = new RequestError({ status: 500 });
      mockSiteClient.setDisplayName.throws(mockError);

      await site.setDisplayName('cloud-id-123', 'UpDog').catch((err) => {
        expect(err).to.equal(mockError);
        expect(mockSiteClient.setDisplayName.calledWith('cloud-id-123', 'UpDog')).to.equal(true);
      });
    });
  });

  describe('suggestChanges', () => {
    const cloudId = 'cloudId';
    const body = {
      userId: '51c08261-ffe3-4afd-90e7-abc2b5743a37',
      email: 'test@example.com',
      displayName: 'Joe Black',
    };

    it('should call user management client passing name and email', async () => {
      await site.suggestChanges(cloudId, body);
      expect(mockUserManagementClient.suggestChanges.firstCall.calledWith(cloudId, body))
        .to.equal(true);
    });

    it('throws error if UM client throws', async () => {
      const mockError = new RequestError({ status: 500 });
      mockUserManagementClient.suggestChanges.throws(mockError);

      await site.suggestChanges(cloudId, body).catch((err) => {
        expect(err).to.equal(mockError);
      });
    });
  });

  describe('isXFlowEnabled', () => {
    it('should return true when the endpoint 404s', async () => {
      const mockError = new RequestError({ status: 404 });
      mockSiteClient.isXFlowEnabled.throws(mockError);

      const result = await site.isXFlowEnabled('mockCloudId');
      expect(result).to.equal(true);

      expect(mockSiteClient.isXFlowEnabled.calledOnce).to.equal(true);
      expect(mockSiteClient.isXFlowEnabled.calledWith('mockCloudId')).to.equal(true);
    });

    [true, false].forEach((storeValue) => {
      it(`should return ${storeValue} when the endpoint returns ${storeValue} in JSON`, async () => {
        mockSiteClient.isXFlowEnabled.resolves({
          'product-suggestions-enabled': storeValue,
        });

        const result = await site.isXFlowEnabled('mockCloudId');
        expect(result).to.equal(storeValue);

        expect(mockSiteClient.isXFlowEnabled.calledOnce).to.equal(true);
        expect(mockSiteClient.isXFlowEnabled.calledWith('mockCloudId')).to.equal(true);
      });
    });

    it('should propagate other serious request errors', async () => {
      const mockError = new RequestError({ status: 500 });
      mockSiteClient.isXFlowEnabled.throws(mockError);

      await site.isXFlowEnabled('mockCloudId').catch((err) => {
        expect(err).to.equal(mockError);

        expect(mockSiteClient.isXFlowEnabled.calledOnce).to.equal(true);
        expect(mockSiteClient.isXFlowEnabled.calledWith('mockCloudId')).to.equal(true);
      });
    });
  });

  describe('getRenameStatus', () => {
    it('should resolve successfully', async () => {
      mockCnasClient.getRenameStatus.resolves({
        cloudName: 'foo',
        cloudNamespace: 'atlaz.io',
        renamesUsed: 1,
        renameLimit: 9000,
      });
      await site.getRenameStatus('cloud-id-123');
      expect(mockCnasClient.getRenameStatus.calledWith('cloud-id-123')).to.equal(true);
    });

    it('should throw error if unable to set successfully', async () => {
      const mockError = new RequestError({ status: 500 });
      mockCnasClient.getRenameStatus.throws(mockError);

      await site.getRenameStatus('cloud-id-123').catch((err) => {
        expect(err).to.equal(mockError);
        expect(mockCnasClient.getRenameStatus.calledWith('cloud-id-123')).to.equal(true);
      });
    });
  });

  describe('rename', () => {
    it('should resolve successfully', async () => {
      mockCofsClient.renameSite.resolves();
      await site.rename('cloud-id-123', 'updog', 'atlassian.net');
      expect(mockCofsClient.renameSite.calledWith('cloud-id-123', 'updog', 'atlassian.net')).to.equal(true);
    });

    it('should throw error if unable to set successfully', async () => {
      const mockError = new RequestError({ status: 500 });
      mockCofsClient.renameSite.throws(mockError);

      await site.rename('cloud-id-123', 'updog', 'atlassian.net').catch((err) => {
        expect(err).to.equal(mockError);
        expect(mockCofsClient.renameSite.calledWith('cloud-id-123', 'updog', 'atlassian.net')).to.equal(true);
      });
    });
  });
});
