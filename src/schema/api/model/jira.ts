import { CustomersJsdArgs, JsdCustomer, PaginatedJsdCustomers } from '../../schema-types/schema-types';
import { apiFacade, JiraClient } from '../client';
import { ClientJsdCustomer } from '../client/jira-client';
import { handleErrors } from './utils/handle-errors.decorator';

export class Jira {
  constructor(private jiraClient: JiraClient) {}

  @handleErrors()
  public async getJsdCustomers(cloudId: string, { count, displayName, start, activeStatus }: CustomersJsdArgs = { count: 20, start: 1 }): Promise<PaginatedJsdCustomers> {
    const response = await this.jiraClient.getJsdCustomers(cloudId, start - 1, count + 1, displayName, activeStatus);

    if (!response.localCustomers || !response.localCustomers.length) {
      return {
        hasMoreResults: false,
        customers: [],
      };
    }

    // If we have more results than asked for then we know another page exists
    const hasMoreResults = response.localCustomers.length > count;

    if (hasMoreResults) {
      // Remove the extra customer used to calculate hasMoreResults
      response.localCustomers = response.localCustomers.slice(0, -1);
    }

    const customers = response.localCustomers.map<JsdCustomer>((customer: ClientJsdCustomer) => {
      const out: JsdCustomer = {
        id: customer.accountId,
        username: customer.username,
        email: customer.emailAddress,
        active: customer.active,
        displayName: customer.displayName,
      };

      const lastLogin = customer.attributes['customer.account.last.login'];
      const lastLoginLegacy = customer.attributes['login.lastLoginMillis'];

      if (lastLogin && lastLogin.length) {
        out.lastLogin = lastLogin[0];
      } else if (lastLoginLegacy && lastLoginLegacy.length) {
        out.lastLogin = lastLoginLegacy[0];
      }

      return out;
    });

    return { hasMoreResults, customers };
  }

  @handleErrors()
  public async migrateToAtlassianAccount(cloudId: string, accountId: string): Promise<void> {
    return this.jiraClient.migrateToAtlassianAccount(cloudId, accountId);
  }

  public static create(): Jira {
    return new Jira(apiFacade.jiraClient);
  }
}
