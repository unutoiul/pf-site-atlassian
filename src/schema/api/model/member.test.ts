import { assert, expect } from 'chai';
import { sandbox as sinonSandbox, SinonSandbox, SinonStub, stub } from 'sinon';

import { RequestError } from 'common/error';

import {
  DeactivatedInfo,
  LifecycleHookResult,
  Member as MemberSchemaType,
  MemberAccess,
  MemberToken,
  PendingInfo,
  UpdateMemberMutationVariables,
} from '../../schema-types';

import {
  AccountDisabledStateResponse,
  ForgetAboutMeClient,
  IdentityClient,
  MemberAccessApiResponse,
  MemberClient,
  MemberDetailsApiResponse,
  MembersApiResponse,
  OrganizationClient,
  ProfileMutabilityClient,
  ProfileMutabilityResponse,
} from '../client';
import { Member } from './member';

describe('Member', () => {
  let sandbox: SinonSandbox;
  let memberClientMock: { [K in keyof MemberClient]: any };
  let orgClientMock: { [K in keyof OrganizationClient]: any };
  let forgetAboutMeClientMock: { [K in keyof ForgetAboutMeClient]: any };
  let profileMutabilityClientMock: { [K in keyof ProfileMutabilityClient]: SinonStub };
  let identityClientMock: { [K in keyof IdentityClient]: any};

  beforeEach(() => {
    sandbox = sinonSandbox.create();
    const noop = () => undefined;
    memberClientMock = {
      updateMember: noop,
    };
    identityClientMock = {
      getMemberTokens: noop,
      revokeMemberToken: noop,
    },
    orgClientMock = {
      createOrganization: noop,
      evaluateAtlassianAccess: noop,
      getAllOrgs: noop,
      getMemberExport: noop,
      getMember: stub(),
      getMembers: noop,
      getOrganizationAdmins: stub(),
      getSiteAdmins: noop,
      getSitesLinkedToOrg: noop,
      getSitesNotLinkedToOrg: noop,
      initiateMemberCsvEmailExport: noop,
      linkSite: noop,
      renameOrganization: noop,
      getProducts: noop,
      checkDomainClaimed: noop,
      addOrganizationAdmin: noop,
    };
    forgetAboutMeClientMock = {
      canCloseAccount: noop,
      canDeactivateAccount: noop,
      closeAccount: noop,
      cancelDeleteAccount: noop,
      getDisabledState: stub(),
      deactivateAccount: noop,
      activateAccount: noop,
    };
    profileMutabilityClientMock = {
      getMemberMutability: stub(),
    };
  });

  afterEach(() => {
    sandbox.restore();
  });

  const params: UpdateMemberMutationVariables = {
    id: '1234',
    member: '2345',
    displayName: 'pc',
    emailAddress: 'party@corgi.com',
    jobTitle: '',
  };

  const createMemberModel = () => {
    return new Member(memberClientMock as any, orgClientMock as any, forgetAboutMeClientMock as any, profileMutabilityClientMock as any, identityClientMock as any);
  };

  const stubUpdateMember = (response: Promise<any>) => {
    sandbox.stub(memberClientMock, 'updateMember').returns(response);
  };

  const stubGetMember = (response: Promise<MemberDetailsApiResponse>) => {
    orgClientMock.getMember.returns(response);
  };

  const stubGetOrganizationAdmins = (response: Promise<MembersApiResponse>) => {
    orgClientMock.getOrganizationAdmins.returns(response);
  };

  const stubCanCloseMember = (response: Promise<LifecycleHookResult>) => {
    sandbox.stub(forgetAboutMeClientMock, 'canCloseAccount').returns(response);
  };

  const stubCanDeactivateMember = (response: Promise<LifecycleHookResult>) => {
    sandbox.stub(forgetAboutMeClientMock, 'canDeactivateAccount').returns(response);
  };

  const stubCloseMember = (response: Promise<any>) => {
    sandbox.stub(forgetAboutMeClientMock, 'closeAccount').returns(response);
  };

  const stubCancelDeleteMember = (response: Promise<any>) => {
    sandbox.stub(forgetAboutMeClientMock, 'cancelDeleteAccount').returns(response);
  };

  const stubGetMemberToken = (response: Promise<any>) => {
    sandbox.stub(identityClientMock, 'getMemberTokens').returns(response);
  };

  const stubGetDisabledState = (response: Promise<AccountDisabledStateResponse>) => {
    forgetAboutMeClientMock.getDisabledState.returns(response);
  };

  const spyRevokeMemberToken = () => {
    return sandbox.spy(identityClientMock, 'revokeMemberToken');
  };

  const stubDeactivateMember = (response: Promise<any>) => {
    sandbox.stub(forgetAboutMeClientMock, 'deactivateAccount').returns(response);
  };

  const stubActivateMember = (response: Promise<any>) => {
    sandbox.stub(forgetAboutMeClientMock, 'activateAccount').returns(response);
  };

  describe('getMember', () => {

    const orgId: string = 'orgId';

    const basicMember: MemberDetailsApiResponse = {
      id: 'memberId',
      displayName: 'Bob Bobkowski',
      active: 'ENABLED',
      emails: [],
      memberAccess: null,
      pendingInfo: null,
      deactivatedInfo: null,
    };

    it('returns user data when not scheduled for deletion nor deactivated', async () => {
      stubGetMember(Promise.resolve(basicMember));

      const result = await createMemberModel().getMember(orgId, basicMember.id, true);

      expect(result).to.be.deep.equal({
        id: 'memberId',
        displayName: 'Bob Bobkowski',
        active: 'ENABLED',
        emails: [],
        memberAccess: {
          errors: [],
          products: [],
          sites: [],
        },
        pendingInfo: null,
        deactivatedInfo: null,
      } as MemberSchemaType);
    });

    it('returns pendingInfo when scheduled for deletion', async () => {
      const startedOn = new Date().toISOString();
      const deletedByHimself: MemberDetailsApiResponse = {
        ...basicMember,
        pendingInfo: {
          startedBy: basicMember.id,
          startedOn,
        },
      };

      stubGetMember(Promise.resolve(deletedByHimself));

      const result = await createMemberModel().getMember(orgId, basicMember.id, true);

      expect(result.pendingInfo).not.null();
      expect(result.pendingInfo).to.be.deep.equal({ startedBy: basicMember.id, startedOn } as PendingInfo);
    });

    it('returns deactivatedInfo when deactivated', async () => {
      const deactivatedOn = new Date().toISOString();
      const deactivatedByHimself: MemberDetailsApiResponse = {
        ...basicMember,
        deactivatedInfo: {
          deactivatedBy: basicMember.id,
          deactivatedOn,
        },
      };

      stubGetMember(Promise.resolve(deactivatedByHimself));

      const result = await createMemberModel().getMember(orgId, basicMember.id, true);

      expect(result.deactivatedInfo).not.null();
      expect(result.deactivatedInfo).to.be.deep.equal({ deactivatedBy: basicMember.id, deactivatedOn } as DeactivatedInfo);
    });

    describe('pendingInfo and deactivatedInfo from IOM feature flag', () => {

      it('uses FAM for pendingInfo when feature flag deactivated', async () => {
        const famPendingDeletionInfo: AccountDisabledStateResponse = {
          pendingInfo: {
            startedBy: 'fam-initiator-name',
            startedOn: new Date().toISOString(),
          },
        };

        stubGetMember(Promise.resolve(basicMember));
        stubGetDisabledState(Promise.resolve(famPendingDeletionInfo));

        const result = await createMemberModel().getMember(orgId, basicMember.id, false);

        expect(result.pendingInfo).to.deep.equal(famPendingDeletionInfo.pendingInfo);
      });

      it('uses FAM for deactivatedInfo when feature flag deactivated', async () => {
        const famPendingDeletionInfo: AccountDisabledStateResponse = {
          deactivatedInfo: {
            deactivatedBy: 'fam-initiator-name',
            deactivatedOn: new Date().toISOString(),
          },
        };

        stubGetMember(Promise.resolve(basicMember));
        stubGetDisabledState(Promise.resolve(famPendingDeletionInfo));

        const result = await createMemberModel().getMember(orgId, basicMember.id, false);

        expect(result.deactivatedInfo).to.deep.equal(famPendingDeletionInfo.deactivatedInfo);
      });

      it('fallbacks to no pendingInfo, deactivatedInfo when FAM call fails', async () => {
        stubGetMember(Promise.resolve(basicMember));
        stubGetDisabledState(Promise.reject(Error('Stubbed error')));

        const result = await createMemberModel().getMember(orgId, basicMember.id, false);

        expect(result.deactivatedInfo).to.be.null();
        expect(result.pendingInfo).to.be.null();
      });

      it('makes no call to FAM when feature flag activated', async () => {
        stubGetMember(Promise.resolve(basicMember));

        await createMemberModel().getMember(orgId, basicMember.id, true);

        sandbox.assert.notCalled(forgetAboutMeClientMock.getDisabledState);
      });
    });
  });

  describe('getInitiatorName', () => {

    const firstAdmin: MemberDetailsApiResponse = {
      id: 'first-admin-id',
      displayName: 'Adam Admin',
      active: 'ENABLED',
      emails: [],
      memberAccess: {
        errors: [],
        products: [],
        sites: [],
      },
    };

    const secondAdmin: MemberDetailsApiResponse = {
      id: 'second-admin-id',
      displayName: 'Bob Admin',
      active: 'ENABLED',
      emails: [],
      memberAccess: {
        errors: [],
        products: [],
        sites: [],
      },
    };

    const organizationAdminsResponse: MembersApiResponse = {
      total: 2,
      users: [firstAdmin, secondAdmin],
    };

    it(`should filter out and return single admin's name`, async () => {
      stubGetOrganizationAdmins(Promise.resolve(organizationAdminsResponse));

      const result = await createMemberModel().getInitiatorName('orgId', 'first-admin-id');

      expect(result).to.equal(firstAdmin.displayName);
    });

    it('should return null when no admin found', async () => {
      stubGetOrganizationAdmins(Promise.resolve(organizationAdminsResponse));

      const result = await createMemberModel().getInitiatorName('orgId', 'non-existent-admin-id');

      expect(result).to.be.null();
    });

    it('should return null when request fails', async () => {
      stubGetOrganizationAdmins(Promise.reject(Error('stubbed error')));

      const result = await createMemberModel().getInitiatorName('orgId', 'first-admin-id');

      expect(result).to.be.null();
    });

  });

  describe('updateMember', () => {
    it('should return true if successful', async () => {
      stubUpdateMember(Promise.resolve(true));

      createMemberModel().updateMember(params).catch(() => assert.fail());
    });

    it('should throw exception when error occurs', async () => {
      stubUpdateMember(Promise.reject(new RequestError({})));

      await createMemberModel().updateMember(params)
        .then(() => assert.fail())
        .catch(e => {
          expect(e instanceof Error).to.equal(true);
        });
    });

    it('should throw exception when error occurs', async () => {
      stubUpdateMember(Promise.reject(new RequestError({})));

      await createMemberModel().updateMember(params)
        .then(() => assert.fail())
        .catch(e => {
          expect(e instanceof Error).to.equal(true);
        });
    });
  });

  describe('handleCanClose', () => {
    it('should return correct response', async () => {
      const canCloseResult: LifecycleHookResult = {
        errors: [
          {
            code: 'HELLO',
            message: 'Error 1',
            link: 'http://atlassian.com',
          },
          {
            code: 'HELLO2',
            message: 'Error 2',
            link: 'http://atlassian.com',
          },
        ],
        warnings: [],
      };

      stubCanCloseMember(Promise.resolve(canCloseResult));

      const result = await createMemberModel().handleCanCloseMember({ accountId: 'test1232' }).
        catch(() => {
          assert.fail();
        });

      expect(canCloseResult).to.deep.equals(result);

    });

    it('should not throw exception when error occurs', async () => {
      stubCanCloseMember(Promise.reject(new Error('Test')));

      const result = await createMemberModel().handleCanCloseMember({ accountId: 'test1232' }).
        catch(() => {
          assert.fail();
        });

      expect({ errors: [], warnings: [] }).to.deep.equals(result);

    });
  });

  describe('handleCanDeactivate', () => {
    it('should return correct response', async () => {
      const canDeactivateResult: LifecycleHookResult = {
        errors: [
          {
            code: 'HELLO',
            message: 'Error 1',
            link: 'http://atlassian.com',
          },
          {
            code: 'HELLO2',
            message: 'Error 2',
            link: 'http://atlassian.com',
          },
        ],
        warnings: [],
      };

      stubCanDeactivateMember(Promise.resolve(canDeactivateResult));

      const result = await createMemberModel().handleCanDeactivateMember({ accountId: 'test1232' }).catch(() => {
        assert.fail();
      });

      expect(canDeactivateResult).to.deep.equals(result);

    });

    it('should not throw exception when error occurs', async () => {
      stubCanDeactivateMember(Promise.reject(new Error('Test')));

      const result = await createMemberModel().handleCanDeactivateMember({ accountId: 'test1232' }).catch(() => {
        assert.fail();
      });

      expect({ errors: [], warnings: [] }).to.deep.equals(result);

    });
  });

  describe('handleCloseMember', async () => {
    it('should return success', () => {
      stubCloseMember(Promise.resolve());

      createMemberModel().handleCloseMember({ accountId: 'test1232' })
        .catch(() => {
          assert.fail();
        });
    });

    it('should return error', () => {
      const error = new RequestError({});
      stubCloseMember(Promise.reject(error));

      createMemberModel().handleCloseMember({ accountId: 'test1232' })
        .then(() => {
          assert.fail();
        })
        .catch(e => {
          expect(e instanceof Error).to.equal(true);
        });
    });
  });

  describe('handleCancelDeleteMember', async () => {
    it('should return success', () => {
      stubCancelDeleteMember(Promise.resolve());

      createMemberModel().handleCancelDeleteMember({ accountId: 'test1232' })
        .catch(() => {
          assert.fail();
        });
    });

    it('should return error', () => {
      const error = new RequestError({});
      stubCancelDeleteMember(Promise.reject(error));

      createMemberModel().handleCancelDeleteMember({ accountId: 'test1232' })
        .then(() => {
          assert.fail();
        })
        .catch(e => {
          expect(e instanceof Error).to.equal(true);
        });
    });
  });

  describe('handleDeactivateMember', async () => {
    it('should return success', () => {
      stubDeactivateMember(Promise.resolve());

      createMemberModel().handleDeactivateMember({ accountId: 'test1232' })
        .catch(() => {
          assert.fail();
        });
    });

    it('should return error', () => {
      const error = new RequestError({});
      stubDeactivateMember(Promise.reject(error));

      createMemberModel().handleDeactivateMember({ accountId: 'test1232' })
        .then(() => {
          assert.fail();
        })
        .catch(e => {
          expect(e instanceof Error).to.equal(true);
        });
    });
  });

  describe('handleActivateMember', async () => {
    it('should return success', () => {
      stubActivateMember(Promise.resolve());

      createMemberModel().handleActivateMember({ accountId: 'test1232' })
        .catch(() => {
          assert.fail();
        });
    });

    it('should return error', () => {
      const error = new RequestError({});
      stubActivateMember(Promise.reject(error));

      createMemberModel().handleActivateMember({ accountId: 'test1232' })
        .then(() => {
          assert.fail();
        })
        .catch(e => {
          expect(e instanceof Error).to.equal(true);
        });
    });
  });

  describe('getMembers', () => {
    describe('should transform member access', () => {
      const stubGetMemberWithProductAccess = (memberAccess: MemberAccessApiResponse | null | undefined) => {
        orgClientMock.getMember.resolves({ memberAccess });
      };

      const expectMemberAccessToBe = async (expected: MemberAccess | null | undefined) => {
        const result = await createMemberModel().getMember('', {} as any, true);

        expect(result.memberAccess).to.deep.equal(expected);
      };

      it('if member has product access of null', async () => {
        stubGetMemberWithProductAccess(null);

        await expectMemberAccessToBe({ errors: [], products: [], sites: [] });
      });

      it('if member has product access of undefined', async () => {
        stubGetMemberWithProductAccess(undefined);

        await expectMemberAccessToBe({ errors: [], products: [], sites: [] });
      });

      it('if member has product access without errors', async () => {
        stubGetMemberWithProductAccess({ sites: [], products: [] });

        await expectMemberAccessToBe({ errors: [], products: [], sites: [] });
      });

      it('if member has valid product access', async () => {
        stubGetMemberWithProductAccess({ sites: [{ siteAdmin: true, siteUrl: 'test' }], products: [{ productName: 'test-product', productKey: 'test-product-key', siteUrl: 'test' }] });

        await expectMemberAccessToBe({ errors: [], products: [{ productName: 'test-product', productKey: 'test-product-key', siteUrl: 'test' }], sites: [{ isSiteAdmin: true, siteUrl: 'test' }] });
      });
    });
  });

  describe('getMembers', () => {
    describe('should transform member access', () => {
      const stubGetMembersWithProductAccess = (memberAccess: MemberAccessApiResponse | null | undefined) => {
        sandbox.stub(orgClientMock, 'getMembers').returns(Promise.resolve<MembersApiResponse>({
          total: 1,
          users: [
            {
              memberAccess,
            } as MemberDetailsApiResponse,
          ],
        }));
      };

      const expectMemberAccessToBe = async (expected: MemberAccess | null | undefined) => {
        const result = await createMemberModel().getMembers('', {} as any);

        expect(result.users![0]!.memberAccess).to.deep.equal(expected);
      };

      it('if member has product access of null', async () => {
        stubGetMembersWithProductAccess(null);

        await expectMemberAccessToBe({ errors: [], products: [], sites: [] });
      });

      it('if member has product access of undefined', async () => {
        stubGetMembersWithProductAccess(undefined);

        await expectMemberAccessToBe({ errors: [], products: [], sites: [] });
      });

      it('if member has product access without errors', async () => {
        stubGetMembersWithProductAccess({ sites: [], products: [] });

        await expectMemberAccessToBe({ errors: [], products: [], sites: [] });
      });

      it('if member has valid product access', async () => {
        stubGetMembersWithProductAccess({ sites: [{ siteAdmin: true, siteUrl: 'test' }], products: [{ productName: 'test-product', productKey: 'test-product-key', siteUrl: 'test' }] });

        await expectMemberAccessToBe({ errors: [], products: [{ productName: 'test-product', productKey: 'test-product-key', siteUrl: 'test' }], sites: [{ isSiteAdmin: true, siteUrl: 'test' }] });
      });
    });
  });

  describe('getProvisioningSource', () => {
    it('should invoke profile mutability client', async () => {
      profileMutabilityClientMock.getMemberMutability.resolves({});

      await createMemberModel().getProvisioningSource('test-id');

      expect(profileMutabilityClientMock.getMemberMutability.callCount).to.equal(1);
      expect(profileMutabilityClientMock.getMemberMutability.calledWith('test-id')).to.equal(true);
    });

    it('should swallow errors that happen inside profile mutability client', async () => {
      profileMutabilityClientMock.getMemberMutability.rejects(new Error('some error'));

      const result = await createMemberModel().getProvisioningSource('test-id');
      expect(result).to.be.null();
    });

    describe('remapping', () => {
      it('should remap "ext.dir.scim" immutability reason to SCIM provisioning source', async () => {
        profileMutabilityClientMock.getMemberMutability.resolves({
          not_editable: [
            { field: 'b', reason: 'ext.dir.scim' },
          ],
        } as ProfileMutabilityResponse);

        const result = await createMemberModel().getProvisioningSource('test-id');

        expect(result).to.equal('SCIM');
      });

      it('should remap "ext.dir.google" immutability reason to GSYNC provisioning source', async () => {
        profileMutabilityClientMock.getMemberMutability.resolves({
          not_editable: [
            { field: 'a', reason: 'ext.dir.google' },
          ],
        } as ProfileMutabilityResponse);

        const result = await createMemberModel().getProvisioningSource('test-id');

        expect(result).to.equal('GSYNC');
      });

      it('should remap other values to null', async () => {
        profileMutabilityClientMock.getMemberMutability.resolves({
          not_editable: [
            { field: 'a', reason: 'something something' as any },
          ],
        } as ProfileMutabilityResponse);

        const result = await createMemberModel().getProvisioningSource('test-id');

        expect(result).to.be.null();
      });
    });
  });

  describe('getMemberTokens', () => {
    it('should call the identityClient and return an array', async () => {
      const getMemberTokenResult: MemberToken[] = [
        {
          id: 'api-token-id-1',
          label: 'api-token',
          createdAt: '2019-01-03T00:11:46.608Z',
          lastAccess: '2019-01-05T00:10:46.768Z',
        },
        {
          id: 'api-token-id-2',
          label: 'api-token',
          createdAt: '2019-01-02T00:13:46.712Z',
          lastAccess: '2019-01-06T00:15:23.608Z',
        },
      ];

      stubGetMemberToken(Promise.resolve(getMemberTokenResult));

      const result = await createMemberModel().getMemberTokens('foo').
      catch(() => {
        assert.fail();
      });

      expect(getMemberTokenResult).to.deep.equals(result);
    });
  });

  describe('revokeMemberToken', () => {
    it('should call the identityClient', async () => {
      const identityClientSpy = spyRevokeMemberToken();

      await createMemberModel().revokeMemberToken('foo', '1').
        catch(() => {
          assert.fail();
        });

      expect(identityClientSpy.called).to.be.true();
    });
  });
});
