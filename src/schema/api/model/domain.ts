import { BadRequestError, ConflictError } from 'common/error';

import { ClaimDomainMutationVariables, ClaimDomainStatus } from '../../schema-types';
import {
  apiFacade,
  DomainClient,
} from '../client';
import { handleErrors } from './utils/handle-errors.decorator';

export class Domain {
  constructor(
    private domainClient: DomainClient,
  ) {
  }

  @handleErrors()
  public async claimDomain(params: ClaimDomainMutationVariables): Promise<ClaimDomainStatus> {
    try {
      const response = await this.domainClient.claimDomain(params);

      if (response && response.taskId) {
        const pollArgs = { id: params.id, domain: params.domain, taskId: response.taskId, skip: false };

        return await this.domainClient.pollClaimDomainStatus(pollArgs);
      }

      return {
        status: 'SUCCESS',
      };
    } catch (error) {
      if (error instanceof ConflictError || error instanceof BadRequestError) {
        error.ignore = true;
      }

      throw error;
    }
  }

  public static init(): Domain {
    return new Domain(
      apiFacade.domainClient,
    );
  }
}
