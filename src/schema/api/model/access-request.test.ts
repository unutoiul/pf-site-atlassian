import { expect } from 'chai';
import { sandbox as sinonSandbox, SinonSandbox, SinonStub } from 'sinon';

import { RequestError } from 'common/error';

import { AccessRequestStatus } from '../../schema-types/schema-types';
import { AccessRequest } from './access-request';

describe('AccessRequest', () => {
  const cloudId = 'cloudId';
  const sandbox: SinonSandbox = sinonSandbox.create();

  let accessRequest: AccessRequest;
  let mockUserManagementClient: {
    getAccessRequests: SinonStub,
    approveAccess: SinonStub,
    denyAccess: SinonStub,
  };

  beforeEach(() => {
    mockUserManagementClient = {
      getAccessRequests: sandbox.stub(),
      approveAccess: sandbox.stub(),
      denyAccess: sandbox.stub(),
    };
    accessRequest = new AccessRequest(mockUserManagementClient as any);
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('getRequests', () => {
    const input = {
      status: ['PENDING'] as AccessRequestStatus[],
      start: 1,
    };
    it('should get pending access requests', async () => {
      const response = {
        total: 1,
        accessRequests: [{
          user: {
            id: '557057:80abfbd6-e1b5-4f23-99d9-077658050e25',
            displayName: 'Alex Sim',
            email: 'asimkin@atlassian.com',
            active: true,
            system: false,
          },
          requestedProducts: [{
            productId: 'confluence',
            productName: 'Confluence',
            status: 'PENDING',
            requestedTime: '2017-07-10T14:00:00Z',
          }],
        }],
      };
      mockUserManagementClient.getAccessRequests.resolves(response);

      const result = await accessRequest.getRequests(cloudId, input);
      const accessRequestsStub = mockUserManagementClient.getAccessRequests;
      const [arg1, arg2] = accessRequestsStub.firstCall.args;

      expect(accessRequestsStub.calledOnce).to.equal(true);
      expect(arg1).to.equal(cloudId);
      expect(arg2).to.equal(input);
      expect(result).to.equal(response);
    });
    it('should throw error if unsuccessful', async () => {
      const error = new RequestError({ status: 500 });
      mockUserManagementClient.getAccessRequests.rejects(error);

      await accessRequest.getRequests(cloudId, input).then(() => {
        expect.fail();
      }, (e) => {
        expect(e).to.equal(error);
      });
    });
  });

  describe('approveAccess', () => {
    const input = {
      userId: 'userId',
      productId: 'productId',
      atlOrigin: 'abc-xyz',
    };
    it('should approve access requests', async () => {
      const response = {};
      mockUserManagementClient.approveAccess.resolves(response);

      await accessRequest.approveAccess(cloudId, input);
      const approveAccess = mockUserManagementClient.approveAccess;
      const [arg1, arg2] = approveAccess.firstCall.args;

      expect(approveAccess.calledOnce).to.equal(true);
      expect(arg1).to.equal(cloudId);
      expect(arg2).to.equal(input);
    });
    it('should throw error if unsuccessful', async () => {
      const error = new RequestError({ status: 500 });
      mockUserManagementClient.approveAccess.rejects(error);

      await accessRequest.approveAccess(cloudId, input).then(() => {
        expect.fail();
      }, (e) => {
        expect(e).to.equal(error);
      });
    });
  });

  describe('denyAccess', () => {
    const input = {
      userId: 'userId',
      productId: 'productId',
      denialReason: 'hello world',
    };
    it('should deny access requests', async () => {
      const response = {};
      mockUserManagementClient.denyAccess.resolves(response);

      await accessRequest.denyAccess(cloudId, input);
      const denyAccess = mockUserManagementClient.denyAccess;
      const [arg1, arg2] = denyAccess.firstCall.args;

      expect(denyAccess.calledOnce).to.equal(true);
      expect(arg1).to.equal(cloudId);
      expect(arg2).to.equal(input);
    });
    it('should throw error if unsuccessful', async () => {
      const error = new RequestError({ status: 500 });
      mockUserManagementClient.denyAccess.rejects(error);

      await accessRequest.denyAccess(cloudId, input).then(() => {
        expect.fail();
      }, (e) => {
        expect(e).to.equal(error);
      });
    });
  });
});
