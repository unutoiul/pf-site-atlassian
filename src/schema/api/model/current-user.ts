import {
  AuthenticationError,
  AuthorizationError,
  RequestError,
} from 'common/error';

import { OrgSite } from '../../schema-types';
import { apiFacade, MeClient, OrganizationClient, SiteClient, SiteInfo, SiteResponse, UnlinkedSites, UserManagementClient } from '../client';
import { ProductShape } from '../client/organization-client';
import { handleErrors, handleErrorsLegacy } from './utils/handle-errors.decorator';

export interface CurrentUserInfo {
  id: string;
}

export class CurrentUser {
  constructor(
    private meClient: MeClient,
    private siteClient: SiteClient,
    private umClient: UserManagementClient,
    private organizationClient: OrganizationClient,
  ) {
  }

  @handleErrorsLegacy({
    title: 'Uh oh! Something went wrong.',
    description: 'Please try again later.',
  })
  public async getInfoOrThrow(): Promise<CurrentUserInfo> {
    const user = await this.meClient.getCurrentUser();

    return {
      id: user.account_id,
    };
  }

  @handleErrorsLegacy({
    title: 'Uh oh! Something went wrong.',
    description: 'Please try again later.',
  })
  public async getAccountId(): Promise<string> {
    return (await this.meClient.getCurrentUser()).account_id;
  }

  @handleErrorsLegacy({
    title: 'Uh oh! Something went wrong.',
    description: 'Please try again later.',
  })
  public async getEmail(): Promise<string> {
    return (await this.meClient.getCurrentUser()).email;
  }

  @handleErrorsLegacy({
    title: 'Uh oh! Something went wrong.',
    description: 'Please try again later.',
  })
  public async isSystemAdmin(cloudId: string): Promise<boolean> {
    try {
      const currentUser = await this.meClient.getCurrentUser();
      const currentUserDetails = await this.umClient.getUser(cloudId, currentUser.account_id);

      return !!currentUserDetails.system;
    } catch (e) {
      if (e instanceof RequestError || e instanceof AuthenticationError || e instanceof AuthorizationError) {
        // treat all request errors as if the user is not a system admin
        return false;
      }

      throw e;
    }
  }

  public async isSiteAdmin(): Promise<boolean> {
    try {
      await this.umClient.getApplications();

      return true;
    } catch (e) {
      if (e instanceof RequestError || e instanceof AuthenticationError || e instanceof AuthorizationError) {
        // treat all request errors as if the user is not a site admin
        return false;
      }

      throw e;
    }
  }

  public async getSites(): Promise<SiteInfo[]> {
    try {
      const sitesResponse: SiteResponse = await this.siteClient.getSites();

      return sitesResponse.sites;
    } catch (e) {
      if (e instanceof AuthorizationError) {
        return [];
      }

      throw e;
    }
  }

  @handleErrors()
  public async getUnlinkedSites(): Promise<OrgSite[]> {
    const sitesResponse: UnlinkedSites = await this.organizationClient.getSitesNotLinkedToOrg();

    return sitesResponse.sites.map<OrgSite>(({ products, ...rest }) => {
      const definedProducts: ProductShape[] = products || [];

      return {
        ...rest,
        products: definedProducts.map(product => ({
          key: product.productKey,
          name: product.productName,
        })),
      };
    });
  }

  public static create(): CurrentUser {
    return new CurrentUser(
      apiFacade.meClient,
      apiFacade.siteClient,
      apiFacade.userManagementClient,
      apiFacade.organizationClient,
    );
  }
}
