import { ConflictError } from 'common/error';

import {
  Member as MemberSchemaType,
  MemberProvisioningSource,
  MemberToken,
  PaginatedMembers,
  UpdateMemberMutationVariables,
} from '../../schema-types';

import {
  apiFacade,
  ForgetAboutMeClient,
  IdentityClient,
  MemberClient,
  MemberDetailsApiResponse,
  MembersFilterArgs,
  OrganizationClient,
  ProfileMutabilityClient,
} from '../client';

import { handleErrors } from './utils/handle-errors.decorator';

function transformApiMemberToGraphqlMember(apiMemberDetails: MemberDetailsApiResponse): MemberSchemaType {
  const { memberAccess, ...rest } = apiMemberDetails;

  const computedMemberAccess = !memberAccess
    ? { sites: [], products: [], errors: [] }
    : {
      errors: memberAccess.errors || [],
      sites: memberAccess.sites ? memberAccess.sites.map(({ siteAdmin: isSiteAdmin, ...otherFields }) => ({ ...otherFields, isSiteAdmin })) : [],
      products: memberAccess.products || [],
    };

  return {
    ...rest,
    memberAccess: computedMemberAccess,
  };
}

export class Member {
  constructor(
    private memberClient: MemberClient,
    private organizationClient: OrganizationClient,
    private forgetAboutMeClient: ForgetAboutMeClient,
    private profileMutabilityClient: ProfileMutabilityClient,
    private identityClient: IdentityClient,
  ) {
  }

  @handleErrors()
  public async handleCanCloseMember({ accountId }) {
    return this.forgetAboutMeClient.canCloseAccount({ accountId })
      .catch(() => {
        return {
          errors: [],
          warnings: [],
        };
      });
  }

  @handleErrors()
  public async handleCanDeactivateMember({ accountId }) {
    return this.forgetAboutMeClient.canDeactivateAccount({ accountId })
      .catch(() => {
        return {
          errors: [],
          warnings: [],
        };
      });
  }

  @handleErrors()
  public async handleDeactivateMember({ accountId }) {
    return this.forgetAboutMeClient.deactivateAccount({ accountId })
      .catch(() => {
        return {
          errors: [],
          warnings: [],
        };
      });
  }

  @handleErrors()
  public async handleActivateMember({ accountId }) {
    return this.forgetAboutMeClient.activateAccount({ accountId })
      .catch(() => {
        return {
          errors: [],
          warnings: [],
        };
      });
  }

  @handleErrors()
  public async handleCloseMember({ accountId }): Promise<boolean> {
    return this.forgetAboutMeClient.closeAccount({ accountId })
      .then(() => {
        return true;
      });
  }

  @handleErrors()
  public async handleCancelDeleteMember({ accountId }): Promise<boolean> {
    return this.forgetAboutMeClient.cancelDeleteAccount({ accountId })
      .then(() => {
        return true;
      });
  }

  @handleErrors()
  public async getMember(orgId: string, memberId: string, pendingAndDeactivatedInfoReadFromIOM: boolean): Promise<MemberSchemaType> {
    const member = await this.organizationClient.getMember(orgId, memberId);

    if (pendingAndDeactivatedInfoReadFromIOM) {
      return transformApiMemberToGraphqlMember(member);
    }

    const { pendingInfo, deactivatedInfo, ...rest } = member; /* tslint:disable-line:no-unused */

    const disabledState = await this.forgetAboutMeClient.getDisabledState({ accountId: memberId })
      .catch(async () => (
        Promise.resolve({ pendingInfo: null, deactivatedInfo: null })
      ));

    return transformApiMemberToGraphqlMember({
      pendingInfo: disabledState.pendingInfo,
      deactivatedInfo: disabledState.deactivatedInfo,
      ...rest,
    });
  }

  @handleErrors()
  public async getInitiatorName(orgId: string, adminId: string): Promise<string | null> {
    const response = await this.organizationClient.getOrganizationAdmins(orgId).catch(async () => Promise.resolve(null));
    if (response && response.users) {
      const admin = response.users.find(a => a.id === adminId);
      if (admin) {
        return Promise.resolve(admin.displayName);
      }
    }

    return Promise.resolve(null);
  }

  @handleErrors()
  public async getMembers(orgId: string, filter: MembersFilterArgs): Promise<PaginatedMembers> {
    const { total, users } = await this.organizationClient.getMembers(orgId, filter);

    return {
      total,
      users: (users || []).map(transformApiMemberToGraphqlMember),
    };
  }

  @handleErrors()
  public async updateMember(params: UpdateMemberMutationVariables): Promise<void> {
    try {
      await this.memberClient.updateMember(params);
    } catch (error) {
      if (error instanceof ConflictError) {
        error.ignore = true;
      }

      throw error;
    }
  }

  @handleErrors()
  public async getProvisioningSource(id: string): Promise<MemberProvisioningSource | null> {
    // Done as part of https://hello.atlassian.net/browse/ESPRESSO-308
    // This try/catch logic can be removed after https://hello.atlassian.net/browse/ESPRESSO-308 has been implemented
    try {
      // tslint:disable-next-line:no-return-await Awaiting in order for try/catch to work properly
      return await this.getProvisioningSourceFromMemberMutability(id);
    } catch (_) {
      // all relevant errors have already been submitted to New Relic, so we're ignoring this one here
      return null;
    }
  }

  @handleErrors()
  public async getMemberTokens(memberId: string): Promise<MemberToken[]> {
    return this.identityClient.getMemberTokens(memberId);
  }

  @handleErrors()
  public async revokeMemberToken(memberId: string, memberTokenId: string): Promise<void> {
    await this.identityClient.revokeMemberToken(memberId, memberTokenId);
  }

  public static init(): Member {
    return new Member(
      apiFacade.memberClient,
      apiFacade.organizationClient,
      apiFacade.forgetAboutMeClient,
      apiFacade.profileMutabilityClient,
      apiFacade.identityClient,
    );
  }

  // Extracting this as a separate function so we could catch API errors and report them to New Relic
  @handleErrors()
  private async getProvisioningSourceFromMemberMutability(id: string): Promise<MemberProvisioningSource | null> {
    const mutabilityResponse = await this.profileMutabilityClient.getMemberMutability(id);

    const mutabilityContraintReasons = (mutabilityResponse.not_editable || []).map(({ reason }) => reason);
    if (mutabilityContraintReasons.includes('ext.dir.google')) {
      return 'GSYNC';
    }

    if (mutabilityContraintReasons.includes('ext.dir.scim')) {
      return 'SCIM';
    }

    return null;
  }
}
