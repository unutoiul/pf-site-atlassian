import { apiFacade } from '../client';
import { CadsClient } from '../client/cads-client';
import { handleErrors } from './utils/handle-errors.decorator';

export class Cads {
  constructor(private cadsClient: CadsClient) {}

  @handleErrors()
  public async getDirectoryId(cloudId: string) {
    return this.cadsClient.getDirectoryId(cloudId);
  }

  @handleErrors()
  public async deleteCustomer(directoryId: string, accountId: string) {
    return this.cadsClient.deleteCustomer(directoryId, accountId);
  }

  @handleErrors()
  public async getCustomerExport(directoryId: string, errorRedirectUrl: string) {
    return this.cadsClient.getCustomerExport(directoryId, errorRedirectUrl);
  }

  @handleErrors()
  public async updateCustomerAccess(directoryId: string, accountId: string, access: boolean): Promise<void> {
    return this.cadsClient.updateCustomerAccess(directoryId, accountId, access);
  }

  @handleErrors()
  public async updateCustomerName(directoryId: string, accountId: string, name: string): Promise<void> {
    return this.cadsClient.updateCustomerAccount(directoryId, accountId, name);
  }

  @handleErrors()
  public async updateCustomerPassword(directoryId: string, accountId: string, password: string): Promise<void> {
    return this.cadsClient.updateCustomerAccount(directoryId, accountId, undefined, password);
  }

  public static create(): Cads {
    return new Cads(
      apiFacade.cadsClient,
    );
  }
}
