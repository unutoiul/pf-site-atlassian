import { PaginatedGroups, ProductAccess, User as UserDetails, UserManagedStatus } from '../../schema-types';
import { apiFacade, UserManagementClient } from '../client';
import { handleErrors } from './utils/handle-errors.decorator';

export class User {
  constructor(
    private umClient: UserManagementClient,
    private cloudId: string,
    private userId: string,
  ) {
  }

  @handleErrors()
  public async activate(): Promise<void> {
    return this.umClient.activateUser(this.cloudId, this.userId);
  }

  @handleErrors()
  public async deactivate(): Promise<void> {
    return this.umClient.deactivateUser(this.cloudId, this.userId);
  }

  @handleErrors()
  public async getDetails(): Promise<UserDetails> {
    return this.umClient.getUser(this.cloudId, this.userId);
  }

  @handleErrors()
  public async getGroups(start: number, count: number): Promise<PaginatedGroups> {
    return this.umClient.getDirectGroups(this.cloudId, this.userId, start, count);
  }

  @handleErrors()
  public async getProductAccess(): Promise<ProductAccess[]> {
    return this.umClient.getProductAccess(this.cloudId, this.userId);
  }

  @handleErrors()
  public async getUserManagedStatus(): Promise<UserManagedStatus> {
    return this.umClient.getUserManagedStatus(this.cloudId, this.userId);
  }

  @handleErrors()
  public async reinvite(): Promise<void> {
    return this.umClient.reinviteUser(this.cloudId, this.userId);
  }

  public static create(cloudId: string, userId: string): User {
    return new User(apiFacade.userManagementClient, cloudId, userId);
  }
}
