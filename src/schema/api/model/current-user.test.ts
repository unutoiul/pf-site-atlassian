import { assert, expect } from 'chai';
import * as fetchMock from 'fetch-mock';

import { AuthenticationError, AuthorizationError } from 'common/error';

import { SiteClient } from '../client/site-client';

import { MeClient } from '../client/me-client';
import { CurrentUser } from './current-user';

describe('CurrentUser', () => {
  function createTestUserWithMeClient(
    meClientMock: Partial<MeClient>,
  ): CurrentUser {
    return new CurrentUser(
      meClientMock as any,
      null as any,
      null as any,
      null as any,
    );
  }

  function createTestUserWithSiteClient(
    siteClientMock: Partial<SiteClient>,
  ): CurrentUser {
    return new CurrentUser(null as any, siteClientMock as any, null as any, null as any);
  }

  describe('getInfoOrThrow', () => {
    afterEach(() => {
      fetchMock.restore();
    });

    it('should return an empty object if the me client response is ok', async () => {
      const testUser = { account_id: '1', email: 'test' };
      const user = createTestUserWithMeClient({
        getCurrentUser: async () => Promise.resolve(testUser),
      });

      await user.getInfoOrThrow()
        .then((result) => expect(result).to.eql({
          id: '1',
        }))
        .catch(() => assert.fail());
    });

    it('should re-throw an AuthenticationError from me client without logging the error', async () => {
      const user = createTestUserWithMeClient({
        getCurrentUser: async () => Promise.reject(new AuthenticationError({})),
      });

      await user.getInfoOrThrow()
        .then(() => assert.fail())
        .catch(e => {
          expect(e instanceof AuthenticationError).to.equal(true);
        });
    });

    it('should re-throw non-authentication errors from me client and log them', async () => {
      const user = createTestUserWithMeClient({
        getCurrentUser: async () => Promise.reject(new Error('test')),
      });

      await user.getInfoOrThrow()
        .then(() => assert.fail())
        .catch(e => {
          expect(e instanceof Error).to.equal(true);
        });
    });
  });

  describe('getSites', () => {
    it('should return list of sites that the user is site admin of', async () => {
      const sites = [{ id: 'my-site-id', displayName: 'My Awesome Site', avatarUrl: 'some-link' }];
      const user = createTestUserWithSiteClient({
        getSites: async () => Promise.resolve({ sites }),
      });

      const actualSites = await user.getSites();

      expect(actualSites).to.deep.equal(sites);
    });

    it('should return empty list of sites when authorization error occurs', async () => {
      const user = createTestUserWithSiteClient({
        getSites: async () => Promise.reject(new AuthorizationError({})),
      });

      const actualSites = await user.getSites();

      expect(actualSites).to.deep.equal([]);
    });

    it('should throw exception when error occurs', async () => {
      const user = createTestUserWithSiteClient({
        getSites: async () => Promise.reject(new Error('some random error')),

      });

      await user.getSites()
        .then(() => assert.fail())
        .catch(e => {
          expect(e instanceof Error).to.equal(true);
        });
    });
  });
});
