import { assert, expect } from 'chai';
import { sandbox as sinonSandbox, SinonSandbox } from 'sinon';

import { BadRequestError, ConflictError, RequestError } from 'common/error';

import { ClaimDomainMutationVariables } from '../../schema-types';
import { DomainClient } from '../client/domain-client';
import { Domain } from './domain';

describe('Domain', () => {
  let sandbox: SinonSandbox;
  let domainClientMock: { [K in keyof DomainClient]: any };

  beforeEach(() => {
    sandbox = sinonSandbox.create();
    domainClientMock = {
      claimDomain: () => undefined,
      pollClaimDomainStatus: () => undefined,
    };
  });

  afterEach(() => {
    sandbox.restore();
  });

  const createDomain = () => {
    return new Domain(domainClientMock as any);
  };

  describe('claimDomain', async () => {

    const params: ClaimDomainMutationVariables = {
      id: '1234',
      domain: 'partycorgi.com',
      method: 'dns',
    };

    const stubClaimDomain = (response: Promise<any>) => {
      sandbox.stub(domainClientMock, 'claimDomain').returns(response);
    };

    const stubPollClaimDomainStatus = (response: Promise<any>) => {
      sandbox.stub(domainClientMock, 'pollClaimDomainStatus').returns(response);
    };

    it('should return success if response is empty', async () => {
      stubClaimDomain(Promise.resolve({}));

      createDomain().claimDomain(params)
        .then((response) => {
          expect(response.status).to.equal('SUCCESS');
        })
        .catch(() => assert.fail());
    });

    it('should poll if response is not empty', async () => {
      stubClaimDomain(Promise.resolve({ taskId: '1234' }));
      stubPollClaimDomainStatus(Promise.resolve({ status: 'SUCCESS' }));

      createDomain().claimDomain(params)
        .then((response) => {
          expect(response.status).to.equal('SUCCESS');
        })
        .catch(() => assert.fail());
    });

    it('should ignore conflict errors', async () => {
      stubClaimDomain(Promise.reject(new ConflictError({})));

      await createDomain().claimDomain(params)
        .then(() => assert.fail())
        .catch(e => {
          expect(e instanceof ConflictError).to.equal(true);
          expect(e.ignore).to.equal(true);
        });
    });

    it('should ignore bad request errors', async () => {
      stubClaimDomain(Promise.reject(new BadRequestError({})));

      await createDomain().claimDomain(params)
        .then(() => assert.fail())
        .catch(e => {
          expect(e instanceof BadRequestError).to.equal(true);
          expect(e.ignore).to.equal(true);
        });
    });

    it('should throw exception when error occurs', async () => {
      stubClaimDomain(Promise.reject(new RequestError({})));

      await createDomain().claimDomain(params)
        .then(() => assert.fail())
        .catch(e => {
          expect(e instanceof Error).to.equal(true);
        });
    });
  });
});
