import { assert, expect } from 'chai';
import * as fetchMock from 'fetch-mock';

import { ValidationError } from 'common/error';

import { RequestHelper } from '../client/request-helper';
import {
  AppGrant,
  AppInstallationQueryResponse,
  AppInstallationSummary,
  UserConnectedAppsClient,
} from '../client/user-connected-apps-client';
import { extractCloudId, UserConnectedApps } from './user-connected-apps';

const contextForJiraCloudId = (cloudId) => `ari:cloud:jira:${cloudId}:project/`;
const contextForJSDCloudId = (cloudId) => `ari:cloud:jira-servicedesk::site/${cloudId}`;
const contextForConfluenceCloudId = (cloudId) => `ari:cloud:confluence::site/${cloudId}`;

describe('user-connected-apps-client', async () => {
  const RIGHT_SITE = '497ea592-beb4-43c3-9137-a6e5fa301088';
  const WRONG_SITE = 'fa301088-beb4-9137-43c3-497ea5921088';

  const getTestClient = () => new UserConnectedApps(new UserConnectedAppsClient(new RequestHelper()));

  const response = (installations: AppInstallationSummary[]): AppInstallationQueryResponse => ({
    values: installations,
    _links: { self: '/installations?self' },
    limit: 100,
    size: installations.length,
    start: 0,
  });

  const installation = (context: string, grants?: AppGrant[], name?: string, oauthClientId: string = 'oauth-client-id'): AppInstallationSummary => ({
    id: 'installation:id',
    definitionId: 'definitionId',
    oauthClientId,
    key: 'app-key',
    latest: true,
    origin: 'dac',
    _links: { self: '/app-key?self', definition: '/app-key?definition' },
    application: 'jira',
    enabled: true,
    installTime: '2018-10-17T22:01:59.439Z',
    baseUrl: 'https://connect.prod.public.atl-paas.net',
    context,
    grants,
    name,
  });

  const grantForContext = (
    context,
    oauthClientId: string = 'oauth-client-id',
    id: string = 'grant:id',
    createdAt: string = '2018-05-30T07:07:50.482Z',
  ): AppGrant => ({
    id,
    accountId: id,
    audience: 'api.stg.atlassian.com',
    context,
    oauthClientId,
    type: 'user',
    createdAt,
    scopes: [
      'read:jira-user',
    ],
  });

  beforeEach(fetchMock.restore);

  it('raises a validation error if apps for other sites are found', async () => {
    const withBadSites = response([
      installation(contextForJiraCloudId(RIGHT_SITE)),
      installation(contextForJiraCloudId(WRONG_SITE)),
    ]);
    const url = UserConnectedAppsClient.buildAppsUrl([contextForJiraCloudId(RIGHT_SITE), contextForJSDCloudId(RIGHT_SITE), contextForConfluenceCloudId(RIGHT_SITE)], 0, 20);
    fetchMock.get(url, {
      status: 200,
      body: withBadSites,
    });
    try {
      await getTestClient().getUserConnectedApps(RIGHT_SITE);
    } catch (e) {
      expect(e instanceof ValidationError).to.be.true('Expected validation error, but a different kind of error was thrown');

      return;
    }
    assert.fail('Expected validation error, but no error was thrown');
  });

  it('raises a validation error if apps for other grants are found', async () => {
    const withBadGrants = response([
      installation(contextForJiraCloudId(RIGHT_SITE), [
        grantForContext(contextForJiraCloudId(RIGHT_SITE)),
        grantForContext(contextForJiraCloudId(WRONG_SITE)),
      ]),
    ]);
    const url = UserConnectedAppsClient.buildAppsUrl([contextForJiraCloudId(RIGHT_SITE), contextForJSDCloudId(RIGHT_SITE), contextForConfluenceCloudId(RIGHT_SITE)], 0, 20);
    fetchMock.get(url, {
      status: 200,
      body: withBadGrants,
    });
    try {
      await getTestClient().getUserConnectedApps(RIGHT_SITE);
    } catch (e) {
      expect(e instanceof ValidationError).to.be.true('Expected validation error, but a different kind of error was thrown');

      return;
    }
    assert.fail('Expected validation error, but no error was thrown');
  });

  it('sorts apps and users correctly', async () => {
    const withUnordereredApps = response([
      installation(contextForJiraCloudId(RIGHT_SITE), [
        grantForContext(contextForJiraCloudId(RIGHT_SITE), '1', 'grant1', '2017-05-30T07:07:50.482Z'),
        grantForContext(contextForJiraCloudId(RIGHT_SITE), '1', 'grant2', '2018-05-30T07:07:50.482Z'),
      ], 'Damogran', '1'),
      installation(contextForJSDCloudId(RIGHT_SITE), [
        grantForContext(contextForJSDCloudId(RIGHT_SITE), '2'),
        grantForContext(contextForJSDCloudId(RIGHT_SITE), '2'),
      ], 'hawalius', '2'),
      installation(contextForConfluenceCloudId(RIGHT_SITE), [
        grantForContext(contextForConfluenceCloudId(RIGHT_SITE), '3'),
        grantForContext(contextForConfluenceCloudId(RIGHT_SITE), '3'),
      ], 'Barnard\'s Star', '3'),
    ]);
    const url = UserConnectedAppsClient.buildAppsUrl([contextForJiraCloudId(RIGHT_SITE), contextForJSDCloudId(RIGHT_SITE), contextForConfluenceCloudId(RIGHT_SITE)], 0, 20);
    fetchMock.get(url, {
      status: 200,
      body: withUnordereredApps,
    });
    const result = await getTestClient().getUserConnectedApps(RIGHT_SITE);

    assert.equal(result[0].name, 'Barnard\'s Star');
    assert.equal(result[1].name, 'Damogran');
    assert.equal(result[2].name, 'hawalius');

    assert.equal(result[1].userGrants[0].accountId, 'grant2');
    assert.equal(result[1].userGrants[1].accountId, 'grant1');
  });
});

describe('extractCloudId', () => {
  const cloudId = '38b7be26-03f7-42a9-b18b-9cdb56d204c2';

  it('normal cloudId', () => {
    assert.equal(extractCloudId(contextForJiraCloudId(cloudId)), cloudId);
  });

  it('"DUMMY-" prefixed cloudId', () => {
    // product-fabric.atlassian.net and some staging cloudIds have a "DUMMY-" prefix.
    const dummyCloudId = `DUMMY-${cloudId}`;
    assert.equal(extractCloudId(contextForJiraCloudId(dummyCloudId)), dummyCloudId);
  });
});
