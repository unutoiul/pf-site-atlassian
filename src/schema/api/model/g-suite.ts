import { GSuiteConnection, GSuiteState } from '../../schema-types';
import { apiFacade, GSuiteClient } from '../client';
import { handleErrors } from './utils/handle-errors.decorator';

export class GSuite {
  constructor(
    private gSuiteClient: GSuiteClient,
    private cloudId: string,
  ) {
  }

  @handleErrors()
  public async getState(): Promise<GSuiteState | undefined> {
    return this.gSuiteClient.getState(this.cloudId);
  }

  @handleErrors()
  public async getGroups(): Promise<GSuiteState | undefined> {
    return this.gSuiteClient.getGroups(this.cloudId);
  }
  public async getConnection(): Promise<GSuiteConnection> {
    return this.gSuiteClient.getConnection(this.cloudId);
  }

  public static create(cloudId: string): GSuite {
    return new GSuite(apiFacade.gSuiteClient, cloudId);
  }
}
