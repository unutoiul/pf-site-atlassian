import { AccessRequestList, AccessRequestListInput, ApproveAccessInput, DenyAccessInput } from '../../schema-types/schema-types';
import { apiFacade, UserManagementClient } from '../client';
import { handleErrors } from './utils/handle-errors.decorator';

export class AccessRequest {
  constructor(
    private umClient: UserManagementClient,
  ) {
  }

  @handleErrors()
  public async getRequests(cloudId: string, paginationParams: AccessRequestListInput): Promise<AccessRequestList> {
    return this.umClient.getAccessRequests(cloudId, paginationParams);
  }

  @handleErrors()
  public async approveAccess(cloudId: string, approveAccessBody: ApproveAccessInput): Promise<void> {
    return this.umClient.approveAccess(cloudId, approveAccessBody);
  }

  @handleErrors()
  public async denyAccess(cloudId: string, denyAccessBody: DenyAccessInput): Promise<void> {
    return this.umClient.denyAccess(cloudId, denyAccessBody);
  }

  public static create(): AccessRequest {
    return new AccessRequest(apiFacade.userManagementClient);
  }
}
