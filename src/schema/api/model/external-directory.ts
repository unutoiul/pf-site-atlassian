import { CreateDirectoryMutationArgs, CreateDirectoryResponse, LogEntryCollection, RegenerateDirectoryApiKeyMutationArgs, RegenerateDirectoryApiKeyResponse, RemoveDirectoryMutationArgs, SyncedGroupsCollection, SyncedUsers } from '../../schema-types';
import {
  apiFacade,
  ExternalDirectoryClient,
  GroupsRequestParams,
  LogsRequestParams,
  SyncedUsersRequestParams,
} from '../client';
import { handleErrors } from './utils/handle-errors.decorator';

export class ExternalDirectory {
  constructor(
    private externalDirectoryClient: ExternalDirectoryClient,
  ) {
  }

  @handleErrors()
  public async revoke(params: RemoveDirectoryMutationArgs): Promise<void> {
    await this.externalDirectoryClient.removeDirectory(params);
  }

  @handleErrors()
  public async regenerate(params: RegenerateDirectoryApiKeyMutationArgs): Promise<RegenerateDirectoryApiKeyResponse> {
    const response = await this.externalDirectoryClient.regenerateDirectoryApiKey(params);

    return { apiKey: response.token };
  }

  @handleErrors()
  public async create(params: CreateDirectoryMutationArgs): Promise<CreateDirectoryResponse> {
    const response = await this.externalDirectoryClient.createDirectory(params);
    const { token: apiKey, ...rest } = response;

    return {
      apiKey,
      ...rest,
    };
  }

  @handleErrors()
  public async syncedUsers(params: SyncedUsersRequestParams): Promise<SyncedUsers> {
    return this.externalDirectoryClient.getSyncedUsers(params);
  }

  public static init(): ExternalDirectory {
    return new ExternalDirectory(
      apiFacade.externalDirectoryClient,
    );
  }

  @handleErrors()
  public async logs(params: LogsRequestParams): Promise<LogEntryCollection> {
    const response = await this.externalDirectoryClient.getLogs(params);
    const { totalResults, startIndex, itemsPerPage, Resources } = response;
    const logEntries = Resources.map(({ created_on: createdOn, ...rest }) => ({ createdOn, ...rest }));

    return {
      totalResults,
      startIndex,
      itemsPerPage,
      logEntries,
    };
  }

  @handleErrors()
  public async groups(params: GroupsRequestParams): Promise<SyncedGroupsCollection> {
    const response = await this.externalDirectoryClient.getGroups(params);
    const { totalResults, startIndex, itemsPerPage, Resources: groups } = response;

    return {
      totalResults,
      startIndex,
      itemsPerPage,
      groups,
    };
  }
}
