/* tslint:disable */
// This file was automatically generated
export const typeDefs = `
enum AccessLevel {
  NONE
  USE
  ADMIN
  SYSADMIN
}

enum ProductPermission { 
  WRITE 
  MANAGE
}

type ProductAccess {
  productId: ID!
  productName: String!
  presence: String
  accessLevel: AccessLevel!
}

type GroupAccess {
  id: String!
  name: String!
  productPermission: [ProductPermission]!
  requiresApproval: Boolean!
  default: Boolean!
}

type GroupsAccessConfig {
  id: String
  product: ProductAccess!
  groups: [GroupAccess]!
}

enum AccessRequestStatus {
  PENDING,
  APPROVED,
  REJECTED
}

enum AccessRequestType {
 INVITE,
 REQUEST
}

input AccessRequestListInput {
  status: [AccessRequestStatus!]!
  start: Int!
  count: Int
}

input ApproveAccessInput {
  userId: String!
  productId: String!
  atlOrigin: String!
}

input DenyAccessInput {
  userId: String!
  productId: String!
  denialReason: String
}

type SubProduct {
  productId: String!
  productName: String!
}

type RequestedProduct {
  productId: String!
  productName: String!
  status: AccessRequestStatus!
  requestedTime: String!
  subProducts: [SubProduct!]
}

type AccessRequest {
  user: User!,
  requestedProducts: [RequestedProduct!]!
  type: AccessRequestType!
  requester: User
}

type AccessRequestList {
  total: Int!
  accessRequests: [AccessRequest!]!
}

extend type Query {
  accessRequestList(cloudId: String!, input: AccessRequestListInput!): AccessRequestList!
}

extend type Mutation {
  approveAccess(cloudId: String!, input: ApproveAccessInput!): Boolean
  denyAccess(cloudId: String!, input: DenyAccessInput!): Boolean
}


type AdminApiKey {
  id: String!
  name: String!
  createdBy: Member!
  creationDate: String!
  expirationDate: String!
}

type AdminApiKeyCollection {
  scrollId: String
  tokens: [AdminApiKey!]!
}

type AdminApiKeyWithToken {
  id: String!
  name: String!
  createdBy: Member!
  creationDate: String!
  expirationDate: String!
  token: String!
}

extend type Mutation {
  createAdminApiKey(orgId: String!, name: String!): AdminApiKeyWithToken!
  deleteAdminApiKey(orgId: String!, apiKeyId: String!): Boolean!
}
scalar UnixTimestamp

# These placeholder fields can be removed once https://github.com/graphql/graphql-js/issues/937 
# is resolved.

type Query {
  _: Boolean
}

type Mutation {
  _: Boolean
}

type CurrentUser {
  id: ID!
  organizations: [Organization]!
  sites: [Site]!
  unlinkedSites: [OrgSite!]!
  email: String!
  isSiteAdmin: Boolean
  isSystemAdmin(cloudId: String!): Boolean
  flag(flagKey: String!): FeatureFlag!
}

extend type Query {
  currentUser: CurrentUser
}

enum VerificationType {
  http
  dns
}

type Domain {
  id: String!
  domain: String!
  status: String!
  verificationType: VerificationType
  verified: Boolean!
}

type CheckDomainClaimed {
  claimed: Boolean!
}

extend type Mutation {
  claimDomain(id: String!, domain: String!, method: VerificationType!): ClaimDomainStatus
  deleteDomain(id: String!, domain: String!): Boolean
}

extend type Query {
  checkDomainClaimed(domain: String!): CheckDomainClaimed!
}

type DomainClaim {
  domains: [Domain]
  token: String
}

enum ClaimDomainStatusType {
  SUCCESS,
  IN_PROGRESS,
  FAILED
}

type ClaimDomainStatus {
  status: ClaimDomainStatusType!
}

type ExternalDirectory {
  id: ID!
  baseUrl: String!
  name: String!
  creationDate: String!
  syncedUsers: SyncedUsers!
  logs(startIndex: Int, count: Int): LogEntryCollection!
  groups(startIndex: Int, count: Int): SyncedGroupsCollection!
}

type LogEntryCollection {
  totalResults: Int!
  startIndex: Int!
  itemsPerPage: Int!
  logEntries: [LogEntries!]!
}

type LogEntries {
  error: String!
  createdOn: String!
}

type SyncedGroupsCollection {
  totalResults: Int!
  startIndex: Int!
  itemsPerPage: Int!
  groups:[SyncedGroup!]!
}

type SyncedGroup {
  name: String!
  membershipCount: Int!
}

type SyncedUsers {
  countOfUsers: Int!
}

type CreateDirectoryResponse {
  id: ID!
  baseUrl: String!
  name: String!
  creationDate: String!
  apiKey: String!
}

type RegenerateDirectoryApiKeyResponse {
  apiKey: String!
}

extend type Mutation {
  createDirectory(orgId: String!, name: String!): CreateDirectoryResponse!
  regenerateDirectoryApiKey(directoryId: String!): RegenerateDirectoryApiKeyResponse!
  removeDirectory(directoryId: String!): Boolean!
}
type FeatureFlag {
  id: ID!
  value: Boolean
}

extend type Query {
  userFeatureFlag(flag: String!): FeatureFlag
}
extend type Mutation {
  submitFeedback(comment: String!, email: String!, webInfo: String!): Boolean
}

type GSuite {
  state: GSuiteState,
  groups: [GSuiteGroups!]
}

type GSuiteGroups {
  id: ID!
  name: String!
}

type GSuiteState {
  id: ID!
  syncConfig: [String]!
  syncStartTime: String
  nextSyncStartTime: String
  lastSync: GSuiteLastSync
  selectedGroups: [String]!
  noSyncs: Int!
  user: String!
}

type GSuiteLastSync {
  startTime: String!
  failed: Int!
  errors: [String]!
  updated: Int!
  hasReport: Boolean!
  status: GSuiteLastSyncStatus!
  finishTime: String!
  numOfSyncedUsers: Int!
  deleted: Int!
  created: Int!
}

type GSuiteConnection { 
  url: String!
}

extend type Mutation { 
  getConnection (
    id: String!,
  ): GSuiteConnection!
}

enum GSuiteLastSyncStatus {
  successful,
  failure
}

type Group {
  description: String
  id: ID!
  name: String!
  productPermissions: [GroupProductPermissions]!
  defaultForProducts: [GroupProduct]!
  sitePrivilege: SitePrivilege!
  unmodifiable: Boolean!
  userTotal: Int!
  managementAccess: ManagementAccess!
  ownerType: OwnerType
}

enum SitePrivilege {
  NONE
  SYS_ADMIN
  SITE_ADMIN
}

enum Permission {
  WRITE
  MANAGE
}

enum ManagementAccess {
  NONE
  READ_ONLY
  CANNOT_DELETE
  ALL
}

enum OwnerType {
  EXT_GSYNC
  EXT_SCIM
  MANAGED
  ORG
}

type GroupProductPermissions {
  productName: String!
  productId: String!
  permissions: [Permission]!
}

type GroupProduct {
  productName: String!
  productId: String!
}

type PaginatedGroups {
  total: Int!
  groups: [Group]!
}

type GroupDetails {
  groupDetails: Group!
  members(
    start: Int
    count: Int
  ): PaginatedUsers!
}

extend type Query {
  group(cloudId: String!, id: String!): GroupDetails!
  groupList(
    cloudId: String!
    start: Int
    count: Int
    displayName: String
  ): PaginatedGroups!
}

input UpdateGroupInput {
  description: String
  name: String!
}

input AddUsersToGroupsInput {
  groups: [String]!
  users: [String]!
}

extend type Mutation {
  createGroup(
    cloudId: String!,
    input: UpdateGroupInput!
  ): Group!

  updateGroup(
    cloudId: String!,
    id: String!,
    input: UpdateGroupInput!
  ): Group!

  deleteGroup(
    cloudId: String!,
    id: String!
  ): Boolean!

  addUsersToGroups(
    cloudId: String!,
    input: AddUsersToGroupsInput!
  ): Boolean!

  deleteUserFromGroup(
    cloudId: String!,
    userId: String!,
    groupId: String!
  ): Boolean!
}

type InviteUrl {
  url: String!
  productKey: String!
  productName: String!
  expiration: String!
}

input GenerateInviteUrlInput {
  productKey: String!
}

input DeleteInviteUrlInput {
  productKey: String!
}

extend type Mutation {
  generateInviteUrl(id: String!, input: GenerateInviteUrlInput!): Boolean
  deleteInviteUrl(id: String!, input: DeleteInviteUrlInput!): Boolean
}

type Directory {
  id: ID!
  customerExport(errorRedirectUrl: String!): String!
}

type JsdCustomer {
  id: ID!
  username: String!
  email: String!
  active: Boolean!
  displayName: String!
  lastLogin: String
}

type PaginatedJsdCustomers {
  hasMoreResults: Boolean!
  customers: [JsdCustomer!]!
}

type Jsd {
  customers(start: Int!, count: Int!, displayName: String, activeStatus: String): PaginatedJsdCustomers!
  directory: Directory!
}

extend type Mutation {
  deleteJsdCustomer(directoryId: ID!, accountId: ID!): Boolean
  updateJsdCustomerAccess(directoryId: ID!, accountId: ID!, access: Boolean!): Boolean
  updateJsdCustomerName(directoryId: ID!, accountId: ID!, name: String!): Boolean
  updateJsdCustomerPassword(directoryId: ID!, accountId: ID!, password: String!): Boolean
  migrateJsdCustomer(cloudId: String!, accountId: ID!): Boolean
}
type PaginatedMembers {
  total: Int!
  users: [Member]
}

type MemberTotal {
  total: Int!
  enabledTotal: Int!
  disabledTotal: Int!
}

enum MemberStatus {
  ENABLED
  DISABLED
  BLOCKED
}

type MemberSiteAccess {
  siteUrl: String!
  isSiteAdmin: Boolean!
}

type MemberAccessProducts {
  siteUrl: String!
  productName: String!
  productKey: String!
}

type MemberAccess {
  sites: [MemberSiteAccess!]!
  products: [MemberAccessProducts!]!
  errors: [String!]!
}

type PendingInfo {
  startedOn: String!
  startedBy: String!
  initiatorName(orgId: String!): String
}

type DeactivatedInfo {
  deactivatedOn: String!
  deactivatedBy: String!
  initiatorName(orgId: String!): String
}

type Member {
  active: MemberStatus!
  displayName: String!
  emails: [MemberEmail]!
  id: ID!
  jobTitle: String
  useMfa: Boolean
  memberAccess: MemberAccess
  primaryEmail: String
  provisionedBy: MemberProvisioningSource
  pendingInfo: PendingInfo
  deactivatedInfo: DeactivatedInfo
}

enum MemberProvisioningSource {
  GSYNC
  SCIM
}

type LifecycleHookReason {
  message: String!
  code: String!
  link: String
}

type LifecycleHookResult {
  errors: [LifecycleHookReason]!
  warnings: [LifecycleHookReason]!
}

type MemberProduct {
  productName: String
  siteUrl: String
}

type MemberEmail {
  primary: Boolean!
  value: String!
}

type MemberProfileMinimal {
  id: String!
  fullName: String!
  avatarUrl: String!
}


type MemberToken {
  id: ID!
  label: String!
  createdAt: String!
  lastAccess: String
}

type MemberDetails {
  memberDetails(pendingAndDeactivatedInfoReadFromIOM: Boolean!): Member!
  canCloseAccount: LifecycleHookResult
  canDeactivateAccount: LifecycleHookResult
  memberTokens: [MemberToken!]!
}

extend type Query {
  member(id: String!, memberId: String!): MemberDetails!
  membersList(id: String!, aaIds: [String]!): [MemberProfileMinimal]!
}

extend type Mutation {
  updateMember(
    id: String!
    member: String!
    displayName: String!
    emailAddress: String!
    jobTitle: String!
  ): Boolean
  updateMemberEnabled(id: String!, member: String!, enabled: Boolean!): Boolean
  resetMemberPassword(id: String!, member: String!): Boolean!
  closeMemberAccount(memberId: String!): Boolean
  cancelDeleteMemberAccount(memberId: String!): Boolean
  deactivateMemberAccount(memberId: String!): Boolean
  activateMemberAccount(memberId: String!): Boolean
  revokeMemberToken(memberId: String!, memberTokenId: String!): Boolean
}

type MfaExemptUser {
  userId: String!
}

input UpdateMfaExemptionInput {
  id: String!
  member: String!
  exempt: Boolean!
}

input UpdateMfaEnrollmentInput {
  id: String!
  member: String!
}

extend type Mutation {
  updateMfaExemption(input: UpdateMfaExemptionInput!): Boolean!
  updateMfaEnrollment(input: UpdateMfaEnrollmentInput!): Boolean!
}

type Onboarding {
  id: String!
  dismissed: Boolean!
}

extend type Query {
  onboarding(id: String!): Onboarding!
}

extend type Mutation {
  dismissOnboarding(id: String!): Onboarding!
}

type OrgProducts {
  id: ID!
  identityManager: Boolean!
}

extend type Query {
  products(id: String!): OrgProducts!
}

extend type Mutation {
  evaluateIdentityManager(orgId: String!): OrgProducts!
}

type OrganizationSecurity {
  passwordPolicy: PasswordPolicy
  saml: SamlConfiguration
  twoStepVerification: TwoStepVerification
}
enum MemberStateFilter {
  ALL
  ENABLED
  DISABLED
  BILLABLE
  NON_BILLABLE
}

type OrganizationUsers {
  admins: Admins!
  members(
    start: Int
    count: Int
    active: Boolean
    mfa: Boolean
    displayName: String
    domain: String
    filter: String
    products: [String!]
    memberState: MemberStateFilter
  ): PaginatedMembers
  memberTotal: MemberTotal
  mfaExemptMembers: [MfaExemptUser]
}

type Admins {
  id: String!
  total: Int!
  users: [Member]
}

enum MigrationState {
  ORGS
  IM_GRANDFATHERED
  IM
}

type Organization {
  id: String!
  name: String!
  users: OrganizationUsers!
  security: OrganizationSecurity!
  domainClaim: DomainClaim!
  migrationState: MigrationState
  flag(flagKey: String!): FeatureFlag!
  isBillable: Boolean!
  externalDirectories: [ExternalDirectory]!
  siteAdmins(cloudId: String!): [SiteAdmin!]!
  memberExport(exportId: String!): MemberExport!
  sites: [OrgSite!]!
  adminApiKeys(scrollId: String): AdminApiKeyCollection!
  products: [Product!]!
}

type OrgSite {
  id: ID!
  displayName: String
  avatar: String
  siteUrl: String!
  products: [Product!]!
}

type SiteAdmin {
  id: ID!
  displayName: String!
  email: String!
  isOrgAdmin: Boolean!
}

type OrgSiteLinkingResult {
  isSuccess: Boolean!
  errors: [String!]
}

type MemberExport {
  id: ID!
  content: String!
}

extend type Query {
  organization(id: String!): Organization!
}

extend type Mutation {
  createOrganization(name: String!): PollCreateOrgInfo!
  addOrganizationAdmin(orgId: String!, email: String!): Boolean!
  removeOrganizationAdmin(orgId: String!, adminId: String!): Boolean!
  renameOrganization(id: String!, name: String!): Organization!
  initiateMemberCsvEmailExport(orgId: String!): Boolean!
  linkSiteToOrganization(orgId: String!, cloudId: String!): OrgSiteLinkingResult!
}

type PasswordPolicy {
  minimumStrength: String
  expiryDays: Int
}

input UpdatePasswordPolicyInput {
  id: String!
  minimumStrength: String!
  expiryDays: Int
  resetPasswords: Boolean!
}

extend type Mutation {
  updatePasswordPolicy(input: UpdatePasswordPolicyInput!): Boolean
  resetPasswords(id: String!): Boolean
}

type PollCreateOrgState {
  completed: Boolean!
  successful: Boolean!
}

type PollCreateOrgInfo {
  progressUri: String!
  id: String!
}

extend type Query {
  pollCreateOrg(progressUri: String): PollCreateOrgState
}

type Product {
  key: String!
  name: String!
}
type RolePermissions {
  permissionIds: [String]!
}

extend type Query {
  rolePermissions(cloudId: String!, roleId: String!): RolePermissions!
}

enum Auth0State {
  LEGACY
  AUTH0
  AUTH0FORCED
}

type SamlConfiguration {
  issuer: String!
  ssoUrl: String!
  publicCertificate: String!
  auth0MigrationState: Auth0State!
  serviceProvider: SamlServiceProvider!
}

input UpdateSamlInput {
  id: String!
  issuer: String!
  ssoUrl: String!
  publicCertificate: String!
  auth0MigrationState: Auth0State!
}

type SamlServiceProvider {
  entityId: String!
  acsUrl: String!
}

extend type Mutation {
  updateSaml(input: UpdateSamlInput!): Boolean!
  deleteSaml(id: String!): Boolean!
}

type SiteAccess {
  signupEnabled: Boolean!
  openInvite: Boolean!
  notifyAdmin: Boolean!
  domains: [String!]!
}

input UpdateSiteAccessInput {
  signupEnabled: Boolean!
  openInvite: Boolean!
  notifyAdmin: Boolean!
  domains: [String!]!
}

extend type Mutation {
  updateSiteAccess(id: String!, input: UpdateSiteAccessInput!): Boolean!
}

type SiteNameAvailabilityCheckResult {
  isAlreadyOwner: Boolean!,
  renameLimitReached: Boolean!,
  result: Boolean!,
  taken: Boolean!
}

extend type Mutation {
  checkSiteNameAvailability(cloudId: String!, siteName: String!): SiteNameAvailabilityCheckResult!
}

type SiteRenameStatus {
  id: ID!
  cloudName: String!
  cloudNamespace: String!
  renamesUsed: Int!
  renameLimit: Int!
}

type RenameSiteRespnose {
  progressUri: String!
}

input RenameSiteInput {
  cloudName: String!
  cloudNamespace: String!
}

extend type Mutation {
  renameSite(cloudId: String!, input: RenameSiteInput!): RenameSiteRespnose!
}

type EmojiSettings {
  id: String!
  uploadEnabled: Boolean!
}

input EmojiSettingsInput {
  uploadEnabled: Boolean!
}

type ServiceDeskSettings {
  enabled: Boolean!
}

type XFlowSettings {
  enabled: Boolean!
}

type SiteSettings {
    id: String!
    emoji: EmojiSettings!
    serviceDesk: ServiceDeskSettings!
    xFlow: XFlowSettings!
}

extend type Mutation {
  updateEmojiSettings(id: String!, input: EmojiSettingsInput!): EmojiSettings!
}

type Application {
  id: ID!
  name: String!,
  adminUrl: String!,
  url: String!,
  product: String!,
}

type SiteContext {
  firstActivationDate: String!
}

type DefaultProduct {
  productId: ID!
  productName: String!
  canonicalProductKey: String!
}

type SiteFlags {
  selfSignupADG3Migration: Boolean!,
  accessConfigADG3Migration: Boolean!
  groupsADG3Migration: Boolean!
  gSuiteADG3Migration: Boolean!
  userExportADG3Migration: Boolean!
  billingADG3Migration: Boolean!
  usersADG3Migration: Boolean!
  jsdADG3Migration: Boolean!
  siteAdminPageMigrationRedirect: Boolean!
  accessRequestsPage: Boolean!
  inviteUrls: Boolean!
  userConnectedAppsPage: Boolean!
  siteUrlPage: Boolean!
}

input DefaultProductsInput {
  productId: String!
  productDefault: Boolean!
}

input DefaultGroupsInput {
  productId: String!
  groupId: String!
  isDefault: Boolean!
}

enum ProductType {
  USE
  ADMIN
}

input AddGroupsAccessToProductInput {
  productId: String!
  groups: [String!]!
  productType: ProductType!
}

input RemoveGroupAccessToProductInput {
  productId: String!
  groupId: String!
  productType: ProductType!
}

input GroupImportInput {
  productId: String!
  groupId: String!
}

input GenerateUserExportInput {
  includeGroups: Boolean!
  includeProductAccess: Boolean!
  includeInactiveUsers: Boolean!
  selectedGroupIds: [String!]!
}

type Site {
  applications: [Application]
  avatarUrl: String!
  context: SiteContext
  defaultApps: [DefaultProduct]!
  displayName: String!
  flag(flagKey: String!): FeatureFlag!
  flags: SiteFlags!
  groupsAdminAccessConfig: [GroupsAccessConfig]!
  groupsUseAccessConfig: [GroupsAccessConfig]!
  gSuite: GSuite!
  id: ID!
  inviteUrls: [InviteUrl!]!
  jsd: Jsd!
  settings: SiteSettings!
  siteAccess: SiteAccess!
  siteRenameStatus: SiteRenameStatus!
  userConnectedApps: [UserConnectedApp!]!
}

extend type Query {
  site(id: String!): Site!
  currentSite: Site!
}

extend type Mutation {
  removeUserFromSite(cloudId: String!, id: String!): Boolean
  changeUserRole(cloudId: String!, userId: String!, roleId: String!): Boolean
  userExport(cloudId: String!, input: GenerateUserExportInput!): String!
  inviteUsers(id: String!, input: SiteInviteUsersInput): Boolean
  updateEmojiSettings(id: String!, input: EmojiSettingsInput!): EmojiSettings!
  setDefaultProducts(id: String!, input: DefaultProductsInput!): Boolean
  setDefaultGroup(id: String!, input: DefaultGroupsInput!): Boolean
  addGroupsAccessToProduct(id: String!, input: AddGroupsAccessToProductInput!): Boolean
  removeGroupAccessToProduct(id: String!, input: RemoveGroupAccessToProductInput!): Boolean
  approveImportedGroup(id: String!, input: GroupImportInput!): Boolean
  rejectImportedGroup(id: String!, input: GroupImportInput!): Boolean
}

# 1-to-1 mapping with the exporter CAPI https://bitbucket.org/hipchat/megatron/src/dea190b59760d6c391e1b5492283bfb784997a8d/exporter-capi.yaml#lines-237
enum StrideExportStatus {
  completed,
  pending,
  failed,
  cancelled,
  unknown
}

type StrideExportSettings {
  startDate: String
  endDate: String
  includePublicRooms: Boolean!
  includePrivateRooms: Boolean!
  includeDirects: Boolean!
}

type StrideExport {
  id: ID!
  startedAt: String!
  expiresAt: String
  creatorName: String!
  creatorEmail: String!
  status: StrideExportStatus!
  settings: StrideExportSettings!
  downloadLocation: String
  integrationsLocation: String
}

type Stride {
  strideExports: [StrideExport!]!
  strideExport(exportId: String!): StrideExport!
}

input CreateStrideExportInput {
  startDate: String
  endDate: String
  includePublicRooms: Boolean!
  includePrivateRooms: Boolean!
  includeDirects: Boolean!
  passphrase: String!
}

extend type Query {
  stride(cloudId: String!): Stride!
}

extend type Mutation {
  deleteStrideExport(cloudId: String!, exportId: String!): Boolean!
  cancelStrideExport(cloudId: String!, exportId: String!): Boolean!
  createStrideExport(cloudId: String!, input: CreateStrideExportInput!): StrideExport!
}

type TwoStepVerification {
  dateEnforced: UnixTimestamp
}

extend type Mutation {
  enableTwoStepVerificationEnforcement(
    id: String!
    date: UnixTimestamp!
  ): Boolean
  disableTwoStepVerificationEnforcement(id: String!): Boolean
}

enum Drawer {
  IDM
  SAML
  USER_PROVISIONING_CREATE_DIRECTORY
  USER_PROVISIONING_REGEN_API_KEY
  ORG_SITE_LINKING
  CREATE_ADMIN_API_KEY_DRAWER
  NONE
  SITE_CREATE
  INVITE_USER
  EXPORT_USERS
}

type XFlow {
  isDialogOpen: Boolean!
  sourceComponent: String
  targetProduct: String
}

type Navigation {
  showContainer: Boolean!
  activeDrawer: Drawer!
}

type Analytics {
  referrer: String!
}

type UI {
  isFeedbackOpen: Boolean!
  xflow: XFlow!
  navigation: Navigation!
  analytics: Analytics!
}

extend type Query {
  ui: UI!
}

extend type Mutation {
  updateIsFeedbackOpen(isOpen: Boolean!): Boolean
  updateXFlowDialog(isDialogOpen: Boolean!, sourceComponent: String, targetProduct: String): Boolean
  updateNavigationContainer(showContainer: Boolean!): Boolean
  updateActiveDrawer(activeDrawer: String!): Boolean
  updateReferrer(referrer: String!): Boolean
}

type UserConnectedApp {
  id: String!
  name: String!
  description: String!
  avatarUrl: String
  vendorName: String
  scopes: [AppScope!]!
  userGrants: [UserGrant!]!
}

type AppScope {
  id: String!
  untranslatedName: String!
  untranslatedDescription: String!
}

type UserGrant {
  id: String!
  accountId: String!
  createdAt: String!
  scopes: [AppScope!]!
}

type User {
  id: ID!
  email: String!
  displayName: String!
  active: Boolean!
  nickname: String
  location: String
  companyName: String
  department: String
  title: String
  timezone: String
  system: Boolean!
  presence: String
  activeStatus: ActiveStatus
  siteAdmin: Boolean
  sysAdmin: Boolean
  hasVerifiedEmail: Boolean!
  trustedUser: Boolean
}

enum ActiveStatus {
  ENABLED,
  DISABLED,
  BLOCKED
}

type PaginatedUsers {
  total: Int!
  users: [User]!
}

input SiteInviteUsersInput {
  emails: [String]
  productIds: [String]
  additionalGroups: [String]
  sendNotification: Boolean
  notificationText: String
  role: String
  atlOrigin: String
}

input SiteUsersListInput {
  start: Int!
  count: Int!
  displayName: String
  contains: String
  activeStatus: [String]
  productUse: [String]
  productAdmin: [String]
  sitePrivilege: [String]
}

input SuggestChangesInput {
  userId: String!
  email: String
  displayName: String
}

type DomainOwner {
  ownerType: String!
  ownerId: String!
}

type UserManagedStatus {
  managed: Boolean!
  owner: DomainOwner
}

type UserDetails {
  userDetails: User!
  groups(start: Int, count: Int): PaginatedGroups!
  productAccess: [ProductAccess]!
  managedStatus: UserManagedStatus
}

extend type Query {
  user(cloudId: String!, id: String!): UserDetails!
  siteUsersList(cloudId: String!, input: SiteUsersListInput!): PaginatedUsers!
}

extend type Mutation {
  activateUser(cloudId: String!, id: String!): Boolean
  deactivateUser(cloudId: String!, id: String!): Boolean
  impersonateUser(cloudId: String!, userId: String!): Boolean
  reinviteUser(cloudId: String!, userId: String!): Boolean
  promptResetPassword(cloudId: String!, userId: String!): Boolean
  grantAccessToProducts(cloudId: String!, users: [String!]!, productIds: [String!]!): Boolean
  revokeAccessToProducts(cloudId: String!, users: [String!]!, productIds: [String!]!): Boolean
  suggestChanges(cloudId: String!, userId: String!, email: String!, name: String!): Boolean
}

`;
/* tslint:enable */
