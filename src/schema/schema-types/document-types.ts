/* tslint:disable */
//  This file was automatically generated and should not be edited.

export type ManagementAccess =
  "NONE" |
  "READ_ONLY" |
  "CANNOT_DELETE" |
  "ALL";


export type OwnerType =
  "EXT_GSYNC" |
  "EXT_SCIM" |
  "MANAGED" |
  "ORG";


export type SitePrivilege =
  "NONE" |
  "SYS_ADMIN" |
  "SITE_ADMIN";


export type Drawer =
  "IDM" |
  "SAML" |
  "USER_PROVISIONING_CREATE_DIRECTORY" |
  "USER_PROVISIONING_REGEN_API_KEY" |
  "ORG_SITE_LINKING" |
  "CREATE_ADMIN_API_KEY_DRAWER" |
  "NONE" |
  "SITE_CREATE" |
  "INVITE_USER" |
  "EXPORT_USERS";


export type MemberStatus =
  "ENABLED" |
  "DISABLED" |
  "BLOCKED";


export type VerificationType =
  "http" |
  "dns";


export type ClaimDomainStatusType =
  "SUCCESS" |
  "IN_PROGRESS" |
  "FAILED";


export type MemberProvisioningSource =
  "GSYNC" |
  "SCIM";


export type UpdateMfaEnrollmentInput = {
  id: string,
  member: string,
};

export type UpdateMfaExemptionInput = {
  id: string,
  member: string,
  exempt: boolean,
};

export type MemberStateFilter =
  "ALL" |
  "ENABLED" |
  "DISABLED" |
  "BILLABLE" |
  "NON_BILLABLE";


export type MigrationState =
  "ORGS" |
  "IM_GRANDFATHERED" |
  "IM";


export type UpdatePasswordPolicyInput = {
  id: string,
  minimumStrength: string,
  expiryDays?: number | null,
  resetPasswords: boolean,
};

export type Auth0State =
  "LEGACY" |
  "AUTH0" |
  "AUTH0FORCED";


export type UpdateSamlInput = {
  id: string,
  issuer: string,
  ssoUrl: string,
  publicCertificate: string,
  auth0MigrationState: Auth0State,
};

export type EmojiSettingsInput = {
  uploadEnabled: boolean,
};

export type CreateStrideExportInput = {
  startDate?: string | null,
  endDate?: string | null,
  includePublicRooms: boolean,
  includePrivateRooms: boolean,
  includeDirects: boolean,
  passphrase: string,
};

// 1-to-1 mapping with the exporter CAPI https://bitbucket.org/hipchat/megatron/src/dea190b59760d6c391e1b5492283bfb784997a8d/exporter-capi.yaml#lines-237
export type StrideExportStatus =
  "completed" |
  "pending" |
  "failed" |
  "cancelled" |
  "unknown";


export type AddGroupsAccessToProductInput = {
  productId: string,
  groups: Array< string >,
  productType: ProductType,
};

export type ProductType =
  "USE" |
  "ADMIN";


export type GroupImportInput = {
  productId: string,
  groupId: string,
};

export type DefaultProductsInput = {
  productId: string,
  productDefault: boolean,
};

export type RemoveGroupAccessToProductInput = {
  productId: string,
  groupId: string,
  productType: ProductType,
};

export type DefaultGroupsInput = {
  productId: string,
  groupId: string,
  isDefault: boolean,
};

export type ProductPermission =
  "WRITE" |
  "MANAGE";


export type AccessRequestListInput = {
  status: Array< AccessRequestStatus >,
  start: number,
  count?: number | null,
};

export type AccessRequestStatus =
  "PENDING" |
  "APPROVED" |
  "REJECTED";


export type AccessRequestType =
  "INVITE" |
  "REQUEST";


export type ApproveAccessInput = {
  userId: string,
  productId: string,
  atlOrigin: string,
};

export type DenyAccessInput = {
  userId: string,
  productId: string,
  denialReason?: string | null,
};

export type GSuiteLastSyncStatus =
  "successful" |
  "failure";


export type AddUsersToGroupsInput = {
  groups: Array< string | null >,
  users: Array< string | null >,
};

export type SiteUsersListInput = {
  start: number,
  count: number,
  displayName?: string | null,
  contains?: string | null,
  activeStatus?: Array< string | null > | null,
  productUse?: Array< string | null > | null,
  productAdmin?: Array< string | null > | null,
  sitePrivilege?: Array< string | null > | null,
};

export type UpdateGroupInput = {
  description?: string | null,
  name: string,
};

export type Permission =
  "WRITE" |
  "MANAGE";


export type DeleteInviteUrlInput = {
  productKey: string,
};

export type GenerateInviteUrlInput = {
  productKey: string,
};

export type UpdateSiteAccessInput = {
  signupEnabled: boolean,
  openInvite: boolean,
  notifyAdmin: boolean,
  domains: Array< string >,
};

export type RenameSiteInput = {
  cloudName: string,
  cloudNamespace: string,
};

export type ActiveStatus =
  "ENABLED" |
  "DISABLED" |
  "BLOCKED";


export type AccessLevel =
  "NONE" |
  "USE" |
  "ADMIN" |
  "SYSADMIN";


export type GenerateUserExportInput = {
  includeGroups: boolean,
  includeProductAccess: boolean,
  includeInactiveUsers: boolean,
  selectedGroupIds: Array< string >,
};

export type SiteInviteUsersInput = {
  emails?: Array< string | null > | null,
  productIds?: Array< string | null > | null,
  additionalGroups?: Array< string | null > | null,
  sendNotification?: boolean | null,
  notificationText?: string | null,
  role?: string | null,
  atlOrigin?: string | null,
};

export type BillingUserQuery = {
  currentUser:  {
    __typename: string,
    id: string,
    email: string,
    organizations:  Array< {
      __typename: string,
      id: string,
      name: string,
    } >,
    sites:  Array< {
      __typename: string,
      id: string,
      name: string,
    } >,
  } | null,
};

export type BillingNavMounterQuery = {
  currentSite:  {
    __typename: string,
    id: string,
  },
};

export type ConnectedAccountQueryVariables = {
  cloudId: string,
  id: string,
};

export type ConnectedAccountQuery = {
  user:  {
    __typename: string,
    userDetails:  {
      __typename: string,
      id: string,
      email: string,
      displayName: string,
      active: boolean,
    },
  },
};

export type UpdateReferrerMutationVariables = {
  referrer: string,
};

export type UpdateReferrerMutation = {
  updateReferrer: boolean | null,
};

export type ClearReferrerMutation = {
  updateReferrer: boolean | null,
};

export type ScreenEventSenderQuery = {
  ui:  {
    __typename: string,
    analytics:  {
      __typename: string,
      referrer: string,
    },
  },
};

export type AsyncInlineGroupSelectQueryVariables = {
  cloudId: string,
  displayName?: string | null,
};

export type AsyncInlineGroupSelectQuery = {
  groupList:  {
    __typename: string,
    total: number,
    groups:  Array< {
      __typename: string,
      id: string,
      name: string,
      description: string | null,
      unmodifiable: boolean,
      managementAccess: ManagementAccess,
      ownerType: OwnerType | null,
      sitePrivilege: SitePrivilege,
    } >,
  },
  currentUser:  {
    __typename: string,
    id: string,
    isSystemAdmin: boolean | null,
  } | null,
};

export type DrawerCloseSubscriberQuery = {
  ui:  {
    __typename: string,
    navigation:  {
      __typename: string,
      activeDrawer: Drawer,
    },
  },
};

export type ShowDrawerMutationVariables = {
  drawer: string,
};

export type ShowDrawerMutation = {
  updateActiveDrawer: boolean | null,
};

export type OrgFeatureFlagsQueryVariables = {
  orgId: string,
  flagKey: string,
};

export type OrgFeatureFlagsQuery = {
  organization:  {
    __typename: string,
    id: string,
    flag:  {
      __typename: string,
      id: string,
      value: boolean | null,
    },
  },
};

export type SiteFeatureFlagsQueryVariables = {
  flagKey: string,
};

export type SiteFeatureFlagsQuery = {
  currentSite:  {
    __typename: string,
    id: string,
    flag:  {
      __typename: string,
      id: string,
      value: boolean | null,
    },
  },
};

export type UserFeatureFlagsQueryVariables = {
  flagKey: string,
};

export type UserFeatureFlagsQuery = {
  currentUser:  {
    __typename: string,
    id: string,
    flag:  {
      __typename: string,
      id: string,
      value: boolean | null,
    },
  } | null,
};

export type DissmissFeedbackMutationMutation = {
  updateIsFeedbackOpen: boolean | null,
};

export type FeedbackDialogIsOpenQuery = {
  ui:  {
    __typename: string,
    isFeedbackOpen: boolean,
  },
};

export type FeedbackDialogQuery = {
  currentUser:  {
    __typename: string,
    id: string,
    email: string,
  } | null,
};

export type UpdateIsFeedbackOpenMutation = {
  updateIsFeedbackOpen: boolean | null,
};

export type SubmitFeedbackMutationVariables = {
  comment: string,
  email: string,
  webInfo: string,
};

export type SubmitFeedbackMutation = {
  submitFeedback: boolean | null,
};

export type MutationModalGroupVariableMutationVariables = {
  name: string,
  cloudId?: string | null,
};

export type MutationModalGroupVariableMutation = {
  createGroup:  {
    __typename: string,
    id: string,
  },
};

export type MutationModalGroupMutation = {
  createGroup:  {
    __typename: string,
    id: string,
  },
};

export type MutationModalReferrerVariablesMutationVariables = {
  value: string,
};

export type MutationModalReferrerVariablesMutation = {
  updateReferrer: boolean | null,
};

export type MutationModalReferrerMutation = {
  updateReferrer: boolean | null,
};

export type DismissOnboardingMutationVariables = {
  id: string,
};

export type DismissOnboardingMutation = {
  dismissOnboarding:  {
    __typename: string,
    id: string,
    dismissed: boolean,
  },
};

export type OnboardingQueryVariables = {
  id: string,
};

export type OnboardingQuery = {
  onboarding:  {
    __typename: string,
    id: string,
    dismissed: boolean,
  },
};

export type SetContainerNavVisibilityMutationVariables = {
  show: boolean,
};

export type SetContainerNavVisibilityMutation = {
  updateNavigationContainer: boolean | null,
};

export type UserMenuQuery = {
  currentUser:  {
    __typename: string,
    id: string,
  } | null,
};

export type WithAccessCountsOrganizationsQuery = {
  currentUser:  {
    __typename: string,
    id: string,
    organizations:  Array< {
      __typename: string,
      id: string,
    } >,
  } | null,
};

export type WithAccessCountsSitesQuery = {
  currentUser:  {
    __typename: string,
    id: string,
    sites:  Array< {
      __typename: string,
      id: string,
    } >,
  } | null,
};

export type OrganizationsForDashboardRedirectQuery = {
  currentUser:  {
    __typename: string,
    id: string,
    organizations:  Array< {
      __typename: string,
      id: string,
      domainClaim:  {
        __typename: string,
        domains:  Array< {
          __typename: string,
          domain: string,
          verified: boolean,
        } > | null,
      },
    } >,
  } | null,
};

export type DashboardWithSitesRedirectQuery = {
  currentUser:  {
    __typename: string,
    id: string,
    organizations:  Array< {
      __typename: string,
      id: string,
      domainClaim:  {
        __typename: string,
        domains:  Array< {
          __typename: string,
          domain: string,
          verified: boolean,
        } > | null,
      },
      sites:  Array< {
        __typename: string,
        id: string,
      } >,
    } >,
    unlinkedSites:  Array< {
      __typename: string,
      id: string,
      siteUrl: string,
    } >,
  } | null,
};

export type OrganizationCardQueryVariables = {
  id: string,
  sitesFeatureFlagDisabled: boolean,
};

export type OrganizationCardQuery = {
  organization:  {
    __typename: string,
    id: string,
    users:  {
      __typename: string,
      memberTotal:  {
        __typename: string,
        total: number,
      } | null,
    },
    domainClaim:  {
      __typename: string,
      domains:  Array< {
        __typename: string,
        domain: string,
        verified: boolean,
      } > | null,
    },
    sites:  Array< {
      __typename: string,
      id: string,
      displayName: string | null,
      avatar: string | null,
      siteUrl: string,
      products:  Array< {
        __typename: string,
        key: string,
        name: string,
      } >,
    } >,
  },
  currentUser:  {
    __typename: string,
    id: string,
    unlinkedSites:  Array< {
      __typename: string,
      id: string,
    } >,
  } | null,
  products:  {
    __typename: string,
    id: string,
    identityManager: boolean,
  },
};

export type OrganizationsOverviewQuery = {
  currentUser:  {
    __typename: string,
    id: string,
    organizations:  Array< {
      __typename: string,
      id: string,
      name: string,
    } >,
  } | null,
};

export type UnlinkedSitesQueryVariables = {
  sitesFeatureFlagDisabled: boolean,
};

export type UnlinkedSitesQuery = {
  currentUser:  {
    __typename: string,
    id: string,
    unlinkedSites:  Array< {
      __typename: string,
      id: string,
      products:  Array< {
        __typename: string,
        key: string,
        name: string,
      } >,
      siteUrl: string,
    } >,
  } | null,
};

export type PageTemplateCurrentUserOrgsQuery = {
  currentUser:  {
    __typename: string,
    id: string,
    organizations:  Array< {
      __typename: string,
      id: string,
      name: string,
    } >,
  } | null,
};

export type TryIdentityManagerButtonMutationVariables = {
  orgId: string,
};

export type TryIdentityManagerButtonMutation = {
  evaluateIdentityManager:  {
    __typename: string,
    id: string,
    identityManager: boolean,
  },
};

export type NavigationHeaderQueryVariables = {
  id: string,
};

export type NavigationHeaderQuery = {
  organization:  {
    __typename: string,
    id: string,
    name: string,
  },
};

export type NavigationQuery = {
  ui:  {
    __typename: string,
    navigation:  {
      __typename: string,
      showContainer: boolean,
      activeDrawer: Drawer,
    },
  },
};

export type OrgBillingNavFeatureFlagQueryVariables = {
  id: string,
};

export type OrgBillingNavFeatureFlagQuery = {
  organization:  {
    __typename: string,
    id: string,
    isBillable: boolean,
  },
};

export type NestedNavigationQueryVariables = {
  cloudId: string,
};

export type NestedNavigationQuery = {
  site:  {
    __typename: string,
    groupsUseAccessConfig:  Array< {
      __typename: string,
      product:  {
        __typename: string,
        productId: string,
      },
    } >,
  },
};

export type SecurityLinkQuery = {
  currentUser:  {
    __typename: string,
    id: string,
    organizations:  Array< {
      __typename: string,
      id: string,
    } >,
  } | null,
};

export type AdminApiKeysQueryVariables = {
  id: string,
  scrollId?: string | null,
};

export type AdminApiKeysQuery = {
  organization:  {
    __typename: string,
    id: string,
    adminApiKeys:  {
      __typename: string,
      scrollId: string | null,
      tokens:  Array< {
        __typename: string,
        id: string,
        name: string,
        createdBy:  {
          __typename: string,
          id: string,
          displayName: string,
          primaryEmail: string | null,
        },
        creationDate: string,
        expirationDate: string,
      } >,
    },
  },
};

export type CreateAdminApiKeyMutationVariables = {
  orgId: string,
  name: string,
};

export type CreateAdminApiKeyMutation = {
  createAdminApiKey:  {
    __typename: string,
    id: string,
    name: string,
    token: string,
  },
};

export type RevokeAdminApiKeyMutationVariables = {
  orgId: string,
  apiKeyId: string,
};

export type RevokeAdminApiKeyMutation = {
  deleteAdminApiKey: boolean,
};

export type AddAdminMutationVariables = {
  orgId: string,
  email: string,
};

export type AddAdminMutation = {
  addOrganizationAdmin: boolean,
};

export type AdminQueryVariables = {
  id: string,
};

export type AdminQuery = {
  currentUser:  {
    __typename: string,
    id: string,
  } | null,
  organization:  {
    __typename: string,
    id: string,
    users:  {
      __typename: string,
      admins:  {
        __typename: string,
        id: string,
        total: number,
        users:  Array< {
          __typename: string,
          active: MemberStatus,
          id: string,
          displayName: string,
          emails:  Array< {
            __typename: string,
            primary: boolean,
            value: string,
          } >,
        } > | null,
      },
    },
  },
};

export type RemoveAdminMutationVariables = {
  orgId: string,
  adminId: string,
};

export type RemoveAdminMutation = {
  removeOrganizationAdmin: boolean,
};

export type OrgBreadcrumbQueryVariables = {
  id: string,
};

export type OrgBreadcrumbQuery = {
  organization:  {
    __typename: string,
    id: string,
    name: string,
  },
};

export type CreateOrgMutationVariables = {
  name: string,
};

export type CreateOrgMutation = {
  createOrganization:  {
    __typename: string,
    id: string,
    progressUri: string,
  },
};

export type ClaimDomainMutationVariables = {
  id: string,
  domain: string,
  method: VerificationType,
};

export type ClaimDomainMutation = {
  claimDomain:  {
    __typename: string,
    status: ClaimDomainStatusType,
  } | null,
};

export type DeleteDomainMutationVariables = {
  id: string,
  domain: string,
};

export type DeleteDomainMutation = {
  deleteDomain: boolean | null,
};

export type DomainCheckQueryVariables = {
  domain: string,
  skip: boolean,
};

export type DomainCheckQuery = {
  checkDomainClaimed:  {
    __typename: string,
    claimed: boolean,
  },
};

export type DomainClaimSuccessQueryVariables = {
  id: string,
  domain: string,
};

export type DomainClaimSuccessQuery = {
  organization:  {
    __typename: string,
    id: string,
    users:  {
      __typename: string,
      members:  {
        __typename: string,
        total: number,
      } | null,
    },
  },
};

export type DomainClaimQueryVariables = {
  id: string,
};

export type DomainClaimQuery = {
  organization:  {
    __typename: string,
    id: string,
    domainClaim:  {
      __typename: string,
      domains:  Array< {
        __typename: string,
        domain: string,
        status: string,
        verified: boolean,
      } > | null,
    },
  },
};

export type GetDomainsQueryVariables = {
  id: string,
};

export type GetDomainsQuery = {
  organization:  {
    __typename: string,
    id: string,
    domainClaim:  {
      __typename: string,
      domains:  Array< {
        __typename: string,
        domain: string,
        status: string,
        verificationType: VerificationType | null,
        verified: boolean,
      } > | null,
    },
  },
};

export type EvaluateIdentityManagerMutationVariables = {
  orgId: string,
};

export type EvaluateIdentityManagerMutation = {
  evaluateIdentityManager:  {
    __typename: string,
    id: string,
    identityManager: boolean,
  },
};

export type PollCreateOrgQueryVariables = {
  progressUri?: string | null,
};

export type PollCreateOrgQuery = {
  pollCreateOrg:  {
    __typename: string,
    completed: boolean,
    successful: boolean,
  } | null,
};

export type LinkedOrgLandingPageQueryVariables = {
  id: string,
};

export type LinkedOrgLandingPageQuery = {
  organization:  {
    __typename: string,
    id: string,
    name: string,
    users:  {
      __typename: string,
      memberTotal:  {
        __typename: string,
        total: number,
        disabledTotal: number,
      } | null,
    },
  },
};

export type ActivateMemberAccountMutationVariables = {
  memberId: string,
};

export type ActivateMemberAccountMutation = {
  activateMemberAccount: boolean | null,
};

export type CancelDeleteMemberAccountMutationVariables = {
  memberId: string,
};

export type CancelDeleteMemberAccountMutation = {
  cancelDeleteMemberAccount: boolean | null,
};

export type DeactivateMemberAccountMutationVariables = {
  memberId: string,
};

export type DeactivateMemberAccountMutation = {
  deactivateMemberAccount: boolean | null,
};

export type MemberAccessQueryVariables = {
  id: string,
  memberId: string,
};

export type MemberAccessQuery = {
  member:  {
    __typename: string,
    memberDetails:  {
      __typename: string,
      id: string,
      memberAccess:  {
        __typename: string,
        sites:  Array< {
          __typename: string,
          siteUrl: string,
          isSiteAdmin: boolean,
        } >,
        products:  Array< {
          __typename: string,
          siteUrl: string,
          productName: string,
          productKey: string,
        } >,
        errors: Array< string >,
      } | null,
    },
  },
};

export type CloseMemberAccountMutationVariables = {
  memberId: string,
};

export type CloseMemberAccountMutation = {
  closeMemberAccount: boolean | null,
};

export type MemberDetailsCanCloseQueryVariables = {
  id: string,
  memberId: string,
};

export type MemberDetailsCanCloseQuery = {
  member:  {
    __typename: string,
    canCloseAccount:  {
      __typename: string,
      errors:  Array< {
        __typename: string,
        message: string,
        code: string,
        link: string | null,
      } >,
      warnings:  Array< {
        __typename: string,
        message: string,
        code: string,
        link: string | null,
      } >,
    } | null,
  },
};

export type MemberDetailsQueryVariables = {
  id: string,
  memberId: string,
  memberTokenFeatureFlagDisabled: boolean,
  pendingAndDeactivatedInfoReadFromIOM: boolean,
};

export type MemberDetailsQuery = {
  currentUser:  {
    __typename: string,
    id: string,
  } | null,
  organization:  {
    __typename: string,
    id: string,
    users:  {
      __typename: string,
      mfaExemptMembers:  Array< {
        __typename: string,
        userId: string,
      } > | null,
    },
    security:  {
      __typename: string,
      twoStepVerification:  {
        __typename: string,
        dateEnforced: string | null,
      } | null,
    },
  },
  member:  {
    __typename: string,
    memberDetails:  {
      __typename: string,
      id: string,
      active: MemberStatus,
      displayName: string,
      jobTitle: string | null,
      useMfa: boolean | null,
      emails:  Array< {
        __typename: string,
        primary: boolean,
        value: string,
      } >,
      provisionedBy: MemberProvisioningSource | null,
      pendingInfo:  {
        __typename: string,
        startedBy: string,
        startedOn: string,
        initiatorName: string | null,
      } | null,
      deactivatedInfo:  {
        __typename: string,
        deactivatedBy: string,
        deactivatedOn: string,
        initiatorName: string | null,
      } | null,
    },
    canDeactivateAccount:  {
      __typename: string,
      errors:  Array< {
        __typename: string,
        message: string,
        code: string,
        link: string | null,
      } >,
      warnings:  Array< {
        __typename: string,
        message: string,
        code: string,
        link: string | null,
      } >,
    } | null,
    memberTokens:  Array< {
      __typename: string,
      id: string,
      label: string,
      createdAt: string,
      lastAccess: string | null,
    } >,
  },
  products:  {
    __typename: string,
    identityManager: boolean,
  },
};

export type ResetMemberPasswordMutationVariables = {
  id: string,
  member: string,
};

export type ResetMemberPasswordMutation = {
  resetMemberPassword: boolean,
};

export type UpdateMemberEnabledMutationVariables = {
  id: string,
  member: string,
  enabled: boolean,
};

export type UpdateMemberEnabledMutation = {
  updateMemberEnabled: boolean | null,
};

export type UpdateMemberMutationVariables = {
  id: string,
  member: string,
  displayName: string,
  emailAddress: string,
  jobTitle: string,
};

export type UpdateMemberMutation = {
  updateMember: boolean | null,
};

export type UpdateMfaEnrollmentMutationVariables = {
  input: UpdateMfaEnrollmentInput,
};

export type UpdateMfaEnrollmentMutation = {
  updateMfaEnrollment: boolean,
};

export type UpdateMfaExemptionMutationVariables = {
  input: UpdateMfaExemptionInput,
};

export type UpdateMfaExemptionMutation = {
  updateMfaExemption: boolean,
};

export type MemberExportDownloadPageQueryVariables = {
  id: string,
  exportId: string,
};

export type MemberExportDownloadPageQuery = {
  organization:  {
    __typename: string,
    id: string,
    memberExport:  {
      __typename: string,
      id: string,
      content: string,
    },
  },
};

export type MemberPageTableQueryVariables = {
  id: string,
  start?: number | null,
  count?: number | null,
  active?: boolean | null,
  mfa?: boolean | null,
  displayName?: string | null,
  domain?: string | null,
  filter?: string | null,
  products?: Array< string > | null,
  memberState?: MemberStateFilter | null,
};

export type MemberPageTableQuery = {
  organization:  {
    __typename: string,
    id: string,
    users:  {
      __typename: string,
      members:  {
        __typename: string,
        total: number,
        users:  Array< {
          __typename: string,
          active: MemberStatus,
          id: string,
          displayName: string,
          emails:  Array< {
            __typename: string,
            primary: boolean,
            value: string,
          } >,
          memberAccess:  {
            __typename: string,
            errors: Array< string >,
            products:  Array< {
              __typename: string,
              siteUrl: string,
              productName: string,
            } >,
          } | null,
        } > | null,
      } | null,
    },
  },
};

export type MemberPageDataQueryVariables = {
  id: string,
};

export type MemberPageDataQuery = {
  organization:  {
    __typename: string,
    id: string,
    users:  {
      __typename: string,
      memberTotal:  {
        __typename: string,
        total: number,
        disabledTotal: number,
      } | null,
    },
    domainClaim:  {
      __typename: string,
      domains:  Array< {
        __typename: string,
        domain: string,
        verified: boolean,
      } > | null,
    },
  },
};

export type MemberTableStableDataQueryVariables = {
  id: string,
  productFilteringFeatureDisabled: boolean,
};

export type MemberTableStableDataQuery = {
  organization:  {
    __typename: string,
    id: string,
    users:  {
      __typename: string,
      mfaExemptMembers:  Array< {
        __typename: string,
        userId: string,
      } > | null,
    },
    security:  {
      __typename: string,
      twoStepVerification:  {
        __typename: string,
        dateEnforced: string | null,
      } | null,
    },
    domainClaim:  {
      __typename: string,
      domains:  Array< {
        __typename: string,
        domain: string,
        verified: boolean,
      } > | null,
    },
    products:  Array< {
      __typename: string,
      key: string,
      name: string,
    } >,
  },
};

export type ExportMembersModalQuery = {
  currentUser:  {
    __typename: string,
    id: string,
    email: string,
  } | null,
};

export type ExportMembersMutationVariables = {
  orgId: string,
};

export type ExportMembersMutation = {
  initiateMemberCsvEmailExport: boolean,
};

export type NoOrgOnboardingPageQuery = {
  currentSite:  {
    __typename: string,
    id: string,
  },
};

export type RenameOrgMutationVariables = {
  id: string,
  name: string,
};

export type RenameOrgMutation = {
  renameOrganization:  {
    __typename: string,
    id: string,
    name: string,
  },
};

export type CurrentUserQuery = {
  currentUser:  {
    __typename: string,
    id: string,
    organizations:  Array< {
      __typename: string,
      id: string,
      name: string,
      migrationState: MigrationState | null,
    } >,
  } | null,
};

export type IdentityManagerPageQueryVariables = {
  id: string,
};

export type IdentityManagerPageQuery = {
  products:  {
    __typename: string,
    id: string,
    identityManager: boolean,
  },
  organization:  {
    __typename: string,
    id: string,
    domainClaim:  {
      __typename: string,
      domains:  Array< {
        __typename: string,
        verified: boolean,
      } > | null,
    },
  },
};

export type TwoStepVerificationEnforcementQueryVariables = {
  id: string,
};

export type TwoStepVerificationEnforcementQuery = {
  organization:  {
    __typename: string,
    id: string,
    security:  {
      __typename: string,
      twoStepVerification:  {
        __typename: string,
        dateEnforced: string | null,
      } | null,
    },
  },
};

export type PasswordPolicyQueryVariables = {
  id: string,
};

export type PasswordPolicyQuery = {
  organization:  {
    __typename: string,
    id: string,
    security:  {
      __typename: string,
      passwordPolicy:  {
        __typename: string,
        minimumStrength: string | null,
        expiryDays: number | null,
      } | null,
    },
    domainClaim:  {
      __typename: string,
      domains:  Array< {
        __typename: string,
        domain: string,
        status: string,
        verified: boolean,
      } > | null,
    },
  },
};

export type ProductsQueryVariables = {
  id: string,
};

export type ProductsQuery = {
  products:  {
    __typename: string,
    id: string,
    identityManager: boolean,
  },
};

export type ResetPasswordsMutationVariables = {
  id: string,
};

export type ResetPasswordsMutation = {
  resetPasswords: boolean | null,
};

export type UpdatePasswordPolicyMutationVariables = {
  input: UpdatePasswordPolicyInput,
};

export type UpdatePasswordPolicyMutation = {
  updatePasswordPolicy: boolean | null,
};

export type OrgRoutesQueryVariables = {
  id: string,
};

export type OrgRoutesQuery = {
  organization:  {
    __typename: string,
    id: string,
  },
};

export type DeleteSamlMutationVariables = {
  id: string,
};

export type DeleteSamlMutation = {
  deleteSaml: boolean,
};

export type SamlConfigurationQueryVariables = {
  id: string,
};

export type SamlConfigurationQuery = {
  organization:  {
    __typename: string,
    id: string,
    domainClaim:  {
      __typename: string,
      domains:  Array< {
        __typename: string,
        verified: boolean,
      } > | null,
    },
    security:  {
      __typename: string,
      saml:  {
        __typename: string,
        issuer: string,
        ssoUrl: string,
        publicCertificate: string,
        auth0MigrationState: Auth0State,
        serviceProvider:  {
          __typename: string,
          entityId: string,
          acsUrl: string,
        },
      } | null,
    },
  },
};

export type UpdateSamlMutationVariables = {
  input: UpdateSamlInput,
};

export type UpdateSamlMutation = {
  updateSaml: boolean,
};

export type OrgDataForLinkingQueryVariables = {
  orgId: string,
};

export type OrgDataForLinkingQuery = {
  currentUser:  {
    __typename: string,
    id: string,
    flag:  {
      __typename: string,
      id: string,
      value: boolean | null,
    },
  } | null,
  organization:  {
    __typename: string,
    id: string,
    name: string,
  },
};

export type OrgSiteAdminsQueryVariables = {
  orgId: string,
  cloudId: string,
  skip: boolean,
};

export type OrgSiteAdminsQuery = {
  currentUser:  {
    __typename: string,
    id: string,
  } | null,
  organization:  {
    __typename: string,
    id: string,
    siteAdmins:  Array< {
      __typename: string,
      id: string,
      email: string,
      displayName: string,
      isOrgAdmin: boolean,
    } >,
  },
};

export type LinkSiteToOrgMutationVariables = {
  orgId: string,
  cloudId: string,
};

export type LinkSiteToOrgMutation = {
  linkSiteToOrganization:  {
    __typename: string,
    isSuccess: boolean,
    errors: Array< string > | null,
  },
};

export type RefetchOrgSitesQueryVariables = {
  id: string,
};

export type RefetchOrgSitesQuery = {
  organization:  {
    __typename: string,
    id: string,
    sites:  Array< {
      __typename: string,
      id: string,
    } >,
  },
};

export type UserSitesQuery = {
  currentUser:  {
    __typename: string,
    id: string,
    unlinkedSites:  Array< {
      __typename: string,
      id: string,
      products:  Array< {
        __typename: string,
        key: string,
        name: string,
      } >,
      siteUrl: string,
    } >,
  } | null,
};

export type DisableTwoStepVerificationEnforcementMutationVariables = {
  id: string,
};

export type DisableTwoStepVerificationEnforcementMutation = {
  disableTwoStepVerificationEnforcement: boolean | null,
};

export type EnforceTwoStepVerificationMutationVariables = {
  id: string,
  date: string,
};

export type EnforceTwoStepVerificationMutation = {
  enableTwoStepVerificationEnforcement: boolean | null,
};

export type AuditLogQueryVariables = {
  id: string,
  start?: number | null,
  count?: number | null,
};

export type AuditLogQuery = {
  organization:  {
    __typename: string,
    id: string,
    externalDirectories:  Array< {
      __typename: string,
      id: string,
      logs:  {
        __typename: string,
        totalResults: number,
        startIndex: number,
        itemsPerPage: number,
        logEntries:  Array< {
          __typename: string,
          error: string,
          createdOn: string,
        } >,
      },
    } >,
  },
};

export type CreateDirectoryMutationVariables = {
  orgId: string,
  name: string,
};

export type CreateDirectoryMutation = {
  createDirectory:  {
    __typename: string,
    id: string,
    baseUrl: string,
    name: string,
    apiKey: string,
  },
};

export type DirectoryQueryVariables = {
  id: string,
};

export type DirectoryQuery = {
  organization:  {
    __typename: string,
    id: string,
    externalDirectories:  Array< {
      __typename: string,
      id: string,
      name: string,
      creationDate: string,
    } >,
  },
};

export type ProductAccessQueryVariables = {
  id: string,
};

export type ProductAccessQuery = {
  organization:  {
    __typename: string,
    id: string,
    sites:  Array< {
      __typename: string,
      id: string,
      siteUrl: string,
      products:  Array< {
        __typename: string,
        name: string,
      } >,
    } >,
  },
};

export type RegenerateDirectoryApiKeyMutationVariables = {
  directoryId: string,
};

export type RegenerateDirectoryApiKeyMutation = {
  regenerateDirectoryApiKey:  {
    __typename: string,
    apiKey: string,
  },
};

export type RegenerateApiKeyQueryVariables = {
  id: string,
};

export type RegenerateApiKeyQuery = {
  organization:  {
    __typename: string,
    id: string,
    externalDirectories:  Array< {
      __typename: string,
      id: string,
      name: string,
      creationDate: string,
      baseUrl: string,
    } >,
  },
};

export type RemoveDirectoryMutationVariables = {
  directoryId: string,
};

export type RemoveDirectoryMutation = {
  removeDirectory: boolean,
};

export type SyncedGroupsQueryVariables = {
  id: string,
  start?: number | null,
  count?: number | null,
};

export type SyncedGroupsQuery = {
  organization:  {
    __typename: string,
    id: string,
    externalDirectories:  Array< {
      __typename: string,
      id: string,
      groups:  {
        __typename: string,
        totalResults: number,
        startIndex: number,
        itemsPerPage: number,
        groups:  Array< {
          __typename: string,
          name: string,
          membershipCount: number,
        } >,
      },
    } >,
  },
};

export type UserProvisioningLandingPageQueryVariables = {
  id: string,
};

export type UserProvisioningLandingPageQuery = {
  products:  {
    __typename: string,
    id: string,
    identityManager: boolean,
  },
  organization:  {
    __typename: string,
    id: string,
    domainClaim:  {
      __typename: string,
      domains:  Array< {
        __typename: string,
        verified: boolean,
      } > | null,
    },
  },
};

export type UserProvisioningQueryVariables = {
  id: string,
};

export type UserProvisioningQuery = {
  organization:  {
    __typename: string,
    id: string,
    externalDirectories:  Array< {
      __typename: string,
      id: string,
      syncedUsers:  {
        __typename: string,
        countOfUsers: number,
      },
      groups:  {
        __typename: string,
        totalResults: number,
      },
    } >,
  },
};

export type OrgLinkedSitesQueryVariables = {
  id: string,
};

export type OrgLinkedSitesQuery = {
  organization:  {
    __typename: string,
    id: string,
    sites:  Array< {
      __typename: string,
      id: string,
      displayName: string | null,
      siteUrl: string,
      avatar: string | null,
      products:  Array< {
        __typename: string,
        key: string,
        name: string,
      } >,
    } >,
  },
};

export type AppSwitcherQuery = {
  currentSite:  {
    __typename: string,
    id: string,
    applications:  Array< {
      __typename: string,
      id: string,
      name: string,
      product: string,
      url: string,
      adminUrl: string,
    } > | null,
    defaultApps:  Array< {
      __typename: string,
      productId: string,
      productName: string,
    } >,
    settings:  {
      __typename: string,
      id: string,
      serviceDesk:  {
        __typename: string,
        enabled: boolean,
      },
      xFlow:  {
        __typename: string,
        enabled: boolean,
      },
    },
  },
};

export type UpdateXFlowDialogMutationVariables = {
  sourceComponent?: string | null,
  targetProduct?: string | null,
};

export type UpdateXFlowDialogMutation = {
  updateXFlowDialog: boolean | null,
};

export type CsatSurveyQueryVariables = {
  cloudId: string,
  flagKey: string,
  onboardingKey: string,
};

export type CsatSurveyQuery = {
  currentUser:  {
    __typename: string,
    id: string,
  } | null,
  site:  {
    __typename: string,
    id: string,
    flag:  {
      __typename: string,
      id: string,
      value: boolean | null,
    },
  },
  onboarding:  {
    __typename: string,
    id: string,
    dismissed: boolean,
  },
};

export type EmojiPageQuery = {
  currentSite:  {
    __typename: string,
    id: string,
  },
};

export type EmojiSettingQuery = {
  currentSite:  {
    __typename: string,
    id: string,
    settings:  {
      __typename: string,
      id: string,
      emoji:  {
        __typename: string,
        id: string,
        uploadEnabled: boolean,
      },
    },
  },
};

export type FlagsQuery = {
  currentSite:  {
    __typename: string,
    id: string,
  },
};

export type MembersListQueryVariables = {
  id: string,
  aaId: Array< string | null >,
};

export type MembersListQuery = {
  membersList:  Array< {
    __typename: string,
    id: string,
    fullName: string,
    avatarUrl: string,
  } >,
};

export type UpdateEmojiSettingsMutationVariables = {
  input: EmojiSettingsInput,
  id: string,
};

export type UpdateEmojiSettingsMutation = {
  updateEmojiSettings:  {
    __typename: string,
    uploadEnabled: boolean,
    id: string,
  },
};

export type CurrentSiteApplicationsQuery = {
  currentSite:  {
    __typename: string,
    id: string,
    applications:  Array< {
      __typename: string,
      id: string,
      name: string,
      product: string,
      url: string,
      adminUrl: string,
    } > | null,
  },
};

export type JsdSettingsQuery = {
  currentSite:  {
    __typename: string,
    id: string,
    flags:  {
      __typename: string,
      jsdADG3Migration: boolean,
    },
    settings:  {
      __typename: string,
      id: string,
      serviceDesk:  {
        __typename: string,
        enabled: boolean,
      },
    },
  },
};

export type NavigationV1Query = {
  currentSite:  {
    __typename: string,
    id: string,
  },
};

export type GroupDetailRedirectQueryVariables = {
  cloudId: string,
  displayName: string,
};

export type GroupDetailRedirectQuery = {
  groupList:  {
    __typename: string,
    groups:  Array< {
      __typename: string,
      id: string,
      name: string,
    } >,
  },
};

export type PageRedirectQuery = {
  currentSite:  {
    __typename: string,
    id: string,
    flags:  {
      __typename: string,
      selfSignupADG3Migration: boolean,
      accessConfigADG3Migration: boolean,
      groupsADG3Migration: boolean,
      gSuiteADG3Migration: boolean,
      usersADG3Migration: boolean,
      userExportADG3Migration: boolean,
      jsdADG3Migration: boolean,
      siteAdminPageMigrationRedirect: boolean,
    },
  },
};

export type UserDetailRedirectQueryVariables = {
  cloudId: string,
  identifier: string,
};

export type UserDetailRedirectQuery = {
  siteUsersList:  {
    __typename: string,
    total: number,
    users:  Array< {
      __typename: string,
      id: string,
    } >,
  },
};

export type CurrentUserSitesQuery = {
  currentUser:  {
    __typename: string,
    id: string,
    sites:  Array< {
      __typename: string,
      id: string,
    } >,
  } | null,
};

export type CancelStrideExportMutationVariables = {
  cloudId: string,
  exportId: string,
};

export type CancelStrideExportMutation = {
  cancelStrideExport: boolean,
};

export type CreateStrideExportMutationVariables = {
  cloudId: string,
  input: CreateStrideExportInput,
};

export type CreateStrideExportMutation = {
  createStrideExport:  {
    __typename: string,
    id: string,
    startedAt: string,
    expiresAt: string | null,
    status: StrideExportStatus,
    downloadLocation: string | null,
  },
};

export type DeleteStrideExportMutationVariables = {
  cloudId: string,
  exportId: string,
};

export type DeleteStrideExportMutation = {
  deleteStrideExport: boolean,
};

export type StrideExportDetailQueryVariables = {
  cloudId: string,
  exportId: string,
};

export type StrideExportDetailQuery = {
  stride:  {
    __typename: string,
    strideExport:  {
      __typename: string,
      id: string,
      startedAt: string,
      expiresAt: string | null,
      creatorName: string,
      creatorEmail: string,
      status: StrideExportStatus,
      downloadLocation: string | null,
      integrationsLocation: string | null,
      settings:  {
        __typename: string,
        startDate: string | null,
        endDate: string | null,
        includePublicRooms: boolean,
        includePrivateRooms: boolean,
        includeDirects: boolean,
      },
    },
  },
};

export type StrideExportsQueryVariables = {
  cloudId: string,
};

export type StrideExportsQuery = {
  stride:  {
    __typename: string,
    strideExports:  Array< {
      __typename: string,
      id: string,
      startedAt: string,
      expiresAt: string | null,
      status: StrideExportStatus,
      downloadLocation: string | null,
    } >,
  },
};

export type UserConnectedAppsQueryVariables = {
  id: string,
};

export type UserConnectedAppsQuery = {
  site:  {
    __typename: string,
    id: string,
    userConnectedApps:  Array< {
      __typename: string,
      id: string,
      name: string,
      vendorName: string | null,
      description: string,
      avatarUrl: string | null,
      scopes:  Array< {
        __typename: string,
        id: string,
        untranslatedName: string,
        untranslatedDescription: string,
      } >,
      userGrants:  Array< {
        __typename: string,
        id: string,
        accountId: string,
        createdAt: string,
        scopes:  Array< {
          __typename: string,
          id: string,
          untranslatedName: string,
          untranslatedDescription: string,
        } >,
      } >,
    } >,
  },
};

export type AddGroupsAccessToProductMutationVariables = {
  id: string,
  input: AddGroupsAccessToProductInput,
};

export type AddGroupsAccessToProductMutation = {
  addGroupsAccessToProduct: boolean | null,
};

export type ApproveImportedGroupMutationVariables = {
  id: string,
  input: GroupImportInput,
};

export type ApproveImportedGroupMutation = {
  approveImportedGroup: boolean | null,
};

export type DefaultProductsQuery = {
  currentSite:  {
    __typename: string,
    id: string,
    defaultApps:  Array< {
      __typename: string,
      productId: string,
      productName: string,
    } >,
  },
};

export type SetDefaultProductsMutationVariables = {
  id: string,
  input: DefaultProductsInput,
};

export type SetDefaultProductsMutation = {
  setDefaultProducts: boolean | null,
};

export type RejectImportedGroupMutationVariables = {
  id: string,
  input: GroupImportInput,
};

export type RejectImportedGroupMutation = {
  rejectImportedGroup: boolean | null,
};

export type RemoveGroupMutationVariables = {
  id: string,
  input: RemoveGroupAccessToProductInput,
};

export type RemoveGroupMutation = {
  removeGroupAccessToProduct: boolean | null,
};

export type SetDefaultGroupMutationVariables = {
  id: string,
  input: DefaultGroupsInput,
};

export type SetDefaultGroupMutation = {
  setDefaultGroup: boolean | null,
};

export type AccessConfigQuery = {
  currentSite:  {
    __typename: string,
    id: string,
    groupsUseAccessConfig:  Array< {
      __typename: string,
      product:  {
        __typename: string,
        productId: string,
        productName: string,
      },
      groups:  Array< {
        __typename: string,
        id: string,
        name: string,
        default: boolean,
        productPermission: Array< ProductPermission | null >,
        requiresApproval: boolean,
      } >,
    } >,
    groupsAdminAccessConfig:  Array< {
      __typename: string,
      product:  {
        __typename: string,
        productId: string,
        productName: string,
      },
      groups:  Array< {
        __typename: string,
        id: string,
        name: string,
        default: boolean,
        productPermission: Array< ProductPermission | null >,
        requiresApproval: boolean,
      } >,
    } >,
  },
};

export type AccessRequestsQueryVariables = {
  id: string,
  input: AccessRequestListInput,
};

export type AccessRequestsQuery = {
  accessRequestList:  {
    __typename: string,
    total: number,
    accessRequests:  Array< {
      __typename: string,
      user:  {
        __typename: string,
        id: string,
        email: string,
        displayName: string,
        active: boolean,
        system: boolean,
        hasVerifiedEmail: boolean,
      },
      requestedProducts:  Array< {
        __typename: string,
        productId: string,
        productName: string,
        status: AccessRequestStatus,
        requestedTime: string,
        subProducts:  Array< {
          __typename: string,
          productId: string,
          productName: string,
        } > | null,
      } >,
      type: AccessRequestType,
      requester:  {
        __typename: string,
        id: string,
        email: string,
        displayName: string,
        active: boolean,
        system: boolean,
        hasVerifiedEmail: boolean,
      } | null,
    } >,
  },
};

export type ApproveAccessMutationVariables = {
  cloudId: string,
  input: ApproveAccessInput,
};

export type ApproveAccessMutation = {
  approveAccess: boolean | null,
};

export type DenyAccessMutationVariables = {
  cloudId: string,
  input: DenyAccessInput,
};

export type DenyAccessMutation = {
  denyAccess: boolean | null,
};

export type GroupsPickerModalQueryVariables = {
  cloudId: string,
  start?: number | null,
  count?: number | null,
  displayName?: string | null,
};

export type GroupsPickerModalQuery = {
  groupList:  {
    __typename: string,
    total: number,
    groups:  Array< {
      __typename: string,
      id: string,
      name: string,
      description: string | null,
      managementAccess: ManagementAccess,
      ownerType: OwnerType | null,
      unmodifiable: boolean,
      sitePrivilege: SitePrivilege,
    } >,
  },
  currentUser:  {
    __typename: string,
    id: string,
    isSystemAdmin: boolean | null,
  } | null,
};

export type GSuitePageQueryVariables = {
  cloudId: string,
};

export type GSuitePageQuery = {
  site:  {
    __typename: string,
    id: string,
    gSuite:  {
      __typename: string,
      state:  {
        __typename: string,
        syncConfig: Array< string | null >,
        syncStartTime: string | null,
        nextSyncStartTime: string | null,
        lastSync:  {
          __typename: string,
          startTime: string,
          failed: number,
          errors: Array< string | null >,
          updated: number,
          hasReport: boolean,
          status: GSuiteLastSyncStatus,
          finishTime: string,
          numOfSyncedUsers: number,
          deleted: number,
          created: number,
        } | null,
        selectedGroups: Array< string | null >,
        noSyncs: number,
        user: string,
      } | null,
      groups:  Array< {
        __typename: string,
        id: string,
        name: string,
      } > | null,
    },
  },
};

export type GetGSuiteConnectionMutationVariables = {
  cloudId: string,
};

export type GetGSuiteConnectionMutation = {
  getConnection:  {
    __typename: string,
    url: string,
  },
};

export type AddMembersToGroupsMutationVariables = {
  cloudId: string,
  input: AddUsersToGroupsInput,
};

export type AddMembersToGroupsMutation = {
  addUsersToGroups: boolean,
};

export type AddMembersDialogQueryVariables = {
  cloudId: string,
  input: SiteUsersListInput,
};

export type AddMembersDialogQuery = {
  siteUsersList:  {
    __typename: string,
    total: number,
    users:  Array< {
      __typename: string,
      displayName: string,
      email: string,
      id: string,
      system: boolean,
    } >,
  },
};

export type CreateGroupMutationVariables = {
  cloudId: string,
  input: UpdateGroupInput,
};

export type CreateGroupMutation = {
  createGroup:  {
    __typename: string,
    description: string | null,
    id: string,
    name: string,
  },
};

export type IsCurrentAdminSystemAdminQueryVariables = {
  cloudId: string,
};

export type IsCurrentAdminSystemAdminQuery = {
  currentUser:  {
    __typename: string,
    id: string,
    isSystemAdmin: boolean | null,
  } | null,
};

export type DeleteGroupMutationVariables = {
  cloudId: string,
  id: string,
};

export type DeleteGroupMutation = {
  deleteGroup: boolean,
};

export type DeleteMemberMutationVariables = {
  cloudId: string,
  userId: string,
  groupId: string,
};

export type DeleteMemberMutation = {
  deleteUserFromGroup: boolean,
};

export type GroupDetailsQueryVariables = {
  cloudId: string,
  id: string,
  start?: number | null,
  count?: number | null,
};

export type GroupDetailsQuery = {
  group:  {
    __typename: string,
    groupDetails:  {
      __typename: string,
      description: string | null,
      id: string,
      name: string,
      productPermissions:  Array< {
        __typename: string,
        productName: string,
        productId: string,
        permissions: Array< Permission | null >,
      } >,
      defaultForProducts:  Array< {
        __typename: string,
        productName: string,
        productId: string,
      } >,
      sitePrivilege: SitePrivilege,
      unmodifiable: boolean,
      managementAccess: ManagementAccess,
      ownerType: OwnerType | null,
    },
    members:  {
      __typename: string,
      total: number,
      users:  Array< {
        __typename: string,
        active: boolean,
        id: string,
        displayName: string,
        email: string,
        system: boolean,
      } >,
    },
  },
};

export type GroupMemberListQuery = {
  currentUser:  {
    __typename: string,
    id: string,
  } | null,
};

export type GroupsPageQueryVariables = {
  cloudId: string,
  start?: number | null,
  count?: number | null,
  displayName?: string | null,
};

export type GroupsPageQuery = {
  groupList:  {
    __typename: string,
    total: number,
    groups:  Array< {
      __typename: string,
      id: string,
      name: string,
      description: string | null,
      productPermissions:  Array< {
        __typename: string,
        productName: string,
        productId: string,
        permissions: Array< Permission | null >,
      } >,
      defaultForProducts:  Array< {
        __typename: string,
        productName: string,
        productId: string,
      } >,
      sitePrivilege: SitePrivilege,
      unmodifiable: boolean,
      userTotal: number,
      managementAccess: ManagementAccess,
      ownerType: OwnerType | null,
    } >,
  },
  groupListWithoutFilter:  {
    __typename: string,
    total: number,
  },
};

export type UpdateGroupMutationVariables = {
  cloudId: string,
  id: string,
  input: UpdateGroupInput,
};

export type UpdateGroupMutation = {
  updateGroup:  {
    __typename: string,
    description: string | null,
    id: string,
    name: string,
  },
};

export type JsdExportQueryVariables = {
  cloudId: string,
  errorRedirectUrl: string,
};

export type JsdExportQuery = {
  site:  {
    __typename: string,
    id: string,
    jsd:  {
      __typename: string,
      directory:  {
        __typename: string,
        id: string,
        customerExport: string,
      },
    },
  },
};

export type JsdTableQueryVariables = {
  cloudId: string,
  start: number,
  count: number,
  displayName?: string | null,
  activeStatus?: string | null,
};

export type JsdTableQuery = {
  site:  {
    __typename: string,
    jsd:  {
      __typename: string,
      directory:  {
        __typename: string,
        id: string,
      },
      customers:  {
        __typename: string,
        hasMoreResults: boolean,
        customers:  Array< {
          __typename: string,
          id: string,
          username: string,
          email: string,
          active: boolean,
          displayName: string,
          lastLogin: string | null,
        } >,
      },
    },
  },
};

export type JsdChangePasswordModalMutationVariables = {
  directoryId: string,
  accountId: string,
  password: string,
};

export type JsdChangePasswordModalMutation = {
  updateJsdCustomerPassword: boolean | null,
};

export type JsdDeleteModalMutationVariables = {
  directoryId: string,
  accountId: string,
};

export type JsdDeleteModalMutation = {
  deleteJsdCustomer: boolean | null,
};

export type JsdEditNameModalMutationVariables = {
  directoryId: string,
  accountId: string,
  name: string,
};

export type JsdEditNameModalMutation = {
  updateJsdCustomerName: boolean | null,
};

export type JsdGrantRevokeModalMutationVariables = {
  directoryId: string,
  accountId: string,
  access: boolean,
};

export type JsdGrantRevokeModalMutation = {
  updateJsdCustomerAccess: boolean | null,
};

export type JsdMigrateAccountModalMutationVariables = {
  cloudId: string,
  accountId: string,
};

export type JsdMigrateAccountModalMutation = {
  migrateJsdCustomer: boolean | null,
};

export type DeleteInviteUrlMutationVariables = {
  id: string,
  input: DeleteInviteUrlInput,
};

export type DeleteInviteUrlMutation = {
  deleteInviteUrl: boolean | null,
};

export type GenerateInviteUrlMutationVariables = {
  id: string,
  input: GenerateInviteUrlInput,
};

export type GenerateInviteUrlMutation = {
  generateInviteUrl: boolean | null,
};

export type SiteAccessQuery = {
  currentSite:  {
    __typename: string,
    id: string,
    siteAccess:  {
      __typename: string,
      signupEnabled: boolean,
      openInvite: boolean,
      notifyAdmin: boolean,
      domains: Array< string >,
    },
    defaultApps:  Array< {
      __typename: string,
      productName: string,
      canonicalProductKey: string,
    } >,
    inviteUrls:  Array< {
      __typename: string,
      url: string,
      productKey: string,
      productName: string,
      expiration: string,
    } >,
    flags:  {
      __typename: string,
      inviteUrls: boolean,
    },
  },
};

export type UpdateSiteAccessMutationVariables = {
  id: string,
  input: UpdateSiteAccessInput,
};

export type UpdateSiteAccessMutation = {
  updateSiteAccess: boolean,
};

export type FormConfirmationModalMutationVariables = {
  id: string,
  input: RenameSiteInput,
};

export type FormConfirmationModalMutation = {
  renameSite:  {
    __typename: string,
    progressUri: string,
  },
};

export type SiteUrlPageNameCheckMutationVariables = {
  id: string,
  siteName: string,
};

export type SiteUrlPageNameCheckMutation = {
  checkSiteNameAvailability:  {
    __typename: string,
    isAlreadyOwner: boolean,
    result: boolean,
    taken: boolean,
    renameLimitReached: boolean,
  },
};

export type SiteUrlPageQuery = {
  currentSite:  {
    __typename: string,
    id: string,
    siteRenameStatus:  {
      __typename: string,
      id: string,
      cloudName: string,
      cloudNamespace: string,
      renamesUsed: number,
      renameLimit: number,
    },
  },
};

export type UserManagementMigrationQuery = {
  currentSite:  {
    __typename: string,
    id: string,
    flags:  {
      __typename: string,
      selfSignupADG3Migration: boolean,
      accessConfigADG3Migration: boolean,
      groupsADG3Migration: boolean,
      gSuiteADG3Migration: boolean,
      usersADG3Migration: boolean,
      accessRequestsPage: boolean,
      userConnectedAppsPage: boolean,
      siteUrlPage: boolean,
    },
  },
};

export type GetUserProductAccessQueryVariables = {
  cloudId: string,
  userId: string,
};

export type GetUserProductAccessQuery = {
  user:  {
    __typename: string,
    userDetails:  {
      __typename: string,
      id: string,
      displayName: string,
      presence: string | null,
      active: boolean,
      activeStatus: ActiveStatus | null,
      system: boolean,
    },
    productAccess:  Array< {
      __typename: string,
      productId: string,
      productName: string,
      presence: string | null,
      accessLevel: AccessLevel,
    } >,
  },
};

export type ActivateUserMutationVariables = {
  cloudId: string,
  userId: string,
};

export type ActivateUserMutation = {
  activateUser: boolean | null,
};

export type DeactivateUserMutationVariables = {
  cloudId: string,
  userId: string,
};

export type DeactivateUserMutation = {
  deactivateUser: boolean | null,
};

export type UserDetailsGrantProductAccessMutationVariables = {
  cloudId: string,
  users: Array< string >,
  productIds: Array< string >,
};

export type UserDetailsGrantProductAccessMutation = {
  grantAccessToProducts: boolean | null,
};

export type UserDetailsReinviteUserMutationVariables = {
  cloudId: string,
  userId: string,
};

export type UserDetailsReinviteUserMutation = {
  reinviteUser: boolean | null,
};

export type RevokeProductAccessMutationVariables = {
  cloudId: string,
  users: Array< string >,
  productIds: Array< string >,
};

export type RevokeProductAccessMutation = {
  revokeAccessToProducts: boolean | null,
};

export type UserDetailsAccountSectionQueryVariables = {
  cloudId: string,
  id: string,
  isAdminHub: boolean,
};

export type UserDetailsAccountSectionQuery = {
  user:  {
    __typename: string,
    userDetails:  {
      __typename: string,
      id: string,
      email: string,
      displayName: string,
      active: boolean,
      activeStatus: ActiveStatus | null,
      nickname: string | null,
      location: string | null,
      companyName: string | null,
      department: string | null,
      title: string | null,
      timezone: string | null,
      system: boolean,
      presence: string | null,
    },
    managedStatus:  {
      __typename: string,
      managed: boolean,
    } | null,
  },
  site:  {
    __typename: string,
    context:  {
      __typename: string,
      firstActivationDate: string,
    } | null,
  },
};

export type UserDetailsSuggestChangesMutationVariables = {
  cloudId: string,
  userId: string,
  email: string,
  name: string,
};

export type UserDetailsSuggestChangesMutation = {
  suggestChanges: boolean | null,
};

export type UserGroupsQueryVariables = {
  cloudId: string,
  userId: string,
  start: number,
  count: number,
};

export type UserGroupsQuery = {
  user:  {
    __typename: string,
    groups:  {
      __typename: string,
      total: number,
      groups:  Array< {
        __typename: string,
        id: string,
        name: string,
        description: string | null,
        productPermissions:  Array< {
          __typename: string,
          productId: string,
          productName: string,
          permissions: Array< Permission | null >,
        } >,
        defaultForProducts:  Array< {
          __typename: string,
          productId: string,
          productName: string,
        } >,
        sitePrivilege: SitePrivilege,
        unmodifiable: boolean,
        userTotal: number,
        managementAccess: ManagementAccess,
        ownerType: OwnerType | null,
      } >,
    },
  },
};

export type RemoveUserFromGroupMutationVariables = {
  cloudId: string,
  userId: string,
  groupId: string,
};

export type RemoveUserFromGroupMutation = {
  deleteUserFromGroup: boolean,
};

export type UserDetailsImpersonateUserMutationVariables = {
  cloudId: string,
  userId: string,
};

export type UserDetailsImpersonateUserMutation = {
  impersonateUser: boolean | null,
};

export type RemoveUserFromSiteMutationVariables = {
  cloudId: string,
  id: string,
};

export type RemoveUserFromSiteMutation = {
  removeUserFromSite: boolean | null,
};

export type UserDetailsPromptResetPasswordMutationVariables = {
  cloudId: string,
  userId: string,
};

export type UserDetailsPromptResetPasswordMutation = {
  promptResetPassword: boolean | null,
};

export type UserDetailsPageQueryVariables = {
  cloudId: string,
  userId: string,
  start: number,
  count: number,
};

export type UserDetailsPageQuery = {
  user:  {
    __typename: string,
    userDetails:  {
      __typename: string,
      id: string,
      system: boolean,
      displayName: string,
      active: boolean,
      activeStatus: ActiveStatus | null,
      presence: string | null,
      trustedUser: boolean | null,
      siteAdmin: boolean | null,
    },
    groups:  {
      __typename: string,
      groups:  Array< {
        __typename: string,
        id: string,
        sitePrivilege: SitePrivilege,
      } >,
      total: number,
    },
  },
  currentUser:  {
    __typename: string,
    id: string,
  } | null,
};

export type AddUsersToGroupsMutationVariables = {
  cloudId: string,
  input: AddUsersToGroupsInput,
};

export type AddUsersToGroupsMutation = {
  addUsersToGroups: boolean,
};

export type ChangeUserRoleMutationVariables = {
  cloudId: string,
  userId: string,
  roleId: string,
};

export type ChangeUserRoleMutation = {
  changeUserRole: boolean | null,
};

export type GroupListQueryVariables = {
  cloudId: string,
  start?: number | null,
  count?: number | null,
  displayName?: string | null,
};

export type GroupListQuery = {
  groupList:  {
    __typename: string,
    groups:  Array< {
      __typename: string,
      id: string,
      name: string,
    } >,
  },
};

export type UserPermissionsQueryVariables = {
  cloudId: string,
  userId: string,
};

export type UserPermissionsQuery = {
  user:  {
    __typename: string,
    userDetails:  {
      __typename: string,
      id: string,
      active: boolean,
      activeStatus: ActiveStatus | null,
      system: boolean,
      sysAdmin: boolean | null,
      siteAdmin: boolean | null,
      trustedUser: boolean | null,
    },
  },
};

export type GenerateUserExportMutationVariables = {
  cloudId: string,
  input: GenerateUserExportInput,
};

export type GenerateUserExportMutation = {
  userExport: string,
};

export type UserExportQueryVariables = {
  cloudId: string,
};

export type UserExportQuery = {
  groupList:  {
    __typename: string,
    total: number,
    groups:  Array< {
      __typename: string,
      id: string,
      name: string,
    } >,
  },
  siteUsersList:  {
    __typename: string,
    total: number,
  },
};

export type UserInviteDrawerInviteMutationVariables = {
  id: string,
  input: SiteInviteUsersInput,
};

export type UserInviteDrawerInviteMutation = {
  inviteUsers: boolean | null,
};

export type UserInviteDrawerUpdateSiteAccessMutationVariables = {
  id: string,
  input: UpdateSiteAccessInput,
};

export type UserInviteDrawerUpdateSiteAccessMutation = {
  updateSiteAccess: boolean,
};

export type UserInviteDrawerQuery = {
  currentSite:  {
    __typename: string,
    id: string,
    groupsUseAccessConfig:  Array< {
      __typename: string,
      product:  {
        __typename: string,
        productId: string,
        productName: string,
      },
    } >,
    defaultApps:  Array< {
      __typename: string,
      productId: string,
    } >,
    siteAccess:  {
      __typename: string,
      signupEnabled: boolean,
      domains: Array< string >,
    },
  },
};

export type UsersListAdminProductsQuery = {
  currentSite:  {
    __typename: string,
    id: string,
    groupsAdminAccessConfig:  Array< {
      __typename: string,
      product:  {
        __typename: string,
        productId: string,
        productName: string,
      },
    } >,
  },
};

export type UsersListProductsQuery = {
  currentSite:  {
    __typename: string,
    id: string,
    groupsUseAccessConfig:  Array< {
      __typename: string,
      product:  {
        __typename: string,
        productId: string,
        productName: string,
      },
    } >,
  },
};

export type UserPageReinviteUserMutationVariables = {
  cloudId: string,
  userId: string,
};

export type UserPageReinviteUserMutation = {
  reinviteUser: boolean | null,
};

export type SiteUsersListQueryVariables = {
  cloudId: string,
  input: SiteUsersListInput,
};

export type SiteUsersListQuery = {
  siteUsersList:  {
    __typename: string,
    total: number,
    users:  Array< {
      __typename: string,
      active: boolean,
      displayName: string,
      email: string,
      id: string,
      activeStatus: ActiveStatus | null,
      system: boolean,
      siteAdmin: boolean | null,
      presence: string | null,
      hasVerifiedEmail: boolean,
      trustedUser: boolean | null,
    } >,
  },
  currentSite:  {
    __typename: string,
    id: string,
    flags:  {
      __typename: string,
      userExportADG3Migration: boolean,
    },
  },
};

export type UsersShowAccessModalGrantAccessMutationVariables = {
  cloudId: string,
  users: Array< string >,
  productIds: Array< string >,
};

export type UsersShowAccessModalGrantAccessMutation = {
  grantAccessToProducts: boolean | null,
};

export type UsersShowAccessModalRevokeAccessMutationVariables = {
  cloudId: string,
  users: Array< string >,
  productIds: Array< string >,
};

export type UsersShowAccessModalRevokeAccessMutation = {
  revokeAccessToProducts: boolean | null,
};

export type UsersShowAccessModalQueryVariables = {
  cloudId: string,
  userId: string,
  start: number,
  count: number,
};

export type UsersShowAccessModalQuery = {
  user:  {
    __typename: string,
    productAccess:  Array< {
      __typename: string,
      productId: string,
      productName: string,
      accessLevel: AccessLevel,
    } >,
    groups:  {
      __typename: string,
      groups:  Array< {
        __typename: string,
        id: string,
        sitePrivilege: SitePrivilege,
      } >,
    },
  },
  currentUser:  {
    __typename: string,
    id: string,
  } | null,
};

export type InviteUsersMutationVariables = {
  id: string,
  input: SiteInviteUsersInput,
};

export type InviteUsersMutation = {
  inviteUsers: boolean | null,
};

export type RolePermissionsQueryVariables = {
  cloudId: string,
};

export type RolePermissionsQuery = {
  rolePermissions:  {
    __typename: string,
    permissionIds: Array< string | null >,
  },
};

export type CloseXFlowDialogMutation = {
  updateXFlowDialog: boolean | null,
};

export type XFlowDialogQuery = {
  ui:  {
    __typename: string,
    xflow:  {
      __typename: string,
      isDialogOpen: boolean,
      sourceComponent: string | null,
      targetProduct: string | null,
    },
  },
};

export type XFlowQuery = {
  currentSite:  {
    __typename: string,
    id: string,
  },
};
/* tslint:enable */
