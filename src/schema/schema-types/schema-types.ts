/* tslint:disable */

export type UnixTimestamp = any;

export interface Query {
  _?: boolean | null;
  accessRequestList: AccessRequestList;
  currentUser?: CurrentUser | null;
  checkDomainClaimed: CheckDomainClaimed;
  userFeatureFlag?: FeatureFlag | null;
  group: GroupDetails;
  groupList: PaginatedGroups;
  member: MemberDetails;
  membersList: (MemberProfileMinimal | null)[];
  onboarding: Onboarding;
  products: OrgProducts;
  organization: Organization;
  pollCreateOrg?: PollCreateOrgState | null;
  rolePermissions: RolePermissions;
  site: Site;
  currentSite: Site;
  stride: Stride;
  ui: UI;
  user: UserDetails;
  siteUsersList: PaginatedUsers;
}

export interface AccessRequestList {
  total: number;
  accessRequests: AccessRequest[];
}

export interface AccessRequest {
  user: User;
  requestedProducts: RequestedProduct[];
  type: AccessRequestType;
  requester?: User | null;
}

export interface User {
  id: string;
  email: string;
  displayName: string;
  active: boolean;
  nickname?: string | null;
  location?: string | null;
  companyName?: string | null;
  department?: string | null;
  title?: string | null;
  timezone?: string | null;
  system: boolean;
  presence?: string | null;
  activeStatus?: ActiveStatus | null;
  siteAdmin?: boolean | null;
  sysAdmin?: boolean | null;
  hasVerifiedEmail: boolean;
  trustedUser?: boolean | null;
}

export interface RequestedProduct {
  productId: string;
  productName: string;
  status: AccessRequestStatus;
  requestedTime: string;
  subProducts?: SubProduct[] | null;
}

export interface SubProduct {
  productId: string;
  productName: string;
}

export interface CurrentUser {
  id: string;
  organizations: (Organization | null)[];
  sites: (Site | null)[];
  unlinkedSites: OrgSite[];
  email: string;
  isSiteAdmin?: boolean | null;
  isSystemAdmin?: boolean | null;
  flag: FeatureFlag;
}

export interface Organization {
  id: string;
  name: string;
  users: OrganizationUsers;
  security: OrganizationSecurity;
  domainClaim: DomainClaim;
  migrationState?: MigrationState | null;
  flag: FeatureFlag;
  isBillable: boolean;
  externalDirectories: (ExternalDirectory | null)[];
  siteAdmins: SiteAdmin[];
  memberExport: MemberExport;
  sites: OrgSite[];
  adminApiKeys: AdminApiKeyCollection;
  products: Product[];
}

export interface OrganizationUsers {
  admins: Admins;
  members?: PaginatedMembers | null;
  memberTotal?: MemberTotal | null;
  mfaExemptMembers?: (MfaExemptUser | null)[] | null;
}

export interface Admins {
  id: string;
  total: number;
  users?: (Member | null)[] | null;
}

export interface Member {
  active: MemberStatus;
  displayName: string;
  emails: (MemberEmail | null)[];
  id: string;
  jobTitle?: string | null;
  useMfa?: boolean | null;
  memberAccess?: MemberAccess | null;
  primaryEmail?: string | null;
  provisionedBy?: MemberProvisioningSource | null;
  pendingInfo?: PendingInfo | null;
  deactivatedInfo?: DeactivatedInfo | null;
}

export interface MemberEmail {
  primary: boolean;
  value: string;
}

export interface MemberAccess {
  sites: MemberSiteAccess[];
  products: MemberAccessProducts[];
  errors: string[];
}

export interface MemberSiteAccess {
  siteUrl: string;
  isSiteAdmin: boolean;
}

export interface MemberAccessProducts {
  siteUrl: string;
  productName: string;
  productKey: string;
}

export interface PendingInfo {
  startedOn: string;
  startedBy: string;
  initiatorName?: string | null;
}

export interface DeactivatedInfo {
  deactivatedOn: string;
  deactivatedBy: string;
  initiatorName?: string | null;
}

export interface PaginatedMembers {
  total: number;
  users?: (Member | null)[] | null;
}

export interface MemberTotal {
  total: number;
  enabledTotal: number;
  disabledTotal: number;
}

export interface MfaExemptUser {
  userId: string;
}

export interface OrganizationSecurity {
  passwordPolicy?: PasswordPolicy | null;
  saml?: SamlConfiguration | null;
  twoStepVerification?: TwoStepVerification | null;
}

export interface PasswordPolicy {
  minimumStrength?: string | null;
  expiryDays?: number | null;
}

export interface SamlConfiguration {
  issuer: string;
  ssoUrl: string;
  publicCertificate: string;
  auth0MigrationState: Auth0State;
  serviceProvider: SamlServiceProvider;
}

export interface SamlServiceProvider {
  entityId: string;
  acsUrl: string;
}

export interface TwoStepVerification {
  dateEnforced?: UnixTimestamp | null;
}

export interface DomainClaim {
  domains?: (Domain | null)[] | null;
  token?: string | null;
}

export interface Domain {
  id: string;
  domain: string;
  status: string;
  verificationType?: VerificationType | null;
  verified: boolean;
}

export interface FeatureFlag {
  id: string;
  value?: boolean | null;
}

export interface ExternalDirectory {
  id: string;
  baseUrl: string;
  name: string;
  creationDate: string;
  syncedUsers: SyncedUsers;
  logs: LogEntryCollection;
  groups: SyncedGroupsCollection;
}

export interface SyncedUsers {
  countOfUsers: number;
}

export interface LogEntryCollection {
  totalResults: number;
  startIndex: number;
  itemsPerPage: number;
  logEntries: LogEntries[];
}

export interface LogEntries {
  error: string;
  createdOn: string;
}

export interface SyncedGroupsCollection {
  totalResults: number;
  startIndex: number;
  itemsPerPage: number;
  groups: SyncedGroup[];
}

export interface SyncedGroup {
  name: string;
  membershipCount: number;
}

export interface SiteAdmin {
  id: string;
  displayName: string;
  email: string;
  isOrgAdmin: boolean;
}

export interface MemberExport {
  id: string;
  content: string;
}

export interface OrgSite {
  id: string;
  displayName?: string | null;
  avatar?: string | null;
  siteUrl: string;
  products: Product[];
}

export interface Product {
  key: string;
  name: string;
}

export interface AdminApiKeyCollection {
  scrollId?: string | null;
  tokens: AdminApiKey[];
}

export interface AdminApiKey {
  id: string;
  name: string;
  createdBy: Member;
  creationDate: string;
  expirationDate: string;
}

export interface Site {
  applications?: (Application | null)[] | null;
  avatarUrl: string;
  context?: SiteContext | null;
  defaultApps: (DefaultProduct | null)[];
  displayName: string;
  flag: FeatureFlag;
  flags: SiteFlags;
  groupsAdminAccessConfig: (GroupsAccessConfig | null)[];
  groupsUseAccessConfig: (GroupsAccessConfig | null)[];
  gSuite: GSuite;
  id: string;
  inviteUrls: InviteUrl[];
  jsd: Jsd;
  settings: SiteSettings;
  siteAccess: SiteAccess;
  siteRenameStatus: SiteRenameStatus;
  userConnectedApps: UserConnectedApp[];
}

export interface Application {
  id: string;
  name: string;
  adminUrl: string;
  url: string;
  product: string;
}

export interface SiteContext {
  firstActivationDate: string;
}

export interface DefaultProduct {
  productId: string;
  productName: string;
  canonicalProductKey: string;
}

export interface SiteFlags {
  selfSignupADG3Migration: boolean;
  accessConfigADG3Migration: boolean;
  groupsADG3Migration: boolean;
  gSuiteADG3Migration: boolean;
  userExportADG3Migration: boolean;
  billingADG3Migration: boolean;
  usersADG3Migration: boolean;
  jsdADG3Migration: boolean;
  siteAdminPageMigrationRedirect: boolean;
  accessRequestsPage: boolean;
  inviteUrls: boolean;
  userConnectedAppsPage: boolean;
  siteUrlPage: boolean;
}

export interface GroupsAccessConfig {
  id?: string | null;
  product: ProductAccess;
  groups: (GroupAccess | null)[];
}

export interface ProductAccess {
  productId: string;
  productName: string;
  presence?: string | null;
  accessLevel: AccessLevel;
}

export interface GroupAccess {
  id: string;
  name: string;
  productPermission: (ProductPermission | null)[];
  requiresApproval: boolean;
  default: boolean;
}

export interface GSuite {
  state?: GSuiteState | null;
  groups?: GSuiteGroups[] | null;
}

export interface GSuiteState {
  id: string;
  syncConfig: (string | null)[];
  syncStartTime?: string | null;
  nextSyncStartTime?: string | null;
  lastSync?: GSuiteLastSync | null;
  selectedGroups: (string | null)[];
  noSyncs: number;
  user: string;
}

export interface GSuiteLastSync {
  startTime: string;
  failed: number;
  errors: (string | null)[];
  updated: number;
  hasReport: boolean;
  status: GSuiteLastSyncStatus;
  finishTime: string;
  numOfSyncedUsers: number;
  deleted: number;
  created: number;
}

export interface GSuiteGroups {
  id: string;
  name: string;
}

export interface InviteUrl {
  url: string;
  productKey: string;
  productName: string;
  expiration: string;
}

export interface Jsd {
  customers: PaginatedJsdCustomers;
  directory: Directory;
}

export interface PaginatedJsdCustomers {
  hasMoreResults: boolean;
  customers: JsdCustomer[];
}

export interface JsdCustomer {
  id: string;
  username: string;
  email: string;
  active: boolean;
  displayName: string;
  lastLogin?: string | null;
}

export interface Directory {
  id: string;
  customerExport: string;
}

export interface SiteSettings {
  id: string;
  emoji: EmojiSettings;
  serviceDesk: ServiceDeskSettings;
  xFlow: XFlowSettings;
}

export interface EmojiSettings {
  id: string;
  uploadEnabled: boolean;
}

export interface ServiceDeskSettings {
  enabled: boolean;
}

export interface XFlowSettings {
  enabled: boolean;
}

export interface SiteAccess {
  signupEnabled: boolean;
  openInvite: boolean;
  notifyAdmin: boolean;
  domains: string[];
}

export interface SiteRenameStatus {
  id: string;
  cloudName: string;
  cloudNamespace: string;
  renamesUsed: number;
  renameLimit: number;
}

export interface UserConnectedApp {
  id: string;
  name: string;
  description: string;
  avatarUrl?: string | null;
  vendorName?: string | null;
  scopes: AppScope[];
  userGrants: UserGrant[];
}

export interface AppScope {
  id: string;
  untranslatedName: string;
  untranslatedDescription: string;
}

export interface UserGrant {
  id: string;
  accountId: string;
  createdAt: string;
  scopes: AppScope[];
}

export interface CheckDomainClaimed {
  claimed: boolean;
}

export interface GroupDetails {
  groupDetails: Group;
  members: PaginatedUsers;
}

export interface Group {
  description?: string | null;
  id: string;
  name: string;
  productPermissions: (GroupProductPermissions | null)[];
  defaultForProducts: (GroupProduct | null)[];
  sitePrivilege: SitePrivilege;
  unmodifiable: boolean;
  userTotal: number;
  managementAccess: ManagementAccess;
  ownerType?: OwnerType | null;
}

export interface GroupProductPermissions {
  productName: string;
  productId: string;
  permissions: (Permission | null)[];
}

export interface GroupProduct {
  productName: string;
  productId: string;
}

export interface PaginatedUsers {
  total: number;
  users: (User | null)[];
}

export interface PaginatedGroups {
  total: number;
  groups: (Group | null)[];
}

export interface MemberDetails {
  memberDetails: Member;
  canCloseAccount?: LifecycleHookResult | null;
  canDeactivateAccount?: LifecycleHookResult | null;
  memberTokens: MemberToken[];
}

export interface LifecycleHookResult {
  errors: (LifecycleHookReason | null)[];
  warnings: (LifecycleHookReason | null)[];
}

export interface LifecycleHookReason {
  message: string;
  code: string;
  link?: string | null;
}

export interface MemberToken {
  id: string;
  label: string;
  createdAt: string;
  lastAccess?: string | null;
}

export interface MemberProfileMinimal {
  id: string;
  fullName: string;
  avatarUrl: string;
}

export interface Onboarding {
  id: string;
  dismissed: boolean;
}

export interface OrgProducts {
  id: string;
  identityManager: boolean;
}

export interface PollCreateOrgState {
  completed: boolean;
  successful: boolean;
}

export interface RolePermissions {
  permissionIds: (string | null)[];
}

export interface Stride {
  strideExports: StrideExport[];
  strideExport: StrideExport;
}

export interface StrideExport {
  id: string;
  startedAt: string;
  expiresAt?: string | null;
  creatorName: string;
  creatorEmail: string;
  status: StrideExportStatus;
  settings: StrideExportSettings;
  downloadLocation?: string | null;
  integrationsLocation?: string | null;
}

export interface StrideExportSettings {
  startDate?: string | null;
  endDate?: string | null;
  includePublicRooms: boolean;
  includePrivateRooms: boolean;
  includeDirects: boolean;
}

export interface UI {
  isFeedbackOpen: boolean;
  xflow: XFlow;
  navigation: Navigation;
  analytics: Analytics;
}

export interface XFlow {
  isDialogOpen: boolean;
  sourceComponent?: string | null;
  targetProduct?: string | null;
}

export interface Navigation {
  showContainer: boolean;
  activeDrawer: Drawer;
}

export interface Analytics {
  referrer: string;
}

export interface UserDetails {
  userDetails: User;
  groups: PaginatedGroups;
  productAccess: (ProductAccess | null)[];
  managedStatus?: UserManagedStatus | null;
}

export interface UserManagedStatus {
  managed: boolean;
  owner?: DomainOwner | null;
}

export interface DomainOwner {
  ownerType: string;
  ownerId: string;
}

export interface Mutation {
  _?: boolean | null;
  approveAccess?: boolean | null;
  denyAccess?: boolean | null;
  createAdminApiKey: AdminApiKeyWithToken;
  deleteAdminApiKey: boolean;
  claimDomain?: ClaimDomainStatus | null;
  deleteDomain?: boolean | null;
  createDirectory: CreateDirectoryResponse;
  regenerateDirectoryApiKey: RegenerateDirectoryApiKeyResponse;
  removeDirectory: boolean;
  submitFeedback?: boolean | null;
  getConnection: GSuiteConnection;
  createGroup: Group;
  updateGroup: Group;
  deleteGroup: boolean;
  addUsersToGroups: boolean;
  deleteUserFromGroup: boolean;
  generateInviteUrl?: boolean | null;
  deleteInviteUrl?: boolean | null;
  deleteJsdCustomer?: boolean | null;
  updateJsdCustomerAccess?: boolean | null;
  updateJsdCustomerName?: boolean | null;
  updateJsdCustomerPassword?: boolean | null;
  migrateJsdCustomer?: boolean | null;
  updateMember?: boolean | null;
  updateMemberEnabled?: boolean | null;
  resetMemberPassword: boolean;
  closeMemberAccount?: boolean | null;
  cancelDeleteMemberAccount?: boolean | null;
  deactivateMemberAccount?: boolean | null;
  activateMemberAccount?: boolean | null;
  revokeMemberToken?: boolean | null;
  updateMfaExemption: boolean;
  updateMfaEnrollment: boolean;
  dismissOnboarding: Onboarding;
  evaluateIdentityManager: OrgProducts;
  createOrganization: PollCreateOrgInfo;
  addOrganizationAdmin: boolean;
  removeOrganizationAdmin: boolean;
  renameOrganization: Organization;
  initiateMemberCsvEmailExport: boolean;
  linkSiteToOrganization: OrgSiteLinkingResult;
  updatePasswordPolicy?: boolean | null;
  resetPasswords?: boolean | null;
  updateSaml: boolean;
  deleteSaml: boolean;
  updateSiteAccess: boolean;
  checkSiteNameAvailability: SiteNameAvailabilityCheckResult;
  renameSite: RenameSiteRespnose;
  updateEmojiSettings: EmojiSettings;
  removeUserFromSite?: boolean | null;
  changeUserRole?: boolean | null;
  userExport: string;
  inviteUsers?: boolean | null;
  setDefaultProducts?: boolean | null;
  setDefaultGroup?: boolean | null;
  addGroupsAccessToProduct?: boolean | null;
  removeGroupAccessToProduct?: boolean | null;
  approveImportedGroup?: boolean | null;
  rejectImportedGroup?: boolean | null;
  deleteStrideExport: boolean;
  cancelStrideExport: boolean;
  createStrideExport: StrideExport;
  enableTwoStepVerificationEnforcement?: boolean | null;
  disableTwoStepVerificationEnforcement?: boolean | null;
  updateIsFeedbackOpen?: boolean | null;
  updateXFlowDialog?: boolean | null;
  updateNavigationContainer?: boolean | null;
  updateActiveDrawer?: boolean | null;
  updateReferrer?: boolean | null;
  activateUser?: boolean | null;
  deactivateUser?: boolean | null;
  impersonateUser?: boolean | null;
  reinviteUser?: boolean | null;
  promptResetPassword?: boolean | null;
  grantAccessToProducts?: boolean | null;
  revokeAccessToProducts?: boolean | null;
  suggestChanges?: boolean | null;
}

export interface AdminApiKeyWithToken {
  id: string;
  name: string;
  createdBy: Member;
  creationDate: string;
  expirationDate: string;
  token: string;
}

export interface ClaimDomainStatus {
  status: ClaimDomainStatusType;
}

export interface CreateDirectoryResponse {
  id: string;
  baseUrl: string;
  name: string;
  creationDate: string;
  apiKey: string;
}

export interface RegenerateDirectoryApiKeyResponse {
  apiKey: string;
}

export interface GSuiteConnection {
  url: string;
}

export interface PollCreateOrgInfo {
  progressUri: string;
  id: string;
}

export interface OrgSiteLinkingResult {
  isSuccess: boolean;
  errors?: string[] | null;
}

export interface SiteNameAvailabilityCheckResult {
  isAlreadyOwner: boolean;
  renameLimitReached: boolean;
  result: boolean;
  taken: boolean;
}

export interface RenameSiteRespnose {
  progressUri: string;
}

export interface MemberProduct {
  productName?: string | null;
  siteUrl?: string | null;
}

export interface AccessRequestListInput {
  status: AccessRequestStatus[];
  start: number;
  count?: number | null;
}

export interface SiteUsersListInput {
  start: number;
  count: number;
  displayName?: string | null;
  contains?: string | null;
  activeStatus?: (string | null)[] | null;
  productUse?: (string | null)[] | null;
  productAdmin?: (string | null)[] | null;
  sitePrivilege?: (string | null)[] | null;
}

export interface ApproveAccessInput {
  userId: string;
  productId: string;
  atlOrigin: string;
}

export interface DenyAccessInput {
  userId: string;
  productId: string;
  denialReason?: string | null;
}

export interface UpdateGroupInput {
  description?: string | null;
  name: string;
}

export interface AddUsersToGroupsInput {
  groups: (string | null)[];
  users: (string | null)[];
}

export interface GenerateInviteUrlInput {
  productKey: string;
}

export interface DeleteInviteUrlInput {
  productKey: string;
}

export interface UpdateMfaExemptionInput {
  id: string;
  member: string;
  exempt: boolean;
}

export interface UpdateMfaEnrollmentInput {
  id: string;
  member: string;
}

export interface UpdatePasswordPolicyInput {
  id: string;
  minimumStrength: string;
  expiryDays?: number | null;
  resetPasswords: boolean;
}

export interface UpdateSamlInput {
  id: string;
  issuer: string;
  ssoUrl: string;
  publicCertificate: string;
  auth0MigrationState: Auth0State;
}

export interface UpdateSiteAccessInput {
  signupEnabled: boolean;
  openInvite: boolean;
  notifyAdmin: boolean;
  domains: string[];
}

export interface RenameSiteInput {
  cloudName: string;
  cloudNamespace: string;
}

export interface EmojiSettingsInput {
  uploadEnabled: boolean;
}

export interface GenerateUserExportInput {
  includeGroups: boolean;
  includeProductAccess: boolean;
  includeInactiveUsers: boolean;
  selectedGroupIds: string[];
}

export interface SiteInviteUsersInput {
  emails?: (string | null)[] | null;
  productIds?: (string | null)[] | null;
  additionalGroups?: (string | null)[] | null;
  sendNotification?: boolean | null;
  notificationText?: string | null;
  role?: string | null;
  atlOrigin?: string | null;
}

export interface DefaultProductsInput {
  productId: string;
  productDefault: boolean;
}

export interface DefaultGroupsInput {
  productId: string;
  groupId: string;
  isDefault: boolean;
}

export interface AddGroupsAccessToProductInput {
  productId: string;
  groups: string[];
  productType: ProductType;
}

export interface RemoveGroupAccessToProductInput {
  productId: string;
  groupId: string;
  productType: ProductType;
}

export interface GroupImportInput {
  productId: string;
  groupId: string;
}

export interface CreateStrideExportInput {
  startDate?: string | null;
  endDate?: string | null;
  includePublicRooms: boolean;
  includePrivateRooms: boolean;
  includeDirects: boolean;
  passphrase: string;
}

export interface SuggestChangesInput {
  userId: string;
  email?: string | null;
  displayName?: string | null;
}
export interface AccessRequestListQueryArgs {
  cloudId: string;
  input: AccessRequestListInput;
}
export interface CheckDomainClaimedQueryArgs {
  domain: string;
}
export interface UserFeatureFlagQueryArgs {
  flag: string;
}
export interface GroupQueryArgs {
  cloudId: string;
  id: string;
}
export interface GroupListQueryArgs {
  cloudId: string;
  start?: number | null;
  count?: number | null;
  displayName?: string | null;
}
export interface MemberQueryArgs {
  id: string;
  memberId: string;
}
export interface MembersListQueryArgs {
  id: string;
  aaIds: (string | null)[];
}
export interface OnboardingQueryArgs {
  id: string;
}
export interface ProductsQueryArgs {
  id: string;
}
export interface OrganizationQueryArgs {
  id: string;
}
export interface PollCreateOrgQueryArgs {
  progressUri?: string | null;
}
export interface RolePermissionsQueryArgs {
  cloudId: string;
  roleId: string;
}
export interface SiteQueryArgs {
  id: string;
}
export interface StrideQueryArgs {
  cloudId: string;
}
export interface UserQueryArgs {
  cloudId: string;
  id: string;
}
export interface SiteUsersListQueryArgs {
  cloudId: string;
  input: SiteUsersListInput;
}
export interface IsSystemAdminCurrentUserArgs {
  cloudId: string;
}
export interface FlagCurrentUserArgs {
  flagKey: string;
}
export interface FlagOrganizationArgs {
  flagKey: string;
}
export interface SiteAdminsOrganizationArgs {
  cloudId: string;
}
export interface MemberExportOrganizationArgs {
  exportId: string;
}
export interface AdminApiKeysOrganizationArgs {
  scrollId?: string | null;
}
export interface MembersOrganizationUsersArgs {
  start?: number | null;
  count?: number | null;
  active?: boolean | null;
  mfa?: boolean | null;
  displayName?: string | null;
  domain?: string | null;
  filter?: string | null;
  products?: string[] | null;
  memberState?: MemberStateFilter | null;
}
export interface InitiatorNamePendingInfoArgs {
  orgId: string;
}
export interface InitiatorNameDeactivatedInfoArgs {
  orgId: string;
}
export interface LogsExternalDirectoryArgs {
  startIndex?: number | null;
  count?: number | null;
}
export interface GroupsExternalDirectoryArgs {
  startIndex?: number | null;
  count?: number | null;
}
export interface FlagSiteArgs {
  flagKey: string;
}
export interface CustomersJsdArgs {
  start: number;
  count: number;
  displayName?: string | null;
  activeStatus?: string | null;
}
export interface CustomerExportDirectoryArgs {
  errorRedirectUrl: string;
}
export interface MembersGroupDetailsArgs {
  start?: number | null;
  count?: number | null;
}
export interface MemberDetailsMemberDetailsArgs {
  pendingAndDeactivatedInfoReadFromIOM: boolean;
}
export interface StrideExportStrideArgs {
  exportId: string;
}
export interface GroupsUserDetailsArgs {
  start?: number | null;
  count?: number | null;
}
export interface ApproveAccessMutationArgs {
  cloudId: string;
  input: ApproveAccessInput;
}
export interface DenyAccessMutationArgs {
  cloudId: string;
  input: DenyAccessInput;
}
export interface CreateAdminApiKeyMutationArgs {
  orgId: string;
  name: string;
}
export interface DeleteAdminApiKeyMutationArgs {
  orgId: string;
  apiKeyId: string;
}
export interface ClaimDomainMutationArgs {
  id: string;
  domain: string;
  method: VerificationType;
}
export interface DeleteDomainMutationArgs {
  id: string;
  domain: string;
}
export interface CreateDirectoryMutationArgs {
  orgId: string;
  name: string;
}
export interface RegenerateDirectoryApiKeyMutationArgs {
  directoryId: string;
}
export interface RemoveDirectoryMutationArgs {
  directoryId: string;
}
export interface SubmitFeedbackMutationArgs {
  comment: string;
  email: string;
  webInfo: string;
}
export interface GetConnectionMutationArgs {
  id: string;
}
export interface CreateGroupMutationArgs {
  cloudId: string;
  input: UpdateGroupInput;
}
export interface UpdateGroupMutationArgs {
  cloudId: string;
  id: string;
  input: UpdateGroupInput;
}
export interface DeleteGroupMutationArgs {
  cloudId: string;
  id: string;
}
export interface AddUsersToGroupsMutationArgs {
  cloudId: string;
  input: AddUsersToGroupsInput;
}
export interface DeleteUserFromGroupMutationArgs {
  cloudId: string;
  userId: string;
  groupId: string;
}
export interface GenerateInviteUrlMutationArgs {
  id: string;
  input: GenerateInviteUrlInput;
}
export interface DeleteInviteUrlMutationArgs {
  id: string;
  input: DeleteInviteUrlInput;
}
export interface DeleteJsdCustomerMutationArgs {
  directoryId: string;
  accountId: string;
}
export interface UpdateJsdCustomerAccessMutationArgs {
  directoryId: string;
  accountId: string;
  access: boolean;
}
export interface UpdateJsdCustomerNameMutationArgs {
  directoryId: string;
  accountId: string;
  name: string;
}
export interface UpdateJsdCustomerPasswordMutationArgs {
  directoryId: string;
  accountId: string;
  password: string;
}
export interface MigrateJsdCustomerMutationArgs {
  cloudId: string;
  accountId: string;
}
export interface UpdateMemberMutationArgs {
  id: string;
  member: string;
  displayName: string;
  emailAddress: string;
  jobTitle: string;
}
export interface UpdateMemberEnabledMutationArgs {
  id: string;
  member: string;
  enabled: boolean;
}
export interface ResetMemberPasswordMutationArgs {
  id: string;
  member: string;
}
export interface CloseMemberAccountMutationArgs {
  memberId: string;
}
export interface CancelDeleteMemberAccountMutationArgs {
  memberId: string;
}
export interface DeactivateMemberAccountMutationArgs {
  memberId: string;
}
export interface ActivateMemberAccountMutationArgs {
  memberId: string;
}
export interface RevokeMemberTokenMutationArgs {
  memberId: string;
  memberTokenId: string;
}
export interface UpdateMfaExemptionMutationArgs {
  input: UpdateMfaExemptionInput;
}
export interface UpdateMfaEnrollmentMutationArgs {
  input: UpdateMfaEnrollmentInput;
}
export interface DismissOnboardingMutationArgs {
  id: string;
}
export interface EvaluateIdentityManagerMutationArgs {
  orgId: string;
}
export interface CreateOrganizationMutationArgs {
  name: string;
}
export interface AddOrganizationAdminMutationArgs {
  orgId: string;
  email: string;
}
export interface RemoveOrganizationAdminMutationArgs {
  orgId: string;
  adminId: string;
}
export interface RenameOrganizationMutationArgs {
  id: string;
  name: string;
}
export interface InitiateMemberCsvEmailExportMutationArgs {
  orgId: string;
}
export interface LinkSiteToOrganizationMutationArgs {
  orgId: string;
  cloudId: string;
}
export interface UpdatePasswordPolicyMutationArgs {
  input: UpdatePasswordPolicyInput;
}
export interface ResetPasswordsMutationArgs {
  id: string;
}
export interface UpdateSamlMutationArgs {
  input: UpdateSamlInput;
}
export interface DeleteSamlMutationArgs {
  id: string;
}
export interface UpdateSiteAccessMutationArgs {
  id: string;
  input: UpdateSiteAccessInput;
}
export interface CheckSiteNameAvailabilityMutationArgs {
  cloudId: string;
  siteName: string;
}
export interface RenameSiteMutationArgs {
  cloudId: string;
  input: RenameSiteInput;
}
export interface UpdateEmojiSettingsMutationArgs {
  id: string;
  input: EmojiSettingsInput;
}
export interface RemoveUserFromSiteMutationArgs {
  cloudId: string;
  id: string;
}
export interface ChangeUserRoleMutationArgs {
  cloudId: string;
  userId: string;
  roleId: string;
}
export interface UserExportMutationArgs {
  cloudId: string;
  input: GenerateUserExportInput;
}
export interface InviteUsersMutationArgs {
  id: string;
  input?: SiteInviteUsersInput | null;
}
export interface SetDefaultProductsMutationArgs {
  id: string;
  input: DefaultProductsInput;
}
export interface SetDefaultGroupMutationArgs {
  id: string;
  input: DefaultGroupsInput;
}
export interface AddGroupsAccessToProductMutationArgs {
  id: string;
  input: AddGroupsAccessToProductInput;
}
export interface RemoveGroupAccessToProductMutationArgs {
  id: string;
  input: RemoveGroupAccessToProductInput;
}
export interface ApproveImportedGroupMutationArgs {
  id: string;
  input: GroupImportInput;
}
export interface RejectImportedGroupMutationArgs {
  id: string;
  input: GroupImportInput;
}
export interface DeleteStrideExportMutationArgs {
  cloudId: string;
  exportId: string;
}
export interface CancelStrideExportMutationArgs {
  cloudId: string;
  exportId: string;
}
export interface CreateStrideExportMutationArgs {
  cloudId: string;
  input: CreateStrideExportInput;
}
export interface EnableTwoStepVerificationEnforcementMutationArgs {
  id: string;
  date: UnixTimestamp;
}
export interface DisableTwoStepVerificationEnforcementMutationArgs {
  id: string;
}
export interface UpdateIsFeedbackOpenMutationArgs {
  isOpen: boolean;
}
export interface UpdateXFlowDialogMutationArgs {
  isDialogOpen: boolean;
  sourceComponent?: string | null;
  targetProduct?: string | null;
}
export interface UpdateNavigationContainerMutationArgs {
  showContainer: boolean;
}
export interface UpdateActiveDrawerMutationArgs {
  activeDrawer: string;
}
export interface UpdateReferrerMutationArgs {
  referrer: string;
}
export interface ActivateUserMutationArgs {
  cloudId: string;
  id: string;
}
export interface DeactivateUserMutationArgs {
  cloudId: string;
  id: string;
}
export interface ImpersonateUserMutationArgs {
  cloudId: string;
  userId: string;
}
export interface ReinviteUserMutationArgs {
  cloudId: string;
  userId: string;
}
export interface PromptResetPasswordMutationArgs {
  cloudId: string;
  userId: string;
}
export interface GrantAccessToProductsMutationArgs {
  cloudId: string;
  users: string[];
  productIds: string[];
}
export interface RevokeAccessToProductsMutationArgs {
  cloudId: string;
  users: string[];
  productIds: string[];
}
export interface SuggestChangesMutationArgs {
  cloudId: string;
  userId: string;
  email: string;
  name: string;
}

export type AccessRequestStatus = "PENDING" | "APPROVED" | "REJECTED";

export type ActiveStatus = "ENABLED" | "DISABLED" | "BLOCKED";

export type AccessRequestType = "INVITE" | "REQUEST";

export type MemberStatus = "ENABLED" | "DISABLED" | "BLOCKED";

export type MemberProvisioningSource = "GSYNC" | "SCIM";

export type MemberStateFilter =
  | "ALL"
  | "ENABLED"
  | "DISABLED"
  | "BILLABLE"
  | "NON_BILLABLE";

export type Auth0State = "LEGACY" | "AUTH0" | "AUTH0FORCED";

export type VerificationType = "http" | "dns";

export type MigrationState = "ORGS" | "IM_GRANDFATHERED" | "IM";

export type AccessLevel = "NONE" | "USE" | "ADMIN" | "SYSADMIN";

export type ProductPermission = "WRITE" | "MANAGE";

export type GSuiteLastSyncStatus = "successful" | "failure";

export type Permission = "WRITE" | "MANAGE";

export type SitePrivilege = "NONE" | "SYS_ADMIN" | "SITE_ADMIN";

export type ManagementAccess = "NONE" | "READ_ONLY" | "CANNOT_DELETE" | "ALL";

export type OwnerType = "EXT_GSYNC" | "EXT_SCIM" | "MANAGED" | "ORG";
/** 1-to-1 mapping with the exporter CAPI https://bitbucket.org/hipchat/megatron/src/dea190b59760d6c391e1b5492283bfb784997a8d/exporter-capi.yaml#lines-237 */
export type StrideExportStatus =
  | "completed"
  | "pending"
  | "failed"
  | "cancelled"
  | "unknown";

export type Drawer =
  | "IDM"
  | "SAML"
  | "USER_PROVISIONING_CREATE_DIRECTORY"
  | "USER_PROVISIONING_REGEN_API_KEY"
  | "ORG_SITE_LINKING"
  | "CREATE_ADMIN_API_KEY_DRAWER"
  | "NONE"
  | "SITE_CREATE"
  | "INVITE_USER"
  | "EXPORT_USERS";

export type ClaimDomainStatusType = "SUCCESS" | "IN_PROGRESS" | "FAILED";

export type ProductType = "USE" | "ADMIN";
