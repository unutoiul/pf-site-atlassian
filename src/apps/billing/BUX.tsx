import * as React from 'react';

import { injectIntl } from 'react-intl';

import { addCollapsedNavProp } from 'common/page-layout/add-collapsed-nav-prop';

import Application from 'bux/components/Application';
import { DeepURLProvider } from 'bux/components/Page/with/urlTransformer';
import {
  BillEstimate,
  BillHistory,
  BillOverview,
  DiscoverNewProducts,
  ManageSubscriptions,
  PaymentDetail,
} from 'bux/pages';

import { BUXAnalyticsAdapter } from './adapters/AnalyticsAdapter';
import { FlagAdapter } from './adapters/FlagAdapter';
import { HubToBuxStateTransferer } from './adapters/HubToBuxStateTransferer';
import { LanguageAdapter } from './adapters/LanguageAdapter';
import { BUXRootLogger } from './adapters/Logger';
import { RouterAdapter } from './adapters/ReactRouterAdapter';

// BUX is mounted on "page" level, so just strip last part.
const getBUXPath = (path: string) => {
  const paths = path.split('/');

  return paths.slice(0, paths.indexOf('billing') + 1).join('/');
};

const ApplicationFrame = ({ children, match }) => (
  <DeepURLProvider path={getBUXPath(match.path)}>
    <BUXRootLogger>
      <HubToBuxStateTransferer match={match}>
        <BUXAnalyticsAdapter>
          <LanguageAdapter>
            <FlagAdapter />
            <RouterAdapter />
            <Application>
              {children}
            </Application>
          </LanguageAdapter>
        </BUXAnalyticsAdapter>
      </HubToBuxStateTransferer>
    </BUXRootLogger>
  </DeepURLProvider>
);

const inBux = (Component) => (props) => (
  <ApplicationFrame {...props}>
    <Component />
  </ApplicationFrame>
);

export const BUX = {
  BillOverview,
  BillEstimate,
  PaymentDetail,
  BillHistory,
  DiscoverNewProducts,
  ManageSubscriptions,
};

Object
  .keys(BUX)
  .forEach(key => BUX[key] = addCollapsedNavProp(injectIntl(inBux(BUX[key]))));
