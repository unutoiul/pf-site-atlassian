// tslint:disable-next-line:no-import-side-effect
import '@atlaskit/reduced-ui-pack';

import { BUX } from './BUX';

export const BillOverview = BUX.BillOverview;
export const BillEstimate = BUX.BillEstimate;
export const PaymentDetail = BUX.PaymentDetail;
export const BillHistory = BUX.BillHistory;
export const ManageSubscriptions = BUX.ManageSubscriptions;
export const DiscoverNewProducts = BUX.DiscoverNewProducts;
