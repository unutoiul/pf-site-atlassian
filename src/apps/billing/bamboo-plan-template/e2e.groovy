plan(key:'E2EBUXAAH',name:'E2E BUX Admin Hub',description:'Running e2e tests for BUX in AAH') {
   project(key:'EXPERIMENTAL',name:'Experimental Plans Go Here')
   repository(name:'pf-site-admin-ui')
   trigger(type:'polling',description:'Polling Bitbucket cloud repo',
      strategy:'periodically',frequency:'180') {
      repository(name:'pf-site-admin-ui')
   }

   notification(type:'Failed Builds and First Successful',recipient:'committers')

   stage(name:'Plan template') {
      job(key:'BUIL',name:'Default Job') {
         task(type:'checkout',description:'Checkout Default Repository') {
            repository(name:'pf-site-admin-ui')
         }
         task(type:'custom',createTaskKey:'com.atlassian.bamboo.plugin.bamboo-plan-templates:com.atlassian.bamboo.plugin.plantemplates',
            description:'execute the plan template',template:'src/apps/billing/bamboo-plan-template/e2e.groovy',
            passwordVariableCheck:'true',passwordVariable:'${bamboo.plan.template.rollout.bot.password}',
            executionStrategy:'executionStrategy.executeOnMaster',
            bambooServer:'https://sox-bamboo.internal.atlassian.com',
            username:'${bamboo.plan.template.rollout.bot.username}')
      }
   }
   stage(name:'Test') {
      job(key:'JOB1',name:'Cypress cloud tests') {
         requirement(key:'os',condition:'equals',value:'Linux')
         artifactDefinition(name:'Test Output',location:'./src/apps/billing/build-output/tests',
            pattern:'**/*',shared:'false')
         miscellaneousConfiguration() {
            pbc(enabled:'true',image:'docker.atl-paas.net/cloud-admin/pf-site-admin-ui-builder:latest')
         }
         task(type:'checkout',description:'Checkout source code') {
            repository(name:'pf-site-admin-ui')
         }
         task(type:'script',description:'Setup, build and test',
            scriptBody:'./src/apps/billing/scripts/build-run-cypress.sh',
            interpreter:'LEGACY_SH_BAT')
         task(type:'jUnitParser',description:'import mocha test results',
            final:'true',resultsDirectory:'src/apps/billing/build-output/**/*.xml')
      }
      job(key:'COT',name:'Cypress organization tests') {
         requirement(key:'os',condition:'equals',value:'Linux')
         artifactDefinition(name:'Test Output',location:'./src/apps/billing/build-output/tests',
            pattern:'**/*',shared:'false')
         miscellaneousConfiguration() {
            pbc(enabled:'true',image:'docker.atl-paas.net/cloud-admin/pf-site-admin-ui-builder:latest')
         }
         task(type:'checkout',description:'Checkout source code') {
            repository(name:'pf-site-admin-ui')
         }
         task(type:'script',description:'Setup, build and test',
            scriptBody:'./src/apps/billing/scripts/build-run-cypress.sh',
            environmentVariables:'ORGANIZATION=1',
            interpreter:'LEGACY_SH_BAT')
         task(type:'jUnitParser',description:'import mocha test results',
            final:'true',resultsDirectory:'src/apps/billing/build-output/**/*.xml')
      }
   }
   branchMonitoring(notificationStrategy:'INHERIT') {
      createBranch(matchingPattern:'.*')
      inactiveBranchCleanup(periodInDays:'10')
      deletedBranchCleanup(periodInDays:'1')
   }
   dependencies(triggerForBranches:'true')
   planMiscellaneous() {
      hungBuildKiller(enabled:'true')
      planOwnership(owner:'lrezendelemos')
   }
   permissions() {
      user(name:'arattihalli',permissions:'read,write,build')
      user(name:'lrezendelemos',permissions:'read,write,build,clone,administration')
      group(name:'pursre-acl-dl-pur-soxbac-plan-template-access',
         permissions:'read,write,build,clone,administration')
      loggedInUser(permissions:'read,build')
   }
}
