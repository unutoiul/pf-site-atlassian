const BAMBOO_BUILD = !!process.env.BAMBOO_BUILD;

let browserOverride;

if (process.env.e2e_browser) {
  browserOverride = process.env.e2e_browser;
}

// eslint-disable-next-line
module.exports = (function (settings) {
  const result = { ...settings };
  const browserStackSettings = {
    host: 'hub-cloud.browserstack.com',
    port: 80,
  };

  if (process.argv.includes('--browserstack')) {
    // eslint-disable-next-line global-require
    const gitCommit = require('child_process')
      .execSync('git rev-parse HEAD')
      .toString().trim()
      .slice(0, 7);
    // eslint-disable-next-line global-require
    const gitBranch = require('child_process')
      .execSync('git rev-parse --abbrev-ref HEAD')
      .toString().trim();

    const build = `${gitBranch} ${gitCommit}`;

    console.log('Running tests on BrowserStack');
    result.selenium.host = browserStackSettings.host;
    result.selenium.port = browserStackSettings.port;
    result.common_capabilities['browserstack.user'] = process.env.BROWSERSTACK_USERNAME;
    result.common_capabilities['browserstack.key'] = process.env.BROWSERSTACK_ACCESS_KEY;
    result.common_capabilities.build = `Billing UX Frontend Tests - ${build}`;
    /* eslint-disable */
    for (const i in result.test_settings) {
      const config = result.test_settings[i];
      config.selenium_host = browserStackSettings.host;
      config.selenium_port = browserStackSettings.port;
      config.launch_url = 'http://localhost';
      config.desiredCapabilities = config.desiredCapabilities || {};
      for (const j in result.common_capabilities) {
        config.desiredCapabilities[j] = config.desiredCapabilities[j] || result.common_capabilities[j];
      }
    }
    /* eslint-enable */
  } else {
    console.log('Running tests locally');
    if (BAMBOO_BUILD) {
      result.test_settings.default.desiredCapabilities.firefox_binary = '/usr/bin/firefox';
      result.selenium.cli_args['webdriver.gecko.driver'] = '/usr/bin/geckodriver';
    }

    if (browserOverride) {
      console.log(`Using browser from e2e_browser override env variable: ${browserOverride}`);
      result.test_settings.default.desiredCapabilities.browserName = browserOverride;
    }
  }

  return settings;
}(require('./nightwatch.json')));
