const path = require('path');

const root = path.resolve(`${__dirname}/bux`);

module.exports = {
  resolve: {
    alias: {
      bux: path.resolve(root)
    }
  }
};
