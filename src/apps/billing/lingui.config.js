module.exports = {
  sourceLocale: 'en',
  fallbackLocale: 'en',
  srcPathDirs: [
    '<rootDir>/bux',
  ],
  localeDir: '<rootDir>/bux/i18n',
  format: 'minimal',
};
