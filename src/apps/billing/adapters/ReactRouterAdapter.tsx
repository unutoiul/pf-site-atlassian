import * as React from 'react';
import { ReduxTrigger } from 'react-redux-loop';
import { withRouter } from 'react-router';

import { withUrlTransformer, withUrlTransformerProvided } from 'bux/components/Page/with/urlTransformer';

import { REDIRECT_CALL } from 'bux/core/state/router/actionTypes';

const Trigger = (({ history, urlTransform }) => (
  <ReduxTrigger
    when={REDIRECT_CALL}
    then={({ payload }) => history.push(urlTransform(payload))}
  />
));

const ConnectedTrigger = withUrlTransformerProvided(withUrlTransformer(Trigger));

export const RouterAdapter = withRouter(ConnectedTrigger);
