import { func } from 'prop-types';
import * as React from 'react';
import { connect } from 'react-redux';

import { getCloudId, getOrganizationId, getUserId } from 'bux/core/state/meta/selectors';
import { BUXState } from 'bux/core/state/types';

interface Props {
  orgId: string;
  cloudId: string;
  userId: string;
}

class StatelessBUXAnalyticsAdapter extends React.Component<Props> {
  public static contextTypes = {
    sendAnalyticEvent: func,
  };

  public static childContextTypes = {
    sendAnalyticEvent: func,
  };

  public getChildContext() {
    return {
      sendAnalyticEvent: this.sendAnalyticEvent,
    };
  }

  public sendAnalyticEvent = (name, payload = {}) => {
    const { cloudId, orgId, userId } = this.props;
    // userId may not be initially present

    this.context.sendAnalyticEvent(name, {
      cloudId,
      orgId,
      userId,
      ...payload,
    });
  };

  public render() {
    return React.Children.only(this.props.children);
  }
}

export const BUXAnalyticsAdapter = connect(
  (state: BUXState) => ({
    orgId: getOrganizationId(state),
    cloudId: getCloudId(state),
    userId: getUserId(state),
  }),
)(StatelessBUXAnalyticsAdapter);
