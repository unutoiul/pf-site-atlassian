import * as React from 'react';
import { connect } from 'react-redux';

import { getFeatureFlags } from 'bux/core/state/meta/selectors';

import FeatureFlagProvider from 'bux/components/FeatureFlags/FeatureFlagProvider';
import { FeatureFlagsType } from 'bux/core/state/meta/types';
import { BUXState } from 'bux/core/state/types';

const StatelessFeatureFlagAdapter: React.SFC<{ featureFlags: FeatureFlagsType }> = ({ featureFlags, children }) => (
  <FeatureFlagProvider featureFlags={featureFlags}>
    {children}
  </FeatureFlagProvider>
);

export const FeatureFlagAdapter = connect(
  (state: BUXState) => ({
    featureFlags: getFeatureFlags(state),
  }),
)(StatelessFeatureFlagAdapter);
