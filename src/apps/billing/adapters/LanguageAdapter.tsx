import * as React from 'react';

import en from 'bux/i18n/en/messages.json';
import ja from 'bux/i18n/ja/messages.json';

import { LanguageProvider, LinguiContext } from '../../../i18n/jsLingui/linguiProvider';

class LanguageInjector extends React.PureComponent<LanguageProvider> {
  public componentDidMount() {
    // no code splitting today
    this.props.injectLanguage('en', en);
    this.props.injectLanguage('ja', ja);
  }

  public render() {
    return null;
  }
}

export const LanguageAdapter: React.SFC = ({ children }) => (
  <React.Fragment>
    <LinguiContext.Consumer>
      {context => <LanguageInjector {...context} />}
    </LinguiContext.Consumer>
    {children}
  </React.Fragment>
);
