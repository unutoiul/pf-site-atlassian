import * as React from 'react';
import { ChildProps, graphql } from 'react-apollo';
import { Provider } from 'react-redux';

import { setUserMetadata, updateSiteState } from 'bux/core/state/meta/actions';
import { getCloudId, getOrganizationId } from 'bux/core/state/meta/selectors';
import { connect } from 'bux/helpers/redux';

import { BUXState } from 'bux/core/state/types';

import billingUserQuery from './billing-user.query.graphql';

import { BillingUserQuery } from '../../../schema/schema-types';
import { BUXStore } from '../store';

interface ComponentProps {
  matchParams: { orgId: string, cloudId: string };
}

interface MappedProps {
  initialized: boolean;

  updateSiteState(payload: any): any;

  setUserMetadata(payload: any): any;
}

type TransferProps = ChildProps<ComponentProps & MappedProps, BillingUserQuery>;

class StateTransferer extends React.Component<TransferProps> {
  public componentDidMount() {
    this.componentDidUpdate({});
  }

  public componentDidUpdate(prevProps) {
    const update = {
      organisationId: this.props.matchParams.orgId,
      cloudId: this.props.matchParams.cloudId,
    };
    if (
      !prevProps.matchParams ||
      prevProps.matchParams.orgId !== update.organisationId ||
      prevProps.matchParams.cloudId !== update.cloudId
    ) {
      this.props.updateSiteState(update);
    }

    if (this.props.data && this.props.data.currentUser) {
      this.props.setUserMetadata({ user: this.props.data.currentUser });
    } else {
      this.props.setUserMetadata(null);
    }
  }

  public render() {
    return this.props.initialized
      ? React.Children.only(this.props.children)
      : null;
  }
}

// TODO: remove generics when actions/selectors got typed
const Transferer = connect(
  (state: BUXState) => ({
    initialized: !!(getOrganizationId(state) || getCloudId(state)),
  }),
  { updateSiteState, setUserMetadata },
)(StateTransferer);

const BUXGraphConnector = graphql<ComponentProps, BillingUserQuery, {}, ComponentProps>(billingUserQuery)(Transferer);

export const HubToBuxStateTransferer = ({ match, children }) => (
  <Provider store={BUXStore}>
    <BUXGraphConnector matchParams={match.params}>
      {children}
    </BUXGraphConnector>
  </Provider>
);
