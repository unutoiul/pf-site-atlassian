import { withI18n } from '@lingui/react';
import * as React from 'react';
import { compose } from 'react-apollo';
import { connect } from 'react-redux';
import { ReduxTrigger } from 'react-redux-loop';

import { withFlag } from 'common/flag';

import { adapt, findTemplate } from 'bux/components/Notifications/Notifications';
import {
  CLOSE_NOTIFICATION,
  SHOW_NOTIFICATION_TEMPLATE,
} from 'bux/core/state/notifications/actionTypes';

const FlagTrigger = ({ dispatch, showFlag, hideFlag, i18n }) => {
  const raiseFlag = flag => {
    hideFlag(flag.id);
    showFlag(flag);
  };

  return (
    <div>
      <ReduxTrigger
        when={CLOSE_NOTIFICATION}
        then={({ payload }) => hideFlag(payload.id)}
      />
      <ReduxTrigger
        when={SHOW_NOTIFICATION_TEMPLATE}
        then={({ payload }) => raiseFlag(adapt(findTemplate(i18n, payload.template, payload.props), dispatch))}
      />
    </div>
  );
};

const mapDispatch = dispatch => ({
  dispatch,
});

export const FlagAdapter = compose(
  withI18n(),
  withFlag,
  connect(null, mapDispatch),
)(FlagTrigger);
