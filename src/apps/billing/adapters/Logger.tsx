import connectLogRoot from 'bux/components/log/root';

const InnerContent = ({ children }) => children;

export const BUXRootLogger = connectLogRoot('unified.admin.billing.BUX')(InnerContent);
