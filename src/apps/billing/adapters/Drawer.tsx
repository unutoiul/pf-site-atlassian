import * as React from 'react';
import * as ReactDOM from 'react-dom';

import FocusedTaskContainer from 'bux/features/FocusedTask/container';

const FOCUSED_DIV_ID = 'bux-portaled-focused-task';

export const BUXDrawer = () => (
  <div id={FOCUSED_DIV_ID} />
);

export class FocusedTaskAdapter extends React.Component<{}, { blocked: boolean }> {
  public state = {
    blocked: true,
  };

  public componentDidMount() {
    this.unlockPortal();
  }

  public unlockPortal = () => {
    const portal = document.getElementById(FOCUSED_DIV_ID);
    if (portal) {
      this.setState({
        blocked: false,
      });

      return;
    }
    setTimeout(this.unlockPortal, 16);
  };

  public render() {
    const { blocked } = this.state;

    return blocked
      ? null
      : ReactDOM.createPortal(
        <FocusedTaskContainer />,
        document.getElementById(FOCUSED_DIV_ID)!,
      );
  }
}
