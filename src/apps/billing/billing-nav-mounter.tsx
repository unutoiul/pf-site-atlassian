import * as React from 'react';
import { ChildProps, compose, graphql } from 'react-apollo';
import { defineMessages } from 'react-intl';

import { FeatureFlagAddedProp, withUserFeatureFlag } from 'common/feature-flags';
import { NavigationLinkIconGlyph, NavigationMounterProps, NavigationSectionId } from 'common/navigation';

import billingNavMounterQuery from './billing-nav-mounter.query.graphql';

import { BillingNavMounterQuery } from '../../schema/schema-types';

const messages = defineMessages({
  billing: {
    defaultMessage: 'Billing',
    description: '',
    id: 'chrome.nav-sections.billing.billing',
  },
  discoverApplications: {
    defaultMessage: 'Discover applications',
    description: '',
    id: 'chrome.nav-sections.billing.discover-applications',
  },
  billingOverview: {
    defaultMessage: 'Overview',
    description: '',
    id: 'chrome.nav-sections.billing.billing-overview',
  },
  billingPaymentDetails: {
    defaultMessage: 'Billing details',
    description: '',
    id: 'chrome.nav-sections.billing.billing-payment-details',
  },
  billingHistory: {
    defaultMessage: 'Billing history',
    description: '',
    id: 'chrome.nav-sections.billing.billing-history',
  },
  billingEstimate: {
    defaultMessage: 'Bill estimate',
    description: '',
    id: 'chrome.nav-sections.billing.billing-estimate',
  },
  billingApplications: {
    defaultMessage: 'Manage subscriptions',
    description: '',
    id: 'chrome.nav-sections.billing.billing-applications',
  },
});

type BillingNavMounterProps =
  ChildProps<
    Pick<NavigationMounterProps, 'removeNavigationSection' | 'updateNavigationSection'>,
    BillingNavMounterQuery
  > & FeatureFlagAddedProp;

export class BillingNavMounterImpl extends React.Component<BillingNavMounterProps> {
  public componentWillReceiveProps(nextProps: BillingNavMounterProps) {
    const {
      data: {
        loading: prevLoading = true,
      } = {},
    } = this.props;

    this.updateNavIfNeeded(nextProps, prevLoading);
  }

  public render() {
    return null;
  }

  public componentDidMount() {
    const { removeNavigationSection } = this.props;

    removeNavigationSection(NavigationSectionId.Billing);
    this.updateNavIfNeeded(this.props, true);
  }

  private updateNavIfNeeded = (nextProps: BillingNavMounterProps, prevLoading: boolean) => {
    const { updateNavigationSection, data: { loading = true } = {} } = nextProps;

    const isLoading = loading || nextProps.featureFlag.isLoading;
    const wasLoading = prevLoading || this.props.featureFlag.isLoading;

    if (isLoading) {
      return;
    }

    const {
      data: {
        currentSite: {
          id: currentSiteId = null,
        } = {},
      } = {},
      featureFlag,
    } = nextProps;

    if (
      isLoading === wasLoading &&
      this.props.featureFlag.value === nextProps.featureFlag.value
    ) {
      return;
    }

    const billingPathPrefix = `/admin/s/${currentSiteId}/billing`;
    const discoverApplicationsLink = this.createDiscoverLink(billingPathPrefix);

    if (featureFlag.value) {
      const billingFlatNavItem = this.createFlatSiteLinks(billingPathPrefix);
      // flat navigation
      updateNavigationSection({
        id: NavigationSectionId.Subscriptions,
        links: [
          ...billingFlatNavItem,
          discoverApplicationsLink,
        ],
      });
    } else {
      const billingNestedNavItem = this.createSiteLinks(billingPathPrefix);
      // nested navigation
      updateNavigationSection({
        id: NavigationSectionId.Subscriptions,
        links: [billingNestedNavItem, discoverApplicationsLink],
      });
    }
  };

  private createSiteLinks = (billingPathPrefix) => {
    return {
      title: messages.billing,
      id: NavigationSectionId.Billing,
      priority: 0,
      icon: NavigationLinkIconGlyph.Billing,
      links: [
        {
          analyticsSubjectId: 'billing-overview',
          path: `${billingPathPrefix}/overview`,
          title: messages.billingOverview,
        },
        {
          analyticsSubjectId: 'billing-details',
          path: `${billingPathPrefix}/paymentdetails`,
          title: messages.billingPaymentDetails,
        },
        {
          analyticsSubjectId: 'billing-history',
          path: `${billingPathPrefix}/history`,
          title: messages.billingHistory,
        },
        {
          analyticsSubjectId: 'billing-estimate',
          path: `${billingPathPrefix}/estimate`,
          title: messages.billingEstimate,
        },
        {
          analyticsSubjectId: 'billing-applications',
          path: `${billingPathPrefix}/applications`,
          title: messages.billingApplications,
        },
      ],
    };
  };

  private createFlatSiteLinks = (billingPathPrefix) => {
    return [
      {
        id: 'overview',
        analyticsSubjectId: 'billing-overview',
        path: `${billingPathPrefix}`,
        icon: NavigationLinkIconGlyph.Billing,
        title: messages.billing,
      },
      {
        id: 'applications',
        analyticsSubjectId: 'billing-applications',
        path: `${billingPathPrefix}/applications`,
        icon: NavigationLinkIconGlyph.BillingManage,
        title: messages.billingApplications,
      },
    ];
  };

  private createDiscoverLink = (billingPathPrefix) => {
    return {
      analyticsSubjectId: 'discover-applications',
      path: `${billingPathPrefix}/addapplication`,
      title: messages.discoverApplications,
      icon: NavigationLinkIconGlyph.DiscoverFilled,
      priority: 50,
    };
  };
}

export const BillingNavMounter = compose(
  graphql<NavigationMounterProps, BillingNavMounterQuery>(billingNavMounterQuery),
  withUserFeatureFlag({
    flagKey: 'admin.billing.menu.flat',
    defaultValue: false,
    name: 'featureFlag',
  }),
)(BillingNavMounterImpl);
