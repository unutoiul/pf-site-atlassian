import { createSelectorCreator, defaultMemoize } from 'reselect';

const noMemoize = (func: any) => func;

const createZeroSelector = (...args) => {
  const selector = (createSelectorCreator as any)(noMemoize)(...args);

  return (state = {}) => selector({ ...state });
};

type Selector<State, Result> = (state: State) => Result;

// We have to use this custom typings until reselect v4 release.
interface BUXReselect {
  <S1, R1, T>(
    selector1: Selector<S1, R1>,
    combiner: (a1: R1) => T,
  ): (state: S1) => T;

  <S1, S2, R1, R2, T>(
    selector1: Selector<S1, R1>,
    selector2: Selector<S2, R2>,
    combiner: (a1: R1, a2: R2) => T,
  ): (state: S1 & S2) => T;

  <S1, S2, S3, R1, R2, R3, T>(
    selector1: Selector<S1, R1>,
    selector2: Selector<S2, R2>,
    selector3: Selector<S3, R3>,
    combiner: (a1: R1, a2: R2, a3: R3) => T,
  ): (state: S1 & S2 & S3) => T;

  <S1, S2, S3, S4, R1, R2, R3, R4, T>(
    selector1: Selector<S1, R1>,
    selector2: Selector<S2, R2>,
    selector3: Selector<S3, R3>,
    selector4: Selector<S4, R4>,
    combiner: (a1: R1, a2: R2, a3: R3, a4: R4) => T,
  ): (state: S1 & S2 & S3) => T;
}

const createSelector: BUXReselect = (
  process.env.BABEL_ENV !== 'test'
    ? createSelectorCreator(defaultMemoize)
    : createZeroSelector
) as any;

const NonEmpty = {} as any;

export { createSelector, NonEmpty };
