import React from 'react';
import { func } from 'prop-types';
import Button from 'bux/components/Button';
import LogScope from 'bux/components/log/Scope';

class MockPayPalButton extends React.PureComponent {
  static propTypes = {
    onAuthorize: func
  }

  onAuthorize = () => {
    this.props.onAuthorize({ nonce: '123456', details: { email: 'user@test.org' } });
  }

  render() {
    return (
      <LogScope>
        <Button primary onClick={this.onAuthorize}>Pay with PayPal</Button>
      </LogScope>
    );
  }
}

export default MockPayPalButton;
