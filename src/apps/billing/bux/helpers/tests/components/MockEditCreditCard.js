import React from 'react';
import { func } from 'prop-types';
import sinon from 'sinon';

class EditCreditCard extends React.PureComponent {
  static propTypes = {
    onReady: func
  }

  componentDidMount() {
    this.props.onReady();
  }

  getCreditCardData() {
    return sinon.stub().resolves({});
  }

  render() {
    return null;
  }
}

export default EditCreditCard;
