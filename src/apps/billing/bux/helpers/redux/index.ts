import { connect as reduxConnect } from 'react-redux';
import { Dispatch } from 'redux';
import { Diff } from 'utility-types';

import Provider from './Provider';

type StateType = any;

// this is drop-in replacement for react-redux connect
// for the period of time, when we don't have actions/selectors typed
// it just works a bit differently
export function connect<
  MappedProps extends object,
  DispatchedProps extends object,
  OwnProps extends object
  >(
  mapStateToProps: ((
    store: StateType,
    ownProps?: OwnProps,
  ) => MappedProps | null) | null,
  mapDispatchToProps?:
    | ((dispatch: Dispatch<StateType>, ownProps?: OwnProps) => DispatchedProps)
    | DispatchedProps
    | null,
): <Props extends MappedProps & DispatchedProps>(
  WrappedComponent: React.ComponentType<Props>,
) => React.ComponentType<
  Diff<Props, MappedProps & DispatchedProps> & OwnProps
  > {
  return WrappedComponent =>
    reduxConnect(mapStateToProps, mapDispatchToProps as any)(
      WrappedComponent as any,
    ) as any;
}

export { Provider };
