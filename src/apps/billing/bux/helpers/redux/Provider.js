import Page from '@atlaskit/page';
import React from 'react';
import PropTypes from 'prop-types';
import { StaticRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { ApolloProvider } from 'react-apollo';
import { AnalyticsScreenContext } from 'bux/components/Analytics/components/Screen';
import createStore from 'bux/core/createStore';
import { createApolloClient } from '../../../../../apollo-client';

const apolloClient = createApolloClient();

const screenContext = {
  name: 'test',
  analyticsClient: {
    sendTrackEvent: () => {},
    sendUIEvent: () => {},
  },
  tenantInfo: {},
  sendTrackEvent: () => {},
  sendUIEvent: () => {},
};

const ApplicationProvider = ({ store, children, client }) => (
  <ApolloProvider client={client || apolloClient}>
    <Provider store={store || createStore()}>
      <StaticRouter context={{}}>
        <AnalyticsScreenContext.Provider value={screenContext}>
          <Page>
            {children}
          </Page>
        </AnalyticsScreenContext.Provider>
      </StaticRouter>
    </Provider>
  </ApolloProvider>
);

ApplicationProvider.propTypes = {
  store: PropTypes.any,
  children: PropTypes.node.isRequired,
  client: PropTypes.any,
};

export default ApplicationProvider;
