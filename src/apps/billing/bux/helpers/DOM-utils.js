export const simulateKeyDown = (key, target) => {
  const evt = document.createEvent('Events');
  evt.initEvent('keydown', true, true);
  evt.key = key;
  target.dispatchEvent(evt);
};