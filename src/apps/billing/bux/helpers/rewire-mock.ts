
import { resolve } from 'path';
import rewiremock, { addPlugin, overrideEntryPoint, plugins } from 'rewiremock';
import { configure } from 'rewiremock/lib/plugins/webpack-alias';

configure(resolve(`${__dirname}/../../webpack.config.test.js`));

overrideEntryPoint(module);
// we need webpack aliases
addPlugin(plugins.webpackAlias);
addPlugin(plugins.relative);
addPlugin(plugins.usedByDefault);

export { rewiremock };
