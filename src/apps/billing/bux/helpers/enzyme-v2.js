import { mount as realMount, shallow as realShallow, render as realRender } from 'enzyme';

const proxyfy = (wrapper, settings = {}) => new Proxy(wrapper, {
  get(target, prop) {
    // update first
    if (prop !== 'update' && !settings.child) {
      if (target.update) {
        target.update();
      }
    }

    // v2 compatible children
    if (prop === 'children') {
      return () => {
        const children = target.prop('children');
        if (Array.isArray(children)) {
          return children.filter(x => !!x);
        }
        return children;
      };
    }
    if (prop === 'childAt') {
      return n => target.wrap(target.prop('children')[n]);
    }
    if (prop === 'find') {
      return (...args) => {
        let result = Reflect.apply(target[prop], target, args);
        if (typeof args[0] === 'string' && result.length > 1) {
          result = result.hostNodes();
        }
        return proxyfy(result, { child: true });
      };
    }
    return Reflect.get(target, prop);
  }
});

export const shallow = (node, params = {}) => proxyfy(realShallow(node, { disableLifecycleMethods: true, ...params }));
export const mount = (node, params = {}) => proxyfy(realMount(node, params));
export const render = realRender;