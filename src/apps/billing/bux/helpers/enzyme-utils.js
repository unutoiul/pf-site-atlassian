export const until = async (predicate,
  text = 'until ',
  timeoutDuration = 1000
) => new Promise((resolve, reject) => {
  const interval = setInterval(() => {
    if (predicate()) {
      clearInterval(interval);
      resolve();
    }
  }, 1);

  setTimeout(() => {
    if (!predicate()) {
      clearInterval(interval);
      reject(new Error(`${text} timed out after ${timeoutDuration}ms`));
    }
  }, timeoutDuration);
});

export const flushPromise = async () => new Promise(resolve => setImmediate(resolve));
