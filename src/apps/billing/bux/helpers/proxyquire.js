import { resolve } from 'path';
import { Class as Proxyquire, configure } from 'proxyquire-webpack-alias';

// drop local cache
delete require.cache[require.resolve(__filename)];

const patchedProxyquire = (new Proxyquire(module.parent));

configure(resolve(`${__dirname}/../../webpack.config.test.js`));

patchedProxyquire
  .noUnusedStubs()
  .noPreserveCache()
  .onlyForProjectFiles();

export default patchedProxyquire;
