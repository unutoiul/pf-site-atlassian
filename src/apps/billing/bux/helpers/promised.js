import { expect } from 'chai';

export const expectRejected = async (promise) => {
  try {
    await promise;
    expect.fail(0, 1, 'Expected promise that will be rejected');
  } catch (e) {
    // Error is expected
  }
};
