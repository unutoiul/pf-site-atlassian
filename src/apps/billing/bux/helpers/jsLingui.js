export const i18n = {
  language: 'en',
  t: () => (strings, ...values) => strings.reduce((acc, str, index) => (acc + str + values[index - 1])),
  plural: ({ value, one, other }) => (value === 1 ? one : other).replace('#', value),
  select: values => values[values.value] || values.other
};
