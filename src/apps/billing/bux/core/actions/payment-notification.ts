import { logException } from 'bux/common/helpers/monitoring';

import {
  showNotificationByTemplate, showNotificationNetworkError,
} from '../state/notifications/actions';

const tnsFailureCodes = [
  'default',
  'EXTERNAL_VERIFICATION_DECLINED',
  'EXTERNAL_VERIFICATION_DECLINED_EXPIRED_CARD',
  'EXTERNAL_VERIFICATION_DECLINED_INVALID_CSC',
  'EXTERNAL_VERIFICATION_PROCESSING_ERROR',
  'invalid_fields',
  'request_timeout',
  'system_error',
  'unknown',
];

const showVerificationError = (code) => {
  if (tnsFailureCodes.includes(code)) {
    return showNotificationByTemplate(`payment-failure-${code}`);
  }
  if (code) {
    logException(`unhandled CC response: ${code}`);
  }

  return showNotificationNetworkError();
};

export const notifyFailPaymentRequest = (error) => {
  if (!error) {
    return showVerificationError('default');
  }
  if (error.couldNotReadErrorPayload) {
    return showNotificationNetworkError();
  }
  const validationCode = error.responseData ? error.responseData.creditCardValidationCode : error.reason || error;

  return showVerificationError(validationCode || 'default');
};
