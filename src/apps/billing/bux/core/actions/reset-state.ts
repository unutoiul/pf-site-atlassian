import { createAction } from 'redux-actions';

import { RESET_STATE } from 'bux/core/action-types';

export const resetStateAction = createAction(RESET_STATE);
