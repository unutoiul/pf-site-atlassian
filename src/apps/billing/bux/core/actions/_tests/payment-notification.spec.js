import sinon from 'sinon';
import proxyquire from 'bux/helpers/proxyquire';
import { expect } from 'chai';

describe('payment-notification: ', () => {
  /**
   * Dependency mocks
   */
  let showNotificationByTemplate;
  let showNotificationNetworkError;
  let closeNotificationNetworkError;

  let notifyFailPaymentRequest;

  const responseWith = value => ({ responseData: value });

  beforeEach(() => {
    showNotificationByTemplate = sinon.stub();
    showNotificationNetworkError = sinon.stub();
    closeNotificationNetworkError = sinon.stub();

    notifyFailPaymentRequest = proxyquire.noCallThru().load('../payment-notification', {
      '../state/notifications/actions': {
        showNotificationByTemplate,
        showNotificationNetworkError,
        closeNotificationNetworkError
      }
    }).notifyFailPaymentRequest;
  });

  const verifyValidationType = (code) => {
    const request = responseWith({
      creditCardValidationCode: code
    });

    notifyFailPaymentRequest(request);

    expect(showNotificationByTemplate).to.have.been.calledWith(`payment-failure-${code}`);
  };

  it('should notify authorisation declined', () => {
    verifyValidationType('EXTERNAL_VERIFICATION_DECLINED');
  });

  it('should notify authorisation expired', () => {
    verifyValidationType('EXTERNAL_VERIFICATION_DECLINED_EXPIRED_CARD');
  });

  it('should notify authorisation invalid csc', () => {
    verifyValidationType('EXTERNAL_VERIFICATION_DECLINED_INVALID_CSC');
  });

  it('should notify generic failure', () => {
    notifyFailPaymentRequest(responseWith({}));

    expect(showNotificationByTemplate).to.have.been.calledWith('payment-failure-default');
  });

  it('should notify network failure', () => {
    notifyFailPaymentRequest('error');
    expect(showNotificationNetworkError).to.have.been.called();
  });
});
