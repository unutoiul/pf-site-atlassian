import PropTypes from 'prop-types';

const IdType = PropTypes.oneOfType([PropTypes.number, PropTypes.string]);

const ExtendedPropTypes = {
  metadata: PropTypes.shape({
    display: PropTypes.bool,
    error: PropTypes.any,
    loading: PropTypes.bool
  }),
  notification: PropTypes.shape({
    id: IdType.isRequired,
    title: PropTypes.string.isRequired,
    type: PropTypes.oneOf(['success', 'error', 'info']),
    message: PropTypes.string,
    dismissAction: PropTypes.object,
    persist: PropTypes.bool,
    actions: PropTypes.arrayOf(PropTypes.shape({
      content: PropTypes.node,
      onClick: PropTypes.object
    }))
  })
};

export default ExtendedPropTypes;
