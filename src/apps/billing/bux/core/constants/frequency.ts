export const ANNUAL = 'ANNUAL';
export const MONTHLY = 'MONTHLY';
export const NONE = 'NONE';

export default { ANNUAL, MONTHLY, NONE };
