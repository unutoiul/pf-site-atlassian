import * as moment from 'moment';
import { AnyAction } from 'redux';
import { call, put, takeLatest } from 'redux-saga/effects';

import config from 'bux/common/config';

import { closeNotificationAction, showNotificationByTemplate } from 'bux/core/state/notifications/actions';

import {
  getItemFromSessionStorage,
  setItemFromSessionStorage,
} from 'bux/common/helpers/session-storage';

import { getHostname, openNewTab } from 'bux/common/helpers/browser';

import {
  CLOSE_CSAT_SURVEY,
  OPEN_CSAT_SURVEY,
  START_CSAT_SURVEY,
} from './actionTypes';

import {
  closeCSATSurvey,
  getCSATSurvey,
} from './actions';

const SURVEY_NOTIFICATION_ID = 'survey';
const SURVEY_DISMISS_DATE = 'csatSurveyDismissDate';
const SURVEY_ENCOUNTER_DATE = 'csatSurveyFirstViewDate';

export function getSessionDate(keyName: string) {
  let value;

  try {
    value = getItemFromSessionStorage(keyName);
    if (!value) {
      return null;
    }
  } catch (e) {
    // The storage is corrupted, so we return null to show the flag
    return null;
  }

  const numericValue = parseInt(value, 10);

  if (isNaN(numericValue)) {
    return null;
  }

  return moment(numericValue);
}

export const getDismissDate = () => getSessionDate(SURVEY_DISMISS_DATE);
export const getEncounterDate = () => getSessionDate(SURVEY_ENCOUNTER_DATE);

export const shouldShowSurveyFlag = lastCSATDate => (
  !lastCSATDate || moment(lastCSATDate).isBefore(moment().subtract(6, 'months'))
);

export const couldShowSurveyFlag = encounterDate => !!(
  encounterDate && moment(encounterDate).isBefore(moment().subtract(30, 'minutes'))
);

export const showSurveyNotification = () =>
  showNotificationByTemplate(SURVEY_NOTIFICATION_ID, { closeCSATSurvey: closeCSATSurvey(), getCSATSurvey: getCSATSurvey() });

export function* dismissSurveyNotification() {
  setItemFromSessionStorage(SURVEY_DISMISS_DATE, +new Date());
  yield put(closeNotificationAction({ id: SURVEY_NOTIFICATION_ID }));
}

export function setEncounterDate() {
  setItemFromSessionStorage(SURVEY_ENCOUNTER_DATE, +new Date());
}

export function* openSurveyInNewTab() {
  let url = config.surveyGizmoUrl;
  let hostname;

  try {
    hostname = getHostname();

    if (hostname) {
      url = `${url}&hostname=${hostname}`;
    }
  } catch (e) {
    // do nothing in this case
  }

  openNewTab(url);
  yield dismissSurveyNotification();
}

export function* dismissOrOpenSurveyNotification({ type }: AnyAction) {
  switch (type) {
    case OPEN_CSAT_SURVEY:
      yield call(openSurveyInNewTab);
      break;

    default:
      yield call(dismissSurveyNotification);
      break;
  }
}

export function* startSurveySaga() {
  const csatSurveyDismissDate = getDismissDate();
  const csatEncounterDate = getEncounterDate();

  if (shouldShowSurveyFlag(csatSurveyDismissDate) && couldShowSurveyFlag(csatEncounterDate)) {
    yield put(showSurveyNotification());
    yield takeLatest([CLOSE_CSAT_SURVEY, OPEN_CSAT_SURVEY], dismissOrOpenSurveyNotification);
  } else if (!csatEncounterDate) {
    yield call(setEncounterDate);
  }
}

function* rootSaga() {
  yield takeLatest(START_CSAT_SURVEY, startSurveySaga);
}

export default rootSaga;
