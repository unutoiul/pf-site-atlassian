import {
  CLOSE_CSAT_SURVEY,
  OPEN_CSAT_SURVEY,
  START_CSAT_SURVEY,
} from './actionTypes';

export const getCSATSurvey = () => ({ type: OPEN_CSAT_SURVEY });
export const closeCSATSurvey = () => ({ type: CLOSE_CSAT_SURVEY });
export const startCSATSurvey = () => ({ type: START_CSAT_SURVEY });
