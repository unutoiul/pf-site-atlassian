export const OPEN_CSAT_SURVEY = 'bux/features/csat/actions/open';
export const CLOSE_CSAT_SURVEY = 'bux/features/csat/actions/close';
export const START_CSAT_SURVEY = 'bux/features/csat/start';
