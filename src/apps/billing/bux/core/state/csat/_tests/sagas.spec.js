import { expect } from 'chai';
import sinon from 'sinon';
import moment from 'moment';
import { rewiremock } from 'bux/helpers/rewire-mock';
import { takeLatest, call, put } from 'redux-saga/effects';

import { CLOSE_CSAT_SURVEY, OPEN_CSAT_SURVEY, START_CSAT_SURVEY } from '../actionTypes';

let getItemFromSessionStorage;
let setItemFromSessionStorage;
let openNewTab;
let openCsatSaga;
let getHostname;

const surveyGizmoUrl = 'http://someurl/with-value?this=that&something=something';

describe('csat', () => {
  beforeEach(() => {
    setItemFromSessionStorage = sinon.stub();
    getItemFromSessionStorage = sinon.stub();
    openNewTab = sinon.stub();
    getHostname = sinon.stub();
    openCsatSaga = rewiremock.proxy('../sagas', {
      'bux/common/config': {
        default: {
          surveyGizmoUrl
        }
      },
      'bux/common/helpers/session-storage': {
        getItemFromSessionStorage,
        setItemFromSessionStorage
      },
      'bux/common/helpers/browser': {
        openNewTab,
        getHostname
      }
    });
  });
  describe('default export (root saga)', () => {
    it('should export 1 sub sagas', () => {
      const saga = openCsatSaga.default();
      const sagas = saga.next().value;
      expect(sagas).to.be.deep.equal(takeLatest(START_CSAT_SURVEY, openCsatSaga.startSurveySaga));
    });
  });

  describe('helper functions', () => {
    it('should getDismissDate', () => {
      const rightNow = +new Date();
      getItemFromSessionStorage.returns(rightNow);
      const value = openCsatSaga.getDismissDate();
      expect(value).to.be.instanceof(moment);
      expect(value).to.be.deep.equal(moment(rightNow));
    });

    it('should return null if getDismissDate throws', () => {
      getItemFromSessionStorage.throws();
      const value = openCsatSaga.getDismissDate();
      expect(value).to.be.be.null();
    });

    it('should return null if sessionStorage is empty or NaN', () => {
      getItemFromSessionStorage.returns(null);
      expect(openCsatSaga.getSessionDate()).to.be.be.null();

      getItemFromSessionStorage.returns('');
      expect(openCsatSaga.getSessionDate()).to.be.be.null();

      getItemFromSessionStorage.returns('not a number bro.');
      expect(openCsatSaga.getSessionDate()).to.be.be.null();
    });

    it('should show the flag if argument is null or falsey', () => {
      let result = openCsatSaga.shouldShowSurveyFlag();
      expect(result).to.be.true();

      result = openCsatSaga.shouldShowSurveyFlag(null);
      expect(result).to.be.true();

      result = openCsatSaga.shouldShowSurveyFlag('');
      expect(result).to.be.true();

      result = openCsatSaga.shouldShowSurveyFlag(0);
      expect(result).to.be.true();
    });

    it('should show the flag if the dismissal date past 6 months', () => {
      const unixEpoch = moment().unix();
      const result = openCsatSaga.shouldShowSurveyFlag(moment(unixEpoch));
      expect(result).to.be.true();
    });

    it('should not show the flag if the dismissal date is within is null', () => {
      const result = openCsatSaga.shouldShowSurveyFlag(moment());
      expect(result).to.be.false();
    });

    it('could show the flag if argument is null or falsey', () => {
      let result = openCsatSaga.couldShowSurveyFlag();
      expect(result).to.be.false();

      result = openCsatSaga.couldShowSurveyFlag(null);
      expect(result).to.be.false();

      result = openCsatSaga.couldShowSurveyFlag('');
      expect(result).to.be.false();

      result = openCsatSaga.couldShowSurveyFlag(0);
      expect(result).to.be.false();
    });

    it('could show the flag if the dismissal date past 6 months', () => {
      const unixEpoch = moment().unix();
      const result = openCsatSaga.couldShowSurveyFlag(moment(unixEpoch));
      expect(result).to.be.true();
    });

    it('could not show the flag if the dismissal date is within is null', () => {
      const result = openCsatSaga.couldShowSurveyFlag(moment());
      expect(result).to.be.false();
    });
  });

  describe('sagas', () => {
    it('should open a new tab with the survey url', () => {
      const sagaValue = openCsatSaga.dismissOrOpenSurveyNotification({ type: OPEN_CSAT_SURVEY }).next().value;
      expect(sagaValue).to.deep.equal(call(openCsatSaga.openSurveyInNewTab));
    });

    it('should dismiss the notification', () => {
      const sagaValue = openCsatSaga.dismissOrOpenSurveyNotification({ type: 'whatever action' }).next().value;
      expect(sagaValue).to.deep.equal(call(openCsatSaga.dismissSurveyNotification));
    });

    it('shouldn\'t start the survey saga', () => {
      const rightNow = +new Date();
      getItemFromSessionStorage.returns(rightNow);
      expect(openCsatSaga.startSurveySaga().next().done).to.be.true();
    });

    it('should set first encounter', () => {
      getItemFromSessionStorage.returns(0);
      expect(openCsatSaga.startSurveySaga().next().value).to.deep.equal(call(openCsatSaga.setEncounterDate));
    });

    it('should start the survey saga', () => {
      const unixEpoch = 1191910704;
      getItemFromSessionStorage.returns(unixEpoch);
      const runningSaga = openCsatSaga.startSurveySaga();
      expect(runningSaga.next().value).to.deep.equal(put(openCsatSaga.showSurveyNotification()));
      expect(runningSaga.next().value).to.deep.equal(takeLatest(
        [CLOSE_CSAT_SURVEY, OPEN_CSAT_SURVEY],
        openCsatSaga.dismissOrOpenSurveyNotification
      ));
      expect(runningSaga.next().done).to.be.true();
    });
  });

  describe('When openning the survey in a new tab', () => {
    it('should open with the hostname in the url if available', () => {
      getHostname.returns('somehostname');

      const finalUrl = `${surveyGizmoUrl}&hostname=somehostname`;
      openCsatSaga.openSurveyInNewTab().next();
      expect(openNewTab).to.have.been.calledWith(finalUrl);
    });

    it('should open without the hostname in the url if not available', () => {
      getHostname.returns(null);

      openCsatSaga.openSurveyInNewTab().next();
      expect(openNewTab).to.have.been.calledWith(surveyGizmoUrl);
    });

    it('should open without the hostname in the url if getHostname throws', () => {
      getHostname.throws();

      openCsatSaga.openSurveyInNewTab().next();
      expect(openNewTab).to.have.been.calledWith(surveyGizmoUrl);
    });
  });
});
