import { createAction } from 'redux-actions';

import {
  CLOSE_NOTIFICATION,
  SHOW_NOTIFICATION_TEMPLATE,
} from './actionTypes';

export const closeNotificationAction = createAction(CLOSE_NOTIFICATION);
export const showNotificationByTemplateAction = createAction(SHOW_NOTIFICATION_TEMPLATE);

export const closeNotification = (id: string) => dispatch => (
  dispatch(closeNotificationAction({ id }))
);

export const closeNotificationNetworkError = () => (
  closeNotificationAction({ id: 'networkError' })
);

export const showNotificationNetworkError = () => (
  showNotificationByTemplateAction({ template: 'network-error' })
);

export const showNotificationByTemplate = (template: string, props: any = {}) => (
  showNotificationByTemplateAction({ template, props })
);
