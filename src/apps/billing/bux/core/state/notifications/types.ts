import { NAME } from './constants';

export interface NotificationType {
  id: string;
  title: string;
  message: string;
  dismissAction: string;
  actions: string;
  persist: boolean;
  type: 'success' | 'error' | 'info';
}

export interface NotificationsState {
  [NAME]: NotificationType[];
}
