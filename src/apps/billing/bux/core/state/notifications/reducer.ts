import { handleActions } from 'redux-actions';

import {
  CLOSE_NOTIFICATION,
} from './actionTypes';

export const INITIAL_NOTIFICATION_STATE = [];

const filterState = (state, payload) => state.filter(message => message.id !== payload.id);

export default handleActions({
  [CLOSE_NOTIFICATION]: (state, { payload }) => filterState(state, payload),
}, INITIAL_NOTIFICATION_STATE);
