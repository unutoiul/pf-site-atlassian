import { NotificationsState } from 'bux/core/state/notifications/types';

export const getNotification = (state: NotificationsState) => state.notification;
