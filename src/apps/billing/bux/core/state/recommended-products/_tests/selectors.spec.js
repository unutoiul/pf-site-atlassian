import { expect } from 'chai';
import { tags } from 'bux/common/data/products';

import testMetadata from '../../_helpers/metadata/testHelpers/metadata';
import { NAME } from '../constants';
import {
  getState,
  getProducts,
  getPromotedProductDetails,
  getRecommendedProductsMetadata,
  getApps
} from '../selectors';

const mockState = (data = {
  data: false, isFetching: false, error: false, lastUpdated: false
}) => ({
  [NAME]: data
});

const mockProducts = {
  nextBillingPeriod: {
    renewalFrequency: 'MONTHLY',
  },
  recommendedProducts: {
    products: [
      {
        productKey: 'jira-core.ondemand',
        familyName: 'jira',
        price: 10.00,
        initialUnitPackage: 10,
        unitLabel: 'USER',
        activating: false,
        unlimitedTier: false,
        reactivation: false,
        eligibleForTrial: true,
        promoted: false,
        upgrading: false,
      },
      {
        productKey: 'jira-servicedesk.ondemand',
        familyName: 'jira',
        price: 10.00,
        initialUnitPackage: 3,
        unitLabel: 'AGENT',
        activating: false,
        unlimitedTier: false,
        reactivation: false,
        eligibleForTrial: true,
        promoted: true,
        upgrading: false,
      },
      {
        productKey: 'hipchat.cloud',
        familyName: 'stride',
        price: 0.00,
        initialUnitPackage: 1,
        unitLabel: 'USER',
        activating: false,
        unlimitedTier: true,
        reactivation: false,
        eligibleForTrial: true,
        promoted: false,
        upgrading: false,
      },
      {
        productKey: 'confluence.ondemand',
        familyName: 'confluence',
        price: 10.00,
        initialUnitPackage: 3,
        unitLabel: 'USER',
        activating: false,
        unlimitedTier: false,
        reactivation: false,
        eligibleForTrial: true,
        promoted: true,
        upgrading: false
      }
    ],
    families: [
      {
        familyName: 'confluence',
        apps: [
          {
            productKey: 'team.calendars.confluence.ondemand',
            familyName: 'confluence',
            parentProductKeys: [
              'confluence.ondemand',
            ],
            price: 10.00,
            initialUnitPackage: 10,
            unitLabel: 'USER',
            activating: false,
            unlimitedTier: false,
            reactivation: false,
            eligibleForTrial: true,
            promoted: false,
            upgrading: false,
          },
          {
            productKey: 'com.atlassian.confluence.plugins.confluence-questions.ondemand',
            familyName: 'confluence',
            parentProductKeys: [
              'confluence.ondemand',
            ],
            price: 0.00,
            initialUnitPackage: 10,
            unitLabel: 'USER',
            activating: false,
            unlimitedTier: false,
            reactivation: false,
            eligibleForTrial: true,
            promoted: false,
            upgrading: false,
          },
        ],
      },
      {
        familyName: 'jira',
        apps: [
          {
            productKey: 'bonfire.jira.ondemand',
            familyName: 'jira',
            parentProductKeys: [
              'jira.ondemand',
              'jira-core.ondemand',
              'jira-software.ondemand',
              'jira-servicedesk.ondemand',
            ],
            price: 10.00,
            initialUnitPackage: 10,
            unitLabel: 'USER',
            activating: false,
            unlimitedTier: false,
            reactivation: false,
            eligibleForTrial: true,
            promoted: false,
            upgrading: false,
          },
          {
            productKey: 'com.radiantminds.roadmaps-jira.ondemand',
            familyName: 'jira',
            parentProductKeys: [
              'jira.ondemand',
              'jira-core.ondemand',
              'jira-software.ondemand',
              'jira-servicedesk.ondemand',
            ],
            price: 10.00,
            initialUnitPackage: 10,
            unitLabel: 'USER',
            activating: false,
            unlimitedTier: false,
            reactivation: false,
            eligibleForTrial: true,
            promoted: false,
            upgrading: false,
          },
        ],
      },
    ],
  },
};

describe('Recommended products selectors:', () => {
  describe('getState:', () => {
    it('should return the full state', () => {
      const state = mockState({ data: mockProducts });
      expect(getState(state)).to.be.deep.equal(state[NAME].data);
    });
    it('should return an empty object', () => {
      const state = mockState();
      expect(getState(state)).to.be.empty();
    });
  });

  describe('getRecommendedProductsMetadata:', () => {
    it('should process regular metadata in a standard way', () => {
      testMetadata(mockState, getRecommendedProductsMetadata);
    });

    it('should have default error data when bill estimate returns special status code', () => {
      const error = { response: { status: 423 } };
      const state = mockState({ error });
      const billEstimateMetadata = getRecommendedProductsMetadata(state);
      expect(billEstimateMetadata.error).to.include(error);
    });
  });

  describe('getProducts:', () => {
    let products;
    const findProduct = key => products.find(item => item.productKey === key);

    before(() => {
      const state = mockState({ data: mockProducts });
      products = getProducts(state);
    });

    it('should return product jira', () => {
      const product = findProduct('jira-core.ondemand');
      expect(product).to.include({
        productKey: 'jira-core.ondemand',
        activating: false,
        name: 'Jira Core',
        tag: tags.track,
        link: 'https://www.atlassian.com/software/jira/core',
        pricingLink: 'https://www.atlassian.com/software/jira/core/pricing?tab=cloud',
        additionalInfo: null
      });
      expect(product.price).to.be.eql(10);
    });
    it('should return a product with default pricing link', () => {
      const product = findProduct('jira-core.ondemand');
      expect(product).to.have.property('pricingLink', 'https://www.atlassian.com/software/jira/core/pricing?tab=cloud');
    });
  });

  describe('getPromotedProductDetails:', () => {
    let product;

    before(() => {
      const state = mockState({ data: mockProducts });
      product = getPromotedProductDetails(state);
    });

    it('should return confluence productKey', () => {
      expect(product).to.include({
        productKey: 'confluence.ondemand',
        activating: false,
        name: 'Confluence',
        link: 'https://www.atlassian.com/software/confluence',
        eligibleForTrial: true,
        additionalInfo: null,
        promoted: true
      });
    });
  });

  describe('getApps:', () => {
    let apps;
    const findParent = key => apps.find(item => item.familyName === key);

    before(() => {
      const state = mockState({ data: mockProducts });
      apps = getApps(state);
    });

    it('should return apps for confluence', () => {
      const parent = findParent('confluence');
      expect(parent).to.have.property('apps');
      expect(parent.apps).to.have.length(2);
      const app = parent.apps[0];

      expect(app).to.include({
        productKey: 'team.calendars.confluence.ondemand',
        activating: false,
        name: 'Team Calendars for Confluence',
        tag: tags.messaging,
        link: 'https://www.atlassian.com/software/confluence/team-calendars',
        pricingLink: 'https://www.atlassian.com/software',
        additionalInfo: null
      });
      expect(app.price).to.be.eql(10);
    });
  });
});
