import { expect } from 'chai';
import { UPGRADE, REACTIVATE, ACTIVATING, UPGRADING, TRY } from 'bux/core/state/recommended-products/constants';
import { getActionType, withDetails, withPrice, missingDetails } from '../helpers';

describe('Recommended products helpers:', () => {
  context('getActionType', () => {
    it('should return ACTIVATING', () => {
      expect(getActionType({ activating: true })).to.be.eql(ACTIVATING);
      expect(getActionType({ activating: true, reactivation: true })).to.be.eql(ACTIVATING);
    });
    it('should return UPGRADE', () => {
      expect(getActionType({ upgrading: true })).to.be.eql(UPGRADE);
      expect(getActionType({ upgrading: true, upgradePrice: 2 })).to.be.eql(UPGRADE);
    });
    it('should return UPGRADING', () => {
      expect(getActionType({ upgrading: true, upgradeDate: new Date() })).to.be.eql(UPGRADING);
    });
    it('should return REACTIVATE', () => {
      expect(getActionType({ reactivation: true })).to.be.eql(REACTIVATE);
    });
    it('should return TRY', () => {
      expect(getActionType({})).to.be.eql(TRY);
    });
  });

  context('withDetails', () => {
    it('should return object with details', () => {
      expect(withDetails({ jira: { some: 'data' } })({ productKey: 'jira' }))
        .to.be.deep.equal({ productKey: 'jira', some: 'data', isApp: false });
    });
    it('should return object with isApp = true', () => {
      expect(withDetails({ jira: { some: 'data', parentKey: 'any' } })({ productKey: 'jira' }))
        .to.be.deep.equal({
          productKey: 'jira', some: 'data', parentKey: 'any', isApp: true
        });
    });
  });

  context('withPrice', () => {
    it('should return object with price', () => {
      const result = withPrice({ foo: 'bar', price: 10, upgradePriceExtraInfo: 'info' });
      expect(result).to.have.property('price').and.equal(10);
      expect(result).to.have.property('additionalInfo', null);
      expect(result).to.have.property('foo', 'bar');
      expect(result).to.have.property('action').and.have.property('type', TRY);
    });
    it('should return object with upgrade price', () => {
      const result = withPrice({
        price: 10, upgrading: true, upgradePrice: 2, upgradePriceExtraInfo: 'info'
      });
      expect(result).to.have.property('price').and.equal(2);
      expect(result).to.have.property('additionalInfo', 'info');
    });
    it('should return object with product pricing link', () => {
      expect(withPrice({ pricingLink: 'https://www.test.org' }))
        .to.have.property('pricingLink', 'https://www.test.org');
    });
    it('should return object with default link', () => {
      expect(withPrice({}))
        .to.have.property('pricingLink', 'https://www.atlassian.com/software');
    });
    it('should return object with null link', () => {
      expect(withPrice({ pricingLink: null }))
        .to.have.property('pricingLink', null);
    });
  });

  context('missingDetails', () => {
    it('should filter objects without name attr', () => {
      const result = [{}].filter(missingDetails);
      expect(result).to.have.length(0);
    });
    it('should not filter objects with name attr', () => {
      const result = [{ name: 'jira' }].filter(missingDetails);
      expect(result).to.have.length(1);
    });
  });
});
