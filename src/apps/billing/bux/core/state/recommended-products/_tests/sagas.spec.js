import { expect } from 'chai';
import { call } from 'redux-saga/effects';
import { getRecommendedProducts } from 'bux/common/api/billing-ux-service';
import recommendedProductsSaga from '../sagas';
import fetchSaga from '../../_helpers/metadata/saga';
import { REQUEST_RECOMMENDED_PRODUCTS, REQUEST_RECOMMENDED_PRODUCTS_REFRESH } from '../actionTypes';

describe('Recommended Products sagas', () => {
  it('should use fetch saga', () => {
    const saga = recommendedProductsSaga();
    expect(saga.next().value).to.deep.equal(call(
      fetchSaga,
      REQUEST_RECOMMENDED_PRODUCTS_REFRESH,
      getRecommendedProducts,
      REQUEST_RECOMMENDED_PRODUCTS
    ));
  });
});
