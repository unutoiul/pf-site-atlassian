import testReducer, { testReducerStateReset } from '../../_helpers/metadata/testHelpers/reducer';
import { REQUEST_RECOMMENDED_PRODUCTS } from '../actionTypes';
import reducer, { INITIAL_RECOMMENDED_PRODUCTS_STATE } from '../reducer';

describe('Recommended Products reducer', () => {
  testReducer(reducer, REQUEST_RECOMMENDED_PRODUCTS, INITIAL_RECOMMENDED_PRODUCTS_STATE);
  testReducerStateReset(reducer, INITIAL_RECOMMENDED_PRODUCTS_STATE);
});
