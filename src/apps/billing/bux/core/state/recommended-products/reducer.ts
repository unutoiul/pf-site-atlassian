import { handleActions } from 'redux-actions';

import { RESET_STATE } from '../../action-types';
import sagaReducer, { withMetadata } from '../_helpers/metadata/reducer';
import { REQUEST_RECOMMENDED_PRODUCTS } from './actionTypes';

export const INITIAL_RECOMMENDED_PRODUCTS_STATE = withMetadata({
  nextBillingPeriod: {
    startDate: '',
    endDate: '',
    renewalFrequency: '',
  },
  products: {},
  recommendedProducts: {
    products: [],
  },
});

export default handleActions({
  [REQUEST_RECOMMENDED_PRODUCTS]: sagaReducer,
  [RESET_STATE]: () => INITIAL_RECOMMENDED_PRODUCTS_STATE,
}, INITIAL_RECOMMENDED_PRODUCTS_STATE);
