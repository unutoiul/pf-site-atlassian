import config from 'bux/common/config';
import {
  ACTIVATING,
  REACTIVATE,
  TRY,
  UPGRADE,
  UPGRADING,
} from 'bux/core/state/recommended-products/constants';
import {
  DetailedProductType,
  ExtendedProductType,
  ProductDetails,
  ProductType,
} from 'bux/core/state/recommended-products/types';

export const getActionType = ({
                                activating,
                                upgradeDate,
                                upgrading,
                                reactivation,
                              }: ProductType) => {
  if (activating) {
    return ACTIVATING;
  }
  if (upgradeDate) {
    return UPGRADING;
  }
  if (upgrading) {
    return UPGRADE;
  }
  if (reactivation) {
    return REACTIVATE;
  }

  return TRY;
};

export const withDetails = (data: Record<string, ProductDetails>) => (product: ExtendedProductType): DetailedProductType => ({
  ...product,
  ...data[product.productKey],
  isApp: !!data[product.productKey].parentKey,
});

export const withPrice = (product: DetailedProductType) => {
  const { price, upgradePrice, upgrading, pricingLink = config.defaultPricingLink } = product;

  // if customer has hipchat cloud show upgrade price for Stride. Hardcoded for now.
  const value = upgrading ? upgradePrice : price;

  return {
    ...product,
    price: value,
    pricingLink,
    additionalInfo: upgrading ? product.upgradePriceExtraInfo : null,
    action: {
      type: getActionType(product),
    },
  };
};

// Filter out server response that does not exist in local products.js file
export const missingDetails = product => typeof product.name !== 'undefined';

export const withPriceAndDetails = (list: ExtendedProductType[], details: any) =>
  list
    .map(withDetails(details))
    .map(withPrice)
    .filter(missingDetails);
