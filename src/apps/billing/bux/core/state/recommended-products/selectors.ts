import { default as appsDetails } from 'bux/common/data/apps';
import { default as productsDetails } from 'bux/common/data/products';
import {
  ExtendedAppList,
  ExtendedProductType, ProductMetaType,
  RecommendedProductsState,
  RecommendedProductsType,
} from 'bux/core/state/recommended-products/types';
import { createSelector } from 'bux/helpers/reselect';

import { getMetadata } from '../_helpers/metadata/selectors';
import { ANNUAL, NAME } from './constants';
import { withPriceAndDetails } from './helpers';

export const getState = (state: RecommendedProductsState): RecommendedProductsType => state[NAME].data || {};

export const getRecommendedProductsMetadata = createSelector((state: RecommendedProductsState) => state[NAME], getMetadata);

export const productsList = (state: RecommendedProductsState): ExtendedProductType[] => {
  const {
    recommendedProducts: { products = [] } = {},
    nextBillingPeriod: { renewalFrequency = '' } = {},
  } = getState(state);
  const isAnnual = renewalFrequency === ANNUAL;

  return products.map(product => ({ ...product, isAnnual }));
};

const appsList = (state: RecommendedProductsState): ExtendedAppList[] => {
  const {
    recommendedProducts: { families = [] } = {},
    nextBillingPeriod: { renewalFrequency = [] } = {},
  } = getState(state);
  const isAnnual = renewalFrequency === ANNUAL;

  return families.map((family) => {
    const { familyName, apps = [] } = family;

    return {
      familyName,
      apps: apps.map(app => ({ ...app, isAnnual })),
    };
  });
};

const isPromotedProduct = (product: ProductMetaType) => product.promotable && product.promoted && !product.reactivation;

export const getPromotedProductDetails = (state: RecommendedProductsState) => {
  const promotedProducts = withPriceAndDetails(productsList(state), productsDetails)
    .filter(isPromotedProduct)
    .sort((p1, p2) => p1.priority - p2.priority);

  return promotedProducts.length > 0 ? promotedProducts[0] : null;
};

export const getProducts = (state: RecommendedProductsState) => {
  const promotedProduct = getPromotedProductDetails(state);

  return withPriceAndDetails(productsList(state), productsDetails)
    .filter(product => !promotedProduct || product.productKey !== promotedProduct.productKey);
};

export const getAllRecommendedProducts = (state: RecommendedProductsState) =>
  withPriceAndDetails(productsList(state), productsDetails);

export const isAccountExpired = (state: RecommendedProductsState) => getState(state).expiredAccount;

export const getApps = (state: RecommendedProductsState) => (
  appsList(state)
    .map(parent => ({
      ...parent,
      apps: withPriceAndDetails(parent.apps, appsDetails),
    }))
);
