import { REQUEST_RECOMMENDED_PRODUCTS_REFRESH } from './actionTypes';

export const getRecommendedProducts = () => ({
  type: REQUEST_RECOMMENDED_PRODUCTS_REFRESH,
});
