import { call } from 'redux-saga/effects';

import { getRecommendedProducts } from 'bux/common/api/billing-ux-service';

import fetchSaga from '../_helpers/metadata/saga';
import { REQUEST_RECOMMENDED_PRODUCTS, REQUEST_RECOMMENDED_PRODUCTS_REFRESH } from './actionTypes';

function* rootSaga() {
  yield call(fetchSaga, REQUEST_RECOMMENDED_PRODUCTS_REFRESH, getRecommendedProducts, REQUEST_RECOMMENDED_PRODUCTS);
}

export default rootSaga;
