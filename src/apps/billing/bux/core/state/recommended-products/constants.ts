export const NAME = 'recommendedProducts';
export const ANNUAL = 'ANNUAL';
export const USER = 'USER';
export const AGENT = 'AGENT';

export const UPGRADE = 'UPGRADE';
export const REACTIVATE = 'REACTIVATE';
export const ACTIVATING = 'ACTIVATING';
export const UPGRADING = 'UPGRADING';
export const TRY = 'TRY';
