import { BUXMetaReducedType } from 'bux/core/state/_helpers/metadata/types';

import { NAME } from './constants';

export interface ProductMetaType {
  promoted: boolean;
  promotable: boolean;
  upgrading: boolean;
  reactivation: boolean;
}

export type ProductType = ProductMetaType & {
  productKey: string,
  familyName: string,
  parentProductKeys: string[],
  price: number,
  initialUnitPackage: number,
  unitLabel: string,
  activating: boolean,
  unlimitedTier: boolean,
  eligibleForTrial: boolean,
  upgradeDate: string,
};

export type ExtendedProductType = ProductType & { isAnnual: boolean };

export interface ProductDetails {
  name: string;
  tag: string;
  publisher: string;
  description: string;
  link: string;
  logo: string;
  icon: string;
  contextPath: string;
  dependencies: any[];
  UMKey: string;
  outsideInstall: string;
  pricingLink: string;

  upgradePrice: number;
  upgradePriceExtraInfo: string;
  upgradeUrl: string;
  promotable: boolean;

  parentKey: string;
  priority: number;
}

export type DetailedProductType = ExtendedProductType & ProductDetails & { isApp: boolean };

export type CompleteProductType = DetailedProductType & {
  price: { value: number, currencyCode: string },
  pricingLink: string,
  additionalInfo?: string,
  action: {
    type: string,
  },
};

export type AppType = ProductType;

export type ExtendedAppType = AppType & { isAnnual: boolean };

export interface AppList {
  familyName: string;
  apps: AppType[];
}

export interface ExtendedAppList {
  familyName: string;
  apps: ExtendedAppType[];
}

export interface RecommendedProductsType {
  expiredAccount: boolean;
  daysBeforeBillingEmail: number;
  estimatedActivationTime: number;
  nextBillingPeriod: {
    startDate: string,
    endDate: string,
    renewalFrequency: string,
  };
  primaryBillingContact: string;
  recommendedProducts: {
    products: ProductType[],
    families: AppList[];
  };
  currency: string;
}

export interface RecommendedProductsState {
  [NAME]: BUXMetaReducedType<RecommendedProductsType>;
}
