import { call } from 'redux-saga/effects';

import { getBillHistory } from 'bux/common/api/billing-ux-service';

import fetchSaga from '../_helpers/metadata/saga';
import { REQUEST_BILL_HISTORY, REQUEST_BILL_HISTORY_REFRESH } from './actionTypes';

function* rootSaga() {
  yield call(fetchSaga, REQUEST_BILL_HISTORY_REFRESH, getBillHistory, REQUEST_BILL_HISTORY);
}

export default rootSaga;
