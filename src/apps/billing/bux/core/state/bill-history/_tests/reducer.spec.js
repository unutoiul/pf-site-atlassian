import testReducer, { testReducerStateReset } from '../../_helpers/metadata/testHelpers/reducer';
import { REQUEST_BILL_HISTORY } from '../actionTypes';
import reducer, { INITIAL_BILLING_HISTORY_STATE } from '../reducer';

describe('bill-history reducer', () => {
  testReducer(reducer, REQUEST_BILL_HISTORY, INITIAL_BILLING_HISTORY_STATE);
  testReducerStateReset(reducer, INITIAL_BILLING_HISTORY_STATE);
});
