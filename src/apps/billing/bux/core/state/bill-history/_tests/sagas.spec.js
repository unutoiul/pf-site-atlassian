import { expect } from 'chai';
import { call } from 'redux-saga/effects';
import { getBillHistory } from 'bux/common/api/billing-ux-service';
import billHistorySaga from '../sagas';
import fetchSaga from '../../_helpers/metadata/saga';
import { REQUEST_BILL_HISTORY, REQUEST_BILL_HISTORY_REFRESH } from '../actionTypes';

describe('bill-history sagas', () => {
  it('should use fetch saga', () => {
    const saga = billHistorySaga();
    expect(saga.next().value).to.deep.equal(call(
      fetchSaga,
      REQUEST_BILL_HISTORY_REFRESH,
      getBillHistory,
      REQUEST_BILL_HISTORY
    ));
  });
});
