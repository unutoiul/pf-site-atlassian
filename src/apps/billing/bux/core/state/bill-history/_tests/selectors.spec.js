import { expect } from 'chai';

import testMetadata from '../../_helpers/metadata/testHelpers/metadata';
import { NAME } from '../constants';
import { getInvoices, getInvoicesMetadata } from '../selectors';

const mockState = (data = {
  data: false, isFetching: false, error: false, lastUpdated: false 
}) => ({
  [NAME]: data
});
const mockStateWithBillHistory = (billHistory = {}) => mockState({ data: billHistory });

describe('Bill History selectors:', () => {
  describe('getInvoicesMetadata:', () => {
    testMetadata(mockState, getInvoicesMetadata);
  });

  describe('getInvoices:', () => {
    const createInvoice = key => ({
      invoiceNumber: key,
      amount: 1,
      billDate: '2017-04-01',
      invoiceUrl: 'invoice.url',
      currencyCode: 'USD'
    });

    it('should return empty array when there are no invoice', () => (
      expect(getInvoices(mockStateWithBillHistory({ invoices: [] }))).to.deep.equal([])
    ));

    it('should return invoices array when present', () => {
      const invoiceList = getInvoices(mockStateWithBillHistory({
        invoices: [
          createInvoice('1'),
          createInvoice('2')
        ]
      }));
      expect(invoiceList.length).to.equal(2);
    });
  });
});
