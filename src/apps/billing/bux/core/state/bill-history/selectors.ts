import { HistoryState } from 'bux/core/state/bill-history/types';
import { createSelector } from 'bux/helpers/reselect';

import { getMetadata } from '../_helpers/metadata/selectors';
import { NAME } from './constants';

export const getBillHistory = (state: HistoryState) => state[NAME].data;

export const getInvoices = (state: HistoryState) => getBillHistory(state).invoices;
export const getInvoicesMetadata = createSelector((state: HistoryState) => state[NAME], getMetadata);
