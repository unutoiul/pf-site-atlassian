import { BUXMetaReducedType } from 'bux/core/state/_helpers/metadata/types';

import { NAME } from './constants';

interface Invoice {
  invoiceNumber: string;
  amount: number;
  billDate: string;
  invoiceUrl: string;
  currencyCode: string;
}

interface Invoices {
  invoices: Invoice[];
}

export interface HistoryState {
  [NAME]: BUXMetaReducedType<Invoices>;
}
