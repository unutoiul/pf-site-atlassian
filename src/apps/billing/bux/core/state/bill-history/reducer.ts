import { handleActions } from 'redux-actions';

import { RESET_STATE } from '../../action-types';
import sagaReducer, { withMetadata } from '../_helpers/metadata/reducer';
import { REQUEST_BILL_HISTORY } from './actionTypes';

export const INITIAL_BILLING_HISTORY_STATE = withMetadata({
  invoices: [],
});

export default handleActions({
  [REQUEST_BILL_HISTORY]: sagaReducer,
  [RESET_STATE]: () => INITIAL_BILLING_HISTORY_STATE,
}, INITIAL_BILLING_HISTORY_STATE);
