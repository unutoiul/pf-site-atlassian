import { REQUEST_BILL_HISTORY_REFRESH } from './actionTypes';

export const getBillHistory = () => ({ type: REQUEST_BILL_HISTORY_REFRESH });
