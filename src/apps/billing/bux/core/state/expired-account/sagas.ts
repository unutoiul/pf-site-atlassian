import { AnyAction } from 'redux';
import { all, put, select, takeLatest } from 'redux-saga/effects';

import { closeNotificationAction, showNotificationByTemplate } from 'bux/core/state/notifications/actions';
import { REQUEST_RECOMMENDED_PRODUCTS } from 'bux/core/state/recommended-products/actionTypes';
import { isAccountExpired } from 'bux/core/state/recommended-products/selectors';
import { push as redirect } from 'bux/core/state/router/actions';

import { closeExpiredAccount, openBillingDetails } from './actions';
import { CLOSE_EXPIRED_ACCOUNT_NOTIFICATION, OPEN_BILLING_DETAILS } from './actionTypes';

export const EXPIRED_ACCOUNT_NOTIFICATION_ID = 'expired-account';

export function* showExpiredAccountNotification() {
  const isExpired = yield select(isAccountExpired);
  if (!isExpired) {
    return undefined;
  }
  yield put(showNotificationByTemplate(EXPIRED_ACCOUNT_NOTIFICATION_ID, {
    closeExpiredAccount: closeExpiredAccount(), openBillingDetails: openBillingDetails(),
  }));
}

export function* dismissExpiredAccountNotificationOrRedirect({ type }: AnyAction) {
  if (type === OPEN_BILLING_DETAILS) {
    yield put(redirect('/paymentdetails'));
  }
  yield put(closeNotificationAction({ id: EXPIRED_ACCOUNT_NOTIFICATION_ID }));
}

function* rootSaga() {
  yield all([
    takeLatest([REQUEST_RECOMMENDED_PRODUCTS], showExpiredAccountNotification),
    takeLatest([CLOSE_EXPIRED_ACCOUNT_NOTIFICATION, OPEN_BILLING_DETAILS], dismissExpiredAccountNotificationOrRedirect),
  ]);
}

export default rootSaga;
