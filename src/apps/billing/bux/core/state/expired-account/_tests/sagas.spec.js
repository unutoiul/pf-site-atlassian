import { select } from 'redux-saga/effects';
import { expectSaga } from 'redux-saga-test-plan';

import { REDIRECT } from 'bux/core/state/router/actionTypes';
import { CLOSE_NOTIFICATION, SHOW_NOTIFICATION_TEMPLATE } from 'bux/core/state/notifications/actionTypes';
import { isAccountExpired } from 'bux/core/state/recommended-products/selectors';
import { REQUEST_RECOMMENDED_PRODUCTS } from 'bux/core/state/recommended-products/actionTypes';
import { CLOSE_EXPIRED_ACCOUNT_NOTIFICATION, OPEN_BILLING_DETAILS } from 'bux/core/state/expired-account/actionTypes';
import rootSaga, { EXPIRED_ACCOUNT_NOTIFICATION_ID } from '../sagas';

const createAction = (type, payload) => ({ action: { type, payload } });
const createEvent = (type, payload = {}) => ({ type, payload });

describe('expiredAccount', () => {
  describe('sagas', () => {
    const REQUEST_RECOMMENDED_PRODUCTS_EVENT = createEvent(REQUEST_RECOMMENDED_PRODUCTS);
    const OPEN_BILLING_DETAILS_EVENT = createEvent(OPEN_BILLING_DETAILS);
    const CLOSE_EXPIRED_ACCOUNT_NOTIFICATION_EVENT = createEvent(CLOSE_EXPIRED_ACCOUNT_NOTIFICATION);

    const SHOW_NOTIFICATION_ACTION = createAction(SHOW_NOTIFICATION_TEMPLATE, {
      template: EXPIRED_ACCOUNT_NOTIFICATION_ID
    });
    const CLOSE_NOTIFICATION_ACTION = createAction(CLOSE_NOTIFICATION, { id: EXPIRED_ACCOUNT_NOTIFICATION_ID });
    const REDIRECT_ACTION = createAction(REDIRECT, { url: '/paymentdetails' });


    it('should show error notification', () => expectSaga(rootSaga)
      .dispatch(REQUEST_RECOMMENDED_PRODUCTS_EVENT)
      .provide([[select(isAccountExpired), true]])
      .put.like(SHOW_NOTIFICATION_ACTION)
      .run());

    it('should not show error notification', () => expectSaga(rootSaga)
      .dispatch(REQUEST_RECOMMENDED_PRODUCTS_EVENT)
      .provide([[select(isAccountExpired), false]])
      .not.put.like(SHOW_NOTIFICATION_ACTION)
      .run());

    it('should redirect to payment details', () => expectSaga(rootSaga)
      .dispatch(OPEN_BILLING_DETAILS_EVENT)
      .put.like(REDIRECT_ACTION)
      .put.like(CLOSE_NOTIFICATION_ACTION)
      .run());

    it('should close notification', () => expectSaga(rootSaga)
      .dispatch(CLOSE_EXPIRED_ACCOUNT_NOTIFICATION_EVENT)
      .not.put.like(REDIRECT_ACTION)
      .put.like(CLOSE_NOTIFICATION_ACTION)
      .run());
  });
});
