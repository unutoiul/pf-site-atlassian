import { createAction } from 'redux-actions';

import { CLOSE_EXPIRED_ACCOUNT_NOTIFICATION, OPEN_BILLING_DETAILS } from './actionTypes';

export const openBillingDetails = createAction(OPEN_BILLING_DETAILS);
export const closeExpiredAccount = createAction(CLOSE_EXPIRED_ACCOUNT_NOTIFICATION);
