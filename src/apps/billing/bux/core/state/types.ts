import { BillEstimateState } from 'bux/core/state/bill-estimate/types';
import { HistoryState } from 'bux/core/state/bill-history/types';
import { BillingDetailsState } from 'bux/core/state/billing-details/types';
import { EntitlementsGroupState } from 'bux/core/state/entitlement-group/types';
import { FocusedTasksState } from 'bux/core/state/focused-task/types';
import { MetaState } from 'bux/core/state/meta/types';
import { NotificationsState } from 'bux/core/state/notifications/types';
import { RecommendedProductsState } from 'bux/core/state/recommended-products/types';

export type BUXState =
  BillEstimateState &
  HistoryState &
  BillingDetailsState &
  EntitlementsGroupState &
  FocusedTasksState &
  MetaState &
  RecommendedProductsState &
  NotificationsState;
