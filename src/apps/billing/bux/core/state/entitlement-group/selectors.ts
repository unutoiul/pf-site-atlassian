import { createSelector } from 'bux/helpers/reselect';

import { EntitlementsGroupState } from 'bux/core/state/entitlement-group/types';

import { getMetadata } from '../_helpers/metadata/selectors';
import { inLoadingState } from '../_helpers/metadata/stateCreators';
import { NAME } from './constants';

/*
 * Subscriptions
 */
const store = (state: EntitlementsGroupState) => state[NAME];
const entitlementGroupSelector =
  (state: EntitlementsGroupState) => store(state).entitlementGroup;
const entitlementEditionSelector = entitlement =>
  (state: EntitlementsGroupState) => store(state).entitlementEditions[entitlement] || inLoadingState();

export const getEntitlementGroup = (state: EntitlementsGroupState) =>
  entitlementGroupSelector(state).data || {};

export const getEntitlements = (state: EntitlementsGroupState) =>
  getEntitlementGroup(state).entitlements || [];

export const getEntitlementEdition = entitlement => (state: EntitlementsGroupState) =>
  entitlementEditionSelector(entitlement)(state).data;

export const getEntitlementGroupMetadata =
  createSelector(entitlementGroupSelector, getMetadata);

export const getEntitlementEditionMetadata = entitlement =>
  createSelector(entitlementEditionSelector(entitlement), getMetadata);

export const getEntitlementById = entitlementId =>
  createSelector(
    getEntitlements,
    entitlements => entitlements.find(item => item.id === entitlementId),
  );

export const isEntitlementActive = entitlementsId =>
  createSelector(
    getEntitlementById(entitlementsId),
    entitlement => !!(entitlement && entitlement.status === 'ACTIVE' && !entitlement.suspended),
  );
