export const REQUEST_ENTITLEMENT_GROUP = 'bux/features/entitlement-group/actions/api-request';
export const REQUEST_ENTITLEMENT_GROUP_REFRESH = 'bux/features/entitlement-group/actions/api-refresh';

export const REQUEST_ENTITLEMENT_EDITION = 'bux/features/entitlement-group/actions/edition/api-request';
export const REQUEST_ENTITLEMENT_EDITION_REFRESH = 'bux/features/entitlement-group/actions/edition/api-refresh';
