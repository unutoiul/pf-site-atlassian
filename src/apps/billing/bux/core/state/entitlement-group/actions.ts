import {
  REQUEST_ENTITLEMENT_EDITION_REFRESH,
  REQUEST_ENTITLEMENT_GROUP_REFRESH,
} from './actionTypes';

export const getEntitlementGroup =
  () => ({ type: REQUEST_ENTITLEMENT_GROUP_REFRESH });
export const getEntitlementEdition =
  (entitlementId: string) => ({ type: REQUEST_ENTITLEMENT_EDITION_REFRESH, payload: { entitlementId } });
