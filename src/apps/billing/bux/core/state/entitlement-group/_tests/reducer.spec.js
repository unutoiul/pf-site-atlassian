import { expect } from 'chai';
import sinon from 'sinon';
import testReducer, { testReducerStateReset } from '../../_helpers/metadata/testHelpers/reducer';
import { inActiveState } from '../../_helpers/metadata/stateCreators';
import { withMetadata } from '../../_helpers/metadata/reducer';
import {
  REQUEST_ENTITLEMENT_GROUP,
  REQUEST_ENTITLEMENT_EDITION,
} from '../actionTypes';
import reducer, {
  handleEntitlementGroup,
  handleEntitlementEdition,
  INITIAL_ENTITLEMENT_GROUP_STATE,
  INITIAL_ENTITLEMENT_EDITION_STATE,
  entitlementInternalStoreBuilder
} from '../reducer';


describe('entitlement  reducer', () => {
  describe('root reducer', () => {
    let clock;
    beforeEach(() => {
      clock = sinon.useFakeTimers(1);
    });

    afterEach(() => {
      clock.restore();
    });

    it('should process events for entitlementGroup', () => {
      const result = reducer(undefined, { type: REQUEST_ENTITLEMENT_GROUP, payload: { string: 'entitlementGroup' } });
      expect(result).to.deep.equal(entitlementInternalStoreBuilder(inActiveState({ string: 'entitlementGroup' })));
    });

    it('should process events for entitlementEdition', () => {
      const result = reducer(undefined, {
        type: REQUEST_ENTITLEMENT_EDITION,
        payload: {
          entitlementId: 'id',
          edition: {
            payload: {
              string: 'entitlementEdition'
            }
          }
        }
      });
      expect(result).to.deep.equal(entitlementInternalStoreBuilder(undefined, {
        id: inActiveState({ string: 'entitlementEdition' })
      }));
    });
  });

  describe('EntitlementGroup', () => {
    testReducer(handleEntitlementGroup, REQUEST_ENTITLEMENT_GROUP, INITIAL_ENTITLEMENT_GROUP_STATE);
    testReducerStateReset(handleEntitlementGroup, INITIAL_ENTITLEMENT_GROUP_STATE);
  });

  describe('EntitlementEdition', () => {
    testReducer((state, action) => {
      const ret = handleEntitlementEdition({
        entitlementId: state || withMetadata({})
      }, {
        ...action,
        payload: {
          entitlementId: 'entitlementId',
          edition: {
            payload: action.payload
          }
        }
      });
      return ret.entitlementId;
    }, REQUEST_ENTITLEMENT_EDITION, INITIAL_ENTITLEMENT_EDITION_STATE);
  });
});
