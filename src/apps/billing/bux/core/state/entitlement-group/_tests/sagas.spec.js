import { expect } from 'chai';
import sinon from 'sinon';
import { call, all } from 'redux-saga/effects';
import { getEntitlementGroup } from 'bux/common/api/billing-ux-service';
import fetchSaga from '../../_helpers/metadata/saga';
import entitlementSaga, {
  callToGetEntitlementEdition,

  getEntitlementEditionAdapter
} from '../sagas';

import {
  REQUEST_ENTITLEMENT_GROUP,
  REQUEST_ENTITLEMENT_GROUP_REFRESH,
  REQUEST_ENTITLEMENT_EDITION,
  REQUEST_ENTITLEMENT_EDITION_REFRESH,
} from '../actionTypes';

describe('entitlement sagas', () => {
  describe('default export (root saga)', () => {
    it('should export 2 sub sagas', () => {
      const saga = entitlementSaga();
      const sagas = saga.next().value;
      expect(sagas).to.be.deep.equal(all([
        call(fetchSaga, REQUEST_ENTITLEMENT_GROUP_REFRESH, getEntitlementGroup, REQUEST_ENTITLEMENT_GROUP),
        call(
          fetchSaga,
          REQUEST_ENTITLEMENT_EDITION_REFRESH,
          callToGetEntitlementEdition,
          REQUEST_ENTITLEMENT_EDITION
        )
      ]));
    });
  });

  describe('entitlementEdition', () => {
    it('it should wrap getEntitlementEdition', () => {
      const spy = sinon.stub();
      spy.returns(Promise.resolve({ string: '42' }));

      const args = { entitlementId: 42, somethingElse: 42 };

      return getEntitlementEditionAdapter(args, spy).then((result) => {
        expect(result).to.be.deep.equal({
          entitlementId: 42,
          edition: {
            payload: {
              string: '42'
            }
          }
        });

        expect(spy).to.be.calledWithMatch(args);
      });
    });
  });
});
