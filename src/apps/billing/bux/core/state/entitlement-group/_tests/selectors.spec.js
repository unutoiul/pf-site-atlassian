import { expect } from 'chai';
import testMetadata from '../../_helpers/metadata/testHelpers/metadata';
import {
  getEntitlementGroupMetadata,
  getEntitlementEditionMetadata,
  getEntitlementGroup,
  getEntitlementEdition,
  getEntitlementById,
  isEntitlementActive,
} from '../selectors';
import { entitlementStoreBuilder } from '../reducer';


const mockData = (data = {}) => ({
  data: false, isFetching: false, error: false, lastUpdated: false, ...data
});

const mockGroupState = (data = {}) =>
  entitlementStoreBuilder(mockData(data));

const mockEditionState = (data = {}) =>
  entitlementStoreBuilder(undefined, { edition: mockData(data) });


describe('Entitlement selectors:', () => {
  describe('getEntitlementGroupMetadata:', () => {
    testMetadata(mockGroupState, getEntitlementGroupMetadata);
  });

  describe('getEntitlementGroupMetadata:', () => {
    testMetadata(mockEditionState, getEntitlementEditionMetadata('edition'));
  });

  describe('getEntitlementGroup:', () => {
    it('should return group from state', () => (
      expect(getEntitlementGroup(mockGroupState({ data: 'payload' }))).to.equal('payload')
    ));
  });

  describe('getEntitlementEdition:', () => {
    it('should return group from state', () => (
      expect(getEntitlementEdition('edition')(mockEditionState({ data: 'payload' }))).to.equal('payload')
    ));
  });

  describe('getEntitlementById:', () => {
    const state = { data: { entitlements: [{ id: '123' }] } };
    it('should return entitlement', () => {
      expect(getEntitlementById('123')(mockGroupState(state))).to.be.deep.equal({ id: '123' });
    });
    it('should return undefined', () => {
      expect(getEntitlementById('1234')(mockGroupState(state))).to.be.undefined();
    });
  });

  describe('isEntitlementActive:', () => {
    const state = {
      data: {
        entitlements: [
          { id: 'active', status: 'ACTIVE' },
          { id: 'inactive', status: 'INACTIVE' },
          { id: 'also_inactive', status: 'ACTIVE', suspended: true }
        ]
      }
    };
    it('should return true for active product', () => {
      expect(isEntitlementActive('active')(mockGroupState(state))).to.be.true();
    });
    it('should return false for inactive product', () => {
      expect(isEntitlementActive('inactive')(mockGroupState(state))).to.be.false();
      expect(isEntitlementActive('also_inactive')(mockGroupState(state))).to.be.false();
    });
  });
});
