import { BUXMetaReducedType } from 'bux/core/state/_helpers/metadata/types';

import { NAME } from './constants';

export type EntitlementEditionTransitionType = 'UPGRADE' | 'DOWNGRADE';
export type Status = 'ACTIVE' | 'INACTIVE';

export interface EntitlementEditionType {
  name: string;
}

export interface EntitlementType {
  creationDate: string;
  name: string;
  selectedEdition: string;
  futureEdition: string;
  futureEditionTransition: EntitlementEditionTransitionType | '';
  trialEndDate: string;
  entitlementGroupId: string;
  productKey: string;
  endDate: string;
  status: Status;
  accountId: string;
  sen: string;
  startDate: string;
  id: string;
  suspended: boolean;
  editionTransitions: EntitlementEditionTransitionType[];
}

export type RenewalPeriod = 'MONTHLY' | 'ANNUALLY';

export interface Entitlements {
  invoiceContactId: string;
  name: string;
  creationDate: string;
  currentBillingPeriod: {
    endDate: string,
    startDate: string,
    renewalFrequency: RenewalPeriod,
  };
  billingDetailsId: string;
  status: Status;
  license: string;
  entitlements: EntitlementType[];
  accountId: string;
  id: string;
  autoRenewed: true;
  sen: string;
}

export interface EditionType {
  edition: string;
  needsPaymentDetailsOnAccount: string;
  transition: string;
}

export interface EntitlementEdition {
  currentEdition: string;
  availableEditions: EditionType[];
}

export interface EntitlementsGroupState {
  [NAME]: {
    entitlementGroup: BUXMetaReducedType<Entitlements>,
    entitlementEditions: Record<string, BUXMetaReducedType<EntitlementEdition>>,
  };
}
