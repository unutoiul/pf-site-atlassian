import { all, call } from 'redux-saga/effects';

import {
  getEntitlementEdition,
  getEntitlementGroup,
} from 'bux/common/api/billing-ux-service';

import fetchSaga from '../_helpers/metadata/saga';
import {
  REQUEST_ENTITLEMENT_EDITION,
  REQUEST_ENTITLEMENT_EDITION_REFRESH,
  REQUEST_ENTITLEMENT_GROUP,
  REQUEST_ENTITLEMENT_GROUP_REFRESH,
} from './actionTypes';

export const getEntitlementEditionAdapter = (args, apiCall) =>
  apiCall(args)
    .then(edition => ({
      entitlementId: args.entitlementId,
      edition: {
        payload: edition,
      },
    }));

export const callToGetEntitlementEdition = args =>
  getEntitlementEditionAdapter(args, getEntitlementEdition);

function* rootSaga() {
  yield all([
    call(fetchSaga, REQUEST_ENTITLEMENT_GROUP_REFRESH, getEntitlementGroup, REQUEST_ENTITLEMENT_GROUP),
    call(fetchSaga, REQUEST_ENTITLEMENT_EDITION_REFRESH, callToGetEntitlementEdition, REQUEST_ENTITLEMENT_EDITION),
  ]);
}

export default rootSaga;
