import { combineReducers } from 'redux';
import { handleActions } from 'redux-actions';

import { RESET_STATE } from '../../action-types';
import reducer, { withMetadata } from '../_helpers/metadata/reducer';
import {
  REQUEST_ENTITLEMENT_EDITION,
  REQUEST_ENTITLEMENT_GROUP,
} from './actionTypes';
import { NAME } from './constants';

export const INITIAL_ENTITLEMENT_GROUP_STATE = withMetadata({
  invoiceContactId: '',
  name: '',
  currentBillingPeriod: {},
  billingDetailsId: '',
  status: '',
  entitlements: [],
});

export const INITIAL_ENTITLEMENT_EDITION_STATE = withMetadata({});

export const handleEntitlementGroup = handleActions({
  [REQUEST_ENTITLEMENT_GROUP]: reducer,
  [RESET_STATE]: () => INITIAL_ENTITLEMENT_GROUP_STATE,
}, INITIAL_ENTITLEMENT_GROUP_STATE);

export const handleEntitlementEdition = handleActions({
  [REQUEST_ENTITLEMENT_EDITION]: (state, { error, payload }) => {
    if (!payload) {
      return { ...state };
    }
    const { entitlementId, edition = {} } = payload;

    return {
      ...state,
      [entitlementId]: reducer(state[entitlementId] || INITIAL_ENTITLEMENT_EDITION_STATE, { error, ...edition }),
    };
  },
}, {});

export const entitlementInternalStoreBuilder = (
  entitlementGroup = INITIAL_ENTITLEMENT_GROUP_STATE,
  entitlementEditions = {},
) => ({
  entitlementGroup,
  entitlementEditions,
});

export const entitlementStoreBuilder = (
  entitlementGroup: any = {},
  entitlementEditions: any = {},
  // entitlementChangeEditionEstimate = {},
) => ({
  [NAME]: entitlementInternalStoreBuilder(entitlementGroup, entitlementEditions),
});

const combinedReducer = combineReducers({
  entitlementGroup: handleEntitlementGroup,
  entitlementEditions: handleEntitlementEdition,
});

export default combinedReducer;
