import { AnyAction } from 'redux';
import { all, call, put, take, takeLatest } from 'redux-saga/effects';

import { onlyWithPayload } from '../_helpers/metadata/saga';
import { resubscribe as requestResubscribe } from '../api';
import { getBillEstimate } from '../bill-estimate/actions';
import { getEntitlementGroup } from '../entitlement-group/actions';
import { REQUEST_ENTITLEMENT_GROUP } from '../entitlement-group/actionTypes';
import { showNotificationByTemplate, showNotificationNetworkError } from '../notifications/actions';
import { RESUBSCRIBE, RESUBSCRIBE_RESPONSE } from './actionTypes';
import { resubscribeRoutine } from './routines';

export const REACTIVATION_NOTIFICATION_ID = 'resubscribe';

export const showResubscribeComplete = () => showNotificationByTemplate(REACTIVATION_NOTIFICATION_ID);

export const ENTITLEMENT_UPDATED = onlyWithPayload(REQUEST_ENTITLEMENT_GROUP);

export function* resubscribe({ payload: productKeys }: AnyAction) {
  try {
    yield put.resolve(requestResubscribe({ productKeys }));
    yield put(getBillEstimate());
    yield put(getEntitlementGroup());
    yield take(ENTITLEMENT_UPDATED);
    yield put(showResubscribeComplete());
    yield put(resubscribeRoutine.success());
  } catch (error) {
    yield* [
      put(showNotificationNetworkError()),
      put(resubscribeRoutine.failure()),
      put({ type: RESUBSCRIBE_RESPONSE, error }),
    ];
  }
}

export function* awaitForResubscribe() {
  yield takeLatest(RESUBSCRIBE, resubscribe);
}

function* rootSaga() {
  yield all([
    call(awaitForResubscribe),
  ]);
}

export default rootSaga;
