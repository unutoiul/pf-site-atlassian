import { promisifyRoutine } from 'redux-saga-routines';

import { RESUBSCRIBE } from './actionTypes';
import { resubscribeRoutine } from './routines';

const resubscribeAction = payload => ({ payload, type: RESUBSCRIBE });
const resubscribePromiseCreator = promisifyRoutine(resubscribeRoutine);

export const resubscribe = payload => (dispatch) => {
  dispatch(resubscribeAction(payload));

  return resubscribePromiseCreator(payload, dispatch);
};
