import { expect } from 'chai';
import { takeLatest, call, put, all, take } from 'redux-saga/effects';
import { showNotificationNetworkError } from 'bux/core/state/notifications/actions';
import { getEntitlementGroup } from '../../entitlement-group/actions';
import { getBillEstimate } from '../../bill-estimate/actions';
import { resubscribe as requestResubscribe } from '../../api';
import { RESUBSCRIBE, RESUBSCRIBE_RESPONSE } from '../actionTypes';
import { resubscribeRoutine } from '../routines';
import * as resubscribeSaga from '../sagas';

describe('resubscribe', () => {
  const productKeys = ['productKey1', 'productKey2'];

  describe('root saga', () => {
    it('should export 1 sub sagas', () => {
      const saga = resubscribeSaga.default();
      expect(saga.next().value).to.be.deep.equal(all([
        call(resubscribeSaga.awaitForResubscribe)
      ]));
    });
  });

  describe('reactivate saga', () => {
    it('trigger', () => {
      const flow = resubscribeSaga.awaitForResubscribe();
      expect(flow.next().value).to.deep.equal(takeLatest(RESUBSCRIBE, resubscribeSaga.resubscribe));
    });

    it('normal flow', () => {
      const flow = resubscribeSaga.resubscribe({ payload: productKeys });
      expect(flow.next().value).to.be.deep.equal(put.resolve(requestResubscribe({ productKeys })));
      expect(flow.next().value).to.be.deep.equal(put(getBillEstimate()));
      expect(flow.next().value).to.be.deep.equal(put(getEntitlementGroup()));
      expect(flow.next().value).to.be.deep.equal(take(resubscribeSaga.ENTITLEMENT_UPDATED));
      expect(flow.next().value).to.be.deep.equal(put(resubscribeSaga.showResubscribeComplete()));
      expect(flow.next().value).to.be.deep.equal(put(resubscribeRoutine.success()));
      expect(flow.next().done).to.be.true();
    });

    it('error flow', () => {
      const flow = resubscribeSaga.resubscribe({ payload: productKeys });
      expect(flow.next().value).to.be.deep.equal(put.resolve(requestResubscribe({ productKeys })));
      expect(flow.throw('test error').value).to.be.deep.equal(put(showNotificationNetworkError()));
      expect(flow.next().value).to.be.deep.equal(put(resubscribeRoutine.failure()));
      expect(flow.next().value).to.be.deep.equal(put({ type: RESUBSCRIBE_RESPONSE, error: 'test error' }));
      expect(flow.next().done).to.be.true();
    });
  });
});
