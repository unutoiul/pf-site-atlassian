import { select } from 'redux-saga/effects';
import { getOrganizationId, getCloudId } from '../../meta/selectors';

function* provisionCallArg(payload) {
  const org = yield select(getOrganizationId);
  const cloudId = yield select(getCloudId);
  const callArg = { ...payload };

  callArg.options = { org, cloudId };

  return callArg;
}

export { provisionCallArg };
