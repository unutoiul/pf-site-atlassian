import { call, put, takeLatest } from 'redux-saga/effects';
// import { takeLeading } from './effects';
import { provisionCallArg } from './getCallArgs';

export const onlyWithPayload = ACTION_NAME => action => (
  action.type === ACTION_NAME && (action.payload || action.error)
);

const normalizePayload = ({ payload }) => {
  if (payload) {
    return { payload };
  }
  return {};
};

/**
 * @param {String} awaitForAction - action await for
 * @param {Function} fetcher - execute after it
 * @param {String} dispatchAction - actionName to dispatch with result
 * @returns {{fetch: Generator, saga: Generator}}
 */
export const createSaga = (awaitForAction, fetcher, dispatchAction) => {
  function* fetch(originalAction = {}) {
    const action = normalizePayload(originalAction);

    const callArg = yield* provisionCallArg(action.payload);

    try {
      yield put({ type: dispatchAction });
      const apiResponse = yield call(fetcher, callArg);
      yield put({ type: dispatchAction, payload: apiResponse });
    } catch (error) {
      yield put({ type: dispatchAction, error });
    }
  }

  function* saga() {
    yield takeLatest(awaitForAction, fetch);
  }

  return {
    fetch,
    saga
  };
};


const fetchSaga = (awaitForAction, fetcher, dispatchAction) =>
  createSaga(awaitForAction, fetcher, dispatchAction).saga();

export default fetchSaga;
