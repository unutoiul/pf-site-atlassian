export interface BUXMetaReducedType<T> {
  data: T;
  isFetching: boolean | undefined;
  error: false | { response: any } | undefined;
  lastUpdated: number | undefined;
}

export interface BUXMetaDataType {
  loading: boolean;
  display: boolean;
  error: any;
}

export interface BUXMetaActionType {
  payload: any;
  error: any;
}
