import { expect } from 'chai';
import { call, put, takeLatest, select } from 'redux-saga/effects';
import { getOrganizationId, getCloudId } from '../../../meta/selectors';
// import { takeLeading } from '../effects';
import { createSaga, onlyWithPayload } from '../saga';

describe('meta-saga', () => {
  const INPUT_ACTION = 'INPUT_ACTION';
  const OUT_ACTION = 'OUT_ACTION';
  const ACTION = () => {
  };

  const { saga, fetch } = createSaga(INPUT_ACTION, ACTION, OUT_ACTION);

  it('should wait for a ACTION and then start fetch process', () => {
    expect(saga().next().value).to.deep.equal(takeLatest(INPUT_ACTION, fetch));
  });

  it('should dispatch the response when the bill estimate is successfully fetched', () => {
    const flow = fetch();
    expect(flow.next().value).to.deep.equal(select(getOrganizationId));
    expect(flow.next(1).value).to.deep.equal(select(getCloudId));
    expect(flow.next(2).value).to.deep.equal(put({ type: OUT_ACTION }));
    expect(flow.next().value).to.deep.equal(call(ACTION, { options: { org: 1, cloudId: 2 } }));
    expect(flow.next({ totalAmountWithTax: 10 }).value)
      .to.deep.equal(put({ type: OUT_ACTION, payload: { totalAmountWithTax: 10 } }));
    expect(flow.next().done).to.be.true();
  });

  it('should dispatch an error when the bill estimate fails to be fetched', () => {
    const flow = fetch();
    expect(flow.next().value).to.deep.equal(select(getOrganizationId));
    expect(flow.next(1).value).to.deep.equal(select(getCloudId));
    expect(flow.next(2).value).to.deep.equal(put({ type: OUT_ACTION }));
    expect(flow.next().value).to.deep.equal(call(ACTION, { options: { org: 1, cloudId: 2 } }));

    const NotFoundError = new Error('Not found');
    expect(flow.throw(NotFoundError).value).to.deep.equal(put({ type: OUT_ACTION, error: NotFoundError }));
    expect(flow.next().done).to.be.true();
  });
});

describe('onlyWithPayload', () => {
  it('should return undefined when there is no payload or error', () => {
    expect(onlyWithPayload('ACTION')({ type: 'ACTION' })).to.be.undefined();
  });
  it('should return payload when there is payload', () => {
    expect(onlyWithPayload('ACTION')({ type: 'ACTION', payload: 'payload' })).to.be.equal('payload');
  });
  it('should return error when there is error', () => {
    expect(onlyWithPayload('ACTION')({ type: 'ACTION', error: 'error' })).to.be.equal('error');
  });
});
