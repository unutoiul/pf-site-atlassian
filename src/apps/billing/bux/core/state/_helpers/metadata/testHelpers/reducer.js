import { expect } from 'chai';
import sinon from 'sinon';
import { RESET_STATE } from 'bux/core/action-types';

const testMetadataReducer = (reducer, ACTION, INITIAL_STATE) => {
  describe('metadata-test', () => {
    it('should return the initial state', () => {
      expect(reducer(undefined, {})).to.deep.equal({
        data: INITIAL_STATE.data,
        isFetching: false,
        error: undefined,
        lastUpdated: undefined
      });
    });

    describe('when action is fired', () => {
      it('should set isFetching to false when the action includes an error', () => {
        const { isFetching } = reducer(undefined, { type: ACTION, error: 'something' });
        expect(isFetching).to.be.false();
      });

      it('should set isFetching to true when no error or payload is given', () => {
        const { isFetching } = reducer(undefined, { type: ACTION });
        expect(isFetching).to.be.true();
      });

      it('should set isFetching to false when the action includes a payload', () => {
        const { isFetching } = reducer(undefined, { type: ACTION, payload: { myData: true } });
        expect(isFetching).to.be.false();
      });

      it('should set data to the payload of the action when the action includes a payload', () => {
        const { data } = reducer(undefined, { type: ACTION, payload: { myData: true } });
        expect(data).to.deep.equal({ myData: true });
      });

      it('should update the lastUpdated timestamp when the action includes a payload', () => {
        const clock = sinon.useFakeTimers(Date.now());
        const { lastUpdated } = reducer(undefined, { type: ACTION, payload: { myData: true } });
        expect(lastUpdated).to.equal(Date.now());
        clock.restore();
      });

      it('should not update the lastUpdated timestamp when no error or payload is given', () => {
        const state = reducer({ lastUpdated: 'myValue' }, { type: ACTION });
        const { lastUpdated } = reducer(state, { type: ACTION });
        expect(lastUpdated).to.equal('myValue');
      });

      it('should not update the data when the action has no payload', () => {
        const { data } = reducer(undefined, { type: ACTION });
        expect(data).to.deep.equal(INITIAL_STATE.data);
      });

      it('should set error to the error of the action when the action includes an error', () => {
        const { error } = reducer(undefined, { type: ACTION, error: 'Something happened' });
        expect(error).to.equal('Something happened');
      });
    });
  });
};

export const testReducerStateReset = (reducer, INITIAL_STATE) => {
  const exampleState = {
    data: {
      list: ['abc'],
      number: 5
    }
  };

  describe('reset state', () => {
    it('should set state to initial state', () => {
      const state = reducer(exampleState, { type: RESET_STATE });
      expect(state).to.be.equal(INITIAL_STATE);
    });
  });
};

export default testMetadataReducer;
