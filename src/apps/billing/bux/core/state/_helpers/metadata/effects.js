import { call, fork, take } from 'redux-saga/effects';

/**
 * It takes "leading" action and ignores all incoming ones of the same type while the "leading" is still handled
 * TODO: Remove this when takeLeading from redux-saga is not on beta anymore.
 * https://github.com/redux-saga/redux-saga/releases/tag/v1.0.0-beta.1
 * @param pattern
 * @param saga
 * @param args
 * @returns {*}
 */
export function* takeLeading(pattern, saga, ...args) {
  const task = yield fork(function* taskToFork() {
    let firstTask = true;
    // eslint-disable-next-line no-constant-condition
    while (true) {
      const action = yield take(pattern);
      if (firstTask) {
        firstTask = false;
        yield call(saga, ...args.concat(action));
        firstTask = true;
      }
    }
  });
  return task;
}
