import { BUXMetaDataType, BUXMetaReducedType } from 'bux/core/state/_helpers/metadata/types';

const getMetadata = (state: BUXMetaReducedType<any> = {} as any): BUXMetaDataType => ({
  loading: !!(state.isFetching),
  display: !!(state.data && state.lastUpdated),
  error: state.error || false,
});

export { getMetadata };
