import { expect } from 'chai';

const testMetadata = (mockState, getMetaData) => {
  describe('getMetaData:', () => {
    it('should set display to true when data and lastUpdated timestamp is present', () => {
      const state = mockState({ data: 'stuff', lastUpdated: 111222333444 });
      expect(getMetaData(state).display).to.be.true();
    });

    it('should set display to false when data is not present', () => {
      const state = mockState({ data: null });
      expect(getMetaData(state).display).to.be.false();
    });

    it('should set display to false when data is present but lastUpdated timestamp is not', () => {
      const state = mockState({ data: 'stuff' });
      expect(getMetaData(state).display).to.be.false();
    });

    it('should set loading to true when isFetching is true', () => {
      const state = mockState({ isFetching: true });
      expect(getMetaData(state).loading).to.be.true();
    });

    it('should set loading to false when isFetching is false', () => {
      const state = mockState({ isFetching: false });
      expect(getMetaData(state).loading).to.be.false();
    });

    it('should set error to stored value when error is present', () => {
      const state = mockState({ error: 'an error' });
      expect(getMetaData(state).error).to.equal('an error');
    });

    it('should set error to false when error is not present', () => {
      const state = mockState({ error: null });
      expect(getMetaData(state).error).to.be.false();
    });
  });
};

export default testMetadata;