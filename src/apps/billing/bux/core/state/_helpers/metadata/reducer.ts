import identityFn from 'bux/common/helpers/identity';
import { BUXMetaActionType, BUXMetaReducedType } from 'bux/core/state/_helpers/metadata/types';

type sagaReduceMapper = (state: BUXMetaReducedType<any>, action: BUXMetaActionType) => BUXMetaReducedType<any>;

const sagaReducerMap =
  (mapper: (payload: any) => any): sagaReduceMapper =>
    (state = {} as any, action = {} as any) =>
      ({
        data: (action.payload && mapper(action.payload)) || state.data,
        isFetching: (!action.error && !action.payload),
        error: action.error,
        lastUpdated: (action.payload && Date.now()) || state.lastUpdated,
      });

type SagaReducer = sagaReduceMapper & {
  map(mapper: (payload: any) => any): sagaReduceMapper;
};

const sagaReducer: SagaReducer = sagaReducerMap(identityFn) as any;
sagaReducer.map = sagaReducerMap;

export const withMetadata =
  <T extends object>
  (state: T, metadata = {}): BUXMetaReducedType<T> => ({
    data: state,
    isFetching: false,
    error: undefined,
    lastUpdated: undefined,
    ...metadata,
  });

export default sagaReducer;
