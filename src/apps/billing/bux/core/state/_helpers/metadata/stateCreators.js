import { withMetadata } from './reducer';

export const inLoadingState = (data = {}) => withMetadata(data, {
  isFetching: true
});

export const inActiveState = (data = {}) => withMetadata(data, {
  lastUpdated: Date.now()
});
