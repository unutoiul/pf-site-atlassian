// eslint-disable-next-line import/no-extraneous-dependencies
import shallowEqual from 'shallowequal';
// eslint-disable-next-line import/no-extraneous-dependencies
import memoizeOne from 'memoize-one';

export const sagaWrapper = (func, name) => {
  const type = `BUX/API/${name}`;

  const actionCreator = (args, extra = {}) => ({
    type,
    payload: {
      func,
      args,
      ...extra
    }
  });

  const caller = args => dispatch => (
    new Promise((resolve, reject) => {
      dispatch(actionCreator(args, { resolve, reject }));
    })
  );

  // memoizing last action create to bypass tests
  // TODO: remove this after migration to saga-test-helpers
  const wrapper = memoizeOne(caller, shallowEqual);

  function frontWrapper(...args) {
    // hide the `this` to fix the tests
    return wrapper(...args);
  }

  frontWrapper.typeName = type;
  frontWrapper.actionCreator = actionCreator;

  return frontWrapper;
};
