import { expect } from 'chai';
import sinon from 'sinon';
import { all, takeEvery, select } from 'redux-saga/effects';
import { expectSaga } from 'redux-saga-test-plan';
import * as matchers from 'redux-saga-test-plan/matchers';
import { getOrganizationId } from '../../meta/selectors';
import * as API from '../index';
import apiSaga, { executeAPICall } from '../sagas';

describe('API saga', () => {
  it('should await for all APIs', () => {
    const flow = apiSaga();
    const awaits = Object
      .keys(API)
      .map(fn => takeEvery(API[fn].typeName, executeAPICall));

    expect(flow.next().value).to.deep.equal(all(awaits));
  });

  it('should execute API call', () => {
    const func = sinon.stub();
    const resolve = sinon.stub();
    const reject = sinon.stub();

    func.resolves(42);

    return expectSaga(executeAPICall, {
      payload: {
        func,
        args: { value: 24 },
        resolve,
        reject
      }
    })
      .provide([
        [select(getOrganizationId), 42],
        [matchers.call.fn(getOrganizationId), 43],
      ])
      .run()
      .then(() => {
        expect(resolve).to.have.been.calledWith(42);
        expect(reject).not.to.have.been.called();
      });
  });

  it('should execute API call', () => {
    const func = sinon.stub();
    const resolve = sinon.stub();
    const reject = sinon.stub();

    func.throws('error');

    return expectSaga(executeAPICall, {
      payload: {
        func,
        args: { value: 42 },
        resolve,
        reject
      }
    })
      .run()
      .then(() => {
        expect(func).to.have.been.calledWith({ options: { cloudId: null, org: null }, value: 42 });
        expect(reject).to.have.been.called();
        expect(resolve).not.to.have.been.called();
      });
  });
});
