import { expect } from 'chai';
import sinon from 'sinon';
import { sagaWrapper } from '../sagaWrapper';

describe('API saga / saga wrapper', () => {
  it('should wrap function', () => {
    const fn = sinon.stub().returns(43);
    const dispatch = sinon.stub().callsFake(({ payload: { resolve, args, func } }) => resolve(func(args)));

    const wrapper = sagaWrapper(fn, 'testName');

    expect(wrapper.typeName).to.be.equal('BUX/API/testName');
    expect(wrapper.actionCreator(42)).to.be.deep.equal({
      payload: {
        args: 42,
        func: fn,
      },
      type: 'BUX/API/testName'
    });

    return wrapper(42)(dispatch).then((result) => {
      expect(result).to.be.equal(43);
      expect(dispatch).to.be.calledWithMatch({
        type: 'BUX/API/testName',
        payload: {
          func: fn,
          args: 42
        }
      });
    });
  });
});