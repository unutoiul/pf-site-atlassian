import { all, call, takeEvery } from 'redux-saga/effects';
import { provisionCallArg } from '../_helpers/metadata/getCallArgs';
import * as API from './index';

export function* executeAPICall({
  payload: {
    func,
    args,
    resolve,
    reject
  }
}) {
  try {
    const callArg = yield* provisionCallArg(args);
    const result = yield call(func, callArg);
    if (resolve) {
      resolve(result);
    }
  } catch (e) {
    if (reject) {
      reject(e);
    }
  }
}

function* rootAPISaga() {
  yield all(Object
    .keys(API)
    .map(fn => takeEvery(API[fn].typeName, executeAPICall)));
}

export default rootAPISaga;
