import * as API from 'bux/common/api/billing-ux-service';
import { sagaWrapper } from './sagaWrapper';

const exports = {};
Object
  .keys(API)
  .forEach(key => (exports[key] = sagaWrapper(API[key], key)));

export const getRecommendedProducts = exports.getRecommendedProducts;
export const requestConvertToAnnual = exports.requestConvertToAnnual;
export const getAnnualConversionEstimate = exports.getAnnualConversionEstimate;
export const requestConvertToAnnualQuote = exports.requestConvertToAnnualQuote;
export const selectEdition = exports.selectEdition;
export const updatePaymentDetails = exports.updatePaymentDetails;
export const updatePaymentMethod = exports.updatePaymentMethod;
export const updateInvoiceContact = exports.updateInvoiceContact;
export const createCreditCardSession = exports.createCreditCardSession;
export const saveCreditCardSession = exports.saveCreditCardSession;

export const deactivate = exports.deactivate;
export const resubscribe = exports.resubscribe;
export const updateBillingContact = exports.updateBillingContact;
export const createQuoteForEstimate = exports.createQuoteForEstimate;
export const requestProcessOrder = exports.requestProcessOrder;
export const requestProcessOrderProgress = exports.requestProcessOrderProgress;
export const validateBillingDetails = exports.validateBillingDetails;
