import { AnyAction } from 'redux';
import { all, call, put, select, take, takeLatest } from 'redux-saga/effects';

import { showNotificationByTemplate, showNotificationNetworkError } from 'bux/core/state/notifications/actions';

import { onlyWithPayload } from '../_helpers/metadata/saga';
import { updateBillingContact } from '../api';
import { getBillingDetails } from '../billing-details/actions';
import { getUserMetadata } from '../meta/actions';
import { RECEIVE_META_USER } from '../meta/actionTypes';
import { getUser } from '../meta/selectors';
import { MAKE_ME_BILLING_CONTACT, UPDATE_BILLING_CONTACT, UPDATE_BILLING_CONTACT_RESPONSE } from './actionTypes';
import { updateBillingContactRoutine } from './routines';

export function* handleError(error) {
  yield put(updateBillingContactRoutine.failure());
  if (error && error.response && error.response.status === 408) {
    yield put(showNotificationNetworkError());
  } else {
    yield put(showNotificationByTemplate('update-billing-contact-error'));
  }
  yield put({ type: UPDATE_BILLING_CONTACT_RESPONSE, error });
}

export function* updateBillingContactSaga({ payload }: AnyAction) {
  try {
    const { email, main } = payload;
    yield put.resolve(updateBillingContact({ email, main }));
    yield put({ type: UPDATE_BILLING_CONTACT_RESPONSE });
    yield put(updateBillingContactRoutine.success());
    yield put(getBillingDetails());
  } catch (error) {
    yield call(handleError, error);
  }
}

export const RECEIVE_META_USER_UPDATED = onlyWithPayload(RECEIVE_META_USER);

export function* makeMeBillingContactSaga({ payload }: AnyAction) {
  try {
    const { main } = payload;
    yield put(getUserMetadata());
    yield take(RECEIVE_META_USER_UPDATED);
    const { email } = yield select(getUser);
    yield call(updateBillingContactSaga, { type: null, payload: { main, email } });
  } catch (error) {
    yield call(handleError, error);
  }
}

export function* awaitUpdate() {
  yield takeLatest(UPDATE_BILLING_CONTACT, updateBillingContactSaga);
}

export function* awaitMakeMe() {
  yield takeLatest(MAKE_ME_BILLING_CONTACT, makeMeBillingContactSaga);
}

function* rootSaga() {
  yield all([
    call(awaitUpdate),
    call(awaitMakeMe),
  ]);
}

export default rootSaga;
