import { createRoutine } from 'redux-saga-routines';

export const updateBillingContactRoutine = createRoutine('UPDATE_BILLING_CONTACT');
