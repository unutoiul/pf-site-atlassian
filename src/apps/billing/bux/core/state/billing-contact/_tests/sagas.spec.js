import { expect } from 'chai';
import sinon from 'sinon';
import { rewiremock } from 'bux/helpers/rewire-mock';
import { take, takeLatest, put, select, call } from 'redux-saga/effects';
import { showNotificationByTemplate, showNotificationNetworkError } from 'bux/core/state/notifications/actions';
import { updateBillingContact } from '../../api/index';
import { UPDATE_BILLING_CONTACT, UPDATE_BILLING_CONTACT_RESPONSE, MAKE_ME_BILLING_CONTACT } from '../actionTypes';
import { updateBillingContactRoutine } from '../routines';

describe('billing contact saga', () => {
  let billingContactSaga;
  let requestMetadata;
  let getUser;
  let getBillingDetails;
  let clock;

  beforeEach(() => {
    clock = sinon.useFakeTimers(Date.now());
    requestMetadata = sinon.stub();
    requestMetadata.returns(() => {});
    getUser = sinon.stub();
    getBillingDetails = sinon.stub().returns(() => {});

    billingContactSaga = rewiremock.proxy('../sagas', {
      'bux/core/state/meta/actions': {
        getUserMetadata: requestMetadata
      },
      'bux/core/state/meta/selectors': {
        getUser
      },
      'bux/core/state/billing-details/actions': {
        getBillingDetails
      }
    });
  });

  afterEach(() => {
    clock.restore();
  });

  describe('updateBillingContactSaga', () => {
    it('updateBillingContactSaga trigger', () => {
      const flow = billingContactSaga.awaitUpdate();
      expect(flow.next().value).to.deep.equal(takeLatest(
        UPDATE_BILLING_CONTACT,
        billingContactSaga.updateBillingContactSaga
      ));
    });

    it('updateBillingContactSaga normal flow', () => {
      const flow = billingContactSaga.updateBillingContactSaga({ payload: { email: 'email' } });
      expect(flow.next({ email: 'email' }).value)
        .to.be.deep.equal(put.resolve(updateBillingContact({ email: 'email', main: undefined })));
      expect(flow.next().value).to.be.deep.equal(put({ type: UPDATE_BILLING_CONTACT_RESPONSE }));
      expect(flow.next().value).to.eql(put(updateBillingContactRoutine.success()));
      expect(flow.next().value).to.eql(put(getBillingDetails()));
      expect(flow.next().done).to.be.true();
    });

    it('updateBillingContactSaga error flow', () => {
      const flow = billingContactSaga.updateBillingContactSaga({ payload: { email: 'email' } });
      expect(flow.next({ email: 'email' }).value)
        .to.be.deep.equal(put.resolve(updateBillingContact({ email: 'email', main: undefined })));
      expect(flow.throw('test error').value).to.eql(call(billingContactSaga.handleError, 'test error'));
      expect(flow.next().done).to.be.true();
    });
  });

  describe('makeMeBillingContactSaga', () => {
    it('makeMeBillingContactSaga trigger', () => {
      const flow = billingContactSaga.awaitMakeMe();
      expect(flow.next().value)
        .to.deep.equal(takeLatest(MAKE_ME_BILLING_CONTACT, billingContactSaga.makeMeBillingContactSaga));
    });

    it('makeMeBillingContactSaga normal flow', () => {
      const flow = billingContactSaga.makeMeBillingContactSaga({ payload: {} });
      expect(flow.next().value).to.be.deep.equal(put(requestMetadata()));
      expect(flow.next().value).to.be.deep.equal(take(billingContactSaga.RECEIVE_META_USER_UPDATED));
      expect(flow.next().value).to.be.deep.equal(select(getUser));
      expect(flow.next({ email: 'email', main: undefined }).value)
        .to.be.deep.equal(call(
          billingContactSaga.updateBillingContactSaga,
          { type: null, payload: { email: 'email', main: undefined } }
        ));
      expect(flow.next().done).to.be.true();
    });

    it('makeMeBillingContactSaga error flow', () => {
      const flow = billingContactSaga.makeMeBillingContactSaga({ payload: {} });
      expect(flow.next().value).to.be.deep.equal(put(requestMetadata()));
      expect(flow.throw('test error').value).to.eql(call(billingContactSaga.handleError, 'test error'));
      expect(flow.next().done).to.be.true();
    });
  });

  it('handleError server error', () => {
    const flow = billingContactSaga.handleError('test error');
    expect(flow.next().value).to.be.deep.equal(put(updateBillingContactRoutine.failure()));
    expect(flow.next().value).to.be.deep.equal(put(showNotificationByTemplate('update-billing-contact-error')));
    expect(flow.next().value).to.be.deep.equal(put({ type: UPDATE_BILLING_CONTACT_RESPONSE, error: 'test error' }));
  });

  it('handleError network error', () => {
    const error = { response: { status: 408 } };
    const flow = billingContactSaga.handleError(error);
    expect(flow.next().value).to.be.deep.equal(put(updateBillingContactRoutine.failure()));
    expect(flow.next().value).to.be.deep.equal(put(showNotificationNetworkError()));
    expect(flow.next().value).to.be.deep.equal(put({ type: UPDATE_BILLING_CONTACT_RESPONSE, error }));
  });
});
