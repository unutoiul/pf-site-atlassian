export const UPDATE_BILLING_CONTACT = 'bux/features/billing-contact/update/request';
export const UPDATE_BILLING_CONTACT_RESPONSE = 'bux/features/billing-contact/update/response';
export const MAKE_ME_BILLING_CONTACT = 'bux/features/billing-contact/make-me/request';
