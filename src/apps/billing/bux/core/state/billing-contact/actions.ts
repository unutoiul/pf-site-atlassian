import { promisifyRoutine } from 'redux-saga-routines';

import { MAKE_ME_BILLING_CONTACT, UPDATE_BILLING_CONTACT } from './actionTypes';
import { updateBillingContactRoutine } from './routines';

const updateBillingContactAction = payload => ({ payload, type: UPDATE_BILLING_CONTACT });
const updateBillingContactPromiseCreator = promisifyRoutine(updateBillingContactRoutine);

export const updateBillingContact = ({ main, email }: { main?: string, email?: string } = {}) => (dispatch) => {
  dispatch(updateBillingContactAction({ main, email }));

  return updateBillingContactPromiseCreator({ main, email }, dispatch);
};

const makeMeBillingContactAction = payload => ({ payload, type: MAKE_ME_BILLING_CONTACT });

export const makeMeBillingContact = ({ main }: { main?: string } = {}) => (dispatch) => {
  dispatch(makeMeBillingContactAction({ main }));

  return updateBillingContactPromiseCreator({ main }, dispatch);
};
