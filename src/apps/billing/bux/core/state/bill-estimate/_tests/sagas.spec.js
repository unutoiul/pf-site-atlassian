import { expect } from 'chai';
import { call, all, takeLatest } from 'redux-saga/effects';
import { getBillEstimate, getEditionChangeEstimate, getAnnualConversionEstimate }
  from 'bux/common/api/billing-ux-service';
import billEstimateSaga, { createQuote } from '../sagas';
import fetchSaga from '../../_helpers/metadata/saga';
import {
  REQUEST_BILL_ESTIMATE,
  REQUEST_BILL_ESTIMATE_REFRESH,
  REQUEST_BILL_ESTIMATE_CHANGE_EDITION_ESTIMATE_REFRESH,
  REQUEST_BILL_ESTIMATE_CHANGE_EDITION_ESTIMATE,
  REQUEST_BILL_ESTIMATE_ANNUAL_CONVERSION_ESTIMATE_REFRESH,
  REQUEST_BILL_ESTIMATE_ANNUAL_CONVERSION_ESTIMATE
} from '../actionTypes';
import { createQuoteForEstimateAction } from '../actions';

describe('bill-estimate sagas', () => {
  it('should use fetch saga', () => {
    const saga = billEstimateSaga();
    expect(saga.next().value).to.deep.equal(all([
      call(
        fetchSaga,
        REQUEST_BILL_ESTIMATE_REFRESH,
        getBillEstimate,
        REQUEST_BILL_ESTIMATE
      ),
      call(
        fetchSaga,
        REQUEST_BILL_ESTIMATE_CHANGE_EDITION_ESTIMATE_REFRESH,
        getEditionChangeEstimate,
        REQUEST_BILL_ESTIMATE_CHANGE_EDITION_ESTIMATE
      ),
      call(
        fetchSaga,
        REQUEST_BILL_ESTIMATE_ANNUAL_CONVERSION_ESTIMATE_REFRESH,
        getAnnualConversionEstimate,
        REQUEST_BILL_ESTIMATE_ANNUAL_CONVERSION_ESTIMATE
      ),
      takeLatest(createQuoteForEstimateAction.TRIGGER, createQuote),
    ]));
  });
});
