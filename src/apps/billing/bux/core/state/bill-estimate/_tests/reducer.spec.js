import testReducer, { testReducerStateReset } from '../../_helpers/metadata/testHelpers/reducer';
import {
  REQUEST_BILL_ESTIMATE, REQUEST_BILL_ESTIMATE_CHANGE_EDITION_ESTIMATE,
  REQUEST_BILL_ESTIMATE_ANNUAL_CONVERSION_ESTIMATE
} from '../actionTypes';
import {
  handleCurrentBillEstimate,
  handleChangeEditionEstimate,
  handleAnnualConversionEstimate,
  INITIAL_BILL_STATE,
  INITIAL_CHANGE_EDITION_ESTIMATE_STATE,
  INITIAL_ANNUAL_CONVERSION_ESTIMATE_STATE,
} from '../reducer';


describe('bill-estimate reducer', () => {
  testReducer(handleCurrentBillEstimate, REQUEST_BILL_ESTIMATE, INITIAL_BILL_STATE);
  testReducerStateReset(handleCurrentBillEstimate, INITIAL_BILL_STATE);
});
describe('change edition estimate reducer', () => {
  testReducer(
    handleChangeEditionEstimate,
    REQUEST_BILL_ESTIMATE_CHANGE_EDITION_ESTIMATE,
    INITIAL_CHANGE_EDITION_ESTIMATE_STATE
  );
  testReducerStateReset(handleChangeEditionEstimate, INITIAL_CHANGE_EDITION_ESTIMATE_STATE);
});
describe('convert to annual estimate reducer', () => {
  testReducer(
    handleAnnualConversionEstimate,
    REQUEST_BILL_ESTIMATE_ANNUAL_CONVERSION_ESTIMATE,
    INITIAL_ANNUAL_CONVERSION_ESTIMATE_STATE
  );
  testReducerStateReset(handleAnnualConversionEstimate, INITIAL_ANNUAL_CONVERSION_ESTIMATE_STATE);
});

