import { expect } from 'chai';
import testMetadata from '../../_helpers/metadata/testHelpers/metadata';
import { NAME, CURRENT, CHANGE_EDITION, SWITCH_TO_ANNUAL } from '../constants';
import {
  getBill,
  getProductList,
  getBillEstimateMetadata,
  getChangeEditionEstimateMetadata,
  getAnnualConversionEstimateMetadata,
  billFactory,
  getChangeEditionBill,
  upcomingCurrencyExtras,
  isNextBillInNewCurrency,
} from '../selectors';
import { getCombinedSortedLines } from '../helpers';

const mockStateFor = store => (data = {
  data: false, isFetching: false, error: false, lastUpdated: false
}) => ({
  [NAME]: {
    [store]: data
  }
});
const mockState = mockStateFor(CURRENT);
const mockStateWithBillEstimate = (billEstimate = {}) => mockState({ data: billEstimate });
const mockStateChangeEdition = mockStateFor(CHANGE_EDITION);
const mockStateConvertToAnnual = mockStateFor(SWITCH_TO_ANNUAL);

describe('Bill Estimate selectors:', () => {
  describe('getBillEstimateMetadata:', () => {
    it('should process regular metadata in a standard way', () => {
      testMetadata(mockState, getBillEstimateMetadata);
    });

    it('should have deactivated error when bill estimate returns locked due to instance deactivation', () => {
      const state = mockState({ error: { response: { status: 423 } } });
      expect(getBillEstimateMetadata(state).error).to.include({ errorKey: 'service.account.deactivated' });
    });

    it('should have default error data when bill estimate returns special status code', () => {
      const error = { response: { status: 423 } };
      const state = mockState({ error });
      const billEstimateMetadata = getBillEstimateMetadata(state);
      expect(billEstimateMetadata.error).to.include(error);
    });
  });

  describe('getChangeEditionEstimateMetadata:', () => {
    it('should process regular metadata in a standard way', () => {
      testMetadata(mockStateChangeEdition, getChangeEditionEstimateMetadata);
    });

    it('should have deactivated error when bill estimate returns locked due to instance deactivation', () => {
      const state = mockStateChangeEdition({ error: { response: { status: 423 } } });
      expect(getChangeEditionEstimateMetadata(state).error).to.include({ errorKey: 'service.account.deactivated' });
    });

    it('should have default error data when bill estimate returns special status code', () => {
      const error = { response: { status: 423 } };
      const state = mockStateChangeEdition({ error });
      const billEstimateMetadata = getChangeEditionEstimateMetadata(state);
      expect(billEstimateMetadata.error).to.include(error);
    });
  });

  describe('getAnnualConversionEstimateMetadata:', () => {
    it('should process regular metadata in a standard way', () => {
      testMetadata(mockStateConvertToAnnual, getAnnualConversionEstimateMetadata);
    });

    it('should have deactivated error when bill estimate returns locked due to instance deactivation', () => {
      const state = mockStateConvertToAnnual({ error: { response: { status: 423 } } });
      expect(getAnnualConversionEstimateMetadata(state).error).to.include({ errorKey: 'service.account.deactivated' });
    });

    it('should have default error data when bill estimate returns special status code', () => {
      const error = { response: { status: 423 } };
      const state = mockStateConvertToAnnual({ error });
      const billEstimateMetadata = getAnnualConversionEstimateMetadata(state);
      expect(billEstimateMetadata.error).to.include(error);
    });
  });

  describe('getProductList:', () => {
    const createProduct = (name, key) => ({
      productName: name,
      productKey: key,
      amountBeforeTax: 0,
      usage: []
    });

    it('should return empty array when lines object has zero item', () => (
      expect(getProductList(mockStateWithBillEstimate({ lines: [] }))).to.deep.equal([])
    ));

    it('getProductList should sort by name and key', () => {
      const productList = getProductList(mockStateWithBillEstimate({
        lines: [
          createProduct('lonfluence', 'b.lonfluence'),
          createProduct('lira', 'b.lira'),
          createProduct('lira', 'a.lira')
        ]
      }));
      expect(productList).to.deep.equal([
        {
          name: 'lira', key: 'a.lira', amount: 0, usage: [], entitlement: undefined
        },
        {
          name: 'lira', key: 'b.lira', amount: 0, usage: [], entitlement: undefined
        },
        {
          name: 'lonfluence', key: 'b.lonfluence', amount: 0, usage: [], entitlement: undefined
        }
      ]);
    });
  });

  describe('getFullProductList:', () => {
    const createProduct = (name, key, entitlement) => ({
      productName: name,
      productKey: key,
      amountBeforeTax: 0,
      usage: [],
      entitlementId: entitlement
    });
    const createEntitlement = (id, isActive) => ({
      id,
      status: isActive ? 'Active' : 'Inactive'
    });

    it('getProductList should sort by name and key', () => {
      const e1 = createEntitlement('e1', true);
      const e2 = createEntitlement('e2', true);
      const e3 = createEntitlement('e3', false);
      const productList = getCombinedSortedLines({
        products: [
          createProduct('lonfluence', 'b.lonfluence', 'e1'),
          createProduct('lira', 'b.lira', 'e2'),
          createProduct('lira', 'a.lira', 'e3')
        ],
        entitlements: [e1, e2, e3]
      });
      expect(productList).to.deep.equal([
        {
          name: 'lira', key: 'a.lira', amount: 0, usage: [], entitlement: e3
        },
        {
          name: 'lira', key: 'b.lira', amount: 0, usage: [], entitlement: e2
        },
        {
          name: 'lonfluence', key: 'b.lonfluence', amount: 0, usage: [], entitlement: e1
        }
      ]);
    });

    it('short should not differ from full productList', () => {
      const e1 = createEntitlement('e1', true);
      const e2 = createEntitlement('e2', true);
      const e3 = createEntitlement('e3', false);
      const state = {
        products: [
          createProduct('lonfluence', 'b.lonfluence', 'e1'),
          createProduct('lira', 'b.lira', 'e2'),
          createProduct('lira', 'a.lira', 'e3')
        ],
        entitlements: [e1, e2, e3]
      };
      expect(getCombinedSortedLines(state).map(x => ({
        ...x,
        entitlement: undefined
      }))).to.deep.equal(getProductList(mockStateWithBillEstimate({ lines: state.products })));
    });
  });
});

describe('billFactory:', () => {
  describe('getBill:', () => {
    it('should return from current state', () => {
      const state = mockStateWithBillEstimate('any data');
      expect(getBill(state)).to.be.equal('any data');
    });
    it('should return from change edition state', () => {
      const state = mockStateChangeEdition({ data: { billEstimate: 'any data' } });
      const { getBill: modifiedGetBill } = billFactory(getChangeEditionBill);
      expect(modifiedGetBill(state)).to.be.equal('any data');
    });
  });
});

describe('Upcoming Currency Extra', () => {
  it('should return upcoming currency extra if billestimate has upcomingCurrencyExtras', () => {
    const state = mockStateWithBillEstimate({ upcomingCurrencyExtras: 'upcomingCurrencyExtra' });
    expect(upcomingCurrencyExtras(state)).to.be.equal('upcomingCurrencyExtra');
  });

  it('should return undefined if billestimate does not have upcomingCurrencyExtra', () => {
    const state = mockStateWithBillEstimate({});
    expect(upcomingCurrencyExtras(state)).to.be.equal(undefined);
  });
});

describe('isNextBillInNewCurrency', () => {
  it('should return undefined if billestimate does not have upcomingCurrencyExtra', () => {
    const state = mockStateWithBillEstimate({});
    expect(isNextBillInNewCurrency(state)).to.be.equal(undefined);
  });

  it('should return true if upcomingCurrencyExtra falls in nextBillingPeriod', () => {
    const state = mockStateWithBillEstimate({ 
      nextBillingPeriod: {
        endDate: '2022-11-16',
        startDate: '2016-10-17'
      },
      upcomingCurrencyExtras: {
        period: {
          startDate: '2021-01-01T00:00:00.000-0600',
          endDate: '2023-02-01T00:00:00.000-0600',
        }
      }
    });
    expect(isNextBillInNewCurrency(state)).to.be.equal(true);
  });

  it('should return false if upcomingCurrencyExtra does not fall in nextBillingPeriod', () => {
    const state = mockStateWithBillEstimate({
      nextBillingPeriod: {
        endDate: '2022-11-16',
        startDate: '2016-10-17'
      },
      upcomingCurrencyExtras: {
        period: {
          startDate: '2025-01-01T00:00:00.000-0600',
          endDate: '2026-02-01T00:00:00.000-0600',
        }
      }
    });
    expect(isNextBillInNewCurrency(state)).to.be.equal(false);
  });
});
