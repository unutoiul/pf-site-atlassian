export const NAME = 'billEstimate';

export const CURRENT = 'current';
export const CHANGE_EDITION = 'changeEdition';
export const SWITCH_TO_ANNUAL = 'switchToAnnual';
