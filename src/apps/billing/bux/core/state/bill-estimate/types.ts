import { BUXMetaReducedType } from 'bux/core/state/_helpers/metadata/types';
import { CHANGE_EDITION, CURRENT, NAME, SWITCH_TO_ANNUAL } from 'bux/core/state/bill-estimate/constants';

export type ProductUsageType = {
  amountBeforeTax: number,
  taxRate: number,
  description: string,
  units: number,
  tiered?: boolean,
  unitCode: string,
  unlimited: boolean,
  taxAmount: number,
  amountWithTax: number,
} & {
  currentUnits: number,
  tiered: true,
};

export interface BillEstimateLineType {
  productKey: string;
  productName: string;
  usage: ProductUsageType[];
  selectedEdition: string;
  amountBeforeTax: number;
  amountWithTax: number;
  taxAmount: number;
  entitlementId: string;
}

interface Period {
  startDate: string;
  endDate: string;
  renewalFrequency: string;
}

export interface UpcomingCurrencyExtras {
  currencyCode: string;
  period: {
    startDate: string,
    endDate: string,
    renewalFrequency: string,
  };
}

export interface BillEstimateData {
  currentBillingPeriod: {
    endDate: string,
  };
  nextBillingPeriod: Period;
  totalTaxAmount: number;
  totalAmountWithTax: number;
  totalAmountBeforeTax: number;
  currencyCode: string;
  lines: BillEstimateLineType[];
  atlassianAccessExtras?: {
    trello: {
      period: Period,
      unitCount: number,
      additionalUnitCount: number,
      amountBeforeTax: number,
      amountWithTax: number,
    },
    grandfathering: {
      period: Period,
      amountBeforeTax: number,
      amountWithTax: number,
      pricingPlanSummary: {
        description: string,
      },
    },
  };
  upcomingCurrencyExtras: UpcomingCurrencyExtras;
}

export interface BillEstimateSubStates {
  [CURRENT]: BUXMetaReducedType<BillEstimateData>;
  [CHANGE_EDITION]: BUXMetaReducedType<{
    billEstimate: BillEstimateData;
    estimateId: string;
  }>;
  [SWITCH_TO_ANNUAL]: BUXMetaReducedType<{
    billEstimate: BillEstimateData;
    estimateId: string;
  }>;
}

export interface BillEstimateState {
  [NAME]: BillEstimateSubStates;
}
