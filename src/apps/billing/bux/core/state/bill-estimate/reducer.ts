import { combineReducers } from 'redux';
import { handleActions } from 'redux-actions';

import { RESET_STATE } from 'bux/core/action-types';
import { BillEstimateData } from 'bux/core/state/bill-estimate/types';

import sagaReducer, { withMetadata } from '../_helpers/metadata/reducer';
import {
  REQUEST_BILL_ESTIMATE,
  REQUEST_BILL_ESTIMATE_ANNUAL_CONVERSION_ESTIMATE,
  REQUEST_BILL_ESTIMATE_CHANGE_EDITION_ESTIMATE,
} from './actionTypes';
import { CHANGE_EDITION, CURRENT, SWITCH_TO_ANNUAL } from './constants';

const INITIAL_STATE: BillEstimateData = {
  currentBillingPeriod: {
    endDate: '',
  },
  nextBillingPeriod: {
    startDate: '',
    endDate: '',
    renewalFrequency: '',
  },
  totalTaxAmount: 0,
  totalAmountWithTax: 0,
  totalAmountBeforeTax: 0,
  currencyCode: '',
  lines: [],
  upcomingCurrencyExtras: {
    currencyCode: '',
    period: {
      startDate: '',
      endDate: '',
      renewalFrequency: '',
    },
  },
};

export const INITIAL_BILL_STATE = withMetadata(INITIAL_STATE);
export const INITIAL_CHANGE_EDITION_ESTIMATE_STATE = withMetadata({ billEstimate: INITIAL_STATE });
export const INITIAL_ANNUAL_CONVERSION_ESTIMATE_STATE = withMetadata({ billEstimate: INITIAL_STATE });

export const handleCurrentBillEstimate = handleActions({
  [REQUEST_BILL_ESTIMATE]: sagaReducer,
  [RESET_STATE]: () => INITIAL_BILL_STATE,
}, INITIAL_BILL_STATE);

export const handleChangeEditionEstimate = handleActions({
  [REQUEST_BILL_ESTIMATE_CHANGE_EDITION_ESTIMATE]: sagaReducer,
  [RESET_STATE]: () => INITIAL_CHANGE_EDITION_ESTIMATE_STATE,
}, INITIAL_CHANGE_EDITION_ESTIMATE_STATE);

export const handleAnnualConversionEstimate = handleActions({
  [REQUEST_BILL_ESTIMATE_ANNUAL_CONVERSION_ESTIMATE]: sagaReducer,
  [RESET_STATE]: () => INITIAL_ANNUAL_CONVERSION_ESTIMATE_STATE,
}, INITIAL_ANNUAL_CONVERSION_ESTIMATE_STATE);

const combinedReducer = combineReducers({
  [CURRENT]: handleCurrentBillEstimate,
  [CHANGE_EDITION]: handleChangeEditionEstimate,
  [SWITCH_TO_ANNUAL]: handleAnnualConversionEstimate,
});

export default combinedReducer;
