export const REQUEST_BILL_ESTIMATE = 'bux/features/bill-estimate/actions/api-request';
export const REQUEST_BILL_ESTIMATE_REFRESH = 'bux/features/bill-estimate/actions/api-refresh';

export const REQUEST_BILL_ESTIMATE_CHANGE_EDITION_ESTIMATE =
  'bux/features/bill-estimate/actions/change-edition-estimate/api-request';
export const REQUEST_BILL_ESTIMATE_CHANGE_EDITION_ESTIMATE_REFRESH =
  'bux/features/bill-estimate/actions/change-edition-estimate/api-refresh';

export const CREATE_QUOTE_FOR_ESTIMATE = 'bux/features/bill-estimate/actions/create-quote-for-estimate';

export const REQUEST_BILL_ESTIMATE_ANNUAL_CONVERSION_ESTIMATE =
  'bux/features/bill-estimate/actions/annual-conversion-estimate/api-request';
export const REQUEST_BILL_ESTIMATE_ANNUAL_CONVERSION_ESTIMATE_REFRESH =
  'bux/features/bill-estimate/actions/annual-conversion-estimate/api-refresh';
