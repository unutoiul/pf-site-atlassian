import { isDateInThePast } from 'bux/common/formatters/date-formatter';
import { ANNUAL, MONTHLY } from 'bux/core/constants/frequency';
import { getMetadata } from 'bux/core/state/_helpers/metadata/selectors';
import { BUXMetaReducedType } from 'bux/core/state/_helpers/metadata/types';
import { BillEstimateData } from 'bux/core/state/bill-estimate/types';
import { getEntitlements } from 'bux/core/state/entitlement-group/selectors';
import { isOrganization } from 'bux/core/state/meta/selectors';
import { createSelector, NonEmpty } from 'bux/helpers/reselect';

import { CHANGE_EDITION, CURRENT, NAME, SWITCH_TO_ANNUAL } from './constants';
import {
  getCombinedSortedLines,
  getSortedItemisedLines,
  getUserCount,
  isAtlassianAccess,
  isStrideFree,
} from './helpers';
import { BillEstimateState, BillEstimateSubStates } from './types';

const store = (state: BillEstimateState): BillEstimateSubStates => state[NAME];

const getCurrentStore = (state: BillEstimateState) => store(state)[CURRENT] || ({} as any);
const getChangeEditionStore = (state: BillEstimateState) => store(state)[CHANGE_EDITION] || ({} as any);
const getConvertToAnnualStore = (state: BillEstimateState) => store(state)[SWITCH_TO_ANNUAL] || {};

export const getCurrentBill = (state: BillEstimateState) => getCurrentStore(state).data || {};
export const getChangeEditionBill = (state: BillEstimateState) => getChangeEditionStore(state).data.billEstimate || {};
export const getChangeEditionEstimateId = (state: BillEstimateState) => getChangeEditionStore(state).data.estimateId;
export const getAnnualConversionEstimate = (state: BillEstimateState) => getConvertToAnnualStore(state).data.billEstimate || {};
export const getAnnualConversionEstimateId = (state: BillEstimateState) => getConvertToAnnualStore(state).data.estimateId;

/* tslint:disable no-shadowed-variable */
export const billFactory = (getBill: (s: BillEstimateState) => BillEstimateData) => {
  const getBillEstimateLines = (state: BillEstimateState) => getBill(state).lines || [];

  const getFullProductList = createSelector(
    getEntitlements,
    getBillEstimateLines,
    (entitlements, products) => getCombinedSortedLines({ entitlements, products }),
  );

  const getProductList = createSelector(
    getBillEstimateLines,
    getSortedItemisedLines,
  );

  const getNextBillingPeriodRenewalFrequency = (state: BillEstimateState) =>
    (getBill(state).nextBillingPeriod || {}).renewalFrequency;
  const isNextBillingPeriodRenewalFrequencyMonthly = (state: BillEstimateState) =>
    (getNextBillingPeriodRenewalFrequency(state) === MONTHLY);
  const isNextBillingPeriodRenewalFrequencyAnnual = (state: BillEstimateState) =>
    (getNextBillingPeriodRenewalFrequency(state) === ANNUAL);

  const isExpiredInstance = (state: BillEstimateState) => isDateInThePast(getBill(state).currentBillingPeriod.endDate);

  const hasOnlyStrideFree = (state) => {
    const lines = getBillEstimateLines(state);

    return lines.length === 1 && isStrideFree(lines[0]);
  };

  const getBillingPeriod = createSelector(getBill, bill => (bill ? bill.nextBillingPeriod : undefined));

  const hasAtlassianAccess = (state: BillEstimateState) => isOrganization(state as any) && getBillEstimateLines(state).some(isAtlassianAccess);

  const atlassianAccessExtras = (state: BillEstimateState) => getBill(state).atlassianAccessExtras || {};

  const getNextRenewalBillingDate = (state: BillEstimateState) => getBill(state).currentBillingPeriod.endDate;

  const getAtlassianAccessTotalUnitCount = createSelector(
    getBillEstimateLines,
    lines => getUserCount(lines.find(isAtlassianAccess)),
  );

  const getTotalCost = (state, excludeTax) => {
    const bill = getBill(state);

    return excludeTax ? bill.totalAmountBeforeTax : bill.totalAmountWithTax;
  };

  const upcomingCurrencyExtras = (state: BillEstimateState) => getBill(state).upcomingCurrencyExtras || undefined;

  const isNextBillInNewCurrency = (state: BillEstimateState) => upcomingCurrencyExtras(state) && isWithinDateRange(state, getBillingPeriod(state));

  const isWithinDateRange = (state: BillEstimateState, billingPeriod?: BillEstimateData['nextBillingPeriod']) => {
    if (!billingPeriod) {
      return false;
    }
    const billingPeriodStartDate = new Date(billingPeriod.startDate);
    const billingPeriodEndDate = new Date(billingPeriod.endDate);
    const upcomingCurrencyStartDate = new Date(upcomingCurrencyExtras(state).period.startDate);

    return billingPeriodStartDate >= upcomingCurrencyStartDate || upcomingCurrencyStartDate <= billingPeriodEndDate;
  };

  return {
    getBill,
    getBillEstimateLines,
    getFullProductList,
    getProductList,
    getNextBillingPeriodRenewalFrequency,
    isNextBillingPeriodRenewalFrequencyMonthly,
    isNextBillingPeriodRenewalFrequencyAnnual,
    isExpiredInstance,
    hasOnlyStrideFree,
    getBillingPeriod,
    hasAtlassianAccess,
    atlassianAccessExtras,
    getAtlassianAccessTotalUnitCount,
    getTotalCost,
    getNextRenewalBillingDate,
    isNextBillInNewCurrency,
    upcomingCurrencyExtras,
  };
};
/* tslint:enable */

export const getBillEstimateError = (state: BUXMetaReducedType<any>) => {
  const status = state.error && state.error.response ? state.error.response.status : null;
  // Bill estimate returns 423 (Locked) for instance which is deactivated
  if (status && status === 423) {
    return {
      ...state.error,
      errorKey: 'service.account.deactivated',
    };
  }

  return state.error;
};

export const getBillEstimateMetadata = createSelector(getCurrentStore, (state: BUXMetaReducedType<any> = NonEmpty) => ({
  ...getMetadata(state),
  error: getBillEstimateError(state) || false,
}));

export const getChangeEditionEstimateMetadata = createSelector(getChangeEditionStore, (state: BUXMetaReducedType<any> = NonEmpty) => ({
  ...getMetadata(state),
  error: getBillEstimateError(state) || false,
}));

export const getAnnualConversionEstimateMetadata = createSelector(
  getConvertToAnnualStore,
  (state) => ({
    ...getMetadata(state),
    error: getBillEstimateError(state) || false,
  }),
);

export const {
  getBill,
  getBillEstimateLines,
  getFullProductList,
  getProductList,
  getNextBillingPeriodRenewalFrequency,
  isNextBillingPeriodRenewalFrequencyMonthly,
  isNextBillingPeriodRenewalFrequencyAnnual,
  isExpiredInstance,
  hasOnlyStrideFree,
  getBillingPeriod,
  hasAtlassianAccess,
  atlassianAccessExtras,
  getAtlassianAccessTotalUnitCount,
  getTotalCost,
  getNextRenewalBillingDate,
  isNextBillInNewCurrency,
  upcomingCurrencyExtras,
} = billFactory(getCurrentBill);
