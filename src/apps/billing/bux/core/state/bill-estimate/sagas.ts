import { AnyAction } from 'redux';
import { all, call, put, takeLatest } from 'redux-saga/effects';

import {
  getAnnualConversionEstimate,
  getBillEstimate,
  getEditionChangeEstimate,
} from 'bux/common/api/billing-ux-service';
import { createQuoteForEstimate } from 'bux/core/state/api';
import { showNotificationByTemplate, showNotificationNetworkError } from 'bux/core/state/notifications/actions';

import fetchSaga from '../_helpers/metadata/saga';
import { createQuoteForEstimateAction } from './actions';
import {
  REQUEST_BILL_ESTIMATE,
  REQUEST_BILL_ESTIMATE_ANNUAL_CONVERSION_ESTIMATE,
  REQUEST_BILL_ESTIMATE_ANNUAL_CONVERSION_ESTIMATE_REFRESH,
  REQUEST_BILL_ESTIMATE_CHANGE_EDITION_ESTIMATE,
  REQUEST_BILL_ESTIMATE_CHANGE_EDITION_ESTIMATE_REFRESH,
  REQUEST_BILL_ESTIMATE_REFRESH,
} from './actionTypes';

function showError(error) {
  if (error && error.response && error.response.status === 408) {
    return put(showNotificationNetworkError());
  }

  return put(showNotificationByTemplate('create-quote-error'));
}

export function* createQuote({ payload }: AnyAction) {
  const { estimateId } = payload;

  try {
    const response = yield put.resolve(createQuoteForEstimate({ estimateId }));
    yield put(createQuoteForEstimateAction.success(response));
  } catch (error) {
    yield showError(error);
    yield put(createQuoteForEstimateAction.failure());
  }

  return yield put(createQuoteForEstimateAction.fulfill());
}

function* rootSaga() {
  yield all([
    call(fetchSaga, REQUEST_BILL_ESTIMATE_REFRESH, getBillEstimate, REQUEST_BILL_ESTIMATE),
    call(
      fetchSaga,
      REQUEST_BILL_ESTIMATE_CHANGE_EDITION_ESTIMATE_REFRESH,
      getEditionChangeEstimate,
      REQUEST_BILL_ESTIMATE_CHANGE_EDITION_ESTIMATE,
    ),
    call(
      fetchSaga,
      REQUEST_BILL_ESTIMATE_ANNUAL_CONVERSION_ESTIMATE_REFRESH,
      getAnnualConversionEstimate,
      REQUEST_BILL_ESTIMATE_ANNUAL_CONVERSION_ESTIMATE,
    ),
    takeLatest(createQuoteForEstimateAction.TRIGGER, createQuote),
  ]);
}

export default rootSaga;
