import * as sortBy from 'lodash.sortby';

import { combineProductAndEntitlement } from 'bux/core/selectors/common/entitlements';
import { BillEstimateLineType } from 'bux/core/state/bill-estimate/types';
import { EntitlementType } from 'bux/core/state/entitlement-group/types';

export const getUserCount = (item?: BillEstimateLineType) => {
  if (!item) {
    return 0;
  }
  if (!item.usage) {
    return 0;
  }
  const users = item.usage.find(u => u.unitCode === 'users');

  return users ? users.units : 0;
};

export type JoinedLinedItem = BillEstimateLineType & { entitlement?: EntitlementType };

const getItemisedLines = (lines: JoinedLinedItem[]) =>
  lines.map(line => ({
    key: line.productKey,
    name: line.productName,
    amount: line.amountBeforeTax,
    usage: line.usage,
    entitlement: line.entitlement,
  }));

export const getSortedItemisedLines = (lines: JoinedLinedItem[]) => sortBy(getItemisedLines(lines), ['name', 'key']);

export const getCombinedSortedLines =
  ({ entitlements, products }: { entitlements: EntitlementType[], products: BillEstimateLineType[] }) => (
    getSortedItemisedLines(
      products
        .map(product => combineProductAndEntitlement(product, entitlements))
        .map(({ product, entitlement }) => ({
          ...product,
          entitlement,
        })),
    )
  );

export const isStride = (line: BillEstimateLineType) => line.productKey === 'hipchat.cloud';
const isFreeEdition = (line: BillEstimateLineType) => line.selectedEdition && line.selectedEdition.toLowerCase() === 'free';
export const isStrideFree = (line: BillEstimateLineType) => isStride(line) && isFreeEdition(line);
export const isAtlassianAccess = (line: BillEstimateLineType) => line.productKey === 'com.atlassian.identity-manager';
