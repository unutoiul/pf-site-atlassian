import { createAction } from 'redux-actions';
import { createRoutine, promisifyRoutine } from 'redux-saga-routines';

import {
  CREATE_QUOTE_FOR_ESTIMATE,
  REQUEST_BILL_ESTIMATE_ANNUAL_CONVERSION_ESTIMATE_REFRESH,
  REQUEST_BILL_ESTIMATE_CHANGE_EDITION_ESTIMATE_REFRESH,
  REQUEST_BILL_ESTIMATE_REFRESH,
} from './actionTypes';

export const getBillEstimate = () => ({ type: REQUEST_BILL_ESTIMATE_REFRESH });

export const getEditionChangeEstimate = createAction(REQUEST_BILL_ESTIMATE_CHANGE_EDITION_ESTIMATE_REFRESH);
export const getAnnualConversionEstimate = createAction(REQUEST_BILL_ESTIMATE_ANNUAL_CONVERSION_ESTIMATE_REFRESH);

export const createQuoteForEstimateAction = createRoutine(CREATE_QUOTE_FOR_ESTIMATE);
export const createQuoteForEstimatePromise = promisifyRoutine(createQuoteForEstimateAction);
