import * as qs from 'query-string';

interface URL {
  pathname: string;
  search: string;
}

interface Args {
  org?: boolean;
}

const queryToString = (query: Record<string, string>) => {
  const queryString = decodeURIComponent(qs.stringify(query));

  return queryString.charAt(0) === '?' || queryString.length === 0 ? queryString : `?${queryString}`;
};

const addOrgToQueryString = (queryString: string, { org }: Args) => {
  const parsedQueryString = qs.parse(queryString);
  let params = { ...parsedQueryString };
  if (org) {
    params = { ...params, org };
  }

  return queryToString(params);
};

const transformObjectReactRouterUrl = (url: URL, args: Args) => ({
  ...url,
  search: addOrgToQueryString(url.search, args),
});

const transformStringUrl = (url: string, args: Args) => {
  const urlParts = url.split('?');
  const baseUrl = urlParts.shift();
  const queryString = urlParts.shift();

  return `${baseUrl}${addOrgToQueryString(queryString || '', args)}`;
};

const transformUrl = (url: string | URL, args: Args) => {
  if (typeof url === 'object') {
    return transformObjectReactRouterUrl(url, args);
  } else if (typeof url === 'string') {
    return transformStringUrl(url, args);
  }

  return url;
};

export default transformUrl;
