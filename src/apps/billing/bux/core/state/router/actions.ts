import { REDIRECT } from './actionTypes';

export const push = (url: string) => ({ type: REDIRECT, payload: { url } });
