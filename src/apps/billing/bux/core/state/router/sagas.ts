import { AnyAction } from 'redux';
import {
  call,
  put,
  takeLatest,
//  select
} from 'redux-saga/effects';

import {
  REDIRECT,
  REDIRECT_CALL,
} from './actionTypes';

export function* redirect({ payload }: AnyAction) {
  const { url } = payload;
  yield put({ type: REDIRECT_CALL, payload: url });
}

export function* redirectSaga() {
  yield takeLatest(REDIRECT, redirect);
}

function* rootSaga() {
  yield call(redirectSaga);
}

export default rootSaga;
