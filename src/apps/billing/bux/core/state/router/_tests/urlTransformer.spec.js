import { expect } from 'chai';
import urlTransformer from '../urlTransformer';

describe('urlTransformer module', () => {
  it('should transform url when to is an object', () => {
    expect(urlTransformer({ pathname: '/paymentdetails' }, { org: '123' }))
      .to.be.deep.equals({ pathname: '/paymentdetails', search: '?org=123' });
  });
  it('should transform url when to is an object and has other query params', () => {
    expect(urlTransformer({ pathname: '/paymentdetails', search: '?test=yes' }, { org: '123' }))
      .to.be.deep.equals({ pathname: '/paymentdetails', search: '?org=123&test=yes' });
  });
  it('should transform url when to is a string', () => {
    expect(urlTransformer('/paymentdetails', { org: '123' })).to.be.equals('/paymentdetails?org=123');
  });
  it('should transform url when to is a string and has other query params', () => {
    expect(urlTransformer('/paymentdetails?test=yes', { org: '123' })).to.be.equals('/paymentdetails?org=123&test=yes');
  });
  it('should not transform url when no arg is provided', () => {
    expect(urlTransformer('/paymentdetails?test=yes', {})).to.be.equals('/paymentdetails?test=yes');
    expect(urlTransformer('/paymentdetails', {})).to.be.equals('/paymentdetails');
  });
});
