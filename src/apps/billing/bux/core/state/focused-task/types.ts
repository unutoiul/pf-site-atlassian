import { NAME } from './constants';

export interface FocusedTaskType {
  component: number;
  props: Record<string, any>;
  logPrefix?: string;
}

export interface FocusedTasksType {
  focusedTask: FocusedTaskType[];
  modalDialog: FocusedTaskType[];
}

export interface FocusedTasksState {
  [NAME]: FocusedTasksType;
}
