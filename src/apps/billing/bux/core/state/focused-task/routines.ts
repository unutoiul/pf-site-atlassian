import { createRoutine } from 'redux-saga-routines';

export const modalDialogRoutine = createRoutine('MODAL_DIALOG');
