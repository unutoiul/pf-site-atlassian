import { combineReducers } from 'redux';
import { handleActions } from 'redux-actions';

import {
  CLOSE_FOCUSED_TASK,
  CLOSE_MODAL_DIALOG,
  OPEN_FOCUSED_TASK,
  OPEN_MODAL_DIALOG,
} from './action-types';

const handleFocusedTask = (openAction, closeAction) => handleActions({
  [openAction]: (_: any, { payload }) => ({ ...payload }),
  [closeAction]: () => null,
}, null);

const reducer = combineReducers({
  focusedTask: handleFocusedTask(OPEN_FOCUSED_TASK, CLOSE_FOCUSED_TASK),
  modalDialog: handleFocusedTask(OPEN_MODAL_DIALOG, CLOSE_MODAL_DIALOG),
});

export default reducer;
