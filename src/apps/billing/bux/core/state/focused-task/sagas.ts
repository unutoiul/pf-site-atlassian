import { AnyAction } from 'redux';
import { all, call, put, takeEvery } from 'redux-saga/effects';

import { CLOSE_MODAL_DIALOG } from './action-types';
import { modalDialogRoutine } from './routines';

export function* processModalCloseAction({ payload }: AnyAction) {
  if (payload && payload instanceof Error) {
    yield put(modalDialogRoutine.failure(payload));
  } else {
    yield put(modalDialogRoutine.success(payload));
  }
}

export function* modalDialogSaga() {
  yield takeEvery(CLOSE_MODAL_DIALOG, processModalCloseAction);
}

function* rootSaga() {
  yield all([
    call(modalDialogSaga),
  ]);
}

export default rootSaga;
