import { expect } from 'chai';
import {
  OPEN_FOCUSED_TASK,
  CLOSE_FOCUSED_TASK,
  OPEN_MODAL_DIALOG,
  CLOSE_MODAL_DIALOG
} from '../action-types';
import reducer from '../reducer';

describe('FocusedTask tests: ', () => {
  const testAction = (fieldName, openAction, closeAction) => {
    describe(`${fieldName}\`s actions`, () => {
      it('should store payload in state, when OPEN action is fired', () => {
        const result = reducer(undefined, { type: openAction, payload: { string: 'actionPayload' } });
        expect(result[fieldName]).to.deep.equal({ string: 'actionPayload' });
      });

      it('should remove payload from state, when CLOSE action is fired', () => {
        const result = reducer({
          [fieldName]: 'payload'
        }, {
          type: closeAction
        });
        expect(result[fieldName]).to.equal(null);
      });
    });
  };

  testAction('focusedTask', OPEN_FOCUSED_TASK, CLOSE_FOCUSED_TASK);
  testAction('modalDialog', OPEN_MODAL_DIALOG, CLOSE_MODAL_DIALOG);
});
