import { expect } from 'chai';
import createStore from 'bux/core/createStore';
import {
  OPEN_FOCUSED_TASK,
  CLOSE_FOCUSED_TASK,
  OPEN_MODAL_DIALOG,
  CLOSE_MODAL_DIALOG
} from '../action-types';
import {
  openModalDialog,
  openFocusedTask,
  closeModalDialog,
  closeFocusedTask
} from '../actions';

describe('FocusedTask actions: ', () => {
  describe('Modal dialog: ', () => {
    it('should create actions', () => {
      const data = { payload: 42 };
      const dispatch = (creator) => {
        const events = [];
        creator(event => events.push(event));
        return events;
      };

      const openTask = dispatch(openModalDialog(data));
      expect(openTask).to.have.length(2);
      expect(openTask[0]).to.be.deep.equal({ type: OPEN_MODAL_DIALOG, payload: data });
      expect(closeModalDialog(data)).to.be.deep.equal({ type: CLOSE_MODAL_DIALOG, payload: data });

      expect(openFocusedTask(data)).to.be.deep.equal({ type: OPEN_FOCUSED_TASK, payload: data });
      expect(closeFocusedTask(data)).to.be.deep.equal({ type: CLOSE_FOCUSED_TASK, payload: data });
    });

    it('should fulfill promises', () => {
      const store = createStore();
      const dispatch = store.dispatch;
      const data = { payload: 42 };
      const promise = dispatch(openModalDialog());
      dispatch(closeModalDialog(data));
      return promise
        .then((payload) => {
          expect(payload).to.be.undefined();
        });
    });

    it('should reject promises', () => {
      const store = createStore();
      const dispatch = store.dispatch;
      const promise = dispatch(openModalDialog());
      dispatch(closeModalDialog(new Error('error')));
      return promise
        .then(() => {
          throw new Error('should not be called');
        }, () => {
        });
    });
  });
});
