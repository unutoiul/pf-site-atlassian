import { expect } from 'chai';
import { NAME } from '../constants';
import { getFocusedTask, getModalDialog } from '../selectors';

describe('FocusedTask selector:  ', () => {
  const toState = payload => ({ [NAME]: payload });

  it('should not fail if state not exists', () => {
    getFocusedTask({});
  });

  it('getFocusedTask should return zero if no task exists', () => {
    expect(getFocusedTask(toState({}))).to.equal(undefined);
  });

  it('getFocusedTask should return task from store', () => {
    expect(getFocusedTask(toState({ focusedTask: 'task' }))).to.equal('task');
  });

  it('getModalDialog should return zero if no task exists', () => {
    expect(getModalDialog(toState({}))).to.equal(undefined);
  });

  it('getModalDialog should return task from store', () => {
    expect(getModalDialog(toState({ modalDialog: 'modal' }))).to.equal('modal');
  });
});