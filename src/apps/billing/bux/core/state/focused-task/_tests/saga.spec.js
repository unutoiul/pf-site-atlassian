import { expect } from 'chai';
import { call, all, takeEvery, put } from 'redux-saga/effects';
import rootSaga, {
  modalDialogSaga,
  processModalCloseAction
} from '../sagas';
import { CLOSE_MODAL_DIALOG } from '../action-types';
import { modalDialogRoutine } from '../routines';

describe('Focused-task sagas', () => {
  describe('default export (root saga)', () => {
    it('should export 1 sub sagas', () => {
      const saga = rootSaga();
      const sagas = saga.next().value;
      expect(sagas).to.be.deep.equal(all([
        call(modalDialogSaga)]));
    });
  });

  describe('Modal dialog', () => {
    it('should await for CLOSE_MODAL_DIALOG', () => {
      const iterator = modalDialogSaga();
      expect(iterator.next().value).to.eql(takeEvery(CLOSE_MODAL_DIALOG, processModalCloseAction));
      expect(iterator.next().done).to.equal(true);
    });


    describe('Promises', () => {
      it('should resolve promise', () => {
        const data = { payload: { data: 42 } };
        const iterator = processModalCloseAction(data);
        expect(iterator.next().value).to.eql(put(modalDialogRoutine.success(data.payload)));
        expect(iterator.next().done).to.equal(true);
      });

      it('should reject promise', () => {
        const data = { payload: new Error('42') };
        const iterator = processModalCloseAction(data);
        expect(iterator.next().value).to.eql(put(modalDialogRoutine.failure(data.payload)));
        expect(iterator.next().done).to.equal(true);
      });
    });
  });
});