import { FocusedTasksState } from 'bux/core/state/focused-task/types';

import { NAME } from './constants';

const getState = (state: FocusedTasksState) => state[NAME] || {};

export const getFocusedTask = state => getState(state).focusedTask;
export const getModalDialog = state => getState(state).modalDialog;
