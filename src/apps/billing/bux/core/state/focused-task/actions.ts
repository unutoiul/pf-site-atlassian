import { createAction } from 'redux-actions';
import { promisifyRoutine } from 'redux-saga-routines';

import {
  CLOSE_FOCUSED_TASK,
  CLOSE_MODAL_DIALOG,
  OPEN_FOCUSED_TASK,
  OPEN_MODAL_DIALOG,
} from './action-types';
import { modalDialogRoutine } from './routines';

// focused-tasks

export const openFocusedTask = createAction(OPEN_FOCUSED_TASK);
export const closeFocusedTask = createAction(CLOSE_FOCUSED_TASK);

// modals
const openModalDialogActionCreator = createAction(OPEN_MODAL_DIALOG);
const modalDialogPromiseCreator = promisifyRoutine(modalDialogRoutine);

export const openModalDialog = payload => (dispatch) => {
  dispatch(openModalDialogActionCreator(payload));

  return modalDialogPromiseCreator(payload, dispatch);
};

export const closeModalDialog = createAction(CLOSE_MODAL_DIALOG);
