export const NAME = 'billingDetails';

export const PAYMENT_METHOD_PAYPAL = 'PAYPAL_ACCOUNT';
export const PAYMENT_METHOD_CREDIT_CARD = 'TOKENIZED_CREDIT_CARD';
