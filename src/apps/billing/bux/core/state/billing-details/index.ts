import * as actions from './actions';
import * as constants from './constants';
import reducer from './reducer';
import saga from './sagas';
import * as selectors from './selectors';

export default {
  actions, constants, reducer, selectors, saga,
};
