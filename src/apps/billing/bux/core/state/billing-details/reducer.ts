import { handleActions } from 'redux-actions';

import { RESET_STATE } from '../../action-types';
import sagaReducer, { withMetadata } from '../_helpers/metadata/reducer';
import { REQUEST_BILLING_DETAILS } from './actionTypes';

export const INITIAL_BILLING_DETAILS_STATE = withMetadata({
  organisationName: '',
  managedByPartner: false,
  firstName: '',
  lastName: '',
  email: '',
  addressLine1: '',
  addressLine2: '',
  city: '',
  state: '',
  country: '',
  postcode: '',
  paymentMethod: '',
  creditCard: {},
  paypalAccount: {},
});

export default handleActions({
  [REQUEST_BILLING_DETAILS]: sagaReducer,
  [RESET_STATE]: () => INITIAL_BILLING_DETAILS_STATE,
}, INITIAL_BILLING_DETAILS_STATE);
