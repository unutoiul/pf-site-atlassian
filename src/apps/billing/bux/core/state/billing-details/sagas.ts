import { AnyAction } from 'redux';
import { all, call, put, takeLatest } from 'redux-saga/effects';

import {
  getBillingDetails,
  saveCreditCardSession,
} from 'bux/common/api/billing-ux-service';
import {
  showNotificationByTemplate, showNotificationNetworkError,
} from 'bux/core/state/notifications/actions';

import {
  updateInvoiceContact,
  updatePaymentDetails,
  updatePaymentMethod,
} from 'bux/core/state/api';
import {
  getBillEstimate,
} from 'bux/core/state/bill-estimate/actions';
import {
  getBillingDetails as getBillingDetailsAction,
  updateBillingDetailsAction,
  updatePaymentMethodAction,
} from 'bux/core/state/billing-details/actions';

import { notifyFailPaymentRequest } from '../../actions/payment-notification';
import fetchSaga from '../_helpers/metadata/saga';
import { REQUEST_BILLING_DETAILS, REQUEST_BILLING_DETAILS_REFRESH } from './actionTypes';
import { PAYMENT_METHOD_CREDIT_CARD } from './constants';

function showError(error) {
  if (error && error.response && error.response.status === 408) {
    return put(showNotificationNetworkError());
  }

  return put(showNotificationByTemplate('payment-error'));
}

function* tokenizeCreditCard(creditCard) {
  try {
    const { sessionId } = creditCard;
    if (sessionId) {
      const result = yield call(saveCreditCardSession, { token: sessionId });

      return { ...creditCard, token: result.creditCardToken };
    }
  } catch (error) {
    // If token acquisition fails, bail out.
    yield put(notifyFailPaymentRequest(error));
  }

  return creditCard;
}

function updateContact(details) {
  if (details.invoiceContact && details.invoiceContact.email) {
    return put.resolve(updateInvoiceContact(details));
  }

  return null;
}

export function* updatePaymentMethodSaga({ payload }: AnyAction) {
  const { paymentMethod, paypalAccount } = payload;
  let { creditCard } = payload;

  if (paymentMethod === PAYMENT_METHOD_CREDIT_CARD) {
    creditCard = yield* tokenizeCreditCard(creditCard);
    if (!creditCard.token) {
      yield put(updatePaymentMethodAction.failure());

      return yield put(updatePaymentMethodAction.fulfill());
    }
  }

  try {
    yield put.resolve(updatePaymentMethod({ paymentMethod, creditCard, paypalAccount }));
    yield put(getBillingDetailsAction());
    yield put(showNotificationByTemplate('payment-method-success'));
    yield put(updatePaymentMethodAction.success());
  } catch (error) {
    yield showError(error);
    yield put(updatePaymentMethodAction.failure());
  }

  return yield put(updatePaymentMethodAction.fulfill());
}

export function* updateBillingDetails({ payload }: AnyAction) {
  const { invoiceContactEmail, ...form } = payload;
  let { creditCard } = payload;

  if (creditCard) {
    creditCard = yield* tokenizeCreditCard(creditCard);
    if (!creditCard.token) {
      yield put(updateBillingDetailsAction.failure());

      return yield put(updateBillingDetailsAction.fulfill());
    }
  }

  // One we have the ccToken, this branch is more lenient.
  try {
    const details = {
      ...form,
      creditCard,
    };
    yield put.resolve(updatePaymentDetails({ details }));
    yield updateContact({ invoiceContact: { email: invoiceContactEmail } });
    yield put(getBillingDetailsAction());
    yield put(getBillEstimate());
    yield put(showNotificationByTemplate('billing-details-success'));
    yield put(updateBillingDetailsAction.success());
  } catch (error) {
    yield showError(error);
    yield put(updateBillingDetailsAction.failure());
  }

  return yield put(updateBillingDetailsAction.fulfill());
}

export default function* rootSaga() {
  yield all([
    call(fetchSaga, REQUEST_BILLING_DETAILS_REFRESH, getBillingDetails, REQUEST_BILLING_DETAILS),
    takeLatest(updateBillingDetailsAction.TRIGGER, updateBillingDetails),
    takeLatest(updatePaymentMethodAction.TRIGGER, updatePaymentMethodSaga),
  ]);
}
