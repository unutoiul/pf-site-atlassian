import { createRoutine, promisifyRoutine } from 'redux-saga-routines';

import { REQUEST_BILLING_DETAILS_REFRESH, UPDATE_BILLING_DETAILS, UPDATE_PAYMENT_METHOD } from './actionTypes';

export const getBillingDetails = () => ({ type: REQUEST_BILLING_DETAILS_REFRESH });

export const updateBillingDetailsAction = createRoutine(UPDATE_BILLING_DETAILS);
export const updateBillingDetailsPromise = promisifyRoutine(updateBillingDetailsAction);

export const updatePaymentMethodAction = createRoutine(UPDATE_PAYMENT_METHOD);
export const updatePaymentMethodPromise = promisifyRoutine(updatePaymentMethodAction);
