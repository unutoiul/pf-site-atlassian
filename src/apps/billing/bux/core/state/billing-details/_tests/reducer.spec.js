import testReducer, { testReducerStateReset } from '../../_helpers/metadata/testHelpers/reducer';
import { REQUEST_BILLING_DETAILS } from '../actionTypes';
import reducer, { INITIAL_BILLING_DETAILS_STATE } from '../reducer';

describe('billing-details reducer', () => {
  testReducer(reducer, REQUEST_BILLING_DETAILS, INITIAL_BILLING_DETAILS_STATE);
  testReducerStateReset(reducer, INITIAL_BILLING_DETAILS_STATE);
});
