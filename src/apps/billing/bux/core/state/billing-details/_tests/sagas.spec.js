import sinon from 'sinon';
import { rewiremock } from 'bux/helpers/rewire-mock';
import { expect } from 'chai';
import { testSaga, expectSaga } from 'redux-saga-test-plan';
import { throwError } from 'redux-saga-test-plan/providers';
import { call, all, put, takeLatest } from 'redux-saga/effects';
import { getBillingDetails } from 'bux/common/api/billing-ux-service';
import { getBillEstimate } from 'bux/core/state/bill-estimate/actions';
import {
  updateBillingDetailsAction,
  updatePaymentMethodAction,
  getBillingDetails as getBillingDetailsAction
} from 'bux/core/state/billing-details/actions';
import fetchSaga from 'bux/core/state/_helpers/metadata/saga';

import billingDetailsSaga, { updateBillingDetails, updatePaymentMethodSaga } from '../sagas';
import { REQUEST_BILLING_DETAILS, REQUEST_BILLING_DETAILS_REFRESH } from '../actionTypes';

describe('billing-details sagas', () => {
  it('should use fetch saga', () => {
    const saga = billingDetailsSaga();
    expect(saga.next().value).to.deep.equal(all([
      call(fetchSaga, REQUEST_BILLING_DETAILS_REFRESH, getBillingDetails, REQUEST_BILLING_DETAILS),
      takeLatest(updateBillingDetailsAction.TRIGGER, updateBillingDetails),
      takeLatest(updatePaymentMethodAction.TRIGGER, updatePaymentMethodSaga)
    ]));
  });
});

describe('update billing details saga: ', () => {
  // api calls
  let getBillingDetailsMock;
  let updateInvoiceContact;
  let saveCreditCardSession;
  let updatePaymentDetails;

  // Notification mocks
  let showNotificationByTemplate;
  let showNotificationNetworkError;
  let notifyFailPaymentRequest;

  let updateBillingDetailsSaga;

  beforeEach(() => {
    getBillingDetailsMock = sinon.stub().resolves();

    updateInvoiceContact = sinon.stub().returns({});
    updatePaymentDetails = sinon.stub().returns({});

    saveCreditCardSession = sinon.stub().resolves({
      creditCardToken: 'SESSION000000000001',
      creditCardSessionId: 'SESSION'
    });

    showNotificationByTemplate = sinon.stub().returns(() => {});
    showNotificationNetworkError = sinon.stub().returns(() => {});
    notifyFailPaymentRequest = sinon.stub().returns(I => J => I || J);

    updateBillingDetailsSaga = rewiremock.proxy('../sagas', {
      'bux/core/state/api': {
        updatePaymentDetails,
        updateInvoiceContact
      },
      'bux/common/api/billing-ux-service': {
        saveCreditCardSession,
        getBillingDetails: getBillingDetailsMock
      },
      'bux/core/actions/payment-notification': { notifyFailPaymentRequest },
      'bux/core/state/billing-details/actions': {
        getBillingDetails: getBillingDetailsAction,
        updateBillingDetailsAction
      },
      'bux/core/state/notifications/actions': {
        showNotificationByTemplate,
        showNotificationNetworkError
      }
    }).updateBillingDetails;
  });

  describe('while running the saga', () => {
    it('should get billing details after successfully updating billing details', () => {
      const payload = {
        data: 'content',
        creditCard: {
          sessionId: 'SESSION',
          creditCardNumber: 'xxx'
        },
        invoiceContactEmail: 'gsgritta@atlassian.com'
      };

      const details = {
        data: 'content',
        creditCard: {
          sessionId: 'SESSION',
          token: 'SESSION000000000001',
          creditCardNumber: 'xxx'
        }
      };

      return testSaga(updateBillingDetailsSaga, { payload })
        .next()
        .call(saveCreditCardSession, { token: 'SESSION' })
        .next({
          creditCardToken: 'SESSION000000000001',
          creditCardSessionId: 'SESSION'
        })
        .put.resolve(updatePaymentDetails({ details }))
        .next()
        .put.resolve(updateInvoiceContact({ invoiceContact: { email: 'gsgritta@atlassian.com' } }))
        .next()
        .put(getBillingDetailsAction())
        .next()
        .put(getBillEstimate())
        .next()
        .put(showNotificationByTemplate('payment-method-success'))
        .next()
        .put(updateBillingDetailsAction.success())
        .next()
        .put(updateBillingDetailsAction.fulfill())
        .next()
        .isDone();
    });

    describe('and there is no credit card data', () => {
      it('should save billing details and invoice contact', () => {
        const payload = {
          data: 'content',
          invoiceContactEmail: 'gsgritta@atlassian.com'
        };

        return testSaga(updateBillingDetailsSaga, { payload })
          .next()
          .next()
          .put.resolve(updateInvoiceContact({ invoiceContact: { email: 'gsgritta@atlassian.com' } }))
          .next()
          .put(getBillingDetailsAction())
          .next()
          .put(getBillEstimate())
          .next()
          .put(showNotificationByTemplate('payment-method-success'))
          .next()
          .put(updateBillingDetailsAction.success())
          .next()
          .put(updateBillingDetailsAction.fulfill())
          .next()
          .isDone();
      });
    });

    describe('and there is an unexpected error', () => {
      it('should showNotificationByTemplate with generic message if updatePaymentDetails throws', () => {
        const payload = {
          creditCard: {
            sessionId: 'SESSION',
          }
        };

        const details = {
          creditCard: {
            sessionId: 'SESSION',
            token: 'SESSION000000000001',
          }
        };

        return testSaga(updateBillingDetailsSaga, { payload })
          .next()
          .next({
            creditCardToken: 'SESSION000000000001',
            creditCardSessionId: 'SESSION'
          })
          .put.resolve(updatePaymentDetails({ details }))
          .throw({
            response: {
              status: 500
            }
          })
          .put(showNotificationByTemplate('payment-error'))
          .next()
          .put(updateBillingDetailsAction.failure())
          .next()
          .put(updateBillingDetailsAction.fulfill())
          .next()
          .isDone();
      });

      it('should showNotificationByTemplate if updatePaymentDetails throws', () => {
        const payload = {
          creditCard: {
            sessionId: 'SESSION',
          }
        };

        const details = {
          creditCard: {
            sessionId: 'SESSION',
            token: 'SESSION000000000001',
          }
        };

        return testSaga(updateBillingDetailsSaga, { payload })
          .next()
          .next({
            creditCardToken: 'SESSION000000000001',
            creditCardSessionId: 'SESSION'
          })
          .put.resolve(updatePaymentDetails({ details }))
          .throw(new Error('error'))
          .put(showNotificationByTemplate('payment-error'))
          .next()
          .put(updateBillingDetailsAction.failure())
          .next()
          .put(updateBillingDetailsAction.fulfill())
          .next()
          .isDone();
      });

      it('should showNotificationNetworkError if has timeout error', () => {
        const payload = {
          creditCard: {
            sessionId: 'SESSION',
          }
        };

        const details = {
          creditCard: {
            sessionId: 'SESSION',
            token: 'SESSION000000000001',
          }
        };

        const error = new Error();
        error.response = { status: 408 };

        return testSaga(updateBillingDetailsSaga, { payload })
          .next()
          .next({
            creditCardToken: 'SESSION000000000001',
            creditCardSessionId: 'SESSION'
          })
          .put.resolve(updatePaymentDetails({ details }))
          .throw(error)
          .put(showNotificationNetworkError())
          .next()
          .put(updateBillingDetailsAction.failure())
          .next()
          .put(updateBillingDetailsAction.fulfill())
          .next()
          .isDone();
      });
    });

    describe('and getting the credit card token fails', () => {
      it('should bail out of the normal saga execution with default error message', () => {
        saveCreditCardSession = sinon.stub().rejects();
        updateBillingDetailsSaga = rewiremock.proxy('../sagas', {
          'bux/core/state/api': {
            updatePaymentDetails,
            updateInvoiceContact,
          },
          'bux/common/api/billing-ux-service': {
            saveCreditCardSession,
            getBillingDetails: getBillingDetailsMock
          },
          'bux/core/actions/payment-notification': { notifyFailPaymentRequest },
          'bux/core/state/billing-details/actions': {
            getBillingDetails: getBillingDetailsAction,
            updateBillingDetailsAction
          },
          'bux/core/state/notifications/actions': {
            showNotificationByTemplate,
            showNotificationNetworkError
          }
        }).updateBillingDetails;

        const payload = {
          creditCard: {
            sessionId: 'SESSION',
          }
        };
        return testSaga(updateBillingDetailsSaga, { payload })
          .next()
          .throw(new Error('Error'))
          .put(notifyFailPaymentRequest())
          .next()
          .put(updateBillingDetailsAction.failure())
          .next()
          .put(updateBillingDetailsAction.fulfill())
          .next()
          .isDone();
      });
    });
  });

  describe('updatePaymentDetails:', () => {
    describe('when there is a credit card', () => {
      it('should update values in the server', (done) => {
        const payload = {
          rest1: 'stuff',
          rest2: 'stuff2',
          creditCard: {
            sessionId: 'SESSION',
            creditCardNumber: 'xxx'
          },
          invoiceContactEmail: 'gsgritta@atlassian.com'
        };
        expectSaga(updateBillingDetailsSaga, { payload })
          .put(getBillingDetailsAction())
          .put(showNotificationByTemplate('payment-method-success'))
          .put(updateBillingDetailsAction.success())
          .put(updateBillingDetailsAction.fulfill())
          .run()
          .then(() => {
            const details = {
              rest1: 'stuff',
              rest2: 'stuff2',
              creditCard: {
                sessionId: 'SESSION',
                token: 'SESSION000000000001',
                creditCardNumber: 'xxx'
              }
            };

            expect(saveCreditCardSession).to.have.been.calledWith({ token: 'SESSION' });
            expect(updatePaymentDetails).to.have.been.calledWith({ details });
            expect(updateInvoiceContact).to.have.been
              .calledWith({ invoiceContact: { email: 'gsgritta@atlassian.com' } });
            done();
          });
      });

      it('should show an error when we fail to save the session to TNS', (done) => {
        const payload = {
          rest1: 'stuff',
          rest2: 'stuff2',
          creditCard: {
            sessionId: 'SESSION',
            creditCardNumber: 'xxx'
          },
          invoiceContactEmail: 'gsgritta@atlassian.com'
        };

        const err = new Error();
        saveCreditCardSession = sinon.stub().rejects(err);
        updateBillingDetailsSaga = rewiremock.proxy('../sagas', {
          'bux/core/state/api': {
            updatePaymentDetails,
            updateInvoiceContact
          },
          'bux/common/api/billing-ux-service': {
            saveCreditCardSession,
            getBillingDetails: getBillingDetailsMock
          },
          'bux/core/actions/payment-notification': { notifyFailPaymentRequest },
          'bux/core/state/billing-details/actions': {
            getBillingDetails: getBillingDetailsAction,
            updateBillingDetailsAction
          },
          'bux/core/state/notifications/actions': {
            showNotificationByTemplate,
            showNotificationNetworkError
          }
        }).updateBillingDetails;

        expectSaga(updateBillingDetailsSaga, { payload })
          .put(notifyFailPaymentRequest(err))
          .put(updateBillingDetailsAction.failure())
          .put(updateBillingDetailsAction.fulfill())
          .run()
          .then(() => {
            expect(saveCreditCardSession).to.have.been.calledWith({ token: 'SESSION' });
            expect(notifyFailPaymentRequest).to.have.been.calledWith(err);
            done();
          });
      });

      it('should reset the local billing details', (done) => {
        const payload = {
          rest1: 'stuff',
          rest2: 'stuff2',
          creditCard: {
            sessionId: 'SESSION',
            creditCardNumber: 'xxx'
          },
          invoiceContactEmail: 'gsgritta@atlassian.com'
        };
        expectSaga(updateBillingDetailsSaga, { payload })
          .put(getBillingDetailsAction())
          .put(showNotificationByTemplate('payment-method-success'))
          .put(updateBillingDetailsAction.success())
          .put(updateBillingDetailsAction.fulfill())
          .run()
          .then(() => {
            expect(saveCreditCardSession).to.have.been.called();
            done();
          });
      });
    });

    describe('when there is no credit card update', () => {
      it('should update billing details and invoice contact on the server', (done) => {
        const payload = {
          rest1: 'stuff',
          rest2: 'stuff2',
          invoiceContactEmail: 'gsgritta@atlassian.com'
        };
        expectSaga(updateBillingDetailsSaga, { payload })
          .put(getBillingDetailsAction())
          .put(showNotificationByTemplate('payment-method-success'))
          .put(updateBillingDetailsAction.success())
          .put(updateBillingDetailsAction.fulfill())
          .run()
          .then(() => {
            const details = {
              rest1: 'stuff',
              rest2: 'stuff2',
              creditCard: undefined
            };

            expect(saveCreditCardSession).to.have.not.been.called();
            expect(updatePaymentDetails).to.have.been.calledWith({ details });
            expect(updateInvoiceContact).to.have.been
              .calledWith({ invoiceContact: { email: 'gsgritta@atlassian.com' } });
            done();
          });
      });

      it('should not update invoice contact when it is not provided', (done) => {
        const payload = {
          rest1: 'stuff',
          rest2: 'stuff2',
          creditCard: {
            sessionId: 'SESSION',
            creditCardNumber: 'xxx'
          }
        };
        expectSaga(updateBillingDetailsSaga, { payload })
          .put(getBillingDetailsAction())
          .put(showNotificationByTemplate('payment-method-success'))
          .put(updateBillingDetailsAction.success())
          .put(updateBillingDetailsAction.fulfill())
          .run()
          .then(() => {
            const details = {
              rest1: 'stuff',
              rest2: 'stuff2',
              creditCard: {
                sessionId: 'SESSION',
                token: 'SESSION000000000001',
                creditCardNumber: 'xxx'
              }
            };

            expect(updatePaymentDetails).to.have.been.calledWith({ details });
            expect(updateInvoiceContact).to.have.not.been.called();
            done();
          });
      });
    });

    it('should show an error if the update billing details request fails', () => {
      const payload = {
        rest1: 'stuff',
        rest2: 'stuff2',
        creditCard: {
          sessionId: 'SESSION',
          creditCardNumber: 'xxx'
        }
      };

      const err = new Error();
      updateBillingDetailsSaga = rewiremock.proxy('../sagas', {
        'bux/core/state/api': {
          updatePaymentDetails,
          updateInvoiceContact
        },
        'bux/common/api/billing-ux-service': {
          saveCreditCardSession,
          getBillingDetails: getBillingDetailsMock
        },
        'bux/core/actions/payment-notification': { notifyFailPaymentRequest },
        'bux/core/state/billing-details/actions': {
          getBillingDetails: getBillingDetailsAction,
          updateBillingDetailsAction
        },
        'bux/core/state/notifications/actions': {
          showNotificationByTemplate,
          showNotificationNetworkError
        }
      }).updateBillingDetails;

      return expectSaga(updateBillingDetailsSaga, { payload })
        .provide([
          [put.resolve(updatePaymentDetails()), throwError(err)]
        ])
        .put(showNotificationByTemplate('payment-error'))
        .put(updateBillingDetailsAction.failure())
        .put(updateBillingDetailsAction.fulfill())
        .run()
        .then(() => {
          expect(showNotificationByTemplate).to.have.been.called();
        });
    });
  });
});

describe('update payment method saga: ', () => {
  let proxyedUpdatePaymentMethodSaga;

  let updatePaymentMethod;
  let getBillingDetailsMock;
  let showNotificationByTemplate;
  let showNotificationNetworkError;
  let notifyFailPaymentRequest;
  let saveCreditCardSession;

  beforeEach(() => {
    getBillingDetailsMock = sinon.stub().resolves();

    updatePaymentMethod = sinon.stub().returns({});

    saveCreditCardSession = sinon.stub().resolves({
      creditCardToken: 'SESSION000000000001',
      creditCardSessionId: 'SESSION'
    });

    showNotificationByTemplate = sinon.stub().returns(() => {});
    showNotificationNetworkError = sinon.stub().returns(() => {});
    notifyFailPaymentRequest = sinon.stub().returns(I => J => I || J);

    proxyedUpdatePaymentMethodSaga = rewiremock.proxy('../sagas', {
      'bux/core/state/api': {
        updatePaymentMethod
      },
      'bux/common/api/billing-ux-service': {
        saveCreditCardSession,
        getBillingDetails: getBillingDetailsMock
      },
      'bux/core/actions/payment-notification': { notifyFailPaymentRequest },
      'bux/core/state/billing-details/actions': {
        getBillingDetails: getBillingDetailsAction,
        updatePaymentMethodAction
      },
      'bux/core/state/notifications/actions': {
        showNotificationByTemplate,
        showNotificationNetworkError
      }
    }).updatePaymentMethodSaga;
  });

  describe('payment methods: ', () => {
    it('should save credit card payment method', () => {
      const payload = {
        paymentMethod: 'TOKENIZED_CREDIT_CARD',
        creditCard: {
          sessionId: 'SESSION',
          creditCardNumber: 'xxx'
        }
      };
      const params = {
        paymentMethod: 'TOKENIZED_CREDIT_CARD',
        creditCard: {
          sessionId: 'SESSION',
          token: 'SESSION000000000001',
          creditCardNumber: 'xxx'
        }
      };

      return testSaga(proxyedUpdatePaymentMethodSaga, { payload })
        .next()
        .call(saveCreditCardSession, { token: 'SESSION' })
        .next({
          creditCardToken: 'SESSION000000000001',
          creditCardSessionId: 'SESSION'
        })
        .put.resolve(updatePaymentMethod(params))
        .next()
        .put(getBillingDetailsAction())
        .next()
        .put(showNotificationByTemplate('payment-method-success'))
        .next()
        .put(updatePaymentMethodAction.success())
        .next()
        .put(updatePaymentMethodAction.fulfill())
        .next()
        .isDone();
    });

    it('should save paypal payment method', () => {
      const payload = {
        paymentMethod: 'PAYPAL_ACCOUNT',
        paypalAccount: {
          email: 'lrezendelemos@atlassian.com'
        }
      };
      const params = {
        paymentMethod: 'PAYPAL_ACCOUNT',
        paypalAccount: {
          email: 'lrezendelemos@atlassian.com'
        }
      };

      return testSaga(proxyedUpdatePaymentMethodSaga, { payload })
        .next()
        .put.resolve(updatePaymentMethod(params))
        .next()
        .put(getBillingDetailsAction())
        .next()
        .put(showNotificationByTemplate('payment-method-success'))
        .next()
        .put(updatePaymentMethodAction.success())
        .next()
        .put(updatePaymentMethodAction.fulfill())
        .next()
        .isDone();
    });
  });

  describe('and there is an unexpected error', () => {
    it('should bail out if saveCreditCardSession fails', () => {
      saveCreditCardSession.rejects();
      const payload = {
        paymentMethod: 'TOKENIZED_CREDIT_CARD',
        creditCard: {
          sessionId: 'SESSION',
          creditCardNumber: 'xxx'
        }
      };
      return testSaga(proxyedUpdatePaymentMethodSaga, { payload })
        .next()
        .throw(new Error('Error'))
        .put(notifyFailPaymentRequest())
        .next()
        .put(updatePaymentMethodAction.failure())
        .next()
        .put(updatePaymentMethodAction.fulfill())
        .next()
        .isDone();
    });

    it('should showNotificationByTemplate with generic message if updatePaymentMethod throws', () => {
      const payload = {
        paymentMethod: 'TOKENIZED_CREDIT_CARD',
        creditCard: {
          sessionId: 'SESSION',
          creditCardNumber: 'xxx'
        }
      };
      const params = {
        paymentMethod: 'TOKENIZED_CREDIT_CARD',
        creditCard: {
          sessionId: 'SESSION',
          token: 'SESSION000000000001',
          creditCardNumber: 'xxx'
        }
      };

      return testSaga(proxyedUpdatePaymentMethodSaga, { payload })
        .next()
        .next({
          creditCardToken: 'SESSION000000000001',
          creditCardSessionId: 'SESSION'
        })
        .put.resolve(updatePaymentMethod(params))
        .throw({
          response: {
            status: 500
          }
        })
        .put(showNotificationByTemplate('payment-error'))
        .next()
        .put(updatePaymentMethodAction.failure())
        .next()
        .put(updatePaymentMethodAction.fulfill())
        .next()
        .isDone();
    });
  });
});
