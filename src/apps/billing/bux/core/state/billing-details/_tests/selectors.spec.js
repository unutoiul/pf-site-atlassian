import { expect } from 'chai';
import sinon from 'sinon';

import testMetadata from '../../_helpers/metadata/testHelpers/metadata';
import { NAME, PAYMENT_METHOD_CREDIT_CARD, PAYMENT_METHOD_PAYPAL } from '../constants';
import BillingDetailsMock from '../../../../../mock-server/bux/api/billing/billing-details-get';
import {
  getBillingDetails,
  getCreditCardDetails,
  getBillingDetailsMetadata,
  getBillingContact,
  getInvoiceContactEmail,
  hasCreditCard,
  hasCreditCardWithinExpiryDate,
  isManagedByPartner,
  getPaypalDetails,
  getPaymentMethod,
  hasPaypal,
  hasPaymentMethod,
  getPaymentMethodDetails
} from '../selectors';

const mockState = (data = {
  data: false, isFetching: false, error: false, lastUpdated: false
}) => ({
  [NAME]: data
});
const mockStateWithBillingDetails = (billingDetails = {}) => mockState({ data: billingDetails });

describe('Billing details selectors:', () => {
  let clock;
  beforeEach(() => {
    clock = sinon.useFakeTimers(Date.UTC(2018, 12, 25));
  });
  afterEach(() => {
    clock.restore();
  });

  describe('getBillingDetailsMetadata:', () => {
    testMetadata(mockState, getBillingDetailsMetadata);
  });

  describe('getBillingDetails:', () => {
    it('should return billing details', () => {
      expect(getBillingDetails(mockStateWithBillingDetails(BillingDetailsMock)))
        .to.be.deep.equal(BillingDetailsMock);
    });
  });

  describe('getCreditCardDetails:', () => {
    it('should return creditcard details', () => {
      expect(getCreditCardDetails(mockStateWithBillingDetails(BillingDetailsMock)))
        .to.be.deep.equal({
          suffix: '1111',
          expiryMonth: 1,
          expiryYear: 2022,
          type: 'VISA',
          name: 'Superplasma Team'
        });
    });
  });

  describe('getPaypalDetails:', () => {
    it('should return paypal details', () => {
      expect(getPaypalDetails(mockStateWithBillingDetails({
        ...BillingDetailsMock,
        paymentMethod: 'PAYPAL_ACCOUNT',
        paypalAccount: {
          email: 'bob-paypal@example.com'
        }
      })))
        .to.be.deep.equal({
          email: 'bob-paypal@example.com'
        });
    });
  });

  describe('getPaymentMethodDetails:', () => {
    it('should return paypal details', () => {
      expect(getPaymentMethodDetails(mockStateWithBillingDetails({
        ...BillingDetailsMock,
        paymentMethod: 'PAYPAL_ACCOUNT',
        paypalAccount: {
          email: 'bob-paypal@example.com'
        }
      })))
        .to.be.deep.equal({
          email: 'bob-paypal@example.com'
        });
    });
    it('should return credit card details', () => {
      expect(getPaymentMethodDetails(mockStateWithBillingDetails(BillingDetailsMock)))
        .to.be.deep.equal({
          suffix: '1111',
          expiryMonth: 1,
          expiryYear: 2022,
          type: 'VISA',
          name: 'Superplasma Team'
        });
    });
    it('should return empty object if either paypal or credit card are not present', () => {
      expect(getPaymentMethodDetails(mockStateWithBillingDetails({
        ...BillingDetailsMock,
        paypalAccount: {},
        creditCard: {}
      }))).to.be.deep.equal({});
    });
  });

  describe('getPaymentMethod:', () => {
    it('should return payment method credit card', () => {
      expect(getPaymentMethod(mockStateWithBillingDetails({
        ...BillingDetailsMock,
        paymentMethod: PAYMENT_METHOD_CREDIT_CARD
      })))
        .to.be.deep.equal(PAYMENT_METHOD_CREDIT_CARD);
    });
    it('should return payment method paypal', () => {
      expect(getPaymentMethod(mockStateWithBillingDetails({
        ...BillingDetailsMock,
        paymentMethod: PAYMENT_METHOD_PAYPAL
      })))
        .to.be.deep.equal(PAYMENT_METHOD_PAYPAL);
    });
  });

  describe('getBillingContact:', () => {
    it('should return contact details', () => {
      expect(getBillingContact(mockStateWithBillingDetails(BillingDetailsMock)))
        .to.be.deep.equal({
          firstName: 'Supernova',
          lastName: 'Team',
          email: 'bob@example.com',
        });
    });
  });

  describe('getInvoiceContactEmail:', () => {
    it('should return invoice contact details', () => {
      expect(getInvoiceContactEmail(mockStateWithBillingDetails(BillingDetailsMock)))
        .to.be.equal('bob-invoice@example.com');
    });
    it('should return null if there is no invoice contact details', () => {
      const noInvoiceContactState = { ...BillingDetailsMock };
      delete noInvoiceContactState.invoiceContact;
      expect(getInvoiceContactEmail(mockStateWithBillingDetails(noInvoiceContactState)))
        .to.be.equal(null);
    });
  });

  describe('isManagedByPartner:', () => {
    it('should passthought a value', () => {
      expect(isManagedByPartner(mockStateWithBillingDetails({ managedByPartner: 42 }))).to.be.equal(42);
    });
  });

  describe('hasCreditCard:', () => {
    it('should return true if there is a credit card', () => {
      expect(hasCreditCard(mockStateWithBillingDetails(BillingDetailsMock))).to.be.true();
    });
    it('should return false if there is no credit card', () => {
      const noCreditCardState = { ...BillingDetailsMock, creditCard: {} };
      expect(hasCreditCard(mockStateWithBillingDetails(noCreditCardState))).to.be.false();
    });
  });

  describe('hasPaypal:', () => {
    it('should return false if there is no paypal account', () => {
      expect(hasPaypal(mockStateWithBillingDetails(BillingDetailsMock))).to.be.false();
    });
    it('should return true if there is a paypal account', () => {
      expect(hasPaypal(mockStateWithBillingDetails({
        ...BillingDetailsMock,
        paymentMethod: 'PAYPAL_ACCOUNT',
        paypalAccount: {
          email: 'bob-paypal@example.com'
        }
      }))).to.be.true();
    });
  });

  describe('hasPaymentMethod:', () => {
    it('should return false if there is no paypal account neither credit card', () => {
      expect(hasPaymentMethod(mockStateWithBillingDetails({
        ...BillingDetailsMock,
        paypalAccount: {},
        creditCard: {}
      }))).to.be.false();
    });
    it('should return true if there is a paypal account', () => {
      expect(hasPaymentMethod(mockStateWithBillingDetails({
        ...BillingDetailsMock,
        paymentMethod: 'PAYPAL_ACCOUNT',
        paypalAccount: {
          email: 'bob-paypal@example.com'
        }
      }))).to.be.true();
    });
    it('should return true if there is a credit card', () => {
      expect(hasPaymentMethod(mockStateWithBillingDetails(BillingDetailsMock))).to.be.true();
    });
  });

  describe('hasCreditCardWithinExpiryDate:', () => {
    it('should return true if there is a valid expiration date', () => {
      const nextMonth = new Date();
      nextMonth.setMonth(nextMonth.getMonth() + 2);
      const validDateState = {
        ...BillingDetailsMock,
        creditCard: {
          expiryMonth: nextMonth.getMonth(),
          expiryYear: nextMonth.getFullYear()
        }
      };
      expect(hasCreditCardWithinExpiryDate(mockStateWithBillingDetails(validDateState)))
        .to.be.true();
    });
    it('should return false if there is no valid expiration date', () => {
      const lastMonth = new Date();
      const invalidDateState = {
        ...BillingDetailsMock,
        creditCard: {
          expiryMonth: lastMonth.getMonth(),
          expiryYear: lastMonth.getFullYear()
        }
      };
      expect(hasCreditCardWithinExpiryDate(mockStateWithBillingDetails(invalidDateState)))
        .to.be.false();
    });
    it('should return false if there is no credit card', () => {
      const noCreditCardState = { ...BillingDetailsMock, creditCard: {} };
      expect(hasCreditCardWithinExpiryDate(mockStateWithBillingDetails(noCreditCardState)))
        .to.be.false();
    });
  });
});
