import { isExpiryDateValid } from 'bux/common/formatters/date-formatter';
import { BillingDetailsState } from 'bux/core/state/billing-details/types';
import { createSelector } from 'bux/helpers/reselect';

import { getMetadata } from '../_helpers/metadata/selectors';
import { NAME, PAYMENT_METHOD_CREDIT_CARD } from './constants';

export const getBillingDetails = (state: BillingDetailsState) => state[NAME].data || {};

export const getPaymentMethod = (state: BillingDetailsState) => getBillingDetails(state).paymentMethod || PAYMENT_METHOD_CREDIT_CARD;

export const getCreditCardDetails = (state: BillingDetailsState) => getBillingDetails(state).creditCard || {};

export const getPaypalDetails = (state: BillingDetailsState) => getBillingDetails(state).paypalAccount || {};

export const getBillingContact = createSelector(
  getBillingDetails,
  ({ firstName, lastName, email }) => ({ firstName, lastName, email }),
);

export const getInvoiceContactEmail = createSelector(
  getBillingDetails,
  ({ invoiceContact }) => (invoiceContact ? invoiceContact.email : null),
);

export const hasPaypal = createSelector(
  getPaypalDetails,
  paypalDetails => !!(paypalDetails && Object.keys(paypalDetails).length),
);

export const hasCreditCard = createSelector(
  getCreditCardDetails,
  creditCardDetails => !!(creditCardDetails && Object.keys(creditCardDetails).length),
);

export const hasCreditCardWithinExpiryDate = createSelector(
  hasCreditCard, getCreditCardDetails,
  (hasCC, ccDetails) => (
    hasCC
      ? isExpiryDateValid(ccDetails.expiryMonth, ccDetails.expiryYear)
      : false
  ),
);

export const hasPaymentMethod = createSelector(
  hasCreditCard, hasPaypal,
  (creditCard, paypal) => creditCard || paypal,
);

export const getPaymentMethodDetails = createSelector(
  getCreditCardDetails, getPaypalDetails, hasCreditCard, hasPaypal,
  (creditCardDetails, paypalDetails, hasCC, hasPP) => {
    if (hasPP) {
      return paypalDetails;
    }

    if (hasCC) {
      return creditCardDetails;
    }

    return {};
  },
);

export const getBillingDetailsMetadata = createSelector((state: BillingDetailsState) => state[NAME], getMetadata);

export const isManagedByPartner = (state: BillingDetailsState) => getBillingDetails(state).managedByPartner;
