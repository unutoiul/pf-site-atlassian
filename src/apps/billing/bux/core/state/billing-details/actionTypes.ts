export const UPDATE_BILLING_DETAILS = 'bux/features/billing-details/actions/api-update';
export const UPDATE_PAYMENT_METHOD = 'bux/features/payment-method/actions/api-update';
export const REQUEST_BILLING_DETAILS = 'bux/features/billing-details/actions/api-request';
export const REQUEST_BILLING_DETAILS_REFRESH = 'bux/features/billing-details/actions/api-refresh';
