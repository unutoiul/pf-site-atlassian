import { BUXMetaReducedType } from 'bux/core/state/_helpers/metadata/types';

import { NAME } from './constants';

export interface BillingDetailsType {
  organisationName: string;
  managedByPartner: boolean;
  firstName: string;
  lastName: string;
  email: string;
  addressLine1: string;
  addressLine2: string;
  city: string;
  state: string;
  country: string;
  postcode: string;
  paymentMethod: string;
  taxpayerId: string;
  creditCard: {
    suffix: string,
    expiryMonth: number,
    expiryYear: number,
    type: string,
    name: string,
  };
  invoiceContact: {
    email: string,
  };
  paypalAccount: {};
}

export interface BillingDetailsState {
  [NAME]: BUXMetaReducedType<BillingDetailsType>;
}
