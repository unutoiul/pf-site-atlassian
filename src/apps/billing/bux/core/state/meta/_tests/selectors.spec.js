import { expect } from 'chai';
import {
  getUser,
  getUserLocation,
  getCountryList,
  getFeatureFlags,
  shouldUseNewChangeEdition,
  getOrganizationId,
  getCloudId,
  getTenantInfo
} from '../selectors';
import { NAME } from '../constants';

describe('Metadata: ', () => {
  const stateData = (key, data) => ({ [NAME]: { [key]: { data } } });

  describe('getUser: ', () => {
    it('should return user from metadata state', () => {
      expect(getUser(stateData('user', { email: 'email' })))
        .to.be.deep.equal({ email: 'email' });
    });
  });
  describe('getUserLocation: ', () => {
    it('should return location from metadata state', () => {
      expect(getUserLocation(stateData('location', { country: 'AU' })))
        .to.be.deep.equal({ country: 'AU' });
    });
  });
  describe('getCountryList: ', () => {
    it('should return country list from metadata state', () => {
      expect(getCountryList(stateData('countryList', ['AU'])))
        .to.be.deep.equal(['AU']);
    });
  });
  describe('getFeatureFlags: ', () => {
    it('should return country list from metadata state', () => {
      expect(getFeatureFlags(stateData('featureFlags', { flag1: 42 })))
        .to.be.deep.equal({ flag1: 42 });
    });
  });
  describe('shouldUseNewChangeEdition: ', () => {
    it('should return true', () => {
      expect(shouldUseNewChangeEdition(stateData('featureFlags', { 'new-change-edition-flow': true })))
        .to.be.true();
    });
    it('should return false', () => {
      expect(shouldUseNewChangeEdition(stateData('featureFlags', {})))
        .to.be.false();
    });
  });
  describe('getOrganizationId: ', () => {
    it('should return organizationId from metadata state', () => {
      expect(getOrganizationId(stateData('site', { organisationId: 42 })))
        .to.be.deep.equal(42);
    });
  });
  describe('cloudId: ', () => {
    it('should return cloudId from metadata state', () => {
      expect(getCloudId(stateData('site', { cloudId: 42 })))
        .to.be.deep.equal(42);
    });
  });
  describe('getTenantInfo: ', () => {
    const tenantState = siteData => ({
      [NAME]: {
        user: {
          data: {
            organizations: [{ id: 42, name: 'organization' }],
            sites: [{ id: 43, name: 'site' }]
          }
        },
        site: {
          data: siteData
        }
      }
    });

    it('should return current cloud site', () => {
      expect(getTenantInfo(tenantState({ cloudId: 43 })))
        .to.be.deep.equal({ id: 43, name: 'site', isOrg: false });
    });
    it('should return current organization', () => {
      expect(getTenantInfo(tenantState({ organisationId: 42 })))
        .to.be.deep.equal({ id: 42, name: 'organization', isOrg: true });
    });
  });
});
