import { SiteType, UserType } from 'bux/core/state/meta/types';

import { RECEIVE_BUX_USER, REQUEST_META_COUNTRY_LIST, REQUEST_META_USER, UPDATE_SITE_STATE } from './actionTypes';

export const getCountryList = () => ({ type: REQUEST_META_COUNTRY_LIST });
export const getUserMetadata = () => ({ type: REQUEST_META_USER });

export const setUserMetadata = (payload: UserType) => ({ type: RECEIVE_BUX_USER, payload });

export const updateSiteState = (payload: SiteType) => ({ type: UPDATE_SITE_STATE, payload });
