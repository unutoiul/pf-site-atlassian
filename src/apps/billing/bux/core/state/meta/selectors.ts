import { createSelector } from 'reselect';

import { FeatureFlagsType, MetaState, MultivariateFeatureFlagsType } from 'bux/core/state/meta/types';

import { getMetadata } from '../_helpers/metadata/selectors';
import { NAME } from './constants';

const emptyState = {
  featureFlags: {},
};

const getMeta = (state: MetaState) => state[NAME] || emptyState;

export const getUser = (state: MetaState) => getMeta(state).user.data;

export const getUserLocation = (state: MetaState) => getMeta(state).location.data;
export const getUserLocationMetadata = createSelector((state: MetaState) => state[NAME].location, getMetadata);

export const getCountryList = (state: MetaState) => getMeta(state).countryList.data;
export const getCountryListMetadata = createSelector((state: MetaState) => state[NAME].countryList, getMetadata);

export const getSite = (state: MetaState) => getMeta(state).site.data || {};
export const getFeatureFlags = (state: MetaState): FeatureFlagsType => getMeta(state).featureFlags.data || ({} as any);
export const getMultivariateFeatureFlags = (state: MetaState): MultivariateFeatureFlagsType => getMeta(state).multivariateFeatureFlags.data || ({} as any);

export const getOrganizationId = (state: MetaState) => (state ? getSite(state).organisationId : null);
const getOrganizations = (state: MetaState) => (getUser(state).organizations) || [];
const getCurrentOrganization = createSelector(
  getOrganizationId,
  getOrganizations,
  (orgId, organizations) => organizations.find(org => org.id === orgId),
);

export const getCloudId = (state: MetaState) => (state ? getSite(state).cloudId : null);
export const getUserId = (state: MetaState) => getUser(state).id;
const getSites = (state: MetaState) => (getUser(state).sites) || [];
const getCurrentSite = createSelector(
  getCloudId,
  getSites,
  (cloudId, sites) => sites.find(site => site.id === cloudId),
);

export const isOrganization = (state: MetaState) => !!getOrganizationId(state);
export const getTenantInfo = createSelector(
  isOrganization,
  getCurrentOrganization,
  getCurrentSite,
  (isOrg, currentOrg, currentSite) => ({ ...(isOrg ? currentOrg : currentSite), isOrg }),
);

export const shouldUseNewChangeEdition = createSelector(
  getFeatureFlags,
  featureFlags => featureFlags['new-change-edition-flow'] || false,
);
