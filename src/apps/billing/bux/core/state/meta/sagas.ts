import { all } from 'redux-saga/effects';

import { getCountryList, getUserMetadata } from 'bux/common/api/billing-ux-service';

import fetchSaga from '../_helpers/metadata/saga';

import {
  RECEIVE_META_COUNTRY_LIST, RECEIVE_META_USER,
  REQUEST_META_COUNTRY_LIST, REQUEST_META_USER,
} from './actionTypes';

function* rootSaga() {
  yield all([
    fetchSaga(REQUEST_META_COUNTRY_LIST, getCountryList, RECEIVE_META_COUNTRY_LIST),
    fetchSaga(REQUEST_META_USER, getUserMetadata, RECEIVE_META_USER),
  ]);
}

export default rootSaga;
