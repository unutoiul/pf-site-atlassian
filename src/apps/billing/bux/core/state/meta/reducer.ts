import { combineReducers } from 'redux';
import { handleActions } from 'redux-actions';

import sagaReducer, { withMetadata } from '../_helpers/metadata/reducer';
import { RECEIVE_BUX_USER, RECEIVE_META_COUNTRY_LIST, RECEIVE_META_USER, UPDATE_SITE_STATE } from './actionTypes';

export const INITIAL_LOCATION_STATE = withMetadata({
  countryIsoCode: 'US',
  countryName: '',
  regionIsoCode: '',
  regionName: '',
  city: '',
  latitude: null,
  longitude: null,
});

export const INITIAL_USER_STATE = withMetadata({
  name: '',
  email: '',
  organizations: [],
  sites: [],
});

export const INITIAL_COUNTRY_LIST_STATE = withMetadata([
  {
    isoCode: 'US',
    nativeName: 'United States',
    name: 'United States',
    currency: 'usd',
  },
  {
    isoCode: 'AU',
    nativeName: 'Australia',
    name: 'Australia',
    currency: 'usd',
  },
]);

export const INITIAL_FEATURE_FLAGS_STATE = {};

const INITIAL_SITE_STATE = {
  organisationId: '',
  cloudId: '',
};

const location = handleActions({
  [RECEIVE_META_USER]: sagaReducer.map(payload => payload.location),
}, INITIAL_LOCATION_STATE);

const user = handleActions({
  [RECEIVE_BUX_USER]: sagaReducer.map(payload => payload.user), // NOT FROM METADATA!
}, INITIAL_USER_STATE);

const countryList = handleActions({
  [RECEIVE_META_COUNTRY_LIST]: sagaReducer,
}, INITIAL_COUNTRY_LIST_STATE);

const featureFlags = handleActions({
  [RECEIVE_META_USER]: sagaReducer.map(payload => payload.featureFlags),
}, INITIAL_FEATURE_FLAGS_STATE);

const multivariateFeatureFlags = handleActions({
  [RECEIVE_META_USER]: sagaReducer.map(payload => payload.multivariateFeatureFlags),
}, INITIAL_FEATURE_FLAGS_STATE);

const site = handleActions({
  [UPDATE_SITE_STATE]: sagaReducer.map(payload => payload),
}, INITIAL_SITE_STATE);

export default combineReducers({
  location,
  user,
  countryList,
  featureFlags,
  multivariateFeatureFlags,
  site,
});
