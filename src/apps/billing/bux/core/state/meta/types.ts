import { BUXMetaReducedType } from 'bux/core/state/_helpers/metadata/types';

import { NAME } from './constants';

export interface FeatureFlagsType {
  'organizaions-support': boolean;
  'stride-annual.enabled': boolean;
  'atlassian-access-warnings': boolean;
  'new-change-edition-flow': boolean;
  'churn-survey.required': boolean;
}

export interface MultivariateFeatureFlagsType {
  [s: string]: string;
}

export interface SiteType {
  id: string;
  name: string;
}

export interface CountryType {
  isoCode: string;
  nativeName: string;
  name: string;
  currency: string;
}

export interface UserType {
  id: string;
  email: string;
  organizations: SiteType[];
  sites: SiteType[];
}

export interface LocationType {
  countryIsoCode: string;
  countryName: string;
  regionIsoCode: string;
  regionName: string;
  city: string;
  latitude: number;
  longitude: number;
}

export interface CurrentSiteType {
  organisationId: string;
  cloudId: string;
}

interface MetaType {
  location: BUXMetaReducedType<LocationType>;
  user: BUXMetaReducedType<UserType>;
  countryList: BUXMetaReducedType<CountryType[]>;
  featureFlags: BUXMetaReducedType<FeatureFlagsType>;
  multivariateFeatureFlags: BUXMetaReducedType<MultivariateFeatureFlagsType>;
  site: BUXMetaReducedType<CurrentSiteType>;
}

export interface TenantInfoType {
  isOrg: boolean;
  id: string;
  name: string;
}

export interface MetaState {
  [NAME]: MetaType;
}
