import { loopMiddleware } from 'react-redux-loop';
import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import { createLogger } from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import thunk from 'redux-thunk';

import { currentEnvironment } from 'bux/common/config';
import identityFn from 'bux/common/helpers/identity';

import rootReducer from './reducers';
import rootSaga from './sagas';

const shouldUseDevToolsExtensionMiddleware = () => {
  const hasDevToolsExtensions = window && 'devToolsExtension' in window;

  return currentEnvironment !== 'production' && hasDevToolsExtensions
    ? (window as any).devToolsExtension()
    : identityFn;
};

const buildStore = () => {
  const reducer = combineReducers({
    ...rootReducer,
  });

  const sagaMiddleware = createSagaMiddleware();
  const middleware = [
    thunk,
    sagaMiddleware,
    loopMiddleware,
  ];

  if (currentEnvironment !== 'production' && process.env.BABEL_ENV !== 'test') {
    middleware.push(createLogger());
  }

  const store = createStore(
    reducer,
    compose(
      applyMiddleware(...middleware),
      shouldUseDevToolsExtensionMiddleware(),
    ),
  );

  sagaMiddleware.run(rootSaga);

  return store;
};

export default buildStore;
