import { routinePromiseWatcherSaga } from 'redux-saga-routines';
import { all, call } from 'redux-saga/effects';

import convertToAnnualSaga from '../features/monthly-to-annual/sagas';
import apiSaga from './state/api/sagas';
import billEstimateSaga from './state/bill-estimate/sagas';
import billHistorySaga from './state/bill-history/sagas';
import billingContactSaga from './state/billing-contact/sagas';
import billingDetailsSaga from './state/billing-details/sagas';
import csatSaga from './state/csat/sagas';
import entitlementGroupSaga from './state/entitlement-group/sagas';
import expiredAccountSaga from './state/expired-account/sagas';
import focusedSaga from './state/focused-task/sagas';
import metaSaga from './state/meta/sagas';
import recommendedProductsSaga from './state/recommended-products/sagas';
import resubscribeSaga from './state/resubscribe/sagas';
import routerSaga from './state/router/sagas';

export default function* rootSaga() {
  yield all([
    call(routinePromiseWatcherSaga),

    call(billEstimateSaga),
    call(billHistorySaga),
    call(billingDetailsSaga),
    call(entitlementGroupSaga),
    call(csatSaga),
    call(expiredAccountSaga),
    call(focusedSaga),
    call(routerSaga),
    call(resubscribeSaga),
    call(billingContactSaga),
    call(recommendedProductsSaga),
    call(metaSaga),
    call(apiSaga),
    call(convertToAnnualSaga),
  ]);
}
