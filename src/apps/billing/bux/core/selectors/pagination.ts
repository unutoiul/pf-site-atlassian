import { createSelector } from 'bux/helpers/reselect';

/*
 * Threshold to start paginating the result. Default if 5
 *
 * If number of items is 5 or less  : show all items.
 * If number of items is more than 5: show first 5, and cut off the rest
 * If number of items is 6          : show all
 */

const createPagination = (selector: () => any[], cutoff = 5) => {
  const sliceIfAboveThreshold = (list = []) => (
    list.length - 1 > cutoff
      ? list.slice(0, cutoff)
      : list
  );

  const getPaginatedList = createSelector(
    selector,
    sliceIfAboveThreshold,
  );

  /*
   * Returns subtraction number of total summary lines from estimated threshold
   * Handled edge case: if only 1 line is hidden, all lines will be showed
   * */
  const getNumberOfRemainingLines = createSelector(
    selector,
    (lines) => {
      const areThereSomeRestSummaryLines = lines.length - 1 > cutoff;

      return areThereSomeRestSummaryLines ? lines.length - cutoff : 0;
    },
  );

  return {
    getPaginatedList,
    getNumberOfRemainingLines,
  };
};

export default createPagination;
