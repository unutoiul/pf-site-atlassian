import { createSelector } from 'bux/helpers/reselect';

import { BUXMetaDataType } from 'bux/core/state/_helpers/metadata/types';
import { BUXState } from 'bux/core/state/types';

import billEstimate from '../state/bill-estimate';
import billingDetails from '../state/billing-details';

/**
 * Threshold to show a global error (number of widgets in failure)
 */
const GLOBAL_ERROR_THRESHOLD = 2;

const isInstanceDeactivatedError = error => error && error.errorKey === 'service.account.deactivated';

const isErrorCritical = isInstanceDeactivatedError;

const ifCritical = error => (
  isErrorCritical(error) ? error : undefined
);

const mostCriticalOf = (errorA, errorB) => (
  ifCritical(errorA) || ifCritical(errorB) || errorA || errorB || false
);

type MetaSelectors = (state: BUXState) => BUXMetaDataType;

export const combineAny = (selectors: MetaSelectors[] = []) =>
  (state: BUXState) => selectors
    .map(selector => selector(state))
    .reduce((a: BUXMetaDataType, b) => ({
      loading: a.loading || b.loading,
      display: a.display || b.display,
      error: mostCriticalOf(a.error, b.error),
    } as BUXMetaDataType),
    { display: false, loading: false, error: false },
    );

export const combineAll = (selectors: MetaSelectors[] = []) =>
  (state) => {
    const combined = combineAny(selectors)(state);

    return {
      ...combined,
      display: combined.display && !combined.error,
    };
  };

export const combineAllDisplayable = selectors =>
  (state) => {
    const combined = selectors
      .map(selector => selector(state))
      .reduce((a, b) => ({
        loading: a.loading || b.loading,
        display: a.display && b.display,
        error: mostCriticalOf(a.error, b.error),
      }), { display: true, loading: false, error: false });

    return {
      ...combined,
      display: combined.display && !isErrorCritical(combined.error),
    };
  };

const isOverviewInError = (widgets) => {
  const widgetInErrors = widgets.filter(widget => (!!widget.error));
  const criticalError = widgetInErrors.map(widget => widget.error).find(isErrorCritical);
  if (criticalError) {
    return criticalError;
  }
  if (widgetInErrors.length >= GLOBAL_ERROR_THRESHOLD) {
    return { message: 'Multiple errors while loading overview', errors: widgetInErrors };
  }

  return undefined;
};

const getOverviewWidgetMetadata = createSelector(
  billEstimate.selectors.getBillEstimateMetadata,
  billingDetails.selectors.getBillingDetailsMetadata,
  (billEstimateMetadata, billingDetailMetadata) => ([
    { name: 'bill-estimate', important: true, isFetching: false, ...billEstimateMetadata },
    { name: 'payment-detail', important: true, isFetching: false, ...billingDetailMetadata },
    { name: 'invoice-contact', important: false, isFetching: false, ...billingDetailMetadata },
  ]));

export const getOverviewMetadata = createSelector(
  getOverviewWidgetMetadata,
  (widgets) => {
    const loading = widgets.filter(widget => (widget.loading || widget.isFetching)).length !== 0;
    const display = widgets.filter(widget => (widget.important && !widget.display)).length === 0;
    const error = isOverviewInError(widgets) || !display;

    return {
      /**
       * Wait for all widgets to have loaded before rendering the page
       */
      loading,
      /**
       * Whether or not the overview page is displayable depends on whether there's an error
       */
      display: !error && display,
      error,
    };
  });
