import { getQueryParams } from 'bux/common/helpers/browser';
import { getEntitlementGroup } from 'bux/core/state/entitlement-group/selectors';
import { BUXState } from 'bux/core/state/types';

const FREE_ORGANIZATION_TYPES = ['COMMUNITY', 'OPEN_SOURCE'];

export const getPathname = () => {
  return window.location.pathname;
};

export const isRequestingProductOptOut =
  () => (!!getQueryParams().requestproductoptout);

export const isWithFreeLicense =
  (state: BUXState) => FREE_ORGANIZATION_TYPES.indexOf(getEntitlementGroup(state).license) >= 0;

export const getFocusProductKey = () => getQueryParams().product;
