import * as moment from 'moment';

import { BillEstimateLineType } from 'bux/core/state/bill-estimate/types';
import { EntitlementType } from 'bux/core/state/entitlement-group/types';

export type EntitlementFilter = (entitlement: EntitlementType, product?: BillEstimateLineType) => boolean;

// common selector, shared between bill-estimate and entitlement group to break up cycles

export const isTrial: EntitlementFilter = (entitlement: EntitlementType) => moment().isSameOrBefore(moment(entitlement.trialEndDate || 0));
export const isSuspended: EntitlementFilter = (entitlement: EntitlementType) => entitlement.status === 'ACTIVE' && entitlement.suspended;
export const isActive: EntitlementFilter = (entitlement: EntitlementType, billingRecord = {} as any) =>
  entitlement.status === 'ACTIVE' &&
  !entitlement.suspended &&
  !!billingRecord;
export const isInactive: EntitlementFilter = (entitlement, billingRecord = {} as any) => !isActive(entitlement, billingRecord);

const mergeFunction = (entitlement: EntitlementType, product: BillEstimateLineType) =>
  (entitlement.id !== undefined && product.entitlementId === entitlement.id) ||
(entitlement.productKey !== undefined && product.productKey === entitlement.productKey);

export const findBillingRecord = (entitlement: EntitlementType, billing: BillEstimateLineType[]) =>
  billing.find(product => mergeFunction(entitlement, product));

export const findEntitlementRecord = (entitlements: EntitlementType[], product) =>
  entitlements.find(entitlement => mergeFunction(entitlement, product));

export const combineProductAndEntitlement = (product: BillEstimateLineType, entitlements: EntitlementType[]) => ({
  entitlement: findEntitlementRecord(entitlements, product),
  product,
});
