import { createSelector } from 'bux/helpers/reselect';

import { EntitlementType } from 'bux/core/state/entitlement-group/types';

import { getBillEstimateLines } from '../state/bill-estimate/selectors';
import { getEntitlements } from '../state/entitlement-group/selectors';
import { isOrganization } from '../state/meta/selectors';

import { EntitlementFilter, findBillingRecord, isActive, isInactive, isSuspended } from './common/entitlements';

const mapSections = (filters: Record<string, EntitlementFilter>, entitlements: EntitlementType[], billing) => {
  const result = {};
  const filterKeys = Object.keys(filters);
  filterKeys.forEach(filter => (result[filter] = []));

  entitlements.forEach((entitlement) => {
    const billingRecord = findBillingRecord(entitlement, billing);
    const mergedRecord = {
      title: billingRecord ? billingRecord.productName : entitlement.name,
      entitlement,
      billingRecord,
    };

    const group = filterKeys.find(filter => filters[filter](entitlement, billingRecord));
    if (group) {
      result[group].push(mergedRecord);
    }
  });

  return result;
};

// Unsubscribed means that the product can be resubscribed again
// Currently that is only possible for organization's products as for cloud products we delete the data
const isUnsubscribed = (isOrg: boolean): EntitlementFilter => {
  if (isOrg) {
    return isInactive;
  }

  return () => false;
};

export const getEntitlementSections = createSelector(
  getEntitlements,
  getBillEstimateLines,
  isOrganization,
  (entitlements, billEstimate, isOrg) =>
    mapSections({
      active: isActive,
      suspended: isSuspended,
      unsubscribed: isUnsubscribed(isOrg),
    }, entitlements, billEstimate));
