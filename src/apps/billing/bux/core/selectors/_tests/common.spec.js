import { expect } from 'chai';
import sinon from 'sinon';
import { rewiremock } from 'bux/helpers/rewire-mock';

describe('Routing: ', () => {
  describe('getFocusProductKey: ', () => {
    let getFocusProductKey;
    let getQueryParams;

    beforeEach(() => {
      getQueryParams = sinon.stub().returns({ productKey: 'ABC', product: 'XYZ' });
      getFocusProductKey = rewiremock.proxy('../common', {
        'bux/common/helpers/browser': {
          getQueryParams
        }
      }).getFocusProductKey;
    });

    it('should return query string from routing state', () => {
      expect(getFocusProductKey())
        .to.be.equal('XYZ');
      expect(getQueryParams).to.be.called();
    });
  });
});
