import { expect } from 'chai';
import sinon from 'sinon';
import proxyquire from 'bux/helpers/proxyquire';

describe('Entitlement Group: ', () => {
  let getEntitlementSections;
  let getEntitlements;
  let getBillEstimateLines;
  let isOrganization;

  const createProxyquire = () => {
    getEntitlements = sinon.stub().returns([]);
    getBillEstimateLines = sinon.stub().returns([]);
    isOrganization = sinon.stub().returns(false);

    getEntitlementSections = proxyquire.noCallThru().load('../entitlements', {
      'bux/core/state/entitlement-group/selectors': { getEntitlements },
      'bux/core/state/bill-estimate/selectors': { getBillEstimateLines },
      'bux/core/state/meta/selectors': { isOrganization },
    }).getEntitlementSections;
  };

  beforeEach(() => {
    createProxyquire();
    getBillEstimateLines.returns([{
      entitlementId: 1,
      productKey: 'product1',
      productName: 'product1'
    }, {
      productKey: 'product2',
      productName: 'product2'
    }]);
  });

  it('should map different sections on entitlement id', () => {
    getEntitlements.returns([{
      id: 1,
      status: 'ACTIVE'
    }]);
    const sections = getEntitlementSections({ unique: 1 });
    expect(sections.active).to.have.length(1);
    expect(sections.active[0].entitlement).to.deep.equal({
      id: 1,
      status: 'ACTIVE'
    });
    expect(sections.active[0].billingRecord).to.deep.equal({
      entitlementId: 1,
      productKey: 'product1',
      productName: 'product1'
    });
  });

  it('should map on product key if entitlement id is not present in the bill estimate', () => {
    getEntitlements.returns([{
      entitlementId: 2,
      productKey: 'product2',
      status: 'ACTIVE'
    }]);
    const sections = getEntitlementSections({ unique: 2 });
    expect(sections.active).to.have.length(1);
    expect(sections.active[0].entitlement).to.deep.equal({
      entitlementId: 2,
      productKey: 'product2',
      status: 'ACTIVE'
    });
    expect(sections.active[0].billingRecord).to.deep.equal({
      productKey: 'product2',
      productName: 'product2'
    });
  });
});
