import { rewiremock } from 'bux/helpers/rewire-mock';
import sinon from 'sinon';
import { expect } from 'chai';

describe('metadata selector: ', () => {
  let getBillingDetails;
  let getBillingDetailsMetadata;
  let getBillEstimateMetadata;
  let getOverviewMetadata;
  let combineAnySelector;
  let combineAllDisplayable;
  let combineAllSelector;

  beforeEach(() => {
    getBillEstimateMetadata = sinon.stub().returns({ display: true });
    getBillingDetails = sinon.stub().returns({ display: true });
    getBillingDetailsMetadata = sinon.stub().returns({ display: true });

    const selectors = rewiremock.proxy('../metadata', {
      '../state/bill-estimate': {
        selectors: { getBillEstimateMetadata }
      },
      '../state/billing-details': {
        selectors: { getBillingDetails, getBillingDetailsMetadata }
      }
    });

    getOverviewMetadata = selectors.getOverviewMetadata;
    combineAnySelector = selectors.combineAny;
    combineAllDisplayable = selectors.combineAllDisplayable;
    combineAllSelector = selectors.combineAll;
  });

  const withErrors = stubs => stubs.forEach(stub => stub.returns({ error: { some: 'error' } }));
  const withError = stub => (withErrors([stub]));
  const criticalError = { errorKey: 'service.account.deactivated' };

  it('getOverviewMetadata should always return no global error if all ok', () => {
    const selected = getOverviewMetadata();
    expect(selected.error).to.be.false();
    expect(selected.display).to.be.true();
  });

  it('getOverviewMetadata should always return global error if one error', () => {
    withError(getBillEstimateMetadata);
    const selected = getOverviewMetadata();

    expect(selected.error).not.to.be.undefined();
    expect(selected.display).to.be.false();
  });

  it('getOverviewMetadata should always return global error if two errors', () => {
    withErrors([getBillEstimateMetadata, getBillingDetails]);
    const selected = getOverviewMetadata();

    expect(selected.error).not.to.be.undefined();
    expect(selected.display).to.be.false();
  });

  it('getOverviewMetadata should always return critical global error if there are other errors', () => {
    withErrors([getBillingDetails]);
    getBillEstimateMetadata.returns({ error: criticalError });
    const selected = getOverviewMetadata();

    expect(selected.error).to.be.equal(criticalError);
    expect(selected.display).to.be.false();
  });

  it('getOverviewMetadata should always return not loading if none loading', () => {
    getBillEstimateMetadata.returns({ isFetching: false });
    getBillingDetails.returns({ loading: false });

    expect(getOverviewMetadata().loading).to.be.false();
  });

  it('getOverviewMetadata should return loading if only one more is loading', () => {
    getBillEstimateMetadata.returns({ isFetching: true });
    getBillingDetails.returns({ loading: false });
    expect(getOverviewMetadata().loading).to.be.true();
  });

  it('getOverviewMetadata should always return loading if all loading', () => {
    getBillEstimateMetadata.returns({ isFetching: true });
    getBillingDetails.returns({ loading: true });

    expect(getOverviewMetadata().loading).to.be.true();
  });

  describe('combineAny: ', () => {
    let combined;

    beforeEach(() => {
      combined = combineAnySelector([getBillEstimateMetadata, getBillingDetails]);
    });

    it('combineAny should always return loading if something is loading', () => {
      getBillEstimateMetadata.returns({ loading: false });
      getBillingDetails.returns({ loading: true });

      expect(combined().loading).to.be.true();
    });

    it('combineAny should not return displayable if all is not displayable', () => {
      getBillEstimateMetadata.returns({ display: false });
      getBillingDetails.returns({ display: false });

      expect(combined().display).to.be.false();
    });

    it('combineAny should return displayable if at least one is displayable', () => {
      getBillEstimateMetadata.returns({ display: true });
      getBillingDetails.returns({ display: false });

      expect(combined().display).to.be.true();
    });

    it('combineAny should return first error', () => {
      const firstError = { reason: 'Not sure?' };
      getBillEstimateMetadata.returns({ error: firstError });
      getBillingDetails.returns({ display: true });

      expect(combined().error).to.be.equal(firstError);
      expect(combined().display).to.be.true();
    });

    it('combineAny should return critical error even if it is not first', () => {
      const firstError = { reason: 'Not sure?' };
      getBillEstimateMetadata.returns({ error: firstError });
      getBillingDetails.returns({ error: criticalError });

      expect(combined().error).to.be.equal(criticalError);
    });
  });

  describe('combineAll: ', () => {
    let combined;

    beforeEach(() => {
      combined = combineAllSelector([getBillEstimateMetadata, getBillingDetails]);
    });

    it('combineAny should always return loading if something is loading', () => {
      getBillEstimateMetadata.returns({ loading: false });
      getBillingDetails.returns({ loading: true });

      expect(combined().loading).to.be.true();
    });

    it('combineAny should not return displayable if all is not displayable', () => {
      getBillEstimateMetadata.returns({ display: false });
      getBillingDetails.returns({ display: false });

      expect(combined().display).to.be.false();
    });

    it('combineAny should return displayable if at least one is displayable', () => {
      getBillEstimateMetadata.returns({ display: true });
      getBillingDetails.returns({ display: false });

      expect(combined().display).to.be.true();
    });

    it('combineAny should return first error', () => {
      const firstError = { reason: 'Not sure?' };
      getBillEstimateMetadata.returns({ error: firstError });
      getBillingDetails.returns({ display: true });

      expect(combined().error).to.be.equal(firstError);
      expect(combined().display).to.be.false();
    });

    it('combineAny should return critical error even if it is not first', () => {
      const firstError = { reason: 'Not sure?' };
      getBillEstimateMetadata.returns({ error: firstError });
      getBillingDetails.returns({ error: criticalError });

      expect(combined().error).to.be.equal(criticalError);
    });
  });

  describe('combineAllDisplayable: ', () => {
    let combined;

    beforeEach(() => {
      combined = combineAllDisplayable([getBillEstimateMetadata, getBillingDetails]);
    });

    it('combineAllDisplayable should always return loading if something is loading', () => {
      getBillEstimateMetadata.returns({ loading: false });
      getBillingDetails.returns({ loading: true });

      expect(combined().loading).to.be.true();
    });

    it('combineAllDisplayable should not return displayable if all is not displayable', () => {
      getBillEstimateMetadata.returns({ display: false });
      getBillingDetails.returns({ display: false });

      expect(combined().display).to.be.false();
    });

    it('combineAllDisplayable should return displayable if at least one is displayable', () => {
      getBillEstimateMetadata.returns({ display: true });
      getBillingDetails.returns({ display: false });

      expect(combined().display).to.be.false();
    });

    it('combineAllDisplayable should return first error', () => {
      const firstError = { reason: 'Not sure?' };
      getBillEstimateMetadata.returns({ error: firstError });
      getBillingDetails.returns({ display: true });

      expect(combined().error).to.be.equal(firstError);
      expect(combined().display).not.to.be.true();
    });

    it('combineAllDisplayable should not display if there is a critical error', () => {
      getBillEstimateMetadata.returns({ display: true });
      getBillingDetails.returns({ error: criticalError, display: false });

      expect(combined().error).to.be.equal(criticalError);
      expect(combined().display).to.be.false();
    });
  });
});
