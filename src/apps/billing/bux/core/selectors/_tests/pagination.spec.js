import sinon from 'sinon';
import { expect } from 'chai';

import createPagination from '../pagination';

describe('pagination selector:  ', () => {
  const getProductList = sinon.stub();

  const { getPaginatedList, getNumberOfRemainingLines } = createPagination(getProductList);

  it('getPaginatedList should return first 5 items if there are more than 5 items but not 6', () => {
    getProductList.returns([1, 2, 3, 4, 5, 6, 7]);
    expect(getPaginatedList()).to.deep.equal([1, 2, 3, 4, 5]);
  });

  it('getPaginatedList should return all items if there are less than 5 items', () => {
    getProductList.returns([1, 2, 3, 4, 5]);
    expect(getPaginatedList()).to.deep.equal([1, 2, 3, 4, 5]);
  });

  it('getPaginatedList should return all items if there are 6 items', () => {
    getProductList.returns([1, 2, 3, 4, 5, 6]);
    expect(getPaginatedList()).to.deep.equal([1, 2, 3, 4, 5, 6]);
  });

  it('getNumberOfRemainingLines should return 0 if less or equal then 6 items was passed', () => {
    getProductList.returns([1, 2, 3, 4, 5, 6]);
    expect(getNumberOfRemainingLines()).to.equal(0);
  });

  it('getNumberOfRemainingLines should return subtraction of total items from 5 ', () => {
    getProductList.returns([1, 2, 3, 4, 5, 6, 7, 8]);
    expect(getNumberOfRemainingLines()).to.equal(3);
  });
});