import { reducer as form } from 'redux-form';

import billEstimate from '../state/bill-estimate';
import billHistory from '../state/bill-history';
import billingDetails from '../state/billing-details';
import entitlementGroup from '../state/entitlement-group';
import focusedTask from '../state/focused-task';
import meta from '../state/meta';
import notification from '../state/notifications';
import recommendedProducts from '../state/recommended-products';

export default {
// main stores
  [billEstimate.constants.NAME]: billEstimate.reducer,
  [billHistory.constants.NAME]: billHistory.reducer,
  [focusedTask.constants.NAME]: focusedTask.reducer,
  [entitlementGroup.constants.NAME]: entitlementGroup.reducer,
  [billingDetails.constants.NAME]: billingDetails.reducer,
  [recommendedProducts.constants.NAME]: recommendedProducts.reducer,

  // client-side stores
  [notification.constants.NAME]: notification.reducer,

  // system stores
  [meta.constants.NAME]: meta.reducer,

  // third party
  form, // redux form
};
