import { combineAllDisplayable } from 'bux/core/selectors/metadata';

import billEstimate from 'bux/core/state/bill-estimate';
import entitlementGroup from 'bux/core/state/entitlement-group';

const isLoaded = state => combineAllDisplayable([
  billEstimate.selectors.getBillEstimateMetadata,
  entitlementGroup.selectors.getEntitlementGroupMetadata
])(state);

export { isLoaded };
