import React from 'react';
import sinon from 'sinon';
import { expect } from 'chai';
import { shallow, mount, render } from 'enzyme';
import { Provider } from 'bux/helpers/redux';
import proxyquire from 'bux/helpers/proxyquire';
import getAnalytic from 'bux/components/log/helpers/getAnalytic';
import Page from 'bux/components/Page';
import { BillEstimatePage, AugmentedComponent } from '../BillEstimate';
import BillEstimate from '../content';

describe('BillEstimate page: ', () => {
  it('should dispatch events to gather information', () => {
    const getEntitlementGroup = sinon.spy();
    const getBillEstimate = sinon.spy();
    const getBillingDetails = sinon.spy();
    const dispatch = () => {
    };

    const action = proxyquire.noCallThru().load('../actions', {
      'bux/core/state/entitlement-group': {
        actions: { getEntitlementGroup }
      },
      'bux/core/state/bill-estimate': {
        actions: { getBillEstimate }
      },
      'bux/core/state/billing-details': {
        actions: { getBillingDetails }
      }
    }).getBillEstimateData;

    action()(dispatch);

    expect(getEntitlementGroup).to.have.been.called();
    expect(getBillEstimate).to.have.been.called();
    expect(getBillingDetails).to.have.been.called();
  });

  it('should trigger callbacks on Component mount', (done) => {
    const isLoaded = sinon.stub().returns({ display: true });
    const getBillEstimateData = sinon.stub().returns(() => {
    });

    // eslint-disable-next-line no-shadow
    const { AugmentedComponent } = proxyquire.noCallThru().load('../BillEstimate', {
      './selectors': { isLoaded },
      './actions': { getBillEstimateData }
    });

    const wrapper = mount(<Provider><AugmentedComponent /></Provider>);

    expect(getBillEstimateData).to.have.been.called();
    expect(isLoaded).to.have.been.called();
    setImmediate(() => {
      wrapper.update();
      expect(wrapper.find(BillEstimate)).to.be.present();
      done();
    });
  });

  it('should pass title and metadata', () => {
    const wrapper = shallow(<BillEstimatePage />);
    const page = wrapper.find(Page);
    expect(page.prop('title')).to.equal('Bill estimate');
    expect(wrapper).to.have.prop('logPrefix', 'billEstimate');
    expect(wrapper.find(AugmentedComponent)).to.be.present();
  });

  it('should render billing period', () => {
    const wrapper = shallow(<BillEstimatePage
      date={{
        endDate: '2016-11-06',
        startDate: '2016-10-07'
      }}
    />);
    const page = wrapper.find(Page);
    expect(render(page.prop('subTitle'))).to.contain.text('Oct 7, 2016 – Nov 6, 2016');
  });

  it('should get date from store', () => {
    const date = {
      endDate: '2016-11-06',
      startDate: '2016-10-07'
    };
    const isNextBillingPeriodRenewalFrequencyAnnual = sinon.stub().returns(false);
    const getBillingPeriod = sinon.stub().returns(date);
    const isLoaded = sinon.stub().returns({ display: true });
    const getBillEstimateData = sinon.stub().returns(() => {
    });

    const billEstimate = proxyquire.noCallThru().load('../BillEstimate', {
      './selectors': { isLoaded },
      './actions': { getBillEstimateData },
      'bux/core/state/bill-estimate/selectors': { getBillingPeriod, isNextBillingPeriodRenewalFrequencyAnnual }
    });
    const ConnectedPage = billEstimate.default;
    const BillPage = billEstimate.BillEstimatePage;
    const { context } = getAnalytic();
    const wrapper = mount(<Provider><ConnectedPage /></Provider>, context);
    expect(getBillingPeriod).to.have.been.called();
    expect(wrapper.find(BillPage)).to.have.prop('date', date);
  });
});
