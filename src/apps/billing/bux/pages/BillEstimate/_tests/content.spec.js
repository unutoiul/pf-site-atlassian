import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import { BillEstimateDetail } from 'bux/features/bill-estimate/components';
import BillEstimateFooter from 'bux/features/bill-estimate/components/components/BillEstimateFooter';
import { BillEstimateContent } from '../content';

const DEFAULT_PROPS = {
  isAnnual: true,
  currencyCode: 'usd',
  totalCost: 10,
  nextRenewalDate: '2018-01-01'
};

const mountComponent = options => <BillEstimateContent {...DEFAULT_PROPS} {...options} />;

describe('BillEstimateContent component: ', () => {
  it('should render component', () => {
    const wrapper = shallow(mountComponent());
    expect(wrapper.find(BillEstimateFooter)).to.be.present();
    expect(wrapper.find(BillEstimateDetail)).to.be.present();
    expect(wrapper.find(BillEstimateFooter)).to.have.props({
      isAnnual: DEFAULT_PROPS.isAnnual,
      currencyCode: DEFAULT_PROPS.currencyCode,
      totalCost: DEFAULT_PROPS.totalCost,
      nextRenewalDate: DEFAULT_PROPS.nextRenewalDate
    });
  });
});
