import { I18n } from '@lingui/react';
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getBillingPeriod } from 'bux/core/state/bill-estimate/selectors';
import Page from 'bux/components/Page';
import withAsyncPageLoader from 'bux/components/withAsyncPageLoader';
import { Breadcrumbs, BillingBreadcrumb } from 'bux/components/Breadcrumb';
import Date from 'bux/components/Date';
import styles from './styles.less';
import { getBillEstimateData } from './actions';
import { isLoaded } from './selectors';

const loader = () => import(/* webpackChunkName:'billing' */ './content');
export const AugmentedComponent = withAsyncPageLoader(isLoaded, getBillEstimateData)(loader);

const isPeriod = date => date && date.startDate && date.endDate;

const billingPeriod = date => isPeriod(date) && (
  <span className={styles.topBillingPeriod}>
    <Date value={date.startDate} />
    &nbsp;–&nbsp;
    <Date value={date.endDate} />
  </span>
);

const breadcrumbs = (
  <Breadcrumbs>
    <BillingBreadcrumb />
  </Breadcrumbs>
);

export const BillEstimatePage = ({ date }) => (
  <I18n>{({ i18n }) => (
    <Page
      title={i18n.t('billing.bill-estimate.page-title')`Bill estimate`}
      subTitle={billingPeriod(date)}
      layout="aside"
      logPrefix="billEstimate"
      breadcrumbs={breadcrumbs}
    >
      <AugmentedComponent />
    </Page>
  )}</I18n>
);

BillEstimatePage.propTypes = {
  date: PropTypes.object,
  isAnnual: PropTypes.bool
};

export const mapStateToProps = state => ({
  date: getBillingPeriod(state),
});

export default connect(mapStateToProps)(BillEstimatePage);
