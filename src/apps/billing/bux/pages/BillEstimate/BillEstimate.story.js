import { storiesOf } from '@storybook/react';
import { inScenarios, inPageChrome, render } from 'bux/common/helpers/storybook';

import BillEstimate from './BillEstimate';

storiesOf('BUX|Pages/Bill estimate', module)
  .add('Default', () => inScenarios([], inPageChrome(render(BillEstimate))))
  .add('Annual', () => inScenarios(['bill-estimate-annual'], inPageChrome(render(BillEstimate))))
  .add('Error', () => inScenarios(['bill-estimate-error'], inPageChrome(render(BillEstimate))));
