import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { asPageContent } from 'bux/components/Page/Page';
import { shouldNotBeManaged } from 'bux/components/Page/ManagementWall';
import { BillEstimateDetail, AsideLinks } from 'bux/features/bill-estimate/components';
import BillEstimateFooter from 'bux/features/bill-estimate/components/components/BillEstimateFooter';
import ContentAsideLayout from 'bux/components/layout/ContentAsideLayout';
import {
  isNextBillingPeriodRenewalFrequencyAnnual,
  getBill,
  getNextRenewalBillingDate,
  isNextBillInNewCurrency as isAfterTheCutOverDate, 
  upcomingCurrencyExtras,
} from 'bux/core/state/bill-estimate/selectors';
import CurrencyUpdateNotification from 'bux/components/CurrencyUpdateNotification';
import FeatureFlagged from '../../components/FeatureFlags/FeatureFlagged';
import styles from './styles.less';

export const BillEstimateContent = ({
  totalCost, currencyCode, isAnnual, nextRenewalDate, upcomingCurrencyExtra, isNextBillInNewCurrency,
}) => (
    <div>
      <FeatureFlagged flags={['2018-upcoming-currency-notice']}>
        {
          upcomingCurrencyExtra &&
          <div className={styles.noticesRow} data-test="currency-update-notices-row">
            <CurrencyUpdateNotification 
              upcomingCurrencyExtra={upcomingCurrencyExtra} 
              isNextBillInNewCurrency={isNextBillInNewCurrency} 
            />
          </div>
        }
      </FeatureFlagged>
      <BillEstimateDetail style="full" showTotalDueFormat />
      <BillEstimateFooter
        isAnnual={isAnnual}
        totalCost={totalCost}
        nextRenewalDate={nextRenewalDate}
        currencyCode={currencyCode}
      />
    </div>
);

BillEstimateContent.propTypes = {
  totalCost: PropTypes.number,
  currencyCode: PropTypes.string.isRequired,
  isAnnual: PropTypes.bool,
  nextRenewalDate: PropTypes.string,
  isNextBillInNewCurrency: PropTypes.bool,
  upcomingCurrencyExtra: PropTypes.object,
};

const mapStateToProps = state => ({
  totalCost: getBill(state).totalAmountWithTax,
  currencyCode: getBill(state).currencyCode,
  isAnnual: isNextBillingPeriodRenewalFrequencyAnnual(state),
  nextRenewalDate: getNextRenewalBillingDate(state),
  upcomingCurrencyExtra: upcomingCurrencyExtras(state),
  isNextBillInNewCurrency: isAfterTheCutOverDate(state),
});

const ConnectedBillEstimateContent = connect(mapStateToProps)(BillEstimateContent);

const BillEstimatePage = () => (
  <ContentAsideLayout
    content={<ConnectedBillEstimateContent />}
    aside={<AsideLinks />}
  />
);

export default compose(
  asPageContent({
    name: 'billingEstimateScreen'
  }), 
  shouldNotBeManaged,
)(BillEstimatePage);
