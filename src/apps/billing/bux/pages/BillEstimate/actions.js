import billEstimate from 'bux/core/state/bill-estimate';
import entitlementGroup from 'bux/core/state/entitlement-group';
import billingDetails from 'bux/core/state/billing-details';

const getBillEstimateData = () => (dispatch) => {
  dispatch(billEstimate.actions.getBillEstimate());
  dispatch(entitlementGroup.actions.getEntitlementGroup());
  dispatch(billingDetails.actions.getBillingDetails());
};

export { getBillEstimateData };
