import { I18n } from '@lingui/react';
import React from 'react';
import { UserFeatureFlag } from 'common/feature-flags';
import Page from 'bux/components/Page';
import withAsyncPageLoader from 'bux/components/withAsyncPageLoader';
import { Breadcrumbs, BillingBreadcrumb } from 'bux/components/Breadcrumb';
import { getEntitlementData } from './actions';
import { isLoaded } from './selectors';

const loader = () => import(/* webpackChunkName:'billing' */ './content');
export const AugmentedComponent = withAsyncPageLoader(isLoaded, getEntitlementData)(loader);

const billingBreadcrumbs = (
  <Breadcrumbs>
    <BillingBreadcrumb />
  </Breadcrumbs>
);

const breadcrumbs = (
  <Breadcrumbs />
);

export default () => (
  <UserFeatureFlag flagKey="admin.billing.menu.flat" defaultValue={false}>
    {flag => (
      <I18n>{({ i18n }) => (
        <Page
          title={i18n.t('billing.manage-subscriptions.page-title')`Manage subscriptions`}
          logPrefix="manageSubscriptions"
          breadcrumbs={!flag.value ? billingBreadcrumbs : breadcrumbs}
        >
          <AugmentedComponent />
        </Page>
      )}</I18n>
    )}
  </UserFeatureFlag>
);

