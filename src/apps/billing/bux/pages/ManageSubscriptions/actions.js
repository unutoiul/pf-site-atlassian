import billingDetails from 'bux/core/state/billing-details';
import entitlementGroup from 'bux/core/state/entitlement-group';
import billEstimate from 'bux/core/state/bill-estimate';
import recommendedProducts from 'bux/core/state/recommended-products';
import meta from 'bux/core/state/meta';

const getEntitlementData = () => (dispatch) => {
  dispatch(recommendedProducts.actions.getRecommendedProducts());
  dispatch(entitlementGroup.actions.getEntitlementGroup());
  dispatch(billEstimate.actions.getBillEstimate());
  dispatch(billingDetails.actions.getBillingDetails());
  dispatch(meta.actions.getCountryList());
  dispatch(meta.actions.getUserMetadata());
};

export { getEntitlementData };
