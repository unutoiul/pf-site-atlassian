import { storiesOf } from '@storybook/react';
import { asOrganization, inScenarios, inPageChrome, render } from 'bux/common/helpers/storybook';

import ManageSubscriptionsPage from './ManageSubscriptions';

const inOrganizationScenarios = scenarios => (
  inScenarios(scenarios, inPageChrome(asOrganization(render(ManageSubscriptionsPage))))
);

storiesOf('BUX|Pages/Manage subscriptions', module)
  .add('Default', () => inScenarios([], inPageChrome(render(ManageSubscriptionsPage))))

  .add('Annual', () => inScenarios(['bill-estimate-annual'], inPageChrome(render(ManageSubscriptionsPage))))

  .add('No subscriptions', () => inScenarios(
    ['entitlement-group-empty'],
    inPageChrome(render(ManageSubscriptionsPage))
  ))
  .add('With Jira suspended', () => inScenarios(
    ['entitlement-group-jira-suspended'],
    inPageChrome(render(ManageSubscriptionsPage))
  ))
  .add('With unsubscribed', () => inOrganizationScenarios(['entitlement-group-jira-inactive']))
  .add('Error', () => inScenarios(['bill-estimate-error'], inPageChrome(render(ManageSubscriptionsPage))));
