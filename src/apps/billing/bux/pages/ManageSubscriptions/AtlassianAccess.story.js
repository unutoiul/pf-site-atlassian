import { storiesOf } from '@storybook/react';
import { asOrganization, inScenarios, inPageChrome, render } from 'bux/common/helpers/storybook';

import ManageSubscriptionsPage from './ManageSubscriptions';

const inOrganizationScenarios = scenarios => (
  inScenarios(scenarios, inPageChrome(asOrganization(render(ManageSubscriptionsPage))))
);

storiesOf('BUX|Pages/Manage subscriptions/Atlassian access', module)
  .add('Default', () => inOrganizationScenarios([
    'entitlement-group-atlassian-access', 'bill-estimate-atlassian-access'
  ]))
  .add('Annual', () => inOrganizationScenarios([
    'entitlement-group-atlassian-access', 'bill-estimate-atlassian-access', 'bill-estimate-annual'
  ]))
  .add('Above tier', () => inOrganizationScenarios([
    'entitlement-group-atlassian-access', 'bill-estimate-atlassian-access-trello-above'
  ]))
  .add('Above tier annual', () => inOrganizationScenarios([
    'entitlement-group-atlassian-access', 'bill-estimate-atlassian-access-trello-above', 'bill-estimate-annual'
  ]));
