import React from 'react';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import { Route, Switch, withRouter } from 'react-router-dom';
import AutoFocus from 'bux/components/behaviour/focus/AutoFocus';
import { ManageSubscriptions } from 'bux/features/ManageSubscriptions/components';
import { asPageContent } from 'bux/components/Page/Page';
import { ChangeEditionPage } from 'bux/features/ManageSubscriptions/components/ChangeEdition';

const FocusedManageSubscriptions = ({ match }) => (
  <AutoFocus>
    <ManageSubscriptions />
    <Switch>
      <Route path={`${match.url}/change-edition/:entitlementId`} component={ChangeEditionPage} />
    </Switch>
  </AutoFocus>
);

FocusedManageSubscriptions.propTypes = {
  match: PropTypes.object.isRequired
};

export default compose(
  asPageContent({
    name: 'billingApplicationsScreen'
  }),
  withRouter,
)(FocusedManageSubscriptions);
