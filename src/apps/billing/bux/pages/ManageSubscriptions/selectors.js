import { combineAllDisplayable } from 'bux/core/selectors/metadata';
import entitlementGroup from 'bux/core/state/entitlement-group';
import billingDetails from 'bux/core/state/billing-details';
import billEstimate from 'bux/core/state/bill-estimate';
import recommendedProducts from 'bux/core/state/recommended-products';
import { getUserLocationMetadata } from 'bux/core/state/meta/selectors';

const isLoaded = state => combineAllDisplayable([
  getUserLocationMetadata,
  recommendedProducts.selectors.getRecommendedProductsMetadata,
  entitlementGroup.selectors.getEntitlementGroupMetadata,
  billEstimate.selectors.getBillEstimateMetadata,
  billingDetails.selectors.getBillingDetailsMetadata
])(state);

export { isLoaded };
