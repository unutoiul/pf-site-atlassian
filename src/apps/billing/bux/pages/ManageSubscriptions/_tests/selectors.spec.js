import proxyquire from 'bux/helpers/proxyquire';
import sinon from 'sinon';
import { expect } from 'chai';
import { combineAllDisplayable } from 'bux/core/selectors/metadata';

describe('ManageSubscriptions selectors:', () => {
  let selectors;
  let getUserLocationMetadata;
  let getBillingDetailsMetadata;
  let getBillEstimateMetadata;
  let getEntitlementGroupMetadata;


  beforeEach(() => {
    getUserLocationMetadata = sinon.stub();
    getBillingDetailsMetadata = sinon.stub();
    getEntitlementGroupMetadata = sinon.stub();
    getBillEstimateMetadata = sinon.stub();

    selectors = proxyquire.noCallThru().load('../selectors', {
      'bux/core/selectors/metadata': {
        combineAllDisplayable
      },
      'bux/core/state/entitlement-group': {
        selectors: { getEntitlementGroupMetadata }
      },
      'bux/core/state/bill-estimate': {
        selectors: { getBillEstimateMetadata }
      },
      'bux/core/state/billing-details': {
        selectors: { getBillingDetailsMetadata }
      },
      'bux/core/state/meta/selectors': {
        getUserLocationMetadata
      }
    });
  });

  describe('isLoaded:', () => {
    it('should be loading when all sources are loading', () => {
      getUserLocationMetadata.returns({ loading: true });
      getBillingDetailsMetadata.returns({ loading: true });
      getBillEstimateMetadata.returns({ loading: true });
      getEntitlementGroupMetadata.returns({ loading: true });

      expect(selectors.isLoaded().loading).to.be.true();
    });

    it('should not be loading when all sources are loaded', () => {
      getUserLocationMetadata.returns({ loading: false });
      getBillingDetailsMetadata.returns({ loading: false });
      getBillEstimateMetadata.returns({ loading: false });
      getEntitlementGroupMetadata.returns({ loading: false });

      expect(selectors.isLoaded().loading).to.be.false();
    });

    it('should not be loading when any source is not loaded', () => {
      getUserLocationMetadata.returns({ loading: true });
      getBillingDetailsMetadata.returns({ loading: true });
      getBillEstimateMetadata.returns({ loading: false });
      getEntitlementGroupMetadata.returns({ loading: true });

      expect(selectors.isLoaded().loading).to.be.true();
    });
  });
});
