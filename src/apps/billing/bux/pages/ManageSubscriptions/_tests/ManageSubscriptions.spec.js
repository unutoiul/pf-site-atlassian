import React from 'react';
import { remock } from 'react-remock';
import sinon from 'sinon';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';
import { UserFeatureFlag } from 'common/feature-flags';
import { Provider } from 'bux/helpers/redux';
import proxyquire from 'bux/helpers/proxyquire';
import Page from 'bux/components/Page';
import { BillingBreadcrumb } from 'bux/components/Breadcrumb';

import ManageSubscriptionsPage, { AugmentedComponent } from '../ManageSubscriptions';
import ConnectedManageSubscriptions from '../content';


describe('ManageSubscriptions page: ', () => {
  it('should trigger callbacks on Component mount', (done) => {
    const isLoaded = sinon.stub().returns({ display: true });
    const getEntitlementData = sinon.stub().returns(() => {
    });

    // eslint-disable-next-line no-shadow
    const { AugmentedComponent } = proxyquire
      .noCallThru()
      .load('../ManageSubscriptions', {
        './selectors': { isLoaded },
        './actions': { getEntitlementData }
      });

    const wrapper = mount(<Provider><AugmentedComponent /></Provider>);

    expect(getEntitlementData).to.have.been.called();
    expect(isLoaded).to.have.been.called();
    setImmediate(() => {
      expect(wrapper.update().find(ConnectedManageSubscriptions)).to.be.present();
      done();
    });
  });

  it('should pass render content', () => {
    remock.push();
    remock.renderProp(UserFeatureFlag, { value: false });
    const wrapper = shallow(<ManageSubscriptionsPage />);
    const page = wrapper.find(Page);

    expect(wrapper.find(AugmentedComponent)).to.be.present();
    expect(page).to.have.prop('title', 'Manage subscriptions');
    expect(page).to.have.prop('logPrefix', 'manageSubscriptions');
    remock.pop();
  });

  it('should render back button for old nav', () => {
    remock.push();
    remock.renderProp(UserFeatureFlag, { value: false });
    remock.mock(AugmentedComponent);
    const wrapper = mount(<Provider><ManageSubscriptionsPage /></Provider>);
    expect(wrapper.find(BillingBreadcrumb)).to.be.present();
    remock.pop();
  });

  it('should not display back button for a new navigation', () => {
    remock.push();
    remock.renderProp(UserFeatureFlag, { value: true });
    remock.mock(AugmentedComponent);
    const wrapper = mount(<Provider><ManageSubscriptionsPage /></Provider>);
    expect(wrapper.find(BillingBreadcrumb)).not.to.be.present();
    remock.pop();
  });
});
