import proxyquire from 'bux/helpers/proxyquire';
import sinon from 'sinon';
import { expect } from 'chai';

describe('ManageSubscriptions actions:', () => {
  let actions;
  let getBillingDetails;
  let getBillEstimate;
  let getEntitlementGroup;

  beforeEach(() => {
    getBillingDetails = sinon.stub();
    getBillEstimate = sinon.stub();
    getEntitlementGroup = sinon.spy();

    actions = proxyquire.noCallThru().load('../actions', {
      'bux/core/state/entitlement-group': {
        actions: { getEntitlementGroup }
      },
      'bux/core/state/bill-estimate': {
        actions: { getBillEstimate }
      },
      'bux/core/state/billing-details': {
        actions: { getBillingDetails }
      }
    });
  });

  describe('getEntitlementData', () => {
    const dispatch = () => {};

    it('should fetch billing details data', () => {
      actions.getEntitlementData()(dispatch);
      expect(getBillingDetails).to.have.been.called();
    });

    it('should fetch bill estimate data', () => {
      actions.getEntitlementData()(dispatch);
      expect(getBillEstimate).to.have.been.called();
    });

    it('should fetch entitlement information', () => {
      actions.getEntitlementData()(dispatch);
      expect(getEntitlementGroup).to.have.been.called();
    });
  });
});
