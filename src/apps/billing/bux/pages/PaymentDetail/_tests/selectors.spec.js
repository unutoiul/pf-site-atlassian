import proxyquire from 'bux/helpers/proxyquire';
import sinon from 'sinon';
import { expect } from 'chai';
import { combineAllDisplayable } from 'bux/core/selectors/metadata';

describe('PaymentDetail selectors:', () => {
  let selectors;
  let getBillingDetailsMetadata;
  let getUserLocationMetadata;
  let getCountryListMetadata;
  let getBillEstimateMetadata;
  let getEntitlementGroupMetadata;


  beforeEach(() => {
    getBillingDetailsMetadata = sinon.stub();
    getUserLocationMetadata = sinon.stub();
    getCountryListMetadata = sinon.stub();
    getBillEstimateMetadata = sinon.stub();
    getEntitlementGroupMetadata = sinon.stub();

    selectors = proxyquire.noCallThru().load('../selectors', {
      'bux/core/selectors/metadata': {
        combineAllDisplayable
      },
      'bux/core/state/meta': {
        selectors: {
          getUserLocationMetadata,
          getCountryListMetadata
        }
      },
      'bux/core/state/bill-estimate': {
        selectors: { getBillEstimateMetadata }
      },
      'bux/core/state/billing-details': {
        selectors: { getBillingDetailsMetadata }
      },
      'bux/core/state/entitlement-group': {
        selectors: { getEntitlementGroupMetadata }
      },
    });
  });

  describe('isLoaded:', () => {
    it('should be loading when all sources are loading', () => {
      getBillingDetailsMetadata.returns({ loading: true });
      getUserLocationMetadata.returns({ loading: true });
      getCountryListMetadata.returns({ loading: true });
      getBillEstimateMetadata.returns({ loading: true });
      getEntitlementGroupMetadata.returns({ loading: true });

      expect(selectors.isLoaded().loading).to.be.true();

      expect(getBillingDetailsMetadata).to.be.called();
      expect(getUserLocationMetadata).to.be.called();
      expect(getCountryListMetadata).to.be.called();
      expect(getBillEstimateMetadata).to.be.called();
      expect(getEntitlementGroupMetadata).to.be.called();
    });

    it('should not be loading when all sources are loaded', () => {
      getBillingDetailsMetadata.returns({ loading: false });
      getUserLocationMetadata.returns({ loading: false });
      getCountryListMetadata.returns({ loading: false });
      getBillEstimateMetadata.returns({ loading: false });
      getEntitlementGroupMetadata.returns({ loading: false });

      expect(selectors.isLoaded().loading).to.be.false();
    });

    it('should not be loading when any source is not loaded', () => {
      getBillingDetailsMetadata.returns({ loading: true });
      getUserLocationMetadata.returns({ loading: false });
      getCountryListMetadata.returns({ loading: false });
      getBillEstimateMetadata.returns({ loading: false });
      getEntitlementGroupMetadata.returns({ loading: true });

      expect(selectors.isLoaded().loading).to.be.true();
    });

    it('should allow secondary resource to update', () => {
      const LOADED = { display: true, loading: false, error: false };
      getBillingDetailsMetadata.returns(LOADED);
      getUserLocationMetadata.returns(LOADED);
      getCountryListMetadata.returns(LOADED);
      getBillEstimateMetadata.returns(LOADED);
      getEntitlementGroupMetadata.returns({ ...LOADED, loading: true });

      expect(selectors.isLoaded().display).to.be.true();
    });

    it('should not allow primary resource to update', () => {
      const LOADED = { display: true, loading: false, error: false };
      getBillingDetailsMetadata.returns({ ...LOADED, loading: true });
      getUserLocationMetadata.returns(LOADED);
      getCountryListMetadata.returns(LOADED);
      getBillEstimateMetadata.returns(LOADED);
      getEntitlementGroupMetadata.returns(LOADED);

      expect(selectors.isLoaded().display).to.be.false();
    });
  });
});
