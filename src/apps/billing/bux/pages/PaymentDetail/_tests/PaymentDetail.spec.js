import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow, mount } from 'enzyme';
import { Provider } from 'bux/helpers/redux';
import proxyquire from 'bux/helpers/proxyquire';
import Page from 'bux/components/Page';
import { PaymentDetailsForm } from 'bux/features/payment-details/components';

import PaymentDetails from '../PaymentDetail';
import ConnectedPaymentDetailsPage, { PaymentDetailPage } from '../content';

describe('PaymentDetail page:', () => {
  it('should render Billing details form', () => {
    const wrapper = shallow(<PaymentDetailPage match={{}} />);
    expect(wrapper.find(PaymentDetailsForm)).to.be.present();
  });

  it('should render Page component', () => {
    const wrapper = shallow(<PaymentDetails />);
    const page = wrapper.find(Page);
    expect(page).to.have.prop('title', 'Update billing details');
    expect(page).to.have.prop('logPrefix', 'paymentDetails');
  });

  it('should trigger callbacks on Component mount', (done) => {
    const isLoaded = sinon.stub().returns({ display: true });
    const getPaymentDetailsData = sinon.stub().returns(() => {});

    // eslint-disable-next-line no-shadow
    const { AugmentedComponent } = proxyquire.noCallThru().load('../PaymentDetail', {
      './selectors': { isLoaded },
      './actions': { getPaymentDetailsData }
    });

    const wrapper = mount(<Provider><AugmentedComponent /></Provider>);

    expect(getPaymentDetailsData).to.have.been.called();
    expect(isLoaded).to.have.been.called();
    setImmediate(() => {
      expect(wrapper.update().find(ConnectedPaymentDetailsPage)).to.be.present();
      wrapper.unmount();
      done();
    });
  });
});

