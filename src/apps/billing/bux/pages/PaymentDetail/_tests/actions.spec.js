import proxyquire from 'bux/helpers/proxyquire';
import sinon from 'sinon';
import { expect } from 'chai';

describe('PaymentDetail actions:', () => {
  let actions;
  let getBillingDetails;
  let getBillEstimate;
  let getUserMetadata;
  let getCountryList;
  let getEntitlementGroup;

  beforeEach(() => {
    getBillingDetails = sinon.stub();
    getBillEstimate = sinon.stub();
    getUserMetadata = sinon.stub();
    getCountryList = sinon.stub();
    getEntitlementGroup = sinon.stub();

    actions = proxyquire.noCallThru().load('../actions', {
      'bux/core/state/meta': {
        actions: { getUserMetadata, getCountryList }
      },
      'bux/core/state/bill-estimate': {
        actions: { getBillEstimate }
      },
      'bux/core/state/entitlement-group': {
        actions: { getEntitlementGroup }
      },
      'bux/core/state/billing-details': {
        actions: { getBillingDetails }
      }
    });
  });

  describe('getPaymentDetailsData', () => {
    const dispatch = () => {};

    it('should fetch billing details data', () => {
      actions.getPaymentDetailsData()(dispatch);
      expect(getBillingDetails).to.have.been.called();
    });

    it('should fetch user metadata data', () => {
      actions.getPaymentDetailsData()(dispatch);
      expect(getUserMetadata).to.have.been.called();
    });

    it('should fetch bill estimate data', () => {
      actions.getPaymentDetailsData()(dispatch);
      expect(getBillEstimate).to.have.been.called();
    });

    it('should fetch country list data', () => {
      actions.getPaymentDetailsData()(dispatch);
      expect(getCountryList).to.have.been.called();
    });

    it('should fetch entitlement groups', () => {
      actions.getPaymentDetailsData()(dispatch);
      expect(getEntitlementGroup).to.have.been.called();
    });
  });
});
