import billingDetails from 'bux/core/state/billing-details';
import entitlementGroup from 'bux/core/state/entitlement-group';
import billEstimate from 'bux/core/state/bill-estimate';
import meta from 'bux/core/state/meta';

export const getPaymentDetailsData = () => (dispatch) => {
  dispatch(entitlementGroup.actions.getEntitlementGroup());
  dispatch(billingDetails.actions.getBillingDetails());
  dispatch(billEstimate.actions.getBillEstimate());
  dispatch(meta.actions.getUserMetadata());
  dispatch(meta.actions.getCountryList());
};
