import { combineAllDisplayable } from 'bux/core/selectors/metadata';
import entitlementGroup from 'bux/core/state/entitlement-group';
import billEstimate from 'bux/core/state/bill-estimate';
import billingDetails from 'bux/core/state/billing-details';
import meta from 'bux/core/state/meta';

const allLoaded = callback => (state) => {
  const metadata = callback(state);
  return {
    ...metadata,
    display: metadata.display && !metadata.loading
  };
};

const combiner = combineAllDisplayable([
  entitlementGroup.selectors.getEntitlementGroupMetadata,
  billEstimate.selectors.getBillEstimateMetadata,

  allLoaded(combineAllDisplayable([
    billingDetails.selectors.getBillingDetailsMetadata,
    meta.selectors.getUserLocationMetadata,
    meta.selectors.getCountryListMetadata
  ]))
]);


export const isLoaded = state => combiner(state);
