import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { push as redirect } from 'bux/core/state/router/actions';
import AutoFocus from 'bux/components/behaviour/focus/AutoFocus';
import { PaymentDetailsForm } from 'bux/features/payment-details/components';
import { UpdatePaymentMethodPage } from 'bux/features/payment-details/components/UpdatePaymentMethod';
import { AddPaymentDetailsPage } from 'bux/features/payment-details/components/AddPaymentDetails';
import { asPageContent } from 'bux/components/Page/Page';
import { shouldNotBeManaged } from 'bux/components/Page/ManagementWall';
import { Route, Switch, withRouter } from 'react-router-dom';
import { MonthlyToAnnualPage } from 'bux/features/monthly-to-annual/components';
import { isNextBillingPeriodRenewalFrequencyAnnual } from 'bux/core/state/bill-estimate/selectors';
import { hasPaymentMethod } from 'bux/core/state/billing-details/selectors';

export class PaymentDetailPage extends Component {
  static propTypes = {
    redirect: PropTypes.func,
    isAnnual: PropTypes.bool.isRequired,
    hasPaymentMethodOnAccount: PropTypes.bool.isRequired,
    match: PropTypes.object.isRequired,
  };

  onSubmitSuccess = () => {
    this.props.redirect('/overview');
  };

  render() {
    const { match } = this.props;
    return (
      <AutoFocus>
        <PaymentDetailsForm onSubmitSuccess={this.onSubmitSuccess} />
        <Switch>
          <Route path={`${match.url}/update`} component={UpdatePaymentMethodPage} />
          <Route path={`${match.url}/add`} component={AddPaymentDetailsPage} />
          <Route path={`${match.url}/switch-to-annual`} component={MonthlyToAnnualPage} />
        </Switch>
      </AutoFocus>
    );
  }
}

const mapStateToProps = state => ({
  isAnnual: isNextBillingPeriodRenewalFrequencyAnnual(state),
  hasPaymentMethodOnAccount: hasPaymentMethod(state),
});

const mapDispatchToProps = {
  redirect
};

const mapPropsToAnalytics = ({ hasPaymentMethodOnAccount, isAnnual }) => ({
  name: 'billingDetailsScreen',
  attributes: {
    hasPaymentMethodOnAccount,
    isAnnual,
  },
});

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
  asPageContent(mapPropsToAnalytics),
  shouldNotBeManaged,
)(PaymentDetailPage);
