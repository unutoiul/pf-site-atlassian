import { storiesOf } from '@storybook/react';
import { inScenarios, inPageChrome, render } from 'bux/common/helpers/storybook';

import PaymentDetailPage from './PaymentDetail';

storiesOf('BUX|Pages/Billing details', module)
  .add('Default', () => inScenarios([], inPageChrome(render(PaymentDetailPage))))
  .add('No Payment method', () => inScenarios(
    ['billing-details-no-payment-method'],
    inPageChrome(render(PaymentDetailPage))
  ))
  .add('PayPal', () => inScenarios(
    ['billing-details-paypal'],
    inPageChrome(render(PaymentDetailPage))
  ))
  .add('Annual', () => inScenarios(
    ['bill-estimate-annual'],
    inPageChrome(render(PaymentDetailPage))
  ))
  .add('Error', () => inScenarios(
    ['billing-details-error'],
    inPageChrome(render(PaymentDetailPage))
  ));
