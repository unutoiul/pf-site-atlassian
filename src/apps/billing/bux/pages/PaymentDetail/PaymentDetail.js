import { I18n } from '@lingui/react';
import React from 'react';
import Page from 'bux/components/Page';
import withAsyncPageLoader from 'bux/components/withAsyncPageLoader';
import { Breadcrumbs, BillingBreadcrumb } from 'bux/components/Breadcrumb';
import { isLoaded } from './selectors';
import { getPaymentDetailsData } from './actions';

const loader = () => import(/* webpackChunkName:'billing' */ './content');
export const AugmentedComponent = withAsyncPageLoader(isLoaded, getPaymentDetailsData)(loader);

const breadcrumbs = (
  <Breadcrumbs>
    <BillingBreadcrumb />
  </Breadcrumbs>
);

export default () => (
  <I18n>{({ i18n }) => (
    <Page
      title={i18n.t('billing.payment-details.page-title')`Update billing details`}
      logPrefix="paymentDetails"
      breadcrumbs={breadcrumbs}
    >
    <AugmentedComponent />
  </Page>)}
</I18n>
);
