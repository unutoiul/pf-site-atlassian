import { I18n } from '@lingui/react';
import React from 'react';
import Page from 'bux/components/Page';
import withAsyncPageLoader from 'bux/components/withAsyncPageLoader';
import { Breadcrumbs } from 'bux/components/Breadcrumb';
import { isLoaded } from './selectors';
import { getRecommendedProductsAndAddonsData } from './actions';

const loader = () => import(/* webpackChunkName:'billing' */ './content');
export const AugmentedComponent = withAsyncPageLoader(isLoaded, getRecommendedProductsAndAddonsData)(loader);

const breadcrumbs = (
  <Breadcrumbs />
);

export default () => (
  <I18n>{({ i18n }) => (
    <Page
      title={i18n.t('billing.discover.page-title')`Discover new products`}
      logPrefix="discoverNewProducts"
      breadcrumbs={breadcrumbs}
    >
      <AugmentedComponent />
    </Page>
  )}</I18n>
);
