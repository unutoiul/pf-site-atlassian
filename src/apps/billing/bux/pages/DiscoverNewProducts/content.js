import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { asPageContent } from 'bux/components/Page/Page';
import { isRequestingProductOptOut, getFocusProductKey } from 'bux/core/selectors/common';
import ContentAsideLayout from 'bux/components/layout/ContentAsideLayout';
import PageLayout from 'bux/components/layout/PageLayout';
import {
  ConnectedProductList,
  AppList,
  ExtraProductsList,
  RequestProductOptOut,
  ConnectedPromotedProduct,
  AsideSubscriptons
} from 'bux/features/DiscoverNewProducts/components';
import { push } from 'bux/core/state/router/actions';
import { Media } from 'common/responsive/Matcher';

const DiscoverNewProduct = ({ shouldDisplayOptOut, redirect, focusProductKey }) => (
  <PageLayout>
    <Media.Above mobile>
      <ConnectedPromotedProduct
        focusProductKey={!shouldDisplayOptOut ? focusProductKey : undefined}
      />
    </Media.Above>
    <ContentAsideLayout
      content={
        <div>
          <ConnectedProductList
            focusProductKey={!shouldDisplayOptOut ? focusProductKey : undefined}
          />
          <AppList />
          <ExtraProductsList />
          {shouldDisplayOptOut && <RequestProductOptOut redirect={redirect} />}
        </div>
      }
      aside={<div style={{ marginTop: 72 }}><AsideSubscriptons /></div>}
    />
  </PageLayout>
);

DiscoverNewProduct.propTypes = {
  shouldDisplayOptOut: PropTypes.bool.isRequired,
  focusProductKey: PropTypes.string,
  redirect: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  shouldDisplayOptOut: isRequestingProductOptOut(state),
  focusProductKey: getFocusProductKey(state)
});

const mapDispatchToProps = {
  redirect: push
};

const ConnectedDiscoverNewProduct = connect(
  mapStateToProps,
  mapDispatchToProps
)(DiscoverNewProduct);

export default asPageContent({
  name: 'discoverNewProductsScreen'
})(ConnectedDiscoverNewProduct);
