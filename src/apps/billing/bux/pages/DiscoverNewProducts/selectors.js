import { combineAllDisplayable } from 'bux/core/selectors/metadata';
import { getRecommendedProductsMetadata } from 'bux/core/state/recommended-products/selectors';
import { getEntitlementGroupMetadata } from 'bux/core/state/entitlement-group/selectors';

export const isLoaded = combineAllDisplayable([
  getRecommendedProductsMetadata,
  getEntitlementGroupMetadata
]);
