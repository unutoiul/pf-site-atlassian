import { getRecommendedProducts } from 'bux/core/state/recommended-products/actions';
import { getBillEstimate } from 'bux/core/state/bill-estimate/actions';
import entitlementGroup from 'bux/core/state/entitlement-group';

export const getRecommendedProductsAndAddonsData = () => (dispatch) => {
  dispatch(getBillEstimate());
  dispatch(getRecommendedProducts());
  dispatch(entitlementGroup.actions.getEntitlementGroup());
};
