import { storiesOf } from '@storybook/react';
import { inScenarios, inPageChrome, render, asSite } from 'bux/common/helpers/storybook';

import DiscoverNewProductsPage from './DiscoverNewProducts';

storiesOf('BUX|Pages/Discover new products', module)
  .add('Default', () => inScenarios([], inPageChrome(asSite(render(DiscoverNewProductsPage)))));
