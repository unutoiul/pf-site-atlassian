import React from 'react';
import sinon from 'sinon';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';
import { Provider } from 'bux/helpers/redux';
import proxyquire from 'bux/helpers/proxyquire';
import Page from 'bux/components/Page';

import DiscoverNewProductsPage, { AugmentedComponent } from '../DiscoverNewProducts';

describe('DiscoverNewProducts page: ', () => {
  it('should trigger callbacks on Component mount', () => {
    const isLoaded = sinon.stub().returns({ display: false });
    const getRecommendedProductsAndAddonsData = sinon.stub().returns(() => {});

    // eslint-disable-next-line no-shadow
    const { AugmentedComponent } = proxyquire
      .noCallThru()
      .load('../DiscoverNewProducts', {
        './selectors': { isLoaded },
        './actions': { getRecommendedProductsAndAddonsData }
      });

    mount(<Provider><AugmentedComponent /></Provider>);

    expect(getRecommendedProductsAndAddonsData).to.have.been.called();
    expect(isLoaded).to.have.been.called();

    // TODO: This test is failing in CLI without any clear reason
    // setImmediate(() => {
    //   expect(wrapper.update().find(ConnectedManageSubscriptions)).to.be.present();
    //   done();
    // });
  });

  it('should pass title and metadata', () => {
    const wrapper = shallow(<DiscoverNewProductsPage />);
    const page = wrapper.find(Page);

    expect(page).to.have.prop('title', 'Discover new products');
    expect(page).to.have.prop('logPrefix', 'discoverNewProducts');
    expect(wrapper.find(AugmentedComponent)).to.be.present();
  });
});
