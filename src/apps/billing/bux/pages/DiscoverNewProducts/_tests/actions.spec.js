import proxyquire from 'bux/helpers/proxyquire';
import sinon from 'sinon';
import { expect } from 'chai';

describe('DiscoverNewProducts actions:', () => {
  let actions;
  let getRecommendedProducts;
  let getEntitlementGroup;

  beforeEach(() => {
    getRecommendedProducts = sinon.stub();
    getEntitlementGroup = sinon.spy();

    actions = proxyquire.noCallThru().load('../actions', {
      'bux/core/state/entitlement-group': {
        actions: { getEntitlementGroup }
      },
      'bux/core/state/recommended-products/actions': {
        getRecommendedProducts
      },
    });
  });

  describe('getEntitlementData', () => {
    const dispatch = () => {};

    it('should fetch recommended products data', () => {
      actions.getRecommendedProductsAndAddonsData()(dispatch);
      expect(getRecommendedProducts).to.have.been.called();
    });

    it('should fetch entitlement information', () => {
      actions.getRecommendedProductsAndAddonsData()(dispatch);
      expect(getEntitlementGroup).to.have.been.called();
    });
  });
});
