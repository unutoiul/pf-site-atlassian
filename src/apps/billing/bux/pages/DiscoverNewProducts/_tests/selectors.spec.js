import proxyquire from 'bux/helpers/proxyquire';
import sinon from 'sinon';
import { expect } from 'chai';
import { combineAllDisplayable } from 'bux/core/selectors/metadata';

describe('DiscoverNewProducts selectors:', () => {
  let selectors;
  let getRecommendedProductsMetadata;
  let getEntitlementGroupMetadata;


  beforeEach(() => {
    getEntitlementGroupMetadata = sinon.stub().returns({});
    getRecommendedProductsMetadata = sinon.stub().returns({});

    selectors = proxyquire.noCallThru().load('../selectors', {
      'bux/core/selectors/metadata': {
        combineAllDisplayable
      },
      'bux/core/state/entitlement-group/selectors': {
        getEntitlementGroupMetadata
      },
      'bux/core/state/recommended-products/selectors': {
        getRecommendedProductsMetadata
      }
    });
  });

  describe('isLoaded:', () => {
    it('should be loading when all sources are loading', () => {
      getRecommendedProductsMetadata.returns({ loading: true });
      getEntitlementGroupMetadata.returns({ loading: true });

      expect(selectors.isLoaded().loading).to.be.true();
    });

    it('should not be loading when all sources are loaded', () => {
      getRecommendedProductsMetadata.returns({ loading: false });
      getEntitlementGroupMetadata.returns({ loading: false });

      expect(selectors.isLoaded().loading).to.be.false();
    });

    it('should call proper selectors', () => {
      selectors.isLoaded();
      expect(getRecommendedProductsMetadata).to.have.been.called();
      expect(getEntitlementGroupMetadata).to.have.been.called();
    });
  });
});
