import { I18n } from '@lingui/react';
import React from 'react';
import Page from 'bux/components/Page';
import withAsyncPageLoader from 'bux/components/withAsyncPageLoader';
import { Breadcrumbs } from 'bux/components/Breadcrumb';
import { isLoaded } from './selectors';
import { getBillOverviewData } from './actions';

const loader = () => import(/* webpackChunkName:'billing' */ './content');
export const AugmentedComponent = withAsyncPageLoader(isLoaded, getBillOverviewData)(loader);

const breadcrumbs = (
  <Breadcrumbs />
);

export default () => (
  <I18n>{({ i18n }) => (
    <Page
      title={i18n.t('billing.overview.page-title')`Billing overview`}
      documentTitle={i18n.t('billing.overview.document-title')`Overview`}
      logPrefix="billOverview"
      breadcrumbs={breadcrumbs}
    >
      <AugmentedComponent />
    </Page>
  )}</I18n>
);
