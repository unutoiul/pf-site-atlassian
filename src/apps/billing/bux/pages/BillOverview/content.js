import { I18n, Trans } from '@lingui/react';
import PropTypes from 'prop-types';
import React from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import SectionMessage from '@atlaskit/section-message';
import { asPageContent } from 'bux/components/Page/Page';
import AutoFocus from 'bux/components/behaviour/focus/AutoFocus';
import { InvoiceContactWidget, PaymentDetailsWidget, PaymentDetailsPrompt }
  from 'bux/features/payment-details/components';
import { BillEstimateWidget } from 'bux/features/bill-estimate/components';
import { BillHistoryWidget } from 'bux/features/bill-history/components';
import ConnectedManagementWall from 'bux/components/Page/ManagementWall/ManagementWall';
import AccountIsUnderManagementNotice from 'bux/components/Page/ManagementWall/AccountIsUnderManagementNotice';
import Link from 'bux/components/Link';
import billHistory from 'bux/core/state/bill-history';

import billingDetails from 'bux/core/state/billing-details';
import {
  isNextBillingPeriodRenewalFrequencyAnnual,
  isNextBillInNewCurrency as isAfterTheCutOverDate,
  upcomingCurrencyExtras
} from 'bux/core/state/bill-estimate/selectors';
import CurrencyUpdateNotification from 'bux/components/CurrencyUpdateNotification';
import { withAnalytics } from 'bux/components/Analytics';
import { UserFeatureFlag } from 'common/feature-flags';
import FeatureFlagged from '../../components/FeatureFlags/FeatureFlagged';
import styles from './styles.less';

const sendAnalytics = analytics => () => {
  const event = {
    actionSubject: 'link',
    actionSubjectId: 'learnMoreHereLink',
    action: 'clicked',
    attributes: { 
      linkContainer: 'banner',
      objectType: 'pricing'
    }
  };
  analytics.sendUIEvent(event);
};

export const WarnPricingChangesMessage = withAnalytics(({ analytics }) => (
  <I18n>{({ i18n }) => (
    <SectionMessage
      actions={[
        {
          href: 'https://www.atlassian.com/licensing/future-pricing',
          onClick: sendAnalytics(analytics),
          text: i18n.t('billing.overview.price-change-warn.learn-more')`Learn more here`,
        }]}
      appearance="info"
    >
      <Trans id="billing.overview.price-change-warn.info">
        Atlassian is adjusting its Cloud and Server pricing effective October 12, 2018.
      </Trans>
    </SectionMessage>
  )}</I18n>
));

export const InfoPricingChangedMessage = withAnalytics(({ analytics }) => (
  <I18n>{({ i18n }) => (
    <SectionMessage
      actions={[
        {
          href: 'https://www.atlassian.com/licensing/future-pricing',
          onClick: sendAnalytics(analytics),
          text: i18n.t('billing.overview.price-change-warn.learn-more')`Learn more here`,
        }]}
      appearance="info"
    >
      <Trans id="billing.overview.price-changed-info.info">
        Atlassian recently changed its Cloud and Server pricing.
      </Trans>
    </SectionMessage>
  )}</I18n>
));

export const OldBillingHistoryMessage = () => (
  <SectionMessage>
    <Trans id="billing.overview.old-billing-history">
      To see your invoices from when you were directly billed, please go to <Link to="/history">Bill History</Link>
    </Trans>
  </SectionMessage>
);

export const ManagedOverview = ({ invoices }) => (
  <React.Fragment>
    {invoices.length ? <OldBillingHistoryMessage /> : null}
    <AccountIsUnderManagementNotice />
  </React.Fragment>
);

ManagedOverview.propTypes = {
  invoices: PropTypes.arrayOf(PropTypes.any)
};

const ConnectedManagementOverview = connect(state => ({
  invoices: billHistory.selectors.getInvoices(state),
}))(ManagedOverview);

export const BillOverviewWidgets = ({
  hasPaymentMethodOnAccount = true, isAnnual, isNextBillInNewCurrency, upcomingCurrencyExtra 
}) => (
  <UserFeatureFlag flagKey="admin.billing.menu.flat" defaultValue={false}>
    { flag =>
      ((flag.value || hasPaymentMethodOnAccount || isAnnual)
        ? (
          <AutoFocus>
            <div>
              <FeatureFlagged flags={['2018-price-update-notice']}>
                <div className={styles.noticesRow} data-test="notices-row">
                  <WarnPricingChangesMessage />
                </div>
              </FeatureFlagged>
              <FeatureFlagged flags={['2018-price-changed-info']}>
                <div className={styles.noticesRow} data-test="post-update-notices-row">
                  <InfoPricingChangedMessage />
                </div>
              </FeatureFlagged>
              <FeatureFlagged flags={['2018-upcoming-currency-notice']}>
                {
                  upcomingCurrencyExtra &&
                  <div className={styles.noticesRow} data-test="currency-update-notices-row">
                    <CurrencyUpdateNotification
                      isNextBillInNewCurrency={isNextBillInNewCurrency}
                      upcomingCurrencyExtra={upcomingCurrencyExtra}
                    />
                  </div>
                }
              </FeatureFlagged>
              <div className={styles.widgetsRow}>
                <BillEstimateWidget className={styles.widget} />
                <PaymentDetailsWidget className={styles.widget} />
              </div>
              <div className={styles.widgetsRow}>
                <BillHistoryWidget className={styles.widget} />
                <InvoiceContactWidget className={styles.widget} />
              </div>
            </div>
          </AutoFocus>
        ) : (
          <PaymentDetailsPrompt fullPage />
        ))
    }
  </UserFeatureFlag>
);

BillOverviewWidgets.propTypes = {
  hasPaymentMethodOnAccount: PropTypes.bool,
  isAnnual: PropTypes.bool,
  isNextBillInNewCurrency: PropTypes.bool,
  upcomingCurrencyExtra: PropTypes.object,
};

export const BillOverview = ({
  hasPaymentMethodOnAccount, isAnnual, isNextBillInNewCurrency, upcomingCurrencyExtra 
}) => (
  <ConnectedManagementWall wall={<ConnectedManagementOverview />}>
    <BillOverviewWidgets
      hasPaymentMethodOnAccount={hasPaymentMethodOnAccount}
      isAnnual={isAnnual}
      isNextBillInNewCurrency={isNextBillInNewCurrency}
      upcomingCurrencyExtra={upcomingCurrencyExtra}
    />
  </ConnectedManagementWall>
);

BillOverview.propTypes = {
  hasPaymentMethodOnAccount: PropTypes.bool,
  isAnnual: PropTypes.bool,
  isNextBillInNewCurrency: PropTypes.bool,
  upcomingCurrencyExtra: PropTypes.object
};

export const mapStateToProps = state => ({
  hasPaymentMethodOnAccount: billingDetails.selectors.hasPaymentMethod(state),
  isAnnual: isNextBillingPeriodRenewalFrequencyAnnual(state),
  upcomingCurrencyExtra: upcomingCurrencyExtras(state),
  isNextBillInNewCurrency: isAfterTheCutOverDate(state),
});

const mapPropsToAnalytics = ({
  hasPaymentMethodOnAccount,
  isAnnual
}) => ({
  name: 'billingOverviewScreen',
  attributes: {
    hasPaymentMethodOnAccount, 
    isAnnual
  }
});

export default compose(
  connect(mapStateToProps),
  asPageContent(mapPropsToAnalytics),
)(BillOverview);
