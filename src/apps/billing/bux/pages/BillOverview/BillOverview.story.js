
import { storiesOf } from '@storybook/react';
import { inScenarios, inPageChrome, render, withFeatureFlagsChrome, withUserFeatureFlag }
  from 'bux/common/helpers/storybook';
import BillOverviewPage from './BillOverview';

storiesOf('BUX|Pages/Billing overview', module)
  .add('Default', () => inScenarios([], inPageChrome(render(BillOverviewPage))))
  .add('Changed price notice', () => inScenarios(
    [],
    inPageChrome(withFeatureFlagsChrome({
      '2018-price-update-notice': false,
      '2018-price-changed-info': true
    }, render(BillOverviewPage)))
  ))
  .add('No Payment method - fullpage prompt', () => inScenarios(
    ['billing-details-no-payment-method'],
    inPageChrome(render(BillOverviewPage))
  ))
  .add('No Payment method - widget prompt', () => inScenarios(
    ['billing-details-no-payment-method'],
    withUserFeatureFlag('admin.billing.menu.flat', render(BillOverviewPage))
  ))
  .add('PayPal', () => inScenarios(
    ['billing-details-paypal'],
    inPageChrome(render(BillOverviewPage))
  ))
  .add('Annual', () => inScenarios(
    ['bill-estimate-annual'],
    inPageChrome(render(BillOverviewPage))
  ))
  .add('Billing details error', () => inScenarios(
    ['billing-details-error'],
    inPageChrome(render(BillOverviewPage))
  ))
  .add('Managed by Partner', () => inScenarios(
    ['billing-details-managed-by-partner'],
    inPageChrome(render(BillOverviewPage))
  ))
  .add('Invoice history error', () => inScenarios(
    ['invoice-history-error'],
    inPageChrome(render(BillOverviewPage))
  ));
