import proxyquire from 'bux/helpers/proxyquire';
import sinon from 'sinon';
import { expect } from 'chai';
import { CURRENT } from 'bux/core/state/bill-estimate/constants';
import { INITIAL_BILL_STATE } from 'bux/core/state/bill-estimate/reducer';
import { INITIAL_BILLING_DETAILS_STATE } from 'bux/core/state/billing-details/reducer';

import { isLoaded } from '../selectors';

describe('BillOverview selectors:', () => {
  const validData = {
    data: {},
    lastUpdated: 42,
    display: true,
    loading: false
  };
  const getInitialState = (overrideState = {}) => ({
    billEstimate: { [CURRENT]: { ...INITIAL_BILL_STATE, ...validData, ...overrideState.billEstimate } },
    billingDetails: { ...INITIAL_BILLING_DETAILS_STATE, ...validData, ...overrideState.billingDetails },
  });

  const expectOverviewError = (numWidgetsWithError) => {
    const states = getInitialState();
    Object.keys(states)
      .splice(0, numWidgetsWithError)
      .forEach(key => (
        states[key] = { error: { some: 'error' } }
      ));
    return expect(isLoaded(states).error);
  };

  it('should not be in error if all widgets are ok', () => {
    expectOverviewError(0).to.be.false();
  });

  it('should be in error if one widget is in error', () => {
    expectOverviewError(1).not.to.be.false();
  });

  it('should be in error if two widgets are in error', () => {
    expectOverviewError(2).not.to.be.false();
  });

  it('should be in error if all widgets are failing', () => {
    expectOverviewError(3).not.to.be.false();
  });

  describe('isLoaded:', () => {
    let selectors;
    let getOverviewMetadata;

    beforeEach(() => {
      getOverviewMetadata = sinon.stub();

      selectors = proxyquire.noCallThru().load('../selectors', {
        'bux/core/selectors/metadata': {
          getOverviewMetadata,
        }
      });
    });

    it('should be loading when all sources are loading', () => {
      getOverviewMetadata.returns({ loading: true });
      expect(selectors.isLoaded().loading).to.be.true();
    });

    it('should not be loading when all sources are loaded', () => {
      getOverviewMetadata.returns({ loading: false });
      expect(selectors.isLoaded().loading).to.be.false();
    });
  });
});
