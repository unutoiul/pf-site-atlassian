import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { mount, shallow } from 'enzyme';
import { rewiremock } from 'bux/helpers/rewire-mock';
import { remock } from 'react-remock';
import { Provider } from 'bux/helpers/redux';
import Page from 'bux/components/Page';
import { InvoiceContactWidget, PaymentDetailsWidget, PaymentDetailsPrompt }
  from 'bux/features/payment-details/components';
import { BillEstimateWidget } from 'bux/features/bill-estimate/components';
import { UserFeatureFlag } from 'common/feature-flags';
import BillOverviewPage, { AugmentedComponent } from '../BillOverview';
import ConnectedBillOverview, {
  BillOverview,
  BillOverviewWidgets,
  ManagedOverview,
  OldBillingHistoryMessage,
  mapStateToProps
} from '../content';

describe('BillOverview page: ', () => {
  it('should display billing details prompt container when there is no payment methods', () => {
    const props = mapStateToProps({
      billEstimate: { loading: false },
      billingDetails: { loading: false },
      invoiceContact: { loading: false }
    });

    const wrapper = mount(<Provider><BillOverviewWidgets {...props} /></Provider>);
    expect(wrapper.find(PaymentDetailsPrompt)).to.be.present();
    expect(wrapper.find(PaymentDetailsPrompt)).to.have.prop('fullPage');
  });

  it('should display billing widgets when feature flag is on', () => {
    remock.push();
    remock.renderProp(UserFeatureFlag, { value: true });
    remock.mock(AugmentedComponent);

    const props = mapStateToProps({
      billEstimate: { loading: false },
      billingDetails: { loading: false },
      invoiceContact: { loading: false }
    });

    const wrapper = mount(<Provider><BillOverviewWidgets {...props} /></Provider>);
    expect(wrapper.find(BillEstimateWidget)).to.be.present();
    expect(wrapper.find(PaymentDetailsWidget)).to.be.present();
    expect(wrapper.find(InvoiceContactWidget)).to.be.present();
    remock.pop();
  });

  it('should show set of widgets when already provided CC', () => {
    const props = mapStateToProps({
      billEstimate: { loading: false },
      billingDetails: {
        loading: false,
        data: { creditCard: 'my-precious' },
      },
      invoiceContact: { loading: false }
    });
    const wrapper = mount(<Provider><BillOverviewWidgets {...props} /></Provider>);
    expect(wrapper.find(BillEstimateWidget)).to.be.present();
    expect(wrapper.find(PaymentDetailsWidget)).to.be.present();
    expect(wrapper.find(InvoiceContactWidget)).to.be.present();
  });

  it('should trigger callbacks on Component mount', (done) => {
    const isLoaded = sinon.stub().returns({ display: true });
    const getBillOverviewData = sinon.stub().returns(() => {
    });

    // eslint-disable-next-line no-shadow
    const { AugmentedComponent } = rewiremock.proxy('../BillOverview', {
      './selectors': { isLoaded },
      './actions': { getBillOverviewData }
    });

    const wrapper = mount(<Provider><AugmentedComponent /></Provider>);

    expect(getBillOverviewData).to.have.been.called();
    expect(isLoaded).to.have.been.called();
    setImmediate(() => {
      expect(wrapper.update().find(ConnectedBillOverview)).to.be.present();
      expect(wrapper.update().find(BillOverview)).to.be.present();
      done();
    });
  });

  it('should not display management wall', () => {
    const props = { hasPaymentMethodOnAccount: true };
    const wrapper = mount(<Provider><BillOverview {...props} /></Provider>);
    expect(wrapper.find(ManagedOverview)).not.to.be.present();
  });

  it('should display management wall when account is controlled', () => {
    const isManagedByPartner = sinon.stub().returns(() => true);
    return rewiremock.around(
      () => import('../content'),
      () => {
        rewiremock('bux/core/state/billing-details/selectors')
          .callThrough()
          .atAnyPlace()
          .with({ isManagedByPartner });
      }
    // eslint-disable-next-line no-shadow
    ).then(({ BillOverview, ManagedOverview }) => {
      const wrapper = mount(<Provider><BillOverview /></Provider>);
      expect(wrapper.find(ManagedOverview)).to.be.present();
    });
  });

  it('should display link to billing history only if invoices exists', () => {
    expect(shallow(<ManagedOverview invoices={[]} />).find(OldBillingHistoryMessage)).not.to.be.present();
    expect(shallow(<ManagedOverview invoices={[1]} />).find(OldBillingHistoryMessage)).to.be.present();
  });

  it('should pass title and metadata', () => {
    const wrapper = shallow(<BillOverviewPage />);
    const page = wrapper.find(Page);
    expect(page.props().title).to.equal('Billing overview');
    expect(page).to.have.prop('logPrefix', 'billOverview');
    expect(wrapper.find(AugmentedComponent)).to.be.present();
  });
});
