import proxyquire from 'bux/helpers/proxyquire';
import sinon from 'sinon';
import { expect } from 'chai';

describe('BillOverview actions:', () => {
  let actions;
  let getBillEstimate;
  let getBillHistory;
  let getBillingDetails;
  let getUserMetadata;

  beforeEach(() => {
    getBillingDetails = sinon.stub();
    getBillEstimate = sinon.stub();
    getBillHistory = sinon.stub();
    getUserMetadata = sinon.stub();

    actions = proxyquire.noCallThru().load('../actions', {
      'bux/core/state/bill-estimate': {
        actions: { getBillEstimate }
      },
      'bux/core/state/bill-history': {
        actions: { getBillHistory }
      },
      'bux/core/state/billing-details': {
        actions: { getBillingDetails }
      },
      'bux/core/state/meta': {
        actions: { getUserMetadata }
      }
    });
  });

  describe('BillOverview', () => {
    const dispatch = () => {};

    it('should fetch billing details data', () => {
      actions.getBillOverviewData()(dispatch);
      expect(getBillingDetails).to.have.been.called();
    });

    it('should fetch bill estimate data', () => {
      actions.getBillOverviewData()(dispatch);
      expect(getBillEstimate).to.have.been.called();
    });

    it('should fetch bill history data', () => {
      actions.getBillOverviewData()(dispatch);
      expect(getBillHistory).to.have.been.called();
    });
  });
});
