import billingDetails from 'bux/core/state/billing-details';
import billEstimate from 'bux/core/state/bill-estimate';
import billHistory from 'bux/core/state/bill-history';
import meta from 'bux/core/state/meta';

export const getBillOverviewData = () => (dispatch) => {
  dispatch(billEstimate.actions.getBillEstimate());
  dispatch(billHistory.actions.getBillHistory());
  dispatch(billingDetails.actions.getBillingDetails());
  dispatch(meta.actions.getUserMetadata());
};
