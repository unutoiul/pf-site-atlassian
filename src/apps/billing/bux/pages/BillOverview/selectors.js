import { getOverviewMetadata } from 'bux/core/selectors/metadata';

export const isLoaded = state => getOverviewMetadata(state);
