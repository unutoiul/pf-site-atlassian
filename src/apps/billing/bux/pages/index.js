import billHistory from 'bux/features/bill-history';
import BillEstimate from './BillEstimate';
import BillOverview from './BillOverview';
import PaymentDetail from './PaymentDetail';
import DiscoverNewProducts from './DiscoverNewProducts';
import ManageSubscriptions from './ManageSubscriptions';

const BillHistory = billHistory.Page;
export {
  BillEstimate,
  BillOverview,
  PaymentDetail,
  BillHistory,
  DiscoverNewProducts,
  ManageSubscriptions,
};
