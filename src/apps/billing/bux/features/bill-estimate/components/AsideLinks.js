import { Trans } from '@lingui/react';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { Analytics } from 'bux/components/Analytics';
import { ExternalLinkTo } from 'bux/components/Link';
import LogScope from 'bux/components/log/Scope/index';
import { isNextBillingPeriodRenewalFrequencyAnnual } from 'bux/core/state/bill-estimate/selectors';
import { isOrganization as isOrganizationSelector } from 'bux/core/state/meta/selectors';
import styles from './AsideLinks.less';

export const StatelessLinks = ({ isAnnual, isOrganization }) => {
  const FaqLink = isOrganization
    ? ExternalLinkTo.FaqOrg
    : ExternalLinkTo.Faq;

  return (
    <div className={styles.aside}>
      <LogScope prefix="aside">
        <h2 className={styles.title}>
          <Trans id="billing.bill-estimate.aside-links.header">Billing information</Trans>
        </h2>
        <ul className={styles.sideLinks}>
          <li>
            <Analytics.UI 
              as="faqLink"
              attributes={{ linkContainer: 'aside' }}
            >
              <FaqLink>
                <Trans id="billing.bill-estimate.aside-links.faq">FAQ</Trans>
              </FaqLink>
            </Analytics.UI>
          </li>
          <li>
            <Analytics.UI 
              as={isAnnual ? 'upgradeTierLink' : 'contactUsAboutYourBillLink'}
              attributes={{ linkContainer: 'billEstimateSummary' }}
            >
              <ExternalLinkTo.PurchasingLicense>
                {isAnnual
                  ? <Trans id="billing.bill-estimate.aside-links.update">Upgrade Tier</Trans>
                  : <Trans id="billing.bill-estimate.aside-links.contact-us">Contact us about your bill</Trans>
                }
              </ExternalLinkTo.PurchasingLicense>
            </Analytics.UI>
          </li>
        </ul>
      </LogScope>
    </div>
  );
};

StatelessLinks.propTypes = {
  isAnnual: PropTypes.bool,
  isOrganization: PropTypes.bool,
};

const mapStateToProps = state => ({
  isAnnual: isNextBillingPeriodRenewalFrequencyAnnual(state),
  isOrganization: isOrganizationSelector(state),
});


const ConnectedLinks = connect(
  mapStateToProps,
  null,
)(StatelessLinks);

export default ConnectedLinks;
