import { connect } from 'react-redux';
import { 
  getBill, 
  getTotalCost, 
  getFullProductList,
  isNextBillInNewCurrency as isAfterTheCutOverDate, 
  upcomingCurrencyExtras, 
} from 'bux/core/state/bill-estimate/selectors';
import BillEstimateDetailComponent from './components/BillEstimateDetail';

const mapStateToProps = (state, props) => ({
  taxAmount: getBill(state).totalTaxAmount,
  totalCost: getTotalCost(state, props.excludeTax),
  endDate: getBill(state).currentBillingPeriod.endDate,
  currencyCode: getBill(state).currencyCode,
  productList: getFullProductList(state),
  upcomingCurrencyExtra: upcomingCurrencyExtras(state),
  isNextBillInNewCurrency: isAfterTheCutOverDate(state),
});

export const BillEstimateDetail = connect(mapStateToProps)(BillEstimateDetailComponent);
