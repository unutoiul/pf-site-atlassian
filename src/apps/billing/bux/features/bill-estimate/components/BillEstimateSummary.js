import { Trans } from '@lingui/react';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import cx from 'classnames';
import createPagination from 'bux/core/selectors/pagination';
import { getBillingPeriod } from 'bux/core/state/bill-estimate/selectors';
import Money from 'bux/components/Money';
import Link, { ExternalLink } from 'bux/components/Link';
import Island from 'bux/components/blocks/Island/Island';
import { FullDate } from 'bux/components/Date';
import billEstimate from 'bux/core/state/bill-estimate';
import { Analytics } from 'bux/components/Analytics';
import styles from './BillEstimateSummary.less';

const productListType = PropTypes.arrayOf(PropTypes.shape({
  key: PropTypes.string,
  name: PropTypes.string,
  amount: PropTypes.number
}));

export class BillEstimateSummary extends PureComponent {
  static propTypes = {
    currencyCode: PropTypes.string.isRequired,
    numberOfRemainingLines: PropTypes.number.isRequired,
    costOfRemainingLines: PropTypes.number.isRequired,
    shortProductList: productListType.isRequired,
    fullProductList: productListType.isRequired,
    totalCost: PropTypes.number.isRequired,
    totalTax: PropTypes.number.isRequired,
    billingPeriod: PropTypes.object.isRequired
  };

  state = { expanded: false };

  onExpandClick = () => {
    this.setState({
      expanded: true
    });
  };

  onCollapseClick = () => {
    this.setState({
      expanded: false
    });
  };

  render() {
    const {
      currencyCode,
      shortProductList,
      fullProductList,
      totalCost,
      totalTax,
      billingPeriod,
      numberOfRemainingLines,
      costOfRemainingLines
    } = this.props;
    const isShort = !this.state.expanded;
    const productList = isShort ? shortProductList : fullProductList;

    return (
      <Island className="bill-estimate-summary">
        <div className={cx('product-caption', styles.headerBlock, styles.singleLine)}>
          <span className={styles.title}>
            <Trans id="billing.estimate-summary.title">Bill estimate</Trans>
          </span>
          <span className={styles.priceDates}>
            <FullDate value={billingPeriod.startDate} />
            &nbsp;-&nbsp;
            <FullDate value={billingPeriod.endDate} />
          </span>
        </div>
        <div className={styles.container}>
          <div className={cx('product-list', styles.products)}>
            <ul>
              {
                productList.map(product => (
                  <li key={product.key} className={cx('product-line', styles.lineItem)}>
                    <span className="product-description">
                      {product.name}
                    </span>
                    <span className="product-price">
                      <Money amount={product.amount} productKey={product.key} currency={currencyCode} showFreeIfZero />
                    </span>
                  </li>
                ))
              }
              {
                isShort && numberOfRemainingLines > 0 &&
                <li className={cx('number-of-rest-summary-lines', styles.lineItem)}>
                  <Analytics.UI as="showMoreEstimateItemsLink">
                    <ExternalLink onClick={this.onExpandClick} className="expand-bill" logPrefix="expand">
                      <Trans id="billing.estimate-summary.lines-remaining">+ {numberOfRemainingLines} more</Trans>
                    </ExternalLink>
                  </Analytics.UI>

                  <span className="price-rest">
                    <Money amount={costOfRemainingLines} currency={currencyCode} />
                  </span>
                </li>
              }
              {
                !isShort && numberOfRemainingLines > 0 &&
                <li className={cx('number-of-rest-summary-lines', styles.lineItem)}>
                  <Analytics.UI as="showLessEstimateItemsLink">
                    <ExternalLink onClick={this.onCollapseClick} className="collapse-bill" logPrefix="collapse">
                      <Trans id="billing.estimate-summary.show-less">Show less</Trans>
                    </ExternalLink>
                  </Analytics.UI>
                </li>
              }

            </ul>
          </div>

          <hr />

          <div className={styles.tax}>
            <span><Trans id="billing.bill-estimate.summary.tax">Tax</Trans></span>
            <span className="total-tax">
              <Money amount={totalTax} currency={currencyCode} withCents />
            </span>
          </div>
          <div className={styles.total}>
            <span><Trans id="billing.bill-estimate.summary.total">Total</Trans></span>
            <span className={cx('total-cost', styles.totalPrice)}>
              <Money amount={totalCost} currency={currencyCode} includeLabel />
            </span>
          </div>
          <div className={cx('fine-print', styles.finePrint)}>
            <Analytics.UI
              as="viewFullEstimateLink"
              attributes={{
              linkContainer: 'billEstimateSummary'
            }}
            >
              <Link to="/estimate" logPrefix="viewFull">
                <Trans id="billing.bill-estimate.view-full">View full estimate</Trans>
              </Link>
            </Analytics.UI>
          </div>
        </div>
      </Island>
    );
  }
}

const { getPaginatedList } = createPagination(billEstimate.selectors.getProductList, 3);
const { getNumberOfRemainingLines } = createPagination(billEstimate.selectors.getBillEstimateLines, 3);

const getFullCost = list => list.reduce((sum, element) => sum + element.amount, 0);
const getCostDiff = (productList1, productList2) => getFullCost(productList1) - getFullCost(productList2);

const getProductInfomation = (state) => {
  const result = {
    fullProductList: billEstimate.selectors.getProductList(state),
    shortProductList: getPaginatedList(state),
  };
  result.costOfRemainingLines = getCostDiff(result.fullProductList, result.shortProductList);
  return result;
};

const mapStateToProps = state => ({
  currencyCode: billEstimate.selectors.getBill(state).currencyCode,
  numberOfRemainingLines: getNumberOfRemainingLines(state),
  totalTax: billEstimate.selectors.getBill(state).totalTaxAmount,
  totalCost: billEstimate.selectors.getBill(state).totalAmountWithTax,
  billingPeriod: getBillingPeriod(state),
  ...getProductInfomation(state)
});

export default connect(mapStateToProps)(BillEstimateSummary);
