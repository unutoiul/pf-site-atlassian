import configureMockStore from 'redux-mock-store';
import proxyquire from 'bux/helpers/proxyquire';
import React from 'react';
import sinon from 'sinon';
import { expect } from 'chai';
import { shallow } from 'enzyme';

describe('Connected BillEstimateSummary: ', () => {
  let BillEstimateSummary;
  let getBill;
  let getBillEstimateLines;
  let isNextBillingPeriodRenewalFrequencyAnnual;
  let getBillingPeriod;
  let getProductList;

  const renderContainer = () => {
    getBill.returns({
      totalAmountWithTax: 10,
      totalTaxAmount: 1,
      currencyCode: 'USD',
      nextBillingPeriod: {
        startDate: '20161010',
        endDate: '20161210'
      }
    });
    return shallow(<BillEstimateSummary store={configureMockStore()({ billEstimate: { data: {} } })} />);
  };

  beforeEach(() => {
    getBill = sinon.stub().returns({});
    getBillEstimateLines = sinon.stub().returns([]);
    getProductList = sinon.stub();
    isNextBillingPeriodRenewalFrequencyAnnual = sinon.stub().returns(false);
    getBillingPeriod = sinon.stub().returns({
      startDate: '20161010',
      endDate: '20161210'
    });

    const billEstimate = { selectors: { getProductList, getBill, getBillEstimateLines } };
    BillEstimateSummary = proxyquire.noCallThru().load('../BillEstimateSummary', {
      'bux/core/state/bill-estimate/selectors': { isNextBillingPeriodRenewalFrequencyAnnual, getBillingPeriod },
      'bux/core/state/bill-estimate': billEstimate
    }).default;
  });

  it('should map state to props', () => {
    getProductList.returns([
      { key: 'product1', amount: 1 },
      { key: 'product2', amount: 2 },
      { key: 'product3', amount: 3 },
      { key: 'product4', amount: 4 },
      { key: 'product5', amount: 5 }
    ]);
    getBillEstimateLines.returns([1, 2, 3, 4, 5]);

    const wrapper = renderContainer();
    expect(wrapper).to.have.props({
      totalCost: 10,
      totalTax: 1,
      currencyCode: 'USD',
      numberOfRemainingLines: 2,
      costOfRemainingLines: 9
    });


    expect(wrapper).to.have.prop('billingPeriod').to.deep.equal({
      startDate: '20161010',
      endDate: '20161210'
    });

    expect(wrapper).to.have.prop('shortProductList').to.deep.equal([
      { key: 'product1', amount: 1 },
      { key: 'product2', amount: 2 },
      { key: 'product3', amount: 3 }
    ]);

    expect(wrapper).to.have.prop('fullProductList').to.deep.equal([
      { key: 'product1', amount: 1 },
      { key: 'product2', amount: 2 },
      { key: 'product3', amount: 3 },
      { key: 'product4', amount: 4 },
      { key: 'product5', amount: 5 }
    ]);
  });
});
