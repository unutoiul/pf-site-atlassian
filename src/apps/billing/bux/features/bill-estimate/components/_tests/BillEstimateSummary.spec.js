import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Money from 'bux/components/Money';
import { ExternalLink } from 'bux/components/Link';
import { BillEstimateSummary } from '../BillEstimateSummary';

const DEFAULT_PROPS = {
  shortProductList: [],
  fullProductList: [],
  totalCost: 0,
  totalTax: 0,
  currencyCode: '',
  billingPeriod: {
    startDate: '',
    endDate: ''
  },
  costOfRemainingLines: 0,
  numberOfRemainingLines: 0
};
const renderWidget = options => <BillEstimateSummary {...DEFAULT_PROPS} {...options} />;
const createProduct = name => ({ key: name, amount: 0, name });

describe('BillEstimateSummary component: ', () => {
  it('should not render product list when empty', () => {
    const productList = shallow(renderWidget()).find('.product-list');
    expect(productList).to.be.present();
    expect(productList).to.have.text('');
  });

  it('should render multiple line items when product list has more than one item', () => {
    const productList = shallow(renderWidget({
      shortProductList: [
        createProduct('item1'),
        createProduct('item2'),
        createProduct('item3'),
        createProduct('item4'),
        createProduct('item5')
      ]
    })).find('.product-list ul');

    expect(productList).to.be.present();
    expect(productList.children()).to.have.length(5);
    expect(productList.childAt(0)).to.contain.text('item1');
    expect(productList.childAt(1)).to.contain.text('item2');
    expect(productList.childAt(2)).to.contain.text('item3');
    expect(productList.childAt(3)).to.contain.text('item4');
    expect(productList.childAt(4)).to.contain.text('item5');
  });

  it('should render number of rest summary lines if it exists', () => {
    const wrapper = shallow(renderWidget({
      numberOfRemainingLines: 10
    }));

    expect(wrapper).to.be.present();
    const numberOfRestSummaryLinesSpan = wrapper.find('.number-of-rest-summary-lines');
    expect(numberOfRestSummaryLinesSpan).to.be.present();
    expect(numberOfRestSummaryLinesSpan.find(ExternalLink).text()).to.include('10');
  });

  it('should not render number of rest summary lines if no lines left', () => {
    const wrapper = shallow(renderWidget({
      numberOfRemainingLines: 0
    }));

    expect(wrapper).to.be.present();
    const numberOfRestSummaryLinesSpan = wrapper.find('.number-of-rest-summary-lines');
    expect(numberOfRestSummaryLinesSpan).not.to.be.present();
  });

  it('should render number of rest summary lines if expanded', () => {
    const wrapper = shallow(renderWidget({
      numberOfRemainingLines: 10,
      shortProductList: [
        createProduct('item1')
      ],
      fullProductList: [
        createProduct('item1'),
        createProduct('item2'),
      ],
    }));
    let productList = wrapper.find('.product-list ul .product-line');

    expect(wrapper).to.be.present();
    expect(productList).to.have.length(1);
    expect(wrapper.find('.number-of-rest-summary-lines')).to.be.present();

    wrapper.find('.expand-bill').simulate('click');

    productList = wrapper.find('.product-list ul .product-line');
    expect(productList).to.have.length(2);
    expect(wrapper.find('.expand-bill')).not.to.be.present();

    wrapper.find('.collapse-bill').simulate('click');
    expect(wrapper.find('.expand-bill')).to.be.present();
    productList = wrapper.find('.product-list ul .product-line');
    expect(productList).to.have.length(1);
  });

  it('should render total cost', () => {
    const total = shallow(renderWidget({
      totalCost: 9000
    })).find('.total-cost');

    expect(total).to.have.length(1);
    expect(total).to.contain(<Money amount={9000} currency="" includeLabel />);
  });

  it('should render total tax', () => {
    const tax = shallow(renderWidget({
      totalTax: 4321
    })).find('.total-tax');

    expect(tax).to.have.length(1);
    expect(tax).to.contain(<Money amount={4321} currency="" withCents />);
  });
});
