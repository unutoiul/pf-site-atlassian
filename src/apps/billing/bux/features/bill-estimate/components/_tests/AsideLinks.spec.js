import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow, mount } from 'enzyme';
import { ExternalLinkTo } from 'bux/components/Link';
import proxyquire from 'bux/helpers/proxyquire';
import { Provider } from 'bux/helpers/redux';

import { StatelessLinks } from '../AsideLinks';

describe('AsideLinks', () => {
  describe('Stateless', () => {
    it('should contain two links', () => {
      expect(shallow(<StatelessLinks />).find(ExternalLinkTo.PurchasingLicense)).to.have.length(1);
      expect(shallow(<StatelessLinks />).find(ExternalLinkTo.Faq)).to.have.length(1);
    });

    it('first link is FAQ', () => {
      expect(mount(<StatelessLinks />).find(ExternalLinkTo.Faq)).to.contain.text('FAQ');
    });

    it('first FAQ link points to cloud', () => {
      expect(mount(<StatelessLinks />).find(ExternalLinkTo.PurchasingLicense)).to.be.present();
    });

    it('first FAQ link points to AAH', () => {
      expect(mount(<StatelessLinks isOrganization />).find(ExternalLinkTo.FaqOrg)).to.be.present();
    });

    it('second link is contact for monthly customers', () => {
      expect(mount(<StatelessLinks />)
        .find(ExternalLinkTo.PurchasingLicense)).to.contain.text('Contact us about your bill');
    });

    it('second link is Upgrade for annual customers', () => {
      expect(mount(<StatelessLinks isAnnual />)
        .find(ExternalLinkTo.PurchasingLicense)).to.contain.text('Upgrade Tier');
    });
  });


  it('connect component to the store', () => {
    const isNextBillingPeriodRenewalFrequencyAnnual = sinon.stub().returns(true);

    const { default: Links, StatelessLinks: Stateless } = proxyquire.load('../AsideLinks', {
      'bux/core/state/bill-estimate/selectors': { isNextBillingPeriodRenewalFrequencyAnnual }
    });
    const wrapper = mount(<Provider><Links /></Provider>);
    expect(isNextBillingPeriodRenewalFrequencyAnnual).to.be.called();
    expect(wrapper.find(Stateless)).to.have.prop('isAnnual', true);
  });
});
