import React from 'react';
import { expect } from 'chai';
import { shallow, render } from 'enzyme';
import { Provider } from 'bux/helpers/redux';
import Link from 'bux/components/Link';
import Money from 'bux/components/Money';
import createStore from 'bux/core/createStore';
import { BillEstimateWidget } from '../BillEstimateWidget';

const DEFAULT_PROPS = {
  totalCost: 0,
  endDate: '',
  currencyCode: ''
};
const renderWidget = options =>
  (<Provider store={createStore()}>
    <BillEstimateWidget {...DEFAULT_PROPS} {...options} />
   </Provider>);

describe('BillEstimateWidget component: ', () => {
  it('should render total cost with currency', () => {
    const cost = shallow(<BillEstimateWidget {...DEFAULT_PROPS} totalCost={9000} currencyCode="usd" />)
      .find('.price');

    expect(cost).to.have.length(1);
    expect(cost).to.contain(<Money amount={9000} currency="usd" includeLabel />);
  });

  it('should render the date', () => {
    const endDate = render(renderWidget({
      endDate: '20161110'
    })).find('.date');

    expect(endDate).to.have.length(1);
    expect(endDate).to.have.text('Next Charge: Nov 10, 2016');
  });

  it('should render view details link', () => {
    const wrapper = shallow(<BillEstimateWidget {...DEFAULT_PROPS} />);

    expect(wrapper).to.be.present();
    expect(wrapper.find(Link)).to.be.present();
  });
});
