import configureMockStore from 'redux-mock-store';
import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import { CURRENT } from 'bux/core/state/bill-estimate/constants';
import { INITIAL_BILL_STATE } from 'bux/core/state/bill-estimate/reducer';

// eslint-disable-next-line import/no-named-as-default
import BillEstimateWidget from '../BillEstimateWidget';

describe('BillEstimateWidgetContainer: ', () => {
  const mockStoreCreator = (data = {}, error = false) => configureMockStore()({
    billEstimate: { [CURRENT]: { ...INITIAL_BILL_STATE, data: { ...INITIAL_BILL_STATE.data, ...data }, error } }
  });

  it('should map state to props', () => {
    const mockedStore = mockStoreCreator({
      totalAmountWithTax: 99,
      currentBillingPeriod: { endDate: '20161020' },
      currencyCode: 'ATL'
    });

    const wrapper = shallow(<BillEstimateWidget store={mockedStore} />);
    const props = wrapper.props();

    expect(props.endDate).to.equal('20161020');
    expect(props.totalCost).to.equal(99);
    expect(props.currencyCode).to.equal('ATL');
  });

  it('should map error state to props', () => {
    const mockedStore = mockStoreCreator(undefined, { status: 500 });

    const wrapper = shallow(<BillEstimateWidget store={mockedStore} />);
    const props = wrapper.props();

    expect(props.metadata.error.status).to.equal(500);
  });
});
