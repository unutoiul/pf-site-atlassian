function ProductDetailBuilder() {
  const productDetail = {
    usage: [],
    entitlement: {
      status: 'ACTIVE'
    }
  };
  return {
    withKey(key) {
      productDetail.key = key;
      return this;
    },
    withName(name) {
      productDetail.name = name;
      return this;
    },
    withAmount(amount) {
      productDetail.amount = amount;
      return this;
    },
    withUsage(unit, unitCode) {
      productDetail.usage.push({ units: unit, unitCode });
      return this;
    },
    build() {
      return { ...productDetail };
    },
    withStatus(status) {
      productDetail.entitlement.status = status;
      return this;
    },
    withTrialEndDate(date) {
      productDetail.entitlement.trialEndDate = date;
      return this;
    }
  };
}

function BillEstimateDetailBuilder() {
  const detail = {
    productList: []
  };
  return {
    withProduct(product) {
      detail.productList.push(product);
      return this;
    },
    withTrialProduct() {
      const product = new ProductDetailBuilder()
        .withName('Jira activating')
        .withKey('jira-free-plan')
        .withAmount(0)
        .withTrialEndDate(new Date(2150, 1, 1));
      detail.productList.push(product.build());
      return this;
    },
    withInactiveProduct() {
      const product = new ProductDetailBuilder()
        .withName('Jira activating')
        .withKey('jira-free-plan')
        .withAmount(0)
        .withStatus('INACTIVE');
      detail.productList.push(product.build());
      return this;
    },
    withFreeProduct() {
      const product = new ProductDetailBuilder()
        .withName('Jira free plan')
        .withKey('jira-free-plan')
        .withAmount(0)
        .withUsage(10, 'users');
      detail.productList.push(product.build());
      return this;
    },
    withCostlessProduct() {
      const product = new ProductDetailBuilder()
        .withName('Jira free plan')
        .withKey('jira-free-plan')
        .withAmount(null)
        .withUsage(10, 'users');
      detail.productList.push(product.build());
      return this;
    },
    withMultipleUsageProduct() {
      const product = new ProductDetailBuilder()
        .withName('Bitbucket')
        .withKey('bitbucket')
        .withAmount(10)
        .withUsage(10, 'users')
        .withUsage(200, 'build minutes');
      detail.productList.push(product.build());
      return this;
    },
    withManyProduct(count) {
      for (let i = 1; i <= count; i += 1) {
        const product = new ProductDetailBuilder()
          .withName(`Product ${i}`)
          .withKey(i)
          .withAmount(10)
          .withUsage(10, 'users');
        detail.productList.push(product.build());
      }
      return this;
    },
    withCurrency(code) {
      detail.currencyCode = code;
      return this;
    },
    withTotalTax(tax) {
      detail.totalTax = tax;
      return this;
    },
    withTotalCost(price) {
      detail.totalCost = price;
      return this;
    },
    withExcludeTax() {
      detail.excludeTax = true;
      return this;
    },
    withEndDate(date) {
      detail.endDate = date;
      return this;
    },
    withShowTotalDueFormat() {
      detail.showTotalDueFormat = true;
      return this;
    },
    withStyle(style) {
      detail.style = style;
      return this;
    },
    build() {
      return { ...detail };
    }
  };
}

export default BillEstimateDetailBuilder;
