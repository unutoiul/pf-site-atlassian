import configureMockStore from 'redux-mock-store';
import proxyquire from 'bux/helpers/proxyquire';
import React from 'react';
import sinon from 'sinon';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import { CURRENT } from 'bux/core/state/bill-estimate/constants';
import { INITIAL_BILL_STATE } from 'bux/core/state/bill-estimate/reducer';
import billEstimate from 'bux/core/state/bill-estimate';

describe('Connected BillEstimateDetail: ', () => {
  let BillEstimateDetail;
  let getFullProductList;

  const mockStoreCreator = (override = {}) => configureMockStore()({
    billEstimate: { [CURRENT]: { ...INITIAL_BILL_STATE, data: { ...INITIAL_BILL_STATE.data, ...override } } }
  });

  beforeEach(() => {
    getFullProductList = sinon.stub();
    BillEstimateDetail = proxyquire.noCallThru().load('../BillEstimateDetail', {
      'bux/core/state/bill-estimate/selectors': {
        getBill: billEstimate.selectors.getBill,
        getTotalCost: billEstimate.selectors.getTotalCost,
        upcomingCurrencyExtras: billEstimate.selectors.upcomingCurrencyExtras,
        isNextBillInNewCurrency: billEstimate.selectors.isNextBillInNewCurrency,
        getFullProductList
      }
    }).BillEstimateDetail;
  });

  it('should map state to props', () => {
    getFullProductList.returns([{ description: 'some-product' }]);
    const mockedStore = mockStoreCreator({
      totalAmountWithTax: 123456789,
      totalAmountBeforeTax: 123456788,
      totalTaxAmount: 1337,
      currentBillingPeriod: {
        endDate: '20180102'
      }
    });
    const wrapper = shallow(<BillEstimateDetail title="someTitle" store={mockedStore} />);
    const props = wrapper.props();
    expect(props.totalCost).to.equal(123456789);
    expect(props.endDate).to.equal('20180102');
    expect(props.title).to.equal('someTitle');
    expect(props.productList).to.deep.equal([{ description: 'some-product' }]);
  });

  it('should use totalAmountBeforeTax when tax are excluded', () => {
    const mockedStore = mockStoreCreator({
      totalAmountBeforeTax: 123456788
    });

    const wrapper = shallow(<BillEstimateDetail excludeTax title="someTitle" store={mockedStore} />);
    expect(wrapper.props().totalCost).to.equal(123456788);
  });
});
