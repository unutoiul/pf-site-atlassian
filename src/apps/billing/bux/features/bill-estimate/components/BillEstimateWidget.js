import { I18n, Trans } from '@lingui/react';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import Link from 'bux/components/Link';
import cx from 'classnames';
import Money from 'bux/components/Money';
import Date from 'bux/components/Date';
import Widget from 'bux/components/Widget';
import ExtendedPropTypes from 'bux/core/extended-proptypes';
import billEstimate from 'bux/core/state/bill-estimate';
import Scope from 'bux/components/log/Scope';
import { Analytics } from 'bux/components/Analytics';
import styles from './BillEstimateWidget.less';

export const BillEstimateWidget = ({
  className, currencyCode, totalCost, endDate, isAnnual, metadata
}) => {
  const periodEndDate = <Date value={endDate} />;
  return (
    <I18n>{({ i18n }) => (
      <Widget
        name={i18n.t('billing.widget.estimate.name')`Bill estimate`}
        errorMessage={{ name: i18n.t('billing.widget.estimate.name.in-error-message')`bill estimate` }}
        className={cx(className, 'bill-estimate-widget')}
        metadata={metadata}
      >
        <Scope prefix="billEstimate">
          <div>
            <div>
            <span className={cx('price', styles.price)}>
              <Money amount={totalCost} currency={currencyCode} includeLabel />
            </span>
              &nbsp;
              <Trans id="billing.widget.estimate.tax-inclusive">
                (tax inclusive)
              </Trans>
            </div>
            {isAnnual
              ?
              <div>
                <Trans id="billing.widget.estimate.next-charge-info.annual">
                  We will send you a renewal quote 60 days prior to the renewal date ({periodEndDate}).
                </Trans>
              </div>
              :
              <div className="date">
                <Trans id="billing.widget.estimate.next-charge-info.monthly">
                  Next Charge: {periodEndDate}
                </Trans>
              </div>
            }
            <Analytics.UI as="viewFullEstimateLink" attributes={{ linkContainer: 'billEstimateSummary' }}>
              <Link className={cx(styles.link, 'link')} to="/estimate">
                <Trans id="billing.widget.estimate.view-full">
                  View full estimate
                </Trans>
              </Link>
            </Analytics.UI>
          </div>
        </Scope>
      </Widget>
    )}</I18n>
  );
};

BillEstimateWidget.propTypes = {
  className: PropTypes.string,
  dispatch: PropTypes.func,
  currencyCode: PropTypes.string,
  totalCost: PropTypes.number,
  endDate: PropTypes.string,
  isAnnual: PropTypes.bool,
  metadata: ExtendedPropTypes.metadata
};

const mapStateToProps = state => ({
  isAnnual: billEstimate.selectors.isNextBillingPeriodRenewalFrequencyAnnual(state),
  metadata: billEstimate.selectors.getBillEstimateMetadata(state),
  currencyCode: billEstimate.selectors.getBill(state).currencyCode,
  totalCost: billEstimate.selectors.getBill(state).totalAmountWithTax,
  endDate: billEstimate.selectors.getBill(state).currentBillingPeriod.endDate,
});

export default connect(mapStateToProps)(BillEstimateWidget);
