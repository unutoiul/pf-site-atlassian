import React from 'react';
import { storiesOf } from '@storybook/react';
import { inContentChrome, inStoryKnob } from 'bux/common/helpers/storybook';
import { StaticRouter } from 'react-router-dom';

import { BillEstimateWidget } from './BillEstimateWidget';

const defaultProps = {
  dispatch: () => {},
  currencyCode: 'usd',
  totalCost: 100,
  endDate: '2018-08-01',
  isAnnual: false,
  metadata: {
    display: true,
    error: null,
    loading: false
  }
};

const Component = props => (
  <StaticRouter context={{}}>
    <BillEstimateWidget {...props} />
  </StaticRouter>
);
Component.propTypes = BillEstimateWidget.propTypes;

storiesOf('BUX|Features/bill-estimate/BillEstimateWidget', module)
  .add('Loading', () => inContentChrome(inStoryKnob(Component, { ...defaultProps, metadata: { loading: true } })))
  .add('Error', () => inContentChrome(inStoryKnob(Component, { ...defaultProps, metadata: { error: true } })))
  .add('Monthly', () => inContentChrome(inStoryKnob(Component, defaultProps)))
  .add('Monthly in JPY', () =>
    inContentChrome(inStoryKnob(Component, { ...defaultProps, totalCost: 10000, currencyCode: 'jpy' })))
  .add('Annual', () => inContentChrome(inStoryKnob(Component, { ...defaultProps, isAnnual: true })));
