import React from 'react';
import { expect } from 'chai';
import { shallow, mount, render } from 'enzyme';
import Lozenge from '@atlaskit/lozenge';
import BillEstimateDetail from '../BillEstimateDetail';
import BillEstimateDetailBuilder from '../../_tests/builder/BillEstimateDetailBuilder';

const DEFAULT_PROPS = {
  endDate: '',
  productList: [],
  totalCost: 0,
  taxAmount: 0,
  currencyCode: 'usd',
  title: 'Bill estimate',
  style: 'full',
  metadata: {
    loading: false,
    empty: false,
    error: null
  }
};

const mountDetail = options => <BillEstimateDetail {...DEFAULT_PROPS} {...options} />;

describe('BillEstimateDetail component: ', () => {
  it('should not mount product list when empty', () => {
    const wrapper = shallow(mountDetail());
    expect(wrapper.find('.product-list')).to.be.present();
    expect(wrapper.find('.product-line')).to.have.length(0);
  });

  it('should mount a single item with description, price and tier when one product', () => {
    const detail = new BillEstimateDetailBuilder().withManyProduct(1).build();

    const wrapper = mount(mountDetail(detail));
    const productLines = wrapper.find('.product-line');

    expect(productLines).to.have.length(1);
    expect(productLines).to.contain.text('Product 1');
    expect(productLines).to.contain.text('$10');
    expect(productLines).to.contain.text('10 users');
  });

  it('should mount a multiple usage when present', () => {
    const detail = new BillEstimateDetailBuilder().withMultipleUsageProduct().build();

    const wrapper = mount(mountDetail(detail));
    const productLines = wrapper.find('.product-line');

    expect(productLines).to.have.length(1);
    expect(productLines).to.contain.text('Bitbucket');
    expect(productLines).to.contain.text('$10');
    expect(productLines).to.contain.text('10 users');
    expect(productLines.find('.product-usage').at(0)).to.contain.text('10 users');
    expect(productLines.find('.product-usage').at(1)).to.contain.text('200 build minutes');
  });

  it('should mount a compact multiple usage when present', () => {
    const detail = new BillEstimateDetailBuilder().withMultipleUsageProduct().withStyle('compact').build();

    const wrapper = mount(mountDetail(detail));
    const productLines = wrapper.find('.product-line');

    expect(productLines).to.have.length(1);
    expect(productLines.find('.product-usage')).to.not.be.present();
    expect(productLines.find('.product-usage-list')).to.contain.text('10 users, 200 build minutes');
  });

  it('should mount a multiple items when multiple products', () => {
    const detail = new BillEstimateDetailBuilder().withManyProduct(3).build();

    const productLines = render(mountDetail(detail)).find('.product-line');
    expect(productLines).to.have.length(3);
    expect(productLines).to.contain.text('Product 1');
    expect(productLines).to.contain.text('Product 2');
    expect(productLines).to.contain.text('Product 3');
  });

  it('should mount free when product is free', () => {
    const detail = new BillEstimateDetailBuilder().withFreeProduct().build();

    const wrapper = mount(mountDetail(detail));
    const productPrice = wrapper.find('.product-line .product-price');

    expect(productPrice).to.have.length(1);
    expect(productPrice).to.have.text('Free');
  });

  it('should mount inactive when product is inactive', () => {
    const detail = new BillEstimateDetailBuilder().withInactiveProduct().build();

    const wrapper = mount(mountDetail(detail));
    const productPrice = wrapper.find('.product-line .product-price');

    expect(productPrice).to.have.length(1);
    expect(productPrice).to.have.text('Inactive');
  });

  it('should mount unavailable when no cost provided', () => {
    const detail = new BillEstimateDetailBuilder().withCostlessProduct().build();

    const wrapper = mount(mountDetail(detail));
    const productPrice = wrapper.find('.product-line .product-price');

    expect(productPrice).to.have.length(1);
    expect(productPrice).to.have.text('Unavailable');
  });


  it('should mount trial when product is trial', () => {
    const detail = new BillEstimateDetailBuilder().withTrialProduct().build();

    const wrapper = mount(mountDetail(detail));
    const trialLozenge = wrapper.find(Lozenge);

    expect(trialLozenge).to.have.length(1);
    expect(trialLozenge).to.have.text('Free trial until Feb 1, 2150');
  });

  it('should not mount trial when product is trial but is compact', () => {
    const detail = new BillEstimateDetailBuilder().withTrialProduct().withStyle('compact').build();

    const wrapper = mount(mountDetail(detail));
    const trialLozenge = wrapper.find(Lozenge);

    expect(trialLozenge).to.not.be.present();
  });

  it('should mount end date', () => {
    const detail = new BillEstimateDetailBuilder().withShowTotalDueFormat().withEndDate('20161010').build();

    const endDate = mount(mountDetail(detail)).find('.total-date');

    expect(endDate).to.have.text('Total due Oct 10, 2016');
  });

  it('should mount without total due date', () => {
    const detail = new BillEstimateDetailBuilder().withEndDate('20161010').build();

    const endDate = mount(mountDetail(detail)).find('.total-date');

    expect(endDate).to.have.text('Total');
  });

  it('should mount total cost', () => {
    const detail = new BillEstimateDetailBuilder().withTotalCost(100).build();

    const cost = mount(mountDetail(detail)).find('.total-cost');

    expect(cost.text()).to.be.eq('USD100.00');
  });

  it('should display tax by default', () => {
    const detail = new BillEstimateDetailBuilder().build();

    const billEstimate = mount(mountDetail(detail));

    expect(billEstimate.find('.tax-total')).to.be.present();
    expect(billEstimate.find('Footnote')).to.be.not.present();
    expect(billEstimate.find('FootnoteIndicator')).to.have.prop('condition', false);
  });

  it('should not display tax when excluded', () => {
    const detail = new BillEstimateDetailBuilder().withExcludeTax().build();

    const billEstimate = mount(mountDetail(detail));

    expect(billEstimate.find('.tax-total')).to.not.be.present();
    expect(billEstimate.find('Footnote')).to.be.present();
    expect(billEstimate.find('FootnoteIndicator')).to.have.prop('condition', true);
  });
});
