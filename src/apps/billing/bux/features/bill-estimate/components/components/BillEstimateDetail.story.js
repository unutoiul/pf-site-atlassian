import { storiesOf } from '@storybook/react';
import { inContentChrome, inStoryKnob } from 'bux/common/helpers/storybook';

import BillEstimateDetail from './BillEstimateDetail';

const props = {
  taxAmount: 10,
  totalCost: 6962,
  endDate: '20180228',
  currencyCode: 'usd',
  productList: [
    {
      key: 'jira-servicedesk.ondemand',
      name: 'Jira Service Desk',
      usage: [
        {
          description: 'JIRA Service Desk Cloud Agents',
          taxRate: 0,
          amountBeforeTax: 5420,
          units: 271,
          unitCode: 'agents',
          unlimited: false,
          amountWithTax: 5962,
          taxAmount: 542
        },
        {
          description: 'JIRA Service Desk Cloud Users',
          taxRate: 0,
          amountBeforeTax: 5420,
          units: 34,
          unitCode: 'users',
          unlimited: false,
          amountWithTax: 5962,
          taxAmount: 542
        }
      ],
      productName: 'JIRA Service Desk (Cloud)',
      entitlementId: '8beba60c-d25f-43dc-8152-fb88a2a998bb',
      entitlement: { status: 'ACTIVE' },
      amount: 5962
    },
    {
      key: 'confluence.ondemand',
      name: 'Confluence',
      productName: 'Confluence (Cloud)',
      usage: [
        {
          amountBeforeTax: 1000,
          taxRate: 10,
          description: 'Confluence Cloud',
          units: 820,
          unitCode: 'users',
          unlimited: false,
          taxAmount: 100,
          amountWithTax: 1100
        }
      ],
      selectedEdition: 'Confluence Monthly',
      entitlementId: 'a2177415-1a6d-4d91-a942-41bd03d1c4ab',
      entitlement: { status: 'ACTIVE', trialEndDate: '20241212' },
      amount: 1000
    },
  ],
  showTotalDueFormat: false
};

storiesOf('BUX|Features/bill-estimate/BillEstimateDetail', module)
  .add('Compact', () => inContentChrome(inStoryKnob(BillEstimateDetail, { ...props, style: 'compact' })))
  .add('Full', () => inContentChrome(inStoryKnob(BillEstimateDetail, { ...props, showTotalDueFormat: true })));
