import { Trans } from '@lingui/react';
import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';
import Money from 'bux/components/Money';
import Date from 'bux/components/Date';
import styles from './BillEstimateFooter.less';

const BillEstimateFooter = ({
  totalCost, currencyCode, isAnnual, nextRenewalDate
}) => {
  const periodEndDate = <Date value={nextRenewalDate} />;

  return (
    <div>
      { isAnnual ?
        <div className={cx(styles.finePrint, 'auto-renewal-label')}>
          <Trans id="billing.bill-estimate.annual-renewal-info">
           We will send you a renewal quote 60 days prior to the renewal date {periodEndDate}.
          </Trans>
        </div> :
        <div className={cx(styles.finePrint, 'auto-charged-label')}>
          <Trans id="billing.bill-estimate.charge-info">
          Based on your current configuration, you are being automatically
          charged <Money amount={totalCost} currency={currencyCode} />.
          </Trans>
        </div>
      }
   </div>
  );
};

BillEstimateFooter.propTypes = {
  totalCost: PropTypes.number,
  currencyCode: PropTypes.string.isRequired,
  isAnnual: PropTypes.bool,
  nextRenewalDate: PropTypes.string
};

export default BillEstimateFooter;
