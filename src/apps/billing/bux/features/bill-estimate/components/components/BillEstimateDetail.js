import { Trans } from '@lingui/react';
import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';
import Money from 'bux/components/Money';
import Date from 'bux/components/Date';
import FootnoteIndicator from 'bux/components/Footnote/FootnoteIndicator';
import Footnote from 'bux/components/Footnote/Footnote';
import BillEstimateDetailLine from './BillEstimateDetailLine';
import styles from './BillEstimateDetail.less';

const BillEstimateDetail = ({
  productList,
  endDate,
  taxAmount,
  totalCost,
  currencyCode,
  showTotalDueFormat,
  style,
  excludeTax
}) => {
  const periodEndDate = <Date value={endDate} />;

  return (
    <div className={styles.container}>
      <ul className={cx('product-list', styles.productList, styles[style])}>
        {
          productList.map(product => (
            <BillEstimateDetailLine
              key={product.key}
              currencyCode={currencyCode}
              style={style}
              {...product}
            />
          ))
        }
      </ul>
      <ul className={cx('summary', styles.summary, styles[style])}>
        { !excludeTax &&
        <li className={cx('product-line-tax-total', styles.tax, styles[style])}>
          <div className={cx(styles.taxTotalDescription, styles[style])}>
            <Trans id="billing.bill-estimate.details.tax">Tax</Trans>
          </div>
          <div className={cx('tax-total', styles.taxTotalPrice, styles[style])}>
            <Money amount={taxAmount} currency={currencyCode} includeLabel />
          </div>
        </li>
        }

        <li className={cx('product-line-total', styles.total, styles[style])}>
          <div className={cx('total-date', styles.totalDescription, styles[style])}>
            { showTotalDueFormat ?
              <span>
              <Trans id="billing.bill-estimate.details.total-due">Total due {periodEndDate}</Trans>
            </span>
              :
              <span>
              <Trans id="billing.bill-estimate.details.total">Total</Trans>
            </span>
            }
          </div>
          <div className={cx('total-cost', styles.totalPrice, styles[style])}>
            <FootnoteIndicator condition={excludeTax}>
              <Money amount={totalCost} currency={currencyCode} includeLabel />
            </FootnoteIndicator>
          </div>
        </li>
      </ul>
      {excludeTax &&
      <Footnote>
        <Trans id="billing.bill-estimate.details.subject-to-taxes">Subject to all applicable taxes</Trans>
      </Footnote>
      }
    </div>
  );
};

BillEstimateDetail.propTypes = {
  endDate: PropTypes.string,
  productList: PropTypes.arrayOf(PropTypes.object),
  taxAmount: PropTypes.number,
  totalCost: PropTypes.number,
  currencyCode: PropTypes.string.isRequired,
  showTotalDueFormat: PropTypes.bool,
  style: PropTypes.oneOf(['compact', 'full']),
  excludeTax: PropTypes.bool
};

BillEstimateDetail.defaultProps = {
  style: 'full',
  excludeTax: false
};

export default BillEstimateDetail;
