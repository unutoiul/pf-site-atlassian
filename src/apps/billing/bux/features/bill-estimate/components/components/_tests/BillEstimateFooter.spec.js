import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import BillEstimateFooter from 'bux/features/bill-estimate/components/components/BillEstimateFooter';
import Money from 'bux/components/Money';

const DEFAULT_PROPS = {
  isAnnual: true,
  currencyCode: 'usd',
  totalCost: 10,
  nextRenewalDate: '2018-01-01'
};

const mountComponent = options => <BillEstimateFooter {...DEFAULT_PROPS} {...options} />;

describe('BillEstimateFooter component: ', () => {
  it('should show auto charged label if not annual', () => {
    const wrapper = shallow(mountComponent({ isAnnual: false }));
    expect(wrapper.find('.auto-charged-label')).to.be.present();
    expect(wrapper.find('.auto-renewal-label')).to.not.be.present();
    expect(wrapper.find('.auto-charged-label').find(Money)).to.be.present();
  });

  it('should show auto renewal message if annual', () => {
    const wrapper = shallow(mountComponent());
    expect(wrapper.find('.auto-renewal-label')).to.be.present();
    expect(wrapper.find('.auto-charged-label')).to.not.be.present();
  });
});
