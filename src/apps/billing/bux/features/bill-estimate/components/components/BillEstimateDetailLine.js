import { Trans } from '@lingui/react';
import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';
import Lozenge from '@atlaskit/lozenge';
import Money from 'bux/components/Money';
import Usage from 'bux/components/Usage';
import { isInactive, isTrial } from 'bux/core/selectors/common/entitlements';
import Date from 'bux/components/Date';
import styles from './BillEstimateDetail.less';

const wrapWithKeyElement = (child, key) => (<React.Fragment key={key}>{child}</React.Fragment>);

const UsageList = ({ usage, style }) => {
  const usages = usage.map(({ units, unitCode, unlimited }) => (
    <Usage unitsLimit={units} unitCode={unitCode} unlimited={unlimited} />
  ));

  return (
    <div className={cx(styles.productUsage, 'product-usage-list', styles[style])}>
      {style === 'full'
        ? usages.map((child, index) => (<div key={index} className="product-usage">{child}</div>))
        : usages.map(wrapWithKeyElement).reduce((prev, curr) => [prev, ', ', curr])
      }
    </div>
  );
};

UsageList.propTypes = {
  usage: PropTypes.array,
  style: PropTypes.string
};

const displayCost = (amount, currencyCode, entitlement) => {
  if (isInactive(entitlement)) {
    return (
      <span>
        <Trans id="billing.bill-estimate.line.inactive">Inactive</Trans>
      </span>
    );
  }

  if (amount === null) {
    return (
      <span>
        <Trans id="billing.bill-estimate.line.unavailable">Unavailable</Trans>
      </span>
    );
  }

  return <Money amount={amount} currency={currencyCode} productKey={entitlement.productKey} showFreeIfZero />;
};

const BillEstimateDetailLine = ({
  name, amount, currencyCode, usage, entitlement = {}, style
}) => {
  const trialEndDate = <Date value={entitlement.trialEndDate} />;

  return (
    <li className={cx('product-line', styles.productLine)}>
      <div className={styles.description}>
        <span className={cx('product-description', styles.productName, styles[style])}>{name}</span>
        {
          isTrial(entitlement) && style === 'full' &&
          <div className={cx(styles.lozenge, 'trialLozenge')}>
            <Lozenge appearance="success" maxWidth={230}>
              <Trans id="billing.bill-estimate.line.trial-until">
                Free trial until {trialEndDate}
              </Trans>
            </Lozenge>
          </div>
        }
        {usage && usage.length > 0 && <UsageList usage={usage} style={style} />}
      </div>
      <div className={cx('product-price', styles.productPrice, styles[style])}>
        { displayCost(amount, currencyCode, entitlement)}
      </div>
    </li>
  );
};

BillEstimateDetailLine.propTypes = {
  name: PropTypes.string.isRequired,
  amount: PropTypes.number,
  currencyCode: PropTypes.string,
  usage: PropTypes.array,
  entitlement: PropTypes.object,
  style: PropTypes.oneOf(['compact', 'full']).isRequired
};

export default BillEstimateDetailLine;
