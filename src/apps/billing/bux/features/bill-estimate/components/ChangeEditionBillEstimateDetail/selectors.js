import { billFactory, getChangeEditionBill } from 'bux/core/state/bill-estimate/selectors';

export const {
  getBill,
  getFullProductList,
  getTotalCost
} = billFactory(getChangeEditionBill);
