import { connect } from 'react-redux';
import { getBill, getTotalCost, getFullProductList } from './selectors';
import BillEstimateDetailComponent from '../components/BillEstimateDetail';

const mapStateToProps = (state, { excludeTax }) => ({
  taxAmount: getBill(state).totalTaxAmount,
  totalCost: getTotalCost(state, excludeTax),
  endDate: getBill(state).currentBillingPeriod.endDate,
  currencyCode: getBill(state).currencyCode,
  productList: getFullProductList(state)
});

export const ChangeEditionBillEstimateDetail = connect(mapStateToProps)(BillEstimateDetailComponent);
