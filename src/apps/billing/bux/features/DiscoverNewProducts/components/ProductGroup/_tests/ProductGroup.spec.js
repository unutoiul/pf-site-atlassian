import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import { TRY, USER } from 'bux/core/state/recommended-products/constants';
import getAnalytic from 'bux/components/log/helpers/getAnalytic';
import ProductGroup from '../ProductGroup';
import ProductList from '../../ProductList';
import ProductButton from '../../ProductCard/components/ProductButton';

const BASE_PROPS = {
  productKey: 'confluence.ondemand',
  title: 'test title',
  isExpired: false,
  products: [{
    productKey: 'confluence.ondemand',
    name: 'Confluence',
    tag: 'chat',
    description: 'Loren ipsum dolor sit amet',
    price: { value: 0, currencyCode: 'usd' },
    action: {
      type: TRY,
      onClick: () => {}
    },
    unitLabel: USER,
    isAnnual: false
  }]
};
const Component = props => (<ProductGroup {...BASE_PROPS} {...props} />);

describe('ProductGroup', () => {
  it('should render a list of products', () => {
    const context = getAnalytic().context;
    const wrapper = mount(<Component />, context);
    expect(wrapper).to.be.present();
    expect(wrapper.find('h3')).to.be.present().and.have.text(BASE_PROPS.title);
    expect(wrapper.find(ProductList)).to.be.present();
  });
  it('should render disabled buttons', () => {
    const context = getAnalytic().context;
    const wrapper = mount(<Component isExpired />, context);
    expect(wrapper).to.be.present();
    expect(wrapper.find('h3')).to.be.present().and.have.text(BASE_PROPS.title);
    expect(wrapper.find(ProductList)).to.be.present();
    expect(wrapper.find(ProductButton)).to.have.props({
      disabled: true
    });
  });
  it('should not render if there is no products', () => {
    const context = getAnalytic().context;
    const wrapper = mount(<Component products={[]} />, context);
    expect(wrapper).to.be.present();
    expect(wrapper.find('h3')).to.not.be.present();
    expect(wrapper.find(ProductList)).to.not.be.present();
  });
});
