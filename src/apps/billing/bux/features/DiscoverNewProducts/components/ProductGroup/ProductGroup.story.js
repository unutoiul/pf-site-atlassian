import { storiesOf } from '@storybook/react';
import { inContentChrome, inStoryKnob } from 'bux/common/helpers/storybook';

import ProductGroup from './ProductGroup';

const props = {
  productKey: 'jira-software.ondemand',
  title: 'Recommended Jira apps',
  products: [
    {
      productKey: 'jira-software.ondemand',
      name: 'Jira Software',
      tag: 'track',
      description:
        'Plan, track, and release world-class software. The #1 software development tool used by agile teams.',
      link: 'https://www.atlassian.com/software/jira',
      price: 10,
      currencyCode: 'usd',
      pricingLink: 'https://www.atlassian.com/software/jira',
      unitLabel: 'USER',
      isAnnual: false,
      action: {
        type: 'TRY',
        onClick: () => {}
      }
    },
    {
      productKey: 'jira-servicedesk.ondemand',
      name: 'Jira Service Desk',
      tag: 'track',
      description:
        'Give your customers an easy way to ask for help and your agents a fast way to resolve incidents.',
      link: 'https://www.atlassian.com/software/jira',
      price: 10,
      currencyCode: 'usd',
      pricingLink: 'https://www.atlassian.com/software/jira',
      unitLabel: 'USER',
      isAnnual: false,
      action: {
        type: 'TRY',
        onClick: () => {}
      }
    }
  ]
};

storiesOf('BUX|Features/DiscoverNewProducts/ProductGroup', module).add('Product Group', () =>
  inContentChrome(inStoryKnob(ProductGroup, props)));
