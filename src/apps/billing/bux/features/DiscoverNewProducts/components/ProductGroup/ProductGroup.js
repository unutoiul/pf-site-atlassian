import { Trans } from '@lingui/react';
import React from 'react';
import PropTypes from 'prop-types';
import { ExternalLink } from 'bux/components/Link';
import config from 'bux/common/config';
import { capitalize } from 'bux/common/helpers/utils';
import ProductList from '../ProductList';
import styles from './style.less';

const getLink = family => config.marketplaceUrl[family];

const ProductGroup = ({
  productKey,
  title, products,
  focusProductKey,
  isExpired,
  reloadRecommendedProducts,
  currencyCode,
  discoverLink = true
}) => {
  if (products.length === 0) {
    return null;
  }

  const productGroupName = capitalize(productKey);
  return (
    <div className={styles.productGroup} data-test={`products-group:${productKey}`}>
      <h3 className={styles.caption}>{title}</h3>
      <ProductList
        products={products}
        isExpired={isExpired}
        currencyCode={currencyCode}
        focusProductKey={focusProductKey}
        reloadRecommendedProducts={reloadRecommendedProducts}
      />

      {discoverLink && (
        <ExternalLink href={getLink(productKey)} data-test={`discover-more-apps-${productKey}`}>
          <Trans id="billing.discover.products.group.discover-more">Discover more {productGroupName} apps</Trans>
        </ExternalLink>
      )}
    </div>
  );
};

ProductGroup.propTypes = {
  productKey: PropTypes.string.isRequired,
  title: PropTypes.node.isRequired,
  products: PropTypes.array.isRequired,
  isExpired: PropTypes.bool.isRequired,
  discoverLink: PropTypes.bool,
  focusProductKey: PropTypes.string,
  currencyCode: PropTypes.string,
  reloadRecommendedProducts: PropTypes.func
};

export default ProductGroup;
