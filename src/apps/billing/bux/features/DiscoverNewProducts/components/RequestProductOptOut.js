import { I18n } from '@lingui/react';
import React from 'react';
import PropTypes from 'prop-types';
import { record } from 'bux/common/helpers/logger';
import { dangerouslyCreateSafeString } from '@atlassiansox/analytics';
import { XFlowImporter } from './xflow/xflow-importer';

class RequestProductOptOut extends React.PureComponent {
  static propTypes = {
    redirect: PropTypes.func.isRequired
  };

  componentWillMount() {
    record('grow0.xflow.experiment.triggered.admin.opt.out');
  }

  onComplete = () => this.props.redirect('/addapplication');

  onAnalyticsEvent = (name, properties) => {
    const mapObject = (obj, key) => {
      const value = properties[key];
      return {
        ...obj,
        [key]: typeof value === 'string' ? dangerouslyCreateSafeString(value) : value
      };
    };
    const attributes = Object.keys(properties);
    const safeProps = attributes.length > 0 ? attributes.reduce(mapObject, {}) : properties;
    record(`grow0.${name}`, safeProps);
  };

  render() {
    return (
      <I18n>{({ i18n }) => (
        <XFlowImporter>
          {({ XFlowOptOut }) => (
            <XFlowOptOut
              locale={i18n.language}
              onComplete={this.onComplete}
              onAnalyticsEvent={this.onAnalyticsEvent}
            />
          )}
        </XFlowImporter>
      )}</I18n>
    );
  }
}

export default RequestProductOptOut;
