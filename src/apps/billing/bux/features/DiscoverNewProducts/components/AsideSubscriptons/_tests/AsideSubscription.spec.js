import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { mount } from 'enzyme';
import { Provider } from 'bux/helpers/redux';
import proxyquire from 'bux/helpers/proxyquire';
import Link from 'bux/components/Link';
import { UnconnectedAsideSubscriptions } from '../AsideSubscriptions';

describe('AsideSubscriptions', () => {
  it('should connect to redux', () => {
    const data = [];
    const getEntitlementSections = sinon.stub().returns({ active: data });
    const { default: Aside, UnconnectedAsideSubscriptions: Inner } = proxyquire('../AsideSubscriptions', {
      'bux/core/selectors/entitlements': { getEntitlementSections }
    });
    const wrapper = mount(<Provider><Aside /></Provider>);
    expect(wrapper.find(Inner).prop('list')).to.equal(data);
  });

  it('should not render when list is empty', () => {
    const wrapper = mount(<UnconnectedAsideSubscriptions list={[]} />);
    expect(wrapper.html()).to.be.equal(null);
  });

  it('should render product list', () => {
    const wrapper = mount(<Provider><UnconnectedAsideSubscriptions list={[{ title: 'My Product' }]} /></Provider>);
    expect(wrapper.find('li')).be.have.length(1);
    expect(wrapper).to.contain.text('My Product');
    expect(wrapper.find(Link)).to.have.prop('to', '/applications');
  });
});
