import { Trans } from '@lingui/react';
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getEntitlementSections } from 'bux/core/selectors/entitlements';
import Island from 'bux/components/blocks/Island';
import Link from 'bux/components/Link';
import styles from './AsideSubscriptions.less';

export const UnconnectedAsideSubscriptions = ({ list }) => (
  list.length
    ? (
      <Island>
        <h4>
          <Trans id="billing.discover.subscriptions.title">
            Your current subscriptions
          </Trans>
        </h4>
        <p>
          <Trans id="billing.discover.subscriptions.info">
            You are currently subscribed to the following products and apps:
          </Trans>
        </p>

        <ul className={styles.list}>
          {list.map((item, index) => (
            <li key={index}>{item.title}</li>
          ))}
        </ul>

        <p>
          <Link to="/applications" name="manageSubscriptions" logPrefix="aside">
            <Trans id="billing.discover.subscriptions.manage">
              Manage your subscriptions
            </Trans>
          </Link>
        </p>
      </Island>
    )
    : null
);

UnconnectedAsideSubscriptions.propTypes = {
  list: PropTypes.arrayOf(PropTypes.any)
};

export default connect(state => ({
  list: getEntitlementSections(state).active
}))(UnconnectedAsideSubscriptions);
