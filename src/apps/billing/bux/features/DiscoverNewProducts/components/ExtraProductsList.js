import { Trans } from '@lingui/react';
import React from 'react';
import { redirect } from 'bux/common/helpers/browser';
import { tags } from 'bux/common/data/products';
import ProductGroup from './ProductGroup';

const goToSignUpPage = () => redirect('https://bitbucket.org/account/signup/');
const goToAccessPage = () => redirect('https://admin.atlassian.com/atlassian-access');

const extraProducts = [
  {
    productKey: 'bitbucket',
    familyName: 'bitbucket',
    name: 'Bitbucket',
    tag: tags.development,

    //price: { value: 0, currencyCode: 'usd' },
    activating: false,
    unlimitedTier: false,
    reactivation: false,
    eligibleForTrial: true,
    promoted: false,
    upgrading: false,
    isAnnual: false,

    description: (
      <Trans id="billing.products.bitbucket.description">
        Source code management in the cloud that's secure, fast and built for teams.
        Create and manage repositories, set up fine-grained permissions, and conduct code reviews.
      </Trans>
    ),
    link: 'http://www.bitbucket.org/',

    action: {
      onClick: goToSignUpPage,
      type: 'TRY',
      label: <Trans id="billing.products.bitbucket.button">Sign up for free</Trans>
    }
  },
  {
    productKey: 'atlassianAccess',
    familyName: 'atlassianAccess',
    name: 'Atlassian Access',
    tag: tags.security,
    activating: false,
    unlimitedTier: false,
    reactivation: false,
    eligibleForTrial: true,
    promoted: false,
    upgrading: false,
    isAnnual: false,

    description: (
      <Trans id="billing.products.atlassian-access.description">
        Enhanced security and unified user management across Jira, Confluence, and Bitbucket.
        Enable SAML SSO, automated user provisioning, enforced 2FA, and more.
        Access pricing is scalable - you'll only pay once per user (monthly or annually)
        across an unlimited number of Atlassian cloud products.
      </Trans>
    ),
    action: {
      onClick: goToAccessPage,
      type: 'TRY',
      label: 'Free trial'
    }
  }
];

export default () => (
  <ProductGroup
    title={<Trans id="billing.products.extra.title">More Atlassian applications</Trans>}
    productKey="extras"
    isExpired={false}
    discoverLink={false}
    products={extraProducts}
  />
);
