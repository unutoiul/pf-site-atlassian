import { Trans } from '@lingui/react';
import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  getState as getRecommendedProductsInfo,
  getApps,
  isAccountExpired
} from 'bux/core/state/recommended-products/selectors';
import { capitalize } from 'bux/common/helpers/utils';
import ProductGroup from './ProductGroup';

const AppList = ({ parents, isExpired, currencyCode }) => (
  <div>
    {parents.map((parent) => {
      const productGroupName = capitalize(parent.familyName);

      return (
        <ProductGroup
          key={parent.familyName}
          productKey={parent.familyName}
          title={<Trans id="billing.discover.products.apps.title">Recommended {productGroupName} apps</Trans>}
          products={parent.apps}
          currencyCode={currencyCode}
          isExpired={isExpired}
        />
      );
    })}
  </div>
);

AppList.propTypes = {
  currencyCode: PropTypes.string.isRequired,
  parents: PropTypes.array.isRequired,
  isExpired: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
  currencyCode: getRecommendedProductsInfo(state).currency,
  parents: getApps(state),
  isExpired: isAccountExpired(state),
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(AppList);
