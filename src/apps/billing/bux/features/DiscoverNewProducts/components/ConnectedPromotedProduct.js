import { connect } from 'react-redux';
import { isAccountExpired, getPromotedProductDetails } from 'bux/core/state/recommended-products/selectors';
import { getRecommendedProducts } from 'bux/core/state/recommended-products/actions';
import PromotedProduct from './PromotedProduct';

const mapStateToProps = state => ({
  product: getPromotedProductDetails(state),
  isExpired: isAccountExpired(state)
});

const mapDispatchToProps = {
  reloadRecommendedProducts: getRecommendedProducts
};

export default connect(mapStateToProps, mapDispatchToProps)(PromotedProduct);
