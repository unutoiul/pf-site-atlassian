import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import LogScope from 'bux/components/log/Scope';
import getAnalytic from 'bux/components/log/helpers/getAnalytic';
import PromotedProduct from '../PromotedProduct';
import Button from '../../../../../components/Button/Button';

const PROMOTED_PRODUCT = {
  productKey: 'confluence.ondemand',
  name: 'Confluence',
  tag: 'chat',
  description: 'Loren ipsum dolor sit amet',
  price: { value: 0, currencyCode: 'usd' },
};

describe('PromotedProduct', () => {
  it('should render', () => {
    const context = getAnalytic().context;
    const wrapper = shallow(<PromotedProduct product={PROMOTED_PRODUCT} />, context);

    const logScope = wrapper.find(LogScope);
    expect(logScope).to.be.present();
    expect(logScope).to.have.prop('prefix', 'promotedProduct');

    const buttons = wrapper.find(Button);
    expect(buttons).to.have.lengthOf(2);

    const tryButton = buttons.find({ name: 'try.confluence.ondemand' });
    expect(tryButton).to.be.present();
    expect(tryButton.children()).to.have.text('Try free');

    const learnMoreButton = buttons.find({ name: 'learnMore.confluence.ondemand' });
    expect(learnMoreButton).to.be.present();
    expect(learnMoreButton.children()).to.have.text('Learn more');
  });
});
