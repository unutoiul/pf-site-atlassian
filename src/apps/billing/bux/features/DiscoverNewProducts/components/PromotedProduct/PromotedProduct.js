import { Trans } from '@lingui/react';
import React from 'react';
import PropTypes, { func } from 'prop-types';
import { ButtonGroup } from '@atlaskit/button';
import Button from 'bux/components/Button';
import LogScope from 'bux/components/log/Scope';
import ProductLogo from 'bux/components/ak/logo-atlassian';
import JSDProductHeroImg from './images/service-desk-hero.png';
import style from './style.less';
import ConfluenceProductHero from './images/creating-content.svgx';
import ProductXflow from '../ProductCard/ProductXflow';

const JSDProductHero = () =>
  (<img src={JSDProductHeroImg} className={style.jsdHero} role="presentation" alt="Jira Service Desk" />);

const ConfigByProductKey = {
  'confluence.ondemand': {
    title: <Trans id="billing.discover.promoted.confluence.title">Jira Software’s perfect partner</Trans>,
    proposition: (
      <Trans id="billing.discover.promoted.confluence.description">
        Stop searching across email, Google Drive, and Word folders and start finding what you need.
        Keep code reviews, requirements, release notes, and more in Confluence.
      </Trans>
    ),
    heroImage: <ConfluenceProductHero />
  },
  'jira-servicedesk.ondemand': {
    title: (
      <Trans id="billing.discover.promoted.jira-servicedesk.title">
        Service desk software for modern IT teams
      </Trans>
    ),
    proposition: (
      <Trans id="billing.discover.promoted.jira-servicedesk.description">
        By linking Jira Service Desk with Jira Software,
        developer and IT teams can collaborate on a single platform to fix
        incidents faster and push changes with confidence.
      </Trans>
    ),
    heroImage: <JSDProductHero />
  }
};

class PromotedProduct extends React.PureComponent {
  static propTypes = {
    product: PropTypes.object,
    isExpired: PropTypes.bool,
    focusProductKey: PropTypes.string,
    reloadRecommendedProducts: PropTypes.func
  };

  static contextTypes = {
    sendAnalyticEvent: func.isRequired
  };

  state = {
    triggerXFlow: this.props.product && this.props.focusProductKey === this.props.product.productKey
  };

  onAnalyticsEvent = send => (evt, payload) => send(evt, payload);

  onComplete = () => {
    this.setState({ triggerXFlow: false });
    this.reloadRecommendedProducts();
    // TODO: Remove this after AK-4611 is merged. This fixes the scroll lock problem after closing xflow modal dialog.
    setTimeout(() => {
      document.body.style.overflow = '';
    }, 1000);
  };

  reloadRecommendedProducts = () => {
    if (this.props.reloadRecommendedProducts) {
      this.props.reloadRecommendedProducts();
    }
  };

  activate = () => {
    this.setState({ triggerXFlow: true });
  };

  render() {
    const { product, isExpired, focusProductKey } = this.props;
    if (!product || !ConfigByProductKey[product.productKey]) {
      return null;
    }

    const productConfig = ConfigByProductKey[product.productKey];
    if (!productConfig) {
      return null;
    }

    const { triggerXFlow } = this.state;
    const focus = product.productKey === focusProductKey;
    const sourceComponent = focus ? 'BUX#addapplication>RequestedTrialEmail' : 'BUX#addapplication>PromotedProduct';

    return (<LogScope prefix="promotedProduct">
      <div className={style.panelOuter} data-test="promoted-product">
        <div className={style.panelInner}>
          <div className={style.hero}>
            {productConfig.heroImage}
          </div>
          <div className={style.rightContainer}>
            <ProductLogo name={product.name} product={product.productKey} size="medium" />
            <p className={style.productTitle} data-test="product-title">{productConfig.title}</p>
            <p className={style.productProposition} data-test="product-proposition">{productConfig.proposition}</p>
            <ButtonGroup>
              <Button name={`try.${product.productKey}`} onClick={this.activate} primary disabled={isExpired}>
                <Trans id="billing.discover.promoted.try">Try free</Trans>
              </Button>
              <Button
                name={`learnMore.${product.productKey}`}
                subtle
                href={product.link}
                target="_blank"
                data-test="learn-more-link"
              >
                <Trans id="billing.discover.promoted.learn-more">Learn more</Trans>
              </Button>
            </ButtonGroup>
            {triggerXFlow &&
              <ProductXflow
                sourceComponent={sourceComponent}
                productKey={product.productKey}
                onComplete={this.onComplete}
                grantAccessEnabled
              />
            }
          </div>
        </div>
      </div>
    </LogScope>);
  }
}

export default PromotedProduct;
