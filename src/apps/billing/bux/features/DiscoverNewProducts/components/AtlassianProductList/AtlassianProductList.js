import { Trans } from '@lingui/react';
import React from 'react';
import ProductList from 'bux/features/DiscoverNewProducts/components/ProductList/ProductList';
import ProductGroup from 'bux/features/DiscoverNewProducts/components/ProductGroup/ProductGroup';

const AtlassianProductList = ({
  products, isExpired, reloadRecommendedProducts, focusProductKey, currencyCode,
}) => (
  <ProductGroup
    productKey="atlassian"
    title={<Trans id="billing.discover.products.atlassian.title">Atlassian products</Trans>}
    products={products}
    isExpired={isExpired}
    discoverLink={false}
    currencyCode={currencyCode}
    reloadRecommendedProducts={reloadRecommendedProducts}
    focusProductKey={focusProductKey}
  />
);

AtlassianProductList.propTypes = ProductList.propTypes;

export default AtlassianProductList;
