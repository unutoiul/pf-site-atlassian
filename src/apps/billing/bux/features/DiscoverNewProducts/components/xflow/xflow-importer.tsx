import * as React from 'react';
import Loadable from 'react-loadable';

import ErrorComponent from 'bux/components/deferred/ErrorIndicator';

interface XFlowLibrary {
  XFlow: any;
  RequestOrStartTrial: any;
  XFlowOptOut: any;
}

interface XFlowProps {
  xflow: XFlowLibrary;
}

interface XFlowPublicProps<T> {
  init(XFLOW): T;
  children(flow: XFlowLibrary, state: T): React.ReactNode;
}

class XFlowMounter<T> extends React.Component<XFlowProps & XFlowPublicProps<T>, T> {
  constructor(props) {
    super(props);
    this.state = props.init && props.init(props.xflow);
  }

  public render() {
    return this.props.children(this.props.xflow, this.state);
  }
}

// migrate to react-loadable-library one day.
export const XFlowImporter = Loadable({
  // tslint:disable-next-line:space-in-parens
  loader: async () => import(/* webpackChunkName:'xflow' */ '@atlassiansox/xflow-ui'),
  loading: ({ error, timedOut }) => {
    if (error || timedOut) {
      return <ErrorComponent />;
    }

    return null;
  },
  render: (xflowlib, props: XFlowPublicProps<any>) => <XFlowMounter {...props} xflow={xflowlib} />,
});
