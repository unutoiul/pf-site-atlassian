import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import { ExternalLink } from 'bux/components/Link';
import ProductDescription from '../ProductDescription';
import ProductContext from '../../../ProductContext';

const BASE_PROPS = {
  description: 'Loren ipsum dolor sit amet'
};
const Component = props => (
  <ProductContext.Provider value={{ ...BASE_PROPS, ...props }}>
    <ProductDescription />
  </ProductContext.Provider>
);

describe('ProductDescription', () => {
  it('should render', () => {
    const wrapper = mount(<Component />);
    expect(wrapper).to.be.present();
    expect(wrapper.find('a')).to.be.not.present();
  });
  it('should render learn more link', () => {
    const props = {
      link: 'http://someurl.org'
    };
    const wrapper = mount(<Component {...props} />);
    expect(wrapper.find(ExternalLink)).to.be.present();
    const link = wrapper.find(ExternalLink);
    expect(link).to.have.attr('href', props.link);
    expect(link).to.have.text('Learn more');
  });
});
