import { Trans } from '@lingui/react';
import React from 'react';
import PropTypes from 'prop-types';
import { Analytics } from 'bux/components/Analytics';
import { ExternalLink } from 'bux/components/Link';
import { Footnote } from 'bux/components/Footnote';
import styles from './style.less';
import PriceValue from './components/PriceValue';
import PriceString from './components/PriceString';
import GetStartedLabel from './components/GetStartedLabel';
import ProductContext from '../../ProductContext';

const ProductPrice = ({ onPricingClick = () => {} }) => (
  <ProductContext.Consumer>
    {({ price, pricingLink, additionalInfo }) =>
      (<div className={styles.productPrice}>
        <GetStartedLabel />
        { price === 0 ? <PriceString /> : <PriceValue />}
        {additionalInfo && <Footnote>{additionalInfo}</Footnote>}
        {pricingLink && (
          <Analytics.UI as="seeFullPricingLink">
            <ExternalLink onClick={onPricingClick} href={pricingLink} target="_blank" data-test="full-price-link">
              <Trans id="billing.discover.product.full-pricing">See full pricing</Trans>
            </ExternalLink>
          </Analytics.UI>
        )}
       </div>)
    }
  </ProductContext.Consumer>
);

ProductPrice.propTypes = {
  onPricingClick: PropTypes.func
};

export default ProductPrice;
