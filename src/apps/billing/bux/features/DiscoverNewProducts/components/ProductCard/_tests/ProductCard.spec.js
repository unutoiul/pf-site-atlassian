import React from 'react';
import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import LogScope from 'bux/components/log/Scope';
import getAnalytic from 'bux/components/log/helpers/getAnalytic';
import { TRY, USER, UPGRADING } from 'bux/core/state/recommended-products/constants';
import ProductCard from '../../ProductCard';
import ProductDescription from '../../ProductCard/components/ProductDescription';
import ProductPrice from '../../ProductCard/components/ProductPrice';
import ProductButton from '../../ProductCard/components/ProductButton';
import ScheduledUpgrade from '../../ProductCard/components/ScheduledUpgrade';

const BASE_PROPS = {
  productKey: 'confluence.ondemand',
  name: 'Confluence',
  tag: 'chat',
  description: 'Loren ipsum dolor sit amet',
  price: 0,
  action: {
    type: TRY,
    onClick: () => {}
  },
  unitLabel: USER,
  isAnnual: false,
  currencyCode: 'usd'
};
const Component = props => (<ProductCard {...BASE_PROPS} {...props} />);

describe('ProductCard', () => {
  it('should render', () => {
    const context = getAnalytic().context;
    const wrapper = mount(<Component />, context);

    const listItem = wrapper.find('li');
    expect(listItem).to.be.present();

    const logScope = listItem.find(LogScope);
    expect(logScope).to.be.present();
    expect(logScope).to.have.prop('prefix', BASE_PROPS.productKey);

    expect(listItem.find(ProductButton)).to.be.present();

    const description = listItem.find(ProductDescription);
    expect(description).to.be.present();
    expect(description).to.contain.text(BASE_PROPS.description);

    const price = listItem.find(ProductPrice);
    expect(price).to.be.present();
    expect(price).to.contain.text('Free for all teams');
  });

  it('should not render product price', () => {
    const context = getAnalytic().context;
    const props = { ...BASE_PROPS, upgradeDate: '2020-02-28', action: { ...BASE_PROPS.action, type: UPGRADING } };
    const wrapper = mount(<Component {...props} />, context);
    expect(wrapper.find(ProductPrice)).to.not.be.present();
    expect(wrapper.find(ScheduledUpgrade)).to.be.present();
    expect(wrapper.find(ScheduledUpgrade)).to.contain.text('Feb 28, 2020');
  });

  it('should triggerXFlow for focus product', () => {
    const context = getAnalytic().context;
    const props = { ...BASE_PROPS, focus: true };
    const wrapper = shallow(<ProductCard {...props} />, context);
    expect(wrapper).to.have.state('triggerXFlow', true);
  });
});
