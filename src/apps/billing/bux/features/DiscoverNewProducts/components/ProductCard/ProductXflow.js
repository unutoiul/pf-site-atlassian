import { I18n } from '@lingui/react';
import React from 'react';
import { string, func, bool } from 'prop-types';
import { XFlowImporter } from '../xflow/xflow-importer';

const defaultRequestOrStartTrialProps = ({
  productKey, sourceContext, sourceComponent, grantAccessEnabled, isCrossSell
}) => ({
  sourceComponent,
  sourceContext: sourceContext || 'addapplication-page',
  targetProduct: productKey,
  canCurrentUserAddProduct: () => true, //for BUX this is always true
  canCurrentUserGrantAccessToProducts: () => true, //for BUX this is always true
  grantAccessEnabled,
  isCrossSell,
  retrieveIsOptOutEnabled: () => false,
  retrieveCanManageSubscriptions: () => true,
});

const ProductXflow = (props, context) => (
  <I18n>{({ i18n }) => (
    <XFlowImporter>
      {({ XFlow }) => (
        <XFlow
          {...defaultRequestOrStartTrialProps(props)}
          locale={i18n.language}
          onAnalyticsEvent={context.sendAnalyticEvent}
          onComplete={props.onComplete}
        />
      )}
    </XFlowImporter>
  )}</I18n>
);

ProductXflow.propTypes = {
  sourceContext: string,
  sourceComponent: string.isRequired,
  productKey: string.isRequired,
  onComplete: func.isRequired,
  grantAccessEnabled: bool,
  isCrossSell: bool,
};

ProductXflow.defaultProps = {
  grantAccessEnabled: false,
  isCrossSell: false,
};

ProductXflow.contextTypes = {
  sendAnalyticEvent: func.isRequired
};

export default ProductXflow;
