import { Trans } from '@lingui/react';
import PropTypes from 'prop-types';
import React from 'react';
import Usage from 'bux/components/Usage';
import ProductContext from '../../../ProductContext';
import styles from '../style.less';

const Label = ({
  upgrading, name, upgradePrice, isApp, initialUnitPackage, unitLabel
}) => {
  if (upgrading && upgradePrice) {
    return <Trans id="billing.discover.product.get-started.upgrade">Upgrade to {name} now!</Trans>;
  }
  if (!initialUnitPackage && isApp) {
    return <Trans id="billing.discover.product.get-started.based-on-current">Based on your current instance</Trans>;
  }
  const unit = <Usage unitsLimit={initialUnitPackage} unitCode={unitLabel} />;
  if (initialUnitPackage > 1 && unit) {
    return (
      <Trans id="billing.discover.product.get-started.start-with">
        Get started with {unit}
      </Trans>
    );
  }
  return <Trans id="billing.discover.product.get-started.now">Get started now</Trans>;
};

Label.propTypes = {
  name: PropTypes.string,
  upgrading: PropTypes.bool,
  upgradePrice: PropTypes.number,
  isApp: PropTypes.bool,
  initialUnitPackage: PropTypes.number,
  unitLabel: PropTypes.string,
};

const GetStartedLabel = () => (
  <ProductContext.Consumer>
    {props =>
      <p className={styles.getStartedLabel} data-test="get-started-label"><Label {...props} /></p>
    }
  </ProductContext.Consumer>
);

export default GetStartedLabel;
