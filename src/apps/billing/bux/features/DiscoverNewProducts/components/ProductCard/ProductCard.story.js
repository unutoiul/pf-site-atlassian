import { storiesOf } from '@storybook/react';
import { inContentChrome, inStoryKnob } from 'bux/common/helpers/storybook';

import ProductCard from './ProductCard';

const props = {
  productKey: 'jira-software.ondemand',
  name: 'Jira Software',
  tag: 'track',
  description: 'Plan, track, and release world-class software. The #1 software development tool used by agile teams.',
  link: 'https://www.atlassian.com/software/jira',
  price: 10,
  currencyCode: 'usd',
  pricingLink: 'https://www.atlassian.com/software/jira',
  unitLabel: 'USER',
  isAnnual: false,
  action: {
    type: 'TRY',
    onClick: () => {}
  }
};

storiesOf('BUX|Features/DiscoverNewProducts/ProductCard', module)
  .add('Priced product', () => inContentChrome(inStoryKnob(ProductCard, props)))
  .add('Product with additional info', () =>
    inContentChrome(inStoryKnob(ProductCard, { ...props, additionalInfo: 'until the end of December' })))
  .add('Free product', () =>
    inContentChrome(inStoryKnob(ProductCard, { ...props, price: 0 })))
  .add('Free product with no pricing link', () =>
    inContentChrome(inStoryKnob(ProductCard, {
      ...props,
      price: 0,
      pricingLink: null
    })))
  .add('Free product with custom pricing label', () =>
    inContentChrome(inStoryKnob(ProductCard, {
      ...props,
      price: 0,
      priceFreeLabel: 'Free early access'
    })))
  .add('Activating product', () =>
    inContentChrome(inStoryKnob(ProductCard, { ...props, action: { type: 'ACTIVATING', onClick: () => {} } })))
  .add('Scheduled upgrade', () =>
    inContentChrome(inStoryKnob(ProductCard, {
      ...props,
      action: { type: 'UPGRADING', onClick: () => {} },
      upgradeDate: '2010-10-02'
    })))
  .add('Custom call to action', () =>
    inContentChrome(inStoryKnob(ProductCard, {
      ...props,
      action: { label: 'Upgrade to Stride', onClick: () => {} }
    })));
