import { Trans } from '@lingui/react';
import React from 'react';
import PropTypes from 'prop-types';
import { Analytics } from 'bux/components/Analytics';
import { ExternalLink } from 'bux/components/Link';
import styles from './style.less';
import ProductContext from '../../ProductContext';

const ProductDescription = ({ onLearnMoreClick = () => {} }) => (
  <ProductContext.Consumer>
    {({ description, link }) =>
      (<div className={styles.productDescription}>
        <p>{description}</p>
        {link &&
          <Analytics.UI as="learnMoreLink">
            <ExternalLink onClick={onLearnMoreClick} href={link} target="_blank" data-test="learn-more-link">
              <Trans id="billing.discover.product.learn-more">
                Learn more
              </Trans>
            </ExternalLink>
          </Analytics.UI>
        }
       </div>)
    }
  </ProductContext.Consumer>
);

ProductDescription.propTypes = {
  onLearnMoreClick: PropTypes.func
};

export default ProductDescription;
