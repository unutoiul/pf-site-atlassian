import React from 'react';
import cx from 'classnames';
import { priceFreeLabels } from 'bux/common/data/products';
import styles from '../style.less';
import ProductContext from '../../../ProductContext';

const PriceString = () => (
  <ProductContext.Consumer>
    {({ priceFreeLabel = priceFreeLabels.default, additionalInfo }) =>
      (<p className={cx(styles.priceLabel, { [styles.priceLabelAdditionalInfo]: !!additionalInfo })} data-test="price">
        {priceFreeLabel}
       </p>)
    }
  </ProductContext.Consumer>
);

export default PriceString;
