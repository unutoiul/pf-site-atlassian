import { Trans } from '@lingui/react';
import React from 'react';
import { FormattedDate } from 'bux/components/Date';
import styles from './style.less';
import ProductContext from '../../ProductContext';

const ScheduledUpgrade = () => (
  <ProductContext.Consumer>
    {({ name: productName, upgradeDate }) => {
      const formattedUpgradeDate = <strong><FormattedDate value={upgradeDate} format="full" /></strong>;

      return (<div className={styles.scheduledUpgrade}>
          <Trans id="billing.discover.product.scheduled-upgrade">
            Your upgrade to {productName} has been scheduled and will be available on:<br />{formattedUpgradeDate}
          </Trans>
        </div>
      );
    }}
  </ProductContext.Consumer>
);

export default ScheduledUpgrade;
