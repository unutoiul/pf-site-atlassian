import React, { Component } from 'react';
import { string, shape, bool, number, func, node, oneOfType } from 'prop-types';
import Lozenge from '@atlaskit/lozenge';
import { Media } from 'common/responsive/Matcher';

import { redirect } from 'bux/common/helpers/browser';
import LogScope from 'bux/components/log/Scope';
import ProductLogo from 'bux/components/ak/logo-atlassian';
import { UPGRADE, UPGRADING } from 'bux/core/state/recommended-products/constants';
import ProductDescription from './components/ProductDescription';
import ProductPrice from './components/ProductPrice';
import ProductButton from './components/ProductButton';
import ScheduledUpgrade from './components/ScheduledUpgrade';
import ProductContext from './ProductContext';
import styles from './style.less';
import ProductXflow from './ProductXflow';

export const shouldGrantAccessEnabled = [
  'jira-core.ondemand',
  'jira-software.ondemand',
  'jira-servicedesk.ondemand',
  'jira.ondemand',
  'jira-incident-manager.ondemand',
  'confluence.ondemand',
];

export default class ProductCard extends Component {
  static propTypes = {
    productKey: string.isRequired,
    familyName: string,
    name: string.isRequired,
    tag: node.isRequired,
    description: node.isRequired,
    link: string,
    action: shape({
      label: oneOfType([string, node]),
      type: string.isRequired,
      onClick: func
    }).isRequired,
    upgradeDate: string,
    price: number,
    currencyCode: string,
    upgradePrice: number,
    upgradeUrl: string,
    priceFreeLabel: node,
    pricingLink: string,
    additionalInfo: string,
    unitLabel: string,
    initialUnitPackage: number,
    isAnnual: bool.isRequired,
    isApp: bool,
    infiniteTier: bool,
    isExpired: bool,
    outsideInstall: string,
    reloadRecommendedProducts: func,
    focus: bool
  };

  static contextTypes = {
    sendAnalyticEvent: func.isRequired
  };

  state = {
    triggerXFlow: this.props.focus
  };

  onComplete = () => {
    this.setState({ triggerXFlow: false });
    this.reloadRecommendedProducts();
    // TODO: Remove this after AK-4611 is merged. This fixes the scroll lock problem after closing xflow modal dialog.
    setTimeout(() => {
      document.body.style.overflow = '';
    }, 1000);
  };

  reloadRecommendedProducts = () => {
    if (this.props.reloadRecommendedProducts) {
      this.props.reloadRecommendedProducts();
    }
  };

  activate = () => {
    const {
      isApp, upgradeUrl, action: { type }, outsideInstall
    } = this.props;
    if (type === UPGRADE && upgradeUrl) {
      redirect(upgradeUrl);
      return;
    }

    if (isApp) {
      redirect(outsideInstall);
      return;
    }

    this.setState({ triggerXFlow: true });
  };

  render() {
    const {
      productKey,
      name,
      tag,
      isExpired,
      price,
      currencyCode,
      focus,
      action: { type }
    } = this.props;

    const { triggerXFlow } = this.state;
    const sourceComponent = focus ? 'BUX#addapplication>RequestedTrialEmail' : 'BUX#addapplication>ProductCard';

    return (
      <li className={styles.productItem} data-test={`product:${productKey}`}>
        <LogScope prefix={productKey}>
          <ProductContext.Provider value={this.props}>
            <div className={styles.productCard}>
              <div className={styles.productTitleBlock}>
                <div className={styles.productTitle} data-test="product-title">
                  <ProductLogo product={productKey} size="medium" name={name} />
                  <Media.Below including mobile>
                      <div className={styles.productFamily} data-test="product-tag">
                        <Lozenge>{tag}</Lozenge>
                      </div>
                  </Media.Below>
                </div>
                <Media.Above mobile>
                    <div className={styles.productFamily} data-test="product-tag">
                      <Lozenge>{tag}</Lozenge>
                    </div>
                </Media.Above>
                <ProductButton onClick={this.activate} disabled={isExpired} />
                {
                  triggerXFlow &&
                    <ProductXflow
                      sourceComponent={sourceComponent}
                      productKey={productKey}
                      onComplete={this.onComplete}
                      grantAccessEnabled={shouldGrantAccessEnabled.includes(productKey)}
                    />
                }
              </div>
              <div className={styles.productInfoBlock}>
                <ProductDescription />
                {type !== UPGRADING && typeof price !== 'undefined' && currencyCode && <ProductPrice />}
                {type === UPGRADING && <ScheduledUpgrade />}
              </div>
            </div>
          </ProductContext.Provider>
        </LogScope>
      </li>
    );
  }
}
