import React from 'react';
import PropTypes from 'prop-types';
import sinon from 'sinon';
import { expect } from 'chai';
import { mount } from 'enzyme';
import Button from 'bux/components/Button';
import getAnalytic from 'bux/components/log/helpers/getAnalytic';
import { TRY, UPGRADING, ACTIVATING, UPGRADE, REACTIVATE } from 'bux/core/state/recommended-products/constants';
import ProductButton from '../ProductButton';
import ProductContext from '../../../ProductContext';

const BASE_PROPS = {
  action: {
    type: TRY,
    onClick: () => {},
  }
};
const Component = props => (
  <ProductContext.Provider value={{ ...BASE_PROPS, ...props }}>
    <ProductButton disabled={props.disabled} />
  </ProductContext.Provider>
);

Component.propTypes = {
  disabled: PropTypes.bool
};

describe('ProductButton', () => {
  context('Label', () => {
    const shouldRenderButton = (props) => {
      const wrapper = mount(<Component {...props} />);
      expect(wrapper).to.be.present();
      const button = wrapper.find(Button);
      expect(button).to.be.present();
      return button;
    };

    it('should render free trial', () => {
      const button = shouldRenderButton();
      expect(button).to.have.text('Free trial');
    });

    it('should render upgrade scheduled', () => {
      const button = shouldRenderButton({ action: { ...BASE_PROPS.action, type: UPGRADING } });
      expect(button).to.have.text('Upgrade scheduled');
      expect(button).to.have.prop('disabled', true);
    });

    it('should render upgrade to...', () => {
      const button = shouldRenderButton({ name: 'Stride', action: { ...BASE_PROPS.action, type: UPGRADE } });
      expect(button).to.have.text('Upgrade to Stride');
    });

    it('should render reactivate', () => {
      const button = shouldRenderButton({ action: { ...BASE_PROPS.action, type: REACTIVATE } });
      expect(button).to.have.text('Reactivate');
    });

    it('should render custom label ', () => {
      const button = shouldRenderButton({ action: { ...BASE_PROPS.action, label: 'Label' } });
      expect(button).to.have.text('Label');
    });

    it('should set button in activating mode', () => {
      const button = shouldRenderButton({ action: { ...BASE_PROPS.action, type: ACTIVATING } });
      expect(button).to.have.text('Activating');
      expect(button).to.have.props({
        spinning: true,
        disabled: true
      });
    });

    it('should render Stride as "Start now"', () => {
      const button = shouldRenderButton({ name: 'Stride', action: { ...BASE_PROPS.action } });
      expect(button).to.have.text('Start now');
    });

    it('should render Jira Ops as "Start now"', () => {
      const button = shouldRenderButton({ name: 'Jira Ops', action: { ...BASE_PROPS.action } });
      expect(button).to.have.text('Start now');
    });

    it('should render disabled button', () => {
      const button = shouldRenderButton({ disabled: true });
      expect(button).to.have.text('Free trial');
      expect(button).to.have.props({
        disabled: true
      });
    });
  });
  context('Click', () => {
    it('should call onClick when the button is clicked', () => {
      const onClickHandle = sinon.stub();
      const analytic = getAnalytic();
      const wrapper = mount(<Component action={{ onClick: onClickHandle }} />, analytic.context);
      expect(wrapper).to.be.present();
      const button = wrapper.find(Button);
      expect(button).to.be.present();
      button.simulate('click');
      expect(onClickHandle).to.be.calledOnce();
      expect(analytic.sendAnalyticEvent).to.be.calledOnce();
    });
    it('should NOT call onClick when the product is activating', () => {
      const onClickHandle = sinon.stub();
      const wrapper = mount(<Component action={{ type: ACTIVATING, onClick: onClickHandle }} />);
      expect(wrapper).to.be.present();
      const button = wrapper.find(Button);
      expect(button).to.be.present();
      button.simulate('click');
      expect(onClickHandle).to.not.be.called();
    });
    it('should not call onClick when the button is disabled', () => {
      const onClickHandle = sinon.stub();
      const wrapper = mount(<Component action={{ onClick: onClickHandle }} disabled />);
      expect(wrapper).to.be.present();
      const button = wrapper.find(Button);
      expect(button).to.be.present();
      button.simulate('click');
      expect(onClickHandle).to.not.be.called();
    });
  });
});
