import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import ScheduledUpgrade from '../ScheduledUpgrade';
import ProductContext from '../../../ProductContext';

const BASE_PROPS = {
  name: 'Jira',
  upgradeDate: '2010-10-01'
};
const Component = props => (
  <ProductContext.Provider value={{ ...BASE_PROPS, ...props }}>
    <ScheduledUpgrade />
  </ProductContext.Provider>
);

describe('ScheduledUpgrade', () => {
  it('should render', () => {
    const wrapper = mount(<Component />);
    expect(wrapper).to.be.present();
    expect(wrapper).to.contain.text('Your upgrade to Jira');
    expect(wrapper).to.contain.text('Oct 1, 2010');
  });
});
