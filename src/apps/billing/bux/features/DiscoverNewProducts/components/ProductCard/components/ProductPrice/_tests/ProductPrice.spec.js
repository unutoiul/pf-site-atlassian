import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import { ExternalLink } from 'bux/components/Link';
import { USER } from 'bux/core/state/recommended-products/constants';
import ProductPrice from '../ProductPrice';
import PriceString from '../components/PriceString';
import PriceValue from '../components/PriceValue';
import GetStartedLabel from '../components/GetStartedLabel';
import ProductContext from '../../../ProductContext';

const BASE_PROPS = {
  price: 0,
  currencyCode: 'usd',
  unitLabel: USER,
  isAnnual: false
};
const Component = props => (
  <ProductContext.Provider value={{ ...BASE_PROPS, ...props }}>
    <ProductPrice />
  </ProductContext.Provider>
);

describe('ProductPrice', () => {
  it('should render free format', () => {
    const wrapper = mount(<Component />);
    expect(wrapper).to.be.present();
    const priceString = wrapper.find(PriceString);
    expect(priceString).to.have.text('Free for all teams');
  });

  it('should render additional info', () => {
    const wrapper = mount(<Component additionalInfo="this is additional info" />);
    expect(wrapper).to.be.present();
    expect(wrapper).to.contain.text('this is additional info');
  });

  it('should render full price link', () => {
    const props = {
      pricingLink: 'http://someurl.org'
    };
    const wrapper = mount(<Component {...props} />);
    expect(wrapper).to.be.present();
    expect(wrapper.find(ExternalLink)).to.be.present();
    const link = wrapper.find(ExternalLink);
    expect(link).to.have.text('See full pricing');
    expect(link).to.have.attr('href', props.pricingLink);
  });

  context('PriceValue', () => {
    it('should render price/cycle format', () => {
      const props = {
        price: 10
      };
      const wrapper = mount(<Component {...props} />);
      expect(wrapper).to.be.present();
      const priceValue = wrapper.find(PriceValue);
      expect(priceValue).to.have.text('$10 / month');
    });

    it('should render price/unit/cycle format', () => {
      const props = {
        price: 10,
        initialUnitPackage: 1
      };
      const wrapper = mount(<Component {...props} />);
      expect(wrapper).to.be.present();
      const priceValue = wrapper.find(PriceValue);
      expect(priceValue).to.have.text('$10 / user / month');
    });
  });

  context('GetStartedLabel', () => {
    it('should render Upgrade to Stride now!', () => {
      const props = {
        price: 10,
        upgradePrice: 2,
        upgrading: true,
        name: 'Stride'
      };
      const wrapper = mount(<Component {...props} />);
      expect(wrapper).to.be.present();
      const getStartedLabel = wrapper.find(GetStartedLabel);
      expect(getStartedLabel).to.have.text('Upgrade to Stride now!');
    });
    it('should render Based on your current instance', () => {
      const props = {
        price: 10,
        isApp: true
      };
      const wrapper = mount(<Component {...props} />);
      expect(wrapper).to.be.present();
      const getStartedLabel = wrapper.find(GetStartedLabel);
      expect(getStartedLabel).to.have.text('Based on your current instance');
    });
    it('should render Get started with 10 users', () => {
      const props = {
        price: 10,
        initialUnitPackage: 10
      };
      const wrapper = mount(<Component {...props} />);
      expect(wrapper).to.be.present();
      const getStartedLabel = wrapper.find(GetStartedLabel);
      expect(getStartedLabel).to.have.text('Get started with 10 users');
    });
    it('should render Get started now', () => {
      const props = {
        price: 10
      };
      const wrapper = mount(<Component {...props} />);
      expect(wrapper).to.be.present();
      const getStartedLabel = wrapper.find(GetStartedLabel);
      expect(getStartedLabel).to.have.text('Get started now');
    });
  });
});
