import { Trans } from '@lingui/react';
import React from 'react';
import propTypes from 'prop-types';
import { Analytics } from 'bux/components/Analytics';
import { SubmitButton } from 'bux/components/Button';
import { UPGRADE, REACTIVATE, ACTIVATING, UPGRADING } from 'bux/core/state/recommended-products/constants';
import ProductContext from '../../ProductContext';

const getButtonInfo = ({ label, name, type }) => {
  switch (type) {
    case ACTIVATING:
      return {
        label: <Trans id="billing.discover.product.button.activating">Activating</Trans>
      };
    case UPGRADING:
      return {
        label: <Trans id="billing.discover.product.button.upgrading">Upgrade scheduled</Trans>
      };
    case UPGRADE:
      return {
        label: <Trans id="billing.discover.product.button.upgrade">Upgrade to {name}</Trans>
      };
    case REACTIVATE:
      return {
        label: <Trans id="billing.discover.product.button.reactivate">Reactivate</Trans>
      };
    default:
      if (name === 'Stride' || name === 'Jira Ops') {
        return {
          label: <Trans id="billing.discover.product.button.start-now">Start now</Trans>
        };
      }
      if (label) {
        return { label };
      }
      return {
        actionSubjectId: 'freeTrialButton',
        label: <Trans id="billing.discover.product.button.trial">Free trial</Trans>
      };
  }
};

const ProductButton = ({ onClick, disabled }) => (
  <ProductContext.Consumer>
    {({ action: { onClick: contextOnClick, type, label } = {}, name }) => {
      const info = getButtonInfo({ label, name, type });
      return (
        <Analytics.UI as={info.actionSubjectId}>
          <SubmitButton
            spinning={type === ACTIVATING}
            disabled={disabled || type === ACTIVATING || type === UPGRADING}
            onClick={contextOnClick || onClick}
          >
            {info.label}
          </SubmitButton>
        </Analytics.UI>
      );
    }}
  </ProductContext.Consumer>
);

ProductButton.propTypes = {
  onClick: propTypes.func,
  disabled: propTypes.bool,
};

export default ProductButton;
