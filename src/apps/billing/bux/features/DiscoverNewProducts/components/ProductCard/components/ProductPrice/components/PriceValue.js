import { Trans } from '@lingui/react';
import React from 'react';
import cx from 'classnames';
import Money from 'bux/components/Money';
import { FootnoteIndicator } from 'bux/components/Footnote';
import { UsageUnitSingularLabel } from 'bux/components/Usage';
import styles from '../style.less';
import ProductContext from '../../../ProductContext';

const PriceValue = () => (
  <p className={cx(styles.priceLabel)} data-test="price">
    <ProductContext.Consumer>
      {({
  price, currencyCode, additionalInfo, unitLabel, isAnnual, initialUnitPackage,
}) => {
        const moneyAmount = (
          <FootnoteIndicator condition={!!additionalInfo}>
            <Money amount={price} currency={currencyCode} />
          </FootnoteIndicator>
        );

        const unit = <UsageUnitSingularLabel unitCode={unitLabel} />;
        const displayUnits = (initialUnitPackage === 1 && unit);
        if (isAnnual) {
          if (displayUnits) {
            return (
              <Trans id="billing.discover.product.price.units.annual">
                {moneyAmount}<small> / {unit} / year</small>
              </Trans>
            );
          }

          return (
            <Trans id="billing.discover.product.price.annual">
              {moneyAmount}<small> / year</small>
            </Trans>
          );
        }

        if (displayUnits) {
          return (
            <Trans id="billing.discover.product.price.units.monthly">
              {moneyAmount}<small> / {unit} / month</small>
            </Trans>
          );
        }

        return (
          <Trans id="billing.discover.product.price.monthly">
            {moneyAmount}<small> / month</small>
          </Trans>
        );
      }}
    </ProductContext.Consumer>
  </p>
);

export default PriceValue;
