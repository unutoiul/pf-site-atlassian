import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import configureMockStore from 'redux-mock-store';
import getAnalytic from 'bux/components/log/helpers/getAnalytic';
import ConnectedPromotedProduct from '../ConnectedPromotedProduct';
import recommendedProducts from '../../../../../mock-server/bux/api/billing/recommended-products-get.json';

describe('ConnectedPromotedProduct', () => {
  const mockStoreCreator = () => {
    const data = {
      ...recommendedProducts
    };

    expect(data.recommendedProducts.products).to.have.lengthOf(6);
    return configureMockStore()({
      recommendedProducts: { data }
    });
  };

  it('should render a promoted product', () => {
    const context = getAnalytic().context;
    const mockedStore = mockStoreCreator();
    const wrapper = shallow(<ConnectedPromotedProduct store={mockedStore} />, context);
    expect(wrapper).to.be.present();
    expect(wrapper).to.have.prop('product');
  });
});
