import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import configureMockStore from 'redux-mock-store';
import getAnalytic from 'bux/components/log/helpers/getAnalytic';
import AppList from '../AppList';
import ProductGroup from '../ProductGroup';
import ProductList from '../ProductList';
import recommendedProducts from '../../../../../mock-server/bux/api/billing/recommended-products-get.json';

describe('AppList', () => {
  const mockStoreCreator = () => configureMockStore()({
    recommendedProducts: { data: recommendedProducts },
    billEstimate: { current: { currencyCode: 'usd' } },
  });

  it('should render a list of product groups', () => {
    const context = getAnalytic().context;
    const mockedStore = mockStoreCreator();
    const wrapper = mount(<AppList store={mockedStore} />, context);
    expect(wrapper).to.be.present();
    expect(wrapper.find(ProductGroup)).to.have.length(2);
    const group = wrapper.find(ProductGroup).first();
    expect(group.find(ProductList)).to.have.prop('products').and.length(2);
  });
});
