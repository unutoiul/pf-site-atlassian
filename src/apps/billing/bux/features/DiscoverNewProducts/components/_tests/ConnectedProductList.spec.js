import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import configureMockStore from 'redux-mock-store';
import getAnalytic from 'bux/components/log/helpers/getAnalytic';
import ConnectedProductList from '../ConnectedProductList';
import ProductList from '../ProductList';
import ProductCard from '../ProductCard';
import recommendedProducts from '../../../../../mock-server/bux/api/billing/recommended-products-get.json';

describe('ConnectedProductList', () => {
  const mockStoreCreator = (products) => {
    let data = {
      ...recommendedProducts
    };
    if (products) {
      data = { ...data, recommendedProducts: { products } };
    }
    return configureMockStore()({
      recommendedProducts: { data },
      billEstimate: { current: { currencyCode: 'usd' } },
    });
  };

  it('should render a list of products', () => {
    const context = getAnalytic().context;
    const mockedStore = mockStoreCreator();
    const wrapper = mount(<ConnectedProductList store={mockedStore} />, context);
    expect(wrapper).to.be.present();
    expect(wrapper.find('h3')).to.be.present();
    expect(wrapper.find(ProductList)).to.have.prop('products').and.length(5);
    expect(wrapper.find(ProductCard)).to.have.length(5);
  });

  it('when no products should not render a list of products', () => {
    const context = getAnalytic().context;
    const mockedStore = mockStoreCreator([]);
    const wrapper = mount(<ConnectedProductList store={mockedStore} />, context);
    expect(wrapper).to.be.present();
    expect(wrapper.find('h3')).to.not.be.present();
    expect(wrapper.find(ProductList)).to.not.be.present();
  });
});
