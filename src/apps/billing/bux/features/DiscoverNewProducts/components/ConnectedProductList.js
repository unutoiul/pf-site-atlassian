import { connect } from 'react-redux';
import {
  getState as getRecommendedProductsInfo,
  getProducts,
  isAccountExpired
} from 'bux/core/state/recommended-products/selectors';
import { getRecommendedProducts } from 'bux/core/state/recommended-products/actions';
import AtlassianProductList from './AtlassianProductList';

const mapStateToProps = state => ({
  currencyCode: getRecommendedProductsInfo(state).currency,
  products: getProducts(state),
  isExpired: isAccountExpired(state),
});

const mapDispatchToProps = {
  reloadRecommendedProducts: getRecommendedProducts
};

export default connect(mapStateToProps, mapDispatchToProps)(AtlassianProductList);
