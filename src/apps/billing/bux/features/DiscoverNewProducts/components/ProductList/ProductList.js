import React from 'react';
import PropTypes from 'prop-types';
import ProductCard from '../ProductCard';
import styles from './style.less';

const ProductList = ({
  products, isExpired, currencyCode, reloadRecommendedProducts, focusProductKey
}) => (
  products.length > 0 ?
    <ul className={styles.productList} data-test="product-list">
      {
        products.map(product =>
          (<ProductCard
            key={product.productKey}
            isExpired={isExpired}
            currencyCode={currencyCode}
            focus={product.productKey === focusProductKey}
            reloadRecommendedProducts={reloadRecommendedProducts}
            {...product}
          />))
      }
    </ul>
    : null
);

ProductList.propTypes = {
  products: PropTypes.arrayOf(PropTypes.shape(ProductCard.propTypes)).isRequired,
  isExpired: PropTypes.bool.isRequired,
  currencyCode: PropTypes.string,
  reloadRecommendedProducts: PropTypes.func,
  focusProductKey: PropTypes.string
};

export default ProductList;
