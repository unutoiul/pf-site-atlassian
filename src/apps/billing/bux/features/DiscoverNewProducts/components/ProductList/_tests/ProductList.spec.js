import React from 'react';
import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import { TRY, USER } from 'bux/core/state/recommended-products/constants';
import getAnalytic from 'bux/components/log/helpers/getAnalytic';
import ProductList from '../ProductList';
import ProductCard from '../../ProductCard';

const BASE_PROPS = {
  isExpired: false,
  products: [
    {
      productKey: 'confluence.ondemand',
      name: 'Confluence',
      tag: 'chat',
      description: 'Loren ipsum dolor sit amet',
      price: { value: 0, currencyCode: 'usd' },
      action: {
        type: TRY,
        onClick: () => {}
      },
      unitLabel: USER,
      isAnnual: false
    },
    {
      productKey: 'jira.ondemand',
      name: 'Jira',
      tag: 'chat',
      description: 'Loren ipsum dolor sit amet',
      price: { value: 0, currencyCode: 'usd' },
      action: {
        type: TRY,
        onClick: () => {}
      },
      unitLabel: USER,
      isAnnual: false
    }
  ]
};
const Component = props => <ProductList {...BASE_PROPS} {...props} />;

describe('ProductList', () => {
  it('should render', () => {
    const context = getAnalytic().context;
    const wrapper = mount(<Component />, context);
    expect(wrapper.find(ProductCard)).to.have.length(2);
  });

  it('should pass focus product key down to ProductCard', () => {
    const context = getAnalytic().context;
    const props = { ...BASE_PROPS, focusProductKey: 'jira.ondemand' };
    const wrapper = shallow(<ProductList {...props} />, context);
    expect(wrapper.find({ productKey: 'jira.ondemand' })).to.have.prop('focus', true);
  });
});
