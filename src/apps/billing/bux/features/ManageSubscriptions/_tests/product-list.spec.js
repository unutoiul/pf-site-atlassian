import { expect } from 'chai';
import sinon from 'sinon';

import { i18n } from 'bux/helpers/jsLingui';
import proxyquire from 'bux/helpers/proxyquire';

describe('ManageSubscriptions.products', () => {
  // eslint-disable-next-line no-shadow
  let getActions;
  let hasCreditCard;
  let getEntitlementSections;
  let isTrial;
  let isNextBillingPeriodRenewalFrequencyAnnual;
  let manageUsers;
  let changeEdition;
  let addPaymentDetails;
  let isOrganization;
  let sendToMAC;
  let deleteAction;

  beforeEach(() => {
    hasCreditCard = sinon.stub();
    hasCreditCard.returns(false);
    getEntitlementSections = sinon.stub();
    getEntitlementSections.returns({ active: [1, 2] });
    isTrial = sinon.stub();
    isTrial.returns(true);
    isNextBillingPeriodRenewalFrequencyAnnual = sinon.stub().returns(false);
    isOrganization = sinon.stub();
    isOrganization.returns(false);

    deleteAction = proxyquire.load('../actions/delete', {
      'bux/core/state/meta/selectors': { isOrganization },
      'bux/core/selectors/entitlements': { getEntitlementSections }
    });
    const unsubscribe = proxyquire.load('../actions/unsubscribe', {
      'bux/core/state/meta/selectors': { isOrganization },
    });
    sendToMAC = proxyquire.load('../actions/sendToMAC', {
      'bux/core/state/meta/selectors': { isOrganization },
      'bux/core/selectors/entitlements': { getEntitlementSections }
    });
    manageUsers = proxyquire.load('../actions/manageUsers', {
      'bux/core/state/bill-estimate/selectors': { isNextBillingPeriodRenewalFrequencyAnnual },
      'bux/core/state/meta/selectors': { isOrganization },
    });
    changeEdition = proxyquire.load('../actions/changeEdition', {});
    addPaymentDetails = proxyquire.load('../actions/addPaymentDetails', {
      'bux/core/selectors/common/entitlements': { isTrial },
      'bux/core/state/billing-details/selectors': { hasCreditCard },
      'bux/core/state/bill-estimate/selectors': { isNextBillingPeriodRenewalFrequencyAnnual }
    });

    getActions = proxyquire.load('../index', {
      './actions': proxyquire.load('../actions', {
        './delete': deleteAction,
        './sendToMAC': sendToMAC,
        './manageUsers': manageUsers,
        './unsubscribe': unsubscribe,
        './changeEdition': changeEdition,
        './addPaymentDetails': addPaymentDetails
      }),
    }).getActions;
  });

  describe('getActions', () => {
    const testEntitlement = {
      productKey: 'unexisting.product',
      status: 'ACTIVE'
    };

    it('should return default actions for any entitlement', () => {
      const actions = getActions(testEntitlement, {}, i18n);
      expect(actions.map(action => action.label))
        .to.be.deep.equal(['Manage users', 'Delete', 'Get support']);
    });

    it('should return special Stride actions', () => {
      const actions = getActions({ ...testEntitlement, productKey: 'hipchat.cloud' }, {}, i18n);
      expect(actions.map(action => action.label))
        .to.be.deep.equal(['Manage users', 'Unsubscribe', 'Get support']);
    });

    it('should return delete action for active entitlement', () => {
      const actions = getActions(testEntitlement, {}, i18n);
      expect(actions.map(action => action.label)).to.include('Delete');
    });

    it('should not return delete action for inactive entitlement', () => {
      const actions = getActions({
        productKey: 'unexisting.product',
        status: 'INACTIVE'
      }, {}, i18n);
      expect(actions.map(action => action.label)).to.not.include('Delete');
    });

    it('should return sendToMac action for last active entitlement', () => {
      getEntitlementSections.returns({ active: [1, 2] });
      const actions = getActions(testEntitlement, {}, i18n);
      expect(actions.map(action => action.action)).to.not.include(sendToMAC.default.action);
      expect(actions.map(action => action.action)).to.include(deleteAction.default.action);
    });

    it('should return sendToMac action for last active entitlement', () => {
      getEntitlementSections.returns({ active: [1] });
      const actions = getActions(testEntitlement, {}, i18n);
      expect(actions.map(action => action.action)).to.include(sendToMAC.default.action);
      expect(actions.map(action => action.action)).to.not.include(deleteAction.default.action);
    });

    it('should return unsubscribe action for active entitlement in organization', () => {
      isOrganization.returns(true);
      const actions = getActions(testEntitlement, {}, i18n);
      expect(actions.map(action => action.label)).to.include('Unsubscribe');
      expect(actions.map(action => action.label)).to.not.include('Delete');
    });

    describe('Manager users', () => {
      it('should return default main action', () => {
        const actions = getActions(testEntitlement, {}, i18n);
        expect(actions.find(action => action.primaryAction).label).to.be.equal('Manage users');
        expect(actions.find(action => action.primaryAction).action)
          .to.be.equal(manageUsers.manageMonthlyUsers.action);
      });

      it('should return different main action for annual users', () => {
        isNextBillingPeriodRenewalFrequencyAnnual.returns(true);
        const actions = getActions(testEntitlement, {}, i18n);
        expect(actions.find(action => action.primaryAction).label).to.be.equal('Upgrade tier');
        expect(actions.find(action => action.primaryAction).action)
          .to.be.deep.equal(manageUsers.manageAnnualUsers.action);
      });

      it('should return different main action for monthly org products users', () => {
        isOrganization.returns(true);
        const actions = getActions(testEntitlement, {}, i18n);
        expect(actions.find(action => action.primaryAction).label).to.be.equal('Manage users');
        expect(actions.find(action => action.primaryAction).action)
          .to.be.deep.equal(manageUsers.manageMonthlyOrganizationUsers.action);
      });
    });

    describe('Stride', () => {
      const findChangeEdition = action => !action.primaryAction && action.label === 'Change plan';

      it('should return Manage Users when no transitions found', () => {
        const actions = getActions({
          productKey: 'hipchat.cloud',
          status: 'ACTIVE',
          editionTransitions: []
        }, {}, i18n);
        expect(actions.find(action => action.primaryAction).label).to.be.equal('Manage users');
        expect(actions.find(findChangeEdition)).to.be.undefined();
      });

      it('should return change edition when more than one transition exists', () => {
        isTrial.returns(false);
        const actions = getActions({
          productKey: 'hipchat.cloud',
          editionTransitions: ['UPGRADE', 'DOWNGRADE']
        }, {}, i18n);
        expect(actions.find(action => action.primaryAction).label).to.be.equal('Manage users');
        expect(actions.find(findChangeEdition)).not.to.be.undefined();
      });

      it('should return Upgrade edition when only it is possible', () => {
        const actions = getActions({
          productKey: 'hipchat.cloud',
          status: 'INACTIVE',
          editionTransitions: ['UPGRADE']
        }, {}, i18n);
        expect(actions.find(action => action.primaryAction).label).to.be.equal('Upgrade plan');
        expect(actions.find(findChangeEdition)).not.to.be.undefined();
      });

      describe('Add billing details action', () => {
        const getProps = () => ({
          productKey: 'hipchat.cloud',
          status: 'ACTIVE',
          trialEndDate: '2042-12-17',
          editionTransitions: ['DOWNGRADE']
        });

        it('should return CTA if trial, not upgradable, and no billing details', () => {
          const actions = getActions(getProps(), {}, i18n);
          expect(actions.find(action => action.primaryAction).label).to.be.equal('Add billing details');
        });

        it('should not return CTA if upgradable', () => {
          const actions = getActions({
            ...getProps(),
            editionTransitions: ['UPGRADE']
          }, {}, i18n);
          expect(actions.find(action => action.primaryAction).label).not.to.be.equal('Add billing details');
        });

        it('should not return CTA if billing details were provided', () => {
          hasCreditCard.returns(true);
          const actions = getActions(getProps(), {}, i18n);
          expect(actions.find(action => action.primaryAction).label).not.to.be.equal('Add billing details');
        });
      });
    });
  });
});
