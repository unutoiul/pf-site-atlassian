import { ActionType } from './propTypes';

import defaultActions from './actions';

const filterAction = (entitlement, state, action) => !action.condition || action.condition(entitlement, state);

const filterActions = (entitlement, state, actions, i18n) =>
  actions
    .filter(action => filterAction(entitlement, state, action))
    .map((action, index) => ({
      order: 0,
      ...action,
      label: action.label(i18n, entitlement),
      index
    }));

const sortActions = (a, b) => (b.order - a.order) || (b.index - a.index);

const getActions = (entitlement, state, i18n) =>
  filterActions(entitlement, state, defaultActions, i18n)
    .sort(sortActions);

export {
  ActionType,
  getActions
};
