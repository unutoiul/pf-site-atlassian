import configureMockStore from 'redux-mock-store';
import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow } from 'enzyme';
import proxyquire from 'bux/helpers/proxyquire';
import { CURRENT } from 'bux/core/state/bill-estimate/constants';

import billEstimate from 'bux/core/state/bill-estimate';
import { entitlementStoreBuilder } from 'bux/core/state/entitlement-group/reducer';

describe('ManageSubscriptions: ', () => {
  const mockStoreCreator = (billOverride = {}, entitlementGroupOverride = {}) => configureMockStore()({
    billEstimate: { [CURRENT]: { ...billOverride } },
    ...entitlementStoreBuilder(entitlementGroupOverride)
  });

  let ProxyedContainer;
  let getEntitlementSections;
  let isNextBillingPeriodRenewalFrequencyAnnual;
  let getBill;
  let isOrganization;
  let isWithFreeLicense;
  let hasAtlassianAccess;
  let atlassianAccessExtras;
  let getAtlassianAccessTotalUnitCount;
  let upcomingCurrencyExtras;
  let isNextBillInNewCurrency;

  const createProxyquire = () => {
    getEntitlementSections = sinon.stub().returns({});
    getBill = sinon.stub().returns({ type: '', currencyCode: 'USD' });
    isNextBillingPeriodRenewalFrequencyAnnual = sinon.stub().returns(false);
    isOrganization = sinon.stub().returns(false);
    isWithFreeLicense = sinon.stub().returns(false);
    hasAtlassianAccess = sinon.stub().returns(false);
    atlassianAccessExtras = sinon.stub().returns({});
    getAtlassianAccessTotalUnitCount = sinon.stub().returns(10);
    upcomingCurrencyExtras = sinon.stub.returns({});
    isNextBillInNewCurrency = sinon.stub.returns(false);

    ProxyedContainer = proxyquire.noCallThru().load('../ManageSubscriptions', {
      'bux/core/state/bill-estimate/selectors': {
        isNextBillingPeriodRenewalFrequencyAnnual,
        hasAtlassianAccess,
        atlassianAccessExtras,
        getAtlassianAccessTotalUnitCount,
        upcomingCurrencyExtras,
        isNextBillInNewCurrency,
      },
      'bux/core/state/bill-estimate': { selectors: { ...billEstimate.selectors, getBill } },
      'bux/core/selectors/entitlements': { getEntitlementSections },
      'bux/core/state/meta/selectors': { isOrganization, isWithFreeLicense },
      'bux/core/selectors/common': { isWithFreeLicense },
    }).default;
  };

  describe('metadata', () => {
    it('should map normal state to props', () => {
      createProxyquire();
      const mockedStore = mockStoreCreator();
      const wrapper = shallow(<ProxyedContainer store={mockedStore} />);
      expect(wrapper.props().metadata.error).to.be.false();
    });

    it('should map bill error state to props', () => {
      createProxyquire();
      const mockedStore = mockStoreCreator({
        error: { status: 500 }
      });
      const wrapper = shallow(<ProxyedContainer store={mockedStore} />);
      expect(wrapper.props().metadata.error.status).to.equal(500);
    });

    it('should map group error state to props', () => {
      createProxyquire();
      const mockedStore = mockStoreCreator({}, {
        error: { status: 500 }
      });
      const wrapper = shallow(<ProxyedContainer store={mockedStore} />);
      expect(wrapper.props().metadata.error.status).to.equal(500);
    });
  });

  it('should map currency', () => {
    createProxyquire();
    getBill.returns({ currencyCode: 'money' });
    const wrapper = shallow(<ProxyedContainer store={mockStoreCreator()} />);
    expect(wrapper.props().currency).to.be.equal('money');
  });

  it('should map isAnnual', () => {
    createProxyquire();
    const wrapper1 = shallow(<ProxyedContainer store={mockStoreCreator()} />);
    expect(wrapper1.props().isAnnual).to.be.false();
    isNextBillingPeriodRenewalFrequencyAnnual.returns(true);
    const wrapper2 = shallow(<ProxyedContainer store={mockStoreCreator()} />);
    expect(wrapper2.props().isAnnual).to.be.true();
  });

  it('should map allowCancelSite', () => {
    createProxyquire();
    const wrapper1 = shallow(<ProxyedContainer store={mockStoreCreator()} />);
    expect(wrapper1.props().isOrganization).to.be.false();

    isOrganization.returns(true);
    const wrapper2 = shallow(<ProxyedContainer store={mockStoreCreator()} />);
    expect(wrapper2.props().isOrganization).to.be.true();
  });

  describe('should map elements to sections', () => {
    beforeEach(() => {
      createProxyquire();
    });

    it('should map one active entitlement', () => {
      getEntitlementSections.returns({
        active: [{
          id: 1,
          trialEndDate: Date.now() + 100500,
          title: 'B'
        }, {
          id: 2,
          trialEndDate: Date.now() + 100500,
          title: 'A'
        }]
      });
      const wrapper = shallow(<ProxyedContainer store={mockStoreCreator()} />);
      expect(wrapper.props().sections.active.items).to.have.length(2);
      expect(wrapper.props().sections.active.items[0].id).to.be.equal(2);
      expect(wrapper.props().sections.active.items[1].id).to.be.equal(1);
    });
  });
});
