import { getFocusedTask } from 'bux/core/state/focused-task/selectors';
import {
  getEntitlementById,
  getEntitlementEditionMetadata,
  isEntitlementActive
} from 'bux/core/state/entitlement-group/selectors';
import { shouldUseNewChangeEdition } from 'bux/core/state/meta/selectors';
import productsEditionsDetails from 'bux/common/data/products-editions';

export const isFocusedTaskOpen = state => !!getFocusedTask(state);

const productHasEditionsDetails = productKey =>
  (productsEditionsDetails[productKey] ? productsEditionsDetails[productKey].length > 0 : false);

export const isAccessible = entitlementId => (state) => {
  const entitlement = getEntitlementById(entitlementId)(state);
  return !!entitlement
    && productHasEditionsDetails(entitlement.productKey)
    && shouldUseNewChangeEdition(state)
    && isEntitlementActive(entitlementId)(state);
};

export const isLoaded = (state, { entitlement }) => getEntitlementEditionMetadata(entitlement.id)(state);
