import { connect } from 'react-redux';
import { openFocusedTask, closeFocusedTask } from 'bux/core/state/focused-task/actions';
import library from 'bux/features/FocusedTask/library';
import { push as redirect } from 'bux/core/state/router/actions';
import FocusedTaskPage from 'bux/components/FocusedTaskPage';
import { getEntitlementById } from 'bux/core/state/entitlement-group/selectors';
import { isFocusedTaskOpen, isAccessible } from './selectors';
import ChangeEdition from './ChangeEdition';

const mapStateToProps = (state, { match: { params: { entitlementId } } }) => ({
  isFocusedTaskOpen: isFocusedTaskOpen(state),
  isAccessible: isAccessible(entitlementId)(state),
  entitlement: getEntitlementById(entitlementId)(state)
});

const component = library.add(ChangeEdition);

const mapDispatchToProps = dispatch => ({
  open: (props) => {
    dispatch(openFocusedTask({
      component,
      props,
      logPrefix: 'manageSubscriptions.ChangeEdition'
    }));
  },
  exit: () => {
    dispatch(closeFocusedTask());
    dispatch(redirect('/applications'));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FocusedTaskPage);
