import { storiesOf } from '@storybook/react';

import { inStepFlowChrome, render, withServicePreLoader } from 'bux/common/helpers/storybook';

import StepOverview from './StepOverview';

storiesOf('BUX|Focused tasks/Change edition/Step 2: Overview', module)
  .add('Default', () => inStepFlowChrome(withServicePreLoader(render(StepOverview, {
    stepSelectEditionData: {
      edition: {
        edition: 'premium',
        name: 'Premium',
      },
      entitlement: {
        id: 'b7db7827-e2ac-492e-a3bb-e1c310764043',
        name: 'Stride',
      },
    },
    isFirstStep: false,
  }))));
