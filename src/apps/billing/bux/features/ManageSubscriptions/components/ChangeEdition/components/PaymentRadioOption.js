import React from 'react';
import { func, bool, string } from 'prop-types';
import { AkRadio } from '@atlaskit/field-radio-group';
import styles from '../ChangeEdition.less';

const PaymentRadioOption = ({
  isSelected, value, label, description, onChangePaymentOption
}) => (
  <div className={styles.paymentOption} data-test={`option-${value}`}>
    <AkRadio
      isSelected={isSelected}
      value={value}
      onChange={onChangePaymentOption}
    >
      {label}
    </AkRadio>
    { description && <p>{description}</p> }
  </div>
);
PaymentRadioOption.propTypes = {
  isSelected: bool.isRequired,
  value: string.isRequired,
  label: string.isRequired,
  onChangePaymentOption: func.isRequired,
  description: string,
};

export default PaymentRadioOption;
