import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import { AkRadio } from '@atlaskit/field-radio-group';
import PaymentRadioOption from '../PaymentRadioOption';

describe('PaymentRadioOption', () => {
  const props = {
    isSelected: false,
    value: 'test',
    label: 'test',
    onChangePaymentOption: () => {}
  };

  it('should render', () => {
    const wrapper = shallow(<PaymentRadioOption {...props} />);
    expect(wrapper).to.be.present();
    expect(wrapper.find(AkRadio)).to.be.present();
    expect(wrapper.find(AkRadio)).to.contain.props({
      isSelected: props.isSelected,
      value: props.value,
      onChange: props.onChangePaymentOption,
    });
    expect(wrapper.find('p')).to.not.be.present();
  });

  it('should render with description', () => {
    const description = 'test';
    const wrapper = shallow(<PaymentRadioOption {...props} description={description} />);
    expect(wrapper.find('p')).to.be.present().and.have.text(description);
  });
});
