import { expect } from 'chai';
import proxyquire from 'bux/helpers/proxyquire';
import { isFocusedTaskOpen } from '../selectors';

describe('ChangeEdition selectors: ', () => {
  describe('isFocusedTaskOpen', () => {
    it('should return false for closed focused task', () => {
      const state = { focusedTask: {} };
      expect(isFocusedTaskOpen(state)).to.be.false();
    });
    it('should return true for opened focused task', () => {
      const state = { focusedTask: { focusedTask: 'not empty string' } };
      expect(isFocusedTaskOpen(state)).to.be.true();
    });
  });

  describe('isAccessible', () => {
    const setup = ({
      entitlementId, ff, entitlement, editions
    }) => {
      const state = {
        metadata: {
          featureFlags: {
            data: {
              'new-change-edition-flow': ff
            }
          }
        },
        entitlementGroup: {
          entitlementGroup: {
            data: {
              entitlements: [entitlement]
            }
          }
        }
      };
      const isAccessible = proxyquire.noCallThru().load('../selectors', {
        'bux/common/data/products-editions': editions
      }).isAccessible;
      return isAccessible(entitlementId)(state);
    };

    const scenario = {
      entitlementId: '123',
      ff: true,
      entitlement: { productKey: 'jira', id: '123', status: 'ACTIVE' },
      editions: { jira: [1] }
    };

    it('should return true', () => {
      expect(setup(scenario)).to.be.true();
    });
    it('should not be accessible when product is inactive', () => {
      expect(setup({ ...scenario, entitlement: { ...scenario.entitlement, status: 'INACTIVE' } })).to.be.false();
    });
    it('should not be accessible when product is not owned', () => {
      expect(setup({ ...scenario, entitlement: { ...scenario.entitlement, id: '1234', status: 'ACTIVE' } }))
        .to.be.false();
    });
    it('should not be accessible when FF is disabled', () => {
      expect(setup({ ...scenario, ff: false })).to.be.false();
    });
    it('should not be accessible when product has no editions', () => {
      expect(setup({ ...scenario, editions: {} })).to.be.false();
    });
  });
});
