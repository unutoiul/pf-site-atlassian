import { getEntitlementGroup, getEntitlementEdition } from 'bux/core/state/entitlement-group/actions';
import { showNotificationByTemplate } from 'bux/core/state/notifications/actions';
import { getBillEstimate } from 'bux/core/state/bill-estimate/actions';
import { selectEdition } from 'bux/core/state/api';

export const mountAction = ({ entitlement }) =>
  getEntitlementEdition(entitlement.id);

export const changeEdition = ({ edition: { edition, name }, entitlement }) => dispatch =>
  // nothing to do. Mock with expected behavior
  dispatch(selectEdition({ entitlementId: entitlement.id, edition }))
    .then(() => {
      dispatch(showNotificationByTemplate('change-edition-success', {
        entitlementName: entitlement.name, editionName: name
      }));
    })
    .catch((e) => {
      dispatch(showNotificationByTemplate('change-edition-error', {
        entitlementName: entitlement.name, editionName: name
      }));
      throw e;
    });

export const refreshStoreAfterChangeEdition = () => (dispatch) => {
  dispatch(getEntitlementGroup());
  dispatch(getBillEstimate());
};
