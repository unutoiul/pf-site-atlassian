import React from 'react';
import { string, number } from 'prop-types';
import Money from 'bux/components/Money';
import { PAYMENT_OPTION_CREDIT_CARD } from '../contants';
import styles from '../ChangeEdition.less';

const AdviseText = ({ paymentOption, totalCost, currencyCode }) => {
  const TotalCost = <Money amount={totalCost} currency={currencyCode} />;
  return (
    <p className={styles.flowAdviseTextSecondary} data-test="payment-option-advise">
      { paymentOption === PAYMENT_OPTION_CREDIT_CARD
      ? <span>
          You will be redirected to my.atlassian.com to provide your credit card information. Once it is processed,
          you will be charged {TotalCost} and your edition upgraded.
        </span>
      : <span>
          A PDF of the quote will be downloaded for your records, and your upgrade status will be pending until we
          have received the payment.
        </span>
      }
    </p>
  );
};
AdviseText.propTypes = {
  paymentOption: string.isRequired,
  totalCost: number.isRequired,
  currencyCode: string.isRequired,
};

export default AdviseText;
