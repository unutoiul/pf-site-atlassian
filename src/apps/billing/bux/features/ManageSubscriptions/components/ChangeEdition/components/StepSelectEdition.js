import React from 'react';
import { ChangeEditionWizard } from 'bux/features/ManageSubscriptions/actions/components/ChangeEdition/ChangeEdition';
import { connect } from 'react-redux';
import {
  changeEdition,
  refreshStoreAfterChangeEdition
} from 'bux/features/ManageSubscriptions/components/ChangeEdition/actions';
import PropTypes from 'prop-types';
import { isNextBillingPeriodRenewalFrequencyAnnual } from 'bux/core/state/bill-estimate/selectors';
import { hasPaymentMethod } from 'bux/core/state/billing-details/selectors';
import { getEntitlementEdition } from 'bux/core/state/entitlement-group/selectors';
import extendEditions
  from 'bux/features/ManageSubscriptions/actions/components/ChangeEdition/editions/extended-editions';
import { push as redirectAction } from 'bux/core/state/router/actions';

export class StepSelectEditionComponent extends React.PureComponent {
  static propTypes = {
    edition: PropTypes.object.isRequired,
    entitlement: PropTypes.object.isRequired,
    closeAction: PropTypes.func.isRequired,
    changeEdition: PropTypes.func.isRequired,
    refreshStoreAfterChangeEdition: PropTypes.func.isRequired,
    hasPaymentDetails: PropTypes.bool.isRequired,
    isAnnual: PropTypes.bool.isRequired,
    className: PropTypes.string,
    push: PropTypes.func.isRequired,
    next: PropTypes.func.isRequired,
    redirect: PropTypes.func.isRequired,
  };

  upgrade = ({ edition }) => {
    const {
      next, push, entitlement
    } = this.props;
    if (edition && edition.transition === 'UPGRADE') {
      push({ stepSelectEditionData: { edition, entitlement } });
      next();
      return false;
    }
    return true;
  };

  render() {
    const {
      className, edition, isAnnual, redirect, ...rest
    } = this.props;
    return (
      <div className={className}>
        <div>
          <ChangeEditionWizard
            {...rest}
            renewalFrequency={isAnnual ? 'ANNUAL' : 'MONTHLY'}
            editions={extendEditions(edition)}
            upgradeWithNewFlow={this.upgrade}
            redirect={redirect}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, { entitlement }) => ({
  edition: getEntitlementEdition(entitlement.id)(state),
  hasPaymentDetails: hasPaymentMethod(state),
  isAnnual: isNextBillingPeriodRenewalFrequencyAnnual(state),
});

const mapDispatchToProps = {
  changeEdition,
  refreshStoreAfterChangeEdition,
  redirect: redirectAction
};

const StepSelectEdition = connect(
  mapStateToProps,
  mapDispatchToProps
)(StepSelectEditionComponent);

export default StepSelectEdition;
