import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import ChangeEdition from '../ChangeEdition';

describe('ChangeEdition', () => {
  const props = {
    entitlement: { id: '123' },
    closeAction: () => {},
    redirect: () => {},
  };

  it('should render component ChangeEdition', () => {
    const wrapper = shallow(<ChangeEdition {...props} />);
    expect(wrapper).to.be.present();
    expect(wrapper.find('Connect(WithLoader)')).to.be.present();
  });
});
