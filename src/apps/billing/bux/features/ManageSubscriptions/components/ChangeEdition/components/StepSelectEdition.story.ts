import { storiesOf } from '@storybook/react';

import { inPageChrome, mockedFunction, render, withServicePreLoader } from 'bux/common/helpers/storybook';
import Downgrade from 'bux/features/ManageSubscriptions/actions/components/ChangeEdition/steps/Downgrade';

import StepSelectEdition from './StepSelectEdition';

const entitlement = {
  accountId: '363bf5a5-47d7-42f9-9c51-930c40d6b3b9',
  creationDate: '2017-05-03',
  editionTransitions: ['UPGRADE'],
  endDate: '2017-06-02',
  entitlementGroupId: '4d854acf-e651-4a4b-bdd0-d0c76f0995ed',
  futureEdition: 'free',
  futureEditionTransition: 'DOWNGRADE',
  id: 'b7db7827-e2ac-492e-a3bb-e1c310764043',
  name: 'Stride',
  productKey: 'hipchat.cloud',
  selectedEdition: 'standard',
  sen: 'A929349',
  startDate: '2016-12-05',
  status: 'ACTIVE',
  trialEndDate: '2016-12-17',
};

const edition = {
  cost: { monthly: 0, annual: 0, currency: 'usd' },
  edition: 'free',
  features: [
    'Unlimited users',
    'Unlimited group chat rooms',
    'Unlimited direct messaging',
    'Unlimited group video meetings',
    'Unlimited voice meetings',
    'Built-in collaboration tools',
  ],
  isCurrent: false,
  name: 'Free',
  needsPaymentDetailsOnAccount: false,
  transition: 'DOWNGRADE',
};

const props = {
  entitlement,
  closeAction: mockedFunction,
  push: mockedFunction,
  next: mockedFunction,
};

const downgradeProps = {
  entitlement,
  edition,
  activeEdition: edition,
  next: mockedFunction,
  prev: mockedFunction,
  allOpen: true,
};

storiesOf('BUX|Focused tasks/Change edition/Step 1: Select edition', module)
  .add('Default', () => inPageChrome(withServicePreLoader(render(StepSelectEdition, props))))
  .add('Compare plans', () => inPageChrome(withServicePreLoader(render(StepSelectEdition, { ...props, showingComparisonTable: true }))))
  .add('Downgrade', () => inPageChrome(withServicePreLoader(render(Downgrade, downgradeProps))));
