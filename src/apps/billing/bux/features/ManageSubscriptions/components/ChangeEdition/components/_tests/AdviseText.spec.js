import React from 'react';
import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import { PAYMENT_OPTION_CREDIT_CARD, PAYMENT_OPTION_QUOTE } from '../../contants';
import AdviseText from '../AdviseText';

describe('AdviseText', () => {
  it('should render credit card advise', () => {
    const wrapper = mount(<AdviseText paymentOption={PAYMENT_OPTION_CREDIT_CARD} totalCost={1} currencyCode="usd" />);
    expect(wrapper).to.be.present();
    expect(wrapper.find('p')).to.contain.text('You will be redirected to my.atlassian.com');
    expect(wrapper.find('p').find('Money')).to.be.present();
  });
  it('should render quote advise', () => {
    const wrapper = shallow(<AdviseText paymentOption={PAYMENT_OPTION_QUOTE} totalCost={1} currencyCode="usd" />);
    expect(wrapper.find('p')).to.contain.text('A PDF of the quote will be downloaded for your records');
  });
});
