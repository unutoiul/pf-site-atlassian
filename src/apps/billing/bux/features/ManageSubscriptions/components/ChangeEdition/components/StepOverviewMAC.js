import React from 'react';
import { func, object, bool, string, number } from 'prop-types';
import { connect } from 'react-redux';
import { bindPromiseCreators } from 'redux-saga-routines';
import Button, { SubmitButton } from 'bux/components/Button';
import LogScope from 'bux/components/log/Scope/index';
import Loading from 'bux/components/Loading/delayed';
import { ChangeEditionBillEstimateDetail } from 'bux/features/bill-estimate/components';
import { getTenantInfo } from 'bux/core/state/meta/selectors';
import { getEditionChangeEstimate, createQuoteForEstimatePromise } from 'bux/core/state/bill-estimate/actions';
import {
  getChangeEditionEstimateMetadata,
  getChangeEditionEstimateId,
  billFactory,
  getChangeEditionBill
} from 'bux/core/state/bill-estimate/selectors';
import { getBillingDetails } from 'bux/core/state/billing-details/selectors';
import { constructFullAddress, getAddressOrUserLocation } from 'bux/common/helpers/address';
import { Section, Footer, StepUnavailableError } from 'bux/components/Wizard';
import { PayingForSection } from 'bux/components/PaymentWizard/PayingForSection';
import { redirect } from 'bux/common/helpers/browser';
import Link from 'bux/components/Link';
import { PAYMENT_OPTION_CREDIT_CARD, PAYMENT_OPTION_QUOTE } from '../contants';
import AdviseText from './AdviseText';
import PaymentRadioOption from './PaymentRadioOption';
import styles from './styles.less';

class StepOverviewMAC extends React.PureComponent {
  static propTypes = {
    next: func.isRequired,
    back: func.isRequired,
    stepSelectEditionData: object.isRequired,
    createQuote: func.isRequired,
    getEstimate: func.isRequired,
    isLoading: bool.isRequired,
    hasError: bool.isRequired,
    estimateId: string,
    totalCost: number.isRequired,
    currencyCode: string.isRequired,
    tenantInfo: object.isRequired,
    nextBillingPeriod: object.isRequired,
    billingAddress: object.isRequired,
  };

  state = { submitting: false, paymentOption: PAYMENT_OPTION_CREDIT_CARD };

  componentDidMount() {
    this.loadEstimate();
  }

  onSubmit = () =>
    this.setSubmittingStateOn()
      .then(() => ({ estimateId: this.props.estimateId }))
      .then(this.props.createQuote)
      .then(this.handleCreatedQuote)
      .then(this.props.next)
      .catch(this.setSubmittingStateOff);

  onChangePaymentOption = (event) => {
    this.setState({ paymentOption: event.target.value });
  };

  setSubmittingStateOn = () => new Promise(resolve => this.setState({ submitting: true }, resolve));

  setSubmittingStateOff = () => this.setState({ submitting: false });

  loadEstimate = () => {
    const { stepSelectEditionData: { edition: { edition }, entitlement: { id } } } = this.props;
    this.props.getEstimate({ entitlementId: id, edition });
  };

  handleCreatedQuote = ({ paymentLink, quoteLink }) => {
    redirect(this.state.paymentOption === PAYMENT_OPTION_CREDIT_CARD ? paymentLink : quoteLink);
  };

  render() {
    const {
      back,
      isLoading,
      hasError,
      totalCost,
      currencyCode,
      tenantInfo,
      nextBillingPeriod,
      billingAddress,
    } = this.props;
    const { submitting, paymentOption } = this.state;

    const { organisationName, addressLineFull, country } = billingAddress;

    const hasAddress = organisationName && addressLineFull && country;

    if (isLoading) {
      return (<Loading />);
    }

    if (hasError) {
      return (
        <StepUnavailableError message="Sorry, we can't estimate right now" onRetry={this.loadEstimate} />
      );
    }

    return (
      <LogScope prefix="StepOverview">
        <div data-test="stepOverview">
          <h2>Confirm your billing details</h2>
          {hasAddress &&
            <Section title="Billing address:">
              <p className={styles.address} data-test="address">
                {`${organisationName}, ${addressLineFull}, ${country}`}
              </p>
              <div className={styles.editText}>
                If you need to update your billing address, go back to&nbsp;
                <Link to="/paymentdetails" name="edit-payment-details">Billing details</Link>.
              </div>
            </Section>
          }
          <Section title="Select your payment option:">
            <PaymentRadioOption
              isSelected={paymentOption === PAYMENT_OPTION_CREDIT_CARD}
              value={PAYMENT_OPTION_CREDIT_CARD}
              label="Pay now with a credit/debit card"
              onChangePaymentOption={this.onChangePaymentOption}
            />
            <PaymentRadioOption
              isSelected={paymentOption === PAYMENT_OPTION_QUOTE}
              value={PAYMENT_OPTION_QUOTE}
              label="Get a quote now and pay later"
              onChangePaymentOption={this.onChangePaymentOption}
              description="Your no-obligation quote includes payment instructions for paying by a bank transfer,
                check or credit/debit card."
            />
          </Section>
          <PayingForSection
            className="test-user-paying-for"
            name={tenantInfo.name}
            isOrg={tenantInfo.isOrg}
            startDate={nextBillingPeriod.startDate}
            endDate={nextBillingPeriod.endDate}
          />
          <Section>
            <ChangeEditionBillEstimateDetail style="compact" excludeTax />
          </Section>
          <Footer>
            <Button name="back" className="back-flow" onClick={back} disabled={submitting} subtle>Back</Button>
            <SubmitButton
              name={paymentOption === PAYMENT_OPTION_CREDIT_CARD ? 'add-card' : 'download-quote'}
              onClick={this.onSubmit}
              disabled={submitting}
              spinning={submitting}
            >
              { paymentOption === PAYMENT_OPTION_CREDIT_CARD ? 'Add card information' : 'Download quote' }
            </SubmitButton>
          </Footer>
          <AdviseText paymentOption={paymentOption} totalCost={totalCost} currencyCode={currencyCode} />
        </div>
      </LogScope>
    );
  }
}

const changeEditionEstimate = billFactory(getChangeEditionBill);

const mapStateToProps = state => ({
  isLoading: getChangeEditionEstimateMetadata(state).loading,
  hasError: getChangeEditionEstimateMetadata(state).error,
  estimateId: getChangeEditionEstimateId(state),
  totalCost: changeEditionEstimate.getTotalCost(state, true),
  currencyCode: changeEditionEstimate.getBill(state).currencyCode,
  nextBillingPeriod: changeEditionEstimate.getBill(state).nextBillingPeriod,
  tenantInfo: getTenantInfo(state),
  billingAddress: {
    organisationName: getBillingDetails(state).organisationName,
    ...getAddressOrUserLocation(state)(getBillingDetails),
    addressLineFull: constructFullAddress(state)(getBillingDetails),
  },
});

const mapDispatchToProps = dispatch => ({
  ...bindPromiseCreators({
    createQuote: createQuoteForEstimatePromise,
  }, dispatch),
  getEstimate: payload => dispatch(getEditionChangeEstimate(payload)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StepOverviewMAC);
