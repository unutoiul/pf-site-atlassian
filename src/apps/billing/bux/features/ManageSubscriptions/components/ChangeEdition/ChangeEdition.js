import { Trans } from '@lingui/react';
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { isNextBillingPeriodRenewalFrequencyAnnual } from 'bux/core/state/bill-estimate/selectors';
import { hasPaymentMethod } from 'bux/core/state/billing-details/selectors';
import { Flow, Step } from 'bux/components/flow';
import AutoFocus from 'bux/components/behaviour/focus/AutoFocus';
import ScopedState, { connectScopedState } from 'bux/components/ScopedState';
import { withPageLoader } from 'bux/components/withLoader';
import withPageLevelLog from 'bux/components/log/with/pageLevel';
import StepBillingAddress from 'bux/features/payment-details/components/AddPaymentDetails/StepBillingAddress';
import StepBillingMethod from 'bux/features/payment-details/components/AddPaymentDetails/StepBillingMethod';
import StepOverviewMAC from './components/StepOverviewMAC';
import StepOverview from './components/StepOverview';
import StepSelectEdition from './components/StepSelectEdition';
import { mountAction } from './actions';
import { isLoaded } from './selectors';
import styles from './ChangeEdition.less';

const ScopedStateStepSelectEdition = connectScopedState(StepSelectEdition);
const ScopedStateStepBillingAddress = connectScopedState(StepBillingAddress);
const ScopedStateStepBillingMethod = connectScopedState(StepBillingMethod);
const ScopedStateStepOverviewMAC = connectScopedState(StepOverviewMAC);
const ScopedStateStepOverview = connectScopedState(StepOverview);

export class ChangeEditionImpl extends React.PureComponent {
  static propTypes = {
    entitlement: PropTypes.object.isRequired,
    closeAction: PropTypes.func.isRequired,
    isAnnual: PropTypes.bool.isRequired,
    hasPaymentMethodSet: PropTypes.bool.isRequired,
  };

  shouldFillBillingAddress = () => !this.props.isAnnual && !this.props.hasPaymentMethodSet;
  shouldFillPaymentMethod = () => !this.props.isAnnual && !this.props.hasPaymentMethodSet;
  shouldFinalizeOnMAC = () => this.props.isAnnual;
  shouldFinalizeOnBUX = () => !this.props.isAnnual;

  render() {
    const { entitlement, closeAction } = this.props;
    return (
      <AutoFocus>
        <div data-test="change-edition-flow">
          <ScopedState>
            <Flow
              width="narrow"
              onCancel={closeAction}
              onComplete={closeAction}
            >
              <Step
                title={<Trans id="billing.change-edition.flow.plan-selection.title">Plan selection</Trans>}
                Component={ScopedStateStepSelectEdition}
                className={styles.planSelection}
                entitlement={entitlement}
                closeAction={closeAction}
              />
              <Step
                title={<Trans id="billing.change-edition.flow.billing-address.title">Billing address</Trans>}
                Component={ScopedStateStepBillingAddress}
                condition={this.shouldFillBillingAddress}
              />
              <Step
                title={<Trans id="billing.change-edition.flow.payment-method.title">Payment method</Trans>}
                Component={ScopedStateStepBillingMethod}
                condition={this.shouldFillPaymentMethod}
              />
              <Step
                title={<Trans id="billing.change-edition.flow.payment-option.title">Payment option</Trans>}
                Component={ScopedStateStepOverviewMAC}
                condition={this.shouldFinalizeOnMAC}
              />
              <Step
                title={<Trans id="billing.change-edition.flow.confirmation.title">Confirmation</Trans>}
                Component={ScopedStateStepOverview}
                condition={this.shouldFinalizeOnBUX}
              />
            </Flow>
          </ScopedState>
        </div>
      </AutoFocus>
    );
  }
}

const mapStateToProps = state => ({
  isAnnual: isNextBillingPeriodRenewalFrequencyAnnual(state),
  hasPaymentMethodSet: hasPaymentMethod(state),
});

const ConnectedChangeEdition = connect(mapStateToProps)(ChangeEditionImpl);

const ChangeEdition = withPageLoader(isLoaded, mountAction)(withPageLevelLog(ConnectedChangeEdition));

export default ChangeEdition;
