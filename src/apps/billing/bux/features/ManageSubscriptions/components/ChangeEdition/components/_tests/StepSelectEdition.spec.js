import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import proxyquire from 'bux/helpers/proxyquire';
import { Provider } from 'bux/helpers/redux';

describe('StepSelectEdition', () => {
  let StepSelectEdition;
  let StepSelectEditionComponent;
  let isNextBillingPeriodRenewalFrequencyAnnual;
  let hasPaymentMethod;
  let getEntitlementEdition;
  let changeEdition;
  let refreshStoreAfterChangeEdition;
  let extendEditions;

  beforeEach(() => {
    isNextBillingPeriodRenewalFrequencyAnnual = sinon.stub().returns(false);
    hasPaymentMethod = sinon.stub().returns(true);
    getEntitlementEdition = sinon.stub().returns(() => ({}));
    changeEdition = sinon.stub();
    refreshStoreAfterChangeEdition = sinon.stub();
    extendEditions = sinon.stub().returns([{
      name: '',
      features: [],
      edition: '',
      cost: {
        amount: 0,
        currency: ''
      },
      transition: 'UPGRADE',
      needsPaymentDetailsOnAccount: false
    }]);

    ({ default: StepSelectEdition, StepSelectEditionComponent } = proxyquire.noCallThru().load('../StepSelectEdition', {
      'bux/features/ManageSubscriptions/actions/components/ChangeEdition/editions/extended-editions': extendEditions,
      'bux/core/state/bill-estimate/selectors': { isNextBillingPeriodRenewalFrequencyAnnual },
      'bux/core/state/billing-details/selectors': { hasPaymentMethod },
      'bux/core/state/entitlement-group/selectors': { getEntitlementEdition },
      'bux/features/ManageSubscriptions/components/ChangeEdition/actions': {
        changeEdition,
        refreshStoreAfterChangeEdition
      }
    }));
  });

  describe('StepSelectEdition', () => {
    it('should render component StepSelectEdition', () => {
      const props = {
        entitlement: { id: '' },
        closeAction: () => {
        }
      };

      const wrapper = shallow(<Provider><StepSelectEdition {...props} /></Provider>);
      expect(wrapper).to.be.present();
    });
  });

  describe('StepSelectEditionComponent', () => {
    const props = {
      entitlement: { id: '123' },
      edition: { id: '123' },
      hasPaymentDetails: true,
      isAnnual: false,
      closeAction: () => {},
      changeEdition: () => {},
      refreshStoreAfterChangeEdition: () => {},
      push: sinon.stub(),
      next: sinon.stub(),
      redirect: () => {}
    };

    it('should render component StepSelectEditionComponent', () => {
      const wrapper = shallow(<StepSelectEditionComponent {...props} />);
      expect(wrapper).to.be.present();
      expect(wrapper.find('ChangeEditionWizard'))
        .to.be.present()
        .and.have.prop('editions', extendEditions());
    });

    describe('upgrade method', () => {
      it('should return false', () => {
        const edition = { transition: 'UPGRADE' };
        const wrapper = shallow(<StepSelectEditionComponent {...props} />);
        expect(wrapper.instance().upgrade({ edition })).to.be.false();
        expect(props.push).to.be.calledWith({ stepSelectEditionData: { edition, entitlement: { id: '123' } } });
        expect(props.next).to.be.called();
      });
    });
  });
});
