import { Trans } from '@lingui/react';
import * as React from 'react';
import { connect } from 'react-redux';
import { bindPromiseCreators } from 'redux-saga-routines';

import { constructFullAddress, getAddressOrUserLocation } from 'bux/common/helpers/address';
import Link from 'bux/components/Link/Link';
import { Billing } from 'bux/components/PaymentWizard';
import { getEditionChangeEstimate } from 'bux/core/state/bill-estimate/actions';
import { getBill, getChangeEditionEstimateMetadata } from 'bux/core/state/bill-estimate/selectors';
import { updateBillingDetailsPromise } from 'bux/core/state/billing-details/actions';
import { PAYMENT_METHOD_CREDIT_CARD, PAYMENT_METHOD_PAYPAL } from 'bux/core/state/billing-details/constants';
import {
  getBillingDetails,
  getInvoiceContactEmail,
  getPaymentMethod,
  getPaymentMethodDetails,
} from 'bux/core/state/billing-details/selectors';
import { getTenantInfo } from 'bux/core/state/meta/selectors';
import { ChangeEditionBillEstimateDetail } from 'bux/features/bill-estimate/components';
import {
  changeEdition,
  refreshStoreAfterChangeEdition,
} from 'bux/features/ManageSubscriptions/components/ChangeEdition/actions';

interface StepOverviewProps {
  stepSelectEditionData: any;
  stepBillingAddressData?: any;
  stepBillingMethodData?: any;
  currentBillingMethod?: any;
  currentBillingAddress?: any;
  isFirstStep: boolean;
  billEstimate: any;
  tenantInfo: any;
  totalCost: number;
  isLoading: boolean;
  hasLoadingError?: boolean;
  back(): void;
  next(): void;
  getEstimate(payload: any): void;
  savePaymentDetails(data: any): Promise<any>;
  changeEdition(data: any): Promise<any>;
  refreshStoreAfterChangeEdition(): Promise<any>;
}

class StepOverview extends React.PureComponent<StepOverviewProps> {
  public state = {
    isSubmitting: false,
    isAgreed: false,
  };

  public onAgreedChanged = (agreed: boolean) => {
    this.setState({ isAgreed: agreed });
  };

  public componentDidMount() {
    this.loadEstimate();
  }

  public loadEstimate = () => {
    const { stepSelectEditionData: { edition: { edition }, entitlement: { id } } } = this.props;
    this.props.getEstimate({ entitlementId: id, edition });
  };

  public handleSubmit = async (): Promise<any> => {
    const { stepSelectEditionData: { edition, entitlement } } = this.props;
    this.setState({ isSubmitting: true });

    return this.savePaymentDetails()
      .then(() => ({ edition, entitlement }))
      .then(this.props.changeEdition)
      .then(this.props.refreshStoreAfterChangeEdition)
      .then(this.props.next);
  };

  public savePaymentDetails = () => {
    const { stepBillingMethodData, stepBillingAddressData } = this.props;

    if (!stepBillingAddressData) {
      return Promise.resolve();
    }

    const { method, paymentDetails } = stepBillingMethodData;

    return this.props.savePaymentDetails({
      ...stepBillingAddressData,
      paymentMethod: method,
      creditCard: method === PAYMENT_METHOD_CREDIT_CARD ? paymentDetails : undefined,
      paypalAccount: method === PAYMENT_METHOD_PAYPAL ? paymentDetails : undefined,
      invoiceContactEmail: undefined,
    });
  };

  public getBillingMethodProp = () => {
    const { stepBillingMethodData, currentBillingMethod } = this.props;
    if (stepBillingMethodData) {
      return stepBillingMethodData;
    }

    return currentBillingMethod;
  };

  public getBillingAddressProp = () => {
    const { stepBillingAddressData, currentBillingAddress } = this.props;
    if (stepBillingAddressData) {
      return stepBillingAddressData;
    }

    return currentBillingAddress;
  };

  public render() {
    const { isSubmitting, isAgreed } = this.state;
    const {
      back, billEstimate, isFirstStep, isLoading, hasLoadingError, stepSelectEditionData, stepBillingMethodData,
      tenantInfo,
    } = this.props;

    const noBillingDetailsOnFile = !!stepBillingMethodData;
    const { nextBillingPeriod } = billEstimate;
    const { edition: { name: editionName }, entitlement: { name: productName } } = stepSelectEditionData;

    const { organisationName, country, addressLineFull } = this.getBillingAddressProp();
    const { method, paymentDetails } = this.getBillingMethodProp();

    return (
      <Billing.Overview
        title={<Trans id="billing.change-edition.overview.title">Confirm and upgrade</Trans>}
        logPrefix="StepOverview"
        dataTest="stepOverview"
        isLoading={isLoading}
        hasLoadingError={hasLoadingError}
        loadingErrorMsg={<Trans id="billing.change-edition.overview.estimate-error">Sorry, we can't estimate right now</Trans>}
        loadingErrorRetry={this.loadEstimate}
      >
        <Billing.Address
          organisationName={organisationName}
          addressLineFull={addressLineFull}
          country={country}
        />
        <Billing.PaymentMethod method={method} details={paymentDetails} />
        <Billing.Text className="test-edit-payment-method">
          <Trans id="billing.change-edition.overview.update-billing-details">
            To update payment details, go to <Link to="/paymentdetails">Billing details</Link>.
          </Trans>
        </Billing.Text>
        <Billing.Period
          title={<Trans id="billing.change-edition.overview.period.title">Billing period</Trans>}
          startDate={nextBillingPeriod.startDate}
          endDate={nextBillingPeriod.endDate}
        />
        <Billing.Estimate
          title={<Trans id="billing.change-edition.overview.summary.title">Billing summary</Trans>}
          isOrg={tenantInfo.isOrg}
          name={tenantInfo.name}
        >
          <ChangeEditionBillEstimateDetail style="compact" excludeTax={noBillingDetailsOnFile} />
        </Billing.Estimate>
        <Billing.Text className="test-charge-advise">
          <Trans id="billing.change-edition.overview.immediately-charged">
            You will be immediately charged and upgraded to {productName} {editionName}.
          </Trans>
        </Billing.Text>
        {noBillingDetailsOnFile &&
        <Billing.Agreement
          isBusy={isSubmitting}
          onAgreedChanged={this.onAgreedChanged}
        />
        }
        <Billing.Controls
          submitLabel={<Trans id="billing.change-edition.overview.button.upgrade">Upgrade</Trans>}
          submitName="upgrade"
          onSubmit={this.handleSubmit}
          onCancel={back}
          isFirstStep={isFirstStep}
          isBusy={isSubmitting}
          isSubmitDisabled={noBillingDetailsOnFile && !isAgreed}
        />
      </Billing.Overview>
    );
  }
}

const mapStateToProps = state => ({
  isLoading: getChangeEditionEstimateMetadata(state).loading,
  hasLoadingError: getChangeEditionEstimateMetadata(state).error,
  billEstimate: getBill(state),
  tenantInfo: getTenantInfo(state),
  currentBillingMethod: {
    method: getPaymentMethod(state),
    paymentDetails: getPaymentMethodDetails(state),
  },
  currentBillingAddress: {
    organisationName: getBillingDetails(state).organisationName,
    taxpayerId: getBillingDetails(state).taxpayerId,
    invoiceContactEmail: getInvoiceContactEmail(state),
    ...getAddressOrUserLocation(state)(getBillingDetails),
    addressLineFull: constructFullAddress(state)(getBillingDetails),
    originalAddressLineFull: constructFullAddress(state)(getBillingDetails),
  },
});

const mapDispatchToProps = dispatch => ({
  ...bindPromiseCreators({
    savePaymentDetails: updateBillingDetailsPromise,
  }, dispatch),
  getEstimate: payload => dispatch(getEditionChangeEstimate(payload)),
  changeEdition: payload => dispatch(changeEdition(payload)),
  refreshStoreAfterChangeEdition: () => dispatch(refreshStoreAfterChangeEdition()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(StepOverview);
