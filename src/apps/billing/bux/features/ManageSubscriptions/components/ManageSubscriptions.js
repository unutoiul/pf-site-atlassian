import { connect } from 'react-redux';
import {
  isNextBillingPeriodRenewalFrequencyAnnual,
  hasAtlassianAccess,
  atlassianAccessExtras,
  getAtlassianAccessTotalUnitCount,
  isNextBillInNewCurrency as isAfterTheCutOverDate, 
  upcomingCurrencyExtras
} from 'bux/core/state/bill-estimate/selectors';
import ManageSubscriptions from 'bux/components/ManageSubscriptions';
import billEstimate from 'bux/core/state/bill-estimate';
import entitlementGroup from 'bux/core/state/entitlement-group';
import { getEntitlementSections } from 'bux/core/selectors/entitlements';
import { combineAllDisplayable } from 'bux/core/selectors/metadata';
import { isOrganization } from 'bux/core/state/meta/selectors';
import productsEditions from 'bux/common/data/products-editions';
import ConfirmCancel from 'bux/components/ManageSubscriptions/sections/components/ConfirmCancel';
import { openModalDialog } from 'bux/core/state/focused-task/actions';
import { makeMeBillingContact } from 'bux/core/state/billing-contact/actions';
import { isWithFreeLicense } from 'bux/core/selectors/common';
import library from '../../FocusedTask/library';

const compareProducts = (a, b) => {
  if (a.title < b.title) {
    return -1;
  } else if (a.title > b.title) {
    return 1;
  }
  return 0;
};

const addEditionToItem = (item) => {
  const entitlement = item.entitlement;
  if (!entitlement) {
    return item;
  }
  const editions = productsEditions[entitlement.productKey];
  if (!editions) {
    return item;
  }
  const edition = editions.find(element => element.edition === entitlement.selectedEdition);
  if (!edition) {
    return item;
  }
  return { ...item, edition };
};

const addEditionToItems = items =>
  items
    .map(addEditionToItem)
    .sort(compareProducts);

const createSections = (state, selector) => {
  const groups = selector(state);
  const sections = {};

  Object
    .keys(groups)
    .filter(key => groups[key].length)
    .forEach(key => (sections[key] = {
      items: addEditionToItems(groups[key])
    }));

  return sections;
};

const mapStateToProps = state => ({
  metadata: combineAllDisplayable([
    entitlementGroup.selectors.getEntitlementGroupMetadata,
    billEstimate.selectors.getBillEstimateMetadata
  ])(state),
  currency: billEstimate.selectors.getBill(state).currencyCode,
  sections: createSections(state, getEntitlementSections),
  isOrganization: isOrganization(state),
  freeLicense: isWithFreeLicense(state),
  hasAtlassianAccess: hasAtlassianAccess(state),
  isAnnual: isNextBillingPeriodRenewalFrequencyAnnual(state),
  atlassianAccessExtras: atlassianAccessExtras(state),
  atlassianAccessTotalUnitCount: getAtlassianAccessTotalUnitCount(state),
  upcomingCurrencyExtra: upcomingCurrencyExtras(state),
  isNextBillInNewCurrency: isAfterTheCutOverDate(state),
});

const confirmAction = dispatch => () => dispatch(makeMeBillingContact());

const componentId = library.add(ConfirmCancel);
const cancelSubscription = () => (dispatch) => {
  const openDrawer = props => openModalDialog({
    component: componentId,
    props,
    logPrefix: 'manageSubscriptions.cancel.subscription'
  });
  dispatch(openDrawer({ confirmAction: confirmAction(dispatch) }));
};

const mapDispatchToProps = {
  cancelSubscription
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ManageSubscriptions);
