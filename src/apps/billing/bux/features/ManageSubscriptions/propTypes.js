import PropTypes from 'prop-types';

export const ActionType = PropTypes.shape({
  label: PropTypes.string.isRequired,
  order: PropTypes.number,
  primaryAction: PropTypes.bool,
  condition: PropTypes.func,
  action: PropTypes.func
});