import { openFocusedTask } from 'bux/core/state/focused-task/actions';
import { isOrganization } from 'bux/core/state/meta/selectors';
import library from '../../FocusedTask/library';

import Deactivate from './components/Deactivate';

const component = library.add(Deactivate);

const openDrawer = props => openFocusedTask({
  component,
  props,
  logPrefix: `manageSubscription.${props.entitlement.productKey}.unsubscribe`
});

export default {
  label: i18n => i18n.t('billing.manage-subscriptions.actions.unsubscribe')`Unsubscribe`,
  order: -1,
  condition: (entitlement, state) => entitlement.status === 'ACTIVE' && isOrganization(state),
  action: ({ dispatch, entitlement }) => dispatch(openDrawer({ entitlement })),
  analytics: {
    actionSubjectId: 'unsubscribeDropListItem',
  },
};
