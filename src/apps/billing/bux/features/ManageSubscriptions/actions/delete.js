import { getEntitlementSections } from 'bux/core/selectors/entitlements';
import { openFocusedTask } from 'bux/core/state/focused-task/actions';
import { isOrganization } from 'bux/core/state/meta/selectors';
import { isStride } from 'bux/core/state/bill-estimate/helpers';
import library from 'bux/features/FocusedTask/library';

import Deactivate from './components/Deactivate';

const component = library.add(Deactivate);

const openDrawer = props => openFocusedTask({
  component,
  props,
  logPrefix: `manageSubscription.${props.entitlement.productKey}.delete`
});

const isActiveOrTrial = entitlement => entitlement.status === 'ACTIVE' || entitlement.status === 'TRIAL';

const hasMoreThanOneEntitlement = state => getEntitlementSections(state).active.length > 1;

const action = ({ dispatch, entitlement }) => dispatch(openDrawer({ entitlement, deletion: true }));

export default {
  label: (i18n, entitlement) => (isStride(entitlement)
    ? i18n.t('billing.manage-subscriptions.actions.unsubscribe')`Unsubscribe`
    : i18n.t('billing.manage-subscriptions.actions.delete')`Delete`),
  order: -1,
  condition: (entitlement, state) => (
    isActiveOrTrial(entitlement) &&
    hasMoreThanOneEntitlement(state) &&
    !isOrganization(state)
  ),
  action,
  analytics: {
    actionSubjectId: 'deleteDropListItem',
  }
};
