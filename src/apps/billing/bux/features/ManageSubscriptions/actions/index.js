import deleteAction from './delete';
import unsubscribe from './unsubscribe';
import sendToMAC from './sendToMAC';
import getSupport from './getSupport';
import { manageAnnualUsers, manageMonthlyUsers, manageMonthlyOrganizationUsers } from './manageUsers';
import { changeEdition, upgrade } from './changeEdition';
import addPaymentDetails from './addPaymentDetails';

export default [
  deleteAction,
  unsubscribe,
  sendToMAC,
  getSupport,
  manageAnnualUsers,
  manageMonthlyUsers,
  manageMonthlyOrganizationUsers,
  changeEdition,
  upgrade,
  addPaymentDetails
];
