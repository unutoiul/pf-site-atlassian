import { openNewTab } from 'bux/common/helpers/browser';

export default {
  label: i18n => i18n.t('billing.manage-subscriptions.actions.get-support')`Get support`,
  order: -2,
  action: () => openNewTab('https://support.atlassian.com/contact/#/'),
  analytics: {
    actionSubjectId: 'getSupportDropListItem'
  }
};
