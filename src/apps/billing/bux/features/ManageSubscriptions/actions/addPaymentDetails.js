import { hasCreditCard } from 'bux/core/state/billing-details/selectors';
import { isTrial } from 'bux/core/selectors/common/entitlements';
import { push as redirect } from 'bux/core/state/router/actions';
import { isNextBillingPeriodRenewalFrequencyAnnual } from 'bux/core/state/bill-estimate/selectors';

const canDowngrade = entitlement =>
  entitlement.editionTransitions &&
  entitlement.editionTransitions.indexOf('DOWNGRADE') >= 0;

export default {
  action: ({ dispatch }) => dispatch(redirect('/paymentdetails')),
  label: i18n => i18n.t('billing.manage-subscriptions.actions.add-billing-details')`Add billing details`,
  condition: (entitlement, state = {}) => (
    !isNextBillingPeriodRenewalFrequencyAnnual(state) &&
    isTrial(entitlement) &&
    !hasCreditCard(state) &&
    canDowngrade(entitlement)
  ),
  primaryAction: true
};
