import { openModalDialog } from 'bux/core/state/focused-task/actions';
import { isOrganization } from 'bux/core/state/meta/selectors';
import library from 'bux/features/FocusedTask/library';
import { makeMeBillingContact } from 'bux/core/state/billing-contact/actions';
import ConfirmCancel from 'bux/components/ManageSubscriptions/sections/components/ConfirmCancel';
import { getEntitlementSections } from 'bux/core/selectors/entitlements';

const componentId = library.add(ConfirmCancel);

const openDrawer = props => openModalDialog({
  component: componentId,
  props,
  logPrefix: `manageSubscriptions.${props.entitlement.productKey}.cancel.subscription`
});

const isActiveOrTrial = entitlement => entitlement.status === 'ACTIVE' || entitlement.status === 'TRIAL';

const oneEntitlementLeft = state => getEntitlementSections(state).active.length <= 1;

const condition = (entitlement, state) =>
  isActiveOrTrial(entitlement) && oneEntitlementLeft(state) && !isOrganization(state);

export const action = ({ dispatch, entitlement }) => dispatch(openDrawer({
  entitlement,
  confirmAction: () => dispatch(makeMeBillingContact())
}));

export default {
  label: i18n => i18n.t('billing.manage-subscriptions.actions.delete')`Delete`,
  order: -1,
  condition,
  action
};
