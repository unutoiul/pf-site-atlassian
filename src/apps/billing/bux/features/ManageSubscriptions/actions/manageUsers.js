import { isNextBillingPeriodRenewalFrequencyAnnual } from 'bux/core/state/bill-estimate/selectors';
import { isOrganization } from 'bux/core/state/meta/selectors';
import { openNewTab } from 'bux/common/helpers/browser';

export const manageMonthlyUsers = {
  label: i18n => i18n.t('billing.manage-subscriptions.actions.manage-users')`Manage users`,
  condition: (entitlement, state) => !isNextBillingPeriodRenewalFrequencyAnnual(state) && !isOrganization(state),
  action: () => openNewTab('/admin/users', '_top'),
  primaryAction: true,
  analytics: {
    actionSubjectId: 'manageUsersButton'
  }
};

export const manageAnnualUsers = {
  label: i18n => i18n.t('billing.manage-subscriptions.actions.upgrade-tier')`Upgrade tier`,
  condition: (entitlement, state) => isNextBillingPeriodRenewalFrequencyAnnual(state),
  action: () => openNewTab('https://www.atlassian.com/company/contact/purchasing-licensing', '_top'),
  primaryAction: true,
  analytics: {
    actionSubjectId: 'manageUsersButton'
  }
};

export const manageMonthlyOrganizationUsers = {
  label: i18n => i18n.t('billing.manage-subscriptions.actions.manage-users')`Manage users`,
  condition: (entitlement, state) => !isNextBillingPeriodRenewalFrequencyAnnual(state) && isOrganization(state),
  action: ({ entitlement }) => openNewTab(`/o/${entitlement.accountId}/members`, '_top'),
  primaryAction: true,
  analytics: {
    actionSubjectId: 'manageUsersButton'
  }
};
