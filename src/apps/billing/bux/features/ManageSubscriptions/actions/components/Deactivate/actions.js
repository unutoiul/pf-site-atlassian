import { showNotificationByTemplate } from 'bux/core/state/notifications/actions';
import { getEntitlementGroup } from 'bux/core/state/entitlement-group/actions';
import { getRecommendedProducts } from 'bux/core/state/recommended-products/actions';
import { getBillEstimate } from 'bux/core/state/bill-estimate/actions';
import { deactivate as deactivateApi } from 'bux/core/state/api';

export const deactivate = entitlement => dispatch =>
  dispatch(deactivateApi(entitlement))
    .then(() => {
      dispatch(showNotificationByTemplate('deactivate', {
        entitlementName: entitlement.name
      }));
      dispatch(getEntitlementGroup());
      dispatch(getBillEstimate());
      dispatch(getRecommendedProducts());
    })
    .catch((e) => {
      dispatch(showNotificationByTemplate('deactivate-error', {
        entitlementName: entitlement.name
      }));
      throw e;
    });
