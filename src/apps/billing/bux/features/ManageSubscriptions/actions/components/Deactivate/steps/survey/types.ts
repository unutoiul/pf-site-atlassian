import { SurveyOptions } from 'bux/components/Survey/Survey';

export interface SurveyData {
  required?: boolean;
  options: SurveyOptions[];
}
