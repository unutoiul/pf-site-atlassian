import PropTypes from 'prop-types';
import { Trans } from '@lingui/react';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import cx from 'classnames';

import { withAnalytics } from 'bux/components/Analytics';
import Loading from 'bux/components/Loading';

import { deactivate } from '../actions';

import style from '../../style.less';

class Deactivating extends Component {
  static propTypes = {
    entitlement: PropTypes.object,
    next: PropTypes.func.isRequired,
    back: PropTypes.func.isRequired,
    deactivate: PropTypes.func,
    analytics: PropTypes.object.isRequired,
  };

  componentDidMount() {
    this.props.deactivate(this.props.entitlement)
      .then((result) => {
        this.props.analytics.sendTrackEvent({
          action: 'deactivate',
          actionSubject: 'application',
          actionSubjectId: 'deactivateApplication',
          attributes: {
            deactivateProduct: this.props.entitlement.productKey,
          },
        });
        return result;
      })
      .then(this.props.next)
      .catch(this.props.back);
  }

  render() {
    return (
      <div className={cx('deactivatingFocusTask', style.page)}>
        <h3>
          <Trans id="billing.deactivating.title">Deactivating</Trans>
        </h3>
        <Loading mode="paragraph" />
      </div>
    );
  }
}

const mapDispatchToProps = {
  deactivate,
};

export default withAnalytics(connect(
  null,
  mapDispatchToProps
)(Deactivating));

