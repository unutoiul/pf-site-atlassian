import PropTypes from 'prop-types';
import transitions from './transitions';

const TransitionPropType = PropTypes.oneOf(Object.keys(transitions));

const EditionPropType = PropTypes.shape({
  isCurrent: PropTypes.bool,
  name: PropTypes.oneOfType([PropTypes.string, PropTypes.object]).isRequired,
  features: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.string, PropTypes.object])).isRequired,
  edition: PropTypes.string.isRequired,
  cost: PropTypes.shape({
    amount: PropTypes.number,
    currency: PropTypes.string
  }).isRequired,

  transition: TransitionPropType.isRequired,
  needsPaymentDetailsOnAccount: PropTypes.bool.isRequired
});

export {
  EditionPropType,
  TransitionPropType,
};
