import { Trans } from '@lingui/react';
import * as React from 'react';

import { SurveyData } from 'bux/features/ManageSubscriptions/actions/components/Deactivate/steps/survey/types';

const why = (i18n) => ({
  ask: {
    placeholder: i18n.t('billing.change-edition.deactivate.why')`Tell us more`,
    analyticsName: 'surveyOtherField',
  },
});

const getDefaultOptions = (productName, i18n): SurveyData => ({
  options: [{
    type: 'radio',
    items: [
      {
        key: 'not-using',
        label: (
          <Trans id="billing.change-edition.deactivate.options.not-using">
            I'm not using {productName} enough to pay for a subscription
          </Trans>
        ),
      },
      {
        key: 'lacking-features',
        label: (
          <Trans id="billing.change-edition.deactivate.options.lacking-features">
            {productName} does not include all the features I want
          </Trans>
        ),
      },
      {
        key: 'competitor',
        label: (
          <Trans id="billing.change-edition.deactivate.options.competitor">
            I'm using another service
          </Trans>
        ),
      },
      {
        key: 'other',
        label: (<Trans id="billing.change-edition.deactivate.options.other">Other</Trans>),
        nested: why(i18n),
      },
    ],
  }],
});

export default getDefaultOptions;
