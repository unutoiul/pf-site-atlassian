import PropTypes from 'prop-types';
import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { mount, shallow } from 'enzyme';
import Button, { SubmitButton } from 'bux/components/Button';

import { ChangeEditionWizard, WithLog } from '../ChangeEdition';
import transitions from '../transitions';
import EditionBlock from '../EditionBlock';

import SelectEdition from '../steps/SelectEdition';
import Downgrade from '../steps/Downgrade';
import FinalStep from '../steps/FinalScreen';

/* eslint-disable newline-per-chained-call */

describe('ChangeEditionWizard component', () => {
  const getEditions = () => ([
    {
      name: 'Free',
      edition: 'free',
      cost: {
        monthly: 0,
        annual: 0,
        currency: 'usd'
      },
      features: [],
      transition: transitions.DOWNGRADE,
      isCurrent: false,
      needsPaymentDetailsOnAccount: true
    },
    {
      name: 'Standard',
      edition: 'standard',
      cost: {
        monthly: 100,
        annual: 1000,
        currency: 'usd'
      },
      features: [],
      transition: transitions.EQUAL,
      isCurrent: true,
      needsPaymentDetailsOnAccount: true
    },
    {
      name: 'Premium',
      edition: 'premium',
      cost: {
        monthly: 200,
        annual: 2000,
        currency: 'usd'
      },
      features: [],
      transition: transitions.UPGRADE,
      isCurrent: false,
      needsPaymentDetailsOnAccount: true
    }
  ]);

  const redirect = sinon.stub();

  const PROPS = {
    entitlement: { name: 'Banana', selectedEdition: 'standard' },
    hasPaymentDetails: true,
    renewalFrequency: 'MONTHLY',
    closeAction: () => {
    },
    changeEdition: () => Promise.resolve(),
    refreshStoreAfterChangeEdition: () => Promise.resolve(),
    redirect
  };

  const getAnalytic = () => {
    const sendAnalyticEvent = sinon.spy();
    return {
      sendAnalyticEvent,
      context: {
        context: {
          sendAnalyticEvent,
        },
        childContextTypes: {
          sendAnalyticEvent: PropTypes.func
        }
      }
    };
  };

  // due to PaymentDetails and withLoader one cannot use mount (w/o proxyquire) in some cases

  it('should render editions', () => {
    const wrapper =
      shallow(<ChangeEditionWizard {...PROPS} editions={getEditions()} />)
        .find(SelectEdition).dive();
    expect(wrapper.find(EditionBlock)).to.have.length(3);
  });

  it('should trigger pageview on mount', () => {
    const { sendAnalyticEvent, context } = getAnalytic();
    mount(<WithLog {...PROPS} editions={getEditions()} />, context);
    expect(sendAnalyticEvent).to.be.calledWithMatch('pageView');
  });

  it('should choose downgrade path', () => {
    const { context } = getAnalytic();
    const wrapper = mount(<ChangeEditionWizard {...PROPS} editions={getEditions()} />, context);
    // activate first edition
    wrapper.find(EditionBlock).at(0).find(Button).simulate('click');
    expect(wrapper.find(Downgrade)).to.be.present();
  });

  it('should redirect to payment details if no payment details', () => {
    const { context } = getAnalytic();
    const wrapper = shallow(
      <ChangeEditionWizard {...PROPS} hasPaymentDetails={false} editions={getEditions()} />,
      context
    );
    wrapper
      .find(SelectEdition).dive()
      .find(EditionBlock).at(2).dive()
      .find(SubmitButton).simulate('click');
    expect(redirect).to.be.called();
  });

  it('should skip upgrade path is payments details exists', () => {
    const { context } = getAnalytic();
    const wrapper = mount(
      <ChangeEditionWizard {...PROPS} hasPaymentDetails editions={getEditions()} />,
      context
    );

    // activate first edition
    wrapper.find(EditionBlock).at(2).find(Button).simulate('click');
    expect(wrapper.find(FinalStep)).to.be.present();
  });

  it('should go through upgrade flow', (done) => {
    const closeAction = sinon.spy();
    const changeEdition = () => Promise.resolve();
    const { sendAnalyticEvent, context } = getAnalytic();

    const wrapper = shallow(
      (
        <ChangeEditionWizard
          {...PROPS}
          editions={getEditions()}
          closeAction={closeAction}
          changeEdition={changeEdition}
          hasPaymentDetails
        />
      ), context
    );

    //trigger first one
    wrapper
      .find(SelectEdition).dive()
      .find(EditionBlock).at(2).dive()
      .find(SubmitButton).simulate('click');

    setImmediate(() => {
      expect(sendAnalyticEvent).to.be.calledWithMatch('changeTo', { edition: 'Premium' });
      expect(closeAction).to.be.called();
      done();
    });
  });

  it('should reset step, when something goes wrong in edition', (done) => {
    const closeAction = sinon.spy();
    const changeEdition = () => Promise.reject();
    const { context } = getAnalytic();

    const wrapper = shallow(<ChangeEditionWizard
      {...PROPS}
      editions={getEditions()}
      closeAction={closeAction}
      changeEdition={changeEdition}
    />, context);

    wrapper
      .find(SelectEdition).dive()
      .find(EditionBlock).at(2).dive()
      .find(SubmitButton).simulate('click');

    setImmediate(() => {
      expect(wrapper.find(SelectEdition)).to.be.present();
      expect(closeAction).not.to.be.called();
      done();
    });
  });

  it('should go through downgrade flow', (done) => {
    const closeAction = sinon.spy();
    const changeEdition = () => Promise.resolve();
    const { sendAnalyticEvent, context } = getAnalytic();

    const wrapper = shallow(<ChangeEditionWizard
      {...PROPS}
      editions={getEditions()}
      closeAction={closeAction}
      changeEdition={changeEdition}
    />, context);

    wrapper
      .find(SelectEdition).dive()
      .find(EditionBlock).at(0).dive()
      .find(SubmitButton).simulate('click');

    const downgrade = wrapper.update().find(Downgrade);
    expect(downgrade).to.be.present();

    //trigger save
    downgrade.props().next();
    setImmediate(() => {
      expect(sendAnalyticEvent).to.be.calledWithMatch('changedTo', { edition: 'Free' });
      expect(closeAction).to.be.called();
      done();
    });
  });
});
