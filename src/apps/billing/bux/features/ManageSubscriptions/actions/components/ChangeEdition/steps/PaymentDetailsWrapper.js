import PropTypes from 'prop-types';
import React from 'react';
import PaymentDetailsFormContainer from '../PaymentDetails';

const PaymentDetailsWrapper = ({ next }) => (
  <div>
    <h2>Please provide billing details:</h2>
    <br />

    <PaymentDetailsFormContainer
      onSubmitSuccess={next}
      autoFocus
    />
  </div>
);

PaymentDetailsWrapper.propTypes = {
  next: PropTypes.func.isRequired
};


export default PaymentDetailsWrapper;
