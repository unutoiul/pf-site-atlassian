import { Trans } from '@lingui/react';
import * as React from 'react';

import { SurveyData } from 'bux/features/ManageSubscriptions/actions/components/Deactivate/steps/survey/types';

const didNotEnjoy = (i18n) => ({
  label: <Trans id="billing.survey.jsd.did-not-enjoy.label">Which of the following would you use to describe it</Trans>,
  type: 'checkbox',
  items: [
    {
      key: 'complicated',
      label: <Trans id="billing.survey.jsd.complicated.label">Complicated</Trans>,
    },
    {
      key: 'ugly',
      label: <Trans id="billing.survey.jsd.ugly.label">Ugly</Trans>,
    },
    {
      key: 'tacky',
      label: <Trans id="billing.survey.jsd.tacky.label">Tacky</Trans>,
    },
    {
      key: 'unimaginative',
      label: <Trans id="billing.survey.jsd.unimaginative.label">Unimaginative</Trans>,
    },
  ],
  ask: {
    placeholder: i18n.t('billing.survey.jsd.did-not-enjoy.ask')`Tell us more about why you feel this way`,
  },
});

const difficult = (i18n) => ({
  ask: {
    placeholder: i18n.t('billing.survey.jsd.difficult.ask')`How could we have make it easier?`,
  },
});

const usingDifferent = (i18n) => ({
  label: <Trans id="billing.survey.jsd.using-different.label">Which product are you using instead</Trans>,
  type: 'radio',
  ask: {
    placeholder: i18n.t('billing.survey.jsd.using-different.ask')`Why?`,
  },
  items: [
    {
      key: 'zendesk',
      label: <Trans id="billing.survey.jsd.zendesk.label">Zendesk</Trans>,
    },
    {
      key: 'jira',
      label: <Trans id="billing.survey.jsd.jira.label">Jira Software</Trans>,
    },
    {
      key: 'confluence',
      label: <Trans id="billing.survey.jsd.confluence.label">Confluence</Trans>,
    },
    {
      key: 'fresh',
      label: <Trans id="billing.survey.jsd.fresh.label">FreshService / FreshDesk</Trans>,
    },
    {
      key: 'other',
      label: <Trans id="billing.survey.jsd.other.label">Other</Trans>,
    },
  ],
});

const notUsing = (i18n) => ({
  ask: {
    placeholder: i18n.t('billing.survey.jsd.not-using.ask')`Why?`,
  },
});

const missingFeature = (i18n) => ({
  ask: {
    placeholder: i18n.t('billing.survey.jsd.missing-feature.ask')`What feature is missing?`,
  },
});

const pricing = (i18n) => ({
  ask: {
    placeholder: i18n.t('billing.survey.jsd.pricing.ask')`What would you pay to keep using it?`,
  },
});

const why = (i18n) => ({
  ask: {
    placeholder: i18n.t('billing.survey.jsd.why.ask')`Tell us more`,
    analyticsName: 'surveyOtherField',
  },
});

const getJSDOptions = (i18n): SurveyData => ({
  required: true,
  options: [{
    type: 'radio',
    items: [
      {
        key: 'did-not-enjoy',
        label: <Trans id="billing.survey.jsd.options.did-not-enjoy.label">We didn't enjoy the product experience</Trans>,
        nested: didNotEnjoy(i18n),
      },
      {
        key: 'difficult',
        label: <Trans id="billing.survey.jsd.options.difficult.label">It was difficult to set up</Trans>,
        nested: difficult(i18n),
      },
      {
        key: 'using-different',
        label: <Trans id="billing.survey.jsd.options.using-different.label">We're using a different product</Trans>,
        nested: usingDifferent(i18n),
      },
      {
        key: 'not-using',
        label: <Trans id="billing.survey.jsd.options.not-using.label">We're not using it</Trans>,
        nested: notUsing(i18n),
      },
      {
        key: 'missing-feature',
        label: <Trans id="billing.survey.jsd.options.missing-feature.label">It is missing a feature we need</Trans>,
        nested: missingFeature(i18n),
      },
      {
        key: 'pricing',
        label: <Trans id="billing.survey.jsd.options.pricing.label">Pricing</Trans>,
        nested: pricing(i18n),
      },
      {
        key: 'other',
        label: <Trans id="billing.survey.jsd.options.other.label">Other</Trans>,
        nested: why(i18n),
      },
    ],
  }],
});

export default getJSDOptions;
