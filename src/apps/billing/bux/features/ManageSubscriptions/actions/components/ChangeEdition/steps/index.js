import SelectEdition from './SelectEdition';
import Downgrade from './Downgrade';
import FinalScreen from './FinalScreen';

export default {
  SelectEdition,
  Downgrade,
  FinalScreen
};
