
export const SELECT_EDITION = 'SELECT_EDITION';
export const DOWNGRADE_CONFIRMATION = 'DOWNGRADE_CONFIRMATION';
export const CHANGING_EDITION = 'CHANGING_EDITION';
export const ADD_PAYMENT_DETAILS = 'ADD_PAYMENT_DETAILS';

export default {
  SELECT_EDITION,
  DOWNGRADE_CONFIRMATION,
  CHANGING_EDITION,
  ADD_PAYMENT_DETAILS,
};