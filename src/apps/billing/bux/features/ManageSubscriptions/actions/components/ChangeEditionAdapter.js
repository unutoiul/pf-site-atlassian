import { withI18n } from '@lingui/react';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import DocumentTitle from 'bux/components/DocumentTitle';
import billingDetails from 'bux/core/state/billing-details';
import entitlementGroup from 'bux/core/state/entitlement-group';
import billEstimate from 'bux/core/state/bill-estimate';
import { withPageLoader } from 'bux/components/withLoader';

import ChangeEditionComponent from './ChangeEdition';
import extendEditions from './ChangeEdition/editions/extended-editions';

export const ChangeEditionDataProvider = ({
  entitlement, closeAction, edition, hasPaymentDetails, renewalFrequency, i18n,
}) =>
  (
    <DocumentTitle title={i18n.t('billing.change-edition.document.title')`Change edition`}>
      <ChangeEditionComponent
        entitlement={entitlement}
        editions={extendEditions(edition)}
        closeAction={closeAction}
        hasPaymentDetails={hasPaymentDetails}
        renewalFrequency={renewalFrequency}
      />
    </DocumentTitle>
  );

ChangeEditionDataProvider.propTypes = {
  entitlement: PropTypes.object.isRequired,
  closeAction: PropTypes.func,
  edition: PropTypes.object,
  hasPaymentDetails: PropTypes.bool.isRequired,
  renewalFrequency: PropTypes.string,
  i18n: PropTypes.object,
};

const mapStateToProps = (state, { entitlement }) => ({
  edition: entitlementGroup.selectors.getEntitlementEdition(entitlement.id)(state),
  hasPaymentDetails: billingDetails.selectors.hasCreditCard(state),
  renewalFrequency: billEstimate.selectors.getNextBillingPeriodRenewalFrequency(state),
});

const ChangeEditionI18n = withI18n()(ChangeEditionDataProvider);

const ConnectedChangeEditionAdapter = connect(mapStateToProps)(ChangeEditionI18n);

const isLoaded = (state, { entitlement }) =>
  entitlementGroup.selectors.getEntitlementEditionMetadata(entitlement.id)(state);

const mountAction = ({ entitlement }) =>
  entitlementGroup.actions.getEntitlementEdition(entitlement.id);

export default withPageLoader(isLoaded, mountAction)(ConnectedChangeEditionAdapter);
