import { Trans } from '@lingui/react';
import * as React from 'react';

import { SurveyData } from 'bux/features/ManageSubscriptions/actions/components/Deactivate/steps/survey/types';

const difficult = (i18n) => ({
  label: <Trans id="billing.survey.jsw.difficult.why">Why</Trans>,
  type: 'checkbox',
  items: [
    {
      key: 'setup',
      label: (
        <Trans id="billing.survey.jsw.difficult.setup">
          It was too difficult to set up, needed more help getting it adopted
        </Trans>
      ),
    },
    {
      key: 'navigation',
      label: <Trans id="billing.survey.jsw.difficult.navigation">Navigation issues</Trans>,
    },
    {
      key: 'visual',
      label: <Trans id="billing.survey.jsw.difficult.visual">Visual appeal</Trans>,
    },
    {
      key: 'configuration',
      label: (
        <Trans id="billing.survey.jsw.difficult.configuration">
          Administration and/or configuration complexity
        </Trans>
      ),
    },
    {
      key: 'other',
      label: <Trans id="billing.survey.jsw.difficult.other">Other</Trans>,
    },
  ],
  ask: {
    placeholder: i18n.t('billing.survey.jsw.difficult.ask')`How could we have made it easier?`,
  },
});

const usingDifferent = (i18n) => ({
  ask: {
    placeholder: i18n.t('billing.survey.jsw.ask.using-different')`Which other product are you using? Why did you move?`,
  },
});

const notUsing = (i18n) => ({
  ask: {
    placeholder: i18n.t('billing.survey.jsw.ask.not-using')`Please tell us why you no longer need Jira. Would you use Jira again in the future?`,
  },
});

const missingFeature = (i18n) => ({
  ask: {
    placeholder: i18n.t('billing.survey.jsw.ask.missing-feature')`What feature or capability would have made you stay?`,
  },
});

const performance = (i18n) => ({
  ask: {
    placeholder: i18n.t('billing.survey.jsw.ask.performance')`What type of performance issues were you facing?`,
  },
});

const missingIntegrations = (i18n) => ({
  ask: {
    placeholder: i18n.t('billing.survey.jsw.ask.missing-integrations')`Which integrations would you have liked to see?`,
  },
});

const pricing = (i18n) => ({
  ask: {
    placeholder: i18n.t('billing.survey.jsw.ask.pricing')`What would you pay to keep using Jira?`,
  },
});

const support = (i18n) => ({
  ask: {
    placeholder: i18n.t('billing.survey.jsw.ask.support')`How can we improve our support levels?`,
  },
});

const why = (i18n) => ({
  ask: {
    placeholder: i18n.t('billing.survey.jsw.ask.why')`Please tell us how we can improve?`,
    analyticsName: 'surveyOtherField',
  },
});

const getJSWSurvey = (i18n): SurveyData => ({
  required: true,
  options: [
    {
      type: 'radio',
      items: [
        {
          key: 'difficult',
          label: (
            <Trans id="billing.survey.jsw.difficult">Too difficult to use (e.g. UI / UX issues)</Trans>
          ),
          nested: difficult(i18n),
        },
        {
          key: 'missing-feature',
          label: <Trans id="billing.survey.jsw.missing-feature">Missing features or capabilities</Trans>,
          nested: missingFeature(i18n),
        },
        {
          key: 'missing-integrations',
          label: <Trans id="billing.survey.jsw.missing-integrations">Missing integrations with other tools</Trans>,
          nested: missingIntegrations(i18n),
        },
        {
          key: 'using-different',
          label: <Trans id="billing.survey.jsw.using-different">We're using a different product</Trans>,
          nested: usingDifferent(i18n),
        },
        {
          key: 'not-using',
          label: <Trans id="billing.survey.jsw.not-using">Product is no longer needed</Trans>,
          nested: notUsing(i18n),
        },
        {
          key: 'performance',
          label: (
            <Trans id="billing.survey.jsw.performance">
              Experienced performance issues (such as speed, outages and/or persistent bugs)
            </Trans>
          ),
          nested: performance(i18n),
        },
        {
          key: 'pricing',
          label: <Trans id="billing.survey.jsw.pricing">Pricing</Trans>,
          nested: pricing(i18n),
        },
        {
          key: 'support',
          label: (
            <Trans id="billing.survey.jsw.support">
              Dissatisfied with level of support and/or poor product documentation
            </Trans>
          ),
          nested: support(i18n),
        },

        {
          key: 'other',
          label: <Trans id="billing.survey.jsw.other">Other</Trans>,
          nested: why(i18n),
        },
      ],
    },
    {
      type: 'checkbox',
      items: [
        {
          key: 'ok-to-contact',
          label: <Trans id="billing.survey.jsw.ok-to-contact">It's ok to contact me about my feedback</Trans>,
        },
      ],
    },
  ],
});

export default getJSWSurvey;
