import { Trans } from '@lingui/react';
import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';
import Money from 'bux/components/Money';
import Button, { SubmitButton } from 'bux/components/Button';

import { EditionPropType } from './propTypes';
import transitions from './transitions';
import styles from './styles.less';

const TransitionButtonText = ({ transition }) => {
  if (transition === transitions.UPGRADE) {
    return <Trans id="billing.change-edition.edition-block.upgrade">Upgrade</Trans>;
  }

  if (transition === transitions.DOWNGRADE) {
    return <Trans id="billing.change-edition.edition-block.downgrade">Downgrade</Trans>;
  }

  return <Trans id="billing.change-edition.edition-block.your-plan">Your plan</Trans>;
};
TransitionButtonText.propTypes = {
  transition: PropTypes.string,
};

const ProcessingButtonText = ({ transition }) => {
  if (transition === transitions.UPGRADE) {
    return <Trans id="billing.change-edition.edition-block.upgrading">Upgrading</Trans>;
  }

  return <Trans id="billing.change-edition.edition-block.downgrading">Downgrading</Trans>;
};
ProcessingButtonText.propTypes = {
  transition: PropTypes.string,
};

const getUsersText = (amount, renewalFrequency) => {
  if (amount === 0) {
    return <Trans id="billing.change-edition.edition-block.unlimited-users">Unlimited users</Trans>;
  }
  if (renewalFrequency === 'ANNUAL') {
    return <Trans id="billing.change-edition.edition-block.user-year">/ user / year</Trans>;
  }
  return <Trans id="billing.change-edition.edition-block.user-month">/ user / month</Trans>;
};

const EditionBlock = ({
  entitlement, edition, onEditionChange, indicateProcessing, renewalFrequency
}) => {
  const { cost: { currency } = {}, features = [], transition } = edition;
  const amount = edition.cost[renewalFrequency.toLowerCase()];
  const moneyStyle = amount === 0 ? styles.free : styles.paid;
  const usersText = getUsersText(amount, renewalFrequency);
  const isCurrent = transition === transitions.EQUAL;
  const ChangeButton = !isCurrent ? SubmitButton : Button;

  return (
    <div className={cx(styles.editionBlock, 'editionBlock')} data-test={edition.edition}>
      <h2>{entitlement.name} {edition.name}</h2>
      <div className={cx(styles.cost, moneyStyle, 'cost')}>
        <Money amount={amount} currency={currency} showFreeIfZero />
      </div>
      <div className={cx(styles.users, 'users')}>{usersText}</div>
      <div className={cx(styles.features, 'bux/features')}>
        {features.map((feature, index) => (
          <div key={index}>{feature}</div>
        ))}
      </div>

      <div className={styles.changeButton} >
        <ChangeButton
          className={styles.button}
          onClick={onEditionChange}
          disabled={isCurrent || indicateProcessing}
          spinning={indicateProcessing}
        >
          {
            indicateProcessing
              ? <ProcessingButtonText transition={transition} />
              : <TransitionButtonText transition={transition} />
          }
        </ChangeButton>
      </div>
    </div>
  );
};

EditionBlock.propTypes = {
  entitlement: PropTypes.object.isRequired,
  edition: EditionPropType.isRequired,
  onEditionChange: PropTypes.func,
  indicateProcessing: PropTypes.bool,
  renewalFrequency: PropTypes.string.isRequired,
};

export default EditionBlock;
