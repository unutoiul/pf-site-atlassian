import { Trans, withI18n } from '@lingui/react';
import cx from 'classnames';
import * as PropTypes from 'prop-types';
import * as React from 'react';

import AkTextfield from '@atlaskit/textfield';

import products from 'bux/common/data/products';
import { Analytics } from 'bux/components/Analytics';
import { FeatureFlagsContext } from 'bux/components/FeatureFlags';
import FieldBase from 'bux/components/field/FieldBase';
import connectLogRoot from 'bux/components/log/root';
import { Survey } from 'bux/components/Survey/Survey';

import style from '../../style.less';
import getSurvey from './survey';

export interface ConfirmProps {
  entitlement: any;
  deletion: boolean;
  confirmed: boolean;
  allOpen: boolean;
  i18n: any;
  next(): any;
  back(): any;
}

const ActionLabel = ({ deletion }) => (
  deletion
    ? <Trans id="billing.deactivate.action.delete">Delete</Trans>
    : <Trans id="billing.deactivate.action.unsubscribe">Unsubscribe</Trans>
);

export class ConfirmComponent extends React.Component<ConfirmProps, { confirmed: boolean }> {
  public static propTypes = {
    entitlement: PropTypes.object.isRequired,
    deletion: PropTypes.bool,
    next: PropTypes.func.isRequired,
    back: PropTypes.func.isRequired,
  };

  public static contextTypes = {
    sendAnalyticEvent: PropTypes.func,
  };

  public state = {
    confirmed: this.props.confirmed || false,
  };

  public componentDidMount() {
    this.context.sendAnalyticEvent('pageView');
  }

  public onTextChange = (event) => {
    const confirmText = event.target.value.toLowerCase();
    this.setState({ confirmed: confirmText === this.actionName() });
  };

  public actionName() {
    return this.props.deletion ? 'delete' : 'unsubscribe';
  }

  public upperCasedActionName() {
    return this.actionName().toUpperCase();
  }

  public render() {
    const { i18n } = this.props;
    const productName = this.props.entitlement.name;
    const productKey = this.props.entitlement.productKey;
    const deactivationMessage = products[productKey] && products[productKey].deactivationMessage;
    const upperCasedActionName = this.upperCasedActionName();
    const survey = getSurvey(this.props.entitlement.name, this.props.entitlement.productKey, this.props.i18n);

    return (
      <div className={cx(style.page, 'deactivateFocusedTask')}>
      <h3>
        <b>
          <Trans id="billing.deactivate.title">We're sad to see you go</Trans>
        </b>
      </h3>
      <div>
        {this.props.deletion &&
        <p>
          <Trans id="billing.deactivate.data-will-be-deleted">
            Please note that all <b>{productName}</b> data will be <u>deleted</u> immediately.
          </Trans>
        </p>
        }

        {deactivationMessage
          ? <p>{deactivationMessage}</p>
          : <p>
              <Trans id="billing.deactivate.users-no-access">
                Your users will no longer have access to <b>{productName}</b>.
              </Trans>
            </p>
        }

        <p>
          {
            this.props.deletion ?
              <Trans id="billing.deactivate.reactivate.price-change">
                If you choose to reactivate <b>{productName}</b> in the future,
                the price may change from what you are paying today.
              </Trans> :
              <Trans id="billing.deactivate.resubscribe.price-change">
                If you choose to resubscribe <b>{productName}</b> in the future,
                the price may change from what you are paying today.
              </Trans>
          }
        </p>
      </div>
      <fieldset className="ak-field-group">
        <Analytics.UI as="confirmField">
          <FieldBase
            label={
              i18n.t(
                'billing.deactivate.type-to-continue',
              )`If you still want to continue, type "${upperCasedActionName}" below:`
            }
            id="confirm"
            isPaddingDisabled
            isFitContainerWidthEnabled
          >
            <AkTextfield
              id="confirm"
              name="confirm"
              autoFocus
              data-autofocus
              className={cx('deactivateConfirmation', style.confirm, 'ak-field-text')}
              onChange={this.onTextChange}
              autoComplete="off"
              appearance="none"
            />
          </FieldBase>
        </Analytics.UI>
      </fieldset>
      {this.state.confirmed &&
        <div className="deactivateSurvey">
          <p>{
            survey.required
            ? <Trans id="billing.deactivate.survey.required">
                If you have a moment, please tell us your main reason for leaving.
              </Trans>
            : <Trans id="billing.deactivate.survey.not-required">
                Please take a moment to tell us your main reason for leaving, it will help us improve in the future.
              </Trans>
          }</p>
          <FeatureFlagsContext.Consumer>
            {(ff= {}) =>
              <Survey
                cancelAnalyticsName="cancelButton"
                submitAnalyticsName="deleteButton"
                optionsAnalyticsName="deactivateSurveyRadio"
                required={survey.required && ff['churn-survey.required']}
                requiredMessage={<Trans id="billing.deactivate.survey.select-reason">Please select a reason for leaving.</Trans>}
                submitAction={this.props.next}
                submitLabel={<ActionLabel deletion={this.props.deletion} />}
                submitType="dangerous"
                cancelAction={this.props.back}
                options={survey.options}
                allOpen={this.props.allOpen}
              />}
          </FeatureFlagsContext.Consumer>
        </div>
      }
    </div>
    );
  }
}

export default connectLogRoot('confirm')(withI18n()(ConfirmComponent));
