import { storiesOf } from '@storybook/react';

import { inPageChrome, mockedFunction, render } from 'bux/common/helpers/storybook';

import Deactivating from './Deactivating';

storiesOf('BUX|Focused tasks/Deactivate/Step 2: Deactivating', module)
  .add('Default', () => inPageChrome(render(Deactivating, {
    entitlement: {
      id: 'b7db7827-e2ac-492e-a3bb-e1c310764043',
      name: 'Stride',
      productKey: 'jira-core.ondemand',
    },
    next: mockedFunction,
    back: mockedFunction,
  })));
