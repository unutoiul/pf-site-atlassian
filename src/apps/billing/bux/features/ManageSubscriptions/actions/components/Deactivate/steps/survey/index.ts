import { SurveyData } from 'bux/features/ManageSubscriptions/actions/components/Deactivate/steps/survey/types';

import getDefaultSurvey from './default';
import getJSDSurvey from './JSD';
import getJSWSurvey from './JSW';

const getSurvey = (productName, productKey, i18n): SurveyData => {
  switch (productKey) {
    case 'jira-servicedesk.ondemand': return getJSDSurvey(i18n);
    case 'jira-software.ondemand': return getJSWSurvey(i18n);
    case 'jira-core.ondemand': return getJSWSurvey(i18n);
    default: return getDefaultSurvey(productName, i18n);
  }
};

export default getSurvey;
