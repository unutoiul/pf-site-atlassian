import { PaymentDetailsForm } from 'bux/features/payment-details/components';
import { withPageLoader } from 'bux/components/withLoader';
import { isLoaded } from 'bux/pages/PaymentDetail/selectors';
import { getPaymentDetailsData } from 'bux/pages/PaymentDetail/actions';

export default withPageLoader(isLoaded, getPaymentDetailsData)(PaymentDetailsForm);
