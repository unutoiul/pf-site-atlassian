import PropTypes from 'prop-types';
import { Trans } from '@lingui/react';
import React, { Component } from 'react';
import cx from 'classnames';
import AutoFocus from 'bux/components/behaviour/focus/AutoFocus';
import LinkButton from 'bux/components/Link/Button';
import EditionBlock from '../EditionBlock';
import ComparePlans from './components/banana-comparison-table';
import { EditionPropType } from '../propTypes';

import styles from '../styles.less';

class SelectEditionPage extends Component {
  static propTypes = {
    entitlement: PropTypes.object.isRequired,
    editions: PropTypes.arrayOf(EditionPropType).isRequired,
    next: PropTypes.func.isRequired,
    renewalFrequency: PropTypes.string.isRequired,
    showingComparisonTable: PropTypes.bool,
  };

  state = {
    showingComparisonTable: this.props.showingComparisonTable || false
  };

  showComparisonTable = () => {
    this.setState({
      showingComparisonTable: true
    });
  };

  render() {
    const {
      entitlement, editions, next, renewalFrequency
    } = this.props;
    const { name } = entitlement;
    const { showingComparisonTable } = this.state;
    return (
      <div className={styles.editionsScroller}>
        <h1>
          <Trans id="billing.change-edition.select-edition.title">{name} plans & pricing</Trans>
        </h1>
        <AutoFocus>
          <div className={styles.editionsSubScroller}>
            { editions.map((edition, editionIndex) => (
              <EditionBlock
                entitlement={entitlement}
                key={editionIndex}
                edition={edition}
                onEditionChange={() => next({ edition })}
                renewalFrequency={renewalFrequency}
              />
            )) }
          </div>
        </AutoFocus>

        <div className={styles.comparePlans}>
          {
            showingComparisonTable
              ? <span className={styles.comparePlans}>
                  <Trans id="billing.change-edition.select-edition.compare-plans.text">Compare plans</Trans>
                </span>
              : <LinkButton
                className={cx('comparisonTableLink', styles.comparePlansActive)}
                onClick={this.showComparisonTable}
              >
                <Trans id="billing.change-edition.select-edition.compare-plans.button">Compare plans</Trans>
              </LinkButton>
          }
        </div>
        { showingComparisonTable && <ComparePlans editions={editions} /> }
      </div>
    );
  }
}

export default SelectEditionPage;
