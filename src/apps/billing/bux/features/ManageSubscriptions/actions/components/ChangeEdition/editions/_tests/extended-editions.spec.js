import { expect } from 'chai';
import extendEditions from '../extended-editions';

describe('extendEditions', () => {
  it('should extend editions with information', () => {
    const extendedEditions = extendEditions({
      currentEdition: 'free',
      availableEditions: [{
        edition: 'plus-plus',
        someFlag: true
      }, {
        edition: 'standard'
      }]
    });
    expect(extendedEditions[0]).to.be.deep.include({
      cost: {
        monthly: 0,
        annual: 0,
        currency: 'usd'
      },
      edition: 'free',
      isCurrent: true,
      needsPaymentDetailsOnAccount: false,
      transition: 'EQUAL'
    });
    expect(extendedEditions[1]).to.be.deep.include({
      edition: 'plus-plus',
      isCurrent: false,
      someFlag: true
    });
    expect(extendedEditions[2]).to.be.deep.include({
      cost: {
        monthly: 3,
        annual: 30,
        currency: 'usd'
      },
      edition: 'standard',
      isCurrent: false,
      needsPaymentDetailsOnAccount: true
    });
  });
});
