import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow, mount } from 'enzyme';
import transitions from '../../transitions';
import SelectEdition from '../SelectEdition';
import EditionBlock from '../../EditionBlock';
import ComparisonTable from '../components/banana-comparison-table';

describe('SelectEdition component', () => {
  const getEdition = () => ({
    name: 'edition1',
    edition: 'edition1',
    cost: {
      monthly: 10,
      annual: 1000,
      currency: 'usd'
    },
    features: ['feat 1', 'feat 2'],
    transition: transitions.DOWNGRADE,
    needsPaymentDetailsOnAccount: true
  });

  const entitlement = { name: 'Banana' };

  const PROPS = {
    entitlement,
    renewalFrequency: 'MONTHLY',
    editions: [],
    next: () => {
    }
  };

  it('should render editions', () => {
    const editions = [
      getEdition(),
      getEdition(),
      getEdition(),
      getEdition()
    ];
    const next = sinon.spy();
    const wrapper = shallow(<SelectEdition {...PROPS} editions={editions} next={next} />);
    expect(wrapper.find(EditionBlock)).to.have.length(4);
    const block = wrapper.find(EditionBlock).at(0);
    expect(block).to.have.prop('entitlement', entitlement);
    expect(block).to.have.prop('edition', editions[0]);
    block.props().onEditionChange();

    expect(next).to.be.calledWithMatch({ edition: editions[0] });
  });

  it('should autofocus on button', () => {
    const editions = [
      getEdition(),
    ];
    const next = sinon.spy();

    mount(<SelectEdition {...PROPS} editions={editions} next={next} />);
    expect(document.activeElement.innerHTML).to.contain('Downgrade');
  });

  it('should contain comparison table', () => {
    const wrapper = shallow(<SelectEdition {...PROPS} />);
    expect(wrapper.find('.comparisonTableLink')).to.be.present();
    expect(wrapper.find(ComparisonTable)).not.to.be.present();
    wrapper.find('.comparisonTableLink').simulate('click');
    expect(wrapper.find('.comparisonTableLink')).not.to.be.present();
    expect(wrapper.find(ComparisonTable)).to.be.present();
  });
});