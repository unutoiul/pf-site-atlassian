import React from 'react';
import sinon from 'sinon';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';
import { Survey } from 'bux/components/Survey/Survey';
import getAnalytic from 'bux/components/log/helpers/getAnalytic';

import { ConfirmComponent } from '../Confirm';

const DEFAULT_PROPS = {
  entitlement: {
    name: 'Banana',
  },
  next: sinon.spy(),
  back: sinon.spy(),
  i18n: {
    t: () => value => (value)
  }
};

const confirmTextFieldSelector = '.deactivateConfirmation'; 
const surveySelector = '.deactivateSurvey';

describe('Deactivate page', () => {
  it('should display product name', () => {
    const wrapper = shallow(<ConfirmComponent {...DEFAULT_PROPS} />);
    expect(wrapper).to.contain.text('Banana');
  });

  it('should hide survey when confirmation is empty', () => {
    const wrapper = shallow(<ConfirmComponent {...DEFAULT_PROPS} />);
    expect(wrapper.find(surveySelector)).not.to.be.present();
  });

  it('should display survey when confirmation is filled correctly', () => {
    const wrapper = mount(<ConfirmComponent {...DEFAULT_PROPS} />, getAnalytic().context);
    const confirmInput = wrapper.find(confirmTextFieldSelector);

    confirmInput.hostNodes().simulate('change', { target: { name: 'confirm', value: 'unsubscribe' } });
    expect(wrapper.find(surveySelector)).to.be.present();
  });

  it('should contain standard survey options', () => {
    const wrapper = mount(<ConfirmComponent {...DEFAULT_PROPS} />, getAnalytic().context);
    const confirmInput = wrapper.find(confirmTextFieldSelector);
    confirmInput.hostNodes().simulate('change', { target: { name: 'confirm', value: 'unsubscribe' } });
    expect(wrapper.find(Survey).props().options[0].items).to.have.length(4);
  });

  it('should contain JSD survey options', () => {
    const entitlement = {
      entitlement: {
        name: 'JSD',
        productKey: 'jira-servicedesk.ondemand',
      },
    };
    const wrapper = mount(<ConfirmComponent {...DEFAULT_PROPS} {...entitlement} />, getAnalytic().context);
    const confirmInput = wrapper.find(confirmTextFieldSelector);
    confirmInput.hostNodes().simulate('change', { target: { name: 'confirm', value: 'unsubscribe' } });
    expect(wrapper.find(Survey).props().options[0].items).to.have.length(7);
  });

  it('should contain JSW survey options for Jira-software', () => {
    const entitlement = {
      entitlement: {
        name: 'JSW',
        productKey: 'jira-software.ondemand',
      },
    };
    const wrapper = mount(<ConfirmComponent {...DEFAULT_PROPS} {...entitlement} />, getAnalytic().context);
    const confirmInput = wrapper.find(confirmTextFieldSelector);
    confirmInput.hostNodes().simulate('change', { target: { name: 'confirm', value: 'unsubscribe' } });
    expect(wrapper.find(Survey).props().options).to.have.length(2);
  });

  it('should contain JSW survey options for Jira-Core', () => {
    const entitlement = {
      entitlement: {
        name: 'JiraCode',
        productKey: 'jira-core.ondemand',
      },
    };
    const wrapper = mount(<ConfirmComponent {...DEFAULT_PROPS} {...entitlement} />, getAnalytic().context);
    const confirmInput = wrapper.find(confirmTextFieldSelector);
    confirmInput.hostNodes().simulate('change', { target: { name: 'confirm', value: 'unsubscribe' } });
    expect(wrapper.find(Survey).props().options).to.have.length(2);
  });

  it('should display survey when for confirmation for deletion', () => {
    const entitlement = {
      entitlement: {
        name: 'JiraCode',
        productKey: 'jira-core.ondemand',
      },
    };
    const wrapper = mount(<ConfirmComponent {...DEFAULT_PROPS} {...entitlement} deletion />, getAnalytic().context);
    const confirmInput = wrapper.find(confirmTextFieldSelector);
    confirmInput.hostNodes().simulate('change', { target: { name: 'confirm', value: 'delete' } });
    expect(wrapper.find(Survey).props().options).to.have.length(2);
  });

  it('should contain custom deactivation message if it is available', () => {
    const entitlement = {
      entitlement: {
        name: 'Identity Manager',
        productKey: 'com.atlassian.identity-manager',
      },
    };
    const wrapper = mount(<ConfirmComponent {...DEFAULT_PROPS} {...entitlement} />, getAnalytic().context);
    expect(wrapper).to.contain.text('Managed users will not be able to use SAML');
  });
});

