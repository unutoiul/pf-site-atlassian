import productsEditions from 'bux/common/data/products-editions';
import transitions from '../transitions';

const editions = productsEditions['hipchat.cloud'];
const sortOrder = {
  DOWNGRADE: -1,
  EQUAL: 0,
  UPGRADE: 1
};

const zeroCost = {
  amount: 0
};

const currentEditionDefaults = {
  isCurrent: true,
  transition: transitions.EQUAL,
};

const lookupTable = {};
editions.forEach(edition => (lookupTable[edition.edition] = edition));

const sortEditions = (a, b) => {
  const diff = sortOrder[a.transition] - sortOrder[b.transition];
  if (diff) {
    return diff;
  }
  return (a.cost || zeroCost).amount - (b.cost || zeroCost).amount;
};

const mergeData = ({ currentEdition, availableEditions = [] }) => {
  const result = [];

  if (currentEdition) {
    result.push({
      ...lookupTable[currentEdition],
      ...currentEditionDefaults
    });
  }

  availableEditions.forEach((transition) => {
    result.push({
      ...lookupTable[transition.edition],
      ...transition,
      isCurrent: false
    });
  });
  return result;
};

const extendEditions = entitlementEdition =>
  mergeData(entitlementEdition)
    .sort(sortEditions);

export default extendEditions;
