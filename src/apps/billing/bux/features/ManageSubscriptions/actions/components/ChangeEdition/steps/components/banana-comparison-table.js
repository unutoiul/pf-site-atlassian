import { Trans } from '@lingui/react';
import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';

import CheckIcon from '@atlaskit/icon/glyph/check';
import styles from './banana-comparison-table.less';
import { EditionPropType } from '../../propTypes';

const productEditions = ['free', 'standard'];

const products = [
  { key: 'free', name: <Trans id="billing.comparison-table.stride.free">Stride Free</Trans> },
  { key: 'standard', name: <Trans id="billing.comparison-table.stride.standard">Stride Standard</Trans> },
];

const features = {
  'unlimited-users': {
    label: <Trans id="billing.comparison-table.stride.unlimited-users">Unlimited users</Trans>,
    values: [true, true]
  },
  'unlimited-rooms': {
    label: <Trans id="billing.comparison-table.stride.unlimited-rooms">Unlimited group chat rooms</Trans>,
    values: [true, true]
  },
  'unlimited-messages': {
    label: <Trans id="billing.comparison-table.stride.unlimited-messages">Unlimited direct messaging</Trans>,
    values: [true, true]
  },
  'unlimited-video': {
    label: <Trans id="billing.comparison-table.stride.unlimited-video">Unlimited group video meetings</Trans>,
    values: [true, true]
  },
  'unlimited-voice': {
    label: <Trans id="billing.comparison-table.stride.unlimited-voice">Unlimited voice meetings</Trans>,
    values: [true, true]
  },
  'built-in-collab-tools': {
    label: <Trans id="billing.comparison-table.stride.built-in-collab-tools">Built in collaboration tools</Trans>,
    values: [true, true]
  },
  'screen-sharing': {
    label: <Trans id="billing.comparison-table.stride.screen-sharing">Group screen sharing</Trans>,
    values: [false, true]
  },
  'remote-desktop': {
    label: <Trans id="billing.comparison-table.stride.remote-desktop">Remote desktop control</Trans>,
    values: [false, true]
  },
  'apps-and-bots': {
    label: <Trans id="billing.comparison-table.stride.apps-and-bots">Apps and bots</Trans>,
    values: [
      10,
      <Trans id="billing.comparison-table.stride.apps-and-bots.unlimited">Unlimited</Trans>
    ]
  },
  'file-storage': {
    label: <Trans id="billing.comparison-table.stride.file-storage">File storage & sharing</Trans>,
    values: [
      '5 GB',
      <Trans id="billing.comparison-table.stride.file-storage.unlimited">Unlimited</Trans>
    ]
  },
  'message-history': {
    label: <Trans id="billing.comparison-table.stride.message-history">Message history</Trans>,
    values: [
      <Trans id="billing.comparison-table.stride.message-history.amount">25K messages</Trans>,
      <Trans id="billing.comparison-table.stride.message-history.unlimited">Unlimited</Trans>
    ]
  },
};

const filterProduct = (index, editions) => {
  const productEdition = productEditions[index];
  return editions.find(({ edition }) => edition === productEdition);
};

const displayOption = (option) => {
  if (option === true) {
    return (
      <div className={cx('icon-supported', styles.supportedIcon)}>
        <CheckIcon label="Supported" size="medium" />
      </div>
    );
  } else if (option === false) {
    return '';
  }
  return option;
};

const ComparePlans = ({ editions }) => (
  <div className={cx('banana-comparison-table', styles.table)}>

    <div className={cx('comparison-header', styles.row)}>
      <span className={styles.caption}>
        <Trans id="billing.change-edition.select-edition.comparison.features">Features</Trans>
      </span>
      { products.map(({ key, name }, index) => filterProduct(index, editions) &&
      <span key={key} className={styles.product}>{name}</span>)}
    </div>

    {
      Object.keys(features).map(feature => (
        <div key={feature} className={cx('comparison-row', styles.row)}>
          <span className={styles.feature}>{features[feature].label}</span>
          {
            features[feature].values.map((option, index) =>
              filterProduct(index, editions) &&
              <span key={index} className={styles.option}>{displayOption(option)}</span>)
          }
        </div>
      ))
    }
  </div>
);

ComparePlans.propTypes = {
  editions: PropTypes.arrayOf(EditionPropType).isRequired
};


export default ComparePlans;
