import { storiesOf } from '@storybook/react';
import { produce } from 'immer';

import { inPageChrome, mockedFunction, render } from 'bux/common/helpers/storybook';

import Confirm from './Confirm';

const props = {
  entitlement: {
    id: 'b7db7827-e2ac-492e-a3bb-e1c310764043',
    name: 'Stride',
    productKey: 'hipchat.cloud',
  },
  confirmed: true,
  deletion: false,
  allOpen: true,
  next: mockedFunction,
  back: mockedFunction,
};

storiesOf('BUX|Focused tasks/Deactivate/Step 1: Confirm', module)
  .add('Default', () => inPageChrome(render(Confirm, props)))
  .add('Deletion', () => inPageChrome(render(Confirm, produce(props, (draft) => {
    draft.deletion = true;
  }))))
  .add('Service Desk', () => inPageChrome(render(Confirm, produce(props, (draft) => {
    draft.entitlement.productKey = 'jira-servicedesk.ondemand';
  }))))
  .add('Jira Software', () => inPageChrome(render(Confirm, produce(props, (draft) => {
    draft.entitlement.productKey = 'jira-software.ondemand';
  }))))
  .add('Jira Core', () => inPageChrome(render(Confirm, produce(props, (draft) => {
    draft.entitlement.productKey = 'jira-core.ondemand';
  }))));
