import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { mount } from 'enzyme';
import Money from 'bux/components/Money';
import Button, { SubmitButton } from 'bux/components/Button';
import Loading from 'bux/components/Loading';

import EditionBlock from '../EditionBlock';
import transitions from '../transitions';

describe('EditionBlock component', () => {
  const getEdition = transition => ({
    name: 'edition1',
    edition: 'edition2',
    cost: {
      monthly: 10,
      annual: 100,
      currency: 'usd',
    },
    features: ['feat 1', 'feat 2'],
    transition,
    needsPaymentDetailsOnAccount: true,
  });

  const PROPS = {
    entitlement: { name: 'Banana' },
    renewalFrequency: 'MONTHLY',
  };

  const render = (transition = transitions.DOWNGRADE, extras = {}) =>
    mount(<EditionBlock {...PROPS} {...extras} edition={getEdition(transition)} />);

  it('should render cost of edition', () => {
    const money = render().find(Money);
    expect(money).to.have.prop('amount', 10);
    expect(money).to.have.prop('currency', 'usd');
  });

  it('should render name', () => {
    const wrapper = render();
    expect(wrapper.find('h2')).to.contain.text('Banana edition1');
  });

  it('should render features block', () => {
    const wrapper = render();
    expect(wrapper).to.contain.text('feat 1');
    expect(wrapper).to.contain.text('feat 2');
  });

  it('should render downgrade text for downgrade transition', () => {
    const wrapper = render();
    expect(wrapper.find(SubmitButton)).to.have.text('Downgrade');
  });

  it('should render upgrade text for upgrade transition', () => {
    const wrapper = render(transitions.UPGRADE);
    expect(wrapper.find(SubmitButton)).to.have.text('Upgrade');
  });

  it('should render Your plan text for current edition', () => {
    const wrapper = render(transitions.EQUAL);
    expect(wrapper.find(Button)).to.have.text('Your plan');
  });

  it('should render action button', () => {
    const action = sinon.spy();
    const wrapper = render(transitions.DOWNGRADE, { onEditionChange: action });
    wrapper.find(SubmitButton).props().onClick();
    expect(action).to.be.called();
  });

  it('should disable button for current edition', () => {
    const wrapper = render(transitions.EQUAL);
    expect(wrapper.find(Button)).to.have.prop('disabled');
  });

  it('should indicate processing state and disable button', () => {
    const wrapper = render(transitions.DOWNGRADE, { indicateProcessing: true });
    expect(wrapper.find(SubmitButton)).to.have.text('Downgrading');
    expect(wrapper.find(SubmitButton)).to.have.prop('disabled');
  });

  it('should not show spinner in normal state', () => {
    const wrapper = render();
    expect(wrapper.find(Loading)).not.to.be.present();
  });
});
