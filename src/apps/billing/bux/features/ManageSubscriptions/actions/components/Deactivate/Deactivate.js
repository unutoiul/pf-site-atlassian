import { withI18n } from '@lingui/react';
import PropTypes from 'prop-types';
import React from 'react';
import { Analytics } from 'bux/components/Analytics';
import withPageLevelLog from 'bux/components/log/with/pageLevel';
import DocumentTitle from 'bux/components/DocumentTitle';
import { Flow, Step } from 'bux/components/flow';

import Confirm from './steps/Confirm';
import Deactivating from './steps/Deactivating';

const title = (i18n, deletion) => (
  deletion
    ? i18n.t('billing.deactivate.title.delete')`Delete`
    : i18n.t('billing.deactivate.title.unsubscribe')`Unsubscribe`
);

const DeactivationFlow = ({
  entitlement, closeAction, deletion, i18n
}) => (
  <DocumentTitle title={title(i18n, deletion)}>
    <Flow initialState={{ entitlement }} onCancel={closeAction} onComplete={closeAction}>
      <Step stepState={{ deletion }}>
        {stepProps => (
          <Analytics.Screen 
            name="billingApplicationDeleteScreen"
            attributes={{
              productDelete: entitlement.productKey
            }}
          >
            <Confirm {...stepProps} />
          </Analytics.Screen>        
        )}
      </Step>
      <Step>
        {stepProps => (
          <Analytics.Screen 
            name="billingApplicationDeleteDeactivatingScreen"
            attributes={{
              productDelete: entitlement.productKey
            }}
          >
            <Deactivating {...stepProps} />
          </Analytics.Screen>        
        )}
      </Step>
    </Flow>
  </DocumentTitle>
);

DeactivationFlow.propTypes = {
  closeAction: PropTypes.func,
  entitlement: PropTypes.object,
  deletion: PropTypes.bool,
  i18n: PropTypes.object,
};

export default withPageLevelLog(withI18n()(DeactivationFlow));

