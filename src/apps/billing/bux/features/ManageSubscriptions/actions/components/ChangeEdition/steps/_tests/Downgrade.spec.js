import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import moment from 'moment';
import { DowngradeComponent } from '../Downgrade';
import transitions from '../../transitions';

describe('Downgrade component', () => {
  it('should render the text correctly', () => {
    const entitlement = {
      name: 'Stride',
      endDate: moment.utc().startOf('day').add(5, 'days')
    };
    const edition = {
      name: 'new',
      edition: 'newEdition',
      cost: {
        amount: 5,
        currency: 'usd'
      },
      features: ['feat 1', 'feat 2'],
      transition: transitions.DOWNGRADE,
      needsPaymentDetailsOnAccount: true
    };
    const activeEdition = {
      name: 'Current edition',
      edition: 'currentEdition',
      cost: {
        amount: 10,
        currency: 'usd'
      },
      features: ['feat 3'],
      transition: transitions.DOWNGRADE,
      needsPaymentDetailsOnAccount: true
    };

    const PROPS = {
      entitlement, edition, activeEdition, next: () => {}, prev: () => {}
    };

    const wrapper = mount(<DowngradeComponent {...PROPS} />);

    expect(wrapper).to.contain.text('You are about to downgrade Stride');
    expect(wrapper).to.contain.text('You will be downgraded to Stride new at the end of your billing period in 5 days');
    expect(wrapper).to.contain.text('Your users will lose access to Stride Current edition features on this date');
  });
});
