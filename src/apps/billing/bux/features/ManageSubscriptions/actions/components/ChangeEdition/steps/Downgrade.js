import PropTypes from 'prop-types';
import { I18n, Trans } from '@lingui/react';
import React from 'react';
import cx from 'classnames';
import connectLogRoot from 'bux/components/log/root';
import { Survey } from 'bux/components/Survey';
import Date, { DaysRemaining } from 'bux/components/Date';
import AutoFocus from 'bux/components/behaviour/focus/AutoFocus';
import { EditionPropType } from '../propTypes';

import styles from './style.less';

const getDefaultOptions = (i18n, productName, editionName) => ({
  type: 'radio',
  items: [
    {
      key: 'not-using',
      label: <Trans id="billing.change-edition.downgrade.options.not-using">
        I'm not using {productName} {editionName} features enough to pay for them
      </Trans>,
    },
    {
      key: 'lacking-features',
      label: <Trans id="billing.change-edition.downgrade.options.lacking-features">
        {productName} {editionName} does not include all the features I want
      </Trans>
    },
    {
      key: 'other',
      label: <Trans id="billing.change-edition.downgrade.options.other">Other</Trans>,
      nested: { ask: { placeholder: i18n.t('billing.change-edition.downgrade.options.ask')`Tell us more` } }
    },
  ]
});

export const DowngradeComponent = (props) => {
  const {
    entitlement, edition, activeEdition, next, prev, allOpen
  } = props;
  const { name: entitlementName, endDate } = entitlement;
  const { name: editionName } = edition;
  const { name: activeEditionName } = activeEdition;
  const timeRemaining = <b><DaysRemaining value={endDate} /></b>;
  const formattedDate = <Date value={endDate} />;

  return (
    <div className={cx(styles.page, 'downgradeFocusedTask')}>
      <h3>
        <b><Trans id="billing.change-edition.downgrade.title">You are about to downgrade {entitlementName}</Trans></b>
      </h3>
      <div>
        <p>
          <Trans id="billing.change-edition.downgrade.billing-period">
            You will be downgraded to {entitlementName} {editionName} at the end of your billing period
            in {timeRemaining} ({formattedDate}).
          </Trans>
        </p>
        <p>
          <Trans id="billing.change-edition.downgrade.users-info">
            Your users will lose access to {entitlementName} {activeEditionName} features on this date.
          </Trans>
        </p>
      </div>
      <p>
        <Trans id="billing.change-edition.downgrade.reason">
          If you have a moment, please tell us your main reason for downgrading.
        </Trans>
      </p>
      <AutoFocus>
        <I18n>{({ i18n }) => (
          <Survey
            submitAction={next}
            submitLabel={<Trans id="billing.change-edition.downgrade.survey.submit">Downgrade</Trans>}
            cancelAction={prev}
            cancelLabel={<Trans id="billing.change-edition.downgrade.survey.cancel">Back</Trans>}
            options={getDefaultOptions(i18n, entitlementName, activeEditionName)}
            allOpen={allOpen}
          />
        )}</I18n>
      </AutoFocus>
    </div>
  );
};

DowngradeComponent.propTypes = {
  entitlement: PropTypes.object.isRequired,
  edition: EditionPropType.isRequired,
  activeEdition: EditionPropType.isRequired,
  next: PropTypes.func.isRequired,
  prev: PropTypes.func.isRequired,
  allOpen: PropTypes.bool,
};

export default connectLogRoot('downgrade')(DowngradeComponent);
