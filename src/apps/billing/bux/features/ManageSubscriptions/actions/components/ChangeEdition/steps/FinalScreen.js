import PropTypes from 'prop-types';
import React from 'react';
import EditionBlock from '../EditionBlock';
import { EditionPropType } from '../propTypes';

const FinalScreen = ({ entitlement, edition, renewalFrequency }) => (
  <EditionBlock
    entitlement={entitlement}
    edition={{
      ...edition,
      isCurrent: true
    }}
    indicateProcessing
    renewalFrequency={renewalFrequency}
  />
);

FinalScreen.propTypes = {
  entitlement: PropTypes.object.isRequired,
  edition: EditionPropType.isRequired,
  renewalFrequency: PropTypes.string.isRequired,
};

export default FinalScreen;