import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import cx from 'classnames';
import withPageLevelLog from 'bux/components/log/with/pageLevel';
import { push } from 'bux/core/state/router/actions';
import {
  changeEdition,
  refreshStoreAfterChangeEdition
} from 'bux/features/ManageSubscriptions/components/ChangeEdition/actions';
import { EditionPropType } from './propTypes';
import steps from './steps';
import transitions from './transitions';
import SelectEditionPage from './steps/SelectEdition';
import Downgrade from './steps/Downgrade';
import FinalScreen from './steps/FinalScreen';
import PaymentDetailsWrapper from './steps/PaymentDetailsWrapper';

import styles from './styles.less';

export class ChangeEditionWizard extends Component {
  static propTypes = {
    editions: PropTypes.arrayOf(EditionPropType).isRequired,
    entitlement: PropTypes.object.isRequired,
    closeAction: PropTypes.func.isRequired,
    changeEdition: PropTypes.func.isRequired,
    refreshStoreAfterChangeEdition: PropTypes.func.isRequired,
    hasPaymentDetails: PropTypes.bool.isRequired,
    renewalFrequency: PropTypes.string.isRequired,
    upgradeWithNewFlow: PropTypes.func,
    redirect: PropTypes.func.isRequired,
    showingComparisonTable: PropTypes.bool
  };

  static contextTypes = {
    sendAnalyticEvent: PropTypes.func
  };

  state = {
    edition: null,
    currentStep: steps.SELECT_EDITION,
    stepStack: [],
  };

  shouldComponentUpdate(nextProps, nextState) {
    return this.state.currentStep !== nextState.currentStep;
  }

  shouldChangeEdition() {
    switch (this.state.currentStep) {
      case steps.CHANGING_EDITION:
        this.props.changeEdition({ edition: this.state.edition, entitlement: this.props.entitlement })
          .then(this.props.refreshStoreAfterChangeEdition)
          .then(this.props.closeAction)
          .then(() => this.context.sendAnalyticEvent('changedTo', {
            edition: this.state.edition.name
          }))
          .catch(() => this.back());
        break;
      default: break;
    }
  }

  edges = {
    [steps.SELECT_EDITION]: ({ edition }) => {
      switch (edition.transition) {
        case transitions.DOWNGRADE: return steps.DOWNGRADE_CONFIRMATION;
        case transitions.UPGRADE: return this.needsPaymentDetails(edition)
          ? steps.ADD_PAYMENT_DETAILS
          : steps.CHANGING_EDITION;
        default: return steps.SELECT_EDITION;
      }
    },
    [steps.DOWNGRADE_CONFIRMATION]: () => steps.CHANGING_EDITION,
    [steps.ADD_PAYMENT_DETAILS]: () => steps.CHANGING_EDITION,
  };

  next = (params) => {
    const { currentStep, stepStack } = this.state;
    const { upgradeWithNewFlow } = this.props;

    if (!upgradeWithNewFlow || upgradeWithNewFlow(params)) {
      const nextStep = this.edges[currentStep](params);

      if (nextStep === steps.ADD_PAYMENT_DETAILS) {
        this.props.redirect('/paymentdetails/add');
        return;
      }

      if (currentStep === steps.SELECT_EDITION) {
        this.context.sendAnalyticEvent('changeTo', {
          edition: params.edition.name
        });
      }

      if (nextStep !== currentStep) {
        stepStack.push(currentStep);

        this.setState({
          ...params,
          currentStep: nextStep,
          stepStack,
        }, this.shouldChangeEdition);
      }
    }
  };

  back = () => {
    const { closeAction } = this.props;
    const { stepStack } = this.state;
    const currentStep = stepStack.pop();

    if (!currentStep) {
      closeAction();
      return;
    }

    this.setState({
      currentStep,
      stepStack
    });
  };

  needsPaymentDetails = edition => edition.needsPaymentDetailsOnAccount && !this.props.hasPaymentDetails;

  render() {
    const {
      entitlement, editions, renewalFrequency, showingComparisonTable
    } = this.props;
    const { currentStep, edition } = this.state;
    const findActiveEdition = () => editions.find(e => (e.edition === entitlement.selectedEdition));

    const pages = {
      [steps.SELECT_EDITION]: () =>
        (<SelectEditionPage
          entitlement={entitlement}
          editions={editions}
          renewalFrequency={renewalFrequency}
          showingComparisonTable={showingComparisonTable}
          next={this.next}
        />),
      [steps.DOWNGRADE_CONFIRMATION]: () =>
        (<Downgrade
          entitlement={entitlement}
          edition={edition}
          activeEdition={findActiveEdition()}
          next={this.next}
          prev={this.back}
        />),
      [steps.CHANGING_EDITION]: () =>
        <FinalScreen entitlement={entitlement} edition={edition} renewalFrequency={renewalFrequency} />,
      [steps.ADD_PAYMENT_DETAILS]: () =>
        <PaymentDetailsWrapper next={this.next} />,
    };

    return (
      <div className={cx(styles.changeEditionsPage, 'changeEditionFocusedTask')}>
        <div className={`edition-step-${currentStep}`}>
          { pages[currentStep]() }
        </div>
      </div>
    );
  }
}

export const mapDispatchToProps = {
  changeEdition,
  refreshStoreAfterChangeEdition,
  redirect: push
};

export const WithLog = withPageLevelLog(ChangeEditionWizard);

const ConnectedChangeEditionWizard = connect(
  null,
  mapDispatchToProps
)(WithLog);

export default ConnectedChangeEditionWizard;
