import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { mount, shallow } from 'enzyme';
import { Provider } from 'bux/helpers/redux';
import proxyquire from 'bux/helpers/proxyquire';
import { i18n } from 'bux/helpers/jsLingui';
import createStore from 'bux/core/createStore';
import getAnalytic from 'bux/components/log/helpers/getAnalytic';

import { ChangeEditionDataProvider } from '../ChangeEditionAdapter';
import ChangeEditionComponent from '../ChangeEdition/ChangeEdition';

describe('ChangeEditionAdapter component', () => {
  const PROPS = {
    edition: {},
    entitlement: { id: 42 },
    closeAction: () => {},
    hasPaymentDetails: false,
    renewalFrequency: 'MONTHLY',
    i18n
  };

  describe('should fetch data ', () => {
    let ChangeEditionAdapter;

    let getEntitlementEditionMetadata;
    let getEntitlementEditionSelector;
    let getEntitlementEditionAction;
    let getNextBillingPeriodRenewalFrequency;


    beforeEach(() => {
      getEntitlementEditionMetadata = sinon.stub();
      getEntitlementEditionSelector = sinon.stub();
      getNextBillingPeriodRenewalFrequency = sinon.stub();
      // action creator will return a function to not fire any action
      getEntitlementEditionAction = sinon.stub().returns(() => {});

      ChangeEditionAdapter = proxyquire.load('../ChangeEditionAdapter', {
        'bux/core/state/entitlement-group': {
          default: {
            selectors: {
              getEntitlementEditionMetadata,
              getEntitlementEdition: getEntitlementEditionSelector
            },
            actions: {
              getEntitlementEdition: getEntitlementEditionAction
            }
          }
        },
        'bux/core/state/bill-estimate': {
          default: {
            selectors: {
              getNextBillingPeriodRenewalFrequency,
            },
          }
        }
      }).default;
    });

    it('should call metadata selector', () => {
      getEntitlementEditionMetadata.returns(() => ({ loading: true }));
      getEntitlementEditionSelector.returns(() => ({ }));

      const wrapper = mount(<Provider store={createStore()}><ChangeEditionAdapter {...PROPS} /></Provider>);

      expect(getEntitlementEditionMetadata).to.be.calledWithMatch(42);
      expect(getEntitlementEditionAction).to.be.calledWithMatch(42);
      expect(getEntitlementEditionSelector).not.to.be.called();

      expect(wrapper.find(ChangeEditionDataProvider)).not.to.be.present();
    });

    it('should mount payload', () => {
      getEntitlementEditionMetadata.returns(() => ({ loading: false, display: true }));
      getEntitlementEditionSelector.returns(() => ({ }));
      getNextBillingPeriodRenewalFrequency.returns('MONTHLY');
      const { context } = getAnalytic();
      const wrapper = mount(<Provider store={createStore()}><ChangeEditionAdapter {...PROPS} /></Provider>, context);
      expect(wrapper.find(ChangeEditionComponent)).to.be.present();
      expect(getEntitlementEditionSelector).to.be.calledWithMatch(42);
    });
  });

  it('should display changeEdition after load', () => {
    const wrapper = shallow(<ChangeEditionDataProvider {...PROPS} />);
    expect(wrapper.find(ChangeEditionComponent)).to.be.present();
  });

  it('should extend initial editions', () => {
    const edition = { currentEdition: 'free' };
    const wrapper = shallow(<ChangeEditionDataProvider {...PROPS} edition={edition} />);
    expect(wrapper.find(ChangeEditionComponent).props().editions[0]).to.deep.include({
      cost: {
        annual: 0,
        currency: 'usd',
        monthly: 0,
      },
      edition: 'free',
      isCurrent: true,
      needsPaymentDetailsOnAccount: false,
      transition: 'EQUAL'
    });
  });
});
