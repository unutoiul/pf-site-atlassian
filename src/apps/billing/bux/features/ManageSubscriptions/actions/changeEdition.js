import { openFocusedTask } from 'bux/core/state/focused-task/actions';
import { push } from 'bux/core/state/router/actions';
import library from '../../FocusedTask/library';

import ChangeEdition from './components/ChangeEditionAdapter';

const component = library.add(ChangeEdition);

const openDrawer = props => openFocusedTask({
  component,
  props,
  logPrefix: `manageSubscription.${props.entitlement.productKey}.changeEdition`
});

const openNewExperience = ({ entitlement }) =>
  push(`/applications/change-edition/${entitlement.id}`);

const action = ({ dispatch, entitlement, featureFlags }) => (
  featureFlags['new-change-edition-flow']
    ? dispatch(openNewExperience({ entitlement }))
    : dispatch(openDrawer({ entitlement }))
);

export const changeEdition = {
  label: i18n => i18n.t('billing.manage-subscriptions.actions.change-plan')`Change plan`,
  action,
  condition: ({ editionTransitions = [] }) => (
    editionTransitions.length > 0
  )
};

export const upgrade = {
  label: i18n => i18n.t('billing.manage-subscriptions.actions.upgrade-plan')`Upgrade plan`,
  action,
  condition: ({ editionTransitions = [] }) => (
    editionTransitions.length === 1 && editionTransitions[0] === 'UPGRADE'
  ),
  primaryAction: true,
};
