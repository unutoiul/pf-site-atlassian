import { expect } from 'chai';
import library from '../library';

describe('FocusedTask library tests: ', () => {
  it('should should add element with different IDS', () => {
    const id1 = library.add('hello world');
    const id2 = library.add('hello world');
    expect(id1).not.to.be.equal(id2);
  });

  it('should should return element by id', () => {
    const id = library.add('hello my world');
    expect(library.get(id)).to.be.equal('hello my world');
  });

  it('should should delete element by id', () => {
    const id = library.add('hello my world');
    expect(library.get(id)).to.be.equal('hello my world');
    library.remove(id);
    expect(library.get(id)).to.be.undefined();
  });
});
