import PropTypes from 'prop-types';
import React from 'react';
import AKButton from '@atlaskit/button';
import CrossIcon from '@atlaskit/icon/glyph/cross';

import { ScrollLocky } from 'react-scroll-locky';

import FocusLock from 'bux/components/behaviour/focus/FocusLock';
import { Analytics } from 'bux/components/Analytics';

import styles from './styles.less';

const MobileDrawerWrapper = ({ children, closeAction }) => (
  <div className="bux-focused-drawer">
    <ScrollLocky>
      <FocusLock>
        <div className={styles.mobileWrapper}>
          <Analytics.UI as="focusTaskClose">
            <AKButton
              appearance="subtle"
              onClick={closeAction}
              iconBefore={<CrossIcon label="Cross icon" size="medium" />}
            >
              close
            </AKButton>
          </Analytics.UI>
          {children}
        </div>
      </FocusLock>
    </ScrollLocky>
  </div>
);

MobileDrawerWrapper.propTypes = {
  children: PropTypes.node,
  closeAction: PropTypes.func
};

export default MobileDrawerWrapper;
