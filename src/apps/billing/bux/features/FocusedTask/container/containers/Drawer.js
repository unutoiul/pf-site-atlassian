import React from 'react';
import PropTypes from 'prop-types';
import { Media } from 'common/responsive/Matcher';

import DrawerLoader from './loaders/DrawerWrapper';
import MobileLoader from './loaders/MobileWrapper';
import TaskLoader from './TaskLoader';

export const DrawerWrapper = props => (
  <Media.Matcher
    mobile={<MobileLoader {...props} onError={props.closeAction} />}
    tablet={<DrawerLoader {...props} onError={props.closeAction} />}
  />
);

DrawerWrapper.propTypes = {
  closeAction: PropTypes.func
};

const FocusedTaskDrawer = ({ task, closeAction }) => (
  <TaskLoader
    task={task}
    Wrapper={DrawerWrapper}
    closeAction={closeAction}
  />
);

FocusedTaskDrawer.propTypes = {
  task: PropTypes.object,
  closeAction: PropTypes.func
};

export default FocusedTaskDrawer;
