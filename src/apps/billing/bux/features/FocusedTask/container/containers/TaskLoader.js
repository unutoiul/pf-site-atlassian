import PropTypes from 'prop-types';
import React from 'react';
import LogScope from 'bux/components/log/Scope/index';
import library from '../../library';

const TaskLoader = ({
  task, closeAction, Wrapper, TaskWrapper = React.Fragment 
}, { sendAnalyticEvent }) => {
  const { component, props, logPrefix } = task || {}; // task can be null, not undefined
  const PayloadChildren = component ? library.get(component) : undefined;
  const onClose = () => {
    sendAnalyticEvent(`${logPrefix}.back`);
    closeAction();
  };
  return (
    <TaskWrapper>
      {
        PayloadChildren ? (
          <Wrapper closeAction={onClose}>
            <LogScope prefix={logPrefix}>
              <div className="focusedTaskContent">
                <PayloadChildren {...props} closeAction={closeAction} />
              </div>
            </LogScope>
          </Wrapper>
        ) : null
      }
    </TaskWrapper>
  );
};

TaskLoader.propTypes = {
  task: PropTypes.object,
  Wrapper: PropTypes.func.isRequired,
  closeAction: PropTypes.func,
  TaskWrapper: PropTypes.func
};

TaskLoader.contextTypes = {
  sendAnalyticEvent: PropTypes.func
};

export default TaskLoader;
