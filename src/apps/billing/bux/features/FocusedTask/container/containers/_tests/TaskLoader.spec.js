import PropTypes from 'prop-types';
import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import library from '../../../library';
import TaskLoader from '../TaskLoader';

describe('FocusedTask.PayloadComponent: ', () => {
  let componentId = 0;
  let payload;
  const component = props => <div {...props}>payload string</div>;
  const PassThought = ({ children }) => <div>{children}</div>;
  PassThought.propTypes = {
    children: PropTypes.node
  };

  beforeEach(() => {
    componentId = library.add(component);
    payload = {
      component: componentId
    };
  });

  afterEach(() => {
    library.remove(componentId);
  });

  it('should be empty if no payload set', () => {
    const wrapper = shallow(<TaskLoader Wrapper={PassThought} />);
    expect(wrapper.html()).to.be.equal('');
  });

  it('should render payload', () => {
    const wrapper = shallow(<TaskLoader task={payload} Wrapper={PassThought} />);
    expect(wrapper.find(component)).to.be.present();
  });

  it('should render payload with props', () => {
    const wrapper = shallow(<TaskLoader
      task={{
        ...payload,
        props: {
          className: 'someclass'
        }
      }}
      Wrapper={PassThought}
    />);
    expect(wrapper.find(component)).to.be.present();
    expect(wrapper.find(component).props().className).to.be.equal('someclass');
  });

  it('should render unexisting component', () => {
    const wrapper = shallow(<TaskLoader task={{ component: 'wrong payload' }} Wrapper={PassThought} />);
    expect(wrapper.html()).to.be.equal('');
  });
});
