import PropTypes from 'prop-types';
import React from 'react';
import { AkCustomDrawer } from '@atlaskit/navigation';
import CrossIcon from '@atlaskit/icon/glyph/cross';

import { withAnalytics, withAnalyticsEmitter, Analytics } from 'bux/components/Analytics';
import FocusLock from 'bux/components/behaviour/focus/FocusLock';
import { NavigationGlobalLogo } from '../../../../../../../../site/navigation/navigation-global-logo';

const NavigationGlobalLogoWithNewAnalytics = withAnalyticsEmitter(
  'NavigationGlobalLogo', 
  'navigationItem'
)(NavigationGlobalLogo);

class DrawerWrapper extends React.Component {
  onBackButton = (...params) => {
    this.props.analytics.sendUIEvent({
      action: 'clicked',
      actionSubject: 'navigationItem',
      actionSubjectId: 'focusTaskClose',
      attributes: {
        linkContainer: 'navigation'
      }
    });
    if (this.props.closeAction) {
      this.props.closeAction(...params);
    }
  }

  render() {
    const { children, drawer = {} } = this.props;
    return (
      <div className="bux-focused-drawer">
        <FocusLock>
          <AkCustomDrawer
            isOpen
            width="full"
            backIcon={<CrossIcon label="Cross icon" size="medium" />}
            primaryIcon={
              <Analytics.UI 
                as="focusTaskAtlassianLogo" 
                attributes={{ linkContainer: 'navigation' }}
              >
                <NavigationGlobalLogoWithNewAnalytics />
              </Analytics.UI>
            }
            {...drawer}
            onBackButton={this.onBackButton}
          >
            {children}
          </AkCustomDrawer>
        </FocusLock>
      </div>
    );
  }
}

DrawerWrapper.propTypes = {
  children: PropTypes.node,
  closeAction: PropTypes.func,
  drawer: PropTypes.object,
  analytics: PropTypes.object.isRequired,
};

export default withAnalytics(DrawerWrapper);
