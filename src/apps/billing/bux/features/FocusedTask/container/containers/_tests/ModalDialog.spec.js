import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Modal, { WithFocusLock } from '../ModalDialog';
import TaskLoader from '../TaskLoader';

describe('FocusedTask.DrawerComponent: ', () => {
  it('should render Drawer ', () => {
    const payload = { component: 'payload' };
    const wrapper = shallow(<Modal task={payload} />);
    const Payload = wrapper.find(TaskLoader);
    expect(Payload).to.be.present();
    expect(Payload.props().task).to.be.equal(payload);
    expect(Payload.props().Wrapper).to.be.equal(WithFocusLock);
  });
});
