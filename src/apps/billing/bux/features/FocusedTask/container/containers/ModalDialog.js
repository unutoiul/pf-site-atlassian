import { ModalTransition } from '@atlaskit/modal-dialog';
import PropTypes from 'prop-types';
import React from 'react';
import FocusLock from 'bux/components/behaviour/focus/FocusLock';
import TaskLoader from './TaskLoader';

export const WithFocusLock = ({ children }) => <FocusLock>{children}</FocusLock>;
WithFocusLock.propTypes = {
  children: PropTypes.node
};

const ModalDialog = ({ task, closeAction }) => (
  <TaskLoader
    task={task}
    Wrapper={WithFocusLock}
    TaskWrapper={ModalTransition}
    closeAction={closeAction}
  />
);

ModalDialog.propTypes = {
  task: PropTypes.object,
  closeAction: PropTypes.func
};

export default ModalDialog;
