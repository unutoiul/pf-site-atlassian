import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import { Provider } from 'bux/helpers/redux';
import createStore from 'bux/core/createStore';
import * as focusedTaskActions from 'bux/core/state/focused-task/actions';

import FocusedTaskContainer, { FocusedTasks } from '../FocusedTaskContaner';

describe('FocusedTaskContainer: ', () => {
  it('should map state to props', () => {
    const payload = {
      component: 'payload'
    };

    const store = createStore();
    const topWrapper = mount(<Provider store={store}>
      <FocusedTaskContainer />
                             </Provider>);
    let wrapper;
    const testPayload = (getPayload, openAction, closeAction) => {
      wrapper = topWrapper.find(FocusedTasks);
      expect(getPayload()).to.be.null();

      store.dispatch(openAction);
      wrapper = topWrapper.update().find(FocusedTasks);
      expect(getPayload()).to.be.deep.equal(payload);
      store.dispatch(closeAction);
      wrapper = topWrapper.update().find(FocusedTasks);
      expect(getPayload()).to.be.null();
    };

    testPayload(
      () => wrapper.props().taskPayload,
      focusedTaskActions.openFocusedTask(payload),
      focusedTaskActions.closeFocusedTask()
    );

    testPayload(
      () => wrapper.props().modalPayload,
      focusedTaskActions.openModalDialog(payload),
      focusedTaskActions.closeModalDialog()
    );
  });
});
