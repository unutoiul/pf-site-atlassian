import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { getFocusedTask, getModalDialog } from 'bux/core/state/focused-task/selectors';
import {
  closeFocusedTask,
  closeModalDialog
} from 'bux/core/state/focused-task/actions';
import DrawerTask from './containers/Drawer';
import ModalTask from './containers/ModalDialog';

export const FocusedTasks = ({
  taskPayload, modalPayload, closeFocusedTaskAction, closeModalDialogAction
}) => (
  <div>
    <DrawerTask
      task={taskPayload}
      closeAction={closeFocusedTaskAction}
    />
    <ModalTask
      task={modalPayload}
      closeAction={closeModalDialogAction}
    />
  </div>
);

FocusedTasks.propTypes = {
  taskPayload: PropTypes.object,
  modalPayload: PropTypes.object,

  closeFocusedTaskAction: PropTypes.func,
  closeModalDialogAction: PropTypes.func,
};

const mapStateToProps = state => ({
  taskPayload: getFocusedTask(state),
  modalPayload: getModalDialog(state),
});

const mapDispatchToProps = dispatch => ({
  closeFocusedTaskAction: payload => dispatch(closeFocusedTask(payload)),
  closeModalDialogAction: payload => dispatch(closeModalDialog(payload))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(FocusedTasks);
