let counter = 0;
const library = {};

const API = {
  get(id) {
    return library[id];
  },

  add(renderable) {
    counter += 1;
    library[counter] = renderable;
    return counter;
  },

  remove(id) {
    delete library[id];
  }
};

export default API;
