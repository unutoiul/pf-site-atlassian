import billHistory from 'bux/core/state/bill-history';

const isLoaded = state => billHistory.selectors.getInvoicesMetadata(state);

export { isLoaded };
