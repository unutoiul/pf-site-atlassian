import { storiesOf } from '@storybook/react';
import { inContentChrome, inStoryKnob } from 'bux/common/helpers/storybook';

import { BillHistoryDetail } from './components/BillHistoryDetail';

storiesOf('BUX|Features/bill-history/page', module)
  .add('FullHistory', () => inContentChrome(inStoryKnob(BillHistoryDetail, {
    invoices: [
      {
        invoiceNumber: '1',
        amount: 10.50,
        billDate: '2017-04-01',
        invoiceUrl: 'invoice1.url',
        currencyCode: 'USD',
        renewalFrequency: 'ANNUAL'
      },
      {
        invoiceNumber: '2',
        amount: 0,
        billDate: '2017-03-01',
        invoiceUrl: 'invoice2.url',
        currencyCode: 'USD',
        renewalFrequency: 'MONTHLY'
      },
      {
        invoiceNumber: '3',
        amount: 100.11,
        billDate: '2017-02-01',
        invoiceUrl: 'invoice3.url',
        currencyCode: 'USD',
        renewalFrequency: 'MONTHLY'
      },
      {
        invoiceNumber: '4',
        amount: 98,
        billDate: '2017-01-01',
        invoiceUrl: 'invoice3.url',
        currencyCode: 'USD',
        renewalFrequency: 'ANNUAL'
      }
    ]
  })))
  .add('EmptyHistory [prompt]', () => inContentChrome(inStoryKnob(BillHistoryDetail, { invoices: [] })))
  .add('Managed Account', () => inContentChrome(inStoryKnob(BillHistoryDetail, {
    managedByPartner: true,
    invoices: [
      {
        invoiceNumber: '1',
        amount: 10.50,
        billDate: '2017-04-01',
        invoiceUrl: 'invoice1.url',
        currencyCode: 'USD',
        renewalFrequency: 'ANNUAL'
      }
    ]
  })));

