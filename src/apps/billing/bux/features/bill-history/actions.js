import billHistory from 'bux/core/state/bill-history';
import billingDetails from 'bux/core/state/billing-details';

const getBillHistoryData = () => (dispatch) => {
  dispatch(billHistory.actions.getBillHistory());
  dispatch(billingDetails.actions.getBillingDetails());
};

export { getBillHistoryData };
