import React from 'react';
import sinon from 'sinon';
import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import { Provider } from 'bux/helpers/redux';
import proxyquire from 'bux/helpers/proxyquire';
import Page from 'bux/components/Page';
import identityFn from 'bux/common/helpers/identity';
import BillHistoryPage, { AugmentedComponent } from '../BillHistory';
import ConnectedBillHistory from '../content';

describe('BillHistory page: ', () => {
  it('should call get bill history', () => {
    const getBillHistory = sinon.spy();
    const getBillingDetails = sinon.spy();
    const dispatch = () => {
    };

    const action = proxyquire.noCallThru().load('../actions', {
      'bux/core/state/bill-history': {
        actions: { getBillHistory }
      },
      'bux/core/state/billing-details': {
        actions: { getBillingDetails }
      }
    }).getBillHistoryData;

    action()(dispatch);

    expect(getBillHistory).to.have.been.called();
    expect(getBillingDetails).to.have.been.called();
  });

  it('should trigger callbacks on Component mount', (done) => {
    const isLoaded = sinon.stub().returns({ display: true });
    const getBillHistoryData = sinon.stub().returns(identityFn);

    // eslint-disable-next-line no-shadow
    const { AugmentedComponent } = proxyquire.noCallThru().load('../BillHistory', {
      './selectors': { isLoaded },
      './actions': { getBillHistoryData }
    });

    const wrapper = mount(<Provider><AugmentedComponent /></Provider>);

    expect(getBillHistoryData).to.have.been.called();
    expect(isLoaded).to.have.been.called();
    setImmediate(() => {
      wrapper.update();
      expect(wrapper.find(ConnectedBillHistory)).to.be.present();
      done();
    });
  });

  it('should pass title and metadata', () => {
    const wrapper = shallow(<BillHistoryPage />);
    const page = wrapper.find(Page);
    expect(page.props().title).to.equal('Bill history');
    expect(page).to.have.prop('logPrefix', 'billHistory');
    expect(wrapper.find(AugmentedComponent)).to.be.present();
  });
});
