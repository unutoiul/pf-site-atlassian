import { Trans } from '@lingui/react';
import SectionMessage from '@atlaskit/section-message/dist/esm/components/SectionMessage';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import cx from 'classnames';
import { Analytics } from 'bux/components/Analytics';
import Date from 'bux/components/Date';
import Money from 'bux/components/Money';
import ContentAsideLayout from 'bux/components/layout/ContentAsideLayout';
import { ExternalLink } from 'bux/components/Link';
import Prompt from 'bux/components/Page/Prompt';
import billHistory from 'bux/core/state/bill-history';
import { ANNUAL, MONTHLY } from 'bux/core/constants/frequency';
import { isManagedByPartner } from 'bux/core/state/billing-details/selectors';
import styles from './BillHistoryDetail.less';
import EmptyFolder from './empty-folder.svgx';


export const NoHistory = () => (
  <Prompt
    className="empty-billing-history-prompt"
    media={<EmptyFolder />}
    title={<span><Trans id="billing.bill-history.no-invoices">You currently have no bills</Trans></span>}
    message=""
  />
);

const renewalFrequencyValue = (frequency) => {
  switch (frequency) {
    case ANNUAL:
      return <Trans id="billing.bill-history.invoice.renewalFrequency.annual">Annual</Trans>;
    case MONTHLY:
      return <Trans id="billing.bill-history.invoice.renewalFrequency.monthly">Monthly</Trans>;
    default:
      break;// Even though default is not required as we are returning from the case, putting default here
            // due to linting error
  }

  return <Trans id="billing.bill-history.invoice.renewalFrequency.none">None</Trans>;
};

const BillHistoryDetailLine = ({
  invoiceNumber, billDate, amount, currencyCode, invoiceUrl, renewalFrequency
}) => (
  <tr className={cx('invoice-line', styles.invoiceLine)}>
    <td className="bill-date">
      <Date value={billDate} />
    </td>
    <td className="invoice-number">
      {invoiceNumber}
    </td>
    <td className="billing-period">
      {renewalFrequencyValue(renewalFrequency)}
    </td>
    <td className={cx('amount', styles.amountCell)}>
      <Money amount={amount} currency={currencyCode} withCents />
    </td>
    <td className="actions">
      <Analytics.UI as="downloadLink">
        <ExternalLink className="link" href={invoiceUrl} logPrefix="download">
          <Trans id="billing.bill-history.invoice.download">Download</Trans>
        </ExternalLink>
      </Analytics.UI>
    </td>
  </tr>
);

BillHistoryDetailLine.propTypes = {
  invoiceNumber: PropTypes.string.isRequired,
  billDate: PropTypes.string.isRequired,
  amount: PropTypes.number.isRequired,
  currencyCode: PropTypes.string.isRequired,
  invoiceUrl: PropTypes.string.isRequired,
  renewalFrequency: PropTypes.string.isRequired,
};

export const AccountUnderManagementMessage = () => (
  <SectionMessage>
    <Trans id="billing.bill-history.account-under-management">
      Only invoices from periods where the account was not externally managed are available to download.
      Please contact your expert/reseller for other invoices.
    </Trans>
  </SectionMessage>
);

export const BillHistoryDetail = ({ invoices, managedByPartner }) => (
  <React.Fragment>
    {managedByPartner && <AccountUnderManagementMessage />}
    {
      !invoices.length
        ? <NoHistory />
        : (
          <table className={cx('invoice-list', styles.invoiceList)}>
            <thead className={styles.invoiceHeader}>
            <tr>
              <th>
                <Trans id="billing.bill-history.invoice.date">Date</Trans>
              </th>
              <th>
                <Trans id="billing.bill-history.invoice.number">Invoice number</Trans>
              </th>
              <th>
                <Trans id="billing.bill-history.invoice.renewalFrequency">Billing Period</Trans>
              </th>
              <th className={styles.amountCell}>
                <Trans id="billing.bill-history.invoice.amount">Amount</Trans>
              </th>
              <th>
                <Trans id="billing.bill-history.invoice.actions">Actions</Trans>
              </th>
            </tr>
            </thead>
            <tbody>
            {
              invoices.map(invoice => (
                <BillHistoryDetailLine key={invoice.invoiceNumber} {...invoice} />
              ))
            }
            </tbody>
          </table>
        )}
  </React.Fragment>
);

BillHistoryDetail.propTypes = {
  invoices: PropTypes.array.isRequired,
  managedByPartner: PropTypes.bool,
};

const mapStateToProps = state => ({
  invoices: billHistory.selectors.getInvoices(state),
  managedByPartner: isManagedByPartner(state),
});

export const ConnectedBillHistory = connect(mapStateToProps)(BillHistoryDetail);

export default () => (
  <ContentAsideLayout
    content={<ConnectedBillHistory />}
    aside={<span />}
  />
);
