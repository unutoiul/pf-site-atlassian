import { Trans, I18n } from '@lingui/react';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import cx from 'classnames';
import Widget from 'bux/components/Widget';
import Date from 'bux/components/Date';
import Money from 'bux/components/Money';
import Link from 'bux/components/Link';
import ExtendedPropTypes from 'bux/core/extended-proptypes';
import billHistory from 'bux/core/state/bill-history';
import { Analytics } from 'bux/components/Analytics';
import styles from './BillHistoryWidget.less';
import EmptyFolder from './empty-folder.svgx';

export const NoHistory = () => (
  <div className={styles.NoBillsPage}>
    <EmptyFolder />
    <Trans id="billing.widget.bill-history.no-bills">
      You currently have no bills
    </Trans>
  </div>
);

export const InvoiceLine = ({ billDate, amount, currencyCode }) => (
  <li className={cx('invoice-line', styles.invoiceLine)}>
    <div className={cx('bill-date', styles.billDate)}>
      <Date value={billDate} />
    </div>
    <div className={cx('amount', styles.amount)}>
      <Money amount={amount} currency={currencyCode} withCents />
    </div>
  </li>
);

InvoiceLine.propTypes = {
  billDate: PropTypes.string.isRequired,
  amount: PropTypes.number.isRequired,
  currencyCode: PropTypes.string.isRequired,
};

const NB_INVOICES = 3;

const printInvoices = invoices => (
  <div className={styles.invoices}>
    <ul>
      {
        invoices.slice(0, NB_INVOICES)
          .map(invoice => (<InvoiceLine key={invoice.invoiceNumber} {...invoice} />))
      }
    </ul>
    <Analytics.UI as="viewFullHistoryLink" attributes={{ linkContainer: 'billingHistoryCard' }}>
      <Link className={cx(styles.link, 'link-full-history')} to="/history">
        <Trans id="billing.widget.bill-history.full-history">
          View full history
        </Trans>
      </Link>
    </Analytics.UI>
  </div>
);

export const BillHistoryWidget = ({ className, invoices = [], metadata }) => (
  <I18n>{({ i18n }) => (
    <Widget
      name={i18n.t('billing.widget.bill-history.name')`Bill history`}
      errorMessage={{ name: i18n.t('billing.widget.bill-history.name.in-error-message')`bill history` }}
      className={cx(className, 'bill-history-widget')}
      metadata={metadata}
    >
    {
      !invoices.length
        ? <NoHistory />
        : printInvoices(invoices)
    }
    </Widget>
  )}</I18n>
);

BillHistoryWidget.propTypes = {
  className: PropTypes.string,
  dispatch: PropTypes.func,
  invoices: PropTypes.array.isRequired,
  metadata: ExtendedPropTypes.metadata,
};

const mapStateToProps = state => ({
  invoices: billHistory.selectors.getInvoices(state),
  metadata: billHistory.selectors.getInvoicesMetadata(state),
});

export default connect(mapStateToProps)(BillHistoryWidget);
