export const DefaultBillHistory = {
  invoices: [
    {
      invoiceNumber: '10001',
      billDate: '2017-11-11',
      amount: 10,
      currencyCode: 'USD',
      invoiceUrl: 'http://url1',
      renewalFrequency: 'MONTHLY'
    },
    {
      invoiceNumber: '10002',
      billDate: '2017-11-12',
      amount: 12,
      currencyCode: 'USD',
      invoiceUrl: 'http://url2',
      renewalFrequency: 'NONE'
    }
  ]
};
