import configureMockStore from 'redux-mock-store';
import React from 'react';
import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import { StaticRouter } from 'react-router-dom';
import { INITIAL_BILLING_HISTORY_STATE } from 'bux/core/state/bill-history/reducer';
import BillHistoryContainer, { BillHistoryWidget, InvoiceLine, NoHistory } from '../BillHistoryWidget';
import { DefaultBillHistory } from './BillingHistoryTestFactory';

describe('BillHistoryWidget component: ', () => {
  it('should render invoice line', () => {
    const invoiceLine = mount(<StaticRouter context={{}}><BillHistoryWidget {...DefaultBillHistory} /></StaticRouter>)
      .find(InvoiceLine);

    expect(invoiceLine).to.be.present();

    expect(invoiceLine).to.have.length(2);
    expect(invoiceLine.first()).to.contain.text('Nov 11, 2017');
    expect(invoiceLine.first()).to.contain.text('$10');
  });

  it('should render noHistory', () => {
    const wrapper = mount(<BillHistoryWidget invoices={[]} />);

    expect(wrapper.find(NoHistory)).to.be.present();
  });
});

describe('BillHistoryWidget container: ', () => {
  const mockStoreCreator = (data = {}, error = false) => configureMockStore()({
    billHistory: { data: { ...INITIAL_BILLING_HISTORY_STATE.data, ...data }, error }
  });

  it('should map state to props', () => {
    const mockedStore = mockStoreCreator(DefaultBillHistory);

    const wrapper = shallow(<BillHistoryContainer store={mockedStore} />);
    const props = wrapper.props();

    expect(props.invoices).to.have.length(2);
  });

  it('should map error state to props', () => {
    const mockedStore = mockStoreCreator(undefined, { status: 500 });

    const wrapper = shallow(<BillHistoryContainer store={mockedStore} />);
    const props = wrapper.props();

    expect(props.metadata.error.status).to.equal(500);
  });
});
