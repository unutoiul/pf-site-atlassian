import configureMockStore from 'redux-mock-store';
import React from 'react';
import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import { INITIAL_BILLING_HISTORY_STATE } from 'bux/core/state/bill-history/reducer';
import {
  BillHistoryDetail,
  ConnectedBillHistory,
  NoHistory,
  AccountUnderManagementMessage
} from '../BillHistoryDetail';
import { DefaultBillHistory } from './BillingHistoryTestFactory';

const mountDetail = options => <BillHistoryDetail {...DefaultBillHistory} {...options} />;

describe('BillHistoryDetail component: ', () => {
  it('should mount invoices item with actions', () => {
    const wrapper = mount(mountDetail(DefaultBillHistory));
    const invoiceLines = wrapper.find('.invoice-line');

    expect(invoiceLines).to.have.length(2);

    const invoice1 = invoiceLines.first();

    expect(invoice1).to.contain.text('Nov 11, 2017');
    expect(invoice1).to.contain.text('$10');
    expect(invoice1).to.contain.text('10001');
    expect(invoice1).to.contain.text('Monthly');
    expect(invoice1.find('.link').hostNodes().getDOMNode().getAttribute('href'))
      .to.be.equal('http://url1');

    const invoice2 = invoiceLines.at(1);
    expect(invoice2).to.contain.text('None');
  });

  it('should render noHistory', () => {
    const wrapper = mount(<BillHistoryDetail invoices={[]} />);

    expect(wrapper.find(NoHistory)).to.be.present();
  });

  it('should display account under manager notice', () => {
    expect(mount(<BillHistoryDetail invoices={[]} />).find(AccountUnderManagementMessage)).not.to.be.present();
    expect(mount(<BillHistoryDetail
      invoices={[]}
      managedByPartner
    />).find(AccountUnderManagementMessage)).to.be.present();
  });
});

describe('Connected BillHistoryDetail: ', () => {
  const mockStoreCreator = (data = {}) => configureMockStore()({
    billHistory: { data: { ...INITIAL_BILLING_HISTORY_STATE.data, ...data } },
    billingDetails: {}
  });

  it('should map state to props', () => {
    const mockedStore = mockStoreCreator(DefaultBillHistory);
    const wrapper = shallow(<ConnectedBillHistory title="someTitle" store={mockedStore} />);

    const props = wrapper.props();
    expect(props.invoices).to.have.length(2);
    expect(props.title).to.equal('someTitle');
  });
});
