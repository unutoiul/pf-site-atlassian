import { I18n } from '@lingui/react';
import React from 'react';
import Page from 'bux/components/Page';
import withAsyncPageLoader from 'bux/components/withAsyncPageLoader';
import { Breadcrumbs, BillingBreadcrumb } from 'bux/components/Breadcrumb';
import { getBillHistoryData } from './actions';
import { isLoaded } from './selectors';

const loader = () => import(/* webpackChunkName:'billing' */ './content');
export const AugmentedComponent = withAsyncPageLoader(isLoaded, getBillHistoryData)(loader);

const breadcrumbs = (
  <Breadcrumbs>
    <BillingBreadcrumb />
  </Breadcrumbs>
);

export default () => (
  <I18n>{({ i18n }) => (
    <Page
      title={i18n.t('billing.bill-history.page-title')`Bill history`}
      layout="aside"
      logPrefix="billHistory"
      breadcrumbs={breadcrumbs}
    >
      <AugmentedComponent />
    </Page>
  )}</I18n>
);
