import { asPageContent } from 'bux/components/Page/Page';
import { BillHistoryDetail } from './components';

export default asPageContent({
  name: 'billHistoryScreen'
})(BillHistoryDetail);
