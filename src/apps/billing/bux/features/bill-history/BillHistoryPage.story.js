import { storiesOf } from '@storybook/react';
import { inScenarios, inPageChrome, render } from 'bux/common/helpers/storybook';

import BillHistory from './BillHistory';

storiesOf('BUX|Pages/Bill history', module)
  .add('Default', () => inScenarios([], inPageChrome(render(BillHistory))))
  .add('Mixed currency', () => inScenarios(['invoice-history-mixed-currency'], inPageChrome(render(BillHistory))));
