import React from 'react';
import PropTypes from 'prop-types';
import ModalDialog from '@atlaskit/modal-dialog';
import LogScope from 'bux/components/log/Scope/Scope';
import { Analytics } from 'bux/components/Analytics';
import Footer from './ManageContactWarningModalFooter';
import Message from './ManageContactWarningModalMessage';

const withConfirmFooter = ({
  onConfirm, toBeMainContact, pathname, prefix
}) => props => (
  <LogScope prefix={prefix}>
    <Footer {...props} pathname={pathname} onConfirm={onConfirm} email={toBeMainContact} />
  </LogScope>
);

const ManageContactWarningModal = ({
  closeAction,
  confirmAction: onConfirm,
  name,
  pathname,
  toBeMainContact,
  previousMainContact,
  isSiteAdmin
}, { logPrefix }) => {
  const prefix = logPrefix.join('.');
  return (
    <Analytics.Screen name="updateMainContactModal">
      <ModalDialog
        heading="Important"
        width="medium"
        appearance="warning"
        className="update-contact-modal-dialog"
        onClose={closeAction}
        footer={withConfirmFooter({
          onConfirm, toBeMainContact, pathname, prefix
        })}
        autoFocus
      >
        <LogScope prefix={prefix}>
          <Message
            isSiteAdmin={isSiteAdmin}
            name={name}
            toBeMainContact={toBeMainContact}
            previousMainContact={previousMainContact}
          />
        </LogScope>
      </ModalDialog>
    </Analytics.Screen>
  );
};

ManageContactWarningModal.propTypes = {
  closeAction: PropTypes.func.isRequired,
  confirmAction: PropTypes.func.isRequired,
  name: PropTypes.string,
  pathname: PropTypes.string.isRequired,
  toBeMainContact: PropTypes.string.isRequired,
  previousMainContact: PropTypes.string.isRequired,
  isSiteAdmin: PropTypes.bool.isRequired
};
ManageContactWarningModal.contextTypes = {
  sendAnalyticEvent: PropTypes.func,
  logPrefix: PropTypes.arrayOf(PropTypes.string)
};

export default ManageContactWarningModal;
