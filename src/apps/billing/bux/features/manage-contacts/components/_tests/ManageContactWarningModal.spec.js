import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import ManageContactWarningModal from '../ManageContactWarningModal';
import Message from '../ManageContactWarningModalMessage';

const renderWidget = options => <ManageContactWarningModal {...options} />;

const DEFAULT_CONTEXT = { context: { logPrefix: ['test'] } };

describe('ConfirmCancel component: ', () => {
  const DEFAULT_PROPS = {
    closeAction: () => {},
    confirmAction: () => {},
    name: 'name',
    pathname: '/pathname',
    toBeMainContact: 'mainContact',
    previousMainContact: 'previousContact',
    isSiteAdmin: true
  };

  it('should render modal', () => {
    const component = shallow(renderWidget(DEFAULT_PROPS), DEFAULT_CONTEXT);

    expect(component.find(Message)).to.be.present();
  });

  it('should display toBeMainContact', () => {
    const component = shallow(<Message
      {...DEFAULT_PROPS}
      isSiteAdmin
    />);

    expect(component).to.contain.text(DEFAULT_PROPS.toBeMainContact);
  });

  it('should display Site admin', () => {
    const component = shallow(<Message
      {...DEFAULT_PROPS}
      isSiteAdmin
    />);

    expect(component).to.contain.text('Site Administration');
    expect(component).not.to.contain.text('Admin hub');
  });

  it('should display Admin hub', () => {
    const component = shallow(<Message
      {...DEFAULT_PROPS}
      isSiteAdmin={false}
    />);

    expect(component).not.to.contain.text('Site Administration');
    expect(component).to.contain.text('Admin hub');
  });

  //FIXME: Use Gateway to render modal in enzyme.
});