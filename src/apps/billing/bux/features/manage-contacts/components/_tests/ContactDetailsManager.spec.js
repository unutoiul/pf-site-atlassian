import React from 'react';
import sinon from 'sinon';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';
import { AvatarItem } from '@atlaskit/avatar';
import Submitable from 'bux/components/behaviour/state/submitable';
import getAnalytic from 'bux/components/log/helpers/getAnalytic';
import { ExternalLink } from 'bux/components/Link';
import proxyquire from 'bux/helpers/proxyquire';
import { ContactDetailsManager } from '../ContactDetailsManager';

describe('ContactDetailsManager', () => {
  it('should format the Avatar accordingly with full user details', () => {
    const billingContact = {
      firstName: 'firstName',
      lastName: 'lastName',
      email: 'email'
    };

    const wrapper = shallow(<ContactDetailsManager
      billingContact={billingContact}
      handleOpenDialog={() => {}}
      confirmAction={() => {}}
      pathname=""
      addSecondaryContact={() => {}}
      isSiteAdmin
    />);

    const result = wrapper.find(AvatarItem);

    expect(result).to.have.prop('primaryText', 'firstName lastName');
    expect(result).to.have.prop('secondaryText', 'email');
  });

  it('should render with last name and email only', () => {
    const billingContact = {
      lastName: 'lastName',
      email: 'email'
    };

    const wrapper = shallow(<ContactDetailsManager
      billingContact={billingContact}
      handleOpenDialog={() => {}}
      confirmAction={() => {}}
      pathname=""
      addSecondaryContact={() => {}}
      isSiteAdmin
    />);

    const result = wrapper.find(AvatarItem);

    expect(result).to.have.prop('primaryText', 'lastName');
    expect(result).to.have.prop('secondaryText', 'email');
  });

  it('should render with first name and email only', () => {
    const billingContact = {
      firstName: 'firstName',
      email: 'email'
    };

    const wrapper = shallow(<ContactDetailsManager
      billingContact={billingContact}
      handleOpenDialog={() => {}}
      confirmAction={() => {}}
      pathname=""
      addSecondaryContact={() => {}}
      isSiteAdmin
    />);

    const result = wrapper.find(AvatarItem);

    expect(result).to.have.prop('primaryText', 'firstName');
    expect(result).to.have.prop('secondaryText', 'email');
  });

  it('should render only the email if no first or last name is provided', () => {
    const billingContact = {
      email: 'email'
    };

    const wrapper = shallow(<ContactDetailsManager
      billingContact={billingContact}
      handleOpenDialog={() => {}}
      confirmAction={() => {}}
      pathname=""
      addSecondaryContact={() => {}}
      isSiteAdmin
    />);

    const result = wrapper.find(AvatarItem);

    expect(result).to.have.prop('primaryText', 'email');
    expect(result).to.not.have.prop('secondaryText');
  });

  it('should show the primary action if current !== main billing contact', () => {
    const billingContact = {
      email: 'email'
    };

    const current = 'anotheremail';

    const wrapper = shallow(<ContactDetailsManager
      billingContact={billingContact}
      currentUserEmail={current}
      handleOpenDialog={() => {}}
      confirmAction={() => {}}
      pathname=""
      addSecondaryContact={() => {}}
      isSiteAdmin
    />);

    const result = wrapper.find('[name="update-billing-contact"]');

    expect(result).to.have.length(1);
  });

  it('should hide the primary action if current === main billing contact', () => {
    const billingContact = {
      email: 'email'
    };

    const current = 'email';

    const wrapper = shallow(<ContactDetailsManager
      billingContact={billingContact}
      currentUserEmail={current}
      handleOpenDialog={() => {}}
      confirmAction={() => {}}
      pathname=""
      addSecondaryContact={() => {}}
      isSiteAdmin
    />);

    const result = wrapper.find(Submitable);
    const manage = wrapper.find(ExternalLink);

    expect(result).to.have.length(0);
    expect(manage).to.have.length(1);
  });

  it('should handle an open dialog', (done) => {
    const current = 'email2';
    const billingContact = {
      email: 'email'
    };

    const handleOpenDialog = sinon.stub().returns(Promise.resolve());
    const confirmAction = sinon.stub().returns(Promise.resolve());

    const analytics = getAnalytic();

    const wrapper = mount(<ContactDetailsManager
      billingContact={billingContact}
      currentUserEmail={current}
      handleOpenDialog={handleOpenDialog}
      confirmAction={confirmAction}
      pathname="/pathname"
      addSecondaryContact={() => {}}
      isSiteAdmin
      analytics={{}}
    />, analytics.context);

    wrapper.find('[name="update-billing-contact"]').at(0).simulate('click');
    setImmediate(() => {
      const opts = {
        name: billingContact.firstName,
        toBeMainContact: current,
        pathname: '/pathname',
        previousMainContact: billingContact.email,
        isSiteAdmin: true,
      };
      expect(handleOpenDialog).to.have.been.calledWithMatch(opts);
      done();
    });
  });

  it('should handle an addSecondaryContact', (done) => {
    const current = 'email2';
    const billingContact = {
      email: 'email'
    };

    const addSecondaryContact = sinon.stub().returns(Promise.resolve());
    const redirect = sinon.stub().returns();
    const sendUIEvent = sinon.stub().returns();

    const analytics = getAnalytic();
    const { ContactDetailsManager: Component } = proxyquire.load('../ContactDetailsManager', {
      'bux/common/helpers/browser': {
        redirect
      }
    });

    const wrapper = mount(<Component
      billingContact={billingContact}
      currentUserEmail={current}
      addSecondaryContact={addSecondaryContact}
      handleOpenDialog={() => {}}
      confirmAction={() => {}}
      pathname=""
      isSiteAdmin
      analytics={{ sendUIEvent }}
    />, analytics.context);

    wrapper.find('[name="manage-contacts"]').at(0).simulate('click');
    setImmediate(() => {
      expect(addSecondaryContact).to.have.been.calledWith(current);
      expect(sendUIEvent).to.have.been.called();
      expect(redirect).to.have.been.called();
      done();
    });
  });
});
