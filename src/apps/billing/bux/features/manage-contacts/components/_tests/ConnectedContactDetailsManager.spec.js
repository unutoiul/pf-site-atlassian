import { expect } from 'chai';
import sinon from 'sinon';
import proxyquire from 'bux/helpers/proxyquire';

describe('connectedContactDetailsManager', () => {
  it('should map state to props', () => {
    const billingDetails = { firstName: 'firstname', lastName: 'lastname', email: 'email' };
    const getBillingContact = sinon.stub().returns(billingDetails);
    const getUser = sinon.stub().returns(billingDetails);
    const getPathname = sinon.stub().returns('/path');
    const isOrganization = sinon.stub().returns(false);

    const { mapStateToProps } = proxyquire.noCallThru().load('../ConnectedContactDetailsManager', {
      'bux/core/state/billing-details/selectors': { getBillingContact },
      'bux/core/state/meta/selectors': { getUser, isOrganization },
      'bux/core/selectors/common': { getPathname }
    });

    const result = mapStateToProps();

    expect(getBillingContact).to.have.been.called();
    expect(getUser).to.have.been.called();

    expect(result).to.deep.equals({
      billingContact: billingDetails,
      currentUserEmail: 'email',
      pathname: '/path',
      isSiteAdmin: true
    });
  });

  it('should handle open dialog', () => {
    const openModalDialog = sinon.stub();
    const confirmAction = sinon.stub();
    const pathname = '/path';

    const { mapDispatchToProps } = proxyquire.noCallThru().load('../ConnectedContactDetailsManager', {
      'bux/core/state/focused-task/actions': { openModalDialog },
      'bux/features/FocusedTask/library': { add: () => 2 }
    });

    const opts = {
      name: 'name',
      toBeMainContact: 'main',
      pathname,
      previousMainContact: 'previous',
      isSiteAdmin: false,
      confirmAction
    };
    mapDispatchToProps.handleOpenDialog(opts);

    expect(openModalDialog).to.have.been.calledWith({
      component: 2,
      logPrefix: 'billOverview.billingDetails.updateMainContact',
      props: opts
    });
  });

  it('should handle addSecondaryContact', () => {
    const email = 'email';
    const updateBillingContact = sinon.stub();

    const { mapDispatchToProps } = proxyquire.noCallThru().load('../ConnectedContactDetailsManager', {
      'bux/core/state/billing-contact/actions': { updateBillingContact }
    });

    mapDispatchToProps.addSecondaryContact(email);

    expect(updateBillingContact).to.have.been.calledWith({
      main: false,
      email
    });
  });

  it('should handle confirmAction', () => {
    const email = 'email';
    const updateBillingContact = sinon.stub();

    const { mapDispatchToProps } = proxyquire.noCallThru().load('../ConnectedContactDetailsManager', {
      'bux/core/state/billing-contact/actions': { updateBillingContact }
    });

    mapDispatchToProps.confirmAction(email);

    expect(updateBillingContact).to.have.been.calledWith({
      main: true,
      email
    });
  });
});
