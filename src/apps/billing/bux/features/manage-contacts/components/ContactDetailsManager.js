import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import cx from 'classnames';
import Avatar, { AvatarItem } from '@atlaskit/avatar';
import config from 'bux/common/config';
import Button from 'bux/components/Button';
import { redirect } from 'bux/common/helpers/browser';
import { ExternalLink } from 'bux/components/Link';
import Submitable from 'bux/components/behaviour/state/submitable';
import { Analytics, withAnalytics } from 'bux/components/Analytics';
import { Trans } from '@lingui/react';

const getDetailsWithFallback = ({ firstName, lastName, email }) => (
  (!firstName && !lastName) ?
    { primaryText: email } :
    { primaryText: `${firstName || ''} ${lastName || ''}`.trim(), secondaryText: email }
);

export const ContactDetailsManager = ({
  className,
  pathname,
  currentUserEmail,
  billingContact,
  handleOpenDialog,
  confirmAction,
  addSecondaryContact,
  isSiteAdmin,
  primary = false,
  analytics,
}) => {
  const isMain = billingContact.email === currentUserEmail;

  return (<section className={cx(className, 'avatar')}>
    <AvatarItem
      avatar={<Avatar size="large" />}
      {...getDetailsWithFallback(billingContact)}
    />
    { !isMain && <footer>
      <Analytics.UI as="updateMainContactButton" attributes={{ linkContainer: 'billingContactsCard' }}>
        <Button
          primary={primary}
          name="update-billing-contact"
          className="update-billing-contact"
          onClick={() => {
            handleOpenDialog({
              pathname,
              name: billingContact.firstName,
              toBeMainContact: currentUserEmail,
              previousMainContact: billingContact.email,
              confirmAction: () => {
                const event = {
                  actionSubject: 'button',
                  actionSubjectId: 'confirmUpdateMainContactButton',
                  action: 'clicked',
                  attributes: { linkContainer: 'updateMainContactModal' }
                };
                analytics.sendUIEvent(event);
                return confirmAction();
              },
              isSiteAdmin
            });
          }}
        >
          <Trans id="billing.contact-details-manager.make-me-billing-contact-button">
            Make me a billing contact
          </Trans>
        </Button>
      </Analytics.UI>

      <Submitable
        name="manage-contacts"
        className="manage-contacts"
        component={Button}
        action={() => addSecondaryContact(currentUserEmail)}
        afterSubmit={() => {
          const event = {
            actionSubject: 'link',
            actionSubjectId: 'manageContactsLink',
            action: 'clicked',
            attributes: { linkContainer: 'billingContactsCard' }
          };
          analytics.sendUIEvent(event);
          redirect(config.macUrl);
        }}
        map={submitting => ({
          disabled: submitting,
          spinning: submitting
        })}
        spinningAfter
        link
      >
        <Trans id="billing.contact-details-manager.manage-contacts-button">
          Manage contacts
        </Trans>
      </Submitable>
    </footer> }

    { isMain && <footer>
      <Analytics.UI as="manageContactsLink">
        <ExternalLink className="myLink" href={config.macUrl} target="_blank">
          <Trans id="billing.contact-details-manager.manage-contacts-link">
            Manage contacts
          </Trans>
        </ExternalLink>
      </Analytics.UI>
    </footer>}
  </section>);
};

ContactDetailsManager.propTypes = {
  className: PropTypes.string,
  pathname: PropTypes.string.isRequired,
  currentUserEmail: PropTypes.string,
  billingContact: PropTypes.shape({
    firstName: PropTypes.string,
    lastName: PropTypes.string,
    email: PropTypes.string.isRequired
  }),
  handleOpenDialog: PropTypes.func.isRequired,
  confirmAction: PropTypes.func.isRequired,
  addSecondaryContact: PropTypes.func.isRequired,
  primary: PropTypes.bool,
  isSiteAdmin: PropTypes.bool.isRequired,
  analytics: PropTypes.object.isRequired,
};

const StyledContactDetailsManager = styled(ContactDetailsManager)`
  header > h2 {
    margin-bottom: 5px;
  }

  footer {
  margin-top: 10px;
  }

  footer > button {
    margin: 5px 20px 5px 0;
  }
`;

export default withAnalytics(StyledContactDetailsManager);
