import PropTypes from 'prop-types';
import React from 'react';
import config from 'bux/common/config';
import { ExternalLink } from 'bux/components/Link';

const Message = ({
  name, toBeMainContact, previousMainContact, isSiteAdmin
}) => (
  <div className="update-contact-warning-modal">
    <p>
      You are about to replace {name} as the primary billing contact.
    </p>
    <p>This means you'll be able to access future invoices
      through <ExternalLink href={config.macUrl}>My Atlassian</ExternalLink> and
      continue to view them in { isSiteAdmin ? 'Site Administration' : 'Admin hub' }.
      You'll also receive them via email at <b>{toBeMainContact}</b>.
    </p>
    <p>A notification email will be sent to {previousMainContact} letting
      them know they are no longer the primary billing contact.
    </p>
  </div>
);

Message.propTypes = {
  name: PropTypes.string,
  toBeMainContact: PropTypes.string.isRequired,
  previousMainContact: PropTypes.string.isRequired,
  isSiteAdmin: PropTypes.bool.isRequired
};
export default Message;

