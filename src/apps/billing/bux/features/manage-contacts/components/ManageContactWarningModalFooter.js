import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { ModalFooter } from '@atlaskit/modal-dialog';
import Button, { SubmitButton } from 'bux/components/Button';
import LogScope from 'bux/components/log/Scope/Scope';
import WithAnalyticsEvent from 'bux/components/log/with/analyticsEvent';
import Submitable from 'bux/components/behaviour/state/submitable';
import { getHostname } from 'bux/common/helpers/browser';

const ActionButtons = styled.div`
& {
    width: 100%;
    display: flex;
    justify-content: flex-end;
    margin: 16px 0 0 0;

    & button {
        margin: 0 4px;
    }
}`;

const Footer = ({
  onConfirm, onClose, email, pathname 
}) => (
  <ModalFooter>
    <ActionButtons className="redirectCancelModal">
      <LogScope prefix="confirm">
        <WithAnalyticsEvent>
          {sendAnalyticsEvent => (<Submitable
            className="button-submit"
            component={SubmitButton}
            action={() => onConfirm(email)}
            afterSubmit={() => {
              onClose();
            }}
            onError={(error) => {
              sendAnalyticsEvent('updateMainBillingContact.error', {
                instanceName: getHostname(),
                error,
                pathname
              });
              onClose();
            }}
            map={submitting => ({
              disabled: submitting,
              spinning: submitting
            })}
            spinningAfter
            preventDefault
          >Make me a billing contact
                                  </Submitable>)}
        </WithAnalyticsEvent>
      </LogScope>
      <Button className="button-cancel" onClick={onClose}>
        Close
      </Button>
    </ActionButtons>
  </ModalFooter>
);

Footer.propTypes = {
  onClose: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired,
  email: PropTypes.string.isRequired,
  pathname: PropTypes.string.isRequired
};

export default Footer;
