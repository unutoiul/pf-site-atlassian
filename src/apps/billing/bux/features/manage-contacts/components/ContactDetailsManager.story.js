import { storiesOf } from '@storybook/react';
import { inPageChrome, inStoryKnob } from 'bux/common/helpers/storybook';

import { ContactDetailsManager } from './ContactDetailsManager';

storiesOf('BUX|Features/manage-contacts/components/ContactDetailsManager', module)
  .add('ContactDetailsManager', () => inPageChrome(inStoryKnob(ContactDetailsManager, {
    currentUserEmail: 'gsgritta@atlassian.com',
    pathname: 'blahblah',
    billingContact: {
      firstName: 'invoice',
      lastName: 'contact',
      email: 'invoice-contact@atlassian.com'
    },
    habdleOpenDialog: () => {},
    confirmAction: () => {},
    addSecondaryAction: () => {},
    primary: true
  })));

