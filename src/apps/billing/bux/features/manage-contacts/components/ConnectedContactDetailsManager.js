import { connect } from 'react-redux';
import { updateBillingContact } from 'bux/core/state/billing-contact/actions';
import { getBillingContact } from 'bux/core/state/billing-details/selectors';
import { getUser, isOrganization } from 'bux/core/state/meta/selectors';
import { getPathname } from 'bux/core/selectors/common';
import { openModalDialog } from 'bux/core/state/focused-task/actions';
import library from '../../FocusedTask/library';
import ManageContactWarningModal from './ManageContactWarningModal';
import StyledContactDetailsManager from './ContactDetailsManager';

const logPrefix = 'billOverview.billingDetails.updateMainContact';
const component = library.add(ManageContactWarningModal);

const handleOpenDialog = ({
  name,
  pathname,
  toBeMainContact,
  previousMainContact,
  confirmAction,
  isSiteAdmin
}) => {
  const dialogOpts = {
    component,
    logPrefix,
    props: {
      confirmAction,
      name,
      pathname,
      toBeMainContact,
      previousMainContact,
      isSiteAdmin
    }
  };

  return openModalDialog(dialogOpts);
};

const handleContactAction = main => email => updateBillingContact({ main, email });

export const mapStateToProps = state => ({
  billingContact: getBillingContact(state),
  currentUserEmail: getUser(state).email,
  pathname: getPathname(state),
  isSiteAdmin: !isOrganization(state),
});

export const mapDispatchToProps = {
  handleOpenDialog,
  addSecondaryContact: handleContactAction(false),
  confirmAction: handleContactAction(true)
};


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StyledContactDetailsManager);
