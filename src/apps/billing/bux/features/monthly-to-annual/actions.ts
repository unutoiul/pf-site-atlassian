import { promisifyRoutine } from 'redux-saga-routines';

import { DOWNLOAD_QUOTE, PROCESS_ORDER } from './constants';
import { downloadQuoteRoutine, processOrderRoutine } from './routines';

const downloadQuoteAction = payload => ({ payload, type: DOWNLOAD_QUOTE });
const downloadQuotePromiseCreator = promisifyRoutine(downloadQuoteRoutine);

export const downloadQuote = payload => (dispatch) => {
  dispatch(downloadQuoteAction(payload));

  return downloadQuotePromiseCreator(payload, dispatch);
};

const processOrderAction = payload => ({ payload, type: PROCESS_ORDER });
const processOrderPromiseCreator = promisifyRoutine(processOrderRoutine);

export const processOrder = payload => (dispatch) => {
  dispatch(processOrderAction(payload));

  return processOrderPromiseCreator(payload, dispatch);
};
