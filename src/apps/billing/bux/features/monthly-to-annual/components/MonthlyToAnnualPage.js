import { connect } from 'react-redux';
import { openFocusedTask, closeFocusedTask } from 'bux/core/state/focused-task/actions';
import { getFocusedTask } from 'bux/core/state/focused-task/selectors';
import { push as redirect } from 'bux/core/state/router/actions';
import FocusedTaskPage from 'bux/components/FocusedTaskPage';
import MonthlyToAnnual from 'bux/features/payment-details/components/MonthlyToAnnual';
import { isEligibleForChangingRenewalFrequency } from '../selectors';
import library from '../../FocusedTask/library';

const mapStateToProps = state => ({
  isFocusedTaskOpen: !!getFocusedTask(state),
  isAccessible: isEligibleForChangingRenewalFrequency(state),
});

const component = library.add(MonthlyToAnnual);

const mapDispatchToProps = dispatch => ({
  open: (props) => {
    dispatch(openFocusedTask({
      component,
      props,
      logPrefix: 'paymentDetails.monthly-to-annual'
    }));
  },
  exit: () => {
    dispatch(closeFocusedTask());
    dispatch(redirect('/paymentdetails'));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FocusedTaskPage);
