import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import * as React from 'react';

import { ConvertToAnnualError, OrderProgress } from 'bux/features/monthly-to-annual/types';

import { MonthlyToAnnualErrorMessage } from '../MonthlyToAnnualErrorMessage';

describe('MonthlyToAnnualErrorMessage', () => {
  const order: OrderProgress = {
    completed: false,
    successful: false,
    errorMessage: '',
    status: '',
    orderReference: 'AT-12345',
    orderId: '12345',
  };

  it('should not render component if there is no error', () => {
    const wrapper = shallow(<MonthlyToAnnualErrorMessage />);
    expect(wrapper.get(0)).to.be.null();
  });

  it('should not render component if error is generic', () => {
    const error = ConvertToAnnualError.generic();
    const wrapper = shallow(<MonthlyToAnnualErrorMessage error={error} />);
    expect(wrapper.get(0)).to.be.null();
  });

  it('should render order server error', () => {
    const error = ConvertToAnnualError.orderServer();
    const wrapper = mount(<MonthlyToAnnualErrorMessage error={error} />);

    expect(wrapper.first()).to.be.present();
    expect(wrapper.first()).to.contain.text('Failed to process your request');
    expect(wrapper.first()).to.contain.text('Please check your payment method details');
  });

  const assertProgressError = (error) => {
    const wrapper = mount(<MonthlyToAnnualErrorMessage error={error} />);

    expect(wrapper.first()).to.be.present();
    expect(wrapper.first()).to.contain.text('Failed to process your request');
    expect(wrapper.first()).to.contain.text('order reference number: AT-12345');
  };

  it('should render progress network error', () => {
    const error = ConvertToAnnualError.progressNetwork(order);
    assertProgressError(error);
  });

  it('should render progress server error', () => {
    const error = ConvertToAnnualError.progressServer(order);
    assertProgressError(error);
  });

  it('should render progress unsuccessful error', () => {
    const error = ConvertToAnnualError.progressUnsuccessful(order);
    assertProgressError(error);
  });

  it('should render progress timeout error', () => {
    const error = ConvertToAnnualError.progressTimedOut(order);
    const wrapper = mount(<MonthlyToAnnualErrorMessage error={error} />);

    expect(wrapper.first()).to.be.present();
    expect(wrapper.first()).to.contain.text('Your request is taking more than usual');
    expect(wrapper.first()).to.contain.text('order reference number: AT-12345');
  });
});
