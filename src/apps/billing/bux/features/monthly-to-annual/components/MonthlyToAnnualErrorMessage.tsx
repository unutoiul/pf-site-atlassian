import { Trans } from '@lingui/react';
import * as React from 'react';

import { ExternalLinkTo } from 'bux/components/Link';
import { Billing } from 'bux/components/PaymentWizard';
import {
  ConvertToAnnualError,
  ORDER_SERVER_ERROR, PROGRESS_NETWORK_ERROR, PROGRESS_SERVER_ERROR, PROGRESS_TIMEOUT_ERROR,
  PROGRESS_UNSUCCESSFUL_ERROR,
} from 'bux/features/monthly-to-annual/types';

interface MonthlyToAnnualErrorMessageProps {
  error?: ConvertToAnnualError;
}

export const MonthlyToAnnualErrorMessage: React.SFC<MonthlyToAnnualErrorMessageProps> = ({ error }) => {
  if (!error) {
    return null;
  }

  const orderReference = error.order && error.order.orderReference;

  let Title = () => (
    <Trans id="billing.convert-to-annual.error.failed.title">Failed to process your request</Trans>
  );

  let Message;
  switch (error.type) {
    case ORDER_SERVER_ERROR:
      Message = () => (
        <Trans id="billing.convert-to-annual.error.server-error">
          An error occurred and we could not process your request. Please check your payment method details and try
          again. If the problem persists, please contact <ExternalLinkTo.Support>our support</ExternalLinkTo.Support> for assistance.
        </Trans>
      );
      break;
    case PROGRESS_UNSUCCESSFUL_ERROR:
    case PROGRESS_NETWORK_ERROR:
    case PROGRESS_SERVER_ERROR:
      Message = () => (
        <Trans id="billing.convert-to-annual.error.progress-error">
          There was an error finalizing your request, please contact <ExternalLinkTo.Support>our support</ExternalLinkTo.Support> team
          with the following order reference number: {orderReference}.
        </Trans>
      );
      break;
    case PROGRESS_TIMEOUT_ERROR:
      Title = () => (
        <Trans id="billing.convert-to-annual.error.more-time.title">Your request is taking more than usual</Trans>
      );
      Message = () => (
        <Trans id="billing.convert-to-annual.error.timeout">
          Please contact <ExternalLinkTo.Support>our support</ExternalLinkTo.Support> team with the following order reference
          number: {orderReference}.
        </Trans>
      );
      break;
    default:
      Message = undefined;
      break;
  }

  if (!Message) {
    return null;
  }

  return (
    <Billing.ErrorMessage title={<Title />}>
      <Message />
    </Billing.ErrorMessage>
  );
};
