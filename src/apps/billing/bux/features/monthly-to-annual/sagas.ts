import { AnyAction } from 'redux';
import { delay } from 'redux-saga';
import { all, call, put, race, take, takeLatest } from 'redux-saga/effects';

import config from 'bux/common/config';
import { requestConvertToAnnualQuote, requestProcessOrder, requestProcessOrderProgress } from 'bux/core/state/api';
import { getBillEstimate } from 'bux/core/state/bill-estimate/actions';
import {
  closeNotificationAction, showNotificationByTemplate, showNotificationNetworkError,
} from 'bux/core/state/notifications/actions';

import {
  DOWNLOAD_QUOTE,
  PROCESS_ORDER,
} from './constants';
import { downloadQuoteRoutine, processOrderRoutine } from './routines';
import { ConvertToAnnualError, ORDER_NETWORK_ERROR } from './types';

// --------------------------------------
// Download quote
// --------------------------------------

export const DOWNLOADING_QUOTE_INFO_NOTIFICATION_ID = 'convert-to-annual-downloading-quote-info';
export const DOWNLOAD_QUOTE_ERROR_NOTIFICATION_ID = 'convert-to-annual-download-quote-error';
export const PREPARING_QUOTE_INFO_NOTIFICATION_ID = 'convert-to-annual-preparing-quote-info';

export const showDownloadingInfo = () => showNotificationByTemplate(DOWNLOADING_QUOTE_INFO_NOTIFICATION_ID);

export const showPreparingInfo = () => showNotificationByTemplate(PREPARING_QUOTE_INFO_NOTIFICATION_ID);

export const showDownloadError = () => showNotificationByTemplate(DOWNLOAD_QUOTE_ERROR_NOTIFICATION_ID);

export function* downloadQuote({ payload: estimateId }: AnyAction) {
  try {
    yield put(showPreparingInfo());
    const result = yield put.resolve(requestConvertToAnnualQuote({ estimateId }));

    yield put(closeNotificationAction({ id: DOWNLOAD_QUOTE_ERROR_NOTIFICATION_ID }));
    yield put(closeNotificationAction({ id: PREPARING_QUOTE_INFO_NOTIFICATION_ID }));
    yield put(showDownloadingInfo());
    yield put(downloadQuoteRoutine.success(result));
  } catch (error) {
    yield put(showDownloadError());
    yield put(downloadQuoteRoutine.failure(ConvertToAnnualError.generic()));
  }
}

export function* awaitForDownloadQuote() {
  yield takeLatest(DOWNLOAD_QUOTE, downloadQuote);
}

// --------------------------------------
// Process order
// --------------------------------------

export const showProcessOrderSuccess = () => showNotificationByTemplate('process-order-success');

export const PROCESS_ORDER_POLL_START = 'PROCESS_ORDER_POLL_START';
export const PROCESS_ORDER_POLL_COMPLETE = 'PROCESS_ORDER_POLL_COMPLETE';
export const PROCESS_ORDER_ERROR = 'PROCESS_ORDER_ERROR';
export const CHECK_ORDER_PROGRESS_EVERY = 2000;
export const ORDER_PROGRESS_CHECK_TIMEOUT = 60000;

export function* awaitForProcessOrder() {
  yield takeLatest(PROCESS_ORDER, processOrder);
}

export function* awaitForCheckOrderProgressPoolComplete() {
  yield takeLatest(PROCESS_ORDER_POLL_COMPLETE, checkOrderProgress);
}

export function* awaitForOrderError() {
  yield takeLatest(PROCESS_ORDER_ERROR, onError);
}

export function* awaitForCheckOrderProgressPoolStart() {
  while (true) {
    const { order } = yield take(PROCESS_ORDER_POLL_START);
    yield race({
      progress: call(fetchOrderProgressUntilComplete, order),
      stop: take(PROCESS_ORDER_POLL_COMPLETE),
      error: take(PROCESS_ORDER_ERROR),
      timeout: call(checkOrderProgressTimeout, order),
    });
  }
}

export function* fetchOrderProgressUntilComplete(order) {
  while (true) {
    try {
      const progress = yield put.resolve(requestProcessOrderProgress({ orderId: order.orderId }));
      if (progress.completed) {
        yield put({ type: PROCESS_ORDER_POLL_COMPLETE, order: progress });
      }
      yield call(delay, CHECK_ORDER_PROGRESS_EVERY);
    } catch (error) {
      yield put({ type: PROCESS_ORDER_ERROR, error: ConvertToAnnualError.fromProgressError(error, order) });
    }
  }
}

export function* checkOrderProgressTimeout(order) {
  yield call(delay, config.mockConvertToAnnualPoolingTimeout || ORDER_PROGRESS_CHECK_TIMEOUT);
  yield put({ type: PROCESS_ORDER_ERROR, error: ConvertToAnnualError.progressTimedOut(order) });
}

export function* checkOrderProgress({ order }: AnyAction) {
  if (order.successful) {
    yield put(getBillEstimate());
    yield put(showProcessOrderSuccess());
    yield put(processOrderRoutine.success());

    return undefined;
  }
  yield put({ type: PROCESS_ORDER_ERROR, error: ConvertToAnnualError.progressUnsuccessful(order) });
}

export function* processOrder({ payload: estimateId }: AnyAction) {
  try {
    const order = yield put.resolve(requestProcessOrder({ estimateId }));
    yield put({ type: PROCESS_ORDER_POLL_START, order });
  } catch (error) {
    yield put({ type: PROCESS_ORDER_ERROR, error: ConvertToAnnualError.fromOrderError(error) });
  }
}

export function* onError({ error }: AnyAction) {
  if (error.type === ORDER_NETWORK_ERROR) {
    yield put(showNotificationNetworkError());
  }
  yield put(processOrderRoutine.failure(error));
}

// --------------------------------------
// Export root saga
// --------------------------------------

export function* rootSaga() {
  yield all([
    call(awaitForCheckOrderProgressPoolStart),
    call(awaitForCheckOrderProgressPoolComplete),
    call(awaitForOrderError),
    call(awaitForDownloadQuote),
    call(awaitForProcessOrder),
  ]);
}

export default rootSaga;
