import { createSelector } from 'bux/helpers/reselect';
import { hasCreditCardWithinExpiryDate, hasPaypal } from 'bux/core/state/billing-details/selectors';
import {
  isExpiredInstance, isNextBillingPeriodRenewalFrequencyMonthly, getNextBillingPeriodRenewalFrequency,
  hasOnlyStrideFree
} from 'bux/core/state/bill-estimate/selectors';

export const isEligibleForChangingRenewalFrequency = createSelector([
  hasPaypal,
  hasCreditCardWithinExpiryDate,
  isNextBillingPeriodRenewalFrequencyMonthly,
  isExpiredInstance,
  hasOnlyStrideFree
], (isPaypal, hasCcWithValidExpiry, isMonthly, isExpired, onlyStride) =>
  (isPaypal || hasCcWithValidExpiry) && isMonthly && !isExpired && !onlyStride);

export const getRenewalFrequency = getNextBillingPeriodRenewalFrequency;
