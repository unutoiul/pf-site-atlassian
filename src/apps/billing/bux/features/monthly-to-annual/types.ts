
export const GENERIC_ERROR = 'GENERIC_ERROR';
export const ORDER_NETWORK_ERROR = 'ORDER_NETWORK_ERROR';
export const PROGRESS_NETWORK_ERROR = 'PROGRESS_NETWORK_ERROR';

export const ORDER_SERVER_ERROR = 'ORDER_SERVER_ERROR';
export const PROGRESS_SERVER_ERROR = 'PROGRESS_SERVER_ERROR';
export const PROGRESS_TIMEOUT_ERROR = 'PROGRESS_TIMEOUT_ERROR';
export const PROGRESS_UNSUCCESSFUL_ERROR = 'PROGRESS_UNSUCCESSFUL_ERROR';

export interface OrderProgress {
  completed: boolean;
  successful: boolean;
  errorMessage: string;
  status: string;
  orderReference: string;
  orderId: string;
}

export class ConvertToAnnualError extends Error {
  public type: string;
  public order?: OrderProgress;

  constructor(message: string) {
    super(message);
    this.type = GENERIC_ERROR;
    Object.setPrototypeOf(this, ConvertToAnnualError.prototype);
  }

  public static fromType(type: string, order?: OrderProgress): ConvertToAnnualError {
    const error = new ConvertToAnnualError(type);
    error.type = type;
    error.order = order;

    return error;
  }

  private static isNetworkError(error): boolean {
    return error && error.response && error.response.status === 408;
  }

  public static fromOrderError(error): ConvertToAnnualError {
    if (ConvertToAnnualError.isNetworkError(error)) {
      return ConvertToAnnualError.orderNetwork();
    }

    return ConvertToAnnualError.orderServer();
  }

  public static fromProgressError(error, order: OrderProgress): ConvertToAnnualError {
    if (ConvertToAnnualError.isNetworkError(error)) {
      return ConvertToAnnualError.progressNetwork(order);
    }

    return ConvertToAnnualError.progressServer(order);
  }

  public static orderNetwork(): ConvertToAnnualError {
    return ConvertToAnnualError.fromType(ORDER_NETWORK_ERROR);
  }

  public static orderServer(): ConvertToAnnualError {
    return ConvertToAnnualError.fromType(ORDER_SERVER_ERROR);
  }

  public static progressNetwork(order: OrderProgress): ConvertToAnnualError {
    return ConvertToAnnualError.fromType(PROGRESS_NETWORK_ERROR, order);
  }

  public static progressServer(order: OrderProgress): ConvertToAnnualError {
    return ConvertToAnnualError.fromType(PROGRESS_SERVER_ERROR, order);
  }

  public static progressTimedOut(order: OrderProgress): ConvertToAnnualError {
    return ConvertToAnnualError.fromType(PROGRESS_TIMEOUT_ERROR, order);
  }

  public static generic(): ConvertToAnnualError {
    return ConvertToAnnualError.fromType(GENERIC_ERROR);
  }

  public static progressUnsuccessful(order: OrderProgress): ConvertToAnnualError {
    return ConvertToAnnualError.fromType(PROGRESS_UNSUCCESSFUL_ERROR, order);
  }
}
