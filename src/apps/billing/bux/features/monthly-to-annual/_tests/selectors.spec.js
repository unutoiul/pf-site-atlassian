import { expect } from 'chai';
import sinon from 'sinon';
import proxyquire from 'bux/helpers/proxyquire';

describe('monthly-to-annual selectors:', () => {
  let mockHasPaypal;
  let mockHasCreditCardWithinExpiryDate;
  let mockGetNextBillingPeriodRenewalFrequency;
  let mockIsNextBillingPeriodRenewalFrequencyMonthly;
  let mockIsExpiredInstance;
  let selectors;
  let mockHasOnlyStrideFree;

  beforeEach(() => {
    mockHasPaypal = sinon.stub();
    mockHasCreditCardWithinExpiryDate = sinon.stub();
    mockGetNextBillingPeriodRenewalFrequency = sinon.stub();
    mockIsNextBillingPeriodRenewalFrequencyMonthly = sinon.stub();
    mockIsExpiredInstance = sinon.stub();
    mockHasOnlyStrideFree = sinon.stub();
    selectors = proxyquire.noCallThru().load('../selectors', {
      'bux/core/state/billing-details/selectors': {
        hasCreditCardWithinExpiryDate: mockHasCreditCardWithinExpiryDate,
        hasPaypal: mockHasPaypal
      },
      'bux/core/state/bill-estimate/selectors': {
        getNextBillingPeriodRenewalFrequency: mockGetNextBillingPeriodRenewalFrequency,
        isNextBillingPeriodRenewalFrequencyMonthly: mockIsNextBillingPeriodRenewalFrequencyMonthly,
        isExpiredInstance: mockIsExpiredInstance,
        hasOnlyStrideFree: mockHasOnlyStrideFree,
      },
    });
  });

  describe('isEligibleForChangingRenewalFrequency:', () => {
    describe('should return true', () => {
      it('when user has valid cc, non-expired instance, monthly renewal frequency, not only Stride free', () => {
        mockHasCreditCardWithinExpiryDate.returns(true);
        mockIsNextBillingPeriodRenewalFrequencyMonthly.returns(true);
        mockIsExpiredInstance.returns(false);
        mockHasOnlyStrideFree.returns(false);
        expect(selectors.isEligibleForChangingRenewalFrequency()).to.be.true();
      });
      it('when user has paypal, non-expired instance, monthly renewal frequency, not only Stride free', () => {
        mockHasPaypal.returns(true);
        mockHasCreditCardWithinExpiryDate.returns(false);
        mockIsNextBillingPeriodRenewalFrequencyMonthly.returns(true);
        mockIsExpiredInstance.returns(false);
        mockHasOnlyStrideFree.returns(false);
        expect(selectors.isEligibleForChangingRenewalFrequency()).to.be.true();
      });
    });

    describe('should return false', () => {
      it('when user has invalid cc, non-expired instance, monthly renewal frequency, not only Stride free', () => {
        mockHasCreditCardWithinExpiryDate.returns(false);
        mockIsNextBillingPeriodRenewalFrequencyMonthly.returns(true);
        mockIsExpiredInstance.returns(false);
        mockHasOnlyStrideFree.returns(false);
        expect(selectors.isEligibleForChangingRenewalFrequency()).to.be.false();
      });

      it('when user has valid cc, expired instance, monthly renewal frequency, not only Stride free', () => {
        mockHasCreditCardWithinExpiryDate.returns(true);
        mockIsNextBillingPeriodRenewalFrequencyMonthly.returns(true);
        mockIsExpiredInstance.returns(true);
        mockHasOnlyStrideFree.returns(false);
        expect(selectors.isEligibleForChangingRenewalFrequency()).to.be.false();
      });

      it('when user has valid cc, non-expired instance, non-monthly renewal frequency, not only Stride free', () => {
        mockHasCreditCardWithinExpiryDate.returns(true);
        mockIsNextBillingPeriodRenewalFrequencyMonthly.returns(false);
        mockIsExpiredInstance.returns(false);
        mockHasOnlyStrideFree.returns(false);
        expect(selectors.isEligibleForChangingRenewalFrequency()).to.be.false();
      });

      it('when user has valid cc, non-expired instance, monthly renewal frequency, only Stride free', () => {
        mockHasCreditCardWithinExpiryDate.returns(true);
        mockIsNextBillingPeriodRenewalFrequencyMonthly.returns(true);
        mockIsExpiredInstance.returns(false);
        mockHasOnlyStrideFree.returns(true);
        expect(selectors.isEligibleForChangingRenewalFrequency()).to.be.false();
      });
    });
  });

  describe('getRenewalFrequency:', () => {
    it('should the renewal frequency for the next billing period', () => {
      mockGetNextBillingPeriodRenewalFrequency.returns('CUSTOM');
      expect(selectors.getRenewalFrequency()).to.equal('CUSTOM');
    });
  });
});
