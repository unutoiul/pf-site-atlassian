import { expect } from 'chai';
import { delay } from 'redux-saga';
import { testSaga } from 'redux-saga-test-plan';
import { all, call, take, takeLatest } from 'redux-saga/effects';
import * as sinon from 'sinon';

import { getBillEstimate } from 'bux/core/state/bill-estimate/actions';
import { closeNotificationAction, showNotificationNetworkError } from 'bux/core/state/notifications/actions';
import { rewiremock } from 'bux/helpers/rewire-mock';

import {
  DOWNLOAD_QUOTE_ERROR_NOTIFICATION_ID, PREPARING_QUOTE_INFO_NOTIFICATION_ID,
  PROCESS_ORDER_ERROR, PROCESS_ORDER_POLL_COMPLETE, PROCESS_ORDER_POLL_START,
} from 'bux/features/monthly-to-annual/sagas';
import { ConvertToAnnualError, OrderProgress } from 'bux/features/monthly-to-annual/types';

import { DOWNLOAD_QUOTE, PROCESS_ORDER } from '../constants';
import { downloadQuoteRoutine, processOrderRoutine } from '../routines';

describe('Monthly to annual sagas', () => {
  const estimateId = '123';
  const quoteResult = { quoteLink: 'http://test.org' };
  const requestConvertToAnnualQuote = sinon.stub().returns(quoteResult);
  const orderResult = { successful: false, completed: false };
  const requestProcessOrder = sinon.stub().returns(orderResult);
  const requestProcessOrderProgress = sinon.stub().returns({});
  let sagas;

  before(async () => {
    sagas = await rewiremock.module(async () => import('../sagas'), {
      'bux/core/state/api': {
        requestConvertToAnnualQuote,
        requestProcessOrder,
        requestProcessOrderProgress,
      },
    });
  });

  it('should export 1 sub sagas', () => {
    const saga = sagas.default();
    expect(saga.next().value).to.be.deep.equal(all([
      call(sagas.awaitForCheckOrderProgressPoolStart),
      call(sagas.awaitForCheckOrderProgressPoolComplete),
      call(sagas.awaitForOrderError),
      call(sagas.awaitForDownloadQuote),
      call(sagas.awaitForProcessOrder),
    ]));
  });

  describe('download quote saga', () => {
    it('trigger', () => {
      const flow = sagas.awaitForDownloadQuote();
      expect(flow.next().value).to.deep.equal(takeLatest(DOWNLOAD_QUOTE, sagas.downloadQuote));
    });

    it('normal flow', () => {
      return testSaga(sagas.downloadQuote, { estimateId })
        .next()
        .put(sagas.showPreparingInfo())
        .next()
        .put.resolve(requestConvertToAnnualQuote({ estimateId }))
        .next(quoteResult)
        .put(closeNotificationAction({ id: DOWNLOAD_QUOTE_ERROR_NOTIFICATION_ID }))
        .next()
        .put(closeNotificationAction({ id: PREPARING_QUOTE_INFO_NOTIFICATION_ID }))
        .next()
        .put(sagas.showDownloadingInfo())
        .next()
        .put(downloadQuoteRoutine.success(quoteResult))
        .next()
        .isDone();
    });

    it('error flow', () => {
      return testSaga(sagas.downloadQuote, { estimateId })
        .next()
        .put(sagas.showPreparingInfo())
        .next()
        .put.resolve(requestConvertToAnnualQuote({ estimateId }))
        .throw(new Error())
        .put(sagas.showDownloadError())
        .next()
        .put(downloadQuoteRoutine.failure(ConvertToAnnualError.generic()))
        .next()
        .isDone();
    });
  });

  describe('process order saga', () => {
    it('trigger', () => {
      const flow = sagas.awaitForProcessOrder();
      expect(flow.next().value).to.deep.equal(takeLatest(PROCESS_ORDER, sagas.processOrder));
    });

    it('processOrder', () => {
      return testSaga(sagas.processOrder, { estimateId })
        .next()
        .put.resolve(requestProcessOrder({ estimateId }))
        .next(orderResult)
        .put({ type: sagas.PROCESS_ORDER_POLL_START, order: orderResult })
        .next()
        .isDone();
    });

    describe('checkOrderProgress', () => {
      it('successful order', () => {
        const successfulOrder = { successful: true, completed: true };

        return testSaga(sagas.checkOrderProgress, { order: successfulOrder })
          .next()
          .put(getBillEstimate())
          .next()
          .put(sagas.showProcessOrderSuccess())
          .next()
          .put(processOrderRoutine.success())
          .next()
          .isDone();
      });

      it('unsuccessful order', () => {
        const unsuccessfulOrder: OrderProgress = {
          successful: false,
          completed: true,
          errorMessage: '',
          orderId: '',
          orderReference: '',
          status: '',
        };

        return testSaga(sagas.checkOrderProgress, { order: unsuccessfulOrder })
          .next()
          .put({ type: PROCESS_ORDER_ERROR, error: ConvertToAnnualError.progressUnsuccessful(unsuccessfulOrder) })
          .next()
          .isDone();
      });
    });

    describe('onError', () => {
      it('should put notification if network error', () => {
        const error = ConvertToAnnualError.orderNetwork();

        return testSaga(sagas.onError, { error })
          .next()
          .put(showNotificationNetworkError())
          .next()
          .put(processOrderRoutine.failure(error))
          .next()
          .isDone();
      });

      it('should not put network notification', () => {
        const error = ConvertToAnnualError.orderServer();

        return testSaga(sagas.onError, { error })
          .next()
          .put(processOrderRoutine.failure(error))
          .next()
          .isDone();
      });
    });

    it('checkOrderProgressTimeout', () => {
      const order: OrderProgress = {
        successful: true,
        completed: true,
        errorMessage: '',
        orderId: '',
        orderReference: '',
        status: '',
      };

      return testSaga(sagas.checkOrderProgressTimeout, { order })
        .next()
        .call(delay, sagas.ORDER_PROGRESS_CHECK_TIMEOUT)
        .next()
        .put({ type: PROCESS_ORDER_ERROR, error: ConvertToAnnualError.progressTimedOut(order) })
        .next()
        .isDone();
    });

    describe('fetchOrderProgressUntilComplete', () => {
      const order: OrderProgress = {
        successful: true,
        completed: true,
        errorMessage: '',
        orderId: '12345',
        orderReference: '',
        status: '',
      };

      it('should yield until order is completed', () => {
        return testSaga(sagas.fetchOrderProgressUntilComplete, order)
          .next()
          .put.resolve(requestProcessOrderProgress({ orderId: order.orderId }))
          .next({ ...order, completed: false })
          .call(delay, sagas.CHECK_ORDER_PROGRESS_EVERY)
          .next()
          .put.resolve(requestProcessOrderProgress({ orderId: order.orderId }))
          .next({ ...order, completed: false })
          .call(delay, sagas.CHECK_ORDER_PROGRESS_EVERY)
          .next()
          .put.resolve(requestProcessOrderProgress({ orderId: order.orderId }))
          .next(order)
          .put({ type: PROCESS_ORDER_POLL_COMPLETE, order })
          .next()
          .call(delay, sagas.CHECK_ORDER_PROGRESS_EVERY);
          // Will never be done because it will be killed by
          // race in awaitForCheckOrderProgressPoolStart
      });

      it('should put error if fetch throws', () => {
        const error = new Error();

        return testSaga(sagas.fetchOrderProgressUntilComplete, order)
          .next()
          .put.resolve(requestProcessOrderProgress({ orderId: order.orderId }))
          .throw(error)
          .put({ type: PROCESS_ORDER_ERROR, error: ConvertToAnnualError.fromProgressError(error, order) });
          // Will never be done because it will be killed by
          // race in awaitForCheckOrderProgressPoolStart
      });
    });

    it('awaitForCheckOrderProgressPoolStart', () => {
      const order: OrderProgress = {
        successful: true,
        completed: true,
        errorMessage: '',
        orderId: '12345',
        orderReference: '',
        status: '',
      };

      return testSaga(sagas.awaitForCheckOrderProgressPoolStart)
        .next()
        .take(PROCESS_ORDER_POLL_START)
        .next({ order })
        .race({
          progress: call(sagas.fetchOrderProgressUntilComplete, order),
          stop: take(PROCESS_ORDER_POLL_COMPLETE),
          error: take(PROCESS_ORDER_ERROR),
          timeout: call(sagas.checkOrderProgressTimeout, order),
        });
    });

    it('awaitForOrderError', () => {
      const flow = sagas.awaitForOrderError();
      expect(flow.next().value).to.deep.equal(takeLatest(PROCESS_ORDER_ERROR, sagas.onError));
    });

    it('awaitForCheckOrderProgressPoolComplete', () => {
      const flow = sagas.awaitForCheckOrderProgressPoolComplete();
      expect(flow.next().value).to.deep.equal(takeLatest(PROCESS_ORDER_POLL_COMPLETE, sagas.checkOrderProgress));
    });

    it('awaitForProcessOrder', () => {
      const flow = sagas.awaitForProcessOrder();
      expect(flow.next().value).to.deep.equal(takeLatest(PROCESS_ORDER, sagas.processOrder));
    });
  });
});
