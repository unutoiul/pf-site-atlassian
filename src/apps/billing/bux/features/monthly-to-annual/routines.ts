import { createRoutine } from 'redux-saga-routines';

export const downloadQuoteRoutine = createRoutine('DOWNLOAD_QUOTE');
export const processOrderRoutine = createRoutine('PROCESS_ORDER');
