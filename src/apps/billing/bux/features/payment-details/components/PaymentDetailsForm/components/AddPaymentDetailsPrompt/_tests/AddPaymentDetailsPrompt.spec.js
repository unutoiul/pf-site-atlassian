import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import { RouterButton } from 'bux/components/Button';
import AddPaymentDetailsPrompt from '../AddPaymentDetailsPrompt';

describe('AddPaymentDetailsPrompt: ', () => {
  it('should render component', () => {
    const wrapper = shallow(<AddPaymentDetailsPrompt />);

    expect(wrapper).to.be.present();
    expect(wrapper.find(RouterButton)).to.be.present();
  });
});
