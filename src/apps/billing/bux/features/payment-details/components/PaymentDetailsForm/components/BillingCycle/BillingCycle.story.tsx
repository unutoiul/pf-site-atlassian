import { storiesOf } from '@storybook/react';

import { inContentChrome, mockedFunction, render } from 'bux/common/helpers/storybook';

import { BillingCycleImpl, BillingCycleProps } from './BillingCycle';

const props: BillingCycleProps = {
  isAnnual: false,
  isEligible: true,
  nextRenewalDate: '2018-08-26',
  onClick: mockedFunction,
};

storiesOf('BUX|Features/PaymentDetails/BillingCycle', module)
  .add('Monthly', () => inContentChrome(render(BillingCycleImpl, props)))
  .add('Annual', () => inContentChrome(render(BillingCycleImpl, { ...props, isAnnual: true })))
  .add('Not eligible', () => inContentChrome(render(BillingCycleImpl, { ...props, isEligible: true })));
