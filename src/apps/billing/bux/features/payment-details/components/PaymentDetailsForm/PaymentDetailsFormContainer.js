import PropTypes from 'prop-types';
import React from 'react';
import ContentAsideLayout from 'bux/components/layout/ContentAsideLayout';
import LogScope from 'bux/components/log/Scope/index';
import billEstimate from '../../../bill-estimate';
import { default as PaymentDetailsForm } from './PaymentDetailsForm';

const PaymentDetailsFormContainer = ({ onSubmitSuccess, onSubmitFail }) => (
  <ContentAsideLayout
    content={(
      <PaymentDetailsForm
        onSubmitSuccess={onSubmitSuccess}
        onSubmitFail={onSubmitFail}
      />
      )}
    aside={(
      <div>
        <LogScope prefix="billEstimate">
          <billEstimate.components.BillEstimateSummary />
        </LogScope>
      </div>
    )}
  />
);

PaymentDetailsFormContainer.propTypes = {
  onSubmitSuccess: PropTypes.func,
  onSubmitFail: PropTypes.func
};


export default PaymentDetailsFormContainer;
