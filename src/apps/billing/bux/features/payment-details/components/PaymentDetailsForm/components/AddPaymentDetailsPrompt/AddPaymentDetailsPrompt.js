import { Trans } from '@lingui/react';
import React from 'react';
import { node, string } from 'prop-types';
import cx from 'classnames';
import { AutoFocusInside } from 'react-focus-lock';
import { Analytics } from 'bux/components/Analytics';
import { RouterButton } from 'bux/components/Button';
import CreditCards from 'bux/static/credit-cards.svgx';
import resizeSVG from 'bux/common/helpers/resizeSVG';
import styles from './AddPaymentDetailsPrompt.less';

const Image = resizeSVG(CreditCards, 40);

const AddPaymentDetailsPrompt = ({ message, className }) => (
  <div data-test="payment-details-prompt" className={cx(className, styles.well)}>
    <div className={styles.imageBlock}><Image /></div>
    <AutoFocusInside>
      <Analytics.UI as="addBillingDetailsButton">
        <RouterButton to="/paymentdetails/add" name="add" primary>
          <Trans id="billing.payment-details.add.header">Add payment method</Trans>
        </RouterButton>
      </Analytics.UI>
    </AutoFocusInside>
    <p className={styles.label}>
      <Trans id="billing.payment-details.add.methods-accepted">
        We accept major credit/debit cards and PayPal.
      </Trans>
    </p>
    {message && <p>{message}</p>}
  </div>
);

AddPaymentDetailsPrompt.propTypes = {
  message: node,
  className: string
};

export default AddPaymentDetailsPrompt;
