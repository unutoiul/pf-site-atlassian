import { Trans } from '@lingui/react';
import React from 'react';
import { AutoFocusInside } from 'react-focus-lock';
import PaymentMethod from 'bux/components/PaymentMethod';
import EditLink from 'bux/components/Widget/EditLink';
import Card from 'bux/components/blocks/Card';
import { Analytics } from 'bux/components/Analytics';
import AddPaymentDetailsPrompt from './AddPaymentDetailsPrompt';
import styles from './PaymentMethodPanel.less';

const hasDetails = details => details && Object.keys(details).length > 0;

const PaymentMethodPanel = ({ paymentMethod, details }) => {
  if (hasDetails(details)) {
    return (
      <div data-test="payment-method-panel">
        <h2><Trans id="billing.payment-method.header">Payment method</Trans></h2>
        <Card className={styles.card}>
          <PaymentMethod paymentMethod={paymentMethod} details={details} />
          <AutoFocusInside>
            <Analytics.UI as="editPaymentMethodButton" objectType="creditCardDetails" attributes={{ paymentMethod }}>
              <EditLink name="edit" to="/paymentdetails/update" />
            </Analytics.UI>
          </AutoFocusInside>
        </Card>
      </div>
    );
  }
  return <AddPaymentDetailsPrompt className={styles.panel} />;
};

PaymentMethodPanel.propTypes = {
  ...PaymentMethod.propTypes,
};

export default PaymentMethodPanel;
