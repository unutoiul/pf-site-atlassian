import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import PaymentMethod from 'bux/components/PaymentMethod';
import EditLink from 'bux/components/Widget/EditLink';
import { PAYMENT_METHOD_PAYPAL } from 'bux/core/state/billing-details/constants';
import AddPaymentDetailsPrompt from '../AddPaymentDetailsPrompt';
import PaymentMethodPanel from '../PaymentMethodPanel';

describe('PaymentMethodPanel: ', () => {
  it('should render payment method', () => {
    const props = {
      paymentMethod: PAYMENT_METHOD_PAYPAL,
      details: {
        email: 'user@example.com'
      }
    };
    const wrapper = shallow(<PaymentMethodPanel {...props} />);

    expect(wrapper).to.be.present();
    expect(wrapper.find(EditLink)).to.be.present();
    expect(wrapper.find(PaymentMethod)).to.be.present();
    expect(wrapper.find(PaymentMethod)).to.have.props(props);
    expect(wrapper.find(AddPaymentDetailsPrompt)).to.not.be.present();
  });
  it('should render add payment prompt when there is no details', () => {
    const props = {
      paymentMethod: PAYMENT_METHOD_PAYPAL,
      details: {}
    };
    const wrapper = shallow(<PaymentMethodPanel {...props} />);

    expect(wrapper).to.be.present();
    expect(wrapper.find(EditLink)).to.not.be.present();
    expect(wrapper.find(PaymentMethod)).to.not.be.present();
    expect(wrapper.find(AddPaymentDetailsPrompt)).to.be.present();
  });
});
