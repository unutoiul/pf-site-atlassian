import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import AkSectionMessage from '@atlaskit/section-message';

import { ExternalLinkTo } from 'bux/components/Link';

import { BillingCycleImpl, BillingCycleProps } from '../BillingCycle';

describe('BillingCycleImpl', () => {
  const props: BillingCycleProps = {
    isEligible: true,
    isAnnual: false,
    nextRenewalDate: '',
    onClick: sinon.stub(),
  };

  beforeEach(() => {
    (props.onClick as sinon.SinonStub).reset();
  });

  describe('with switch to annual link', () => {
    it('should render if eligible and not annual', () => {
      const wrapper = shallow(<BillingCycleImpl {...props} />);
      expect(wrapper).to.be.present();
      expect(wrapper.find('LinkButton')).to.be.present();
      wrapper.find('LinkButton').simulate('click');
      // tslint:disable-next-line: no-unbound-method
      expect(props.onClick).to.be.called();
    });
  });

  describe('with contact us link', () => {
    it('should render if annual', () => {
      const wrapper = shallow(<BillingCycleImpl {...props} isAnnual={true} isEligible={true} />);
      expect(wrapper).to.be.present();
      expect(wrapper.find('LinkButton')).to.not.be.present();
      expect(wrapper.find(ExternalLinkTo.PurchasingLicense)).to.be.present();
    });
    it('should render if not eligible', () => {
      const wrapper = shallow(<BillingCycleImpl {...props} isAnnual={false} isEligible={false} />);
      expect(wrapper).to.be.present();
      expect(wrapper.find('LinkButton')).to.not.be.present();
      expect(wrapper.find(ExternalLinkTo.PurchasingLicense)).to.be.present();
    });
  });

  describe('with renewal date info', () => {
    it('should render if annual', () => {
      const wrapper = mount(<BillingCycleImpl {...props} isAnnual={true} nextRenewalDate="2018-08-28" />);
      expect(wrapper).to.be.present();
      expect(wrapper.find(AkSectionMessage)).to.be.present();
      expect(wrapper.find(AkSectionMessage)).to.contain.text('Aug 28, 2018');
    });
  });
});
