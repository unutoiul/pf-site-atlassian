import { withI18n, I18n, Trans } from '@lingui/react';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import cx from 'classnames';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { bindPromiseCreators } from 'redux-saga-routines';
import { enchantedForm } from 'bux/components/redux-form';
import { Field, Form, formValueSelector, SubmissionError } from 'redux-form';
import { WideInputField } from 'bux/components/field/InputField';
import { SubmitButton } from 'bux/components/Button';
import LogScope from 'bux/components/log/Scope/index';
import { createValidator } from 'bux/common/helpers/validation';
import { constructFullAddress, getAddress, getAddressOrUserLocation } from 'bux/common/helpers/address';
import { updateBillingDetailsPromise } from 'bux/core/state/billing-details/actions';
import { closeNotification, showNotificationByTemplate } from 'bux/core/state/notifications/actions';
import { getEntitlementSections } from 'bux/core/selectors/entitlements';
import { 
  isNextBillingPeriodRenewalFrequencyAnnual,
  isNextBillInNewCurrency as isAfterTheCutOverDate, 
  upcomingCurrencyExtras, 
} from 'bux/core/state/bill-estimate/selectors';
import {
  getBillingDetails,
  getInvoiceContactEmail,
  getPaymentMethod,
  getPaymentMethodDetails,
  hasPaymentMethod
} from 'bux/core/state/billing-details/selectors';
import { Analytics, withAnalytics, mergeEventData } from 'bux/components/Analytics';
import { validateBillingDetails } from 'bux/core/state/api';
import CurrencyUpdateNotification from 'bux/components/CurrencyUpdateNotification';
import FeatureFlagged from 'bux/components/FeatureFlags/FeatureFlagged';
import ConnectedContactDetailsManager from '../../../manage-contacts';
import PaymentMethodPanel from './components/PaymentMethodPanel';
import styles from './PaymentDetailsForm.less';
import BillingAddress from '../BillingAddress';
import LegalAgreement from '../LegalAgreement';
import { BillingCycle } from './components/BillingCycle';

const sendTrackEvent = (payload, props) => props.analytics.sendTrackEvent(mergeEventData({
  attributes: {
    linkContainer: 'module',
    paymentMethod: props.paymentMethod,
    billingPeriod: props.isAnnual ? 'annual' : 'monthly',
    products: props.entitlements.map(({ productKey }) => productKey),
  }
}, payload));

export class PaymentDetailsForm extends Component {
  static validationRules = {
    invoiceContactEmail: 'email'
  };

  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    updateBillingDetails: PropTypes.func.isRequired,
    isAnnual: PropTypes.bool.isRequired,
    originalAddress: PropTypes.object,
    formValidator: PropTypes.func.isRequired,
    submitting: PropTypes.bool.isRequired,
    autofill: PropTypes.func.isRequired,
    touch: PropTypes.func.isRequired,
    country: PropTypes.string,
    state: PropTypes.string,
    initialValues: PropTypes.object,
    paymentMethod: PropTypes.string,
    paymentDetails: PropTypes.shape({
      expiryMonth: PropTypes.number,
      expiryYear: PropTypes.number,
      name: PropTypes.string,
      suffix: PropTypes.string,
      type: PropTypes.string
    }),
    hasPaymentMethodOnAccount: PropTypes.bool,
    i18n: PropTypes.object,
    analytics: PropTypes.object,
    validateBillingDetail: PropTypes.func.isRequired,
    isNextBillInNewCurrency: PropTypes.bool,
    upcomingCurrencyExtra: PropTypes.object,
    entitlements: PropTypes.arrayOf(PropTypes.object),
  };

  static contextTypes = {
    sendAnalyticEvent: PropTypes.func
  };

  state = {
    disableSave: false
  };

  onSubmit = form =>
    this.validateFormValues(form)
      .then(() => form)
      .then(this.props.updateBillingDetails)
      .then(this.trackSuccessAnalytics)
      .catch((err) => {
        throw new SubmissionError(err);
      });
      
  trackSuccessAnalytics = (formWithPayment) => {
    sendTrackEvent({
      action: 'saved',
      actionSubject: 'billingDetails',
      actionSubjectId: 'billingDetailsUpdated',
    }, this.props);

    if (
      formWithPayment.invoiceContactEmail &&
      formWithPayment.invoiceContactEmail !== this.props.initialValues.invoiceContactEmail
    ) {
      this.context.sendAnalyticEvent('invoiceContact.provided');
    }
  };

  validateFormValues(form) {
    const errors = this.props.formValidator(form, { i18n: this.props.i18n });
    return Object.keys(errors).length === 0
      ? Promise.resolve()
      : Promise.reject(errors);
  }

  shouldDisableSave(isSaveBlocked) {
    this.setState({ disableSave: isSaveBlocked && !isSaveBlocked.props.isWarning });
  }

  render() {
    const {
      handleSubmit,
      submitting,
      autofill,
      touch,
      country,
      state,
      initialValues,
      paymentMethod,
      paymentDetails,
      isAnnual,
      hasPaymentMethodOnAccount,
      validateBillingDetail,
      upcomingCurrencyExtra,
      isNextBillInNewCurrency,
    } = this.props;

    return (
      <Form
        className={cx(styles.form, 'payment-details-form')}
        onSubmit={handleSubmit(this.onSubmit)}
        autoComplete="billing-details-form"
      >
        <div>
          {
            <FeatureFlagged flags={['2018-upcoming-currency-notice']}>
              {
                upcomingCurrencyExtra &&
                <div className={styles.noticesRow} data-test="currency-update-notices-row">
                <CurrencyUpdateNotification 
                  upcomingCurrencyExtra={upcomingCurrencyExtra} 
                  isNextBillInNewCurrency={isNextBillInNewCurrency} 
                />
                </div>
              }
            </FeatureFlagged>
          }
          {(isAnnual || hasPaymentMethodOnAccount) &&
            <section className={styles.section}>
              <LogScope prefix="monthly-to-annual">
                <BillingCycle isAnnual={isAnnual} />
              </LogScope>
            </section>
          }
          { !isAnnual &&
            <section className={styles.section}>
              <LogScope prefix="paymentMethod">
                <PaymentMethodPanel paymentMethod={paymentMethod} details={paymentDetails} />
              </LogScope>
            </section>
          }
          {
            (isAnnual || hasPaymentMethodOnAccount) &&
              <section className={cx(styles.section, styles.formContent, 'billing-address-section')}>
                <h2><Trans id="billing.payment-details.billing-address.header">Billing address</Trans></h2>
                <BillingAddress
                  autofill={autofill}
                  touch={touch}
                  country={country}
                  state={state}
                  initialValues={initialValues}
                  validateCountry={validateBillingDetail}
                  shouldDisableSave={isSaveBlocked => this.shouldDisableSave(isSaveBlocked)}
                />
              </section>
          }

          <div className={cx(styles.section, styles.formContent)}>
            <h2><Trans id="billing.payment-details.billing-contacts.header">Billing contacts</Trans></h2>
            <ConnectedContactDetailsManager />
            <Analytics.UI as="sendCopiesOfInvoicesToField">
              <I18n>{({ i18n }) => (
                <Field
                  name="invoiceContactEmail"
                  component={WideInputField}
                  labelText={
                    i18n.t('billing.payment-details.billing-contacts.send-invoices')`Send copies of invoices to`
                  }
                />
              )}</I18n>
            </Analytics.UI>
          </div>
        </div>

        <LegalAgreement showTerms={false} />

        <div>
          <Analytics.UI as="saveButton">
            <SubmitButton
              className="paymentDetailsSubmit"
              disabled={submitting || this.state.disableSave}
              spinning={submitting}
              attributes={{
                linkContainer: 'pageContent',
              }}
              name="save"
            >
              <Trans id="billing.payment-details.save-button">Save</Trans>
            </SubmitButton>
          </Analytics.UI>
        </div>
      </Form>
    );
  }
}

const FORM_NAME = 'PaymentDetailsForm';
const selector = formValueSelector(FORM_NAME);

const formValidator = createValidator([
  BillingAddress.validationRules,
  PaymentDetailsForm.validationRules
]);

export const mapStateToProps = state => ({
  formValidator,
  isAnnual: isNextBillingPeriodRenewalFrequencyAnnual(state),
  originalAddress: getAddress(getBillingDetails(state)),
  country: selector(state, 'country'),
  state: selector(state, 'state'),
  paymentMethod: getPaymentMethod(state),
  paymentDetails: getPaymentMethodDetails(state),
  entitlements: getEntitlementSections(state).active,
  initialValues: {
    organisationName: getBillingDetails(state).organisationName,
    taxpayerId: getBillingDetails(state).taxpayerId,
    invoiceContactEmail: getInvoiceContactEmail(state),
    country: getBillingDetails(state).country,
    ...getAddressOrUserLocation(state)(getBillingDetails),

    addressLineFull: constructFullAddress(state)(getBillingDetails),
    originalAddressLineFull: constructFullAddress(state)(getBillingDetails)
  },
  hasPaymentMethodOnAccount: hasPaymentMethod(state),
  upcomingCurrencyExtra: upcomingCurrencyExtras(state),
  isNextBillInNewCurrency: isAfterTheCutOverDate(state),
});

export const mapDispatchToProps = dispatch => ({
  ...bindPromiseCreators({
    updateBillingDetails: updateBillingDetailsPromise,
  }, dispatch),
  ...bindActionCreators({
    validateBillingDetail: validateBillingDetails
  }, dispatch)
});

export const DecoratedPaymentDetailsForm = withAnalytics(withI18n()(enchantedForm({
  form: FORM_NAME,
  validate: formValidator,
  onSubmitSuccess: (result, dispatch) => {
    closeNotification('payment-error')(dispatch);
  },
  onSubmitFail: (errors, dispatch, submitError, props) => {
    if (errors && !('@@redux-saga/SAGA_ACTION' in errors)) {
      sendTrackEvent({
        action: 'failed',
        actionSubject: 'billingDetails',
        actionSubjectId: 'missingBillingDetailsError',
        attributes: {
          linkContainer: 'module'
        },
      }, props);
      dispatch(showNotificationByTemplate('billing-details-error'));
    }
  }
})(PaymentDetailsForm)));

const ConnectedPaymentDetailsForm = connect(
  mapStateToProps,
  mapDispatchToProps
)(DecoratedPaymentDetailsForm);

export default ConnectedPaymentDetailsForm;
