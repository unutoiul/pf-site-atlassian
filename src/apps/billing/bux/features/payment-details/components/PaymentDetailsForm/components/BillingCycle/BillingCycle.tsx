import { Trans } from '@lingui/react';
import * as React from 'react';
import { connect } from 'react-redux';

import AkCalendarIcon from '@atlaskit/icon/glyph/calendar';
import AkSectionMessage from '@atlaskit/section-message';
import { akColorN500 } from '@atlaskit/util-shared-styles';

import { Analytics } from 'bux/components/Analytics';
import Date from 'bux/components/Date';
import { ExternalLinkTo } from 'bux/components/Link';
import LinkButton from 'bux/components/Link/Button';
import { getBill } from 'bux/core/state/bill-estimate/selectors';
import { push } from 'bux/core/state/router/actions';
import { isEligibleForChangingRenewalFrequency } from 'bux/features/monthly-to-annual/selectors';

import styles from './styles.less';

export interface BillingCycleProps {
  isAnnual: boolean;
  nextRenewalDate: string;
  isEligible: boolean;
  onClick(): void;
}

const CalendarIconWithColor = (color) => () => (<AkCalendarIcon label="Calendar" primaryColor={color} />);

export const BillingCycleImpl: React.SFC<BillingCycleProps> = ({ isEligible, isAnnual, nextRenewalDate, onClick }) => (
  <div className={styles.billingCyclePanel} data-test="billing-cycle-panel">
    <h2><Trans id="billing.billing-cycle.header">Billing period</Trans></h2>

    { isAnnual ?
      <React.Fragment>
        <p className={styles.billingCycle}>
          <Trans id="billing.billing-cycle.current.annual">Annual</Trans>
        </p>
        <div className="change-renewal-frequency">
          <p>
            <Trans id="billing.billing-cycle.switch-to-monthly-contact-us">
              If you want to switch to monthly payment,{' '}
              <ExternalLinkTo.PurchasingLicense>contact us</ExternalLinkTo.PurchasingLicense>.
            </Trans>
          </p>
        </div>
        <div className={styles.info} data-test="billing-cycle-renewal-msg">
          <AkSectionMessage icon={CalendarIconWithColor(akColorN500)} apperance="authentication">
            <p className={styles.sendQuoteMessage}>
              <Trans id="billing.billing-cycle.annual-renewal-info">
                We send a renewal quote 60 days prior to the renewal date (<Date value={nextRenewalDate}/>).
              </Trans>
            </p>
          </AkSectionMessage>
        </div>
      </React.Fragment>
      :
      <React.Fragment>
        <p className={styles.billingCycle}>
          <Trans id="billing.billing-cycle.current.monthly">Monthly</Trans>
        </p>
        <div className="change-renewal-frequency">
        { isEligible ?
          <Analytics.UI as="switchToAnnualPaymentLink">
            <LinkButton onClick={onClick} className="change-renewal-frequency__link">
              <Trans id="billing.billing-cycle.switch-to-annual">Switch to annual payment</Trans>
            </LinkButton>
          </Analytics.UI>
          :
          <p>
            <Trans id="billing.billing-cycle.switch-to-annual-contact-us">
              If you want to switch to annual payment,{' '}
              <ExternalLinkTo.PurchasingLicense>contact us</ExternalLinkTo.PurchasingLicense>.
            </Trans>
          </p>
        }
        </div>
      </React.Fragment>
    }
  </div>
);

const mapStateToProps = state => ({
  nextRenewalDate: getBill(state).currentBillingPeriod.endDate,
  isEligible: isEligibleForChangingRenewalFrequency(state),
});

const mapDispatchToProps = {
  onClick: () => push('/paymentdetails/switch-to-annual'),
};

export const BillingCycle = connect<{}>(mapStateToProps, mapDispatchToProps)(BillingCycleImpl);
