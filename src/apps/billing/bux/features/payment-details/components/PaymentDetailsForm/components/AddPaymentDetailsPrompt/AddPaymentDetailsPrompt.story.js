import { storiesOf } from '@storybook/react';
import { inContentChrome, render } from 'bux/common/helpers/storybook';

import AddPaymentDetailsPrompt from './AddPaymentDetailsPrompt';

storiesOf('BUX|Features/PaymentDetails/PaymentDetailsForm', module)
  .add('AddPaymentDetailsPrompt', () => inContentChrome(render(AddPaymentDetailsPrompt)));

