import React from 'react';
import { Provider } from 'bux/helpers/redux';
import sinon from 'sinon';
import fetchMock from 'fetch-mock-5';
import proxyquire from 'bux/helpers/proxyquire';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';
import createStore from 'bux/core/createStore';
import identity from 'bux/common/helpers/identity';
import { PAYMENT_METHOD_CREDIT_CARD } from 'bux/core/state/billing-details/constants';
import BillingAddress from '../../BillingAddress';

const DEFAULT_PROPS = {
  formValidator: () => ({}),
  updateBillingDetails: identity,
  validateBillingDetail: identity,
  handleSubmit: identity,
  autofill: identity,
  touch: identity,
  originalAddress: {},
  initialValues: {},
  entitlements: [],
  submitting: false,
  pristine: false,
  isAnnual: false,
  hasPaymentMethodOnAccount: true,
  analytics: {
    sendTrackEvent: () => {},
    sendUIEvent: () => {},
  },
};

describe('PaymentDetailsForm: ', () => {
  let PaymentDetailsFormImports;
  let DecoratedPaymentDetailsForm;
  let PaymentDetailsFormComponent;
  let updateBillingDetailsPromise;
  let getBillingDetails;
  let getPaymentMethod;
  let getPaymentMethodDetails;
  let getAddress;
  let getAddressOrUserLocation;
  let constructFullAddress;
  let getInvoiceContactEmail;
  let isNextBillingPeriodRenewalFrequencyAnnual;
  let hasPaymentMethod;
  let upcomingCurrencyExtras;
  let isNextBillInNewCurrency;
  let getEntitlementSections;

  const billingAddress = {

    addressLineFull: 'full addr',
    originalAddressLineFull: 'full addr',
    addressLine1: 'addressLine1',
    addressLine2: 'addressLine2',
    city: 'Sydney',
    state: 'NSW',
    country: 'AU',
    postcode: '2000',
    firstName: 'Charlie',

    taxpayerId: '12345678200',
    organisationName: 'Atlassian'
  };

  const invoiceContact = {
    email: 'me@atlassian.com'
  };

  const creditCard = {
    suffix: '1111',
    expiryMonth: 1,
    expiryYear: 2017,
    type: 'Visa',
    name: 'Superplasma Team'
  };

  beforeEach(() => {
    updateBillingDetailsPromise = sinon.stub();
    getBillingDetails = () => (billingAddress);
    getPaymentMethod = () => (PAYMENT_METHOD_CREDIT_CARD);
    getPaymentMethodDetails = () => (creditCard);
    isNextBillingPeriodRenewalFrequencyAnnual = sinon.stub().returns(false);
    getAddress = sinon.stub().returnsArg(0);
    getAddressOrUserLocation = () => selector => (selector());
    constructFullAddress = () => () => 'full addr';
    getInvoiceContactEmail = () => (invoiceContact.email);
    hasPaymentMethod = sinon.stub().returns(true);
    upcomingCurrencyExtras = sinon.stub().returns({});
    isNextBillInNewCurrency = sinon.stub().returns(false);
    const data = [];
    getEntitlementSections = sinon.stub().returns({ active: data });

    PaymentDetailsFormImports = proxyquire.noCallThru().load('../PaymentDetailsForm', {
      'bux/core/state/billing-details/actions': { updateBillingDetailsPromise },
      'bux/common/helpers/address': { getAddress, getAddressOrUserLocation, constructFullAddress },
      'bux/core/state/bill-estimate/selectors': { 
        isNextBillingPeriodRenewalFrequencyAnnual, 
        upcomingCurrencyExtras,
        isNextBillInNewCurrency,
      },
      'bux/core/state/billing-details/selectors': {
        getPaymentMethod,
        getPaymentMethodDetails,
        getBillingDetails,
        getInvoiceContactEmail,
        hasPaymentMethod
      },
      'bux/core/selectors/entitlements': { getEntitlementSections },
      'bux/features/manage-contacts': () => <div>42</div>
    });

    PaymentDetailsFormComponent = PaymentDetailsFormImports.PaymentDetailsForm;
    DecoratedPaymentDetailsForm = PaymentDetailsFormImports.DecoratedPaymentDetailsForm;
  });

  describe('mapStateToProps', () => {
    it('should initialise the email field to the invoice contact email', () => {
      const mapStateToProps = PaymentDetailsFormImports.mapStateToProps;
      const props = mapStateToProps({});
      expect(props).to.deep.equal({
        formValidator: props.formValidator,
        isAnnual: false,
        paymentMethod: PAYMENT_METHOD_CREDIT_CARD,
        paymentDetails: {
          expiryMonth: 1,
          expiryYear: 2017,
          suffix: '1111',
          type: 'Visa',
          name: 'Superplasma Team'
        },
        initialValues: {
          ...billingAddress,
          invoiceContactEmail: invoiceContact.email
        },
        entitlements: [],
        originalAddress: billingAddress,
        isNextBillInNewCurrency: false,
        country: undefined,
        state: undefined,
        hasPaymentMethodOnAccount: true,
        upcomingCurrencyExtra: {},
      });
    });
  });

  describe('mapDispatchToProps', () => {
    it('should pass updateBillingDetails as a prop', () => {
      const mapDispatchToProps = PaymentDetailsFormImports.mapDispatchToProps(() => {});
      expect(mapDispatchToProps).to.have.property('updateBillingDetails');
    });

    it('should pass validateBillingDetails as a prop', () => {
      const mapDispatchToProps = PaymentDetailsFormImports.mapDispatchToProps(() => {});
      expect(mapDispatchToProps).to.have.property('validateBillingDetail');
    });
  });

  describe('Billing details form', () => {
    describe('when the form has valid values', () => {
      let form;
      let wrapper;
      const formValidator = sinon.stub();

      const spinUp = () => {
        const creditCardURL = '/admin/rest/billing-ux/api/credit-card/session';
        fetchMock.post(creditCardURL, {
          creditCardSessionId: 'SESSION000000000000000000000001',
          creditCardToken: '5123450000002346'
        });

        const store = createStore();
        wrapper = mount(<Provider store={store}>
          <DecoratedPaymentDetailsForm
            formValidator={formValidator}
            updateBillingDetails={updateBillingDetailsPromise}
            originalAddress={{}}
            initialValues={{ ...billingAddress }}
            validateCountry={sinon.stub()}
            entitlements={[]}
            isAnnual={isNextBillingPeriodRenewalFrequencyAnnual()}
          />
        </Provider>);
        form = wrapper.find('.payment-details-form').hostNodes();
      };

      it('should call updateBillingDetailsPromise with form on submit', () => {
        updateBillingDetailsPromise.returns(Promise.resolve());
        formValidator.returns({});
        spinUp();
        return form.props().onSubmit({ form: 'data' }).then(() => {
          expect(updateBillingDetailsPromise).to.have.been.called();
        });
      });

      it('should call updateBillingDetails with form on submit', () => {
        updateBillingDetailsPromise.returns(Promise.resolve());
        formValidator.returns({ some: 'error' });
        spinUp();
        return form.props().onSubmit({ form: 'data' }).then(() => {
          expect(updateBillingDetailsPromise).not.to.have.been.called();
        });
      });

      it('should not display payment method for Annual Account', () => {
        isNextBillingPeriodRenewalFrequencyAnnual.returns(true);
        formValidator.returns({});
        spinUp();
        expect(wrapper.find('PaymentMethodPanel')).not.to.be.present();
        return form.props().onSubmit({ form: 'data' }).then(() => {
          expect(updateBillingDetailsPromise).to.have.been.called();
        });
      });

      it('should not display billing address when there is not payment details set for monthly', () => {
        hasPaymentMethod.returns(false);
        spinUp();
        expect(wrapper.find(BillingAddress)).to.not.be.present();
      });
    });

    describe('when the form has invalid values', () => {
      let form;

      const spinUp = (initialValues = {}) => {
        const creditCardURL = '/admin/rest/billing-ux/api/credit-card/session';
        fetchMock.post(creditCardURL, {
          creditCardSessionId: 'SESSION000000000000000000000001',
          creditCardToken: '5123450000002346'
        });

        const store = createStore();
        const wrapper = mount(<Provider store={store}>
          <DecoratedPaymentDetailsForm
            formValidator={() => ({})}
            updateBillingDetails={updateBillingDetailsPromise}
            originalAddress={{}}
            entitlements={[]}
            initialValues={initialValues}
            isAnnual
          />
        </Provider>);
        form = wrapper.find('.payment-details-form').hostNodes();
      };

      const allNull = {
        country: undefined,
        addressLine1: undefined,
        city: undefined,
        state: undefined,
        postcode: undefined,
        organisationName: undefined,
      };

      const requiredExceptState = {
        country: ['This field is required.'],
        addressLine1: ['This field is required.'],
        city: ['This field is required.'],
        state: undefined,
        postcode: ['This field is required.'],
        organisationName: ['This field is required.']
      };

      const requiredWithState = {
        country: undefined,
        addressLine1: ['This field is required.'],
        city: ['This field is required.'],
        state: ['This field is required.'],
        postcode: ['This field is required.'],
        organisationName: ['This field is required.']
      };

      it('should validate values', () => {
        spinUp();
        updateBillingDetailsPromise.returns(Promise.resolve());
        const { addressLineFull, ...validation } = form.props().onSubmit({ form: 'data' });
        expect(addressLineFull).to.be.not.null();
        expect({
          ...allNull,
          ...validation
        }).to.be.deep.equal({
          ...requiredExceptState
        });
      });

      it('should validate values then country set', () => {
        spinUp({ country: 'AU' });
        updateBillingDetailsPromise.returns(Promise.resolve());
        const { addressLineFull, ...validation } = form.props().onSubmit({ form: 'data' });
        expect(addressLineFull).to.be.not.null();
        expect({
          ...allNull,
          ...validation
        }).to.be.deep.equal({
          ...requiredWithState
        });
      });

      it('should validate postcode for US', () => {
        spinUp({
          country: 'US',
          postcode: 'invalid'
        });
        updateBillingDetailsPromise.returns(Promise.resolve());
        expect({
          ...allNull,
          ...form.props().onSubmit({ form: 'data' })
        }).to.be.deep.equal({
          ...requiredWithState,
          postcode: 'Please enter a valid postal code.',
        });
      });

      it('should not validate postcode for AU', () => {
        spinUp({
          country: 'AU',
          postcode: 'invalid'
        });
        updateBillingDetailsPromise.returns(Promise.resolve());
        expect({
          ...allNull,
          ...form.props().onSubmit({ form: 'data' })
        }).to.be.deep.equal({
          ...requiredWithState,
          postcode: undefined
        });
      });
    });
  });

  describe('submit button', () => {
    const renderComponent = props => shallow(<PaymentDetailsFormComponent
      {...DEFAULT_PROPS}
      {...props}
    />);

    it('should be disabled while submitting', () => {
      const wrapper = renderComponent({ submitting: true });
      expect(wrapper.find('.paymentDetailsSubmit')).to.have.prop('disabled');
    });

    it('should disabled when the form has validation errors', () => {
      const wrapper = renderComponent({ submitting: false });
      expect(wrapper.find('.paymentDetailsSubmit')).to.have.prop('disabled');
    });

    it('should disabled while disableSave is true', () => {
      const wrapper = renderComponent({ submitting: false });
      wrapper.setState({ disableSave: true });
      expect(wrapper.find('.paymentDetailsSubmit')).to.have.prop('disabled');
    });
  });
});
