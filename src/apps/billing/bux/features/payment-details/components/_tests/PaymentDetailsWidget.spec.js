import configureMockStore from 'redux-mock-store';
import React from 'react';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';
import { Provider } from 'bux/helpers/redux';
import { PAYMENT_METHOD_CREDIT_CARD } from 'bux/core/state/billing-details/constants';
import { INITIAL_BILLING_DETAILS_STATE } from 'bux/core/state/billing-details/reducer';
import { INITIAL_BILL_STATE } from 'bux/core/state/bill-estimate/reducer';
import { CURRENT } from 'bux/core/state/bill-estimate/constants';

import PaymentDetailsWidget from '../PaymentDetailsWidget';

describe('PaymentDetailsWidget: ', () => {
  const mockStoreCreator = (override, error) => configureMockStore()({
    billEstimate: { [CURRENT]: { ...INITIAL_BILL_STATE, data: (override.billEstimate || {}), error } },
    billingDetails: { ...INITIAL_BILLING_DETAILS_STATE, data: (override.billingDetails || {}), error }
  });

  const billingDetails = {
    paymentMethod: PAYMENT_METHOD_CREDIT_CARD,
    creditCard: {
      type: 'Visa',
      suffix: '1234',
      name: 'Alana Smith',
      expiryMonth: 11,
      expiryYear: 2018
    }
  };

  describe('Smoke tests', () => {
    it('should render widget with CC set', () => {
      const mockedStore = mockStoreCreator({
        billingDetails
      });

      mount(<Provider store={mockedStore}><PaymentDetailsWidget /></Provider>);
    });

    it('should render widget without CC', () => {
      const mockedStore = mockStoreCreator({
        billingDetails: {}
      });

      mount(<Provider store={mockedStore}><PaymentDetailsWidget /></Provider>);
    });
  });

  it('should map state to props', () => {
    const mockedStore = mockStoreCreator({
      billingDetails
    });

    const wrapper = shallow(<PaymentDetailsWidget store={mockedStore} />);
    const props = wrapper.props();

    expect(props.details.name).to.equal('Alana Smith');
    expect(props.details.expiryMonth).to.equal(11);
    expect(props.details.expiryYear).to.equal(2018);
    expect(props.details.suffix).to.equal('1234');
    expect(props.details.type).to.equal('Visa');
    expect(props.isAnnual).to.equal(false);
    expect(props.metadata.error).to.be.false();
  });

  it('should map annual to props', () => {
    const mockedStore = mockStoreCreator({
      billEstimate: {
        nextBillingPeriod: {
          renewalFrequency: 'ANNUAL'
        }
      }
    });

    const wrapper = shallow(<PaymentDetailsWidget store={mockedStore} />);
    const props = wrapper.props();

    expect(props.isAnnual).to.equal(true);
  });

  it('should map error state to props', () => {
    const mockedStore = mockStoreCreator({}, { status: 500 });

    const wrapper = shallow(<PaymentDetailsWidget store={mockedStore} />);
    const props = wrapper.props();

    expect(props.metadata.error.status).to.equal(500);
  });
});

