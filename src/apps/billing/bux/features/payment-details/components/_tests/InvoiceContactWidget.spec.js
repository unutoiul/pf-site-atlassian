import configureMockStore from 'redux-mock-store';
import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';

import InvoiceContactWidget from '../InvoiceContactWidget';

describe('InvoiceContactWidget: ', () => {
  const mockStoreCreator = (billing, error) => configureMockStore()({
    billingDetails: { data: billing, error }
  });

  it('should map state to props', () => {
    const mockedStore = mockStoreCreator({
      firstName: 'doctor',
      lastName: 'who',
      thirdName: 'matters',
      email: 'bob@example.com',
      invoiceContact: {
        email: 'doctor@who.com'
      }
    });

    const wrapper = shallow(<InvoiceContactWidget store={mockedStore} />);
    const props = wrapper.props();

    expect(props.invoiceEmail).to.equal('doctor@who.com');
    expect(props.billingContact).to.deep.equal({
      firstName: 'doctor',
      lastName: 'who',
      email: 'bob@example.com'
    });
    expect(props.metadata.error).to.be.false();
  });

  it('should map error state to props', () => {
    const mockedStore = mockStoreCreator({}, { status: 500 });

    const wrapper = shallow(<InvoiceContactWidget store={mockedStore} />);
    const props = wrapper.props();

    expect(props.metadata.error.status).to.equal(500);
  });
});