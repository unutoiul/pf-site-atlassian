import React from 'react';
import sinon from 'sinon';
import { expect } from 'chai';
import { mount } from 'enzyme';
import { Provider } from 'bux/helpers/redux';
import createStore from 'bux/core/createStore';
import proxyquire from 'bux/helpers/proxyquire';
import { reduxForm } from 'redux-form';
import config from 'bux/common/config';
import connectLogRoot from 'bux/components/log/root';

import { 
  isValidForShortForm, 
  constructAddressProp, 
  filterState, 
  processValidationResponse,
  countryChangeBlockedNotification, 
  countryChangeNotification 
} from '../AddressFields';

describe('AddressFields: ', () => {
  let getCountryList;
  let getUserLocation;
  let AddressFields;

  const DEFAULT_PROPS = {
    onCountryChange: sinon.stub()
  };

  beforeEach(() => {
    getCountryList = sinon.stub().returns([{
      isoCode: 'AU',
      nativeName: 'Straya',
      name: 'Straya',
      currency: 'usd'
    }]);

    getUserLocation = sinon.stub().returns({});
    AddressFields = proxyquire.noCallThru().load('../AddressFields', {
      'bux/core/state/meta/selectors': { getCountryList, getUserLocation }
    }).default;
  });

  const createWrapper = (props) => {
    const store = createStore();
    const Container = parentProps => <AddressFields {...DEFAULT_PROPS} {...parentProps} {...props} />;
    const Form = reduxForm({ form: 'testForm' })(connectLogRoot('test')(Container));
    return mount(<Provider store={store}>
      <Form />
                 </Provider>);
  };

  it('should map empty state to props', () => {
    getUserLocation.returns({
      latitude: null,
      longitude: null
    });
    const wrapper = createWrapper();

    const target = wrapper.find('AddressField');
    const props = target.props();
    expect(props.restrict).to.be.deep.equal({
      country: '',
      focusPoint: null
    });
  });

  it('should map state to props', () => {
    getUserLocation.returns({
      latitude: 1,
      longitude: 2
    });
    const wrapper = createWrapper({
      country: 'AU'
    });

    const target = wrapper.find('AddressField');
    const props = target.props();
    expect(props.restrict).to.be.deep.equal({
      country: 'AU',
      focusPoint: [1, 2]
    });
  });

  it('should render a list of countries', () => {
    const wrapper = createWrapper();
    const countrySelector = wrapper.find('SelectField').at(0);
    expect(countrySelector.prop('input').name).to.be.equal('country');
    expect(countrySelector.prop('data')).to.be.deep.equal([
      { id: 'AU', displayName: 'Straya' }
    ]);
  });

  it('should add states and countries', () => {
    const wrapper = createWrapper({
      country: 'AU',
      defaultShortMode: false
    });
    const target = wrapper.find('AddressFields');
    const props = target.props();
    expect(props.countries).to.have.length(1);
    expect(props.states).to.have.length(8);
  });

  it('should reset fields on CountryChange', () => {
    const touch = sinon.stub();
    const autofill = sinon.stub();
    const onCountryChange = sinon.stub();
    const wrapper = createWrapper({
      touch,
      autofill,
      onCountryChange
    });
    wrapper.find('AddressFields').props().onCountryChange();

    expect(onCountryChange).to.be.called();
    const fields = ['addressLineFull', 'state', 'postcode', 'city'];
    fields.forEach((field) => {
      expect(touch).to.be.calledWith(field);
      expect(autofill).to.be.calledWith(field);
    });
  });

  describe('utils', () => {
    describe('filterState', () => {
      it('should return nothing for an empty state', () => expect(filterState('', 'AU')).to.be.undefined());
      it('should not change state for unknown country', () => expect(filterState('ANY', '00')).to.be.equal('ANY'));
      it('should return nothing a wrong state', () => expect(filterState('WSN', 'AU')).to.be.undefined());
      it('should return an existing state', () => expect(filterState('NSW', 'AU')).to.be.equal('NSW'));
    });

    describe('constructAddressProp', () => {
      it('should format result', () => {
        expect(constructAddressProp({
          region: { name: 'New South Wales', code: 'NSW' },
          locality: { name: 'Sydney' },
          street: { name: 'Pitt Street', label: '343 Pitt Street' },
          postalcode: '2000'
        }, 'AU')).to.be.deep.equal({
          addressLine1: '343 Pitt Street',
          addressLine2: '',
          city: 'Sydney',
          postcode: '2000',
          state: 'NSW'
        });
      });

      it('should format house-precise result', () => {
        expect(constructAddressProp({
          street: { name: 'Pitt Street', house: 343, label: 'Pitt Street' },
        }, 'AU').addressLine1).to.be.equal('343 Pitt Street');
      });

      it('should format house-precise result', () => {
        expect(constructAddressProp({
          street: {
            name: 'Pitt Street', premise: 20, house: 343, label: 'Pitt Street' 
          },
        }, 'AU').addressLine1).to.be.equal('20/343 Pitt Street');
      });

      it('should return undefined state for wrong country', () => {
        expect(constructAddressProp({
          region: { name: 'New South Wales', code: 'NSW' },
          locality: { name: 'Sydney' },
          street: { name: 'Pitt Street', label: '343 Pitt Street' },
          postalcode: '2000'
        }, 'US')).to.be.deep.equal({
          addressLine1: '343 Pitt Street',
          addressLine2: '',
          city: 'Sydney',
          postcode: '2000',
          state: undefined
        });
      });

      it('should not modify state for country without restrictions', () => {
        expect(constructAddressProp({
          region: { name: 'New South Wales', code: 'NSW' },
          locality: { name: 'Sydney' },
          street: { name: 'Pitt Street', label: '343 Pitt Street' },
          postalcode: '2000'
        }, '00')).to.be.deep.equal({
          addressLine1: '343 Pitt Street',
          addressLine2: '',
          city: 'Sydney',
          postcode: '2000',
          state: 'NSW'
        });
      });

      it('should handle half empty case result', () => {
        expect(constructAddressProp({
          region: { name: 'New South Wales' },
        }, 'AU')).to.be.deep.equal({
          addressLine1: undefined,
          addressLine2: '',
          city: undefined,
          postcode: undefined,
          state: undefined
        });
      });

      it('should handle empty case result', () => {
        expect(constructAddressProp({}, '')).to.be.deep.equal({
          addressLine1: undefined,
          addressLine2: '',
          city: undefined,
          postcode: undefined,
          state: undefined
        });
      });
    });
  });

  describe('validation', () => {
    it('should require state for listedCountries', () => {
      expect(AddressFields.validationRules({ country: 'AU' }).state).to.equal('required');
      expect(AddressFields.validationRules({ country: 'US' }).state).to.equal('required');
    });

    it('should not require state for reset of the world', () => {
      expect(AddressFields.validationRules({ country: 'DE' }).state).to.be.undefined();
      expect(AddressFields.validationRules({ country: 'GB' }).state).to.be.undefined();
    });

    it('should return blocked notification if only irreconcilable error is present', () => {
      const error = { reconcilable: [], irreconcilable: [{ error: 'sample' }] };
      const validation = processValidationResponse(error)();

      expect(validation).to.deep.equal(countryChangeBlockedNotification());
    });

    it('should return warning notification if only reconcilable error is present', () => {
      const error = { irreconcilable: [], reconcilable: [{ error: 'sample' }] };
      const validation = processValidationResponse(error)();

      expect(validation).to.deep.equal(countryChangeNotification());
    });

    it('should return blocked notification if both reconcilable and irreconcilable error are present', () => {
      const error = { reconcilable: [{ error: 'sample' }], irreconcilable: [{ error: 'sample' }] };
      const validation = processValidationResponse(error)();

      expect(validation).to.deep.equal(countryChangeBlockedNotification());
    });

    it('should return undefined if both reconcilable and irreconcilable error are empty', () => {
      const error = { reconcilable: [], irreconcilable: [] };
      const validation = processValidationResponse(error)();

      expect(validation).to.deep.equal(undefined);
    });

    if (config.autocomplete) {
      describe('isValidForShortForm', () => {
        it('should be valid for empty data', () => {
          expect(isValidForShortForm({})).to.be.true();

          expect(isValidForShortForm({
            state: 'NSW' //should be ignored
          })).to.be.true();
        });

        it('should be valid for full data', () => {
          expect(isValidForShortForm({
            addressLine1: {},
            city: {},
            postcode: {},
            state: {}
          })).to.be.true();
        });

        it('should be valid for full data', () => {
          expect(isValidForShortForm({
            addressLine1: {},
            city: {},
            state: {}
          })).to.be.false();
        });
      });
    }
  });
});
