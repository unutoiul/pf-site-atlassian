import { Trans } from '@lingui/react';
import React from 'react';
import { bool, node } from 'prop-types';
import { connect } from 'react-redux';
import { Analytics } from 'bux/components/Analytics';
import { ExternalLinkTo } from 'bux/components/Link';
import { isOrganization as isOrganizationSelector } from 'bux/core/state/meta/selectors';
import styles from './LegalAgreement.less';

const PrivacyPolicyLink = ({ children }) => (
  <Analytics.UI as="privacyPolicyLink">
    <ExternalLinkTo.PrivacyPolicy>{ children }</ExternalLinkTo.PrivacyPolicy>
  </Analytics.UI>
);
PrivacyPolicyLink.propTypes = {
  children: node.isRequired,
};

export const DumbLegalAgreement = ({
  showTerms = true,
  isOrganization = false,
}) => {
  const CloudPricingLink = isOrganization
    ? ExternalLinkTo.CloudPricingOrg
    : ExternalLinkTo.CloudPricing;

  return (
    <div>
      <div className={styles.legalVisaLabel}>
        <p>
          <Trans id="billing.legal-agreement.statement">
            Your billing statement will display Atlassian B.V., located in Amsterdam, Netherlands.
            Atlassian Pty Limited, our principal place of business, is at
            Level 6, 341 George Street, Sydney NSW Australia 2000.
          </Trans>
        </p>
        <p>
          <Trans id="billing.legal-agreement.cc-issuer-fees">
            Your credit card issuer may charge foreign transaction or cross-border
            fees in addition to the total price above.
          </Trans>
        </p>
      </div>
      {showTerms &&
      <span className={styles.legalLabel}>
          <Trans id="billing.legal-agreement.agreement-acceptance">
            By clicking Save you accept the{' '}
            <ExternalLinkTo.CustomerAgreement>Atlassian Customer Agreement</ExternalLinkTo.CustomerAgreement>,{' '}
            <PrivacyPolicyLink>Privacy Policy</PrivacyPolicyLink>{' '}
            and agree to pay fees charged in accordance with our{' '}
            <CloudPricingLink>
              pricing
            </CloudPricingLink>.
          </Trans>
        </span>
      }
    </div>
  );
};

DumbLegalAgreement.propTypes = {
  showTerms: bool,
  isOrganization: bool,
};

const LegalAgreement = connect(state => ({
  isOrganization: isOrganizationSelector(state),
}))(DumbLegalAgreement);

export default LegalAgreement;
