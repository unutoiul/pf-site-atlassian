import React from 'react';
import { storiesOf } from '@storybook/react';
import { inContentChrome } from 'bux/common/helpers/storybook';

import { DumbLegalAgreement } from './LegalAgreement';

storiesOf('BUX|Features/PaymentDetails/LegalAgreement', module)
  .add('LegalAgreement Site', () => inContentChrome(<DumbLegalAgreement showTerms />))
  .add('LegalAgreement AAH', () => inContentChrome(<DumbLegalAgreement showTerms isOrganization />));

