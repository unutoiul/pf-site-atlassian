import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import natsort from 'natsort';
import { Trans } from '@lingui/react';

import { Media } from 'common/responsive/Matcher';

import config from 'bux/common/config';
import FullAddressFields from 'bux/components/form-sections/AddressFields';
import ShortAddressFields from 'bux/components/form-sections/ShortAddressFields';
import { constructFullAddress } from 'bux/common/helpers/address';
import { getRegionsFor as getRegionsAPI } from 'bux/common/api/autocomplete';
import { getCountryList, getUserLocation } from 'bux/core/state/meta/selectors';

import { ExternalLinkTo } from 'bux/components/Link';
import LogScope from '../../../components/log/Scope';


const natisort = natsort({ insensitive: true });
const nameSort = (a, b) => natisort(a.displayName, b.displayName);

const getCountries = countries =>
  countries
    .map(element => ({
      id: element.isoCode,
      displayName: element.name
    }))
    .sort(nameSort);

const getRegions = country => getRegionsAPI(country)
  .filter(region => !!region.iso)
  .map(region => ({
    id: region.iso,
    displayName: region.name
  }))
  .sort(nameSort);

export const filterState = (state, country) => {
  const states = getRegions(country);
  return states.length
    ? (states.find(region => region.id === state) || {}).id
    : state;
};

const houseNumber = ({ premise, house }) => {
  if (premise) {
    if (house) {
      return `${premise}/${house}`;
    }
  }
  return house;
};

export const constructAddressProp = ({
  street, locality, region, postalcode
}, country) => ({
  addressLine1: street && [houseNumber(street), street.label].filter(address => !!address).join(' '),
  addressLine2: '',
  city: locality && locality.name,
  state: filterState(region && region.code, country),
  postcode: postalcode
});

export const isValidForShortForm = ({
  addressLine1, city, postcode, state
}) => (
  !!config.autocomplete && (// no autocomplete present
    (!!addressLine1 && !!city && !!postcode && !!state) ||
    (!addressLine1 && !city && !postcode)
  )
);

export const countryChangeNotification = futureCurrency => (
  <LogScope prefix="countryChangeNotification" isWarning>
    <span key="countryChangeWarning" className="form-message">
      <Trans id="billing.address-form.country-change-warning">
        Payment currency will now be {futureCurrency} for all Atlassian services for 
        which you're the primary billing contact.
      </Trans>
    </span>
  </LogScope>
);

export const countryChangeBlockedNotification = () => (
  <LogScope prefix="countryChangeNotification">
    <span key="countryChangeError" className="form-message">
      <Trans id="billing.address-form.country-change-error">
        <ExternalLinkTo.Support
          href={config.links('support')}
          logPrefix="learn-more"
          target="_blank"
        >Contact Customer Support
        </ExternalLinkTo.Support> to change your billing country.
      </Trans>
    </span>
  </LogScope>
);

export const processValidationResponse = validationResponse => () => {
  const irreconcilableError = validationResponse.irreconcilable;
  const reconcilableError = validationResponse.reconcilable;

  if (irreconcilableError && Object.keys(irreconcilableError).length !== 0) {
    return countryChangeBlockedNotification();
  } else if (reconcilableError && Object.keys(reconcilableError).length !== 0) {
    return countryChangeNotification(reconcilableError[0].toCurrency);
  }

  return undefined;
};

class AddressFieldsContainer extends Component {
  static validationRules = props => ({
    country: 'required',
    addressLine1: 'required',
    city: 'required',
    postcode: 'required',

    ...(getRegions(props.country).length ? { state: 'required' } : {})
  });

  static propTypes = {
    autofill: PropTypes.func.isRequired,
    touch: PropTypes.func.isRequired,
    onCountryChange: PropTypes.func.isRequired,
    country: PropTypes.string.isRequired,
    countryList: PropTypes.array,
    userLocation: PropTypes.shape({
      latitude: PropTypes.number,
      longitude: PropTypes.number
    }),
    formComponent: PropTypes.string,
    defaultShortMode: PropTypes.bool,
    validateCountry: PropTypes.func,
    shouldDisableSave: PropTypes.func,
  };

  static defaultProps = {
    // eslint-disable-next-line react/default-props-match-prop-types
    country: '',
    formComponent: '',
    defaultShortMode: false
  };

  state = {
    shortMode: this.props.defaultShortMode,
    countries: getCountries(this.props.countryList),
    states: this.props.country ? getRegions(this.props.country) : [],
    autoCompleteWasSuccessful: true
  };

  componentWillReceiveProps(nextProps) {
    if (this.props.country && this.props.country !== nextProps.country) {
      this.onCountryChange();
    }
    if (this.props.countryList !== nextProps.countryList) {
      this.setState({ countries: getCountries(this.props.countryList) });
    }
    if (!nextProps.country) {
      this.setState({ states: [] });
    } else if (this.props.country !== nextProps.country) {
      this.setState({ states: getRegions(nextProps.country) });
    }
  }

  onCountryChange = () => {
    const resetFields = [
      'addressLineFull',
      'addressLine1',
      'addressLine2',
      'city',
      'state',
      'postcode'
    ];
    resetFields.forEach((field) => {
      const fieldKey = this.getAutofillKey(field);
      this.props.autofill(fieldKey, '');
      this.props.touch(fieldKey);
    });

    this.setState({
      addressValue: ''
    });

    this.props.onCountryChange();
  }

  onMapzenResult = ({ addressDetails }) => {
    const { autofill } = this.props;
    const address = constructAddressProp(addressDetails, this.props.country);

    const isValidForShort = isValidForShortForm(address);

    if (isValidForShort) {
      // autogenerated field
      address.addressLineFull = constructFullAddress(address)(x => x);
      this.setState({
        addressValue: address.addressLineFull
      });
      Object.keys(address)
        .forEach(key => autofill(this.getAutofillKey(key), address[key]));
    }

    this.setState({ shortMode: isValidForShort });
  }

  onMapzenStatus = (autoCompleteWasSuccessful) => {
    this.setState({
      autoCompleteWasSuccessful
    });
    // the only way to trigger form revalidation
    this.props.autofill(this.getAutofillKey('hiddenTrigger'), autoCompleteWasSuccessful);
  }

  getAutofillKey(key) {
    const { formComponent } = this.props;
    return (formComponent ? `${formComponent}.${key}` : key);
  }

  enableFullMode = (validation) => {
    this.setState({ shortMode: false, validation });
  }


  render() {
    const { 
      country, 
      userLocation, 
      formComponent,
      validateCountry,
      shouldDisableSave
    } = this.props;
    const { countries, states, autoCompleteWasSuccessful } = this.state;
    const focusPoint = userLocation.latitude ? [userLocation.latitude, userLocation.longitude] : null;
    return (
      <div>
        <Media.Matches>
          {(_, pickMatch) => {
            const AddressFields = pickMatch({
              // iOS and React-autocomplete are not besties
              mobile: FullAddressFields,
              tablet: this.state.shortMode ? ShortAddressFields : FullAddressFields,
            });
            return (
              <AddressFields
                country={country}
                countries={countries}
                states={states}
                focusPoint={focusPoint}
                onCountryChange={this.onCountryChange}
                onFill={this.onMapzenResult}
                onAutocompleteStatus={this.onMapzenStatus}
                autoCompleteFailed={!autoCompleteWasSuccessful}
                formComponent={formComponent}
                toggleFullMode={this.enableFullMode}
                appliedAddressValue={this.state.addressValue}
                processValidationResponse={processValidationResponse}
                validation={this.state.validation}
                shouldDisableSave={shouldDisableSave}
                validateCountry={validateCountry}
              />
            );
          }}
        </Media.Matches>
      </div>
    );
  }
}

export const mapStateToProps = state => ({
  countryList: getCountryList(state),
  userLocation: getUserLocation(state)
});

const ConnectedAddressFieldsContainer = connect(mapStateToProps)(AddressFieldsContainer);

export default ConnectedAddressFieldsContainer;
