import React from 'react';
import { Provider } from 'bux/helpers/redux';
import proxyquire from 'bux/helpers/proxyquire';
import EditCreditCard from 'bux/helpers/tests/components/MockEditCreditCard';
import PayPalButton from 'bux/helpers/tests/components/MockPayPalButton';
import { expect } from 'chai';
import { mount } from 'enzyme';
import getAnalytic from 'bux/components/log/helpers/getAnalytic';
import { SubmitButton } from 'bux/components/Button';
import { PAYMENT_METHOD_CREDIT_CARD, PAYMENT_METHOD_PAYPAL } from 'bux/core/state/billing-details/constants';
import sinon from 'sinon';

describe('UpdatePaymentMethod:', () => {
  let closeAction;
  let getPaymentMethod;
  let getPaymentMethodDetails;
  let updatePaymentMethodPromise;
  let UpdatePaymentMethod;
  let analytics;

  before(() => {
    closeAction = sinon.stub();
    updatePaymentMethodPromise = sinon.stub().resolves();
    getPaymentMethod = sinon.stub();
    getPaymentMethodDetails = sinon.stub();

    UpdatePaymentMethod = proxyquire.noCallThru().load('../UpdatePaymentMethod', {
      'bux/components/ak/creditcard/EditCreditCard': EditCreditCard,
      'bux/components/ak/paypal': PayPalButton,
      'bux/core/state/billing-details/actions': { updatePaymentMethodPromise },
      'bux/core/state/billing-details/selectors': { getPaymentMethod, getPaymentMethodDetails }
    }).default;
    analytics = getAnalytic();
  });

  beforeEach(() => {
    closeAction.reset();
    updatePaymentMethodPromise.reset();
  });

  const mountComponent = () => {
    const props = {
      closeAction
    };
    const wrapper = mount(<Provider>
        <UpdatePaymentMethod {...props} />
        </Provider>, analytics.context);
    return wrapper;
  };

  describe('When current is credit card', () => {
    beforeEach(() => {
      getPaymentMethod.returns(PAYMENT_METHOD_CREDIT_CARD);
      getPaymentMethodDetails.returns({});
    });

    it('should render credit card form', () => {
      const wrapper = mountComponent();
      expect(wrapper).to.be.present();
      expect(wrapper.find('DocumentTitle')).to.be.present();
      expect(wrapper.find(EditCreditCard)).to.be.present();
      expect(wrapper.find('button.button-submit')).to.be.present();
      expect(wrapper.find('button.button-cancel')).to.be.present();
    });

    it('should call closeAction if cancel', () => {
      const wrapper = mountComponent();
      expect(wrapper.find('button.button-cancel')).to.be.present();
      wrapper.find('button.button-cancel').simulate('click');
      expect(closeAction).to.have.been.called();
    });

    describe('and updating credit card', () => {
      describe('when submit', () => {
        it('should call updatePaymentDetails', (done) => {
          const wrapper = mountComponent();
          const mountedEditCreditCard = wrapper.find(EditCreditCard).instance();
          mountedEditCreditCard.getCreditCardData = sinon.stub().resolves({ sessionId: '123' });
          const submit = wrapper.find(SubmitButton);

          expect(submit).to.be.present();
          submit.simulate('click');

          setImmediate(() => {
            expect(updatePaymentMethodPromise).to.have.been.calledWith({
              creditCard: { sessionId: '123' },
              paymentMethod: PAYMENT_METHOD_CREDIT_CARD
            });
            expect(closeAction).to.have.been.called();
            expect(submit).to.have.prop('spinning', false);
            done();
          });
        });

        it('should set state saving off on fail', (done) => {
          const wrapper = mountComponent();
          const mountedEditCreditCard = wrapper.find(EditCreditCard).instance();
          mountedEditCreditCard.getCreditCardData = sinon.stub().rejects();

          const submit = wrapper.find(SubmitButton);
          expect(submit).to.be.present();
          submit.simulate('click');

          setImmediate(() => {
            expect(updatePaymentMethodPromise).to.have.not.been.called();
            expect(closeAction).to.have.not.been.called();
            expect(submit).to.have.prop('spinning', false);
            done();
          });
        });
      });
    });
    describe('and switching to paypal', () => {
      describe('when authorized', () => {
        it('should hide credit card form', (done) => {
          const wrapper = mountComponent();
          const mountedPaypal = wrapper.find('UpdatePaymentMethod').instance();
          mountedPaypal.onAuthorize({ nonce: '123', details: { email: 'user@test.org' } });
          wrapper.update();
          setImmediate(() => {
            expect(wrapper.find(EditCreditCard)).to.not.be.present();
            done();
          });
        });
      });
      describe('when submit', () => {
        it('should call updatePaymentDetails', (done) => {
          const wrapper = mountComponent();
          const mountedPaypal = wrapper.find('UpdatePaymentMethod').instance();
          mountedPaypal.onAuthorize({ nonce: '123', details: { email: 'user@test.org' } });
          const submit = wrapper.find(SubmitButton);

          expect(submit).to.be.present();
          submit.simulate('click');

          setImmediate(() => {
            expect(updatePaymentMethodPromise).to.have.been.calledWith({
              paypalAccount: { nonce: '123', email: 'user@test.org' },
              paymentMethod: PAYMENT_METHOD_PAYPAL
            });
            expect(closeAction).to.have.been.called();
            expect(submit).to.have.prop('spinning', false);
            done();
          });
        });
      });
    });
  });
  describe('When current is paypal', () => {
    beforeEach(() => {
      getPaymentMethod.returns(PAYMENT_METHOD_PAYPAL);
      getPaymentMethodDetails.returns({ email: 'user@test.org' });
    });

    it('should render paypal status comp', () => {
      const wrapper = mountComponent();
      expect(wrapper).to.be.present();
      expect(wrapper.find('DocumentTitle')).to.be.present();
      expect(wrapper.find('PayPalStatusCard')).to.be.present();
      expect(wrapper.find(EditCreditCard)).to.be.present();
      expect(wrapper.find('button.button-submit')).to.be.present();
      expect(wrapper.find('button.button-cancel')).to.be.present();
    });

    describe('when submit', () => {
      it('should call updatePaymentDetails', (done) => {
        const wrapper = mountComponent();
        const mountedEditCreditCard = wrapper.find(EditCreditCard).instance();
        mountedEditCreditCard.getCreditCardData = sinon.stub().resolves({ sessionId: '123' });

        const submit = wrapper.find(SubmitButton);
        expect(submit).to.be.present();
        submit.simulate('click');

        setImmediate(() => {
          expect(updatePaymentMethodPromise).to.have.been.calledWith({
            creditCard: { sessionId: '123' },
            paymentMethod: PAYMENT_METHOD_CREDIT_CARD
          });
          expect(closeAction).to.have.been.called();
          expect(submit).to.have.prop('spinning', false);
          done();
        });
      });
    });
  });
});
