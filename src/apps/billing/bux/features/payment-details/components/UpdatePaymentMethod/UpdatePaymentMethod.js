import React, { PureComponent } from 'react';
import { Trans, withI18n } from '@lingui/react';
import { func, string, object } from 'prop-types';
import { connect } from 'react-redux';
import { bindPromiseCreators } from 'redux-saga-routines';
import cx from 'classnames';
import { Analytics } from 'bux/components/Analytics';
import withPageLevelLog from 'bux/components/log/with/pageLevel';
import DocumentTitle from 'bux/components/DocumentTitle';
import Button, { SubmitButton } from 'bux/components/Button';
import EditCreditCard from 'bux/components/ak/creditcard/EditCreditCard';
import { getPaymentMethod, getPaymentMethodDetails } from 'bux/core/state/billing-details/selectors';
import { updatePaymentMethodPromise } from 'bux/core/state/billing-details/actions';
import { PAYMENT_METHOD_CREDIT_CARD, PAYMENT_METHOD_PAYPAL } from 'bux/core/state/billing-details/constants';
import LogScope from 'bux/components/log/Scope';
import PayPalButton from 'bux/components/ak/paypal';
import LegalAgreement from '../LegalAgreement';
import PayPalStatusCard from '../PayPalStatusCard';
import styles from './styles.less';

class UpdatePaymentMethod extends PureComponent {
  static propTypes = {
    closeAction: func,
    updatePaymentMethod: func,
    paymentMethod: string,
    paymentDetails: object,
    i18n: object,
  };

  state = {
    authorizedInPayPal: false,
    paymentDetails: this.props.paymentDetails,
    submitting: false,
    ready: false
  };

  onReady = () => {
    this.setState({ ready: true });
  };

  onAuthorize = ({ nonce, details: { email } }) => {
    this.setState({ ready: true, authorizedInPayPal: true, paymentDetails: { nonce, email } });
  };

  onSubmit = () => {
    this.setSubmittingStateOn()
      .then(this.getPaymentDetails)
      .then(this.props.updatePaymentMethod)
      .then(this.setSubmittingStateOff)
      .then(this.props.closeAction)
      .catch(this.setSubmittingStateOff);
  };

  setSubmittingStateOn = () => new Promise(resolve => this.setState({ submitting: true }, resolve));

  setSubmittingStateOff = () => this.setState({ submitting: false });

  setCreditCardRef = creditCard => (this.creditCard = creditCard);

  getPaymentDetails = () => {
    const { paymentDetails, authorizedInPayPal } = this.state;
    if (!authorizedInPayPal) {
      return new Promise((resolve, reject) => {
        this.creditCard.getCreditCardData()
          .then((creditCard) => {
            resolve({ creditCard, paymentMethod: PAYMENT_METHOD_CREDIT_CARD });
          })
          .catch(reject);
      });
    }
    return { paypalAccount: paymentDetails, paymentMethod: PAYMENT_METHOD_PAYPAL };
  };

  render() {
    const { closeAction, paymentMethod, i18n } = this.props;
    const {
      submitting, ready, authorizedInPayPal, paymentDetails: { email }
    } = this.state;
    const creditCardForm = (
      <LogScope prefix="creditCard">
        <EditCreditCard
          ref={this.setCreditCardRef}
          onReady={this.onReady}
          className={styles.paymentMethod}
          i18n={i18n}
        />
      </LogScope>
    );
    return (
      <DocumentTitle title={i18n.t('billing.update-payment-details.title')`Update payment method`}>
        <div className={cx(styles.page, 'updatePaymentMethodFocusedTask')}>
          <h2><Trans id="billing.update-payment-details.header.title">Update payment method</Trans></h2>

          {
            authorizedInPayPal &&
            <PayPalStatusCard email={email} showCheck={authorizedInPayPal} />
          }

          {
            !authorizedInPayPal && paymentMethod === PAYMENT_METHOD_CREDIT_CARD &&
            <div>
              {creditCardForm}
                <LogScope prefix="paypal">
                  <div className={styles.section}>
                    <h3 className={styles.heading}>
                      <Trans id="billing.update-payment-details.header.paypal.title">PayPal</Trans>
                    </h3>
                    <p>
                      <Trans id="billing.update-payment-details.header.paypal.text">
                        If you wish to switch your payment method to PayPal, please click the Pay with PayPal button.
                        We will remove your credit/debit card as your payment when you switch to PayPal.
                      </Trans>
                    </p>
                    <div className={styles.paypal}>
                      <PayPalButton onAuthorize={this.onAuthorize} i18n={i18n} />
                    </div>
                  </div>
                </LogScope>
            </div>
          }

          { !authorizedInPayPal && paymentMethod === PAYMENT_METHOD_PAYPAL &&
            <div>
              <PayPalStatusCard email={email} />
              <div className={styles.section}>
                <h3 className={styles.heading}>
                  <Trans id="billing.update-payment-details.header.creditcard.title">
                    Credit card payment
                  </Trans>
                </h3>
                <p>
                  <Trans id="billing.update-payment-details.header.creditcard.text">
                    If you wish to switch your payment method to credit/debit card, please enter your card information
                    below. We will remove PayPal as your payment when you switch to credit/debit card.
                  </Trans>
                </p>
                <div className={styles.creditCard}>
                  {creditCardForm}
                </div>
              </div>
            </div>
          }
          <LegalAgreement showTerms={false} />
          <div className={styles.actionButtons}>
            <LogScope prefix="creditCard">
              <Analytics.UI as="saveButton">
                <SubmitButton
                  onClick={this.onSubmit}
                  className="button-submit"
                  spinning={submitting}
                  disabled={submitting || !ready}
                  name="save"
                  submit
                  primary
                >
                  <Trans id="billing.update-payment-details.button.save">
                    Save
                  </Trans>
                </SubmitButton>
              </Analytics.UI>
            </LogScope>
            <Analytics.UI as="cancelButton">
              <Button name="cancel" className="button-cancel" onClick={closeAction}>
                <Trans id="billing.update-payment-details.button.cancel">
                  Cancel
                </Trans>
              </Button>
            </Analytics.UI>
          </div>
        </div>
      </DocumentTitle>
    );
  }
}

const mapStateToProps = state => ({
  paymentMethod: getPaymentMethod(state),
  paymentDetails: getPaymentMethodDetails(state)
});

export const mapDispatchToProps = dispatch => ({
  ...bindPromiseCreators({
    updatePaymentMethod: updatePaymentMethodPromise
  }, dispatch)
});

const UpdatePaymentMethodI18n = withI18n()(UpdatePaymentMethod);

const ConnectedUpdatePaymentMethod = connect(
  mapStateToProps,
  mapDispatchToProps
)(UpdatePaymentMethodI18n);

export default withPageLevelLog(ConnectedUpdatePaymentMethod);
