import UpdatePaymentMethod from './UpdatePaymentMethod';
import UpdatePaymentMethodPage from './UpdatePaymentMethodPage';

export {
  UpdatePaymentMethod,
  UpdatePaymentMethodPage
};

