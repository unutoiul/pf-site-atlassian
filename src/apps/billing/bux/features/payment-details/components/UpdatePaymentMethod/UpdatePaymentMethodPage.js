import React from 'react';
import { connect } from 'react-redux';
import { Analytics } from 'bux/components/Analytics';
import { openFocusedTask, closeFocusedTask } from 'bux/core/state/focused-task/actions';
import { getFocusedTask } from 'bux/core/state/focused-task/selectors';
import { push as redirect } from 'bux/core/state/router/actions';
import { hasPaymentMethod } from 'bux/core/state/billing-details/selectors';
import { isNextBillingPeriodRenewalFrequencyAnnual } from 'bux/core/state/bill-estimate/selectors';
import FocusedTaskPage from 'bux/components/FocusedTaskPage';
import library from '../../../FocusedTask/library';
import UpdatePaymentMethod from './UpdatePaymentMethod';

const mapStateToProps = state => ({
  isFocusedTaskOpen: !!getFocusedTask(state),
  isAccessible: hasPaymentMethod(state) && !isNextBillingPeriodRenewalFrequencyAnnual(state)
});

const mapDispatchToProps = dispatch => ({
  open: (props) => {
    const component = library.add(UpdatePaymentMethod);
    dispatch(openFocusedTask({
      component,
      props,
      logPrefix: 'paymentDetails.UpdatePaymentMethod'
    }));
  },
  exit: () => {
    dispatch(closeFocusedTask());
    dispatch(redirect('/paymentdetails'));
  }
});

const withScreen = WrappedComponent => props => (
  <Analytics.Screen name="paymentMethodUpdateScreen">
    <WrappedComponent {...props} />
  </Analytics.Screen>
);

export default withScreen(connect(
  mapStateToProps,
  mapDispatchToProps
)(FocusedTaskPage));
