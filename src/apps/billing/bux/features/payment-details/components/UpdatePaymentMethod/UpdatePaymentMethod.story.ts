import { storiesOf } from '@storybook/react';

import { inScenarios, inStepFlowChrome, render, withServicePreLoader } from 'bux/common/helpers/storybook';

import UpdatePaymentMethod from './UpdatePaymentMethod';

const PreparedUpdatePaymentMethod = inStepFlowChrome(withServicePreLoader(render(UpdatePaymentMethod)));

storiesOf('BUX|Focused tasks/Update payment method', module)
  .add('Default', () => inScenarios([], PreparedUpdatePaymentMethod))
  .add('PayPal', () => inScenarios(['billing-details-paypal'], PreparedUpdatePaymentMethod))
  .add('Error', () => inScenarios([], PreparedUpdatePaymentMethod));
