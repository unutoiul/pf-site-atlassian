import { connect } from 'react-redux';
import InvoiceContactWidget from 'bux/components/InvoiceContactWidget';
import billingDetails from 'bux/core/state/billing-details';

const mapStateToProps = state => ({
  metadata: billingDetails.selectors.getBillingDetailsMetadata(state),
  invoiceEmail: billingDetails.selectors.getInvoiceContactEmail(state),
  billingContact: billingDetails.selectors.getBillingContact(state)
});

export default connect(mapStateToProps)(InvoiceContactWidget);
