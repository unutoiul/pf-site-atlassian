import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import PayPalStatusCard from '../PayPalStatusCard';

describe('PayPalStatusCard', () => {
  it('should display email and check icon', () => {
    const wrapper = shallow(<PayPalStatusCard email="user@test.org" showCheck />);
    expect(wrapper.find('[data-test="paypal-status-email"]')).to.have.text('user@test.org');
    expect(wrapper.find('CheckCircleIcon')).to.be.present();
  });
  it('should not display check icon', () => {
    const wrapper = shallow(<PayPalStatusCard email="user@test.org" />);
    expect(wrapper.find('CheckCircleIcon')).to.not.be.present();
  });
});
