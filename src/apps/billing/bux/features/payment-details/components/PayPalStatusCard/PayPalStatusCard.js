import { Trans } from '@lingui/react';
import React from 'react';
import { string, bool } from 'prop-types';
import CheckCircleIcon from '@atlaskit/icon/glyph/check-circle';
import Card from 'bux/components/blocks/Card';
import PaymentTypePaypalLogo from 'bux/static/payment-paypal.svgx';
import resizeSVG from 'bux/common/helpers/resizeSVG';
import styles from './PayPalStatusCard.less';

const paypalLogoHeight = 24;
const PaypalResized = resizeSVG(PaymentTypePaypalLogo, paypalLogoHeight);

const PayPalStatusCard = ({ email, showCheck = false, className }) => (
  <div className={className}>
    <Card className={styles.card}>
      <div className={styles.logo}><PaypalResized /></div>
      <p className={styles.heading}>
        <Trans id="billing.payment-details.paypal.account">
          PayPal account
        </Trans>
        {' '}
        { showCheck && <CheckCircleIcon size="small" />}
      </p>
      <p className={styles.email} data-test="paypal-status-email">{email}</p>
    </Card>
  </div>
);

PayPalStatusCard.propTypes = {
  email: string.isRequired,
  showCheck: bool,
  className: string
};

export default PayPalStatusCard;
