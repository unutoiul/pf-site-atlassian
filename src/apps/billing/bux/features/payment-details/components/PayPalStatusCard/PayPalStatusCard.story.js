import { storiesOf } from '@storybook/react';
import { inContentChrome, inStoryKnob } from 'bux/common/helpers/storybook';
import PayPalStatusCard from './PayPalStatusCard';

storiesOf('BUX|Features/PaymentDetails/PayPalStatusCard', module)
  .add('PayPalStatusCard', () => inContentChrome(inStoryKnob(PayPalStatusCard, {
    email: 'lrezendelemos@atlassian.com',
    showCheck: true
  })));
