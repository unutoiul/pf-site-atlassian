import { Trans, withI18n } from '@lingui/react';
import React from 'react';
import { func, string, bool, object } from 'prop-types';
import { connect } from 'react-redux';
import { SubmissionError, formValueSelector, Form } from 'redux-form';
import { withAnalytics, Analytics } from 'bux/components/Analytics';
import { enchantedForm } from 'bux/components/redux-form';
import { showNotificationByTemplate, closeNotification } from 'bux/core/state/notifications/actions';
import { createValidator } from 'bux/common/helpers/validation';
import { Footer, StepButtons } from 'bux/components/Wizard';
import {
  getBillingDetails,
  getInvoiceContactEmail
} from 'bux/core/state/billing-details/selectors';
import { getAddressOrUserLocation, constructFullAddress, getFullAddress } from 'bux/common/helpers/address';
import { validateBillingDetails } from 'bux/core/state/api';
import { bindActionCreators } from 'redux';
import BillingAddress from '../BillingAddress';

class StepBillingAddress extends React.PureComponent {
  static propTypes = {
    handleSubmit: func.isRequired,
    next: func.isRequired,
    back: func.isRequired,
    autofill: func.isRequired,
    touch: func.isRequired,
    formValidator: func.isRequired,
    submitting: bool.isRequired,
    country: string,
    state: string,
    initialValues: object.isRequired,
    push: func.isRequired,
    isFirstStep: bool.isRequired,
    stepBillingAddressData: object,
    i18n: object,
    validateBillingDetail: func,
    analytics: object,
  };

  state = {
    disableSave: false
  }

  onSubmit = (form) => {
    this.validateFormValues(form)
      .then(this.setChangedValues)
      .then(this.props.next)
      .catch((e) => {
        throw new SubmissionError(e);
      });
  };

  setChangedValues = (data) => {
    const addressLineFull = getFullAddress(data);
    return this.props.push({ stepBillingAddressData: { ...data, addressLineFull } });
  };

  validateFormValues = (form) => {
    const errors = this.props.formValidator(form, { i18n: this.props.i18n });
    return Object.keys(errors).length === 0
      ? Promise.resolve(form)
      : Promise.reject(errors);
  };

  shouldDisableNextButton(isSaveBlocked) {
    this.setState({ disableSave: isSaveBlocked && !isSaveBlocked.props.isWarning });
  } 

  render() {
    const {
      handleSubmit,
      back,
      isFirstStep,
      autofill,
      touch,
      country,
      state,
      submitting,
      initialValues,
      stepBillingAddressData,
      validateBillingDetail,
    } = this.props;
    // Change original address to the one temp saved on the wizard state
    if (stepBillingAddressData) {
      initialValues.originalAddressLineFull = stepBillingAddressData.addressLineFull;
    }
    return (    
      <div data-test="stepBillingAddress">
        <h2><Trans id="billing.step-billing-address.title">Add your billing address</Trans></h2>
        <Form onSubmit={handleSubmit(this.onSubmit)}>
          <BillingAddress
            autofill={autofill}
            touch={touch}
            country={country}
            state={state}
            initialValues={initialValues}
            validateCountry={validateBillingDetail}
            shouldDisableSave={isSaveBlocked => this.shouldDisableNextButton(isSaveBlocked)}
          />
          <Footer>
            <StepButtons
              isFirstStep={isFirstStep}
              submitDisabled={submitting || this.state.disableSave}
              submitBusy={submitting}
              submitLabel={<Trans id="billing.step-billing-address.button.next">Next</Trans>}
              submitName="next"
              onBack={back}
              backDisabled={submitting}
            />
          </Footer>
        </Form>
      </div>
    );
  }
}

const formValidator = createValidator([
  BillingAddress.validationRules
]);

const FORM_NAME = 'StepBillingAddress';
const selector = formValueSelector(FORM_NAME);

const withScreen = WrappedComponent => props => (
  <Analytics.Screen name="billingAddressAddScreen">
    <WrappedComponent {...props} />
  </Analytics.Screen>
);

export const DecoratedStepBillingAddress = withScreen(withAnalytics(withI18n()(enchantedForm({
  form: FORM_NAME,
  validate: formValidator,
  onSubmitSuccess: (result, dispatch) => {
    closeNotification('payment-error')(dispatch);
  },
  onSubmitFail: (errors, dispatch, submitError, props) => {
    if (typeof errors === 'object') {
      props.analytics.sendTrackEvent({
        action: 'failed',
        actionSubject: 'billingAddress',
        actionSubjectId: 'missingBillingAddressError',
        attributes: {
          linkContainer: 'module',
        }
      });
      dispatch(showNotificationByTemplate('billing-address-error'));
    }
  }
})(StepBillingAddress))));

export const mapDispatchToProps = dispatch => ({
  ...bindActionCreators({
    validateBillingDetail: validateBillingDetails
  }, dispatch)
});

const mapStateToProps = (state, { stepBillingAddressData }) => ({
  formValidator,
  country: selector(state, 'country'),
  state: selector(state, 'state'),
  initialValues: {
    organisationName: getBillingDetails(state).organisationName,
    taxpayerId: getBillingDetails(state).taxpayerId,
    invoiceContactEmail: getInvoiceContactEmail(state),
    ...getAddressOrUserLocation(state)(getBillingDetails),
    addressLineFull: constructFullAddress(state)(getBillingDetails),
    originalAddressLineFull: constructFullAddress(state)(getBillingDetails),
    ...stepBillingAddressData,
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(DecoratedStepBillingAddress);
