import { Trans } from '@lingui/react';
import * as React from 'react';
import { connect } from 'react-redux';
import { bindPromiseCreators } from 'redux-saga-routines';

import { Analytics } from 'bux/components/Analytics';
import Date from 'bux/components/Date';
import { FootnoteIndicator } from 'bux/components/Footnote';
import Money from 'bux/components/Money';
import { Billing } from 'bux/components/PaymentWizard';
import { getBill, getTotalCost } from 'bux/core/state/bill-estimate/selectors';
import { updateBillingDetailsPromise } from 'bux/core/state/billing-details/actions';
import { PAYMENT_METHOD_CREDIT_CARD, PAYMENT_METHOD_PAYPAL } from 'bux/core/state/billing-details/constants';
import { getTenantInfo } from 'bux/core/state/meta/selectors';
import { BillEstimateDetail } from 'bux/features/bill-estimate/components';

interface StepOverviewProps {
  stepBillingAddressData: any;
  stepBillingMethodData: any;
  isFirstStep: boolean;
  billEstimate: any;
  tenantInfo: any;
  totalCost: number;
  back(): void;
  savePaymentDetails(data: any): Promise<any>;
}

export class StepOverviewImpl extends React.PureComponent<StepOverviewProps> {
  public state = {
    isSubmitting: false,
    isAgreed: false,
  };

  public onAgreedChanged = (agreed: boolean) => {
    this.setState({ isAgreed: agreed });
  };

  public getBillingDetails = () => {
    const { stepBillingMethodData: { method, paymentDetails }, stepBillingAddressData } = this.props;

    return {
      ...stepBillingAddressData,
      paymentMethod: method,
      creditCard: method === PAYMENT_METHOD_CREDIT_CARD ? paymentDetails : undefined,
      paypalAccount: method === PAYMENT_METHOD_PAYPAL ? paymentDetails : undefined,
      invoiceContactEmail: undefined,
    };
  };

  public handleSubmit = () => {
    const data = this.getBillingDetails();
    this.setState({ isSubmitting: true });
    this.props.savePaymentDetails(data)
      .catch(() => this.setState({ isSubmitting: false }));
  };

  public render() {
    const { isSubmitting, isAgreed } = this.state;
    const {
      back, stepBillingAddressData, stepBillingMethodData, billEstimate, totalCost, isFirstStep, tenantInfo,
    } = this.props;

    const { currencyCode, currentBillingPeriod, nextBillingPeriod } = billEstimate;
    const { organisationName, country, addressLineFull } = stepBillingAddressData;
    const { method, paymentDetails } = stepBillingMethodData;
    const cost = <FootnoteIndicator key="total-cost"><Money amount={totalCost} currency={currencyCode} /></FootnoteIndicator>;
    const date = <Date key="end-date" value={currentBillingPeriod.endDate} />;

    return (
      <Analytics.Screen name="billingConfirmationScreen">
        <Billing.Overview
          title={<Trans id="billing.step-billing-overview.title">Confirm your billing details</Trans>}
          logPrefix="StepBillingOverview"
          dataTest="stepOverview"
        >
          <Billing.Address
            organisationName={organisationName}
            addressLineFull={addressLineFull}
            country={country}
          />
          <Billing.PaymentMethod method={method} details={paymentDetails} />
          <Billing.Period
            title={<Trans id="billing.step-billing-overview.period.title">Billing period</Trans>}
            startDate={nextBillingPeriod.startDate}
            endDate={nextBillingPeriod.endDate}
          />
          <Billing.Estimate
            title={<Trans id="billing.step-billing-overview.summary.title">Monthly billing summary</Trans>}
            isOrg={tenantInfo.isOrg}
            name={tenantInfo.name}
          >
            <BillEstimateDetail style="compact" excludeTax />
          </Billing.Estimate>
          <Billing.Text className="test-charge-advise">
            <Trans id="billing.step-billing-overview.advise">
              Upon subscribing, you’ll be charged {cost} per month starting on {date}.
            </Trans>
          </Billing.Text>
          <Billing.Agreement
            isBusy={isSubmitting}
            onAgreedChanged={this.onAgreedChanged}
          />
          <Billing.Controls
            submitLabel={<Trans id="billing.step-billing-overview.controls.submit">Subscribe</Trans>}
            submitName="subscribe"
            onSubmit={this.handleSubmit}
            onCancel={back}
            isFirstStep={isFirstStep}
            isBusy={isSubmitting}
            isSubmitDisabled={!isAgreed}
          />
        </Billing.Overview>
      </Analytics.Screen>
    );
  }
}

const mapStateToProps = state => ({
  totalCost: getTotalCost(state, true),
  billEstimate: getBill(state),
  tenantInfo: getTenantInfo(state),
});

const mapDispatchToProps = dispatch => ({
  ...bindPromiseCreators({
    savePaymentDetails: updateBillingDetailsPromise,
  }, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(StepOverviewImpl);
