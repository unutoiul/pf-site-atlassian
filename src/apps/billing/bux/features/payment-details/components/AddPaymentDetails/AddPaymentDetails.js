import React from 'react';
import { func } from 'prop-types';
import { Trans } from '@lingui/react';
import { Flow, Step } from 'bux/components/flow';
import AutoFocus from 'bux/components/behaviour/focus/AutoFocus';
import ScopedState, { connectScopedState } from 'bux/components/ScopedState';
import withPageLevelLog from 'bux/components/log/with/pageLevel';

import StepBillingAddress from './StepBillingAddress';
import StepBillingMethod from './StepBillingMethod';
import StepOverview from './StepOverview';

const ScopedStateStepBillingAddress = connectScopedState(StepBillingAddress);
const ScopedStateStepBillingMethod = connectScopedState(StepBillingMethod);
const ScopedStateStepOverview = connectScopedState(StepOverview);

const AddPaymentDetails = ({ closeAction }) => (
  <AutoFocus>
    <div data-test="add-payment-details-flow">
      <ScopedState>
        <Flow
          width="narrow"
          onCancel={closeAction}
          onComplete={closeAction}
        >
          <Step
            title={<Trans id="billing.add-payment.flow.address.title">Billing address</Trans>}
            Component={ScopedStateStepBillingAddress}
          />
          <Step
            title={<Trans id="billing.add-payment.flow.payment.title">Payment method</Trans>}
            Component={ScopedStateStepBillingMethod}
          />
          <Step
            title={<Trans id="billing.add-payment.flow.confirm.title">Billing confirmation</Trans>}
            Component={ScopedStateStepOverview}
          />
        </Flow>
      </ScopedState>
    </div>
  </AutoFocus>
);

AddPaymentDetails.propTypes = {
  closeAction: func.isRequired
};

export default withPageLevelLog(AddPaymentDetails);
