import { connect } from 'react-redux';

import { openFocusedTask, closeFocusedTask } from 'bux/core/state/focused-task/actions';
import { getFocusedTask } from 'bux/core/state/focused-task/selectors';
import { push as redirect } from 'bux/core/state/router/actions';
import { hasPaymentMethod } from 'bux/core/state/billing-details/selectors';
import { isNextBillingPeriodRenewalFrequencyAnnual } from 'bux/core/state/bill-estimate/selectors';
import FocusedTaskPage from 'bux/components/FocusedTaskPage';
import library from '../../../FocusedTask/library';
import AddPaymentDetails from './AddPaymentDetails';

const mapStateToProps = state => ({
  isFocusedTaskOpen: !!getFocusedTask(state),
  isAccessible: !hasPaymentMethod(state) && !isNextBillingPeriodRenewalFrequencyAnnual(state),
});

const mapDispatchToProps = dispatch => ({
  open: (props) => {
    const component = library.add(AddPaymentDetails);
    dispatch(openFocusedTask({
      component,
      props,
      logPrefix: 'paymentDetails.AddPaymentDetails'
    }));
  },
  exit: () => {
    dispatch(closeFocusedTask());
    dispatch(redirect('/paymentdetails'));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FocusedTaskPage);
