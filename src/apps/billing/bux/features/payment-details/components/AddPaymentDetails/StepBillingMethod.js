import React from 'react';
import { withI18n, Trans } from '@lingui/react';
import { func, bool, object } from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { showNotificationByTemplate } from 'bux/core/state/notifications/actions';
import EditCreditCard from 'bux/components/ak/creditcard/EditCreditCard';
import { withAnalytics, Analytics } from 'bux/components/Analytics';
import Button, { SubmitButton } from 'bux/components/Button';
import { PAYMENT_METHOD_CREDIT_CARD, PAYMENT_METHOD_PAYPAL } from 'bux/core/state/billing-details/constants';
import LogScope from 'bux/components/log/Scope';
import HorizontalOrSeparator from 'bux/components/HorizontalOrSeparator';
import PayPalButton from 'bux/components/ak/paypal';
import { Footer } from 'bux/components/Wizard';
import PayPalStatusCard from '../PayPalStatusCard';
import LegalAgreement from '../LegalAgreement';
import styles from './AddPaymentDetails.less';

class StepBillingMethod extends React.PureComponent {
  static propTypes = {
    next: func.isRequired,
    back: func.isRequired,
    onSubmitFail: func.isRequired,
    push: func.isRequired,
    authorizedInPayPal: bool,
    paypalAccount: object,
    i18n: object,
    analytics: object,
  };

  state = {
    submitting: false,
    ready: false,
    authorizedInPayPal: this.props.authorizedInPayPal || false,
    paypalAccount: this.props.paypalAccount || {}
  };

  onReady = () => {
    this.setState({ ready: true });
  };

  onAuthorize = ({ nonce, details: { email } }) => {
    this.setState({ ready: true, authorizedInPayPal: true, paypalAccount: { nonce, email } });
  };

  onSubmit = () => {
    const { next, onSubmitFail } = this.props;
    this.setSubmittingStateOn()
      .then(this.getPaymentDetails)
      .then(this.setChangedValues)
      .then(next)
      .catch(() => {
        this.setSubmittingStateOff();
        this.props.analytics.sendTrackEvent({
          actionSubject: 'missingPaymentMethod',
          acitonSubjectId: 'missingPaymentMethodError',
          attributes: {
            linkContainer: 'module',
          }
        });
        onSubmitFail();
      });
  };

  getPaymentDetails = () => (
    this.state.authorizedInPayPal
      ? this.state.paypalAccount
      : this.creditCard.getCreditCardData()
  );

  setChangedValues = paymentDetails => this.props.push({
    stepBillingMethodData: {
      method: this.state.authorizedInPayPal ? PAYMENT_METHOD_PAYPAL : PAYMENT_METHOD_CREDIT_CARD,
      paymentDetails,
    }
  });

  setSubmittingStateOn = () => new Promise(resolve => this.setState({ submitting: true }, resolve));

  setSubmittingStateOff = () => this.setState({ submitting: false });

  setCreditCardRef = creditCard => (this.creditCard = creditCard);

  render() {
    const {
      submitting, ready, authorizedInPayPal, paypalAccount: { email }
    } = this.state;
    const { back, i18n } = this.props;
    return (
      <LogScope prefix="StepBillingMethod">
        <div data-test="stepBillingMethod">
          <h2><Trans id="billing.step-billing-method.title">Add your payment method</Trans></h2>
          {
            authorizedInPayPal ?
              <div>
                <p>
                  <Trans id="billing.step-billing-method.header.paypal">
                    PayPal is your selected payment method.
                  </Trans>
                </p>
                <PayPalStatusCard email={email} showCheck className={styles.paypal} />
              </div> :
              <div>
                <p>
                  <Trans id="billing.step-billing-method.header.credit-card">
                    Please provide your credit card information or connect your PayPal account.
                  </Trans>
                </p>
                <EditCreditCard
                  ref={this.setCreditCardRef}
                  onReady={this.onReady}
                  className={styles.creditCard}
                  i18n={i18n}
                />
                <div>
                  <HorizontalOrSeparator />
                  <LogScope prefix="paypal">
                    <PayPalButton onAuthorize={this.onAuthorize} className={styles.paypal} i18n={i18n} />
                  </LogScope>
                </div>
              </div>
          }
          <p className={styles.adviseCharge}>
            <Trans id="billing.step-billing-method.charge-information">
              You won't be charged immediately. You will be charged for your monthly subscription 
              on the first day of each billing period using the payment method you've specified 
              above. Atlassian will keep your payment info securely on file.
            </Trans>
          </p>
          <LegalAgreement showTerms={false} />
          <Footer>
            <Analytics.UI as="backButton">
              <Button name="back" className="back-flow" onClick={back} disabled={submitting} subtle>
                <Trans id="billing.step-billing-method.button.back">Back</Trans>
              </Button>
            </Analytics.UI>
            <Analytics.UI as="nextButton">
              <SubmitButton
                name="next"
                onClick={this.onSubmit}
                spinning={submitting}
                disabled={submitting || !ready}
              >
                <Trans id="billing.step-billing-method.button.next">Next</Trans>
              </SubmitButton>
            </Analytics.UI>
          </Footer>
        </div>
      </LogScope>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  onSubmitFail: () => (
    dispatch(showNotificationByTemplate('billing-method-error'))
  ),
});

const withScreen = WrappedComponent => props => (
  <Analytics.Screen name="paymentMethodAddScreen">
    <WrappedComponent {...props} />    
  </Analytics.Screen>
);

export default compose(
  withScreen,
  connect(null, mapDispatchToProps),
  withAnalytics,
  withI18n(),
)(StepBillingMethod);