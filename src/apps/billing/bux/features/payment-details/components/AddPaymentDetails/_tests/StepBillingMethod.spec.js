import React from 'react';
import sinon from 'sinon';
import { expect } from 'chai';
import { mount } from 'enzyme';
import { Provider } from 'bux/helpers/redux';
import proxyquire from 'bux/helpers/proxyquire';
import EditCreditCard from 'bux/helpers/tests/components/MockEditCreditCard';
import PayPalButton from 'bux/helpers/tests/components/MockPayPalButton';
import getAnalytic from 'bux/components/log/helpers/getAnalytic';

describe('StepBillingMethod: ', () => {
  let push;
  let next;
  let back;
  let showNotificationByTemplate;

  const Component = (props) => {
    const StepBillingMethod = proxyquire.noCallThru().load('../StepBillingMethod', {
      'bux/components/ak/creditcard/EditCreditCard': EditCreditCard,
      'bux/components/ak/paypal': PayPalButton,
      'bux/core/state/notifications/actions': { showNotificationByTemplate }
    }).default;

    const defaultProps = {
      next, back, push, ...props
    };
    return (
      <Provider>
        <StepBillingMethod {...defaultProps} />
      </Provider>
    );
  };

  beforeEach(() => {
    push = sinon.stub().returns({});
    next = sinon.stub();
    back = sinon.stub();
    showNotificationByTemplate = sinon.stub().returns({ type: 'test' });
  });

  it('should call back when click back button', () => {
    const wrapper = mount(<Component />, getAnalytic().context);
    expect(wrapper.find('button.back-flow')).to.be.present();
    wrapper.find('button.back-flow').simulate('click');
    expect(back).to.be.called();
  });
  describe('When creditCard', () => {
    it('should not proceed when click next button with invalid form', (done) => {
      const wrapper = mount(<Component />, getAnalytic().context);
      const mountedEditCreditCard = wrapper.find(EditCreditCard).instance();
      mountedEditCreditCard.getCreditCardData = sinon.stub().rejects();

      expect(wrapper.find('SubmitButton')).to.be.present();
      wrapper.find('SubmitButton').simulate('click');
      setImmediate(() => {
        expect(showNotificationByTemplate).to.be.called();
        expect(next).to.not.be.called();
        expect(push).to.not.be.called();
        done();
      });
    });
    it('should proceed when submit with valid form', (done) => {
      const wrapper = mount(<Component />, getAnalytic().context);
      const mountedEditCreditCard = wrapper.find(EditCreditCard).instance();
      mountedEditCreditCard.getCreditCardData = sinon.stub().resolves({ creditCard: 'data' });

      expect(wrapper.find('SubmitButton')).to.be.present();
      wrapper.find('SubmitButton').simulate('click');
      setImmediate(() => {
        expect(showNotificationByTemplate).to.not.be.called();
        expect(next).to.be.called();
        expect(push.args[0][0]).to.be.deep.equal({
          stepBillingMethodData: {
            method: 'TOKENIZED_CREDIT_CARD',
            paymentDetails: {
              creditCard: 'data'
            }
          }
        });
        done();
      });
    });
  });
  describe('When PayPal', () => {
    it('should proceed when submit after authorizing', (done) => {
      const wrapper = mount(<Component />, getAnalytic().context);
      wrapper.find('StepBillingMethod').instance().onAuthorize({ nonce: '123', details: { email: 'user@test.org' } });
      wrapper.find('SubmitButton').simulate('click');
      setImmediate(() => {
        expect(showNotificationByTemplate).to.not.be.called();
        expect(next).to.be.called();
        expect(push.args[0][0]).to.be.deep.equal({
          stepBillingMethodData: {
            method: 'PAYPAL_ACCOUNT',
            paymentDetails: {
              email: 'user@test.org',
              nonce: '123'
            }
          }
        });
        done();
      });
    });
  });
});
