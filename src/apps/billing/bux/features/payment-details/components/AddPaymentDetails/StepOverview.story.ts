import { storiesOf } from '@storybook/react';

import { inStepFlowChrome, render, withServicePreLoader } from 'bux/common/helpers/storybook';

import StepOverview from './StepOverview';

const props = {
  stepBillingAddressData: {
    organisationName: 'Atlassian',
    country: 'AU',
    addressLineFull: '341 George Street, NSW',
  },
  stepBillingMethodData: {
    method: 'TOKENIZED_CREDIT_CARD',
    paymentDetails: {
      suffix: '1111',
      expiryMonth: 1,
      expiryYear: 2022,
      type: 'VISA',
      name: 'Superplasma Team',
    },
  },
  isFirstStep: false,
};

const paypal = {
  stepBillingMethodData: {
    method: 'PAYPAL_ACCOUNT',
    paymentDetails: {
      email: 'customer@example.org',
    },
  },
};

storiesOf('BUX|Focused tasks/Add payment details/Step 3: Overview', module)
  .add('Default', () => inStepFlowChrome(withServicePreLoader(render(StepOverview, props))))
  .add('PayPal', () => inStepFlowChrome(withServicePreLoader(render(StepOverview, { ...props, ...paypal }))));
