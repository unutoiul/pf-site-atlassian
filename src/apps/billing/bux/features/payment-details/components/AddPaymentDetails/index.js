import AddPaymentDetails from './AddPaymentDetails';
import AddPaymentDetailsPage from './AddPaymentDetailsPage';

export {
  AddPaymentDetails,
  AddPaymentDetailsPage
};
