import React from 'react';
import sinon from 'sinon';
import { expect } from 'chai';
import { mount } from 'enzyme';
import { Provider } from 'bux/helpers/redux';
import getAnalytic from 'bux/components/log/helpers/getAnalytic';
import AddPaymentDetails from '../AddPaymentDetails';

describe('AddPaymentDetails: ', () => {
  let closeAction;

  const Component = () => {
    closeAction = sinon.stub();
    return (
      <Provider>
        <AddPaymentDetails closeAction={closeAction} />
      </Provider>
    );
  };

  it('should render component', () => {
    const analytic = getAnalytic();
    const wrapper = mount(<Component />, analytic.context);
    expect(wrapper).to.be.present();
    const flow = wrapper.find('Flow');
    expect(flow).to.be.present();
    expect(flow).to.have.prop('onCancel', closeAction);
    expect(flow).to.have.prop('onComplete', closeAction);

    expect(wrapper.find('Step')).to.have.length(3);
  });
});
