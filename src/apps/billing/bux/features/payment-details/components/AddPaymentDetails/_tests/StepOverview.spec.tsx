import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import { Billing } from 'bux/components/PaymentWizard';
import { Provider } from 'bux/helpers/redux';
import { rewiremock } from 'bux/helpers/rewire-mock';

import { StepOverviewImpl } from '../StepOverview';

describe('StepOverview: ', () => {
  let back;
  let getBill;
  let getTenantInfo;
  let getTotalCost;
  let updateBillingDetailsPromise;

  const stepBillingAddressData = {
    organisationName: 'Potato Corp.',
    addressLineFull: 'Level 6, 341 George St Sydney 2000 NSW',
    country: 'AU',
  };
  const stepBillingMethodData = {
    method: 'TOKENIZED_CREDIT_CARD',
    paymentDetails: {
      maskedCardNumber: 'xxxxxxxx1234',
      expiryMonth: '01',
      expiryYear: '2018',
      type: 'visa',
      name: 'Superplasma',
    },
  };

  const Component = (props) => {
    const { default: StepOverview } = rewiremock.proxy('../StepOverview', {
      'bux/core/state/bill-estimate/selectors': {
        getBill,
        getTotalCost,
      },
      'bux/core/state/meta/selectors': { getTenantInfo },
      'bux/core/state/billing-details/actions': { updateBillingDetailsPromise },
    });

    const defaultProps = {
      isFirstStep: false,
      back,
      stepBillingAddressData,
      stepBillingMethodData,
      ...props,
    };

    return (
      <Provider>
        <StepOverview {...defaultProps} />
      </Provider>
    );
  };

  beforeEach(() => {
    back = sinon.stub();
    getBill = sinon.stub().returns({
      nextBillingPeriod: {
        startDate: '',
        endDate: '',
      },
      currentBillingPeriod: {
        startDate: '',
        endDate: '2018-01-01',
      },
      currencyCode: 'usd',
    });
    getTotalCost = sinon.stub().returns(10);
    updateBillingDetailsPromise = sinon.stub().resolves();
    getTenantInfo = sinon.stub().returns({
      isOrg: false,
      name: 'Test Site',
    });
  });

  it('should render component', () => {
    const wrapper = mount(<Component />);
    expect(wrapper).to.be.present();
    expect(wrapper.find(Billing.Overview)).to.have.props({
      logPrefix: 'StepBillingOverview',
      dataTest: 'stepOverview',
    });
    expect(wrapper.find(Billing.Address)).to.have.props({
      organisationName: stepBillingAddressData.organisationName,
      addressLineFull: stepBillingAddressData.addressLineFull,
      country: stepBillingAddressData.country,
    });
    expect(wrapper.find(Billing.PaymentMethod)).to.have.props({
      method: stepBillingMethodData.method,
      details: stepBillingMethodData.paymentDetails,
    });
    expect(wrapper.find(Billing.Period)).to.have.props({
      startDate: getBill().nextBillingPeriod.startDate,
      endDate: getBill().nextBillingPeriod.endDate,
    });
    expect(wrapper.find(Billing.Estimate)).to.have.props({
      isOrg: getTenantInfo().isOrg,
      name: getTenantInfo().name,
    });
    expect(wrapper.find(Billing.Text)).to.contain.text('$10');
    expect(wrapper.find(Billing.Text)).to.contain.text('Jan 1, 2018');
    expect(wrapper.find(Billing.Agreement)).to.have.props({
      isBusy: false,
    });
    expect(wrapper.find(Billing.Controls)).to.have.props({
      submitName: 'subscribe',
      isFirstStep: false,
      isBusy: false,
      isSubmitDisabled: true,
    });
  });

  it('should call updateBillingDetailsPromise when handleSubmit', () => {
    const wrapper = mount(<Component />);
    (wrapper.find('StepOverviewImpl').instance() as StepOverviewImpl).handleSubmit();
    expect(updateBillingDetailsPromise).to.be.calledWith({
      ...stepBillingAddressData,
      creditCard: stepBillingMethodData.paymentDetails,
      paymentMethod: stepBillingMethodData.method,
      invoiceContactEmail: undefined,
      paypalAccount: undefined,
    });
  });

  it('should enable submit upon agreement ', () => {
    const wrapper = mount(<Component />);
    (wrapper.find('StepOverviewImpl').instance() as StepOverviewImpl).onAgreedChanged(true);
    wrapper.update();
    expect(wrapper.find(Billing.Controls)).to.have.props({
      isSubmitDisabled: false,
    });
  });
});
