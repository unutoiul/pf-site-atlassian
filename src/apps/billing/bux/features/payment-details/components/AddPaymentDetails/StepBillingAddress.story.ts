import { storiesOf } from '@storybook/react';

import { inStepFlowChrome, mockedFunction, render } from 'bux/common/helpers/storybook';

import StepBillingAddress from './StepBillingAddress';

storiesOf('BUX|Focused tasks/Add payment details/Step 1: Billing Address', module)
  .add('Default', () => inStepFlowChrome(render(StepBillingAddress, {
    onSubmit: mockedFunction,
    next: mockedFunction,
    back: mockedFunction,
    push: mockedFunction,
    isFirstStep: true,
  })));
