import React from 'react';
import sinon from 'sinon';
import { expect } from 'chai';
import { mount } from 'enzyme';
import { Provider } from 'bux/helpers/redux';
import { Form } from 'redux-form';
import { DecoratedStepBillingAddress } from '../StepBillingAddress';

const initialValues = {
  organisationName: 'Potato Corp.',
  taxpayerId: '12345678200',
  addressLineFull: 'Level 6, 341 George St Sydney 2000 NSW',
  originalAddressLineFull: 'Level 6, 341 George St Sydney 2000 NSW',
  addressLine1: 'Level 6',
  addressLine2: '341 George St',
  city: 'Sydney',
  postcode: '2000',
  state: 'NSW',
  country: 'AU',
};

describe('StepBillingAddress: ', () => {
  let push;
  let next;
  let back;
  let formValidator;

  const Component = (props) => {
    const defaultProps = {
      formValidator,
      next,
      back,
      push,
      initialValues,
      isFirstStep: false,
      ...props
    };
    return (
      <Provider>
        <DecoratedStepBillingAddress {...defaultProps} />
      </Provider>
    );
  };

  beforeEach(() => {
    push = sinon.stub().returns({});
    next = sinon.stub();
    back = sinon.stub();
    formValidator = sinon.stub().returns({});
  });

  it('should render component', () => {
    const wrapper = mount(<Component />);
    expect(wrapper).to.be.present();
    expect(wrapper.find('BillingAddress')).to.be.present();
  });
  it('should call back when click back button', () => {
    const wrapper = mount(<Component />);
    expect(wrapper.find('button.cancel-flow')).to.be.present();
    wrapper.find('button.cancel-flow').simulate('click');
    expect(back).to.be.called();
  });
  it('should not proceed when click next button with invalid form', () => {
    const props = {
      initialValues: {}
    };
    const wrapper = mount(<Component {...props} />);
    expect(wrapper.find('SubmitButton')).to.be.present();
    wrapper.find('SubmitButton').simulate('click');
    expect(next).to.not.be.called();
    expect(push).to.not.be.called();
  });
  it('should proceed when submit with valid form', (done) => {
    const wrapper = mount(<Component />);
    wrapper.find(Form).props().onSubmit({ form: 'data' });
    setImmediate(() => {
      expect(formValidator).to.be.called();
      expect(push).to.be.calledWithMatch({ stepBillingAddressData: {} });
      expect(next).to.be.called();
      done();
    });
  });
  it('should change originalAddressLineFull if receive stepBillingAddressData', () => {
    const props = {
      stepBillingAddressData: {
        addressLineFull: 'new address'
      }
    };
    const wrapper = mount(<Component {...props} />);
    expect(wrapper.find('BillingAddress'))
      .to.have.prop('initialValues')
      .deep.equal({ ...initialValues, originalAddressLineFull: 'new address' });
  });
});
