import { storiesOf } from '@storybook/react';

import { inStepFlowChrome, mockedFunction, render } from 'bux/common/helpers/storybook';

import StepBillingMethod from './StepBillingMethod';

const props = {
  push: mockedFunction,
  back: mockedFunction,
  next: mockedFunction,
  paypalAccount: {
    email: 'user@example.org',
  },
};

storiesOf('BUX|Focused tasks/Add payment details/Step 2: Billing Method', module)
  .add('Default', () => inStepFlowChrome(render(StepBillingMethod, props)))
  .add('Authorized PayPal', () => inStepFlowChrome(render(StepBillingMethod, { ...props, authorizedInPayPal: true })));
