import { withI18n } from '@lingui/react';
import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import getCountryTaxName from 'bux/common/helpers/countrySpecific/nameOfTaxID';
import validateTaxId from 'bux/common/helpers/validators/taxID';
import { WideInputField } from 'bux/components/field/InputField';
import { FeatureFlagsContext } from 'bux/components/FeatureFlags';
import { Analytics } from 'bux/components/Analytics';

import AddressFields, { isValidForShortForm } from '../AddressFields';

export class BillingAddress extends React.Component {
  static propTypes = {
    autofill: PropTypes.func.isRequired,
    touch: PropTypes.func.isRequired,
    country: PropTypes.string,
    state: PropTypes.string,
    initialValues: PropTypes.object,
    i18n: PropTypes.object,
    validateCountry: PropTypes.func,
    shouldDisableSave: PropTypes.func,
  };

  static validationRules = props => ({
    ...(AddressFields.validationRules(props)),
    organisationName: 'required|string|min:1',
  });

  static contextType = FeatureFlagsContext;

  onCountryChange = () => {
    this.props.autofill('taxpayerId', '');
    this.props.touch('taxpayerId');
  };

  validateTax = (value, values) => (
    validateTaxId(
      values.country,
      // remove state for CA-QC if FF is not set
      values.country === 'CA' && !this.context['tax.qst'] ? '' : values.state,
      value,
      this.props.i18n
    )
  );

  render() {
    const {
      autofill,
      touch,
      country,
      state,
      initialValues,
      i18n,
      validateCountry,
      shouldDisableSave,
    } = this.props;
    const { displayName: taxLabel, description: taxDescription } = getCountryTaxName(country, state);
    return (
      <div>
        <Analytics.UI as="organisationNameField"> 
          <Field
            name="organisationName"
            component={WideInputField}
            required
            labelText={i18n.t('billing.billing-address.company-name')`Company name`}
          />  
        </Analytics.UI>
        <AddressFields
          autofill={autofill}
          touch={touch}
          country={country}
          defaultShortMode={isValidForShortForm(initialValues)}
          onCountryChange={this.onCountryChange}
          initialValues={initialValues}
          validateCountry={validateCountry}
          shouldDisableSave={shouldDisableSave}
        />
        <Analytics.UI as="taxPayerIdField">
          <Field
            name="taxpayerId"
            component={WideInputField}
            labelText={taxLabel}
            help={taxDescription}
            validate={this.validateTax}
          />
        </Analytics.UI>
      </div>
    );
  }
}

const BillingAddressI18n = withI18n()(BillingAddress);

export default BillingAddressI18n;
