import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import identity from 'bux/common/helpers/identity';
import { i18n } from 'bux/helpers/jsLingui';
import { BillingAddress } from '../BillingAddress';

const DEFAULT_PROPS = {
  autofill: identity,
  touch: identity,
  country: 'AU',
  initialValues: {},
  i18n
};

describe('BillingAddress: ', () => {
  describe('TaxID', () => {
    it('should display taxID in localized name', () => {
      const AU = shallow(<BillingAddress {...DEFAULT_PROPS} country="AU" />);
      expect(AU.find({ name: 'taxpayerId' })).to.have.prop('labelText', 'ABN');

      const GB = shallow(<BillingAddress {...DEFAULT_PROPS} country="GB" />);
      expect(GB.find({ name: 'taxpayerId' })).to.have.prop('labelText', 'VAT Number');

      const US = shallow(<BillingAddress {...DEFAULT_PROPS} country="US" />);
      expect(US.find({ name: 'taxpayerId' })).to.have.prop('labelText', 'EIN');
    });
  });
});
