import { Trans } from '@lingui/react';
import cx from 'classnames';
import * as PropTypes from 'prop-types';
import * as React from 'react';
import { connect } from 'react-redux';

import { getFullAddress } from 'bux/common/helpers/address';
import { redirect } from 'bux/common/helpers/browser';
import Button from 'bux/components/Button';
import Link from 'bux/components/Link/Link';
import { Billing, OPTION_BANK_TRANSFER, OPTION_ON_FILE } from 'bux/components/PaymentWizard';
import { Footer } from 'bux/components/Wizard';
import { getAnnualConversionEstimate } from 'bux/core/state/bill-estimate/actions';
import { getAnnualConversionEstimateId, getAnnualConversionEstimateMetadata } from 'bux/core/state/bill-estimate/selectors';
import { PAYMENT_METHOD_CREDIT_CARD } from 'bux/core/state/billing-details/constants';
import { getBillingDetails, getPaymentMethod, getPaymentMethodDetails } from 'bux/core/state/billing-details/selectors';
import { getTenantInfo } from 'bux/core/state/meta/selectors';
import { downloadQuote, processOrder } from 'bux/features/monthly-to-annual/actions';
import { MonthlyToAnnualErrorMessage } from 'bux/features/monthly-to-annual/components/MonthlyToAnnualErrorMessage';
import {
  ConvertToAnnualError, PROGRESS_NETWORK_ERROR, PROGRESS_SERVER_ERROR, PROGRESS_TIMEOUT_ERROR, PROGRESS_UNSUCCESSFUL_ERROR,
} from 'bux/features/monthly-to-annual/types';

import { AnnualConversionBillEstimate, DownloadQuote } from './components';
import { getBill } from './selectors';
import styles from './styles.less';

const SWITCH_TO_ANNUAL_LOG_PREFIX = 'switch-to-annual-confirmation';

export interface MonthlyToAnnualOverviewProps {
  billingDetails: any;
  paymentMethod: string;
  paymentDetails: any;
  tenantInfo: any;
  isLoading?: boolean;
  hasLoadingError?: boolean;
  annualConversionEstimate: any;
  annualConversionEstimateId: string;
  error?: ConvertToAnnualError;
  selectedPaymentOption?: string;
  closeAction();
  getEstimate();
  downloadQuote(estimateId: string);
  processOrder(estimateId: string);
}

export interface MonthlyToAnnualOverviewState {
  isSubmitting: boolean;
  selectedPaymentOption: string;
  error?: ConvertToAnnualError;
}

export class MonthlyToAnnualOverview extends React.Component<MonthlyToAnnualOverviewProps> {
  public static contextTypes = {
    sendAnalyticEvent: PropTypes.func,
  };

  public state: MonthlyToAnnualOverviewState = {
    isSubmitting: false,
    selectedPaymentOption: this.props.selectedPaymentOption || OPTION_ON_FILE,
    error: this.props.error,
  };

  public componentDidMount() {
    this.loadEstimate();
  }

  public loadEstimate = () => {
    this.props.getEstimate();
  };

  public handleChangePaymentMethod = (paymentMethodOption) => {
    this.sendAnalyticEvent(`payment-method.${paymentMethodOption}`);
    this.setState({ selectedPaymentOption: paymentMethodOption });
  };

  public handleSubmit = () => {
    if (this.state.selectedPaymentOption === OPTION_ON_FILE) {
      this.handleSubmitPaymentMethod();
    } else {
      this.handleSubmitBankTransfer();
    }
  };

  public handleSubmitPaymentMethod = () => {
    this.setState({ isSubmitting: true });
    this.props.processOrder(this.props.annualConversionEstimateId)
      .then(() => {
        this.sendAnalyticEvent('order.submit.finished');
        this.props.closeAction();
      })
      .catch((error) => {
        this.sendAnalyticEvent('order.submit.error');
        this.setState({ isSubmitting: false, error });
      });
  };

  public handleSubmitBankTransfer = () => {
    this.setState({ isSubmitting: true });
    this.props.downloadQuote(this.props.annualConversionEstimateId)
      .then((data) => {
        this.sendAnalyticEvent('generate-quote.finished');
        redirect(data.quoteLink);
        this.props.closeAction();
      })
      .catch(() => {
        this.sendAnalyticEvent('generate-quote.error');
        this.setState({ isSubmitting: false });
      });
  };

  public sendAnalyticEvent(event: string) {
    return this.context.sendAnalyticEvent(`${SWITCH_TO_ANNUAL_LOG_PREFIX}.${event}`);
  }

  public render() {
    const { isSubmitting, selectedPaymentOption, error } = this.state;
    const {
      closeAction, billingDetails, paymentMethod, paymentDetails, annualConversionEstimate,
      isLoading, hasLoadingError, tenantInfo, annualConversionEstimateId,
    } = this.props;

    const isBankTransfer = selectedPaymentOption === OPTION_BANK_TRANSFER;
    const addressLineFull = getFullAddress(billingDetails);
    const billingAddress = { ...billingDetails, addressLineFull };
    const { organisationName, country } = billingAddress;
    const { currentBillingPeriod, nextBillingPeriod } = annualConversionEstimate;

    const preventRetryAfterError = error && [
      PROGRESS_NETWORK_ERROR, PROGRESS_SERVER_ERROR, PROGRESS_TIMEOUT_ERROR, PROGRESS_UNSUCCESSFUL_ERROR,
    ].includes(error.type);

    if (hasLoadingError) {
      this.sendAnalyticEvent('estimate.error');
    }

    return (
      <div className={cx('switch-to-annual-wizard', styles.page)} data-test="new-switch-to-annual-wizard">
        <Billing.Overview
          title={<Trans id="billing.convert-to-annual.title">Switch to annual payment plan</Trans>}
          logPrefix={SWITCH_TO_ANNUAL_LOG_PREFIX}
          dataTest="new-switch-to-annual-wizard"
          isLoading={isLoading}
          hasLoadingError={hasLoadingError}
          loadingErrorMsg={<Trans id="billing.convert-to-annual.loading-error">Sorry, we can't estimate right now</Trans>}
          loadingErrorRetry={this.loadEstimate}
        >
          <Billing.Estimate
            title={<Trans id="billing.convert-to-annual.estimate.title">Annual billing summary</Trans>}
            isOrg={tenantInfo.isOrg}
            name={tenantInfo.name}
            info={<Trans id="billing.convert-to-annual.estimate.info">All products on this bill will be switched to an annual payment plan.</Trans>}
          >
            <AnnualConversionBillEstimate />
          </Billing.Estimate>
          <Billing.Period
            title={<Trans id="billing.convert-to-annual.period.title">Annual billing period</Trans>}
            startDate={nextBillingPeriod.startDate}
            endDate={nextBillingPeriod.endDate}
          />
          <Billing.Text>
            <Trans id="billing.convert-to-annual.quote-will-be-sent">
              A renewal quote will be sent 60 days prior to your renewal date.
            </Trans>
          </Billing.Text>
          <Billing.Address
            organisationName={organisationName}
            addressLineFull={addressLineFull}
            country={country}
          />
          <Billing.Text>
            <Trans id="billing.convert-to-annual.update-address">
              To update your billing address, go to &nbsp;
              <Link to="/paymentdetails" name="to-billing-details">Billing details</Link>.
            </Trans>
          </Billing.Text>
          <Billing.PayNowOrLater
            method={paymentMethod}
            details={paymentDetails}
            nextRenewalDate={currentBillingPeriod.endDate}
            onPaymentMethodChange={this.handleChangePaymentMethod}
          />
          <DownloadQuote
            estimateId={annualConversionEstimateId}
            onEndDownload={closeAction}
          />
          <MonthlyToAnnualErrorMessage error={error} />
          { !preventRetryAfterError && isBankTransfer &&
          <Billing.Controls
            submitLabel={<Trans id="billing.convert-to-annual.button.download">Download invoice</Trans>}
            submitName="download-quote"
            isSubmitDisabled={isSubmitting}
            onSubmit={this.handleSubmit}
            onCancel={closeAction}
            isFirstStep
            isBusy={isSubmitting}
          />
          }
          { !preventRetryAfterError && !isBankTransfer &&
          <Billing.Controls
            submitLabel={<Trans id="billing.convert-to-annual.button.pay">Pay & switch to annual</Trans>}
            submitName="convert-to-annual"
            isSubmitDisabled={isSubmitting}
            onSubmit={this.handleSubmit}
            onCancel={closeAction}
            isFirstStep
            isBusy={isSubmitting}
          />
          }
          {preventRetryAfterError &&
          <Footer>
            <Button onClick={closeAction} name="close">
              <Trans id="billing.convert-to-annual.button.close">Close</Trans>
            </Button>
          </Footer>
          }
          {isBankTransfer &&
          <p className={styles.footerNotesHighlighted}>
            <Trans id="billing.convert-to-annual.invoice-instructions">
              Atlassian will provide you an invoice with payment instructions via a PDF download and email.
            </Trans>
          </p>
          }
          {!isBankTransfer && paymentMethod === PAYMENT_METHOD_CREDIT_CARD &&
          <p className={styles.footerNotes}>
            <Trans id="billing.convert-to-annual.statement">
              Your billing statement will display Atlassian B.V., located in Amsterdam, Netherlands.
              Atlassian Pty Limited, our principal place of business, is at Level 6, 341 George Street, Sydney NSW
              Australia 2000. Your credit card issuer may charge foreign transaction or cross-border fees in addition
              to the total price above.
            </Trans>
          </p>
          }
        </Billing.Overview>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isLoading: getAnnualConversionEstimateMetadata(state).loading,
  hasLoadingError: getAnnualConversionEstimateMetadata(state).error,
  billingDetails: getBillingDetails(state),
  paymentDetails: getPaymentMethodDetails(state),
  paymentMethod: getPaymentMethod(state),
  annualConversionEstimate: getBill(state),
  annualConversionEstimateId: getAnnualConversionEstimateId(state),
  tenantInfo: getTenantInfo(state),
});

const mapDispatchToProps = dispatch => ({
  getEstimate: payload => dispatch(getAnnualConversionEstimate(payload)),
  downloadQuote: payload => dispatch(downloadQuote(payload)),
  processOrder: payload => dispatch(processOrder(payload)),
});

export default connect<{}>(mapStateToProps, mapDispatchToProps)(MonthlyToAnnualOverview);
