import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import { ExternalLink } from 'bux/components/Link';
import getAnalytic from 'bux/components/log/helpers/getAnalytic';
import { rewiremock } from 'bux/helpers/rewire-mock';

import { DownloadQuoteProps, QuoteData } from '../DownloadQuote';

describe('DownloadQuote', () => {
  const redirect = sinon.stub();

  const { DownloadQuoteImpl } = rewiremock.proxy('../DownloadQuote', {
    'bux/common/helpers/browser': { redirect },
  });

  let defaultProps: DownloadQuoteProps;

  beforeEach(() => {
    defaultProps = {
      estimateId: 'random-id',
      downloadQuote: sinon.stub(),
    };
  });

  it('should render component', () => {
    const wrapper = shallow(<DownloadQuoteImpl {...defaultProps}/>);
    expect(wrapper).to.be.present();
    expect(wrapper.find(ExternalLink)).to.be.present();
  });

  it('should start download', (done) => {
    const analytic = getAnalytic();
    const quote: QuoteData = { quoteLink: 'dummy' };
    const props: DownloadQuoteProps = {
      ...defaultProps,
      downloadQuote: sinon.stub().resolves(quote),
    };
    const wrapper = shallow(<DownloadQuoteImpl {...props}/>, analytic.context);
    expect(wrapper).to.be.present();
    expect(wrapper.find(ExternalLink)).to.be.present();
    wrapper.find(ExternalLink).simulate('click');

    expect(wrapper).to.have.state('downloadingQuote', true);
    // tslint:disable-next-line: no-unbound-method
    expect(props.downloadQuote).to.be.calledWith(defaultProps.estimateId);

    setImmediate(() => {
      expect(redirect).to.be.calledWith('dummy');
      expect(wrapper).to.have.state('downloadingQuote', false);
      done();
    });
  });

  it('should handle download failure', (done) => {
    const analytic = getAnalytic();
    const props: DownloadQuoteProps = {
      ...defaultProps,
      downloadQuote: sinon.stub().rejects(),
    };
    const wrapper = shallow(<DownloadQuoteImpl {...props}/>, analytic.context);
    expect(wrapper).to.be.present();
    expect(wrapper.find(ExternalLink)).to.be.present();
    wrapper.find(ExternalLink).simulate('click');
    expect(wrapper).to.have.state('downloadingQuote', true);

    // tslint:disable-next-line: no-unbound-method
    expect(props.downloadQuote).to.be.calledWith(defaultProps.estimateId);
    setImmediate(() => {
      expect(wrapper).to.have.state('downloadingQuote', false);
      done();
    });
  });
});
