import BillEstimateDetailComponent from 'bux/features/bill-estimate/components/components/BillEstimateDetail';
import { getBill, getFullProductList, getTotalCost }
  from 'bux/features/payment-details/components/MonthlyToAnnual/selectors';
import { connect } from 'react-redux';

const mapStateToProps = (state, props) => ({
  taxAmount: getBill(state).totalTaxAmount,
  totalCost: getTotalCost(state, props.excludeTax),
  endDate: getBill(state).currentBillingPeriod.endDate,
  currencyCode: getBill(state).currencyCode,
  productList: getFullProductList(state),
  style: 'compact'
});

export const AnnualConversionBillEstimate = connect(mapStateToProps)(BillEstimateDetailComponent);
