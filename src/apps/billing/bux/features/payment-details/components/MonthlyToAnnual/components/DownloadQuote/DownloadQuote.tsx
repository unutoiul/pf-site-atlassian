import { Trans } from '@lingui/react';
import cx from 'classnames';
import * as PropTypes from 'prop-types';
import * as React from 'react';
import { connect } from 'react-redux';

import AkEditorInfoIcon from '@atlaskit/icon/glyph/editor/info';
import { colors as AkColors } from '@atlaskit/theme';

import { redirect } from 'bux/common/helpers/browser';
import { InlineDialog } from 'bux/components/InlineDialog';
import { ExternalLink } from 'bux/components/Link';
import LogScope from 'bux/components/log/Scope';
import { downloadQuote } from 'bux/features/monthly-to-annual/actions';

import styles from './styles.less';

export interface QuoteData {
  quoteLink: string;
}

export interface DownloadQuoteProps {
  estimateId: string;
  downloadQuote(id: string): Promise<QuoteData>;
  onEndDownload?();
}

const LearnMoreContent = () => (
  <div>
    <p>
      <Trans id="billing.download-quote.note">
        <strong>Note:</strong> This quote will no longer be valid if you add new products to the site after it’s been generated.
      </Trans>
    </p>
  </div>
);

export class DownloadQuoteImpl extends React.PureComponent<DownloadQuoteProps> {
  public static contextTypes = {
    sendAnalyticEvent: PropTypes.func,
  };

  public state = {
    downloadingQuote: false,
  };

  public downloadQuote = () => {
    const { estimateId } = this.props;
    this.setState({ downloadingQuote: true });

    this.props.downloadQuote(estimateId)
      .then(data => {
        this.context.sendAnalyticEvent('generate-quote.finished');
        this.setState({ downloadingQuote: false });
        redirect(data.quoteLink);
        if (this.props.onEndDownload) {
          this.props.onEndDownload();
        }
      })
      .catch(() => {
        this.context.sendAnalyticEvent('generate-quote.error');
        this.setState({ downloadingQuote: false });
      });
  };

  public render() {
    const { downloadingQuote } = this.state;
    const link = <ExternalLink className="link" onClick={!downloadingQuote ? this.downloadQuote : null}>download</ExternalLink>;

    return (
      <LogScope prefix="download">
        <div className={cx(styles.downloadQuoteSection)} data-test="download-quote">
          <Trans id="billing.download-quote.not-ready">
            If you're not ready to pay, {link} a no-obligation 30-day price guaranteed quote now and pay later.
          </Trans>{' '}
          <InlineDialog content={<LearnMoreContent />}>
            <AkEditorInfoIcon size="small" primaryColor={AkColors.P300} label="" />
          </InlineDialog>
        </div>
      </LogScope>
    );
  }
}

const mapDispatchProps = (dispatch) => ({
  downloadQuote: estimateId => dispatch(downloadQuote(estimateId)),
});

export const DownloadQuote = connect(null, mapDispatchProps)(DownloadQuoteImpl);
