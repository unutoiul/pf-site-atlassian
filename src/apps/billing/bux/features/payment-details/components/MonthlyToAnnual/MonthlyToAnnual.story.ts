import { storiesOf } from '@storybook/react';

import { inScenarios, inStepFlowChrome, render, withServicePreLoader } from 'bux/common/helpers/storybook';
import { OPTION_BANK_TRANSFER } from 'bux/components/PaymentWizard';
import { ConvertToAnnualError, OrderProgress } from 'bux/features/monthly-to-annual/types';

import MonthlyToAnnual from './MonthlyToAnnual';

const orderProgress: OrderProgress = {
  completed: false,
  successful: false,
  errorMessage: '',
  status: '',
  orderReference: 'AT-1234',
  orderId: '1234',
};
const PreparedMonthlyToAnnual = (props = {}) => inStepFlowChrome(withServicePreLoader(render(MonthlyToAnnual, props)));

storiesOf('BUX|Focused tasks/Monthly to annual', module)
  .add('Default', () => inScenarios([], PreparedMonthlyToAnnual()))
  .add('Bank transfer', () => inScenarios([], PreparedMonthlyToAnnual({ selectedPaymentOption: OPTION_BANK_TRANSFER })))
  .add('PayPal', () => inScenarios(['billing-details-paypal'], PreparedMonthlyToAnnual()))
  .add('Server Error', () => inScenarios([], PreparedMonthlyToAnnual({ error: ConvertToAnnualError.orderServer() })))
  .add('Progress Error', () => inScenarios([], PreparedMonthlyToAnnual({ error: ConvertToAnnualError.progressNetwork(orderProgress) })))
  .add('Progress Timeout', () => inScenarios([], PreparedMonthlyToAnnual({ error: ConvertToAnnualError.progressTimedOut(orderProgress) })))
  .add('Load estimate error', () => inScenarios(['change-renewal-frequency-estimate-error'], PreparedMonthlyToAnnual()));
