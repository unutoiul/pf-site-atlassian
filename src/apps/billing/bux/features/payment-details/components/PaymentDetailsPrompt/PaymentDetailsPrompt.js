import { Trans } from '@lingui/react';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import deferred from 'bux/components/deferred';
import billEstimate from 'bux/core/state/bill-estimate';
import Prompt from 'bux/components/Page/Prompt';
import { RouterButton } from 'bux/components/Button';
import Money from 'bux/components/Money';
import { Analytics } from 'bux/components/Analytics';
import AddPaymentDetailsPrompt
  from 'bux/features/payment-details/components/PaymentDetailsForm/components/AddPaymentDetailsPrompt';


const MissingDetailWarning = deferred(async () => (
  (await import(/* webpackChunkName: "missing-detail" */'bux/static/missing-detail.svgx')).default
));

export const StatelessPaymentDetailsPrompt = ({ totalCost, currencyCode, fullPage }) => {
  const formattedAmount = <Money amount={totalCost} currency={currencyCode} includeLabel />;

  return fullPage ? (
    <Prompt
      className="paymentDetailsPrompt"
      media={<MissingDetailWarning />}
      title={<Trans id="billing.payment-details.prompt.title">You haven't added billing details</Trans>}
      message={
        <span>
          <Trans id="billing.payment-details.prompt.estimate">
            Your bill estimate is {formattedAmount}
          </Trans>
        </span>
      }
      actions={
        <Analytics.UI as="addBillingDetailsButton">
          <RouterButton to="/paymentdetails/add" primary>
            <Trans id="billing.payment-details.prompt.add">
              Add billing details
            </Trans>
          </RouterButton>
        </Analytics.UI>
      }
    />
  ) : (
    <AddPaymentDetailsPrompt className="paymentDetailsPrompt" />
  );
};

StatelessPaymentDetailsPrompt.propTypes = {
  totalCost: PropTypes.number.isRequired,
  currencyCode: PropTypes.string.isRequired,
  fullPage: PropTypes.bool,
};

const mapStateToProps = state => ({
  totalCost: billEstimate.selectors.getBill(state).totalAmountWithTax,
  currencyCode: billEstimate.selectors.getBill(state).currencyCode,
});

export default connect(mapStateToProps)(StatelessPaymentDetailsPrompt);
