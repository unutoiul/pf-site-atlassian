import { storiesOf } from '@storybook/react';
import { inContentChrome, render } from 'bux/common/helpers/storybook';

import PaymentDetailsPrompt from './PaymentDetailsPrompt';

storiesOf('BUX|Features/PaymentDetails/PaymentDetailsPrompt', module)
  .add('PaymentDetailsPrompt [Prompt]', () => inContentChrome(render(PaymentDetailsPrompt)));

