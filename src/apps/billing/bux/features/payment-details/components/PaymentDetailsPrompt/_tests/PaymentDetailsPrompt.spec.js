import configureMockStore from 'redux-mock-store';
import React from 'react';
import sinon from 'sinon';
import { expect } from 'chai';
import { mount } from 'enzyme';
import Link from 'bux/components/Link';
import { Provider } from 'bux/helpers/redux';
import proxyquire from 'bux/helpers/proxyquire';
import createStore from 'bux/core/createStore';
import Money from 'bux/components/Money';
import Button from 'bux/components/Button';
import AddPaymentDetailsPrompt
// eslint-disable-next-line max-len
  from 'bux/features/payment-details/components/PaymentDetailsForm/components/AddPaymentDetailsPrompt/AddPaymentDetailsPrompt';

describe('PaymentDetailsPrompt: ', () => {
  let PaymentDetailsPrompt;
  let getBill;
  let store;

  const renderContainer = ({ fullPage }) => {
    getBill.returns({
      totalAmountWithTax: 1000,
      currencyCode: 'USD'
    });
    return mount(<Provider store={store}>
      <PaymentDetailsPrompt fullPage={!!fullPage} store={configureMockStore()()} />
    </Provider>);
  };

  beforeEach(() => {
    store = createStore();
    getBill = sinon.stub().returns({});
    PaymentDetailsPrompt = proxyquire.noCallThru().load('../PaymentDetailsPrompt', {
      'bux/core/state/bill-estimate': { selectors: { getBill } },
    }).default;
  });

  describe('Full page', () => {
    it('should render promptImage', () => {
      const wrapper = renderContainer({ fullPage: true });
      expect(wrapper).to.be.present();
      expect(wrapper.find('.prompt-image')).to.be.present();
    });

    it('should render mainMessage', () => {
      const wrapper = renderContainer({ fullPage: true });
      expect(wrapper).to.be.present();
      expect(wrapper.find('.main-message')).to.be.present();
    });

    it('should render a Link to billing details page', () => {
      const wrapper = renderContainer({ fullPage: true });
      expect(wrapper).to.be.present();
      expect(wrapper.find(Link)).to.be.present();
      expect(wrapper.find(Link).props().to).to.be.equal('/paymentdetails/add');
    });

    it('should render total cost and currency code', () => {
      const wrapper = renderContainer({ fullPage: true });
      expect(wrapper).to.be.present();
      expect(wrapper).to.contain(<Money amount={1000} currency="USD" includeLabel />);
    });

    it('call to action button should be a primary button', () => {
      const wrapper = renderContainer({ fullPage: true });
      const button = wrapper.find(Button);
      expect(button).to.be.present();
      expect(button.prop('primary')).to.be.equal(true);
      expect(button.children().text()).to.be.equal('Add billing details');
    });
  });

  describe('Widget mode', () => {
    it('should render compact widget', () => {
      const wrapper = renderContainer({});
      expect(wrapper).to.be.present();
      expect(wrapper.find(AddPaymentDetailsPrompt)).to.be.present();
    });
  });
});
