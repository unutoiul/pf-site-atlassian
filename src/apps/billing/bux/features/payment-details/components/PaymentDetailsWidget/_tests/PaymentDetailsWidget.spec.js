import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import { StaticRouter } from 'react-router-dom';
import { PAYMENT_METHOD_CREDIT_CARD } from 'bux/core/state/billing-details/constants';
import { EditLink } from 'bux/components/Widget';
import { PaymentDetailsWidgetImpl } from '../PaymentDetailsWidget';

const DEFAULT_PROPS = {
  name: 'Dr Potato',
  expiryMonth: 0,
  expiryYear: 0,
  suffix: '123',
  type: 'visa',
  isAnnual: false
};

const renderWidget = (ccoptions, props = { paymentMethod: PAYMENT_METHOD_CREDIT_CARD }) =>
  (<StaticRouter context={{}}>
    <PaymentDetailsWidgetImpl details={{ ...DEFAULT_PROPS, ...ccoptions }} {...props} />
   </StaticRouter>);

describe('PaymentDetailsWidgetImpl component: ', () => {
  it('should render edit link', () => {
    const wrapper = mount(renderWidget({}));

    expect(wrapper).to.be.present();
    expect(wrapper.find(EditLink)).to.be.present();
  });

  context('Annual', () => {
    it('should render credit card name', () => {
      const billingCycle = mount(renderWidget({}, { isAnnual: true }))
        .find('[data-test="billing-cycle"]');

      expect(billingCycle).to.contain.text('Annual');
    });
  });
  context('Monthly', () => {
    context('credit card', () => {
      it('should render credit card name', () => {
        const name = mount(renderWidget({ name: 'Username' }))
          .find('.name');

        expect(name).to.have.text('Username');
      });

      it('should render expiry date', () => {
        const expiry = mount(renderWidget({ expiryMonth: 10, expiryYear: 2016 })).find('.expiry');

        expect(expiry).to.have.text('Expires 10/2016');
      });

      it('should render masked number', () => {
        const number = mount(renderWidget({ suffix: '1234' })).find('.number');

        expect(number).to.have.text('•••• •••• •••• 1234');
      });
    });
    context('should render paypal', () => {
      it('should render paypal account email', () => {
        const email = mount(renderWidget({ email: 'user@example.com' }, { paymentMethod: 'PAYPAL_ACCOUNT' }))
          .find('.email');

        expect(email).to.have.text('user@example.com');
      });
    });
  });
});
