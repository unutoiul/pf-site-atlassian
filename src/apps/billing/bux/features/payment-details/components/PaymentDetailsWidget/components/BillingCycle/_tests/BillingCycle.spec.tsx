import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';

import { BillingCycle } from '../BillingCycle';

describe('BillingCycle', () => {
  it('should render annual', () => {
    const wrapper = shallow(<BillingCycle isAnnual={true} />);
    expect(wrapper).to.be.present();
    expect(wrapper).to.contain.text('Annual');
  });

  it('should render monthly', () => {
    const wrapper = shallow(<BillingCycle isAnnual={false} />);
    expect(wrapper).to.be.present();
    expect(wrapper).to.contain.text('Monthly');
  });
});
