import { I18n } from '@lingui/react';
import { oneOfType, shape, string, bool } from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import cx from 'classnames';
import ExtendedPropTypes from 'bux/core/extended-proptypes';
import Widget, { EditLink } from 'bux/components/Widget/index';
import PaymentMethod from 'bux/components/PaymentMethod/index';
import CreditCard from 'bux/components/PaymentMethod/components/CreditCard/index';
import Paypal from 'bux/components/PaymentMethod/components/Paypal/index';
import billingDetails from 'bux/core/state/billing-details';
import Scope from 'bux/components/log/Scope';
import { isNextBillingPeriodRenewalFrequencyAnnual } from 'bux/core/state/bill-estimate/selectors';
import PaymentDetailsPrompt from 'bux/features/payment-details/components/PaymentDetailsPrompt';
import { Analytics } from 'bux/components/Analytics';
import { BillingCycle } from './components/BillingCycle';
import styles from './PaymentDetailsWidget.less';

export const PaymentDetailsWidgetImpl = ({
  paymentMethod, details, isAnnual
}) => (
  <React.Fragment>
    <Scope prefix="paymentDetails">
      <Analytics.UI
        as="billingDetailsEditButton"
        attributes={{ 
          linkContainer: 'billingDetailCard', 
          objectType: 'billingDetail' 
        }}      
      >
        <EditLink to="/paymentdetails" />
      </Analytics.UI>
      <div>
        <BillingCycle isAnnual={isAnnual} />
        {!isAnnual && <PaymentMethod paymentMethod={paymentMethod} details={details} />}
      </div>
    </Scope>
  </React.Fragment>
);

PaymentDetailsWidgetImpl.propTypes = {
  paymentMethod: string,
  details: oneOfType([
    CreditCard.propTypes.details,
    Paypal.propTypes.details,
    shape({})
  ]),
  isAnnual: bool
};

export const PaymentDetailsWidgetSwitch = ({
  hasPaymentMethodOnAccount, className, metadata, ...props
}) => (
  <I18n>
    {({ i18n }) => (
      <Widget
        name={i18n.t('billing.widget.payment-details.name')`Billing details`}
        errorMessage={{ name: i18n.t('billing.widget.payment-details.name.in-error-message')`payment details` }}
        className={cx(className, 'payment-detail-widget')}
        metadata={metadata}
      >
        <Scope prefix="paymentDetails" className={styles.scope}>
          {
            (hasPaymentMethodOnAccount || props.isAnnual)
              ? <PaymentDetailsWidgetImpl {...props} />
              : <PaymentDetailsPrompt />
          }
        </Scope>
      </Widget>
    )}
  </I18n>
);

PaymentDetailsWidgetSwitch.propTypes = {
  ...PaymentDetailsWidgetImpl.propTypes,
  className: string,
  hasPaymentMethodOnAccount: bool,
  metadata: ExtendedPropTypes.metadata,
};

const mapStateToProps = state => ({
  metadata: billingDetails.selectors.getBillingDetailsMetadata(state),
  details: billingDetails.selectors.getPaymentMethodDetails(state),
  paymentMethod: billingDetails.selectors.getPaymentMethod(state),
  isAnnual: isNextBillingPeriodRenewalFrequencyAnnual(state),
  hasPaymentMethodOnAccount: billingDetails.selectors.hasPaymentMethod(state),
});

const PaymentDetailsWidget = connect(mapStateToProps)(PaymentDetailsWidgetSwitch);

export default PaymentDetailsWidget;
