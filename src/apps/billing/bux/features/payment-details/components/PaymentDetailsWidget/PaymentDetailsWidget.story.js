import React from 'react';
import { storiesOf } from '@storybook/react';
import { inContentChrome, inStoryKnob } from 'bux/common/helpers/storybook';
import { StaticRouter } from 'react-router-dom';
import { PAYMENT_METHOD_CREDIT_CARD, PAYMENT_METHOD_PAYPAL } from 'bux/core/state/billing-details/constants';

import { PaymentDetailsWidgetImpl, PaymentDetailsWidgetSwitch } from './PaymentDetailsWidget';

const defaultProps = {
  paymentMethod: PAYMENT_METHOD_CREDIT_CARD,
  hasPaymentMethodOnAccount: true,
  details: {
    name: 'Storybook Rocks',
    expiryMonth: '01',
    expiryYear: '2020',
    suffix: '1234',
    type: 'VISA'
  },
  metadata: {
    display: true,
    loading: false,
    error: null
  },
  isAnnual: false
};

const Component = props => (
  <StaticRouter context={{}}>
    <PaymentDetailsWidgetSwitch {...props} />
  </StaticRouter>
);
Component.propTypes = PaymentDetailsWidgetImpl.propTypes;

const wrap = (story, props) => inContentChrome(inStoryKnob(story, props));

storiesOf('BUX|Features/PaymentDetails/PaymentDetailsWidget', module)
  .add('Credit card', () => wrap(Component, defaultProps))
  .add('Paypal', () =>
    wrap(Component, {
      ...defaultProps,
      paymentMethod: PAYMENT_METHOD_PAYPAL,
      details: { email: 'storybook@test.org' }
    }))
  .add('Annual', () => wrap(Component, { ...defaultProps, isAnnual: true }))
  .add('Loading', () => wrap(Component, { ...defaultProps, metadata: { loading: true } }))
  .add('Error', () => wrap(Component, { ...defaultProps, metadata: { error: true } }))
  .add('No payment', () => wrap(Component, { ...defaultProps, hasPaymentMethodOnAccount: false }))
  .add('Annual/no payment', () => wrap(Component, {
    ...defaultProps,
    isAnnual: true,
    hasPaymentMethodOnAccount: false
  }));
