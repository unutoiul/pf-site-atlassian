import { Trans } from '@lingui/react';
import * as React from 'react';

import styles from './styles.less';

interface BillingCycleProps {
  isAnnual: boolean;
}

export const BillingCycle: React.SFC<BillingCycleProps> = ({ isAnnual }) => (
  <div className={styles.billingCycle} data-test="billing-cycle">
    <div className={styles.title}>
      <Trans id="billing.widget.payment-details.billing-cycle.header">Billing period</Trans>
    </div>
    {isAnnual ?
      <Trans id="billing.widget.payment-details.billing-cycle.annual">Annual</Trans>
      :
      <Trans id="billing.widget.payment-details.billing-cycle.monthly">Monthly</Trans>
    }
  </div>
);
