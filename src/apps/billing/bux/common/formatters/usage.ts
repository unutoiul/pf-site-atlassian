export const formatUnitCode = (unitCode: string = ''): string => unitCode.replace('_', ' ');

export const formatLabel = (units: number, unitCode: string): string => {
  let label = formatUnitCode(unitCode);
  if (Math.abs(units) === 1 && unitCode.endsWith('s')) {
    label = label.substr(0, label.length - 1);
  }

  return label;
};

export const formatUsage = (units: number, unitCode: string): string => {
  const label = formatLabel(units, unitCode);

  return `${units} ${label}`;
};

export const formatTieredUsage = (units: number, currentUnits: number, unitCode: string): string => {
  const label = formatUnitCode(unitCode);

  return `${currentUnits} of ${units} ${label}`;
};
