
export const removeCents = value => (value.replace(/([.,][0]{2})(?![\d])/g, ''));

/**
 * Formats money
 * @param {Number} amount
 * @param {Object|String} options
 * @param {String} [options.currency]
 * @param {Boolean} [options.withCents]
 * @returns {Object}
 */
export const formatMoney = (amount, {
  currency, language, includeLabel, withCents
} = {}) => {
  const format = {
    style: 'currency',
    currency: currency || 'usd',
    currencyDisplay: includeLabel ? 'code' : 'symbol',
  };

  const formatter = new Intl.NumberFormat(language, format);
  const formatted = formatter.format(amount || 0);

  return !withCents && !includeLabel ? removeCents(formatted) : formatted;
};
