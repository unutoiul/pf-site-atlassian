export default function makePlural(term, count) {
  if (typeof count !== 'number' || isNaN(count)) {
    throw new Error(`makePlural expects count to be a number but got: ${count}`);
  }

  if (Math.abs(count) === 1) {
    return term;
  }

  return `${term}s`;
}