import { expect } from 'chai';
import { removeCents } from '../money-formatter';

describe('money-formatter', () => {
  describe('removeCents', () => {
    it('should remove cents', () => {
      expect(removeCents('$123,456.00')).to.be.eq('$123,456');
      expect(removeCents('USD 123,456.00')).to.be.eq('USD 123,456');
      expect(removeCents('US$123,456.00')).to.be.eq('US$123,456');
      expect(removeCents('123.456,00 $')).to.be.eq('123.456 $');
      expect(removeCents('US$ 123.456,00')).to.be.eq('US$ 123.456');
    });
    it('should not remove cents', () => {
      expect(removeCents('$123,456.79')).to.be.eq('$123,456.79');
      expect(removeCents('$123.456,79')).to.be.eq('$123.456,79');
      expect(removeCents('￥123,457')).to.be.eq('￥123,457');
      expect(removeCents('JPY 123,457')).to.be.eq('JPY 123,457');
      expect(removeCents('JP¥123,457')).to.be.eq('JP¥123,457');
      expect(removeCents('123.457 ¥')).to.be.eq('123.457 ¥');
    });
  });
});
