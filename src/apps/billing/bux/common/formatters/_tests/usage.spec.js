import { expect } from 'chai';
import { formatUsage, formatUnitCode } from '../usage';


describe('Usage formatter', () => {
  it('should format usage', () => {
    const formattedUsage = formatUsage(1234, 'tons');
    expect(formattedUsage).to.be.equal('1234 tons');
  });

  it('should remove plurals if 1', () => {
    const formattedUsage = formatUsage(1, 'tons');
    expect(formattedUsage).to.be.equal('1 ton');
  });

  it('should remove underscores', () => {
    const formattedUsage = formatUsage(12, 'rotten_bananas');
    expect(formattedUsage).to.be.equal('12 rotten bananas');
  });
});

describe('Unit code formatter', () => {
  it('should remove underscores', () => {
    const formattedUnitCode = formatUnitCode('rotten_bananas');
    expect(formattedUnitCode).to.be.equal('rotten bananas');
  });
});
