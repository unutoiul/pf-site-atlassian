import { expect } from 'chai';
import replacePlaceholders from '../replace-placeholders';

describe('replace placeholders', () => {
  it('should return original string', () => {
    expect(replacePlaceholders('123')).to.be.equal('123');
    expect(replacePlaceholders('123', {})).to.be.equal('123');
    expect(replacePlaceholders('123', ['somedata'])).to.be.equal('123');
  });

  it('should replace placeholder', () => {
    expect(replacePlaceholders('this {{test}} will {{fail}}', {
      test: 'assertion',
      fail: 'pass'
    })).to.equal('this assertion will pass');

    expect(replacePlaceholders('{{0}} {{1}} {{2}}', [2, 1, 0])).to.equal('2 1 0');
  });
});