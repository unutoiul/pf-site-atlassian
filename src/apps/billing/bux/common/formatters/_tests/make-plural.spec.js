import { expect } from 'chai';
import makePlural from '../make-plural';

describe('make-plural', () => {
  describe('under normal circumstances 👍', () => {
    it('should pluralize a word', () => {
      const term = 'cat';
      const count = 2;
      expect(makePlural(term, count)).to.equal('cats');
    });

    it('should keep singulars', () => {
      const term = 'cat';
      const count = 1;
      expect(makePlural(term, count)).to.equal('cat');
    });

    it('should count negatives as well', () => {
      const term = 'cat';
      const count = -1;
      expect(makePlural(term, count)).to.equal('cat');

      const count2 = -12;
      expect(makePlural(term, count2)).to.equal('cats');
    });
  });

  describe('under ominous circumstances ☠️', () => {
    it('should throw an error if not a number', () => {
      const term = 'cat';
      const count = 'hax0r';
      expect(() => makePlural(term, count)).to.throw(Error, 'makePlural expects count to be a number but got: hax0r');
    });
  });
});
