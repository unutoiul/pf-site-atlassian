import { expect } from 'chai';
import sinon from 'sinon';
import {
  formatDateInterval,
  getTimeRemaining,
  isExpiryDateValid,
  isDateInThePast
} from '../date-formatter';

describe('date-formatter', () => {
  let clock;

  beforeEach(() => {
    clock = sinon.useFakeTimers(Date.UTC(2017, 1, 11, 23, 43, 0));
  });

  afterEach(() => {
    clock.restore();
  });

  it('should return standard date', () => {
    expect(formatDateInterval('20160910', '20161110')).to.equal('10 Nov 2016');
  });

  it('should return diff date', () => {
    expect(formatDateInterval('20161101', '20161111')).to.equal('in 10 days');
  });

  it('should return correct time to end date', () => {
    expect(getTimeRemaining('2017-02-18')).to.equal('7 days');
  });

  it('should return correct time to end date in the past', () => {
    expect(getTimeRemaining('2017-02-11')).to.equal('0 days');
  });

  describe('isDateInThePast', () => {
    it('should return false if invalid expiry date is given', () => {
      expect(isDateInThePast('NOT-A-DATE')).to.be.false();
    });

    it('should return false if the date is in the future', () => {
      expect(isDateInThePast('2017-02-20')).to.be.false();
    });

    it('should return false if the date is not in the past', () => {
      expect(isDateInThePast('2017-02-11')).to.be.false();
    });

    it('should return true if the date is in the past', () => {
      expect(isDateInThePast('2017-02-10')).to.be.true();
    });
  });

  describe('isExpiryDateValid', () => {
    it('should return false if invalid expiry date is given', () => {
      expect(isExpiryDateValid('TEST', 'EXPIRY')).to.be.false();
    });

    it('should return true if the month has not yet passed', () => {
      expect(isExpiryDateValid(2, 2018)).to.be.true();
    });

    it('should return true if in the current month', () => {
      expect(isExpiryDateValid(2, 2017)).to.be.true();
    });

    it('should return false if the month has passed', () => {
      expect(isExpiryDateValid(1, 2017)).to.be.false();
    });
  });
});
