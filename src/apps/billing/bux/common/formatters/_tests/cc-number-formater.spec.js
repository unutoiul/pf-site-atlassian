import { expect } from 'chai';
import { formatCCNumber, getSuffix } from '../cc-number-formatter';

describe('cc-number-formatter', () => {
  const cc = '\u2022\u2022\u2022\u2022 \u2022\u2022\u2022\u2022 \u2022\u2022\u2022\u2022 1234';
  const amexCc = '\u2022\u2022\u2022\u2022 \u2022\u2022\u2022\u2022\u2022\u2022 \u2022\u2022234';

  describe('formatCCNumber', () => {
    it('should throw if input is undefined or null', () => {
      expect(formatCCNumber()).to.equal('');
    });

    it('should return a formatted output when input is exactly 4 characters and type is undefined or not amex', () => {
      expect(formatCCNumber('1234')).to.equal(cc);
      expect(formatCCNumber('1234', 'TYPE')).to.equal(cc);
    });

    it('should return a formatted output when input is of type number', () => {
      expect(formatCCNumber(1234)).to.equal(cc);
      expect(formatCCNumber(234, 'AMEX')).to.equal(amexCc);
    });

    it('should return a formatted output when input is exactly 3 characters and type AMEX', () => {
      expect(formatCCNumber('234', 'AMEX')).to.equal(amexCc);
    });
  });
  describe('getSuffix', () => {
    it('should return the last 4 digits', () => {
      expect(getSuffix('012345xxxxxx6789')).to.be.equal('6789');
    });
  });
});
