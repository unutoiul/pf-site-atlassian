import moment from 'moment';

export const formatDate = (date, format = 'D MMM YYYY') => {
  if (date) {
    const momentDate = moment(date);
    if (momentDate.isValid()) {
      return momentDate.format(format);
    }
  }
  return '';
};

/**
 * Prints difference between dateA(start date), and dateB(target date).
 * @param dateA
 * @param dateB
 * @returns {String}
 */
export const formatDateInterval = (dateA, dateB) => {
  const momentA = moment(dateA);
  const momentB = moment(dateB);

  if (!momentA.diff(momentB, 'months')) {
    return momentA.to(momentB);
  }

  return formatDate(dateB);
};

export const getTimeRemaining = (date) => {
  const currentDate = moment.utc().startOf('day');
  const futureDate = moment.utc(date);
  if (futureDate.diff(currentDate, 'days') <= 0) {
    return '0 days';
  }
  return currentDate.to(futureDate, true);
};

export const padMonth = (month) => {
  if (!month) return '';
  return `${month}`.padStart(2, '0');
};

export const isExpiryDateValid = (month, year) =>
  moment(`${year}-${month}`, 'YYYY-MM').isSameOrAfter(moment(), 'month');

export const isDateInThePast = date => moment.utc(date).isBefore(moment.utc(), 'day');

export default {
  formatDate,
  formatDateInterval,
  getTimeRemaining,
};
