import config from '../config';

export function paymentDetailsMacUri(cloudId = null) {
  return cloudId === null ? config.macUrl : `${config.macPaymentDetailsUrl}/${cloudId}`;
}
