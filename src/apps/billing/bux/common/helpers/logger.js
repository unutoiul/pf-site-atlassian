import herment from '@atlassian/herment';
import nanoid from 'nanoid';
import config from '../config';

/**
 * This is the same as userType.ATLASSIAN_ACCOUNT in analytics-next
 * but since these analytics are deprecated let's not introduce a new dependency to between them.
 * @type {string}
 */
const USER_TYPE = 'atlassian_account';

/**
 * Queue for herment events
 * @type {Array}
 */
const hermentQueue = [];
let hermentInstance;

let isStarted = false;
let currentCloudId;
let currentUserId;

export const sessionId = nanoid();

const resetHermentInstance = (properties = {
  ...config.log,
  queue: hermentQueue
}) => {
  if (isStarted) {
    hermentInstance.destroy();
  }

  hermentInstance = herment(properties);

  isStarted = false;
};

function shouldRecreate(cloudId, userId) {
  if ((cloudId && cloudId !== currentCloudId) || (userId && userId !== currentUserId)) {
    return true;
  }
  return false;
}

/*
 * Why are we recreating the Herment instance?
 * CloudId and UserId are not available upon page load, as each new event comes in with those properties we'll update
 * the Herment client to properly report those fields.
 * This is expected to be in production temporarily until the analytics changes in this project documented at:
 * https://hello.atlassian.net/wiki/spaces/COMMERCE/pages/244211737/Frictionless+Tagging+and+Tracking
 * which are expected to land in January 2019.
 * Since the new changes should make this method of sending analytics redundant.
 * For the same reason we will not handle org-id for now.
 */
const recreateHermentWithProperties = ({ cloudId, userId }) => {
  if (!shouldRecreate(cloudId, userId)) {
    return;
  }

  const userCloudIdData = {
    cloud_id: currentCloudId,
    user_id: currentUserId,
  };
  if (cloudId) {
    userCloudIdData.cloud_id = cloudId;
    currentCloudId = cloudId;
  }
  if (userId) {
    userCloudIdData.user_id = userId;
    userCloudIdData.user_id_type = USER_TYPE;
    currentUserId = userId;
  }

  resetHermentInstance({
    ...config.log,
    ...userCloudIdData,
    queue: hermentQueue
  });
};

/**
 * Record new event
 * @param {String|Array} originalEvent Event name
 * @param [payload]
 */
const record = (originalEvent, payload = {}) => {
  if (process.env.BABEL_ENV === 'test') {
    // skip tests for node
    return;
  }

  const properties = typeof payload === 'object' ? payload : { content: payload };

  recreateHermentWithProperties(properties);

  if (!isStarted) {
    hermentInstance.start();
    isStarted = true;
  }

  const event = typeof originalEvent === 'string' ? [originalEvent] : originalEvent;
  const name = event.filter(e => !!e).join('.');

  properties.environment = config.environment;

  hermentQueue.push({
    name,
    properties: {
      ...properties,
      sessionId
    }
  });
};

const init = () => {
  resetHermentInstance();

  isStarted = false;
  currentCloudId = null;
  currentUserId = null;
};

const getHermentInstance = () => hermentInstance;

init();

export {
  record,
  getHermentInstance,
  init,
};
