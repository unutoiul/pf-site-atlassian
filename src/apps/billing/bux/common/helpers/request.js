import 'whatwg-fetch';
import { handleErrors, handleReject } from './request-handle';

const defaultFetchOptions = {
  mode: 'cors',
  credentials: 'include',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
};

function fetchAPI(url, options = {}) {
  return fetch(url, {
    ...defaultFetchOptions,
    ...options,
    headers: { ...defaultFetchOptions.headers, ...options.headers }
  })
    .then(handleErrors(url, options), handleReject);
}

export default {
  get: (url, options) => fetchAPI(url, { ...options, method: 'GET' }),
  post: (url, body, options) => fetchAPI(url, { ...options, body: JSON.stringify(body), method: 'POST' }),
  put: (url, body, options) => fetchAPI(url, { ...options, body: JSON.stringify(body), method: 'PUT' })
};
