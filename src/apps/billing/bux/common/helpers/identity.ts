function identityFn<T>(I: T): T {
  return I;
}

export default identityFn;
