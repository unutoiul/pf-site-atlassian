export const getItemFromSessionStorage = (key) => {
  if (typeof window !== 'undefined' && window.sessionStorage) {
    return window.sessionStorage.getItem(key);
  }
  return undefined;
};

export const setItemFromSessionStorage = (key, value) => {
  if (typeof window !== 'undefined' && window.sessionStorage) {
    return window.sessionStorage.setItem(key, value);
  }
  return undefined;
};
