import { getUserLocation } from 'bux/core/state/meta/selectors';

export const getAddress = (address, defaultLocation) => {
  if (!address.addressLine1 && defaultLocation) {
    return { country: defaultLocation.countryIsoCode, state: defaultLocation.regionIsoCode };
  }
  const {
    addressLine1 = '',
    addressLine2 = '',
    city = '',
    state = '',
    country = '',
    postcode = '',
  } = address;
  return {
    addressLine1, addressLine2, city, state, country, postcode 
  };
};

export const getAddressOrUserLocation = state => addressSelector =>
  (getAddress(addressSelector(state), getUserLocation(state)));

export const getFullAddress = (address) => {
  const {
    addressLine1 = '',
    addressLine2 = '',
    city = '',
    state = '',
    postcode = ''
  } = address;

  if (
    !addressLine1 ||
    !postcode ||
    !city
  ) {
    return '';
  }
  const lines = [
    addressLine1,
    addressLine2,
    postcode,
    city,
    state
  ];
  return lines.filter(line => !!line).join(', ');
};

export const constructFullAddress = state => addressSelector => getFullAddress(addressSelector(state));
