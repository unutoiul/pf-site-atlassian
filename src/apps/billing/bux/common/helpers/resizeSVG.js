import React from 'react';

const sizes = {
  medium: 30,
  large: 42,
  xlarge: 64
};

const resizeSVG = (SVG, size) => {
  if (typeof SVG === 'string') {
    // unit tests case
    return () => <div>{SVG}</div>;
  }
  if (!SVG.defaultProps) {
    return SVG;
  }
  const targetHeight = sizes[size] || (+size);
  const VB = SVG.defaultProps.viewBox.split(' ');
  const width = (+VB[2]);
  const height = (+VB[3]);
  const aspect = width / height;
  return props => <SVG {...props} height={targetHeight} width={aspect * targetHeight} />;
};

export default resizeSVG;