import { expect } from 'chai';
import { getHostname, getQueryParams, getQueryString } from '../browser';

describe('browser.js: ', () => {
  describe('getHostname: ', () => {
    it('should return nodejs as the hostname when window is undefined', () => (
      expect(getHostname(false)).to.equal('nodejs')
    ));

    it('should return nodejs as the window name is node js', () => (
      expect(getHostname({ name: 'nodejs' })).to.equal('nodejs')
    ));

    it('should return the hostname of the window when the window is defined', () => {
      const window = { location: { hostname: 'atlassian.com' } };
      expect(getHostname(window)).to.equal('atlassian.com');
    });
  });

  describe('getQueryString', () => {
    it('should return empty as the window name is node js', () => (
      expect(getQueryString({ name: 'nodejs' })).to.equal('')
    ));
    it('should return the query string of the window when the window is defined', () => {
      const window = { location: { search: '?org=123' } };
      expect(getQueryString(window)).to.equal('org=123');
    });
  });

  describe('getQueryParams: ', () => {
    it('should return org param from query string', () => {
      expect(getQueryParams('org=ABC'))
        .to.be.deep.equal({ org: 'ABC' });
    });
    it('should return org and test params from query string', () => {
      expect(getQueryParams('org=ABC&test=123'))
        .to.be.deep.equal({ org: 'ABC', test: '123' });
    });
    it('should return query params from encoded query string', () => {
      expect(getQueryParams('org%3DABC%26test%3D123'))
        .to.be.deep.equal({ org: 'ABC', test: '123' });
    });
    it('should return empty when query string is not set at the router state', () => {
      expect(getQueryParams('')).to.be.empty();
    });
  });
});
