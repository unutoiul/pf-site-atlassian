import { expect } from 'chai';
import sinon from 'sinon';
import proxyquire from 'bux/helpers/proxyquire';

describe.skip('monitoring: ', () => {
  it('should setup Bugsnag apiKey and environment', () => {
    const Bugsnag = {};
    const { setupMonitoring } = proxyquire.noCallThru().load('../monitoring', {
      'bugsnag-js': Bugsnag
    });

    setupMonitoring();

    expect(Bugsnag.apiKey).to.equal('bac52b0c95a0bf9d879281822046b1f1');
    expect(Bugsnag.releaseStage).to.equal('local'); //nodejs is local
  });

  it('should log a message', () => {
    const notify = sinon.stub().returns({});
    const { logMessage } = proxyquire.noCallThru().load('../monitoring', {
      'bugsnag-js': { notify }
    });

    logMessage('ErrorName', 'test');

    expect(notify).to.be.calledWith('ErrorName', 'test', undefined);
  });

  it('should log an exception', () => {
    const notifyException = sinon.stub().returns({});
    const { logException } = proxyquire.noCallThru().load('../monitoring', {
      'bugsnag-js': { notifyException }
    });

    logException('error');

    expect(notifyException).to.be.calledWith('error', undefined);
  });
});
