import { expect } from 'chai';
import { i18n } from 'bux/helpers/jsLingui';
import { compose, createValidator } from '../validation';

describe('validation.js: ', () => {
  describe('createValidator: ', () => {
    it('should allow a field to be required', () => {
      const validator = createValidator([{ address: 'required' }]);
      expect(validator({ address: '' }, { i18n })).to.deep.equal({
        address: ['This field is required.']
      });
    });

    it('should allow a field to be email-like', () => {
      const validator = createValidator([{ email_field: 'email' }]);
      expect(validator({ email_field: 'incorrect@email' }, { i18n })).to.deep.equal({
        email_field: ['Please enter a valid email address.']
      });
    });

    it('should return errors as a nested object', () => {
      const validator = createValidator([{ address: { street: 'required' } }]);
      expect(validator({}, { i18n })).to.deep.equal({
        address: {
          street: ['This field is required.']
        }
      });
    });
  });

  describe('compose: ', () => {
    it('should return a validator function when given a function', () => {
      const childValidator = createValidator([{ address: 'required' }]);
      const validator = compose(childValidator);
      expect(validator({ address: '' }, { i18n })).to.deep.equal({
        address: ['This field is required.']
      });
    });

    it('should return a validator function when given an array of functions', () => {
      const addressValidator = createValidator([{ address: 'required' }]);
      const nameValidator = createValidator([{ name: 'required' }]);
      const validator = compose([addressValidator, nameValidator]);
      expect(validator({ address: '' }, { i18n })).to.deep.equal({
        name: ['This field is required.'],
        address: ['This field is required.']
      });
    });

    it('should return a validator function when given an object of functions', () => {
      const addressValidator = createValidator([{ address: 'required' }]);
      const nameValidator = createValidator([{ name: 'required' }]);
      const validator = compose({ left: addressValidator, right: nameValidator });
      expect(validator({
        left: { address: '' },
        right: { name: '' }
      }, { i18n })).to.deep.equal({
        left: { address: ['This field is required.'] },
        right: { name: ['This field is required.'] }
      });
    });

    it('should return a validator function when given an array of objects', () => {
      const addressValidator = createValidator([{ address: 'required' }]);
      const nameValidator = createValidator([{ name: 'required' }]);
      const validator = compose([{ left: addressValidator }, { right: nameValidator }]);
      expect(validator({
        left: { address: '' },
        right: { name: '' }
      }, { i18n })).to.deep.equal({
        left: { address: ['This field is required.'] },
        right: { name: ['This field is required.'] }
      });
    });
  });
});
