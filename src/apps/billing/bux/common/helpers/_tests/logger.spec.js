import sinon from 'sinon';
import { expect } from 'chai';
import { getHermentInstance, record, sessionId, init } from '../logger';

describe('Logger: ', () => {
  describe('Herment: ', () => {
    const defaultProperties = {
      environment: 'local',
      sessionId
    };
    let stored;

    beforeEach(() => {
      stored = process.env.BABEL_ENV;
      process.env.BABEL_ENV = 'not-test';
      init();
    });

    afterEach(() => {
      process.env.BABEL_ENV = stored;
      getHermentInstance().destroy();
    });

    it('should log event', () => {
      const logCallback = sinon.stub();
      record('event');
      getHermentInstance().publishFromQueueAndStorage(logCallback);

      expect(logCallback).to.have.been.called();
    });

    it('should correctly name events', () => {
      let events = [];
      const logCallback = (hermentEvent) => {
        events = hermentEvent;
      };
      record('event1');
      record(['event1'], 'payload');
      record(['event1', 'event2']);
      record(['event1', null, 'event2']);
      record(['event1'], { complexPayload: '42' });

      getHermentInstance().publishFromQueueAndStorage(logCallback);

      expect(events).to.have.length(5);
      expect(events[0].name).to.be.equal('event1');
      expect(events[1].name).to.be.equal('event1');
      expect(events[2].name).to.be.equal('event1.event2');
      expect(events[3].name).to.be.equal('event1.event2');

      expect(events[0].properties).to.be.deep.equal(defaultProperties);
      expect(events[1].properties).to.be.deep.equal({ ...defaultProperties, content: 'payload' });
      expect(events[4].properties).to.be.deep.equal({ ...defaultProperties, complexPayload: '42' });
    });

    it('should set cloud-id on herment client if an event contains cloudId', () => {
      let events = [];
      const logCallback = (hermentEvent) => {
        events = hermentEvent;
      };

      const EXPECTED_CLOUD_ID = 'a-cloud-id';
      const propertiesWithoutCloudId = {
        otherProperties: 'other'
      };
      const propertiesWithCloudId = {
        cloudId: EXPECTED_CLOUD_ID
      };

      record('event1', propertiesWithoutCloudId);
      getHermentInstance().publishFromQueueAndStorage(logCallback);

      expect(events).to.have.length(1);
      expect(events[0].properties).to.be.deep.equal({ ...defaultProperties, ...propertiesWithoutCloudId });
      expect(events[0].cloud_id).to.be.null();

      record('event2', propertiesWithCloudId);
      record('event3', propertiesWithoutCloudId);
      getHermentInstance().publishFromQueueAndStorage(logCallback);

      expect(events).to.have.length(2);
      expect(events[0].properties).to.be.deep.equal({ ...defaultProperties, ...propertiesWithCloudId });
      expect(events[0].cloud_id).to.be.equal(EXPECTED_CLOUD_ID);

      expect(events[1].properties).to.be.deep.equal({ ...defaultProperties, ...propertiesWithoutCloudId });
      expect(events[1].cloud_id).to.be.equal(EXPECTED_CLOUD_ID);
    });

    it('should update cloud-id if it changes between events', () => {
      let events = [];
      const logCallback = (hermentEvent) => {
        events = hermentEvent;
      };

      const EXPECTED_CLOUD_ID_1 = 'cloud-id-1';
      const EXPECTED_CLOUD_ID_2 = 'cloud-id-2';
      const propertiesWithCloudId1 = {
        cloudId: EXPECTED_CLOUD_ID_1,
        userId: 'a-user-id',
      };
      const propertiesWithCloudId2 = {
        cloudId: EXPECTED_CLOUD_ID_2,
        userId: 'a-user-id',
      };

      record('event1', propertiesWithCloudId1);
      getHermentInstance().publishFromQueueAndStorage(logCallback);

      expect(events).to.have.length(1);
      expect(events[0].properties).to.be.deep.equal({ ...defaultProperties, ...propertiesWithCloudId1 });
      expect(events[0].cloud_id).to.be.equal(EXPECTED_CLOUD_ID_1);

      record('event2', propertiesWithCloudId2);
      getHermentInstance().publishFromQueueAndStorage(logCallback);

      expect(events).to.have.length(1);
      expect(events[0].properties).to.be.deep.equal({ ...defaultProperties, ...propertiesWithCloudId2 });
      expect(events[0].cloud_id).to.be.equal(EXPECTED_CLOUD_ID_2);
    });

    it('should add a user-id if present in an event', () => {
      let events = [];
      const logCallback = (hermentEvent) => {
        events = hermentEvent;
      };

      const EXPECTED_USER_ID = 'atlassian-account-id';
      const propertiesWithoutUserId = {
        otherProperties: 'other'
      };
      const propertiesWithUserId = {
        userId: EXPECTED_USER_ID
      };

      record('event1', propertiesWithoutUserId);
      getHermentInstance().publishFromQueueAndStorage(logCallback);

      expect(events).to.have.length(1);
      expect(events[0].properties).to.be.deep.equal({ ...defaultProperties, ...propertiesWithoutUserId });
      expect(events[0].user_id).to.be.null();

      record('event2', propertiesWithUserId);
      record('event3', propertiesWithoutUserId);
      getHermentInstance().publishFromQueueAndStorage(logCallback);

      expect(events).to.have.length(2);
      expect(events[0].properties).to.be.deep.equal({ ...defaultProperties, ...propertiesWithUserId });
      expect(events[0].user_id).to.be.equal(EXPECTED_USER_ID);

      expect(events[1].properties).to.be.deep.equal({ ...defaultProperties, ...propertiesWithoutUserId });
      expect(events[1].user_id).to.be.equal(EXPECTED_USER_ID);
    });

    it('should update user-id if it changes between events', () => {
      let events = [];
      const logCallback = (hermentEvent) => {
        events = hermentEvent;
      };

      const EXPECTED_USER_ID_1 = 'atlassian-account-id';
      const EXPECTED_USER_ID_2 = 'atlassian-account-id-2';
      const propertiesWithUserId1 = {
        cloudId: 'a-cloud-id',
        userId: EXPECTED_USER_ID_1,
      };
      const propertiesWithUserId2 = {
        cloudId: 'a-cloud-id',
        userId: EXPECTED_USER_ID_2,
      };

      record('event1', propertiesWithUserId1);
      getHermentInstance().publishFromQueueAndStorage(logCallback);

      expect(events).to.have.length(1);
      expect(events[0].properties).to.be.deep.equal({ ...defaultProperties, ...propertiesWithUserId1 });
      expect(events[0].user_id).to.be.equal(EXPECTED_USER_ID_1);

      record('event2', propertiesWithUserId2);
      getHermentInstance().publishFromQueueAndStorage(logCallback);

      expect(events).to.have.length(1);
      expect(events[0].properties).to.be.deep.equal({ ...defaultProperties, ...propertiesWithUserId2 });
      expect(events[0].user_id).to.be.equal(EXPECTED_USER_ID_2);
    });

    it('should add both user-id and cloud-id when they appear in events', () => {
      let events = [];
      const logCallback = (hermentEvent) => {
        events = hermentEvent;
      };

      const EXPECTED_USER_ID = 'atlassian-account-id';
      const EXPECTED_CLOUD_ID = 'a-cloud-id';
      const propertiesWithUserId = {
        userId: EXPECTED_USER_ID
      };
      const propertiesWithCloudId = {
        cloudId: EXPECTED_CLOUD_ID
      };

      record('event1', propertiesWithUserId);
      getHermentInstance().publishFromQueueAndStorage(logCallback);

      expect(events).to.have.length(1);
      expect(events[0].properties).to.be.deep.equal({ ...defaultProperties, ...propertiesWithUserId });
      expect(events[0].user_id).to.be.equal(EXPECTED_USER_ID);
      expect(events[0].cloud_id).to.be.null();

      record('event2', propertiesWithCloudId);
      record('event3');
      getHermentInstance().publishFromQueueAndStorage(logCallback);

      expect(events).to.have.length(2);
      expect(events[0].properties).to.be.deep.equal({ ...defaultProperties, ...propertiesWithCloudId });
      expect(events[0].user_id).to.be.equal(EXPECTED_USER_ID);
      expect(events[0].cloud_id).to.be.equal(EXPECTED_CLOUD_ID);

      expect(events[1].properties).to.be.deep.equal(defaultProperties);
      expect(events[1].user_id).to.be.equal(EXPECTED_USER_ID);
      expect(events[1].cloud_id).to.be.equal(EXPECTED_CLOUD_ID);
    });
  });
});
