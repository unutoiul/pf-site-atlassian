import { expect } from 'chai';

import { getAddress, getFullAddress, constructFullAddress } from '../address';

describe('AddressHelper: ', () => {
  it('should return valid address', () => {
    const addressInput = {
      organisationName: 'Potato Corp.',
      addressLine1: '341 George Street',
      addressLine2: 'Level 6',
      city: 'Sydney',
      state: 'NSW',
      postcode: '2000',
      country: 'AU',
      ignoreThisField: 'test',
      taxpayerId: '123456782',
    };

    const result = getAddress(addressInput);

    expect(result).to.deep.equal({
      addressLine1: '341 George Street',
      addressLine2: 'Level 6',
      city: 'Sydney',
      state: 'NSW',
      postcode: '2000',
      country: 'AU',
    });
  });

  it('should use location', () => {
    const addressInput = {};
    expect(getAddress(addressInput)).to.deep.equal({
      addressLine1: '',
      addressLine2: '',
      city: '',
      state: '',
      postcode: '',
      country: ''
    });

    const location = { countryIsoCode: 'AU', regionIsoCode: 'NSW' };
    expect(getAddress(addressInput, location)).to.deep.equal({
      state: 'NSW',
      country: 'AU'
    });
  });

  it('should return full address', () => {
    const addressInput = {
      organisationName: 'Potato Corp.',
      addressLine1: '341 George Street',
      addressLine2: 'Level 6',
      city: 'Sydney',
      state: 'NSW',
      postcode: '2000',
      country: 'AU',
      ignoreThisField: 'test',
      taxpayerId: '123456782',
    };

    expect(getFullAddress(addressInput)).to.be.equal('341 George Street, Level 6, 2000, Sydney, NSW');
  });

  it('should return empty addr when some components are missing', () => {
    const addressInput = {
      organisationName: 'Potato Corp.',
      //addressLine1: '341 George Street',
      addressLine2: 'Level 6',
      city: 'Sydney',
      state: 'NSW',
      postcode: '2000',
      country: 'AU',
      ignoreThisField: 'test',
      taxpayerId: '123456782',
    };

    expect(getFullAddress(addressInput)).to.be.equal('');
  });

  it('should keep consistency', () => {
    const addressInput = {
      organisationName: 'Potato Corp.',
      addressLine1: '341 George Street',
      addressLine2: 'Level 6',
      city: 'Sydney',
      state: 'NSW',
      postcode: '2000',
      country: 'AU',
      ignoreThisField: 'test',
      taxpayerId: '123456782',
    };

    expect(getFullAddress(addressInput)).to.be.equal(constructFullAddress(addressInput)(x => x));
  });
});
