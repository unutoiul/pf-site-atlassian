import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import resizeSVG from '../resizeSVG';

describe('resizeSVG', () => {
  it('should do handle string-based SVG', () => {
    const Resized = resizeSVG('this is SVG', 'medium');
    expect(shallow(<Resized />)).to.contain.text('this is SVG');
  });

  it('should resize SVG', () => {
    const SVG = () => <svg viewBox="0 0 100 50" />;
    SVG.defaultProps = {
      viewBox: '0 0 100 50'
    };
    const Resized = resizeSVG(SVG, 'medium');
    expect(shallow(<Resized />).props()).to.be.deep.equal({
      width: 60,
      height: 30,
      viewBox: '0 0 100 50'
    });
  });
});