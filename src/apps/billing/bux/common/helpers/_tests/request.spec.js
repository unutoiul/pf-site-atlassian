import { expect } from 'chai';
import fetchMock from 'fetch-mock-5';
import request from '../request';

describe('request.js: ', () => {
  beforeEach(() => {
    fetchMock.restore();
  });

  describe('for GET', () => {
    it('should allow options override to defaults', () => {
      fetchMock.get('some-url', {});

      return request.get('some-url', { headers: { something: 'else' } }).then(() => {
        const actualRequest = fetchMock.lastOptions('some-url');
        expect(actualRequest.mode).to.equal('cors');
        expect(actualRequest.headers).to.deep.equal({
          Accept: 'application/json',
          'Content-Type': 'application/json',
          something: 'else'
        });
      });
    });
  });

  describe('for POST', () => {
    it('should allow options override to defaults', () => {
      fetchMock.post('some-url', {});

      return request.post('some-url', { name: 'body' }, { headers: { something: 'else' } }).then(() => {
        const actualRequest = fetchMock.lastOptions('some-url');
        expect(actualRequest.mode).to.equal('cors');
        expect(actualRequest.headers).to.deep.equal({
          Accept: 'application/json',
          'Content-Type': 'application/json',
          something: 'else'
        });
      });
    });

    it('should stringify body to json string', () => {
      fetchMock.post('some-url', {});

      return request.post('some-url', { name: 'body' }).then(() => {
        const actualRequest = fetchMock.lastOptions('some-url');
        expect(actualRequest.body).to.deep.equal('{"name":"body"}');
      });
    });
  });

  describe('for PUT', () => {
    it('should allow options override to defaults', () => {
      fetchMock.put('some-url', {});

      return request.put('some-url', { name: 'body' }, { headers: { something: 'else' } }).then(() => {
        const actualRequest = fetchMock.lastOptions('some-url');
        expect(actualRequest.mode).to.equal('cors');
        expect(actualRequest.headers).to.deep.equal({
          Accept: 'application/json',
          'Content-Type': 'application/json',
          something: 'else'
        });
      });
    });

    it('should stringify body to json string', () => {
      fetchMock.put('some-url', {});

      return request.put('some-url', { name: 'body' }).then(() => {
        const actualRequest = fetchMock.lastOptions('some-url');
        expect(actualRequest.body).to.equal('{"name":"body"}');
      });
    });
  });

  it('should throw error when service returns non 2xx', () => {
    fetchMock.get('some-url', 400);

    return request.get('some-url')
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.status).to.equal(400);
      });
  });

  it('should return the correct response when request is successful', () => {
    fetchMock.get('some-url', { response: 'body' });

    return request.get('some-url')
      .then(response => response.json())
      .then((responseJson) => {
        expect(responseJson).to.deep.equal({ response: 'body' });
      });
  });

  it('should return error body when error response has payload', () => {
    const errorBody = { message: 'We have a problem', errorKey: 'big-problem' };
    fetchMock.get('some-url', { status: 400, body: errorBody });

    return request.get('some-url')
      .then(() => Promise.reject())
      .catch((error) => {
        expect(error.response.status).to.equal(400);
        expect(error.responseData).to.deep.equal(errorBody);
      });
  });
});
