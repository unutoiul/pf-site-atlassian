export const keys = obj => Object.keys(obj);

export const capitalize = word => `${word}`.replace(/^./, c => c.toUpperCase());

export const getDisplayName = WrappedComponent => (
  WrappedComponent.displayName || WrappedComponent.name || 'Component'
);