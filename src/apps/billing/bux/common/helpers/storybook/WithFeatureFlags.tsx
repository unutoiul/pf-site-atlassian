import * as React from 'react';
import { connect } from 'react-redux';

import FeatureFlagProvider from 'bux/components/FeatureFlags/FeatureFlagProvider';
import { getFeatureFlags } from 'bux/core/state/meta/selectors';

interface WithFeatureFlagsProps {
  featureFlags: any;
}

const WithFeatureFlags: React.SFC<WithFeatureFlagsProps> = ({ children, featureFlags }) => (
  <FeatureFlagProvider featureFlags={featureFlags}>
    {children}
  </FeatureFlagProvider>
);

const mapStateToProps = state => ({
  featureFlags: getFeatureFlags(state),
});

export default connect(mapStateToProps)(WithFeatureFlags);
