import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const GridBase = styled.section`
  width: 600px;

  padding:16px;

  font-family: -apple-system, ".SFNSText-Regular", Arial, sans-serif;
  font-size: 14px;
  line-height: 18px;

  display: grid;
  grid-template-columns: 50% 50%;
  grid-template-rows: 100%;
  grid-gap: 3.3%;
`;


export class LanguagePanel extends React.Component {
  state = {
    dualView: false,
    animated: false,
    language: ['en', 'en'],
    languages: [],
    inited: false,
  };

  componentDidMount() {
    const { channel } = this.props;

    channel.on('atlassian/language-control/mount', this.setLanguages);
  }

  componentDidUpdate(oldProp, oldState) {
    this.props.channel.emit('atlassian/language-control/update', this.state);

    if (this.state.animated !== oldState.animated) {
      if (this.state.animated) {
        this.animationTimeout = setInterval(() => {
          const { language, languages } = this.state;
          const unwrapped = [...languages, ...languages];
          const nextLang = unwrapped[unwrapped.indexOf(language[0]) + 1];
          this.setState({
            language: [nextLang, language[1]]
          });
        }, 1000);
      } else {
        clearInterval(this.animationTimeout);
      }
    }
  }

  componentWillUnmount() {
    const { channel } = this.props;
    channel.removeListener('atlassian/language-control/mount', this.setLanguages);
  }

  setLanguages = ({ jsLinguiCatalogs, intlMessages, language }) => {
    this.setState({
      languages: Object.keys({
        ...jsLinguiCatalogs,
        ...intlMessages
      })
    });
    if (!this.state.inited) {
      this.setState({ inited: true, language: [language, language] });
    }
  };

  animationTimeout = 0;

  renderLanguageSelect(selected, onChange) {
    return (
      <select onChange={onChange} value={selected}>
        {
          this.state.languages.map(lng => <option key={lng} value={lng}>{lng}</option>)
        }
      </select>
    );
  }

  render() {
    const { dualView, language, animated } = this.state;

    return (
      <GridBase>
        <div>
          Main view
          <hr />
          {
            this.renderLanguageSelect(
              language[0],
              event => this.setState({ language: [event.target.value, language[1]] })
            )
          }
          <hr />
          <label htmlFor="animated-l1">animated</label>
          <input
            id="animated-l1"
            type="checkbox"
            checked={animated}
            onChange={() => this.setState({ animated: !animated })}
          />
        </div>
        <div>
          Dual view
          <input type="checkbox" checked={dualView} onChange={() => this.setState({ dualView: !dualView })} />
          <hr />
          {
            this.renderLanguageSelect(
              language[1],
              event => this.setState({ language: [language[0], event.target.value] })
            )
          }
        </div>
      </GridBase>
    );
  }
}

LanguagePanel.propTypes = {
  channel: PropTypes.shape({
    emit: PropTypes.func,
    on: PropTypes.func,
    removeListener: PropTypes.func,
  }).isRequired,
  api: PropTypes.shape({
    onStory: PropTypes.func,
    getQueryParam: PropTypes.func,
    setQueryParams: PropTypes.func,
  }).isRequired,
};
