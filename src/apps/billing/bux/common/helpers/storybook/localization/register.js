import React from 'react';
import addons from '@storybook/addons';

import { LanguagePanel } from './components/Panel';

// Register the addon with a unique name.
addons.register('atlassian/language-control', (api) => {
  // Also need to set a unique name to the panel.
  addons.addPanel('atlassian/language-control/panel', {
    title: 'Language',
    render: () => (
      <LanguagePanel channel={addons.getChannel()} api={api} />
    ),
  });
});
