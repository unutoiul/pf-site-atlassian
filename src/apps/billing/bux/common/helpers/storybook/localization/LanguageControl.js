import React from 'react';
import PropTypes from 'prop-types';
import addons from '@storybook/addons';
import { I18nProvider } from '@lingui/react';
import { IntlProvider } from 'react-intl';
import styled from 'styled-components';

const GridBase = styled.section`
  display: grid;
  grid-template-columns: 50% 50%;
  grid-template-rows: 100%;
  grid-gap: 1px;
  
  background-color: #EEE;
  & > div {
    background-color: #FFF;
  }
`;


export class WithLanguageControl extends React.Component {
  static propTypes = {
    children: PropTypes.element.isRequired,
    language: PropTypes.string.isRequired,
    jsLinguiCatalogs: PropTypes.any,
    intlMessages: PropTypes.any,
  };

  state = {
    language: [this.props.language, this.props.language]
  };

  componentDidMount() {
    this.channel.emit('atlassian/language-control/mount', this.props);
    this.channel.on('atlassian/language-control/update', this.update);
  }

  componentWillUnmount() {
    this.channel.removeListener('atlassian/scenarios/update', this.update);
  }

  channel = addons.getChannel();

  update = (options) => {
    this.setState(options);
  };

  renderProvider(language) {
    const { jsLinguiCatalogs, intlMessages, children } = this.props;
    window.globalLanguage = language;
    return (
      <I18nProvider language={language} catalogs={jsLinguiCatalogs}>
        <IntlProvider locale={language} messages={intlMessages[language]}>
          {React.cloneElement(children, { language })}
        </IntlProvider>
      </I18nProvider>
    );
  }

  render() {
    const { language } = this.state;

    if (this.state.dualView) {
      return (
        <GridBase>
          <div>{this.renderProvider(language[0])}</div>
          <div>{this.renderProvider(language[1])}</div>
        </GridBase>
      );
    } 
    return this.renderProvider(language[0]);
  }
}
