import * as React from 'react';
import { connect } from 'react-redux';

import meta from 'bux/core/state/meta';

interface AsOrganizationProps {
  updateSiteState(payload: any);
  setUserMetadata(payload: any);
}

class AsOrganization extends React.Component<AsOrganizationProps> {
  public componentDidMount() {
    this.props.updateSiteState({ organisationId: '123' });
    this.props.setUserMetadata({ user: { organizations: [{ id: '123', name: 'Acme' }] } });
  }

  public render() {
    return <React.Fragment>{this.props.children}</React.Fragment>;
  }
}

const mapDispatchToProps = dispatch => ({
  updateSiteState: (payload) => dispatch(meta.actions.updateSiteState(payload)),
  setUserMetadata: (payload) => dispatch(meta.actions.setUserMetadata(payload)),
});

export default connect(null, mapDispatchToProps)(AsOrganization);
