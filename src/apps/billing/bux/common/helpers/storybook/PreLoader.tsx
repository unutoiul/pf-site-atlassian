import * as React from 'react';
import { connect } from 'react-redux';

import Loading from 'bux/components/Loading/delayed';
import { combineAllDisplayable } from 'bux/core/selectors/metadata';
import { BUXMetaDataType } from 'bux/core/state/_helpers/metadata/types';
import billEstimate from 'bux/core/state/bill-estimate';
import billHistory from 'bux/core/state/bill-history';
import billingDetails from 'bux/core/state/billing-details';
import entitlementGroup from 'bux/core/state/entitlement-group';
import meta from 'bux/core/state/meta';

interface PreLoaderProps {
  metadataStats: BUXMetaDataType;
  loader();
}

class PreLoader extends React.PureComponent<PreLoaderProps> {
  public componentDidMount() {
    this.props.loader();
  }

  public render() {
    if (this.props.metadataStats.loading) {
      return <Loading />;
    }

    return <React.Fragment>{this.props.children}</React.Fragment>;
  }
}

const mapStateToProps = state => ({
  metadataStats: combineAllDisplayable([
    entitlementGroup.selectors.getEntitlementGroupMetadata,
    entitlementGroup.selectors.getEntitlementEditionMetadata('b7db7827-e2ac-492e-a3bb-e1c310764043'),
    billEstimate.selectors.getBillEstimateMetadata,
    billHistory.selectors.getInvoicesMetadata,
    billingDetails.selectors.getBillingDetailsMetadata,
    meta.selectors.getUserLocationMetadata,
    meta.selectors.getCountryListMetadata,
  ])(state),
});

const mapDispatchToProps = dispatch => ({
  loader: () => {
    dispatch(entitlementGroup.actions.getEntitlementGroup());
    dispatch(entitlementGroup.actions.getEntitlementEdition('b7db7827-e2ac-492e-a3bb-e1c310764043'));
    dispatch(billEstimate.actions.getBillEstimate());
    dispatch(billHistory.actions.getBillHistory());
    dispatch(billingDetails.actions.getBillingDetails());
    dispatch(meta.actions.getUserMetadata());
    dispatch(meta.actions.getCountryList());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(PreLoader);
