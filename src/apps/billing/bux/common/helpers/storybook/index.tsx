import { boolean, number, object, text } from '@storybook/addon-knobs';
import * as PropTypes from 'prop-types';
import * as React from 'react';

// tslint:disable-next-line:no-import-side-effect
import '@atlaskit/css-reset';
// tslint:disable-next-line:no-import-side-effect
import '@atlaskit/reduced-ui-pack';

import { configureApolloClientWithUserFeatureFlag } from 'common/feature-flags/test-utils';

import { WithScenarios } from 'bux/common/helpers/storybook/scenarios-addon';
import FeatureFlagProvider from 'bux/components/FeatureFlags/FeatureFlagProvider';
import { Provider } from 'bux/helpers/redux';

import { createApolloClient } from '../../../../../../apollo-client';
import { BUXRootLogger } from '../../../../adapters/Logger';
import AsOrganization from './AsOrganization';
import AsSite from './AsSite';
import PreLoader from './PreLoader';
import WithFeatureFlags from './WithFeatureFlags';

const initialKnobToPropTypeMappings = new Map();
initialKnobToPropTypeMappings.set(PropTypes.bool, boolean);
initialKnobToPropTypeMappings.set(PropTypes.number, number);
initialKnobToPropTypeMappings.set(PropTypes.string, text);
initialKnobToPropTypeMappings.set(PropTypes.object, object);

const knobToPropTypeMappings = new Map();
initialKnobToPropTypeMappings.forEach((value, key) => {
  knobToPropTypeMappings.set(key, value);
  knobToPropTypeMappings.set(key.isRequired, value);
});

function propTypeToKnob(propType) {
  return knobToPropTypeMappings.get(propType);
}

function knobbedProps(propTypes, values) {
  const propKeys = Object.keys(propTypes);

  const newProps = { ...values };

  propKeys.forEach((propKey) => {
    const value = values[propKey];
    const control = propTypeToKnob(propTypes[propKey]) || (typeof value === 'object' && object);
    if (control) {
      newProps[propKey] = control(propKey, value);
    }
  });

  return newProps;
}

interface ReactComponentProps {
  propTypes: object;
  defaultProps: object;
}

interface TestingFrameProps {
  defaultProps: object;
  Component: typeof React.Component & ReactComponentProps;
}

const TestingFrame: React.SFC<TestingFrameProps> = ({ Component, defaultProps }) =>
  <Component {...knobbedProps(Component.propTypes, { ...Component.defaultProps, ...defaultProps })} />;

export const inStoryKnob = (WrappedComponent, defaultProps) =>
  <TestingFrame Component={WrappedComponent} defaultProps={defaultProps} />;

export const render = (SubComponent, props = {}) => <SubComponent {...props} />;

const LayoutFrame = ({ children, width }) =>
  <div style={{ padding: '20px' }}><div style={{ width, margin: '0px auto' }}>{children}</div></div>;

export const Layout = {
  Page: ({ children }) => <LayoutFrame width="920px">{children}</LayoutFrame>,
  Content: ({ children }) => <LayoutFrame width="600px">{children}</LayoutFrame>,
  StepFlow: ({ children }) => <LayoutFrame width="470px">{children}</LayoutFrame>,
};

export const inLayout = (LayoutType, Render, flag = '') => (
    <Provider
      client={
      configureApolloClientWithUserFeatureFlag({
        client: createApolloClient(),
        flagKey: flag,
        flagValue: true,
      })}
    >
      <WithFeatureFlags>
        <BUXRootLogger>
          <LayoutType>{Render}</LayoutType>
        </BUXRootLogger>
      </WithFeatureFlags>
    </Provider>
);

export const withUserFeatureFlag = (flag, Render) => inLayout(Layout.Page, Render, flag);
export const inPageChrome = Render => inLayout(Layout.Page, Render);
export const inContentChrome = Render => inLayout(Layout.Content, Render);
export const inStepFlowChrome = Render => inLayout(Layout.StepFlow, Render);

export const inScenarios = (scenarios, Render) => <WithScenarios scenarios={scenarios}>{Render}</WithScenarios>;

export const withServicePreLoader = (Render) => <PreLoader>{Render}</PreLoader>;
export const asOrganization = (Render) => <AsOrganization>{Render}</AsOrganization>;
export const asSite = (Render) => <AsSite>{Render}</AsSite>;

export const withFeatureFlagsChrome = (flags, Render) => (
  <FeatureFlagProvider featureFlags={flags}>{Render}</FeatureFlagProvider>
);

export const mockedFunction = () => (0);
