let overlays: HTMLDivElement;
let lastMessages = null;
let collectY = 0;
let cycleMock = () => {
  return;
};

const displayOverlays = (target, messageMap) => {
  target.innerHTML = '';
  if (!messageMap || !messageMap.length) {
    return;
  }
  const wrapper = document.querySelector('.whales-story-shooting-target');
  const scope = wrapper ? wrapper.getBoundingClientRect() : { left: 0, top: 0 };
  messageMap.forEach(element => {
    const source = element.key.indexOf('billing') === 0
      ? 'i18n-en-strings-billing'
      : 'i18n-en-strings';
    const d = document.createElement('div');
    d.className = 'whales-translation';
    d.onclick = () => {
      window.open(
        `https://www.transifex.com/atlassian/atlassian-admin-hub/translate/#${(window as any).globalLanguage}/${source}?q=key%3A${encodeURIComponent(element.key)}`,
        'trans',
      );
    };
    const nodeCoords = element.node ? element.node.getBoundingClientRect() : {};
    let coordinates = { ...element.coordinates };
    if (nodeCoords.x) {
      coordinates = nodeCoords;
      coordinates.y += window.scrollY;
    } else {
      coordinates.x += scope.left;
      coordinates.y += scope.top + collectY;
    }
    d.style.left = `${coordinates.x}px`;
    d.style.top = `${coordinates.y}px`;
    d.style.width = `${coordinates.width}px`;
    d.style.height = `${coordinates.height}px`;
    d.title = element.key;
    target.appendChild(d);
  });
};

const injectSideEffect = () => {
  if (overlays) {
    return overlays;
  }
  const styleDef = document.createElement('style');
  styleDef.type = 'text/css';
  styleDef.appendChild(document.createTextNode(`
    .whales-translation {
      position: absolute;
      display: none;
      z-index: 1000;
      font-size: 8px;
      overflow: hidden;
      cursor: pointer;
    }

    .whales-show-translation .whales-translation {
     display: block;
     box-shadow: 0 0 1px 0px #000;
     background-color:rgba(255,255,100,0.05);
  `));
  document.body.appendChild(styleDef);

  overlays = document.createElement('div');
  document.body.appendChild(overlays);

  window.addEventListener('keydown', event => {
    if (event.code !== 'AltLeft') {
      return;
    }

    document.body.classList.add('whales-show-translation');
    collectY = window.scrollY;
    cycleMock();
  });
  window.addEventListener('keyup', event => {
    if (event.code !== 'AltLeft') {
      return;
    }

    document.body.classList.remove('whales-show-translation');
  });

  return overlays;
};

export const processTranslations = ({ messageMap }, { redraw, mock, cycle }) => {
  cycleMock = () => {
    mock();
    redraw();
    setTimeout(cycle, 16);
  };
  const target = injectSideEffect();
  target.innerHTML = '';
  lastMessages = messageMap;
  displayOverlays(overlays, lastMessages);
};
