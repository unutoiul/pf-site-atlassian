import { I18n } from '@lingui/react';
import { FormattedMessage } from 'react-intl';
import { createElement, remock } from 'react-remock';

let keys = {};
let values = {};
let valueMap = {};

// invisible magic markers
const START = '\uFEFF\u200B';
const END = '\u200B\uFEFF';
const REPEAT = '\u200B';

// wraps result of i18n.t with markers
const wrapI18n = (key, value) => {
  keys[key] = value;
  if (!values[value]) {
    values[value] = {};
  }
  if (!(key in values[value])) {
    values[value][key] = Object.keys(values[value]).length;
  }

  // in case of duplicate string value used for different key - add "repeat" marker.
  const resultValue = `${REPEAT.repeat(values[value][key])}${value}`;
  valueMap[resultValue] = key;

  return `${START}${resultValue}${END}`;
};

let jsLinguiMock: null | (() => void);
let intlMock: null | (() => void);

export const setupI18n = () => {
  jsLinguiMock = remock.mock(I18n, (type, props, children) => (
    createElement(type, props,
      (api) => (
        children![0]({
          ...api, i18n: {
            ...api.i18n,
            language: api.i18n.language,
            _: (key, params, options) => {
              return wrapI18n(key, api.i18n._(key, params, options));
            },
          },
        })
      ))
  ));

  intlMock = remock.match(
    (type: any, props: any) => type !== FormattedMessage && (props.intl && props.intl.formatMessage),
    (type, props, children: any[]) => (
      createElement(type, {
        ...props,
        intl: {
          ...props.intl,
          formatMessage: (descriptor: any) => {
            return wrapI18n(descriptor.id, props.intl.formatMessage(descriptor));
          },
        },
      }, ...children)
    ));
};

export const teardownI18n = () => {
  if (jsLinguiMock) {
    jsLinguiMock();
    jsLinguiMock = null;
  }

  if (intlMock) {
    intlMock();
    intlMock = null;
  }

  // reset values
  keys = {};
  values = {};
  valueMap = {};
};

const extractMessage = (message: string) => {
  const match = message.match(new RegExp(`${START}([^${END}]*)${END}`, 'i'));

  return match ? match[1] : '';
};

export const collectI18n = () => {
  const resultMap: any[] = [];
  let messageNodes = Array.from(document.body.querySelectorAll('*'))
  // get all the text from all nodes
    .map(el => ({ el, text: (el as any).innerText, html: (el as any).innerHTML }))
    // keep the one with translations inside
    .filter(el => el.text && el.text.indexOf(START) >= 0)
    // extract translations
    .map(({ el, text, html }) =>
      (text.match(new RegExp(`${START}(.*)${END}`, 'g')) || [])
        .map(extractMessage)
        .map(value => ({ el, value, text, html })),
    )
    .filter(a => a.length)
    // sort from short to long (document.body contains all translations)
    .sort((a, b) => {
      const diff = a.length - b.length;

      return diff || a[0].html.length - b[0].html.length;
    })
    .reduce((acc, item) => [...acc, ...item], [])
    .filter(({ text }) => text);

  const foundMessages = {};
  while (messageNodes.length) {
    const message = messageNodes.shift();
    const key = valueMap[message.value] || valueMap[extractMessage(message.html)];
    if (!key) {
      continue;
    }

    foundMessages[key] = true;
    resultMap.push({
      key,
      node: message.el,
    });
    messageNodes = messageNodes.filter(({ el, value }) => !(el.contains(message.el) && value === message.value));
  }

  return [
    ...resultMap.map(({ key, node }) => ({
      node,
      origin: 'Trans:i18n',
      text: keys[key],
      key,
    })),
    ...(Object
      .keys(keys)
      .filter(key => !foundMessages[key])
      .map(key => ({
        node: undefined,
        origin: 'Trans:i18n:non-found',
        text: keys[key],
        key,
      }))),
  ];
};
