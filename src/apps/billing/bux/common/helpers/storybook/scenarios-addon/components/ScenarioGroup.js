import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { ScenarioCheckbox } from './ScenarioCheckbox';
import scenarios from '../../../../../../mock-server/scenarios';

const Title = styled.div`
  font-family: -apple-system, ".SFNSText-Regular", Arial, sans-serif;
  font-size: 14px;
  font-weight: 600;
  margin-bottom: 4px;
`;

const Group = styled.div`
  padding: 12px;
`;

export const ScenarioGroup = ({ selected, onChange }) => (
  <React.Fragment>
    {scenarios
      .map(scenario => scenario.group)
      .filter((elem, pos, arr) => arr.indexOf(elem) === pos)
      .map(group => (
        <Group key={group}>
          <Title>{group}</Title>
          {scenarios.map((scenario) => {
            if (scenario.group === group) {
              return (
                <ScenarioCheckbox
                  key={scenario.slug}
                  scenario={scenario}
                  selected={selected.includes(scenario.slug)}
                  onChange={onChange}
                />
              );
            }
            return null;
          })}
        </Group>
      ))
    }
  </React.Fragment>
);

ScenarioGroup.propTypes = {
  selected: PropTypes.arrayOf(PropTypes.string),
  onChange: PropTypes.func.isRequired,
};
