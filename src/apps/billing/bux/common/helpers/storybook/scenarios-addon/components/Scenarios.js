import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { ScenarioGroup } from './ScenarioGroup';

const Text = styled.p`
  font-family: -apple-system, ".SFNSText-Regular", Arial, sans-serif;
  font-size: 13px;
  padding: 12px;
  color: #828282;
`;

export class Scenarios extends React.Component {
  state = { values: null };

  componentDidMount() {
    const { channel, api } = this.props;

    channel.on('atlassian/scenarios/set', this.setScenarios);
    this.stopListeningOnStory = api.onStory(() => {
      this.setScenarios();
    });
  }

  componentWillUnmount() {
    if (this.stopListeningOnStory) {
      this.stopListeningOnStory();
    }

    this.unmounted = true;
    const { channel } = this.props;
    channel.removeListener('atlassian/scenarios/set', this.setScenarios);
  }

  onChange = ({ scenario, checked }) => {
    const { values } = this.state;
    if (checked) {
      values.push(scenario.slug);
    } else {
      values.splice(values.indexOf(scenario.slug), 1);
    }
    this.setScenarios(values);
    this.props.channel.emit('atlassian/scenarios/reload');
  };

  setScenarios = (values) => {
    const { api } = this.props;
    api.setQueryParams({
      scenarios: values,
    });
    this.setState({ values });
  };

  render() {
    const { values } = this.state;

    if (!values) {
      return (<Text>Scenarios not supported by this story</Text>);
    }

    return (
      <ScenarioGroup selected={values} onChange={this.onChange} />
    );
  }
}

Scenarios.propTypes = {
  channel: PropTypes.shape({
    emit: PropTypes.func,
    on: PropTypes.func,
    removeListener: PropTypes.func,
  }).isRequired,
  api: PropTypes.shape({
    onStory: PropTypes.func,
    getQueryParam: PropTypes.func,
    setQueryParams: PropTypes.func,
  }).isRequired,
};
