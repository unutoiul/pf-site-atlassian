import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Label = styled.label`
  font-family: -apple-system, ".SFNSText-Regular", Arial, sans-serif;
  font-size: 13px;
`;

export class ScenarioCheckbox extends React.Component {
  static propTypes = {
    scenario: PropTypes.object.isRequired,
    onChange: PropTypes.func.isRequired,
    selected: PropTypes.bool,
  };

  onChange = (e) => {
    const { scenario } = this.props;
    this.props.onChange({ scenario, checked: e.target.checked });
  };

  render() {
    const { scenario, selected } = this.props;

    return (
      <div>
        <Label>
          <input
            type="checkbox"
            checked={selected}
            onChange={this.onChange}
          /> {scenario.name}
        </Label>
      </div>
    );
  }
}
