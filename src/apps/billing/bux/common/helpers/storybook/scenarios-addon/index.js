import React from 'react';
import PropTypes from 'prop-types';
import addons from '@storybook/addons';

export class WithScenarios extends React.Component {
  static propTypes = {
    scenarios: PropTypes.arrayOf(PropTypes.string),
    children: PropTypes.element.isRequired,
  };

  constructor(props) {
    super(props);
    this.channel = addons.getChannel();
  }

  state = { variable: null };

  componentWillMount() {
    this.channel.emit('atlassian/scenarios/set', this.props.scenarios);
    this.channel.on('atlassian/scenarios/reload', this.forceRender);
  }

  componentWillUnmount() {
    this.channel.removeListener('atlassian/scenarios/reload', this.forceRender);
  }

  forceRender = () => {
    this.setState({ variable: Date.now() });
  };

  render() {
    const { children } = this.props;

    return React.Children.toArray(children).map(child => React.cloneElement(child, {
      key: this.state.variable,
    }));
  }
}
