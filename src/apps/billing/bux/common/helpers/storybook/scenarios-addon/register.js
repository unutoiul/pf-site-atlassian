import React from 'react';
import addons from '@storybook/addons';

import { Scenarios } from './components/Scenarios';

// Register the addon with a unique name.
addons.register('atlassian/scenarios', (api) => {
  // Also need to set a unique name to the panel.
  addons.addPanel('atlassian/scenarios/panel', {
    title: 'Scenarios',
    render: () => (
      <Scenarios channel={addons.getChannel()} api={api} />
    ),
  });
});
