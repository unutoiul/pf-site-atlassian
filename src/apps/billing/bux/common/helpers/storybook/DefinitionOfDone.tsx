import * as React from 'react';
import { createElement, remock } from 'react-remock';
import { MountContext } from 'whales-story-shots/storybook/decorator';

import { DefaultLoadingIndicator } from 'bux/components/deferred';
import Loading from 'bux/components/Loading';
import LoadingDelayed from 'bux/components/Loading/delayed';

const LOADER_SPINNED_TM = 5000; // should be enought

class StoryLockerInternal extends React.Component<{
  lock?: any,
}, {
  timedout: boolean,
}> {
  public state = {
    timedout: false,
  };
  public tm = 0;

  public componentDidMount() {
    this.tm = window.setTimeout(this.timedout, LOADER_SPINNED_TM);
    const { lock = null } = this.props.lock || {};
    if (lock) {
      lock();
    }
  }

  public componentWillUnmount() {
    const { unlock = null } = this.props.lock || {};

    clearTimeout(this.tm);
    // "next" loader spinned was timeout 300 to display
    setTimeout(() => {
      if (unlock) {
        unlock();
      }
    }, 400);
  }

  public timedout = () => {
    this.setState({
      timedout: true,
    });
    const { unlock = null } = this.props.lock || {};
    setTimeout(() => {
      if (unlock) {
        unlock();
      }
    }, 16);
  };

  public render() {
    return this.state.timedout ? 'Loading' : this.props.children;
  }
}

const StoryLocker: React.SFC<{ text: string }> = ({ children }) => (
  <MountContext.Consumer>
    {lock => <StoryLockerInternal lock={lock}>{children}</StoryLockerInternal>}
  </MountContext.Consumer>
);

[
  LoadingDelayed,
  Loading,
  DefaultLoadingIndicator,
].forEach(Component =>
  remock.mock(Component, (type, props, children) =>
    createElement(StoryLocker, props, createElement(type, props, children)),
  ),
);
