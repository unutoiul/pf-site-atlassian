import * as React from 'react';
import { connect } from 'react-redux';

import { resetStateAction } from 'bux/core/actions/reset-state';
import meta from 'bux/core/state/meta';
import { getCloudId } from 'bux/core/state/meta/selectors';

interface AsSiteProps {
  cloudId: string;

  updateSiteState(payload: any);

  setUserMetadata(payload: any);

  resetState();
}

class AsSite extends React.Component<AsSiteProps> {
  public componentDidMount() {
    this.props.updateSiteState({ cloudId: '123' });
    this.props.setUserMetadata({ user: {} });
  }

  public componentDidUpdate(prevProps: AsSiteProps) {
    if (
      prevProps.cloudId !== this.props.cloudId
    ) {
      this.props.resetState();
    }
  }

  public render() {
    return <React.Fragment key={this.props.cloudId}>{this.props.children}</React.Fragment>;
  }
}

const mapStateToProps = state => ({
  cloudId: getCloudId(state) || 'no-id',
});

const mapDispatchToProps = dispatch => ({
  resetState: () => dispatch(resetStateAction()),
  updateSiteState: (payload) => dispatch(meta.actions.updateSiteState(payload)),
  setUserMetadata: (payload) => dispatch(meta.actions.setUserMetadata(payload)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AsSite);
