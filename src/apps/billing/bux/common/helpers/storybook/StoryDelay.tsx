import * as React from 'react';
import { MountContext } from 'whales-story-shots/storybook/decorator';

class StoryLockerInternal extends React.Component<{
  lock?: any,
  timeout: number,
}> {
  public tm = 0;

  public componentDidMount() {
    const { unlock = null, lock = null } = this.props.lock || {};
    lock();
    this.tm = window.setTimeout(unlock, this.props.timeout);
  }

  public componentWillUnmount() {
    const { unlock = null } = this.props.lock || {};

    clearTimeout(this.tm);
    unlock();
  }

  public render() {
    return this.props.children;
  }
}

export const StoryDelay: React.SFC<{ timeout: number }> = ({ children, timeout }) => (
  <MountContext.Consumer>
    {lock => <StoryLockerInternal lock={lock} timeout={timeout}>{children}</StoryLockerInternal>}
  </MountContext.Consumer>
);
