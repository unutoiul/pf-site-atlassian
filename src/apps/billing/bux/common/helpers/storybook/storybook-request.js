import { handleErrors, handleReject } from 'bux/common/helpers/request-handle';
import path from 'path';
import { parse } from 'query-string';
import applyScenarios from '../../../../mock-server/helpers/scenarios/apply-scenarios';

function loadMockFile(method, url) {
  let replace = '/gateway/api/billing-ux/api';
  if (url.indexOf(replace) === -1) {
    replace = replace.slice(0, -4);
  }
  const orgRemoverRegex = /(\/organization\/[\d]+)/g;
  const cloudRemoverRegex = /(\/cloud\/[\d]+)/g;
  const pathname = url
    .replace(replace, '')
    .replace(orgRemoverRegex, '')
    .replace(cloudRemoverRegex, '');
  const filePath = path.join('static', 'mock', `${pathname}-${method.toLowerCase()}.json`);
  return fetch(filePath);
}

function applyScenariosAndResponse(method, url) {
  return (data) => {
    const settings = {
      scenarios: parse(top.location.search, { arrayFormat: 'index' }).scenarios
    };

    const newResponse = applyScenarios(settings, method, url, 200, data);
    const blob = new Blob([JSON.stringify(newResponse.body, null, 2)], { type: 'application/json' });
    return new Response(blob, { status: newResponse.status });
  };
}

function fetchMock(method, url, options) {
  return loadMockFile(method, url)
    .then(response => response.json())
    .then(applyScenariosAndResponse(method, url))
    .then(handleErrors(url, options), handleReject);
}

export default {
  get: (url, options) => fetchMock('GET', url, options),
  post: (url, body, options) => fetchMock('POST', url, options),
  put: (url, body, options) => fetchMock('PUT', url, options),
};
