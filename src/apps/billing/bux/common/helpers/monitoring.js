export const setupMonitoring = () => {};

const newrelic = window.newrelic || undefined;

export const logMessage = (name, message) => newrelic && newrelic.addPageAction(name, { message });
export const logException = err => newrelic && newrelic.noticeError(err);

export default {
  logException,
  logMessage,
  setupMonitoring
};
