import { expect } from 'chai';
import taxName from '../nameOfTaxID';

const getTax = ({ displayName }) => displayName;

describe('Name of the tax ID', () => {
  it('should name tax ID', () => {
    expect(getTax(taxName())).to.be.equal('Tax ID');
    expect(getTax(taxName('Netherland'))).to.be.equal('Tax ID');
    expect(getTax(taxName('AU'))).to.be.equal('ABN');
    expect(getTax(taxName('DE'))).to.be.equal('VAT Number');
    expect(getTax(taxName('NZ'))).to.be.equal('NZBN');
    expect(getTax(taxName('US'))).to.be.equal('EIN');

    expect(getTax(taxName('CA'))).to.be.equal('BN / NE');
    expect(getTax(taxName('CA', 'QC'))).to.be.equal('QST');
  });
});
