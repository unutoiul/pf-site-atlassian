import isoToVat from './isoToVat';

const defaultTax = {
  displayName: 'Tax ID',
  description: 'Tax ID Number',
};

const EU_VAT = {
  AT: { description: 'Umsatzsteuer-Identifikationsnummer', localName: 'UID' },
  BE: { description: 'Numéro de TVA', localName: 'TVA' },
  BG: { description: 'Identifikacionen nomer po DDS', localName: 'ДДС' },
  HR: { description: 'PDV Id. Broj OIB', localName: 'PDV-ID' },
  CY: { description: 'Arithmós Engraphḗs phi. pi. a.', localName: 'ΦΠΑ' },
  CZ: { description: 'Daňové identifikační číslo', localName: 'DIČ' },
  DK: { description: 'Momsregistreringsnummer or CVR nummer', localName: 'CVR' },
  EE: { description: 'Käibemaksukohustuslase number', localName: 'KMKR' },
  FI: { description: 'Arvonlisäveronumero', localName: 'ALV' },
  FR: { description: 'Numéro de TVA intracommunautaire', localName: 'TVA' },
  DE: { description: 'Umsatzsteuer-Identifikationsnummer', localName: 'USt-IdNr' },
  EL: { description: 'Arithmós Forologikou Mētrṓou', localName: 'ΑΦΜ' },
  HU: { description: 'Közösségi adószám', localName: 'ANUM' },
  IE: { description: 'Value added tax identification no.', localName: 'VAT' },
  IT: { description: 'Partita IVA', localName: 'P.IVA' },
  LV: { description: 'Pievienotās vērtības nodokļa ', localName: 'PVN' },
  LT: { description: 'Pridėtinės vertės mokestis', localName: 'PVM' },
  LU: { description: 'Numéro d\'identification à la taxe sur la valeur ajoutée', localName: 'TVA' },
  MT: { description: 'Vat reg. no.', localName: 'VAT' },
  NL: { description: 'Btw-nummer', localName: 'BTW' },
  PL: { description: 'Numer Identyfikacji Podatkowej', localName: 'NIP' },
  PT: { description: 'Número de Identificação Fiscal / de Pessoa Colectiva', localName: 'NIF / NIPC' },
  RO: { description: 'Codul de identificare fiscală', localName: 'CIF' },
  SK: { description: 'Identifikačné číslo pre daň z pridanej hodnoty', localName: 'IČ DPH' },
  SI: { description: 'Davčna številka', localName: 'ID za DDV' },
  ES: { description: 'Número de Identificación Fiscal', localName: 'NIF / CIF' },
  SE: { description: 'VAT-nummer or momsnummer or momsregistreringsnummer', localName: 'Momsnr' },
  GB: { description: 'Value added tax registration number', localName: 'VAT' },
};

const VAT_WORLD = {
  AL: { description: 'Numri i Identifikimit për Personin e Tatueshëm', displayName: 'NIPT' },
  AU: { description: 'Australian Business Number', displayName: 'ABN' },
  BY: { description: 'Учетный номер плательщика', displayName: 'УНП' },
  CA: {
    description: 'Business Number', displayName: 'BN / NE', states: {
      QC: {
        description: 'Quebec Sales Tax', displayName: 'QST',
      },
    },
  },
  IS: { description: 'Value Added Tax Number', displayName: 'VSK / VASK' },
  IN: { description: 'Value Added Tax - Taxpayer Identification Number', displayName: 'VAT TIN / CST TIN' },
  ID: { description: 'Nomor Pokok Wajib Pajak', displayName: 'NPWP' },
  NZ: { description: 'NZ Business Number', displayName: 'NZBN' },
  NO: { description: 'Organization number', displayName: 'Orgnr' },
  PH: { description: 'Tax Identification Number', displayName: 'TIN' },
  RU: { description: 'Taxpayer Identification Number', displayName: 'ИНН' },
  SM: { description: 'Codice operatore economico', displayName: 'C.O.E.' },
  RS: { description: 'Tax identification number', displayName: 'PIB' },
  CH: { description: 'Mehrwertsteuernummer', displayName: 'MWST / TVA / IVA' },
  TR: { description: 'Türkiye Cumhuriyeti Kimlik Numarası', displayName: 'T.C.' },
  UA: { description: 'Identyfikacijnyj nomer platnyka podatkiv', displayName: 'ІНПП' },
  UZ: { description: 'Солиқ тўловчиларнинг идентификация рақами', displayName: 'СТИР' },
};

const NON_VAT = {
  US: { description: 'Employer Identification Number', displayName: 'EIN' },
  JP: { description: 'Consumption Tax Identification Number', displayName: 'TIN' },
  BR: { description: 'Cadastro Nacional de Pessoa Jurídica', displayName: 'CNPJ' },
  AR: { description: 'Impuesto al Valor Agregado', displayName: 'IVA' },
};

/* tslint:disable:variable-name */
const getTaxName = (iso3166_1: string, iso3166_2: string = '') => {
  const euroTax = isoToVat[iso3166_1];
  if (euroTax) {
    const { description = '', localName = '' } = EU_VAT[euroTax] || {};

    return {
      description,
      localName: localName !== 'VAT' ? localName : '',
      displayName: 'VAT Number',
    };
  }
  const result = VAT_WORLD[iso3166_1] || NON_VAT[iso3166_1] || defaultTax;
  const stateTax = result.states && iso3166_2 && result.states[iso3166_2];

  return stateTax || result;
};

export default getTaxName;
