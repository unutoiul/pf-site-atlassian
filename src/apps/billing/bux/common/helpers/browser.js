import qs from 'query-string';

const getWindow = () => (typeof window !== 'undefined' ? window : undefined);

export const getHostname = (currentWindow = getWindow()) =>
  ((!currentWindow || currentWindow.name === 'nodejs') ? 'nodejs' : currentWindow.location.hostname) || '';

export const openNewTab = (url, name = '_blank') => window.open(url, name);
export const redirect = url => window.open(url, '_top');

export const getQueryString = (currentWindow = getWindow()) =>
  ((!currentWindow || currentWindow.name === 'nodejs') ? '' : currentWindow.location.search.substr(1));

export const getQueryParams = (queryString = getQueryString()) => qs.parse(decodeURIComponent(queryString));

export const refresh = () => window.location.reload();

export default {
  getHostname,
  getQueryString,
  getQueryParams,
  openNewTab
};
