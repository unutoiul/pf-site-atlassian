import { expect } from 'chai';
import { i18n } from 'bux/helpers/jsLingui';
import validate, { isTaxIdValid } from '../taxID';

describe('valudator.taxId: ', () => {
  describe('isTexIdValid: ', () => {
    it('should fail if no TaxID is present', () => {
      expect(isTaxIdValid('', '', '')).to.be.false();
      expect(isTaxIdValid('AU', '', '')).to.be.false();
      expect(isTaxIdValid('US')).to.be.false();
      expect(isTaxIdValid('CA', 'QC', 'not-valid')).to.be.false();
    });

    it('should pass any TaxId for unsupported countries', () => {
      expect(isTaxIdValid('RU', '', 'not-valid')).to.be.true();
      expect(isTaxIdValid('AU', '', 'not-valid')).to.be.false();
      expect(isTaxIdValid('CA', '', 'not-valid')).to.be.true();
    });

    it('should pass 11-digits number for AU', () => {
      expect(isTaxIdValid('AU', '', '1234567890')).to.be.false();
      expect(isTaxIdValid('AU', '', '12345678901')).to.be.true();
      expect(isTaxIdValid('AU', '', '123456789012')).to.be.false();
    });

    it('should auto-prepend EU\'s taxID by VAT ', () => {
      expect(isTaxIdValid('BE', '', 'BE0123456789')).to.be.true();
      expect(isTaxIdValid('BE', '', '0123456789')).to.be.true();

      expect(isTaxIdValid('BE', '', 'BE01234567890')).to.be.false();
      expect(isTaxIdValid('BE', '', '01234567890')).to.be.false();
      expect(isTaxIdValid('BE', '', 'BY0123456789')).to.be.false();
    });
  });

  describe('validate: ', () => {
    it('should return null if no TaxID present', () => {
      expect(validate('', '')).to.be.undefined();
      expect(validate('AU', '')).to.be.undefined();
      expect(validate('US')).to.be.undefined();
    });

    it('should provide a localized messages', () => {
      expect(validate('RU', '', 'not-valid')).to.be.undefined();//no validation
      expect(validate('AU', '', 'not-valid', i18n)).to.be.equal('Please enter a valid ABN');
      expect(validate('BE', '', 'not-valid', i18n)).to.be.equal('Please enter a valid VAT Number (TVA)');

      expect(validate('CA', '', 'not-valid')).to.be.undefined();//no validation
      expect(validate('CA', 'QC', 'not-valid', i18n)).to.be.equal('Please enter a valid QST');
    });
  });
});
