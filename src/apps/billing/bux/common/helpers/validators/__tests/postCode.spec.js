import { expect } from 'chai';
import { i18n } from 'bux/helpers/jsLingui';
import { messages } from '../../validation';
import validatePostCode from '../postCode';

describe('validator.postCode: ', () => {
  describe('validatePostCode: ', () => {
    it('should pass any postcode for any country except US', () => {
      expect(validatePostCode('', '', i18n)).to.be.undefined();
      expect(validatePostCode('CN', 'potato', i18n)).to.be.undefined();
      expect(validatePostCode('GB', '100500', i18n)).to.be.undefined();

      expect(validatePostCode('US', 'over9000', i18n)).to.be.equal(messages(i18n).postCode);
    });

    it('should validate US postcode, 5[+4]', () => {
      expect(validatePostCode('US', '99524', i18n)).to.be.undefined();
      expect(validatePostCode('US', '99524-1234', i18n)).to.be.undefined();
      expect(validatePostCode('US', '99524 1234', i18n)).to.be.undefined();
    });

    it('US postcode false cases', () => {
      expect(validatePostCode('US', '9952', i18n)).not.to.be.undefined();
      expect(validatePostCode('US', '99-524', i18n)).not.to.be.undefined();
      expect(validatePostCode('US', '99524 12345', i18n)).not.to.be.undefined();
    });
  });
});
