import { messages } from '../validation';
import patterns from '../countrySpecific/postcodePatterns';

const validatePostCode = (country, postCode, i18n) => (
  (!country || !patterns[country] || new RegExp(patterns[country]).test(postCode))
    ? undefined
    : messages(i18n).postCode
);

export default validatePostCode;
