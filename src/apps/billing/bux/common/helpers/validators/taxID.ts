import replacePlaceholders from '../../formatters/replace-placeholders';
import isoToVat from '../countrySpecific/isoToVat';
import getCountryTaxName from '../countrySpecific/nameOfTaxID';
import taxIdPatterns from '../countrySpecific/taxID';
import { messages } from '../validation';

const tryAddVat = (isoCountryCode: string, taxId: string) => {
  const vatPrefix = isoToVat[isoCountryCode] || '';
  // If country is a VAT country and ID doesn't start with the country's VAT prefix - append it before
  // checking validity

  return taxId.indexOf(vatPrefix) !== 0 ? `${vatPrefix}${taxId}` : taxId;
};

const cleanTaxId = (taxId: string) => taxId.replace(/[\s-.]/g, '').toUpperCase();

const normalizeTaxId = (isoCountryCode: string, taxID: string) => (
  tryAddVat(isoCountryCode, cleanTaxId(taxID))
);

export const isTaxIdValid = (isoCountryCode: string, isoStateCode: string, taxID: string) => {
  const taxIdPattern = taxIdPatterns[`${isoCountryCode}-${isoStateCode}`] || taxIdPatterns[isoCountryCode] || '';

  return !!(taxID && new RegExp(taxIdPattern).test(normalizeTaxId(isoCountryCode, taxID)));
};

const validateTaxId = (country, state, value, i18n) => {
  const { localName, displayName } = getCountryTaxName(country, state);
  const taxDisplayName = localName ? `${displayName} (${localName})` : displayName;

  return (!value || isTaxIdValid(country, state, value))
    ? undefined
    : replacePlaceholders(messages(i18n).taxID, { taxDisplayName });
};

export default validateTaxId;
