import { logException, logMessage } from './monitoring';

export const handleErrors = (url, { method }) => (response) => {
  if (!response.ok) {
    logMessage('RequestError', `[${response.status}] ${method} ${url}`, {
      extra: {
        statusText: response.statusText,
        responseUrl: response.url
      }
    });
    const error = new Error(response.statusText);
    error.response = response;
    return response.json()
      .catch(() => { error.couldNotReadErrorPayload = true; })
      .then((data) => { error.responseData = data; })
      .then(() => { throw error; });
  }
  return response;
};

export const handleReject = (rejectReason) => {
  logException(rejectReason);
  // Convert reject to `standard` response
  if (!rejectReason.response) {
    const error = new Error(rejectReason);
    // Response.error() carry no information
    error.response = new Response(null, { status: 408, statusText: rejectReason });
    throw error;
  }
  // in other cases - pass it thought
  return Promise.reject(rejectReason);
};
