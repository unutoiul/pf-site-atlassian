import Validator from 'validatorjs';
import { unflatten } from 'flat';
import merge from 'lodash.merge';

const taxDisplayName = '{{taxDisplayName}}';

export const messages = i18n => ({
  email: i18n.t('billing.validation.email')`Please enter a valid email address.`,
  required: i18n.t('billing.validation.required')`This field is required.`,
  taxID: i18n.t('billing.validation.taxID')`Please enter a valid ${taxDisplayName}`,
  postCode: i18n.t('billing.validation.postCode')`Please enter a valid postal code.`,
});

export const compose = validators => (values, options) => {
  const handleArrayOfValidators = (arrayValidators, arrayValues, arrayOptions) => {
    const errors = {};
    arrayValidators.forEach((validator) => {
      merge(errors, compose(validator)(arrayValues, arrayOptions));
    });
    return errors;
  };

  const handleObjectOfValidators = (objectValidators, objectValues, objectOptions) => {
    const errors = {};
    Object.keys(objectValidators).forEach((field) => {
      const validator = objectValidators[field];
      errors[field] = compose(validator)(objectValues[field], objectOptions);
    });
    return errors;
  };

  if (validators instanceof Array) {
    return handleArrayOfValidators(validators, values, options);
  }

  if (typeof validators === 'object') {
    return handleObjectOfValidators(validators, values, options);
  }

  if (typeof validators === 'function') {
    return validators(values, options);
  }

  throw new Error(`Could not handle validator of type ${typeof validators}`);
};

const executeRule = (rule, data) => (
  typeof rule === 'function'
    ? rule(data)
    : rule
);

/**
 * create a validation function
 * @param {Function[], Object[]} ruleFactories array of factories for the rules.
 */
export const createValidator = ruleFactories => (data = {}, { i18n }) => {
  if (!Array.isArray(ruleFactories)) {
    throw new Error('ruleFactories should be an array');
  }

  const rules = ruleFactories.reduce((acc, rule) => (
    { ...acc, ...executeRule(rule, data) }
  ), {});

  const validator = new Validator(data, rules, messages(i18n));
  validator.passes();
  const errors = validator.errors.all();
  return unflatten(errors);
};
