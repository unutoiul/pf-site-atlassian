export const linksByLanguage = {
  faq: {
    en: 'https://www.atlassian.com/licensing/cloud',
    ja: 'https://ja.atlassian.com/licensing/cloud',
  },
  faqOrg: {
    en: 'https://www.atlassian.com/licensing/atlassian-access',
    ja: 'https://ja.atlassian.com/licensing/atlassian-access',
  },
  cloudPricing: {
    en: 'https://www.atlassian.com/licensing/cloud',
    ja: 'https://ja.atlassian.com/licensing/cloud',
  },
  cloudPricingOrg: {
    en: 'https://www.atlassian.com/licensing/atlassian-access',
    ja: 'https://ja.atlassian.com/licensing/atlassian-access',
  },
  purchasingLicense: {
    en: 'https://www.atlassian.com/company/contact/purchasing-licensing',
    ja: 'https://ja.atlassian.com/company/contact',
  },
  support: {
    en: 'https://support.atlassian.com/',
  },
  cloudTermsOfService: {
    en: 'https://www.atlassian.com/legal/cloud-terms-of-service',
    ja: 'https://ja.atlassian.com/legal/cloud-terms-of-service',
  },
  softwareLicenseAgreement: {
    en: 'https://www.atlassian.com/legal/software-license-agreement',
    ja: 'https://ja.atlassian.com/legal/software-license-agreement',
  },
  summaryOfChanges: {
    en: 'https://www.atlassian.com/legal/summary-of-changes',
    ja: 'https://ja.atlassian.com/legal/summary-of-changes',
  },
  customerAgreement: {
    en: 'https://www.atlassian.com/legal/customer-agreement',
    ja: 'https://ja.atlassian.com/legal/cloud-terms-of-service',
  },
  privacyPolicy: {
    en: 'https://www.atlassian.com/legal/privacy-policy',
    ja: 'https://ja.atlassian.com/legal/privacy-policy',
  },
  accessPricingBilling: {
    en: 'https://confluence.atlassian.com/x/-fOEO',
  },
};

export const DEFAULT_LANGUAGE = 'en';

export const getLink = (links: object) => (id, language = 'en') => {
  const linksById = links[id];
  if (!linksById) {
    throw new Error(`link id ${id} does not exist`);
  }

  const linkByLanguage = linksById[toAvailableLanguage(language, linksById)];
  if (!linkByLanguage) {
    throw new Error(`link id ${id} does not exist with default language`);
  }

  return linkByLanguage;
};

export const toAvailableLanguage = (language: string, links: object): string => {
  const codes = Object.keys(links);

  return codes.find(s => s === language)
    || codes.find(s => s.substr(0, 2) === language.substr(0, 2))
    || DEFAULT_LANGUAGE;
};
