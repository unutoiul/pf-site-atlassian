import ddev from './environments/ddev.json';
import adev from './environments/adev.json';
import local from './environments/local.json';
import test from './environments/test.json';
import production from './environments/production.json';
import staging from './environments/staging.json';
import logConfig from './log/GAS.json';

import { getHostname } from '../helpers/browser';
import { getItemFromSessionStorage } from '../helpers/session-storage';
import { getLink, linksByLanguage } from './links';

const stagingHosts = [
  '.jira-dev.com',
  'stg.atlassian.com',
  'stg.internal.atlassian.com'
];

const isLocal = hostname => __NODE_ENV__ === 'development' || (
  hostname === 'localhost' ||
  hostname === '127.0.0.1' ||
  hostname === '0.0.0.0'
);

const isTest = hostname => hostname === 'nodejs'; // headless integration tests

const isStaging = hostname => stagingHosts.some(match => hostname.endsWith(match));

const localhost = typeof window === 'undefined'
  ? 'http://localhost'
  : `${window.location.protocol}//${window.location.hostname}`;

const fixLocalhost = settings => (
  Object
    .keys(settings)
    .reduce((acc, key) => {
      const value = settings[key];
      acc[key] = typeof value === 'string'
        ? value.replace('http://localhost', localhost)
        : value;
      return acc;
    }, {})
);

const environments = {
  test, local, ddev, adev, staging, production
};
const overrideEnvironment = getItemFromSessionStorage('BUX_ENV');

const getCurrentEnvironment = (hostname) => {
  if (isTest(hostname)) {
    return 'test';
  }
  if (isLocal(hostname)) {
    return 'local';
  }
  if (isStaging(hostname)) {
    return 'staging';
  }
  return 'production';
};

const hostname = getHostname();

export const currentEnvironment = overrideEnvironment || getCurrentEnvironment(hostname);

const config = fixLocalhost(environments[currentEnvironment]);

const domain = url => url.replace(/(^\w+:|^)\/\//, '').replace(/\/\s*$/, '');

export default {
  ...config,
  mockPayPalButton: !!window.mockPayPalButton,
  mockConvertToAnnualPoolingTimeout: window.mockConvertToAnnualPoolingTimeout,
  macDomain: domain(config.macUrl),
  macPaymentDetailsUrl: `${config.macUrl}/products/paymentdetailsforcloudid`,
  log: {
    ...logConfig,
    ...config.log,
    server: hostname
  },
  BUXSTANDALONE: false,
  links: getLink(linksByLanguage, currentEnvironment)
};
