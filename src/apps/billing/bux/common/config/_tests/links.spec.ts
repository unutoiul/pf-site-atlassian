import { expect } from 'chai';

import { DEFAULT_LANGUAGE, getLink, toAvailableLanguage } from '../links';

describe('config links', () => {
  describe('toAvailableLanguage', () => {
    it('should return en for en-US', () => {
      expect(toAvailableLanguage('en-US', { en: 'test' })).to.be.equals('en');
      expect(toAvailableLanguage('en_US', { en: 'test' })).to.be.equals('en');
    });

    it('should return en-US if exists', () => {
      expect(toAvailableLanguage('en-US', { en: 'test', 'en-US': 'test' })).to.be.equals('en-US');
      expect(toAvailableLanguage('en', { en: 'test', 'en-US': 'test' })).to.be.equals('en');
    });

    it('should return default if requested does not exists', () => {
      expect(toAvailableLanguage('es', { en: 'test' })).to.be.equals(DEFAULT_LANGUAGE);
    });
  });

  describe('getLink', () => {
    const links = {
      faq: {
        en: 'en-faq',
        fr: 'fr-faq',
        'pt-BR': 'pt-BR-faq',
      },
      support: {
        es: 'es-support',
      },
    };

    it('should return an existing link with the requested language', () => {
      expect(getLink(links)('faq')).to.be.eql('en-faq');
      expect(getLink(links)('support', 'es')).to.be.eql('es-support');
      expect(getLink(links)('faq', 'fr')).to.be.eql('fr-faq');
      expect(getLink(links)('faq', 'en')).to.be.eql('en-faq');
      expect(getLink(links)('faq', 'pt-BR')).to.be.eql('pt-BR-faq');
    });
    it('should fallback when missing language', () => {
      expect(getLink(links)('faq', 'es')).to.be.eql('en-faq');
      expect(getLink(links)('faq', 'pt')).to.be.eql('pt-BR-faq');
    });
    it('should throw when non-existent id', () => {
      expect(() => getLink(links)('something')).to.throw();
      expect(() => getLink(links)('support', 'en')).to.throw();
    });
  });
});
