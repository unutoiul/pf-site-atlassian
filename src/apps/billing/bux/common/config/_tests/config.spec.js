import proxyquire from 'bux/helpers/proxyquire';
import sinon from 'sinon';
import { expect } from 'chai';

function getConfig(hostname, sessionEnvironment) {
  const getHostname = sinon.stub().returns(hostname);
  const getItemFromSessionStorage = sinon.stub().returns(sessionEnvironment);

  return proxyquire.noCallThru().load('..', {
    '../helpers/browser': { getHostname },
    '../helpers/session-storage': { getItemFromSessionStorage }
  }).default;
}

describe('Config', () => {
  it('returns prod for atlassian.net', () => {
    expect(getConfig('instance.atlassian.net').billingUXServiceUrl)
      .to.equal('/gateway/api/billing-ux');
  });

  it('returns prod for jira.com', () => {
    expect(getConfig('instance.jira.com').billingUXServiceUrl)
      .to.equal('/gateway/api/billing-ux');
  });

  it('returns staging for jira-dev.com', () => {
    expect(getConfig('instance.jira-dev.com').billingUXServiceUrl)
      .to.equal('/gateway/api/billing-ux');
  });

  it('returns local for localhost', () => {
    expect(getConfig('localhost').billingUXServiceUrl)
      .to.equal('/gateway/api/billing-ux');
  });

  it('returns development config when Environments is overridden and set to ddev', () => {
    expect(getConfig('whatever.jira-dev.com', 'ddev').billingUXServiceUrl)
      .to.equal('/gateway/api/billing-ux');
  });

  it('returns development config when Environments is overridden and set to adev', () => {
    expect(getConfig('whatever.jira-dev.com', 'adev').billingUXServiceUrl)
      .to.equal('/gateway/api/billing-ux');
  });
});
