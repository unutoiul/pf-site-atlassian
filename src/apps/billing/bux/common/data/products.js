/* eslint-disable max-len */
import { Trans } from '@lingui/react';
import * as React from 'react';

import config from '../config';

export const tags = {
  track: <Trans id="billing.products.tag.track">Track</Trans>,
  helpdesk: <Trans id="billing.products.tag.helpdesk">IT & helpdesk</Trans>,
  collaborate: <Trans id="billing.products.tag.collaborate">Collaborate</Trans>,
  development: <Trans id="billing.products.tag.development">Development</Trans>,
  chat: <Trans id="billing.products.tag.chat">Chat</Trans>,
  incidentManagement: <Trans id="billing.products.tag.incident-management">Incident Management</Trans>,
  projectManagement: <Trans id="billing.products.tag.project-management">Project management</Trans>,
  messaging: <Trans id="billing.products.tag.messages">Messaging</Trans>,
  security: <Trans id="billing.products.tag.security">Identity & Security</Trans>
};

export const priceFreeLabels = {
  default: <Trans id="billing.products.price.free-label">Free for all teams</Trans>,
  earlyAccess: <Trans id="billing.products.price.free-label.early-access">Free early access</Trans>,
  billedSeparately: <Trans id="billing.products.price.free-label.external-billing">Billed separately</Trans>,
};

const products = {
  'jira-core.ondemand': {
    name: 'Jira Core',
    tag: tags.track,
    publisher: 'Atlassian Software',
    description: (
      <Trans id="billing.products.jira-core.description">
        Simplify projects, from marketing campaigns, HR onboarding, to purchasing approvals and legal document reviews.
      </Trans>
    ),
    link: 'https://www.atlassian.com/software/jira/core',
    logo: '<div class="parent-logo jira-core"><title>Jira Core</title></div>',
    icon: '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" fill="#172B4D" viewBox="0 0 54.95 66"><defs><style></style><linearGradient id="linear-gradient_44b13f" x1="37.13" y1="31.71" x2="52.42" y2="46.24" gradientUnits="userSpaceOnUse"><stop offset="0.17" stop-color="#0052cc"></stop><stop offset="1" stop-color="#2684ff"></stop></linearGradient></defs><title>Jira Core-icon-blue</title><g id="Layer_2" data-name="Layer 2"><g id="Blue"><path d="M33.43,44.48A21.52,21.52,0,0,0,54.95,66h0V23.12L33.43,37.86Z" style="fill: url(&quot;#linear-gradient_44b13f&quot;);"></path><path d="M54.95,23.12V3.21a3.2,3.2,0,0,0-5-2.64L0,34.67a21.52,21.52,0,0,0,29.89,5.69Z" style="fill: rgb(38, 132, 255);"></path></g></g></svg>',
    contextPath: '/',
    onboardingUrl: 'secure/LandingPage.jspa?product=jira-core',
    dependencies: [],
    UMKey: 'JIRA',
    pricingLink: 'https://www.atlassian.com/software/jira/core/pricing?tab=cloud',
    promotable: false
  },
  'jira-software.ondemand': {
    name: 'Jira Software',
    tag: tags.track,
    publisher: 'Atlassian Software',
    description: (
      <Trans id="billing.products.jira-software.description">
        Plan, track, and release world-class software. The #1 software development tool used by agile teams.
      </Trans>
    ),
    link: 'https://www.atlassian.com/software/jira',
    logo: '<div class="parent-logo jira-software"><title>Jira Software</title></div>',
    icon: '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" fill="#172B4D" viewBox="0 0 73.27 75.76"><defs><style></style><linearGradient id="linear-gradient_21de2d" x1="34.64" y1="15.35" x2="19" y2="30.99" gradientUnits="userSpaceOnUse"><stop offset="0.18" stop-color="#0052cc"></stop><stop offset="1" stop-color="#2684ff"></stop></linearGradient><linearGradient id="linear-gradient-2_21de2d" x1="38.78" y1="60.28" x2="54.39" y2="44.67" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#linear-gradient_21de2d"></linearGradient></defs><title>Jira Software-icon-blue</title><g id="Layer_2" data-name="Layer 2"><g id="Blue"><path d="M72.4,35.76,39.8,3.16,36.64,0h0L12.1,24.54h0L.88,35.76A3,3,0,0,0,.88,40L23.3,62.42,36.64,75.76,61.18,51.22l.38-.38L72.4,40A3,3,0,0,0,72.4,35.76ZM36.64,49.08l-11.2-11.2,11.2-11.2,11.2,11.2Z" style="fill: rgb(38, 132, 255);"></path><path d="M36.64,26.68A18.86,18.86,0,0,1,36.56.09L12.05,24.59,25.39,37.93,36.64,26.68Z" style="fill: url(&quot;#linear-gradient_21de2d&quot;);"></path><path d="M47.87,37.85,36.64,49.08a18.86,18.86,0,0,1,0,26.68h0L61.21,51.19Z" style="fill: url(&quot;#linear-gradient-2_21de2d&quot;);"></path></g></g></svg>',
    contextPath: '/',
    onboardingUrl: 'secure/LandingPage.jspa?product=jira-software',
    dependencies: [],
    UMKey: 'JIRA',
    pricingLink: 'https://www.atlassian.com/software/jira/pricing?tab=cloud',
    promotable: false
  },
  'jira-servicedesk.ondemand': {
    name: 'Jira Service Desk',
    tag: tags.helpdesk,
    publisher: 'Atlassian Software',
    description: (
      <Trans id="billing.products.jira-servicedesk.description">
        Give your customers an easy way to ask for help and your agents a fast way to resolve incidents.
      </Trans>
    ),
    link: 'https://www.atlassian.com/software/jira/service-desk',
    logo: '<div class="parent-logo jira-servicedesk"><title>Jira Service Desk</title></div>',
    icon: '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" fill="#172B4D" viewBox="0 0 59.15 75.76"><defs><style></style><linearGradient id="linear-gradient_e11a76" x1="36.5" y1="34.28" x2="45.33" y2="60.42" gradientTransform="translate(-1.77 -1.54) rotate(0.35)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#0052cc"></stop><stop offset="1" stop-color="#2684ff"></stop></linearGradient></defs><title>Jira Service Desk-icon-blue</title><g id="Layer_2" data-name="Layer 2"><g id="Blue"><path d="M35.34,29.92,55.95,30a3.21,3.21,0,0,1,2.55,5.14L28.08,75.76a21.61,21.61,0,0,1-4.38-30.25Z" style="fill: url(&quot;#linear-gradient_e11a76&quot;);"></path><path d="M23.69,45.51,3.2,45.38A3.21,3.21,0,0,1,.65,40.24L30.77,0a21.61,21.61,0,0,1,4.38,30.25Z" style="fill: rgb(38, 132, 255);"></path></g></g></svg>',
    contextPath: '/',
    onboardingUrl: 'secure/LandingPage.jspa?product=jira-servicedesk',
    dependencies: [],
    UMKey: 'JIRA',
    pricingLink: 'https://www.atlassian.com/software/jira/service-desk/pricing?tab=cloud',
    promotable: true,
    priority: 2
  },
  'jira.ondemand': {
    name: 'Jira',
    tag: tags.track,
    publisher: 'Atlassian Software',
    description: (
      <Trans id="billing.products.jira.description">
        Enable your software team to organize their work and deliver superior product smarter and faster than ever before.
      </Trans>
    ),
    link: 'https://www.atlassian.com/software/jira',
    logo: '<div class="parent-logo jira"><title>Jira</title></div>',
    icon: '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" fill="#172B4D" viewBox="0 0 71.81 71.81"><defs><style></style><linearGradient id="New_Gradient_Swatch_1_ea2b9f" x1="53.96" y1="17.29" x2="39.25" y2="32.46" gradientUnits="userSpaceOnUse"><stop offset="0.18" stop-color="#0052cc"></stop><stop offset="1" stop-color="#2684ff"></stop></linearGradient><linearGradient id="New_Gradient_Swatch_1-2_ea2b9f" x1="37.83" y1="34.62" x2="20.82" y2="51.16" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#New_Gradient_Swatch_1_ea2b9f"></linearGradient></defs><title>Jira-icon-blue</title><g id="Layer_2" data-name="Layer 2"><g id="Blue"><path d="M68.81,0H34.23A15.61,15.61,0,0,0,49.84,15.61h6.37v6.15A15.61,15.61,0,0,0,71.81,37.36V3A3,3,0,0,0,68.81,0Z" style="fill: rgb(38, 132, 255);"></path><path d="M51.7,17.23H17.12A15.61,15.61,0,0,0,32.72,32.83h6.37V39A15.61,15.61,0,0,0,54.7,54.59V20.23A3,3,0,0,0,51.7,17.23Z" style="fill: url(&quot;#New_Gradient_Swatch_1_ea2b9f&quot;);"></path><path d="M34.58,34.45H0A15.61,15.61,0,0,0,15.61,50.06H22v6.15A15.61,15.61,0,0,0,37.58,71.81V37.45A3,3,0,0,0,34.58,34.45Z" style="fill: url(&quot;#New_Gradient_Swatch_1-2_ea2b9f&quot;);"></path></g></g></svg>',
    contextPath: '/',
    dependencies: [],
    UMKey: 'JIRA',
    promotable: false
  },
  'confluence.ondemand': {
    name: 'Confluence',
    tag: tags.collaborate,
    publisher: 'Atlassian Software',
    description: (
      <Trans id="billing.products.confluence.description">
        Give your team one place to share, find, and collaborate on information they need to get work done.
      </Trans>
    ),
    link: 'https://www.atlassian.com/software/confluence',
    logo: '<div class="parent-logo confluence"><title>Confluence</title></div>',
    icon: '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" fill="#172B4D" viewBox="0 0 62.75 60.38"><defs><style></style><linearGradient id="linear-gradient_88536c" x1="59.68" y1="64.2" x2="20.35" y2="41.6" gradientUnits="userSpaceOnUse"><stop offset="0.18" stop-color="#0052cc"></stop><stop offset="1" stop-color="#2684ff"></stop></linearGradient><linearGradient id="linear-gradient-2_88536c" x1="279.76" y1="-1619.8" x2="240.42" y2="-1642.4" gradientTransform="translate(282.83 -1623.62) rotate(180)" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#linear-gradient_88536c"></linearGradient></defs><title>Confluence-icon-blue</title><g id="Layer_2" data-name="Layer 2"><g id="Blue"><path d="M2.23,46.07c-.65,1.06-1.38,2.29-2,3.27a2,2,0,0,0,.67,2.72l13,8a2,2,0,0,0,2.77-.68c.52-.87,1.19-2,1.92-3.21,5.15-8.5,10.33-7.46,19.67-3l12.89,6.13a2,2,0,0,0,2.69-1l6.19-14a2,2,0,0,0-1-2.62c-2.72-1.28-8.13-3.83-13-6.18C28.51,27,13.62,27.56,2.23,46.07Z" style="fill: url(&quot;#linear-gradient_88536c&quot;);"></path><path d="M60.52,14.31c.65-1.06,1.38-2.29,2-3.27a2,2,0,0,0-.67-2.72l-13-8A2,2,0,0,0,46,1c-.52.87-1.19,2-1.92,3.21-5.15,8.5-10.33,7.46-19.67,3L11.56,1.09a2,2,0,0,0-2.69,1l-6.19,14a2,2,0,0,0,1,2.62c2.72,1.28,8.13,3.83,13,6.18C34.24,33.38,49.13,32.82,60.52,14.31Z" style="fill: url(&quot;#linear-gradient-2_88536c&quot;);"></path></g></g></svg>',
    contextPath: '/wiki/',
    dependencies: [],
    UMKey: 'CONFLUENCE',
    pricingLink: 'https://www.atlassian.com/software/confluence/pricing?tab=cloud',
    promotable: true,
    priority: 1
  },
  'bamboo.ondemand': {
    name: 'Bamboo',
    tag: tags.development,
    publisher: 'Atlassian Software',
    description: (
      <Trans id="billing.products.bamboo.description">
        Automate your delivery pipeline and see results from builds, tests, and releases inside Jira issues. Requires an Amazon Web Services account.
      </Trans>
    ),
    link: 'https://www.atlassian.com/software/bamboo',
    logo: '<div class="parent-logo bamboo"><title>Bamboo</title></div>',
    icon: '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" fill="#172B4D" viewBox="0 0 63.56 73.55"><defs><style></style><linearGradient id="linear-gradient_cee45e" x1="16.06" y1="10.61" x2="16.06" y2="53.94" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#0052cc"></stop><stop offset="1" stop-color="#2684ff"></stop></linearGradient><linearGradient id="linear-gradient-2_cee45e" x1="15.91" y1="10.61" x2="15.91" y2="53.94" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#linear-gradient_cee45e"></linearGradient></defs><title>Bamboo-icon-blue</title><g id="Layer_2" data-name="Layer 2"><g id="Blue"><path d="M61.56,41.81H46.27a2,2,0,0,0-2,1.7A12.57,12.57,0,0,1,31.81,54.34l4.14,18.91A31.81,31.81,0,0,0,63.56,43.95,2,2,0,0,0,61.56,41.81Z" style="fill: rgb(38, 132, 255);"></path><path d="M35.1,41.33l22-18.91a2,2,0,0,0,0-3L35.1.48A2,2,0,0,0,31.81,2V39.81A2,2,0,0,0,35.1,41.33Z" style="fill: rgb(38, 132, 255);"></path><path d="M.3,45.94A31.81,31.81,0,0,0,31.81,73.55V54.34A12.56,12.56,0,0,1,19.25,41.81Z" style="fill: url(&quot;#linear-gradient_cee45e&quot;);"></path><path d="M0,41.51v.3H19.25A12.56,12.56,0,0,1,31.81,29.28L27.68,10.37A31.79,31.79,0,0,0,0,41.51" style="fill: url(&quot;#linear-gradient-2_cee45e&quot;);"></path></g></g></svg>',
    contextPath: '/builds/',
    dependencies: [],
    UMKey: 'BAMBOO',
    pricingLink: 'https://www.atlassian.com/software/bamboo/pricing',
    promotable: false
  },
  'hipchat.cloud': {
    name: 'Stride',
    tag: tags.chat,
    publisher: 'Atlassian Software',
    description: (
      <Trans id="billing.products.hipchat.cloud.description">
        Bring your team together to talk, meet, decide, and do. Group messaging, video meetings, and built-in collaboration tools across every device.
      </Trans>
    ),
    link: 'https://www.stride.com',
    logo: '<div class="parent-logo jira-software"><title>Stride</title></div>',
    icon: '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" fill="#172B4D" viewBox="0 0 68.23 55.01"><defs><style></style><linearGradient id="New_Gradient_Swatch_1_dc4024" x1="31.3" y1="16.95" x2="1.32" y2="38.89" gradientUnits="userSpaceOnUse"><stop offset="0.18" stop-color="#0052cc"></stop><stop offset="1" stop-color="#2684ff"></stop></linearGradient></defs><title>Stride-icon-blue</title><g id="Layer_2" data-name="Layer 2"><g id="Blue"><path d="M19.16,0V11C19.16,26.23,11.52,32.11,1.8,33A2,2,0,0,0,0,35c0,4.1,0,13.79,0,18a2,2,0,0,0,2.11,2c24.34-1.24,39-19,39-41.05h.07V0Z" style="fill: url(&quot;#New_Gradient_Swatch_1_dc4024&quot;);"></path><path d="M68,52.11,42.23,0H19.16L46.38,53.37A3,3,0,0,0,49.05,55H66.23A2,2,0,0,0,68,52.11Z" style="fill: rgb(38, 132, 255);"></path></g></g></svg>',
    contextPath: config.strideUrl,
    dependencies: [],
    UMKey: 'STRIDE',
    outsideInstall: 'https://www.stride.com',
    pricingLink: 'https://www.stride.com/pricing',
    // this is super hacky, that is because there is no product upgrade concept in our billing system
    upgradePrice: 2, // from grandfathered plan 27d24085-6893-4b9c-8fe7-37537428e1ba
    upgradePriceExtraInfo: 'until June 31st, 2019', // we expect to move all customers to Stride before that date
    upgradeUrl: 'https://hipchat.com/stride_upgrade',
    promotable: false
  },
  opsgenie: {
    name: 'Opsgenie (billed separately)',
    publisher: 'Atlassian Software',
    tag: tags.incidentManagement,
    description: (
      <Trans id="billing.products.opsgenie.description">
        Stay in control during incidents.
      </Trans>
    ),
    link: 'https://www.opsgenie.com',
    dependencies: [],
    pricingLink: 'https://www.opsgenie.com/pricing',
    priceFreeLabel: priceFreeLabels.billedSeparately,
    logo: '<div class="parent-logo jira-software"><title>Opsgenie (billed separately)</title></div>',
    icon: '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="80" height="80" viewBox="0 0 80 80"><defs><style>.cls-1_377f72{fill:url(#linear-gradient_377f72)}.cls-2_377f72{fill:url(#linear-gradient-2_377f72)}.cls-3_377f72{fill:#2684ff}</style><linearGradient id="linear-gradient_377f72" x1="40.29" y1="2010.59" x2="40.29" y2="1973.99" gradientTransform="matrix(1, 0, 0, -1, 0, 2020.8)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#2684ff"></stop><stop offset="0.82" stop-color="#0052cc"></stop></linearGradient><linearGradient id="linear-gradient-2_377f72" x1="29.3" y1="1975.02" x2="41.56" y2="1949.36" gradientTransform="matrix(1, 0, 0, -1, 0, 2020.8)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#2684ff"></stop><stop offset="0.62" stop-color="#0052cc"></stop></linearGradient></defs><title>opgenie-icon-gradient-blue</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1-2"><circle class="cls-1_377f72" cx="40.29" cy="22.11" r="17.83"></circle><path class="cls-2_377f72" d="M38.77,75.28a85.81,85.81,0,0,1-28.23-29.5,2,2,0,0,1,.77-2.72l.1-.05,13.5-6.62a2,2,0,0,1,2.61.79,66.66,66.66,0,0,0,29,26.23A85.52,85.52,0,0,1,41.8,75.28,2.86,2.86,0,0,1,38.77,75.28Z"></path><path class="cls-3_377f72" d="M41.8,75.28A85.81,85.81,0,0,0,70,45.78a2,2,0,0,0-.77-2.72L69.17,43,55.66,36.39a2,2,0,0,0-2.6.79,66.74,66.74,0,0,1-29,26.23A85.14,85.14,0,0,0,38.77,75.28,2.86,2.86,0,0,0,41.8,75.28Z"></path></g></g></svg>'
  },
  'com.atlassian.identity-manager': {
    deactivationMessage: (
      <Trans id="billing.products.identity-manager.deactivation.message">
        Once unsubscribed, you will notice the following changes:
        <ul>
          <li>Managed users will not be able to use SAML to login in and will need to set an Atlassian account password</li>
          <li>Any password policy can no longer be updated</li>
          <li>Two-step verification will become user enabled and your managed users can switch this off at any time</li>
        </ul>
      </Trans>
    )
  },
  'jira-incident-manager.ondemand': {
    name: 'Jira Ops',
    tag: tags.incidentManagement,
    publisher: 'Atlassian Software',
    description: (
      <Trans id="billing.products.jira-incident-manager.description">
        Modern incident management with one place to respond, resolve, and learn from every incident.
      </Trans>
    ),
    link: 'https://www.atlassian.com/software/jira/ops',
    logo: '<div class="parent-logo jira-code"><title>Jira Ops</title></div>',
    icon: '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 49 67.7"><defs><style>.cls-1{fill:url(#linear-gradient);}.cls-2{fill:#2684ff;}</style><linearGradient id="linear-gradient" x1="11.32" y1="39.45" x2="37.52" y2="45.17" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#0052cc"/><stop offset="1" stop-color="#2684ff"/></linearGradient></defs><title>Jira Ops-icon-blue-rgb</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M42.6,20.82,6.85,53.63c.51.51,1,1,1.59,1.47L22.55,67a2.86,2.86,0,0,0,3.68,0c2.54-2.16,7.51-6.36,13.42-11.34C51,46.07,52,32,42.6,20.82Z"/><path class="cls-2" d="M6.41,20.83c-8.78,9.75-8.46,23.89.44,32.8L24.42,37.51C35.17,27.64,36,10,25.39,0,25.39,0,15.19,11.08,6.41,20.83Z"/></g></g></svg>',
    contextPath: '/',
    onboardingUrl: '/welcome/ops',
    dependencies: [],
    UMKey: 'JIRA',
    priceFreeLabel: priceFreeLabels.earlyAccess,
    pricingLink: null,
    promotable: false
  }
};

export default products;
