/* eslint-disable max-len */
import { Trans } from '@lingui/react';
import * as React from 'react';
import { tags } from './products';

const applications = {
  'bonfire.jira.ondemand': {
    name: 'Capture for Jira',
    tag: tags.projectManagement,
    publisher: 'Atlassian Software',
    description: (
      <Trans id="billing.products.jira-capture.description">
        Rapid bug reporting for exploratory and session-based testing.
      </Trans>
    ),
    link: 'https://www.atlassian.com/software/jira/capture',
    parentKey: 'jira.ondemand',
    outsideInstall: '/plugins/servlet/upm/marketplace/plugins/com.atlassian.bonfire.plugin'
  },
  'com.radiantminds.roadmaps-jira.ondemand': {
    name: 'Portfolio for Jira',
    tag: tags.projectManagement,
    publisher: 'Atlassian Software',
    description: (
      <Trans id="billing.products.jira-portfolio.description">
        Know when you can deliver, react to change and keep everyone on the same page.
      </Trans>
    ),
    link: 'https://marketplace.atlassian.com/plugins/com.radiantminds.roadmaps-jira',
    parentKey: 'jira.ondemand',
    outsideInstall: '/plugins/servlet/upm/marketplace/plugins/com.radiantminds.roadmaps-jira'
  },
  'com.atlassian.confluence.plugins.confluence-questions.ondemand': {
    name: 'Questions for Confluence',
    tag: tags.messaging,
    publisher: 'Atlassian Software',
    description: (
      <Trans id="billing.products.confluence-questions.description">
        Capture, learn from, and retain the collective knowledge of your organization as it grows. Ask questions, get answers, and identify experts.
      </Trans>
    ),
    link: 'https://www.atlassian.com/software/confluence/questions',
    parentKey: 'confluence.ondemand',
    outsideInstall: '/wiki/plugins/servlet/upm/marketplace/plugins/com.atlassian.confluence.plugins.confluence-questions'
  },
  'team.calendars.confluence.ondemand': {
    name: 'Team Calendars for Confluence',
    tag: tags.messaging,
    publisher: 'Atlassian Software',
    description: (
      <Trans id="billing.products.confluence-calendars.description">
        Your team's single source of truth for managing team leave, tracking Jira projects, and planning events.
      </Trans>
    ),
    link: 'https://www.atlassian.com/software/confluence/team-calendars',
    parentKey: 'confluence.ondemand',
    outsideInstall: '/wiki/plugins/servlet/upm/marketplace/plugins/com.atlassian.confluence.extra.team-calendars'
  }
};

export default applications;
