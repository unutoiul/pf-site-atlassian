import { Trans } from '@lingui/react';
import React from 'react';

const editions = [
  {
    name: 'Free',
    edition: 'free',
    cost: {
      monthly: 0,
      annual: 0,
      currency: 'usd'
    },
    features: [
      <Trans id="billing.product.editions.free.unlimited-users">Unlimited users</Trans>,
      <Trans id="billing.product.editions.free.unlimited-rooms">Unlimited group chat rooms</Trans>,
      <Trans id="billing.product.editions.free.unlimited-messages">Unlimited direct messaging</Trans>,
      <Trans id="billing.product.editions.free.unlimited-video">Unlimited group video meetings</Trans>,
      <Trans id="billing.product.editions.free.unlimited-voice">Unlimited voice meetings</Trans>,
      <Trans id="billing.product.editions.free.unlimited-tools">Built-in collaboration tools</Trans>,
    ],
    isCurrent: false,
    needsPaymentDetailsOnAccount: false,
  },
  {
    name: 'Standard',
    edition: 'standard',
    cost: {
      monthly: 3,
      annual: 30,
      currency: 'usd'
    },
    features: [
      <Trans id="billing.product.editions.standard.unlimited-share">Unlimited file sharing & storage</Trans>,
      <Trans id="billing.product.editions.standard.unlimited-apps">Unlimited apps and bots</Trans>,
      <Trans id="billing.product.editions.standard.unlimited-history">Unlimited message history</Trans>,
      <Trans id="billing.product.editions.standard.unlimited-screen">Group screen sharing</Trans>,
      <Trans id="billing.product.editions.standard.unlimited-remote">Remote desktop control</Trans>,
    ],
    isCurrent: true,
    needsPaymentDetailsOnAccount: true
  },
  {
    name: 'Premium',
    edition: 'premium',
    cost: {
      monthly: 200,
      annual: 2000,
      currency: 'usd'
    },
    features: ['Emojis', 'Cat pictures', 'blah', 'blah', 'blah'],
    isCurrent: false,
    needsPaymentDetailsOnAccount: true
  }
];

const productEditions = {
  'hipchat.cloud': editions
};

export default productEditions;
