import request from 'bux/common/helpers/request';
import config from '../config';

const BASE_URL = config.billingUXServiceUrl;

const nothing = () => ({});
const toJson = response => response.json();

export const appendHeadersToOptions = (options) => {
  const { org, cloudId, ...rest } = options || {};
  return { ...rest };
};

export const injectCloudParams = (options) => {
  const { org, cloudId } = options || {};
  if (org) {
    return `/organization/${org}`;
  }
  if (cloudId) {
    return `/cloud/${cloudId}`;
  }
  return '';
};

export const billingUxRequest = {
  get: (url, options) => request.get(`${BASE_URL}${injectCloudParams(options)}${url}`, appendHeadersToOptions(options)),
  post: (url, body, options) =>
    request.post(`${BASE_URL}${injectCloudParams(options)}${url}`, body, appendHeadersToOptions(options)),
  put: (url, body, options) =>
    request.put(`${BASE_URL}${injectCloudParams(options)}${url}`, body, appendHeadersToOptions(options))
};

/**
 * Billing UX service requests
 */
export const getBillEstimate = ({ options } = {}) => (
  billingUxRequest
    .get('/billing/bill-estimate', options)
    .then(toJson)
);

export const getBillingDetails = ({ options } = {}) => (
  billingUxRequest
    .get('/billing/billing-details', options)
    .then(toJson)
);

export const getBillHistory = ({ options } = {}) => (
  billingUxRequest
    .get('/billing/invoice-history', options)
    .then(toJson)
);

export const getEntitlementGroup = ({ options } = {}) => (
  billingUxRequest
    .get('/billing/entitlement-group', options)
    .then(toJson)
);

export const getEntitlementEdition = ({ entitlementId, options }) => (
  billingUxRequest
    .get(`/billing/entitlements/${entitlementId}/edition`, options)
    .then(toJson)
);

export const getUserMetadata = ({ options }) => (
  billingUxRequest
    .get('/metadata', options)
    .then(toJson)
);

export const getCountryList = () => (
  billingUxRequest
    .get('/api/countries')
    .then(toJson)
);

export const getRecommendedProducts = ({ options }) => (
  !options.cloudId
    ? {} 
    : billingUxRequest
      .get('/billing/recommended-products', options)
      .then(toJson)
);

export const getEditionChangeEstimate = ({ entitlementId, edition, options }) => (
  billingUxRequest
    .get(`/billing/entitlements/${entitlementId}/edition/${edition}/estimate`, options)
    .then(toJson)
);

export const requestConvertToAnnual = ({ options }) => (
  billingUxRequest
    .post('/billing/entitlements/annual-conversion-estimate', {}, options)
    .then(toJson)
);

export const getAnnualConversionEstimate = ({ options }) => (
  billingUxRequest
    .post('/billing/change-renewal-frequency-estimate/annual', undefined, options)
    .then(toJson)
);

export const requestConvertToAnnualQuote = ({ estimateId, options }) => (
  billingUxRequest
    .post('/billing/entitlements/annual-conversion-quote', {
      estimateId
    }, options)
    .then(toJson)
);

export const requestProcessOrder = ({ estimateId, paymentMethodType = 'PAYMENT_METHOD_ON_FILE', options }) => (
  billingUxRequest
    .post('/order', {
      estimateId,
      paymentMethodType
    }, options)
    .then(toJson)
);

export const requestProcessOrderProgress = ({ orderId, options }) => (
  billingUxRequest
    .get(`/order/${orderId}/progress`, options)
    .then(toJson)
);

// SETTERS

export const selectEdition = ({ entitlementId, edition, options }) => (
  billingUxRequest
    .put(`/billing/entitlements/${entitlementId}/edition/${edition}`, {}, options)
    .then(nothing)
);

export const updatePaymentDetails = ({ details, options }) => (
  billingUxRequest
    .put('/billing/billing-details', {
      firstName: '-',
      organisationName: details.organisationName,
      addressLine1: details.addressLine1,
      addressLine2: details.addressLine2,
      city: details.city,
      state: details.state,
      country: details.country,
      postcode: details.postcode,
      paymentMethod: details.paymentMethod,
      creditCard: details.creditCard,
      paypalAccount: details.paypalAccount,
      taxpayerId: details.taxpayerId
    }, options)
    .then(nothing)
);

export const updatePaymentMethod = ({
  paymentMethod, paypalAccount, creditCard, options
}) => (
  billingUxRequest
    .put('/billing/payment-method', {
      paymentMethod, paypalAccount, creditCard
    }, options)
    .then(() => ({}))
);

export const updateInvoiceContact = ({ invoiceContact, options }) => (
  billingUxRequest
    .put('/billing/invoice-contact', {
      email: invoiceContact.email || ''
    }, options)
    .then(nothing)
);

export const getBraintreeClientToken = ({ options } = {}) => (
  billingUxRequest
    .get('/api/braintree/clientToken', undefined, options)
    .then(response => response.json())
);

export const deactivate = ({ productKey, options }) => (
  billingUxRequest.post(`/billing/deactivate/${productKey}`, undefined, options)
    .then(nothing)
);

export const resubscribe = ({ productKeys, options }) => (
  billingUxRequest
    .post('/billing/resubscribe', {
      products: productKeys.map(productKey => ({ productKey }))
    }, options)
    .then(nothing)
);

export const updateBillingContact = ({ main = false, email, options }) => (
  billingUxRequest
    .put(`/billing/billing-contact?main=${main}`, {
      email
    }, options)
    .then(nothing)
);

export const createQuoteForEstimate = ({ estimateId, options }) => (
  billingUxRequest
    .post(`/billing/estimate/${estimateId}/quote`, undefined, options)
    .then(toJson)
);

// Not bound to Cloud/Org ids

export const createCreditCardSession = ({ options } = {}) => (
  billingUxRequest
    .post('/api/credit-card/session', undefined, options)
    .then(toJson)
);

export const saveCreditCardSession = ({ token, options }) => (
  billingUxRequest
    .post(`/api/credit-card/session/${token}/save`, undefined, options)
    .then(toJson)
    .then(({ creditCardSessionId, creditCardToken }) => ({
      creditCardSessionId,
      creditCardToken
    }))
);

export const validateBillingDetails = ({ country, options }) => (
  billingUxRequest
    .post('/billing/validate-billing-details', { country }, options)
    .then(toJson)
);

