import proxyquire from 'bux/helpers/proxyquire';
import { expect } from 'chai';
import { shouldDoGetRequest, shouldDoPutRequest, shouldDoPostRequest } from './standard-request';

const MOCK_SERVICE_URL = 'https://test-url';
const API_SERVICE_URL = `${MOCK_SERVICE_URL}/billing`;

const getBuxAPI = () => (
  proxyquire.noCallThru().load('../billing-ux-service', {
    '../config': { billingUXServiceUrl: MOCK_SERVICE_URL }
  })
);

describe('Billing UX service API: ', () => {
  describe('injectCloudParams', () => {
    it('should return empty object when there is no org param', () => {
      expect(getBuxAPI().injectCloudParams({})).to.be.empty();
    });
    it('should return Atl-OrgId when org is set', () => {
      expect(getBuxAPI().injectCloudParams({ org: '123' })).to.be.equal('/organization/123');
    });
    it('should return Atl-OrgId when org is set', () => {
      expect(getBuxAPI().injectCloudParams({ cloudId: '123' })).to.be.equal('/cloud/123');
    });
  });

  describe('appendHeadersToOptions', () => {
    it('should return undefined when there is no options', () => {
      expect(getBuxAPI().appendHeadersToOptions().queryParams).to.be.undefined();
    });
  });

  const servicePath = '/api/billing/bill-estimate';
  describe('billingUxRequest', () => {
    describe('get: ', () => {
      shouldDoGetRequest(
        `${MOCK_SERVICE_URL}${servicePath}`,
        () => getBuxAPI().billingUxRequest
          .get(servicePath)
          .then(response => response.json()),
        { response: 'data' }
      );
    });
    describe('get with org id header: ', () => {
      shouldDoGetRequest(
        `${MOCK_SERVICE_URL}/organization/123${servicePath}`,
        () => getBuxAPI().billingUxRequest
          .get(servicePath, { org: '123' })
          .then(response => response.json()),
        { response: 'data' },
      );
    });
    describe('get with cloud id header: ', () => {
      shouldDoGetRequest(
        `${MOCK_SERVICE_URL}/cloud/123${servicePath}`,
        () => getBuxAPI().billingUxRequest
          .get(servicePath, { cloudId: '123' })
          .then(response => response.json()),
        { response: 'data' },
      );
    });
    describe('put: ', () => {
      shouldDoPutRequest(
        `${MOCK_SERVICE_URL}${servicePath}`,
        () => getBuxAPI().billingUxRequest
          .put(servicePath, {})
          .then(response => response.json()),
        {},
        {}
      );
    });
    describe('put with org id header: ', () => {
      shouldDoPutRequest(
        `${MOCK_SERVICE_URL}/organization/123${servicePath}`,
        () => getBuxAPI().billingUxRequest
          .put(servicePath, {}, { org: '123' })
          .then(response => response.json()),
        {},
        {},
      );
    });
    describe('post: ', () => {
      shouldDoPostRequest(
        `${MOCK_SERVICE_URL}${servicePath}`,
        () => getBuxAPI().billingUxRequest
          .post(servicePath)
          .then(response => response.json()),
        undefined,
        {}
      );
    });
    describe('post with org id header: ', () => {
      shouldDoPostRequest(
        `${MOCK_SERVICE_URL}/organization/123${servicePath}`,
        () => getBuxAPI().billingUxRequest
          .post(servicePath, undefined, { org: '123' })
          .then(response => response.json()),
        undefined,
        {}
      );
    });
  });

  describe('getBillEstimate: ', () => {
    shouldDoGetRequest(
      `${API_SERVICE_URL}/bill-estimate`,
      () => getBuxAPI().getBillEstimate(),
      { response: 'data' }
    );
  });

  describe('getBillingDetails: ', () => {
    shouldDoGetRequest(
      `${API_SERVICE_URL}/billing-details`,
      () => getBuxAPI().getBillingDetails(),
      { response: 'data' }
    );
  });

  describe('getBillHistory: ', () => {
    shouldDoGetRequest(
      `${API_SERVICE_URL}/invoice-history`,
      () => getBuxAPI().getBillHistory(),
      { response: 'data' }
    );
  });

  describe('getEntitlementGroup: ', () => {
    shouldDoGetRequest(
      `${API_SERVICE_URL}/entitlement-group`,
      () => getBuxAPI().getEntitlementGroup(),
      { response: 'data' }
    );
  });

  describe('selectEdition: ', () => {
    shouldDoPutRequest(
      `${API_SERVICE_URL}/entitlements/123/edition/newEdition`,
      () => getBuxAPI().selectEdition({ entitlementId: 123, edition: 'newEdition' }),
      {},
      {}
    );
  });

  describe('getEntitlementEdition: ', () => {
    shouldDoGetRequest(
      `${API_SERVICE_URL}/entitlements/42/edition`,
      () => getBuxAPI().getEntitlementEdition({ entitlementId: 42 }),
      { response: 'data' }
    );
  });

  describe('getEditionChangeEstimate:', () => {
    shouldDoGetRequest(
      `${API_SERVICE_URL}/entitlements/42/edition/standard/estimate`,
      () => getBuxAPI().getEditionChangeEstimate({ entitlementId: 42, edition: 'standard' }),
      { response: 'data' }
    );
  });

  describe('getAnnualConversionEstimate:', () => {
    shouldDoPostRequest(
      `${API_SERVICE_URL}/change-renewal-frequency-estimate/annual`,
      () => getBuxAPI().getAnnualConversionEstimate({}),
      undefined,
      { response: 'data' }
    );
  });

  describe('createQuoteForEstimate:', () => {
    shouldDoPostRequest(
      `${API_SERVICE_URL}/estimate/1337/quote`,
      () => getBuxAPI().createQuoteForEstimate({ estimateId: 1337, edition: 'standard' }),
      undefined,
      {
        response: 'data'
      }
    );
  });

  describe('updatePaymentDetails: ', () => {
    shouldDoPutRequest(
      `${API_SERVICE_URL}/billing-details`,
      () => getBuxAPI().updatePaymentDetails({
        details: {
          organisationName: 'Potato Corp.',
          addressLine1: 'Level 6',
          city: 'Sydney',
          postcode: '2000',
          state: 'NSW',
          country: 'AU',
          taxpayerId: '123456782',
          fieldToIgnore: true,
          creditCard: {
            token: '123123123',
            sessionId: 'SESSION',
            name: 'Michael Scott',
            expiryDate: 9,
            expiryYear: 2019,
            type: 'VISA'
          }
        }
      }, '0123456789'),
      {
        organisationName: 'Potato Corp.',
        firstName: '-',
        addressLine1: 'Level 6',
        city: 'Sydney',
        postcode: '2000',
        state: 'NSW',
        country: 'AU',
        taxpayerId: '123456782',
        creditCard: {
          token: '123123123',
          sessionId: 'SESSION',
          name: 'Michael Scott',
          expiryDate: 9,
          expiryYear: 2019,
          type: 'VISA'
        }
      },
      {}
    );
  });

  describe('updatePaymentMethod', () => {
    shouldDoPutRequest(
      `${API_SERVICE_URL}/payment-method`,
      () => getBuxAPI().updatePaymentMethod({
        paymentMethod: 'PAYPAL_ACCOUNT',
        paypalAccount: {
          email: 'paypal@email.com',
          nonce: '4d983dNONCE'
        }
      }),
      {
        paymentMethod: 'PAYPAL_ACCOUNT',
        paypalAccount: {
          email: 'paypal@email.com',
          nonce: '4d983dNONCE',
        }
      },
      {}
    );
  });

  describe('updateInvoiceContact: ', () => {
    shouldDoPutRequest(
      `${API_SERVICE_URL}/invoice-contact`,
      () => getBuxAPI().updateInvoiceContact({
        invoiceContact: {
          email: 'bob@example.com'
        }
      }),
      {
        email: 'bob@example.com'
      },
      {}
    );
  });

  describe('createCreditCardSession: ', () => {
    shouldDoPostRequest(
      `${MOCK_SERVICE_URL}/api/credit-card/session`,
      () => getBuxAPI().createCreditCardSession(),
      undefined,
      {
        creditCardSessionId: 'SESSION000000000000000000000001'
      }
    );
  });

  describe('saveCreditCardSession: ', () => {
    shouldDoPostRequest(
      `${MOCK_SERVICE_URL}/api/credit-card/session/SESSION000000000000000000000001/save`,
      () => getBuxAPI().saveCreditCardSession({ token: 'SESSION000000000000000000000001' }),
      undefined,
      {
        creditCardSessionId: 'SESSION000000000000000000000001',
        creditCardToken: '5123450000002346'
      }
    );
  });

  describe('getBraintreeClientToken', () => {
    shouldDoGetRequest(
      `${MOCK_SERVICE_URL}/api/braintree/clientToken`,
      () => getBuxAPI().getBraintreeClientToken(),
      {
        token: 'eyJ2ZXJzaW9uIjoyLCJTOKEN'
      },
      {}
    );
  });

  describe('deactivate', () => {
    shouldDoPostRequest(
      `${API_SERVICE_URL}/deactivate/product.key`,
      () => getBuxAPI().deactivate({ productKey: 'product.key' }),
      undefined,
      {}
    );
  });

  describe('resubscribe', () => {
    shouldDoPostRequest(
      `${API_SERVICE_URL}/resubscribe`,
      () => getBuxAPI().resubscribe({ productKeys: ['product1.key', 'product2.key'] }),
      {
        products: [
          { productKey: 'product1.key' },
          { productKey: 'product2.key' }
        ]
      },
      {}
    );
  });

  describe('updateBillingContact', () => {
    shouldDoPutRequest(
      `${API_SERVICE_URL}/billing-contact?main=false`,
      () => getBuxAPI().updateBillingContact({ email: 'email' }),
      {
        email: 'email',
      },
      {}
    );
    shouldDoPutRequest(
      `${API_SERVICE_URL}/billing-contact?main=true`,
      () => getBuxAPI().updateBillingContact({ main: true, email: 'email' }),
      {
        email: 'email',
      },
      {}
    );
  });
});
