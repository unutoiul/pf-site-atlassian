import fetchMock from 'fetch-mock-5';
import { expect } from 'chai';


const getRequestBody = (request) => {
  if (request.body) {
    return JSON.parse(request.body);
  }
  return undefined;
};

export const shouldDoGetRequest = (url, doRequest, responseData, expectedHeader, statusCode = 200) => (
  describe('request: ', () => {
    beforeEach(() => {
      fetchMock.restore();
      fetchMock.get(url, { body: responseData, status: statusCode });
    });

    it('should set the request method to GET', () => (
      doRequest().then(() =>
        expect(fetchMock.lastOptions(url).method).to.equal('GET'))
    ));

    it('should send the cookies with the request', () => (
      doRequest().then(() =>
        expect(fetchMock.lastOptions(url).credentials).to.equal('include'))
    ));

    it('should return response as JSON', () => (
      doRequest().then(response =>
        expect(response).to.deep.equal(responseData))
    ));

    if (expectedHeader) {
      it('should set request header from options', () => (
        doRequest().then(() =>
          expect(fetchMock.lastOptions(url).headers).to.include(expectedHeader))
      ));
    }
  })
);

export const shouldDoPutRequest = (url, doRequest, expectedRequest, responseData, expectedHeader) => (
  describe('request: ', () => {
    beforeEach(() => {
      fetchMock.restore();
      fetchMock.put(url, responseData);
    });

    it('should set the request method to PUT', () => (
      doRequest().then(() => {
        expect(fetchMock.lastOptions(url).method).to.equal('PUT');
      })
    ));

    it('should send the cookies with the request', () => (
      doRequest().then(() => {
        expect(fetchMock.lastOptions(url).credentials).to.equal('include');
      })
    ));

    it('should set the request body from given arguments', () => (
      doRequest().then(() => {
        expect(fetchMock.called(url)).to.be.true();
        expect(fetchMock.calls().unmatched.length).to.equal(0);
        const actualRequest = fetchMock.lastOptions(url);
        expect(getRequestBody(actualRequest)).to.deep.equal(expectedRequest);
      })
    ));

    it('should return response as JSON', () => (
      doRequest().then((response) => {
        expect(response).to.deep.equal(responseData);
      })
    ));

    if (expectedHeader) {
      it('should set request header from options', () => (
        doRequest().then(() =>
          expect(fetchMock.lastOptions(url).headers).to.include(expectedHeader))
      ));
    }
  })
);


export const shouldDoPostRequest = (url, doRequest, expectedRequest, responseData, expectedHeader) => (
  describe('request: ', () => {
    beforeEach(() => {
      fetchMock.restore();
      fetchMock.post(url, responseData);
    });

    it('should set the request method to POST', () => (
      doRequest().then(() => {
        expect(fetchMock.lastOptions(url).method).to.equal('POST');
      })
    ));

    it('should send the cookies with the request', () => (
      doRequest().then(() => {
        expect(fetchMock.lastOptions(url).credentials).to.equal('include');
      })
    ));

    it('should set the request body from given arguments', () => (
      doRequest().then(() => {
        expect(fetchMock.called(url)).to.be.true();
        expect(fetchMock.calls().unmatched.length).to.equal(0);
        const actualRequest = fetchMock.lastOptions(url);
        expect(getRequestBody(actualRequest)).to.deep.equal(expectedRequest);
      })
    ));

    it('should return response as JSON', () => (
      doRequest().then((response) => {
        expect(response).to.deep.equal(responseData);
      })
    ));

    if (expectedHeader) {
      it('should set request header from options', () => (
        doRequest().then(() =>
          expect(fetchMock.lastOptions(url).headers).to.include(expectedHeader))
      ));
    }
  })
);
