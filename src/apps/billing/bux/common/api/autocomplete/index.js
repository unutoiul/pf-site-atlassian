import { setup as setupAddressComplete } from '@atlassian/address-complete';
import countrySet from './data-set';
import request from '../../helpers/request'; // location is relative for contract test
import config from '../../config';

const requestFacade = url =>
  request.get(url)
    .then(result => result.json())
    .catch(err => err);

export const createAddressComplete = options => setupAddressComplete(countrySet, {
  request: requestFacade,
  mapzen_apikey: config.mapzen_apikey,

  provider: config.autocomplete && config.autocomplete.provider,
  ...(config.autocomplete ? config.autocomplete : {}),
  ...options
});

const autoCompleter = createAddressComplete();

export const getCountries = autoCompleter.getCountries;
export const getRegionsFor = autoCompleter.getRegionsFor;
export const getDetails = autoCompleter.getDetails;
export const getBadge = autoCompleter.getBadge;

export const getAutoComplete = () => (
  config.autocomplete ? autoCompleter.getAutoComplete() : null
);

