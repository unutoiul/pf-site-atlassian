import { combine, getDataSet } from 'iso3166-2-db/api';
import countryList from 'iso3166-2-db/countryList/en';

import US from 'iso3166-2-db/regions/US/en_ref';
import AU from 'iso3166-2-db/regions/AU/en_ref';
import CA from 'iso3166-2-db/regions/CA/en_ref';
import RU from 'iso3166-2-db/regions/RU/en_ref';
import IN from 'iso3166-2-db/regions/IN/en_ref';

const countrySet = getDataSet('en', combine(countryList, {
  US, CA, AU, RU, IN
}));

export default countrySet;