import { expect } from 'chai';
import { getRegionsFor, getCountries } from '..';


describe('Address information', () => {
  describe('World configuration', () => {
    it('should contain main countries', () => {
      const countries = getCountries();

      expect(countries.filter(country => country.iso === 'AU')).to.have.length(1);
      expect(countries.filter(country => country.iso === 'US')).to.have.length(1);
      expect(countries.filter(country => country.iso === 'CA')).to.have.length(1);
    });

    it('should return states of AU/US/IN', () => {
      expect(Object.keys(getRegionsFor('AU'))).to.have.length(8);
      expect(Object.keys(getRegionsFor('US'))).to.have.length(51); // 50 states + Washington D.C.
      expect(Object.keys(getRegionsFor('IN'))).to.have.length(36);
    });
  });
});
