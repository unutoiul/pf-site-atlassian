import React from 'react';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';
import PageHeader from '../PageHeader';

describe('PageHeader component', () => {
  it('should render title', () => {
    const wrapper = shallow(<PageHeader title="Some title" subTitle="" />);
    expect(wrapper.find('.page-title')).to.contain.text('Some title');
  });

  it('should render subtitle', () => {
    const wrapper = mount(<PageHeader title="" subTitle="Subtitle description" />);
    expect(wrapper.find('.page-subtitle')).to.contain.text('Subtitle description');
  });
});
