import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';

import { PageHeader as AkPageHeader } from 'common/responsive/PageHeader';

import styles from './PageHeader.less';

const Subtitle = ({ children }) => (<div className={cx('page-subtitle', styles.subTitle)}>{children}</div>);
Subtitle.propTypes = {
  children: PropTypes.node.isRequired,
};

const PageHeader = ({ title, subTitle, breadcrumbs }) => (
  <AkPageHeader
    bottomBar={subTitle && <Subtitle>{subTitle}</Subtitle>}
    breadcrumbs={breadcrumbs}
  >
    <div className="page-title">
      {title}
    </div>
  </AkPageHeader>
);

PageHeader.propTypes = {
  title: PropTypes.node.isRequired,
  subTitle: PropTypes.node,
  breadcrumbs: PropTypes.node,
};

export default PageHeader;
