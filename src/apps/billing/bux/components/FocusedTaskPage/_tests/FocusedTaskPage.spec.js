import React from 'react';
import sinon from 'sinon';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';
import FocusedTaskPage from '../FocusedTaskPage';

const open = sinon.stub();
const exit = sinon.stub();

const DEFAULT_PROPS = {
  open,
  exit,
  isFocusedTaskOpen: false,
  isAccessible: false
};

const Component = props => (<FocusedTaskPage {...DEFAULT_PROPS} {...props} />);

describe('FocusedTaskPage: ', () => {
  beforeEach(() => {
    open.reset();
    exit.reset();
  });

  it('should render component', () => {
    const wrapper = shallow(<Component />);
    expect(wrapper).to.be.present();
  });

  it('should call open method when is accessible', () => {
    const wrapper = mount(<Component isAccessible />);
    expect(wrapper).to.be.present();
    expect(DEFAULT_PROPS.open).to.be.called();
  });

  it('should call exit method when is not accessible', () => {
    const wrapper = mount(<Component isAccessible={false} />);
    expect(wrapper).to.be.present();
    expect(DEFAULT_PROPS.exit).to.be.called();
  });

  it('should call exit when receives empty focusedTask', () => {
    const wrapper = mount(<Component isAccessible isFocusedTaskOpen />);
    expect(wrapper).to.be.present();
    wrapper.setProps({ isFocusedTaskOpen: false });
    expect(DEFAULT_PROPS.exit).to.be.called();
  });

  it('should not call exit when receives other props', () => {
    const wrapper = mount(<Component isAccessible isFocusedTaskOpen />);
    expect(wrapper).to.be.present();
    wrapper.setProps({ isAccessible: false });
    expect(DEFAULT_PROPS.exit).to.not.be.called();
  });

  it('should call exit when component unmounts with focusedTask', () => {
    const wrapper = mount(<Component isAccessible isFocusedTaskOpen />);
    expect(wrapper).to.be.present();
    wrapper.unmount();
    expect(DEFAULT_PROPS.exit).to.be.called();
  });

  it('should not call exit when component unmounts without focusedTask', () => {
    const wrapper = mount(<Component isAccessible />);
    expect(wrapper).to.be.present();
    wrapper.unmount();
    expect(DEFAULT_PROPS.exit).to.not.be.called();
  });
});
