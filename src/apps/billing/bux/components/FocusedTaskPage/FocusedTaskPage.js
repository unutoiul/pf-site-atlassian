import { PureComponent } from 'react';
import { func, bool } from 'prop-types';

export default class FocusedTaskPage extends PureComponent {
  static propTypes = {
    open: func.isRequired,
    exit: func.isRequired,
    isFocusedTaskOpen: bool.isRequired,
    isAccessible: bool.isRequired
  }

  componentDidMount() {
    const {
      open, exit, isAccessible, ...rest 
    } = this.props;
    if (isAccessible) {
      open(rest);
    } else {
      exit();
    }
  }

  componentWillReceiveProps({ isFocusedTaskOpen }) {
    if (!isFocusedTaskOpen) {
      this.props.exit();
    }
  }

  componentWillUnmount() {
    const { exit, isFocusedTaskOpen } = this.props;
    if (isFocusedTaskOpen) {
      exit();
    }
  }

  render() {
    return null;
  }
}
