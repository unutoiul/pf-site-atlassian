import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import PaymentTypeVisaLogo from 'bux/static/payment-visa.svgx';
import PaymentTypeMasterCardLogo from 'bux/static/payment-mastercard.svgx';
import PaymentTypeAmexLogo from 'bux/static/payment-amex.svgx';
import PaymentTypePaypalLogo from 'bux/static/payment-paypal.svgx';
import PaymentTypeDefaultLogo from 'bux/static/payment-default.svgx';

import PaymentTypeLogo from '../PaymentTypeLogo';

describe('PaymentTypeLogo component', () => {
  it('should render visa', () => {
    const wrapper = shallow(<PaymentTypeLogo type="visa" />);
    expect(wrapper).to.contain(<PaymentTypeVisaLogo />);
  });

  it('should render mastercard', () => {
    const wrapper = shallow(<PaymentTypeLogo type="mastercard" />);
    expect(wrapper).to.contain(<PaymentTypeMasterCardLogo />);
  });

  it('should render amex', () => {
    const wrapper = shallow(<PaymentTypeLogo type="amex" />);
    expect(wrapper).to.contain(<PaymentTypeAmexLogo />);
  });

  it('should render paypal', () => {
    const wrapper = shallow(<PaymentTypeLogo type="paypal" />);
    expect(wrapper).to.contain(<PaymentTypePaypalLogo />);
  });

  it('should render default', () => {
    const wrapper = shallow(<PaymentTypeLogo type="discover" />);
    expect(wrapper).to.contain(<PaymentTypeDefaultLogo />);
  });
});
