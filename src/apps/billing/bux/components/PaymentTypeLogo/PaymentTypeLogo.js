import PropTypes from 'prop-types';
import React from 'react';
import PaymentTypeVisaLogo from 'bux/static/payment-visa.svgx';
import PaymentTypeMasterCardLogo from 'bux/static/payment-mastercard.svgx';
import PaymentTypeAmexLogo from 'bux/static/payment-amex.svgx';
import PaymentTypeDefaultLogo from 'bux/static/payment-default.svgx';
import PaymentTypePaypalLogo from 'bux/static/payment-paypal.svgx';
import resizeSVG from 'bux/common/helpers/resizeSVG';

const paypalLogoHeight = 22;
const PaypalResized = resizeSVG(PaymentTypePaypalLogo, paypalLogoHeight);

const PaymentTypeLogo = ({ type = 'default' }) => {
  switch (type.toLowerCase()) {
    case 'visa':
      return <PaymentTypeVisaLogo />;
    case 'amex':
      return <PaymentTypeAmexLogo />;
    case 'mastercard':
      return <PaymentTypeMasterCardLogo />;
    case 'paypal':
      return <PaypalResized />;
    default:
      return <PaymentTypeDefaultLogo />;
  }
};

PaymentTypeLogo.propTypes = {
  type: PropTypes.string
};

export default PaymentTypeLogo;
