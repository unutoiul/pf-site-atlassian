import { Trans } from '@lingui/react';
import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';
import { ExternalLinkTo } from 'bux/components/Link';

import styles from './WidgetError.less';

const WidgetError = ({ errorMessage, errorTitle }) => (
  <div className={cx('aui-message', 'aui-message-error', 'widget-error', styles.widget)}>
    <p className="title">
      <strong>
        {errorTitle ||
          <Trans id="billing.widget.error.title.default">Error!</Trans>
        }
      </strong>
    </p>
    <p>
      {errorMessage ||
      <Trans id="billing.widget.error.message.default">
        Something went wrong
      </Trans>
      }
    </p>
    <p>
      <ExternalLinkTo.Support>
        <Trans id="billing.widget.error.contact-support">
          Contact customer support
        </Trans>
      </ExternalLinkTo.Support>
    </p>
  </div>
);

WidgetError.propTypes = {
  errorTitle: PropTypes.string,
  errorMessage: PropTypes.string
};

export default WidgetError;
