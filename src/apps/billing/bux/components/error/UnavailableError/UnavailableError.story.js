import { storiesOf } from '@storybook/react';
import { render, inContentChrome, inStoryKnob } from 'bux/common/helpers/storybook';

import UnavailableError, { MissingPaymentError, UnknownReason } from './UnavailableError';

storiesOf('BUX|Components/error/UnavailableError', module)
  .add('MissingPaymentError [Prompt]', () => inContentChrome(render(MissingPaymentError)))
  .add('UnknownReason [Prompt]', () => inContentChrome(render(UnknownReason)))
  .add('UnavailableError [Prompt]', () => inContentChrome(inStoryKnob(UnavailableError, {
    error: {
      errorKey: 'service.account.deactivated'
    }
  })));

