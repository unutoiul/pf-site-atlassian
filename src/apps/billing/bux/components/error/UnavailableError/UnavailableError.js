import { Trans } from '@lingui/react';
import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';
import errorLogo from 'bux/static/svg/error.svg';
import { ExternalLinkTo } from 'bux/components/Link';
import Button from 'bux/components/Button';
import { paymentDetailsMacUri } from 'bux/common/helpers/uribuilder';

import styles from './UnavailableError.less';

export const UnknownReason = ({ onRetry }) => (
  <div className={cx('unknown-reason', styles.description)}>

    <h1 className={styles.heading}>
      <Trans id="billing.error.unavailable.header">
        We can't load this page right now
      </Trans>
    </h1>

    {onRetry &&
    <div className={styles.errorActionButton}>
      <Button primary onClick={onRetry}>
        <Trans id="billing.error.unavailable.retry">
          Retry
        </Trans>
      </Button>
    </div>}

    <p className={styles.support}>
      <Trans id="billing.error.unavailable.try-later">
        Check back soon or contact <ExternalLinkTo.BoldSupport>customer support</ExternalLinkTo.BoldSupport>
      </Trans>
    </p>
  </div>
);

UnknownReason.propTypes = {
  onRetry: PropTypes.func,
};

export const MissingPaymentError = ({ cloudId }) => (
  <div className={cx('payment-required', styles.description)}>
    <h1 className={styles.heading}>
      <Trans id="billing.error.deactivated.header">
        Your instance is deactivated.
      </Trans>
    </h1>

    <div className={styles.errorActionButton}>
      <Button href={paymentDetailsMacUri(cloudId)} target="_blank" primary>
        <Trans id="billing.error.deactivated.add-billing-details">
          Add billing details
        </Trans>
      </Button>
    </div>

    <p className={styles.support}>
      <Trans id="billing.error.deactivated.instruction">
        Due to missing billing details your instance has been deactivated. <br />
        If you would like to reactivate your instance, please provide billing details.
      </Trans>
    </p>
  </div>
);

MissingPaymentError.propTypes = {
  cloudId: PropTypes.string,
};

const UnavailableError = ({ onRetry, error, cloudId }) => (
  <div className={cx('error', 'unavailable', styles.content)}>
    <img src={errorLogo} className={styles.errorLogo} alt="Error" />

    {error && error.errorKey === 'service.account.deactivated'
      ? <MissingPaymentError cloudId={cloudId} />
      : <UnknownReason onRetry={onRetry} />
    }
  </div>
);

UnavailableError.propTypes = {
  onRetry: PropTypes.func,
  error: PropTypes.any,
  cloudId: PropTypes.string,
};

export default UnavailableError;
