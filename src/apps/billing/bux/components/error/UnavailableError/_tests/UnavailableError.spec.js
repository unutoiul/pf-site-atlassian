import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import { ExternalLinkTo } from 'bux/components/Link';
import Button from 'bux/components/Button';
import UnavailableError, { MissingPaymentError, UnknownReason } from '../UnavailableError';

describe('UnavailableError component: ', () => {
  it('Should render UnknownReason when no error is defined ', () => {
    const wrapper = shallow(<UnavailableError />);
    expect(wrapper.find(UnknownReason)).to.have.length(1);
  });

  it('Should render UnknownReason when error has no specif reason ', () => {
    const wrapper = shallow(<UnavailableError error={{ response: { status: 400, message: 'some error' } }} />);
    expect(wrapper.find(UnknownReason)).to.have.length(1);
  });

  it('Should render MissingPaymentError when error has PaymentRequired status ', () => {
    const wrapper = shallow(<UnavailableError error={{ errorKey: 'service.account.deactivated' }} />);
    expect(wrapper.find(MissingPaymentError)).to.have.length(1);
  });

  describe('for UnknownReason compnent: ', () => {
    it('Should render support link', () => {
      const wrapper = shallow(<UnknownReason />);
      expect(wrapper.find(ExternalLinkTo.BoldSupport)).to.have.length(1);
    });

    it('Should hide retry link if no retry function is provided', () => {
      const wrapper = shallow(<UnknownReason />);
      expect(wrapper.find(Button)).not.to.be.present();
    });

    it('Should show retry link when onRetry is present', () => {
      const wrapper = shallow(<UnknownReason onRetry={() => {}} />);
      expect(wrapper.find(Button)).to.be.present();
      expect(wrapper.find(Button).children().text()).to.be.equal('Retry');
    });

    it('Should show call onRetry when retry button clicked', () => {
      const onRetry = sinon.stub();
      const wrapper = shallow(<UnknownReason onRetry={onRetry} />);
      wrapper.find(Button).simulate('click');
      expect(onRetry).to.be.called();
    });
  });
});
