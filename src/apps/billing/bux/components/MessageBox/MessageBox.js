import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import styles from './MessageBox.less';

const getStyles = (warn) => {
  if (warn) {
    return styles.warnBox;
  }

  return styles.infoBox;
};

const MessageBox = ({ warn, dataTest, children }) => (
  <div
    className={cx(styles.messageBox, getStyles(warn))}
    data-test={dataTest}
  >
    {children}
  </div>
);

MessageBox.propTypes = {
  warn: PropTypes.bool,
  dataTest: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.arrayOf(PropTypes.node)])
};

export default MessageBox;