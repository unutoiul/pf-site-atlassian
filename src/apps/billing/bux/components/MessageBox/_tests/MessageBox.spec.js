import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import MessageBox from '../MessageBox';

describe('MessageBox component', () => {
  it('should render content', () => {
    const wrapper = mount(<MessageBox>
      <div id="a">42</div>
      <div id="b">42</div>
    </MessageBox>);
    expect(wrapper.find('#a').exists()).to.be.true();
    expect(wrapper.find('#b').exists()).to.be.true();
  });

  it('should render warn', () => {
    const wrapper = mount(<MessageBox warn>
      <div id="a">42</div>
    </MessageBox>);
    expect(wrapper.find('#a').exists()).to.be.true();
    expect(wrapper).to.have.prop('warn');
  });
});