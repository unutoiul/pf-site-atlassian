import PropTypes from 'prop-types';
import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';
import { connect } from 'react-redux';
import createStore from 'bux/core/createStore';
import { formValueSelector, Form } from 'redux-form';
import { enchantedForm } from '../index';

describe('Enchanted Form: ', () => {
  const store = createStore();

  const mapStateToPropsProvider = () => (
    () => ({})
  );

  const createWrapper = () => {
    const FormWrapper = ({ handleSubmit, onSubmit }) => (
      <Form onSubmit={handleSubmit(onSubmit)}>
        <button id="something">Submit</button>
      </Form>
    );

    FormWrapper.propTypes = {
      handleSubmit: PropTypes.func.isRequired, // Autowired by redux-forms
      onSubmit: PropTypes.func.isRequired,
      fieldsComponent: PropTypes.func
    };

    const selector = formValueSelector('FormWrapper');
    const mapStateToProps = mapStateToPropsProvider(selector);

    const DecoratedFormWrapper = enchantedForm({ form: 'FormWrapper' })(FormWrapper);
    const ConnectedFormWrapper = connect(mapStateToProps)(DecoratedFormWrapper);

    return ConnectedFormWrapper;
  };

  it('should trigger event on submit', () => {
    const onSubmit = sinon.stub();
    const sendAnalyticEvent = sinon.stub();
    const FormWrapper = createWrapper();

    const wrapper = mount(<FormWrapper store={store} onSubmit={onSubmit} />, {
      context: {
        sendAnalyticEvent
      },
      childContextTypes: {
        sendAnalyticEvent: PropTypes.func
      }
    });
    wrapper.find('form').simulate('submit');

    sinon.assert.called(onSubmit);
    expect(sendAnalyticEvent).to.have.been.calledWithMatch(['FormWrapper', 'submit'], { timeSpent: 0 });
  });
});
