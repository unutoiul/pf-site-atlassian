/**
 * @fileOverview helper for redux-form creation.
 * Add some logging inside
 */
import { reduxForm } from 'redux-form';
import connectLogRoot from '../log/root';
import connectLogForm from '../log/form';

/**
 * Drop-in replacement for redux-form
 * @name enchantedForm
 * @function
 * @param options
 */
const enchantedForm = options => (component) => {
  const Form = reduxForm(options)(component);
  const DecoratedForm = connectLogForm(Form, options);
  return connectLogRoot(options.form)(DecoratedForm);
};

export { enchantedForm };
