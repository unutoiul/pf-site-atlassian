import PropTypes from 'prop-types';
import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow, mount } from 'enzyme';
import { Provider } from 'bux/helpers/redux';
import createStore from 'bux/core/createStore';
import Loading from '../../Loading/delayed';
import UnavailableError from '../../error/UnavailableError';
import ErrorIndicator from '../../deferred/ErrorIndicator';

import withLoader from '../withLoader';

describe('withLoader HoC: ', () => {
  const PayloadComponent = ({ text = 'ready' }) => (<span>{text}</span>);
  PayloadComponent.propTypes = {
    text: PropTypes.string
  };

  it('should render base payload', () => {
    const wrapper = shallow(<PayloadComponent />);
    expect(wrapper).to.contain.text('ready');
  });

  it('should render loading', () => {
    const WrappedComponent = withLoader(
      () => ({ loading: true }),
      () => () => ({})
    )(PayloadComponent);
    const wrapper = mount(<Provider store={createStore()}><WrappedComponent /></Provider>);
    expect(wrapper.find(PayloadComponent)).not.to.be.present();
    expect(wrapper.find(Loading)).to.be.present();
  });

  it('should render payload while loading', () => {
    const WrappedComponent = withLoader(
      () => ({ loading: true, display: true }),
      () => () => ({})
    )(PayloadComponent);
    const wrapper = mount(<Provider store={createStore()}><WrappedComponent /></Provider>);
    expect(wrapper.find(PayloadComponent)).to.be.present();
    expect(wrapper.find(Loading)).not.to.be.present();
  });

  it('should accept boolean true as display: true', () => {
    const WrappedComponent = withLoader(
      () => true,
      () => () => ({})
    )(PayloadComponent);
    const wrapper = mount(<Provider store={createStore()}><WrappedComponent /></Provider>);
    expect(wrapper.find(PayloadComponent)).to.be.present();
  });

  it('should accept boolean false as loading: true', () => {
    const WrappedComponent = withLoader(
      () => false,
      () => () => ({})
    )(PayloadComponent);
    const wrapper = mount(<Provider store={createStore()}><WrappedComponent /></Provider>);
    expect(wrapper.find(Loading)).to.be.present();
  });

  it('should work without mount action, and not pass onRetry to error', () => {
    const WrappedComponent = withLoader(
      () => ({ error: true }),
      undefined
    )(PayloadComponent);
    const wrapper = mount(<Provider store={createStore()}><WrappedComponent /></Provider>);
    expect(wrapper.find(UnavailableError)).to.be.present();
    expect(wrapper.find(UnavailableError).props().onRetry).to.be.equal(undefined);
  });

  it('should work with mount action, and pass onRetry to error', () => {
    const WrappedComponent = withLoader(
      () => ({ error: true }),
      () => () => ({})
    )(PayloadComponent);
    const wrapper = mount(<Provider store={createStore()}><WrappedComponent /></Provider>);
    expect(wrapper.find(UnavailableError)).to.be.present();
    expect(wrapper.find(UnavailableError).props().onRetry).not.to.be.equal(undefined);
  });

  it('should render payload', () => {
    const WrappedComponent = withLoader(
      () => ({ loading: false, display: true }),
      () => () => ({})
    )(PayloadComponent);
    const wrapper = mount(<Provider store={createStore()}><WrappedComponent /></Provider>);
    expect(wrapper.find(PayloadComponent)).to.be.present();
    expect(wrapper.find(Loading)).not.to.be.present();
  });

  it('should call meta selector', () => {
    const meta = sinon.spy();
    const WrappedComponent = withLoader(
      (state, props) => meta(state, props),
      () => () => ({})
    )(PayloadComponent);
    const store = createStore();
    mount(<Provider store={store}><WrappedComponent someProp="42" /></Provider>);
    expect(meta).to.be.calledWithMatch(store.getState(), { someProp: '42' });
  });

  it('should call mount action', () => {
    const mountAction = sinon.spy();
    const WrappedComponent = withLoader(
      () => ({ loading: false }),
      (props) => {
        mountAction(props);
        return () => {
        };
      }
    )(PayloadComponent);
    mount(<Provider store={createStore()}><WrappedComponent someProp="42" /></Provider>);
    expect(mountAction).to.be.calledWithMatch({ someProp: '42' });
  });

  it('should work without mount action', () => {
    const WrappedComponent = withLoader(() => ({ loading: false }))(PayloadComponent);
    mount(<Provider store={createStore()}><WrappedComponent someProp="42" /></Provider>);
    // error was not thrown
  });

  it('should call pass props', () => {
    const WrappedComponent = withLoader(
      () => ({ loading: false, display: true }),
      () => () => ({})
    )(PayloadComponent);
    const wrapper = mount(<Provider store={createStore()}><WrappedComponent text="passed" /></Provider>);
    expect(wrapper.find(PayloadComponent).props().text).to.be.equal('passed');
  });

  it('should render ErrorComponent in case of error', () => {
    const ErrorComponent = () => (<div>error</div>);
    const WrappedComponent = withLoader(
      () => ({ error: true }),
      () => () => ({})
    )(PayloadComponent, ErrorComponent);
    const wrapper = mount(<Provider store={createStore()}><WrappedComponent /></Provider>);
    expect(wrapper.find(PayloadComponent)).not.to.be.present();
    expect(wrapper.find(ErrorComponent)).to.be.present();
  });

  it('should render LoadingComponent when nor error, nor display', () => {
    const ErrorComponent = () => (<div>error</div>);
    const WrappedComponent = withLoader(
      () => ({}),
      () => () => ({})
    )(PayloadComponent, ErrorComponent);
    const wrapper = mount(<Provider store={createStore()}><WrappedComponent /></Provider>);
    expect(wrapper.find(PayloadComponent)).not.to.be.present();
    expect(wrapper.find(Loading)).to.be.present();
  });

  it('should render UnavailableError', () => {
    const WrappedComponent = withLoader(
      () => ({ error: true, display: false }),
      () => () => ({})
    )(PayloadComponent);
    const wrapper = mount(<Provider store={createStore()}><WrappedComponent text="passed" /></Provider>);
    expect(wrapper.find(PayloadComponent)).not.to.be.present();
    expect(wrapper.find(UnavailableError)).to.be.present();
  });

  it('should render Payload is after error it is still displayable', () => {
    const WrappedComponent = withLoader(
      () => ({ error: true, display: true }),
      () => () => ({})
    )(PayloadComponent);
    const wrapper = mount(<Provider store={createStore()}><WrappedComponent text="passed" /></Provider>);
    expect(wrapper.find(PayloadComponent)).to.be.present();
    expect(wrapper.find(ErrorIndicator)).to.be.present();
  });
});
