import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import Loading from '../Loading/delayed';
import UnavailableError from '../error/UnavailableError';
import ErrorIndicator from '../deferred/ErrorIndicator';
import { getCloudId, getOrganizationId } from '../../core/state/meta/selectors';

class WithLoader extends PureComponent {
  static propTypes = {
    metadata: PropTypes.object,
    mountAction: PropTypes.func,
    props: PropTypes.object,
    children: PropTypes.node,
    loaderProps: PropTypes.object,
    Component: PropTypes.func,
    ErrorComponent: PropTypes.func,
  };

  static contextTypes = {
    sendAnalyticEvent: PropTypes.func
  };

  componentDidMount() {
    // uses Did mount to delay mount action and make this component more test-friendly
    if (this.props.mountAction) {
      this.props.mountAction();
    }
    this.mountTime = Date.now();
  }

  componentDidUpdate(oldProps) {
    const loading = this.props.metadata.loading;
    if (oldProps.metadata.loading !== loading && loading) {
      this.context.sendAnalyticEvent('pageLoaded', { pageLoadTime: Date.now() - this.mountTime });
    }
  }

  mountTime = 0;

  retryLoading = () => this.props.mountAction();

  render() {
    const {
      ErrorComponent, loaderProps, metadata = {}, children, props, mountAction
    } = this.props;
    const { loading, display, error } = metadata;

    if (display) {
      return (
        <div>
          {error && <ErrorIndicator />}
          {children}
        </div>
      );
    }

    // TODO: should handle "empty" meta state
    // currently: force loading of nor display, nor loading, nor error state set
    if (loading || !error) {
      return <Loading {...loaderProps.loader} />;
    }

    return ErrorComponent
      ? <ErrorComponent {...props} onRetry={mountAction && this.retryLoading} error={error} />
      : <UnavailableError onRetry={mountAction && this.retryLoading} error={error} />;
  }
}

const normalizeMeta = (meta) => {
  if (meta === true || meta === false) {
    return { display: meta, loading: !meta };
  }
  return meta;
};

const ConnectedLoader = connect(
  (state, { loader, props }) => ({
    metadata: normalizeMeta(loader.metaSelector(state, props)),
    cloudId: getCloudId(state) || getOrganizationId(state),
  }),
  (dispatch, { loader, props }) => ({ mountAction: loader.mountAction && (() => dispatch(loader.mountAction(props))) })
)(WithLoader);

const PageLoader = props => (
  <ConnectedLoader
    {...props}
    loaderProps={{
      mode: 'page'
    }}
  />
);

const withLoader = (metaSelector, mountAction, loaderProps = {}) => (Component, ErrorComponent) => {
  const loader = {
    mountAction,
    metaSelector
  };

  return props => (
    <ConnectedLoader
      ErrorComponent={ErrorComponent}
      loaderProps={loaderProps}
      loader={loader}
      props={props}
    >
      <Component {...props} />
    </ConnectedLoader>
  );
};

const withPageLoader = (metaSelector, mountAction, loaderProps = {}) =>
  withLoader(metaSelector, mountAction, {
    ...loaderProps,
    loader: {
      mode: 'page',
      ...loaderProps.loader
    }
  });

export {
  withPageLoader,
  PageLoader
};

export default withLoader;
