import withLoader, { withPageLoader, PageLoader } from './withLoader';

export {
  withPageLoader,
  PageLoader
};

export default withLoader;