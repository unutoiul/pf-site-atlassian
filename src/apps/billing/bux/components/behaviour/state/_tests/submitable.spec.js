import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import { SubmitButton } from '../../../Button';
import Submitable from '../submitable';

describe('Orchestrate submission:', () => {
  it('should indicate submission', () => {
    const submit = sinon.stub().resolves(true);
    const wrapper = shallow(<Submitable
      action={submit}
      component={SubmitButton}
      map={flag => ({ disabled: flag, spinning: !flag })}
      className="42"
    >A Button
                            </Submitable>);
    let button = wrapper.find(SubmitButton);
    expect(wrapper).to.have.state('submitting', false);
    expect(button).to.have.prop('disabled', false);
    expect(button).to.have.prop('spinning', true);
    expect(button).to.have.prop('className', '42');

    wrapper.instance().onClick();
    button = wrapper.update().find(SubmitButton);
    expect(wrapper).to.have.state('submitting', true);
    expect(button).to.have.prop('disabled', true);
    return submit().then(() => (
      expect(wrapper).to.have.state('submitting', false)
    ));
  });

  it('should call afterSubmission', (done) => {
    const submit = sinon.stub().resolves(true);
    const afterSubmit = sinon.stub();

    const wrapper = shallow(<Submitable
      action={submit}
      component={SubmitButton}
      map={() => {}}
      afterSubmit={afterSubmit}
    >A Button
                            </Submitable>);
    wrapper.instance().onClick();
    setImmediate(() => {
      expect(afterSubmit).to.have.been.called();
      done();
    });
  });

  it('should call onError', (done) => {
    const submit = sinon.stub().rejects();
    const onError = sinon.stub();

    const wrapper = shallow(<Submitable
      action={submit}
      component={SubmitButton}
      map={() => {}}
      onError={onError}
    >A Button
                            </Submitable>);
    expect(wrapper).to.have.state('submitting', false);

    wrapper.instance().onClick();
    setImmediate(() => {
      expect(onError).to.have.been.called();
      expect(wrapper).to.have.state('submitting', false);
      done();
    });
  });
});
