import PropTypes from 'prop-types';
import React, { Component } from 'react';

class SubmittingStateComponent extends Component {
  state = {
    submitting: false
  };

  onClick = (event) => {
    const { afterSubmit, onError } = this.props;
    this.setState(
      { submitting: true },
      () => this.props.action()
        .then(() => this.setState({ submitting: false }))
        .then(() => {
          if (afterSubmit) {
            afterSubmit();
          }
        })
        .catch((res) => {
          this.setState({ submitting: false });
          if (onError) {
            onError(res);
          }
        })
    );
    if (this.props.preventDefault) {
      if (event) {
        event.preventDefault();
      }
      return false;
    }
    return true;
  };

  render() {
    const { component: WrappedComponent, map, ...rest } = this.props;
    return (
      <WrappedComponent
        {...rest}
        {...map(this.state.submitting)}

        onClick={this.onClick}
      />
    );
  }
}

SubmittingStateComponent.propTypes = {
  component: PropTypes.func.isRequired,
  map: PropTypes.func.isRequired,
  action: PropTypes.func.isRequired,
  afterSubmit: PropTypes.func,
  onError: PropTypes.func,
  preventDefault: PropTypes.bool
};

export default SubmittingStateComponent;