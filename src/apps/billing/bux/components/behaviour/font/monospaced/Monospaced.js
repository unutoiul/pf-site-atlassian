import React from 'react';
import styles from './Monospaced.less';

const defaultClassSelect = () => styles.letter;
const costClassSelect = char => (char.match(/[0-9]/) ? styles.letter : styles.ignored);

const toMonospacedFont = (string, classFactory) => (
  String(string || '')
    .split('')
    .map((char, index) => <span key={index} className={classFactory(char)}>{char}</span>)
);

export const asMonospacedFont = string => toMonospacedFont(string, defaultClassSelect);
export const asMonospacedCost = string => toMonospacedFont(string, costClassSelect);
