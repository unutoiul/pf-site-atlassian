import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import proxyquire from 'bux/helpers/proxyquire';

describe('Monospaced component', () => {
  const styles = {
    letter: 'letter',
    dot: 'dot'
  };
  let asMonospacedFont;
  let asMonospacedCost;

  beforeEach(() => {
    const Monospaced = proxyquire('../Monospaced', {
      './Monospaced.less': styles
    });
    asMonospacedFont = Monospaced.asMonospacedFont;
    asMonospacedCost = Monospaced.asMonospacedCost;
  });

  it('should space character inside spans', () => {
    const wrapper = shallow(<div>{asMonospacedFont('123456789.')}</div>);
    expect(wrapper.find('span')).to.have.length(10);
    expect(wrapper.find(`.${styles.letter}`)).to.have.length(10);
    expect(wrapper).to.have.text('123456789.');

    expect(asMonospacedFont('123')).to.have.length(3);
  });

  it('should space character inside spans, but not for dot', () => {
    const wrapper = shallow(<div>{asMonospacedCost('123456.00')}</div>);
    expect(wrapper.find('span')).to.have.length(9);
    expect(wrapper.find(`.${styles.letter}`)).to.have.length(8);
    expect(wrapper).to.have.text('123456.00');
  });

  it('should handle empty value', () => {
    expect(asMonospacedFont('')).to.have.length(0);
    expect(asMonospacedFont()).to.have.length(0);
    expect(asMonospacedFont(null)).to.have.length(0);
  });
});
