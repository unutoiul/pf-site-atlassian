import PropTypes from 'prop-types';
import React, { Component } from 'react';
import style from './styles.less';

const counters = new WeakMap();

export default class Unscrollable extends Component {
  static propTypes = {
    children: PropTypes.node,
    target: PropTypes.any
  };

  static defaultProps = {
    target: document.body
  };

  componentWillMount() {
    const { target } = this.props;
    const counter = counters.get(target);
    counters.set(target, (counter || 0) + 1);
    if (!counter) {
      target.classList.add(style.unscrollable);
    }
  }

  componentWillUnmount() {
    const { target } = this.props;
    const counter = counters.get(target);
    counters.set(target, counter - 1);
    if (counter === 1) {
      target.classList.remove(style.unscrollable);
    }
  }

  render() {
    return (<div>{this.props.children}</div>);
  }
}