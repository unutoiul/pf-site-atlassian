import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import Unscrollable from '../Unscrollable';
import styles from '../styles.less';

describe('Unscrollable component', () => {
  it('Should add class to target', () => {
    const wrapper1 = mount(<Unscrollable><span>content</span></Unscrollable>);
    expect(document.body.classList.contains(styles.unscrollable)).to.be.equal(true);
    const wrapper2 = mount(<Unscrollable>content</Unscrollable>);
    expect(document.body.classList.contains(styles.unscrollable)).to.be.equal(true);
    wrapper1.unmount();
    expect(document.body.classList.contains(styles.unscrollable)).to.be.equal(true);
    wrapper2.unmount();
    expect(document.body.classList.contains(styles.unscrollable)).to.be.equal(false);
  });
});