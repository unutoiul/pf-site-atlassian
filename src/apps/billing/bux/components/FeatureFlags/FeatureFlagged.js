import React from 'react';
import PropTypes from 'prop-types';

import { keys } from 'bux/common/helpers/utils';

import { FeatureFlagsContext } from './FeatureFlagProvider';

//just alias the naming
const FeatureFlagged = ({ children, flags }) => {
  const checkArray = (fromComponent, toProvider) => fromComponent.every(key => toProvider[key]);
  const checkObject = (fromComponent, toProvider) =>
    keys(fromComponent).every(key => fromComponent[key] === (toProvider[key] || false));

  const predicate = Array.isArray(flags) ? checkArray : checkObject;
  const compareFlags = (fromComponent, toProvider = {}) => predicate(fromComponent, toProvider);

  return (
    <FeatureFlagsContext.Consumer>
      {ff => (compareFlags(flags, ff) ? children : null) || null}
    </FeatureFlagsContext.Consumer>
  );
};

FeatureFlagged.propTypes = {
  children: PropTypes.node,
  flags: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.string),
    PropTypes.object
  ])
};

export default FeatureFlagged;
