import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import FeatureFlagProvider, { FeatureSwitch, FeatureCase, FeatureDefault } from '../index';

describe('FeatureSwitch: ', () => {
  describe('With provider: ', () => {
    it('should switch between features', () => {
      // Arrange
      const FLAG_1 = 'flag1';
      const FLAG_2 = 'flag2';
      const dummyProps = { [FLAG_1]: true, [FLAG_2]: true };
      const Dummy1 = () => <div id="someId1">test</div>;
      const Dummy2 = () => <div id="someId2">test</div>;
      const Dummy3 = () => <div id="someId3">test</div>;

      // Act
      const wrapped = mount(<FeatureFlagProvider featureFlags={dummyProps}>
        <FeatureSwitch>
          <FeatureCase value={FLAG_1}>
            <Dummy1 />
          </FeatureCase>

          <FeatureCase value={FLAG_2}>
            <Dummy2 />
          </FeatureCase>

          <FeatureDefault>
            <Dummy3 />
          </FeatureDefault>
        </FeatureSwitch>
                            </FeatureFlagProvider>);

      // Assert
      expect(wrapped.find(Dummy1)).to.be.present();
      expect(wrapped.find(Dummy2)).not.to.be.present();
      expect(wrapped.find(Dummy3)).not.to.be.present();

      wrapped.setProps({ featureFlags: { [FLAG_1]: false, [FLAG_2]: true } });

      expect(wrapped.find(Dummy2)).to.be.present();
      expect(wrapped.find(Dummy1)).not.to.be.present();
      expect(wrapped.find(Dummy3)).not.to.be.present();

      wrapped.setProps({ featureFlags: { } });

      expect(wrapped.find(Dummy3)).to.be.present();
      expect(wrapped.find(Dummy1)).not.to.be.present();
      expect(wrapped.find(Dummy2)).not.to.be.present();
    });

    it('should behave accordingly if none matches and no default', () => {
      // Arrange
      const FLAG_1 = 'flag1';
      const FLAG_2 = 'flag2';
      const dummyProps = { [FLAG_1]: false, [FLAG_2]: false };
      const Dummy1 = () => <div id="someId1">test</div>;
      const Dummy2 = () => <div id="someId2">test</div>;

      // Act
      const wrapped = mount(<FeatureFlagProvider featureFlags={dummyProps}>
        <FeatureSwitch>
          <FeatureCase value={FLAG_1}>
            <Dummy1 />
          </FeatureCase>

          <FeatureCase value={FLAG_2}>
            <Dummy2 />
          </FeatureCase>
        </FeatureSwitch>
                            </FeatureFlagProvider>);

      // Assert
      expect(wrapped.find(Dummy1)).not.to.be.present();
      expect(wrapped.find(Dummy2)).not.to.be.present();
    });
  });

  describe('Without provider: ', () => {
    it('should render default', () => {
      // Arrange
      const FLAG_1 = 'flag1';
      const Dummy1 = () => <div id="someId1">test</div>;
      const Dummy2 = () => <div id="someId2">test</div>;

      // Act
      const wrapped = mount(<FeatureSwitch>
        <FeatureCase value={FLAG_1}>
          <Dummy1 />
        </FeatureCase>

        <FeatureDefault>
          <Dummy2 />
        </FeatureDefault>
                            </FeatureSwitch>);

      // Assert
      expect(wrapped.find(Dummy1)).not.to.be.present();
      expect(wrapped.find(Dummy2)).to.be.present();
    });

    it('should render nothing if there is no default', () => {
      // Arrange
      const FLAG_1 = 'flag1';
      const FLAG_2 = 'flag2';
      const Dummy1 = () => <div id="someId1">test</div>;
      const Dummy2 = () => <div id="someId2">test</div>;

      // Act
      const wrapped = mount(<FeatureSwitch>
        <FeatureCase value={FLAG_1}>
          <Dummy1 />
        </FeatureCase>

        <FeatureCase value={FLAG_2}>
          <Dummy2 />
        </FeatureCase>
                            </FeatureSwitch>);

      // Assert
      expect(wrapped.find(Dummy1)).not.to.be.present();
      expect(wrapped.find(Dummy2)).not.to.be.present();
    });
  });
});