import React from 'react';
import PropTypes from 'prop-types';
import { expect } from 'chai';
import { mount } from 'enzyme';
import FeatureFlagProvider, { FeatureFlagged } from '../index';

describe('FeatureFlagged: ', () => {
  describe('with a provider', () => {
    describe('with an array of flags', () => {
      it('should render Component if flags are present and true', () => {
        // Arrange
        const FLAG_1 = 'flag1';
        const FLAG_2 = 'flag2';
        const dummyProps = { [FLAG_1]: true, [FLAG_2]: true };
        const Dummy = ({ someProp }) => <div id="someId">test {someProp}</div>;
        Dummy.propTypes = {
          someProp: PropTypes.bool
        };

        // Act
        const wrapped = mount(<FeatureFlagProvider featureFlags={dummyProps}>
          <FeatureFlagged flags={[FLAG_1, FLAG_2]}>
            <Dummy someProp />
          </FeatureFlagged>
                              </FeatureFlagProvider>);

        // Assert
        const wrappedDummy = wrapped.find(Dummy);

        expect(wrappedDummy).to.be.present();
      });

      it('should not render Component if at least one flag is absent', () => {
        // Arrange
        const FLAG_1 = 'flag1';
        const FLAG_2 = 'flag2';
        const dummyProps = { [FLAG_1]: true };
        const Dummy = ({ someProp }) => <div id="someId">test {someProp}</div>;
        Dummy.propTypes = {
          someProp: PropTypes.bool
        };

        // Act
        const wrapped = mount(<FeatureFlagProvider featureFlags={dummyProps}>
          <FeatureFlagged flags={[FLAG_1, FLAG_2]}>
            <Dummy someProp />
          </FeatureFlagged>
                              </FeatureFlagProvider>);

        // Assert
        const wrappedDummy = wrapped.find(Dummy);

        expect(wrappedDummy).not.to.be.present();
      });

      it('should not render Component if at least one flag is false', () => {
        // Arrange
        const FLAG_1 = 'flag1';
        const FLAG_2 = 'flag2';
        const dummyProps = { [FLAG_1]: true, [FLAG_2]: false };
        const Dummy = ({ someProp }) => <div id="someId">test {someProp}</div>;
        Dummy.propTypes = {
          someProp: PropTypes.bool
        };

        // Act
        const wrapped = mount(<FeatureFlagProvider featureFlags={dummyProps}>
          <FeatureFlagged flags={[FLAG_1, FLAG_2]}>
            <Dummy someProp />
          </FeatureFlagged>
                              </FeatureFlagProvider>);

        // Assert
        const wrappedDummy = wrapped.find(Dummy);

        expect(wrappedDummy).not.to.be.present();
      });
    });

    describe('with a map of flags', () => {
      it('should render Component if condition yields true', () => {
        // Arrange
        const FLAG_1 = 'flag1';
        const FLAG_2 = 'flag2';
        const FLAG_3 = 'flag3';
        const dummyProps = { [FLAG_1]: true, [FLAG_2]: 'string', [FLAG_3]: true };
        const dummyFlags = { [FLAG_1]: true, [FLAG_2]: 'string' };
        const Dummy = ({ someProp }) => <div id="someId">test {someProp}</div>;
        Dummy.propTypes = {
          someProp: PropTypes.bool
        };

        // Act
        const wrapped = mount(<FeatureFlagProvider featureFlags={dummyProps}>
          <FeatureFlagged flags={dummyFlags}>
            <Dummy someProp />
          </FeatureFlagged>
                              </FeatureFlagProvider>);

        // Assert
        const wrappedDummy = wrapped.find(Dummy);

        expect(wrappedDummy).to.be.present();
      });

      it('should not render component if flags doesn\'t match', () => {
        // Arrange
        const FLAG_1 = 'flag1';
        const FLAG_2 = 'flag2';
        const FLAG_3 = 'flag3';
        const dummyProps = { [FLAG_1]: true, [FLAG_2]: false, [FLAG_3]: true };
        const dummyFlags = { [FLAG_1]: true, [FLAG_2]: true };
        const Dummy = ({ someProp }) => <div id="someId">test {someProp}</div>;
        Dummy.propTypes = {
          someProp: PropTypes.bool
        };

        // Act
        const wrapped = mount(<FeatureFlagProvider featureFlags={dummyProps}>
          <FeatureFlagged flags={dummyFlags}>
            <Dummy someProp />
          </FeatureFlagged>
                              </FeatureFlagProvider>);

        // Assert
        const wrappedDummy = wrapped.find(Dummy);

        expect(wrappedDummy).not.to.be.present();
      });
    });
  });

  describe('without provider', () => {
    const Dummy = () => <div id="someId">test</div>;

    const createComponent = flagValue => () => {
      const FLAG_1 = 'flag1';
      const dummyFlags = { [FLAG_1]: flagValue };
      return (
        <FeatureFlagged flags={dummyFlags}>
          <Dummy />
        </FeatureFlagged>
      );
    };

    it('should not render component if flag is true', () => {
      const FalseFlaggedComponent = createComponent(true);
      const wrapped = mount(<FalseFlaggedComponent />);
      const wrappedDummy = wrapped.find(Dummy);
      expect(wrappedDummy).to.not.be.present();
    });
    it('should render component if flag is false', () => {
      const TrueFlaggedComponent = createComponent(false);
      const wrapped = mount(<TrueFlaggedComponent />);
      const wrappedDummy = wrapped.find(Dummy);
      expect(wrappedDummy).to.be.present();
    });
  });
});