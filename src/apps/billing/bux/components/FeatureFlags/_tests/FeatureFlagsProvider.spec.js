import React from 'react';
import PropTypes from 'prop-types';
import { expect } from 'chai';
import { mount } from 'enzyme';
import FeatureFlagProvider, { FeatureFlagsContext } from '../index';

describe('FeatureFlagsProvider', () => {
  it('should pass featureFlags to a Component', () => {
    // Arrange
    const dummyProps = { flag: true };
    const Dummy = ({ someProp }) => <div id="someId">test {someProp}</div>;
    Dummy.propTypes = {
      someProp: PropTypes.bool
    };

    // Act
    const wrapped = mount(<FeatureFlagProvider featureFlags={dummyProps}>
      <FeatureFlagsContext.Consumer>
        { ff => <Dummy {...ff} someProp /> }
      </FeatureFlagsContext.Consumer>
                          </FeatureFlagProvider>);

    // Assert
    const props = wrapped.find(Dummy).props();

    expect(props).to.have.a.property('flag');
    expect(props.flag).to.be.true();
    expect(props).to.have.a.property('someProp');
    expect(props.someProp).to.be.true();
  });

  it('should pass parentflasg and featureFlags to a Component', () => {
    // Arrange
    const dummyProps1 = { flag1: true };
    const dummyProps2 = { flag2: false };

    const Dummy = ({ someProp }) => <div id="someId">test {someProp}</div>;
    Dummy.propTypes = {
      someProp: PropTypes.bool
    };

    // Act
    const wrapped = mount(<FeatureFlagProvider featureFlags={dummyProps1}>
      <FeatureFlagProvider featureFlags={dummyProps2}>
        <FeatureFlagsContext.Consumer>
          { ff => <Dummy {...ff} someProp /> }
        </FeatureFlagsContext.Consumer>
      </FeatureFlagProvider>
                          </FeatureFlagProvider>);

    // Assert
    const props = wrapped.find(Dummy).props();

    expect(props).to.have.a.property('flag1');
    expect(props).to.have.a.property('flag2');

    expect(props.flag1).to.be.true();
    expect(props.flag2).to.be.false();

    expect(props).to.have.a.property('someProp');
    expect(props.someProp).to.be.true();
  });

  it('should override a parent flag', () => {
    // Arrange
    const dummyProps1 = { flag1: true, flag3: true };
    const dummyProps2 = { flag2: false, flag3: false };

    const Dummy = ({ someProp }) => <div id="someId">test {someProp}</div>;
    Dummy.propTypes = {
      someProp: PropTypes.bool
    };

    // Act
    const wrapped = mount(<FeatureFlagProvider featureFlags={dummyProps1}>
      <FeatureFlagProvider featureFlags={dummyProps2}>
        <FeatureFlagsContext.Consumer>
          { ff => <Dummy {...ff} someProp /> }
        </FeatureFlagsContext.Consumer>
      </FeatureFlagProvider>
                          </FeatureFlagProvider>);

    // Assert
    const props = wrapped.find(Dummy).props();

    expect(props).to.have.a.property('flag3');
    expect(props.flag3).to.be.false();
  });
});