import React from 'react';
import PropTypes from 'prop-types';

export const FeatureFlagsContext = React.createContext();

const FeatureFlagProvider = ({ featureFlags, multivariateFeatureFlags, children }) => (
  <FeatureFlagsContext.Consumer>
    { (parentProps = {}) =>
      (<FeatureFlagsContext.Provider value={{ ...parentProps, ...featureFlags, ...multivariateFeatureFlags }}>
        {children}
       </FeatureFlagsContext.Provider>)
    }
  </FeatureFlagsContext.Consumer>
);

FeatureFlagProvider.propTypes = {
  featureFlags: PropTypes.any,
  multivariateFeatureFlags: PropTypes.any,
  children: PropTypes.node
};

export default FeatureFlagProvider;
