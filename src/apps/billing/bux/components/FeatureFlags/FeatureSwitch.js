import React, { Children } from 'react';
import PropTypes from 'prop-types';
import { FeatureFlagsContext } from './FeatureFlagProvider';

const FeatureCase = ({ children }) => children;
FeatureCase.propTypes = {
  value: PropTypes.string,
  children: PropTypes.node
};

const FeatureDefault = ({ children }) => children;
FeatureDefault.propTypes = {
  children: PropTypes.node
};

const caseOrDefault = (ff = {}, children) => {
  const FeatureCaseType = <FeatureCase />.type;
  const FeatureDefaultType = <FeatureDefault />.type;

  const matchingChildren = child => FeatureCaseType === child.type && ff[child.props.value];
  const matchingDefault = child => FeatureDefaultType === child.type;

  return children.find(matchingChildren) || children.find(matchingDefault) || null;
};

const FeatureSwitch = ({ children }) => (
  <FeatureFlagsContext.Consumer>
    { ff => caseOrDefault(ff, Children.toArray(children)) }
  </FeatureFlagsContext.Consumer>
);
FeatureSwitch.propTypes = {
  children: PropTypes.node
};

export default FeatureSwitch;
export { FeatureCase, FeatureDefault };