import BillEstimateLogo from 'bux/static/bill-estimate.svgx';
import BillHistoryLogo from 'bux/static/bill-history.svgx';
import InvoiceLogo from 'bux/static/invoice.svgx';
import MaintenanceLogo from 'bux/static/maintenance.svgx';

export {
  BillEstimateLogo,
  BillHistoryLogo,
  InvoiceLogo,
  MaintenanceLogo
};
