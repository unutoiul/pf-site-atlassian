export const loadBraintree = () => (
  import(/* webpackChunkName: "braintree-web" */'braintree-web').then(bt => (bt.default ? bt.default : bt))
);
