import { Trans } from '@lingui/react';
import React from 'react';
import { ExternalLinkTo } from 'bux/components/Link';
import PaymentMethodLoadingError from 'bux/components/PaymentMethodLoadingError';

const PaypalLoadingError = () => (
  <PaymentMethodLoadingError className="paypal-loading-error">
    <p>
      <strong>
        <Trans id="billing.paypal.error.title">Sorry, we were unable to load PayPal payment option</Trans>
      </strong>.{' '}
      <Trans id="billing.paypal.error.description">
        Try again later or{' '}
        <ExternalLinkTo.Support>
          contact customer support
        </ExternalLinkTo.Support>{' '}
        if the problem persists.
      </Trans>
    </p>
  </PaymentMethodLoadingError>
);

export default PaypalLoadingError;
