import { getBraintreeClientToken } from 'bux/common/api/billing-ux-service';
import { loadBraintree } from './braintreeLoader';

const createResolveCallback = (resolve, reject) => (error, value) => {
  if (error) {
    reject(error);
    return;
  }
  resolve(value);
};

let braintree = null;

const createBraintreeClient = clientToken => new Promise((resolve, reject) => {
  braintree.client.create({ authorization: clientToken }, createResolveCallback(resolve, reject));
});

const createBraintreePayPalCheckout = braintreeClient => new Promise((resolve, reject) => {
  braintree.paypalCheckout.create({ client: braintreeClient }, createResolveCallback(resolve, reject));
});

// There is a possibility to configure paypal button directly with Braintree
// https://braintree.github.io/braintree-web/3.31.0/PayPalCheckout.html#PayPalCheckout
// However, in that case account details obtained during "tokenizePayment" are ignored,
// that's why we need to do it manually.
export const initializeBraintreePayPalCheckout = () =>
  loadBraintree()
    .then((loaded) => {
      braintree = loaded;
    })
    .then(getBraintreeClientToken)
    .then(response => response.token)
    .then(createBraintreeClient)
    .then(createBraintreePayPalCheckout);

