import { expect } from 'chai';
import sinon from 'sinon';
import proxyquire from 'bux/helpers/proxyquire';
import { expectRejected } from 'bux/helpers/promised';
import { toPayPalSupportedLocale } from '../PayPalButtonInitializer';

describe('initializePayPalButton:', () => {
  const payPalEnvironment = 'test';
  const paypalCheckoutUrl = 'http://paypalCheckoutUrl';
  const paypalCompanyDisplayName = 'Atlassian - Test';
  let loadjsSpy;
  let renderButtonSpy;
  let payPalCheckoutInstance;

  beforeEach(() => {
    payPalCheckoutInstance = { createPayment: sinon.spy(), tokenizePayment: sinon.stub().resolves({}) };
    renderButtonSpy = sinon.stub().resolves();
    loadjsSpy = sinon.spy((script, callbacks) => {
      window.paypal = { Button: { render: renderButtonSpy } };
      callbacks.success();
    });
    window.paypal = null;
  });

  const importInitializePayPalButton = () =>
    proxyquire.noCallThru().load('../PayPalButtonInitializer', {
      loadjs: loadjsSpy,
      'bux/common/config': { payPalEnvironment, paypalCheckoutUrl, paypalCompanyDisplayName },
      './Braintree': { initializeBraintreePayPalCheckout: sinon.stub().resolves(payPalCheckoutInstance) }
    }).initializePayPalButton;

  const initializePayPalButton = (element, locale, callbacks) =>
    importInitializePayPalButton()(element, locale, callbacks);

  it('should finish after button rendered', async () => {
    await initializePayPalButton();

    expect(renderButtonSpy).to.be.called();
  });

  it('should use braintree payment vault flow', async () => {
    renderButtonSpy = sinon.spy((paypalConfig) => {
      paypalConfig.payment();
      expect(payPalCheckoutInstance.createPayment).to.have.been.calledWith({
        flow: 'vault',
        displayName: paypalCompanyDisplayName
      });
    });

    await initializePayPalButton();

    expect(renderButtonSpy).to.be.called();
  });

  it('should tokenize payment', async () => {
    renderButtonSpy = sinon.spy((paypalConfig) => {
      const data = {};
      paypalConfig.onAuthorize(data);
      expect(payPalCheckoutInstance.tokenizePayment).to.have.been.calledWith(data);
    });

    await initializePayPalButton();

    expect(renderButtonSpy).to.be.called();
  });

  it('should trigger callbacks', async () => {
    const data = {};
    const callbacks = {
      onAuthorize: sinon.spy(),
      onCancel: sinon.spy(),
      onError: sinon.spy(),
      onClick: sinon.spy()
    };
    payPalCheckoutInstance.tokenizePayment = sinon.stub().resolves(data);

    renderButtonSpy = sinon.spy((paypalConfig) => {
      paypalConfig.onCancel();
      expect(callbacks.onCancel).to.be.called();

      paypalConfig.onError();
      expect(callbacks.onError).to.be.called();

      paypalConfig.onClick();
      expect(callbacks.onClick).to.be.called();

      paypalConfig.onAuthorize().then(() => {
        expect(callbacks.onAuthorize).to.be.calledWith(data);
      });
    });

    await initializePayPalButton(null, null, callbacks);

    expect(renderButtonSpy).to.be.called();
    expect(callbacks.onAuthorize).to.be.called();
  });

  it('should use environment from the configuration', async () => {
    renderButtonSpy = sinon.spy((paypalConfig) => {
      expect(paypalConfig.env).to.be.equal(payPalEnvironment);
    });

    await initializePayPalButton();

    expect(renderButtonSpy).to.be.called();
  });

  it('should render button to given element', async () => {
    const elementId = 'element-id';
    renderButtonSpy = sinon.spy((paypalConfig, id) => {
      expect(id).to.be.equal(`#${elementId}`);
    });

    await initializePayPalButton(elementId);

    expect(renderButtonSpy).to.be.called();
  });

  it('should not import script when PayPal was already imported', async () => {
    importInitializePayPalButton();

    await initializePayPalButton();
    await initializePayPalButton();
    await initializePayPalButton();

    expect(loadjsSpy).to.be.calledOnce();
  });

  it('should pass error down', async () => {
    loadjsSpy = sinon.spy((script, callbacks) => callbacks.success());
    await expectRejected(initializePayPalButton());
  });
});

describe('toPayPalSupportedLocale', () => {
  it('should convert en to en_US', () => {
    expect(toPayPalSupportedLocale('en')).to.be.eq('en_US');
  });

  it('should convert ja to ja_JP', () => {
    expect(toPayPalSupportedLocale('ja')).to.be.eq('ja_JP');
  });

  it('should convert pt to first pt available', () => {
    expect(toPayPalSupportedLocale('pt')).to.be.eq('pt_BR');
  });

  it('should not convert existent support', () => {
    expect(toPayPalSupportedLocale('pt_PT')).to.be.eq('pt_PT');
  });

  it('should convert undefined to en_US', () => {
    expect(toPayPalSupportedLocale(undefined)).to.be.eq('en_US');
  });

  it('should convert null to en_US', () => {
    expect(toPayPalSupportedLocale(null)).to.be.eq('en_US');
  });

  it('should convert no-existent to en_US', () => {
    expect(toPayPalSupportedLocale('xx_XX')).to.be.eq('en_US');
  });

  it('should convert dash to underline', () => {
    expect(toPayPalSupportedLocale('en-US')).to.be.eq('en_US');
  });
});
