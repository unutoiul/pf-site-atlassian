import React from 'react';
import ReactDOM from 'react-dom';
import loadjs from 'loadjs';
import config from 'bux/common/config';
import MockPayPalButton from 'bux/helpers/tests/components/MockPayPalButton';
import { initializeBraintreePayPalCheckout } from './Braintree';

const loadPayPalCheckoutScript = () => new Promise((resolve, reject) => {
  if (window.paypal) {
    resolve();
  } else {
    loadjs(config.paypalCheckoutUrl, { success: resolve, error: reject });
  }
});

const renderPayPalButton = (parentElementId, locale, payPalCheckoutInstance, {
  onAuthorize, onCancel, onError, onClick
}) =>
  window.paypal.Button.render({
    env: config.payPalEnvironment,
    style: {
      label: 'pay',
      size: 'medium',
      shape: 'rect',
      color: 'gold',
      tagline: false,
    },
    locale,

    payment() {
      return payPalCheckoutInstance.createPayment({
        displayName: config.paypalCompanyDisplayName,
        flow: 'vault' // Client token will be generated that may be used for processing further transactions.
      });
    },

    onAuthorize(data) {
      return payPalCheckoutInstance.tokenizePayment(data)
        .then(onAuthorize);
    },

    onCancel() {
      if (onCancel) {
        onCancel();
      }
    },

    onError(error) {
      if (onError) {
        onError(error);
      }
    },

    onClick() {
      if (onClick) {
        onClick();
      }
    }
  }, `#${parentElementId}`);

const renderMockedPayPalButton = (parentElementId, { onAuthorize }) => {
  ReactDOM.render(React.createElement(MockPayPalButton, { onAuthorize }), document.getElementById(parentElementId));
};

/**
 * Convert any locale to a PayPal supported locale. This needs to convert locale suffix -> full locale (en -> en_US).
 * Supported: https://developer.paypal.com/docs/integration/direct/rest/locale-codes/#supported-locale-codes
 * @param {string} locale
 * @returns {string} PayPal supported locale
 */
export const toPayPalSupportedLocale = (locale) => {
  const defaultLocale = 'en_US';
  const convertDefaults = {
    en: 'en_US', ja: 'ja_JP', fr: 'fr_FR', es: 'es_ES',
  };
  const supported = [
    'ar_EG', 'da_DK', 'de_DE', 'en_AU', 'en_GB', 'en_US', 'es_ES', 'es_XC', 'fr_CA', 'fr_FR', 'fr_XC', 'he_IL',
    'id_ID', 'it_IT', 'ja_JP', 'ko_KR', 'nl_NL', 'no_NO', 'pl_PL', 'pt_BR', 'pt_PT', 'ru_RU', 'sv_SE', 'th_TH',
    'zh_CN', 'zh_HK', 'zh_TW', 'zh_XC'
  ];

  if (!locale) {
    return defaultLocale;
  }

  const fullLocale = locale.length === 2 && convertDefaults[locale] ? convertDefaults[locale] : locale;
  const underlinedLocale = fullLocale.replace('-', '_');
  let supportedFound = supported.includes(underlinedLocale) && underlinedLocale;
  if (!supportedFound) {
    supportedFound = supported.find(item => item.indexOf(locale) === 0);
  }
  return supportedFound || defaultLocale;
};

export const initializePayPalButton = (elementId, locale, callbacks = {}) => {
  // If running e2e tests, mock paypal button.
  if (config.mockPayPalButton) {
    return Promise.resolve(renderMockedPayPalButton(elementId, callbacks));
  }
  return Promise.all([initializeBraintreePayPalCheckout(), loadPayPalCheckoutScript()])
    .then(([paypalCheckoutInstance]) => paypalCheckoutInstance)
    .then(paypalCheckoutInstance =>
      renderPayPalButton(elementId, toPayPalSupportedLocale(locale), paypalCheckoutInstance, callbacks));
};
