import { expect } from 'chai';
import sinon from 'sinon';
import proxyquire from 'bux/helpers/proxyquire';
import { expectRejected } from 'bux/helpers/promised';

const triggerCallback = (options, callback) => {
  callback(null, {});
};

describe('Braintree: initializeBraintreePayPalCheckout', () => {
  const token = 'token';
  const braintreeClient = { client: 'braintree' };
  const paypalCheckout = { checkout: 'paypal' };
  const error = new Error('error');

  let braintree;
  let braintreeClientToken;
  let initializeBraintreePayPalCheckout;

  before(() => {
    braintree = { client: { create: triggerCallback }, paypalCheckout: { create: triggerCallback } };
    braintreeClientToken = sinon.stub().resolves({ token });
    initializeBraintreePayPalCheckout = proxyquire.noCallThru().load('../Braintree', {
      './braintreeLoader': { loadBraintree: () => Promise.resolve(braintree) },
      'bux/common/api/billing-ux-service': { getBraintreeClientToken: braintreeClientToken }
    }).initializeBraintreePayPalCheckout;
  });

  it('should pass down expected configuration data', async () => {
    braintree.client.create = (options, callback) => {
      expect(options.authorization).to.be.equal(token);
      callback(null, braintreeClient);
    };

    braintree.paypalCheckout.create = (options, callback) => {
      expect(options.client).to.be.equal(braintreeClient);
      callback(null, paypalCheckout);
    };

    const result = await initializeBraintreePayPalCheckout();

    expect(result).to.be.equal(paypalCheckout);
  });

  it('should pass down error for BUX', async () => {
    braintreeClientToken.rejects(error);
    await expectRejected(initializeBraintreePayPalCheckout());
  });

  it('should pass down error triggered while creating braintree client', async () => {
    braintree.client.create = (options, callback) => {
      callback(error, null);
    };

    await expectRejected(initializeBraintreePayPalCheckout());
  });

  it('should pass down error triggered while creating paypal checkout object', async () => {
    braintree.paypalCheckout.create = (options, callback) => {
      callback(error, null);
    };

    await expectRejected(initializeBraintreePayPalCheckout());
  });
});
