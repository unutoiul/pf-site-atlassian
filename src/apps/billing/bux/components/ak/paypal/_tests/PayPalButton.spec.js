import { i18n } from 'bux/helpers/jsLingui';
import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';
import { Provider } from 'bux/helpers/redux';
import proxyquire from 'bux/helpers/proxyquire';
import getAnalytic from 'bux/components/log/helpers/getAnalytic';
import Loading from 'bux/components/Loading';

export const notResolvedPromise = new Promise(() => {});

describe('PayPalButton:', () => {
  let analytic;
  let onAuthorize;
  let logException;
  let initializePayPalButton;
  let wrapper;

  beforeEach(() => {
    analytic = getAnalytic();
    onAuthorize = sinon.spy();
    logException = sinon.spy();
  });

  const renderPayPalComponent = () => {
    const PayPalComponent = proxyquire.noCallThru().load('../PayPalButton', {
      './components/PayPalButtonInitializer': { initializePayPalButton },
      'bux/common/helpers/monitoring': { logException }
    }).default;
    wrapper = mount(<Provider>
      <PayPalComponent onAuthorize={onAuthorize} i18n={i18n} />
    </Provider>, analytic.context);
    return new Promise(resolve => setImmediate(resolve));
  };

  it('should render spinner while loading', async () => {
    initializePayPalButton = sinon.stub().returns(notResolvedPromise);

    await renderPayPalComponent();

    expect(initializePayPalButton).to.be.called();
    expect(wrapper).to.be.present();
    expect(wrapper.find(Loading)).to.be.present();
  });

  it('should render hidden element for PayPal button while loading', async () => {
    initializePayPalButton = sinon.stub().returns(notResolvedPromise);

    await renderPayPalComponent();

    expect(initializePayPalButton).to.be.called();
    expect(wrapper).to.be.present();
    expect(wrapper.find('#paypal-button')).to.be.present();
  });

  it('should hide spinner after loaded', async () => {
    initializePayPalButton = sinon.stub().resolves({});

    await renderPayPalComponent();

    expect(initializePayPalButton).to.be.called();
    wrapper.update();
    expect(wrapper).to.be.present();
    expect(wrapper.find(Loading)).not.to.be.present();
  });

  it('should log error when filed to initialized paypal', async () => {
    const error = {};
    initializePayPalButton = sinon.stub().rejects(error);

    await renderPayPalComponent();

    expect(initializePayPalButton).to.be.called();
    expect(logException).to.be.calledWith(error);
    expect(analytic.sendAnalyticEvent).to.be.called();
    wrapper.update();
    expect(wrapper.find('PaypalLoadingError')).to.be.present();
  });

  it('should log analytics', async () => {
    const error = {};
    initializePayPalButton = sinon.spy((element, locale, callbacks) => {
      callbacks.onAuthorize();
      expect(analytic.sendAnalyticEvent).to.be.calledOnce();
      expect(onAuthorize).to.be.calledOnce();
      analytic.sendAnalyticEvent.reset();

      callbacks.onCancel();
      expect(analytic.sendAnalyticEvent).to.be.calledOnce();
      analytic.sendAnalyticEvent.reset();

      callbacks.onClick();
      expect(analytic.sendAnalyticEvent).to.be.calledOnce();
      analytic.sendAnalyticEvent.reset();

      callbacks.onError(error);
      expect(logException).to.be.calledWith(error);
      expect(analytic.sendAnalyticEvent).to.be.calledOnce();

      return Promise.resolve();
    });

    await renderPayPalComponent();

    expect(initializePayPalButton).to.be.called();
  });
});
