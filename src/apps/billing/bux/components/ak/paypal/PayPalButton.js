import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Loading from 'bux/components/Loading';
import { logException } from 'bux/common/helpers/monitoring';
import { initializePayPalButton } from './components/PayPalButtonInitializer';
import PaypalLoadingError from './components/PaypalLoadingError';
import styles from './PayPalButton.less';

export default class PayPalButton extends Component {
  static propTypes = {
    onAuthorize: PropTypes.func.isRequired,
    className: PropTypes.string,
    i18n: PropTypes.object.isRequired
  };

  static contextTypes = {
    sendAnalyticEvent: PropTypes.func
  };

  constructor(props) {
    super(props);

    this.state = {
      configured: false,
      error: false,
    };
  }

  componentDidMount() {
    this.isComponentMounted = true;
    this.init();
  }

  logError(error) {
    logException(error);
    this.context.sendAnalyticEvent('failed', error);
    this.setState({ error: true });
  }

  init() {
    const { i18n } = this.props;
    const timeStart = Date.now();
    initializePayPalButton('paypal-button', i18n.language, {
      onAuthorize: (payload) => {
        this.props.onAuthorize(payload);
        this.context.sendAnalyticEvent('authorized');
      },
      onCancel: () => this.context.sendAnalyticEvent('canceled'),
      onClick: () => this.context.sendAnalyticEvent('clicked'),
      onError: error => this.logError(error)
    })
      .then(() => {
        this.context.sendAnalyticEvent('ok', {
          time: Date.now() - timeStart
        });
        this.setState({ configured: true });
      })
      .catch(error => this.logError(error));
  }

  render() {
    const { configured, error } = this.state;
    return (
      <div className={cx(styles.paypal, this.props.className)}>
        {!configured && !error && <div><Loading mode="paragraph" /></div>}
        <div id="paypal-button" style={{ display: configured && !error ? 'block' : 'none' }} />
        {error && <PaypalLoadingError />}
      </div>
    );
  }
}
