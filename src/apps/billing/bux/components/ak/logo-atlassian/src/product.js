import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';
import resizeSVG from 'bux/common/helpers/resizeSVG';
import styles from './styles.less';

import AtlassianLogo from '../svg/atlassian.svgx';
import BambooLogo from '../svg/bamboo.svgx';
import BitbucketLogo from '../svg/bitbucket.svgx';
import CloverLogo from '../svg/clover.svgx';
import ConfluenceLogo from '../svg/confluence.svgx';
import CrownLogo from '../svg/crowd.svgx';
import CrucibleLogo from '../svg/crucible.svgx';
import FisheyeLogo from '../svg/fisheye.svgx';
import StrideLogo from '../svg/stride.svgx';
import JiraCoreLogo from '../svg/jira-core.svgx';
import JiraServiceLogo from '../svg/jira-servicedesk.svgx';
import JiraSoftwareLogo from '../svg/jira-software.svgx';
import JiraLogo from '../svg/jira.svgx';
import JiraIncidentManagerLogo from '../svg/jira-incident-manager.svgx';
import OpsgenieLogo from '../svg/opsgenie-logo.svgx';

import SourceTreeLogo from '../svg/sourcetree.svgx';

const productLookup = {
  bamboo: BambooLogo,
  bitbucket: BitbucketLogo,
  clover: CloverLogo,
  confluence: ConfluenceLogo,
  crown: CrownLogo,
  crucible: CrucibleLogo,
  fisheye: FisheyeLogo,
  hipchat: StrideLogo,
  sourceTree: SourceTreeLogo,
  jira: JiraLogo,
  opsgenie: OpsgenieLogo,
  'jira-core': JiraCoreLogo,
  'jira-servicedesk': JiraServiceLogo,
  'jira-software': JiraSoftwareLogo,
  'jira-incident-manager': JiraIncidentManagerLogo,
};

const pickProduct = (productKey = '') => productLookup[productKey.split('.')[0].trim()] || AtlassianLogo;

const titleSize = (name = '') => {
  if (name.length >= 40) return styles.small;
  if (name.length >= 20) return styles.medium;
  return styles.big;
};

const ProductLogo = ({
  product, size, name, color = '#172B4D'
}) => {
  const Logo = resizeSVG(pickProduct(product), size);
  return (
    <div className={cx('ak-product-logo', styles.logo, styles[size])} title={name}>
      <div className={styles.productLogo}>
        <Logo fill={color} />
        <span className={titleSize(name)}>{name}</span>
      </div>
    </div>
  );
};

ProductLogo.propTypes = {
  product: PropTypes.string,
  size: PropTypes.oneOf(['small', 'medium', 'large', 'xlarge']),
  name: PropTypes.string.isRequired,
  color: PropTypes.string
};

export default ProductLogo;
