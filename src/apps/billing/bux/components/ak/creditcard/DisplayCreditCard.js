import PropTypes from 'prop-types';
import React, { Component } from 'react';
import cx from 'classnames';
import styled from 'styled-components';
import EditFilledIcon from '@atlaskit/icon/glyph/edit-filled';
import { formatCCNumber } from 'bux/common/formatters/cc-number-formatter';
import { padMonth } from 'bux/common/formatters/date-formatter';
import Button from '../../Button';
import { SmallCreditCard as CreditCard } from './styled/CreditCard';
import MasterCardLogo from './svg/mastercard.svgx';
import VisaCardLogo from './svg/visa.svgx';
import AmexCardLogo from './svg/amex.svgx';
import DefaultCardLogo from './svg/default.svgx';


const TextField = ({ label, value, className }) => (
  <div className={cx(className, 'ak-field-group')}>
    {/*eslint-disable jsx-a11y/label-has-for*/}
    <label>{label}</label>
    {/*eslint-enable */}
    <div>{value}</div>
  </div>
);
TextField.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  className: PropTypes.string
};

const EditIcon = <EditFilledIcon label="Edit Credit Card form" />;

const SelectedCard = ({ type }) => {
  switch (type) {
    case 'VISA':
      return <VisaCardLogo />;
    case 'MASTERCARD':
      return <MasterCardLogo />;
    case 'AMEX':
      return <AmexCardLogo />;
    default:
      return <DefaultCardLogo />;
  }
};

SelectedCard.propTypes = {
  type: PropTypes.string
};

class DisplayCreditCard extends Component {
  static propTypes = {
    className: PropTypes.string.isRequired,
    details: PropTypes.shape({
      expiryMonth: PropTypes.number,
      expiryYear: PropTypes.number,
      name: PropTypes.string,
      suffix: PropTypes.string,
      type: PropTypes.string,
    }),
    onEdit: PropTypes.func
  };

  state = {};

  render() {
    const {
      suffix, type, expiryMonth, expiryYear, name
    } = this.props.details;

    return (
      <CreditCard className={cx('displayCreditCardDetails', this.props.className)}>
        <div className="payment-fields">
          <TextField className="ccNumberDetails" label="Card Number" value={formatCCNumber(suffix, type)} />
          <TextField className="ccNameDetails" label="Name for Card" value={name} />
          <div className="ak-field-group">
            <label htmlFor="creditcard-month">Expiry Date</label>
            <div className="expiry-fields">
              {`${padMonth(expiryMonth)}/${expiryYear}`}
            </div>
          </div>
        </div>
        <div className="sidebar">
          <div className="logos">
            <SelectedCard type={type} />
          </div>
          <Button name="edit" className="paymentDetailsEdit" onClick={this.props.onEdit} iconAfter={EditIcon} >
            Edit
          </Button>
        </div>
      </CreditCard>
    );
  }
}

export default styled(DisplayCreditCard)`
  .ak-field-group {
    padding-top: 10px;
  }

  .ak-field-group > label + div{
    font-size: 1.125em;
    padding-left: 0.75em;
  }

  .payment-fields {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
  }
  
  .sidebar {
    align-self: flex-start;
    margin-left: auto;
    height: 100%;
    justify-content: space-between;
    display: flex;
    flex-direction: column;
  }
  
  .paymentDetailsEdit {
    align-self: flex-end;
  }

  .logos {
    align-self: flex-start;
    margin-top: 12px;
    margin-left: auto;
    > * {
      display: block;
      margin-bottom: 8px;
    }
  }
`;
