import { I18n } from '@lingui/react';
import { storiesOf } from '@storybook/react';
import * as React from 'react';

import { inContentChrome, mockedFunction, render } from 'bux/common/helpers/storybook';

import { errorMessages, nameForInputTypeMap } from './EditCreditCard';
import AddCreditCard from './EditCreditCard/AddCreditCard';

const props = {
  onSave: mockedFunction,
};

const errorsProps = (i18n, type) => {
  const messages = errorMessages(i18n);

  return {
    ...props,
    errors: Object.keys(messages).reduce((prev, curr) => {
      return { ...prev, [nameForInputTypeMap[curr]]: messages[curr][type] };
    }, {}),
  };
};

const WrappedCreditCard = (errorType?: string) => (
  <I18n>{({ i18n }) => (
    <div style={{ position: 'relative', width: '406px' }}>{
      render(AddCreditCard, errorsProps(i18n, errorType))
    }</div>
  )}</I18n>
);

storiesOf('BUX|Components/Credit Card', module)
  .add('Default', () => inContentChrome(WrappedCreditCard()))
  .add('Missing errors', () => inContentChrome(WrappedCreditCard('missing')))
  .add('Invalid errors', () => inContentChrome(WrappedCreditCard('invalid')))
  .add('Unsupported errors', () => inContentChrome(WrappedCreditCard('unsupported')));
