import sinon from 'sinon';
import proxyquire from 'bux/helpers/proxyquire';
import { expect } from 'chai';
import { mapGatewayErrors, handleGatewayErrors } from '../TNS';

describe('TNS Integration Script', () => {
  let createCreditCardSessionStub;
  let loadjsSpy;
  let TNS;

  const configurePaymentSession = (args = {}) => (
    new Promise((resolve, reject) => {
      TNS = proxyquire.noCallThru().load('../TNS', {
        loadjs: loadjsSpy,
        'bux/common/config': { tnsHostedSessionScript: 'https://tns/${MERCHANT_ID}/session.js' },
        'bux/common/api/billing-ux-service': {
          createCreditCardSession: createCreditCardSessionStub
        }
      }).default;

      TNS.configurePaymentSession({
        frameEmbeddingMitigation: ['x-frame-options'],
        ...args
      }).then(resolve).catch(reject);
    })
  );

  const mockPaymentSessionScript = (PaymentSession = {}) => {
    loadjsSpy = sinon.spy((script, callbacks) => {
      window.PaymentSession = {
        ...PaymentSession,
        onChange: sinon.spy(),
        onFocus: sinon.spy(),
        onBlur: sinon.spy(),
        onMouseOver: sinon.spy(),
        onMouseOut: sinon.spy()
      };
      callbacks.success();
    });
  };


  beforeEach(() => {
    createCreditCardSessionStub = sinon.stub();
    loadjsSpy = sinon.spy();
  });

  describe('TNS.configurePaymentSession', () => {
    it('should fail if BUX could not create a new session', () => {
      createCreditCardSessionStub.returns(Promise.reject('failed to get a new session'));
      return configurePaymentSession({ fields: {} }).then(Promise.reject, (error) => {
        expect(error).to.equal('failed to get a new session');
      });
    });

    it('should fail if BUX returns a malicious merchant id', () => {
      createCreditCardSessionStub.returns(Promise.resolve({
        creditCardSessionId: 'SESSION0001',
        merchantId: '../../badsession.js'
      }));
      return configurePaymentSession({ fields: {} }).then(Promise.reject, (error) => {
        expect(error).to.deep.equal({
          reason: 'system_error',
          payload: 'Invalid Merchant ID found: ../../badsession.js'
        });
      });
    });

    describe('when BUX returns a valid session', () => {
      beforeEach(() => {
        createCreditCardSessionStub.returns(Promise.resolve({
          creditCardSessionId: 'SESSION0001',
          merchantId: 'ATLTESTACCOUNT'
        }));
        mockPaymentSessionScript({
          configure: sinon.spy(configuration => configuration.callbacks.initialized({ status: 'ok' }))
        });
      });

      it('should load the TNS PaymentSession script into the page', () => (
        configurePaymentSession().then(() =>
          expect(loadjsSpy).to.have.been.calledWith('https://tns/ATLTESTACCOUNT/session.js'))
      ));

      it('should fail if TNS PaymentSession could not be loaded on the page', () => {
        loadjsSpy = (script, callbacks) => callbacks.error();
        return configurePaymentSession().then(Promise.reject, error => (
          expect(error).to.deep.equal({
            reason: 'system_error',
            payload: 'Error loading https://tns/ATLTESTACCOUNT/session.js'
          })
        ));
      });

      it('should fail if TNS PaymentSession script was incorrectly initialized', () => {
        loadjsSpy = (script, callbacks) => {
          window.PaymentSession = undefined;
          callbacks.success();
        };
        return configurePaymentSession().then(Promise.reject, error => (
          expect(error).to.deep.equal({ reason: 'system_error', payload: 'window.PaymentSession was not defined' })
        ));
      });

      it('should initialize TNS PaymentSession', () => (
        configurePaymentSession().then(() => {
          expect(window.PaymentSession.configure).to.have.been.called();
        })
      ));

      it('should set the frame embedding mitigation strategy to x-frame-options framebuster', () => (
        configurePaymentSession({ frameEmbeddingMitigation: ['x-frame-options'] }).then(() => {
          expect(window.PaymentSession.configure).to.have.been.calledWithMatch({
            frameEmbeddingMitigation: ['x-frame-options']
          });
        })
      ));

      it('should fail if no frame embedding mitigation strategy is given', () => (
        configurePaymentSession({ frameEmbeddingMitigation: [] }).then(Promise.reject, error => (
          expect(error).to.deep.equal({ reason: 'system_error', payload: 'No frame embedding mitigation specified' })
        ))
      ));

      it('should set the session from the createCreditCardSession response', () => (
        configurePaymentSession().then(() => {
          expect(window.PaymentSession.configure).to.have.been.calledWithMatch({ session: 'SESSION0001' });
        })
      ));

      it('should set the callbacks for TNS PaymentSession', () => (
        configurePaymentSession().then(() => {
          expect(window.PaymentSession.configure).to.have.been.calledWithMatch({
            callbacks: {
              initialized: sinon.match.func,
              formSessionUpdate: sinon.match.func
            }
          });
        })
      ));

      it('should set the fields for TNS PaymentSession', () => (
        configurePaymentSession({ fields: { cardNumber: '4000' } }).then(() => {
          expect(window.PaymentSession.configure).to.have.been.calledWithMatch({
            fields: { cardNumber: '4000' }
          });
        })
      ));

      it('should set callback when TNS field is focused', () => (
        configurePaymentSession().then(() => {
          expect(window.PaymentSession.onFocus).to.have.been.calledWithMatch(
            ['card.number', 'card.securityCode'],
            sinon.match.func
          );
        })
      ));

      it('should set callback when TNS field is blurred', () => (
        configurePaymentSession().then(() => {
          expect(window.PaymentSession.onBlur).to.have.been.calledWithMatch(
            ['card.number', 'card.securityCode'],
            sinon.match.func
          );
        })
      ));

      it('should set callback when TNS field is moused over', () => (
        configurePaymentSession().then(() => {
          expect(window.PaymentSession.onMouseOver).to.have.been.calledWithMatch(
            ['card.number', 'card.securityCode'],
            sinon.match.func
          );
        })
      ));

      it('should set callback when TNS field is moused out', () => (
        configurePaymentSession().then(() => {
          expect(window.PaymentSession.onMouseOut).to.have.been.calledWithMatch(
            ['card.number', 'card.securityCode'],
            sinon.match.func
          );
        })
      ));

      it('should set callback when TNS field changes', () => {
        const onFieldChange = sinon.spy();
        configurePaymentSession({ onChange: onFieldChange }).then(() => {
          expect(window.PaymentSession.onChange).to.have.been.calledWithMatch(
            ['card.number', 'card.securityCode'],
            onFieldChange
          );
        });
      });
    });

    describe('when TNS PaymentSession is configured', () => {
      beforeEach(() => {
        createCreditCardSessionStub.returns(Promise.resolve({
          creditCardSessionId: 'SESSION0001',
          merchantId: 'ATLTESTACCOUNT'
        }));
      });

      it('should return response if TNS PaymentSession initialisation was successful', () => {
        mockPaymentSessionScript({
          configure: sinon.spy(configuration =>
            configuration.callbacks.initialized({ status: 'ok', data: 'extra data' }))
        });
        return configurePaymentSession({}).then((response) => {
          expect(response).to.deep.equal({ status: 'ok', data: 'extra data' });
        });
      });

      it('should fail if TNS PaymentSession request times out', () => {
        mockPaymentSessionScript({
          configure: sinon.spy(configuration => configuration.callbacks.initialized({
            status: 'request_timeout',
            errors: { message: 'I timed out' }
          }))
        });
        return configurePaymentSession({}).then(Promise.reject, (error) => {
          expect(error).to.deep.equal({ reason: 'request_timeout', payload: 'I timed out' });
        });
      });

      it('should fail if TNS PaymentSession has a system error', () => {
        mockPaymentSessionScript({
          configure: sinon.spy(configuration => configuration.callbacks.initialized({
            status: 'system_error',
            errors: { message: 'I broke' }
          }))
        });
        return configurePaymentSession({}).then(Promise.reject, (error) => {
          expect(error).to.deep.equal({ reason: 'system_error', payload: 'I broke' });
        });
      });

      it('should fail if TNS PaymentSession finds an unusual error', () => {
        mockPaymentSessionScript({
          configure: sinon.spy(configuration =>
            configuration.callbacks.initialized({ status: 'unknown_error', data: [] }))
        });
        return configurePaymentSession({}).then(Promise.reject, (error) => {
          expect(error).to.deep.equal({ reason: 'unknown', payload: { status: 'unknown_error', data: [] } });
        });
      });
    });
  });

  describe('TNS.updateCurrentSession', () => {
    let formSessionUpdateCallback;

    beforeEach((done) => {
      createCreditCardSessionStub.returns(Promise.resolve({
        creditCardSessionId: 'SESSION0001',
        merchantId: 'ATLTESTACCOUNT'
      }));
      mockPaymentSessionScript({
        configure: sinon.spy((configuration) => {
          configuration.callbacks.initialized({ status: 'ok' });
          formSessionUpdateCallback = configuration.callbacks.formSessionUpdate;
        })
      });

      configurePaymentSession().then(() => done());
    });

    it('should parse the response when it succeeds', () => {
      window.PaymentSession.updateSessionFromForm = sinon.spy(() => formSessionUpdateCallback({
        status: 'ok',
        session: {
          id: 'SESSION000000000000000000000001'
        },
        sourceOfFunds: {
          provided: {
            card: {
              brand: 'MAESTRO',
              number: '111111xxxxxx1111',
              expiry: {
                month: '5',
                year: '22'
              },
              scheme: 'MASTERCARD',
              securityCode: 'xxx'
            }
          }
        }
      }));
      return TNS.updateCurrentSession().then((parsedResponse) => {
        expect(parsedResponse).to.deep.equal({
          expiryMonth: '5',
          expiryYear: '22',
          maskedCardNumber: '111111xxxxxx1111',
          securityCode: 'xxx',
          sessionId: 'SESSION000000000000000000000001',
          type: 'MAESTRO'
        });
      });
    });

    it('should fail if TNS PaymentSession is not present on the page', () => {
      window.PaymentSession = undefined;
      return TNS.updateCurrentSession().then(Promise.reject, error => (
        expect(error).to.deep.equal({ reason: 'system_error', payload: 'window.PaymentSession was not defined' })
      ));
    });

    describe('attempting to update the session from the form', () => {
      it('should fail if TNS PaymentSession finds errors with the fields', () => {
        window.PaymentSession.updateSessionFromForm = sinon.spy(() =>
          formSessionUpdateCallback({ status: 'fields_in_error', errors: ['message'] }));
        return TNS.updateCurrentSession().then(Promise.reject, (error) => {
          expect(error).to.deep.equal({ reason: 'invalid_fields', payload: ['message'] });
        });
      });

      it('should fail if TNS PaymentSession request times out', () => {
        window.PaymentSession.updateSessionFromForm = sinon.spy(() =>
          formSessionUpdateCallback({ status: 'request_timeout', errors: { message: 'I timed out' } }));
        return TNS.updateCurrentSession().then(Promise.reject, (error) => {
          expect(error).to.deep.equal({ reason: 'request_timeout', payload: 'I timed out' });
        });
      });

      it('should fail if TNS PaymentSession has a system error', () => {
        window.PaymentSession.updateSessionFromForm = sinon.spy(() =>
          formSessionUpdateCallback({ status: 'system_error', errors: { message: 'System down' } }));
        return TNS.updateCurrentSession().then(Promise.reject, (error) => {
          expect(error).to.deep.equal({ reason: 'system_error', payload: 'System down' });
        });
      });

      it('should fail if TNS PaymentSession finds an unusual error', () => {
        window.PaymentSession.updateSessionFromForm = sinon.spy(() =>
          formSessionUpdateCallback({ status: 'unknown_error', data: [] }));
        return TNS.updateCurrentSession().then(Promise.reject, (error) => {
          expect(error).to.deep.equal({ reason: 'unknown', payload: { status: 'unknown_error', data: [] } });
        });
      });
    });
  });

  describe('handleGatewayErrors', () => {
    it('should pass ok', () => {
      const ok = { status: 'ok' };
      expect(handleGatewayErrors(ok)).to.be.equal(ok);
    });

    it('should reject with a Promise', () => {
      const unknown = { status: 'undefined' };
      return handleGatewayErrors(unknown)
        .then(
          () => { throw new Error('should reject'); },
          err => expect(err).to.be.deep.equal({
            reason: 'unknown',
            payload: unknown
          })
        );
    });

    it('should map error states', () => {
      const unknown = { status: 'undefined' };
      expect(mapGatewayErrors(unknown)).to.be.deep.equal({
        reason: 'unknown',
        payload: unknown
      });

      const fieldsInError = {
        status: 'fields_in_error',
        errors: 42
      };

      expect(mapGatewayErrors(fieldsInError)).to.be.deep.equal({
        reason: 'invalid_fields',
        payload: 42
      });

      const systemError1 = {
        status: 'system_error',
        errors: {
          message: 42
        }
      };
      expect(mapGatewayErrors(systemError1)).to.be.deep.equal({
        reason: 'system_error',
        payload: 42
      });
      const systemError2 = {
        status: 'system_error',
        errors: 42
      };
      expect(mapGatewayErrors(systemError2)).to.be.deep.equal({
        reason: 'system_error',
        payload: 42
      });
      const systemError3 = {
        status: 'system_error',
        message: 42
      };
      expect(mapGatewayErrors(systemError3)).to.be.deep.equal({
        reason: 'system_error',
        payload: systemError3
      });
    });
  });
});
