import loadjs from 'loadjs';
import config from 'bux/common/config';
import { Deferred } from 'bux/common/helpers/promise';
import { createCreditCardSession } from 'bux/common/api/billing-ux-service';

const toggleHover = isActive => (selector) => {
  document.querySelector(selector).classList.toggle('ak-hover', isActive);
};

const toggleFocus = isActive => (selector) => {
  document.querySelector(selector).classList.toggle('ak-focus', isActive);
};

const getPaymentSessionObject = () => {
  if (window.PaymentSession) {
    return Promise.resolve(window.PaymentSession);
  }
  return Promise.reject({ reason: 'system_error', payload: 'window.PaymentSession was not defined' });
};

const parseSessionCallback = (response) => {
  const { session, sourceOfFunds: { provided: { card } } } = response;
  const {
    number, brand, expiry: { month, year }, securityCode 
  } = card;
  return {
    sessionId: session.id,
    maskedCardNumber: number,
    expiryMonth: month,
    expiryYear: year,
    type: brand,
    securityCode
  };
};

let sessionInitialised = new Deferred();
let sessionUpdated = new Deferred();
const onSessionInitialised = payload => sessionInitialised.resolve(payload);
const onSessionUpdated = payload => sessionUpdated.resolve(payload);

const getErrorMessage = response =>
  (response && response.errors ? response.errors.message : '') ||
  response.errors ||
  response;

export const mapGatewayErrors = (response) => {
  switch (response.status) {
    case 'fields_in_error':
      return { reason: 'invalid_fields', payload: response.errors };
    case 'request_timeout':
      return { reason: 'request_timeout', payload: getErrorMessage(response) };
    case 'system_error':
      return { reason: 'system_error', payload: getErrorMessage(response) };
    default:
      return { reason: 'unknown', payload: response };
  }
};

export const handleGatewayErrors = (response) => {
  switch (response.status) {
    case 'ok':
      return response;
    default:
      return Promise.reject(mapGatewayErrors(response));
  }
};

const getTnsHostedSessionScriptFromMerchantId = (merchantId) => {
  if (merchantId && /^\w+$/.test(merchantId)) {
    return config.tnsHostedSessionScript.replace('${MERCHANT_ID}', merchantId);
  }
  return undefined;
};

const configurePaymentSession = ({ fields = {}, frameEmbeddingMitigation, onChange }) => {
  const loadPaymentGatewayScript = () =>
    createCreditCardSession().then(({ creditCardSessionId, merchantId }) => {
      const tnsLoaded = new Deferred();
      const tnsHostedSessionScript = getTnsHostedSessionScriptFromMerchantId(merchantId);
      if (!tnsHostedSessionScript) {
        return Promise.reject({ reason: 'system_error', payload: `Invalid Merchant ID found: ${merchantId}` });
      }
      loadjs(tnsHostedSessionScript, {
        success: () => tnsLoaded.resolve(creditCardSessionId),
        error: () => tnsLoaded.reject({
          reason: 'system_error',
          payload: `Error loading ${tnsHostedSessionScript}`
        })
      });
      return tnsLoaded.promise;
    });

  const configureTns = ([creditCardSessionId, PaymentSession]) => {
    sessionInitialised = new Deferred();
    if (!frameEmbeddingMitigation || !frameEmbeddingMitigation.length) {
      return Promise.reject({ reason: 'system_error', payload: 'No frame embedding mitigation specified' });
    }
    const configuration = {
      fields,
      formatCard: true,
      frameEmbeddingMitigation,
      session: creditCardSessionId,
      callbacks: {
        initialized: onSessionInitialised,
        formSessionUpdate: onSessionUpdated
      }
    };
    PaymentSession.configure(configuration);
    if (onChange) {
      PaymentSession.onChange(['card.number', 'card.securityCode'], onChange);
    }
    PaymentSession.onFocus(['card.number', 'card.securityCode'], toggleFocus(true));
    PaymentSession.onBlur(['card.number', 'card.securityCode'], toggleFocus(false));
    PaymentSession.onMouseOver(['card.number', 'card.securityCode'], toggleHover(true));
    PaymentSession.onMouseOut(['card.number', 'card.securityCode'], toggleHover(false));
    return sessionInitialised.promise;
  };

  const initialiseTNS = () =>
    loadPaymentGatewayScript().then(token => getPaymentSessionObject().then(PaymentSession => [token, PaymentSession]));

  return initialiseTNS()
    .then(configureTns)
    .then(handleGatewayErrors);
};

const updateCurrentSession = () => {
  const updateTnsSession = (PaymentSession) => {
    sessionUpdated = new Deferred();
    PaymentSession.updateSessionFromForm('card');
    return sessionUpdated.promise;
  };
  return getPaymentSessionObject()
    .then(updateTnsSession)
    .then(handleGatewayErrors)
    .then(parseSessionCallback);
};

export default {
  configurePaymentSession,
  updateCurrentSession
};
