import PropTypes from 'prop-types';
import React, { Component } from 'react';
import EditCreditCard from './EditCreditCard';
import DisplayCreditCard from './DisplayCreditCard';
import CreditCardChrome from './styled/CreditCardChrome';

export default class CreditCard extends Component {
  static propTypes = {
    className: PropTypes.string,
    saveCreditCard: PropTypes.func,
    details: PropTypes.shape({
      expiryMonth: PropTypes.number,
      expiryYear: PropTypes.number,
      name: PropTypes.string,
      suffix: PropTypes.string,
      type: PropTypes.string,
    }),
  };

  constructor(props) {
    super(props);
    this.state = {
      inFocusedEditMode: false,
      inInlineEditMode: !(props.details && Object.keys(props.details).length),
    };
  }

  onEdit = () => {
    this.setState({ inFocusedEditMode: true });
  };

  onEditCancel = () => {
    this.setState({ inFocusedEditMode: false });
  };

  getCreditCardData = () => {
    if (this.creditCard) {
      return this.creditCard.getCreditCardData();
    }

    return Promise.resolve();
  };

  setCreditCardRef = creditCard => (this.creditCard = creditCard);

  render() {
    const { inFocusedEditMode } = this.state;
    const { className, saveCreditCard, details } = this.props;
    return (
      <CreditCardChrome onBackgroundClick={this.onEditCancel} focused={inFocusedEditMode}>
        {
          this.state.inFocusedEditMode || this.state.inInlineEditMode
            ? (
              <EditCreditCard
                ref={this.setCreditCardRef}
                className={className}
                onEditCancel={this.onEditCancel}
                saveCreditCard={inFocusedEditMode ? saveCreditCard : undefined}
              />
            ) : (
              <DisplayCreditCard
                className={className}
                details={details}
                onEdit={this.onEdit}
              />
            )
        }
      </CreditCardChrome>
    );
  }
}
