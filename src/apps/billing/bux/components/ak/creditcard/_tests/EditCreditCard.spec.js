import React from 'react';
import { expect } from 'chai';
import { i18n } from 'bux/helpers/jsLingui';
import { mount } from 'bux/helpers/enzyme-v2';
import sinon from 'sinon';
import proxyquire from 'bux/helpers/proxyquire';
import Select from '@atlaskit/select';
import Button from '@atlaskit/button';
import getAnalytic from '../../../log/helpers/getAnalytic';
import { StackedCreditCard } from '../styled/CreditCard';
import AddCreditCard from '../EditCreditCard/AddCreditCard';
import ErrorCreditCard from '../EditCreditCard/ErrorCreditCard';
import LoadingCreditCard from '../EditCreditCard/LoadingCreditCard';

const waitForPaymentGatewayToConfigure = () => new Promise(resolve => setImmediate(resolve));

// THIS TESTS USES ENZYME 2-to-3 adapter.

describe('EditCreditCard:', () => {
  let paymentGateway;
  let EditCreditCard;

  beforeEach(() => {
    paymentGateway = { configurePaymentSession: sinon.stub(), updateCurrentSession: sinon.stub() };

    EditCreditCard = proxyquire.noCallThru().load('../EditCreditCard', {
      '../internal/TNS': paymentGateway
    }).default;
  });

  describe('when the payment gateway is loading', () => {
    let wrapper;

    beforeEach(() => {
      const pendingPromise = new Promise(() => {});
      paymentGateway.configurePaymentSession.returns(pendingPromise);
      wrapper = mount(<EditCreditCard i18n={i18n} />, getAnalytic().context);
    });

    it('should render the loading state on top of the credit card input', () => (
      waitForPaymentGatewayToConfigure().then(() => {
        expect(wrapper.find(StackedCreditCard).children()).to.have.length(2);
        expect(wrapper.find(StackedCreditCard)).to.containMatchingElement(<AddCreditCard />);
        expect(wrapper.find(StackedCreditCard)).to.containMatchingElement(<LoadingCreditCard />);
      })
    ));

    it('should return an error when getCreditCardData is called', () => (
      wrapper.instance().getCreditCardData().then(Promise.reject, (data) => {
        expect(data).to.deep.equal({ reason: 'system_error', payload: 'Session has not been configured' });
      })
    ));

    describe('when initialising the payment gateway', () => {
      it('should set the embeddedFrameMitigation strategy to x-frame-options', () => {
        expect(paymentGateway.configurePaymentSession).to.have.been.calledWithMatch({
          frameEmbeddingMitigation: ['x-frame-options']
        });
      });

      it('should register the fields with the payment gateway', () => {
        expect(paymentGateway.configurePaymentSession).to.have.been.calledWithMatch({
          fields: {
            card: {
              number: '#creditcard-number',
              securityCode: '#creditcard-security',
              expiryMonth: '[name="creditcard-month"]',
              expiryYear: '[name="creditcard-year"]'
            }
          }
        });
      });

      describe('when registering the fields on the credit card input', () => {
        let configuredArguments;

        beforeEach(() => {
          configuredArguments = paymentGateway.configurePaymentSession.firstCall.args[0];
        });

        it('should register the credit card number field', () => {
          const { fields: { card: { number } } } = configuredArguments;
          expect(wrapper.find(AddCreditCard)).to.have.descendants(number);
        });

        it('should register the security code field', () => {
          const { fields: { card: { securityCode } } } = configuredArguments;
          expect(wrapper.find(AddCreditCard)).to.have.descendants(securityCode);
        });

        it('should register the expiry month field', () => {
          const { fields: { card: { expiryMonth } } } = configuredArguments;
          expect(wrapper.find(AddCreditCard)).to.have.descendants(expiryMonth);
        });

        it('should register the expiry year field', () => {
          const { fields: { card: { expiryYear } } } = configuredArguments;
          expect(wrapper.find(AddCreditCard)).to.have.descendants(expiryYear);
        });
      });
    });
  });

  describe('when the payment gateway is successfully loaded', () => {
    let wrapper;
    let onReady;

    beforeEach(() => {
      onReady = sinon.stub();
      paymentGateway.configurePaymentSession.resolves({});
      wrapper = mount(<EditCreditCard onReady={onReady} i18n={i18n} />, getAnalytic().context);
    });

    it('should only render the credit card input', () => (
      waitForPaymentGatewayToConfigure().then(() => {
        expect(wrapper.find(StackedCreditCard).children()).to.have.length(1);
        expect(wrapper.find(StackedCreditCard)).to.containMatchingElement(<AddCreditCard />);
      })
    ));

    it('should call onReady', () => (
      waitForPaymentGatewayToConfigure().then(() => {
        expect(onReady).to.have.been.called();
      })
    ));

    describe('when a valid session is returned', () => {
      beforeEach(() => {
        paymentGateway.updateCurrentSession.resolves({
          sessionId: 'session',
          maskedCardNumber: '4111xxxxxxxxx1111',
          expiryMonth: 9,
          expiryYear: 25,
          type: 'VISA',
          securityCode: 'xxx'
        });
      });

      it('should return the session information when getCreditCardData is called', () => (
        waitForPaymentGatewayToConfigure().then(() => {
          wrapper.find('input#creditcard-name').simulate('change', {
            target: {
              value: 'Michael Scott',
              name: 'creditcard-name'
            }
          });
          return wrapper.instance().getCreditCardData().then((data) => {
            expect(data).to.deep.equal({
              name: 'Michael Scott',
              sessionId: 'session',
              maskedCardNumber: '4111xxxxxxxxx1111',
              expiryMonth: 9,
              expiryYear: '2025',
              type: 'VISA',
              securityCode: 'xxx'
            });
          });
        })
      ));
    });

    describe('when a invalid session is returned', () => {
      beforeEach(() => {
        paymentGateway.updateCurrentSession.rejects({
          reason: 'system_error',
          payload: 'window.PaymentSession was not defined'
        });
      });

      it('should return an error when getCreditCardData is called', () => (
        waitForPaymentGatewayToConfigure().then(() =>
          wrapper.instance().getCreditCardData().then(Promise.reject, (data) => {
            expect(data).to.deep.equal({
              reason: 'system_error',
              payload: 'window.PaymentSession was not defined'
            });
          }))
      ));
    });


    describe('when a session with invalid fields is returned', () => {
      beforeEach(() => {
        paymentGateway.updateCurrentSession.rejects({
          reason: 'invalid_fields',
          payload: { cardNumber: 'missing', securityCode: 'missing' }
        });
      });

      it('should return an error when getCreditCardData is called', () => (
        waitForPaymentGatewayToConfigure().then(() =>
          wrapper.instance().getCreditCardData().then(Promise.reject, (data) => {
            expect(data).to.deep.equal({
              reason: 'invalid_fields',
              payload: {
                cardNumber: 'missing',
                securityCode: 'missing',
                cardName: 'missing'
              }
            });
          }))
      ));
    });
  });

  describe('when the payment gateway fails to load', () => {
    let wrapper;

    beforeEach(() => {
      paymentGateway.configurePaymentSession.returns(Promise.reject({ reason: 'system_error', payload: 'error' }));
      wrapper = mount(<EditCreditCard i18n={i18n} />, getAnalytic().context);
    });

    it('should only render the error state', () => (
      waitForPaymentGatewayToConfigure().then(() => {
        expect(wrapper.find(StackedCreditCard).children()).to.have.length(1);
        expect(wrapper.find(StackedCreditCard)).to.containMatchingElement(<ErrorCreditCard />);
      })
    ));

    it('should render the credit card form after recovering from an error', () => (
      waitForPaymentGatewayToConfigure().then(() => {
        paymentGateway.configurePaymentSession.resolves({});
        wrapper.find(EditCreditCard).find(Button).simulate('click');
        return waitForPaymentGatewayToConfigure().then(() => {
          expect(wrapper.find(StackedCreditCard).children()).to.have.length(1);
          expect(wrapper.find(StackedCreditCard)).to.containMatchingElement(<AddCreditCard />);
        });
      })
    ));

    it('should return an error when getCreditCardData is called', () => (
      waitForPaymentGatewayToConfigure().then(() =>
        wrapper.instance().getCreditCardData().then(Promise.reject, (data) => {
          expect(data).to.deep.equal({
            reason: 'system_error',
            payload: 'error'
          });
        }))
    ));
  });

  describe('when entering in billing details', () => {
    let wrapper;
    let onIFrameFieldChange;

    const triggerIFrameFieldChange = (inputSelector, fieldErrors) => {
      paymentGateway.updateCurrentSession.rejects({
        reason: 'invalid_fields', payload: { ...fieldErrors }
      });
      onIFrameFieldChange(inputSelector);
      return waitForPaymentGatewayToConfigure();
    };

    const setInvalidFieldErrors = (payload) => {
      paymentGateway.updateCurrentSession.rejects({
        reason: 'invalid_fields', payload
      });
    };

    const fillCCDetails = () => {
      wrapper.find('#creditcard-number').simulate('change', {
        target: {
          name: 'creditcard-number',
          value: '123 555 444'
        }
      });
      wrapper.find('#creditcard-name').simulate('change', { target: { name: 'creditcard-name', value: 'Bruce' } });
    };

    const fillRequiredData = () => {
      wrapper.find('#creditcard-name').simulate('change', {
        target: { name: 'creditcard-name', value: 'Bruce Wayne' }
      });
    };

    const setValidSession = (override = {}) => {
      paymentGateway.updateCurrentSession.resolves({
        sessionId: 'SESSION',
        maskedCardNumber: 'xxxx8769',
        expiryMonth: '05',
        expiryYear: '2021',
        type: 'VISA',
        securityCode: 'xxx',
        ...override
      });
      return waitForPaymentGatewayToConfigure();
    };

    beforeEach(() => {
      paymentGateway.configurePaymentSession.resolves({});
      setValidSession();
      wrapper = mount(<EditCreditCard i18n={i18n} />, getAnalytic().context);
      onIFrameFieldChange = paymentGateway.configurePaymentSession.firstCall.args[0].onChange;
    });

    it('should display all vendors', () =>
      expect(wrapper.find('li.cc-vendor--active')).to.have.length(3));

    it('should display keep one vendor after validation', () =>
      setValidSession()
        .then(fillCCDetails)
        .then(() => onIFrameFieldChange('#creditcard-number'))
        .then(waitForPaymentGatewayToConfigure)
        .then(() => expect(wrapper.find('.cc-vendor--active')).to.have.length(1))
        .then(() => expect(wrapper.find('#creditcard-number-error')).not.to.be.present()));


    it('should display error if vendor is not supported', () =>
      setValidSession({ type: 'Maestro' })
        .then(fillCCDetails)
        .then(() => onIFrameFieldChange('#creditcard-number'))
        .then(waitForPaymentGatewayToConfigure)
        .then(() => expect(wrapper.find('.cc-vendor--active')).to.have.length(0))
        .then(() => expect(wrapper.find('#creditcard-number-error'))
          .to.have.text('Only Visa, MasterCard and AMEX are supported'))
        // then test flag reset
        .then(() => setValidSession())
        .then(fillCCDetails)
        .then(() => onIFrameFieldChange('#creditcard-number'))
        .then(waitForPaymentGatewayToConfigure)
        .then(() => expect(wrapper.find('.cc-vendor--active')).to.have.length(3)) // validate is debounced
        .then(() => expect(wrapper.find('#creditcard-number-error')).not.to.be.present()));


    describe('for credit card number', () => {
      it('should show an error when missing', () =>
        triggerIFrameFieldChange('#creditcard-number', { cardNumber: 'missing' }).then(() => {
          expect(wrapper.find('#creditcard-number-error'))
            .to.have.text('Please enter a valid credit card number');
        }));

      it('should show an error when invalid', () =>
        triggerIFrameFieldChange('#creditcard-number', { cardNumber: 'invalid' }).then(() => {
          expect(wrapper.find('#creditcard-number-error'))
            .to.have.text('Please enter a valid credit card number');
        }));

      it('should not render an error when field is not dirty', () =>
        triggerIFrameFieldChange('#creditcard-different', { cardNumber: 'missing' }).then(() => {
          expect(wrapper.find('#creditcard-number-error').exists()).to.be.false();
        }));

      it('should render an error when field is not dirty but creditcard form has been submitted', () =>
        wrapper.instance().getCreditCardData().then(Promise.reject, () =>
          triggerIFrameFieldChange('#creditcard-different', { cardNumber: 'missing' }).then(() => {
            expect(wrapper.find('#creditcard-number-error'))
              .to.have.text('Please enter a valid credit card number');
          })));

      it('should not render an error when form has been submitted but iframe field change was not triggered', () => {
        setInvalidFieldErrors({ cardNumber: 'missing' });
        fillRequiredData();
        setValidSession();
        return wrapper.instance().getCreditCardData().then(() => {
          expect(wrapper.find('#creditcard-number-error').exists()).to.be.false();
        });
      });
    });
    describe('for credit card name', () => {
      it('should show render an error when missing', () => {
        wrapper.find('#creditcard-name').simulate('change', {
          target: { name: 'creditcard-name', value: 'Bruce Wayne' }
        });
        wrapper.find('#creditcard-name').simulate('change', { target: { name: 'creditcard-name', value: '' } });
        return waitForPaymentGatewayToConfigure().then(() => {
          expect(wrapper.find('#creditcard-name-error'))
            .to.have.text('Please enter a valid cardholder name');
        });
      });

      it('should show render an error when invalid', () => {
        wrapper.find('#creditcard-name').simulate('change', { target: { name: 'creditcard-name', value: '    ' } });
        return waitForPaymentGatewayToConfigure().then(() => {
          expect(wrapper.find('#creditcard-name-error'))
            .to.have.text('Please enter a valid cardholder name');
        });
      });

      it('should not render an error when field is not dirty', () =>
        waitForPaymentGatewayToConfigure().then(() => {
          expect(wrapper.find('#creditcard-name-error').exists()).to.be.false();
        }));

      it('should render an error when field is not dirty but creditcard form has been submitted', () =>
        wrapper.instance().getCreditCardData().then(Promise.reject, () => {
          expect(wrapper.find('#creditcard-name-error'))
            .to.have.text('Please enter a valid cardholder name');
        }));

      describe('with frozen time', () => {
        let clock;
        beforeEach(() => {
          clock = sinon.useFakeTimers();
        });
        afterEach(() => {
          clock.restore();
        });
        it('should debounce updates to TNS on field change', () => {
          wrapper.find('#creditcard-name').simulate('change', {
            target: { name: 'creditcard-name', value: '' }
          });
          expect(paymentGateway.updateCurrentSession).to.have.been.calledOnce();

          wrapper.find('#creditcard-name').simulate('change', {
            target: { name: 'creditcard-name', value: 'Bruce' }
          });
          wrapper.find('#creditcard-name').simulate('change', {
            target: { name: 'creditcard-name', value: 'Bruce Wayne' }
          });

          clock.tick(299);
          expect(paymentGateway.updateCurrentSession).to.have.been.calledOnce();
          clock.tick(1);
          expect(paymentGateway.updateCurrentSession).to.have.been.calledTwice();
        });
      });
    });
    describe('for credit card expiry', () => {
      let setMonthSelectAsDirty;
      let setYearSelectAsDirty;
      let setValidMonth;

      beforeEach(() => {
        const monthSelect = wrapper.find('#creditcard-month').closest(Select);
        const yearSelect = wrapper.find('#creditcard-year').closest(Select);
        setMonthSelectAsDirty = () => {
          monthSelect.simulate('keyDown', { key: 'ArrowDown' });
          monthSelect.simulate('keyDown', { key: 'Enter' });
        };
        setYearSelectAsDirty = () => {
          yearSelect.simulate('keyDown', { key: 'ArrowDown' });
          yearSelect.simulate('keyDown', { key: 'Enter' });
        };
        setValidMonth = () => {
          const currentMonth = new Date().getMonth();
          for (let i = currentMonth; i >= 0; i -= 1) {
            monthSelect.simulate('keyDown', { key: 'ArrowDown' });
          }
          monthSelect.simulate('keyDown', { key: 'Enter' });
        };
      });

      it('should not render an error when field is not dirty', () => {
        setInvalidFieldErrors({ expiryMonth: 'missing', expiryYear: 'missing' });
        return waitForPaymentGatewayToConfigure().then(() => {
          expect(wrapper.find('.expiry-fields + span.ak-error').exists()).to.be.false();
        });
      });

      it('should render an error when field is not dirty but creditcard form has been submitted', () => {
        setInvalidFieldErrors({ expiryMonth: 'missing', expiryYear: 'missing' });
        return wrapper.instance().getCreditCardData().then(Promise.reject, () =>
          waitForPaymentGatewayToConfigure().then(() => {
            expect(wrapper.find('.expiry-fields + span.ak-error')).to.have.text('Please enter a valid expiry date');
          }));
      });

      it('should not render an error when a month is selected ', () => {
        setInvalidFieldErrors({ expiryYear: 'missing' });
        setMonthSelectAsDirty();

        return waitForPaymentGatewayToConfigure().then(() => {
          expect(wrapper.find('.expiry-fields + span.ak-error').exists()).to.be.false();
        });
      });

      it('should not render an error when a valid month and year is selected ', () => {
        setInvalidFieldErrors({});
        setValidMonth();
        setYearSelectAsDirty(); // We only show valid years

        return waitForPaymentGatewayToConfigure().then(() => {
          expect(wrapper.find('.expiry-fields + span.ak-error').exists()).to.be.false();
        });
      });

      it('should render an error when an invalid month and year is selected ', () => {
        // this test would set date to 1 Jan Year. Would not work in any Jan
        // mock selector directly
        wrapper.find(EditCreditCard).instance().hasCreditCardDateExpired = () => true;
        setInvalidFieldErrors({});
        setMonthSelectAsDirty();
        setYearSelectAsDirty();

        return waitForPaymentGatewayToConfigure().then(() => {
          expect(wrapper.find('.expiry-fields + span.ak-error').exists()).to.be.true();
        });
      });

      it('should render an error when the month is invalid', () => {
        setInvalidFieldErrors({ expiryMonth: 'invalid' });
        setMonthSelectAsDirty();
        setYearSelectAsDirty();

        return waitForPaymentGatewayToConfigure().then(() => {
          expect(wrapper.find('.expiry-fields + span.ak-error')).to.have.text('Please enter a valid expiry date');
        });
      });

      it('should render an error when the year is invalid', () => {
        setInvalidFieldErrors({ expiryYear: 'invalid' });
        setMonthSelectAsDirty();
        setYearSelectAsDirty();

        return waitForPaymentGatewayToConfigure().then(() => {
          expect(wrapper.find('.expiry-fields + span.ak-error')).to.have.text('Please enter a valid expiry date');
        });
      });
    });

    describe('for credit card security code', () => {
      it('should show an error when missing', () =>
        triggerIFrameFieldChange('#creditcard-security', { securityCode: 'missing' }).then(() => {
          expect(wrapper.find('#creditcard-security-error'))
            .to.have.text('Please enter a valid CCV');
        }));

      it('should show an error when invalid', () =>
        triggerIFrameFieldChange('#creditcard-security', { securityCode: 'invalid' }).then(() => {
          expect(wrapper.find('#creditcard-security-error'))
            .to.have.text('Please enter a valid CCV');
        }));

      it('should not render an error when field is not dirty', () =>
        triggerIFrameFieldChange('#creditcard-different', { securityCode: 'missing' }).then(() => {
          expect(wrapper.find('#creditcard-security-error').exists()).to.be.false();
        }));

      it('should render an error when field is not dirty but creditcard form has been submitted', () =>
        wrapper.instance().getCreditCardData().then(Promise.reject, () =>
          triggerIFrameFieldChange('#creditcard-different', { securityCode: 'missing' }).then(() => {
            expect(wrapper.find('#creditcard-security-error'))
              .to.have.text('Please enter a valid CCV');
          })));

      it('should not render an error when form has been submitted but iframe field change was not triggered', () => {
        setInvalidFieldErrors({ securityCode: 'missing' });
        fillRequiredData();
        setValidSession();
        return wrapper.instance().getCreditCardData().then(() => {
          expect(wrapper.find('#creditcard-security-error').exists()).to.be.false();
        });
      });
    });
  });
});
