import React from 'react';
import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import sinon from 'sinon';
import Select from '@atlaskit/select';
import SubmitButton from '../../../behaviour/state/submitable';
import getAnalytic from '../../../log/helpers/getAnalytic';
import { AddCreditCardComponent } from '../EditCreditCard/AddCreditCard';

describe('AddCreditCardComponent:', () => {
  describe('Display CreditCard:', () => {
    let clock;
    let wrapper;

    beforeEach(() => {
      const fakedTime = new Date('2017-06-06').getTime();
      clock = sinon.useFakeTimers(fakedTime);
      wrapper = mount(<AddCreditCardComponent />);
    });

    afterEach(() => {
      clock.restore();
    });

    it('should render a list of months for the expiry', () => {
      const expiryMonthSelect = wrapper.find(Select).get(0);
      const items = expiryMonthSelect.props.options;
      expect(items).to.be.an('array').that.has.length(12);

      expect(items).to.deep.include({ value: 1, label: '01' });
      expect(items).to.deep.include({ value: 12, label: '12' });
    });

    it('should render a list of years for the expiry starting from the current year', () => {
      const expiryYearSelect = wrapper.find(Select).get(1);
      const items = expiryYearSelect.props.options;
      expect(items).to.be.an('array').that.has.length(26);
      expect(items).to.deep.include({ value: 2017, label: '2017' });
      expect(items).to.deep.include({ value: 2042, label: '2042' });
    });
  });

  describe('Submit CreditCard:', () => {
    it('should not have submit button', () => {
      const wrapper = shallow(<AddCreditCardComponent />);
      expect(wrapper.find(SubmitButton)).not.to.be.present();
    });

    it('should submit a form', () => {
      const submit = sinon.stub().resolves(true);
      const wrapper = mount(<AddCreditCardComponent onSave={submit} />, getAnalytic().context);
      wrapper.find(SubmitButton).simulate('click');
      expect(submit).to.be.called();
    });
  });
});
