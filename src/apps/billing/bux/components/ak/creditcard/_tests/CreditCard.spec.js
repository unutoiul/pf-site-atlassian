import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import Button from '@atlaskit/button';
import fetchMock from 'fetch-mock-5';
import config from 'bux/common/config';
import getAnalytic from '../../../log/helpers/getAnalytic';

import DisplayCreditCard from '../DisplayCreditCard';
import EditCreditCard from '../EditCreditCard';
import CreditCard from '..';

describe('CreditCard:', () => {
  let wrapper;
  let details;
  let sendAnalyticEvent;

  beforeEach(() => {
    const creditCardURL = `${config.billingUXServiceUrl}/api/credit-card/session`;
    fetchMock.post(creditCardURL, {
      creditCardSessionId: 'SESSION000000000000000000000001',
      creditCardToken: '5123450000002346'
    });

    details = {
      expiryMonth: 12,
      expiryYear: 2017,
      name: 'Bruce Wayne',
      suffix: '1234',
      type: 'VISA',
    };

    const analytics = getAnalytic();
    sendAnalyticEvent = analytics.sendAnalyticEvent;

    wrapper = mount(<CreditCard
      details={details}
    />, analytics.context);
  });

  it('should display credit card information when fed with details', () => {
    const elem = wrapper.find(DisplayCreditCard);
    expect(elem).to.be.present();
    expect(wrapper).to.have.state('inFocusedEditMode', false);
    expect(wrapper).to.have.state('inInlineEditMode', false);
  });

  it('should display EditCreditCard when [ Edit ✏️  ] is clicked', () => {
    expect(wrapper).to.have.state('inFocusedEditMode', false);


    wrapper.find(DisplayCreditCard).find(Button).simulate('click');

    expect(sendAnalyticEvent).to.be.calledWith('edit');

    expect(wrapper).to.have.state('inFocusedEditMode', true);

    const elem = wrapper.find(EditCreditCard);
    expect(elem).to.be.present();
  });

  it('should display EditCreditCard when there are no details', () => {
    wrapper = mount(<CreditCard />);
    expect(wrapper).to.have.state('inInlineEditMode', true);

    const elem = wrapper.find(EditCreditCard);
    expect(elem).to.be.present();
  });
});
