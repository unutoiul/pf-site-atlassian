import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';
import Loading from 'bux/components/Loading';
import { akColorN20 } from '@atlaskit/util-shared-styles';
import CreditCard from '../styled/CreditCard';
import { alignItemsToCenter } from '../styled/mixins';

const LoadingCreditCard = ({ className }) => (
  <CreditCard className={className}>
    <Loading mode="block" />
  </CreditCard>
);
LoadingCreditCard.propTypes = { className: PropTypes.string.isRequired };

export default styled(LoadingCreditCard)`
  z-index: 2;
  background-color: ${akColorN20};
  ${alignItemsToCenter}
`;
