import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';
import styled from 'styled-components';

import MasterCardLogo from '../svg/mastercard.svgx';
import VisaCardLogo from '../svg/visa.svgx';
import AmexCardLogo from '../svg/amex.svgx';


export const CARD_TYPES = {
  MASTERCARD: <MasterCardLogo />,
  VISA: <VisaCardLogo />,
  AMEX: <AmexCardLogo />
};

const CardUl = styled.ul`
  margin: 0;
  padding: 0;
  list-style: none;
`;

const CardLi = styled.li`
  transition-property: opacity;
  transition-duration: 300ms;
  opacity: ${({ enabled }) => (enabled ? 1 : 0.3)}
`;

const CardBlock = ({ cardType }) => (
  <CardUl>
    {
      Object
        .keys(CARD_TYPES)
        .map((type) => {
          const enabled = !cardType || cardType === type;
          return (<CardLi
            key={type}
            enabled={enabled}
            className={cx({ 'cc-vendor--active': enabled })}
          >{CARD_TYPES[type]}
          </CardLi>);
        })
    }
  </CardUl>
);

CardBlock.propTypes = {
  cardType: PropTypes.string
};

export default CardBlock;