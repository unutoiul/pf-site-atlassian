import { I18n, Trans } from '@lingui/react';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import cx from 'classnames';
import styled from 'styled-components';
import { Field } from 'atlaskit-form-next';
import Textfield from '@atlaskit/textfield';
import { akColorN40, akColorN30, akColorB100, akColorN0, akColorR400 } from '@atlaskit/util-shared-styles';
import _range from 'lodash.range';

import { Media } from 'common/responsive/Matcher';
import { Analytics } from 'bux/components/Analytics';
import { AkSelectWithAnalytics } from 'bux/components/field/Select';

import { SubmitButton } from '../../../Button';
import Submitable from '../../../behaviour/state/submitable';

import CreditCard from '../styled/CreditCard';
import SupportedCards from './CardTypes';

const TextField = ({
  id, label, error, className, ...props
}) => (
  <Field 
    name={id}
    id={id}
    className={`${className} ${id}-field-group`} 
    label={label} 
    {...props}
  >
    {({ fieldProps }) => (
      <React.Fragment>
        <Textfield
          {...fieldProps}
          name={undefined}
          {...props}
          isRequired={undefined}
          data-invalid={!!error}
        />
        { !!error && <span id={`${id}-error`} className="ak-error">{error}</span> }
      </React.Fragment>
    )}
  </Field>
);
TextField.propTypes = {
  className: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
  error: PropTypes.string
};

const StyledTextField = styled(TextField)`
  .ak-hover {
    background-color: ${akColorN30};
    border-color: ${akColorN40};
  }

  .ak-focus {
    background-color: ${akColorN0};
    border-color: ${akColorB100};
    border-width: 2px;
    padding: 8px 6px;
  }

  input[data-invalid=true]{
    border-color: ${akColorR400};
    border-width: 2px;
    padding: 8px 6px;
  }
`;

const SecureTextField = props => (
  <StyledTextField {...props} readOnly />
);

export class AddCreditCardComponent extends Component {
  static propTypes = {
    className: PropTypes.string,
    onSave: PropTypes.func,
    onChange: PropTypes.func,
    value: PropTypes.object,
    errors: PropTypes.object,
    cardType: PropTypes.string
  };

  static defaultProps = {
    value: {},
    errors: {}
  };
  
  onItemChange(onChange, name, selectedItem, actionMeta, analyticsEvent) {
    analyticsEvent.fire();
    if (['select-option', 'set-value'].includes(actionMeta.action)) {
      onChange({ target: { value: selectedItem.value, name } });
    }
  }

  getMonths() {
    const months = _range(1, 13).map(month => ({
      value: month,
      label: month.toLocaleString(undefined, { minimumIntegerDigits: 2 })
    }));
    return months;
  }

  getYears() {
    const currentYear = new Date().getFullYear();
    const years = _range(0, 26)
      .map(offset => currentYear + offset)
      .map(year => ({ value: year, label: `${year}` }));
    return years;
  }

  render() {
    const {
      errors, value, onChange, className, onSave, cardType
    } = this.props;

    return (
      <CreditCard className={cx('editCreditCardDetails', className)}>
        <div className="payment-fields">
          <Analytics.UI as="creditCardNumberField">
            <SecureTextField
              id="creditcard-number"
              label={<Trans id="billing.credit-card.fields.card-number">Card number</Trans>}
              error={errors['creditcard-number']}
              autoFocus
            />
          </Analytics.UI>
          <Analytics.UI as="creditCardNameField">
            <StyledTextField
              id="creditcard-name"
              name="creditcard-name"
              label={<Trans id="billing.credit-card.fields.name-on-card">Name on card</Trans>}
              onChange={onChange}
              value={value['creditcard-name']}
              error={errors['creditcard-name']}
            />
          </Analytics.UI>
          <I18n>{({ i18n }) => (
            <div className="expiryAndSecurityCode">
              <div>
                <div className="expiry-fields">
                  <React.Fragment>
                    <Analytics.UI as="creditCardMonthField">
                      <Field 
                        name="creditcard-month"
                        label={i18n.t('billing.credit-card.fields.expiry-date')`Expiry date`}
                      >
                        {({ fieldProps }) => (
                          <AkSelectWithAnalytics
                            {...fieldProps}
                            id="creditcard-month"
                            instanceId="creditcard-month"
                            placeholder={i18n.t('billing.credit-card.fields.expiry-month')`Month`}
                            options={this.getMonths()}
                            onChange={(...other) => this.onItemChange(onChange, 'creditcard-month', ...other)}
                          />
                        )}
                      </Field>
                    </Analytics.UI>
                    <Analytics.UI as="creditCardYearField"> 
                      <Field
                        // Hack to avoid to force alignment with the month field
                        label={<React.Fragment>&nbsp;</React.Fragment>}
                        name="creditcard-year"
                      >
                        {({ fieldProps }) => (
                          <AkSelectWithAnalytics
                            {...fieldProps}
                            id="creditcard-year"
                            instanceId="creditcard-year"
                            placeholder={i18n.t('billing.credit-card.fields.expiry-year')`Year`}
                            options={this.getYears()}
                            onChange={(...other) => this.onItemChange(onChange, 'creditcard-year', ...other)}
                          />
                        )}
                      </Field>                  
                    </Analytics.UI>
                  </React.Fragment>
                </div>
                { (errors['creditcard-month'] || errors['creditcard-year']) &&
                <span className="ak-error">
                    { errors['creditcard-month'] || errors['creditcard-year'] }
                </span>
                }
              </div>
              <Analytics.UI as="creditCardSecurityField">
                <SecureTextField
                  id="creditcard-security"
                  label={i18n.t('billing.credit-card.fields.security-code')`Security code`}
                  error={errors['creditcard-security']}
                />
              </Analytics.UI>
            </div>
          )}</I18n>
        </div>
        
        <Media.Above mobile>
          <div className="logos">
            <SupportedCards cardType={cardType} />
          </div>
        </Media.Above>
        { onSave &&
        <Submitable
          name="save"
          component={SubmitButton}
          className="cc-submit-button"
          action={onSave}
          map={submitting => ({
            disabled: submitting,
            spinning: submitting
          })}
          spinningBefore
          preventDefault
        >
          <Trans id="billing.credit-card.button.save">Save</Trans>
        </Submitable>
        }
      </CreditCard>
    );
  }
}

export default styled(AddCreditCardComponent)`
  .ak-field-group {
    padding-top: 10px;
  }

  .creditcard-number-fieldgroup {
    margin-top: -10px;
  }

  .expiryAndSecurityCode {
    &,
    .expiry-fields {
      display: flex;
    }

    > *,
    .expiry-fields > * {
      margin-right: 8px;
    }
  }
  
  #creditcard-number, #creditcard-name {
    width: 240px;
  }

  .expiry-fields > * {
    min-width: 90px;
  }

  #creditcard-security {
    width: 80px;
  }

  .payment-fields {
    display: flex;
    flex-direction: column;
    justify-content: space-around;
  }

  /* Fix invisible iframe spacing */
  iframe[class^="gw-proxy-"] {
    display:block;
  }

  .logos {
    align-self: flex-start;
    margin-top: 12px;
    margin-left: auto;
    > * {
      display: block;
      margin-bottom: 8px;
    }
  }

  span.ak-error {
    color: ${akColorR400};
    font-size: 0.75em;
    font-weight: bold;
    display: block;
    position: absolute;
  }
  
  .cc-submit-button {
    position: absolute;
    right: 16px;
    bottom: 24px;
  }
`;
