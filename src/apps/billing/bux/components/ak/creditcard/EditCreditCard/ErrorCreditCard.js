import { Trans } from '@lingui/react';
import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';
import Button from '@atlaskit/button';
import WarningIcon from '@atlaskit/icon/glyph/warning';
import { akColorR400, akColorN0 } from '@atlaskit/util-shared-styles';

import { tablet } from 'common/responsive/breakpoints';

import { ExternalLinkTo } from 'bux/components/Link';

import CreditCard from '../styled/CreditCard';
import { alignItemsToCenter } from '../styled/mixins';


const ErrorHeader = styled.div`
  & {
    background-color: ${akColorR400};
    border-radius: 8px 8px 0 0;
    display: flex;
    height: 50px;
    align-items: baseline;
    align-content: flex-start;
    padding: 0 1em;
    align-self: stretch;
    justify-content: space-between;
  }
`;

const Title = styled.div`
  & {
    color: ${akColorN0};
    align-self: center;
  }
`;

const IconWrapper = styled.div`
  & {
    align-self: center;
  }
`;

const ErrorH4 = styled.h4`
  & {
    align-self: center;
  }
`;

const ErrorColophon = styled.p`
  & {
    margin-top; 2em;
    align-self: center;
    margin-bottom: 1em;
  }
`;

const ErrorActions = styled.div`
  & {
    margin-bottom: 3em;
  }
`;

const ErrorCreditCard = ({ className, onRetry }) => (
  <CreditCard className={className}>
    <ErrorHeader>
      <Title><Trans id="billing.credit-card.error.title">Connection problem</Trans></Title>
      <IconWrapper>
        <WarningIcon size="medium" primaryColor="white" label="Warning icon" />
      </IconWrapper>
    </ErrorHeader>

    <ErrorH4>
      <Trans id="billing.credit-card.error.subtitle">We can't load your billing details right now</Trans>
    </ErrorH4>
    <ErrorColophon>
      <Trans id="billing.credit-card.error.description">
        Check back soon or try&nbsp;
        <ExternalLinkTo.Support>
          contacting customer support
        </ExternalLinkTo.Support>
      </Trans>
    </ErrorColophon>

    <ErrorActions className="creditCardErrorActions">
      <Button name="retry" onClick={onRetry}>
        <Trans id="billing.credit-card.error.retry">Retry</Trans>
      </Button>
    </ErrorActions>
  </CreditCard>
);

ErrorCreditCard.propTypes = {
  className: PropTypes.string.isRequired,
  onRetry: PropTypes.func
};

const ccWidth = 404;
const ccHeight = 267;

export default styled(ErrorCreditCard)`
  z-index: 3;
  padding: 0;
  @media (min-width:${tablet}px) {
    width: ${ccWidth}px;
    height: ${ccHeight}px;
  }
  ${alignItemsToCenter}
  justify-content: space-between; 
`;
