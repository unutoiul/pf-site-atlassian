import PropTypes from 'prop-types';
import React, { Component } from 'react';
import debounce from 'lodash.debounce';
import { logException, logMessage } from 'bux/common/helpers/monitoring';
import identityFn from 'bux/common/helpers/identity';
import paymentGateway from '../internal/TNS';
import { StackedCreditCard } from '../styled/CreditCard';
import AddCreditCard from './AddCreditCard';
import ErrorCreditCard from './ErrorCreditCard';
import LoadingCreditCard from './LoadingCreditCard';
import { CARD_TYPES } from './CardTypes';

export const nameForInputTypeMap = {
  cardNumber: 'creditcard-number',
  cardName: 'creditcard-name',
  securityCode: 'creditcard-security',
  expiryMonth: 'creditcard-month',
  expiryYear: 'creditcard-year',
};
const getNameForInputType = inputType => nameForInputTypeMap[inputType];

export const errorMessages = i18n => ({
  cardNumber: {
    missing: i18n.t('billing.credit-card.errors.card-number.missing')`Please enter a valid credit card number`,
    invalid: i18n.t('billing.credit-card.errors.card-number.invalid')`Please enter a valid credit card number`,
    unsupported:
      i18n.t('billing.credit-card.errors.card-number.unsupported')`Only Visa, MasterCard and AMEX are supported`
  },
  securityCode: {
    missing: i18n.t('billing.credit-card.errors.security-code.missing')`Please enter a valid CCV`,
    invalid: i18n.t('billing.credit-card.errors.security-code.invalid')`Please enter a valid CCV`
  },
  expiryMonth: {
    missing: i18n.t('billing.credit-card.errors.expiry-month.missing')`Please enter a valid expiry date`,
    invalid: i18n.t('billing.credit-card.errors.expiry-month.invalid')`Please enter a valid expiry date`
  },
  expiryYear: {
    missing: i18n.t('billing.credit-card.errors.expiry-year.missing')`Please enter a valid expiry date`,
    invalid: i18n.t('billing.credit-card.errors.expiry-year.invalid')`Please enter a valid expiry date`
  },
  cardName: {
    missing: i18n.t('billing.credit-card.errors.name-on-card.missing')`Please enter a valid cardholder name`
  }
});

const getErrorMessage = (i18n, inputType, errorType) =>
  errorMessages(i18n)[inputType] && errorMessages(i18n)[inputType][errorType];

const soilField = (state, fieldName) => (state.includes(fieldName) ? [...state] : [...state, fieldName]);

export default class EditCreditCard extends Component {
  static propTypes = {
    className: PropTypes.string,
    saveCreditCard: PropTypes.func,
    onEditCancel: PropTypes.func,
    onReady: PropTypes.func,
    i18n: PropTypes.object.isRequired
  };

  static contextTypes = {
    sendAnalyticEvent: PropTypes.func
  };

  state = {
    configured: false,
    error: false,
    cardType: undefined,

    form: { 'creditcard-name': '' },
    fieldErrors: {},
    dirty: [],
    submitted: false
  };

  componentDidMount() {
    this.isComponentMounted = true;
    this.loadConfiguration();
  }

  componentWillUnmount() {
    // isMounted is a code smell, in a future - just cancel configuration proxy.
    this.isComponentMounted = false;
  }

  onFieldChange = ({ target: { name, value } }) => {
    this.setState({
      cardType: name === 'creditcard-number' ? undefined : this.state.cardType,
      // Update the field value in the state
      form: { ...this.state.form, [name]: value },
      // Reset the error for the field
      fieldErrors: { ...this.state.fieldErrors, [name]: undefined },
      // Mark the field as dirty
      dirty: soilField(this.state.dirty, name)
    }, () => this.debouncedValidate());
  };

  onSave = () =>
    this.getCreditCardData()
      .then(sessionData => this.props.saveCreditCard(sessionData))
      .then(this.props.onEditCancel, () => true);

  onIFrameFieldChange = (field) => {
    const name = field.substring(1); // Remove leading #
    this.onFieldChange({ target: { name, value: undefined } });
  };

  getCreditCardData = () => {
    if (this.state.error) {
      return Promise.reject(this.state.error);
    }

    if (!this.state.configured) {
      return Promise.reject({ reason: 'system_error', payload: 'Session has not been configured' });
    }

    return this.validate()
      .catch((error) => {
        this.setState({ submitted: true });
        this.context.sendAnalyticEvent('fail');
        return Promise.reject(error);
      })
      .then((sessionData) => {
        this.context.sendAnalyticEvent('success');
        this.setState({ submitted: true, fieldErrors: {} });
        return Object.assign(sessionData, {
          name: this.state.form['creditcard-name'],
          expiryYear: (`20${sessionData.expiryYear}`)
        });
      });
  };

  setErrorFields = (errorFields) => {
    const { i18n } = this.props;
    const fieldErrors =
      Object
        .keys(errorFields)
        .reduce((acc, field) => {
          const fieldName = getNameForInputType(field);
          const errorMessage = getErrorMessage(i18n, field, errorFields[field]);
          return { ...acc, [fieldName]: errorMessage };
        }, {});
    this.setState({
      fieldErrors
    });
  };

  fetchCurrentSession = () =>
    paymentGateway
      .updateCurrentSession()
      .then((response) => {
        const errors = {};

        if (!/\S/.test(this.state.form['creditcard-name'])) {
          errors.cardName = 'missing';
        }

        if (this.hasCreditCardDateExpired()) {
          errors.expiryMonth = 'invalid';
        }

        if (!response.securityCode) {
          errors.securityCode = 'missing';
        }

        if (Object.keys(errors).length > 0) {
          return Promise.reject({ reason: 'invalid_fields', payload: errors });
        }

        return response;
      })
      .catch((error) => {
        if (error.reason === 'invalid_fields') {
          const errors = { ...error.payload };

          if (!/\S/.test(this.state.form['creditcard-name'])) {
            errors.cardName = 'missing';
          }

          if (this.hasCreditCardDateExpired()) {
            errors.expiryMonth = 'invalid';
          }

          return Promise.reject({ reason: 'invalid_fields', payload: errors });
        }

        logMessage('CreditCardError', `${error.reason} - ${JSON.stringify(error.payload)}`);
        return Promise.reject(error);
      });

  hasCreditCardDateExpired = () => {
    // Validating expiry only if month and year are provided. If not, TNS will validate missing date.
    if (/\S/.test(this.state.form['creditcard-month']) && /\S/.test(this.state.form['creditcard-year'])) {
      const month = parseInt(this.state.form['creditcard-month'], 10);
      const year = parseInt(this.state.form['creditcard-year'], 10);
      const currentDate = new Date();
      if (currentDate.getFullYear() === year && currentDate.getMonth() + 1 > month) {
        return true;
      }
    }
    return false;
  };

  validate = () =>
    this
      .fetchCurrentSession()
      .then((valid) => {
        this.setState({
          cardType: valid.type,
          fieldErrors: {}
        });
        if (!(valid.type in CARD_TYPES)) {
          this.setErrorFields({ cardNumber: 'unsupported' }, true);
          // enforce error to be shown
          this.setState({
            dirty: soilField(this.state.dirty, 'creditcard-number')
          });
          return Promise.reject('Not supported CC type');
        }
        return valid;
      }, (error) => {
        if (error.reason === 'invalid_fields') {
          this.setErrorFields(error.payload);
        }
        return Promise.reject(error);
      });

  debouncedValidate = debounce(() => this.validate().catch(identityFn), 300, { leading: true });

  loadConfiguration = () => {
    const timeIn = Date.now();
    this.setState({ configured: false, error: false }, () =>
      paymentGateway.configurePaymentSession({
        fields: {
          card: {
            number: '#creditcard-number',
            securityCode: '#creditcard-security',
            expiryMonth: '[name="creditcard-month"]',
            expiryYear: '[name="creditcard-year"]'
          }
        },
        frameEmbeddingMitigation: ['x-frame-options'],
        onChange: this.onIFrameFieldChange
      })
        .then(() => {
          this.setState({ configured: true });
          this.context.sendAnalyticEvent('tnsLoadTime.ok', { time: Date.now() - timeIn });
          if (this.props.onReady) {
            this.props.onReady();
          }
        })
        .catch((error) => {
          this.setState({ configured: true, error });
          if (this.isComponentMounted) {
            logMessage(
              'PaymentGatewayError',
              `${error.reason} - ${JSON.stringify(error.payload)} - ${error.message}`
            );
            logException(error);
            this.context.sendAnalyticEvent('malfunction', error);
          } else {
            logMessage('unmounted-timeout');
            this.context.sendAnalyticEvent('unmounted-timeout', error);
          }
          this.context.sendAnalyticEvent('tnsLoadTime.error', { time: Date.now() - timeIn });
        }));
  };

  render() {
    const errorsToShowUser = this.state.submitted
      ? this.state.fieldErrors
      : Object.keys(this.state.fieldErrors)
        .filter(key => this.state.dirty.includes(key))
        .reduce((acc, key) => ({ ...acc, [key]: this.state.fieldErrors[key] }), {});

    return (
      <StackedCreditCard className={this.props.className}>
        {!this.state.error &&
        <AddCreditCard
          onSave={this.props.saveCreditCard && this.onSave}
          onChange={this.onFieldChange}
          value={this.state.form}
          errors={errorsToShowUser}
          cardType={this.state.cardType}
        />}
        {!this.state.configured && <LoadingCreditCard />}
        {this.state.error && <ErrorCreditCard onRetry={this.loadConfiguration} />}
      </StackedCreditCard>
    );
  }
}
