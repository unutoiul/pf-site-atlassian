import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';
import FocusLock from '../../../behaviour/focus/FocusLock';

const Background = styled.div`
  z-index: 0;
  position: fixed;
   
  left: 0;
  right: 0; 

  top: 0; 
  bottom:0;
  
 ${({ shadow }) => (
    shadow
      ? 'background-color: rgba(0, 0, 0, 0.6);transition: background-color 300ms, visibility 10ms;'
      : 'background-color: rgba(0, 0, 0, 0.0);transition: background-color 300ms, visibility 300ms;visibility:hidden;'
  )} 
`;

const CreditCardChrome = ({
  focused, children, className, onBackgroundClick 
}) => (
  <div className={className}>
    {focused ? <FocusLock>{children}</FocusLock> : children}
    <Background onClick={onBackgroundClick} shadow={focused} className="cc-shadow" />
  </div>
);

CreditCardChrome.propTypes = {
  focused: PropTypes.bool,
  className: PropTypes.string.isRequired,
  onBackgroundClick: PropTypes.func,
  children: PropTypes.node
};

const Chrome = styled(CreditCardChrome)`
  display: inline-flex;
      
  z-index: 1;
  position: relative;
    
  border-radius: 8px;    
`;

export default Chrome;
