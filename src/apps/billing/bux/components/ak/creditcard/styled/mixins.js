import { css } from 'styled-components';

export const alignItemsToCenter = css`
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;