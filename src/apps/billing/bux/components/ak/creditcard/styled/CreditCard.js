import styled from 'styled-components';
import { tablet } from 'common/responsive/breakpoints';
import Card from '../../../blocks/Card';

const ccWidth = 372;
const ccHeight = 235;
const ccPadding = 16;
const ccBorder = 1;


const CreditCard = styled(Card)`
  @media (min-width:${tablet}px) {
    width: ${ccWidth}px;
    height: ${ccHeight}px;
  }
  z-index: 1;
`;

export const SmallCreditCard = styled(Card)`
  @media (min-width:${tablet}px) {
    width: 330px;
    height: 184px;
  }
`;

const ccFullWidth = ccWidth + (ccPadding * 2) + (ccBorder * 2);
const ccFullHeight = ccHeight + (ccPadding * 2) + (ccBorder * 2);

export const StackedCreditCard = styled.div`
  min-height: ${ccFullHeight}px;
  
  @media (min-width:@tablet) {
    width: ${ccFullWidth}px;
  }

  & > * {
    position: absolute;
  }
`;

export default CreditCard;
