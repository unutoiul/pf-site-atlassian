import * as React from 'react';
import styled from 'styled-components';

import { layers as Aklayers } from '@atlaskit/theme';
import AkTooltip from '@atlaskit/tooltip';

import styles from './styles.less';

export const DialogComponent = styled.span`
  z-index: ${Aklayers.tooltip};
  pointer-events: none;
  position: fixed;
  background: white;
  border-radius: 4px;
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  box-sizing: content-box; /* do not set this to border-box or it will break the overflow handling */
  color: #333;
  max-height: 300px;
  max-width: 300px;
  padding: 8px 12px;
  font-size: 12px;
`;

export interface InlineDialogProps {
  content: React.ReactElement<any> | string;
}

export const InlineDialog: React.SFC<InlineDialogProps> = ({ children, content }) => (
  <span className={styles.makeInline}>
    <AkTooltip component={DialogComponent} content={content}>
      {children}
    </AkTooltip>
  </span>
);
