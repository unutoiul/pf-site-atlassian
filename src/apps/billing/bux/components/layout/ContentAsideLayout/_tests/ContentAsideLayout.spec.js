import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import ContentAsideLayout from '../ContentAsideLayout';

describe('ContentAsideLayout layout component: ', () => {
  it('should render 2 sides', () => {
    const wrapper = shallow(<ContentAsideLayout
      content={<div className="content">content</div>}
      aside={<div className="side">side</div>}
    />);
    expect(wrapper.find('.content')).to.be.present();
    expect(wrapper.find('.side')).to.be.present();
  });
});
