import PropTypes from 'prop-types';
import React from 'react';
import styles from './styles.less';

const ContentAsideLayout = ({ content, aside }) => (
  <div className={styles.grid}>
    <div className={styles.content}>
      {content}
    </div>
    <aside className={styles.aside}>
      {aside}
    </aside>
  </div>
);

ContentAsideLayout.propTypes = {
  content: PropTypes.node.isRequired,
  aside: PropTypes.node.isRequired,
};

export default ContentAsideLayout;
