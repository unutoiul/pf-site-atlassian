import PropTypes from 'prop-types';
import React from 'react';
import styles from './styles.less';

const PageLayout = ({ children }) => (
  <div className={styles.content}>
    {children}
  </div>
);

PageLayout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default PageLayout;