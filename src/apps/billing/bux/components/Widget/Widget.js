import { I18n } from '@lingui/react';
import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';
import ExtendedPropTypes from 'bux/core/extended-proptypes';
import Loading from '../Loading';
import WidgetError from '../error/WidgetError';

import styles from './Widget.less';

const payloadComponent = (errorMessage, name, icon, children, metadata) => {
  if (metadata.loading && !metadata.display) {
    return <Loading />;
  }
  if (metadata.error && !metadata.display) {
    const widgetName = errorMessage.name || name;

    return (
      <I18n>{({ i18n }) => (
        <WidgetError
          errorMessage={i18n.t('billing.widget.error.message')`Check back soon or contact customer support`}
          errorTitle={errorMessage.title || i18n.t('billing.widget.error.title')`We can't load your ${widgetName}`}
        />)}
      </I18n>
    );
  }

  return (
    <div className={cx('widget-content', styles.content)} key="content">
      {icon && <div className={styles.icon}>{icon}</div>}
      {children}
    </div>
  );
};

const Widget = ({
  errorMessage = {},
  name,
  icon,
  children,
  className,
  metadata = {}
}) => (
  <div className={cx(className, styles.subroot)}>
    <div className={cx('widget-title', styles.title)}>
      {name}
    </div>
    { payloadComponent(errorMessage, name, icon, children, metadata) }
  </div>
);

Widget.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  icon: PropTypes.node,
  name: PropTypes.string.isRequired,
  errorMessage: PropTypes.shape({
    name: PropTypes.string,
    title: PropTypes.string
  }),
  metadata: ExtendedPropTypes.metadata,
};

Widget.contextTypes = {
  sendAnalyticEvent: PropTypes.func
};

export default Widget;
