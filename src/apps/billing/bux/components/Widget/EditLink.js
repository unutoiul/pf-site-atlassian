import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';
import Button from '@atlaskit/button';
import CheckIcon from '@atlaskit/icon/glyph/check';
import EditFilledIcon from '@atlaskit/icon/glyph/edit-filled';
import { InternalLink } from '../Link';
import styles from './EditLink.less';

class EditLink extends React.PureComponent {
  static propTypes = {
    to: PropTypes.any,
    isEditing: PropTypes.bool,
    onClick: PropTypes.func,
    name: PropTypes.string
  };

  static defaultProps = {
    to: ''
  };

  handleClick = () => {
    if (this.props.onClick) {
      this.props.onClick();
    }
  };

  render() {
    const { to, isEditing, name } = this.props;
    return (
      <InternalLink
        className={cx('widgetEditLink', styles.editLink)}
        onClick={this.handleClick}
        to={to}
        name={name}
      >
        <Button
          iconAfter={
            isEditing
              ? <CheckIcon label="Done" size="medium" />
              : <EditFilledIcon label="Edit" size="medium" />
          }
        />
      </InternalLink>
    );
  }
}

export default EditLink;
