import React from 'react';
import { StaticRouter } from 'react-router-dom';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';
import CheckIcon from '@atlaskit/icon/glyph/check';
import EditFilledIcon from '@atlaskit/icon/glyph/edit-filled';
import { InternalLink } from '../../Link';
import getAnalytic from '../../log/helpers/getAnalytic';
import EditLink from '../EditLink';

describe('Widget`s EditLink component', () => {
  const mountEditLink = props => (
    mount(
      <StaticRouter context={{}}>
        <EditLink {...props} />
      </StaticRouter>
      , getAnalytic().context
    )
  );

  it('should render component', () => {
    const wrapper = mountEditLink();
    expect(wrapper).to.be.present();
  });

  it('should render Link', () => {
    const wrapper = mountEditLink({ to: 'href' });
    expect(wrapper.find(InternalLink)).to.be.present();
    expect(wrapper.find(InternalLink)).to.have.prop('to', 'href');
  });

  it('should trigger edit events', () => {
    const onClick = sinon.stub();
    const link = mountEditLink({ onClick }).find(InternalLink);

    link.simulate('click');
    expect(onClick).to.be.called();
  });

  it('should show EditFilledIcon by default', () => {
    const wrapper = mountEditLink();
    expect(wrapper).to.have.descendants(EditFilledIcon);
  });

  it('should show CheckIcon by default', () => {
    const wrapper = mountEditLink({ isEditing: true });
    expect(wrapper).to.have.descendants(CheckIcon);
  });
});
