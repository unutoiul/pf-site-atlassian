import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import WidgetError from '../../error/WidgetError';
import Widget from '../Widget';

describe('Widget component', () => {
  it('should render name', () => {
    const wrapper = shallow(<Widget name="supernova name" />);
    expect(wrapper).to.contain.text('supernova name');
  });

  it('should render children', () => {
    const wrapper = shallow(<Widget name="name">
      <div>supernova content</div>
                            </Widget>);
    expect(wrapper).to.contain(<div>supernova content</div>);
  });

  it('should render children', () => {
    const metadata = {};
    const wrapper = shallow(<Widget name="ErrorName" metadata={metadata}>
      <div>supernova content</div>
                            </Widget>);
    expect(wrapper).to.contain(<div>supernova content</div>);
  });

  it('should render error', () => {
    const metadata = {
      error: {},
      display: false
    };
    const wrapper = shallow(<Widget name="ErrorName" metadata={metadata}>
      <div>supernova content</div>
                            </Widget>);

    expect(wrapper).to.not.contains(<div>supernova content</div>);
    const error = wrapper.find(WidgetError);
    expect(error).to.be.present();
    expect(error.props().errorTitle).to.equal('We can\'t load your ErrorName');
  });

  it('should render custom error', () => {
    const metadata = {
      error: {},
      display: false
    };
    const errorMessage = {
      name: 'OtherName'
    };
    const wrapper = shallow(<Widget name="ErrorName" metadata={metadata} errorMessage={errorMessage}>
      <div>supernova content</div>
                            </Widget>);

    expect(wrapper).to.not.contains(<div>supernova content</div>);
    const error = wrapper.find(WidgetError);
    expect(error).to.be.present();
    expect(error.props().errorTitle).to.equal('We can\'t load your OtherName');
  });

  it('should render error with custom title', () => {
    const metadata = {
      error: {},
      display: false
    };
    const errorMessage = {
      title: 'other caption',
      name: 'OtherName'
    };
    const wrapper = shallow(<Widget name="ErrorName" metadata={metadata} errorMessage={errorMessage}>
      <div>supernova content</div>
                            </Widget>);

    expect(wrapper).to.not.contains(<div>supernova content</div>);
    const error = wrapper.find(WidgetError);
    expect(error).to.be.present();
    expect(error.props().errorTitle).to.equal('other caption');
  });

  it('should not render error', () => {
    const metadata = {
      error: {},
      display: true
    };
    const wrapper = shallow(<Widget name="ErrorName" metadata={metadata}>
      <div>supernova content</div>
                            </Widget>);

    expect(wrapper).to.contain(<div>supernova content</div>);
    const widget = wrapper.find(WidgetError);
    expect(widget).to.not.be.present();
  });

  it('should render classname', () => {
    const container = shallow(<Widget name="name" className="my class name" />).find('.my.class.name');

    expect(container).to.be.present();
  });
});
