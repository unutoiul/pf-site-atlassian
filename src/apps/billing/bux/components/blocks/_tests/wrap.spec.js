import React from 'react';
import PropTypes from 'prop-types';
import { mount } from 'enzyme';
import { expect } from 'chai';
import wrap from '../wrap';

describe('wrap HOC', () => {
  it('should wrap', () => {
    const A = ({ children }) => <div>{children}</div>;
    A.propTypes = {
      children: PropTypes.node
    };

    const B = ({ number }) => <span>data {number}</span>;
    B.propTypes = {
      number: PropTypes.number
    };
    const Result = wrap(B).with(A);
    expect(mount(<Result number={42} />)).to.contain.html('<div><span>data 42</span></div>');
    expect(Result.propTypes).to.be.deep.equal(B.propTypes);
  });
});
