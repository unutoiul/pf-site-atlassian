import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import styles from './Island.less';

const Island = ({ children, className }) => (
  <div className={cx(className, styles.island)}>{children}</div>
);

Island.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string
};

export default Island;