import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import styles from './Card.less';

const Card = ({ children, className }) => (
  <div className={cx(className, styles.card)}>{children}</div>
);

Card.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string
};

export default Card;