import React from 'react';

const wrap = InnerComponent => ({
  with(OuterComponent) {
    const Wrapper = props => <OuterComponent><InnerComponent {...props} /></OuterComponent>;
    Wrapper.propTypes = InnerComponent.propTypes;
    return Wrapper;
  }
});

export default wrap;