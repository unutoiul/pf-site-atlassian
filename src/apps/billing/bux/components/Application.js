import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';

import { 
  getFeatureFlags, 
  getMultivariateFeatureFlags, 
  getOrganizationId 
} from 'bux/core/state/meta/selectors';
import { resetStateAction } from 'bux/core/actions/reset-state';
import { getUserMetadata } from 'bux/core/state/meta/actions';
import { startCSATSurvey } from 'bux/core/state/csat/actions';
import { withUrlTransformerProvided } from 'bux/components/Page/with/urlTransformer';

import FeatureFlagProvider from './FeatureFlags/FeatureFlagProvider';

class Application extends Component {
  static propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    orgId: PropTypes.string,
    resetState: PropTypes.func,
    featureFlags: PropTypes.object,
    multivariateFeatureFlags: PropTypes.object,
    getUserMetadata: PropTypes.func,
    startCSAT: PropTypes.func,
    intl: PropTypes.any,
  };

  componentDidMount() {
    this.props.getUserMetadata();
    this.props.startCSAT();
  }

  componentDidUpdate(nextProps) {
    if (
      nextProps.orgId && // could be flake, but always present-or-not
      nextProps.orgId !== this.props.orgId
    ) {
      this.props.resetState();
    }
  }

  render() {
    const { children, className } = this.props;
    return (
      <FeatureFlagProvider 
        featureFlags={this.props.featureFlags} 
        multivariateFeatureFlags={this.props.multivariateFeatureFlags}
      >
        <div className={className}>
          {children}
        </div>
      </FeatureFlagProvider>
    );
  }
}

const mapStateToProps = state => ({
  orgId: getOrganizationId(state) || '',
  featureFlags: getFeatureFlags(state),
  multivariateFeatureFlags: getMultivariateFeatureFlags(state)
});

const mapDispatchToProps = dispatch => ({
  resetState: () => dispatch(resetStateAction()),
  getUserMetadata: () => dispatch(getUserMetadata()),
  startCSAT: () => dispatch(startCSATSurvey())
});

export const UnconnectApplication = Application;

export default connect(
  mapStateToProps,
  mapDispatchToProps, null, { pure: false }
)(withUrlTransformerProvided(Application));
