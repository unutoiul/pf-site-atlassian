import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';
import { Trans } from '@lingui/react';
import { isTrial } from 'bux/core/selectors/common/entitlements';
import { Analytics } from 'bux/components/Analytics';
import Button from '../../Button';
import LogScope from '../../log/Scope/index';
import SubscriptionPropTypes from '../PropTypes';
import SubscriptionItem from './components/SubscriptionItem';
import styles from './style.less';

const ActiveSection = ({
  section, currency, isAnnual, isOrganization, cancelSubscription
}) => (
  <div className={cx(styles.subscriptionsList, 'section-active')}>
    <h2 className={styles.sectionLabel}>
      <Trans id="billing.manage-subscriptions.active-products.header">Active subscriptions</Trans>
    </h2>
    <ul className={styles.productList}>
      {
        section.items.map((item, sectionIndex) => (
          <SubscriptionItem
            key={sectionIndex}
            data={item}
            isTrial={isTrial(item.entitlement)}
            isAnnual={isAnnual}
            currency={currency}
          />
        ))
      }
    </ul>
    { !isOrganization &&
      <Analytics.UI as="cancelSubscriptionLink" attributes={{ linkContainer: 'activeSubscriptions' }}> 
        <LogScope className={styles.cancelMySite} prefix="cancel.subscription">
          <Button onClick={cancelSubscription} className={cx('cancel-subscription-button')} subtle>
            <Trans id="billing.manage-subscriptions.active-products.cancel-subscription">
              Cancel subscription
            </Trans>
          </Button>
        </LogScope>
      </Analytics.UI>
    }
  </div>
);

ActiveSection.propTypes = {
  section: SubscriptionPropTypes.subscription.isRequired,
  currency: PropTypes.string.isRequired,
  cancelSubscription: PropTypes.func.isRequired,
  isAnnual: PropTypes.bool,
  isOrganization: PropTypes.bool
};

export default ActiveSection;
