import { createContext } from 'react';

import { SubscriptionType } from 'bux/components/ManageSubscriptions/Types';

const productContext = createContext({} as SubscriptionType);

export const ProductContextProvider = productContext.Provider;
export const ProductContextConsumer = productContext.Consumer;
