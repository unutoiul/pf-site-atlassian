import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { mount, shallow } from 'enzyme';
import DropdownMenu, { DropdownItemGroup } from '@atlaskit/dropdown-menu';
import getAnalytic from 'bux/components/log/helpers/getAnalytic';
import { StatelessActionButton, DropdownItemWithAnalytics } from '../ActionButton';

describe('SubscriptionItem.ActionButton component', () => {
  it('Display main button', () => {
    const wrapper = mount(<StatelessActionButton
      actions={[
        {
          label: 'someAction',
        },
        {
          label: 'mainAction',
          primaryAction: true
        },
        {
          label: 'otherAction',
          primaryAction: true
        }
      ]}
    />);
    expect(wrapper.find('.mainActionButton').hostNodes()).to.contain.text('mainAction');
  });

  it('Display additional buttons', () => {
    const action = sinon.spy();
    const wrapper = shallow(<StatelessActionButton
      actions={[
        {
          label: 'action1',
          primaryAction: true,
          action
        },
        {
          label: 'secondary1',
          action
        },
        {
          label: 'action2',
          primaryAction: true,
          action
        },
        {
          label: 'secondary2'
        }
      ]}
    />).find(DropdownMenu);

    const items = wrapper.find(DropdownItemGroup).children();
    expect(items).to.have.length(3);
    expect(items.at(0).find(DropdownItemWithAnalytics).children().text()).to.be.equal('secondary1');
    expect(items.at(0).find(DropdownItemWithAnalytics).props().onClick).not.to.be.undefined();
    expect(items.at(1).find(DropdownItemWithAnalytics).children().text()).to.be.equal('action2');
    expect(items.at(1).find(DropdownItemWithAnalytics).props().onClick).not.to.be.undefined();
    expect(items.at(2).find(DropdownItemWithAnalytics).children().text()).to.be.equal('secondary2');
    expect(items.at(2).find(DropdownItemWithAnalytics).props().onClick).not.to.be.undefined();

    items.at(0).find(DropdownItemWithAnalytics).props().onClick();
    expect(action).to.be.called();
  });

  it('log actions', () => {
    const action = sinon.spy();
    const { context, sendAnalyticEvent } = getAnalytic();
    const wrapper = mount(<StatelessActionButton
      actions={[
        {
          label: 'action1',
          primaryAction: true,
          action
        },
        {
          label: 'secondary1',
          action
        }
      ]}
    />, context);
    wrapper.find('.mainActionButton').hostNodes().simulate('click');
    expect(action).to.be.called();
    expect(sendAnalyticEvent).to.be.calledWith('action1.click');
  });
});
