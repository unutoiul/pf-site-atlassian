import PropTypes from 'prop-types';
import React from 'react';
import { Trans } from '@lingui/react';
import styled from 'styled-components';

const BillingDateLabelStyled = styled.div`
  &:after {
    content: "*";
 }`;

const BillingDateLabel = ({
  isAnnual,
  isFree
}) => (
  <span className="label billingDateLabel">
    {isFree && <br />}

    {isAnnual && !isFree &&
      <BillingDateLabelStyled>
        <Trans id="billing.manage-subscriptions.billing-date.annual-label">Subscription end date</Trans>
      </BillingDateLabelStyled>
    }

    {!isAnnual && !isFree &&
     <Trans id="billing.manage-subscriptions.billing-date.monthly-label">Billed on</Trans>
    }
  </span>
);

BillingDateLabel.propTypes = {
  isFree: PropTypes.bool,
  isAnnual: PropTypes.bool,
};

export default BillingDateLabel;
