import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import Message from '../Message';

const renderWidget = options => <Message {...options} />;

describe('Message component: ', () => {
  it('should render confirm message to all products', () => {
    const component = mount(renderWidget({})).find('p');
    expect(component).to.contain.text("We're sad to see you go.");
  });

  it('should render confirm message to last product', () => {
    const component = mount(renderWidget({ entitlement: {} })).find('p');
    expect(component.first()).to.contain.text('Oops, unfortunately you only have one product');
    expect(component.last()).to.contain.text('To do so you will be redirected to');
  });
});
