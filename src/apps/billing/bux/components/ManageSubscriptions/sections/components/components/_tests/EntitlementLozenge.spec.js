import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Lozenge from '@atlaskit/lozenge';
import EntitlementLozenge from '../EntitlementLozenge';

describe('EntitlementLozenge component', () => {
  it('should render no lozenge when neither edition nor trial is present', () => {
    const wrapper = shallow(<EntitlementLozenge />);
    const anyLozenge = wrapper.find(Lozenge);

    expect(anyLozenge).not.to.be.present();
  });

  it('should render the edition lozenge when edition is present', () => {
    const wrapper = shallow(<EntitlementLozenge edition="someEdition" />);
    const editionLozenge = wrapper.find('.editionLozenge');

    expect(editionLozenge).to.be.present();
    expect(editionLozenge.children()).to.contain.text('someEdition');
  });

  it('should render the trial lozenge when trial is present', () => {
    const wrapper = shallow(<EntitlementLozenge isTrial />);
    const trialLozenge = wrapper.find('.trialLozenge');

    expect(trialLozenge).to.be.present();
    expect(trialLozenge.prop('appearance')).to.be.equal('success');
  });

  it('should render downgrading lozenge', () => {
    const wrapper = shallow(<EntitlementLozenge
      isDowngrading
      futureEdition="newEdition"
      billingDate={`${Date.now()}`}
    />);
    expect(wrapper).to.contain.text('downgrading to newEdition');
  });

  it('should render downgrading lozenge with a full date', () => {
    const wrapper = shallow(<EntitlementLozenge
      isDowngrading
      isAnnual
      futureEdition="newEdition"
      billingDate={`${Date.now()}`}
    />);
    expect(wrapper).to.contain.text('downgrading to newEdition');
  });

  it('should render the unsubscribed lozenge when flag is set', () => {
    const wrapper = shallow(<EntitlementLozenge isUnsubscribed />);
    const trialLozenge = wrapper.find('.unsubscribedLozenge');

    expect(trialLozenge).to.be.present();
    expect(trialLozenge.prop('appearance')).to.be.equal('removed');
  });

  it('should render the isSuspended lozenge when flag is set', () => {
    const wrapper = shallow(<EntitlementLozenge isSuspended />);
    const trialLozenge = wrapper.find('.suspendedLozenge');

    expect(trialLozenge).to.be.present();
    expect(trialLozenge.prop('appearance')).to.be.equal('removed');
  });
});