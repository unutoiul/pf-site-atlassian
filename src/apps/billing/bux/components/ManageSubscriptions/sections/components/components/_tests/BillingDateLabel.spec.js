import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import BillingDateLabel from '../BillingDateLabel';

const DEFAULT_PROPS = {
  isFree: true,
  isAnnual: false,
};
const mountComponent = options => <BillingDateLabel {...DEFAULT_PROPS} {...options} />;

describe('BillingDateLabel component', () => {
  it('should not display billing date label if free', () => {
    const wrapper = shallow(mountComponent());
    expect(wrapper.find('.billingDateLabel')).to.be.present();
    expect(wrapper.find('.billingDateLabel')).to.contain.text('');
  });

  it('should display billing date label if not free and if annual', () => {
    const wrapper = shallow(mountComponent({ isFree: false, isAnnual: true }));
    expect(wrapper.find('.billingDateLabel')).to.be.present();
    expect(wrapper.contains('Subscription end date')).to.equal(true);
  });

  it('should display billing date label if not free and if not annual', () => {
    const wrapper = shallow(mountComponent({ isFree: false, isAnnual: false }));
    expect(wrapper.find('.billingDateLabel')).to.be.present();
    expect(wrapper.find('.billingDateLabel')).to.contain.text('Billed on');
  });
});
