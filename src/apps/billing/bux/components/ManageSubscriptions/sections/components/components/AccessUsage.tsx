import { Trans } from '@lingui/react';
import * as React from 'react';

import AkInlineMessage from '@atlaskit/inline-message';

import { ExternalLinkTo } from 'bux/components/Link';
import { UsageLine } from 'bux/components/ManageSubscriptions/sections/components/components/Usage';
import { ProductUsageType } from 'bux/core/state/bill-estimate/types';

export const AccessUsage: React.SFC<{ usage: ProductUsageType }> = ({ usage }) => (
  <UsageLine {...usage}>
    <span data-test="usage-inline-message">
      <AkInlineMessage
        type="info"
      >
        <p>
          <Trans id="billing.manage-subscriptions.access-usage.info">
            We exclude certain accounts to reduce <br />
            your bill.
          </Trans>
          {' '}
          <ExternalLinkTo.AccessPricingBilling>
            <Trans id="billing.manage-subscriptions.access-usage.learn-more">
              See how your bill is calculated
            </Trans>
          </ExternalLinkTo.AccessPricingBilling>
        </p>
      </AkInlineMessage>
    </span>
  </UsageLine>
);

export default AccessUsage;
