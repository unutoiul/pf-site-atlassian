import { Trans } from '@lingui/react';
import cx from 'classnames';
import * as React from 'react';
import { connect } from 'react-redux';

import AkInlineMessage from '@atlaskit/inline-message';

import { ExternalLinkTo } from 'bux/components/Link';
import Usage, { TieredUsage, UsageUnitLabel } from 'bux/components/Usage';
import { ProductUsageType } from 'bux/core/state/bill-estimate/types';
import { getTenantInfo } from 'bux/core/state/meta/selectors';

import { BUXState } from 'bux/core/state/types';

import { ProductContextConsumer } from './productContext';

import styles from './style.less';

export const shouldDisplayTierBreach = (current: number, max: number) => {
  if (!current || current < max - 25) {
    return false;
  }

  return current >= max * 0.9;
};

const TierBreachStatus: React.SFC<{
  tenantName: string,
  productTitle: string,
  limit: number,
  unitCode: string,
}> = ({ tenantName, productTitle, limit, unitCode }) => {
  if (limit < 0) {
    // Units are extracted to variable and eventually will be refactored to a component that supports i18n
    const unitsExceeded = <Usage unitsLimit={-limit} unitCode={unitCode} />;

    return (
      <Trans
        id="billing.manage-subscriptions.usage.tier-over-limit"
        description="Example value of {unitsExceeded}: '4 Users' or '1 Agent'"
      >
        {tenantName} {productTitle} has been exceeded by {unitsExceeded}.
      </Trans>
    );
  }

  const unitsLeft = <Usage unitsLimit={limit} unitCode={unitCode} />;

  if (limit === 0) {

    return (
      <Trans
        id="billing.manage-subscriptions.usage.tier-on-limit"
        description="Number of units in {unitsLeft} will be always 0 i.e. '0 Users' or '0 Agents'"
      >
          {tenantName} {productTitle} has {unitsLeft} left.
      </Trans>
    );
  }

  return (
    <Trans
      id="billing.manage-subscriptions.usage.tier-limit-close"
      description="Example value of {unitsLeft}: '4 Users' or '1 Agent'"
    >
      {tenantName} {productTitle} has only {unitsLeft} left.
    </Trans>
  );
};

export const UnconnectedTierBreachNotification: React.SFC<{
  limit: number,
  unitCode: string,
  tenantInfo: any,
}> = ({ limit, unitCode, tenantInfo }) => {
  return (
    <ProductContextConsumer>
      {({ title }) => (
        <span data-test="tier-breach-inline-message">
        <AkInlineMessage
          type={limit >= 0 ? 'warning' : 'error'}
        >
          <p>
            <TierBreachStatus tenantName={tenantInfo.name} productTitle={title} limit={limit} unitCode={unitCode} />
          </p>
          <p>
            <ExternalLinkTo.PurchasingLicense>
             <Trans id="billing.manage-subscriptions.usage.contact-us">
               Contact us to upgrade tier
             </Trans>
            </ExternalLinkTo.PurchasingLicense>
          </p>
        </AkInlineMessage>
      </span>
      )}
    </ProductContextConsumer>
  );
};

export const TierBreachNotification = connect(
  (state: BUXState) => ({
    tenantInfo: getTenantInfo(state),
  }),
)(UnconnectedTierBreachNotification);

export const UsageLine: React.SFC<ProductUsageType> = ({ units, currentUnits, unitCode, unlimited, tiered, children }) => (
  <div className={cx(styles.informationLine, 'product-usage')}>
    <span className="label"><UsageUnitLabel unitCode={unitCode} /></span>
    <span className={cx('payload', styles.usageLine)}>
      {
        tiered
          ? (
            <React.Fragment>
              <TieredUsage unitsLimit={units} currentUnits={currentUnits} unitCode={unitCode} unlimited={unlimited} />
              {
                shouldDisplayTierBreach(currentUnits, units) &&
                <TierBreachNotification
                  limit={units - currentUnits}
                  unitCode={unitCode}
                />
              }
            </React.Fragment>
          )
          : (<Usage unitsLimit={units} unitCode={unitCode} unlimited={unlimited} />)
      }
      {children}
    </span>
  </div>
);

const SubscriptionUsage: React.SFC<{
  usage: ProductUsageType[],
}> = ({ usage = [] }) => (
  <div className="group-usage">{
    usage.map((usageDetail, index) => (
      <UsageLine
        key={index}
        {...usageDetail}
      />
    ))
  }
  </div>
);

export default SubscriptionUsage;
