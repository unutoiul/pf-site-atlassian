import React from 'react';
import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import InlineMessage from '@atlaskit/inline-message';
import Usage, { TieredUsage } from 'bux/components/Usage';
import { ProductContextProvider } from '../productContext';
import SubscriptionUsage, {
  shouldDisplayTierBreach,
  TierBreachNotification,
  UnconnectedTierBreachNotification,
  UsageLine,
} from '../Usage';

describe('SubscriptionItem.UsersUsage component', () => {
  it('should create usage elements', () => {
    const usage = [1, 2, 3, 4];
    let wrapper;
    wrapper = shallow(<SubscriptionUsage usage={usage} />);
    expect(wrapper.find(UsageLine)).to.have.length(4);

    usage.shift();
    wrapper = shallow(<SubscriptionUsage usage={usage} />);
    expect(wrapper.find(UsageLine)).to.have.length(3);

    usage.shift();
    wrapper = shallow(<SubscriptionUsage usage={usage} />);
    expect(wrapper.find(UsageLine)).to.have.length(2);
  });

  describe('UsageLine component', () => {
    it('should indicate usage', () => {
      const wrapper = shallow(<UsageLine units={100500} unitCode="tons" />);
      expect(wrapper.find(Usage).dive()).to.contain.text('100500 tons');
    });

    it('should remove plurals if 1', () => {
      const wrapper = shallow(<UsageLine units={1} unitCode="tons" />);
      expect(wrapper.find(Usage).dive()).to.contain.text('1 ton');
    });

    it('should remove underscores', () => {
      const wrapper = shallow(<UsageLine units={12} unitCode="rotten_bananas" />);
      expect(wrapper.find(Usage).dive()).to.contain.text('12 rotten bananas');
    });

    it('should display unlimited', () => {
      const wrapper = shallow(<UsageLine units={12} unitCode="users" unlimited />);
      expect(wrapper.find(Usage).dive().dive()).to.contain.text('Unlimited');
    });

    it('should not display breach without tier', () => {
      const wrapper = shallow(<UsageLine units={10} currentUnits={9} unitCode="tons" />);
      expect(wrapper.find(TierBreachNotification)).to.have.length(0);
      expect(wrapper.find(Usage).dive()).to.contain.text('10 tons');
    });

    it('should display breach', () => {
      const wrapper = shallow(<UsageLine units={10} tiered currentUnits={9} unitCode="tons" />);
      expect(wrapper.find(TieredUsage).dive()).to.contain.text('9 of 10 tons');
      expect(wrapper.find(TierBreachNotification)).to.have.length(1);
    });
  });

  describe('TierBreachNotification', () => {
    it('before breach', () => {
      const wrapper = mount(<ProductContextProvider value={{ title: 'BUX' }}>
          <UnconnectedTierBreachNotification limit={1} unitCode="devs" tenantInfo={{ name: 'BUF' }} />
        </ProductContextProvider>);
      const Info = wrapper.find(InlineMessage);
      expect(Info).to.have.prop('type', 'warning');
      expect(mount(Info.prop('children')[0])).to.contain.text('BUF BUX has only 1 dev left');
    });

    it('on breach', () => {
      const wrapper = mount(<ProductContextProvider value={{ title: 'BUX' }}>
        <UnconnectedTierBreachNotification limit={0} unitCode="devs" tenantInfo={{ name: 'BUF' }} />
      </ProductContextProvider>);
      const Info = wrapper.find(InlineMessage);
      expect(Info).to.have.prop('type', 'warning');
      expect(mount(Info.prop('children')[0])).to.contain.text('BUF BUX has 0 devs left');
    });

    it('after breach', () => {
      const wrapper = mount(<ProductContextProvider value={{ title: 'BUX' }}>
          <UnconnectedTierBreachNotification limit={-1} unitCode="devs" tenantInfo={{ name: 'BUF' }} />
        </ProductContextProvider>);
      const Info = wrapper.find(InlineMessage);
      expect(Info).to.have.prop('type', 'error');
      expect(mount(Info.prop('children')[0])).to.contain.text('BUF BUX has been exceeded by 1 dev');
    });
  });

  describe('Tier breach limits', () => {
    it('10% for lower tiers', () => {
      expect(shouldDisplayTierBreach(0, 0)).to.be.false();
      expect(shouldDisplayTierBreach(0, 10)).to.be.false();
      expect(shouldDisplayTierBreach(5, 10)).to.be.false();
      expect(shouldDisplayTierBreach(9, 10)).to.be.true();
      expect(shouldDisplayTierBreach(10, 10)).to.be.true();
    });

    it('no more that 25 users', () => {
      expect(shouldDisplayTierBreach(90, 100)).to.be.true();
      expect(shouldDisplayTierBreach(900, 1000)).to.be.false();
      expect(shouldDisplayTierBreach(975, 1000)).to.be.true();
    });
  });
});
