import PropTypes from 'prop-types';
import React from 'react';
import ModalDialog from '@atlaskit/modal-dialog';
import { I18n } from '@lingui/react';

import { Analytics } from 'bux/components/Analytics';

import LogScope from '../../../../log/Scope/Scope';
import Footer from './Footer';
import Message from './Message';

const withConfirmFooter = ({ onConfirm, prefix }) => props => (
  <LogScope prefix={prefix}>
    <Footer {...props} onConfirm={onConfirm} />
  </LogScope>
);

const ConfirmCancel = ({ closeAction, confirmAction, entitlement }, { logPrefix }) => {
  const prefix = logPrefix.join('.');
  return (
    <Analytics.Screen name="cancelSubscriptionConfirmationScreen">
      <I18n>{({ i18n }) => (
        <ModalDialog
          heading={i18n.t('billing.manage-subscriptions.confirm-cancel.header')`Important`}
          width="small"
          appearance="warning"
          onClose={closeAction}
          footer={withConfirmFooter({ onConfirm: confirmAction, prefix })}
          autoFocus
        >
          <LogScope prefix={prefix}>
            <Message entitlement={entitlement} />
          </LogScope>
        </ModalDialog>
      )}</I18n>
    </Analytics.Screen>
  );
};

ConfirmCancel.propTypes = {
  closeAction: PropTypes.func.isRequired,
  confirmAction: PropTypes.func.isRequired,
  entitlement: PropTypes.object
};
ConfirmCancel.contextTypes = {
  sendAnalyticEvent: PropTypes.func,
  logPrefix: PropTypes.arrayOf(PropTypes.string)
};

export default ConfirmCancel;
