import PropTypes from 'prop-types';
import React from 'react';
import { Trans } from '@lingui/react';
import config from 'bux/common/config';

const macDomain = config.macDomain;

const MessageLastProduct = () => (
  <div>
    <p>
      <Trans id="billing.manage-subscriptions.confirm-cancel.last-product">
        Oops, unfortunately you only have one product remaining and you have chosen to delete it.
        This also means we need to cancel your subscription.
      </Trans>
    </p>
    <p>
      <Trans id="billing.manage-subscriptions.confirm-cancel.redirect-info">
        To do so you will be redirected to {macDomain}
      </Trans>
    </p>
  </div>
);

const MessageAllProducts = () => (<p>
  <Trans id="billing.manage-subscriptions.confirm-cancel.all-products">
    We're sad to see you go. As you have chosen to cancel your subscription you will be redirected to {macDomain}.
  </Trans>
</p>);

const Message = ({ entitlement }) => (entitlement ? <MessageLastProduct /> : <MessageAllProducts />);
Message.propTypes = {
  entitlement: PropTypes.object
};
export default Message;
