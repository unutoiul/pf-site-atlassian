import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import ConfirmCancel from '../ConfirmCancel';
import Message from '../Message';

const renderWidget = options => <ConfirmCancel {...options} />;

const DEFAULT_CONTEXT = { context: { logPrefix: ['test'] } };

describe('ConfirmCancel component: ', () => {
  it('should render modal', () => {
    const component = shallow(renderWidget({ closeAction: () => {}, confirmAction: () => {} }), DEFAULT_CONTEXT);
    expect(component.find(Message)).to.be.present();
  });
});
