import PropTypes from 'prop-types';
import React from 'react';
import { Trans } from '@lingui/react';
import Money from '../../../../Money';
import styles from '../style.less';

const formatCost = (amountToDisplay, currency, displayAs) => {
  switch (displayAs) {
    case 'undefined':
      return '–';
    case 'free':
      return <Trans id="billing.manage-subscriptions.billing-info.free">Free</Trans>;
    default:
      return <Money amount={amountToDisplay} currency={currency} showFreeIfZero />;
  }
};

const BillingInfo = ({ amountToDisplay, currency, displayAs }) => (
  <div>
    <span className="label">
      <Trans id="billing.manage-subscriptions.billing-info.label">Price estimate</Trans>
    </span>
    <div className={styles.informationLine}>
      <span className="price payload">
        {formatCost(amountToDisplay, currency, displayAs)}
      </span>
    </div>
  </div>
);

BillingInfo.propTypes = {
  amountToDisplay: PropTypes.number,
  currency: PropTypes.string.isRequired,
  displayAs: PropTypes.oneOf(['default', 'free', 'undefined']),
};

export default BillingInfo;
