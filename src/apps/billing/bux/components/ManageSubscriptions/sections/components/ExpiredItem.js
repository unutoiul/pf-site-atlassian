import { Trans } from '@lingui/react';
import React from 'react';
import cx from 'classnames';
import products from 'bux/common/data/products';
import { ExternalLink } from 'bux/components/Link';
import ProductLogo from '../../../ak/logo-atlassian';
import SubscriptionPropTypes from '../../PropTypes';
import LogScope from '../../../log/Scope/index';
import EntitlementLozenge from './components/EntitlementLozenge';
import styles from './style.less';

const ExpiredItem = ({ data: { title, entitlement } }) => {
  const productTitle = title.split('(')[0].trim();

  const product = products[entitlement.productKey] || {};
  const description = product.description;
  const link = product.link;
  const isUnsubscribed = entitlement.status === 'INACTIVE';
  const isSuspended = entitlement.suspended;

  return (
    <li className={cx('subscription-line', styles.subscriptionItem)}>
      <LogScope prefix={entitlement.productKey} className={styles.grid}>

        <div className={styles.logoBlock}>
          <div className={styles.logoBlockLogo}>
            <ProductLogo product={entitlement.productKey} size="medium" name={productTitle} />
          </div>
          <div className={styles.logoBlockLozenges}>
            <EntitlementLozenge isUnsubscribed={isUnsubscribed} isSuspended={isSuspended} />
          </div>
        </div>

        {description && <p className={styles.productDescription}>{description}</p>}

        {link &&
        <ExternalLink href={link}>
          <Trans id="billing.manage-subscriptions.inactive-subscriptions.product.learn-more">
            Learn more about {productTitle}.
          </Trans>
        </ExternalLink>
        }

      </LogScope>
    </li>
  );
};

ExpiredItem.propTypes = {
  data: SubscriptionPropTypes.subscription.isRequired
};

export default ExpiredItem;
