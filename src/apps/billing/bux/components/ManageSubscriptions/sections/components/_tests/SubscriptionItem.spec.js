import React from 'react';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';
import Page from '@atlaskit/page';
import { Provider } from 'bux/helpers/redux';
import Lozenge from '@atlaskit/lozenge';
import FeatureFlagProvider from 'bux/components/FeatureFlags/FeatureFlagProvider';
import ProductLogo from '../../../../ak/logo-atlassian';
import SubscriptionItem from '../SubscriptionItem';
import Usage from '../components/Usage';
import BillingDate from '../components/BillingDate';
import BillingInfo from '../components/BillingInfo';
import TrialInfo from '../components/TrialInfo';
import EntitlementLozenge from '../components/EntitlementLozenge';
import AccessUsage from '../components/AccessUsage';


describe('SubscriptionItem component', () => {
  const data = {
    title: 'Product',
    entitlement: {
      name: 'Product',
      creationDate: '2016-12-05',
      entitlementGroupId: '4d854acf-e651-4a4b-bdd0-d0c76f0995ed',
      trialEndDate: '2032-12-17',
      accountId: '363bf5a5-47d7-42f9-9c51-930c40d6b3b9',
      status: 'ACTIVE',
      endDate: '2016-12-17',
      productKey: 'product.key',
      startDate: '2016-12-05',
      id: 'testcase',
      sen: 'A813576'
    },

    billingRecord: {
      usage: [
        {
          taxRate: 10,
          amountBeforeTax: 0,
          description: 'Product Usage',
          units: 1,
          unitCode: 'products',
          amountWithTax: 0,
          taxAmount: 0
        }
      ],
      entitlementId: 'testCase',
      amountWithTax: 0,
      taxAmount: 0,
      amountBeforeTax: 0,
      productKey: 'product.key',
      productName: 'Product'
    },

  };

  const getPayload = (override = {}) => ({
    title: override.title || data.title,
    entitlement: {
      ...data.entitlement,
      ...(override.entitlement || {})
    },
    billingRecord: {
      ...data.billingRecord,
      ...(override.billingRecord || {})
    }
  });

  const wrap = (payload, props = {}) => shallow(<Page>
    <SubscriptionItem data={payload} currency="USD" {...props} />
                                                </Page>).find(SubscriptionItem).shallow();

  it('should render title ', () => {
    const payload = getPayload({ title: 'Product' });
    const wrapper = wrap(payload);
    const logo = wrapper.find(ProductLogo);
    expect(logo.props()).to.be.deep.equal({
      product: 'product.key',
      size: 'medium',
      name: 'Product'
    });
  });

  it('should render usage with billingRecord  ', () => {
    const payload = getPayload();
    const wrapper = wrap(payload);
    expect(wrapper.find(Usage)).to.be.present();
  });

  it('should work without billingRecord  ', () => {
    const payload = getPayload();
    payload.billingRecord = undefined;
    const wrapper = wrap(payload);
    expect(wrapper.find(Usage)).not.to.be.present();
  });

  it('should render usage with for  AA', () => {
    const payload = getPayload();
    payload.entitlement.productKey = 'com.atlassian.identity-manager';
    const wrapper = mount(<FeatureFlagProvider featureFlags={{ 'atlassian-access-warnings': true }}>
      <Provider>
        <SubscriptionItem data={payload} currency="USD" />
      </Provider>
                          </FeatureFlagProvider>);
    expect(wrapper.find(AccessUsage)).to.be.present();
  });

  it('should work without billingRecord and AA', () => {
    const payload = getPayload();
    payload.entitlement.productKey = 'com.atlassian.identity-manager';
    payload.billingRecord = undefined;
    const wrapper = mount(<FeatureFlagProvider featureFlags={{ 'atlassian-access-warnings': true }}>
      <Provider>
        <SubscriptionItem data={payload} currency="USD" />
      </Provider>
                          </FeatureFlagProvider>);
    expect(wrapper.find(AccessUsage)).not.to.be.present();
  });


  it('should render main components', () => {
    const payload = getPayload({ entitlement: { selectedEdition: null } });
    const wrapper = wrap(payload, { isTrial: false });
    expect(wrapper.find(BillingInfo)).to.be.present();
    expect(wrapper.find(Usage)).to.be.present();
    expect(wrapper.find(BillingDate)).to.be.present();
    expect(wrapper.find(Lozenge)).not.to.be.present();
  });

  it('should render EntitlementLozenge with edition', () => {
    const payload = getPayload({ entitlement: { selectedEdition: 'specialEdition' } });
    const wrapper = wrap(payload, {});

    const entitlementLozenge = wrapper.find(EntitlementLozenge);

    expect(entitlementLozenge).to.have.length(1);
    expect(entitlementLozenge.prop('edition')).to.be.equal('specialEdition');
  });

  it('should render EntitlementLozenge with trial', () => {
    const payload = getPayload();
    const wrapper = wrap(payload, { isTrial: true });

    const entitlementLozenge = wrapper.find(EntitlementLozenge).at(0);

    expect(entitlementLozenge.prop('isTrial')).to.be.equal(true);
  });

  it('should not render usage or billing date with zero dollar trial', () => {
    const payload = getPayload();
    const wrapper = wrap(payload, { isTrial: true });
    const lozenges = wrapper.find(EntitlementLozenge);
    const usage = wrapper.find(Usage);
    const billingDate = wrapper.find(BillingDate);
    const billingInfo = wrapper.find(BillingInfo);
    const trialInfo = wrapper.find(TrialInfo);

    expect(usage).not.to.be.present();
    expect(billingDate).not.to.be.present();
    expect(billingInfo).not.to.be.present();

    expect(trialInfo).to.be.present();

    expect(lozenges.prop('isTrial')).to.be.equal(true);
  });

  it('should render usage and billing date with non zero dollar trial', () => {
    const payload = getPayload({
      billingRecord: {
        usage: [
          {
            taxRate: 10,
            amountBeforeTax: 10,
            description: 'Product Usage',
            units: 1,
            unitCode: 'products',
            amountWithTax: 11,
            taxAmount: 1
          }
        ],
        entitlementId: 'testCase',
        amountWithTax: 11,
        taxAmount: 1,
        amountBeforeTax: 10,
        productKey: 'product.key',
        productName: 'Product'
      }
    });
    const wrapper = wrap(payload, { isTrial: true });
    const lozenges = wrapper.find(EntitlementLozenge);
    const usage = wrapper.find(Usage);
    const billingDate = wrapper.find(BillingDate);
    const billingInfo = wrapper.find(BillingInfo);
    const trialInfo = wrapper.find(TrialInfo);

    expect(usage).to.be.present();
    expect(billingDate).to.be.present();
    expect(billingInfo).to.be.present();

    expect(trialInfo).not.to.be.present();

    expect(billingInfo).to.have.prop('amountToDisplay', 10);

    expect(lozenges.prop('isTrial')).to.be.equal(true);
  });

  it('should render downgrade lozenge', () => {
    const payload = getPayload({
      entitlement: {
        futureEdition: 'newEdition',
        futureEditionTransition: 'DOWNGRADE'
      }
    });
    const wrapper = wrap(payload);
    const billedOn = wrapper.find(BillingDate);

    expect(billedOn.prop('isDowngrading')).to.be.equal(true);
    expect(billedOn.prop('futureEdition')).to.be.equal('newEdition');
  });

  it('should render downgrade lozenge with full date when annual', () => {
    const payload = getPayload({
      entitlement: {
        futureEdition: 'newEdition',
        futureEditionTransition: 'DOWNGRADE'
      }
    });
    const wrapper = wrap(payload, { isAnnual: true });
    const billedOn = wrapper.find(BillingDate);

    expect(billedOn.prop('isDowngrading')).to.be.equal(true);
    expect(billedOn.prop('isAnnual')).to.be.equal(true);
    expect(billedOn.prop('futureEdition')).to.be.equal('newEdition');
  });

  it('should render free version of billing date component when amount is 0', () => {
    const payload = getPayload({
      billingRecord: {
        amountWithTax: 0
      }
    });
    const wrapper = wrap(payload);
    const billedOn = wrapper.find(BillingDate);

    expect(billedOn.prop('isFree')).to.be.equal(true);
  });

  it('should undefined billing info when billing record is not present', () => {
    const payload = getPayload();
    payload.billingRecord = undefined;
    const wrapper = wrap(payload);
    const billingInfo = wrapper.find(BillingInfo);

    expect(billingInfo.prop('displayAs')).to.be.equal('undefined');
  });

  it('should default billing info when billing record is not present for trial', () => {
    const payload = getPayload();
    payload.billingRecord = undefined;
    const wrapper = wrap(payload, { isTrial: true });
    const billingInfo = wrapper.find(BillingInfo);

    expect(billingInfo.prop('displayAs')).to.be.equal('default');
  });
});
