import React from 'react';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';
import EntitlementLozenge from '../EntitlementLozenge';
import BillingDate from '../BillingDate';
import BillingDateLabel from '../BillingDateLabel';

const DEFAULT_PROPS = {
  isFree: false,
  isAnnual: true,
};

describe('SubscriptionItem.Billing component', () => {
  describe('BillingDate', () => {
    const entitlement = {
      endDate: '2052-10-05'
    };

    it('should have caption and indicate end date', () => {
      const wrapper = mount(<BillingDate entitlement={entitlement} />);
      expect(wrapper.find(BillingDateLabel)).to.be.present();
      const lozenge = wrapper.find(EntitlementLozenge);

      expect(lozenge).not.to.have.prop('isDowngrading');
      expect(lozenge).not.to.have.prop('futureEdition');
    });

    it('should indicate full date for annual customrs', () => {
      const wrapper = mount(<BillingDate entitlement={entitlement} isAnnual />);
      expect(wrapper.find(BillingDateLabel)).to.be.present();
    });

    it('should not have caption in case of free', () => {
      const wrapper = shallow(<BillingDate entitlement={entitlement} isFree />);
      expect(wrapper.find(BillingDateLabel)).to.be.present();
    });

    it('should show lozenge with date when downgrade', () => {
      const wrapper = shallow(<BillingDate entitlement={entitlement} isDowngrading futureEdition="new" isFree />);
      const lozenge = wrapper.find(EntitlementLozenge);

      expect(lozenge).to.have.prop('isDowngrading');
      expect(lozenge).to.have.prop('futureEdition', 'new');
      expect(lozenge).to.have.prop('billingDate', '2052-10-05');
    });

    it('should show full date in lozenge when annual', () => {
      const wrapper = shallow(<BillingDate
        entitlement={entitlement}
        futureEdition="new"
        isDowngrading
        isAnnual
        isFree
      />);
      const lozenge = wrapper.find(EntitlementLozenge);

      expect(lozenge).to.have.prop('isAnnual');
      expect(lozenge).to.have.prop('futureEdition', 'new');
      expect(lozenge).to.have.prop('billingDate', '2052-10-05');
    });

    it('should render bill date label component', () => {
      const wrapper = shallow(<BillingDate entitlement={entitlement} {...DEFAULT_PROPS} />);
      expect(wrapper.find(BillingDateLabel)).to.be.present();
      expect(wrapper.find(BillingDateLabel)).to.have.props({
        isAnnual: DEFAULT_PROPS.isAnnual,
        isFree: DEFAULT_PROPS.isFree
      });
    });
  });
});
