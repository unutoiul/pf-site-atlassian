import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';
import { FullDate } from 'bux/components/Date';
import SubscriptionPropTypes from '../../../PropTypes';
import EntitlementLozenge from './EntitlementLozenge';
import styles from './style.less';
import BillingDateLabel from './BillingDateLabel';


const BillingDate = ({
  isFree, isDowngrading, futureEdition, entitlement, isAnnual
}) => (
  <div>
    <BillingDateLabel isFree={isFree} isAnnual={isAnnual} />

    <div className={cx(styles.informationLine, 'payload')}>
      <EntitlementLozenge
        isDowngrading={isDowngrading}
        isAnnual={isAnnual}
        futureEdition={futureEdition}
        billingDate={entitlement.endDate}
      />
      {!isFree && !isDowngrading &&
        <time>
          <FullDate value={entitlement.endDate} />
        </time>
      }
    </div>
  </div>
);

BillingDate.propTypes = {
  entitlement: SubscriptionPropTypes.entitlement,
  isFree: PropTypes.bool,
  isDowngrading: PropTypes.bool,
  isAnnual: PropTypes.bool,
  futureEdition: PropTypes.string,
};

export default BillingDate;
