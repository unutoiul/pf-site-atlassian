import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';
import { ModalFooter } from '@atlaskit/modal-dialog';
import proxyquire from 'bux/helpers/proxyquire';
import Provider from 'bux/helpers/redux/Provider';
import getAnalytic from 'bux/components/log/helpers/getAnalytic';

describe('ConfirmCancel component: ', () => {
  let onConfirm;
  let onClose;
  let redirect;
  let EnhancedFooter;
  let renderWidget;
  let FooterWithoutAnalytics;

  beforeEach(() => {
    onConfirm = sinon.stub();
    onClose = sinon.stub();
    redirect = sinon.stub();
    const FooterModule = proxyquire.load('../Footer', {
      'bux/common/helpers/browser': {
        redirect
      }
    });
    FooterWithoutAnalytics = FooterModule.FooterWithoutAnalytics;
    EnhancedFooter = FooterModule.default;
    renderWidget = props => <Provider><EnhancedFooter {...props} /></Provider>;
  });

  it('should render modal footer', () => {
    const component = mount(renderWidget({ onClose: () => {}, onConfirm: () => {} }), getAnalytic().context);
    expect(component.find(ModalFooter)).to.be.present();
  });
  describe('ConfirmCancel call onConfirm: ', () => {
    it('should call onConfirm and onClose when click button-submit', (done) => {
      onConfirm.resolves();
      const component = mount(renderWidget({ onClose, onConfirm }), getAnalytic().context);
      const confirmButton = component.find('.button-submit');
      const footer = component.find(FooterWithoutAnalytics).instance();
      expect(confirmButton).to.be.present();
      confirmButton.hostNodes().simulate('click');

      setImmediate(() => {
        expect(onConfirm).to.have.been.called();
        expect(redirect).to.have.been.called();
        expect(onClose).to.have.been.called();
        expect(footer.state.processing).to.be.true();
        done();
      });
    });

    it('should fail if onConfirm promise reject', (done) => {
      onConfirm.rejects();
      const component = mount(renderWidget({ onClose, onConfirm }), getAnalytic().context);
      const confirmButton = component.find('.button-submit');
      const footer = component.find(FooterWithoutAnalytics).instance();
      expect(confirmButton).to.be.present();
      confirmButton.hostNodes().simulate('click');

      expect(onConfirm).to.have.been.called();
      setTimeout(() => {
        expect(redirect).to.not.have.been.called();
        expect(onClose).to.not.have.been.called();
        expect(footer.state.processing).to.be.false();
        done();
      }, 100);
    });
  });
  it('should call onClose when click button-cancel', () => {
    const component = mount(renderWidget({ onClose, onConfirm: () => {} }), getAnalytic().context);
    const confirmButton = component.find('.button-cancel');
    expect(confirmButton).to.be.present();
    confirmButton.hostNodes().simulate('click');
    expect(onClose).to.have.been.called();
  });
});
