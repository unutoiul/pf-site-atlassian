import PropTypes from 'prop-types';
import React from 'react';
import { Trans } from '@lingui/react';
import { FormattedDate, getFormat } from 'bux/components/Date';
import styles from './style.less';

const TrialInfo = ({ endDate, isAnnual }) => {
  const formattedDate = <FormattedDate value={endDate} format={getFormat(isAnnual)} />;
  return (
    <div>
    <span className="label">
      <Trans id="billing.manage-subscriptions.trial-info.label">Price estimate</Trans>
    </span>

      <div className={styles.informationLine}>
      <span className="price payload">
        <span>
          <Trans id="billing.manage-subscriptions.trial-info.info">
            Free until {formattedDate}
          </Trans>
        </span>
      </span>
      </div>
    </div>
  );
};

TrialInfo.propTypes = {
  endDate: PropTypes.string,
  isAnnual: PropTypes.bool,
};

export default TrialInfo;
