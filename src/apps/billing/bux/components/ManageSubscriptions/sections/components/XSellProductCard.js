import React, { Component } from 'react';
import cx from 'classnames';
import { string, object, func, bool } from 'prop-types';
import { Trans } from '@lingui/react';
import Lozenge from '@atlaskit/lozenge';
import Button from '@atlaskit/button';
import DiscoverFilledIcon from '@atlaskit/icon/glyph/discover-filled';
import ProductLogo from 'bux/components/ak/logo-atlassian';
import { Analytics } from 'bux/components/Analytics';
import ProductPrice from 'bux/features/DiscoverNewProducts/components/ProductCard/components/ProductPrice';
import ProductButton from 'bux/features/DiscoverNewProducts/components/ProductCard/components/ProductButton';
import ProductDescription from 'bux/features/DiscoverNewProducts/components/ProductCard/components/ProductDescription';
import ProductContext from 'bux/features/DiscoverNewProducts/components/ProductCard/ProductContext';
import { Media } from 'common/responsive/Matcher';

import styles from './style.less';

class XSellProductCard extends Component {
  static propTypes = {
    productKey: string.isRequired,
    productData: object.isRequired,
    onCTAClick: func.isRequired,
    onDiscoverApplicationsButtonClick: func.isRequired,
    onTriggerAnalytics: func.isRequired,
    isExpired: bool.isRequired,
  };
  
  handleCTAClick = () => {
    const { productData: { reactivation, promoted }, onCTAClick, onTriggerAnalytics } = this.props;
    onTriggerAnalytics('grow0.experiment.pc8111.manageSubscriptions.tryClicked', {
      isReactivation: reactivation,
      promoted
    });
    if (onCTAClick) {
      onCTAClick();
    }
  };

  handleDiscoverApplicationsButtonClick = () => {
    const { onDiscoverApplicationsButtonClick, onTriggerAnalytics } = this.props;
    onTriggerAnalytics('grow0.experiment.pc8111.manageSubscriptions.discoverApplicationsLinkClicked');
    if (onDiscoverApplicationsButtonClick) {
      onDiscoverApplicationsButtonClick();
    }
  };

  handleLearnMoreClick = () => {
    const { onTriggerAnalytics } = this.props;
    onTriggerAnalytics('grow0.experiment.pc8111.manageSubscriptions.learnMoreLinkClicked');
  }

  handlePricingClick = () => {
    const { onTriggerAnalytics } = this.props;
    onTriggerAnalytics('grow0.experiment.pc8111.manageSubscriptions.pricingLinkClicked');
  }

  render() {
    const { productKey, productData, isExpired } = this.props;
    const { name, tag } = productData;
    return (
      <ProductContext.Provider value={productData}>
        <div className={cx(styles.productItem, 'x-sell-product-card')}>
          <div className={styles.productCard}>
            <div className={styles.productTitleBlock}>
              <div className={styles.productTitle}>
                <ProductLogo
                  product={productKey}
                  size="medium"
                  name={name}
                />
                <Media.Below including mobile>
                  <div className={styles.productFamily} data-test="product-tag">
                    <Lozenge>{tag}</Lozenge>
                  </div>
                </Media.Below>
              </div>
              <Media.Above mobile>
                <div className={styles.productFamily} data-test="product-tag">
                  <Lozenge>{tag}</Lozenge>
                </div>
              </Media.Above>
              <ProductButton onClick={this.handleCTAClick} disabled={isExpired} />
            </div>
            <div className={styles.productInfoBlock}>
              <ProductDescription onLearnMoreClick={this.handleLearnMoreClick} />
              <ProductPrice onPricingClick={this.handlePricingClick} />
            </div>
          </div>
          <div className={cx(styles.noticeRow, 'discover-applications-button')}>
            <div className={styles.discoverApplicationsLink}>
              <Analytics.UI as="discoverMoreAtlassianApplicationsButton">
                <Button
                  onClick={this.handleDiscoverApplicationsButtonClick}
                  appearance="default"
                  iconBefore={<DiscoverFilledIcon label="DiscoverIcon" />}
                >
                  <Trans id="billing.manage-subscriptions.discover-more-atlassian-applications">
                    Discover more Atlassian applications
                  </Trans>
                </Button>
              </Analytics.UI>
            </div>
          </div>
        </div>
      </ProductContext.Provider>
    );
  }
}

export default XSellProductCard;
