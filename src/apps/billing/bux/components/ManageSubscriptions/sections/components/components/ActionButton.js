import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import Button from '@atlaskit/button';
import { I18n } from '@lingui/react';
import DropdownMenu, { DropdownItem, DropdownItemGroup } from '@atlaskit/dropdown-menu';

import { Analytics, withAnalyticsEmitter } from 'bux/components/Analytics';
import { getActions, ActionType } from 'bux/features/ManageSubscriptions';
import { getFeatureFlags } from 'bux/core/state/meta/selectors';

import styles from './style.less';

const createAction = (content, analytics, action) => ({ content, analytics, action });

const convertAction = (action = {}, entitlement, dispatch, featureFlags) => 
  createAction(action.label, action.analytics, () => action.action({
    dispatch, entitlement, featureFlags
  }));

const withLogAction = (actionItem, sendAnalyticEvent) => {
  sendAnalyticEvent(`${actionItem.content}.click`);
  actionItem.action();
};

const getMainAction = actions => actions.find(action => action.primaryAction);

const getSecondaryActions = (actions, entitlement, dispatch, featureFlags, mainAction) =>
  actions
    .filter(action => action !== mainAction)
    .map(action => convertAction(action, entitlement, dispatch, featureFlags));

export const ActionUIListener = ({ children, action }) => {
  if (action.analytics) {
    return (
      <Analytics.UI as={action.analytics.actionSubjectId} attributes={action.analytics.attributes}>
        {children}
      </Analytics.UI>
    );
  }
  return children;
};

export const DropdownItemWithAnalytics = withAnalyticsEmitter(
  'DropdownItem',
  'dropListItem',
  {
    onClick: {
      action: 'clicked'
    }
  }
)(DropdownItem);

export const StatelessActionButton = ({
  actions, entitlement, dispatch, featureFlags
}, { sendAnalyticEvent }) => {
  const mainAction = convertAction(getMainAction(actions), entitlement, dispatch, featureFlags);
  return (
    <div className={styles.buttons}>
      <ActionUIListener action={mainAction}>
        <Button
          onClick={() => withLogAction(mainAction, sendAnalyticEvent)}
          className="mainActionButton"
        >
          {mainAction.content}
        </Button>
      </ActionUIListener>
      <div className="secondaryActions">
        <DropdownMenu
          triggerType="button"
          position="bottom right"
          onItemActivated={({ item }) => withLogAction(item, sendAnalyticEvent)}
          onOpenChange={({ isOpen }) => isOpen && sendAnalyticEvent('showSecondary')}
          boundariesElement="window"
        >
          <DropdownItemGroup>
            {
              getSecondaryActions(actions, entitlement, dispatch, featureFlags, getMainAction(actions))
                .map((action, index) => (
                  <ActionUIListener action={action} key={index}>
                    <DropdownItemWithAnalytics onClick={action.action}>{action.content}</DropdownItemWithAnalytics>
                  </ActionUIListener>
                ))
            }
          </DropdownItemGroup>
        </DropdownMenu>
      </div>
    </div>
  );
};

StatelessActionButton.propTypes = {
  entitlement: PropTypes.any,
  dispatch: PropTypes.func,
  actions: PropTypes.arrayOf(ActionType),
  featureFlags: PropTypes.object
};

StatelessActionButton.contextTypes = {
  sendAnalyticEvent: PropTypes.func
};

const ActionsProvider = ({
  entitlement, state, dispatch, featureFlags
}) => (
  <I18n>{({ i18n }) => (
    <StatelessActionButton
      dispatch={dispatch}
      entitlement={entitlement}
      featureFlags={featureFlags}
      actions={getActions(entitlement, state, i18n)}
    />
  )}</I18n>
);

ActionsProvider.propTypes = {
  state: PropTypes.any,
  entitlement: PropTypes.any,
  dispatch: PropTypes.func,
  featureFlags: PropTypes.object
};

const mapStateToProps = state => ({
  state,
  featureFlags: getFeatureFlags(state)
});

const mapDispatchToProps = dispatch => ({
  dispatch
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ActionsProvider);
