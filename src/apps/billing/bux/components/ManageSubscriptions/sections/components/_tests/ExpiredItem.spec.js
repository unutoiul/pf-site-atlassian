import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import products from 'bux/common/data/products';
import Page from '@atlaskit/page';
import { ExternalLink } from 'bux/components/Link';
import ProductLogo from '../../../../ak/logo-atlassian';
import ExpiredItem from '../ExpiredItem';
import EntitlementLozenge from '../components/EntitlementLozenge';

describe('ExpiredItem component', () => {
  const data = {
    title: 'Product',
    entitlement: {
      name: 'Product',
      creationDate: '2016-12-05',
      entitlementGroupId: '4d854acf-e651-4a4b-bdd0-d0c76f0995ed',
      trialEndDate: '2032-12-17',
      accountId: '363bf5a5-47d7-42f9-9c51-930c40d6b3b9',
      status: 'ACTIVE',
      endDate: '2016-12-17',
      productKey: 'product.key',
      startDate: '2016-12-05',
      id: 'testcase',
      sen: 'A813576'
    },

    billingRecord: {
      usage: [
        {
          taxRate: 10,
          amountBeforeTax: 0,
          description: 'Product Usage',
          units: 1,
          unitCode: 'products',
          amountWithTax: 0,
          taxAmount: 0
        }
      ],
      entitlementId: 'testCase',
      amountWithTax: 0,
      taxAmount: 0,
      amountBeforeTax: 0,
      productKey: 'product.key',
      productName: 'Product'
    },

  };

  const getPayload = (override = {}) => ({
    title: override.title || data.title,
    entitlement: {
      ...data.entitlement,
      ...(override.entitlement || {})
    },
    billingRecord: {
      ...data.billingRecord,
      ...(override.billingRecord || {})
    }
  });

  const wrap = (payload, props = {}) => shallow(<Page>
    <ExpiredItem data={payload} currency="USD" {...props} />
                                                </Page>).find(ExpiredItem).shallow();

  it('should render title', () => {
    const payload = getPayload({ title: 'Product' });
    const wrapper = wrap(payload);
    const logo = wrapper.find(ProductLogo);
    expect(logo.props()).to.be.deep.equal({
      product: 'product.key',
      size: 'medium',
      name: 'Product'
    });
  });

  it('should render product description if available', () => {
    const JIRA_KEY = 'jira-software.ondemand';
    const product = products[JIRA_KEY];
    const payload = getPayload({ entitlement: { productKey: JIRA_KEY } });
    const wrapper = wrap(payload);
    expect(wrapper.find('p')).to.contain.text(product.description.props.children[0]);
  });

  it('should render product link if available', () => {
    const JIRA_KEY = 'jira-software.ondemand';
    const product = products[JIRA_KEY];
    const payload = getPayload({ entitlement: { productKey: JIRA_KEY } });
    const wrapper = wrap(payload);
    expect(wrapper.find(ExternalLink)).to.have.prop('href', product.link);
  });

  it('should handle unknown product', () => {
    const payload = getPayload();
    const wrapper = wrap(payload);
    expect(wrapper.find(ExternalLink)).to.not.present();
  });

  it('should work without billingRecord  ', () => {
    const payload = getPayload();
    payload.billingRecord = undefined;
    wrap(payload);
  });

  it('should render isSuspended lozenge for suspended product', () => {
    const wrapper = wrap(getPayload({ entitlement: { suspended: true } }));
    const lozenge = wrapper.find(EntitlementLozenge);

    expect(lozenge).to.have.prop('isSuspended');
  });

  it('should render isUnsubscribed lozenge for inactive product', () => {
    const wrapper = wrap(getPayload({ entitlement: { status: 'INACTIVE' } }));
    const lozenge = wrapper.find(EntitlementLozenge);

    expect(lozenge).to.have.prop('isUnsubscribed');
  });
});
