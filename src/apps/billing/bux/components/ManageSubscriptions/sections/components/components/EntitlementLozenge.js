import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';
import Lozenge from '@atlaskit/lozenge';
import { Trans } from '@lingui/react';
import { FormattedDate, getFormat } from 'bux/components/Date';
import styles from './style.less';

const EntitlementLozenge = ({
  edition,
  futureEdition,
  isTrial,
  isAnnual,
  isUnsubscribed,
  isSuspended,
  billingDate,
  isDowngrading
}) => (
  <span className={cx('entitlement-lozenge', styles.lozenge)}>
    {
      edition &&
      <Lozenge className="editionLozenge">{edition}</Lozenge>
    }
    {
      isTrial &&
      <Lozenge className="trialLozenge" appearance="success">
        <Trans id="billing.manage-subscriptions.entitlement-lozenge.trial">
          free trial
        </Trans>
      </Lozenge>
    }
    {
      isUnsubscribed &&
      <Lozenge className="unsubscribedLozenge" appearance="removed">
        <Trans id="billing.manage-subscriptions.entitlement-lozenge.unsubscribed">
          unsubscribed
        </Trans>
      </Lozenge>
    }
    {
      isSuspended &&
      <Lozenge className="suspendedLozenge" appearance="removed">
        <Trans id="billing.manage-subscriptions.entitlement-lozenge.suspended">
          suspended
        </Trans>
      </Lozenge>
    }
    {
      isDowngrading && futureEdition &&
      <span className={styles.downgradeLozenge}>
        <Trans id="billing.manage-subscriptions.entitlement-lozenge.downgrading">
          downgrading to {futureEdition} on <FormattedDate value={billingDate} format={getFormat(isAnnual)} />
        </Trans>

      </span>
    }
  </span>
);
EntitlementLozenge.propTypes = {
  edition: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  futureEdition: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  isTrial: PropTypes.bool,
  isAnnual: PropTypes.bool,
  isUnsubscribed: PropTypes.bool,
  isSuspended: PropTypes.bool,
  billingDate: PropTypes.string,
  isDowngrading: PropTypes.bool
};

export default EntitlementLozenge;
