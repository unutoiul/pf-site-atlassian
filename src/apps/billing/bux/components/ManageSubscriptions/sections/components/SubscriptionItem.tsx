import cx from 'classnames';
import * as PropTypes from 'prop-types';
import * as React from 'react';

import ProductLogo from 'bux/components/ak/logo-atlassian';
import { Analytics } from 'bux/components/Analytics';
import FeatureFlagged from 'bux/components/FeatureFlags/FeatureFlagged';
import LogScope from 'bux/components/log/Scope';
import SubscriptionPropTypes from 'bux/components/ManageSubscriptions/PropTypes';
import { SubscriptionType } from 'bux/components/ManageSubscriptions/Types';
import ManagementWall from 'bux/components/Page/ManagementWall';

import AccessUsage from './components/AccessUsage';
import ActionButton from './components/ActionButton';
import BillingDate from './components/BillingDate';
import BillingInfo from './components/BillingInfo';
import EntitlementLozenge from './components/EntitlementLozenge';
import { ProductContextProvider } from './components/productContext';
import TrialInfo from './components/TrialInfo';
import Usage from './components/Usage';
import styles from './style.less';

interface SubscriptionItemProps {
  data: SubscriptionType;
  isTrial: boolean;
  isAnnual: boolean;
  isSubscriptionEnding: boolean;
  currency: string;
}

const SubscriptionItem: React.SFC<SubscriptionItemProps> = (
  {
    data: {
      title, entitlement, billingRecord = ({} as any), edition,
    },
    isTrial = false,
    isAnnual = false,
    isSubscriptionEnding = false,
    currency,
  }) => {
  const hasNoEstimation = Object.keys(billingRecord).length === 0 || entitlement.productKey === 'opsgenie';
  const isFree = billingRecord.amountWithTax === 0;
  const isTrialWithEstimate = isTrial && isFree;
  const clearTitle = title.replace('(Cloud)', '').trim();
  const isDowngrading = entitlement.futureEditionTransition === 'DOWNGRADE';
  const isAtlassianAccess = entitlement.productKey === 'com.atlassian.identity-manager';

  return (
    <ProductContextProvider
      value={{
        title: clearTitle,
        entitlement,
        billingRecord,
        edition,
      }}
    >
      <li className={cx('subscription-line', styles.subscriptionItem)}>
        <LogScope prefix={entitlement.productKey} className={styles.grid}>
          <div className={styles.logoBlock}>
            <div className={styles.logoBlockLogo}>
              <ProductLogo product={entitlement.productKey} size="medium" name={clearTitle} />
            </div>
            <div className={styles.logoBlockLozenges}>
              <EntitlementLozenge
                isTrial={isTrial}
                isAnnual={isAnnual}
                isDowngrading={isDowngrading}
                edition={edition ? edition.name : entitlement.selectedEdition}
                billingDate={entitlement.trialEndDate}
              />
            </div>
            <div className={styles.logoBlockActionButtons}>
              <Analytics.Context.PropagatedProvider
                value={{
                  objectId: entitlement.productKey,
                  objectType: 'subscriptionLine',
                  attributes: {
                    linkContainer: 'activeSubscriptions',
                  },
                }}
              >
                <ActionButton entitlement={entitlement} />
              </Analytics.Context.PropagatedProvider>
            </div>
          </div>

          <div className={styles.block30}>
            {!isTrialWithEstimate && billingRecord.usage &&
            <div className={cx(styles.usageLine, 'usage')}>
              {!isAtlassianAccess && <Usage usage={billingRecord.usage} />}
              <FeatureFlagged flags={['atlassian-access-warnings']}>
                {isAtlassianAccess && <AccessUsage usage={billingRecord.usage[0]} />}
              </FeatureFlagged>
            </div>}
          </div>

          <div className={styles.block30}>
            {!isTrialWithEstimate &&
            <div className={cx(styles.billingDate, 'billedOn')}>
              <BillingDate
                entitlement={entitlement}
                isDowngrading={isDowngrading}
                isFree={isFree}
                isAnnual={isAnnual}
                futureEdition={entitlement.futureEdition}
              />
            </div>
            }
          </div>

          <ManagementWall wall={null}>
            <div className={styles.block30}>
              <div className={cx(styles.billingInfo, 'priceEstimation')}>
                {isTrialWithEstimate
                  ? <TrialInfo endDate={entitlement.endDate} isAnnual={isAnnual} />
                  : <BillingInfo
                    amountToDisplay={billingRecord.amountBeforeTax}
                    currency={currency}
                    displayAs={
                      ((isSubscriptionEnding || (hasNoEstimation && !isTrial)) && 'undefined') ||
                      'default'
                    }
                  />
                }
              </div>
            </div>
          </ManagementWall>
        </LogScope>
      </li>
    </ProductContextProvider>
  );
};

SubscriptionItem.propTypes = {
  data: SubscriptionPropTypes.subscription.isRequired,
  isTrial: PropTypes.bool,
  isAnnual: PropTypes.bool,
  isSubscriptionEnding: PropTypes.bool,
  currency: PropTypes.string.isRequired,
};

export default SubscriptionItem;
