import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { inContentChrome, inStoryKnob, withFeatureFlagsChrome } from 'bux/common/helpers/storybook';

import SubscriptionItem from './SubscriptionItem';
import ExpiredItem from './ExpiredItem';
import XSellProductCard from './XSellProductCard';

const entitlement = {
  name: 'Product',
  creationDate: '2016-12-05',
  entitlementGroupId: '4d854acf-e651-4a4b-bdd0-d0c76f0995ed',
  trialEndDate: '2032-12-17',
  accountId: '363bf5a5-47d7-42f9-9c51-930c40d6b3b9',
  status: 'ACTIVE',
  endDate: '2016-12-17',
  productKey: 'jira-core.ondemand',
  startDate: '2016-12-05',
  id: 'testcase',
  sen: 'A813576'
};

const billingRecord = {
  usage: [
    {
      taxRate: 10,
      amountBeforeTax: 0,
      description: 'Product Usage',
      units: 1,
      unitCode: 'products',
      amountWithTax: 0,
      taxAmount: 0
    },
    {
      taxRate: 10,
      amountBeforeTax: 0,
      description: 'Another Usage',
      units: 10,
      unitCode: 'products',
      amountWithTax: 0,
      taxAmount: 0
    }
  ],
  entitlementId: 'testCase',
  amountWithTax: 11,
  taxAmount: 1,
  amountBeforeTax: 10,
  productKey: 'jira-core.ondemand',
  productName: 'Product'
};

const tieredBillingRecord = {
  usage: [
    {
      taxRate: 10,
      amountBeforeTax: 0,
      description: 'Product Usage',
      units: 20,
      currentUnits: 19,
      tiered: true,
      unitCode: 'users',
      amountWithTax: 0,
      taxAmount: 0
    }
  ],
  entitlementId: 'testCase',
  amountWithTax: 11,
  taxAmount: 1,
  amountBeforeTax: 10,
  productKey: 'jira-core.ondemand',
  productName: 'Product'
};

storiesOf('BUX|Components/ManageSubscriptions/components', module)
  .add('SubscriptionItem', () => inContentChrome(inStoryKnob(SubscriptionItem, {
    data: {
      title: 'I am the product',
      entitlement,
      billingRecord,
      edition: 'Free'
    },
    isTrial: true,
    isAnnual: false,
    isSubscriptionEnding: false,
    currency: 'USD'
  })))
  .add('AtlassianAccessItem', () => withFeatureFlagsChrome({
    'atlassian-access-warnings': true
  }, inContentChrome(inStoryKnob(SubscriptionItem, {
    data: {
      title: 'I am the product',
      entitlement: {
        ...entitlement,
        productKey: 'com.atlassian.identity-manager',
      },
      billingRecord,
      edition: 'Free'
    },
    isTrial: true,
    isAnnual: false,
    isSubscriptionEnding: false,
    currency: 'USD'
  }))))
  .add('AtlassianAccessItem With breach', () => withFeatureFlagsChrome({
    'atlassian-access-warnings': true
  }, inContentChrome(inStoryKnob(SubscriptionItem, {
    data: {
      title: 'I am the product',
      entitlement: {
        ...entitlement,
        productKey: 'com.atlassian.identity-manager',
      },
      billingRecord: tieredBillingRecord,
      edition: 'Free'
    },
    isTrial: true,
    isAnnual: false,
    isSubscriptionEnding: false,
    currency: 'USD'
  }))))
  .add('ExpiredItem', () => inContentChrome(inStoryKnob(ExpiredItem, {
    data: {
      title: 'I am expired',
      entitlement
    }
  })))
  .add('XSellProductCard', () => inContentChrome(inStoryKnob(XSellProductCard, {
    productData: {
      productTitle: 'Confluence',
      tag: 'Collaborate',
      description:
        'Give your team one place to share, find, and collaborate on information they need to get work done.',
      link: 'https://www.atlassian.com/software/confluence',
      price: 10,
      currencyCode: 'usd',
      pricingLink: 'https://www.atlassian.com/software/confluence'
    },
    productKey: 'confluence.ondemand',
    onCTAClick: action('CTA clicked'),
    onDiscoverApplicationsButtonClick: action('Discover Applications Button clicked')
  })));
