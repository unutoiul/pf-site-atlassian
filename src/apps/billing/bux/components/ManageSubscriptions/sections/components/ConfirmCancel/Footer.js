import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { ModalFooter } from '@atlaskit/modal-dialog';
import { Trans } from '@lingui/react';
import cx from 'classnames';

import { Analytics, withAnalytics } from 'bux/components/Analytics';
import config from 'bux/common/config';
import { redirect } from 'bux/common/helpers/browser';

import Button, { SubmitButton } from '../../../../Button';
import LogScope from '../../../../log/Scope/Scope';
import style from './style.less';

export class FooterWithoutAnalytics extends Component {
  static propTypes = {
    onClose: PropTypes.func.isRequired,
    onConfirm: PropTypes.func.isRequired,
    analytics: PropTypes.object.isRequired,
  };

  state = { processing: false };

  handleConfirmClick = () => {
    const { onConfirm, onClose } = this.props;
    this.setState({ processing: true }, () => {
      onConfirm()
        .then(() => {
          this.props.analytics.sendTrackEvent({
            action: 'cancel',
            actionSubject: 'subscription',
            actionSubjectId: 'subscriptionCancel'
          });
          redirect(config.macUrl);
          onClose();
        })
        .catch(() => this.setState({ processing: false }));
    });
  };

  render() {
    const { onClose } = this.props;
    const { processing } = this.state;
    return (
      <ModalFooter>
        <div className={cx('redirectCancelModal', style.actionButtons)}>
          <LogScope prefix="confirm">
            <Analytics.UI as="cancelSubscriptionButton">
              <SubmitButton
                onClick={this.handleConfirmClick}
                className="button-submit"
                spinning={processing}
                submit
                primary
              >
                <Trans id="billing.manage-subscriptions.confirm-cancel.confirm">
                  Cancel subscription
                </Trans>
              </SubmitButton>
            </Analytics.UI>
          </LogScope>
          <Analytics.UI as="closeButton">
            <Button className="button-cancel" onClick={onClose}>
              <Trans id="billing.manage-subscriptions.confirm-cancel.close">
                Close
              </Trans>
            </Button>
          </Analytics.UI>
        </div>
      </ModalFooter>
    );
  }
}

export default withAnalytics(FooterWithoutAnalytics);