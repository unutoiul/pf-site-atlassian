import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Money from 'bux/components/Money';
import BillingInfo from '../BillingInfo';

describe('SubscriptionItem.Billing component', () => {
  describe('BillingInfo', () => {
    it('should indicate cost', () => {
      const number = 825;
      const wrapper = shallow(<BillingInfo amountToDisplay={number} currency="BACKS" />);
      expect(wrapper).not.to.contain.text('Free');
      expect(wrapper.find(Money).props().amount).to.be.equal(825);
      expect(wrapper.find(Money).props().currency).to.be.equal('BACKS');
    });

    it('should indicate free cost', () => {
      const number = 825;
      const wrapper = shallow(<BillingInfo amountToDisplay={number} currency="BACKS" displayAs="free" />);
      expect(wrapper).to.contain.text('Free');
      expect(wrapper.find(Money)).not.to.present();
    });

    it('should indicate no cost', () => {
      const number = 825;
      const wrapper = shallow(<BillingInfo amountToDisplay={number} currency="BACKS" displayAs="undefined" />);
      expect(wrapper).to.contain.text('–');
      expect(wrapper.find(Money)).not.to.present();
    });
  });
});
