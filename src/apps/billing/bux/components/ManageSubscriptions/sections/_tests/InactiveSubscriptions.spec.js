import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import identityFn from 'bux/common/helpers/identity';
import { ExternalLinkTo } from 'bux/components/Link';
import { SubmitButton } from '../../../Button';
import InactiveSubscriptions from '../InactiveSubscriptions';
import ExpiredItem from '../components/ExpiredItem';

describe('SuspendedProducts', () => {
  it('should render entitlements', () => {
    const section = {
      items: [{
        title: 'item',
        entitlement: {}
      }, {
        title: 'item',
        entitlement: {}
      }]
    };
    const wrapper = shallow(<InactiveSubscriptions section={section} currency="USD" />);
    expect(wrapper.find(ExpiredItem)).to.have.length(2);
    expect(wrapper.find(ExpiredItem).at(0)).to.have.prop('data', section.items[0]);
  });

  it('should contain link to resubscribe', () => {
    const wrapper = shallow(<InactiveSubscriptions section={{ items: [] }} currency="USD" />);
    expect(wrapper.find(ExternalLinkTo.BoldSupport)).to.be.present();
  });

  it('should contain button to resubscribe if action is defined', () => {
    const wrapper = shallow(<InactiveSubscriptions
      section={{ items: [] }}
      currency="USD"
      resubscribeAction={identityFn}
    />);
    const button = wrapper.find(SubmitButton);
    expect(button).to.be.present();
    expect(button).to.not.have.prop('spinning');
  });

  it('should contain button spinning to resubscribe if action is defined and resubscribing', () => {
    const wrapper = shallow(<InactiveSubscriptions
      section={{ items: [] }}
      currency="USD"
      resubscribeAction={identityFn}
      resubscribing
    />);
    const button = wrapper.find(SubmitButton);
    expect(button).to.be.present();
    expect(button).to.have.prop('spinning');
  });
});
