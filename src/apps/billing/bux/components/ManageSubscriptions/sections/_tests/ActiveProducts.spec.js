import React from 'react';
import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import Button from '../../../Button';
import ActiveProducts from '../ActiveProducts';
import SubscriptionItem from '../components/SubscriptionItem';

describe('ActiveProducts', () => {
  it('should render entitlements', () => {
    const section = {
      items: [{
        title: 'item',
        entitlement: {}
      }, {
        title: 'item',
        entitlement: {}
      }]
    };
    const wrapper = shallow(<ActiveProducts section={section} currency="USD" cancelSubscription={() => {}} />);
    expect(wrapper.find(SubscriptionItem)).to.have.length(2);
    expect(wrapper.find(SubscriptionItem).at(0)).to.have.prop('data', section.items[0]);
  });

  it('should contain button to cancel subscription', () => {
    const wrapper = mount(<ActiveProducts section={{ items: [] }} currency="USD" cancelSubscription={() => {}} />);
    expect(wrapper.find(Button)).to.contain.text('Cancel subscription');
  });

  it('should not contain button to cancel subscription for organization', () => {
    const wrapper = shallow(<ActiveProducts
      section={{ items: [] }}
      currency="USD"
      cancelSubscription={() => {}}
      isOrganization
    />);
    expect(wrapper.find(Button).exists()).to.be.false();
  });
});
