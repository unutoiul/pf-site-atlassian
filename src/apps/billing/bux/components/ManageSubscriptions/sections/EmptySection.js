import React from 'react';
import cx from 'classnames';
import { Trans } from '@lingui/react';
import { ExternalLinkTo } from 'bux/components/Link';
import styles from './style.less';

const NoSubscriptionMessage = () => (
  <div className={cx(styles.subscriptionsList)}>
    <p>
      <Trans id="billing.manage-subscriptions.empty-section">
        Sorry, we couldn't find any subscriptions for you. Please check back again soon or
        contact <ExternalLinkTo.Support>customer support</ExternalLinkTo.Support>.
      </Trans>
    </p>
  </div>
);

export default NoSubscriptionMessage;
