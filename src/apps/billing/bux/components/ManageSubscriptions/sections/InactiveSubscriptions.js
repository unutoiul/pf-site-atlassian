import { Trans } from '@lingui/react';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import cx from 'classnames';
import { resubscribe } from 'bux/core/state/resubscribe/actions';
import { SubmitButton } from 'bux/components/Button';
import { ExternalLinkTo } from 'bux/components/Link';
import LogScope from 'bux/components/log/Scope/index';
import ExpiredItem from './components/ExpiredItem';
import SubscriptionPropTypes from '../PropTypes';
import styles from './style.less';

const InactiveSubscriptions = ({
  section, currency, resubscribeAction, resubscribing
}) => (
  <div className={cx(styles.subscriptionsList, 'section-inactive')}>
    <h2 className={styles.sectionLabel}>
      <Trans id="billing.manage-subscriptions.inactive-subscriptions.header">
        Inactive subscriptions
      </Trans>
    </h2>
    <ul className={styles.productList}>
      {
        section.items.map((item, sectionIndex) => (
          <ExpiredItem
            key={sectionIndex}
            data={item}
            currency={currency}
          />
        ))
      }
    </ul>

    <LogScope className={styles.resubscribe} prefix="Resubscribe">
      {resubscribeAction
        ? (
          <SubmitButton
            submit
            onClick={resubscribeAction}
            spinning={resubscribing}
            disabled={resubscribing}
          >
            <Trans id="billing.manage-subscriptions.inactive-subscriptions.resubscribe">
              Resubscribe
            </Trans>
          </SubmitButton>
        )
        :
        <span>
          <Trans id="billing.manage-subscriptions.inactive-subscriptions.resubscribe-contact">
            If you would like to resubscribe, please contact{' '}
            <ExternalLinkTo.BoldSupport>customer support</ExternalLinkTo.BoldSupport>
          </Trans>
        </span>
      }
    </LogScope>

  </div>
);

InactiveSubscriptions.propTypes = {
  section: SubscriptionPropTypes.subscription.isRequired,
  currency: PropTypes.string.isRequired,
  resubscribeAction: PropTypes.func,
  resubscribing: PropTypes.bool
};

class StatefulInactiveSubscriptions extends Component {
  static propTypes = {
    section: SubscriptionPropTypes.subscription.isRequired,
    currency: PropTypes.string.isRequired,
    resubscribeAction: PropTypes.func.isRequired,
  };

  state = { spinning: false };
  onClick = () => {
    this.setState({ spinning: true }, () => {
      const productKeys = this.props.section.items.map(item => item.entitlement.productKey);
      this.props.resubscribeAction(productKeys)
        // on success the page is reloaded
        .catch(() => this.setState({ spinning: false }));
    });
  };

  render() {
    return (<InactiveSubscriptions
      section={this.props.section}
      currency={this.props.currency}
      resubscribeAction={this.onClick}
      resubscribing={this.state.spinning}
    />);
  }
}

const mapDispatchToProps = dispatch => ({
  resubscribeAction: productKeys => dispatch(resubscribe(productKeys))
});

export const ConnectedInactiveSubscriptions = connect(
  null,
  mapDispatchToProps
)(StatefulInactiveSubscriptions);


export default InactiveSubscriptions;
