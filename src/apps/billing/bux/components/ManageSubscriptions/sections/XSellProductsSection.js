import React, { Component, Fragment } from 'react';
import { record } from 'bux/common/helpers/logger';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { push as redirect } from 'bux/core/state/router/actions';
import { getEntitlementGroup } from 'bux/core/state/entitlement-group/actions';
import productsData from 'bux/common/data/products';
import {
  getAllRecommendedProducts,
  getRecommendedProductsMetadata,
  isAccountExpired
} from 'bux/core/state/recommended-products/selectors';
import { getCloudId } from 'bux/core/state/meta/selectors';
import XSellProductCard from 'bux/components/ManageSubscriptions/sections/components/XSellProductCard';
import ProductXflow from 'bux/features/DiscoverNewProducts/components/ProductCard/ProductXflow';
import { shouldGrantAccessEnabled } from 'bux/features/DiscoverNewProducts/components/ProductCard/ProductCard';
import { Analytics } from 'bux/components/Analytics';
import { withAnalyticsClient } from 'common/analytics';
import { manageSubscriptionsXSellFeatureExposed } from 'common/analytics/analytics-event-data';
import Loading from 'bux/components/Loading/delayed';

const CONFLUENCE_PRODUCT_KEY = 'confluence.ondemand';

class XSellProductSection extends Component {
  static propTypes = {
    reloadEntitlementGroups: PropTypes.func,
    recommendedProducts: PropTypes.array,
    activeSubscriptionsData: PropTypes.object,
    history: PropTypes.object,
    cloudId: PropTypes.string,
    isExpired: PropTypes.bool,
    redirect: PropTypes.func,
    cohort: PropTypes.string,
    instanceHasConfluence: PropTypes.bool,
    analyticsClient: PropTypes.object,
    metadata: PropTypes.object,
  };

  state = {
    triggerXflow: false
  }

  componentDidMount() {
    const { cohort, instanceHasConfluence } = this.props;
    if (!instanceHasConfluence) {
      this.triggerAnalytics('grow0.experiment.pc8111.manageSubscriptions.experiment.triggered', {
        isShown: cohort === 'variation',
      });

      const e = manageSubscriptionsXSellFeatureExposed('manageSubscriptionsScreen', {
        __cohortId: cohort,
        pcTicket: 'PC-8111',
        tags: ['PC-8111', 'expanders', cohort],
        flagKey: 'growth.manage.subscriptions.xsell.experiment',
        reason: 'N/A',
        ruleId: 'N/A',
        value: cohort
      });
      this.props.analyticsClient.sendTrackEvent(e);
    }
  }

  triggerAnalytics = (event, props = {}) => {
    const { activeSubscriptionsData = { items: [] }, cohort } = this.props;
    record(event, {
      __cohortId: cohort,
      currentProducts: activeSubscriptionsData.items.map(item => item.entitlement.productKey),
      ...props
    });
  }

  handleDiscoverApplicationsButtonClick = () => {
    this.props.redirect('/addapplication');
  }

  handleCTAClick = () => {
    this.setState({
      triggerXflow: true
    });
  }

  handleOnComplete = () => {
    this.setState({
      triggerXflow: false
    });
    this.props.reloadEntitlementGroups();
    // TODO: Remove this after AK-4611 is merged. This fixes the scroll lock problem after closing xflow modal dialog.
    setTimeout(() => {
      document.body.style.overflow = '';
    }, 1000);
  }

  render() {
    const { triggerXflow } = this.state;
    const {
      recommendedProducts,
      isExpired,
      cohort,
      instanceHasConfluence,
      metadata,
    } = this.props;

    const productKey = CONFLUENCE_PRODUCT_KEY;

    const isInVariationCohort = cohort === 'variation';

    if (instanceHasConfluence || !isInVariationCohort) {
      return null;
    }

    if (!metadata.display && metadata.loading) {
      return <Loading />;
    }

    const confluenceRecommendedProductData = recommendedProducts
      .find(recommendedProduct => recommendedProduct.productKey === productKey);

    if (!confluenceRecommendedProductData) {
      return null;
    }

    return (
      <Analytics.Screen name={`${productKey}XSellProductCardScreen`}>
        <Analytics.Context.PropagatedProvider
          value={{
            objectId: productKey,
            objectType: 'XSellProductCard',
            source: 'billingApplications'
          }}
        >
          <Fragment>
            <XSellProductCard
              productKey={productKey}
              productData={{
                ...productsData[productKey],
                ...confluenceRecommendedProductData
              }}
              isExpired={isExpired}
              onCTAClick={this.handleCTAClick}
              onDiscoverApplicationsButtonClick={this.handleDiscoverApplicationsButtonClick}
              onTriggerAnalytics={this.triggerAnalytics}
            />
            {
              triggerXflow &&
              <ProductXflow
                sourceContext="managesubscriptions-page"
                sourceComponent="BUX#managesubscriptions>XSellProductCard"
                productKey={productKey}
                onComplete={this.handleOnComplete}
                grantAccessEnabled={shouldGrantAccessEnabled.includes(productKey)}
              />
            }
          </Fragment>
        </Analytics.Context.PropagatedProvider>
      </Analytics.Screen>
    );
  }
}


const mapStateToProps = state => ({
  cloudId: getCloudId(state),
  recommendedProducts: getAllRecommendedProducts(state),
  metadata: getRecommendedProductsMetadata(state),
  isExpired: isAccountExpired(state),
});

const mapDispatchToProps = {
  reloadEntitlementGroups: getEntitlementGroup,
  redirect
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(withAnalyticsClient(XSellProductSection)));
