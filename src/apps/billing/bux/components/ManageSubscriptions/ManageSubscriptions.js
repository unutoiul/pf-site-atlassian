import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';
import { Plural, Trans } from '@lingui/react';
import SectionMessage from '@atlaskit/section-message';
import { FormattedDate } from 'bux/components/Date';
import billEstimate from 'bux/features/bill-estimate';
import { FeatureFlagged } from 'bux/components/FeatureFlags';
import { FeatureFlagsContext } from 'bux/components/FeatureFlags/FeatureFlagProvider';
import { Analytics } from 'bux/components/Analytics';

import ContentAsideLayout from 'bux/components/layout/ContentAsideLayout';
import LogScope from 'bux/components/log/Scope/index';
import NoSubscriptionMessage from 'bux/components/ManageSubscriptions/sections/EmptySection';
import XSellProductsSection from 'bux/components/ManageSubscriptions/sections/XSellProductsSection';
import ActiveSection from 'bux/components/ManageSubscriptions/sections/ActiveProducts';
import ManagementWall from 'bux/components/Page/ManagementWall';
import InactiveSubscriptionSection, { ConnectedInactiveSubscriptions }
  from 'bux/components/ManageSubscriptions/sections/InactiveSubscriptions';
import MessageBox from 'bux/components/MessageBox/index';
import Link, { ExternalLink, ExternalLinkTo } from 'bux/components/Link';
import TrelloLogo from 'bux/static/Trello-icon-blue.svgx';
import resizeSVG from 'bux/common/helpers/resizeSVG';
import Money from 'bux/components/Money';
import CurrencyUpdateNotification from 'bux/components/CurrencyUpdateNotification';
import SubscriptionPropTypes from './PropTypes';
import styles from './sections/style.less';

const ResizedTrelloLogo = resizeSVG(TrelloLogo, 20);

const LearnMoreLink = () => (
  <ExternalLink
    href="https://confluence.atlassian.com/x/-fOEO#PricingandbillingforAtlassianAccess-trello_billing"
    logPrefix="learn-more"
  >
    <Trans id="billing.manage-subscriptions.learn-more">Learn More</Trans>
  </ExternalLink>
);

const BillingFaqLink = ({ children }) => (
  <ExternalLink
    href="https://confluence.atlassian.com/cloud/atlassian-account-for-users-873871199.html"
    logPrefix="billable-accounts"
  >
    {children}
  </ExternalLink>
);
BillingFaqLink.propTypes = {
  children: PropTypes.node
};

export const GrandfatheringInfo = ({ grandfathering, isAnnual, currency }) => {
  if (!grandfathering) return null;

  const { amountBeforeTax, period = {} } = grandfathering;
  const { endDate } = period;

  if (endDate == null || amountBeforeTax == null) return null;

  const moneyBeforeTax = <Money amount={amountBeforeTax} currency={currency} />;
  const shortEndDate = <FormattedDate value={endDate} format={{ month: 'long', day: 'numeric' }} />;
  const longEndDate = <FormattedDate value={endDate} format={{ month: 'long', day: 'numeric', year: 'numeric' }} />;

  return (
    <MessageBox dataTest="grandfathering-info">
      <h3>
        <Trans id="billing.manage-subscriptions.atlassian-access.grandfathering.header">
          Your free early access period ends {shortEndDate}
        </Trans>
      </h3>
      <p>
        <Trans id="billing.manage-subscriptions.atlassian-access.grandfathering.paid">
          Atlassian Access will become a paid subscription on <strong>{longEndDate}</strong>.
        </Trans>
        {' '}
        { isAnnual ?
          <Trans id="billing.manage-subscriptions.atlassian-access.grandfathering.bill-estimation.annual">
            Based on your current <BillingFaqLink>billable accounts</BillingFaqLink>,
            your first Atlassian Access bill will be <strong>{moneyBeforeTax} per year</strong>.
          </Trans>
          :
          <Trans id="billing.manage-subscriptions.atlassian-access.grandfathering.bill-estimation.monthly">
            Based on your current <BillingFaqLink>billable accounts</BillingFaqLink>,
            your first Atlassian Access bill will be <strong>{moneyBeforeTax} per month</strong>.
          </Trans>
        }
      </p>
      <p>
        <Trans id="billing.manage-subscriptions.atlassian-access.grandfathering.ensure-payment-details">
          <Link to="/paymentdetails" logPrefix="enter-payment-details">
            <Trans id="billing.manage-subscriptions.ensure-payment-details">Ensure you have payment details</Trans>
          </Link>{' '}
          before <strong>{shortEndDate}</strong> to avoid any interruption
          to your Atlassian Access service.
        </Trans>
      </p>
    </MessageBox>
  );
};
GrandfatheringInfo.propTypes = {
  grandfathering: PropTypes.object,
  isAnnual: PropTypes.bool,
  currency: PropTypes.string,
};

const TrelloMessage = ({ children }) => (
  <MessageBox warn dataTest="trello-warn">
    <ResizedTrelloLogo />
    <h3>
      <Trans id="billing.manage-subscriptions.atlassian-access.trello.header">
        Coming soon - Atlassian Access will include your Trello users
      </Trans>
    </h3>
    {children}
  </MessageBox>
);
TrelloMessage.propTypes = {
  children: PropTypes.node
};

const TrelloAccountsInfo = ({ additionalUnitCount }) => (
  <p>
    <Trans id="billing.manage-subscriptions.atlassian-access.trello.account-info">
      Once support for Trello is added to Atlassian Access, there will be an additional{' '}
      <strong><Plural value={additionalUnitCount} one="# managed account" other="# managed accounts" /></strong>{' '}
      that will be added to your organization.
    </Trans>
  </p>
);
TrelloAccountsInfo.propTypes = {
  additionalUnitCount: PropTypes.number
};

const belowAnnualTierTrelloMessage = ({ additionalUnitCount }) => (
  <TrelloMessage>
    <TrelloAccountsInfo additionalUnitCount={additionalUnitCount} />
    <p>
      <Trans id="billing.manage-subscriptions.atlassian-access.trello.bellow-annual">
        Your new total <BillingFaqLink>billable user count</BillingFaqLink> does not exceed your
        current annual user tier. You will not need to upgrade your subscription.
      </Trans>
      {' '}
      <LearnMoreLink />
    </p>
  </TrelloMessage>
);

belowAnnualTierTrelloMessage.propTypes = {
  additionalUnitCount: PropTypes.number
};

const aboveAnnualTierTrelloMessage = ({
  additionalUnitCount, amountBeforeTax, trelloTotalUnitCount, currency
}) => (
  <TrelloMessage>
    <TrelloAccountsInfo additionalUnitCount={additionalUnitCount} />
    <p>
      <Trans id="billing.manage-subscriptions.atlassian-access.trello.above-annual">
        Your new total <BillingFaqLink>billable user count</BillingFaqLink> exceeds your current licensed user limit.
        You will need to upgrade to the {trelloTotalUnitCount} user tier to support the additional users.
        On this new tier, your new bill will be{' '}
        <strong><Money amount={amountBeforeTax} currency={currency} /> per year</strong>.
        To upgrade, please contact us via our{' '}
        <ExternalLinkTo.PurchasingLicense>
          contact us
        </ExternalLinkTo.PurchasingLicense>{' '}
        form.
      </Trans>
      {' '}
      <LearnMoreLink />
    </p>
  </TrelloMessage>
);
aboveAnnualTierTrelloMessage.propTypes = {
  additionalUnitCount: PropTypes.number,
  amountBeforeTax: PropTypes.number,
  trelloTotalUnitCount: PropTypes.number,
  currency: PropTypes.string,
};

const belowMonthlyTrelloMessage = ({ additionalUnitCount }) => (
  <TrelloMessage>
    <TrelloAccountsInfo additionalUnitCount={additionalUnitCount} />
    <p>
      <Trans id="billing.manage-subscriptions.atlassian-access.trello.belowMonthly">
        Your new total <BillingFaqLink>billable accounts</BillingFaqLink> do not exceed your current user tier.
        Based on your current usage, there will be no additional charges for these accounts.
      </Trans>
      {' '}
      <LearnMoreLink />
    </p>
  </TrelloMessage>
);
belowMonthlyTrelloMessage.propTypes = {
  additionalUnitCount: PropTypes.number,
};

const aboveMonthlyTrelloMessage = ({ additionalUnitCount, amountBeforeTax, currency }) => (
  <TrelloMessage>
    <p>
      <Trans id="billing.manage-subscriptions.atlassian-access.trello.account-info.increased-bill">
        Once support for Trello is added to Atlassian Access, there will be an additional{' '}
        <strong><Plural value={additionalUnitCount} one="# managed account" other="# managed accounts" /></strong>{' '}
        that will be added to your organization and your bill will increase to{' '}
        <strong><Money amount={amountBeforeTax} currency={currency} /> per month</strong>.
      </Trans>
    </p>
    <p>
      <Trans id="billing.manage-subscriptions.atlassian-access.trello.account-info.aboveMonthly">
        We'll give you at least <strong>30 days</strong> notice before these changes take effect so you can manage
        how these users will impact your bill.
      </Trans>
      {' '}
      <LearnMoreLink />
    </p>
  </TrelloMessage>
);
aboveMonthlyTrelloMessage.propTypes = {
  additionalUnitCount: PropTypes.number,
  amountBeforeTax: PropTypes.number,
  currency: PropTypes.string,
};

export const TrelloWarn = ({
  trello, isAnnual, atlassianAccessTotalUnitCount, currency
}) => {
  if (!trello) return null;

  const { unitCount: trelloTotalUnitCount, amountBeforeTax, additionalUnitCount } = trello;
  const isAboveTierThreshold = trelloTotalUnitCount > atlassianAccessTotalUnitCount;

  if (additionalUnitCount == null || trelloTotalUnitCount == null || amountBeforeTax == null) return null;

  if (additionalUnitCount <= 0) return null;

  if (isAnnual) {
    if (isAboveTierThreshold) {
      return aboveAnnualTierTrelloMessage({
        additionalUnitCount, amountBeforeTax, trelloTotalUnitCount, currency
      });
    }

    return belowAnnualTierTrelloMessage({ additionalUnitCount });
  }

  if (isAboveTierThreshold) {
    return aboveMonthlyTrelloMessage({ additionalUnitCount, amountBeforeTax, currency });
  }

  return belowMonthlyTrelloMessage({ additionalUnitCount });
};
TrelloWarn.propTypes = {
  trello: PropTypes.object,
  isAnnual: PropTypes.bool,
  isAboveTierThreshold: PropTypes.bool,
  currency: PropTypes.string,
};

const DiscoverMoreProductsTip = () => (
  <div className={cx(styles.noticeRow, 'test-add-more-applications')}>
    <SectionMessage appearance="info">
      <Analytics.UI attributes={{ linkContainer: 'banner' }} as="addMoreAtlassianProductsLink">
        <Trans id="billing.manage-subscriptions.discover-more-products-tip">
          Want to maximize your team's collaboration?{' '}
          <Link name="to-discover-apps" to="/addapplication">Add more Atlassian products</Link>.
        </Trans>
      </Analytics.UI>
    </SectionMessage>
  </div>
);

const MANAGE_SUBSCRIPTIONS_XSELL_FF = 'growth.manage.subscriptions.xsell.experiment';

const instanceHasConfluence = activeSection =>
  activeSection && activeSection.items.find(item =>
    item.entitlement.productKey === 'confluence.ondemand');

// extracted for testing only
export const Sections =
  ({
    sections,
    currency,
    isAnnual,
    isOrganization,
    cancelSubscription,
    hasAtlassianAccess,
    atlassianAccessExtras,
    atlassianAccessTotalUnitCount,
    freeLicense,
    upcomingCurrencyExtra,
    isNextBillInNewCurrency,
  }) => (
    <div>
      {!isOrganization &&
      <FeatureFlagsContext.Consumer>
        {ff => (
          ff && ff[MANAGE_SUBSCRIPTIONS_XSELL_FF] === 'variation' &&
            !instanceHasConfluence(sections.active)
            ? null
            : <DiscoverMoreProductsTip />
        )}
      </FeatureFlagsContext.Consumer>}
      {
      <FeatureFlagged flags={['2018-upcoming-currency-notice']}>
        {
          upcomingCurrencyExtra &&
          <div className={styles.noticesRow} data-test="currency-update-notices-row">
            <CurrencyUpdateNotification
              upcomingCurrencyExtra={upcomingCurrencyExtra}
              isNextBillInNewCurrency={isNextBillInNewCurrency}
            />
          </div>
        }
      </FeatureFlagged>
      }
      {(!sections.active && !sections.suspended && !sections.unsubscribed) && <NoSubscriptionMessage />}
      {sections.active &&
      <ActiveSection
        section={sections.active}
        currency={currency}
        isAnnual={isAnnual}
        isOrganization={isOrganization}
        cancelSubscription={cancelSubscription}
      />}
      {sections.suspended && <InactiveSubscriptionSection section={sections.suspended} currency={currency} />}
      {sections.unsubscribed && <ConnectedInactiveSubscriptions
        section={sections.unsubscribed}
        currency={currency}
      />}
      {!freeLicense && [
        <FeatureFlagged key="grandfathering" flags={['atlassian-access-warnings']}>
          {hasAtlassianAccess &&
            <GrandfatheringInfo {...atlassianAccessExtras} isAnnual={isAnnual} currency={currency} />
          }
        </FeatureFlagged>,
        <FeatureFlagged key="trello" flags={['atlassian-access-warnings']}>
          {hasAtlassianAccess &&
          <TrelloWarn
            {...atlassianAccessExtras}
            atlassianAccessTotalUnitCount={atlassianAccessTotalUnitCount}
            isAnnual={isAnnual}
            currency={currency}
          />
          }
        </FeatureFlagged>
       ]}
      { isAnnual &&
        <div className={cx(styles.finePrint, 'annual-payment-renewal')}>
          <Trans id="billing.manage-subscriptions.annual-payment-renewal.label">
            We will send you a renewal quote 60 days in advance for payment
          </Trans>
        </div>
      }
      {!isOrganization &&
        <FeatureFlagsContext.Consumer>
        {ff => (ff && ff[MANAGE_SUBSCRIPTIONS_XSELL_FF] !== 'not-enrolled' &&
          <XSellProductsSection
            cohort={ff[MANAGE_SUBSCRIPTIONS_XSELL_FF]}
            instanceHasConfluence={instanceHasConfluence(sections.active)}
            activeSubscriptionsData={sections.active}
          />
        )}
      </FeatureFlagsContext.Consumer>}
    </div>
  );

Sections.propTypes = {
  sections: PropTypes.objectOf(SubscriptionPropTypes.subscription).isRequired,
  currency: PropTypes.string.isRequired,
  cancelSubscription: PropTypes.func.isRequired,
  isAnnual: PropTypes.bool,
  isOrganization: PropTypes.bool,
  freeLicense: PropTypes.bool,
  hasAtlassianAccess: PropTypes.bool,
  atlassianAccessExtras: PropTypes.object,
  atlassianAccessTotalUnitCount: PropTypes.number,
  isNextBillInNewCurrency: PropTypes.bool,
  upcomingCurrencyExtra: PropTypes.object,
};


const ManageSubscriptions = props => (
  <ContentAsideLayout
    content={<Sections {...props} />}
    aside={(
      <ManagementWall wall={null}>
        <LogScope prefix="billEstimate">
          <billEstimate.components.BillEstimateSummary />
        </LogScope>
      </ManagementWall>
    )}
  />
);

ManageSubscriptions.propTypes = {
  sections: PropTypes.objectOf(SubscriptionPropTypes.subscription).isRequired,
  currency: PropTypes.string.isRequired,
  cancelSubscription: PropTypes.func.isRequired,
  isAnnual: PropTypes.bool,
  isOrganization: PropTypes.bool,
  freeLicense: PropTypes.bool,
  hasAtlassianAccess: PropTypes.bool,
  atlassianAccessExtras: PropTypes.object,
  atlassianAccessTotalUnitCount: PropTypes.number,
  isNextBillInNewCurrency: PropTypes.bool,
  upcomingCurrencyExtra: PropTypes.object,
};

export default ManageSubscriptions;
