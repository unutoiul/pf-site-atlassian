import PropTypes from 'prop-types';

const actions = PropTypes.arrayOf(PropTypes.any);
const entitlement = PropTypes.object;
const billingRecord = PropTypes.object;

const subscription = PropTypes.shape({
  title: PropTypes.string,
  entitlement,
  billingRecord
});
const subscriptionSection = PropTypes.shape({
  items: PropTypes.arrayOf(subscription)
});

const SubscriptionPropTypes = {
  billingRecord,
  entitlement,
  subscriptionSection,
  subscription,
  actions
};

export default SubscriptionPropTypes;