import { storiesOf } from '@storybook/react';
import { inContentChrome, inStoryKnob } from 'bux/common/helpers/storybook';

import { Sections } from './ManageSubscriptions';

const entitlement = {
  name: 'Product',
  creationDate: '2016-12-05',
  entitlementGroupId: '4d854acf-e651-4a4b-bdd0-d0c76f0995ed',
  trialEndDate: '2032-12-17',
  accountId: '363bf5a5-47d7-42f9-9c51-930c40d6b3b9',
  status: 'ACTIVE',
  endDate: '2016-12-17',
  productKey: 'jira-core.ondemand',
  startDate: '2016-12-05',
  id: 'testcase',
  sen: 'A813576'
};

const billingRecord = {
  usage: [
    {
      taxRate: 10,
      amountBeforeTax: 0,
      description: 'Product Usage',
      units: 1,
      unitCode: 'products',
      amountWithTax: 0,
      taxAmount: 0
    },
    {
      taxRate: 10,
      amountBeforeTax: 0,
      description: 'Another Usage',
      units: 10,
      unitCode: 'products',
      amountWithTax: 0,
      taxAmount: 0
    }
  ],
  entitlementId: 'testCase',
  amountWithTax: 11,
  taxAmount: 1,
  amountBeforeTax: 10,
  productKey: 'jira-core.ondemand',
  productName: 'Product'
};

const tieredBillingRecord = {
  usage: [
    {
      taxRate: 10,
      amountBeforeTax: 0,
      description: 'Product Usage',
      units: 20,
      currentUnits: 19,
      tiered: true,
      unitCode: 'users',
      amountWithTax: 0,
      taxAmount: 0
    }
  ],
  entitlementId: 'testCase',
  amountWithTax: 11,
  taxAmount: 1,
  amountBeforeTax: 10,
  productKey: 'jira-core.ondemand',
  productName: 'Product'
};

const item1 = {
  title: 'Jira',
  entitlement,
  billingRecord
};

const item2 = {
  title: 'Jira',
  entitlement,
  billingRecord: tieredBillingRecord
};

storiesOf('BUX|Components/ManageSubscriptions', module)
  .add('SubscriptionItem', () => inContentChrome(inStoryKnob(Sections, {
    currency: 'usd',
    sections: {
      active: {
        items: [item1, item2]
      },
      suspended: {
        items: [item1]
      },
      unsubscribed: {
        items: [item1]
      }
    },
    isOrganization: false,
    isAnnual: false,
  })));
