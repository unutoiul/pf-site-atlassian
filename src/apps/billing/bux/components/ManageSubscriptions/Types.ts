import { BillEstimateLineType } from 'bux/core/state/bill-estimate/types';
import { EntitlementEditionType, EntitlementType } from 'bux/core/state/entitlement-group/types';

export type Actions = any[];

export interface SubscriptionType {
  title: string;
  entitlement: EntitlementType;
  billingRecord: BillEstimateLineType;
  edition: EntitlementEditionType;
}

export interface SubscriptionSectionType {
  [key: string]: {items: SubscriptionType[]};
}
