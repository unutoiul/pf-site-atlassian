import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import { Provider } from 'bux/helpers/redux';
import billEstimate from 'bux/features/bill-estimate';
import FeatureFlagProvider from 'bux/components/FeatureFlags/FeatureFlagProvider';
import ContentAsideLayout from '../../layout/ContentAsideLayout';
import Button from '../../Button';
import SubscriptionItem from '../sections/components/SubscriptionItem';
import ActiveSection from '../sections/ActiveProducts';
import InactiveSubscriptions, { ConnectedInactiveSubscriptions } from '../sections/InactiveSubscriptions';
import NoSubscriptionMessage from '../sections/EmptySection';
import ManageSubscriptions, { Sections, GrandfatheringInfo, TrelloWarn } from '../ManageSubscriptions';


describe('ManageSubscriptions component', () => {
  const section = {
    items: [{
      title: 'item',
      entitlement: {
        endDate: '2016-12-17'
      },
      billingRecord: {
        amountBeforeTax: 42
      }
    }]
  };

  const shallowToGrid = Component => mount(<Provider>{Component}</Provider>).find(ContentAsideLayout);//.dive();
  const getSections = wrapper => wrapper.find(Sections);
  const getActiveSection = wrapper => getSections(wrapper).find(ActiveSection);

  it('should have billEstimateComponent', () => {
    const wrapper = mount(<Provider>
        <ManageSubscriptions
          currency="USD"
          sections={{}}
          cancelSubscription={() => ({})}
        />
      </Provider>);
    expect(wrapper.find(billEstimate.components.BillEstimateSummary)).to.have.length(1);
  });

  it('should have a button to cancel', () => {
    const wrapper = getActiveSection(shallowToGrid(<ManageSubscriptions
      currency="USD"
      sections={{ active: section }}
      cancelSubscription={() => {
      }}
    />));
    expect(wrapper.find(Button)).to.have.length(1);
  });

  it('should not have a button to cancel for organization', () => {
    const wrapper = getActiveSection(shallowToGrid(<ManageSubscriptions
      currency="USD"
      sections={{ active: section }}
      cancelSubscription={() => {
      }}
      isOrganization
    />));
    expect(wrapper.find(Button).exists()).to.be.false();
  });

  describe('for organizations', () => {
    it('should display warnings', () => {
      const wrapper = mount(<Provider>
        <FeatureFlagProvider featureFlags={{ 'atlassian-access-warnings': true }}>
          <ManageSubscriptions
            sections={{ active: section }}
            hasAtlassianAccess
            currency="AUD"
            cancelSubscription={() => {
            }}
          />
        </FeatureFlagProvider>
      </Provider>).find(ManageSubscriptions);
      expect(wrapper.find(GrandfatheringInfo)).to.be.present();
      expect(wrapper.find(TrelloWarn)).to.be.present();
    });

    it('should not display warnings for not AA users', () => {
      const wrapper = mount(<Provider>
        <FeatureFlagProvider featureFlags={{ 'atlassian-access-warnings': true }}>
          <ManageSubscriptions
            sections={{ active: section }}
            currency="AUD"
            cancelSubscription={() => {
            }}
          />
        </FeatureFlagProvider>
      </Provider>).find(ManageSubscriptions);
      expect(wrapper.find(GrandfatheringInfo)).not.to.be.present();
      expect(wrapper.find(TrelloWarn)).not.to.be.present();
    });

    it('should not display warnings for community', () => {
      const wrapper = mount(<Provider>
        <FeatureFlagProvider featureFlags={{ 'atlassian-access-warnings': true }}>
          <ManageSubscriptions
            sections={{ active: section }}
            hasAtlassianAccess
            currency="AUD"
            freeLicense
            cancelSubscription={() => {
            }}
          />
        </FeatureFlagProvider>
      </Provider>).find(ManageSubscriptions);
      expect(wrapper.find(GrandfatheringInfo)).not.to.be.present();
      expect(wrapper.find(TrelloWarn)).not.to.be.present();
    });
  });

  describe('for sections', () => {
    it('should have info when there is no subscription item', () => {
      const sections = {};
      const wrapper = shallowToGrid(<ManageSubscriptions
        currency="USD"
        sections={sections}
        cancelSubscription={() => {
        }}
      />);
      expect(getSections(wrapper).find(ActiveSection)).to.have.length(0);
      expect(getSections(wrapper).find(InactiveSubscriptions)).to.have.length(0);
      expect(getSections(wrapper).find(ConnectedInactiveSubscriptions)).to.have.length(0);
      expect(getSections(wrapper).find(NoSubscriptionMessage)).to.have.length(1);
    });

    it('should have only active section', () => {
      const sections = { active: section };
      const wrapper = shallowToGrid(<ManageSubscriptions
        currency="USD"
        sections={sections}
        cancelSubscription={() => {
        }}
      />);
      expect(getActiveSection(wrapper).find(SubscriptionItem)).to.have.length(1);
      expect(getSections(wrapper).find(InactiveSubscriptions)).to.have.length(0);
      expect(getSections(wrapper).find(ConnectedInactiveSubscriptions)).to.have.length(0);
      expect(getSections(wrapper).find(NoSubscriptionMessage)).to.have.length(0);
    });

    it('should have only inactive subscriptions section for suspended products', () => {
      const sections = { suspended: section };
      const wrapper = shallowToGrid(<ManageSubscriptions
        currency="USD"
        sections={sections}
        cancelSubscription={() => {
        }}
      />);
      expect(getSections(wrapper).find(ActiveSection)).to.have.length(0);
      expect(getSections(wrapper).find(InactiveSubscriptions)).to.have.length(1);
      expect(getSections(wrapper).find(ConnectedInactiveSubscriptions)).to.have.length(0);
      expect(wrapper.find(NoSubscriptionMessage)).to.have.length(0);
    });

    it('should have only inactive subscriptions section for unsubscribed products', () => {
      const sections = { unsubscribed: section };
      const wrapper = shallowToGrid(<ManageSubscriptions
        currency="USD"
        sections={sections}
        cancelSubscription={() => {
        }}
      />);
      expect(getSections(wrapper).find(ActiveSection)).to.have.length(0);
      expect(getSections(wrapper).find(InactiveSubscriptions)).to.have.length(1);
      expect(getSections(wrapper).find(ConnectedInactiveSubscriptions)).to.have.length(1);
      expect(wrapper.find(NoSubscriptionMessage)).to.have.length(0);
    });

    it('should combine all sections', () => {
      const sections = { active: section, suspended: section, unsubscribed: section };
      const wrapper = shallowToGrid(<ManageSubscriptions
        currency="USD"
        sections={sections}
        cancelSubscription={() => {
        }}
      />).find(Sections);
      expect(getSections(wrapper).find(ActiveSection)).to.have.length(1);
      expect(getSections(wrapper).find(InactiveSubscriptions)).to.have.length(2);
      expect(getSections(wrapper).find(ConnectedInactiveSubscriptions)).to.have.length(1);
    });

    it('should have annual payment renewal quote if annual', () => {
      const wrapper = getSections(shallowToGrid(<ManageSubscriptions
        currency="USD"
        sections={{ active: section }}
        cancelSubscription={() => {
        }}
        isAnnual="true"
      />));
      expect(wrapper.find('.annual-payment-renewal')).to.have.length(1);
      expect(wrapper.find('.annual-payment-renewal')).to.contains
        .text('We will send you a renewal quote 60 days in advance for payment');
    });
  });
});
