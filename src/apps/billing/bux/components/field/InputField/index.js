import createInputField from './createInputField';

const ShortInputField = createInputField('short');
const MediumInputField = createInputField('medium');
const WideInputField = createInputField('wide', { fitParent: true });
const InputField = createInputField();

export { ShortInputField, MediumInputField, WideInputField };

export default InputField;