import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';
import Textfield from '@atlaskit/textfield';
import FieldBase from 'bux/components/field/FieldBase';
import ExtendedPropTypes from '../PropTypes';
import styles from '../fields.less';

const createInputField = (fieldWidth, extraProps = {}) => {
  const classes = fieldWidth ? cx('adg-fieldbase', `${fieldWidth}-bux-field`, styles[fieldWidth]) : '';

  const InputField = (props) => {
    const {
      input,
      labelText,
      type = 'text',
      placeholder,
      required,
      readonly,
      secure,
      help,
      meta: { error, invalid, touched }
    } = props;
    const { name } = input;
    const { fitParent } = extraProps;
    return (
      <FieldBase
        label={labelText}
        id={name}
        isRequired={required}
        isInvalid={touched && invalid}
        invalidMessage={error}
        isFitContainerWidthEnabled={fitParent}
        className={classes}
        help={help}
        isPaddingDisabled
      >
        <Textfield
          {...input}
          appearance="none"
          id={name}
          // Commented out to avoid in built browser validation: isRequired={required}
          isRequired={undefined}
          name={!secure ? input.name : undefined}
          type={type}
          className={cx(styles.base, styles['input-font-size'])}
          isReadOnly={readonly}
          placeholder={placeholder}
          aria-invalid={invalid}

          // disable autocomplete
          autoComplete="bux-input-field"
        /> 
      </FieldBase>
    );
  };

  InputField.propTypes = {
    input: PropTypes.object,
    placeholder: PropTypes.string,
    labelText: PropTypes.string.isRequired,
    type: PropTypes.string,
    required: PropTypes.bool,
    secure: PropTypes.bool,
    readonly: PropTypes.bool,
    help: PropTypes.string,
    meta: ExtendedPropTypes.meta
  };

  InputField.defaultProps = {
    meta: {}
  };

  return InputField;
};

export default createInputField;
