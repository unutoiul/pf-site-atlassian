import React from 'react';
import { expect } from 'chai';
import { shallow, render, mount } from 'enzyme';
import FieldBase from 'bux/components/field/FieldBase';
import Textfield from '@atlaskit/textfield';
import InputField, { ShortInputField, MediumInputField, WideInputField } from '..';

const DEFAULT_PROPS = {
  input: {
    name: 'testName'
  },
  name: 'testInput',
  labelText: '',
  required: false
};

const createInputField = options => <InputField {...DEFAULT_PROPS} {...options} />;
const createShortInputField = options => <ShortInputField {...DEFAULT_PROPS} {...options} />;
const createMediumInputField = options => <MediumInputField {...DEFAULT_PROPS} {...options} />;

const itBehavesLikeAnInputField = (fieldCreator) => {
  it('should render the label', () => {
    const wrapper = shallow(fieldCreator({ labelText: 'TestInputField' }));
    expect(wrapper.find(FieldBase).props().label).to.be.equal('TestInputField');
  });

  it('should indicate if the field is not required', () => {
    const wrapper = shallow(fieldCreator({ required: false }));
    expect(wrapper.find(FieldBase).props().isRequired).to.be.equal(false);
  });

  it('should indicate if the field is required', () => {
    const wrapper = shallow(fieldCreator({ required: true }));
    expect(wrapper.find(FieldBase).props().isRequired).to.be.equal(true);
  });

  it('should render a text input field', () => {
    // Have to use render, not shallow
    const wrapper = render(fieldCreator({ labelText: 'TestInputField' }));
    expect(wrapper.find('input[type="text"]')).to.be.present();
  });

  it('should render placeholder', () => {
    const wrapper = render(fieldCreator({ labelText: 'TestInput', placeholder: '1-2-3-4' }));
    const input = wrapper.find('input');
    expect(input).to.have.attr('placeholder', '1-2-3-4');
  });

  it('should accept the input type as a prop', () => {
    const wrapper = render(fieldCreator({ labelText: 'TestInputField', type: 'number' }));
    expect(wrapper.find('input[type="number"]')).to.be.present();
  });

  describe('when there are errors in the field', () => {
    it('should add error message, but not flag', () => {
      const wrapper = shallow(fieldCreator({ meta: { error: 'some error' } }));
      expect(wrapper.find(FieldBase).props().isInvalid).to.be.equal(undefined);
      expect(wrapper.find(FieldBase).props().invalidMessage).to.be.equal('some error');
    });

    describe('and the field is invalid', () => {
      it('should add the field-error class to the input field group', () => {
        const wrapper = mount(fieldCreator({ meta: { invalid: true, touched: true, error: 'some error' } }));
        expect(wrapper.find(FieldBase).props().isInvalid).to.be.equal(true);
        expect(wrapper.find(FieldBase).props().invalidMessage).to.be.equal('some error');
        expect(wrapper.find('input')).to.have.prop('aria-invalid');
      });
    });
  });
};

describe('InputField: ', () => {
  itBehavesLikeAnInputField(createInputField);

  it('should use name from input prop', () => {
    const wrapper = shallow(createInputField());
    const input = wrapper.find(Textfield);
    expect(input).to.have.prop('name', 'testName');
  });

  it('should not contain name for secure reasons', () => {
    const wrapper = shallow(createInputField({ secure: true }));
    const input = wrapper.find(Textfield);
    expect(input).to.not.have.prop('name');
  });
});

describe('ShortInputField: ', () => {
  itBehavesLikeAnInputField(createShortInputField);

  it('should set have the class "short-field"', () => {
    const wrapper = render(createShortInputField({ labelText: 'TestInputField', type: 'number' }));
    expect(wrapper.find('.adg-fieldbase')).to.have.className('short-bux-field');
  });
});

describe('MediumInputField: ', () => {
  itBehavesLikeAnInputField(createMediumInputField);

  it('should set have the class "medium-field"', () => {
    const wrapper = render(createMediumInputField({ labelText: 'TestInputField', type: 'number' }));
    expect(wrapper.find('.adg-fieldbase')).to.have.className('medium-bux-field');
  });
});

describe('WideInputField', () => {
  it('should be wide', () => {
    const wrapper = shallow(<WideInputField {...DEFAULT_PROPS} />);
    expect(wrapper.find(FieldBase).props().isFitContainerWidthEnabled).to.be.equal(true);
  });
  it('should set have the class "wide-field"', () => {
    const wrapper = render(<WideInputField {...DEFAULT_PROPS} />);
    expect(wrapper.find('.adg-fieldbase')).to.have.className('wide-bux-field');
  });
});
