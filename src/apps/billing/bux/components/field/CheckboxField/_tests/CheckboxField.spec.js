import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import CheckboxField from '../CheckboxField';

const DEFAULT_PROPS = {
  name: 'testInput',
  labelText: '',
  input: {}
};

const createCheckboxField = options => <CheckboxField {...DEFAULT_PROPS} {...options} />;

describe('CheckboxField component: ', () => {
  it('should render the label', () => {
    const wrapper = shallow(createCheckboxField({ labelText: 'TestCheckboxField' }));
    expect(wrapper.find('label')).to.have.text('TestCheckboxField');
  });

  it('should render a checkbox', () => {
    const wrapper = shallow(createCheckboxField({}));
    expect(wrapper.find('input[type="checkbox"]')).to.be.present();
  });

  it('should be unchecked by default', () => {
    const wrapper = shallow(createCheckboxField({}));
    expect(wrapper.find('#testInput')).not.to.be.checked();
  });
});