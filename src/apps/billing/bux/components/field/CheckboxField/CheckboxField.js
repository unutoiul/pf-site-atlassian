import PropTypes from 'prop-types';
import React from 'react';
import AKFieldBase from '@atlaskit/field-base';

const CheckboxField = ({ input = {}, name, labelText }) => (
  <AKFieldBase>
    <div className="checkbox">
      <input id={name} {...input} checked={input.value} type="checkbox" className="checkbox" />
      <label htmlFor={name}>
        {labelText}
      </label>
    </div>
  </AKFieldBase>
);

CheckboxField.propTypes = {
  input: PropTypes.object,
  name: PropTypes.string.isRequired,
  labelText: PropTypes.string.isRequired,
};

export default CheckboxField;
