import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Autocomplete from 'react-autocomplete';
import cx from 'classnames';
import Textfield from '@atlaskit/textfield'; 
import identityFn from 'bux/common/helpers/identity';
import styles from './AutocompleteHelper.less';

const matchStateToTermFabric = selector => (
  (state, value) => selector(state).toLowerCase().indexOf(value.toLowerCase()) !== -1
);

const alwaysTrueFn = () => true;

const sortStatesFabric = selector => (
  (a, b, value) => (
    selector(a).toLowerCase().indexOf(value.toLowerCase()) >
    selector(b).toLowerCase().indexOf(value.toLowerCase()) ? 1 : -1
  )
);

let counter = 0;
const getItemKey = item => (typeof item === 'object' && 'key' in item ? item.key : counter += 1);
const getSimpleValue = value => value;

// No style
const wrapperStyle = {
  width: '100%'
};

class AutocompleteFieldWrapper extends Component {
  componentWillMount() {
    window.addEventListener('scroll', this.repositeMenu, true);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.repositeMenu, true);
  }


  onDataChange = (newValue) => {
    const { input: { onChange }, onInputChange } = this.props;
    this.currentValue = newValue;
    if (onInputChange) {
      onInputChange(newValue);
    }
    if (onChange) {
      onChange(newValue);
    }
  };

  onDataSelect = (newValue) => {
    const { input: { onChange }, onInputSelect } = this.props;
    this.currentValue = null;
    if (onInputSelect) {
      if (onInputSelect(newValue) === false) {
        return;
      }
    }
    if (onChange) {
      onChange(newValue);
    }
  };

  setRef = (ref) => {
    this.autoCompleteRef = ref;
  };

  handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      setImmediate(() => {
        if (this.currentValue) {
          this.onDataSelect(this.currentValue);
        }
      });
      event.preventDefault();
    }
  };

  repositeMenu = () => {
    if (this.autoCompleteRef) {
      this.autoCompleteRef.setMenuPositions();
    }
  };

  renderMenu = (items, value, style) => (
    <div className={cx('autocomplete-menu', 'ak-field-group', styles.menu)} style={style}>
      {!!items.length && (<div className="ak-field-text">{this.props.menuWrapper(items)}</div>)}
    </div>
  );

  renderInput = (props) => {
    const {
      isCompact,
      isDisabled,
      isFocused,
      // isInvalid,
      isMonospaced,
      isReadOnly,
      // isRequired,
      onBlur,
      onFocus,
      width,
      value,
      forwardedRef,
      theme
    } = this.props;
    return (
      <Textfield 
        isCompact={isCompact}
        isDisabled={isDisabled}
        isFocused={isFocused}
        // isInvalid={isInvalid}
        isMonospaced={isMonospaced}
        isReadOnly={isReadOnly}
        // Commented out to avoid in built browser validation: isRequired={required}
        onBlur={onBlur}
        onFocus={onFocus}
        width={width}
        value={value}
        forwardedRef={forwardedRef}
        theme={theme}
        {...props}
        // Despite appearance="none" @atlaskit/textfield shows styling when something is invalid
        isInvalid={undefined}
        isRequired={undefined}
        appearance="none"
      />
    );
  };

  renderItem = (item, isHighlighted) => (
    <div
      className={cx('autocomplete-result', styles.input, { [styles.hightlighted]: isHighlighted })}
      key={getItemKey(item)}
    >{this.props.getItemValue(item)}
    </div>
  );

  render() {
    const inputProps = {
      className: this.props.className,
      type: this.props.type,
      id: this.props.id,
      name: this.props.name,
      onKeyPress: this.handleKeyPress,
      'aria-invalid': this.props.isInvalid
    };

    const getItemValue = this.props.getItemValue;
    const shouldItemRender = this.props.shouldPassAllValues ? alwaysTrueFn : matchStateToTermFabric(getItemValue);

    return (<Autocomplete
      items={[]}
      shouldItemRender={shouldItemRender}
      sortItems={sortStatesFabric(getItemValue)}
      getItemValue={getItemValue}

      renderItem={this.renderItem}

      renderMenu={this.renderMenu}

      renderInput={this.renderInput}

      inputProps={inputProps}
      wrapperStyle={wrapperStyle}

      ref={this.setRef}

      {...this.props}

      onChange={(event, value) => this.onDataChange(value)}
      onSelect={value => this.onDataSelect(value)}

    />);
  }
}

AutocompleteFieldWrapper.propTypes = {
  input: PropTypes.object,
  value: PropTypes.any,
  id: PropTypes.string,
  name: PropTypes.string,
  className: PropTypes.string,
  type: PropTypes.string,
  onChange: PropTypes.func,
  onInputChange: PropTypes.func,
  onInputSelect: PropTypes.func,
  onSelect: PropTypes.func,
  menuWrapper: PropTypes.func,
  shouldItemRender: PropTypes.func,
  sortItems: PropTypes.func,
  getItemValue: PropTypes.func,
  shouldPassAllValues: PropTypes.bool,
  isInvalid: PropTypes.bool,
  ...Textfield.propTypes
};

AutocompleteFieldWrapper.defaultProps = {
  getItemValue: getSimpleValue,
  menuWrapper: identityFn,
  shouldPassAllValues: false
};

export default AutocompleteFieldWrapper;
