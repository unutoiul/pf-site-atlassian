import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow, render, mount } from 'enzyme';
import FieldBase from '../../FieldBase';
import AutocompleteField, { ShortAutocompleteField, MediumAutocompleteField } from '..';
import AutocompleteHelper from '../AutocompleteHelper';

const DEFAULT_PROPS = {
  input: {},
  name: 'testInput',
  labelText: '',
  required: false
};

describe('Autocomplete helper', () => {
  const createAutocompleteField = options => <AutocompleteField {...DEFAULT_PROPS} {...options} />;
  const createShortAutocompleteField = options => <ShortAutocompleteField {...DEFAULT_PROPS} {...options} />;
  const createMediumAutocompleteField = options => <MediumAutocompleteField {...DEFAULT_PROPS} {...options} />;

  const itBehavesLikeAnAutocompleteField = (fieldCreator) => {
    it('should render the label', () => {
      const wrapper = shallow(fieldCreator({ labelText: 'TestAutocompleteField' }));
      expect(wrapper.find(FieldBase).props().label).to.be.equal('TestAutocompleteField');
    });

    it('should indicate if the field is not required', () => {
      const wrapper = shallow(fieldCreator({ required: false }));
      expect(wrapper.find(FieldBase).props().isRequired).to.be.equal(false);
    });

    it('should indicate if the field is required', () => {
      const wrapper = shallow(fieldCreator({ required: true }));
      expect(wrapper.find(FieldBase).props().isRequired).to.be.equal(true);
    });

    it('should render a text input field', () => {
      // Have to use render, not shallow
      const wrapper = render(fieldCreator({ labelText: 'TestAutocompleteField' }));
      expect(wrapper.find('input[type="text"]')).to.be.present();
    });

    it('should accept the input type as a prop', () => {
      const wrapper = render(fieldCreator({ labelText: 'TestAutocompleteField', type: 'number' }));
      expect(wrapper.find('input[type="number"]')).to.be.present();
    });

    describe('when there are errors in the field', () => {
      let wrapper;
      before(() => {
        wrapper = shallow(fieldCreator({ meta: { invalid: true, touched: true, error: ['some error'] } }));
      });

      it('should render text to indicate the form input is invalid', () => {
        expect(wrapper.find(FieldBase).props().isInvalid).to.be.equal(true);
      });
    });

    describe('when autocomplete fail set', () => {
      let wrapper;
      before(() => {
        wrapper = shallow(fieldCreator({ autoCompleteFailed: true }));
      });

      it('should render text to indicate the form input is invalid', () => {
        expect(wrapper.find(FieldBase).props().isInvalid).to.be.equal(true);
      });
    });

    describe('when there are no errors in the field', () => {
      let wrapper;
      before(() => {
        wrapper = mount(fieldCreator({ meta: { invalid: false } }));
      });

      it('should not render text to indicate the form input is invalid', () => {
        expect(wrapper.find(FieldBase).props().isInvalid).to.be.equal(false);
        expect(wrapper.find(FieldBase).props().invalidMessage).to.be.equal(undefined);
        expect(wrapper.find(FieldBase).find(AutocompleteHelper).props().isInvalid).to.be.equal(false);
      });
    });
  };

  describe('AutocompleteField: ', () => {
    itBehavesLikeAnAutocompleteField(createAutocompleteField);
  });

  describe('ShortAutocompleteField: ', () => {
    itBehavesLikeAnAutocompleteField(createShortAutocompleteField);

    it('should set have the class "short-field"', () => {
      const wrapper = mount(createShortAutocompleteField({ labelText: 'TestAutocompleteField', type: 'number' }));
      expect(wrapper.find('.adg-fieldbase')).to.have.className('short-bux-field');
    });
  });

  describe('MediumAutocompleteField: ', () => {
    itBehavesLikeAnAutocompleteField(createMediumAutocompleteField);

    it('should set have the class "medium-field"', () => {
      const wrapper = mount(createMediumAutocompleteField({ labelText: 'TestAutocompleteField', type: 'number' }));
      expect(wrapper.find('.adg-fieldbase')).to.have.className('medium-bux-field');
    });
  });

  describe('Autoselect: ', () => {
    itBehavesLikeAnAutocompleteField(options => createMediumAutocompleteField({ ...options, autocomplete: true }));

    it('should set have the class "medium-field"', () => {
      const wrapper = mount(createMediumAutocompleteField({
        labelText: 'TestAutocompleteField',
        type: 'number',
        autocomplete: true
      }));
      expect(wrapper.find('.adg-fieldbase')).to.have.className('medium-bux-field');
    });

    it('should fill autocomplete', () => {
      const items = [
        '2345',
        '12345'
      ];
      let selectedValue = '';
      const wrapper = mount(createMediumAutocompleteField({
        labelText: 'TestAutocompleteField',
        type: 'number',
        autocomplete: true,
        value: '123',
        onInputSelect: value => (selectedValue = value),
        items,
      }));

      expect(wrapper.find('.autocomplete-menu')).to.not.be.present();

      wrapper.find('input').simulate('focus');
      wrapper.find('input').simulate('click');

      expect(wrapper.find('.autocomplete-menu')).to.be.present();
      expect(wrapper.find('.autocomplete-menu .autocomplete-result')).to.have.length(1);
      wrapper.find('.autocomplete-menu .autocomplete-result').simulate('click');
      expect(selectedValue).to.be.equal('12345');
    });
  });

  it('Autocomplete should tract window', () => {
    const AEL = window.addEventListener;
    const REL = window.removeEventListener;
    window.addEventListener = sinon.spy();
    window.removeEventListener = sinon.spy();

    const wrapper = mount(<AutocompleteHelper {...DEFAULT_PROPS} />);
    expect(window.addEventListener).to.be.calledWith('scroll');
    wrapper.unmount();
    expect(window.removeEventListener).to.be.calledWith('scroll');

    window.addEventListener = AEL;
    window.removeEventListener = REL;
  });
});
