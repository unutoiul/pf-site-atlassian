import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';
import FieldBase from '../FieldBase';
import ExtendedPropTypes from '../PropTypes';
import styles from '../fields.less';
import AutocompleteHelper from './AutocompleteHelper';

const createAutocompleteField = (fieldWidth) => {
  const classes = fieldWidth ? cx(`${fieldWidth}-bux-field`, styles[fieldWidth]) : '';

  const AutocompleteField = (props) => {
    const {
      input,
      labelText,
      type = 'text',
      required,
      meta: { error, invalid, touched },
      autoCompleteFailed,
      isFocused,
    } = props;
    const { name } = input;
    return (
      <FieldBase
        label={labelText}
        id={name}
        isRequired={required}
        isInvalid={autoCompleteFailed || (invalid && touched)}
        invalidMessage={error}
        className={classes}
        isFitContainerWidthEnabled
        putMessageAbove={isFocused}
        isPaddingDisabled
      >
        <AutocompleteHelper
          id={name}
          name={name}
          {...props}
          {...input}
          type={type}
          className={cx(styles.adgInput, styles.wide)}
          isRequired={required}
          isInvalid={invalid}
        />
      </FieldBase>
    );
  };

  AutocompleteField.propTypes = {
    input: PropTypes.object,
    labelText: PropTypes.string.isRequired,
    type: PropTypes.string,
    required: PropTypes.bool,
    isFocused: PropTypes.bool,
    meta: ExtendedPropTypes.meta,
    autoCompleteFailed: PropTypes.bool
  };

  AutocompleteField.defaultProps = {
    meta: {}
  };

  return AutocompleteField;
};

export default createAutocompleteField;
