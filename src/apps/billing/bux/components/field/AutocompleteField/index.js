import createAutocompleteField from './createAutocompleteField';

const ShortAutocompleteField = createAutocompleteField('short');
const MediumAutocompleteField = createAutocompleteField('medium');
const WideAutocompleteField = createAutocompleteField('wide');
const AutocompleteField = createAutocompleteField();

export { ShortAutocompleteField, MediumAutocompleteField, WideAutocompleteField };

export default AutocompleteField;