import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import Select from '@atlaskit/select';
import { simulateKeyDown } from 'bux/helpers/DOM-utils';

import BUXSelect from '..';

const DEFAULT_PROPS = {
  data: []
};

const REDUX_FORM_MOCK = {
  input: {
    name: 'testInput',
    onChange() {
    },
    value: 'Example'
  }
};

const MOCKED_PROPS = {
  ...DEFAULT_PROPS,
  ...REDUX_FORM_MOCK
};

const createSelect = options => (
  <BUXSelect {...MOCKED_PROPS} {...options} />
);
describe('Select: ', () => {
  describe('autocomplete: ', () => {
    it('should render selected item', () => {
      const wrapper = mount(createSelect({
        data: [{
          id: 'Example',
          displayName: 'textValue'
        }],
        autocomplete: true
      }));
      expect(wrapper).to.contain.text('textValue');
    });

    it('should allow user to change the value to anything', () => {
      const wrapper = mount(createSelect({
        data: [{
          id: 'Example',
          displayName: 'textValue'
        }],
        autocomplete: true
      }));
      const input = wrapper.find('input').first();
      input.instance().value = 'otherValue';
      input.simulate('change');
      expect(input).to.have.value('otherValue');
    });

    describe('when in strict mode', () => {
      const createStrictSelectField = options => createSelect({
        ...options,
        autocomplete: true,
        strict: true
      });

      it('should not allow user to select a value that is not provided in the dropdown', () => {
        const wrapper = mount(createStrictSelectField({
          data: [{
            id: 'Example',
            displayName: 'textValue'
          }]
        }));

        const input = wrapper.find(Select).children().find('input').at(1);
        input.instance().value = 'otherValue';
        input.simulate('change');

        expect(wrapper).to.contain.text('textValue');
        // expect(wrapper).to.have.state('filterValue', 'textValue');
      });

      it('should select the default value when the user escapes from the input', () => {
        const wrapper = mount(createStrictSelectField({
          data: [{
            id: 'Example',
            displayName: 'textValue'
          }, {
            id: 'Example 2',
            displayName: 'otherValue'
          }]
        }));

        const input = wrapper.find(Select).children().find('input').first();
        input.instance().value = 'otherV';
        simulateKeyDown('Escape', document);
        // expect(wrapper).to.have.state('filterValue', 'textValue');
      });
    });
  });
});