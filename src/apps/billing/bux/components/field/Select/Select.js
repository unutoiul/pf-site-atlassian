import React from 'react';
import AkSelect from '@atlaskit/select';

import PropTypes from 'prop-types';

import { withAnalyticsEmitter } from 'bux/components/Analytics';

const pickName = (selectedValue, items, defaultValue) => {
  const selected = items.find(item => item.id === selectedValue);
  return selected ? selected.displayName : defaultValue;
};

export const AkSelectWithAnalytics = withAnalyticsEmitter('Select', 'textField', {
  onFocus: {
    action: 'focused',
  },
  onBlur: {
    action: 'blurred',
  },
})(AkSelect);

export default class BUXSelect extends React.PureComponent {
  static defaultProps = {
    placeholder: 'Select...'
  };

  static propTypes = {
    input: PropTypes.object,
    placeholder: PropTypes.string,
    autocomplete: PropTypes.bool,
    data: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.string,
      displayName: PropTypes.string,
    })),
    onChange: PropTypes.func,
    onBlur: PropTypes.func,
    onFocus: PropTypes.func,
  }

  onChange = (selectedItem, actionMeta, analyticsEvent) => {
    if (this.props.input.onChange) {
      this.props.input.onChange(selectedItem.id);
    }
    if (this.props.onChange) {
      this.props.onChange(selectedItem, actionMeta);
    }
    analyticsEvent.fire();
  }

  onBlur = (...params) => {
    if (this.props.onBlur) {
      this.props.onBlur(...params);
    }
  }
  

  onFocus = (...params) => {
    this.props.input.onFocus(...params);
    if (this.props.onFocus) {
      this.props.onFocus(...params);
    }
  }

  getOptionsValue = ({ id }) => id
  
  getOptionLabel = ({ displayName }) => displayName

  render() {
    const {
      input,
      placeholder,
      autocomplete,
      data,
      ...rest
    } = this.props;
    const { name } = input;
    // react-select has a bug where it requires 'value' & 'label' in order to display correctly
    const options = data.map(({ displayName, id }) => ({
      value: id, label: displayName, displayName, id
    }));
    const itemOfValue = options.find(item => item.value === input.value);
    return (
      <AkSelectWithAnalytics
        {...input}
        {...rest}

        // Note that AkSelect wants the entire item, not just the 'value'
        value={itemOfValue || undefined}
        
        id={name}
        instanceId={name}
        name={name}

        isSearchable={autocomplete}
    
        onChange={this.onChange}
        onBlur={this.onBlur}
        onFocus={this.onFocus}

        getOptionLabel={this.getOptionLabel}
        getOptionValue={this.getOptionValue}
        // options={data}
        options={options}

        placeholder={pickName(input.value, options, placeholder)}

        closeMenuOnSelect
      />
    );
  }
}
