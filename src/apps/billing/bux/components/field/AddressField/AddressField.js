import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { getAutoComplete, getBadge, getDetails } from 'bux/common/api/autocomplete';
import { withAnalyticsEvents } from '@atlaskit/analytics-next';

const autoComplete = getAutoComplete();

const restrictResults = (results, restrict) => (
  results.filter(item =>
    !item.addressDetails.country ||
    !item.addressDetails.country.code ||
    item.addressDetails.country.code === restrict.country).map(item => ({
    ...item,
    key: item.id,
  }))
);

const createAddressField = (AutocompleteField) => {
  class AddressField extends Component {
    static propTypes = {
      restrict: PropTypes.shape({
        country: PropTypes.string,
        focusPoint: PropTypes.any,
      }),
      onFill: PropTypes.func.isRequired,
      onAutocompleteStatus: PropTypes.func.isRequired,
      isFocused: PropTypes.bool,
      createAnalyticsEvent: PropTypes.func.isRequired,
    };

    static contextTypes = {
      sendAnalyticEvent: PropTypes.func.isRequired,
    };

    constructor(props) {
      super(props);
      this.state = {
        autoCompleteData: [],
        dataSequence: 0,
        timeStart: Date.now(),
      };
    }

    onSelect = (value) => {
      const item = this.state.autoCompleteData.find(line => line.text === value);
      if (item) {
        this.setState({
          autoCompleteData: [],
        });
        getDetails(item).then(detailedItems => this.props.onFill(detailedItems));
        const timeSpent = Math.floor((Date.now() - this.state.timeStart) / 1000);
        const event = this.props.createAnalyticsEvent({
          action: 'clicked',
          actionSubject: 'textField',
          actionSubjectId: 'autoCompleteResultField',
          timeSpent,
          sequence: this.state.dataSequence,
        });
        event.fire('atlaskit');

        this.context.sendAnalyticEvent('autocompleteSelect', {
          timeSpent,
          sequence: this.state.dataSequence,
        });

        this.setState({
          dataSequence: 0,
        });
        return false;
      }
      this.props.onFill({
        addressDetails: {
          street: {
            label: value,
          },
        },
      });
      return true;
    };

    onChange = (value) => {
      this.autoComplete(value);
    };

    autoComplete(text) {
      const props = this.props;
      const { country } = props.restrict;
      const currentSequence = this.state.dataSequence + 1;

      this.setState({
        dataSequence: currentSequence,
      });

      if (text.length < 3 || !autoComplete) {
        this.setState({
          autoCompleteData: [],
        });
      } else {
        autoComplete(text, {
          focus: props.restrict.focusPoint,
          country,
        }).then((results) => {
          if (currentSequence === this.state.dataSequence) {
            // Mapzen can return address from other country. Restrict.
            const addresses = restrictResults(results, props.restrict);
            this.context.sendAnalyticEvent('autocompleteSuccess', {
              responseCount: addresses.length,
              sequence: currentSequence,
            });
            this.setState({
              autoCompleteData: addresses,
            });
            this.props.onAutocompleteStatus(addresses.length > 0);
          } else {
            // nop
          }
        }).catch(() => {
          if (currentSequence === this.state.dataSequence) {
            this.props.onAutocompleteStatus(false);
            this.context.sendAnalyticEvent('autocompleteFail', {
              sequence: currentSequence,
            });
            this.setState({
              autoCompleteData: [],
            });
          } else {
            //nop
          }
        });
      }
    }

    render() {
      const props = this.props;

      // Add some legal stuff
      const legal = (<div className="legal" style={{ textAlign: 'right', padding: '2px', color: '#666' }}>
        {getBadge()}
      </div>);
      return (
        <AutocompleteField
          shouldPassAllValues

          items={this.state.autoCompleteData}
          getItemValue={item => item.text}

          onInputChange={this.onChange}
          onInputSelect={this.onSelect}
          menuWrapper={items => (<div>{items} {items.length > 0 ? legal : ''}</div>)}

          {...props}
        />
      );
    }
  }
  const AnalyticsAddressField = withAnalyticsEvents()(AddressField);
  // For whatever reason, redux-form does not recognise AnalyticsAddressField as a component so it needs to be wrapped
  return props => <AnalyticsAddressField {...props} />;
};

export default createAddressField;
