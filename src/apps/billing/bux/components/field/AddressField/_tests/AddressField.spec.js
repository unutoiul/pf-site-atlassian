import sinon from 'sinon';
import { expect } from 'chai';

import PropTypes from 'prop-types';

import React from 'react';
import { Provider } from 'bux/helpers/redux';
import createStore from 'bux/core/createStore';
import proxyquire from 'bux/helpers/proxyquire';
import { reduxForm } from 'redux-form';
import { mount } from 'enzyme';

import AutocompleteField from '../../AutocompleteField';


describe('AddressField: ', () => {
  let autoComplete;
  let getBadge;
  let sendAnalyticEvent;
  let onFill;
  let onAutocompleteStatus;

  let AddressField;

  const getDetails = item => Promise.resolve(item);
  const getAutoComplete = () => autoComplete;

  beforeEach(() => {
    autoComplete = sinon.stub();
    getBadge = sinon.stub().returns('test case');
    sendAnalyticEvent = sinon.stub();
    onFill = sinon.stub();
    onAutocompleteStatus = sinon.stub();

    const createAddressField = proxyquire.noCallThru().load('../AddressField', {
      'bux/common/api/autocomplete': { getAutoComplete, getDetails, getBadge }
    }).default;
    AddressField = createAddressField(AutocompleteField);
  });

  const createWrapper = (props) => {
    const store = createStore();
    const Container = parentProps => (
      <AddressField
        {...parentProps}
        name="address"
        labelText="address"
        restrict={{ country: 'AU', focusPoint: null }}
        onFill={onFill}
        onAutocompleteStatus={onAutocompleteStatus}
      />
    );
    const Form = reduxForm({ form: 'testForm', ...props })(Container);
    return mount(<Provider store={store}>
      <Form />
                 </Provider>, {
      context: {
        sendAnalyticEvent,
      },
      childContextTypes: {
        sendAnalyticEvent: PropTypes.func
      }
    });
  };

  it('should fail address autocomplete', (done) => {
    const wrapper = createWrapper({
      input: {
        value: '341'
      }
    });

    autoComplete.returns(Promise.reject());

    expect(wrapper.find('.autocomplete-menu')).to.not.be.present();

    wrapper.find('input').simulate('focus');
    wrapper.find('input').simulate('click');
    wrapper.find('input').simulate('change', { target: { value: '343' } });

    expect(wrapper.find('.autocomplete-menu')).to.be.present();

    setImmediate(() => {
      expect(autoComplete).to.be.called();
      expect(sendAnalyticEvent).to.have.been.calledWithMatch('autocompleteFail', { sequence: 1 });
      expect(onAutocompleteStatus).to.have.been.calledWithMatch(false);
      done();
    });
  });

  it('should perform address autocomplete', (done) => {
    const wrapper = createWrapper({
      input: {
        value: '341'
      }
    });

    autoComplete.returns(Promise.resolve([
      {
        id: 1,
        text: 'item1',
        addressDetails: {
          country: { code: 'AU' }
        }
      },
      {
        id: 2,
        text: 'item2',
        addressDetails: {
          country: { code: 'US' }
        }
      },
      {
        id: 3,
        text: 'item343',
        addressDetails: {
          country: { code: 'AU' }
        }
      },
    ]));

    expect(wrapper.find('.autocomplete-menu')).not.to.be.present();

    wrapper.find('input').simulate('focus');
    wrapper.find('input').simulate('click');
    wrapper.find('input').simulate('change', { target: { value: '340' } });
    wrapper.find('input').simulate('change', { target: { value: '343' } });

    setImmediate(() => {
      wrapper.update();
      expect(autoComplete).to.be.called();
      expect(sendAnalyticEvent).to.have.been.calledWithMatch('autocompleteSuccess', {
        responseCount: 2,
        sequence: 2
      });
      expect(onAutocompleteStatus).to.have.been.calledWithMatch(true);

      expect(wrapper.find('.autocomplete-menu')).to.be.present();
      //test bage
      expect(wrapper.find('.legal')).to.have.text('test case');

      expect(wrapper.find('.autocomplete-menu .autocomplete-result')).to.have.length(2);
      wrapper.find('.autocomplete-menu .autocomplete-result').at(0).simulate('click');
      expect(onFill).not.to.be.called();

      expect(sendAnalyticEvent).to.have.been.calledWithMatch('autocompleteSelect', {
        sequence: 2,
        timeSpent: 0
      });

      // form close
      expect(wrapper.find('.legal')).not.to.be.present();

      setImmediate(() => {
        expect(onFill).to.be.calledWithMatch({
          addressDetails: {
            country: { code: 'AU' }
          }
        });
        expect(wrapper.find('.autocomplete-menu')).to.not.be.present();
        done();
      });
    });
  });

  it('should work as input', (done) => {
    const wrapper = createWrapper({
      input: {
        value: '341'
      }
    });

    autoComplete.returns(Promise.resolve([{
      id: 1,
      text: 'item1',
      addressDetails: {
        country: { code: 'AU' }
      }
    }]));

    expect(wrapper.find('.autocomplete-menu')).not.to.be.present();

    wrapper.find('input').simulate('focus');
    wrapper.find('input').simulate('click');
    wrapper.find('input').simulate('change', { target: { value: 'item' } });

    setImmediate(() => {
      wrapper.update();
      expect(wrapper.find('.autocomplete-menu .autocomplete-result')).to.have.length(1);
      wrapper.find('input').simulate('keyPress', { key: 'Enter' });

      setImmediate(() => {
        expect(onFill).to.be.calledWith({
          addressDetails: {
            street: {
              label: 'item'
            }
          }
        });
        done();
      });
    });
  });
});
