import AutocompleteField, {
  ShortAutocompleteField,
  MediumAutocompleteField,
  WideAutocompleteField
} from 'bux/components/field/AutocompleteField';

import createAddressField from './AddressField';

const ShortAddressField = createAddressField(ShortAutocompleteField);
const MediumAddressField = createAddressField(MediumAutocompleteField);
const WideAddressField = createAddressField(WideAutocompleteField);
const AddressField = createAddressField(AutocompleteField);

export { ShortAddressField, MediumAddressField, WideAddressField };

export default AddressField;
