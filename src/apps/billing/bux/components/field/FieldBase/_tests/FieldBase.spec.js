import React from 'react';
import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import AKFieldBase, { Label } from '@atlaskit/field-base';
import Tooltip from '@atlaskit/tooltip';
import FieldBase from 'bux/components/field/FieldBase';


const props = {
  label: 'label',
  id: 'id',
  isLabelHidden: false,
  isRequired: true,

  isInvalid: true,
  invalidMessage: 'message',
  isDisabled: true,
  isReadOnly: true,
  children: undefined
};

describe('FieldBase: ', () => {
  it('should pass props to AtlasKit', () => {
    const wrapper = mount(<FieldBase {...props}>payload</FieldBase>);

    const label = wrapper.find(Label);
    const base = wrapper.find(AKFieldBase);

    expect(label.props()).to.be.deep.equal({
      appearance: 'default', // default prop
      label: props.label,
      htmlFor: props.id,
      isLabelHidden: props.isLabelHidden,
      isRequired: props.isRequired,
      children: props.children
    });

    expect(base.props()).to.be.deep.include({
      children: 'payload',
      isInvalid: props.isInvalid,
      invalidMessage: props.invalidMessage,
      isDisabled: props.isDisabled,
      isReadOnly: props.isReadOnly,
      isPaddingDisabled: undefined,
      isFitContainerWidthEnabled: undefined
    });
  });
  context('show/hide field help', () => {
    it('should include tooltip if has a help', () => {
      const wrapper = shallow(<FieldBase {...props} help="help">payload</FieldBase>);
      const label = wrapper.find(Label);
      expect(label.find(Tooltip)).to.be.present();
    });
    it('should not include tooltip without help', () => {
      const wrapper = shallow(<FieldBase {...props}>payload</FieldBase>);
      const label = wrapper.find(Label);
      expect(label.find(Tooltip)).to.not.be.present();
    });
  });
});
