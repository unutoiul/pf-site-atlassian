import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';
import EditorInfoIcon from '@atlaskit/icon/glyph/editor/info';
import Tooltip from '@atlaskit/tooltip';
import { akColorP400 } from '@atlaskit/util-shared-styles';
import AKFieldBase, { FieldBaseStateless, Label } from '@atlaskit/field-base';

import { Media } from 'common/responsive/Matcher';

import styles from './FieldBase.less';

const idleFunction = () => {
};

const BUXFieldBase = (props) => {
  const FieldComponent = props.defaultIsDialogOpen ? FieldBaseStateless : AKFieldBase;
  return (
    <div>
      <div className={styles.labelContainer}>
        <Label
          label={props.label}
          htmlFor={props.id}
          isLabelHidden={props.isLabelHidden}
          isRequired={props.isRequired}
        >
          {props.help &&
          <Tooltip content={props.help}>
            <EditorInfoIcon primaryColor={akColorP400} size="small" />
          </Tooltip>
          }
        </Label>
      </div>
      {props.putMessageAbove && (
        <Media.Below including mobile>
          <div className={styles.mobileMessageFieldHolder}>
            <div className={cx(styles.mobileMessageField, !props.invalidMessage && styles.mobileMessageFieldInvisible)}>
              {props.invalidMessage}
            </div>
          </div>
        </Media.Below>
      )}
      <div className={cx('adg-fieldbase', props.className)} data-component>
        <Media.Matches>
          {(_, pickMatch) => (
            <FieldComponent
              isInvalid={props.isInvalid}
              isDialogOpen={props.defaultIsDialogOpen}
              isDisabled={props.isDisabled}
              isRequired={props.isRequired}
              isReadOnly={props.isReadOnly}
              isPaddingDisabled={props.isPaddingDisabled}
              isFitContainerWidthEnabled={props.isFitContainerWidthEnabled}
              invalidMessage={pickMatch({
                tablet: props.invalidMessage,
              })}
              onBlur={idleFunction}
              onFocus={idleFunction}
            >
              {props.children}
            </FieldComponent>
          )}
        </Media.Matches>
      </div>
      {(!props.putMessageAbove || props.isInvalid) && (
        <Media.Below including mobile>
          <div className={cx(styles.mobileInvalidField, !props.invalidMessage && styles.mobileInvalidFieldInvisible)}>
            {props.invalidMessage}
          </div>
        </Media.Below>
      )}
    </div>
  );
};

BUXFieldBase.propTypes = {
  label: PropTypes.string,
  id: PropTypes.string,
  isLabelHidden: PropTypes.bool,
  isRequired: PropTypes.bool,
  isInvalid: PropTypes.bool,
  invalidMessage: PropTypes.node,
  isDisabled: PropTypes.bool,
  isReadOnly: PropTypes.bool,
  isPaddingDisabled: PropTypes.bool,
  isFitContainerWidthEnabled: PropTypes.bool,
  defaultIsDialogOpen: PropTypes.bool,
  putMessageAbove: PropTypes.bool,
  help: PropTypes.string,
  className: PropTypes.string,
  children: PropTypes.node
};


export default BUXFieldBase;
