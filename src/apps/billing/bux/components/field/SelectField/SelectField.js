import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';
import { Field } from 'atlaskit-form-next';

import styles from '../fields.less';
import BUXSelect from '../Select';

const createSelectField = (fieldWidth) => {
  const classes = fieldWidth ? cx(`${fieldWidth}-bux-field`, styles[fieldWidth]) : '';
  const SelectField = ({ 
    input,
    labelText,
    disabled = false,
    required = false,
    handleError,
    validate,
    placeholder,
    autocomplete,
    data,
  }) => {
    const { name } = input;
    return (
      <div className={cx('adg-select', classes)} data-component>
        <Field
          {...input}
          label={labelText}
          name={name}
          isRequired={required}
          isDisabled={disabled}
          validate={(...params) => validate(...params, input.meta)}
          onBlur={() => input.onBlur(input.value)}
        >
          {
            ({ fieldProps, error }) => (
              <React.Fragment>
                <BUXSelect 
                  {...fieldProps}
                  input={input}
                  placeholder={placeholder}
                  autocomplete={autocomplete}
                  data={data}
                />
                { handleError ? handleError(error) : null }
              </React.Fragment>
            )
          }
        </Field>
      </div>
    );
  };
  SelectField.propTypes = {
    input: PropTypes.object.isRequired,
    labelText: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    disabled: PropTypes.bool,
    required: PropTypes.bool,
    autocomplete: PropTypes.bool,
    strict: PropTypes.bool,
    data: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.string.isRequired,
      displayName: PropTypes.string.isRequired
    })).isRequired,
    validate: PropTypes.func,
    handleError: PropTypes.func,
  };

  return SelectField;
};

export default createSelectField;
