import createSelectField from './SelectField';

const ShortSelectField = createSelectField('short');
const MediumSelectField = createSelectField('medium');
const WideSelectField = createSelectField('wide');
const SelectField = createSelectField();

export { ShortSelectField, MediumSelectField, WideSelectField };

export default SelectField;