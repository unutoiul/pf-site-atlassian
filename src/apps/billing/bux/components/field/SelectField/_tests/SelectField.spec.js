import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import Select from '@atlaskit/select';
import SelectField from '..';

const DEFAULT_PROPS = {
  labelText: '',
  data: []
};

const REDUX_FORM_MOCK = {
  input: {
    name: 'testInput',
    onChange() {
    },
    value: 'Example'
  }
};

const MOCKED_PROPS = {
  ...DEFAULT_PROPS,
  ...REDUX_FORM_MOCK
};

const createSelectField = options => (
  <SelectField {...MOCKED_PROPS} {...options} />
);

describe('SelectField: ', () => {
  it('should render the label', () => {
    const wrapper = mount(createSelectField({ labelText: 'SelectFieldInput' }));
    expect(wrapper.find('label')).to.have.text('SelectFieldInput');
  });

  it('should indicate if the field is not required', () => {
    const wrapper = mount(createSelectField());
    expect(wrapper.find(Select).props().isRequired).to.be.equal(false);
  });

  it('should indicate if the field is required', () => {
    const wrapper = mount(createSelectField({ required: true }));
    expect(wrapper.find(Select).props().isRequired).to.be.equal(true);
  });

  it('should display a Select option if the field is required', () => {
    const data = [{ id: 'FIRST', displayName: 'Message' }, { id: 'SECOND', displayName: 'Message' }];
    const wrapper = mount(createSelectField({ required: true, data }));
    expect(wrapper.find(Select).props().placeholder).to.be.equal('Select...');
  });

  it('should render selected item', () => {
    const wrapper = mount(createSelectField({
      data: [{
        id: 'Example',
        displayName: 'textValue'
      }]
    }));
    expect(wrapper).to.contain.text('textValue');
  });

  it('should pass placeholder', () => {
    const data = [{ id: 'FIRST', displayName: 'Message' }, { id: 'SECOND', displayName: 'Message' }];
    const wrapper = mount(createSelectField({ placeholder: 'select', required: true, data }));
    expect(wrapper.find(Select)).to.have.prop('placeholder', 'select');
  });

  it('should display a selected option', () => {
    const data = [{ id: 'FIRST', displayName: 'Message' }, { id: 'SECOND', displayName: 'SELECTED' }];
    const wrapper = mount(createSelectField({ required: false, data, input: { name: 'textInput', value: 'SECOND' } }));
    expect(wrapper.find(Select)).to.have.prop('placeholder', 'SELECTED');
  });
});
