import PropTypes from 'prop-types';
import React, { Component } from 'react';
import _range from 'lodash.range';
import moment from 'moment';
import FieldBase from '../FieldBase';
import ExtendedPropTypes from '../PropTypes';

class SelectDateDropdownField extends Component {
  constructor() {
    super();
    this.state = {
      months: _range(1, 13).map(month => ({ id: month, displayName: moment(month, 'M').format('MM') })),
      years: _range(0, 26)
        .map(offset => moment().add(offset, 'Y').year())
        .map(year => ({ id: year, displayName: year }))
    };
  }

  onMonthChange = (e) => {
    const month = e.target.value;
    const { input: { onChange, value } } = this.props;
    if (onChange) {
      onChange({ ...value, month });
    }
  };

  onYearChange = (e) => {
    const year = e.target.value;
    const { input: { onChange, value } } = this.props;
    if (onChange) {
      onChange({ ...value, year });
    }
  };

  render() {
    const {
      labelText,
      required,
      readonly,
      input: { name, value: { month, year } },
      meta: { error, invalid }
    } = this.props;

    return (
      <FieldBase
        label={labelText}
        isRequired={required}
        isInvalid={invalid}
        invalidMessage={error}
      >
        <div style={{ whiteSpace: 'nowrap', display: 'flex' }}>
          <select
            id={`${name}-month`}
            name={`${name}-month`}
            className="select short-field"
            onChange={this.onMonthChange}
            value={month}
            readOnly={readonly}
            aria-invalid={invalid}
          >
            <option value="">Month</option>
            { this.state.months.map(({ id, displayName }) => <option key={id} value={id}>{displayName}</option>) }
          </select>

          <span>&nbsp;</span>

          <select
            id={`${name}-year`}
            name={`${name}-year`}
            className="select short-field"
            onChange={this.onYearChange}
            value={year}
            readOnly={readonly}
            aria-invalid={invalid}
          >
            <option value="">Year</option>
            { this.state.years.map(({ id, displayName }) => <option key={id} value={id}>{displayName}</option>) }
          </select>
        </div>
      </FieldBase>
    );
  }
}

SelectDateDropdownField.propTypes = {
  labelText: PropTypes.string.isRequired,
  required: PropTypes.bool,
  readonly: PropTypes.bool,
  input: PropTypes.shape({
    onChange: PropTypes.func,
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.shape({
        month: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
        year: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
      })
    ])
  }),
  meta: ExtendedPropTypes.meta
};

SelectDateDropdownField.defaultProps = {
  input: { value: {} }
};

export default SelectDateDropdownField;
