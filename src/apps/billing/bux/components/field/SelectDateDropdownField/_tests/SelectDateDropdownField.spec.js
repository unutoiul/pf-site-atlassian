import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import { Provider } from 'bux/helpers/redux';
import { reduxForm, Field } from 'redux-form';
import createStore from 'bux/core/createStore';
import FieldBase from '../../FieldBase';
import SelectDateDropdownField from '../SelectDateDropdownField';

const DEFAULT_PROPS = {
  name: 'testInput',
  labelText: ''
};

const createSelectDateDropdownField = (options, formProps = {}, store = createStore()) => {
  const Form = () => (
    <div><Field component={SelectDateDropdownField} {...DEFAULT_PROPS} {...options} /></div>
  );
  const TestForm = reduxForm({ form: 'testForm' })(Form);
  return (
    <Provider store={store}>
      <TestForm {...formProps} />
    </Provider>
  );
};

describe('SelectDateDropdownField: ', () => {
  it('should render the label', () => {
    const wrapper = mount(createSelectDateDropdownField({ labelText: 'SelectDateDropdownFieldInput' }));
    expect(wrapper.find('label')).to.have.text('SelectDateDropdownFieldInput');
  });

  it('should indicate if the field is not required', () => {
    const wrapper = mount(createSelectDateDropdownField({ required: false }));
    expect(wrapper.find(FieldBase).props().isRequired).to.be.equal(false);
  });

  it('should indicate if the field is required', () => {
    const wrapper = mount(createSelectDateDropdownField({ required: true }));
    expect(wrapper.find(FieldBase).props().isRequired).to.be.equal(true);
  });

  it('should not allow you to set an invalid month', () => {
    const wrapper = mount(createSelectDateDropdownField({ required: true }));
    wrapper.find('#testInput-month').simulate('change', { target: { value: '13' } });
    expect(wrapper.find('#testInput-month')).to.have.value('');
  });

  it('should not allow you to set an invalid year', () => {
    const wrapper = mount(createSelectDateDropdownField({ required: true }));
    wrapper.find('#testInput-year').simulate('change', { target: { value: '1994' } });
    expect(wrapper.find('#testInput-year')).to.have.value('');
  });

  it('should display a default month label initially', () => {
    const wrapper = mount(createSelectDateDropdownField({ required: true }));
    const monthSelect = wrapper.find('#testInput-month');
    expect(monthSelect.find('option').first()).to.be.selected();
    expect(monthSelect.find('option').first()).to.have.text('Month');
  });

  it('should display a default year label initially', () => {
    const wrapper = mount(createSelectDateDropdownField({ required: true }));
    const yearSelect = wrapper.find('#testInput-year');
    expect(yearSelect.find('option').first()).to.be.selected();
    expect(yearSelect.find('option').first()).to.have.text('Year');
  });

  it('should initialize from redux form in full data format', () => {
    const wrapper = mount(createSelectDateDropdownField({}, {
      initialValues: {
        testInput: {
          //value: {
          month: 10,
          year: 2035
          //}
        }
      }
    }));
    expect(wrapper.find('#testInput-month')).to.have.value('10');
    expect(wrapper.find('#testInput-year')).to.have.value('2035');
  });

  it('should update the form value with the month and year', () => {
    const wrapper = mount(createSelectDateDropdownField({ required: true }));
    wrapper.find('#testInput-month').simulate('change', { target: { value: '10' } });
    wrapper.find('#testInput-year').simulate('change', { target: { value: '2035' } });
    expect(wrapper.find('#testInput-month')).to.have.value('10');
    expect(wrapper.find('#testInput-year')).to.have.value('2035');
  });
});
