import InputField, { ShortInputField, MediumInputField, WideInputField } from '../InputField';
import SelectField, { ShortSelectField, MediumSelectField, WideSelectField } from '../SelectField';
import createSelectInputFallbackField from './SelectInputFallbackField';

const ShortSelectInputFallbackField = createSelectInputFallbackField(ShortInputField, ShortSelectField);
const MediumSelectInputFallbackField = createSelectInputFallbackField(MediumInputField, MediumSelectField);
const WideSelectInputFallbackField = createSelectInputFallbackField(WideInputField, WideSelectField);
const SelectInputFallbackField = createSelectInputFallbackField(InputField, SelectField);

export { ShortSelectInputFallbackField, MediumSelectInputFallbackField, WideSelectInputFallbackField };

export default SelectInputFallbackField;
