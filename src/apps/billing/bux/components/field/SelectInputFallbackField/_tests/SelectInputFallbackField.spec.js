import React from 'react';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';
import sinon from 'sinon';
import InputField from '../../InputField';
import SelectField from '../../SelectField';
import SelectInputFallbackField from '..';

const DEFAULT_PROPS = {
  labelText: '',
  data: [],
  input: {
    name: 'testInput',
    onChange() {
    },
    value: ''
  }
};

const createSelectInputFallbackField = (options = {}) => (
  <SelectInputFallbackField {...DEFAULT_PROPS} {...options} />
);

describe('SelectInputFallbackField: ', () => {
  describe('when given a list of options', () => {
    it('should render a SelectField', () => {
      const wrapper = mount(createSelectInputFallbackField({
        data: [{ id: 'TEST', displayName: 'Test' }],
        placeholder: 'select'
      }));
      expect(wrapper.find(SelectField)).to.be.present();
      expect(wrapper).to.have.prop('data').deep.equal([{
        id: 'TEST',
        displayName: 'Test'
      }]);
      expect(wrapper).to.have.prop('placeholder').equal('select');
    });

    it('should render a SelectField with selectPlaceholder', () => {
      const wrapper = mount(createSelectInputFallbackField({
        data: [{ id: 'TEST', displayName: 'Test' }],
        placeholderSelect: 'select',
        placeholderInput: 'input'
      })).childAt(0);
      expect(wrapper).to.have.prop('placeholder').equal('select');
    });

    it('should clear the field value if the data prop changes to be empty', () => {
      const onChange = sinon.stub();
      const wrapper = shallow(createSelectInputFallbackField({
        data: [{ id: 'TEST', displayName: 'Test' }],
        input: { onChange, value: 'TEST' }
      }));
      wrapper.setProps({ data: [] });
      expect(onChange).to.have.been.calledWithExactly('');
    });

    it('should clear the field value if the option list changes', () => {
      const onChange = sinon.stub();
      const wrapper = shallow(createSelectInputFallbackField({
        data: [{ id: 'TEST', displayName: 'Test' }],
        input: { onChange, value: 'TEST' }
      }));
      wrapper.setProps({ data: [{ id: 'TEST', displayName: 'Different' }] });
      expect(onChange).to.have.been.calledWithExactly('');
    });

    it('should not clear the field value if the data prop does not change', () => {
      const onChange = sinon.stub();
      const data = [{ id: 'TEST', displayName: 'Test' }];
      const wrapper = shallow(createSelectInputFallbackField({ data, input: { onChange, value: 'TEST' } }));
      wrapper.setProps({ data });
      expect(onChange).to.not.have.been.called();
    });
  });

  describe('when given an empty list of options', () => {
    it('should render an InputField', () => {
      const wrapper = mount(createSelectInputFallbackField({
        data: [],
        placeholder: 'input'
      }));
      expect(wrapper.find(InputField)).to.be.present();
      expect(wrapper).to.have.prop('placeholder').equal('input');
    });

    it('should render an InputField with input placeholder', () => {
      const wrapper = mount(createSelectInputFallbackField({
        placeholderSelect: 'select',
        placeholderInput: 'input'
      })).childAt(0);
      expect(wrapper).to.have.prop('placeholder').equal('input');
    });

    it('should not clear the field value when updated with an empty list of options', () => {
      const onChange = sinon.stub();
      const wrapper = shallow(createSelectInputFallbackField({ data: [], input: { onChange, value: '' } }));
      wrapper.setProps({ data: [] });
      expect(onChange).to.not.have.been.called();
    });

    describe('when updated with a list of options that contains an item with the field value as a key', () => {
      it('should not clear the field value', () => {
        const onChange = sinon.stub();
        const wrapper = shallow(createSelectInputFallbackField({ data: [], input: { onChange, value: 'TEST' } }));
        wrapper.setProps({ data: [{ id: 'TEST', displayName: 'Test' }] });
        expect(onChange).to.not.have.been.called();
      });
    });
  });

  it('should render an InputField when options are omitted', () => {
    const wrapper = mount(createSelectInputFallbackField({ data: undefined })).childAt(0);
    expect(wrapper.find(InputField)).to.be.present();
  });
});
