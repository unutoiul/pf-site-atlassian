import PropTypes from 'prop-types';
import React, { Component } from 'react';

const isSelectField = props => props.data && props.data.length > 0;
const isInputField = props => !isSelectField(props);

const createSelectFallbackInputField = (InputField, SelectField) => {
  const Input = InputField;
  const Select = SelectField;

  class SelectFallbackInputField extends Component {
    componentWillMount() {
      const { data, input: { onChange, value } } = this.props;
      if (
        isSelectField(this.props) &&
        value &&
        !data.find(item => item.id === value)
      ) {
        onChange('');
      }
    }

    componentWillReceiveProps(nextProps) {
      const { data, input: { onChange, value } } = nextProps;

      if (isInputField(this.props) && isSelectField(nextProps) && !data.find(item => item.id === value)) {
        onChange('');
      } else if (isSelectField(this.props) && isInputField(nextProps)) {
        onChange('');
      } else if (isSelectField(this.props) && isSelectField(nextProps) && this.props.data !== nextProps.data) {
        onChange('');
      }
    }

    render() {
      const props = this.props;
      return (isSelectField(props)
        ? <Select placeholder={props.placeholderSelect} {...props} />
        : <Input placeholder={props.placeholderInput} {...props} />);
    }
  }

  SelectFallbackInputField.propTypes = {
    data: PropTypes.array,
    placeholderSelect: PropTypes.string,
    placeholderInput: PropTypes.string,
    input: PropTypes.shape({
      name: PropTypes.string.isRequired,
      onChange: PropTypes.func.isRequired,
      value: PropTypes.string.isRequired
    }).isRequired
  };

  return SelectFallbackInputField;
};

export default createSelectFallbackInputField;