import PropTypes from 'prop-types';

const ExtendedPropTypes = {
  meta: PropTypes.shape({
    error: PropTypes.node,
    invalid: PropTypes.bool,
    touched: PropTypes.bool,
    dirty: PropTypes.bool
  })
};

export default ExtendedPropTypes;