import { storiesOf } from '@storybook/react';
import * as React from 'react';

import Date from 'bux/components/Date';
import { FootnoteIndicator } from 'bux/components/Footnote';
import Money from 'bux/components/Money';

import { Billing } from './Billing';

const BillEstimateDetail = () => (<p>This represents bill estimate component</p>);
const Container = ({ children }) => (<div style={{ width: 400, margin: '40px auto' }}>{children}</div>);
const dummyMethod = () => ({});
const overviewProps = {
  paymentMethod: 'TOKENIZED_CREDIT_CARD',
  paymentDetails: {
    expiryMonth: '03',
    expiryYear: '2018',
    name: 'Charlie',
    suffix: '1234',
    type: 'visa',
  },
};

const Overview = (props) => (
  <Billing.Overview
    title="Confirm your billing details"
    logPrefix="StepBillingOverview"
    dataTest="stepOverview"
    isLoading={props.isLoading}
    hasLoadingError={props.hasLoadingError}
    loadingErrorMsg={props.loadingErrorMsg}
    loadingErrorRetry={props.loadingErrorRetry}
  >
    <Billing.Address
      organisationName="Atlassian"
      addressLineFull="341 George St, Sydney 2000, NSW"
      country="AU"
    />
    <Billing.PaymentMethod method={overviewProps.paymentMethod} details={overviewProps.paymentDetails} />
    <Billing.Period
      title="Billing period"
      startDate="2018-01-01"
      endDate="2018-01-31"
    />
    <Billing.Estimate title="Monthly billing summary" isOrg={false} name="Atlassian">
      <BillEstimateDetail/>
    </Billing.Estimate>
    <Billing.Text>
      Upon subscribing, you’ll be charged&nbsp;
      <FootnoteIndicator><Money amount={100} currency="USD" /></FootnoteIndicator>&nbsp;
      per month starting on <Date value="2018-01-01" />.
    </Billing.Text>
    <Billing.Agreement isBusy={false} onAgreedChanged={dummyMethod}/>
    <Billing.Controls
      submitLabel="Subscribe"
      submitName="subscribe"
      onSubmit={dummyMethod}
      onCancel={dummyMethod}
      isFirstStep={true}
      isBusy={false}
      isSubmitDisabled={false}
    />
  </Billing.Overview>
);

storiesOf('BUX|Components/PaymentWizard/Billing', module)
  .add('Idle', () => (
    <Container>
      <Overview />
    </Container>))
  .add('Loading Estimate', () => (
    <Container>
      <Overview isLoading={true}/>
    </Container>))
  .add('Has a loading error', () => (
    <Container>
      <Overview
        hasLoadingError={true}
        loadingErrorMsg="Can't load estimate right now"
        loadingErrorRetry={dummyMethod}
      />
    </Container>));
