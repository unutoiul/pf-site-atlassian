import { Address } from './components/Address';
import { Agreement } from './components/Agreement';
import { Controls } from './components/Controls';
import { ErrorMessage } from './components/ErrorMessage';
import { Estimate } from './components/Estimate';
import { Overview } from './components/Overview';
import { PaymentMethod } from './components/PaymentMethod';
import { PayNowOrLater } from './components/PayNowOrLater';
import { Period } from './components/Period';
import { Text } from './components/Text';

export const Billing = {
  Overview,
  Address,
  Estimate,
  Period,
  PaymentMethod,
  PayNowOrLater,
  Text,
  Controls,
  Agreement,
  ErrorMessage,
};
