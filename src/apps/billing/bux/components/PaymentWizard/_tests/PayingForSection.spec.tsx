import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import * as React from 'react';

import { PayingForSection, Props } from '../PayingForSection';

describe('PaymentWizard', () => {
  describe('PayingForSection', () => {
    it('should render component', () => {
      const props: Props = {
        name: 'whynotdonut123-acme.atlassian.net',
        isOrg: false,
        startDate: '2018-07-05 11:13:00',
        endDate: '2018-07-05 11:13:00',
      };
      const wrapper = mount(<PayingForSection {...props} />);
      expect(wrapper).to.be.present();
      expect(wrapper.find('li').first()).to.have.text('Site: whynotdonut123-acme.atlassian.net');
      expect(wrapper.find('li').last()).to.contain.text('Billing period:');
      expect(wrapper.find('li').last().find('Date').length).to.be.eq(2);
    });
    it('should not render period', () => {
      const props: Props = {
        name: 'whynotdonut123-acme.atlassian.net',
        isOrg: false,
      };
      const wrapper = shallow(<PayingForSection {...props} />);
      expect(wrapper).to.be.present();
      expect(wrapper.find('li').length).to.be.eq(1);
    });
    it('should render organisation', () => {
      const props: Props = {
        name: 'whynotdonut123-acme.atlassian.net',
        isOrg: true,
      };
      const wrapper = shallow(<PayingForSection {...props} />);
      expect(wrapper).to.be.present();
      expect(wrapper.find('li').first()).to.contain.text('Organization');
    });
  });
});
