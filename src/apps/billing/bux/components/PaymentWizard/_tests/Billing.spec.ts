import { expect } from 'chai';

import { Billing } from '../Billing';

describe('PaymentWizard', () => {
  describe('Billing', () => {
    it('should return child components', () => {
      expect(typeof Billing.Overview).to.be.equals('function');
      expect(typeof Billing.Address).to.be.equals('function');
      expect(typeof Billing.Agreement).to.be.equals('function');
      expect(typeof Billing.Controls).to.be.equals('function');
      expect(typeof Billing.Estimate).to.be.equals('function');
      expect(typeof Billing.PaymentMethod).to.be.equals('function');
      expect(typeof Billing.Period).to.be.equals('function');
      expect(typeof Billing.Text).to.be.equals('function');
      expect(typeof Billing.ErrorMessage).to.be.equals('function');
    });
  });
});
