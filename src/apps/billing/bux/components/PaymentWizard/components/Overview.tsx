import * as React from 'react';

import Loading from 'bux/components/Loading/delayed';
import LogScope from 'bux/components/log/Scope';
import { StepUnavailableError } from 'bux/components/Wizard';

import styles from './style.less';

export interface OverviewProps {
  title: string | React.ReactNode;
  logPrefix?: string;
  dataTest?: string;
  isLoading?: boolean;
  hasLoadingError?: boolean;
  loadingErrorMsg?: string | React.ReactNode;
  loadingErrorRetry?(): void;
}

export const Overview: React.SFC<OverviewProps> = (
  { title, logPrefix, dataTest, isLoading, hasLoadingError, loadingErrorMsg, loadingErrorRetry, children },
) => (
  <LogScope prefix={logPrefix}>
    <div data-test={dataTest}>
      {isLoading && <div className={styles.loading}><Loading /></div>}
      {hasLoadingError && <StepUnavailableError message={loadingErrorMsg} onRetry={loadingErrorRetry} />}
      {!isLoading && !hasLoadingError &&
        <div>
          <h2>{title}</h2>
          {children}
        </div>
      }
    </div>
  </LogScope>
);
