import * as React from 'react';

import Date from 'bux/components/Date';
import { Section } from 'bux/components/Wizard';

export interface PeriodProps {
  title: string | React.ReactNode;
  startDate: string;
  endDate: string;
}

export const Period: React.SFC<PeriodProps> = ({ title, startDate, endDate }) => (
  <Section title={title}>
    <div className="test-period-info"><Date value={startDate} /> - <Date value={endDate} /></div>
  </Section>
);
