import { Trans } from '@lingui/react';
import * as React from 'react';

import { getHostname } from 'bux/common/helpers/browser';
import { Section } from 'bux/components/Wizard';

import { Text } from './Text';

export interface EstimateProps {
  title: string | React.ReactNode;
  isOrg: boolean;
  name: string;
  info?: string | React.ReactNode;
}

export const Estimate: React.SFC<EstimateProps> = ({ title, info, isOrg, name, children }) => {
  const siteName = getHostname();

  return (
    <Section title={title}>
      <p className="test-tenant-info">{
        isOrg
          ? <Trans id="billing.payment-wizard.estimate.organization">Organization: {name}</Trans>
          : <Trans id="billing.payment-wizard.estimate.site">Site: {siteName}</Trans>
      }
      </p>
      {info && <Text>{info}</Text>}
      {children}
    </Section>
  );
};
