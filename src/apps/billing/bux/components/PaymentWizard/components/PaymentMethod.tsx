import { Trans } from '@lingui/react';
import * as React from 'react';

import Card from 'bux/components/blocks/Card';
import { default as PaymentMethodPanel } from 'bux/components/PaymentMethod';
import { Section } from 'bux/components/Wizard';

import styles from './style.less';

export interface PaymentMethodProps {
  method: string;
  details: any;
}

export const PaymentMethod: React.SFC<PaymentMethodProps> = ({ method, details }) => (
  <Section title={<Trans id="billing.payment-wizard.payment-method.title">Payment method</Trans>}>
    <Card className={styles.paymentMethodCard}>
      <PaymentMethodPanel paymentMethod={method} details={details} />
    </Card>
  </Section>
);
