import { Trans } from '@lingui/react';
import cx from 'classnames';
import * as React from 'react';

import { createAndFireEvent as akCreateAndFireEvent } from '@atlaskit/analytics-next';

import { Analytics, withAnalyticsEmitter } from 'bux/components/Analytics';
import { ExternalLinkTo } from 'bux/components/Link';

import styles from './style.less';

export interface AgreementProps {
  isBusy: boolean;
  onAgreedChanged(agreed: boolean): void;
}

const InputWithAnalytics = withAnalyticsEmitter('input', 'checkbox', {
  onChange: {
    action: 'clicked',
  },
})('input');

interface AgreementCheckboxProps {
  name?: string;
  input: any;
  label: any;
}

const AgreementCheckbox = ({ name, input = {}, label = {} }: AgreementCheckboxProps) => (
  <React.Fragment>
    <InputWithAnalytics
      type="checkbox"
      name={name}
      {...input}
    />
    <label
      htmlFor={name}
      {...label}
    />
  </React.Fragment>
);

export class Agreement extends React.Component<AgreementProps> {
  public state = { agreed: false };

  public onChangeAgreement = e => {
    akCreateAndFireEvent('atlaskit')({ action: 'click', actionSubject: 'checkbox' });
    this.setState({ agreed: e.target.checked });
    this.props.onAgreedChanged(e.target.checked);
  };

  public render() {
    const { isBusy } = this.props;
    const { agreed = false } = this.state;

    return (
      <div className={cx(styles.agreement, styles.flowAdviseText, 'agreement-section')}>
        <Analytics.UI as="acceptCheckBox">
          <AgreementCheckbox
            name="accept"
            input={{
              id: 'accept',
              'aria-checked': agreed,
              'aria-labelledby': 'accept-label',
              defaultChecked: agreed,
              disabled: isBusy,
              onChange: this.onChangeAgreement,
            }}
            label={{
              id: 'accept-label',
              children: (
                <Trans id="billing.payment-wizard.agreement.accepting">
                  I agree to the Atlassian{' '}
                  <ExternalLinkTo.CloudTermsOfService>
                    Cloud Terms of Service
                  </ExternalLinkTo.CloudTermsOfService>{' '}
                  and{' '}
                  <ExternalLinkTo.PrivacyPolicy>
                    Privacy Policy
                  </ExternalLinkTo.PrivacyPolicy>
                  .
                </Trans>
              ),
            }}
          />
        </Analytics.UI>
      </div>
    );
  }
}
