import * as React from 'react';

import AkSectionMessage from '@atlaskit/section-message';

export interface ErrorMessageProps {
  title: string | React.ReactNode;
  className?: string;
}

export const ErrorMessage: React.SFC<ErrorMessageProps> = ({ children, title, className }) => (
  <div className={className} data-test="billing-error">
    <AkSectionMessage appearance="error" title={title}>{children}</AkSectionMessage>
  </div>
);
