import cx from 'classnames';
import * as React from 'react';

import styles from './style.less';

export interface TextProps {
  className?: string;
}

export const Text: React.SFC<TextProps> = ({ children, className }) => (
  <div className={cx(styles.informText, className)}>{children}</div>
);
