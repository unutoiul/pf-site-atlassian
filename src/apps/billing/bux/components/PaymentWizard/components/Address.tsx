import { Trans } from '@lingui/react';
import * as React from 'react';

import { Section } from 'bux/components/Wizard';

import styles from './style.less';

export interface AddressProps {
  organisationName: string;
  country: string;
  addressLineFull: string;
}

export const Address: React.SFC<AddressProps> = ({ organisationName, country, addressLineFull }) => (
  <Section title={<Trans id="billing.payment-wizard.address.title">Billing address</Trans>}>
    <p className={styles.address} data-test="address">
      {`${organisationName}, ${addressLineFull}, ${country}`}
    </p>
  </Section>
);
