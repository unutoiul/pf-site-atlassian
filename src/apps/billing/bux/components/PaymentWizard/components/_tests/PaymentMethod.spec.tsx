import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';

import { default as PaymentMethodPanel } from 'bux/components/PaymentMethod';

import { PaymentMethod, PaymentMethodProps } from '../PaymentMethod';

describe('PaymentWizard', () => {
  describe('Billing', () => {
    describe('PaymentMethod', () => {
      it('should render component', () => {
        const props: PaymentMethodProps = {
          method: 'method',
          details: {
            foo: 'bar',
          },
        };

        const wrapper = mount(<PaymentMethod {...props} />);
        expect(wrapper).to.be.present();
        expect(wrapper.find(PaymentMethodPanel)).to.have.props({
          paymentMethod: props.method,
          details: props.details,
        });
      });
    });
  });
});
