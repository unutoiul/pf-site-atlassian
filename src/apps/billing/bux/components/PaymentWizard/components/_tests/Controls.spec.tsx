import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';

import { StepButtons } from 'bux/components/Wizard';

import { Controls, ControlsProps } from '../Controls';

describe('PaymentWizard', () => {
  describe('Billing', () => {
    describe('Address', () => {
      const props: ControlsProps = {
        isFirstStep: false,
        isBusy: false,
        isSubmitDisabled: false,
        submitLabel: 'label',
        submitName: 'name',
        onSubmit: () => (0),
        onCancel: () => (0),
      };

      it('should render component', () => {
        const wrapper = mount(<Controls {...props} />);
        expect(wrapper).to.be.present();
        expect(wrapper.find(StepButtons)).to.have.props({
          isFirstStep: props.isFirstStep,
          submitDisabled: false,
          submitBusy: props.isBusy,
          submitLabel: props.submitLabel,
          submitName: props.submitName,
          // tslint:disable-next-line: no-unbound-method
          onSubmit: props.onSubmit,
          // tslint:disable-next-line: no-unbound-method
          onBack: props.onCancel,
          backDisabled: props.isBusy,
        });
      });

      it('should disable submit if isBusy', () => {
        const wrapper = mount(<Controls {...props} isBusy={true} />);
        expect(wrapper).to.be.present();
        expect(wrapper.find(StepButtons)).to.have.prop('submitDisabled', true);
      });

      it('should disable submit if isSubmitDisabled', () => {
        const wrapper = mount(<Controls {...props} isSubmitDisabled={true} />);
        expect(wrapper).to.be.present();
        expect(wrapper.find(StepButtons)).to.have.prop('submitDisabled', true);
      });
    });
  });
});
