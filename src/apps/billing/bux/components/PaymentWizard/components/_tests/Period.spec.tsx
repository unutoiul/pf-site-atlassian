import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';

import { Section } from 'bux/components/Wizard';

import { Period, PeriodProps } from '../Period';

describe('PaymentWizard', () => {
  describe('Billing', () => {
    describe('Period', () => {
      it('should render component', () => {
        const props: PeriodProps = {
          title: 'title',
          startDate: '2018-01-01',
          endDate: '2018-01-01',
        };
        const wrapper = mount(<Period {...props} />);
        expect(wrapper).to.be.present();
        expect(wrapper.find(Section)).to.have.prop('title', props.title);
        expect(wrapper).to.contain.text('Jan 1, 2018');
        expect(wrapper).to.contain.text('Jan 1, 2018');
      });
    });
  });
});
