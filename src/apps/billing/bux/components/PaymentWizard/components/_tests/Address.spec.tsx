import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';

import { Address, AddressProps } from '../Address';

describe('PaymentWizard', () => {
  describe('Billing', () => {
    describe('Address', () => {
      it('should render component', () => {
        const props: AddressProps = {
          organisationName: 'organisationName',
          country: 'country',
          addressLineFull: 'addressLineFull',
        };
        const wrapper = mount(<Address {...props} />);
        expect(wrapper).to.be.present();
        expect(wrapper).to.contain.text(props.organisationName);
        expect(wrapper).to.contain.text(props.country);
        expect(wrapper).to.contain.text(props.addressLineFull);
      });
    });
  });
});
