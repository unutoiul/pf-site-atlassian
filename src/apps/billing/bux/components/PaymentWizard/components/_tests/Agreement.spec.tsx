import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import { ExternalLink } from 'bux/components/Link';

import { Agreement, AgreementProps } from '../Agreement';

describe('PaymentWizard', () => {
  describe('Billing', () => {
    describe('Agreement', () => {
      const props: AgreementProps = {
        isBusy: false,
        onAgreedChanged: sinon.stub(),
      };
      it('should render component', () => {
        const wrapper = mount(<Agreement {...props} />);
        expect(wrapper).to.be.present();
        expect(
          wrapper.find(ExternalLink).filter({ logPrefix: 'cloudTermsOfService' }),
        ).to.have.prop('href', 'https://www.atlassian.com/legal/cloud-terms-of-service');
        expect(
          wrapper.find(ExternalLink).filter({ logPrefix: 'privacyPolicy' }),
        ).to.have.prop('href', 'https://www.atlassian.com/legal/privacy-policy');
        expect(wrapper).to.have.state('agreed', false);

        (wrapper.instance() as Agreement).onChangeAgreement({ target: { checked: true } });
        wrapper.update();
        expect(wrapper).to.have.state('agreed', true);
      });
    });
  });
});
