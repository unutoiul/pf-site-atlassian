import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';

import AkSectionMessage from '@atlaskit/section-message';

import { ErrorMessage, ErrorMessageProps } from '../ErrorMessage';

describe('PaymentWizard', () => {
  describe('Billing', () => {
    describe('Error', () => {
      it('should render component', () => {
        const props: ErrorMessageProps = {
          title: 'title',
        };
        const wrapper = mount(<ErrorMessage {...props}>error message</ErrorMessage>);
        expect(wrapper).to.be.present();
        expect(wrapper.find(AkSectionMessage)).to.have.props({
          title: props.title,
        });
        expect(wrapper).to.contain.text('error message');
      });
    });
  });
});
