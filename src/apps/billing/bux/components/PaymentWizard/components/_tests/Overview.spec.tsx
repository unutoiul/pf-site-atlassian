import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';

import Loading from 'bux/components/Loading/delayed';
import LogScope from 'bux/components/log/Scope';
import { StepUnavailableError } from 'bux/components/Wizard';

import { Overview, OverviewProps } from '../Overview';

describe('PaymentWizard', () => {
  describe('Billing', () => {
    describe('Overview', () => {
      const props: OverviewProps = {
        title: 'title',
      };

      it('should render component', () => {
        const wrapper = mount(<Overview {...props} />);
        expect(wrapper).to.be.present();
        expect(wrapper.find('h2')).to.have.text('title');
        expect(wrapper.find(Loading)).to.not.be.present();
        expect(wrapper.find(StepUnavailableError)).to.not.be.present();
      });

      it('should have log scope', () => {
        const wrapper = mount(<Overview {...props} logPrefix="log" />);
        expect(wrapper).to.be.present();
        expect(wrapper.find(LogScope)).to.have.prop('prefix', 'log');
      });

      it('should have log scope', () => {
        const wrapper = mount(<Overview {...props} isLoading={true} ><div id="child" /></Overview>);
        expect(wrapper).to.be.present();
        expect(wrapper.find(Loading)).to.be.present();
        expect(wrapper.find('div#child')).to.not.be.present();
      });

      it('should render loading', () => {
        const wrapper = mount(<Overview {...props} isLoading={true} ><div id="child" /></Overview>);
        expect(wrapper).to.be.present();
        expect(wrapper.find(Loading)).to.be.present();
        expect(wrapper.find('div#child')).to.not.be.present();
      });

      it('should render error', () => {
        const wrapper = mount(
          <Overview {...props} hasLoadingError={true} loadingErrorMsg="error" ><div id="child" /></Overview>,
        );
        expect(wrapper).to.be.present();
        expect(wrapper.find(StepUnavailableError)).to.be.present();
        expect(wrapper.find(StepUnavailableError)).to.contain.text('error');
        expect(wrapper.find('div#child')).to.not.be.present();
      });
    });
  });
});
