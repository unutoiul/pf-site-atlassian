import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';

import { Section } from 'bux/components/Wizard';

import { Estimate, EstimateProps } from '../Estimate';
import { Text } from '../Text';

describe('PaymentWizard', () => {
  describe('Billing', () => {
    describe('Estimate', () => {
      const props: EstimateProps = {
        title: 'title',
        isOrg: false,
        name: 'name',
      };

      it('should render component', () => {
        const wrapper = mount(<Estimate {...props} />);
        expect(wrapper).to.be.present();
        expect(wrapper.find(Section)).to.have.prop('title', props.title);
        expect(wrapper.find(Section)).to.contain.text('Site');
        expect(wrapper.find(Section)).to.contain.text('nodejs');
        expect(wrapper.find(Text)).to.not.be.present();
      });

      it('should render Organization', () => {
        const wrapper = mount(<Estimate {...props} isOrg={true} />);
        expect(wrapper).to.be.present();
        expect(wrapper.find(Section)).to.contain.text('Organization');
      });

      it('should render info text', () => {
        const wrapper = mount(<Estimate {...props} info="test" />);
        expect(wrapper).to.be.present();
        expect(wrapper.find(Text)).to.contain.text('test');
      });
    });
  });
});
