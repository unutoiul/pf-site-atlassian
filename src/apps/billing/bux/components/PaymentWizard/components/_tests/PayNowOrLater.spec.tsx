import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import { StaticRouter } from 'react-router-dom';
import * as sinon from 'sinon';

import { AkRadio } from '@atlaskit/field-radio-group';

import { default as PaymentMethodPanel } from 'bux/components/PaymentMethod';

import { OPTION_BANK_TRANSFER, OPTION_ON_FILE } from '../../constants';
import { PayNowOrLater, PayNowOrLaterProps } from '../PayNowOrLater';

describe('PaymentWizard', () => {
  describe('Billing', () => {
    describe('PayNowOrLater', () => {
      const props: PayNowOrLaterProps = {
        method: 'TOKENIZED_CREDIT_CARD',
        details: {
          foo: 'bar',
        },
        nextRenewalDate: '2018-01-01',
      };

      it('should render component', () => {
        const wrapper = mount(
          <StaticRouter context={{}}><PayNowOrLater {...props} /></StaticRouter>,
        );
        expect(wrapper).to.be.present();
        expect(wrapper.find(PaymentMethodPanel)).to.have.props({
          paymentMethod: props.method,
          details: props.details,
        });
        expect(wrapper.find(AkRadio).first().find('label')).to.contain.text('Credit card');
        expect(wrapper.find(AkRadio).first()).to.have.props({
          isSelected: true,
          name: 'payment-method',
          value: OPTION_ON_FILE,
        });
        expect(wrapper.find(AkRadio).last()).to.have.props({
          isSelected: false,
          name: 'payment-method',
          value: OPTION_BANK_TRANSFER,
        });
      });

      it('should change payment option', () => {
        const onPaymentMethodChange = sinon.stub();
        const wrapper = mount(
          <StaticRouter context={{}}>
            <PayNowOrLater {...props} onPaymentMethodChange={onPaymentMethodChange} />
          </StaticRouter>,
        );
        expect(wrapper).to.be.present();

        const bankTransferOption = wrapper.find(AkRadio).last();
        const target = { value: OPTION_BANK_TRANSFER };
        bankTransferOption.find('input').simulate('change', { target });

        expect(wrapper.find(AkRadio).last()).to.have.props({
          isSelected: true,
        });

        expect(onPaymentMethodChange).to.have.been.calledWith(target.value);
      });
    });
  });
});
