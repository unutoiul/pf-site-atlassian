import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';

import { Text } from '../Text';

describe('PaymentWizard', () => {
  describe('Billing', () => {
    describe('Text', () => {
      it('should render component', () => {
        const wrapper = mount(<Text>test</Text>);
        expect(wrapper).to.be.present();
        expect(wrapper.find('div')).to.contain.text('test');
      });
    });
  });
});
