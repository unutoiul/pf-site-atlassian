import * as React from 'react';

import { Footer, StepButtons } from 'bux/components/Wizard';

export interface ControlsProps {
  isFirstStep: boolean;
  isBusy: boolean;
  isSubmitDisabled: boolean;
  submitLabel: string | React.ReactNode;
  submitName: string;
  onSubmit();
  onCancel();
}

export const Controls: React.SFC<ControlsProps> = (
  { isFirstStep, isBusy, isSubmitDisabled, submitLabel, submitName, onSubmit, onCancel },
) => (
  <Footer>
    <StepButtons
      isFirstStep={isFirstStep}
      submitDisabled={isSubmitDisabled || isBusy}
      submitBusy={isBusy}
      submitLabel={submitLabel}
      submitName={submitName}
      onSubmit={onSubmit}
      onBack={onCancel}
      backDisabled={isBusy}
      primaryAnalyticsName="subscribeButton"
    />
  </Footer>
);
