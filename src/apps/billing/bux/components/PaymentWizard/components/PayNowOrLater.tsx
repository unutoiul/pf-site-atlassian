import { Trans } from '@lingui/react';
import * as React from 'react';

import { AkRadio } from '@atlaskit/field-radio-group';
import AkEditorInfoIcon from '@atlaskit/icon/glyph/editor/info';
import { colors as AkColors } from '@atlaskit/theme';

import Card from 'bux/components/blocks/Card';
import Date from 'bux/components/Date';
import { InlineDialog } from 'bux/components/InlineDialog';
import Link from 'bux/components/Link/Link';
import { default as PaymentMethodPanel } from 'bux/components/PaymentMethod';
import { Section } from 'bux/components/Wizard';
import { PAYMENT_METHOD_CREDIT_CARD } from 'bux/core/state/billing-details/constants';

import { OPTION_BANK_TRANSFER, OPTION_ON_FILE } from '../constants';
import styles from './style.less';
import { Text } from './Text';

const LearnMoreContent = () => (
  <div>
    <p>
      <Trans id="billing.payment-wizard.pay-now-or-later.learn-more.text">
        If payment is not received by this date, your monthly renewal will be processed as scheduled to prevent a disruption in service. If payment is processed on or after that date, the annual billing period will be adjusted accordingly.
      </Trans>
    </p>
    <p>
      <Trans id="billing.payment-wizard.pay-now-or-later.learn-more.note">
        <strong>Note:</strong> This invoice will no longer be valid if you add new products to the site after it’s been generated.
      </Trans>
    </p>
  </div>
);

export interface PayNowOrLaterProps {
  method: string;
  details: any;
  nextRenewalDate: string;
  onPaymentMethodChange?(selected: string);
}

export class PayNowOrLater extends React.Component<PayNowOrLaterProps> {
  public state = {
    selectedValue: OPTION_ON_FILE,
  };

  public onRadioChange = (event) => {
    const selectedValue = event.target.value;
    this.setState({ selectedValue });
    if (this.props.onPaymentMethodChange) {
      this.props.onPaymentMethodChange(selectedValue);
    }
  };

  public render() {
    const { selectedValue } = this.state;
    const { method, details, nextRenewalDate } = this.props;

    return (
      <Section title={<Trans id="billing.payment-wizard.pay-now-or-later.title">Payment method</Trans>}>
        <div className={styles.paymentOption} data-test="payment-option-current">
          <AkRadio
            isSelected={selectedValue === OPTION_ON_FILE}
            name="payment-method"
            onChange={this.onRadioChange}
            value={OPTION_ON_FILE}
          >
            {
              method === PAYMENT_METHOD_CREDIT_CARD
                ? <Trans id="billing.payment-wizard.pay-now-or-later.credit-card">Credit card</Trans>
                : 'PayPal'
            }
          </AkRadio>
          <Card className={styles.paymentMethodCard}>
            <PaymentMethodPanel paymentMethod={method} details={details} />
          </Card>
          <Text>
            <Trans id="billing.payment-wizard.pay-now-or-later.update-billing-details">
              To update your payment method, go to&nbsp;
              <Link to="/paymentdetails" name="to-billing-details">Billing details</Link>.
            </Trans>
          </Text>
        </div>
        <div className={styles.paymentOption} data-test="payment-option-bank-transfer">
          <AkRadio
            isSelected={selectedValue === OPTION_BANK_TRANSFER}
            name="payment-method"
            onChange={this.onRadioChange}
            value={OPTION_BANK_TRANSFER}
          >
            <Trans id="billing.payment-wizard.pay-now-or-later.bank-transfer">
              Bank transfer, check or ACH
            </Trans>
          </AkRadio>
          <Text>
            <Trans id="billing.payment-wizard.pay-now-or-later.process-payment-before">
              Be sure to process your payment before your next renewal date (<Date value={nextRenewalDate} />).
            </Trans>&nbsp;
            <InlineDialog content={<LearnMoreContent />}>
              <AkEditorInfoIcon size="small" primaryColor={AkColors.P300} label="" />
            </InlineDialog>
          </Text>
        </div>
      </Section>
    );
  }
}
