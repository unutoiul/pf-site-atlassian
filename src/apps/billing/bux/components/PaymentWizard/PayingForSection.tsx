import * as React from 'react';

import Date from 'bux/components/Date';

import { Section } from 'bux/components/Wizard';

export interface Props {
  isOrg: boolean;
  name: string;
  startDate?: string;
  endDate?: string;
  className?: string;
}

export const PayingForSection: React.SFC<Props> = ({ isOrg, name, startDate, endDate, className }) => (
  <Section className={className}>
    <hr />
    <ul>
      <li title={name}><strong>{isOrg ? 'Organization' : 'Site'}:</strong> {name}</li>
      {
        startDate && endDate
        && <li><strong>Billing period:</strong> <Date value={startDate} /> - <Date value={endDate} /></li>
      }
    </ul>
  </Section>
);
