import React from 'react';
import { any, string } from 'prop-types';
import cx from 'classnames';
import { akColorR400 } from '@atlaskit/util-shared-styles';
import Card from 'bux/components/blocks/Card';
import ErrorIcon from '@atlaskit/icon/glyph/error';
import styles from './PaymentMethodLoadingError.less';

const PaymentMethodLoadingError = ({ children, className }) => (
  <Card className={cx(styles.container, className)}>
    <div className={styles.icon}>
      <ErrorIcon primaryColor={akColorR400} />
    </div>
    <div>
      {children}
    </div>
  </Card>
);

PaymentMethodLoadingError.propTypes = {
  children: any.isRequired,
  className: string
};

export default PaymentMethodLoadingError;
