import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import PaymentMethodLoadingError from '../PaymentMethodLoadingError';

describe('PaymentMethodLoadingError: ', () => {
  it('should render child component', () => {
    const wrapper = shallow(<PaymentMethodLoadingError><p>message</p></PaymentMethodLoadingError>);
    expect(wrapper).to.be.present();
    expect(wrapper.find('p')).to.be.present();
    expect(wrapper.find('p')).to.have.text('message');
  });
});
