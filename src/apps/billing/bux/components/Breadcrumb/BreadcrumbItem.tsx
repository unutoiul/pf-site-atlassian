import * as React from 'react';

import { BreadcrumbsItem as AkBreadcrumbsItem } from '@atlaskit/breadcrumbs';

import Link from 'bux/components/Link';

import { withUrlTransformer } from '../Page/with/urlTransformer';

const BreadcrumbRouterComp = ({ urlTransform, href, className, onMouseDown, onMouseEnter, onMouseLeave, children }) => (
  <Link
    urlTransform={urlTransform}
    to={href}
    className={className}
    onMouseDown={onMouseDown}
    onMouseEnter={onMouseEnter}
    onMouseLeave={onMouseLeave}
  >
    {children}
  </Link>
);

export interface Props {
  href: string;
  text: React.ReactNode;
}

const BreadcrumbRouterImpl = (props: Props) => (
  <AkBreadcrumbsItem
    component={BreadcrumbRouterComp}
    truncationWidth={200}
    {...props}
  />
);

export const BreadcrumbRouter = withUrlTransformer(BreadcrumbRouterImpl);

export const BreadcrumbLink = (props) => (
  <AkBreadcrumbsItem
    truncationWidth={200}
    {...props}
  />
);
