import { I18n } from '@lingui/react';
import * as React from 'react';
import { connect } from 'react-redux';

import { BreadcrumbsStateless as AKBreadcrumbsStateless } from '@atlaskit/breadcrumbs';

import { getTenantInfo } from 'bux/core/state/meta/selectors';

import { BreadcrumbLink, BreadcrumbRouter } from './BreadcrumbItem';

export const AdminBreadcrumb = ({ isOrg, ...props }) => (
  <I18n>{({ i18n }) => (
    <BreadcrumbLink
      href={isOrg ? '/' : '/admin'}
      text={i18n.t('billing.bread-crumb.admin')`Admin`}
      {...props}
    />
  )}</I18n>
);

export const OrganizationBreadcrumb = ({ id, name, ...props }) => (
  <BreadcrumbLink href={`/o/${id}/overview`} text={name} {...props} />
);

export const BillingBreadcrumb = (props) => (
  <I18n>{({ i18n }) => (
  <BreadcrumbRouter
    href="/overview"
    text={i18n.t('billing.bread-crumb.billing')`Billing`}
    {...props}
  />
  )}</I18n>
);

const BreadcrumbsComponent = ({ tenantInfo: { id, name, isOrg }, children }) => (
  <div data-test="breadcrumb">
    <AKBreadcrumbsStateless>
      <AdminBreadcrumb isOrg={isOrg} />
      {isOrg && <OrganizationBreadcrumb id={id} name={name} />}
      {children}
    </AKBreadcrumbsStateless>
  </div>
);

export const mapStateToProps = state => ({
  tenantInfo: getTenantInfo(state),
});

export const Breadcrumbs = connect(mapStateToProps)(BreadcrumbsComponent);
