import { I18n } from '@lingui/react';
import * as React from 'react';

import { PageParentLink } from 'common/page-parent-link/page-parent-link';

import { withUrlTransformer } from 'bux/components/Page/with/urlTransformer';

export const BackToBillingLink = withUrlTransformer(({ urlTransform }) => (
  <I18n>{({ i18n }) => (
    <PageParentLink
      linkText={i18n.t('billing.bread-crumb.billing')`Billing`}
      linkLocation={urlTransform ? urlTransform('/overview') : '/'}
    />
  )}
  </I18n>
));
