import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

export class ShowWithDelay extends PureComponent {
  static propTypes = {
    children: PropTypes.node,
    delay: PropTypes.number,
  };

  static defaultProps = {
    delay: 300
  };

  state = {
    timePassed: false
  };

  componentWillMount() {
    this.timer = setTimeout(() => this.setState({ timePassed: true }), this.props.delay);
  }

  componentWillUnmount() {
    clearTimeout(this.timer);
  }

  render() {
    return this.state.timePassed ? React.Children.only(this.props.children) : null;
  }
}

const showWithDelay = (Component, delay = 300) =>
  props => <ShowWithDelay delay={delay}><Component {...props} /></ShowWithDelay>;

export default showWithDelay;