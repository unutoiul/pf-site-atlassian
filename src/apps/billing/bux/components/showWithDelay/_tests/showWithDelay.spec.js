import PropTypes from 'prop-types';
import React from 'react';
import sinon from 'sinon';
import { expect } from 'chai';
import { mount } from 'enzyme';
import showWithDelay, { ShowWithDelay } from '../showWithDelay';

describe('showWithDelay HoC: ', () => {
  it('should show component after delay', () => {
    const clock = sinon.useFakeTimers();
    const Component = ({ value }) => <div>value is {value}</div>;
    Component.propTypes = {
      value: PropTypes.number
    };
    const Delayed = showWithDelay(Component);
    const wrapper = mount(<Delayed value={42} />);

    expect(wrapper.find(ShowWithDelay)).to.be.present();
    expect(wrapper.find(Component)).not.to.be.present();
    clock.tick(1000);
    wrapper.update();
    expect(wrapper.find(Component)).to.be.present();
    expect(wrapper.find(Component)).to.contain.text('42');
    clock.restore();
  });
});