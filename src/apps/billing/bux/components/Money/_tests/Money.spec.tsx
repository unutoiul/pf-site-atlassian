import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';

import Money from '../Money';

describe('Money component', () => {
  it('should render amount with symbol if amount is not 0', () => {
    const wrapper = mount(<Money amount={10} currency="usd" />);
    expect(wrapper).to.have.text('$10');
  });

  it('should render amount with symbol with currency in upper case', () => {
    const wrapper = mount(<Money amount={10} currency="USD" />);
    expect(wrapper).to.have.text('$10');
  });

  it('should render free if amount is 0 and showFreeIfZero is true', () => {
    const wrapper = mount(<Money amount={0} currency="usd" showFreeIfZero />);
    expect(wrapper).to.have.text('Free');
  });

  it('should render zero if amount is 0', () => {
    const wrapper = mount(<Money amount={0} currency="usd" />);
    expect(wrapper).to.have.text('$0');
  });

  it('should render amount with symbol and currency if includeLabel is specified', () => {
    const wrapper = mount(<Money amount={10} currency="usd" includeLabel />);
    expect(wrapper).to.have.text('USD10.00');
  });

  it('should render amount with 2 decimals when having cents', () => {
    const wrapper = mount(<Money amount={10.5} currency="usd" includeLabel />);
    expect(wrapper).to.have.text('USD10.50');
  });

  it('should render amount with 2 decimals when more than 2', () => {
    const wrapper = mount(<Money amount={10.1501} currency="usd" includeLabel />);
    expect(wrapper).to.have.text('USD10.15');
  });
});
