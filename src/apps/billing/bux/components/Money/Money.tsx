import { Trans, withI18n } from '@lingui/react';
import cx from 'classnames';
import * as React from 'react';

import { formatMoney } from 'bux/common/formatters/money-formatter';

import { asMonospacedCost } from '../behaviour/font/monospaced/Monospaced';
import styles from './style.less';

interface MoneyProps {
  currency: string;
  amount?: number;
  withCents?: boolean;
  includeLabel?: boolean;
  showFreeIfZero?: boolean;
  productKey?: string;
  i18n: any;
}

const Money: React.SFC<MoneyProps> = ({
  currency, amount, showFreeIfZero, i18n: { language }, withCents, includeLabel, productKey,
}) => {
  if (productKey === 'opsgenie') {
    return <span className={cx('money', styles.MoneyNeverWrap)}>{asMonospacedCost('---')}</span>;
  }

  if (showFreeIfZero && amount === 0) {
    return <Trans id="billing.money.free">Free</Trans>;
  }

  return (
    <span className={cx('money', styles.MoneyNeverWrap)}>
      {asMonospacedCost(formatMoney(amount, { currency, language, withCents, includeLabel }))}
    </span>
  );
};

export default withI18n()(Money);
