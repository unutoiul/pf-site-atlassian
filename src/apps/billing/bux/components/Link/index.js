import component, { Link as InternalLink } from './Link';

export ExternalLink from './External';
export ExternalLinkTo from './ExternalLinkTo';
export { InternalLink };

export default component;
