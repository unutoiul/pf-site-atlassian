import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import getAnalytic from 'bux/components/log/helpers/getAnalytic';

import ExternalLink from '../External';

describe('External link component: ', () => {
  it('should render text', () => {
    const sendAnalyticEvent = sinon.stub();
    const wrapper = mount(<ExternalLink href="test">LinkText</ExternalLink>, {
      context: {
        sendAnalyticEvent,
      },
    });
    expect(wrapper).to.have.text('LinkText');
  });

  it('should trigger analytics', () => {
    const { context, sendAnalyticEvent } = getAnalytic();
    const wrapper = mount(<ExternalLink href="#">LinkText</ExternalLink>, context);

    wrapper.simulate('click');
    expect(sendAnalyticEvent).to.have.been.calledWithMatch(['click']);
  });

  it('should trigger analytics with prefix', () => {
    const { context, sendAnalyticEvent } = getAnalytic();
    const wrapper = mount(<ExternalLink href="#" logPrefix="prefix">LinkText</ExternalLink>, context);

    wrapper.simulate('click');
    expect(sendAnalyticEvent).to.have.been.calledWithMatch(['prefix', 'click']);
  });
});
