import PropTypes from 'prop-types';
import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';
import LinkButton from '../Button';

describe('LinkButton component: ', () => {
  it('should render text', () => {
    const wrapper = mount(<LinkButton href="test">LinkText</LinkButton>);
    expect(wrapper).to.have.text('LinkText');
  });

  it('should trigger analytics', () => {
    const sendAnalyticEvent = sinon.stub();
    const wrapper = mount(<LinkButton href="#">LinkText</LinkButton>, {
      context: {
        sendAnalyticEvent
      },
      childContextTypes: {
        sendAnalyticEvent: PropTypes.func
      }
    });

    wrapper.simulate('click');
    expect(sendAnalyticEvent).to.have.been.calledWithMatch('click');
  });
});