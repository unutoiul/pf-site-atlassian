import PropTypes from 'prop-types';
import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';
import { Provider } from 'bux/helpers/redux';
import createStore from 'bux/core/createStore';
import { DeepURLProvider, withUrlTransformerProvided } from '../../Page/with/urlTransformer';
import { Link } from '../Link';

describe('Link component: ', () => {
  it('should render text', () => {
    const wrapper = mount(<Provider store={createStore()}>
      <Link to={{ name: '/paymentdetails' }}>LinkText</Link>
    </Provider>);
    expect(wrapper).to.have.text('LinkText');
  });

  it('should trigger analytics', () => {
    const sendAnalyticEvent = sinon.stub();
    const wrapper = mount(<Provider store={createStore()}>
      <Link to={{ name: '/paymentdetails' }}>LinkText</Link>
    </Provider>, {
      context: {
        sendAnalyticEvent,
      },
      childContextTypes: {
        sendAnalyticEvent: PropTypes.func,
      },
    });

    wrapper.find(Link).simulate('click');
    expect(sendAnalyticEvent).to.have.been.calledWithMatch(['click']);
  });

  it('should trigger analytics with name', () => {
    const sendAnalyticEvent = sinon.stub();
    const wrapper = mount(<Provider store={createStore()}>
      <Link to={{ name: '/paymentdetails' }} name="my-link" logPrefix="prefix">LinkText</Link>
    </Provider>, {
      context: {
        sendAnalyticEvent,
      },
      childContextTypes: {
        sendAnalyticEvent: PropTypes.func,
      },
    });

    wrapper.find(Link).simulate('click');
    expect(sendAnalyticEvent).to.have.been.calledWithMatch(['prefix', 'my-link']);
  });

  it('should trigger analytics with prefix', () => {
    const sendAnalyticEvent = sinon.stub();
    const wrapper = mount(<Provider store={createStore()}>
      <Link to={{ name: '/paymentdetails' }} logPrefix="prefix">LinkText</Link>
    </Provider>, {
      context: {
        sendAnalyticEvent,
      },
      childContextTypes: {
        sendAnalyticEvent: PropTypes.func,
      },
    });

    wrapper.find(Link).simulate('click');
    expect(sendAnalyticEvent).to.have.been.calledWithMatch(['prefix', 'click']);
  });

  describe('Link withUrlTransformer', () => {
    it('should transform url when routing location has org param set', () => {
      const Dummy = withUrlTransformerProvided(({ children }) => (<div>{children}</div>));
      const wrapper = mount(<Provider>
        <DeepURLProvider path="root-path">
          <Dummy>
            <Link to="/paymentdetails">LinkText</Link>
          </Dummy>
        </DeepURLProvider>
      </Provider>);
      expect(wrapper.find('a')).to.have.prop('href', '/root-path/paymentdetails');
    });
  });
});
