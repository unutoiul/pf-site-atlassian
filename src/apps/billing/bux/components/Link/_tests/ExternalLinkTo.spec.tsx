import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import * as React from 'react';

import ExternalLink from '../External';
import ExternalLinkTo from '../ExternalLinkTo';

describe('ExternalLinkTo', () => {
  it('should render Faq', () => {
    const wrapper = shallow(<ExternalLinkTo.Faq>test</ExternalLinkTo.Faq>);
    expect(wrapper.find(ExternalLink)).to.have.prop('href', 'https://www.atlassian.com/licensing/cloud');
  });
  it('should render FaqOrg', () => {
    const wrapper = shallow(<ExternalLinkTo.FaqOrg>test</ExternalLinkTo.FaqOrg>);
    expect(wrapper.find(ExternalLink)).to.have.prop('href', 'https://www.atlassian.com/licensing/atlassian-access');
  });
  it('should render CloudPricing', () => {
    const wrapper = shallow(<ExternalLinkTo.CloudPricing>test</ExternalLinkTo.CloudPricing>);
    expect(wrapper.find(ExternalLink)).to.have.prop('href', 'https://www.atlassian.com/licensing/cloud');
  });
  it('should render CloudPricingOrg', () => {
    const wrapper = shallow(<ExternalLinkTo.CloudPricingOrg>test</ExternalLinkTo.CloudPricingOrg>);
    expect(wrapper.find(ExternalLink)).to.have.prop('href', 'https://www.atlassian.com/licensing/atlassian-access');
  });
  it('should render PurchasingLicense', () => {
    const wrapper = shallow(<ExternalLinkTo.PurchasingLicense>test</ExternalLinkTo.PurchasingLicense>);
    expect(wrapper.find(ExternalLink)).to.have.prop('href', 'https://www.atlassian.com/company/contact/purchasing-licensing');
  });
  it('should render Support', () => {
    const wrapper = shallow(<ExternalLinkTo.Support>test</ExternalLinkTo.Support>);
    expect(wrapper.find(ExternalLink)).to.have.prop('href', 'https://support.atlassian.com/');
  });
  it('should render BoldSupport', () => {
    const wrapper = shallow(<ExternalLinkTo.BoldSupport>test</ExternalLinkTo.BoldSupport>);
    expect(wrapper.find(ExternalLink))
      .to.have.prop('href').equal('https://support.atlassian.com/');
    expect(wrapper.find(ExternalLink))
      .and.have.prop('style').deep.equal({ fontWeight: 'bold' });
  });
  it('should render CloudTermsOfService', () => {
    const wrapper = mount(<ExternalLinkTo.CloudTermsOfService>test</ExternalLinkTo.CloudTermsOfService>);
    expect(wrapper.find(ExternalLink)).to.have.prop('href', 'https://www.atlassian.com/legal/cloud-terms-of-service');
  });
  it('should render SoftwareLicenseAgreement', () => {
    const wrapper = shallow(<ExternalLinkTo.SoftwareLicenseAgreement>test</ExternalLinkTo.SoftwareLicenseAgreement>);
    expect(wrapper.find(ExternalLink)).to.have.prop('href', 'https://www.atlassian.com/legal/software-license-agreement');
  });
  it('should render SummaryOfChanges', () => {
    const wrapper = shallow(<ExternalLinkTo.SummaryOfChanges>test</ExternalLinkTo.SummaryOfChanges>);
    expect(wrapper.find(ExternalLink)).to.have.prop('href', 'https://www.atlassian.com/legal/summary-of-changes');
  });
  it('should render CustomerAgreement', () => {
    const wrapper = shallow(<ExternalLinkTo.CustomerAgreement>test</ExternalLinkTo.CustomerAgreement>);
    expect(wrapper.find(ExternalLink)).to.have.prop('href', 'https://www.atlassian.com/legal/customer-agreement');
  });
  it('should render PrivacyPolicy', () => {
    const wrapper = mount(<ExternalLinkTo.PrivacyPolicy>test</ExternalLinkTo.PrivacyPolicy>);
    expect(wrapper.find(ExternalLink)).to.have.prop('href', 'https://www.atlassian.com/legal/privacy-policy');
  });
  it('should render AccessPricingBilling', () => {
    const wrapper = shallow(<ExternalLinkTo.AccessPricingBilling>test</ExternalLinkTo.AccessPricingBilling>);
    expect(wrapper.find(ExternalLink)).to.have.prop('href', 'https://confluence.atlassian.com/x/-fOEO');
  });
});
