import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { withAnalyticsEmitter } from 'bux/components/Analytics';
import { withUrlTransformer } from '../Page/with/urlTransformer';

/**
 * Wrapper around react-router link. Just have ability to log events
 * @param {string} [name] additional event prefix
 */
class LinkImpl extends Component {
  logClick = (event) => {
    if (this.props.onClick) {
      this.props.onClick(event);
    }

    const logPrefix = this.props.logPrefix;
    const logEvent = (logPrefix ? [logPrefix] : []).concat([this.props.name || 'click']);
    this.context.sendAnalyticEvent(logEvent, {
      prevented: event && event.defaultPrevented,
    });
  };

  render() {
    const {
      urlTransform, to, logPrefix, createAnalyticsEvent, ...rest
    } = this.props;
    const transformedTo = urlTransform ? urlTransform(to) : to;
    return (<RouterLink {...rest} to={transformedTo} onClick={this.logClick} />);
  }
}

LinkImpl.contextTypes = {
  sendAnalyticEvent: PropTypes.func,
};

LinkImpl.propTypes = {
  onClick: PropTypes.func,
  urlTransform: PropTypes.func,
  to: PropTypes.oneOfType([PropTypes.string, PropTypes.object]).isRequired,
  name: PropTypes.string,
  logPrefix: PropTypes.string,
  createAnalyticsEvent: PropTypes.func,
};

export const Link = withUrlTransformer(LinkImpl);

export default withAnalyticsEmitter('Link', 'link')(Link);
