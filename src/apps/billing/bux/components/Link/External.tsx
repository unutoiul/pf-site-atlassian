import * as PropTypes from 'prop-types';
import * as React from 'react';

import { withAnalyticsEmitter } from 'bux/components/Analytics';

interface ExternalProps {
  'data-test'?: string;
  href?: string;
  logPrefix?: string;
  createAnalyticsEvent?();
  onClick?(event: Event);
}

class ExternalLink extends React.Component<ExternalProps> {
  public static contextTypes = {
    sendAnalyticEvent: PropTypes.func.isRequired,
  };

  public logClick = (event) => {
    if (this.props.onClick) {
      this.props.onClick(event);
    }

    const logPrefix = this.props.logPrefix;
    const logEvent = [...(logPrefix ? [logPrefix] : []), 'click'];
    this.context.sendAnalyticEvent(logEvent, {
      prevented: event && event.defaultPrevented,
    });
  };

  public render() {
    // tslint:disable-next-line no-unused
    const { logPrefix, createAnalyticsEvent, ...anchorProps } = this.props;

    return (<a {...anchorProps} role="button" onClick={this.logClick} />);
  }
}

export default withAnalyticsEmitter('ExternalLink', 'link')(ExternalLink);
