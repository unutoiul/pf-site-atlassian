import { I18n } from '@lingui/react';
import * as React from 'react';

import config from 'bux/common/config';
import { Analytics } from 'bux/components/Analytics';

import ExternalLink from './External';

const asI18nLink = (id, defaultProps = {}, analyticsName?) => {
  const I18nLink = ({ children, ...props }) => (
    <I18n>{({ i18n }) => (
      <ExternalLink href={config.links(id, i18n.language)} {...defaultProps} {...props}>
        {children}
      </ExternalLink>)}
    </I18n>
  );
  if (analyticsName) {
    return (props) => (
      <Analytics.UI as={analyticsName}>
        <I18nLink {...props}/>
      </Analytics.UI>
    );
  }

  return I18nLink;
};

const ExternalLinkTo = {
  Faq: asI18nLink('faq'),
  FaqOrg: asI18nLink('faqOrg'),
  CloudPricing: asI18nLink('cloudPricing', {
    target: '_blank', logPrefix: 'cloudPricing',
  }, undefined),
  CloudPricingOrg: asI18nLink('cloudPricingOrg', {
    target: '_blank', logPrefix: 'cloudPricing',
  }),
  PurchasingLicense: asI18nLink('purchasingLicense'),
  Support: asI18nLink('support', {
    target: '_blank', rel: 'noopener noreferrer', logPrefix: 'links.contact',
  }),
  BoldSupport: asI18nLink('support', {
    target: '_blank', rel: 'noopener noreferrer', logPrefix: 'links.contact', style: { fontWeight: 'bold' },
  }),
  CloudTermsOfService: asI18nLink('cloudTermsOfService', {
    target: '_blank', logPrefix: 'cloudTermsOfService',
  }, 'cloudTermsOfServiceLink'),
  SoftwareLicenseAgreement: asI18nLink('softwareLicenseAgreement', {
    target: '_blank', logPrefix: 'softwareLicenseAgreement',
  }),
  SummaryOfChanges: asI18nLink('summaryOfChanges', {
    target: '_blank', logPrefix: 'summaryOfChanges',
  }),
  CustomerAgreement: asI18nLink('customerAgreement', {
    target: '_blank', logPrefix: 'customerAgreement',
  }),
  PrivacyPolicy: asI18nLink('privacyPolicy', {
    target: '_blank', logPrefix: 'privacyPolicy',
  }, 'privacyPolicyLink'),
  AccessPricingBilling: asI18nLink('accessPricingBilling', {
    logPrefix: 'learn-more',
  }),
};

export default ExternalLinkTo;
