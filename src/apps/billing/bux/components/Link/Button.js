import PropTypes from 'prop-types';
import React, { Component } from 'react';
import AKButton from '@atlaskit/button';

class LinkButton extends Component {
  logClick = (event, analyticsEvent) => {
    if (this.props.onClick) {
      this.props.onClick(event);
    }
    this.context.sendAnalyticEvent('click', {
      prevented: event && event.defaultPrevented
    });
    // Note that a analytics event with { ..., actionSubject: 'button' } also gets fired elsewhere
    analyticsEvent.update({
      actionSubject: 'link',
    }).fire('atlaskit');
  };

  render() {
    return (<AKButton appearance="link" spacing="none" {...this.props} onClick={this.logClick} />);
  }
}

LinkButton.contextTypes = {
  sendAnalyticEvent: PropTypes.func
};

LinkButton.propTypes = {
  href: PropTypes.string,
  onClick: PropTypes.func
};

export default LinkButton;
