import PropTypes from 'prop-types';
import React from 'react';
import loadable from 'react-loadable';
import { PageLoader } from '../withLoader';
import Loading from '../Loading/delayed';
import UnavailableError from '../error/UnavailableError';
import toLoading from '../deferred/makeLoadingComponent';

const PageLoadingComponent = () => <Loading />;
const PageErrorComponent = ({ retryImport }) => <UnavailableError onRetry={retryImport} />;
PageErrorComponent.propTypes = {
  retryImport: PropTypes.func
};

const withAsyncPageLoader = (metaSelector, mountAction, loaderProps = {}) => (loader) => {
  const componentLoaderProps = {
    mountAction,
    metaSelector
  };

  // preload
  loader().catch(() => {});

  const Loader = loadable({
    loader,
    loading: toLoading(PageLoadingComponent, PageErrorComponent)
  });

  const WithAsyncPageLoader = props => (
    <PageLoader
      loader={componentLoaderProps}
      loaderProps={loaderProps}
    >
      <Loader {...props} />
    </PageLoader>
  );

  return WithAsyncPageLoader;
};

export default withAsyncPageLoader;
