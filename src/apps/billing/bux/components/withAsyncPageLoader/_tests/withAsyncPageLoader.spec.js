import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { Provider } from 'bux/helpers/redux';
import { mount } from 'enzyme';

import UnavailableError from '../../error/UnavailableError';
import withAsyncPageLoader from '../withAsyncPageLoader';

describe('withAsyncPageLoader HoC: ', () => {
  it('should load component', (done) => {
    const metaSelector = sinon.stub().returns({ display: true });
    const mountAction = sinon.stub().returns(() => {});
    const onLoad = sinon.spy();

    const loader = withAsyncPageLoader(metaSelector, mountAction);

    const loadComponent = () => {
      onLoad();
      return import('./targetComponent.js');
    };

    const Component = loader(loadComponent);

    const wrapper = mount(<Provider><Component name="42" /></Provider>);

    expect(metaSelector).to.be.called();
    expect(mountAction).to.be.called();
    expect(onLoad).to.be.called();

    setImmediate(() => {
      wrapper.update();
      expect(wrapper).to.contain.text(42);
      done();
    });
  });

  it.skip('should display error is component is not found', (done) => {
    const metaSelector = sinon.stub().returns({ display: true });
    const mountAction = sinon.stub().returns(() => {
    });
    const loader = withAsyncPageLoader(metaSelector, mountAction);

    const loadComponent = () => import('./missingComponent.js');

    const Component = loader(loadComponent);
    const wrapper = mount(<Provider><Component name="42" /></Provider>);

    setImmediate(() => {
      wrapper.update();
      expect(wrapper.find(UnavailableError)).to.be.present();
      done();
    });
  });

  it.skip('should recover from an error', (done) => {
    const metaSelector = sinon.stub().returns({
      display: true
    });
    const mountAction = sinon.stub().returns(() => {
    });
    const loader = withAsyncPageLoader(metaSelector, mountAction);

    let breakLoad = true;
    const loadComponent = () => {
      const fileName = breakLoad ? './missingComponent.js' : './targetComponent.js';
      return import(fileName);
    };

    const Component = loader(loadComponent);
    const wrapper = mount(<Provider><Component name="42" /></Provider>);

    setImmediate(() => {
      wrapper.update();
      expect(wrapper.find(UnavailableError)).to.be.present();
      breakLoad = false;
      wrapper.find(UnavailableError).props().onRetry();
      setImmediate(() => {
        wrapper.update();
        expect(wrapper).to.contain.text(42);
        done();
      });
    });
  });
});
