import { Trans, I18n } from '@lingui/react';
import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';
import ExtendedPropTypes from 'bux/core/extended-proptypes';
import ConnectedContactDetailsManager from 'bux/features/manage-contacts';
import Scope from 'bux/components/log/Scope';
import { Analytics } from 'bux/components/Analytics';
import Widget, { EditLink } from '../Widget';
import styles from './InvoiceContactWidget.less';

export const InvoiceContact = ({ invoiceEmail }) => (
  <div>
    <Analytics.UI 
      as="billingDetailsEditButton" 
      attributes={{ linkContainer: 'billingContactsCard', objectType: 'billingContacts' }}
    >
      <EditLink to="/paymentdetails" />
    </Analytics.UI>
    <div className={styles.form}>
      <ConnectedContactDetailsManager primary />
      <div>
        <span className={styles.widgetLabel}>
          <Trans id="billing.widget.invoice-contact.send-copies">
            Send copies of invoices to
          </Trans>
        </span>
        <div className={cx('email', styles.email)}>
          {invoiceEmail ||
            <Trans id="billing.widget.invoice-contact.no-contact">
              No invoice contact set
            </Trans>
          }
        </div>
      </div>
    </div>
  </div>
);

InvoiceContact.propTypes = {
  invoiceEmail: PropTypes.string
};


const InvoiceContactWidget = ({ className, metadata, ...rest }) => (
  <I18n>{({ i18n }) => (
  <Widget
    name={i18n.t('billing.widget.invoice-contact.name')`Billing contacts`}
    errorMessage={{ name: i18n.t('billing.widget.invoice-contact.name.in-error-message')`Billing contacts` }}
    className={cx(className, 'invoice-contact-widget')}
    metadata={metadata}
  >
    <Scope prefix="billEstimate" className={styles.billingEmail}>
      <InvoiceContact {...rest} />
    </Scope>
  </Widget>
  )}</I18n>
);

InvoiceContactWidget.propTypes = {
  className: PropTypes.string,
  metadata: ExtendedPropTypes.metadata
};

export default InvoiceContactWidget;
