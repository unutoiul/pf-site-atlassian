import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Widget from '../../Widget';

import InvoiceContactWidget, { InvoiceContact } from '../InvoiceContactWidget';

const DEFAULT_PROPS = {
  email: '',
  billingContact: {}
};

const renderWidget = options => <InvoiceContact {...DEFAULT_PROPS} {...options} />;

describe('InvoiceContactWidget component: ', () => {
  it('should render company contact', () => {
    const invoiceEmail = shallow(renderWidget({ invoiceEmail: 'email@example.com' })).find('.email');
    expect(invoiceEmail).to.have.text('email@example.com');
  });

  it('should render empty contact', () => {
    const email = shallow(renderWidget()).find('.email');
    expect(email).to.have.text('No invoice contact set');
  });

  it('should be rendered inside Widget', () => {
    const meta = {};
    const wrapper = shallow(<InvoiceContactWidget metadata={meta} prop42={42} {...DEFAULT_PROPS} />);
    const widget = wrapper.find(Widget);
    expect(widget).to.have.prop('metadata', meta);
    expect(widget).to.have.prop('name', 'Billing contacts');

    expect(wrapper.find(InvoiceContact)).to.have.prop('prop42', 42);
  });
});
