import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Transparent from '../Transparent';

describe('Transparent component', () => {
  it('should render a child', () => {
    const Payload = () => (<div>payload</div>);
    const wrapper = shallow(<Transparent><Payload /></Transparent>);
    expect(wrapper.find(Payload)).to.be.present();
  });

  it('should pass props', () => {
    const wrapper = shallow(<Transparent style={{ width: 1 }} />);
    expect(wrapper).to.have.prop('style');
  });
});