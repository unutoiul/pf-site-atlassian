import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';
import style from './style.less';

const TransparentComponent = ({ children, className, ...rest }) => (
  <div className={cx(style.inline, className)} {...rest}>
    {children}
  </div>
);

TransparentComponent.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string
};

export default TransparentComponent;