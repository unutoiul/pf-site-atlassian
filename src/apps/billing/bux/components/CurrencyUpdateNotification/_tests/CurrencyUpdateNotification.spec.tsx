import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';

import CurrencyUpdateNotification, { Props } from '../CurrencyUpdateNotification';

describe('Currency Update Notification', () => {
  describe('CurrencyUpdateNotification - Next Bill in New Currency', () => {
    it('should render component - with generic message', () => {
      const props: Props = {
        upcomingCurrencyExtra : {
          currencyCode: 'JPY',
          period: {
            startDate: '2025-01-01T00:00:00.000-0600',
            endDate: '2023-02-01T00:00:00.000-0600',
            renewalFrequency: 'MONTHLY',
          },
        },
        isNextBillInNewCurrency: true,
      };

      const wrapper = mount(<CurrencyUpdateNotification {...props} />);
      expect(wrapper).to.be.present();
      expect(wrapper.find('div').first()).to.have.text('Your next bill will be in Japanese Yen.');
    });

    it('should render component - with date in message', () => {
      const props: Props = {
        upcomingCurrencyExtra : {
          currencyCode: 'JPY',
          period: {
            startDate: '2025-01-01T00:00:00.000-0600',
            endDate: '2023-02-01T00:00:00.000-0600',
            renewalFrequency: 'MONTHLY',
          },
        },
        isNextBillInNewCurrency: false,
      };
      const wrapper = mount(<CurrencyUpdateNotification {...props} />);
      expect(wrapper).to.be.present();
      expect(wrapper.find('div').last()).to.have.text('Your bill on Jan 1, 2025 will be in Japanese Yen.');
    });
  });
});
