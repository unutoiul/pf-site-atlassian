import { Select, Trans } from '@lingui/react';
import cx from 'classnames';
import * as React from 'react';

import AkSectionMessage from '@atlaskit/section-message';

import Date from 'bux/components/Date';
import { UpcomingCurrencyExtras } from 'bux/core/state/bill-estimate/types';

import styles from './styles.less';

export interface Props {
  upcomingCurrencyExtra: UpcomingCurrencyExtras;
  isNextBillInNewCurrency: boolean;
}

const CurrencyUpdateNotification: React.SFC<Props> = ({ isNextBillInNewCurrency, upcomingCurrencyExtra }) => {
  const upcomingCurrency = upcomingCurrencyExtra.currencyCode;

  return isNextBillInNewCurrency
    ? (
    <div className={cx(styles.noticesRow, 'next-bill-currency-change-message')}>
      <AkSectionMessage appearance="info">
        <Trans id="billing.estimate.upcoming-currency.message">
          Your next bill will be in <Select
            value={upcomingCurrency}
            JPY="Japanese Yen"
            other={`different currency(${upcomingCurrency})`}
          />.
        </Trans>
      </AkSectionMessage>
    </div>
    ) : (
    <div className={cx(styles.noticesRow, 'future-bill-currency-change-message')}>
      <AkSectionMessage appearance="info">
        <Trans id="billing.estimate.upcoming-currency-paymentDate.message">
          Your bill on <Date value={upcomingCurrencyExtra.period.startDate} /> will be in <Select
            value={upcomingCurrency}
            JPY="Japanese Yen"
            other={`different currency(${upcomingCurrency})`}
          />.
        </Trans>
      </AkSectionMessage>
    </div>
    );
};

export default CurrencyUpdateNotification;
