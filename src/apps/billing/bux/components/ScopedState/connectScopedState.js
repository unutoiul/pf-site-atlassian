import React from 'react';
import ScopedStateContext from './ScopedStateContext';

const connectScopedState = WrappedComponent => props => (
  <ScopedStateContext.Consumer>
    {injectedProps => <WrappedComponent {...props} {...injectedProps} />}
  </ScopedStateContext.Consumer>
);

export default connectScopedState;
