import React from 'react';
import { func } from 'prop-types';
import { expect } from 'chai';
import { mount } from 'enzyme';
import ScopedState from '../ScopedState';
import connectScopedState from '../connectScopedState';

const CompA = ({ push }) => (<p><button onClick={() => push({ foo: 'bar' })}>Push</button></p>);
CompA.propTypes = { push: func.isRequired };
const CompB = () => (<p>Comp B</p>);

describe('ScopedState: ', () => {
  const Component = () => {
    const ConnectedCompA = connectScopedState(CompA);
    const ConnectedCompB = connectScopedState(CompB);
    return (
      <ScopedState>
        <div>
          <ConnectedCompA />
          <ConnectedCompB />
        </div>
      </ScopedState>
    );
  };

  it('should component A push props to component B', () => {
    const wrapper = mount(<Component />);
    expect(wrapper.find('CompA')).to.be.present();
    expect(wrapper.find('CompB')).to.be.present();

    wrapper.find('CompA').find('button').simulate('click');
    expect(wrapper.find('CompB')).to.have.prop('foo', 'bar');
  });
});
