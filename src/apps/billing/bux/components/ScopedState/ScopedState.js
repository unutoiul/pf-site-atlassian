import React from 'react';
import { any } from 'prop-types';
import ScopedStateContext from './ScopedStateContext';

class ScopedState extends React.Component {
  state = { data: {} };

  push = (data) => {
    this.setState({ data: { ...this.state.data, ...data } });
  }

  render() {
    const { children } = this.props;
    const { data } = this.state;
    return (
      <ScopedStateContext.Provider value={{ push: this.push, ...data }}>
        {children}
      </ScopedStateContext.Provider>
    );
  }
}

ScopedState.propTypes = {
  children: any
};

export default ScopedState;
