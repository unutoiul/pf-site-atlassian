import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';

import { UnconnectApplication } from '../Application';

describe('Application: ', () => {
  const orgId = 'test';

  it.skip('should map state to props', () => {
    // side effect on saga
  });

  it('should dispatch reset state when org id changes', () => {
    const resetStateAction = sinon.spy();
    const getUserMetadata = sinon.spy();
    const startCSAT = sinon.spy();
    const wrapper = mount(<UnconnectApplication
      resetState={resetStateAction}
      getUserMetadata={getUserMetadata}
      startCSAT={startCSAT}
      orgId={orgId}
    />);
    expect(getUserMetadata).to.be.called();
    expect(startCSAT).to.be.called();
    expect(resetStateAction).not.to.be.called();
    wrapper.setProps({ orgId: 'some-org-uuid2', resetState: resetStateAction });
    expect(resetStateAction).to.be.called();
  });

  it('should not dispatch reset state when org id does not change', () => {
    const resetStateAction = sinon.spy();
    const getUserMetadata = sinon.spy();
    const startCSAT = sinon.spy();
    const wrapper = mount(<UnconnectApplication
      resetState={resetStateAction}
      getUserMetadata={getUserMetadata}
      startCSAT={startCSAT}
      orgId={orgId}
    />);

    wrapper.setProps({ orgId, resetState: resetStateAction });
    expect(resetStateAction).not.to.be.called();
  });
});
