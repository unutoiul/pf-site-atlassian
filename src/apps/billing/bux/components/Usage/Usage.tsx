import { Plural, Trans } from '@lingui/react';
import * as React from 'react';

import { formatLabel, formatTieredUsage, formatUnitCode, formatUsage } from 'bux/common/formatters/usage';

const enum UnitTypes {
  USERS = 'users',
  AGENTS = 'agents',
  GIGABYTES = 'gigabytes',
  BUILD_MINUTES = 'build_minutes',
}

const normalizeUnitCode = (unitCode) => {
  // Bill estimate returns units in a lower case plural, product recommendation in a uppercase singular format
  const normalizedUnitCode = unitCode.toLowerCase();
  if (normalizedUnitCode.endsWith('s')) {
    return normalizedUnitCode;
  }

  return `${normalizedUnitCode}s`;
};

export const UsageUnitLabel: React.SFC<{
  unitCode: string,
}> = ({ unitCode }) => {
  switch (normalizeUnitCode(unitCode)) {
    case UnitTypes.USERS:
      return <Trans id="billing.usage.unit-label.users">users</Trans>;

    case UnitTypes.AGENTS:
      return <Trans id="billing.usage.unit-label.agents">agents</Trans>;

    case UnitTypes.GIGABYTES:
      return <Trans id="billing.usage.unit-label.gigabytes">gigabytes</Trans>;

    case UnitTypes.BUILD_MINUTES:
      return <Trans id="billing.usage.unit-label.build_minutes">build minutes</Trans>;

    default:
      // fallback to default english name
      return <React.Fragment>{formatUnitCode(unitCode)}</React.Fragment>;
  }
};

export const UsageUnitSingularLabel: React.SFC<{
  unitCode: string,
}> = ({ unitCode }) => {
  switch (normalizeUnitCode(unitCode)) {
    case UnitTypes.USERS:
      return <Trans id="billing.usage.unit-label.user">user</Trans>;

    case UnitTypes.AGENTS:
      return <Trans id="billing.usage.unit-label.agent">agent</Trans>;

    case UnitTypes.GIGABYTES:
      return <Trans id="billing.usage.unit-label.gigabyte">gigabyte</Trans>;

    case UnitTypes.BUILD_MINUTES:
      return <Trans id="billing.usage.unit-label.build_minute">build minute</Trans>;

    default:
      // fallback to default english name
      return <React.Fragment>{formatLabel(1, unitCode)}</React.Fragment>;
  }
};

export const UnlimitedUsage: React.SFC = () => <Trans id="billing.usage.unit-count.unlimited">Unlimited</Trans>;

const Usage: React.SFC<{
  unitsLimit: number,
  unitCode: string,
  unlimited?: boolean,
}> = ({ unitsLimit, unitCode, unlimited = false }) => {

  if (unlimited) {
    return <UnlimitedUsage />;
  }

  switch (normalizeUnitCode(unitCode)) {
    case UnitTypes.USERS:
      return (
        <Plural
          id="billing.usage.unit-count.users"
          value={unitsLimit}
          one="# user"
          other="# users"
        />
      );

    case UnitTypes.AGENTS:
      return (
        <Plural
          id="billing.usage.unit-count.agents"
          value={unitsLimit}
          one="# agent"
          other="# agents"
        />
      );

    case UnitTypes.GIGABYTES:
      return (
        <Plural
          id="billing.usage.unit-count.gigabytes"
          value={unitsLimit}
          one="# gigabyte"
          other="# gigabytes"
        />
      );

    case UnitTypes.BUILD_MINUTES:
      return (
        <Plural
          id="billing.usage.unit-count.build_minutes"
          value={unitsLimit}
          one="# build minute"
          other="# build minutes"
        />
      );

    default:
      // fallback to default english name
      return <React.Fragment>{formatUsage(unitsLimit, unitCode)}</React.Fragment>;
  }
};

export const TieredUsage: React.SFC<{
  currentUnits: number,
  unitsLimit: number,
  unitCode: string,
  unlimited?: boolean,
}> = ({ currentUnits, unitsLimit, unitCode, unlimited = false }) => {

  if (unlimited) {
    return <UnlimitedUsage />;
  }

  switch (normalizeUnitCode(unitCode)) {
    case UnitTypes.USERS:
      return (
        <Trans id="billing.usage.tiered-unit-count.users">
          {currentUnits} of <Plural value={unitsLimit} one="# user" other="# users" />
        </Trans>
      );

    case UnitTypes.AGENTS:
      return (
        <Trans id="billing.usage.tiered-unit-count.agents">
          {currentUnits} of <Plural value={unitsLimit} one="# agent" other="# agents" />
        </Trans>
      );

    case UnitTypes.GIGABYTES:
      return (
        <Trans id="billing.usage.tiered-unit-count.gigabytes">
          {currentUnits} of <Plural value={unitsLimit} one="# gigabyte" other="# gigabytes" />
        </Trans>
      );

    case UnitTypes.BUILD_MINUTES:
      return (
        <Trans id="billing.usage.tiered-unit-count.build_minutes">
          {currentUnits} of <Plural value={unitsLimit} one="# build minute" other="# build minutes" />
        </Trans>
      );

    default:
      // fallback to default english name
      return <React.Fragment>{formatTieredUsage(unitsLimit, currentUnits, unitCode)}</React.Fragment>;
  }
};

export default Usage;
