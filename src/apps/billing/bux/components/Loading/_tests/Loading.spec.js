import React from 'react';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';
import Spinner from '@atlaskit/spinner';
import Loading from '../Loading';

describe('Loading component: ', () => {
  it('should show spinner', () => {
    const wrapper = shallow(<Loading />);
    expect(wrapper.find(Spinner)).to.have.length(1);
  });

  it('should show text when specified', () => {
    const wrapper = mount(<Loading />);
    expect(wrapper).to.have.text('Loading...');
  });
});
