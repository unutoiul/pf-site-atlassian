import { Trans } from '@lingui/react';
import PropTypes from 'prop-types';
import React from 'react';
import cx from 'classnames';
import Spinner from '@atlaskit/spinner';
import styles from './Loading.less';

const spinnerSizes = {
  block: 'large',
  page: 'medium',
  component: 'small',
  paragraph: 'medium',
};

const Loading = ({ mode = 'component' }) => (
  <div className={cx('loading', styles.loading, styles[mode])}>
    <Spinner size={spinnerSizes[mode]} />
    <span className={styles.text}>
      <Trans id="billing.loading">
        Loading...
      </Trans>
    </span>
  </div>
);

Loading.propTypes = {
  mode: PropTypes.oneOf(['paragraph', 'component', 'page', 'block']),
};

export default Loading;
