import showWithDelay from '../showWithDelay';
import Loading from './index';

export default showWithDelay(Loading);
