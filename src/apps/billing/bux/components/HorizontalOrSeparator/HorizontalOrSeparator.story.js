import { storiesOf } from '@storybook/react';
import { inContentChrome, render } from 'bux/common/helpers/storybook';
import OrSeparator from './HorizontalOrSeparator';

storiesOf('BUX|Components/HorizontalOrSeparator', module)
  .add('Horizontal Or Separator', () => inContentChrome(render(OrSeparator)));

