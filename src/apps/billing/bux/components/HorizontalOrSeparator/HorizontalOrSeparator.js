import React from 'react';
import styles from './styles.less';

const OrSeparator = () => (
  <div className={styles.separator}>OR</div>
);

export default OrSeparator;
