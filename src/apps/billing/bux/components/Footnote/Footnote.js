import PropTypes from 'prop-types';
import React from 'react';
import styles from './Footnote.less';

const Footnote = ({ children }) => (
  <p className={styles.footnote}>{ children }</p>
);

Footnote.propTypes = {
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.string]),
};

export default Footnote;
