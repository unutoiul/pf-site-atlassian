import Footnote from './Footnote';
import FootnoteIndicator from './FootnoteIndicator';

export {
  Footnote,
  FootnoteIndicator
};

