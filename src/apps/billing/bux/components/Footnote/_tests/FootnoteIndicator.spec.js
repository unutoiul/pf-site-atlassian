import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import FootnoteIndicator from '../FootnoteIndicator';

describe('Footnote indicator component', () => {
  const Component = () => <div>component</div>;

  it('should render a footnote indicator if condition is not defined', () => {
    const wrapper = shallow(<FootnoteIndicator><Component /></FootnoteIndicator>);
    expect(wrapper.find('span')).to.be.present();
    expect(wrapper.find(Component)).to.be.present();
  });

  it('should render a footnote indicator if condition is true', () => {
    const wrapper = shallow(<FootnoteIndicator condition><Component /></FootnoteIndicator>);
    expect(wrapper.find('span')).to.be.present();
    expect(wrapper.find(Component)).to.be.present();
  });

  it('should not render a footnote indicator if the condition is false', () => {
    const wrapper = shallow(<FootnoteIndicator condition={false}><Component /></FootnoteIndicator>);
    expect(wrapper.find('span')).to.not.be.present();
    expect(wrapper.find(Component)).to.be.present();
  });
});