import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Footnote from '../Footnote';

describe('Footnote indicator component', () => {
  it('should render a footnote around node', () => {
    const Component = () => <span>component</span>;
    const wrapper = shallow(<Footnote><Component /></Footnote>);
    expect(wrapper.find('p')).to.be.present();
    expect(wrapper.find(Component)).to.be.present();
  });

  it('should render a footnote around text', () => {
    const wrapper = shallow(<Footnote>Text</Footnote>);
    expect(wrapper.find('p')).to.be.present();
    expect(wrapper.find('p')).to.contain.text('Text');
  });
});