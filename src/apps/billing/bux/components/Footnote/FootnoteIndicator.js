import PropTypes from 'prop-types';
import React from 'react';
import styles from './Footnote.less';

const FootnoteIndicator = ({ children, condition }) => (
  condition ?
    <span className={styles.footnoteIndicator}>{ children }</span> :
    children
);

FootnoteIndicator.propTypes = {
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.string]),
  condition: PropTypes.bool
};

FootnoteIndicator.defaultProps = {
  condition: true
};

export default FootnoteIndicator;
