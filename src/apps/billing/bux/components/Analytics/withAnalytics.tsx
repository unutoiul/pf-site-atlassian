import * as React from 'react';

import { AnalyticsScreenContext } from './components/Screen';

export const withAnalytics = (WrappedComponent: React.ComponentType<any>) => props => (
  <AnalyticsScreenContext.Consumer>
    {(analytics) =>
      <WrappedComponent {...props} analytics={analytics} />
    }
  </AnalyticsScreenContext.Consumer>
);
