import * as React from 'react';

import { mergeEventData } from '../mergeEventData';

interface AnalyticsContextValue {
  [s: string]: any;
  attributes?: any;
}

const AnalyticsContext: React.Context<AnalyticsContextValue> = React.createContext({});

interface PropagatedAnalyticsProviderProps {
  value?: AnalyticsContextValue;
}

const PropagatedAnalyticsProvider: React.SFC<PropagatedAnalyticsProviderProps> = ({ value = {}, children }) => (
  <AnalyticsContext.Consumer>
    {(originalValue) => (
      <AnalyticsContext.Provider
        value={mergeEventData(
          originalValue,
          value,
        )}
      >
        {children}
      </AnalyticsContext.Provider>
    )}
  </AnalyticsContext.Consumer>
);

export default {
  Consumer: AnalyticsContext.Consumer,
  PropagatedProvider: PropagatedAnalyticsProvider,
  Provider: AnalyticsContext.Provider,
};
