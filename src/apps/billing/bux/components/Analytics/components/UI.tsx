import * as React from 'react';

import { AnalyticsListener as AkAnalyticsListener } from '@atlaskit/analytics-next';

import { mergeEventData } from '../mergeEventData';
import AnalyticsContext from './Context';

interface UIProps {
  as: string;
  attributes?: object;
}

interface AnalyticsContextProps {
  analyticsContext: { [s: string]: any };
}

class UIAnalyticsListener extends React.PureComponent<UIProps & AnalyticsContextProps> {
  public handleEvent = e => {
    const { as, attributes } = this.props;
    e.update(mergeEventData(
      this.props.analyticsContext,
      e.payload,
      {
        actionSubjectId: as,
        attributes,
      },
    )).fire('bux');
  };

  public render() {
    return (
      <AkAnalyticsListener channel="atlaskit" onEvent={this.handleEvent}>
        {this.props.children}
      </AkAnalyticsListener>
    );
  }
}

const UI: React.SFC<UIProps> = (props) => (
  <AnalyticsContext.Consumer>
    {
      (context) => (
        <UIAnalyticsListener {...props} analyticsContext={context}/>
      )
    }
  </AnalyticsContext.Consumer>
);

export default UI;
