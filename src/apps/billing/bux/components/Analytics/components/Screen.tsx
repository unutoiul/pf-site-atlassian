import { produce } from 'immer';
import * as React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { AnalyticsListener as AkAnalyticsListener } from '@atlaskit/analytics-next';

import { AnalyticsClientProps, NewAnalyticsClient, ScreenEventSender, withAnalyticsClient } from 'common/analytics';

import { getTenantInfo } from 'bux/core/state/meta/selectors';
import { TenantInfoType } from 'bux/core/state/meta/types';

import { mergeEventData } from '../mergeEventData';
import AnalyticsContext from './Context';

interface TrackEventPayload {
  actionSubject: string;
  action: string;
  attributes?: object;
  [s: string]: any;
}

interface UIEventPayload {
  actionSubject: string;
  actionSubjectId: string;
  action: string;
  attributes?: object;
  [s: string]: any;
}

interface AnalyticsScreenProvider {
  source: string;
  tenantInfo: TenantInfoType;
  analyticsClient: NewAnalyticsClient;
  sendTrackEvent(payload: TrackEventPayload): void;
  sendUIEvent(payload: UIEventPayload): void;
}

interface AnalyticsScreenProps extends AnalyticsClientProps {
  name: string;
  tenantInfo: TenantInfoType;
  attributes?: object;
}

interface AnalyticsContextProps {
  analyticsContext: any;
}

export const AnalyticsScreenContext = React.createContext<AnalyticsScreenProvider | undefined>(undefined);

const createEvent = (tenantInfo: TenantInfoType, attributes?: object) => ({
  data: {
    attributes,
    // TODO: Add extra info here like FF, device info...
  },
  ...(tenantInfo.isOrg ? { orgId: tenantInfo.id } : { cloudId: tenantInfo.id }),
});

class Screen extends React.PureComponent<AnalyticsScreenProps & AnalyticsContextProps> {
  private baseEvent: any;
  private screenEvent: any;

  constructor(props) {
    super(props);
    this.baseEvent = this.createEvent();
    this.screenEvent = produce(this.baseEvent, (draft) => {
      draft.data = mergeEventData(draft.data, this.props.analyticsContext);
      draft.data.name = this.props.name;
    });

  }

  public sendTrackEvent = (payload: TrackEventPayload) => {
    const trackEvent = produce(this.baseEvent, (draft) => {
      draft.data = mergeEventData(draft.data, payload);
      draft.data.source = this.props.name;
    });
    this.props.analyticsClient.sendTrackEvent(trackEvent);
  };

  public sendUIEvent = (payload: UIEventPayload) => {
    const trackEvent = produce(this.baseEvent, (draft) => {
      draft.data = mergeEventData(draft.data, payload);
      draft.data.source = this.props.name;
    });
    this.props.analyticsClient.sendUIEvent(trackEvent);
  };

  public handleEvent = (e) => {
    if (!e.payload.actionSubjectId) {
      return;
    }
    this.sendUIEvent(e.payload);
  };

  public createEvent = () => createEvent(this.props.tenantInfo, this.props.attributes);

  public render() {
    const { children, tenantInfo, analyticsClient, name } = this.props;
    const context = {
      source: name,
      tenantInfo,
      analyticsClient,
      sendTrackEvent: this.sendTrackEvent,
      sendUIEvent: this.sendUIEvent,
    };

    return (
      <ScreenEventSender event={this.screenEvent}>
        <AnalyticsScreenContext.Provider value={context}>
          <AkAnalyticsListener channel="bux" onEvent={this.handleEvent}>
            {children}
          </AkAnalyticsListener>
        </AnalyticsScreenContext.Provider>
      </ScreenEventSender>
    );
  }
}

const ScreenWithAnalyticsContext = (props: AnalyticsScreenProps) => (
  <AnalyticsContext.Consumer>
    {(analyticsContext) => {
      return <Screen {...props} analyticsContext={analyticsContext}/>;
    }}
  </AnalyticsContext.Consumer>
);

const mapStateToProps = state => ({
  tenantInfo: getTenantInfo(state),
});

const ComposedScreen: React.ComponentType<any> = compose(
  connect(mapStateToProps, null),
  withAnalyticsClient,
)(ScreenWithAnalyticsContext);

export default ComposedScreen;
