export const mergeEventData = (...data: any[]) => (
  data.reduce((merged, current) => ({
    ...merged,
    ...current,
    attributes: {
      ...merged.attributes,
      ...current.attributes,
    },
  }))
);
