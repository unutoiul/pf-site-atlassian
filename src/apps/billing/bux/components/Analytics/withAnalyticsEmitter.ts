import * as React from 'react';

import {
  createAndFireEvent as AkCreateAndFireEvent,
  withAnalyticsContext as AkWithAnalyticsContext,
  withAnalyticsEvents as AkWithAnalyticsEvents,
} from '@atlaskit/analytics-next';

import { Action, ActionSubject } from 'common/analytics/new-analytics-types';

type ValueOf<T> = T[keyof T];
type ActionSubjectType = ValueOf<ActionSubject>;
type ActionType = ValueOf<Action>;
interface EventsInfo {
  [s: string]: {
    action: ActionType,
    [s: string]: any,
  };
}

export const withAnalyticsEmitter = (
  componentName: string,
  actionSubject: ActionSubjectType,
  eventsInfo: EventsInfo = {
    onClick: {
      action: 'clicked',
    },
  },
) => <T extends React.ComponentType | string>(WrappedComponent: T): T => {
  const createAndFireEventOnAtlasKit = AkCreateAndFireEvent('atlaskit');

  return AkWithAnalyticsContext({
    componentName: actionSubject,
  })(AkWithAnalyticsEvents(Object.keys(eventsInfo)
    .reduce((handlers, key) => {
      const eventInfo = eventsInfo[key];
      handlers[key] = createAndFireEventOnAtlasKit({
        actionSubject,
        ...eventInfo,
        attributes: {
          componentName,
          ...eventInfo.attributes,
        },
      });

      return handlers;
    }, {}))(WrappedComponent));
};
