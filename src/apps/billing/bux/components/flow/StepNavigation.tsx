import * as React from 'react';
import styled from 'styled-components';

import { akColorB300, akColorN70, akColorN800 } from '@atlaskit/util-shared-styles';

const getStepColor = (itemIndex, currentStep) => {
  if (itemIndex < currentStep) {
    return akColorN800;
  }
  if (itemIndex === currentStep) {
    return akColorB300;
  }
  if (itemIndex > currentStep) {
    return akColorN70;
  }

  return '';
};

const Navigation = styled.ol`
  display: flex;
  justify-content: center;
  padding: 0;
  margin: 0;
  list-style: none;
`;

interface StepProps {
  count: number;
  currentStep: number;
  index: number;
}

const Step = styled.li`
  margin: 0;
  padding: 24px 0;
  position: relative;
  width: ${({ count }: StepProps) => 100 / count}%;
  text-align: center;
  transition-property: color, font-weight;
  transition-duration: 300ms;
  color: ${({ currentStep, index }: StepProps) => getStepColor(index, currentStep)};
  font-size: 1em;
  ${({ currentStep, index }: StepProps) => (currentStep >= index ? 'font-weight: 600;' : '')};
  ${
  ({ currentStep, index }: StepProps) => (
    index === 0
      ? `&:before {
          content:'';
          position: absolute;
          left: 50%;
          top: 0;
          right: ${currentStep > 0 ? `${50 - (currentStep * 100)}%` : 'initial'};
          border: 1px solid ${akColorB300};
          box-shadow: 0 0 0 4px ${akColorB300};
          border-radius: 4px;
          transition-property: right;
          transition-duration: 300ms;
          z-index: 1;
          background-color: ${akColorB300};
        }`
      : `&:before {
          content:'';
          position: absolute;
          left: 50%;
          top: 0;
          border: 1px solid ${akColorN70};
          box-shadow: 0 0 0 4px ${akColorN70};
          border-radius: 4px;
          transform: translateX(-2px);
          background-color: ${akColorN70};
        }`
  )}
`;

interface StepNavigationProps {
  steps: any[];
  currentStep: number;
}

const StepNavigation = ({ steps, currentStep }: StepNavigationProps) => (
  <Navigation>
    {
      steps.map((step, index) =>
        (<Step
          key={index}
          currentStep={currentStep}
          index={index}
          count={steps.length}
        >
          {step}
        </Step>))
    }
  </Navigation>
);

export default StepNavigation;
