import cx from 'classnames';
import * as React from 'react';

import styles from './styles.less';

export type StepProps = {
  title?: string | React.ReactNode;
  isFirstStep?: boolean;
  isActive?: boolean;
  stepState?: object;
  className?: string;
  next?(_, stepState: object): any;
  back?(): any;
  condition?(): boolean;
  [s: string]: any;
} & (
  {Component?: React.ComponentType<any>} | {children(props: any): React.ReactNode}
);

class Step extends React.Component<StepProps> {
  public static propTypes = {
    condition: (props) => {
      if (!props.title && props.condition) {
        return new Error('When condition prop is used you should provide title prop as well.');
      }

      return null;
    },
  };

  public render() {
    const {
      Component, children, isActive, next, back, stepState = {}, className, ...rest
    } = this.props;
    const childProps = {
      ...rest,
      ...{ next, back, ...stepState },
    };

    return (
      isActive
        ? (
          <div className={cx(styles.step, className)}>
            {children ? (children as any)(childProps) : <Component {...childProps}/>}
          </div>
        ) : null
    );
  }
}

export default Step;
