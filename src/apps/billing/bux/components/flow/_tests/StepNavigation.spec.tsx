import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';

import StepNavigation from '../StepNavigation';

describe('StepNavigation', () => {
  it('should render steps', () => {
    // have no idea how to test styled components :D
    // checking only structure
    const wrapper = mount(<StepNavigation steps={['a', 'b', 'c']} currentStep={1} />);
    expect(wrapper.find('ol')).to.be.present();
    expect(wrapper.find('li')).to.have.length(3);
    expect(wrapper.find('li').at(0)).to.contain.text('a');
  });
});
