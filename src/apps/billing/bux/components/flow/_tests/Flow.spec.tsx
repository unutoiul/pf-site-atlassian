import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import Flow from '../Flow';
import Step from '../Step';
import StepNavigation from '../StepNavigation';

describe('Flow', () => {
  let onCancel;
  let onComplete;

  beforeEach(() => {
    onCancel = sinon.spy();
    onComplete = sinon.spy();
  });

  it('should render first step initially', () => {
    const wrapper = mount(
      <Flow onCancel={onCancel} onComplete={onComplete}>
        <Step Component={() => <div id="d1"/>} />
        <Step Component={() => <div id="d2"/>} />
      </Flow>);
    expect(wrapper.find('#d1')).to.be.present();
    expect(wrapper.find('#d2')).not.to.be.present();
    expect(onCancel).not.to.be.called();
    expect(onComplete).not.to.be.called();
  });

  it('should render the second step after calling next', () => {
    let next;
    const wrapper = mount(
      <Flow onCancel={onCancel} onComplete={onComplete}>
      <Step
        Component={({ next: _next }) => {
          next = _next;

          return <div id="d1" />;
        }}
      />
      <Step Component={() => <div id="d2" />} />
    </Flow>);
    next();
    wrapper.update();
    expect(wrapper.find('div#d1')).not.to.be.present();
    expect(wrapper.find('div#d2')).to.be.present();
    expect(onCancel).not.to.be.called();
    expect(onComplete).not.to.be.called();
  });

  it('should call onComplete after calling next on the last step', () => {
    let next;
    const wrapper = mount(
      <Flow onCancel={onCancel} onComplete={onComplete}>
      <Step
        Component={({ next: _next }) => {
          next = _next;

          return <div id="d1" />;
        }}
      />
    </Flow>);
    next();
    wrapper.update();
    expect(onComplete).to.be.called();
    expect(onCancel).not.to.be.called();
  });

  it('should call onCancel after calling back on the first step', () => {
    let back;
    const wrapper = mount(
      <Flow onCancel={onCancel} onComplete={onComplete}>
      <Step
        Component={({ back: _back }) => {
          back = _back;

          return <div id="d1" />;
        }}
      />
    </Flow>);
    back();
    wrapper.update();
    expect(onCancel).to.be.called();
    expect(onComplete).not.to.be.called();
  });

  it('should provide a step navigation', () => {
    const wrapper = mount(
      <Flow
        onCancel={onCancel}
        onComplete={onComplete}
        stepNavigation={() => ([1, 2, 3])}
      >
        <Step Component={() => <div />} />
        <Step Component={() => <div />} />
      </Flow>);
    expect(wrapper.update().find(StepNavigation).props().steps).to.be.deep.equal([1, 2, 3]);
  });

  it('should call render first step after calling next and then back', () => {
    let next;
    let back;

    const ComponentA = ({ next: _next }) => {
      next = _next;

      return <div id="d1" />;
    };

    const ComponentB = ({ back: _back, someNumber }) => {
      back = _back;

      return <div id="d2">number-{someNumber}</div>;
    };

    const wrapper = mount(
      <Flow
        onCancel={onCancel}
        onComplete={onComplete}
        stepNavigation={() => ['d1', 'd2']}
      >
        <Step Component={ComponentA} />
        <Step Component={ComponentB}/>
      </Flow>);
    next(null, { someNumber: 42 });
    expect(wrapper.update().find(StepNavigation)).to.have.prop('currentStep', 1);
    expect(wrapper).to.contain.text('number-42');
    back();
    expect(wrapper.update().find(StepNavigation)).to.have.prop('currentStep', 0);
    wrapper.update();
    expect(wrapper.find('#d1')).to.be.present();
    expect(wrapper.find('#d2')).not.to.be.present();
    expect(onCancel).not.to.be.called();
    expect(onComplete).not.to.be.called();
  });

  it('should render step navigation if steps have title', () => {
    const Next = () => (<div />);

    const wrapper = mount(
      <Flow
        onCancel={onCancel}
        onComplete={onComplete}
      >
        <Step title="first" Component={Next} />
        <Step title="second" Component={Next} />
      </Flow>);
    expect(wrapper.find(StepNavigation).props().steps).to.be.deep.equal(['first', 'second']);
  });

  it('should throw error if not all steps define title', () => {
    const Next = () => (<div />);
    const wrapper = mount(
      <Flow
        onCancel={onCancel}
        onComplete={onComplete}
      />);
    const FlowInstance = wrapper.instance() as Flow;

    FlowInstance.getRenderableChildren = () => ([
      <Step key="1" title="title" Component={Next} />,
      <Step key="2" Component={Next} />,
    ]);
    expect(FlowInstance.getTitles).to.throw(Error);

    FlowInstance.getRenderableChildren = () => ([
      <Step key="1" Component={Next} />,
      <Step key="2" title="title" Component={Next} />,
    ]);
    expect(FlowInstance.getTitles).to.throw(Error);
  });

  it('should skip if condition is false', () => {
    const Rendered = () => (<div />);
    const Skipped = () => (<div />);

    const wrapper = mount(
      <Flow
        onCancel={onCancel}
        onComplete={onComplete}
      >
        <Step title="first" Component={Skipped} condition={() => false} />
        <Step title="second" Component={Rendered} condition={() => true} />
      </Flow>);
    expect(wrapper.find(StepNavigation).props().steps).to.be.deep.equal(['second']);
    expect(wrapper.find(Rendered)).to.be.present();
  });
});
