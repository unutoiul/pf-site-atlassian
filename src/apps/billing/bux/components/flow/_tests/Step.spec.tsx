import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';

import Step from '../Step';

describe('Step', () => {
  it('should render component if isActive equals true', () => {
    const Child = () => <div id="d1" />;
    const wrapper = mount(<Step isActive Component={Child} />);
    expect(wrapper.find('#d1')).to.be.present();
  });

  it('should not render component if isActive equals false', () => {
    const Child = () => <div id="d1" />;
    const wrapper = mount(<Step isActive={false} Component={Child} />);
    expect(wrapper.find('#d1')).not.to.be.present();
  });

  it('should passthought props', () => {
    const otherProps = { someNumber: 42 };
    const Child = ({ someNumber }) => <div>{someNumber}</div>;
    const wrapper = mount(<Step isActive {...otherProps} Component={Child} />);
    expect(wrapper).to.contain.text('42');
  });
});
