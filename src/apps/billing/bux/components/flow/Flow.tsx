import * as React from 'react';

import { StepProps } from './Step';
import StepNavigation from './StepNavigation';
import styles from './styles.less';

interface FlowProps {
  initialState?: object;
  width?: 'fluid' | 'narrow';
  children?: Array<React.ReactElement<StepProps>> | React.ReactElement<StepProps>;
  stepNavigation?(): any;
  onCancel(): any;
  onComplete(): any;
}

export default class Flow extends React.Component<FlowProps> {
  public static defaultProps: Partial<FlowProps> = {
    initialState: {},
    width: 'fluid',
  };

  public state = {
    currentStep: 0,
    stepState: {},
  };

  public getRenderableChildren = (): Array<React.ReactElement<StepProps>> => (
    (React.Children.toArray(this.props.children) as Array<React.ReactElement<StepProps>>)
      .filter(child => !child.props.condition || child.props.condition())
  );

  public getTitles = () => {
    let hasTitle = 0;

    const renderableChildren = this.getRenderableChildren();
    const titles = renderableChildren.map((child) => {
      if (child.props.title) {
        hasTitle += 1;
      }

      return child.props.title;
    }).filter(title => !!title);

    if (hasTitle > 0 && hasTitle < renderableChildren.length) {
      throw new Error('As one child defined title, all the other children should define as well');
    }

    return titles;
  };

  public next = (_, stepState = {}) => {
    const nextStep = this.state.currentStep + 1;
    if (nextStep >= this.getRenderableChildren().length) {
      return this.props.onComplete();
    }
    this.setState({
      stepState,
      currentStep: nextStep,
    });
  };

  public back = () => {
    const nextStep = this.state.currentStep - 1;
    if (nextStep < 0) {
      return this.props.onCancel();
    }
    this.setState({
      currentStep: nextStep,
    });
  };

  public render() {
    const {
      initialState, stepNavigation, width = 'fluid',
    } = this.props;
    const { currentStep, stepState } = this.state;

    const navigation = stepNavigation ? stepNavigation() : this.getTitles();

    const steps = this.getRenderableChildren().map((child, index) => {
      return React.cloneElement(child, {
        next: this.next,
        back: this.back,
        isFirstStep: index === 0,
        isActive: index === currentStep,
        stepState: {
          ...initialState,
          ...stepState,
          ...child.props.stepState,
        },
      });
    });

    return (
      <div className={styles[width]}>
        {navigation && <StepNavigation steps={navigation} currentStep={currentStep} />}
        {steps}
      </div>
    );
  }
}
