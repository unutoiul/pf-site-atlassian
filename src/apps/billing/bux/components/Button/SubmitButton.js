import React from 'react';
import Button from './Button';

const SubmitButton = (props) => {
  const { children } = props;
  return (
    <Button
      {...props}
      submit
    >
      {children}
    </Button>
  );
};

SubmitButton.propTypes = {
  ...Button.propTypes,
};

export default SubmitButton;
