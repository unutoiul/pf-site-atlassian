import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Link from 'bux/components/Link';
import Button from '../Button';
import RouterButton from '../RouterButton';

describe('RouterButton', () => {
  it('should have Link to wrap Button', () => {
    const wrapper = shallow(<RouterButton>button</RouterButton>);
    expect(wrapper.find(Link)).to.be.present();
    expect(wrapper.find(Button)).to.be.present();
  });
  it('should set to prop', () => {
    const wrapper = shallow(<RouterButton to="/test">button</RouterButton>);
    expect(wrapper.find(Link)).to.have.prop('to', '/test');
    expect(wrapper.find(Button)).to.not.have.prop('to');
  });
  it('should set onClick prop', () => {
    const onClick = () => {};
    const wrapper = shallow(<RouterButton onClick={onClick}>button</RouterButton>);
    expect(wrapper.find(Link)).to.have.prop('onClick', onClick);
    expect(wrapper.find(Button)).to.not.have.prop('onClick');
  });
  it('should set name prop', () => {
    const wrapper = shallow(<RouterButton name="test">button</RouterButton>);
    expect(wrapper.find(Link)).to.have.prop('name', 'test');
    expect(wrapper.find(Button)).to.not.have.prop('name');
  });
  it('should set urlTransform prop', () => {
    const urlTransform = () => {};
    const wrapper = shallow(<RouterButton urlTransform={urlTransform}>button</RouterButton>);
    expect(wrapper.find(Link)).to.have.prop('urlTransform', urlTransform);
    expect(wrapper.find(Button)).to.not.have.prop('urlTransform');
  });
  it('should set primary prop', () => {
    const wrapper = shallow(<RouterButton primary>button</RouterButton>);
    expect(wrapper.find(Link)).to.not.have.prop('primary');
    expect(wrapper.find(Button)).to.have.prop('primary', true);
  });
});
