import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import SubmitButton from '../SubmitButton';
import Button from '../Button';

describe('SubmitButton', () => {
  it('should wrap Button', () => {
    const wrapper = shallow(<SubmitButton>button</SubmitButton>);
    expect(wrapper.find(Button)).to.be.present();
    expect(wrapper.find(Button)).to.have.prop('submit');
  });
});
