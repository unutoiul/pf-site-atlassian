import PropTypes from 'prop-types';
import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow } from 'enzyme';
import AKButton from '@atlaskit/button';
import Spinner from '@atlaskit/spinner';
import Button from '../Button';

describe('Button component: ', () => {
  it('should render children text', () => {
    const wrapper = shallow(<Button>supernova</Button>);
    expect(wrapper.find(AKButton).props().children).to.be.equal('supernova');
  });

  it('should not have primary style', () => {
    const wrapper = shallow(<Button>supernova</Button>);
    expect(wrapper.find(AKButton).props().appearance).to.be.equal('default');
  });

  it('should have primary style', () => {
    const wrapper = shallow(<Button primary>supernova</Button>);
    expect(wrapper.find(AKButton).props().appearance).to.be.equal('primary');
  });

  it('should have danger style', () => {
    const wrapper = shallow(<Button primary submit danger>supernova</Button>);
    expect(wrapper.find(AKButton).props().appearance).to.be.equal('danger');
  });

  it('should have submit style', () => {
    const wrapper = shallow(<Button submit>supernova</Button>);
    expect(wrapper.find(AKButton).props().appearance).to.be.equal('primary');
    expect(wrapper.find(AKButton).props().type).to.be.equal('submit');
  });

  it('should generate event on click', () => {
    const onClick = sinon.stub();
    const sendAnalyticEvent = sinon.stub();
    const wrapper = shallow(<Button onClick={onClick}>supernova</Button>, {
      context: {
        sendAnalyticEvent
      },
      childContextTypes: {
        sendAnalyticEvent: PropTypes.func
      }
    });

    wrapper.simulate('click');
    expect(sendAnalyticEvent).to.have.been.calledWithMatch('click');
    expect(onClick).to.have.been.called();
  });

  it('should preserve className', () => {
    const wrapper = shallow(<Button className="my-own-class my-other-class">supernova</Button>);
    expect(wrapper).to.have.className('my-own-class');
    expect(wrapper).to.have.className('my-other-class');
  });

  it('should indicate work', () => {
    const wrapper = shallow(<Button>button</Button>);
    expect(wrapper.prop.iconAfter).to.be.undefined();

    const spinWrapper = shallow(<Button spinning>button</Button>);
    const icon = spinWrapper.find(AKButton).props().iconAfter;
    expect(icon.type).to.equal(Spinner);
  });
});
