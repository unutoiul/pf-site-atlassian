import PropTypes from 'prop-types';
import React, { Component } from 'react';
import AKButton from '@atlaskit/button';
import cx from 'classnames';
import Spinner from '@atlaskit/spinner';

function getAppearance({
  primary, submit, danger, subtle, link
}) {
  if (danger) {
    return 'danger';
  }

  if (link) {
    return 'link';
  }

  if (submit || primary) {
    return 'primary';
  }
  return subtle ? 'subtle' : 'default';
}

class Button extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    primary: PropTypes.bool,
    submit: PropTypes.bool,
    danger: PropTypes.bool,
    link: PropTypes.bool,
    disabled: PropTypes.bool,
    className: PropTypes.string,
    onClick: PropTypes.func,
    name: PropTypes.string,
    spinning: PropTypes.bool,
    spinningBefore: PropTypes.bool,
  };

  static contextTypes = {
    sendAnalyticEvent: PropTypes.func
  };

  onClick = (event) => {
    if (this.props.onClick) {
      this.props.onClick(event);
    }
    this.context.sendAnalyticEvent(this.props.name || 'click');
  };

  render() {
    const {
      spinning, spinningBefore, children, submit, disabled, className = '', ...otherProps
    } = this.props;
    const spinner = <Spinner invertColor={!disabled} />;
    return (
      <AKButton
        {...otherProps}
        appearance={getAppearance(this.props)}
        className={cx(className, { submit })}
        type={submit ? 'submit' : 'button'}
        isDisabled={disabled}
        iconBefore={spinningBefore && spinning ? spinner : undefined}
        iconAfter={!spinningBefore && spinning ? spinner : undefined}
        onClick={this.onClick}
      >
        {children}
      </AKButton>
    );
  }
}

export default Button;
