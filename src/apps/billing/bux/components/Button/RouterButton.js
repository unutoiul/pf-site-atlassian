import { node } from 'prop-types';
import React from 'react';
import Link from '../Link';
import Button from './Button';
import styles from './RouterButton.less';

/**
 * Wrap an AK Button into a React Router Link.
 * TODO: Currently fires 2 analytics events, one for the link and another for the button. 
 * Need just the button
 */
const RouterButton = ({
  children, to, onClick, urlTransform, name, ...rest 
}) => (
  <Link to={to} onClick={onClick} urlTransform={urlTransform} name={name} className={styles.routerButton}>
    <Button {...rest}>
      {children}
    </Button>
  </Link>
);

RouterButton.propTypes = {
  children: node.isRequired,
  ...Link.propTypes
};

export default RouterButton;
