import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { mount } from 'enzyme';
import getAnalytic from '../../helpers/getAnalytic';

import withPageViewEvent from '../pageViewEvent';
import withTimeTracking from '../timeTracking';

describe('withPageViewEvent', () => {
  it('should trigger pageview Event subcomponent', () => {
    const clock = sinon.useFakeTimers(Date.now());
    const { context, sendAnalyticEvent } = getAnalytic();
    const Component = () => <span>test</span>;
    const Wrapped = withTimeTracking(withPageViewEvent(withPageViewEvent(Component)));
    mount(<Wrapped />, context);
    const base = {
      pathname: 'http://localhost/',
      referrer: '',
    };
    expect(sendAnalyticEvent).to.be.calledWith('pageView', {
      timeFromPageStart: 0,
      order: 0,
      ...base,
    });

    expect(sendAnalyticEvent).to.be.calledWith('pageView', {
      timeFromPageStart: 0,
      order: 1,
      ...base,
    });
    clock.restore();
  });
});
