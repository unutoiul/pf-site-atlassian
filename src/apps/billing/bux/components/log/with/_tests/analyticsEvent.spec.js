import React from 'react';
import PropTypes from 'prop-types';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';
import getAnalytic from '../../helpers/getAnalytic';

import WithAnalyticsEvent from '../analyticsEvent';

describe('WithAnalyticsEvent', () => {
  it('should render subcomponent', () => {
    const { sendAnalyticEvent, context } = getAnalytic();

    /* eslint no-shadow: "off" */
    const Component = ({ sendAnalyticEvent }) => <span onClick={sendAnalyticEvent}>test</span>;

    Component.propTypes = {
      sendAnalyticEvent: PropTypes.func
    };

    const wrapper = shallow(<WithAnalyticsEvent>
      { sendAnalyticEvent => <Component sendAnalyticEvent={sendAnalyticEvent} /> }
                            </WithAnalyticsEvent>, context);

    const sub = wrapper.find(Component);

    expect(sub).to.be.present();
    expect(sub).to.have.prop('sendAnalyticEvent', sendAnalyticEvent);
  });

  it('should trigger pageview Event subcomponent', () => {
    const { sendAnalyticEvent, context } = getAnalytic();
    const payload = { prop: 42 };

    /* eslint no-shadow: "off" */
    const Component = ({ sendAnalyticEvent }) => (
      <span onClick={() => sendAnalyticEvent('test', payload)}>
        test
      </span>);

    Component.propTypes = {
      sendAnalyticEvent: PropTypes.func
    };

    const wrapper = mount(<WithAnalyticsEvent>
      { sendAnalyticEvent => <Component sendAnalyticEvent={sendAnalyticEvent} /> }
                          </WithAnalyticsEvent>, context);

    wrapper.find(Component).simulate('click');

    expect(sendAnalyticEvent).to.be.calledWith('test', payload);
  });
});
