import React from 'react';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';
import getAnalytic from '../../helpers/getAnalytic';

import withPageViewEvent from '../pageViewEvent';

describe('withPageViewEvent', () => {
  it('should render subcomponent', () => {
    const { context } = getAnalytic();
    const Component = () => <span>test</span>;
    const Wrapped = withPageViewEvent(Component);
    const wrapper = shallow(<Wrapped prop="42" />, context);
    const sub = wrapper.find(Component);
    expect(sub).to.be.present();
    expect(sub).to.have.prop('prop', '42');
  });

  it('should trigger pageview Event subcomponent', () => {
    const { context, sendAnalyticEvent } = getAnalytic();
    const Component = () => <span>test</span>;
    const Wrapped = withPageViewEvent(Component);
    mount(<Wrapped />, context);
    expect(sendAnalyticEvent).to.be.calledWith('pageView');
  });
});
