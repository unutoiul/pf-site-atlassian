import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { mount } from 'enzyme';
import getAnalytic from '../../helpers/getAnalytic';

import withPageViewEvent from '../pageViewEvent';
import withPageFeatures from '../pageFeatures';

describe('withPageFeatures', () => {
  it('should instrument event', () => {
    const clock = sinon.useFakeTimers(Date.now());
    const { context, sendAnalyticEvent } = getAnalytic();
    const Component = () => <span>test</span>;
    const Wrapped = withPageFeatures(withPageViewEvent(Component));
    mount(<Wrapped />, context);

    expect(sendAnalyticEvent).to.be.calledWith('pageView', {
      language: 'en',
      device: {},
      featureFlags: {},
      pathname: 'http://localhost/',
      referrer: '',
    });

    clock.restore();
  });
});
