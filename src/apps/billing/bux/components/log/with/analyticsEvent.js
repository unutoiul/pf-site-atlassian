import PropTypes from 'prop-types';
import { Component } from 'react';

class WithAnalyticsEvent extends Component {
  static propTypes = {
    children: PropTypes.func
  };

  static contextTypes = {
    sendAnalyticEvent: PropTypes.func
  };

  render() {
    return this.props.children(this.context.sendAnalyticEvent);
  }
}

export default WithAnalyticsEvent;