import { I18n } from '@lingui/react';
import * as PropTypes from 'prop-types';
import * as React from 'react';

import { UserFeatureFlag } from 'common/feature-flags';
import { Media } from 'common/responsive/Matcher';

import { FeatureFlagsContext } from 'bux/components/FeatureFlags';

const trackedSiteFlags = (
  process.env.BABEL_ENV === 'test'
    ? [] // tracking is disabled in test environment due to GQL absence
    : [
      'admin.responsive.nav',
      'admin.billing.menu.flat',
    ]
);

const trackedBuxFlags = [
  // nothing
];

const filterFlags = (flags = {}, list: string[]) => (
  Object
    .keys(flags)
    .reduce((acc, key) => {
      if (list.includes(key)) {
        return {
          ...acc,
          [key]: flags[key],
        };
      }

      return acc;
    }, {})
);

const FlagsAdopted: React.SFC<{ flags: Record<any, boolean> }> = trackedSiteFlags.reduce(
  (Acc: any, flag: string) => (
    ({ flags, ...rest }) => (
      <UserFeatureFlag flagKey={flag} defaultValue={false}>
        {({ value }) => <Acc flags={{ ...flags, [flag]: value }} {...rest} />}
      </UserFeatureFlag>
    )
  ),
  ({ flags, children }) => children(flags),
);

const UserFeatureFlags = ({ children }) => <FlagsAdopted flags={{}}>{children}</FlagsAdopted>;

const pageFeatures = (WrappedComponent) => {
  class WithPageFeatures extends React.Component<{
    language: string,
    device: Record<string, boolean>,
    featureFlags: Record<string, boolean>,
    children: React.ReactNode,
  }> {
    public static contextTypes = {
      sendAnalyticEvent: PropTypes.func,
    };

    public static childContextTypes = {
      sendAnalyticEvent: PropTypes.func,
    };

    public getChildContext() {
      return {
        sendAnalyticEvent: this.sendAnalyticEvent,
      };
    }

    public render() {
      return <WrappedComponent {...this.props} />;
    }

    private sendAnalyticEvent = (name, payload = {}) => {
      const { device, featureFlags, language } = this.props;
      this.context.sendAnalyticEvent(name, {
        language,
        device,
        featureFlags,
        ...payload,
      });
    };
  }

  return props => (
    <I18n>
      {({ language = 'en' }) => (
        <Media.Matches>
          {device => (
            <UserFeatureFlags>
              {userFeatureFlags =>
                (<FeatureFlagsContext.Consumer>
                  {buxFeatureFlags =>
                    (<WithPageFeatures
                      language={language}
                      device={device}
                      featureFlags={{
                        ...filterFlags(buxFeatureFlags, trackedBuxFlags),
                        ...userFeatureFlags,
                      }}
                      {...props}
                    />)
                  }
                </FeatureFlagsContext.Consumer>)
              }
            </UserFeatureFlags>
          )}
        </Media.Matches>
      )}
    </I18n>
  );
};

export default pageFeatures;
