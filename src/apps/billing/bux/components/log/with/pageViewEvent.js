import PropTypes from 'prop-types';
import React, { Component } from 'react';

const scriptEnterTime = Date.now();

const withPageViewEvent = WrappedComponent => class WithViewLoadEvent extends Component {
  static contextTypes = {
    sendAnalyticEvent: PropTypes.func
  };

  componentDidMount() {
    const perfData = window.performance.timing;
    let perfTiming = {};
    if (perfData) {
      perfTiming = {
        pageLoadTime: perfData.loadEventEnd - perfData.navigationStart,
        jsReadyTime: scriptEnterTime - perfData.navigationStart,
        domReadyTime: perfData.domComplete - perfData.domLoading,
        componentMountDelta: Date.now() - scriptEnterTime,
      };
    }

    this.context.sendAnalyticEvent('pageView', {
      ...perfTiming,
      referrer: document.referrer,
      pathname: window.location.href,
    });
  }

  render() {
    return <WrappedComponent {...this.props} />;
  }
};

export default withPageViewEvent;
