import PropTypes from 'prop-types';
import React from 'react';
import TransparentComponent from '../../Transparent';
import withPageViewEvent from './pageViewEvent';
import withTimeTracking from './timeTracking';
import withPageFeatures from './pageFeatures';

const PlaceHolder = ({ Component, props }) => <Component {...props} />;
PlaceHolder.propTypes = {
  Component: PropTypes.func,
  props: PropTypes.object
};

const Tracker = withPageFeatures(withPageViewEvent(withTimeTracking(PlaceHolder)));

const withPageLevel = Component => props => <Tracker Component={Component} props={props} />;

export const WithPageLevelEvents = withPageLevel(TransparentComponent);

export default withPageLevel;
