import PropTypes from 'prop-types';
import React, { Component } from 'react';

const withTimeTracking = WrappedComponent => class WithTimeTracking extends Component {
  static contextTypes = {
    sendAnalyticEvent: PropTypes.func
  };

  static childContextTypes = {
    sendAnalyticEvent: PropTypes.func
  };

  getChildContext() {
    return {
      sendAnalyticEvent: this.sendAnalyticEvent
    };
  }

  logState = {
    startTime: Date.now(),
    order: 0
  };

  sendAnalyticEvent = (name, payload = {}) => {
    const { startTime, order } = this.logState;
    const timeFromPageStart = Date.now() - startTime;

    this.logState.order += 1;

    this.context.sendAnalyticEvent(name, {
      timeFromPageStart,
      order,

      ...payload
    });
  };

  render() {
    return <WrappedComponent {...this.props} />;
  }
};

export default withTimeTracking;
