import PropTypes from 'prop-types';
import sinon from 'sinon';

const getAnalytic = () => {
  const sendAnalyticEvent = sinon.spy();
  return {
    sendAnalyticEvent,
    context: {
      context: {
        sendAnalyticEvent,
        buxSendAnalyticEvent: sendAnalyticEvent,
      },
      childContextTypes: {
        sendAnalyticEvent: PropTypes.func,
        buxSendAnalyticEvent: PropTypes.func
      }
    }
  };
};

export default getAnalytic;
