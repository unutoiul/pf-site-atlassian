import * as PropTypes from 'prop-types';
import * as React from 'react';

import UnavailableError from 'bux/components/error/UnavailableError';

// TODO: there is common/error-boundary component, which would send event to the analytics
// we should migrate to it in the future

export class ErrorBoundary extends React.Component<{ children: React.ReactNode }, { error: any, errorInfo: any }> {
  public static contextTypes = {
    sendAnalyticEvent: PropTypes.func,
  };

  public state = {
    error: null,
    errorInfo: null,
  };

  public componentDidCatch(error, errorInfo) {
    this.setState({
      error,
      errorInfo,
    });
    this.context.sendAnalyticEvent('reactError', {
      error: String(error),
    });
  }

  public render() {
    const { error } = this.state;
    const { children } = this.props;

    return (
      error
        ? <UnavailableError onRetry={this.repeat} />
        : children
    );
  }

  private repeat = () => this.setState({
    error: null,
    errorInfo: null,
  });
}
