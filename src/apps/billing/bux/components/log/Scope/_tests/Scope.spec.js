import PropTypes from 'prop-types';
import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import proxyquire from 'bux/helpers/proxyquire';
import sinon from 'sinon';

const joinPrefix = event => event.join('.');

describe('Log scope component', () => {
  let Scope;
  let record;

  const prepareProxyquire = () => {
    record = sinon.stub();
    return proxyquire.noCallThru().load('../Scope', {
      'bux/common/helpers/logger': { record }
    });
  };

  beforeEach(() => (
    Scope = prepareProxyquire().default
  ));

  it('should apply cascade to logPrefix', () => {
    const Payload = () => (<div>payload</div>);
    const App = () =>
      (<Scope prefix="b"><Scope prefix="c"><Scope prefix="d">
        <Payload />
                                           </Scope>
                         </Scope>
       </Scope>);
    const wrapper = mount(<App />, {
      context: {
        logPrefix: ['a']
      },
      childContextTypes: {
        logPrefix: PropTypes.arrayOf(PropTypes.string),
        sendAnalyticEvent: PropTypes.func
      }
    });
    expect(joinPrefix(wrapper.find(Scope).at(2).instance().context.logPrefix)).to.be.equal('a.b.c');
  });

  it('should skip empty nodes', () => {
    const Payload = () => <div>payload</div>;
    const App = () => <Scope><Scope prefix="c"><Scope prefix="d"><Payload /></Scope></Scope></Scope>;
    const wrapper = mount(<App />, {
      context: {
        logPrefix: ['a']
      },
      childContextTypes: {
        logPrefix: PropTypes.arrayOf(PropTypes.string),
      }
    });

    expect(joinPrefix(wrapper.find(Scope).at(2).instance().context.logPrefix)).to.be.equal('a.c');
  });

  it('should call sendAnalyticEvent with breadcumps', () => {
    /* eslint-disable react/prefer-stateless-function */
    class Payload extends React.Component {
      static contextTypes = {
        sendAnalyticEvent: PropTypes.func
      };
      render() {
        return (<div>payload</div>);
      }
    }
    /* eslint-enable react/prefer-stateless-function */
    const sendAnalyticEvent = sinon.spy();
    const App = () => <Scope prefix="top"><Scope prefix="c"><Scope prefix="d"><Payload /></Scope></Scope></Scope>;
    const wrapper = mount(<App />, {
      context: {
        sendAnalyticEvent
      },
      childContextTypes: {
        sendAnalyticEvent: PropTypes.func
      }
    });


    wrapper.find(Scope).at(2).instance().context.sendAnalyticEvent('event', 'payload');
    expect(sendAnalyticEvent).to.be.calledWith(['top', 'c', 'event'], 'payload');

    wrapper.find(Payload).instance().context.sendAnalyticEvent('event', 'payload');
    expect(sendAnalyticEvent).to.be.calledWith(['top', 'c', 'd', 'event'], 'payload');
  });

  it('should call record method', () => {
    const Payload = (props, context) => {
      context.sendAnalyticEvent('render');
      return (<div>payload</div>);
    };

    Payload.contextTypes = {
      sendAnalyticEvent: PropTypes.func
    };

    const App = () => <Scope prefix="b"><Scope prefix="c"><Scope prefix="d"><Payload /></Scope></Scope></Scope>;
    const wrapper = mount(<App />, {
      context: {
        logPrefix: ['a']
      },
      childContextTypes: {
        logPrefix: PropTypes.arrayOf(PropTypes.string),
        sendAnalyticEvent: PropTypes.func
      }
    });

    expect(record).to.have.been.calledWithMatch(['a', 'b', 'c', 'd', 'render']);
    wrapper.find(Scope).at(2).instance().context.sendAnalyticEvent('event');
    expect(record).to.have.been.calledWithMatch(['a', 'b', 'c', 'event']);
  });
});
