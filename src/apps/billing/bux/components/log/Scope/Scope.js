import PropTypes from 'prop-types';
import React, { Component } from 'react';

import { record } from 'bux/common/helpers/logger';
import { ErrorBoundary } from 'bux/components/log/ErrorBoundary';

import TransparentComponent from '../../Transparent';


class LogScope extends Component {
  static childContextTypes = {
    logPrefix: PropTypes.arrayOf(PropTypes.string),
    sendAnalyticEvent: PropTypes.func
  };

  static contextTypes = {
    logPrefix: PropTypes.arrayOf(PropTypes.string),
    sendAnalyticEvent: PropTypes.func
  };

  static propTypes = {
    prefix: PropTypes.string,
    className: PropTypes.string,
    children: PropTypes.node
  };

  getChildContext() {
    return {
      logPrefix: this.getLogPrefix(),
      sendAnalyticEvent: this.sendAnalyticEvent
    };
  }

  getLogPrefix() {
    const parentName = this.context.logPrefix;
    if (!this.props.prefix) {
      return parentName;
    }
    const logPrefixElement = [this.props.prefix];
    return parentName ? parentName.concat(logPrefixElement) : logPrefixElement;
  }

  sendAnalyticEvent = (name, payload) => {
    const { prefix } = this.props;
    const thisEvent = typeof name === 'string' ? [name] : name;
    const nextEvent = prefix ? [prefix].concat(thisEvent) : thisEvent;
    if (this.context.sendAnalyticEvent) {
      this.context.sendAnalyticEvent(nextEvent, payload);
    } else {
      const parentName = this.context.logPrefix || [];
      record(parentName.concat(nextEvent), payload);
    }
  };

  render() {
    const { prefix, className, children } = this.props;
    return children ? (
      <TransparentComponent data-logroot={prefix} className={className}>
        <ErrorBoundary>
          {children}
        </ErrorBoundary>
      </TransparentComponent>
    ) : null;
  }
}

export default LogScope;
