import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import LogScope from '../../Scope';
import createLogRoot from '../logRoot';

describe('connectLogRoot component', () => {
  it('Should generate scope', () => {
    const Payload = () => (<div>payload</div>);
    const Wrapped = createLogRoot('testlog')(Payload);
    const wrapper = mount(<Wrapped anotherprop={42} />);

    expect(wrapper.find(LogScope)).to.have.prop('prefix', 'testlog');
    expect(wrapper.find(Payload)).to.have.prop('anotherprop', 42);
  });
});
