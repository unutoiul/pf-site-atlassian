import React from 'react';
import Scope from '../Scope';
/**
 * decorator around {Component}. Add sendAnalyticEvent to context.
 * @param {String} logPrefix
 * @returns {function({Component})}
 */
const connectLogRoot = logPrefix => LogComponent =>
  props => <Scope prefix={logPrefix}><LogComponent {...props} /></Scope>;

export default connectLogRoot;
