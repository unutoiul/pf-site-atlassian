import PropTypes from 'prop-types';
import { Component, createElement } from 'react';

/**
 * wraps redux-form with logger (requires connectLogRoot to be applied before)
 * @param {ReduxForm} component
 * @param {Object} [options]
 * @returns {LogConnectedForm}
 */
const connectLogForm = (component, options = {}) => {
  class LogConnectedForm extends Component {
    constructor() {
      super();
      this.state = {
        timeStart: Date.now()
      };
    }

    onSubmitSuccess = (...args) => {
      this.context.sendAnalyticEvent('submit', {
        timeSpent: Math.floor((Date.now() - this.state.timeStart) / 1000)
      });
      if (options.onSubmitSuccess) {
        options.onSubmitSuccess(...args);
      }
      if (this.props.onSubmitSuccess) {
        this.props.onSubmitSuccess(...args);
      }
    };

    onSubmitFail = (...args) => {
      this.context.sendAnalyticEvent('submitFail', {
        timeSpent: Math.floor((Date.now() - this.state.timeStart) / 1000)
      });
      if (options.onSubmitFail) {
        options.onSubmitFail(...args);
      }
      if (this.props.onSubmitFail) {
        this.props.onSubmitFail(...args);
      }
      // original redux form will throw error in case onSubmit handler is not exists.
      // dont repeat it.
    };

    render() {
      return createElement(component, {
        ...this.props,
        onSubmitSuccess: this.onSubmitSuccess,
        onSubmitFail: this.onSubmitFail
      });
    }
  }

  LogConnectedForm.propTypes = {
    onSubmitSuccess: PropTypes.func,
    onSubmitFail: PropTypes.func
  };

  LogConnectedForm.contextTypes = {
    sendAnalyticEvent: PropTypes.func.isRequired
  };

  return LogConnectedForm;
};


export default connectLogForm;