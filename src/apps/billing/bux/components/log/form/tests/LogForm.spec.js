import PropTypes from 'prop-types';
import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';
import { connect } from 'react-redux';
import createStore from 'bux/core/createStore';
import { reduxForm, formValueSelector, Form } from 'redux-form';
import connectLogForm from '../logForm';

describe('LogForm: ', () => {
  const store = createStore();

  const mapStateToPropsProvider = () => (
    () => ({})
  );

  const createWrapper = (options = {}) => {
    const FormWrapper = ({ handleSubmit, onSubmit }) => (
      <Form onSubmit={handleSubmit(onSubmit)}>
        <button id="something">Submit</button>
      </Form>
    );

    FormWrapper.propTypes = {
      handleSubmit: PropTypes.func.isRequired, // Autowired by redux-forms
      onSubmit: PropTypes.func.isRequired,
      fieldsComponent: PropTypes.func
    };

    const selector = formValueSelector('FormWrapper');

    const mapStateToProps = mapStateToPropsProvider(selector);

    const DecoratedFormWrapper = reduxForm({ ...options, form: 'FormWrapper' })(FormWrapper);
    const LogConnectedForm = connectLogForm(DecoratedFormWrapper, options);
    const ConnectedFormWrapper = connect(mapStateToProps)(LogConnectedForm);

    return ConnectedFormWrapper;
  };

  it('should trigger event on submit', () => {
    const onSubmit = sinon.stub();
    const record = sinon.stub();
    const FormWrapper = createWrapper();

    const wrapper = mount(<FormWrapper store={store} onSubmit={onSubmit} />, {
      context: {
        logPrefix: ['root'],
        sendAnalyticEvent: record,
      },
      childContextTypes: {
        logPrefix: PropTypes.arrayOf(PropTypes.string),
        sendAnalyticEvent: PropTypes.func
      }
    });
    wrapper.find('form').simulate('submit');

    expect(onSubmit).to.have.been.called();
    expect(record).to.have.been.calledWithMatch('submit', { timeSpent: 0 });
  });

  it('should trigger event on error submit', (done) => {
    const onSubmit = sinon.stub();
    const record = sinon.stub();
    onSubmit.returns(Promise.reject());

    const FormWrapper = createWrapper();

    const wrapper = mount(<FormWrapper
      store={store}
      onSubmit={onSubmit}
    />, {
      context: {
        logPrefix: ['root'],
        sendAnalyticEvent: record,
      },
      childContextTypes: {
        logPrefix: PropTypes.arrayOf(PropTypes.string),
        sendAnalyticEvent: PropTypes.func
      }
    });

    wrapper.find('form').props().onSubmit().then(() => {
      expect(onSubmit).to.have.been.called();
      expect(record).to.have.been.calledWithMatch('submitFail', { timeSpent: 0 });
      done();
    });
  });

  it('should trigger exception on error submit', (done) => {
    const onSubmit = () => {
      throw new Error();
    };
    const record = sinon.stub();
    const FormWrapper = createWrapper();

    const wrapper = mount(<FormWrapper
      store={store}
      onSubmit={onSubmit}
    />, {
      context: {
        logPrefix: ['root'],
        sendAnalyticEvent: record,
      },
      childContextTypes: {
        logPrefix: PropTypes.arrayOf(PropTypes.string),
        sendAnalyticEvent: PropTypes.func
      }
    });

    try {
      wrapper.find('form').props().onSubmit();
      expect(false).to.be.equal(true);
    } catch (error) {
      done();
    }
  });


  it('should trigger onSubmitSuccess event on submit', () => {
    const onSubmit = sinon.stub();
    const onSubmitSuccess = sinon.stub();
    const onSubmitFail = sinon.stub();
    const record = sinon.stub();
    const FormWrapper = createWrapper();

    const wrapper = mount(<FormWrapper
      store={store}
      onSubmit={onSubmit}
      onSubmitSuccess={onSubmitSuccess}
      onSubmitFail={onSubmitFail}
    />, {
      context: {
        logPrefix: ['root'],
        sendAnalyticEvent: record,
      },
      childContextTypes: {
        logPrefix: PropTypes.arrayOf(PropTypes.string),
        sendAnalyticEvent: PropTypes.func
      }
    });
    wrapper.find('form').simulate('submit');

    expect(onSubmit).to.have.been.called();
    expect(onSubmitSuccess).to.have.been.called();
    expect(onSubmitFail).to.not.have.been.called();
    expect(record).to.have.been.calledWithMatch('submit', {
      timeSpent: 0
    });
  });

  it('should trigger onSubmitFail event on submit', () => {
    const onSubmit = () => {
      throw new Error();
    };
    const onSubmitSuccess = sinon.stub();
    const onSubmitFail = sinon.stub();
    const optOnSubmitFail = sinon.stub();
    const record = sinon.stub();
    const FormWrapper = createWrapper({
      onSubmitFail: optOnSubmitFail
    });

    const wrapper = mount(<FormWrapper
      store={store}
      onSubmit={onSubmit}
      onSubmitSuccess={onSubmitSuccess}
      onSubmitFail={onSubmitFail}
    />, {
      context: {
        logPrefix: ['root'],
        sendAnalyticEvent: record,
      },
      childContextTypes: {
        logPrefix: PropTypes.arrayOf(PropTypes.string),
        sendAnalyticEvent: PropTypes.func
      }
    });
    wrapper.find('form').simulate('submit');
    // exception will be eaten by onSubmitFail

    expect(onSubmitSuccess).to.not.have.been.called();
    expect(onSubmitFail).to.have.been.called();
    expect(optOnSubmitFail).to.have.been.called();
    expect(record).to.have.been.calledWithMatch('submitFail', {
      timeSpent: 0
    });
  });
});
