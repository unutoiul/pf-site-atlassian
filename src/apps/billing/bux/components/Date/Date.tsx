import { DateFormat, Plural, Trans } from '@lingui/react';
import * as moment from 'moment';
import * as React from 'react';

export const enum Format {
  SHORT = 'short',
  MEDIUM = 'medium',
  FULL = 'full',
}

interface DateProps {
  value: string;
}

interface IntlDateTimeFormat {
  year?: string;
  month?: string;
  day?: string;
  timeZone?: string;
}

interface FormattedDateProps extends DateProps {
  format: Format | IntlDateTimeFormat;
}

const Date: React.SFC<DateProps & {
  format: IntlDateTimeFormat,
}> = ({ value, format }) => {
  const momentDate = moment(value);
  if (!momentDate.isValid()) {
    return null;
  }

  if (!format.timeZone) {
    format.timeZone = 'UTC';
  }

  return <DateFormat value={momentDate.format('YYYY-MM-DD')} format={format} />;
};

export const FullDate: React.SFC<DateProps> = ({ value }) => (
  <Date value={value} format={{ year: 'numeric', month: 'short', day: 'numeric' }} />
);

export const MediumDate: React.SFC<DateProps> = ({ value }) => (
  <Date value={value} format={{ year: '2-digit', month: 'short', day: 'numeric' }} />
);

export const ShortDate: React.SFC<DateProps> = ({ value }) => (
  <Date value={value} format={{ month: 'short', day: 'numeric' }} />
);

export const FormattedDate: React.SFC<FormattedDateProps> = ({ value, format = Format.MEDIUM }) => {
  switch (format) {
    case Format.SHORT:
      return <ShortDate value={value} />;
    case Format.FULL:
      return <FullDate value={value} />;
    case Format.MEDIUM:
      return <MediumDate value={value} />;
    default:
      return <Date value={value} format={format} />;
  }
};

export const DaysRemaining: React.SFC<DateProps> = ({ value }) => {
  const currentDate = moment.utc().startOf('day');
  const futureDate = moment.utc(value);

  const diffDays = Math.max(futureDate.diff(currentDate, 'days'), 0);

  if (diffDays > 30) {
    const diffMonths = futureDate.diff(currentDate, 'months');

    return (
      <Trans id="billing.date.remaining.months">
        <Plural value={diffMonths} one="# month" other="# months"/>
      </Trans>
    );
  }

  return (
    <Trans id="billing.date.remaining.days">
      <Plural value={diffDays} one="# day" other="# days"/>
    </Trans>
  );
};
