import { storiesOf } from '@storybook/react';

import { inContentChrome, render } from 'bux/common/helpers/storybook';

import { FormattedDate, FullDate, MediumDate, ShortDate } from './Date';

storiesOf('BUX|Components/Date', module)
  .add('FullDate', () => inContentChrome(render(FullDate, { value: '20160606' })))
  .add('MediumDate', () => inContentChrome(render(MediumDate, { value: '20160606' })))
  .add('ShortDate', () => inContentChrome(render(ShortDate, { value: '20160606' })))
  .add('FormattedDate', () => inContentChrome(render(FormattedDate, { value: '20160606' })));
