import { DaysRemaining, Format, FormattedDate, FullDate, ShortDate } from './Date';

const getFormat = isAnnual => (isAnnual ? Format.FULL : Format.SHORT);

export default FullDate;
export {
  FullDate,
  ShortDate,
  FormattedDate,
  DaysRemaining,
  getFormat,
  Format,
};
