import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import { DaysRemaining, Format, FormattedDate, FullDate, MediumDate, ShortDate } from '../Date';

describe('Date', () => {
  describe('FullDate component', () => {
    it('should render date', () => {
      const wrapper = mount(<FullDate value="20160606" />);
      expect(wrapper).to.have.text('Jun 6, 2016');
    });

    it('should render an empty date when invalid', () => {
      const wrapper = mount(<FullDate value="" />);
      expect(wrapper.html()).to.be.null();
    });
  });

  describe('Medium component', () => {
    it('should render date', () => {
      const wrapper = mount(<MediumDate value="20160606" />);
      expect(wrapper).to.have.text('Jun 6, 16');
    });
  });

  describe('ShortDate component', () => {
    it('should render date', () => {
      const wrapper = mount(<ShortDate value="20160606" />);
      expect(wrapper).to.have.text('Jun 6');
    });

    it('should render an empty date when invalid', () => {
      const wrapper = mount(<ShortDate value="" />);
      expect(wrapper.html()).to.be.null();
    });
  });

  describe('Auto component', () => {
    it('should pick ShortDate', () => {
      const wrapper1 = mount(<FormattedDate value="20160606" format={Format.SHORT} />);
      expect(wrapper1.find(ShortDate)).to.be.present();
    });

    it('should pick MedDate', () => {
      const wrapper1 = mount(<FormattedDate value="20160606" format={Format.MEDIUM} />);
      expect(wrapper1.find(MediumDate)).to.be.present();
    });

    it('should pick Full', () => {
      const wrapper1 = mount(<FormattedDate value="20160606" format={Format.FULL} />);
      expect(wrapper1.find(FullDate)).to.be.present();
    });
  });

  describe('DaysRemaining', () => {
    let clock;

    beforeEach(() => {
      clock = sinon.useFakeTimers(Date.UTC(2018, 0, 1, 23, 59, 59));
    });

    afterEach(() => {
      clock.restore();
    });

    it('should render 0 days', () => {
      const wrapper = mount(<DaysRemaining value="20180101" />);
      expect(wrapper).to.have.text('0 days');
    });

    it('should render 0 days with past date', () => {
      const wrapper = mount(<DaysRemaining value="20170101" />);
      expect(wrapper).to.have.text('0 days');
    });

    it('should render 1 day', () => {
      const wrapper = mount(<DaysRemaining value="20180102" />);
      expect(wrapper).to.have.text('1 day');
    });

    it('should render 10 days', () => {
      const wrapper = mount(<DaysRemaining value="20180111" />);
      expect(wrapper).to.have.text('10 days');
    });

    it('should render 1 month', () => {
      const wrapper = mount(<DaysRemaining value="20180215" />);
      expect(wrapper).to.have.text('1 month');
    });

    it('should render 2 months', () => {
      const wrapper = mount(<DaysRemaining value="20180315" />);
      expect(wrapper).to.have.text('2 months');
    });
  });
});
