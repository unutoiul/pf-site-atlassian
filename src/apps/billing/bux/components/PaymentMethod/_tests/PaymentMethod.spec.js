import React from 'react';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';
import { PAYMENT_METHOD_PAYPAL, PAYMENT_METHOD_CREDIT_CARD } from 'bux/core/state/billing-details/constants';
import PaymentMethod from '../PaymentMethod';
import CreditCard from '../components/CreditCard';
import Paypal from '../components/Paypal';

describe('PaymentMethod component: ', () => {
  it('should render credit card details', () => {
    const details = {
      name: 'Dr Potato',
      expiryMonth: 0,
      expiryYear: 0,
      suffix: '123',
      type: 'visa'
    };
    const wrapper = mount(<PaymentMethod paymentMethod={PAYMENT_METHOD_CREDIT_CARD} details={details} />);

    expect(wrapper).to.be.present();
    expect(wrapper.find(CreditCard)).to.be.present();
    expect(wrapper.find(CreditCard).find('div.number'))
      .to.have.text('\u2022\u2022\u2022\u2022 \u2022\u2022\u2022\u2022 \u2022\u2022\u2022\u2022 \u2022123');
    expect(wrapper.find(Paypal)).to.not.be.present();
  });
  it('should render credit card details even with maskedCardNumber', () => {
    const details = {
      name: 'Dr Potato',
      expiryMonth: 0,
      expiryYear: 0,
      maskedCardNumber: '1111xxxxxxxx1123',
      type: 'visa'
    };
    const wrapper = mount(<PaymentMethod paymentMethod={PAYMENT_METHOD_CREDIT_CARD} details={details} />);

    expect(wrapper).to.be.present();
    expect(wrapper.find(CreditCard)).to.be.present();
    expect(wrapper.find(CreditCard).find('div.number'))
      .to.have.text('\u2022\u2022\u2022\u2022 \u2022\u2022\u2022\u2022 \u2022\u2022\u2022\u2022 1123');
    expect(wrapper.find(Paypal)).to.not.be.present();
  });
  it('should render paypal account details', () => {
    const details = {
      email: 'user@example.com'
    };
    const wrapper = shallow(<PaymentMethod paymentMethod={PAYMENT_METHOD_PAYPAL} details={details} />);

    expect(wrapper).to.be.present();
    expect(wrapper.find(Paypal)).to.be.present();
    expect(wrapper.find(CreditCard)).to.not.be.present();
  });
});
