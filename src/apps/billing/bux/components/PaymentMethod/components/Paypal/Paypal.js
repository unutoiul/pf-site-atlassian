import React from 'react';
import { shape, string } from 'prop-types';
import PaymentTypeLogo from '../../../PaymentTypeLogo';
import styles from './styles.less';

const Paypal = ({ details: { email } }) => (
  <div data-test="paypal">
    <div className={styles.icon}><PaymentTypeLogo type="paypal" /></div>
    <div className="email">{email}</div>
  </div>
);

Paypal.propTypes = {
  details: shape({
    email: string
  })
};

export default Paypal;
