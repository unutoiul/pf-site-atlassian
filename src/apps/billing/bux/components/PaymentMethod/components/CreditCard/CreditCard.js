import { Trans } from '@lingui/react';
import React from 'react';
import { number, oneOfType, shape, string } from 'prop-types';
import cx from 'classnames';
import { formatCCNumber, getSuffix } from 'bux/common/formatters/cc-number-formatter';
import { padMonth } from 'bux/common/formatters/date-formatter';
import PaymentTypeLogo from '../../../PaymentTypeLogo';
import styles from './styles.less';

const CreditCard = ({
  details: {
    name, expiryMonth, expiryYear, suffix, type, maskedCardNumber
  }
}) => {
  const expiryMonthFormatted = padMonth(expiryMonth);
  return (
    <div className={styles.content} data-test="credit-card">
      <div className={styles.icon}><PaymentTypeLogo type={type} /></div>
      <div>
        <div className={cx('number', styles.number)}>
          {formatCCNumber(suffix || getSuffix(maskedCardNumber), type)}
        </div>
        <div className={cx('name', styles.name)}>{name}</div>
        <div className={cx('expiry', styles.expiry)}>
          <Trans id="billing.credit-card.expires">
            Expires {expiryMonthFormatted}/{expiryYear}
          </Trans>
        </div>
      </div>
    </div>
  );
};

CreditCard.propTypes = {
  details: shape({
    name: string,
    expiryMonth: oneOfType([number, string]),
    expiryYear: oneOfType([number, string]),
    suffix: string,
    type: string,
  }),
};

export default CreditCard;
