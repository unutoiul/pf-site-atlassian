import { storiesOf } from '@storybook/react';
import { inContentChrome, inStoryKnob } from 'bux/common/helpers/storybook';
import { PAYMENT_METHOD_CREDIT_CARD, PAYMENT_METHOD_PAYPAL } from 'bux/core/state/billing-details/constants';
import PaymentMethod from './PaymentMethod';

storiesOf('BUX|Components/PaymentMethod', module)
  .add('Credit Card', () => inContentChrome(inStoryKnob(PaymentMethod, {
    paymentMethod: PAYMENT_METHOD_CREDIT_CARD,
    details: {
      name: 'Storybook Rocks',
      expiryMonth: 1,
      expiryYear: 2020,
      suffix: '1234',
      type: 'VISA',
    }
  })))
  .add('Paypal', () => inContentChrome(inStoryKnob(PaymentMethod, {
    paymentMethod: PAYMENT_METHOD_PAYPAL,
    details: {
      email: 'storybook@test.org'
    }
  })));
