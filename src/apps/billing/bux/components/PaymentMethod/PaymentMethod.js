import React from 'react';
import { oneOfType, shape, string } from 'prop-types';
import { PAYMENT_METHOD_PAYPAL, PAYMENT_METHOD_CREDIT_CARD } from 'bux/core/state/billing-details/constants';
import CreditCard from './components/CreditCard';
import Paypal from './components/Paypal';

const PaymentMethod = ({ paymentMethod, ...rest }) => (
  <div data-test="payment-method">
    {paymentMethod === PAYMENT_METHOD_PAYPAL && <Paypal {...rest} />}
    {paymentMethod === PAYMENT_METHOD_CREDIT_CARD && <CreditCard {...rest} />}
  </div>
);

PaymentMethod.propTypes = {
  paymentMethod: string,
  details: oneOfType([
    CreditCard.propTypes.details,
    Paypal.propTypes.details,
    shape({})
  ])
};

export default PaymentMethod;
