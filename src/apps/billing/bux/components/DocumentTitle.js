import { I18n } from '@lingui/react';
import PropTypes from 'prop-types';
import React from 'react';
import DocumentTitle from 'react-document-title';

const BUXDocumentTitle = ({ title, children }) => (
  title
    ? <I18n>{({ i18n }) => (
      <DocumentTitle title={i18n.t('billing.page-title')`Billing – ${title}`}>
        {children}
        </DocumentTitle>
    )}</I18n>
    : React.Children.only(children)
);

BUXDocumentTitle.propTypes = {
  children: PropTypes.node,
  title: PropTypes.node,
};

export default BUXDocumentTitle;
