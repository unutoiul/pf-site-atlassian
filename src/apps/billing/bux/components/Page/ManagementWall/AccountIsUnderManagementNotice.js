import React from 'react';
import { getHostname } from 'bux/common/helpers/browser';
import ShoppingCart from 'bux/static/adg3/shopping-bag-1.svgx';
import Prompt from '../Prompt';

const AccountIsUnderManagementNotice = () => (
  <Prompt
    className="account-under-management-prompt"
    media={<ShoppingCart />}
    title={<span>Account is under external management</span>}
    message={
      <span>Please contact your Expert or reseller to make any changes to your <strong>{getHostname()}</strong></span>
    }
  />
);

export default AccountIsUnderManagementNotice;
