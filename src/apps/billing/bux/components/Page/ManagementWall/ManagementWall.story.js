import { storiesOf } from '@storybook/react';
import { render, inContentChrome } from 'bux/common/helpers/storybook';

import AccountIsUnderManagementNotice from './AccountIsUnderManagementNotice';

storiesOf('BUX|Components/Page/ManagementWall', module)
  .add('AccountIsUnderManagementNotice [Prompt]', () => inContentChrome(render(AccountIsUnderManagementNotice)));

