import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';
import proxyquire from 'bux/helpers/proxyquire';
import { Provider } from 'bux/helpers/redux';

describe('ManagementWall', () => {
  let shouldNotBeManaged;
  let Wall;

  let isManagedByPartner;

  const WallSlot = () => <div>slot 1</div>;
  const PayloadSlot = () => <div>slot 2</div>;

  beforeEach(() => {
    isManagedByPartner = sinon.stub();
    const mocked = proxyquire.load('../ManagementWall', {
      'bux/core/state/billing-details/selectors': { isManagedByPartner }
    });

    Wall = mocked.default;
    shouldNotBeManaged = mocked.shouldNotBeManaged;
  });

  it('should display children when not under management', () => {
    isManagedByPartner.returns(false);
    const wrapper = mount(<Provider><Wall wall={<WallSlot />}><PayloadSlot /></Wall></Provider>);
    expect(wrapper.find(WallSlot)).not.to.be.present();
    expect(wrapper.find(PayloadSlot)).to.be.present();
  });

  it('should display wall when under management', () => {
    isManagedByPartner.returns(true);
    const wrapper = mount(<Provider><Wall wall={<WallSlot />}><PayloadSlot /></Wall></Provider>);
    expect(wrapper.find(WallSlot)).to.be.present();
    expect(wrapper.find(PayloadSlot)).not.to.be.present();
  });

  it('should render component using shouldNotBeManaged', () => {
    isManagedByPartner.returns(false);
    const Wrapped = shouldNotBeManaged(PayloadSlot);
    const wrapper = mount(<Provider><Wrapped /></Provider>);
    expect(wrapper.find(Wall)).to.be.present();
    expect(wrapper.find(PayloadSlot)).to.be.present();
  });
});
