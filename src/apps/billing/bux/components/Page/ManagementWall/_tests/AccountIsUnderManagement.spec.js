import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import AccountIsUnderManagementNotice from '../AccountIsUnderManagementNotice';

describe('AccountIsUnderManagement', () => {
  it('show setup Prompt', () => {
    const wrapper = mount(<AccountIsUnderManagementNotice />);
    expect(wrapper).to.contain.text('Account is under external management');
    expect(wrapper).to.contain.text('Please contact your Expert or reseller');
  });
});