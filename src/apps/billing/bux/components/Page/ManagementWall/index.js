import ManagementWall, { shouldNotBeManaged } from './ManagementWall';

export { shouldNotBeManaged };

export default ManagementWall;