import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { isManagedByPartner } from 'bux/core/state/billing-details/selectors';
import AccountIsUnderManagementNotice from './AccountIsUnderManagementNotice';

const ManagementWall = ({ flag, wall, children }) => (
  flag ? wall : children
);

ManagementWall.propTypes = {
  flag: PropTypes.bool.isRequired,
  wall: PropTypes.node,
  children: PropTypes.node
};

const mapStateToProps = state => ({
  flag: isManagedByPartner(state),
});

const ConnectedManagementWall = connect(mapStateToProps)(ManagementWall);

export const shouldNotBeManaged = Component =>
  props => (
    <ConnectedManagementWall wall={<AccountIsUnderManagementNotice />}>
      <Component {...props} />
    </ConnectedManagementWall>
  );

export default ConnectedManagementWall;
