import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { Analytics } from 'bux/components/Analytics';
import { getOrganizationId } from 'bux/core/state/meta/selectors';
import cx from 'classnames';
import DocumentTitle from '../DocumentTitle';
import PageHeader from '../PageHeader';
import LogScope from '../log/Scope';
import { WithPageLevelEvents } from '../log/with/pageLevel';
import styles from './Page.less';
import { FocusedTaskAdapter } from '../../../adapters/Drawer';

export const PageComponent = ({
  title, back, documentTitle, subTitle = '', logPrefix, layout, children, orgId, breadcrumbs,
}) => (
  <LogScope prefix={logPrefix}>
    <DocumentTitle title={documentTitle || title}>
      <WithPageLevelEvents>
        <div style={{ zIndex: 0, position: 'relative' }} className={cx('page-detail', styles.container)}>
          <LogScope prefix="backNavigation">
            { back }
          </LogScope>
          {title && <PageHeader title={title} subTitle={subTitle} layout={layout} breadcrumbs={breadcrumbs} />}

          {/* key property is to be sure that page is remounted when orgId is changed */}
          <div className={styles.content} key={orgId}>
            {children}
          </div>
        </div>
      </WithPageLevelEvents>
    </DocumentTitle>
  </LogScope>
);
PageComponent.propTypes = {
  children: PropTypes.node,
  title: PropTypes.node,
  subTitle: PropTypes.node,
  documentTitle: PropTypes.string,
  logPrefix: PropTypes.string,
  layout: PropTypes.string,
  orgId: PropTypes.string,
  breadcrumbs: PropTypes.node,
  back: PropTypes.node,
};

const PageContent = ({ WrappedComponent, props = {}, screenAnalyticsProps }) => {
  const screenProps = typeof screenAnalyticsProps === 'function' ? screenAnalyticsProps(props) : screenAnalyticsProps;
  return (
    <Analytics.Screen {...screenProps}>
      <FocusedTaskAdapter />
      <div className="page-content">
        <WrappedComponent {...props} />
      </div>
    </Analytics.Screen>
  );
}; 

PageContent.propTypes = {
  WrappedComponent: PropTypes.func.isRequired,
  props: PropTypes.object,
  screenAnalyticsProps: PropTypes.oneOfType([PropTypes.func.isRequired, PropTypes.object]).isRequired
};

export const asPageContent = screenAnalyticsProps => WrappedComponent => props =>
  <PageContent WrappedComponent={WrappedComponent} screenAnalyticsProps={screenAnalyticsProps} props={props} />;

const mapStateToProps = state => ({
  orgId: getOrganizationId(state),
});

export default connect(mapStateToProps)(PageComponent);
