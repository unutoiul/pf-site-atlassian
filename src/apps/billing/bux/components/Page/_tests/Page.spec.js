import React, { Component } from 'react';
import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import { Provider } from 'bux/helpers/redux';
import createStore from 'bux/core/createStore';
import sinon from 'sinon';
import Loading from '../../Loading';
import PageHeader from '../../PageHeader';
import getAnalytic from '../../log/helpers/getAnalytic';

import Page, { PageComponent } from '../Page';

describe('Page component', () => {
  const store = createStore();
  it('should render PageHeader', () => {
    const wrapper = mount(<Provider store={store}>
        <Page title="supernova detail" />
      </Provider>);
    const pageHeader = wrapper.find(PageHeader);
    expect(pageHeader).to.be.present();
    expect(pageHeader).to.have.prop('title', 'supernova detail');
  });

  it('should not render PageHeader if prop is missing', () => {
    const wrapper = mount(<Provider store={store}><Page /></Provider>);
    const pageHeader = wrapper.find(PageHeader);
    expect(pageHeader).not.to.be.present();
  });

  it('should emit pageView events', () => {
    const { sendAnalyticEvent, context } = getAnalytic();
    mount(<Provider store={store}><Page logPrefix="test-page" /></Provider>, context);
    expect(sendAnalyticEvent).to.be.calledWithMatch(['test-page', 'pageView']);
  });

  it('should render children ', () => {
    const wrapper = shallow(<Provider store={store}>
      <Page title="title">
        <div>supernova content</div>
      </Page>
    </Provider>);
    expect(wrapper).to.contain(<div>supernova content</div>);
    expect(wrapper.find(Loading)).to.not.be.present();
  });

  describe('for org id changes', () => {
    let lastWrapper;
    let orgId;
    const setOrgQueryParam = (id) => {
      orgId = id;
      if (lastWrapper) {
        lastWrapper.setProps({ orgId });
      }
    };

    afterEach(() => {
      lastWrapper = '';
    });

    const getMountingSpiesOfChildren = () => {
      const mountSpy = sinon.spy();
      const unmountSpy = sinon.spy();

      class Content extends Component {
        componentWillMount() {
          mountSpy();
        }

        componentWillUnmount() {
          unmountSpy();
        }

        render() {
          return <div>supernova content</div>;
        }
      }

      const wrapper = mount(<PageComponent title="title" orgId={orgId} analytics={{ name: 'test' }}>
        <Content />
      </PageComponent>);
      lastWrapper = wrapper;
      return { mountSpy, unmountSpy, wrapper };
    };

    it('should rerender children when orgId is changed', () => {
      setOrgQueryParam('some-org-uuid1');
      const { mountSpy, unmountSpy } = getMountingSpiesOfChildren();

      setOrgQueryParam('some-org-uuid2');

      expect(mountSpy).to.have.been.calledTwice();
      expect(unmountSpy).to.have.been.calledOnce();
    });

    it('should rerender children when orgId is removed', () => {
      setOrgQueryParam('some-org-uuid');
      const { mountSpy, unmountSpy } = getMountingSpiesOfChildren();

      setOrgQueryParam();

      expect(mountSpy).to.have.been.calledTwice();
      expect(unmountSpy).to.have.been.calledOnce();
    });

    it('should not rerender children when orgId have not changed', () => {
      setOrgQueryParam('some-org-uuid');
      const { mountSpy, unmountSpy } = getMountingSpiesOfChildren();

      setOrgQueryParam('some-org-uuid');

      expect(mountSpy).to.have.been.calledOnce();
      expect(unmountSpy).not.to.be.called();
    });
  });
});
