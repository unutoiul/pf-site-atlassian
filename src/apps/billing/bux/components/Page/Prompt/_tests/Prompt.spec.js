import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Prompt from '../Prompt';

describe('Prompt', () => {
  const Payload = () => <div>42</div>;

  const slots = {
    media: '.prompt-image',
    title: '.main-message',
    message: '.sub-message',
    actions: '.prompt-actions'
  };

  Object
    .keys(slots)
    .forEach(slot =>
      it(`should display ${slot}`, () => {
        const wrapper = shallow(<Prompt {...{ [slot]: <Payload /> }} />);
        expect(wrapper.find(slots[slot]).find(Payload)).to.be.present();
      }));
});
