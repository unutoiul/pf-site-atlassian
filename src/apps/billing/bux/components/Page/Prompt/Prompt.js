import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import styles from './Prompt.less';

const Prompt = ({
  className, media, title, message, actions 
}) => (
  <div className={cx(className, styles.paymentDetailsPrompt)}>
    {
      media && <div className={cx('prompt-image', styles.promptImage)}>
        {media}
      </div>
    }
    {
      title && <h3 className={cx('main-message', styles.mainMessage)}>
        {title}
      </h3>
    }
    {
      message && <div className={cx('sub-message', styles.subMessage)}>
        {message}
      </div>
    }
    {
      actions && <div className={cx('prompt-actions', styles.promptActions)}>
        {actions}
      </div>
    }
  </div>
);

Prompt.propTypes = {
  className: PropTypes.string,
  media: PropTypes.node,
  title: PropTypes.node,
  message: PropTypes.node,
  actions: PropTypes.node,
};

export default Prompt;