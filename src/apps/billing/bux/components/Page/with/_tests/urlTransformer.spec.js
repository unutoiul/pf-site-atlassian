import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';
import proxyquire from 'bux/helpers/proxyquire';
import { Provider } from 'bux/helpers/redux';
import createStore from 'bux/core/createStore';

/* eslint-disable react/prefer-stateless-function */
class Component extends React.Component {
  render() {
    return <span>test</span>;
  }
}

/* eslint-enable react/prefer-stateless-function */

describe('with UrlTransformer', () => {
  describe('withUrlTransformer', () => {
    it('should read Transformed from context and set urlTransform prop to subComponent', () => {
      const { withUrlTransformer, TransformerContext } = proxyquire.load('../urlTransformer');
      const Transform = sinon.stub();
      const Wrapped = withUrlTransformer(Component);
      const wrapper = mount(<div>
        <TransformerContext.Provider value={Transform}>
          <Wrapped prop1={42} />
        </TransformerContext.Provider>
                            </div>);
      const subComponent = wrapper.find(Component);

      expect(subComponent).to.be.present();
      expect(subComponent.props()).to.deep.equal({ prop1: 42, urlTransform: Transform });
    });
  });

  describe('withUrlTransformerProvided', () => {
    let subComponent;
    let getOrganizationId;
    let urlTransformer;
    const url = '/overview';

    const createComponent = ({ path = '/bux/:orgId' } = {}) => {
      const { withUrlTransformerProvided, withUrlTransformer, DeepURLProvider } = proxyquire.load('../urlTransformer', {
        'bux/core/state/meta/selectors': { getOrganizationId }
      });

      const Parent = () => <div><Component /></div>;
      const Wrapped = withUrlTransformerProvided(withUrlTransformer(Parent));
      const store = createStore();
      const wrapper = mount(<Provider store={store}>
        <DeepURLProvider path={path}>
          <Wrapped />
        </DeepURLProvider>
                            </Provider>);
      subComponent = wrapper.find(Parent);
      urlTransformer = subComponent.props().urlTransform;
    };

    before(() => {
      getOrganizationId = sinon.stub().returns('123');
      createComponent();
    });

    it('should have set urlTransformer context to subComponent', () => {
      expect(getOrganizationId).to.be.called();
      expect(subComponent).to.be.present();
      expect(subComponent.props()).to.have.property('urlTransform');
    });
    it('should return transformed url when call urlTransformer', () => {
      expect(urlTransformer(url)).to.be.equal('/bux/123/overview');
    });

    it('should return same url when there is no org id provided', () => {
      getOrganizationId = sinon.stub().returns(undefined);
      createComponent();
      expect(urlTransformer(url)).to.be.equal(`/bux/undefined${url}`);
    });

    it('should return same url when there is no org used', () => {
      getOrganizationId = sinon.stub().returns('42');
      createComponent({ path: 'bux' });
      expect(urlTransformer(url)).to.be.equal(`bux${url}`);
    });
  });
});
