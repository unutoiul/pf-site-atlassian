import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getOrganizationId, getCloudId } from 'bux/core/state/meta/selectors';

const MatchContext = React.createContext();
export const TransformerContext = React.createContext();

export const withUrlTransformer = (WrappedComponent) => {
  const TransformedComponent = props => (
    <TransformerContext.Consumer>
      {urlTransform => <WrappedComponent {...props} urlTransform={urlTransform} />}
    </TransformerContext.Consumer>
  );
  return TransformedComponent;
};

export const DeepURLProvider = ({ path, children }) => (
  <MatchContext.Provider value={path}>
    {children}
  </MatchContext.Provider>
);
DeepURLProvider.propTypes = {
  path: PropTypes.any,
  children: PropTypes.node,
};

const transform = (url, path = '', replace) =>
  path.replace(/:(orgId|cloudId)/, match => replace[match.substr(1)]) + url;

export const withUrlTransformerProvided = (WrappedComponent) => {
  const WithTransformer = ({ organizationId: orgId, cloudId, ...rest }) => (
    <MatchContext.Consumer>
      {(path = '') => (
        <TransformerContext.Provider
          value={url => transform(url, path, { orgId, cloudId })}
        >
          <WrappedComponent {...rest} />
        </TransformerContext.Provider>
      )}
    </MatchContext.Consumer>
  );
  WithTransformer.propTypes = {
    organizationId: PropTypes.string,
    cloudId: PropTypes.string,
  };

  return connect(state => ({
    organizationId: getOrganizationId(state),
    cloudId: getCloudId(state),
  }))(WithTransformer);
};
