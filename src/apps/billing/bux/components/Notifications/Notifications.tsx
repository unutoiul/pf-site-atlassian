import * as React from 'react';

import AkSuccessIcon from '@atlaskit/icon/glyph/check-circle';
import AkErrorIcon from '@atlaskit/icon/glyph/editor/error';
import AkInfoIcon from '@atlaskit/icon/glyph/editor/info';
import { colors as AkColors } from '@atlaskit/theme';

import { FlagAppearance, FlagDescriptor } from 'common/flag';

import { closeNotification } from 'bux/core/state/notifications/actions';

interface NotificationActionType {
  content: string;
  key: string;
  onClick();
}

interface NotificationType {
  template: string;
  group: string; // only for better story organization
  id: string;
  title: string;
  message?: string;
  type: FlagAppearance;
  persist?: boolean;
  actions?: NotificationActionType[];
  dismissAction?();
}

export const notifications = (i18n: any, props: any = {}): NotificationType[] => {
  const entitlementName = props.entitlementName;
  const editionName = props.editionName;

  return [
    {
      template: 'payment-method-success',
      group: 'billing-details',
      id: `payment-method-success${Date.now()}`,
      title: i18n.t('billing.notification.payment-method-success.title')`Payment method updated`,
      type: 'success',
    },
    {
      template: 'billing-details-success',
      group: 'billing-details',
      id: `payment-success${Date.now()}`,
      title: i18n.t('billing.notification.billing-details-success.title')`Billing details updated`,
      type: 'success',
    },
    {
      template: 'payment-error',
      group: 'billing-details',
      id: `payment-error${Date.now()}`,
      title: i18n.t('billing.notification.payment-error.title')`Failed to update billing details`,
      message: i18n.t('billing.notification.payment-error.message')`Please try again, or contact support if the problem persists`,
      type: 'error',
    },
    {
      template: 'deactivate',
      group: 'deactivate',
      id: 'deactivate',
      title: i18n.t('billing.notification.deactivate.title')`Successfully deactivated`,
      message: i18n.t('billing.notification.deactivate.message')`You are no longer subscribed to ${entitlementName}.`,
      type: 'success',
    },
    {
      template: 'deactivate-error',
      group: 'deactivate',
      id: 'deactivate',
      title: i18n.t('billing.notification.deactivate-error.title')`Something went wrong`,
      message: i18n.t('billing.notification.deactivate-error.message')`Sorry, we couldn't deactivate ${entitlementName}. Please try again`,
      type: 'error',
    },
    {
      template: 'billing-address-error',
      group: 'billing-address',
      id: 'payment-error',
      title: i18n.t('billing.notification.billing-address-error.title')`Aww shoot, you're missing billing address`,
      message: i18n.t('billing.notification.billing-address-error.message')`Correct the errors above and we'll try again!`,
      type: 'error',
    },
    {
      template: 'billing-method-error',
      group: 'billing-method',
      id: 'payment-error',
      title: i18n.t('billing.notification.billing-method-error.title')`Aww shoot, you're missing payment method`,
      message: i18n.t('billing.notification.billing-method-error.message')`Correct the errors above and we'll try again!`,
      type: 'error',
    },
    {
      template: 'billing-details-error',
      group: 'payment-details-form',
      id: 'payment-error',
      title: i18n.t('billing.notification.billing-details-error.title')`Aww shoot, you're missing billing details`,
      message: i18n.t('billing.notification.billing-details-error.message')`Correct the errors above and we'll try again!`,
      type: 'error',
    },
    {
      template: 'change-edition-success',
      group: 'change-edition',
      id: 'changeEdition',
      title: i18n.t('billing.notification.change-edition-success.title')`Plan successfully updated`,
      message: i18n.t('billing.notification.change-edition-success.message')`You are now on ${entitlementName} ${editionName}`,
      type: 'success',
    },
    {
      template: 'change-edition-error',
      group: 'change-edition',
      id: 'changeEdition',
      title: i18n.t('billing.notification.change-edition-error.title')`Something went wrong`,
      message: i18n.t('billing.notification.change-edition-error.message')`Sorry, we couldn't change the plan for ${entitlementName}. Please try again`,
      type: 'error',
    },
    {
      template: 'process-order-success',
      group: 'monthly-to-annual',
      id: 'process-order-success',
      title: i18n.t('billing.notification.process-order-success.title')`The order was processed successfully.`,
      type: 'success',
    },
    {
      template: 'convert-to-annual-downloading-quote-info',
      group: 'monthly-to-annual',
      id: 'convert-to-annual-downloading-quote-info',
      title: i18n.t('billing.notification.convert-to-annual-downloading-quote-info.title')`Please wait until the download begins...`,
      type: 'info',
    },
    {
      template: 'convert-to-annual-preparing-quote-info',
      group: 'monthly-to-annual',
      id: 'convert-to-annual-preparing-quote-info',
      title: i18n.t('billing.notification.convert-to-annual-preparing-quote-info.title')`Preparing the PDF, please wait.`,
      type: 'info',
    },
    {
      template: 'convert-to-annual-download-quote-error',
      group: 'monthly-to-annual',
      id: 'convert-to-annual-download-quote-error',
      title: i18n.t('billing.notification.convert-to-annual-download-quote-error.title')`Quote generation failed`,
      type: 'error',
    },
    {
      template: 'resubscribe',
      group: 'resubscribe',
      id: 'resubscribe',
      title: i18n.t('billing.notification.resubscribe.title')`You're ready to go`,
      message: i18n.t('billing.notification.resubscribe.message')`You have been resubscribed successfully`,
      type: 'success',
    },
    {
      template: 'update-billing-contact-error',
      group: 'billing-contact',
      id: `update-billing-contact-${Date.now()}`,
      title: i18n.t('billing.notification.update-billing-contact-error.title')`Failed to update your billing contact`,
      message: i18n.t('billing.notification.update-billing-contact-error.message')`Please try again, or contact support if the problem persists`,
      type: 'error',
    },
    {
      template: 'create-quote-error',
      group: 'bill-estimate',
      id: `create-quote-${Date.now()}`,
      title: i18n.t('billing.notification.create-quote-error.title')`Failed to create quote`,
      message: i18n.t('billing.notification.create-quote-error.message')`Please try again, or contact support if the problem persists`,
      type: 'error',
    },
    {
      template: 'expired-account',
      group: 'expired-account',
      id: 'expired-account',
      title: i18n.t('billing.notification.expired-account.title')`Your subscription has expired.`,
      message: i18n.t('billing.notification.expired-account.message')`You will be unable to activate new products until payment is made.`,
      dismissAction: props.closeExpiredAccount,
      persist: true,
      actions: [{
        content: i18n.t('billing.notification.expired-account.actions.update')`Update billing details`,
        onClick: props.openBillingDetails, key: 'expired-account',
      }],
      type: 'error',
    },
    {
      template: 'payment-failure-default',
      group: 'tns',
      id: `payment-failure${Date.now()}`,
      title: i18n.t('billing.notification.payment-failure-default.title')`Transaction could not be processed`,
      message: i18n.t('billing.notification.payment-failure-default.message')`Try a different card or payment type`,
      type: 'error',
    },
    {
      template: 'payment-failure-EXTERNAL_VERIFICATION_DECLINED',
      group: 'tns',
      id: `payment-failure${Date.now()}`,
      title: i18n.t('billing.notification.payment-failure-declined.title')`Transaction declined`,
      message: i18n.t('billing.notification.payment-failure-declined.message')`Declined by issuer`,
      type: 'error',
    },
    {
      template: 'payment-failure-EXTERNAL_VERIFICATION_DECLINED_EXPIRED_CARD',
      group: 'tns',
      id: `payment-failure${Date.now()}`,
      title: i18n.t('billing.notification.payment-failure-expired.title')`Transaction declined`,
      message: i18n.t('billing.notification.payment-failure-expired.message')`Expired card`,
      type: 'error',
    },
    {
      template: 'payment-failure-EXTERNAL_VERIFICATION_DECLINED_INVALID_CSC',
      group: 'tns',
      id: `payment-failure${Date.now()}`,
      title: i18n.t('billing.notification.payment-failure-invalid-csc.title')`Transaction declined`,
      message: i18n.t('billing.notification.payment-failure-invalid-csc.message')`Invalid card security code`,
      type: 'error',
    },
    {
      template: 'payment-failure-EXTERNAL_VERIFICATION_PROCESSING_ERROR',
      group: 'tns',
      id: `payment-failure${Date.now()}`,
      title: i18n.t('billing.notification.payment-failure-process.title')`Transaction declined`,
      message: i18n.t('billing.notification.payment-failure-process.message')`Failed to process your credit card`,
      type: 'error',
    },
    {
      template: 'payment-failure-invalid_fields',
      group: 'tns',
      id: `payment-failure${Date.now()}`,
      title: i18n.t('billing.notification.payment-failure-invalid-fields.title')`Some fields are incorrect`,
      message: i18n.t('billing.notification.payment-failure-invalid-fields.message')`Provide correct card information`,
      type: 'error',
    },
    {
      template: 'payment-failure-request_timeout',
      group: 'tns',
      id: `payment-failure${Date.now()}`,
      title: i18n.t('billing.notification.payment-failure-timeout.title')`Information outdated`,
      message: i18n.t('billing.notification.payment-failure-timeout.message')`Try reloading the page`,
      type: 'error',
    },
    {
      template: 'payment-failure-system_error',
      group: 'tns',
      id: `payment-failure${Date.now()}`,
      title: i18n.t('billing.notification.payment-failure-system.title')`Payment system failure`,
      message: i18n.t('billing.notification.payment-failure-system.message')`Try reloading the page`,
      type: 'error',
    },
    {
      template: 'payment-failure-unknown',
      group: 'tns',
      id: `payment-failure${Date.now()}`,
      title: i18n.t('billing.notification.payment-failure-unknown.title')`Payment system failure`,
      message: i18n.t('billing.notification.payment-failure-unknown.message')`Try reloading the page`,
      type: 'error',
    },
    {
      template: 'survey',
      group: 'survey',
      id: 'survey',
      title: i18n.t('billing.notification.survey.title')`We'd like to hear from you`,
      message: i18n.t('billing.notification.survey.message')`Help us improve your billing experience. Take this 30 second survey`,
      dismissAction: props.closeCSATSurvey,
      actions: [
        { content: i18n.t('billing.notification.survey.actions.sure')`Sure, I am happy to help`, onClick: props.getCSATSurvey, key: 'survey' },
      ],
      type: 'info',
    },
    {
      template: 'chunk-loading-error',
      group: 'generic',
      id: `loading-${props.chunkName}`,
      title: i18n.t('billing.notification.chunk-loading-error.title')`We cant load information`,
      message: i18n.t('billing.notification.chunk-loading-error.message')`An error occurred. Try to repeat action.`,
      type: 'error',
    },
    {
      template: 'network-error',
      group: 'generic',
      id: 'networkError',
      title: i18n.t('billing.notification.network-error.title')`Network error`,
      message: i18n.t('billing.notification.network-error.message')`Try repeating this action in a few moments`,
      type: 'error',
    },
  ];
};

export const findTemplate = (i18n: any, template: string, props: any = {}): NotificationType | undefined => (
  notifications(i18n, props).find(notification => notification.template === template)
);

const TYPE_TO_CLASS_MAP = {
  success: <AkSuccessIcon label="success" secondaryColor={AkColors.G400} />,
  error: <AkErrorIcon label="error" secondaryColor={AkColors.R300} />,
  info: <AkInfoIcon label="information" secondaryColor={AkColors.N500} />,
};

const convertActions = (dispatch, actions) => actions.map(({ content, onClick }) => ({
  content,
  onClick: () => dispatch(onClick),
}));

export const adapt = (payload, dispatch?): FlagDescriptor => {
  return {
    id: payload.id,
    title: payload.title,
    description: payload.message,
    onDismissed: () => {
      if (payload.dismissAction) {
        dispatch(payload.dismissAction);
      }
      dispatch(closeNotification(payload.id));
    },
    actions: payload.actions && convertActions(dispatch, payload.actions),
    icon: TYPE_TO_CLASS_MAP[payload.type],
    appearance: payload.type === 'info' ? 'normal' : payload.type,
    autoDismiss: !payload.persist,
  };
};
