import { I18n } from '@lingui/react';
import { storiesOf } from '@storybook/react';
import * as React from 'react';

import AkFlag from '@atlaskit/flag';

import { inContentChrome } from 'bux/common/helpers/storybook';
import { StoryDelay } from 'bux/common/helpers/storybook/StoryDelay';
import { adapt, notifications } from 'bux/components/Notifications/Notifications';

class ExpandAll extends React.PureComponent {
  public componentDidMount() {
    const expandSpan = document.querySelectorAll('button > span[aria-label="Toggle flag body"]');
    if (expandSpan.length === 0) {
      return;
    }

    for (const key in expandSpan) {
      if (!expandSpan.hasOwnProperty(key)) {
        continue;
      }
      const button = expandSpan[key].parentElement;
      if (button) {
        button.click();
      }
    }
  }

  public render() {
    return <StoryDelay timeout={1000}>{this.props.children}</StoryDelay>;
  }
}

const props = { entitlementName: 'Stride', editionName: 'Free' };

const renderNotifications = (groups: string[]) => (
  inContentChrome((
    <I18n>{({ i18n }) => (
      <ExpandAll>
        {notifications(i18n, props).filter(notification => groups.includes(notification.group)).map(notification => (
          <div key={notification.template} style={{ width: '400px', marginBottom: '12px' }}>
            <AkFlag {...adapt(notification)} isDismissAllowed={true} />
          </div>
        ))}
      </ExpandAll>
    )}</I18n>
  )));

storiesOf('BUX|Notifications', module)
  .add('Generic', () => renderNotifications(['generic']))
  .add('Survey', () => renderNotifications(['survey']))
  .add('Billing details', () => renderNotifications(['billing-details', 'billing-address', 'billing-method', 'payment-details-form', 'billing-contact']))
  .add('Resubscribe & Deactivate', () => renderNotifications(['resubscribe', 'deactivate']))
  .add('Generate quote', () => renderNotifications(['bill-estimate']))
  .add('Expired account', () => renderNotifications(['expired-account']))
  .add('Change edition', () => renderNotifications(['change-edition']))
  .add('TNS', () => renderNotifications(['tns']))
  .add('Monthly to annual', () => renderNotifications(['monthly-to-annual']));
