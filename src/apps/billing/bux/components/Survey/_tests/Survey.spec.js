import Textarea from '@atlaskit/textarea';
import PropTypes from 'prop-types';
import React from 'react';
import sinon from 'sinon';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';

import { Survey as DefaultExport, SurveyComponent, InputWithAnalytics } from '../Survey';

describe('Survey', () => {
  let options;
  beforeEach(() => {
    options = {
      type: 'radio',
      items: [
        { key: '1', label: 'label1' },
        { key: '2', label: 'label2' },
        { key: '3', label: 'label3' },
        { key: '4', label: 'label4' },
        { key: '5', label: 'label5' },
      ]
    };
  });

  const getAnalytic = () => {
    const sendAnalyticEvent = sinon.spy();
    return {
      sendAnalyticEvent,
      context: {
        context: {
          sendAnalyticEvent,
        },
        childContextTypes: {
          sendAnalyticEvent: PropTypes.func
        }
      }
    };
  };

  afterEach(() => {
  });

  it('should render all options + other', () => {
    const wrapper = shallow(<SurveyComponent options={options} />);
    const radioButtons = wrapper.find(InputWithAnalytics).findWhere(w => w.prop('type') === 'radio');

    expect(radioButtons).to.be.length(options.items.length);
  });

  it('should render comment box if other is checked', () => {
    const askOptions = {
      type: 'radio',
      items: [
        { key: 'key0', label: 'label0' },
        { key: 'key1', label: 'label1', nested: { ask: { placeholder: 'Whatsup?' } } },
      ]
    };

    const wrapper = shallow(<SurveyComponent options={askOptions} />);

    expect(wrapper.find(Textarea)).not.to.be.present();

    wrapper.find('[value="key1"]').simulate('change', { target: { type: 'radio', name: 'survey', value: 'key1' } });

    expect(wrapper.find(Textarea)).to.be.present();
  });

  it('should override submit label', () => {
    const label = 'Potato';
    const wrapper = shallow(<SurveyComponent options={options} submitLabel={label} />);
    const submitButton = wrapper.find('.button-submit');

    expect(submitButton.children()).to.contain.text(label);
  });

  it('should override cancel label', () => {
    const label = 'Potato';
    const wrapper = shallow(<SurveyComponent options={options} cancelLabel={label} />);
    const cancelButton = wrapper.find('.button-cancel');

    expect(cancelButton.children()).to.contain.text(label);
  });

  it('should call submitAction on submit with empty object', () => {
    const { sendAnalyticEvent, context } = getAnalytic();
    const action = sinon.spy();
    const wrapper = shallow(<SurveyComponent options={options} submitAction={action} />, context);
    const submitButton = wrapper.find('.button-submit');

    submitButton.simulate('click', {
      preventDefault: () => ({})
    });

    expect(action).to.have.been.calledWith({ selected: 'skipped', message: '', comment: '' });
    expect(sendAnalyticEvent)
      .to.have.been.calledWithMatch('result.skipped', { selected: 'skipped', message: '', comment: '' });
  });

  it('should disable submitAction if filling form is required', () => {
    const { context } = getAnalytic();
    const action = sinon.spy();
    const wrapper = shallow(<SurveyComponent options={options} submitAction={action} required />, context);

    expect(wrapper.update().find('.button-submit').props().disabled).to.be.true();

    wrapper.find('[value="1"]')
      .simulate('change', { target: { type: 'radio', name: 'survey', value: '1' } });

    expect(wrapper.update().find('.button-submit').props().disabled).to.be.false();
  });

  it('should pass submitAction selected survey option', () => {
    const { sendAnalyticEvent, context } = getAnalytic();
    const action = sinon.spy();
    const wrapper = shallow(<SurveyComponent options={options} submitAction={action} />, context);
    const submitButton = wrapper.find('.button-submit');

    wrapper.find('[value="1"]')
      .simulate('change', { target: { type: 'radio', name: 'survey', value: '1' } });

    submitButton.simulate('click', {
      preventDefault: () => {
      }
    });

    expect(action).to.have.been.calledWith({
      selected: '1', message: 'label1', comment: '', 1: true
    });
    expect(sendAnalyticEvent)
      .to.have.been.calledWithMatch('result.1', {
        selected: '1', message: 'label1', comment: '', 1: true
      });
  });

  it('should pass submitAction survey comment', () => {
    const { sendAnalyticEvent, context } = getAnalytic();
    const action = sinon.spy();
    const askOptions = {
      type: 'radio',
      items: [
        { key: 'key0', label: 'label0' },
        { key: 'other', label: 'Other', nested: { ask: { placeholder: 'Whatsup?' } } },
      ]
    };
    const comment = 'Potato is the best vegetable.';
    const wrapper = shallow(<SurveyComponent options={askOptions} submitAction={action} />, context);
    const submitButton = wrapper.find('.button-submit');

    wrapper.find('[value="other"]')
      .simulate('change', { target: { type: 'radio', name: 'survey', value: 'other' } });

    wrapper.find('[name="survey.other"]')
      .simulate('change', { target: { type: 'textarea', name: 'survey.other', value: comment } });

    submitButton.simulate('click', {
      preventDefault: () => {
      }
    });

    expect(action).to.have.been.calledWith({
      selected: 'other', message: 'Other', comment, other: true
    });
    expect(sendAnalyticEvent)
      .to.have.been.calledWithMatch('result.other', {
        selected: 'other', message: 'Other', comment, other: true
      });
  });

  it('should render nested items', () => {
    const { context } = getAnalytic();
    const action = sinon.spy();
    const askOptions = {
      type: 'checkbox',
      items: [
        { key: 'key0', label: 'label0' },
        { key: 'other', label: 'Other', nested: options },
      ]
    };
    const wrapper = shallow(<SurveyComponent options={askOptions} submitAction={action} />, context);

    expect(wrapper.find(InputWithAnalytics)).to.be.length(2);

    wrapper.find('[value="other"]')
      .simulate('change', { target: { type: 'radio', name: 'survey', value: 'other' } });

    expect(wrapper.find(InputWithAnalytics)).to.be.length(7);
    expect(wrapper.find(InputWithAnalytics).findWhere(w => w.prop('type') === 'radio')).to.be.length(5);
  });

  it('should call cancelAction on cancel', () => {
    const { sendAnalyticEvent, context } = getAnalytic();
    const action = sinon.spy();
    const wrapper = shallow(<SurveyComponent options={options} cancelAction={action} />, context);
    const cancelButton = wrapper.find('.button-cancel');

    cancelButton.simulate('click', {
      preventDefault: () => {
      }
    });

    expect(action).to.have.been.called();
    expect(sendAnalyticEvent).to.have.been.calledWith('cancel');
  });

  it('should trigger pageview event', () => {
    const { sendAnalyticEvent, context } = getAnalytic();
    const action = sinon.spy();
    mount(<DefaultExport options={options} cancelAction={action} />, context);

    expect(sendAnalyticEvent).to.have.been.calledWith(['survey', 'pageview']);
  });
});
