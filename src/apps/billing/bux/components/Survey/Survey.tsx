import { Trans } from '@lingui/react';
import cx from 'classnames';
import { func } from 'prop-types';
import * as React from 'react';

import { withAnalyticsEvents as akWithAnalyticsEvents } from '@atlaskit/analytics-next';
import AkTextarea from '@atlaskit/textarea';

import { Analytics } from 'bux/components/Analytics';

import Button from '../Button';
import connectLogRoot from '../log/root';

import styles from './style.less';

const compactOptions = options => (
  Array.isArray(options)
    ? options.reduce((acc, x) => [...acc, ...x.items], [])
    : options.items
);

export type SurveyItemType = 'checkbox' | 'radio' | 'textarea';

export interface AskOption {
  placeholder: string | React.ReactNode;
  analyticsName?: string;
}

export interface SurveyOption {
  key?: string;
  label?: string | React.ReactNode;
  ask?: AskOption;
}

export interface SurveyAskItem {
  key: string;
  label: string | React.ReactNode;
  nested?: SurveyOption;
}

export interface SurveyOptions {
  type: SurveyItemType;
  label?: string | React.ReactNode;
  ask?: AskOption;
  items?: SurveyAskItem[];
}

export interface SurveyProps {
  required: boolean;
  requiredMessage: string;
  submitLabel: string;
  cancelLabel: string;
  submitAnalyticsName: string;
  cancelAnalyticsName?: string;
  optionsAnalyticsName?: string;
  submitType: 'safe' | 'dangerous';
  options: SurveyOptions[];
  allOpen: boolean;

  submitAction(results: any): any;

  cancelAction(): any;
}

export interface SurveyState {
  [key: string]: {
    comment?: string,
    [key: string]: string | boolean | undefined,
  };
}

const prevent = (e: React.FormEvent<any>) => e.preventDefault();

const inputTypeToActionSubject = (type: SurveyItemType) => {
  switch (type) {
    case 'checkbox':
      return 'checkbox';
    case 'textarea':
      return 'textField';
    case 'radio':
      return 'radio';
    default:
      // tslint:disable:no-console
      console.error(`${type} does not have an action subject`);

      return undefined;
  }
};

export const InputWithAnalytics = akWithAnalyticsEvents({
  onChange: (createAnalyticsEvent, props) => {
    if (props.type !== 'radio') {
      return;
    }

    return createAnalyticsEvent({
      action: 'changed',
      actionSubject: inputTypeToActionSubject(props.type),
      attributes: {
        componentName: 'input',
      },
    }).fire('atlaskit');
  },
})('input');

export class SurveyComponent extends React.Component<SurveyProps, SurveyState> {

  public static contextTypes = {
    sendAnalyticEvent: func,
  };

  public static defaultProps = {
    submitType: 'safe',
  };

  public state: SurveyState = {};

  public componentDidMount() {
    this.context.sendAnalyticEvent('pageview');
  }

  public onChange = (event: React.ChangeEvent<any>): void => {
    const {
      name, value, type, checked,
    } = event.target;
    switch (type) {
      case 'checkbox':
        this.setState({ [name]: { ...this.state[name], [value]: checked } });
        break;
      case 'textarea':
        this.setState({ [name]: { ...this.state[name], comment: value } });
        break;
      default:
        this.setState({ [name]: { [value]: true } });
        break;
    }
  };

  public selectedOptionMessage = (selectedOption) => {
    const { options } = this.props;
    if (selectedOption) {
      const { label } = compactOptions(options).find(({ key }) => key === selectedOption);
      if (typeof label === 'object') {
        return label.props.defaults;
      } else {
        return label;
      }
    }

    return '';
  };

  public onSubmit = (event) => {
    event.preventDefault();
    const state = this.state;
    const selectedOption = state.survey ? Object.keys(state.survey)[0] : '';
    const message = this.selectedOptionMessage(selectedOption);
    const selected = selectedOption || 'skipped';
    const results = Object
      .keys(state)
      .map(key => this.state[key])
      .reduce((acc, value) => ({ ...acc, ...value }), {
        selected,
        message,
        comment: '',
      });
    this.context.sendAnalyticEvent(`result.${selected}`, results);
    this.props.submitAction(results);
  };

  public onCancel = () => {
    this.context.sendAnalyticEvent('cancel');
    this.props.cancelAction();
  };

  public isSelected(name: string, key: string) {
    if (this.props.allOpen) {
      return true;
    }

    return this.state[name] && this.state[name][key];
  }

  public renderItems(name = '', leaf) {
    if (Array.isArray(leaf)) {
      return leaf.map(l => this.renderItems(name, l));
    }
    const { ask, type, label: leafLabel } = leaf;
    let items: Array<React.ReactElement<any>> = [];
    if (leafLabel) {
      items.push(<p key="caption">{leafLabel}</p>);
    }
    if (leaf.items) {
      items = items.concat(leaf.items.map(({ key, label, nested }) => (
        <div key={key} className={cx(styles.option, `ak-field-${type}`)}>
          <Analytics.UI as={this.props.optionsAnalyticsName as string}>
            <InputWithAnalytics id={`survey-${name}-${key}`} type={type} name={name} value={key} onChange={this.onChange} />
            <label htmlFor={`survey-${name}-${key}`}>{label}</label>
            {nested && this.isSelected(name, key) && (
              <div className={styles.nested}>{this.renderItems(`${name}.${key}`, nested)}</div>
            )}
          </Analytics.UI>
        </div>
      )));
    }
    if (ask) {
      items.push((
        <Analytics.UI as={ask.analyticsName} key="comment">
          <AkTextarea
            minimumRows={5}
            name={name}
            onChange={this.onChange}
            placeholder={ask.placeholder}
          />
        </Analytics.UI>
      ));
    }

    return items;
  }

  public render() {
    const { submitLabel, submitType, cancelLabel, cancelAnalyticsName, submitAnalyticsName, required, requiredMessage } = this.props;
    const completed = !required || !!this.state.survey;

    return (
      <form className={cx(styles.survey, 'survey-form')} onSubmit={prevent}>
        <fieldset className="ak-field-group">
          {this.renderItems('survey', this.props.options)}
        </fieldset>
        <div className={styles.actionButtons}>
          {!completed && requiredMessage}
          <Analytics.UI
            as={submitAnalyticsName}
            attributes={{
              linkContainer: 'navigation',
            }}
          >
            <Button
              className="button-submit"
              onClick={this.onSubmit}
              primary={true}
              submit={true}
              danger={submitType === 'dangerous'}
              disabled={!completed}
            >{submitLabel}
            </Button>
          </Analytics.UI>
          <Analytics.UI
            as={cancelAnalyticsName as string}
            attributes={{
              linkContainer: 'navigation',
            }}
          >
            <Button className="button-cancel" onClick={this.onCancel}>
              {cancelLabel ? cancelLabel : <Trans id="billing.survey.action.cancel">Cancel</Trans>}
            </Button>
          </Analytics.UI>
        </div>
      </form>
    );
  }
}

export const Survey = connectLogRoot('survey')(SurveyComponent);
