import { Trans } from '@lingui/react';
import * as React from 'react';

import { Analytics } from 'bux/components/Analytics';
import Button, { SubmitButton } from 'bux/components/Button';

export interface StepButtonsProps {
  isFirstStep: boolean;
  submitDisabled: boolean;
  submitBusy: boolean;
  submitLabel: string;
  submitName: string;
  backDisabled?: boolean;
  primaryAnalyticsName?: string;
  onBack(): any;
  onSubmit?(): any;
}

export const StepButtons = ({
  isFirstStep, submitDisabled, submitBusy, submitLabel, submitName, backDisabled, onBack, onSubmit,
  primaryAnalyticsName = 'nextButton',
}: StepButtonsProps) => {
  const secondaryName = isFirstStep ? 'cancel' : 'back';
  const primary = (
    <Analytics.UI as={primaryAnalyticsName as string}>
      <SubmitButton
        name={submitName}
        disabled={submitDisabled}
        spinning={submitBusy}
        onClick={onSubmit}
      >
        {submitLabel}
      </SubmitButton>
    </Analytics.UI>
  );
  const secondary = (
    <Analytics.UI as={isFirstStep ? 'cancelButton' : 'backButton'}>
      <Button
        name={secondaryName}
        className="cancel-flow"
        onClick={onBack}
        disabled={backDisabled}
        subtle="true"
      >
        {isFirstStep
          ? <Trans id="billing.step-buttons.cancel">Cancel</Trans>
          : <Trans id="billing.step-buttons.back">Back</Trans>
        }
      </Button>
    </Analytics.UI>
  );
  if (isFirstStep) {
    return <div>{primary}{secondary}</div>;
  }

  return <div>{secondary}{primary}</div>;
};
