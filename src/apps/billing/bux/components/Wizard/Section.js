import React from 'react';
import { node, string, oneOfType, object } from 'prop-types';
import cx from 'classnames';
import styles from './Wizard.less';

const Section = ({ title, className, children }) => (
  <div className={cx(styles.section, className)}>
    { title && <h3 className={styles.heading}>{title}</h3> }
    {children}
  </div>
);

Section.propTypes = {
  title: oneOfType([string, object]),
  className: string,
  children: node.isRequired
};

export default Section;
