import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Footer from '../Footer';

describe('Wizard', () => {
  describe('Footer', () => {
    it('should render component', () => {
      const wrapper = shallow(<Footer><button>Hello</button></Footer>);
      expect(wrapper).to.be.present();
      expect(wrapper.find('button')).to.be.present();
    });
    it('should use class name', () => {
      const wrapper = shallow(<Footer className="test"><button>Hello</button></Footer>);
      expect(wrapper).to.have.attr('class', 'test');
    });
  });
});
