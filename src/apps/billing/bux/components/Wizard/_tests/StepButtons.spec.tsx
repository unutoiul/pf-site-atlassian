import { expect } from 'chai';
import { mount } from 'enzyme';
import * as React from 'react';
import * as sinon from 'sinon';

import getAnalytic from 'bux/components/log/helpers/getAnalytic';

import { StepButtons, StepButtonsProps } from '../StepButtons';

describe('Wizard', () => {
  describe('StepButtons', () => {
    const props: StepButtonsProps = {
      isFirstStep: true,
      submitDisabled: false,
      submitBusy: false,
      submitLabel: 'test',
      submitName: 'test',
      onBack: sinon.stub(),
      onSubmit: sinon.stub(),
      backDisabled: false,
    };

    it('should render component', () => {
      const wrapper = mount(<StepButtons {...props} />);

      expect(wrapper).to.be.present();

      expect(wrapper.find('SubmitButton')).to.be.present();
      expect(wrapper.find('Button')).to.be.present();

      const submit = wrapper.find('SubmitButton');
      expect(submit).to.have.prop('children', props.submitLabel);
      expect(submit).to.have.prop('disabled', props.submitDisabled);
      expect(submit).to.have.prop('spinning', props.submitBusy);

      const cancel = wrapper.find('Button[name="cancel"]').first();
      expect(cancel).to.have.text('Cancel');
      expect(cancel).to.have.prop('disabled', props.backDisabled);
    });

    it('should render Cancel button as Back', () => {
      const wrapper = mount(<StepButtons {...props} isFirstStep={false} />);

      const cancel = wrapper.find('Button[name="back"]').first();
      expect(cancel).to.have.text('Back');
    });

    it('should call onBack when click back button', () => {
      const wrapper = mount(<StepButtons {...props} />, getAnalytic().context);
      expect(wrapper.find('button.cancel-flow')).to.be.present();
      wrapper.find('button.cancel-flow').simulate('click');
      // tslint:disable-next-line: no-unbound-method
      expect(props.onBack).to.be.called();
    });

    it('should call onSubmit when click call to action', () => {
      const wrapper = mount(<StepButtons {...props} />, getAnalytic().context);
      expect(wrapper.find('SubmitButton')).to.be.present();
      wrapper.find('SubmitButton').simulate('click');
      // tslint:disable-next-line: no-unbound-method
      expect(props.onSubmit).to.be.called();
    });
  });
});
