import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Section from '../Section';

describe('Wizard', () => {
  describe('Section', () => {
    it('should render component', () => {
      const wrapper = shallow(<Section><p>Hello</p></Section>);
      expect(wrapper).to.be.present();
      expect(wrapper.find('h3')).to.not.be.present();
      expect(wrapper.find('p')).to.be.present();
    });
    it('should use class name', () => {
      const wrapper = shallow(<Section className="test"><p>Hello</p></Section>);
      expect(wrapper).to.have.attr('class', 'test');
    });
    it('should render title', () => {
      const wrapper = shallow(<Section title="test"><p>Hello</p></Section>);
      expect(wrapper.find('h3')).to.be.present().and.have.text('test');
    });
  });
});
