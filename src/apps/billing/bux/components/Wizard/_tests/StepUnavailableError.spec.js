import React from 'react';
import sinon from 'sinon';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import StepUnavailableError from '../StepUnavailableError';

describe('Wizard', () => {
  describe('StepUnavailableError', () => {
    let retry;

    beforeEach(() => {
      retry = sinon.stub();
    });

    it('should render component', () => {
      const wrapper = shallow(<StepUnavailableError message="test" onRetry={retry} />);
      expect(wrapper).to.be.present();
      expect(wrapper.find('h1')).to.have.text('test');
    });
    it('should retry when button is clicked', () => {
      const wrapper = shallow(<StepUnavailableError message="test" onRetry={retry} />);
      expect(wrapper).to.be.present();
      wrapper.find('Button').simulate('click');
      expect(retry).to.have.been.called();
    });
    it('should not render retry button', () => {
      const wrapper = shallow(<StepUnavailableError message="test" />);
      expect(wrapper).to.be.present();
      expect(wrapper.find('Button')).to.not.be.present();
    });
  });
});
