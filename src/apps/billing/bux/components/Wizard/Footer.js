import React from 'react';
import { node, string } from 'prop-types';
import cx from 'classnames';
import styles from './Wizard.less';

const Footer = ({ className, children }) => (
  <div className={cx(styles.footer, className)}>
    {children}
  </div>
);

Footer.propTypes = {
  className: string,
  children: node.isRequired
};

export default Footer;
