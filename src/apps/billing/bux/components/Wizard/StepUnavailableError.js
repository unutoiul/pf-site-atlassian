import { Trans } from '@lingui/react';
import React from 'react';
import PropTypes from 'prop-types';
import { ExternalLinkTo } from 'bux/components/Link';
import Button from 'bux/components/Button';
import errorLogo from 'bux/static/svg/error.svg';
import styles from './Wizard.less';

const StepUnavailableError = ({ message, onRetry }) => (
  <div className={styles.content} data-test="step-unavailable-error">
    <div>
      <img src={errorLogo} height={120} alt="Error" />
    </div>

    <h1 className={styles.heading}>{message}</h1>
    { onRetry &&
      <Button onClick={onRetry} primary>
        <Trans id="billing.wizard.unavailable.error.retry">Retry</Trans>
      </Button>
    }
    <p className={styles.support}>
      <Trans id="billing.wizard.unavailable.error.message">
        Check back soon or contact <ExternalLinkTo.BoldSupport>customer support</ExternalLinkTo.BoldSupport>
      </Trans>
    </p>
  </div>
);

StepUnavailableError.propTypes = {
  message: PropTypes.string.isRequired,
  onRetry: PropTypes.func,
};

export default StepUnavailableError;
