import { withI18n, I18n, Trans } from '@lingui/react';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Field } from 'redux-form';
import validatePostCode from 'bux/common/helpers/validators/postCode';
import { Analytics } from 'bux/components/Analytics';
import { MediumSelectField } from '../field/SelectField';
import AddressField from '../field/AddressField';
import LinkButton from '../Link/Button';
import LogScope from '../log/Scope';

const getDefaultAddressValue = (values, formComponent) => {
  const formSection = formComponent ? values[formComponent] : values;
  return formSection && formSection.originalAddressLineFull;
};

export const openFullFormNotification = (autoCompleteFailed, toggleFullMode, formComponent, validation) => (
  <LogScope prefix="openFullAddressForm">
    <Analytics.UI as="openFullAddressFormLink">
      {autoCompleteFailed
        ? (
          <span className="form-message">
            <Trans id="billing.short-address-form.open-full-form.error">
              Oops, we couldn't find your address. <br />
              Don't worry, just{' '}
              <LinkButton onClick={() => toggleFullMode(validation)} className={`${formComponent}OpenFullAddressForm`}>
                open the full form
              </LinkButton>
              <br />
              and enter it manually.
            </Trans>
          </span>
        ) : (
          <span className="form-message">
            <Trans id="billing.short-address-form.open-full-form">
              Select a value from the list below, <br />
              or{' '}
                <LinkButton 
                  onClick={() => toggleFullMode(validation)} 
                  className={`${formComponent}OpenFullAddressForm`}
                >
                  open the full form
                </LinkButton>.
            </Trans>
          </span>
        )
      }
    </Analytics.UI>
  </LogScope>
);


export const validateShortInput =
  (autoCompleteFailed, currentAddress, formComponent, toggleFullMode, i18n, validation) => (value, values) => {
    const targetValue = currentAddress || getDefaultAddressValue(values, formComponent);
    const invalidPostCode = !!validatePostCode(values.country, values.postcode, i18n) || !values.postcode;
    const invalidCity = !values.city;
    const shouldShowNotification = autoCompleteFailed || value !== targetValue || invalidPostCode || invalidCity;
    return shouldShowNotification 
      && openFullFormNotification(autoCompleteFailed, toggleFullMode, formComponent, validation);
  };


class ShortAddressFields extends Component {
  static propTypes = {
    country: PropTypes.string.isRequired,
    countries: PropTypes.array.isRequired,
    onFill: PropTypes.func.isRequired,
    onAutocompleteStatus: PropTypes.func.isRequired,
    autoCompleteFailed: PropTypes.bool.isRequired,
    focusPoint: PropTypes.arrayOf(PropTypes.number),
    toggleFullMode: PropTypes.func.isRequired,
    formComponent: PropTypes.string.isRequired,
    appliedAddressValue: PropTypes.string,
    i18n: PropTypes.object,
    validationResponse: PropTypes.object,
    validateCountry: PropTypes.func,
    initialValues: PropTypes.object,
    processValidationResponse: PropTypes.func,
    validation: PropTypes.func,
    shouldDisableSave: PropTypes.func,
  };

  state = {
    addressFieldFocused: false,
    validation: undefined
  };

  componentDidUpdate(prevProps) {
    if (prevProps.country !== this.props.country) {
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({ validation: this.props.validation });
    }
  }

  onAddressFocus = () => this.setState({ addressFieldFocused: true });
  onAddressBlur = () => this.setState({ addressFieldFocused: false });

  inputValidator = (value, values) => {
    const {
      toggleFullMode,
      formComponent,
      appliedAddressValue,
      autoCompleteFailed,
      i18n,
    } = this.props;
    const {
      validation
    } = this.state;
    return validateShortInput(
      autoCompleteFailed,
      appliedAddressValue,
      formComponent,
      toggleFullMode,
      i18n,
      validation
    )(value, values);
  };

  countryValidator = (value, country) => {
    this.props.validateCountry({ country })
      .then((response) => { 
        this.setState({ validation: this.props.processValidationResponse(response) });
        this.props.shouldDisableSave(this.state.validation && this.state.validation());
      });
  };

  render() {
    const {
      country,
      countries,
      focusPoint,
      onFill,
      onAutocompleteStatus,
      autoCompleteFailed
    } = this.props;

    return (
      <I18n>
        {({ i18n }) => (
          <div className="address-fields">
            <Analytics.UI as="countryField">
              <Field
                name="country"
                component={MediumSelectField}
                data={countries}
                required
                autocomplete
                strict
                labelText={i18n.t('billing.short-address-form.country')`Country`}
                placeholder={i18n.t('billing.short-address-form.country.placeholder')`Select Country`}
              />
            </Analytics.UI>
            <div onFocus={this.onAddressFocus} onBlur={this.onAddressBlur}>
              <Analytics.UI as="addressLineFullField">
                <Field
                  name="addressLineFull"
                  component={AddressField}
                  restrict={{ country, focusPoint }}
                  onFill={onFill}
                  onAutocompleteStatus={onAutocompleteStatus}
                  autoCompleteFailed={autoCompleteFailed}
                  isShort
                  isFocused={this.state.addressFieldFocused}
                  required
                  validate={this.inputValidator}
                  labelText={i18n.t('billing.short-address-form.address')`Company address`}
                  placeholder={i18n.t('billing.short-address-form.address.placeholder')`Start typing your address`}
                />
              </Analytics.UI>
            </div>
          </div>
        )}
      </I18n>
    );
  }
}

export default withI18n()(ShortAddressFields);
