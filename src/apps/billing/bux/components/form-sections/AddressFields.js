import { withI18n } from '@lingui/react';
import PropTypes from 'prop-types';
import React from 'react';
import { Field } from 'redux-form';
import validatePostCode from 'bux/common/helpers/validators/postCode';
import { MediumSelectField } from '../field/SelectField';
import { MediumSelectInputFallbackField } from '../field/SelectInputFallbackField';
import { MediumInputField, WideInputField } from '../field/InputField';
import { WideAddressField } from '../field/AddressField';

class AddressFields extends React.PureComponent {
  static propTypes = {
    country: PropTypes.string.isRequired,
    countries: PropTypes.array.isRequired,
    states: PropTypes.array.isRequired,
    onFill: PropTypes.func.isRequired,
    onAutocompleteStatus: PropTypes.func.isRequired,
    autoCompleteFailed: PropTypes.bool.isRequired,
    focusPoint: PropTypes.arrayOf(PropTypes.number),
    i18n: PropTypes.object.isRequired,
    validateCountry: PropTypes.func,
    processValidationResponse: PropTypes.func,
    validation: PropTypes.func,
    shouldDisableSave: PropTypes.func,
  };

  state = {
    validation: this.props.validation
  };

  componentDidUpdate(prevProps) {
    if (prevProps.country !== this.props.country) {
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({ validation: this.props.validation });
    }
  }

  postCodeValidator = (value, values) => validatePostCode(values.country, values.postcode, this.props.i18n);

  countryValidator = (value, country) => {
    this.props.validateCountry({ country })
      .then((response) => { 
        this.setState({ validation: this.props.processValidationResponse(response) });
        this.props.shouldDisableSave(this.state.validation && this.state.validation());
      });
  };
  
  render() {
    const {
      country, countries, states, focusPoint, onFill, onAutocompleteStatus, i18n
    } = this.props;
    return (
      <div className="address-fields">
        <Field
          name="country"
          component={MediumSelectField}
          labelText={i18n.t('billing.address-form.country')`Country`}
          placeholder={i18n.t('billing.address-form.country.placeholder')`Select Country`}
          data={countries}
          required
          autocomplete
          strict
          validate={this.state.validation}
          onChange={this.countryValidator}
        />
        <Field
          name="addressLine1"
          component={WideAddressField}
          labelText={i18n.t('billing.address-form.address-line-1')`Address 1`}
          restrict={{ country, focusPoint }}
          onFill={onFill}
          onAutocompleteStatus={onAutocompleteStatus}
          required
        />
        <Field
          name="addressLine2"
          component={WideInputField}
          labelText={i18n.t('billing.address-form.address-line-2')`Address 2`}
        />
        <Field
          name="city"
          component={WideInputField}
          labelText={i18n.t('billing.address-form.city')`City`}
          required
        />
        <Field
          name="state"
          component={MediumSelectInputFallbackField}
          labelText={i18n.t('billing.address-form.state')`State`}
          placeholderSelect={i18n.t('billing.address-form.state.placeholder.select')`Select a state`}
          placeholderInput={i18n.t('billing.address-form.state.placeholder.input')`Provide a state`}
          data={states}
          required={!!states.length}
          autocomplete
          strict
        />
        <Field
          name="postcode"
          component={MediumInputField}
          labelText={i18n.t('billing.address-form.postal-code')`Postal Code`}
          validate={this.postCodeValidator}
          required
        />
      </div>
    );
  }
}

export default withI18n()(AddressFields);
