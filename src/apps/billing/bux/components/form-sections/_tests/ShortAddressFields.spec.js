import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { mount } from 'enzyme';
import { Provider } from 'bux/helpers/redux';
import { reduxForm } from 'redux-form';
import createStore from 'bux/core/createStore';
import identityFn from 'bux/common/helpers/identity';
import { i18n } from 'bux/helpers/jsLingui';
import connectLogRoot from '../../log/root';
import getAnalytic from '../../log/helpers/getAnalytic';

import ShortAddressFields, { validateShortInput, openFullFormNotification } from '../ShortAddressFields';

const createDecorated = (props, store = createStore()) => {
  const Component = passProps => <ShortAddressFields {...passProps} />;

  const TestForm = reduxForm({ form: 'testForm' })(connectLogRoot('test')(Component));

  return (
    <Provider store={store}>
      <TestForm {...props} />
    </Provider>
  );
};

describe('ShortAddressFields component', () => {
  it('should render address', () => {
    const wrapper = mount(createDecorated({
      country: 'US',
      countries: [],
      formComponent: 'test',
      toggleFullMode: identityFn,
      onFill: identityFn,
      onAutocompleteStatus: identityFn,
      autoCompleteFailed: false,
      initialValues: {
        country: 'US',
        addressLineFull: 'some address'
      }
    }));
    expect(wrapper.find('[name="addressLineFull"]').hostNodes()).to.have.value('some address');
  });

  it('should switch to full form', () => {
    const toggle = sinon.stub();
    const { context, sendAnalyticEvent } = getAnalytic();
    const wrapper = mount(openFullFormNotification(false, toggle, 'test'), context);

    wrapper.find('.testOpenFullAddressForm').hostNodes().simulate('click');

    expect(toggle).to.be.called();
    expect(sendAnalyticEvent).to.be.calledWith(['openFullAddressForm', 'click']);
  });

  describe('should provide different texts', () => {
    const toggle = sinon.stub();
    const { context } = getAnalytic();

    it('should ask to pick an item from autocomplete', () => {
      const successWrapper = mount(openFullFormNotification(false, toggle, 'test'), context);
      expect(successWrapper).to.contain.text('Select a value from the list below');
    });

    it('should ask to provide address manually', () => {
      const failedWrapper = mount(openFullFormNotification(true, toggle, 'test'), context);
      expect(failedWrapper).to.contain.text('Oops, we couldn\'t find your address');
    });
  });

  describe('validation', () => {
    const DEFAULT = {
      city: 'knowhere',
      postcode: '123'
    };

    it('validateShortInput should pass if value is not changed', () => {
      expect(validateShortInput(false, 'testValue', null)('testValue', DEFAULT)).to.be.equal(false);
    });

    it('validateShortInput should trigger then autocomplete fail', () => {
      expect(validateShortInput(true, 'testValue', null)('testValue', DEFAULT)).not.to.be.equal(false);
    });

    it('validateShortInput should trigger if value changed', () => {
      expect(validateShortInput(false, 'testValue', null)('otherValue', DEFAULT)).not.to.be.equal(false);
    });

    it('validateShortInput should trigger is postal code is incorrect', () => {
      expect(validateShortInput(false, 'a', null, undefined, i18n)('a', {
        ...DEFAULT, country: 'AU', postcode: 'wrong'
      })).to.be.equal(false);
      expect(validateShortInput(false, 'a', null, undefined, i18n)('a', {
        ...DEFAULT, country: 'US', postcode: '12345'
      })).to.be.equal(false);
      expect(validateShortInput(false, 'a', null, undefined, i18n)('a', {
        ...DEFAULT, country: 'US', postcode: 'wrong'
      })).not.to.be.equal(false);
      expect(validateShortInput(false, 'a', null, undefined, i18n)('a', {
        ...DEFAULT, country: 'US', postcode: ''
      })).not.to.be.equal(false);
    });

    it('validateShortInput should pass if default value stored in values', () => {
      expect(validateShortInput(false, '', null)('otherValue', { ...DEFAULT, originalAddressLineFull: 'otherValue' }))
        .to.be.equal(false);
    });

    it('validateShortInput should pass if default value stored in values behind form section', () => {
      expect(validateShortInput(false, '', 'test')('otherValue', {
        ...DEFAULT,
        test: { originalAddressLineFull: 'otherValue' }
      }))
        .to.be.equal(false);
    });
  });
});
