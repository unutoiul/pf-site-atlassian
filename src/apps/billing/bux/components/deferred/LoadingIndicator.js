import React, { PureComponent } from 'react';
import styled from 'styled-components';
import Loading from '../Loading';

const LoadWrapper = styled.div`
  position: absolute;
  z-index: 1000;  
  left: 0;
  right: 0;
  top: 0;
  padding-top: 1em;
  padding-bottom: 1em;
  text-align: center;
  background-color: #F4F4F4;
`;

class LoadIndicator extends PureComponent {
  state = {
    timePassed: false
  };

  componentWillMount() {
    this.timer = setTimeout(() => this.setState({ timePassed: true }), 300);
  }

  componentWillUnmount() {
    clearTimeout(this.timer);
  }

  render() {
    if (this.state.timePassed) {
      return (
        <LoadWrapper>
          <Loading mode="component" />
        </LoadWrapper>
      );
    }
    return null;
  }
}

export default LoadIndicator;
