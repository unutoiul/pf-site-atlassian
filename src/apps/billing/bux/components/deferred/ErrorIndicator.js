import PropTypes from 'prop-types';
import { Component } from 'react';
import { connect } from 'react-redux';
import { showNotificationByTemplate } from 'bux/core/state/notifications/actions';

export class ErrorIndicatorComponent extends Component {
  static propTypes = {
    notify: PropTypes.func,
    onError: PropTypes.func
  };

  componentWillMount() {
    this.props.notify();
    if (this.props.onError) {
      this.props.onError();
    }
  }

  render() {
    return null;
  }
}

const ErrorIndicator = connect(
  null,
  (dispatch, props) => ({
    notify: () => (
      dispatch(showNotificationByTemplate('chunk-loading-error', props))
    )
  })
)(ErrorIndicatorComponent);

export default ErrorIndicator;
