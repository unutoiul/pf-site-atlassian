import deferred, { DefaultLoadingIndicator } from './deferred';

export { DefaultLoadingIndicator };

export default deferred;
