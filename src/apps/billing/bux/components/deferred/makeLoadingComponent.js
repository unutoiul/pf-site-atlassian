import React from 'react';
import PropTypes from 'prop-types';

const toLoading = (LoadingComponent, ErrorComponent) => {
  const LoadableLoading = ({ error, timedOut, pastDelay }) => {
    if (error || timedOut) return <ErrorComponent />;
    if (pastDelay) return <LoadingComponent />;
    return null;
  };

  LoadableLoading.propTypes = {
    error: PropTypes.any,
    timedOut: PropTypes.bool,
    pastDelay: PropTypes.bool,
  };

  return LoadableLoading;
};

export default toLoading;
