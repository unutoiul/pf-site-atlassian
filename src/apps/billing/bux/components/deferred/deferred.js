import loadable from 'react-loadable';
import showWithDelay from '../showWithDelay';
import LoadIndicator from './LoadingIndicator';
import ErrorIndicator from './ErrorIndicator';
import toLoading from './makeLoadingComponent';

export const DefaultLoadingIndicator = showWithDelay(LoadIndicator);

const deferred = (loader, options = {}) => loadable({
  loader,
  loading: toLoading(options.loading || DefaultLoadingIndicator, options.error || ErrorIndicator)
});

export default deferred;
