import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';
import proxyquire from 'bux/helpers/proxyquire';
import { Provider } from 'bux/helpers/redux';
import { ErrorIndicatorComponent } from '../ErrorIndicator';

describe('ErrorIndicator component: ', () => {
  it('should trigger notify and onError', () => {
    const notify = sinon.spy();
    const onError = sinon.spy();
    mount(<ErrorIndicatorComponent notify={notify} onError={onError} />);
    expect(notify).to.be.called();
    expect(onError).to.be.called();
  });

  it('should display global notification', () => {
    const showNotificationByTemplate = sinon.stub().returns(() => {});
    const Component = proxyquire.load('../ErrorIndicator', {
      'bux/core/state/notifications/actions': { showNotificationByTemplate }
    }).default;
    mount(<Provider><Component /></Provider>);
    expect(showNotificationByTemplate).to.be.called();
  });
});
