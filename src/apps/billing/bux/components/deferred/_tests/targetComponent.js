import PropTypes from 'prop-types';
import React from 'react';

const Component = ({ name }) => <span>{name}</span>;
Component.propTypes = {
  name: PropTypes.string
};

export default Component;
