import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';
import { Provider } from 'bux/helpers/redux';
import deferred, { DefaultLoadingIndicator } from '../deferred';
import ErrorIndicator from '../ErrorIndicator';

describe('Deferred component: ', () => {
  it('should load component', (done) => {
    const loader = () => import('./targetComponent.js');
    const Wrapper = deferred(loader);
    const wrapper = mount(<Wrapper name="42" />);

    setImmediate(() => {
      wrapper.update();
      expect(wrapper.find(DefaultLoadingIndicator)).not.to.be.present();
      expect(wrapper).to.contain.text(42);
      done();
    });
  });

  it.skip('should indicate error', (done) => {
    const spy = sinon.spy();
    const loader = () => import('./unexistingComponent.js');
    const Wrapper = deferred(loader);
    const wrapper = mount(<Provider><Wrapper name="42" onError={spy} /></Provider>);

    setImmediate(() => {
      wrapper.update();
      expect(wrapper.find(DefaultLoadingIndicator)).not.to.be.present();
      expect(wrapper.find(ErrorIndicator)).to.be.present();
      expect(wrapper).not.to.contain.text(42);
      expect(spy).to.be.called();
      done();
    });
  });
});
