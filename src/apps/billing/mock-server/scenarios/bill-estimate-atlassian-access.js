const { produce } = require('immer');

const invoke = (status, originalBody) => ({
  status,
  body: produce(originalBody, (body) => {
    body.lines = [
      {
        amountBeforeTax: 30,
        amountWithTax: 33,
        taxAmount: 3,
        entitlementId: '7530796',
        productKey: 'com.atlassian.identity-manager',
        productName: 'Atlassian Access (Cloud)',
        usage: [{
          units: 20,
          unlimited: false,
          description: 'Atlassian Access users',
          unitCode: 'users',
          amountBeforeTax: 30,
          taxRate: 0,
          taxAmount: 3,
          amountWithTax: 33
        }]
      }
    ];
    body.atlassianAccessExtras = {
      trello: {
        period: {
          startDate: '2018-10-02',
          endDate: '2018-11-02',
          renewalFrequency: 'MONTHLY'
        },
        unitCount: 18,
        additionalUnitCount: 2,
        amountBeforeTax: 416.00,
        amountWithTax: 457.60
      },
      grandfathering: {
        period: {
          startDate: '2018-09-02',
          endDate: '2018-10-02',
          renewalFrequency: 'MONTHLY'
        },
        amountBeforeTax: 189.00,
        amountWithTax: 207.90,
        pricingPlanSummary: {
          description: 'A sample GF plan'
        }
      }
    };
  })
});

module.exports = {
  group: 'Bill Estimate',
  name: 'Atlassian access',
  condition: (method, url) => (url.indexOf('/billing/bill-estimate') !== -1 && method === 'GET'),
  invoke
};
