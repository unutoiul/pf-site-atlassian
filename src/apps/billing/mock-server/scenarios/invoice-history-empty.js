const { produce } = require('immer');

const invoke = (status, originalBody) => ({
  status,
  body: produce(originalBody, (body) => {
    body.invoices = [];
  })
});

module.exports = {
  group: 'Invoice History',
  name: 'No history',
  condition: (method, url) => (url.indexOf('/billing/invoice-history') !== -1 && method === 'GET'),
  invoke
};
