const { produce } = require('immer');

const invoke = (status, originalBody) => ({
  status,
  body: produce(originalBody, (body) => {
    body.invoiceContact = {};
  })
});

module.exports = {
  group: 'Billing Details',
  name: 'No invoice contact',
  condition: (method, url) => (url.indexOf('/billing/billing-details') !== -1 && method === 'GET'),
  invoke
};
