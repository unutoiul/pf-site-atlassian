module.exports = {
  group: 'Bill Estimate',
  name: 'Error',
  condition: (method, url) => (url.indexOf('/billing/bill-estimate') !== -1 && method === 'GET'),
  invoke: (status, body) => ({ status: 400, body })
};
