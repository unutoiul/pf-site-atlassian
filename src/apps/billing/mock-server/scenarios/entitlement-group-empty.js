const { produce } = require('immer');

const invoke = (status, originalBody) => ({
  status,
  body: produce(originalBody, (body) => {
    body.entitlements = [];
  })
});

module.exports = {
  group: 'Entitlement group',
  name: 'Empty',
  condition: (method, url) => (url.indexOf('/billing/entitlement-group') !== -1 && method === 'GET'),
  invoke
};
