const { produce } = require('immer');

const invoke = (status, originalBody) => ({
  status,
  body: produce(originalBody, (body) => {
    body.nextBillingPeriod.renewalFrequency = 'ANNUAL';
  })
});

module.exports = {
  group: 'Bill Estimate',
  name: 'Annual',
  condition: (method, url) => (url.indexOf('/billing/bill-estimate') !== -1 && method === 'GET'),
  invoke
};
