const { produce } = require('immer');

const invoke = (status, originalBody) => ({
  status,
  body: produce(originalBody, (body) => {
    body.lines = [];
  })
});

module.exports = {
  group: 'Bill Estimate',
  name: 'Empty',
  condition: (method, url) => (url.indexOf('/billing/bill-estimate') !== -1 && method === 'GET'),
  invoke
};
