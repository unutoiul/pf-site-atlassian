module.exports = {
  group: 'Invoice History',
  name: 'Error',
  condition: (method, url) => (url.indexOf('/billing/invoice-history') !== -1 && method === 'GET'),
  invoke: (status, body) => ({ status: 400, body })
};
