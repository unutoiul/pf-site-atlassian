const { produce } = require('immer');

const invoke = (status, originalBody) => ({
  status,
  body: produce(originalBody, (body) => {
    body.invoices[body.invoices.length - 1].currencyCode = 'JPY';
  })
});

module.exports = {
  group: 'Invoice History',
  name: 'Mixed currency',
  condition: (method, url) => (url.indexOf('/billing/invoice-history') !== -1 && method === 'GET'),
  invoke
};
