module.exports = {
  group: 'Change renewal frequency estimate',
  name: 'Error',
  condition: (method, url) => (url.indexOf('/billing/change-renewal-frequency-estimate') !== -1 && method === 'POST'),
  invoke: (status, body) => ({ status: 400, body })
};
