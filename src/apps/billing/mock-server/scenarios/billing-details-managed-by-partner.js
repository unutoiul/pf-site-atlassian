const { produce } = require('immer');

const invoke = (status, originalBody) => ({
  status,
  body: produce(originalBody, (body) => {
    body.managedByPartner = true;
  })
});

module.exports = {
  group: 'Billing Details',
  name: 'Managed by Partner',
  condition: (method, url) => (url.indexOf('/billing/billing-details') !== -1 && method === 'GET'),
  invoke
};
