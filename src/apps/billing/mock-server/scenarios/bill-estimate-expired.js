const { produce } = require('immer');
const moment = require('moment');

const invoke = (status, originalBody) => ({
  status,
  body: produce(originalBody, (body) => {
    const date = moment().subtract(1, 'day');
    body.currentBillingPeriod.endDate = date.format('YYYY-MM-DD');
    body.currentBillingPeriod.startDate = date.subtract(30, 'day').format('YYYY-MM-DD');
  })
});

module.exports = {
  group: 'Bill Estimate',
  name: 'Expired',
  condition: (method, url) => (url.indexOf('/billing/bill-estimate') !== -1 && method === 'GET'),
  invoke
};
