const { produce } = require('immer');

const invoke = (status, originalBody) => ({
  status,
  body: produce(originalBody, (body) => {
    body.entitlements = [
      {
        creationDate: '2016-12-05',
        name: 'Jira Software (Cloud)',
        selectedEdition: 'standard',
        trialEndDate: '2016-12-17',
        entitlementGroupId: '4d854acf-e651-4a4b-bdd0-d0c76f0995ed',
        productKey: 'jira-software.ondemand',
        endDate: '2017-12-17',
        status: 'ACTIVE',
        accountId: '363bf5a5-47d7-42f9-9c51-930c40d6b3b9',
        sen: 'A813575',
        startDate: '2016-12-05',
        id: 'bc6e50b0-d10e-4e99-a5e7-7a0c076793c1'
      },
    ];
  })
});

module.exports = {
  group: 'Entitlement group',
  name: 'Jira',
  condition: (method, url) => (url.indexOf('/billing/entitlement-group') !== -1 && method === 'GET'),
  invoke
};
