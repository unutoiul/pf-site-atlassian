const { produce } = require('immer');

const invoke = (status, originalBody) => ({
  status,
  body: produce(originalBody, (body) => {
    body.paymentMethod = 'PAYPAL_ACCOUNT';
    body.creditCard = {};
    body.paypalAccount = {
      email: 'user@test.org',
    };
  })
});

module.exports = {
  group: 'Billing Details',
  name: 'PayPal',
  condition: (method, url) => (url.indexOf('/billing/billing-details') !== -1 && method === 'GET'),
  invoke
};
