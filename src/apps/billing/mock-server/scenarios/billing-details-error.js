module.exports = {
  group: 'Billing Details',
  name: 'Error',
  condition: (method, url) => (url.indexOf('/billing/billing-details') !== -1 && method === 'GET'),
  invoke: (status, body) => ({ status: 400, body })
};
