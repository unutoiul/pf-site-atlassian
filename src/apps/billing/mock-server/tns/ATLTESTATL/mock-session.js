
// compile babel

/* eslint-disable no-unused-vars, no-var */

var PaymentSession = (function PaymentSession() {
  var config = {};

  var refreshStyling = function refreshStyling(field) {
    var iframe = document.getElementById(`gw-proxy-${field}`);
    var iframeDocument = iframe.contentWindow.document;
    var originalElement = document.querySelector(config.fields.card[field]);
    var clonedElement = iframeDocument.querySelector(config.fields.card[field]);
    var originalStyle = originalElement.ownerDocument.defaultView.getComputedStyle(originalElement, null);
    clonedElement.style.cssText = originalStyle.cssText;
    clonedElement.style.display = 'block';
  };

  var emitEvent = function emitEvent(field, eventType) {
    setTimeout(() => {
      var callback = config.fieldCallbacks[`card.${field}`][eventType];
      if (callback) {
        callback(config.fields.card[field]);
        refreshStyling(field);
      }
    }, 0);
  };

  var cloneElement = function cloneElement(elementId, element, iframeDocument) {
    var elementTag = element.tagName;
    if (['INPUT', 'SELECT'].indexOf(elementTag) < 0) {
      throw new Error(`Unsupported HTML element tag [${elementTag}]`);
    }

    const clonedElement = iframeDocument.createElement(elementTag);
    if (elementTag === 'INPUT') {
      const inputType = element.getAttribute('type') || 'text';
      if (['text', 'number'].indexOf(inputType) < 0) {
        throw new Error(`Unsupported input type [${inputType}]`);
      }
    }

    if (elementTag === 'SELECT') {
      const children = Array.from(element.getElementsByTagName('OPTION'));
      children.forEach((item) => {
        var clonedItem = cloneElement(null, item, iframeDocument);
        clonedElement.appendChild(clonedItem);
      });
    }

    const attributes = element.attributes;
    Array.from(attributes)
      .forEach(pair => !['readonly', 'value'].includes(pair.name) && clonedElement.setAttribute(pair.name, pair.value));

    clonedElement.setAttribute('onfocus', `emitEvent('${elementId}', 'focus')`);
    clonedElement.setAttribute('onblur', `emitEvent('${elementId}', 'blur')`);
    clonedElement.setAttribute('onchange', `emitEvent('${elementId}', 'change')`);
    clonedElement.setAttribute('onmouseover', `emitEvent('${elementId}', 'mouseOver')`);
    clonedElement.setAttribute('onmouseout', `emitEvent('${elementId}', 'mouseOut')`);

    // Copy style of existing input to cloned input
    clonedElement.style.cssText = element.ownerDocument.defaultView.getComputedStyle(element, null).cssText;
    return clonedElement;
  };

  var replaceField = function replaceField(field) {
    var selector = config.fields.card[field];
    var oldField = document.querySelectorAll(selector)[0];
    if (!oldField) {
      throw new Error(`Field [${selector}] could not be found`);
    }

    const hostedField = document.createElement('iframe');
    hostedField.className = `gw-proxy-${field}`;
    // Added only for a test hook, is not added by real TNS
    hostedField.id = `gw-proxy-${field}`;
    oldField.parentNode.insertBefore(hostedField, oldField);
    const iframeDocument = hostedField.contentWindow.document;

    iframeDocument.open();
    iframeDocument.write('<html><body style="margin: 0 !important; padding 0 !important;"></body></html>');
    iframeDocument.close();

    iframeDocument.body.appendChild(cloneElement(field, oldField, iframeDocument));
    hostedField.contentWindow.emitEvent = emitEvent;

    // Style the IFrame to appear like a normal form field
    hostedField.height = oldField.offsetHeight;
    hostedField.width = oldField.offsetWidth;
    hostedField.scrolling = 'no';
    hostedField.frameBorder = '0';
    hostedField.seamless = 'seamless';

    // Remove the old field
    oldField.style.display = 'none';
  };

  var configure = function configure(opts) {
    config = opts;
    config.fieldCallbacks = {};
    // Initialise callbacks and iframe the fields
    const sensitiveFields = ['number', 'securityCode'];
    const fields = config.fields.card;
    Object.keys(fields).forEach((fieldName) => {
      if (sensitiveFields.includes(fieldName)) {
        config.fieldCallbacks[`card.${fieldName}`] = {};
        replaceField(fieldName);
      }
    });
    setTimeout(() => {
      config.callbacks.initialized({ status: 'ok' });
    }, 0);
  };

  var setCallback = function setCallback(fields, eventType, callback) {
    return fields.forEach((field) => {
      config.fieldCallbacks[field][eventType] = callback;
    });
  };

  var getCallback = function getCallback(field, eventType) {
    /* eslint-disable-next-line */
    return config.fieldCallbacks[field][eventType] || function () {};
  };

  var onBlur = function onBlur(fields, callback) {
    return setCallback(fields, 'blur', callback);
  };
  var onFocus = function onFocus(fields, callback) {
    return setCallback(fields, 'focus', callback);
  };
  var onChange = function onChange(fields, callback) {
    return setCallback(fields, 'change', callback);
  };
  var onMouseOver = function onMouseOver(fields, callback) {
    return setCallback(fields, 'mouseOver', callback);
  };
  var onMouseOut = function onMouseOut(fields, callback) {
    return setCallback(fields, 'mouseOut', callback);
  };

  var updateSessionFromForm = function updateSessionFromForm() {
    setTimeout(() => {
      config.callbacks.formSessionUpdate({
        status: 'ok',
        session: { id: config.session || 'SESSION000000000000000000000001' },
        sourceOfFunds: {
          provided: {
            card: {
              brand: 'VISA',
              number: '111111xxxxxx1111',
              expiry: {
                month: '5',
                year: '22'
              },
              scheme: 'VISA',
              securityCode: 'xxx'
            }
          }
        }
      });
    });
  };

  return {
    configure,
    updateSessionFromForm,
    onBlur,
    onFocus,
    onChange,
    onMouseOver,
    onMouseOut,

    setFocus: function setFocus() {},
    setFocusStyle: function setFocusStyle() {},
    setHoverStyle: function setHoverStyle() {}
  };
}());
