/* eslint-disable-next-line */
const express = require('express');
/* eslint-disable-next-line */
const bodyParser = require('body-parser');
const path = require('path');
const metadata = require('../bux/api/metadata-get');
const scenariosList = require('../scenarios/index');
const mergeFeatureFlags = require('../helpers/scenarios/merge-feature-flags');

module.exports = (server) => {
  server.use(
    '/adg3/css-reset',
    express.static(path.join(__dirname, '../../../../../', '/node_modules/@atlaskit/css-reset/dist/'))
  );
  server.use(
    '/adg3/reduced-ui-pack',
    express.static(path.join(__dirname, '../../../../../', '/node_modules/@atlaskit/reduced-ui-pack/dist/'))
  );
  server.set('views', `${__dirname}/../views`).set('view engine', 'ejs');
  server.route('/scenarios')
    .get((req, res) => {
      const saved = req.cookies.mockSettings ? JSON.parse(req.cookies.mockSettings) : {};

      const scenarios = scenariosList.map((scenario) => {
        const { invoke, ...rest } = scenario;
        return rest;
      });

      const groups = scenarios
        .map(scenario => scenario.group)
        .filter((elem, pos, arr) => arr.indexOf(elem) === pos);

      res.render('scenarios', {
        selectedScenarios: saved.scenarios || [],
        delay: saved.delay || 0,
        scenarios,
        groups,
        featureFlags: saved.featureFlags || metadata.featureFlags,
      });
    })

    .post(bodyParser.urlencoded({ extended: true }), (req, res) => {
      const { scenarios, delay, featureFlags } = req.body;

      const offFeatureFlags = Object.keys(metadata.featureFlags)
        .reduce((prev, curr) => ({ ...prev, [curr]: false }), {});
      const mergedFeatureFlags = mergeFeatureFlags(offFeatureFlags, featureFlags);

      res.cookie('mockSettings', JSON.stringify({
        scenarios: scenarios ? Object.keys(scenarios) : [], delay, featureFlags: mergedFeatureFlags
      }));
      res.redirect('/scenarios');
    })

    .delete((req, res) => {
      res.clearCookie('mockSettings');
      res.json('ok');
    });
};
