const mung = require('express-mung');
const applyScenarios = require('../helpers/scenarios/apply-scenarios');

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function scenarioMiddleware(prevBody, req, res) {
  const mockSettings = req.cookies.mockSettings ? JSON.parse(req.cookies.mockSettings) : {};

  const { status, body } = applyScenarios(mockSettings, req.method, req.url, res.statusCode, prevBody);

  res.setHeader('X-Included-scenarios', JSON.stringify(mockSettings));
  res.status(status);

  await sleep(mockSettings.delay || 0);

  return body;
}

module.exports = mung.jsonAsync(scenarioMiddleware);
