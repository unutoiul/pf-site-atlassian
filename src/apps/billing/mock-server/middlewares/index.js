const getMiddlewareForMock = require('./file-mock-middleware');
const getMiddlewareForOverride = require('./override-mock-middleware');

const getMiddlewares = name => [
  ...getMiddlewareForOverride(),
  ...getMiddlewareForMock(name)
];

module.exports = getMiddlewares;
