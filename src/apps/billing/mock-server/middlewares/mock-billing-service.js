const PRICING_RESPONSE = require('../billing/api/instance/pricing-get');
const PROSPECTIVE_PRICES_RESPONSE = require('../billing/api/instance/prospective-prices-post');

/* eslint-disable-next-line */
const router = require('express').Router();

const respondJson = require('../common/respondJson');

router.get('/pricing', respondJson(PRICING_RESPONSE));
router.post('/prospective-prices', respondJson(PROSPECTIVE_PRICES_RESPONSE));

module.exports = router;
