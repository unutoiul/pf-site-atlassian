const request = require('request');

const proxyRequest = apikey => (req, res) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');

  const method = req.method.toUpperCase();

  if (method === 'GET') {
    const url = `https://maps.googleapis.com/maps/api/place${req.url}`;

    const target = url
      .replace('_APIKEY_', apikey)
      .replace('?', 'json?');

    console.log(target);

    request(target).pipe(res);
  } else {
    res.send();
  }
};

module.exports = proxyRequest;
