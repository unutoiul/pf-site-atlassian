/* eslint-disable-next-line */
const router = require('express').Router();

const respondJson = require('../common/respondJson');
const licenseInformation = require('../gateway/api/xflow/license-information/license-information-get.json');
const queryRole = require('../gateway/api/xflow/query-role/query-role-post.json');

router.get('/api/permissions/permitted', respondJson({
  permitted: true
}));

router.get('/api/xflow/:uuid/license-information', respondJson(licenseInformation));

router.post('/api/xflow/:uuid/query-role', respondJson(queryRole));

module.exports = router;
