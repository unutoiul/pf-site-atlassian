const fs = require('fs');
const path = require('path');
const url = require('url');

const getMiddlewareForFileMock = name => [
  (req, res, next) => {
    let pathname = url.parse(req.url).pathname;

    if (pathname.indexOf('/place/') !== -1) {
      if (req.param('input') && 'somethingyoucantcomplete'.indexOf(req.param('input').split(',')[0]) === 0) {
        pathname += 'failed';
      } else {
        pathname += 'success';
      }
    }

    const filepath = path.join(__dirname, '..', name, `${pathname}-${req.method.toLowerCase()}.json`);

    const DELAY = 0;
    setTimeout(() => {
      fs.readFile(filepath, 'utf8', (err, data) => {
        if (err) {
          console.error(`'${filepath}' not found. Skipping...`, err);
          next();
        } else {
          console.log(`'${filepath}' found. Returning...`);
          if (data) {
            res.json(JSON.parse(data));
          } else {
            res.sendStatus(204);
          }
        }
      });
    }, DELAY);
  }
];

module.exports = getMiddlewareForFileMock;
