/**
 * Mapping of URL to override object.
 *
 * Override object looks like:
 * {
 *   method {string} the expected http method
 *   times {number} the number of times to return the overridden response
 *   body {string} the json string to return from the mock server
 *   status {number} the http code to return from the mock server
 * }
 */
const mocks = {};

const handleOverrideRequest = (req, res, next) => {
  const url = req.url;
  const method = req.method;
  const override = req.body;

  if (method === 'PATCH') {
    console.log(`Overriding '${url}' with '${JSON.stringify(override)}'`);
    mocks[url] = override;

    res.sendStatus(204);
  } else {
    next();
  }
};

const handleRespondWithOverride = (req, res, next) => {
  const url = req.url;
  const method = req.method.toLowerCase();

  const mock = mocks[url];
  if (mock && mock.method.toLowerCase() === method && mock.times > 0) {
    console.log(`Responding with override ${JSON.stringify(mock)}`);
    mock.times -= 1;

    if (mock.status) {
      res.status(mock.status);
    }

    if (mock.body) {
      res.json(mock.body);
    } else {
      res.send();
    }
  } else {
    next();
  }
};

const getMiddlewareForOverride = () => [
  handleOverrideRequest,
  handleRespondWithOverride
];

module.exports = getMiddlewareForOverride;
