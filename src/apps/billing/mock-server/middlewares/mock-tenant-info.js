/* eslint-disable-next-line */
const router = require('express').Router();

const respondJson = require('../common/respondJson');

router.get('/_edge/tenant_info', respondJson({
  cloudId: '7b111aae-534e-4bc9-be95-1f25877eaf02'
}));

module.exports = router;
