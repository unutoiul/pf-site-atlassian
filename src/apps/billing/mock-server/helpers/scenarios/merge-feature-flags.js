module.exports = (oldValues, newValues) => {
  const parsedValues = Object.keys(newValues)
    .reduce((prev, cur) => ({ ...prev, [cur]: Boolean(newValues[cur]) }), {});
  return { ...oldValues, ...parsedValues };
};
