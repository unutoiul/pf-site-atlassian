const slugfy = require('../slugfy');

function importScenariosWebpack() {
  const scenarios = require.context('../../scenarios', true, /.js?$/);
  return scenarios.keys()
    .filter(file => file !== './index.js')
    .map(scenarios)
    .map(scenario => ({ ...scenario, slug: `${slugfy(scenario.group)}-${slugfy(scenario.name)}` }));
}

module.exports = importScenariosWebpack();
