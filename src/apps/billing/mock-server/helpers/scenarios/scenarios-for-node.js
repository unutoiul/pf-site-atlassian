const path = require('path');
const fs = require('fs');
const slugfy = require('../slugfy');

function importScenariosNode() {
  /* eslint-disable */
  const folder = path.join(__dirname, '..', '..', 'scenarios');
  return fs.readdirSync(folder)
    .filter(file => file !== 'index.js')
    .filter(file => file.slice(-3) === '.js')
    .map(file => require(path.join(folder, file)))
    .map(scenario => ({ ...scenario, slug: `${slugfy(scenario.group)}-${slugfy(scenario.name)}` }));
  /* eslint-enable */
}

module.exports = importScenariosNode();
