const { produce } = require('immer');
const availableScenarios = require('../../scenarios');
const mergeFeatureFlags = require('./merge-feature-flags');

const overwriteFeatureFlags = (modifiedFeatureFlags, method, url, originalBody) => {
  if (url.indexOf('/metadata') !== -1 && method === 'GET') {
    return produce(originalBody, (body) => {
      body.featureFlags = mergeFeatureFlags(body.featureFlags, modifiedFeatureFlags);
    });
  }
  return originalBody;
};

module.exports = ({ scenarios = [], featureFlags = {} }, method, url, status, body) => {
  const appliedScenarios = availableScenarios
    .filter(scenario => scenarios.includes(scenario.slug)) // filter out unrequested scenarios
    .filter(scenario => scenario.condition(method, url)) // filter out scenarios that does not apply to the request
    .reduce((previous, current) => current.invoke(previous.status, previous.body, method, url), { body, status });

  return {
    body: overwriteFeatureFlags(featureFlags, method, url, appliedScenarios.body),
    status: appliedScenarios.status
  };
};
