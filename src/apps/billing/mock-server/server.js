const jsonServer = require('json-server');
/* eslint-disable-next-line */
const express = require('express');
/* eslint-disable-next-line */
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const path = require('path');
const respondJson = require('./common/respondJson');
const middlewares = require('./middlewares');
const googleAutocomplete = require('./middlewares/googleAutocomplete');
const mockBillingService = require('./middlewares/mock-billing-service');
const mockTenantInfo = require('./middlewares/mock-tenant-info');
const mockGatewayPermissions = require('./middlewares/mock-gateway-permissions');
const applyScenarios = require('./middlewares/scenarios');
const scenariosSettingsPage = require('./middlewares/scenarios-settings-page');

const PORT = 3100;

const server = jsonServer.create();
server.use(jsonServer.defaults());
server.use(bodyParser.json());
server.use(cookieParser());
server.use(applyScenarios);

server.use('/gateway/api/billing-ux/api', middlewares('bux/api'));
// Using Google's own key!
server.use('/gateway/api/billing-ux/api/place', googleAutocomplete('AIzaSyDIJ9XX2ZvRKCJcFRrl-lRanEtFUow4piM'));
server.use('/gateway/api/billing-ux/cloud/38b7be26-03f7-42a9-b18b-9cdb56d204c2/', middlewares('bux/api'));
server.use('/gateway/api/billing-ux/cloud/e80be99d-56a4-48e6-b1cb-80d0c7ff74f1/', middlewares('bux/api'));
server.use('/gateway/api/billing-ux/cloud/c0b444df-5b8a-43b7-b3dd-5a8d08b723f5/', middlewares('bux/api'));
server.use('/gateway/api/billing-ux/organization/1363002c-4b37-303f-b9e3-06734d54989a/', middlewares('bux/api'));

server.use('/gateway/api/billing-ux/cloud/*/billing', middlewares('bux/api/billing'));
server.use('/gateway/api/billing-ux/organization/*/billing', middlewares('bux/api/billing'));

server.use('/mock-tns', express.static(path.join(__dirname, 'tns')));

server.use('/admin/rest/billing/api/instance', mockBillingService);
server.use('/', mockTenantInfo);
server.use('/gateway', mockGatewayPermissions);

server.post('/mock-events', respondJson({}));

// UM site admin specific mocks. Simply return empty success here to properly initialise site admin.
server.get('/admin/rest/um/1/*', respondJson({}));

scenariosSettingsPage(server);

server.listen(PORT, () => {
  /* eslint no-console: off */
  console.log(`Mock Server is running on ${PORT}`);
});
