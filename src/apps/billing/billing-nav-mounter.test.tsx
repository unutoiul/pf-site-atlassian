import { expect } from 'chai';
import { shallow } from 'enzyme';
import * as React from 'react';
import { assert, match, SinonSpy, spy } from 'sinon';

import { createMockIntlContext } from '../../utilities/testing';
import { BillingNavMounterImpl } from './billing-nav-mounter';

function createSiteBillingLink(pathPrefix) {
  return {
    icon: 'billing',
    id: 'pricing',
    priority: 0,
    title: {
      defaultMessage: 'Billing',
      description: '',
      id: 'chrome.nav-sections.billing.billing',
    },
    links: [
      {
        analyticsSubjectId: 'billing-overview',
        path: `${pathPrefix}/billing/overview`,
        title: {
          defaultMessage: 'Overview',
          description: '',
          id: 'chrome.nav-sections.billing.billing-overview',
        },
      },
      {
        analyticsSubjectId: 'billing-details',
        path: `${pathPrefix}/billing/paymentdetails`,
        title: {
          defaultMessage: 'Billing details',
          description: '',
          id: 'chrome.nav-sections.billing.billing-payment-details',
        },
      },
      {
        analyticsSubjectId: 'billing-history',
        path: `${pathPrefix}/billing/history`,
        title: {
          defaultMessage: 'Billing history',
          description: '',
          id: 'chrome.nav-sections.billing.billing-history',
        },
      },
      {
        analyticsSubjectId: 'billing-estimate',
        path: `${pathPrefix}/billing/estimate`,
        title: {
          defaultMessage: 'Bill estimate',
          description: '',
          id: 'chrome.nav-sections.billing.billing-estimate',
        },
      },
      {
        analyticsSubjectId: 'billing-applications',
        path: `${pathPrefix}/billing/applications`,
        title: {
          defaultMessage: 'Manage subscriptions',
          description: '',
          id: 'chrome.nav-sections.billing.billing-applications',
        },
      },
    ],
  };
}

function createDiscoverLink(pathPrefix) {
  return {
    analyticsSubjectId: 'discover-applications',
    path: `${pathPrefix}/billing/addapplication`,
    title: {
      defaultMessage: 'Discover applications',
      description: '',
      id: 'chrome.nav-sections.billing.discover-applications',
    },
    icon: 'discover-filled',
    priority: 50,
  };
}

describe('BillingNavMounter', () => {

  let updateNavigationSection: SinonSpy;
  let removeNavigationSection: SinonSpy;

  const featureFlag: any = {
    value: false,
  };

  beforeEach(() => {
    updateNavigationSection = spy();
    removeNavigationSection = spy();
  });

  const createBillingNavMounterWithData = (data) => {
    return shallow(
      <BillingNavMounterImpl
        data={data}
        featureFlag={featureFlag}
        updateNavigationSection={updateNavigationSection}
        removeNavigationSection={removeNavigationSection}
      />,
      createMockIntlContext(),
    );
  };

  it('should not render any children', () => {
    const component = shallow(
      <BillingNavMounterImpl
        featureFlag={featureFlag}
        updateNavigationSection={updateNavigationSection}
        removeNavigationSection={removeNavigationSection}
      >
        <div className="child" />
      </BillingNavMounterImpl>,
      createMockIntlContext(),
    );
    expect(component.find('.child').length).to.equal(0);
  });

  it('should not update navigation when still loading data', () => {
    createBillingNavMounterWithData({
      loading: true,
      currentUser: {
      },
    });

    expect(removeNavigationSection.called).to.equal(true);
    expect(updateNavigationSection.notCalled).to.equal(true);
  });

  it('should update navigation only once', () => {
    const wrapper = createBillingNavMounterWithData({
      loading: true,
      currentSite: {
      },
    });
    wrapper.setProps({
      data: {
        loading: false,
      },
    });

    wrapper.setProps({
      data: {
        loading: false,
      },
    });

    expect(removeNavigationSection.callCount).to.equal(1);
    expect(updateNavigationSection.callCount).to.equal(1);
  });

  it('should update navigation with site specific billing links when billingADG3 flag is enabled', () => {
    const wrapper = createBillingNavMounterWithData({
      loading: true,
    });
    wrapper.setProps({
      updateNavigationSection,
      data: {
        loading: false,
        currentSite: {
          id: 'site1',
          flags: {
          },
        },
      },
    });

    assert.calledWith(updateNavigationSection, match({
      id: 'subscriptions',
      links: [createSiteBillingLink('/admin/s/site1'), createDiscoverLink('/admin/s/site1')],
    }));
  });
});
