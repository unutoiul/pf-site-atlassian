import { expect } from 'chai';
import proxyquire from 'bux/helpers/proxyquire';
import request from 'bux/common/helpers/request';

export const PactOpts = {
  consumer: 'billing-ux-frontend',
  provider: 'billing-ux-service',
  providerPort: 1234
};

const MockServerUrl = `http://localhost:${PactOpts.providerPort}`;

export const billingUxService = proxyquire.noCallThru().load('../../src/shared/common/api/billing-ux-service.js', {
  'bux/common/config': { billingUXServiceUrl: `http://localhost:${PactOpts.providerPort}` },
  'bux/common/helpers/request': request,
});

//pact needs a string, it doesn't handle JSON directly
export function wrapBuxResponse(serviceCall) {
  return serviceCall.then(data => JSON.stringify(data));
}


function makeRequest(path, aidToken, method = 'get') {
  const options = { headers: { cookie: `ATL_TOKEN=${aidToken}` } };
  let requestPromise;
  if (method === 'get') {
    requestPromise = request.get(MockServerUrl + path, options);
  } else {
    requestPromise = request[method](MockServerUrl + path, {}, options);
  }
  return requestPromise
    .then(response => JSON.stringify({
      statusCode: response.status,
      data: response.text()
    }))
    .catch(error => JSON.stringify({
      statusCode: error.response.status
    }));
}

export function shouldHandle403(path, method = 'get') {
  PactConsumer(PactOpts, () => {
    addInteractions([
      {
        state: 'billing-ux-service is healthy',
        uponReceiving: `a request to ${path} details with a invalid AID cookie`,
        withRequest: {
          method,
          path,
          headers: { Accept: 'application/json', cookie: 'ATL_TOKEN=invalid' }
        },
        willRespondWith: {
          status: 403
        }
      }
    ]);
    verify(
      'forbidden error is returned',
      () => makeRequest(path, 'invalid', method),
      (result, done) => {
        expect(JSON.parse(result).statusCode).to.eql(403);
        done();
      }
    );

    finalizePact();
  });
}
