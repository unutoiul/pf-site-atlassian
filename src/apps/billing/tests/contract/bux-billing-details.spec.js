import { expect } from 'chai';
import { shouldHandle403, billingUxService, wrapBuxResponse, PactOpts } from './commonPact';
import BillingDetailsMock from '../../mock-server/bux/api/billing/billing-details-get';

shouldHandle403('/api/billing/billing-details');

PactConsumer(PactOpts, () => {
  addInteractions([{
    state: 'billing-ux-service is healthy',
    uponReceiving: 'a request for billing details with a valid AID cookie',
    withRequest: {
      method: 'get',
      path: '/api/billing/billing-details',
    },
    willRespondWith: {
      status: 200,
      headers: { 'Content-Type': 'application/json; charset=utf-8' },
      body: BillingDetailsMock
    }
  }]);

  verify(
    'billing details are returned',
    () => wrapBuxResponse(billingUxService.getBillingDetails()),
    (result, done) => {
      expect(JSON.parse(result).firstName).to.eql('Supernova');
      done();
    }
  );

  finalizePact();
});

//todo understand why the BUX-service swagger doc does specify a 401 error case
//todo understand why the BUX-service swagger doc doesn't specify a 500 error case
