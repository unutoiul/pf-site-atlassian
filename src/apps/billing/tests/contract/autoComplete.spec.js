import { expect } from 'chai';
// We just checking internal module
import { getAutoComplete } from '../../bux/common/api/autocomplete';

const atlassAddress = {
  id: 1,
  point: [151, -34],
  text: '341 George Street, Sydney, NSW, Australia',
  addressDetails: {
    country: { name: 'Australia', code: 'AU' },
    region: { name: 'New South Wales', code: 'NSW' },
    locality: { name: 'Sydney' },
    street: { name: 'George Street', label: '341 George Street' },
    postalcode: '2000'
  }
};

describe('Address information', () => {
  describe.skip('Autocomplete API', () => {
    it('should keep format', (done) => {
      const autoComplete = getAutoComplete();
      if (autoComplete) {
        autoComplete('341 George street, Sy', { country: 'AU' }).then((results) => {
          const atlassianOffice = results.filter(line =>
            line.text.indexOf('341 George Street') >= 0 &&
            line.text.indexOf('Sydney') > 0 &&
            line.text.indexOf('Australia') > 0);

          expect(atlassianOffice).not.to.have.length(0);

          // drop floating point
          const point = atlassianOffice[0].point;
          point[0] = Math.floor(point[0]);
          point[1] = Math.floor(point[1]);
          // drop server-side sorting
          atlassAddress.id = atlassianOffice[0].id;
          atlassAddress.text = atlassianOffice[0].text;

          expect(JSON.stringify(atlassianOffice[0])).to.be.equal(JSON.stringify(atlassAddress));
          // Do not work for level 2+
          // expect(atlassianOffice[0]).to.be.deep.equal(atlassAddress);

          done();
        }).catch((err) => {
          throw new Error(err);
        });
      }
    });
  });
});
