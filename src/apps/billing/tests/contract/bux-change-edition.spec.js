import { expect } from 'chai';
import { billingUxService, PactOpts, wrapBuxResponse, shouldHandle403 } from './commonPact';

shouldHandle403('/api/billing/entitlements/666/edition/free', 'put');

PactConsumer(PactOpts, () => {
  addInteractions([{
    state: 'billing-ux-service is healthy',
    uponReceiving: 'a request to change edition with a valid AID cookie',
    withRequest: {
      method: 'put',
      path: '/api/billing/entitlements/12345/edition/free',
    },
    willRespondWith: {
      status: 200,
      headers: { 'Content-Type': 'application/json; charset=utf-8' }
    }
  }]);

  verify(
    'change edition returns a successful response',
    () => wrapBuxResponse(billingUxService.selectEdition({ entitlementId: '12345', edition: 'free' })),
    (result, done) => {
      expect(JSON.parse(result)).to.eql({});
      done();
    }
  );

  finalizePact();
});