import { expect } from 'chai';
import { billingUxService, PactOpts, shouldHandle403, wrapBuxResponse } from './commonPact';
import Invoices from '../../mock-server/bux/api/billing/invoice-history-get.json';

shouldHandle403('/api/billing/invoice-history');

PactConsumer(PactOpts, () => {
  addInteractions([{
    state: 'billing-ux-service is healthy',
    uponReceiving: 'a request for billing history with a valid AID cookie',
    withRequest: {
      method: 'get',
      path: '/api/billing/invoice-history',
    },
    willRespondWith: {
      status: 200,
      headers: { 'Content-Type': 'application/json; charset=utf-8' },
      body: Invoices
    }
  }]);

  verify(
    'bill history is returned',
    () => wrapBuxResponse(billingUxService.getBillHistory()),
    (result, done) => {
      expect(JSON.parse(result).invoices.length).to.eql(4);
      done();
    }
  );

  finalizePact();
});

