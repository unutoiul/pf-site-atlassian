const selenium = require('selenium-standalone');

function install(cb) {
  console.log('Installing selenium');
  selenium.install({
    // check for more recent versions of selenium here:
    // https://selenium-release.storage.googleapis.com/index.html
    version: '3.8.1',
    baseURL: 'https://selenium-release.storage.googleapis.com',
    logger(message) {
      console.log(message);
    },
  }, cb);
}

module.exports = {
  start(cb) {
    install((err) => {
      if (err) {
        cb(err);
      } else {
        selenium.start({}, cb);
      }
    });
  }
};
