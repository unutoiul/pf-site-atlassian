import { Fragment } from 'react';
import { remock } from 'react-remock';
import { Trans } from '@lingui/react';

remock.mock(Trans, () => ({ type: Fragment }));
