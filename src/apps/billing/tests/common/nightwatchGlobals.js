const seleniumServer = require('./seleniumServer');
// const { fork } = require('child_process');
const terminate = require('terminate');

const defaultConf = {
  serverUrl: 'http://localhost:4000/admin/s/38b7be26-03f7-42a9-b18b-9cdb56d204c2/billing',
  buxMockUrl: 'http://localhost:4000/gateway/api/billing-ux/cloud/38b7be26-03f7-42a9-b18b-9cdb56d204c2'
};

let seleniumProcess;
// let e2eServerProcess;
// let mockServerProcess;

module.exports = {
  default: defaultConf,
  chrome: defaultConf,
  firefox: defaultConf,
  ie: defaultConf,
  edge: defaultConf,

  before(cb) {
    // e2eServerProcess = fork('tests/e2e/server.js');
    // mockServerProcess = fork('mock-server/server.js');
    seleniumServer.start((err, child) => {
      if (err) {
        console.log('Error starting selenium', err);
        throw err;
      }
      seleniumProcess = child;
      cb();
    });
  },
  after() {
    const logErrorIfExists = err => err && console.log(err);
    terminate(seleniumProcess.pid, logErrorIfExists());
    // e2eServerProcess.kill();
    // mockServerProcess.kill();
  },
  afterEach(browser, cb) {
    console.log('Closing browser at the end of each test case.');
    browser.end(cb);
  },
};
