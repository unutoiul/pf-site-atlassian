const path = require('path');
/* eslint-disable-next-line */
const wrapper = require('@pact-foundation/pact-node');
/* eslint-disable-next-line */
require('isomorphic-fetch');

const mockServer = wrapper.createServer({
  port: 1234,
  log: path.resolve(process.cwd(), 'build-output/pact', 'pact-bux-ui.log'),
  dir: path.resolve(process.cwd(), 'pacts'),
  spec: 2
});

mockServer.start().then(() => {
  run();
  process.on('SIGINT', () => {
    wrapper.removeAllServers();
  });
});
