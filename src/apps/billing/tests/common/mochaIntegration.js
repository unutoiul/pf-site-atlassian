import React from 'react';
import chai from 'chai';
import chaiEnzyme from 'chai-enzyme';
import { configure as configureEnzyme } from 'enzyme';
import dirtyChai from 'dirty-chai';
import sinonChai from 'sinon-chai';
import { remock } from 'react-remock';
import { I18n, Trans } from '@lingui/react';
import { i18n } from 'bux/helpers/jsLingui';
import IntlPolyfill from 'intl';
import './reactMocks';

configureEnzyme({ disableLifecycleMethods: true });

remock.mock(I18n, (type, props, children) => (
  children[0]({ i18n })
));

// remove props from Trans to supress "Invalid prop `id` supplied to `React.Fragment`"
remock.mock(Trans, (type, props, children) => <React.Fragment>{children}</React.Fragment>);
/**
 * Allow chai assertions to end in function form.
 * This is to get around the no-unused-expressions linting rule.
 *
 * For example:
 *
 *    expect(value).to.have.been.true;
 *
 * can now be:
 *
 *    expect(value).to.have.been.true();
 */
chai.use(dirtyChai);

/**
 * Extend chai to allow assertions on enzyme wrappers.
 */
chai.use(chaiEnzyme());

/**
 * Extend chai to allow assertions on sinon spyies/stubs.
 */
chai.use(sinonChai);

// Polyfill NumberFormat to keep compatibility between node versions.
// https://github.com/andyearnshaw/Intl.js/
Intl.NumberFormat = IntlPolyfill.NumberFormat;
