import globals from '../common/nightwatchGlobals';

const getOverrides = (options = {}) => ({
  method: options.method || 'GET',
  status: options.status || 200,
  body: options.body || {},
  times: options.times || 1
});

async function mockRespondWith(url, options, callback) {
  const overrides = getOverrides(options);

  return fetch(`${globals.default.buxMockUrl}/${url}`, {
    method: 'patch',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(overrides)
  }).then(() => {
    // eslint-disable-next-line no-console
    console.log('Patching complete, resuming tests');
    callback();
  });
}

function createMockRespondWith(url) {
  return async function mockRespondWithForUrl(options, callback = () => {}) {
    return mockRespondWith(url, options, callback);
  };
}


export const mockBillingDetails = createMockRespondWith('api/billing/billing-details');
export const mockBillEstimate = createMockRespondWith('api/billing/bill-estimate');
export const mockCompanyDetails = createMockRespondWith('api/billing/customer-details');
export const mockEntitlementGroup = createMockRespondWith('api/billing/entitlement-group');
export const mockInvoiceContact = createMockRespondWith('api/billing/invoice-contact');
export const mockInvoiceHistory = createMockRespondWith('api/billing/invoice-history');
export const mockMetadata = createMockRespondWith('api/metadata');
export const mockBillingValidation = createMockRespondWith('api/billing/validate-billing-details');

