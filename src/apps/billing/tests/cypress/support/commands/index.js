/* global Cypress cy */

// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

require('./payment-details');
require('./manage-subscriptions');
const { formatMoney } = require('../../../../bux/common/formatters/money-formatter');

// Override visit cause currently we can't stub fetch, waiting on:
// https://github.com/cypress-io/cypress/issues/95
Cypress.Commands.overwrite('visit', (originalFn, url, options) => {
  cy.on('window:before:load', (win) => {
    /* eslint no-param-reassign: 0 */
    win.fetch = null;
  });
  return originalFn(url, options);
});

// Make a route become faulty with status 500
Cypress.Commands.add('asError', { prevSubject: true }, (subject, { response = {}, status = 500 } = {}) =>
  cy.route(Object.assign({}, subject, { response, status })));

// Change a fixture body
Cypress.Commands.add('patch', { prevSubject: true }, (subject, options) => Object.assign({}, subject, options));

// Select option on AK select
Cypress.Commands.add('selectAkOption', { prevSubject: true }, (subject, { value, autocomplete = false }) => {
  if (autocomplete === true) {
    cy.get(subject.selector)
      .click({ force: true })
      .clear({ force: true })
      .type(value, { force: true });
  } else {
    cy.get(subject.selector)
      .click({ force: true });
  }
  return cy
    .get(`div[role="option"]:contains(${value})`)
    .click();
});

// Fill input inside an iframe
Cypress.Commands.add('setIFrameFormValue', (iframe, value) => (
  cy.get(iframe).then(($iframe) => {
    $iframe.contents().find('input').val(value).trigger('change');
  })
));

// Check notification title
Cypress.Commands.add('notification', (title, type) => (
  cy.get('[data-flag-e2e]')
    .should('have.attr', 'data-title')
    .and('eql', title)
    .then(() => {
      if (type) {
        return cy.get('[data-flag-e2e]')
          .should('have.attr', 'data-type')
          .and('eql', type);
      } 
      return null;
    })
));

// Close notification
Cypress.Commands.add('closeNotification', () => (
  cy.get('body')
    .then($body => $body.find('[role=alert]'))
    .each(el => (
      cy.wrap(el)
        .get('[data-flag-e2e]')
        .then(flag => (flag.length ? el.get('[aria-label="Dismiss flag"]') : null))
        .then(close => (close && close.length ? close.click() : null))
    ))
));

// Close focused task
Cypress.Commands.add('closeFocusedTask', () => (
  cy.get('body').then(($body) => {
    const button = $body.find('.bux-focused-drawer button[aria-haspopup=true][role=button]').first();
    if (button.length) {
      button.click();
    }
  })
));

const getFlagsByType = (featureFlags, flagType) =>
  Object.entries(featureFlags).reduce((ans, [key, value]) => {
    const typeOfFeatureFlag = typeof value;
    if (typeOfFeatureFlag === flagType) {
      ans[key] = value;
    }
    return ans;
  }, {});

// Patch feature flags
Cypress.Commands.add('featureFlags', featureFlags =>
  cy.fixture('metadata-get')
    .patch({
      featureFlags: getFlagsByType(featureFlags, 'boolean'),
      multivariateFeatureFlags: getFlagsByType(featureFlags, 'string')
    })
    .as('mock-metadata')
    .route('**/api/billing-ux/**/metadata', '@mock-metadata')
    .as('mock-metadata-feature-flag'));

Cypress.Commands.add('checkTenantInfo', (tenantName) => {
  let name = `Site: ${tenantName}`;
  if (Cypress.env('ORGANIZATION')) {
    name = `Organization: ${tenantName}`;
  }
  return cy.get('.test-tenant-info').should('contain', name);
});

Cypress.Commands.add('checkPeriodInfo', period => (
  cy.get('.test-period-info').should('have.text', period)
));

Cypress.Commands.add('currency', { prevSubject: true }, (subject, {
  amount, includeLabel = false, withCents = false, currency = 'USD', language = 'en-US'
} = {}) => (
  cy.wrap(subject).should('have.text', formatMoney(amount, {
    includeLabel, withCents, currency, language
  }))
));
