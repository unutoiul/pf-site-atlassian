/* global Cypress cy */

//----------------------------------------------
// MANAGE SUBSCRIPTIONS HELPERS
//----------------------------------------------

Cypress.Commands.add('isAtChangeEditionFocusedTask', (url) => {
  cy.get('div[data-logroot="hipchat.cloud"] button.mainActionButton').click()
    .get('.focusedTaskContent').should('exist')
    .url()
    .should('contain', `${url}/change-edition/b7db7827-e2ac-492e-a3bb-e1c310764043`);
});

Cypress.Commands.add('closedFocusedTask', () => (cy.get('.focusedTaskContent').should('not.exist')));

Cypress.Commands.add('isAtManageSubscriptionsPage', () => (
  cy.get('[data-logroot="manageSubscriptions"] .page-content')
));
