/* global Cypress cy */

//----------------------------------------------
// PAYMENT DETAILS FORM HELPERS
//----------------------------------------------
Cypress.Commands.add(
  'fillBillingAddress',
  ({
    organisationName, taxpayerId, addressLineFull, postcode, city, addressLine1, addressLine2, country, state
  }) => {
    if (organisationName) cy.get('input[name="organisationName"]').type(organisationName);

    if (addressLineFull) {
      cy.get('#addressLineFull').clear().type(addressLineFull);
      cy.get('.form-message').should('contain', 'Select a value from the list below');
    }
    cy.server();
    cy.route('GET', '**/place/autocomplete/?*').as('getAddress');
    cy.get('#addressLineFull').clear().type('somethingyoucantcomplete');
    cy.wait('@getAddress');
    cy.get('.form-message').should('contain', 'Oops, we couldn\'t find your address.');
    cy.get('.OpenFullAddressForm').click();

    // State as select field when US
    cy.get('#country').selectAkOption({ value: 'United States of America', autocomplete: true });
    cy.get('#state').should('be', 'select');

    // State as input field when BE
    cy.get('#country').selectAkOption({ value: 'Belgium', autocomplete: true });
    cy.get('#state').should('be', 'input');

    if (country) cy.get('#country').selectAkOption({ value: country, autocomplete: true });
    if (state) cy.get('#state').selectAkOption({ value: state, autocomplete: true });

    if (postcode) cy.get('#postcode').type(postcode);
    if (city) cy.get('#city').type(city);
    if (addressLine1) cy.get('#addressLine1').type(addressLine1);
    if (addressLine2) cy.get('#addressLine2').type(addressLine2);

    if (taxpayerId) cy.get('input[name="taxpayerId"]').type(taxpayerId);
  }
);

Cypress.Commands.add(
  'fillShortBillingAddress',
  ({
    organisationName, taxpayerId, addressLineFull,
  }) => {
    if (organisationName) cy.get('input[name="organisationName"]').type(organisationName);

    cy.server();
    cy.route('GET', '**/place/autocomplete/?*').as('getAddress');
    cy.route('GET', '**/place/details/?*').as('getAddressDetails');

    cy.get('#addressLineFull').clear().type(addressLineFull);

    cy.wait('@getAddress');

    cy.get('.form-message').should('contain', 'Select a value from the list below');
    cy.get('#addressLineFull').type('{downarrow}').type('{enter}');

    cy.wait('@getAddressDetails');

    if (taxpayerId) cy.get('input[name="taxpayerId"]').type(taxpayerId);
  }
);

Cypress.Commands.add('fillCreditCard', ({
  number, name, expiryMonth, expiryYear, securityCode
}) => {
  if (number) cy.setIFrameFormValue('.gw-proxy-number', number);
  if (name) cy.get('input#creditcard-name').type(name);
  if (expiryMonth) cy.get('#react-select-creditcard-month-input').selectAkOption({ value: expiryMonth });
  if (expiryYear) cy.get('#react-select-creditcard-year-input').selectAkOption({ value: expiryYear });
  if (securityCode) cy.setIFrameFormValue('.gw-proxy-securityCode', securityCode);
});

Cypress.Commands.add('checkCreditCardPaymentMethod', ({
  number, name, expiryMonth, expiryYear
}) => (
  cy.get('[data-test="payment-method"]').within(() => {
    cy.get('[data-test="credit-card"]').should('exist')
      .get('.number').should('contain', number.slice(-4))
      .get('.name')
      .should('have.text', name)
      .get('.expiry')
      .should('contain', `${expiryMonth}/${expiryYear}`);
  })
));

Cypress.Commands.add('mockPayPalButton', () => {
  cy.on('window:before:load', (win) => {
    /* eslint no-param-reassign: 0 */
    win.mockPayPalButton = true;
  });
});

Cypress.Commands.add('checkEstimate', ({ products, total, hasTax }) => {
  cy.get('.product-list').should('exist').within(() => {
    cy.get('.product-line').should('have.length', products.length);
    products.forEach((product, index) => {
      cy.get(`.product-line:eq(${index})`).within(() => {
        cy.get('.product-description').should('have.text', product.name);
        cy.get('.product-price').currency(product.cost);
        cy.get('.product-usage-list').should('have.text', product.usage);
      });
    });
  });
  cy.get('.summary').within(() => {
    if (hasTax) {
      cy.get('.product-line-tax-total .tax-total').should('exist');
    } else {
      cy.get('.product-line-tax-total .tax-total').should('not.exist');
    }
    cy.get('.product-line-total .total-date').should('have.text', 'Total');
    cy.get('.product-line-total .total-cost').currency(total);
  });
});

Cypress.Commands.add('checkAddress', ({
  organisationName, addressLine1, addressLine2, postcode, city, stateCode, countryCode,
}) => {
  const line1 = addressLine1 ? `${addressLine1}, ` : '';
  const line2 = addressLine2 ? `${addressLine2}, ` : '';
  return cy.get('[data-test="address"]')
    .should(
      'have.text',
      `${organisationName}, ${line1}${line2}${postcode}, ${city}, ${stateCode}, ${countryCode}`
    );
});
