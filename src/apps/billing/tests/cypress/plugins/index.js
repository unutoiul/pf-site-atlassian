// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)
const webpack = require('@cypress/webpack-preprocessor');

module.exports = (on, config) => {
  // `on` is used to hook into various events Cypress emits
  // `config` is the resolved Cypress config
  on('file:preprocessor', webpack());

  const modifiedConfig = config;

  let billingUrl;
  let screenshotsFolder;
  if (config.env.ORGANIZATION === 1) {
    billingUrl = config.env.organizationUrl;
    screenshotsFolder = './build-output/tests/cypress-organization/screenshots';
  } else {
    billingUrl = config.env.cloudUrl;
    screenshotsFolder = './build-output/tests/cypress-cloud/screenshots';
  }
  modifiedConfig.env.billingUrl = billingUrl;
  modifiedConfig.screenshotsFolder = screenshotsFolder;

  if (config.env.BASE_URL) {
    modifiedConfig.baseUrl = config.env.BASE_URL;
  }
  return modifiedConfig;
};
