/* global cy Cypress */
/* eslint newline-per-chained-call: 0 */

const URL = `${Cypress.env('billingUrl')}/paymentdetails`;
const URL_SWITCH_TO_ANNUAL = `${URL}/switch-to-annual`;
const address = {
  organisationName: 'Potato Corp.',
  taxpayerId: '12345678200',
  addressLineFull: '343 George street',
  postcode: '2000',
  city: 'Sydney',
  addressLine1: 'Level 6',
  addressLine2: '341 George St',
  country: 'Australia',
  countryCode: 'AU',
  state: 'New South Wales',
  stateCode: 'NSW',
};
const creditCard = {
  number: '4444333322221111',
  name: 'Superplasma Team',
  expiryMonth: '01',
  expiryYear: '2022',
  securityCode: '100'
};
const estimate = {
  products: [
    { name: 'Confluence (Cloud)', cost: { amount: 1000 }, usage: '820 users' },
    { name: 'JIRA Service Desk (Cloud)', cost: { amount: 5420 }, usage: '271 agents' },
    { name: 'JIRA Software (Cloud)', cost: { amount: 750 }, usage: '301 users' },
  ],
  total: { amount: 7887, includeLabel: true },
  hasTax: true,
};

describe('Monthly to annual', () => {
  describe('Happy paths', () => {
    it('should download quote', () => {
      cy.on('window:before:load', (win) => {
        cy.stub(win, 'open').as('windowOpen');
      });

      cy.visit(URL_SWITCH_TO_ANNUAL);

      cy.get('[data-test="download-quote"').within(() => {
        cy.get('a').click();
      });

      cy.notification('Please wait until the download begins...', 'normal');

      cy.get('@windowOpen')
        .should('be.calledWithMatch', 'https://my.atlassian.com/billing/pdf', '_top');
    });

    it('should pay by check or bank transfer', () => {
      cy.on('window:before:load', (win) => {
        cy.stub(win, 'open').as('windowOpen');
      });

      cy.visit(URL_SWITCH_TO_ANNUAL);

      cy.get('[data-test="new-switch-to-annual-wizard"]').within(() => {
        cy.get('div[data-test=payment-option-bank-transfer] label').click();

        cy.get('button[type=submit]')
          .should('have.text', 'Download invoice')
          .click();
      });

      cy.notification('Please wait until the download begins...', 'normal');

      cy.get('@windowOpen')
        .should('be.calledWithMatch', 'https://my.atlassian.com/billing/pdf', '_top');
    });

    it('should use current credit card', () => {
      cy.visit(URL_SWITCH_TO_ANNUAL);

      cy.get('[data-test="new-switch-to-annual-wizard"]').within(() => {
        cy.checkAddress(address);
        cy.checkCreditCardPaymentMethod(creditCard);
        let name = 'localhost';
        if (Cypress.env('ORGANIZATION')) {
          name = 'Acme';
        }
        cy.checkTenantInfo(name);
        cy.checkPeriodInfo('Oct 17, 2016 - Nov 16, 2022');
        cy.checkEstimate(estimate);

        cy.get('button[type=submit]')
          .should('have.text', 'Pay & switch to annual')
          .click();
      });

      cy.notification('The order was processed successfully.', 'success');
    });
  });

  describe('Fail', () => {
    it('should show error if process order have a network error', () => {
      cy.server()
        .route('POST', '**/api/**/order')
        .asError({ status: 408 })
        .as('post-process-order');

      cy.visit(URL_SWITCH_TO_ANNUAL);

      cy.get('[data-test="new-switch-to-annual-wizard"]').within(() => {
        cy.get('button[type=submit]')
          .click();
      });

      cy.wait('@post-process-order');

      cy.notification('Network error', 'error');
    });

    it('should show error if process order have a server error', () => {
      cy.server()
        .route('POST', '**/api/**/order')
        .asError({ status: 400 })
        .as('post-process-order');

      cy.visit(URL_SWITCH_TO_ANNUAL);

      cy.get('[data-test="new-switch-to-annual-wizard"]').within(() => {
        cy.get('button[type=submit]')
          .click();
      });

      cy.get('[data-test=billing-error]')
        .should('contain', 'An error occurred and we could not process your request.');
    });

    const showErrorWithOrderReference = () => {
      cy.visit(URL_SWITCH_TO_ANNUAL);

      cy.get('[data-test="new-switch-to-annual-wizard"]').within(() => {
        cy.get('button[type=submit]')
          .click();

        cy.wait('@post-process-order');

        cy.get('[data-test=billing-error]')
          .should('contain', 'team with the following order reference number: AT-12345.');
        cy.get('button[type=submit]')
          .should('not.exist');
      });
    };

    it('should show error if order is unsuccessful', () => {
      cy.fixture('order/12345/progress-get')
        .patch({ successful: false })
        .as('mock-order-progress');

      cy.server()
        .route('GET', '**/api/**/order/12345/progress', '@mock-order-progress')
        .as('post-process-order');

      showErrorWithOrderReference();
    });

    it('should show error if order pooling have network error', () => {
      cy.server()
        .route('GET', '**/api/**/order/12345/progress')
        .asError({ status: 408 })
        .as('post-process-order');

      showErrorWithOrderReference();
    });

    it('should show error if order pooling have server error', () => {
      cy.server()
        .route('GET', '**/api/**/order/12345/progress')
        .asError({ status: 400 })
        .as('post-process-order');

      showErrorWithOrderReference();
    });

    it('should show error if order pooling have timeout error', () => {
      cy.on('window:before:load', (win) => {
        /* eslint no-param-reassign: 0 */
        win.mockConvertToAnnualPoolingTimeout = 3000;
      });

      cy.fixture('order/12345/progress-get')
        .patch({ completed: false })
        .as('mock-order-progress');

      cy.server()
        .route('GET', '**/api/**/order/12345/progress', '@mock-order-progress')
        .as('post-process-order');

      showErrorWithOrderReference();
      cy.get('[data-test=billing-error]')
        .should('contain', 'Your request is taking more than usual');
    });
  });

  describe('Show/hide convert to annual link', () => {
    it('should not have access to flow if on annual', () => {
      cy.fixture('billing/bill-estimate-get')
        .patch({
          nextBillingPeriod: {
            renewalFrequency: 'ANNUAL',
          },
        })
        .as('mock-bill-estimate');

      cy.server()
        .route('**/api/billing-ux/**/bill-estimate', '@mock-bill-estimate')
        .as('fetch-bill-estimate');

      cy.visit(URL);
      cy.wait('@fetch-bill-estimate');
      cy.get('[data-logroot="paymentDetails"] .page-content')
        .get('.change-renewal-frequency__link').should('not.exist');
    });

    it('should not have access to flow if there is no payment method', () => {
      cy.fixture('billing/billing-details-get')
        .patch({
          creditCard: undefined,
        })
        .as('mock-billing-details');

      cy.server()
        .route('**/api/billing-ux/**/billing-details', '@mock-billing-details')
        .as('fetch-billing-details');

      cy.visit(URL);
      cy.wait('@fetch-billing-details');
      cy.get('[data-logroot="paymentDetails"] .page-content')
        .get('.change-renewal-frequency__link').should('not.exist');
    });

    it('should not have access to flow if has expired account', () => {
      cy.fixture('billing/bill-estimate-get')
        .patch({
          currentBillingPeriod: {
            endDate: '2018-01-01',
          },
        })
        .as('mock-bill-estimate');

      cy.server()
        .route('**/api/billing-ux/**/bill-estimate', '@mock-bill-estimate')
        .as('fetch-bill-estimate');

      cy.visit(URL);
      cy.wait('@fetch-bill-estimate');
      cy.get('[data-logroot="paymentDetails"] .page-content')
        .get('.change-renewal-frequency__link').should('not.exist');
    });
  });
});
