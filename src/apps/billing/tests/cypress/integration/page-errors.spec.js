/* global cy Cypress */

const URL = `${Cypress.env('billingUrl')}`;

context('Error pages', () => {
  context('Bill estimate', () => {
    it('Bill Estimate loading error page test', () => {
      cy
        .server()
        .route('**/api/billing-ux/**/bill-estimate')
        .asError()
        .as('fetch-billing-details')

        .visit(`${URL}/estimate`)
        .wait('@fetch-billing-details')
        .get('.error.unavailable')
        .should('exist')
        .get('.error.unavailable .payment-required')
        .should('not.exist');
    });

    it('Bill Estimate deactivated error page test', () => {
      cy
        .server()
        .route('**/api/billing-ux/**/bill-estimate')
        .asError({ status: 423 })
        .as('fetch-bill-estimate')

        .visit(`${URL}/estimate`)
        .wait('@fetch-bill-estimate')
        .get('.error.unavailable .payment-required')
        .should('exist');
    });
  });
});
