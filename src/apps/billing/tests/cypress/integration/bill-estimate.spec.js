/* global cy Cypress */

const URL = `${Cypress.env('billingUrl')}/estimate`;

describe('Bill Estimate', () => {
  before(() => {
    cy.visit(URL);
  });

  it('Should render the page', () => {
    cy.get('[data-logroot="billEstimate"] .page-content');
  });

  it('Should render products list', () => {
    cy.get('.product-list').should('exist').within(() => {
      cy.get('.product-line').should('have.length', 9);
      // Bitbucket
      cy.get('.product-line:eq(0)').within(() => {
        cy.get('.product-description').should('have.text', 'Bitbucket');
        cy.get('.product-price').currency({ amount: 3720 });
        cy.get('.product-usage:eq(0)').should('have.text', '742 users');
        cy.get('.product-usage:eq(1)').should('have.text', '30 gigabytes');
        cy.get('.product-usage:eq(2)').should('have.text', '50 build minutes');
      });
      // Identity Manager (Cloud)
      cy.get('.product-line:eq(2)').within(() => {
        cy.get('.product-description').should('have.text', 'Identity Manager (Cloud)');
        cy.get('.product-price').should('have.text', 'Inactive');
        cy.get('.product-usage:eq(0)').should('have.text', 'Unlimited');
      });
      // JIRA Core (Cloud)
      cy.get('.product-line:eq(3)').within(() => {
        cy.get('.product-description').should('have.text', 'JIRA Core (Cloud)');
        cy.get('.product-price').should('have.text', 'Free');
        cy.get('.trialLozenge').should('have.text', 'Free trial until Dec 17, 2032');
        cy.get('.product-usage:eq(0)').should('have.text', '1 user');
      });
    });
    cy.get('.summary').within(() => {
      cy.get('.product-line-tax-total .tax-total').currency({ amount: 1139, includeLabel: true });
      cy.get('.product-line-total .total-date').should('have.text', 'Total due Oct 6, 2022');
      cy.get('.product-line-total .total-cost').currency({ amount: 12529, includeLabel: true });
    });
    cy.get('.auto-charged-label')
      .should('have.text', 'Based on your current configuration, you are being automatically charged $12,529.');
  });
});
