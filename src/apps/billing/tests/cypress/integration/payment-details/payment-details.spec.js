/* global cy Cypress */
/* eslint newline-per-chained-call: 0 */
import { PAYMENT_METHOD_CREDIT_CARD } from '../../../../bux/core/state/billing-details/constants.ts';

const URL = `${Cypress.env('billingUrl')}/paymentdetails`;

describe('Payment Details', () => {
  before(() => cy.visit(URL));

  it('Should render the page', () => {
    cy.title('Update billing details')
      .get('[data-logroot="paymentDetails"] .page-content');
  });

  it('Billing details page default', () => {
    cy.get('div[data-test="payment-method-panel"]').should('exist');
    cy.get('div[data-test="credit-card"]').should('exist');
    cy.get('input[name="organisationName"]').should('exist');

    cy.get('.change-renewal-frequency__link').should('contain', 'Switch to annual');

    // Sidebar - Bill
    cy.get('.bill-estimate-summary')
      .within(() => {
        cy
          .get('.product-line')
          .should('have.length', 3)
          .get('.product-line:nth-child(1)')
          .within(() => {
            cy.get('.product-description')
              .should('have.text', 'Bitbucket')
              .get('.product-price')
              .currency({ amount: 3720 });
          })
          .get('.product-line:nth-child(2)')
          .within(() => {
            cy.get('.product-description')
              .should('have.text', 'Confluence (Cloud)')
              .get('.product-price')
              .currency({ amount: 1000 });
          })
          .get('.price-rest')
          .currency({ amount: 6170 })
          .get('.total-tax')
          .currency({ amount: 1139, withCents: true })
          .get('.total-cost')
          .currency({ amount: 12529, includeLabel: true })
          .get('.expand-bill')
          .click()
          .get('.product-line')
          .should('have.length', 9)
          .get('.collapse-bill')
          .click()
          .get('.product-line')
          .should('have.length', 3);
      });
  });

  it('Billing details page address support', () => (
    cy.get('input[name="organisationName"]').should('have.value', 'Potato Corp.')
      .get('input[name="taxpayerId"]').should('have.value', '12345678200')
      .get('input[name="invoiceContactEmail"]').should('have.value', 'bob-invoice@example.com')

      .get('#addressLineFull').should('have.value', 'Level 6, 341 George St, 2000, Sydney, NSW')
      .get('#addressLineFull').clear().type('343 George street')
      .get('.form-message').should('contain', 'Select a value from the list below')

      .get('#addressLineFull').clear().type('somethingyoucantcomplete')
      .get('.form-message').should('contain', 'Oops, we couldn\'t find your address.')

      .get('.OpenFullAddressForm').click()
      .get('#addressLine1').should('have.value', 'Level 6')
      .get('#addressLine2').should('have.value', '341 George St')
      .get('#city').should('have.value', 'Sydney')
      .get('#state').should('have.text', 'New South Wales')
      .get('#postcode').should('have.value', '2000')
      .get('#country').should('have.text', 'Australia')

      // State as input field when US
      .get('#react-select-country-input').selectAkOption({ value: 'United States of America', autocomplete: true })
      .get('#react-select-state-input').should('be', 'input')

      // State as input field when BE
      .get('#react-select-country-input').selectAkOption({ value: 'Belgium', autocomplete: true })
      .get('#react-select-state-input').should('be', 'input')

      // Fill full form with AU address
      .get('#react-select-country-input').selectAkOption({ value: 'Australia', autocomplete: true })
      .get('#react-select-state-input').selectAkOption({ value: 'New South Wales', autocomplete: true })
      .get('#postcode').type('2000')
      .get('#city').type('Sydney')
      .get('#addressLine1').type('Level 6')
      .get('#addressLine2').type('341 George St')
  ));

  it('Billing details page validation', () => {
    const checkValidation = (asError = true) => cy.get('.paymentDetailsSubmit')
      .click()
      .then(() => {
        if (asError) {
          return cy.notification('Aww shoot, you\'re missing billing details', 'error');
        }
        return cy.notification('Billing details updated', 'success');
      })
      .closeNotification();

    cy.fixture('billing/billing-details-get')
      .patch({
        organisationName: undefined,
      })
      .as('mock-billing-details')

      .server()
      .route('**/api/billing-ux/**/billing-details', '@mock-billing-details')
      .as('fetch-billing-details')

      .visit(URL)
      .wait('@fetch-billing-details')
      .closeNotification()

      .then(() => checkValidation())

      .get('input[name="organisationName"]')
      .type('Potato Corp.')

      .then(() => checkValidation(false));
  });

  describe('Check current payment method panel', () => {
    const mockAndVisit = (details) => {
      cy.fixture('billing/billing-details-get')
        .patch(details)
        .as('mock-billing-details');

      cy.server()
        .route('**/api/billing-ux/**/billing-details', '@mock-billing-details')
        .as('fetch-billing-details');

      cy.visit(URL)
        .wait('@fetch-billing-details');
    };

    it('Should have credit card details', () => {
      mockAndVisit({
        paymentMethod: PAYMENT_METHOD_CREDIT_CARD,
        creditCard: {
          suffix: '1111',
          expiryMonth: 1,
          expiryYear: 2022,
          type: 'VISA',
          name: 'Superplasma Team'
        },
      });
      cy.get('div[data-test="credit-card"]').within(() => {
        cy.get('.number').should('contain', '1111');
        cy.get('.name').should('have.text', 'Superplasma Team');
        cy.get('.expiry').should('contain', '01/2022');
      });
    });

    it('Should have paypal details', () => {
      mockAndVisit({
        paymentMethod: 'PAYPAL_ACCOUNT',
        paypalAccount: {
          email: 'bob-paypal@example.com'
        }
      });
      cy.get('div[data-test="paypal"]').within(() => {
        cy.get('.email').should('have.text', 'bob-paypal@example.com');
      });
    });
  });
});
