/* global cy Cypress */
/* eslint newline-per-chained-call: 0 */

const URL = `${Cypress.env('billingUrl')}/paymentdetails`;
const address = {
  organisationName: 'Potato Corp.',
  taxpayerId: '12345678200',
  addressLineFull: '343 George street',
  postcode: '2000',
  city: 'Sydney',
  addressLine1: 'Level 6',
  addressLine2: '341 George St',
  country: 'Australia',
  state: 'New South Wales'
};

describe('Jorney: setup credit card as payment method', () => {
  context('Happy path', () => {
    before(() => {
      cy.fixture('billing/billing-details-get')
        .patch({
          creditCard: {},
          organisationName: '',
          addressLine1: '',
          addressLine2: '',
          city: '',
          postcode: '',
          state: '',
          country: '',
          taxpayerId: ''
        })
        .as('mock-billing-details');

      cy.server()
        .route('**/api/billing-ux/**/billing-details', '@mock-billing-details')
        .as('get-billing-details');

      cy.mockPayPalButton();
      cy.visit(URL);
    });

    it('should render payment details page', () => {
      cy.get('[data-logroot="paymentDetails"] .page-content');
    });

    it('should render add payment details prompt', () => {
      cy.get('div[data-test=payment-details-prompt]').within(() => {
        cy.get('a').should('have.attr', 'href').and('include', '/paymentdetails/add');
      });
    });

    it('should open add payment details focused task', () => {
      cy.get('div[data-test=payment-details-prompt] a').click();
      cy.url().should('contain', '/add');
    });

    it('should fill billing address', () => {
      cy.get('[data-test="stepBillingAddress"]').should('exist');
      cy.fillShortBillingAddress(address);
      cy.get('[data-test="stepBillingAddress"] button[type=submit]').click();
    });

    it('should have paypal button', () => {
      cy.get('[data-test="stepBillingMethod"]').should('exist');
      cy.get('#paypal-button').should('exist');
      cy.get('#paypal-button button').click();
    });

    it('should authorize paypal', () => {
      cy.get('p[data-test="paypal-status-email"]').should('have.text', 'user@test.org');
      cy.get('[data-test="stepBillingMethod"] button[type=submit]').click();
    });

    it('should have confirmation data', () => {
      cy.get('[data-test="stepOverview"]').within(() => {
        cy.get('[data-test="address"]')
          .should('have.text', 'Potato Corp., 341 George Street, 2000, Sydney, NSW, AU');

        cy.get('[data-test="payment-method"]').within(() => {
          cy.get('[data-test="paypal"]').should('exist')
            .get('.email').should('contain', 'user@test.org');
        });

        cy.get('button[type=submit]').should('be.disabled');

        cy.get('input[type="checkbox"]').check();
      });
    });

    it('should send a PUT request', () => {
      cy.server();
      cy.route('PUT', '**/billing-ux/**/billing/billing-details').as('putChanges');
      cy.get('[data-test="stepOverview"] button[type=submit]').click();
      cy.wait('@putChanges').its('request.body').should('be.deep.equal', {
        addressLine1: '341 George Street',
        addressLine2: '',
        city: 'Sydney',
        country: 'AU',
        firstName: '-',
        organisationName: 'Potato Corp.',
        postcode: '2000',
        state: 'NSW',
        taxpayerId: '12345678200',
        paymentMethod: 'PAYPAL_ACCOUNT',
        paypalAccount: {
          email: 'user@test.org',
          nonce: '123456'
        }
      });
    });
  });
});
