/* global cy Cypress */
/* eslint newline-per-chained-call: 0 */

const URL = `${Cypress.env('billingUrl')}/paymentdetails`;
const creditCard = {
  number: '4444333322221111',
  name: 'Charlie',
  expiryMonth: '05',
  expiryYear: new Date().getFullYear() + 1,
  securityCode: '100'
};

const openFocusedTask = () => (
  cy.get('.widgetEditLink').click()
    .get('.focusedTaskContent').should('exist')
    .url().should('contain', `${URL}/update`)
);

const assertPutRequest = () => {
  cy.server();
  cy.route('PUT', '**/billing-ux/**/billing/payment-method').as('putChanges');
  cy.get('.button-submit').click();
  cy.wait('@putChanges').its('request.body').should('be.deep.equal', {
    paymentMethod: 'TOKENIZED_CREDIT_CARD',
    creditCard: {
      expiryMonth: '5',
      expiryYear: '2022',
      maskedCardNumber: '111111xxxxxx1111',
      name: 'Charlie',
      securityCode: 'xxx',
      sessionId: 'SESSION000000000000000000000001',
      token: '5123450000002346',
      type: 'VISA'
    }
  });
};

describe('Jorney: change payment method to credit card', () => {
  before(() => {
    cy.mockPayPalButton();
    cy.visit(URL);
  });

  context('Replace credit card', () => {
    it('should render payment details page', () => {
      cy.get('[data-logroot="paymentDetails"] .page-content');
    });
    it('should open edit on focused task', () => {
      openFocusedTask();
    });
    it('should have paypal button', () => {
      cy.get('#paypal-button').should('exist');
    });
    it('should validate missing cardholder name', () => {
      cy.fillCreditCard({ ...creditCard, name: '' });
      cy.get('.button-submit').click();

      cy.get('#creditcard-name-error').should('have.text', 'Please enter a valid cardholder name');
    });
    it('should save credit card details', () => {
      cy.get('input#creditcard-name').type('Charlie');
    });
    it('should send a PUT request', () => {
      assertPutRequest();
    });
    it('should close focused task', () => {
      cy.get('[data-logroot="paymentDetails"] .page-content')
        .url().should('not.contain', '/update');
    });
    it('should see success notification', () => {
      cy.notification('Payment method updated', 'success');
    });
  });

  context('From paypal to credit card', () => {
    before(() => {
      cy.fixture('billing/billing-details-get')
        .patch({
          paymentMethod: 'PAYPAL_ACCOUNT',
          creditCard: {},
          paypalAccount: {
            email: 'user@test.org'
          }
        })
        .as('mock-billing-details');

      cy.server()
        .route('**/api/billing-ux/**/billing-details', '@mock-billing-details')
        .as('get-billing-details');

      cy.visit(URL);
    });

    it('should render payment details page', () => {
      cy.get('[data-logroot="paymentDetails"] .page-content');
    });
    it('should open edit on focused task', () => {
      openFocusedTask();
    });
    it('should not have paypal button', () => {
      cy.get('#paypal-button').should('not.exist');
    });
    it('should have current paypal account details', () => {
      cy.get('[data-test="paypal-status-email"]').should('have.text', 'user@test.org');
    });
    it('should validate missing cardholder name', () => {
      cy.fillCreditCard({ ...creditCard, name: '' });
      cy.get('.button-submit').click();

      cy.get('#creditcard-name-error').should('have.text', 'Please enter a valid cardholder name');
    });
    it('should save credit card details', () => {
      cy.get('input#creditcard-name').type('Charlie');
    });
    it('should send a PUT request', () => {
      assertPutRequest();
    });
    it('should close focused task', () => {
      cy.get('[data-logroot="paymentDetails"] .page-content')
        .url().should('not.contain', '/update');
    });
    it('should see success notification', () => {
      cy.notification('Payment method updated', 'success');
    });
  });

  context('Fail to get TNS session', () => {
    it('should display credit card error message', () => {
      cy.server()
        .route('POST', '**/api/credit-card/session')
        .asError()
        .as('fetch-credit-card-session');

      cy.visit(`${URL}/update`)
        .wait('@fetch-credit-card-session');

      cy.get('.creditCardErrorActions').should('exist');
    });
  });

  context('Fail to save credit card details', () => {
    it('should display notification transaction could not be processed', () => {
      cy.server()
        .route('POST', '**/api/credit-card/session/SESSION000000000000000000000001/save')
        .asError()
        .as('save-credit-card-session')
        .route('POST', '**/api/billing-ux/api/credit-card/session')
        .as('tns');

      cy.visit(`${URL}/update`);
      cy.wait('@tns');

      cy.fillCreditCard(creditCard);
      cy.get('.button-submit').click();

      cy.wait('@save-credit-card-session');

      cy.closeFocusedTask();

      cy.notification('Transaction could not be processed', 'error');
    });
  });

  context('Focused task flow', () => {
    it('should open focused task and close by button click', () => {
      openFocusedTask();
      cy.closeFocusedTask()
        .get('.focusedTaskContent').should('not.exist')
        .url().should('not.contain', `${URL}/update`);
    });
    it('should open focused task and close by back navigation', () => {
      openFocusedTask();
      cy.go('back')
        .get('.focusedTaskContent').should('not.exist')
        .url().should('not.contain', `${URL}/update`);
    });
    it('should open focused task by accessing the update url', () => {
      cy.fixture('billing/billing-details-get')
        .as('mock-billing-details');

      cy.server()
        .route('**/api/billing-ux/**/billing-details', '@mock-billing-details')
        .as('get-billing-details');

      cy.visit(`${URL}/update`)
        .get('.focusedTaskContent').should('exist')
        .url().should('contain', `${URL}/update`);
      cy.closeFocusedTask();
    });
  });

  describe('Fail to access /update if it is not allowed', () => {
    it('should redirect to /paymentdetails if there is no payment details to update', () => {
      cy.fixture('billing/billing-details-get')
        .patch({ creditCard: {} })
        .as('mock-billing-details');

      cy.server()
        .route('**/api/billing-ux/**/billing-details', '@mock-billing-details')
        .as('get-billing-details');

      cy.visit(`${URL}/update`);
      cy.wait('@get-billing-details');
      cy.url().should('not.contain', '/update');
    });
  });
});
