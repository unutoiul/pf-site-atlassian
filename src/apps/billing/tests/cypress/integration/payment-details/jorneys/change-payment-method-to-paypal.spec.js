/* global cy Cypress */
/* eslint newline-per-chained-call: 0 */

const URL = `${Cypress.env('billingUrl')}/paymentdetails`;

const openFocusedTask = () => (
  cy.get('.widgetEditLink').click()
    .get('.focusedTaskContent').should('exist')
    .url().should('contain', `${URL}/update`)
);

describe('Jorney: change payment method to paypal', () => {
  before(() => {
    cy.mockPayPalButton();
    cy.visit(URL);
  });

  context('Happy path', () => {
    it('should render payment details page', () => {
      cy.get('[data-logroot="paymentDetails"] .page-content');
    });
    it('should open edit on focused task', () => {
      openFocusedTask();
    });
    it('should have paypal button', () => {
      cy.get('#paypal-button').should('exist');
      cy.get('#paypal-button button').click();
    });
    it('should authorize paypal', () => {
      cy.get('p[data-test="paypal-status-email"]').should('have.text', 'user@test.org');
    });
    it('should send a PUT request', () => {
      cy.server();
      cy.route('PUT', '**/billing-ux/**/billing/payment-method').as('putChanges');
      cy.get('.button-submit').click();
      cy.wait('@putChanges').its('request.body').should('be.deep.equal', {
        paymentMethod: 'PAYPAL_ACCOUNT',
        paypalAccount: {
          email: 'user@test.org',
          nonce: '123456'
        }
      });
    });
    it('should close focused task', () => {
      cy.get('[data-logroot="paymentDetails"] .page-content')
        .url().should('not.contain', '/update');
    });
    it('should see success notification', () => {
      cy.notification('Payment method updated', 'success');
    });
  });
});
