/* global cy Cypress */
/* eslint newline-per-chained-call: 0 */

const URL = `${Cypress.env('billingUrl')}/paymentdetails`;
const address = {
  organisationName: 'Potato Corp.',
  taxpayerId: '12345678200',
  addressLineFull: '341 George street',
  postcode: '2000',
  city: 'Sydney',
  addressLine1: 'Level 6',
  addressLine2: '341 George St',
  country: 'Australia',
  state: 'New South Wales'
};
const creditCard = {
  number: '4444333322221111',
  name: 'Charlie',
  expiryMonth: '05',
  expiryYear: new Date().getFullYear() + 1,
  securityCode: '100'
};

describe('Jorney: setup credit card as payment method', () => {
  context('Happy path', () => {
    before(() => {
      cy.fixture('billing/billing-details-get')
        .patch({
          creditCard: {},
          organisationName: '',
          addressLine1: '',
          addressLine2: '',
          city: '',
          postcode: '',
          state: '',
          country: '',
          taxpayerId: ''
        })
        .as('mock-billing-details');

      cy.server()
        .route('**/api/billing-ux/**/billing-details', '@mock-billing-details')
        .as('get-billing-details');

      cy.mockPayPalButton();
      cy.visit(URL);
    });

    it('should render payment details page', () => {
      cy.get('[data-logroot="paymentDetails"] .page-content');
    });

    it('should render add payment details prompt', () => {
      cy.get('div[data-test=payment-details-prompt]').within(() => {
        cy.get('a').should('have.attr', 'href').and('include', '/paymentdetails/add');
      });
    });

    it('should open add payment details focused task', () => {
      cy.get('div[data-test=payment-details-prompt] a').click();
      cy.url().should('contain', '/add');
    });

    it('should fill full billing address', () => {
      cy.get('[data-test="stepBillingAddress"]').should('exist');
      cy.fillShortBillingAddress(address);
      cy.get('[data-test="stepBillingAddress"] button[type=submit]').click();
    });

    it('should refill address if go back', () => {
      cy.get('[data-test="stepBillingMethod"]').should('exist')
        .get('[data-test="stepBillingMethod"] button.back-flow[type=button]').click()
        .get('[data-test="stepBillingAddress"]').should('exist');

      cy.get('input[name="organisationName"]').should('have.value', 'Potato Corp.')
        .get('input[name="taxpayerId"]').should('have.value', '12345678200')
        .get('#addressLineFull').should('have.value', '341 George Street, 2000, Sydney, NSW');

      cy.get('[data-test="stepBillingAddress"] button[type=submit]').click();
    });

    it('should have paypal button', () => {
      cy.get('[data-test="stepBillingMethod"]').should('exist');
      cy.get('#paypal-button').should('exist');
    });

    it('should fill credit card details', () => {
      cy.fillCreditCard(creditCard);
      cy.get('[data-test="stepBillingMethod"] button[type=submit]').click();
    });

    it('should have confirmation data', () => {
      cy.get('[data-test="stepOverview"]').within(() => {
        cy.get('[data-test="address"]')
          .should('have.text', 'Potato Corp., 341 George Street, 2000, Sydney, NSW, AU');

        cy.get('[data-test="payment-method"]').within(() => {
          cy.get('[data-test="credit-card"]').should('exist')
            .get('.number').should('contain', '1111')
            .get('.name').should('have.text', 'Charlie')
            .get('.expiry').should('contain', '05/2022');
        });

        cy.checkTenantInfo(name);
        cy.checkPeriodInfo('Oct 17, 2016 - Nov 16, 2022');

        // Bill estimate
        cy.get('.product-list').should('exist').within(() => {
          cy.get('.product-line').should('have.length', 9);
          // Bitbucket
          cy.get('.product-line:eq(0)').within(() => {
            cy.get('.product-description').should('have.text', 'Bitbucket');
            cy.get('.product-price').currency({ amount: 3720 });
            cy.get('.product-usage-list').should('have.text', '742 users, 30 gigabytes, 50 build minutes');
          });
          // Identity Manager (Cloud)
          cy.get('.product-line:eq(2)').within(() => {
            cy.get('.product-description').should('have.text', 'Identity Manager (Cloud)');
            cy.get('.product-price').should('have.text', 'Inactive');
            cy.get('.product-usage-list').should('have.text', 'Unlimited');
          });
          // JIRA Core (Cloud)
          cy.get('.product-line:eq(3)').within(() => {
            cy.get('.product-description').should('have.text', 'JIRA Core (Cloud)');
            cy.get('.product-price').should('have.text', 'Free');
            cy.get('.product-usage-list').should('have.text', '1 user');
          });
        });
        cy.get('.summary').within(() => {
          cy.get('.product-line-tax-total .tax-total').should('not.exist');
          cy.get('.product-line-total .total-date').should('have.text', 'Total');
          cy.get('.product-line-total .total-cost').currency({ amount: 11390, includeLabel: true });
        });

        cy.get('.test-charge-advise')
          .should('contain', 'Oct 6, 2022')
          .should('contain', '11,390');

        cy.get('button[type=submit]').should('be.disabled');

        cy.get('input[type="checkbox"]').check();
      });
    });
    it('should send a PUT request', () => {
      cy.server();
      cy.route('PUT', '**/billing-ux/**/billing/billing-details').as('putChanges');
      cy.get('[data-test="stepOverview"] button[type=submit]').click();
      cy.wait('@putChanges').its('request.body').should('be.deep.equal', {
        addressLine1: '341 George Street',
        addressLine2: '',
        city: 'Sydney',
        country: 'AU',
        firstName: '-',
        organisationName: 'Potato Corp.',
        postcode: '2000',
        state: 'NSW',
        taxpayerId: '12345678200',
        paymentMethod: 'TOKENIZED_CREDIT_CARD',
        creditCard: {
          expiryMonth: '5',
          expiryYear: '2022',
          maskedCardNumber: '111111xxxxxx1111',
          name: 'Charlie',
          securityCode: 'xxx',
          sessionId: 'SESSION000000000000000000000001',
          token: '5123450000002346',
          type: 'VISA'
        }
      });
    });
  });

  context('Fail to advance without address', () => {
    it('should validate single address form', () => {
      cy.fixture('billing/billing-details-get')
        .patch({
          addressLine1: '',
          addressLine2: '',
          city: '',
          postcode: '',
          state: '',
          country: '',
          creditCard: {}
        })
        .as('mock-billing-details');

      cy.server()
        .route('**/api/billing-ux/**/billing-details', '@mock-billing-details')
        .as('get-billing-details');

      cy.visit(`${URL}/add`);
      cy.wait('@get-billing-details');

      cy.get('[data-test="stepBillingAddress"] button[type=submit]').click();
      cy.notification('Aww shoot, you\'re missing billing address', 'error');
    });

    it('should validate full address form', () => {
      cy.fixture('billing/billing-details-get')
        .patch({
          addressLine1: '',
          addressLine2: '',
          city: '',
          postcode: '',
          state: '',
          country: '',
          creditCard: {}
        })
        .as('mock-billing-details');

      cy.server()
        .route('**/api/billing-ux/**/billing-details', '@mock-billing-details')
        .as('get-billing-details');

      cy.visit(`${URL}/add`);
      cy.wait('@get-billing-details');

      cy.get('#addressLineFull').clear().type('somethingyoucantcomplete');
      cy.get('.OpenFullAddressForm').click();

      cy.get('[data-test="stepBillingAddress"] button[type=submit]').click();
      cy.notification('Aww shoot, you\'re missing billing address', 'error');
    });
  });

  context('Fail to get TNS session', () => {
    it('should display credit card error message', () => {
      cy.server()
        .route('POST', '**/api/credit-card/session')
        .asError()
        .as('fetch-credit-card-session');

      cy.fixture('billing/billing-details-get')
        .patch({
          creditCard: {}
        })
        .as('mock-billing-details');

      cy.route('**/api/billing-ux/**/billing-details', '@mock-billing-details')
        .as('get-billing-details');

      cy.visit(`${URL}/add`);

      cy.get('[data-test="stepBillingAddress"] button[type=submit]').click();

      cy.wait('@fetch-credit-card-session');
      cy.get('.creditCardErrorActions').should('exist');
    });
  });

  context('Fail to save credit card details', () => {
    it('should display notification transaction could not be processed', () => {
      cy.server()
        .route('POST', '**/api/credit-card/session/SESSION000000000000000000000001/save')
        .asError()
        .as('save-credit-card-session');

      cy.fixture('billing/billing-details-get')
        .patch({
          creditCard: {}
        })
        .as('mock-billing-details');

      cy.route('**/api/billing-ux/**/billing-details', '@mock-billing-details')
        .as('get-billing-details');

      cy.visit(`${URL}/add`);

      cy.get('[data-test="stepBillingAddress"] button[type=submit]').click();
      cy.fillCreditCard(creditCard);
      cy.get('[data-test="stepBillingMethod"] button[type=submit]').click();
      cy.get('[data-test="stepOverview"]').within(() => {
        cy.get('input[type="checkbox"]').check()
          .get('button[type=submit]').click()
          .wait('@save-credit-card-session')
          .get('button[type=submit]').should('not.be.disabled');
      });

      cy.notification('Transaction could not be processed', 'error');
    });
  });

  context('Fail to access /add if it is not allowed', () => {
    it('should redirect to /paymentdetails if the flag is off', () => {
      cy.visit(`${URL}/add`);
      cy.url().should('not.contain', `${URL}/add`);
    });
    it('should redirect to /paymentdetails if there is already payment details set', () => {
      cy.server()
        .route('**/api/billing-ux/**/billing-details')
        .as('get-billing-details');

      cy.visit(`${URL}/add`);
      cy.wait('@get-billing-details');
      cy.url().should('not.contain', '/add');
    });
  });
});
