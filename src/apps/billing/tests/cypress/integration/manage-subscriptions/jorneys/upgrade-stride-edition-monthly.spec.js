/* global cy Cypress */
/* eslint newline-per-chained-call: 0 */

const URL = `${Cypress.env('billingUrl')}/applications`;
const address = {
  organisationName: 'Potato Corp.',
  taxpayerId: '12345678200',
  addressLineFull: '341 George street',
  postcode: '2000',
  city: 'Sydney',
  addressLine1: '341 George Street',
  addressLine2: '',
  country: 'Australia',
  countryCode: 'AU',
  state: 'New South Wales',
  stateCode: 'NSW',
};
const creditCard = {
  number: '4444333322221111',
  name: 'Charlie',
  expiryMonth: '05',
  expiryYear: '2022',
  securityCode: '100'
};
const paypal = {
  nonce: '123456',
  email: 'user@test.org'
};
const estimate = {
  products: [
    { name: 'Stride (Cloud)', cost: { amount: 19 }, usage: '10 users' },
  ],
  total: { amount: 19, includeLabel: true },
  hasTax: false,
};

describe('Journey: upgrade Stride edition monthly', () => {
  const chooseToUpgrade = () => (cy.get('div[data-test="premium"] button').click());

  const doSubmitWithAgreement = () => {
    cy.get('button[type=submit]').should('be.disabled');
    cy.get('input[type="checkbox"]').check();
    cy.get('button[type=submit]').click();
  };

  const checkInfoText = () => (
    cy.get('.test-charge-advise')
      .should('contain', 'Stride Premium')
  );

  beforeEach(() => {
    cy.server()
      .featureFlags({
        'new-change-edition-flow': true,
      });
  });

  context('Happy path', () => {
    context('With payment details on file', () => {
      it('should use current payment details', () => {
        cy.fixture('billing/billing-details-get')
          .patch({
            addressLine1: '341 George Street',
            addressLine2: '',
            creditCard: {
              suffix: creditCard.number.slice(-4),
              type: 'VISA',
              ...creditCard
            },
          })
          .as('mock-billing-details');

        cy.server()
          .route('**/api/billing-ux/**/billing-details', '@mock-billing-details')
          .as('get-billing-details');

        cy.visit(URL);

        cy.isAtManageSubscriptionsPage();
        cy.isAtChangeEditionFocusedTask(URL);
        chooseToUpgrade();

        cy.get('[data-test="stepOverview"]').within(() => {
          cy.checkAddress(address);
          cy.checkCreditCardPaymentMethod(creditCard);
          let name = 'localhost';
          if (Cypress.env('ORGANIZATION')) {
            name = 'Acme';
          }
          cy.checkTenantInfo(name);
          cy.checkPeriodInfo('Oct 17, 2016 - Nov 16, 2022');
          cy.checkEstimate({ ...estimate, hasTax: true });
          checkInfoText();

          cy.get('.test-edit-payment-method').should('exist');

          cy.server();
          cy.route('PUT', '**/billing/entitlements/b7db7827-e2ac-492e-a3bb-e1c310764043/edition/premium')
            .as('putChangeEdition');

          cy.get('button[type=submit]').click();

          cy.wait('@putChangeEdition');
        });
        cy.closedFocusedTask();
        cy.notification('Plan successfully updated', 'success');
      });
    });

    context('With NO payment details on file', () => {
      beforeEach(() => {
        cy.fixture('billing/billing-details-get')
          .patch({
            creditCard: {},
            organisationName: '',
            addressLine1: '',
            addressLine2: '',
            city: '',
            postcode: '',
            state: '',
            country: '',
            taxpayerId: ''
          })
          .as('mock-billing-details');

        cy.server()
          .route('**/api/billing-ux/**/billing-details', '@mock-billing-details')
          .as('get-billing-details')
          .route('POST', '**/api/billing-ux/api/credit-card/session')
          .as('tns');

        cy.mockPayPalButton();
        cy.visit(URL);
      });

      it('should input credit card', () => {
        cy.isAtManageSubscriptionsPage();
        cy.isAtChangeEditionFocusedTask(URL);
        chooseToUpgrade();

        cy.get('[data-test="stepBillingAddress"]').should('exist');
        cy.fillShortBillingAddress(address);
        cy.get('[data-test="stepBillingAddress"] button[type=submit]').click();

        cy.get('[data-test="stepBillingMethod"]').should('exist');
        cy.wait('@tns');
        cy.fillCreditCard(creditCard);
        cy.get('[data-test="stepBillingMethod"] button[type=submit]').click();

        cy.get('[data-test="stepOverview"]').within(() => {
          cy.checkAddress(address);
          cy.checkCreditCardPaymentMethod(creditCard);
          let name = 'localhost';
          if (Cypress.env('ORGANIZATION')) {
            name = 'Acme';
          }
          cy.checkTenantInfo(name);
          cy.checkPeriodInfo('Oct 17, 2016 - Nov 16, 2022');
          cy.checkEstimate(estimate);
          checkInfoText();

          cy.server();
          cy.route('PUT', '**/billing-ux/**/billing/billing-details').as('putChanges');
          cy.route('PUT', '**/billing/entitlements/b7db7827-e2ac-492e-a3bb-e1c310764043/edition/premium')
            .as('putChangeEdition');

          doSubmitWithAgreement();

          cy.wait('@putChanges').its('request.body').should('be.deep.equal', {
            addressLine1: address.addressLine1,
            addressLine2: address.addressLine2,
            city: address.city,
            country: address.countryCode,
            firstName: '-',
            organisationName: address.organisationName,
            postcode: address.postcode,
            state: address.stateCode,
            taxpayerId: address.taxpayerId,
            paymentMethod: 'TOKENIZED_CREDIT_CARD',
            // credit card will always post the same
            creditCard: {
              expiryMonth: '5',
              expiryYear: '2022',
              maskedCardNumber: '111111xxxxxx1111',
              name: 'Charlie',
              securityCode: 'xxx',
              sessionId: 'SESSION000000000000000000000001',
              token: '5123450000002346',
              type: 'VISA'
            }
          });
          cy.wait('@putChangeEdition');
        });

        cy.closedFocusedTask();
        cy.notification('Plan successfully updated', 'success');
      });

      it('should input paypal', () => {
        cy.isAtManageSubscriptionsPage();
        cy.isAtChangeEditionFocusedTask(URL);
        chooseToUpgrade();

        cy.get('[data-test="stepBillingAddress"]').should('exist');
        cy.fillShortBillingAddress(address);
        cy.get('[data-test="stepBillingAddress"] button[type=submit]').click();

        cy.get('[data-test="stepBillingMethod"]').should('exist');
        cy.get('#paypal-button').should('exist');
        cy.get('#paypal-button button').click();
        cy.get('p[data-test="paypal-status-email"]').should('have.text', paypal.email);
        cy.get('[data-test="stepBillingMethod"] button[type=submit]').click();

        cy.get('[data-test="stepOverview"]').within(() => {
          cy.checkAddress(address);

          cy.get('[data-test="payment-method"]').within(() => {
            cy.get('[data-test="paypal"]').should('exist')
              .get('.email').should('contain', paypal.email);
          });

          let name = 'localhost';
          if (Cypress.env('ORGANIZATION')) {
            name = 'Acme';
          }
          cy.checkTenantInfo(name);
          cy.checkPeriodInfo('Oct 17, 2016 - Nov 16, 2022');
          cy.checkEstimate(estimate);
          checkInfoText();

          cy.server();
          cy.route('PUT', '**/billing-ux/**/billing/billing-details').as('putChanges');
          cy.route('PUT', '**/billing/entitlements/b7db7827-e2ac-492e-a3bb-e1c310764043/edition/premium')
            .as('putChangeEdition');

          doSubmitWithAgreement();

          cy.wait('@putChanges').its('request.body').should('be.deep.equal', {
            addressLine1: address.addressLine1,
            addressLine2: address.addressLine2,
            city: address.city,
            country: address.countryCode,
            firstName: '-',
            organisationName: address.organisationName,
            postcode: address.postcode,
            state: address.stateCode,
            taxpayerId: address.taxpayerId,
            paymentMethod: 'PAYPAL_ACCOUNT',
            paypalAccount: paypal
          });

          cy.wait('@putChangeEdition');
        });

        cy.closedFocusedTask();
        cy.notification('Plan successfully updated', 'success');
      });
    });
  });

  context('Fail to load estimate', () => {
    it('should display error message', () => {
      cy.server()
        .route('GET', '**/api/billing-ux/**/billing/entitlements/**/edition/premium/estimate')
        .asError()
        .as('fetch-estimate');

      cy.visit(URL);

      cy.isAtManageSubscriptionsPage();
      cy.isAtChangeEditionFocusedTask(URL);
      chooseToUpgrade();

      // should display load error
      cy.get('[data-test="step-unavailable-error"]').should('exist');

      cy.fixture('billing/entitlements/b7db7827-e2ac-492e-a3bb-e1c310764043/edition/premium/estimate-get')
        .as('mock-entitlement-estimate');

      cy.server()
        .route(
          'GET',
          '**/api/billing-ux/**/billing/entitlements/**/edition/premium/estimate',
          '@mock-entitlement-estimate'
        ).as('fetch-estimate');

      // should retry and recover
      cy.get('[data-test="step-unavailable-error"]').within(() => {
        cy.get('button').click();
      });

      cy.wait('@fetch-estimate');

      cy.get('[data-test="step-unavailable-error"]').should('not.exist');
    });
  });

  context('Old experience', () => {
    it('should upgrade paying with current payment method', () => {
      cy.server()
        .featureFlags({
          'new-change-edition-flow': false
        });

      cy.visit(URL);

      cy.isAtManageSubscriptionsPage();

      // should open focused task
      cy.get('div[data-logroot="hipchat.cloud"] button.mainActionButton').click()
        .get('.focusedTaskContent').should('exist');

      chooseToUpgrade();

      cy.get('.focusedTaskContent').should('not.exist');
      cy.notification('Plan successfully updated');
    });
  });
});
