/* global cy Cypress */
/* eslint newline-per-chained-call: 0 */

const URL = `${Cypress.env('billingUrl')}/applications`;

describe('Journey: upgrade Stride edition annual', () => {
  beforeEach(() => {
    cy.server()
      .featureFlags({
        'new-change-edition-flow': true
      });

    cy.fixture('billing/bill-estimate-get')
      .patch({
        nextBillingPeriod: {
          renewalFrequency: 'ANNUAL',
        },
      })
      .as('mock-bill-estimate');

    cy.server()
      .route('**/api/billing-ux/**/bill-estimate', '@mock-bill-estimate')
      .as('get-bill-estimate');
  });

  it('should use credit card', () => {
    cy.on('window:before:load', (win) => {
      cy.stub(win, 'open').as('windowOpen');
    });

    cy.visit(URL);

    cy.isAtManageSubscriptionsPage();
    cy.isAtChangeEditionFocusedTask(URL);

    // should choose upgrade path
    cy.get('div[data-test="premium"] button').click();

    // should display overview
    cy.get('[data-test="stepOverview"]').should('exist');
    cy.get('[data-test="payment-option-advise"]')
      .should('contain', 'You will be redirected to my.atlassian.com');
    cy.get('[data-test="stepOverview"] button[type=submit]').should('have.text', 'Add card information');

    // should redirect to MAC for payment
    cy.get('[data-test="stepOverview"] button[type=submit]').click();
    cy.get('@windowOpen').should(
      'be.calledWith',
      'https://my.atlassian.com/purchase/confirmpayment?orderId=55290767&currency=usd&confirm=20.90',
      '_top'
    );

    cy.closedFocusedTask();
  });

  it('should generate quote', () => {
    cy.on('window:before:load', (win) => {
      cy.stub(win, 'open').as('windowOpen');
    });

    cy.visit(URL);

    cy.isAtManageSubscriptionsPage();
    cy.isAtChangeEditionFocusedTask(URL);

    // should choose upgrade path
    cy.get('div[data-test="premium"] button').click();

    // should display overview
    cy.get('[data-test="stepOverview"]').should('exist');

    // should select to generate a quote
    cy.get('[data-test="option-QUOTE"] input').check({ force: true });
    cy.get('[data-test="stepOverview"] button[type=submit]').should('have.text', 'Download quote');
    cy.get('[data-test="payment-option-advise"]')
      .should('contain', 'A PDF of the quote will be downloaded for your');

    // should redirect to MAC for payment
    cy.get('[data-test="stepOverview"] button[type=submit]').click();
    cy.get('@windowOpen').should(
      'be.calledWith',
      'https://my.atlassian.com/billing/pdf?id=55290767&hash=5ae7e064c7fd9b6598e81bf7d4627e7a',
      '_top'
    );

    cy.closedFocusedTask();
  });

  context('Fail to create quote', () => {
    it('should display error notification', () => {
      cy.server()
        .route('POST', '**/api/billing-ux/**/billing/estimate/**/quote')
        .asError()
        .as('create-quote');

      cy.visit(URL);

      cy.isAtManageSubscriptionsPage();
      cy.isAtChangeEditionFocusedTask(URL);

      // should choose upgrade path
      cy.get('div[data-test="premium"] button').click();

      // should display overview
      cy.get('[data-test="stepOverview"]').should('exist');
      cy.get('[data-test="stepOverview"] button[type=submit]').click();
      cy.wait('@create-quote');
      cy.notification('Failed to create quote', 'error');
    });
  });

  context('Fail to load estimate', () => {
    it('should display error message', () => {
      cy.server()
        .route('GET', '**/api/billing-ux/**/billing/entitlements/**/edition/premium/estimate')
        .asError()
        .as('fetch-estimate');

      cy.visit(URL);

      cy.isAtManageSubscriptionsPage();
      cy.isAtChangeEditionFocusedTask(URL);

      // should choose upgrade path
      cy.get('div[data-test="premium"] button').click();
      cy.wait('@fetch-estimate');

      // should display load error
      cy.get('[data-test="step-unavailable-error"]').should('exist');

      // reset mock and retry
      cy.server()
        .route('GET', '**/api/billing-ux/**/billing/entitlements/**/edition/premium/estimate')
        .as('fetch-estimate');
      cy.get('[data-test="step-unavailable-error"] button').click();
      cy.get('[data-test="stepOverview"]').should('exist');
    });
  });
});
