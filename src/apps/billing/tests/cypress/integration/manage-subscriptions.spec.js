/* global cy Cypress */
const entitlementGroupData = require('../../../mock-server/bux/api/billing/entitlement-group-get.json');

const URL = `${Cypress.env('billingUrl')}/applications`;

describe('Manage subscriptions', () => {
  before(() => {
    cy.visit(URL);
  });

  it('Should render the page', () => {
    cy.get('[data-logroot="manageSubscriptions"] .page-content');
  });

  it('Should render products list', () => {
    cy.get('.subscription-line').should('have.length', 8);
  });


  describe('subscriptions', () => {
    it('Should render BitBucket usage', () => {
      cy.get('[data-logroot="bitbucket.ondemand"]').within(() => {
        cy.get('.product-usage').should('have.text', [
          'users742 users',
          'gigabytes30 gigabytes',
          'build minutes50 build minutes'
        ].join(''));
      });
    });

    it('Should render Confluence usage', () => {
      cy.get('[data-logroot="confluence.ondemand"]').within(() => {
        let name = 'Tesla';
        if (Cypress.env('ORGANIZATION')) {
          name = 'Acme';
        }
        cy.get('.product-usage').should('have.text', 'users800 of 820 users');
        cy.get('[data-test="tier-breach-inline-message"]')
          .click()
          .contains(`${name} Confluence has only 20 users left.`);
      });
    });
  });

  it('Should render bill estimate', () => {
    cy.get('.bill-estimate-summary').should('exist');
    cy.get('.product-line').should('have.length', 3);

    cy.get('.expand-bill').click();
    cy.get('.product-line').should('have.length', 9);
    cy.get('.collapse-bill').click({ force: true });
    cy.get('.product-line').should('have.length', 3);
  });

  it('Should render add more applications if not organization', () => {
    if (Cypress.env('ORGANIZATION')) {
      cy.get('.test-add-more-applications').should('not.exist');
    } else {
      cy.get('.test-add-more-applications').should('exist');
    }
  });

  describe('XSell Experiment', () => {
    beforeEach(() => {
      cy.server()
        .featureFlags({
          'growth.manage.subscriptions.xsell.experiment': 'variation'
        })
        .fixture('billing/entitlement-group-get')
        .patch({
          entitlements: [entitlementGroupData.entitlements[0]]
        })
        .as('mock-entitlement-group')
        .route('**/api/billing-ux/**/entitlement-group', '@mock-entitlement-group');
    });

    it('should render the experiment', () => {
      cy.visit(URL);
      if (Cypress.env('ORGANIZATION')) {
        cy.get('.x-sell-product-card').should('not.exist');
      } else {
        cy.get('.x-sell-product-card').should('exist');
      }
    });

    it('should be able to trigger xflow', () => {
      if (Cypress.env('ORGANIZATION')) { return; }
      cy.get('.submit').should('exist');
      cy.get('.submit').click();
      cy.get('#xflow-confirm-trial-confirm-button').should('exist');
      cy.get('#xflow-confirm-trial-cancel-button').should('exist');
      cy.get('#xflow-confirm-trial-cancel-button').click();
    });

    it('should be able to go to discover applications', () => {
      if (Cypress.env('ORGANIZATION')) { return; }
      cy.get('.discover-applications-button button').should('exist');
      cy.get('.discover-applications-button button').click();
      cy.get('[data-test=\'products-group:atlassian\']').should('exist');
    });
  });
});
