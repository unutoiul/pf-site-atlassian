/* global cy Cypress */
const URL = `${Cypress.env('billingUrl')}/overview`;
const ORG_URL = `${Cypress.env('organizationUrl')}/overview`;

describe('Bill Overview', () => {
  context('widgets', () => {
    before(() => {
      cy.visit(URL);
    });

    it('Should render the page', () => {
      cy.get('[data-logroot="billOverview"] .page-content');
    });

    context('Protip about future price changes', () => {
      it('Should render a notice protip if the feature flag is on', () => {
        cy.get('div[data-test=notices-row]')
          .within(() => {
            cy.get('a')
              .should('have.attr', 'href')
              .and('include', 'https://www.atlassian.com/licensing/future-pricing');
          });
      });

      it('Should not render a notice protip if the feature flag is off', () => {
        cy.server()
          .featureFlags({
            '2018-price-update-notice': false
          });

        cy.visit(URL)
          .wait('@mock-metadata-feature-flag');

        cy.get('div[data-test=notices-row]')
          .should('not.exist');
      });
    });

    context('Protip about recent price changes', () => {
      it('Should render a notice protip if the feature flag is on', () => {
        cy.server()
          .featureFlags({
            '2018-price-changed-info': true
          });

        cy.visit(URL)
          .wait('@mock-metadata-feature-flag');

        cy.get('div[data-test=post-update-notices-row]')
          .within(() => {
            cy.get('a')
              .should('have.attr', 'href')
              .and('include', 'https://www.atlassian.com/licensing/future-pricing');
          });
      });

      it('Should not render a notice protip if the feature flag is off', () => {
        cy.server()
          .featureFlags({
            '2018-price-changed-info': false
          });

        cy.visit(URL)
          .wait('@mock-metadata-feature-flag');

        cy.get('div[data-test=post-update-notices-row]')
          .should('not.exist');
      });
    });

    context('Protip about upcoming currency change info', () => {
      it('Should render a notice protip if the feature flag is on', () => {
        cy.server()
          .featureFlags({
            '2018-upcoming-currency-notice': true
          });

        cy.visit(URL)
          .wait('@mock-metadata-feature-flag');

        cy.get('div[data-test=currency-update-notices-row]')
          .should('exist');
      });

      it('Should not render a notice protip if the feature flag is off', () => {
        cy.server()
          .featureFlags({
            '2018-upcoming-currency-notice': false
          });

        cy.visit(URL)
          .wait('@mock-metadata-feature-flag');

        cy.get('div[data-test=currency-update-notices-row]')
          .should('not.exist');
      });

      it('Should render the right message for Currency change', () => {
        cy.server()
          .featureFlags({
            '2018-upcoming-currency-notice': true
          });

        cy.visit(URL)
          .wait('@mock-metadata-feature-flag');

        cy.get('div[data-test=currency-update-notices-row]')
          .contains('Your bill on Jan 1, 2025 will be in Japanese Yen.');
      });
    });

    context('Billing Estimate widget', () => {
      it('should render widget', () =>
        cy.get('.bill-estimate-widget'));
      it('has correct title', () =>
        cy.get('.bill-estimate-widget > .widget-title')
          .should('have.text', 'Bill estimate'));
      it('has correct price', () =>
        cy.get('.bill-estimate-widget .price').currency({ amount: 12529, includeLabel: true }));
      it('has correct date', () =>
        cy.get('.bill-estimate-widget .date')
          .should('have.text', 'Next Charge: Oct 6, 2022'));
      it('has correct link href', () =>
        cy.get('.bill-estimate-widget .link')
          .should('have.attr', 'href')
          .and('include', '/estimate'));
    });

    it('Should contain billing details widget credit card', () => {
      const ccNumber = '\u2022\u2022\u2022\u2022 \u2022\u2022\u2022\u2022 \u2022\u2022\u2022\u2022 1111';
      cy.get('.payment-detail-widget')
        .within(() => {
          cy.get('.widget-title')
            .should('have.text', 'Billing details')
            .get('.number')
            .should('have.text', ccNumber)
            .get('.expiry')
            .should('have.text', 'Expires 01/2022')
            .get('.name')
            .should('have.text', 'Superplasma Team')
            .get('.widgetEditLink')
            .should('have.attr', 'href')
            .and('include', '/paymentdetails');
        });
    });

    it('Should contain bill history widget', () => {
      cy.get('.bill-history-widget')
        .within(() => {
          cy.get('.widget-title')
            .should('have.text', 'Bill history')
            .get('.invoice-line')
            .should('have.length', 3)
            .get('.invoice-line:first-child')
            .within(() => {
              cy.get('.bill-date').should('have.text', 'Apr 1, 2017')
                .get('.amount').currency({ amount: 10.5, withCents: true });
            });
        });
    });

    it('Should contain invoice contact widget', () => {
      const email = 'bob-invoice@example.com';

      cy.fixture('billing/billing-details-get')
        .patch({ email })
        .as('mock-billing-details');

      cy.server()
        .route('**/api/billing-ux/**/billing-details', '@mock-billing-details')
        .as('fetch-billing-details')
        .route('**/api/me', { account_id: '557057:26ead946-4955-46ef-a11e-b1f9ae987de7', email })
        .as('fetch-me')

        .visit(URL)
        .wait('@fetch-billing-details')
        .wait('@fetch-me')

        .get('.invoice-contact-widget')
        .within(() => {
          cy.get('.widget-title')
            .should('have.text', 'Billing contacts')
            .get('.email')
            .should('have.text', email)
            .get('.avatar')
            .should('contain', 'Supernova Team')
            .get('.myLink')
            .get('.widgetEditLink')
            .should('have.attr', 'href')
            .and('include', '/paymentdetails');
        });
    });

    it('Should contain billing details widget with paypal', () => {
      cy.fixture('billing/billing-details-get')
        .patch({
          paymentMethod: 'PAYPAL_ACCOUNT',
          paypalAccount: {
            email: 'bob-paypal@example.com'
          }
        })
        .as('mock-billing-details')

        .server()
        .route('**/api/billing-ux/**/billing-details', '@mock-billing-details')
        .as('fetch-billing-details')

        .visit(URL)
        .wait('@fetch-billing-details')

        .get('.payment-detail-widget')
        .within(() => {
          cy.get('.widget-title')
            .should('have.text', 'Billing details')
            .get('.email')
            .should('have.text', 'bob-paypal@example.com')
            .get('.widgetEditLink')
            .should('have.attr', 'href')
            .and('include', '/paymentdetails');
        });
    });
  });

  context('actions', () => {
    it('Should update billing contact', () => {
      const email = 'bob-invoice@example.com';

      cy.server()
        .route('**/api/me', { account_id: '557057:26ead946-4955-46ef-a11e-b1f9ae987de7', email })
        .as('fetch-me');

      cy.visit(URL)
        .wait('@fetch-me')

        .get('.invoice-contact-widget')
        .fixture('billing/billing-details-get')
        .patch({
          firstName: 'Bruce',
          lastName: 'Wayne',
          email
        })
        .as('mock-billing-details')

        // Mock after page loaded so we can reflect changes upon submit
        .server()
        .route('**/api/billing-ux/**/billing-details', '@mock-billing-details')
        .as('fetch-billing-details')
        .route('**/api/me', { account_id: '557057:26ead946-4955-46ef-a11e-b1f9ae987de7', email })
        .as('fetch-me')

        .get('.invoice-contact-widget')
        .within(() => {
          cy.get('.update-billing-contact')
            .click();
        })
        .get('.update-contact-warning-modal')
        .get('.button-submit')
        .click()
        .wait('@fetch-billing-details')
        .get('.myLink');
    });

    it('Should make user secondary and send him to manage contacts', () => {
      cy
        .on('window:before:load', (win) => {
          cy.stub(win, 'open').as('windowOpen');
        })
        .visit(URL)

        .get('.invoice-contact-widget')
        .within(() => {
          cy.get('.manage-contacts').click();
        })
        .get('@windowOpen')
        .should('be.calledWith', 'https://my.stg.internal.atlassian.com/', '_top');
    });

    it('Should pass orgId to links when set in the query params', () => {
      const email = 'bob-invoice@example.com';

      cy.fixture('billing/billing-details-get')
        .patch({ email })
        .as('mock-billing-details')

        .server()
        .route('**/api/billing-ux/**/billing-details', '@mock-billing-details')
        .as('fetch-billing-details')
        .route('**/api/me', { account_id: '557057:26ead946-4955-46ef-a11e-b1f9ae987de7', email })
        .as('fetch-me')

        .visit(`${ORG_URL}`)
        .wait('@fetch-billing-details')

        .get('.bill-estimate-widget')
        .within(() => {
          cy.get('.link')
            .should('have.attr', 'href')
            .and('include', `${Cypress.env('organizationUrl')}/estimate`);
        })
        .get('.payment-detail-widget')
        .within(() => {
          cy.get('.widgetEditLink')
            .should('have.attr', 'href')
            .and('include', `${Cypress.env('organizationUrl')}/paymentdetails`);
        })
        .get('.invoice-contact-widget')
        .within(() => {
          cy.get('.widgetEditLink')
            .should('have.attr', 'href')
            .and('include', `${Cypress.env('organizationUrl')}/paymentdetails`)
            .get('.myLink')
            .should('have.attr', 'href')
            .and('include', 'https://my.stg.internal.atlassian.com/');
        });
    });
  });

  context('error states', () => {
    it('Should get error on whole page is important source missing', () => {
      cy.server()
        .route('**/api/billing-ux/**/bill-estimate')
        .asError()
        .as('fetch-bill-estimate')

        .visit(URL)
        .wait('@fetch-bill-estimate')

        .get('.bill-estimate-widget')
        .should('not.exist')
        .get('.error.unavailable');
    });

    it('Should get error on whole page is important source missing', () => {
      cy.server()
        .route('**/api/billing-ux/**/invoice-history')
        .asError()
        .as('fetch-invoice-history')

        .visit(URL)
        .wait('@fetch-invoice-history')

        .get('.bill-history-widget')
        .within(() => {
          cy.get('.widget-error');
        });
    });
  });

  context('prompts', () => {
    it('Should display payment details prompt when they are not provided', () => {
      cy.fixture('billing/billing-details-get')
        .patch({ creditCard: {} })
        .as('mock-billing-details')

        .server()
        .route('**/api/billing-ux/**/billing-details', '@mock-billing-details')
        .as('fetch-billing-details')

        .visit(`${ORG_URL}`)
        .wait('@fetch-billing-details')

        .get('.paymentDetailsPrompt')
        .within(() => {
          cy.get('a')
            .should('have.attr', 'href')
            .and('include', `${Cypress.env('organizationUrl')}/paymentdetails`);
        });
    });

    it('Should display account-is-under-management prompt when it is', () => {
      cy.fixture('billing/billing-details-get')
        .patch({ managedByPartner: true })
        .as('mock-billing-details')

        .server()
        .route('**/api/billing-ux/**/billing-details', '@mock-billing-details')
        .as('fetch-billing-details')

        .visit(URL)
        .wait('@fetch-billing-details')

        .get('.account-under-management-prompt')
        .get('.page-content a')
        .contains('Bill History');
    });

    it('Should not display link to Bill History for account-is-under-management if history is empty', () => {
      cy
        .fixture('billing/billing-details-get')
        .patch({ managedByPartner: true })
        .as('mock-billing-details')

        .fixture('billing/invoice-history-get')
        .patch({ invoices: [] })
        .as('mock-invoice-history')

        .server()
        .route('**/api/billing-ux/**/billing-details', '@mock-billing-details')
        .as('fetch-billing-details')
        .route('**/api/billing-ux/**/invoice-history', '@mock-invoice-history')
        .as('fetch-invoice-history')

        .visit(URL)
        .wait('@fetch-billing-details')
        .wait('@fetch-invoice-history')

        .get('.account-under-management-prompt')
        .get('.page-content a')
        .should('not.exist');
    });
  });
});
