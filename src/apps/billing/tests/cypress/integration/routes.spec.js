/* global cy Cypress */

const URL = `${Cypress.env('billingUrl')}`;

const assertBreadcrumb = (hasParentBilling = true) => {
  let adminUrl = '/admin';
  if (Cypress.env('ORGANIZATION')) {
    adminUrl = '/';
  }
  cy.get('div[data-test=breadcrumb]').within(() => {
    cy.get('a').first()
      .should('have.text', 'Admin')
      .should('have.attr', 'href')
      .and('equal', adminUrl);

    if (Cypress.env('ORGANIZATION')) {
      cy.get('a').eq(1)
        .should('have.text', 'Acme')
        .should('have.attr', 'href')
        .and('contain', '/o/')
        .and('contain', '/overview');
    }

    if (hasParentBilling) {
      cy.get('a').last()
        .should('have.text', 'Billing')
        .should('have.attr', 'href')
        .and('contain', '/billing/overview');
    }
  });
};

const assert = {
  overview: (go) => {
    go(`${URL}/overview`);

    cy.get('[data-logroot="billOverview"] .page-content');
    cy.title().should('eq', 'Billing – Overview');
    assertBreadcrumb(false);
  },
  estimate: (go) => {
    go(`${URL}/estimate`);

    cy.get('[data-logroot="billEstimate"] .page-content');
    cy.title().should('eq', 'Billing – Bill estimate');
    assertBreadcrumb();
  },
  history: (go) => {
    go(`${URL}/history`);

    cy.get('[data-logroot="billHistory"] .page-content');
    cy.title().should('eq', 'Billing – Bill history');
    assertBreadcrumb();
  },
  paymentdetails: (go) => {
    go(`${URL}/paymentdetails`);

    cy.get('[data-logroot="paymentDetails"] .page-content');
    cy.title().should('eq', 'Billing – Update billing details');
    assertBreadcrumb();
  },
  application: (go) => {
    go(`${URL}/applications`);

    cy.get('[data-logroot="manageSubscriptions"] .page-content');
    cy.title().should('eq', 'Billing – Manage subscriptions');
    assertBreadcrumb();
  },
};

if (!Cypress.env('ORGANIZATION')) {
  assert.addapplication = (go) => {
    go(`${URL}/addapplication`);

    cy.get('[data-logroot="discoverNewProducts"] .page-content');
    cy.title().should('eq', 'Billing – Discover new products');
    assertBreadcrumb(false);
  };
}

const goLink = url => cy.get(`a[href="${url}"]`).first().click();

describe('All page routes', () => {
  it('should be able to navigate to all pages', () => {
    cy.visit(`${URL}/overview`);
    Object.values(assert).forEach(cb => cb(goLink));
  });

  Object.keys(assert)
    .forEach(key => (
      it(`should open ${key}`, () => assert[key](cy.visit))
    ));
});
