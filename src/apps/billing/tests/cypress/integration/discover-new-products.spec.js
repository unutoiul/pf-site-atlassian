/* global cy Cypress */
const URL = `${Cypress.env('billingUrl')}/addapplication`;
const productListSelector = 'ul[data-test="product-list"]';
const promotedProductSelector = 'div[data-test="promoted-product"]';

const checkProduct = ({
  productKey, name, tag, link, getStartedLabel, price, pricingLink, button
}) => {
  cy.get(`li[data-test="product:${productKey}"]`).within(() => {
    cy.get('div').first().should('have.attr', 'data-logroot', productKey);
    cy.get('div[data-test="product-title"]').should('contain', name);
    cy.get('div[data-test="product-tag"]').should('contain', tag);
    cy.get('a[data-test="learn-more-link"]').should('have.attr', 'href', link);
    cy.get('p[data-test="price"]').should('have.text', price);
    cy.get('p[data-test="get-started-label"]').should('have.text', getStartedLabel);
    if (pricingLink) {
      cy.get('a[data-test="full-price-link"]').should('have.attr', 'href', pricingLink);
    }
    cy.get('button').should('have.text', button);
  });
};

const checkExtraProduct = ({
  productKey, name, tag, link, button
}) => {
  cy.get(`li[data-test="product:${productKey}"]`).within(() => {
    cy.get('div').first().should('have.attr', 'data-logroot', productKey);
    cy.get('div[data-test="product-title"]').should('contain', name);
    cy.get('div[data-test="product-tag"]').should('contain', tag);
    if (link) {
      cy.get('a[data-test="learn-more-link"]').should('have.attr', 'href', link);
    }
    cy.get('button').should('have.text', button);
  });
};

describe('Discover new products', () => {
  if (Cypress.env('ORGANIZATION')) {
    return;
  }

  context('Happy path', () => {
    before(() => {
      cy.visit(URL);
    });

    it('Should render the page', () => {
      cy.title('Products')
        .get('[data-logroot="discoverNewProducts"] .page-content');
    });

    it('should render one promoted product', () => {
      cy.get(promotedProductSelector).first().within(() => {
        cy.get('p[data-test="product-title"]').should('exist');
      });
    });

    it('should render confluence info', () => {
      cy.get(promotedProductSelector).first().within(() => {
        cy.get('p[data-test="product-title"]').should('have.text', 'Jira Software’s perfect partner');
        cy.contains('Try free').should('exist');
        cy.contains('Learn more').should('have.attr', 'href', 'https://www.atlassian.com/software/confluence');
      });
    });

    it('should render Confluence apps group', () => {
      cy.get('div[data-test="products-group:confluence"]').within(() => {
        cy.get('h3').should('have.text', 'Recommended Confluence apps');

        cy.get('a[data-test="discover-more-apps-confluence"]')
          .should('have.text', 'Discover more Confluence apps')
          .should('have.attr', 'href', '/wiki/plugins/servlet/upm/marketplace');
      });
    });

    it('should render Team Calendars App', () => {
      checkProduct({
        productKey: 'team.calendars.confluence.ondemand',
        name: 'Team Calendars for Confluence',
        tag: 'Messaging',
        link: 'https://www.atlassian.com/software/confluence/team-calendars',
        getStartedLabel: 'Get started with 10 users',
        price: '$10 / month',
        pricingLink: 'https://www.atlassian.com/software',
        button: 'Free trial'
      });
    });

    it('should render Jira apps group', () => {
      cy.get('div[data-test="products-group:jira"]').within(() => {
        cy.get('h3').should('have.text', 'Recommended Jira apps');

        cy.get('a[data-test="discover-more-apps-jira"]')
          .should('have.text', 'Discover more Jira apps')
          .should('have.attr', 'href', '/plugins/servlet/upm/marketplace');
      });
    });

    it('should render Capture for Jira App', () => {
      checkProduct({
        productKey: 'bonfire.jira.ondemand',
        name: 'Capture for Jira',
        tag: 'Project management',
        link: 'https://www.atlassian.com/software/jira/capture',
        getStartedLabel: 'Get started with 10 users',
        price: '$10 / month',
        pricingLink: 'https://www.atlassian.com/software',
        button: 'Free trial'
      });
    });

    it('should render More Atlassian Applications group', () => {
      cy.get('div[data-test="products-group:extras"]').within(() => {
        cy.get('h3').should('have.text', 'More Atlassian applications');
        cy.get('ul[data-test="product-list"]');
      });
    });

    it('should render Bitbucket product', () => {
      checkExtraProduct({
        productKey: 'bitbucket',
        name: 'Bitbucket',
        tag: 'Development',
        link: 'http://www.bitbucket.org/',
        button: 'Sign up for free'
      });
    });

    it('should render Atlassian access product', () => {
      checkExtraProduct({
        productKey: 'atlassianAccess',
        name: 'Atlassian Access',
        tag: 'Identity & Security',
        button: 'Free trial'
      });
    });
  });

  context('expired account', () => {
    it('should render disabled buttons and show notification', () => {
      cy.fixture('billing/recommended-products-get')
        .patch({ expiredAccount: true })
        .as('mock-recommended-products')
        .server()
        .route('**/api/billing-ux/**/recommended-products', '@mock-recommended-products')
        .as('fetch-recommended-products')
        .visit(URL)
        .wait('@fetch-recommended-products')
        .get(productListSelector)
        .within(() => {
          cy.get('li[data-test="product:jira-core.ondemand"]').within(() => {
            cy.get('button').should('be.disabled');
          });
        })
        .get('div[data-flag-id=expired-account]')
        .should('exist');
    });
  });

  context('error/loading states', () => {
    it('should get error on whole page', () => {
      cy.server()
        .route('GET', '**/api/billing-ux/**/recommended-products')
        .asError()
        .as('fetch-recommended-products')

        .visit(URL)
        .wait('@fetch-recommended-products')

        .get(productListSelector)
        .should('not.exist')
        .get('.error.unavailable');
    });

    it('should get loading state', () => {
      cy.fixture('billing/recommended-products-get')
        .as('mock-recommended-products')
        .server()
        .route({
          method: 'GET',
          url: '**/api/billing-ux/**/recommended-products',
          response: '@mock-recommended-products',
          delay: 10000,
        })
        .as('fetch-recommended-products')

        .visit(URL)

        .get(productListSelector)
        .should('not.exist')
        .get('.loading');
    });
  });

  context('Request product opt out', () => {
    it('should display xflow opt out dialog', () => {
      cy.visit(`${URL}?requestproductoptout=1`);
      cy.get('#xflow-opt-out-turn-off-button', { timeout: 10000 }).should('exist');
    });
  });

  context('with productKey ', () => {
    it('should display xflow dialog', () => {
      cy.visit(`${URL}?product=jira-core.ondemand`);
      cy.get('#xflow-confirm-trial-confirm-button', { timeout: 10000 }).should('exist');
    });
  });

  context('should render products list', () => {
    context('for MONTHLY', () => {
      it('should render products title', () => {
        cy.get('h3').first().should('have.text', 'Atlassian products');
      });

      it('should render products', () => {
        cy.get(productListSelector).first().within(() => {
          cy.get('li').should('have.length', 5);
        });
      });

      it('should render Jira Ops product', () => {
        checkProduct({
          productKey: 'jira-incident-manager.ondemand',
          name: 'Jira Ops',
          tag: 'Incident Management',
          link: 'https://www.atlassian.com/software/jira/ops',
          getStartedLabel: 'Get started now',
          price: 'Free early access',
          pricingLink: null,
          button: 'Start now'
        });
      });

      it('should render Jira Core product', () => {
        checkProduct({
          productKey: 'jira-core.ondemand',
          name: 'Jira Core',
          tag: 'Track',
          link: 'https://www.atlassian.com/software/jira/core',
          getStartedLabel: 'Get started with 10 users',
          price: '$10 / month',
          pricingLink: 'https://www.atlassian.com/software/jira/core/pricing?tab=cloud',
          button: 'Free trial'
        });
      });

      it('should render Jira Service Desk product', () => {
        checkProduct({
          productKey: 'jira-servicedesk.ondemand',
          name: 'Jira Service Desk',
          tag: 'IT & helpdesk',
          link: 'https://www.atlassian.com/software/jira/service-desk',
          getStartedLabel: 'Get started with 3 agents',
          price: '$10 / month',
          pricingLink: 'https://www.atlassian.com/software/jira/service-desk/pricing?tab=cloud',
          button: 'Free trial'
        });
      });

      it('should render Stride product', () => {
        checkProduct({
          productKey: 'hipchat.cloud',
          name: 'Stride',
          tag: 'Chat',
          link: 'https://www.stride.com',
          getStartedLabel: 'Get started now',
          price: 'Free for all teams',
          pricingLink: 'https://www.stride.com/pricing',
          button: 'Start now'
        });
      });
    });
    context('for ANNUAL', () => {
      before(() => {
        cy.fixture('billing/recommended-products-get')
          .patch({ nextBillingPeriod: { renewalFrequency: 'ANNUAL' } })
          .as('mock-recommended-products');

        cy.server()
          .route('**/api/billing-ux/**/recommended-products', '@mock-recommended-products')
          .as('fetch-recommended-products');

        cy.visit(URL);
      });

      it('should render products title', () => {
        cy.get('h3').first().should('have.text', 'Atlassian products');
      });

      function checkAtlassianProductList() {
        checkProduct({
          productKey: 'jira-incident-manager.ondemand',
          name: 'Jira Ops',
          tag: 'Incident Management',
          link: 'https://www.atlassian.com/software/jira/ops',
          getStartedLabel: 'Get started now',
          price: 'Free early access',
          pricingLink: null,
          button: 'Start now'
        });

        checkProduct({
          productKey: 'jira-core.ondemand',
          name: 'Jira Core',
          tag: 'Track',
          link: 'https://www.atlassian.com/software/jira/core',
          getStartedLabel: 'Get started with 10 users',
          price: '$10 / year',
          pricingLink: 'https://www.atlassian.com/software/jira/core/pricing?tab=cloud',
          button: 'Free trial'
        });

        checkProduct({
          productKey: 'jira-servicedesk.ondemand',
          name: 'Jira Service Desk',
          tag: 'IT & helpdesk',
          link: 'https://www.atlassian.com/software/jira/service-desk',
          getStartedLabel: 'Get started with 3 agents',
          price: '$10 / year',
          pricingLink: 'https://www.atlassian.com/software/jira/service-desk/pricing?tab=cloud',
          button: 'Free trial'
        });

        checkProduct({
          productKey: 'hipchat.cloud',
          name: 'Stride',
          tag: 'Chat',
          link: 'https://www.stride.com',
          getStartedLabel: 'Get started now',
          price: 'Free for all teams',
          pricingLink: 'https://www.stride.com/pricing',
          button: 'Start now'
        });
      }

      it('should render Atlassian products', () => {
        cy.get(productListSelector).first().within(() => {
          cy.get('li').should('have.length', 5);
        });

        checkAtlassianProductList();
      });

      it('should render Confluence apps group', () => {
        cy.get('div[data-test="products-group:confluence"]').within(() => {
          cy.get('h3').should('have.text', 'Recommended Confluence apps');
          cy.get('li').should('have.length', 2);
        });

        checkProduct({
          productKey: 'team.calendars.confluence.ondemand',
          name: 'Team Calendars for Confluence',
          tag: 'Messaging',
          link: 'https://www.atlassian.com/software/confluence/team-calendars',
          getStartedLabel: 'Get started with 10 users',
          price: '$10 / year',
          pricingLink: 'https://www.atlassian.com/software',
          button: 'Free trial'
        });

        checkProduct({
          productKey: 'team.calendars.confluence.ondemand',
          name: 'Team Calendars for Confluence',
          tag: 'Messaging',
          link: 'https://www.atlassian.com/software/confluence/team-calendars',
          getStartedLabel: 'Get started with 10 users',
          price: '$10 / year',
          pricingLink: 'https://www.atlassian.com/software',
          button: 'Free trial'
        });

        checkProduct({
          productKey: 'com.atlassian.confluence.plugins.confluence-questions.ondemand',
          name: 'Questions for Confluence',
          tag: 'Messaging',
          link: 'https://www.atlassian.com/software/confluence/questions',
          getStartedLabel: 'Get started with 10 users',
          price: '$10 / year',
          pricingLink: 'https://www.atlassian.com/software',
          button: 'Free trial'
        });
      });

      it('should render Jira apps group', () => {
        cy.get('div[data-test="products-group:jira"]').within(() => {
          cy.get('h3').should('have.text', 'Recommended Jira apps');
          cy.get('li').should('have.length', 2);
        });

        checkProduct({
          productKey: 'bonfire.jira.ondemand',
          name: 'Capture for Jira',
          tag: 'Project management',
          link: 'https://www.atlassian.com/software/jira/capture',
          getStartedLabel: 'Get started with 10 users',
          price: '$10 / year',
          pricingLink: 'https://www.atlassian.com/software',
          button: 'Free trial'
        });

        checkProduct({
          productKey: 'com.radiantminds.roadmaps-jira.ondemand',
          name: 'Portfolio for Jira',
          tag: 'Project management',
          link: 'https://marketplace.atlassian.com/plugins/com.radiantminds.roadmaps-jira',
          getStartedLabel: 'Get started with 10 users',
          price: '$10 / year',
          pricingLink: 'https://www.atlassian.com/software',
          button: 'Free trial'
        });
      });

      it('should render More Atlassian Applications group', () => {
        cy.get('div[data-test="products-group:extras"]').within(() => {
          cy.get('h3').should('have.text', 'More Atlassian applications');
          cy.get('li').should('have.length', 2);
        });
        checkExtraProduct({
          productKey: 'bitbucket',
          name: 'Bitbucket',
          tag: 'Development',
          link: 'http://www.bitbucket.org/',
          button: 'Sign up for free'
        });
        checkExtraProduct({
          productKey: 'atlassianAccess',
          name: 'Atlassian Access',
          tag: 'Identity & Security',
          button: 'Free trial'
        });
      });
    });
  });
});
