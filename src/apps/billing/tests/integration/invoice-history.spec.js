import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import billHistory from 'bux/features/bill-history';
import { Provider } from 'bux/helpers/redux';
import { until } from 'bux/helpers/enzyme-utils';

describe('Invoice history page', () => {
  const BillHistoryPage = billHistory.Page;

  it('should render the invoice list', async () => {
    console.log('Testing BillHistoryPage');

    const page = mount(<Provider><BillHistoryPage /></Provider>);

    expect(page.find('.page-title')).to.have.text('Bill history');

    await until(() => page.update().find('.invoice-list').length, 'Waiting for invoices');

    expect(page.find('.invoice-line')).to.have.length(4);

    const invoiceRow = page.find('.invoice-line').first();

    expect(invoiceRow.find('.bill-date')).to.have.text('1 Apr 2017');
    expect(invoiceRow.find('.invoice-number')).to.have.text('1');
    expect(invoiceRow.find('.amount')).to.have.text('$10.50');

    const link = invoiceRow.find('.link').hostNodes();

    expect(link).to.have.text('Download');

    link.simulate('click');

    expect(link.props().href).to.equal('invoice1.url');
  });
});

