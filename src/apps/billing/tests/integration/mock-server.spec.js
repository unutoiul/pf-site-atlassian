const spawn = require('child_process').spawn;

let mockServerProcess;

before((done) => {
  console.log('Starting mock server for integration tests');
  mockServerProcess = spawn('yarn', ['mock-server'], {
    cwd: process.cwd()
  });

  mockServerProcess.stdout.on('data', (data) => {
    const stringData = data.toString().trim();
    if (stringData === 'Mock Server is running') {
      console.log('mock server is ready');
      done();
    }
    console.log(stringData);
  });

  mockServerProcess.stderr.on('data', (data) => {
    console.log(`stderr: ${data}`);
  });

  mockServerProcess.on('close', (code) => {
    console.log(`child process exited with code ${code}`);
  });
});

after(() => {
  console.log(`Stopping mock server for integration tests with pid${mockServerProcess.pid}`);
  mockServerProcess.kill('SIGTERM');
});
