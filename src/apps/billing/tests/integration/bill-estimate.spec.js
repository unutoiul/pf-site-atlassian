import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import BillEstimate from 'bux/pages/BillEstimate';
import { Provider } from 'bux/helpers/redux';
import { until } from 'bux/helpers/enzyme-utils';


describe('Bill Estimate page test', () => {
  it('should render the bill estimate', async () => {
    console.log('Testing Bill Estimate page');

    const page = mount(<Provider><BillEstimate /></Provider>);
    await until(() => page.update().find('.product-line').length, 'Waiting for bill estimate lines');

    expect(page.find('.page-title')).to.contain.text('Bill estimate');

    const nonBreakingSpace = '\xa0';
    expect(page.find('.page-subtitle')).to.have.text(`17 Oct${nonBreakingSpace}–${nonBreakingSpace}16 Nov`);
    expect(page.find('.product-line')).to.have.length(8);
    expect(page.find('.total-cost')).to.have.text('$12529 USD');

    const testedProducts = [];

    page.find('.product-line').forEach((product) => {
      const prodName = product.find('.product-description').text();
      const usage = product.find('.product-usage').map(node => node.text());

      if (prodName === 'Bitbucket') {
        console.log('Verifying Bill Estimate of %s', prodName);
        expect(product.find('.product-price')).to.contain.text('$3720');
        expect(product.find('.product-usage')).to.have.length(3);
        expect(usage).includes('742 users');
        expect(usage).includes('30 gigabytes');
        expect(usage).includes('50 build minutes');
        testedProducts.push(prodName);
      } else if (prodName === 'Confluence (Cloud)') {
        console.log('Verifying Bill Estimate of %s', prodName);
        expect(product.find('.product-price')).to.contain.text('$1000');
        expect(usage).includes('820 users');
        testedProducts.push(prodName);
      } else if (prodName === 'JIRA Software (Cloud)') {
        console.log('Verifying Bill Estimate of %s', prodName);
        expect(product.find('.product-price')).to.contain.text('$750');
        expect(usage).includes('301 users');
        testedProducts.push(prodName);
      } else if (prodName === 'JIRA Core (Cloud)') {
        console.log('Verifying Bill Estimate of %s', prodName);
        expect(usage).includes('1 user');
        testedProducts.push(prodName);
      } else if (prodName === 'Stride') {
        console.log('Verifying Bill Estimate of %s', prodName);
        expect(product.find('.product-price')).to.contain.text('Free');
        expect(usage).includes('500 users');
        testedProducts.push(prodName);
      } else if (prodName === 'Identity Manager (Cloud)') {
        console.log('Verifying Bill Estimate of %s', prodName);
        expect(product.find('.product-price')).to.contain.text('Inactive');
        expect(usage).includes('Unlimited');
        testedProducts.push(prodName);
      }
    });

    // Check that all expected products were tested
    expect(testedProducts).to.have.members(['Bitbucket', 'Confluence (Cloud)', 'JIRA Software (Cloud)',
      'JIRA Core (Cloud)', 'Stride', 'Identity Manager (Cloud)']);
  });
});
