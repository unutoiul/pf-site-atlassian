import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { mount } from 'enzyme';
import { Provider } from 'bux/helpers/redux';
import proxyquire from 'bux/helpers/proxyquire';
import DiscoverNewProducts from 'bux/pages/DiscoverNewProducts';
import { until } from 'bux/helpers/enzyme-utils';

const productListSelector = 'ul[data-test="product-list"]';

const checkProduct = ({
  productList, productKey, name, tag, link, getStartedLabel, price, pricingLink, button
}) => {
  const product = productList.find(`li[data-test="product:${productKey}"]`);
  expect(product).to.be.present();
  expect(product.find('div').first()).to.have.attr('data-logroot', productKey);
  expect(product.find('div[data-test="product-title"]')).to.contain.text(name);
  expect(product.find('div[data-test="product-tag"]')).to.contain.text(tag);
  expect(product.find('a[data-test="learn-more-link"]')).to.have.attr('href', link);
  expect(product.find('p[data-test="get-started-label"]')).to.have.text(getStartedLabel);
  expect(product.find('p[data-test="price"]')).to.have.text(price);
  expect(product.find('a[data-test="full-price-link"]')).to.have.attr('href', pricingLink);
  expect(product.find('button')).to.have.text(button);
};

describe('Discover new products page', () => {
  let page;

  before(() => {
    page = mount(<Provider><DiscoverNewProducts /></Provider>);
  });

  it('should render page title', async () => {
    expect(page.find('.page-title')).to.have.text('Discover new products');
  });

  it('should render products list', async () => {
    await until(() => page.update().find(productListSelector).length, 'Waiting for page to load');
    expect(page.find('h3').first()).to.have.text('Atlassian products');
    const productList = page.find(productListSelector).first();
    expect(productList.find('li')).to.have.length(3);

    checkProduct({
      productList,
      productKey: 'jira-core.ondemand',
      name: 'Jira Core',
      tag: 'Track',
      link: 'https://www.atlassian.com/software/jira/core',
      getStartedLabel: 'Get started with 10 users',
      price: '$10 / month',
      pricingLink: 'https://www.atlassian.com/software/jira/core/pricing?tab=cloud',
      button: 'Free trial'
    });
    checkProduct({
      productList,
      productKey: 'jira-servicedesk.ondemand',
      name: 'Jira Service Desk',
      tag: 'IT & helpdesk',
      link: 'https://www.atlassian.com/software/jira/service-desk',
      getStartedLabel: 'Get started with 3 agents',
      price: '$10 / month',
      pricingLink: 'https://www.atlassian.com/software/jira/service-desk/pricing?tab=cloud',
      button: 'Free trial'
    });
    checkProduct({
      productList,
      productKey: 'hipchat.cloud',
      name: 'Stride',
      tag: 'Chat',
      link: 'https://www.stride.com',
      getStartedLabel: 'Get started now',
      price: 'Free for all teams',
      pricingLink: 'https://www.stride.com/pricing',
      button: 'Start now'
    });
  });

  it('should render applications list', async () => {
    await until(() => page.update().find(productListSelector).length, 'Waiting for page to load');
    const confluenceGroup = page.find('div[data-test="products-group:confluence"]');
    expect(confluenceGroup.find('h3')).to.have.text('Recommended Confluence apps');
    const confluenceAppList = confluenceGroup.find(productListSelector);
    checkProduct({
      productList: confluenceAppList,
      productKey: 'team.calendars.confluence.ondemand',
      name: 'Team Calendars for Confluence',
      tag: 'Messaging',
      link: 'https://www.atlassian.com/software/confluence/team-calendars',
      getStartedLabel: 'Get started with 10 users',
      price: '$10 / month',
      pricingLink: 'https://www.atlassian.com/software',
      button: 'Free trial'
    });

    const jiraGroup = page.find('div[data-test="products-group:jira"]');
    expect(jiraGroup.find('h3')).to.have.text('Recommended Jira apps');
    const jiraAppList = jiraGroup.find(productListSelector);
    checkProduct({
      productList: jiraAppList,
      productKey: 'bonfire.jira.ondemand',
      name: 'Capture for Jira',
      tag: 'Project management',
      link: 'https://www.atlassian.com/software/jira/capture',
      getStartedLabel: 'Get started with 10 users',
      price: '$10 / month',
      pricingLink: 'https://www.atlassian.com/software',
      button: 'Free trial'
    });
  });

  describe('Discover new products page loading/error states', () => {
    let isLoaded;
    let AugmentedComponent;

    beforeEach(() => {
      isLoaded = sinon.stub();
      ({ AugmentedComponent } = proxyquire.noCallThru()
        .load('../../src/shared/pages/DiscoverNewProducts/DiscoverNewProducts', {
          './selectors': { isLoaded },
        }));
    });

    afterEach(() => {
      isLoaded.reset();
    });

    it('should show loading on whole page if service is loading', async () => {
      isLoaded.returns({ loading: true });

      page = mount(<Provider><AugmentedComponent /></Provider>);

      const loadingShowDelay = 350;
      await until(() => page.update().find('.loading').length, 'Waiting for loading component', loadingShowDelay);
      expect(page.find(productListSelector)).not.to.be.present();
    });

    it('should show error on whole page if major service is on error', async () => {
      isLoaded.returns({ display: false, error: true });

      page = mount(<Provider><AugmentedComponent /></Provider>);

      await until(() => page.update().find('.error.unavailable').length, 'Waiting for error component');
      expect(page.find(productListSelector)).not.to.be.present();
    });
  });
});
