import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { mount } from 'enzyme';
import fetchMock from 'fetch-mock';
import proxyquire from 'bux/helpers/proxyquire';
import { push } from 'bux/core/state/router/actions';
/* eslint-disable-next-line */
import { GatewayProvider, GatewayDest } from '@atlaskit/layer-manager';
import BillOverviewPage from 'bux/pages/BillOverview';
import Application from 'bux/components/Application';
import { Provider } from 'bux/helpers/redux';
import config from 'bux/common/config';
import createStore from 'bux/core/createStore';
import Button from 'bux/components/Button';
import { until, flushPromise } from 'bux/helpers/enzyme-utils';
import BillingEstimateMock from '../../mock-server/bux/api/billing/bill-estimate-get.json';
import BillingDetailsMock from '../../mock-server/bux/api/billing/billing-details-get.json';
import MetadataMock from '../../mock-server/bux/api/metadata-get.json';
import { mockMetadata, mockBillingDetails } from '../helpers/mockRespondWith';

describe('Bill overview page', () => {
  const billEstimateWidgetSelector = '.bill-estimate-widget';
  const paymentDetailsWidgetSelector = '.payment-detail-widget';
  const billHistoryWidgetSelector = '.bill-history-widget';
  const billingContactWidgetSelector = '.invoice-contact-widget';
  const updateContactModalDialogSelector = '.update-contact-modal-dialog';
  const updateContactWarningModalSelector = '.update-contact-warning-modal';
  let page;

  before(() => {
    page = mount(<Provider><BillOverviewPage /></Provider>);
  });

  it('should render page title', async () => {
    expect(page.find('.page-title')).to.have.text('Billing overview');
  });

  describe('Bill overview page widgets', () => {
    it('should render bill estimate widget', async () => {
      const store = createStore();
      store.dispatch(push('/overview'));

      await flushPromise();

      page = mount(<Provider store={store}>
            <BillOverviewPage />
        </Provider>);

      await until(() => page.update().find(billEstimateWidgetSelector).length, 'Waiting for bill estimate');

      const billEstimateWidget = page.find(billEstimateWidgetSelector);
      expect(billEstimateWidget).to.be.present();

      expect(billEstimateWidget.find('.widget-title')).to.have.text('Bill estimate');
      expect(billEstimateWidget.find('.price')).to.have.text('$12529 USD');
      expect(billEstimateWidget.find('.date')).to.have.text('Next Charge: Oct 6, 2022');

      const link = billEstimateWidget.find('.link').hostNodes();
      expect(link).to.be.present();
      expect(link).to.have.text('View full estimate');
      expect(link).to.have.attr('href', '/estimate');
    });

    it('should render billing details widget', async () => {
      await until(() => page.update().find(paymentDetailsWidgetSelector).length, 'Waiting for billing details');

      const paymentDetailsWidget = page.find(paymentDetailsWidgetSelector);
      expect(paymentDetailsWidget).to.be.present();

      expect(paymentDetailsWidget.find('.widget-title')).to.have.text('Billing details');
      const ccNumber = '\u2022\u2022\u2022\u2022 \u2022\u2022\u2022\u2022 \u2022\u2022\u2022\u2022 1111';
      expect(paymentDetailsWidget.find('.number')).to.have.text(ccNumber);
      expect(paymentDetailsWidget.find('.expiry')).to.have.text('Expires 01/2022');
      expect(paymentDetailsWidget.find('.name')).to.have.text('Superplasma Team');

      const link = paymentDetailsWidget.find('.widgetEditLink').hostNodes();
      expect(link).to.be.present();
      expect(link).to.have.attr('href', '/paymentdetails');
    });

    it('should render bill history widget', async () => {
      await until(() => page.update().find(billHistoryWidgetSelector).length, 'Waiting for bill history');

      const billHistoryWidget = page.find(billHistoryWidgetSelector);
      expect(billHistoryWidget).to.be.present();

      expect(billHistoryWidget.find('.widget-title')).to.have.text('Bill history');

      const invoiceLine = billHistoryWidget.find('.invoice-line');
      expect(invoiceLine).to.have.length(3);

      const invoiceRow = invoiceLine.first();
      expect(invoiceRow.find('.bill-date')).to.have.text('Apr 1, 2017');
      expect(invoiceRow.find('.amount')).to.have.text('$10.50');
    });

    it('should render billing contacts widget', async () => {
      await until(() => page.update().find(billingContactWidgetSelector).length, 'Waiting for billing contacts');

      const billingContactWidget = page.find(billingContactWidgetSelector);
      expect(billingContactWidget).to.be.present();

      expect(billingContactWidget.find('.widget-title')).to.have.text('Billing contacts');

      expect(billingContactWidget.find('.avatar div').children().at(1)).to.have.text('Supernova Team');
      expect(billingContactWidget.find('.avatar div').children().at(2)).to.have.text('bob@example.com');

      const makeMeBilingBtn = billingContactWidget.find('[name="update-billing-contact"]').find(Button);
      expect(makeMeBilingBtn).to.have.text('Make me a billing contact');

      const manageContactsBtn = billingContactWidget.find('[name="manage-contacts"]').find(Button);
      expect(manageContactsBtn).to.have.text('Manage contacts');

      expect(billingContactWidget.find('.email')).to.have.text('bob-invoice@example.com');
    });

    it('should render billing contact widget without CTA if main contact is current user', async () => {
      await mockMetadata({
        status: 200,
        method: 'GET',
        body: Object.assign({}, MetadataMock, {
          user: {
            ...MetadataMock.user,
            email: 'bob@example.com'
          }
        })
      });

      page = mount(<Provider><BillOverviewPage /></Provider>);

      await until(() => page.update().find(billingContactWidgetSelector).length, 'Waiting for billing contacts');

      const billingContactWidget = page.find(billingContactWidgetSelector);

      await flushPromise();

      const makeMeBillingBtn = billingContactWidget.find('[name="update-billing-contact"]').find(Button);
      expect(makeMeBillingBtn).to.not.be.present();
    });

    it.skip('should display a warning when user clicks update billing contact CTA', async () => {
      page = mount(<Provider>
          <GatewayProvider>
            <Application>
              <BillOverviewPage />
            </Application>
            <div>MODAAAAAAL</div>
            <GatewayDest name="modal" />
          </GatewayProvider>
        </Provider>);

      await until(() => page.update().find(billingContactWidgetSelector).length, 'Waiting for billing contacts');

      await mockBillingDetails({
        status: 200,
        method: 'GET',
        body: Object.assign({}, BillingDetailsMock, {
          firstName: 'Poro',
          lastName: 'Tito',
          email: 'bob-invoice@example.com',
        })
      });

      const billingContactWidget = page.find(billingContactWidgetSelector);

      expect(billingContactWidget.find('.avatar div').children().at(1)).to.have.text('Supernova Team');
      expect(billingContactWidget.find('.avatar div').children().at(2)).to.have.text('bob@example.com');

      const makeMeBillingBtn = billingContactWidget.find('[name="update-billing-contact"]').find(Button);
      expect(makeMeBillingBtn).to.have.text('Make me a billing contact');

      makeMeBillingBtn.simulate('click');

      await until(() => page.update().find(updateContactModalDialogSelector).length, 'Waiting for billing contacts');

      expect(page.find(updateContactWarningModalSelector))
        .to.contain.text('You are about to replace Supernova as the primary billing contact.');
    });

    it.skip('should update the billing contact when user clicks CTA', async () => {
      page = mount(<Provider><BillOverviewPage /></Provider>);

      await until(() => page.update().find(billingContactWidgetSelector).length, 'Waiting for billing contacts');

      await mockBillingDetails({
        status: 200,
        method: 'GET',
        body: Object.assign({}, BillingDetailsMock, {
          firstName: 'Poro',
          lastName: 'Tito',
          email: 'bob-invoice@example.com',
        })
      });

      const billingContactWidget = page.find(billingContactWidgetSelector);

      expect(billingContactWidget.find('.avatar div').children().at(1)).to.have.text('Supernova Team');
      expect(billingContactWidget.find('.avatar div').children().at(2)).to.have.text('bob@example.com');

      const makeMeBillingBtn = billingContactWidget.find('[name="update-billing-contact"]').find(Button);
      expect(makeMeBillingBtn).to.have.text('Make me a billing contact');

      makeMeBillingBtn.simulate('click');

      await flushPromise();

      expect(billingContactWidget.find('.avatar div').children().at(1)).to.have.text('Poro Tito');
      expect(billingContactWidget.find('.avatar div').children().at(2)).to.have.text('bob-invoice@example.com');
      expect(makeMeBillingBtn).to.have.length(0);
    });
  });

  describe('Bill overview page loading/error states', () => {
    let isLoaded;
    let AugmentedComponent;

    beforeEach(() => {
      isLoaded = sinon.stub();
      ({ AugmentedComponent } = proxyquire.noCallThru().load('../../src/shared/pages/BillOverview/BillOverview', {
        './selectors': { isLoaded },
      }));
    });

    afterEach(() => {
      isLoaded.reset();
    });

    it('should show loading on whole page if service is loading', async () => {
      isLoaded.returns({ loading: true });

      page = mount(<Provider><AugmentedComponent /></Provider>);

      const loadingShowDelay = 350;
      await until(() => page.update().find('.loading').length, 'Waiting for loading component', loadingShowDelay);
      expect(page.find(billEstimateWidgetSelector)).not.to.be.present();
      expect(page.find(paymentDetailsWidgetSelector)).not.to.be.present();
      expect(page.find(billHistoryWidgetSelector)).not.to.be.present();
      expect(page.find(billingContactWidgetSelector)).not.to.be.present();
    });

    it('should show error on whole page if major service is on error', async () => {
      isLoaded.returns({ display: false, error: true });

      page = mount(<Provider><AugmentedComponent /></Provider>);

      await until(() => page.update().find('.error.unavailable').length, 'Waiting for error component');
      expect(page.find(billEstimateWidgetSelector)).not.to.be.present();
      expect(page.find(paymentDetailsWidgetSelector)).not.to.be.present();
      expect(page.find(billHistoryWidgetSelector)).not.to.be.present();
      expect(page.find(billingContactWidgetSelector)).not.to.be.present();
    });

    it('should display the overview page with widget in errors when minor error occurs', async () => {
      fetchMock
        .mock(`${config.billingUXServiceUrl}/api/billing/invoice-history`, 423)
        .mock(`${config.billingUXServiceUrl}/api/billing/bill-estimate`, BillingEstimateMock)
        .mock(`${config.billingUXServiceUrl}/api/billing/billing-details`, BillingDetailsMock)
        .spy();

      page = mount(<Provider><BillOverviewPage /></Provider>);

      await until(() => page.update().find(billEstimateWidgetSelector).length, 'Waiting for first widget');
      expect(page.find(billEstimateWidgetSelector)).to.be.present();
      expect(page.find(paymentDetailsWidgetSelector)).to.be.present();
      expect(page.find(billingContactWidgetSelector)).to.be.present();

      const billHistoryWidget = page.find(billHistoryWidgetSelector);
      expect(billHistoryWidget.find('.aui-message.aui-message-error.widget-error')).to.be.present();

      fetchMock.restore();
    });
  });

  describe('Query param Atl-OrgId', () => {
    const loadPage = () => {
      const store = createStore();
      store.dispatch(push('/overview?org=123'));
      page = mount(<Provider store={store}>
            <BillOverviewPage />
        </Provider>);
    };

    it('should pass Atl-OrgId to bux service when routing has org id query param', () => {
      loadPage();

      fetchMock
        .mock(`${config.billingUXServiceUrl}/api/billing/invoice-history`, 200)
        .mock(`${config.billingUXServiceUrl}/api/billing/bill-estimate`, 200)
        .mock(`${config.billingUXServiceUrl}/api/billing/billing-details`, 200)
        .spy();

      const fetchCalled = fetchMock.calls();
      expect(fetchCalled).to.have.property('matched');
      fetchCalled.matched.forEach((matched) => {
        const matchedParams = matched[1];
        expect(matchedParams).to.have.property('headers');
        expect(matchedParams.headers).to.have.property('X-Atl-OrgId', '123');
      });

      fetchMock.restore();
    });

    it('should pass org param to all page links when routing has org id query param', async () => {
      await mockBillingDetails({
        status: 200,
        method: 'GET',
        body: Object.assign({}, BillingDetailsMock, {
          firstName: 'Poro',
          lastName: 'Tito',
          email: 'bob-invoice@example.com',
        })
      });

      loadPage();

      await until(() => (
        page.update().find(billingContactWidgetSelector).find('.myLink').length > 0 &&
        page.update().find(billEstimateWidgetSelector).find('.link').length > 0 &&
        page.update().find(paymentDetailsWidgetSelector).find('.widgetEditLink').length > 0
      ), 'Waiting for first widget');

      expect(page.find(billEstimateWidgetSelector).find('.link').hostNodes())
        .to.have.prop('href', '/estimate?org=123');
      expect(page.find(paymentDetailsWidgetSelector).find('.widgetEditLink').hostNodes())
        .to.have.prop('href', '/paymentdetails?org=123');
      expect(page.find(billingContactWidgetSelector).find('.widgetEditLink').hostNodes())
        .to.have.prop('href', '/paymentdetails?org=123');
      expect(page.find(billingContactWidgetSelector).find('.myLink').hostNodes())
        .to.have.prop('href').and.not.include('?org=123');
    });
  });
});
