module.exports = {
  assertSection() {
    const billEstimate = this;
    billEstimate.assert.count('@product', 3);
    billEstimate.expect.element(billEstimate.dynEl('@productRow', 1, 'description'))
      .text.to.equal('Bitbucket');
    billEstimate.expect.element(billEstimate.dynEl('@productRow', 1, 'price')).text.to.equal('$3,720');

    billEstimate.expect.element(billEstimate.dynEl('@productRow', 2, 'description'))
      .text.to.equal('Confluence (Cloud)');
    billEstimate.expect.element(billEstimate.dynEl('@productRow', 2, 'price')).text.to.equal('$1,000');

    billEstimate.expect.element('@priceRest').text.to.equal('$6,170');

    billEstimate.expect.element('@tax').text.to.equal('$1,139.00');
    billEstimate.expect.element('@total').text.to.match(/USD\s?12,529.00/);


    billEstimate.click('@actionExpand');
    billEstimate.assert.count('@product', 9);
    billEstimate.click('@actionCollapse');
    billEstimate.assert.count('@product', 3);
  }
};
