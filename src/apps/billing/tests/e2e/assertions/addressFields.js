const assertFieldsExist = (addressFields, addressValues) => {
  addressFields.expect.element('@addressLine1').value.to.equal(addressValues.addressLine1);
  addressFields.expect.element('@addressLine2').value.to.equal(addressValues.addressLine2);
  addressFields.expect.element('@city').value.to.equal(addressValues.city);
  addressFields.expect.element('@postcode').value.to.equal(addressValues.postcode);
  addressFields.expect.element('#state').text.to.equal(addressValues.state);

  addressFields.expect.element('#country').text.to.equal(addressValues.country);
};

const assertFieldsDoNotExist = (addressFields) => {
  addressFields.expect.element('@addressLineFull').to.be.not.present;
  addressFields.expect.element('@addressLine1').to.be.not.present;
  addressFields.expect.element('@addressLine2').to.be.not.present;
  addressFields.expect.element('@city').to.be.not.present;
  addressFields.expect.element('@postcode').to.be.not.present;
  addressFields.expect.element('@state').to.be.not.present;
  addressFields.expect.element('@country').to.be.not.present;
};

const setFieldValues = (addressFields, addressValues) => {
  addressFields.setFormValue('@addressLine1', addressValues.addressLine1);
  addressFields.setFormValue('@addressLine2', addressValues.addressLine2);
  addressFields.setFormValue('@city', addressValues.city);
  addressFields.selectOption(this.api, addressFields.elements.state, addressValues.state, true);
  addressFields.setFormValue('@postcode', addressValues.postcode);
};

const assertCountrySelection = (client, addressFields) => {
  const setCountry = countryName => (
    addressFields.selectOption(client, addressFields.elements.country, countryName, true)
  );

  setCountry('United States of America');
  addressFields.expect.element('@state').to.be.a('input').before(100);
  setCountry('United Kingdom');
  addressFields.expect.element('@state').to.be.not.present;
  addressFields.expect.element('#state').to.be.a('input').before(100);
  setCountry('Australia');
  addressFields.expect.element('@state').to.be.a('input').before(100);
};

module.exports = {
  assertAddressSupport(addressValues) {
    this.setFormValue('@addressLineFull', '343 George street');
    this.expect.element('@notificationFormMessage')
      .to.contain.text('Select a value from the list below').before(10000);

    this.setFormValue('@addressLineFull', 'somethingyoucantcomplete');
    this.expect.element('@notificationFormMessage')
      .to.contain.text('Oops, we couldn\'t find your address.').before(10000);

    this.click('@fullFormToggle');
    assertFieldsExist(this, addressValues);
    assertCountrySelection(this.api, this);
    setFieldValues(this, addressValues);
  },
  assertFieldsExist(addressValues) {
    this.setFormValue('@addressLineFull', 'Open full form');
    this.click('@fullFormToggle');
    assertFieldsExist(this, addressValues);
  },
  assertFieldsDoNotExist() {
    assertFieldsDoNotExist(this);
  }
};
