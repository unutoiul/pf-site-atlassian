exports.assertion = function countAssertion(selector, count) {
  this.message = `Testing if element <${selector}> has count: ${count}`;
  this.expected = count;
  this.pass = val => val === this.expected;
  this.value = res => res.value.length;
  this.command = cb => this.api.elements(this.client.locateStrategy, selector, cb);
};