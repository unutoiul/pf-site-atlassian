const expectElement = (pageObject, object) => pageObject.api.expect.element(pageObject.elements[object].selector);

module.exports = {
  assertMessage(type, message) {
    message && expectElement(this, 'notification').to.have.attribute('data-title').equal(message);
    type && expectElement(this, 'notification').to.have.attribute('data-type').equal(type);
  }
};