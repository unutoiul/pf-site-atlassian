const billingDetailsMock = require('../../mock-server/bux/api/billing/billing-details-get.json');

const CHUNK_AWAIT_TIMEOUT = 10000;

module.exports = {
  beforeEach(client) {
    client.reset();
    client.maximizeWindow();
  },
  /**/

  'Should contain billing estimate widget': (client) => {
    const billOverview = client.page.billOverview();

    billOverview.navigate();
    billOverview.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    billOverview.expect.section('@billEstimate').to.be.visible;

    const billEstimate = billOverview.section.billEstimate;
    billEstimate.expect.element('@title').text.to.equal('Bill estimate');

    billEstimate.expect.element('@total').text.to.match(/USD\s?12,529.00/);
    billEstimate.expect.element('@endDate').text.to.equal('Next Charge: Oct 6, 2022');

    billEstimate.click('@detailLink');
    client.assert.urlContains('/estimate');
  },

  'Should contain billing details widget': (client) => {
    const ccNumber = '\u2022\u2022\u2022\u2022 \u2022\u2022\u2022\u2022 \u2022\u2022\u2022\u2022 1111';
    const billOverview = client.page.billOverview();
    billOverview.navigate();
    billOverview.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    billOverview.expect.section('@paymentDetail').to.be.visible;

    const paymentDetail = billOverview.section.paymentDetail;
    paymentDetail.expect.element('@number').text.to.equal(ccNumber);
    paymentDetail.expect.element('@expiry').text.to.equal('Expires 01/2022');
    paymentDetail.expect.element('@name').text.to.equal('Superplasma Team');

    paymentDetail.click('@editLink');
    client.assert.urlContains('/paymentdetails');
  },

  'Should contain bill history widget': (client) => {
    const billOverview = client.page.billOverview();

    billOverview.navigate();
    billOverview.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    billOverview.expect.section('@billHistory').to.be.visible;

    const billHistory = billOverview.section.billHistory;
    billHistory.expect.element('@title').text.to.equal('Bill history');

    billHistory.assert.count('@invoices', 3);
    billHistory.expect.element(billHistory.dynEl('@invoiceRow', 1, 'bill-date')).text.to.equal('Apr 1, 2017');
    billHistory.expect.element(billHistory.dynEl('@invoiceRow', 1, 'amount')).text.to.equal('$10.50');
  },

  'Should contain invoice contact widget': (client) => {
    client.mockBillingDetails({
      status: 200,
      method: 'GET',
      body: Object.assign({}, billingDetailsMock, {
        email: 'bob-invoice@example.com',
      })
    });

    const billOverview = client.page.billOverview();
    billOverview.navigate();
    billOverview.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    billOverview.expect.section('@invoiceContact').to.be.visible;

    const invoiceContact = billOverview.section.invoiceContact;
    invoiceContact.expect.element('@title').text.to.equal('Billing contacts');
    invoiceContact.expect.element('@email').text.to.equal('bob-invoice@example.com');
    invoiceContact.expect.element('@avatar').text.to.contain('Supernova Team');
    // TODO: myLink tests fail without mocking graphql
    // invoiceContact.expect.element('@myLink').to.be.present.before(1);
  },

  'Should update billing contact': (client) => {
    const billOverview = client.page.billOverview();

    billOverview.navigate();
    billOverview.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    client.mockBillingDetails({
      status: 200,
      method: 'GET',
      body: Object.assign({}, billingDetailsMock, {
        firstName: 'Bruce',
        lastName: 'Wayne',
        email: 'bob-invoice@example.com',
      })
    });

    billOverview.expect.section('@invoiceContact').to.be.visible;

    const invoiceContact = billOverview.section.invoiceContact;

    invoiceContact.click('@updateMain');

    billOverview.waitForElementVisible('@modalMessage', 1000);
    billOverview.click('@confirmButton');

    // TODO: myLink tests fail without mocking graphql
    // invoiceContact.waitForElementVisible('@myLink', CHUNK_AWAIT_TIMEOUT);

    invoiceContact.expect.element('@avatar').text.to.contain('Bruce Wayne');
    invoiceContact.expect.element('@avatar').text.to.contain('bob-invoice@example.com');
    // TODO: myLink tests fail without mocking graphql
    // invoiceContact.expect.element('@myLink').to.be.present.before(1);
  },

  'Should make user secondary and send him to manage contacts': (client) => {
    const billOverview = client.page.billOverview();

    billOverview.navigate();
    billOverview.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    billOverview.expect.section('@invoiceContact').to.be.visible;

    const invoiceContact = billOverview.section.invoiceContact;

    invoiceContact.click('@secondaryContact');

    client.back();

    invoiceContact.expect.element('@avatar').text.to.contain('Supernova Team').before(CHUNK_AWAIT_TIMEOUT);
    invoiceContact.expect.element('@avatar').text.to.contain('bob@example.com').before(CHUNK_AWAIT_TIMEOUT);
    invoiceContact.expect.element('@myLink').to.not.be.present.before(1);
  },

  'Should get error on whole page is important source missing': (client) => {
    const billOverview = client.page.billOverview();

    client.mockBillEstimate({ status: 500 });
    billOverview.navigate();

    billOverview.expect.section('@billEstimate').to.be.not.present;
    billOverview.expect.element('@globalError').to.be.visible.before(CHUNK_AWAIT_TIMEOUT);
  },

  'Should still display the overview page with widget in errors when minor error occurs': (client) => {
    const billOverview = client.page.billOverview();

    client.mockInvoiceHistory({ status: 500 });
    billOverview.navigate();

    billOverview.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);
    const billHistory = billOverview.section.billHistory;
    billHistory.expect.element('@error').to.be.visible.before(CHUNK_AWAIT_TIMEOUT);
  },

  'Should display payment details prompt when they are not provided': (client) => {
    client.mockBillingDetails({
      status: 200,
      method: 'GET',
      body: Object.assign({}, billingDetailsMock, {
        creditCard: {}
      })
    });
    const billOverview = client.page.billOverview();

    billOverview.navigate();
    billOverview.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    billOverview.expect.section('@paymentDetailsPrompt').to.be.visible;
  },

  'Should display account-is-under-management prompt when it is': (client) => {
    client.mockBillingDetails({
      status: 200,
      method: 'GET',
      body: Object.assign({}, billingDetailsMock, {
        managedByPartner: true
      })
    });
    const billOverview = client.page.billOverview();

    billOverview.navigate();
    billOverview.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    billOverview.expect.section('@accountIsUnderManagement').to.be.visible.before(CHUNK_AWAIT_TIMEOUT);
  },

  'Should contain billing details widget with paypal': (client) => {
    client.mockBillingDetails({
      status: 200,
      method: 'GET',
      body: Object.assign({}, billingDetailsMock, {
        paymentMethod: 'PAYPAL_ACCOUNT',
        paypalAccount: {
          email: 'bob-paypal@example.com'
        }
      })
    });

    const billOverview = client.page.billOverview();
    billOverview.navigate();
    billOverview.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    billOverview.expect.section('@paymentDetail').to.be.visible.before(CHUNK_AWAIT_TIMEOUT);

    const paymentDetail = billOverview.section.paymentDetail;
    paymentDetail.expect.element('@title').text.to.equal('Billing details');
    paymentDetail.expect.element('@email').text.to.equal('bob-paypal@example.com');
  },
};
