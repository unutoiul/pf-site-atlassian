const billingDetailsMock = require('../../mock-server/bux/api/billing/billing-details-get.json');
const billEstimateMock = require('../../mock-server/bux/api/billing/bill-estimate-get.json');
const entitlementGroupMock = require('../../mock-server/bux/api/billing/entitlement-group-get.json');
const metadataMock = require('../../mock-server/bux/api/metadata-get.json');

let STRIDE_ELEMENT_ID = 6;
const CHUNK_AWAIT_TIMEOUT = 10000;

const identityManagerEntitlement = {
  accountId: 'a1c4dc4d-259b-493a-a179-6377cdfd414b',
  creationDate: '2017-12-18',
  editionTransitions: [],
  endDate: '2018-01-18',
  id: '7515299',
  name: 'Atlassian Access (Cloud)',
  productKey: 'com.atlassian.identity-manager',
  sen: 'SEN-7515299',
  startDate: '2017-12-18',
  status: 'ACTIVE',
  suspended: false
};

const atlassianAccessExtras = {
  lines: [{
    amountBeforeTax: 30,
    amountWithTax: 33,
    taxAmount: 3,
    entitlementId: '7530796',
    productKey: 'com.atlassian.identity-manager',
    productName: 'Atlassian Access (Cloud)',
    usage: [{
      units: 20,
      unlimited: false,
      description: 'Atlassian Access users',
      unitCode: 'users',
      amountBeforeTax: 30,
      taxRate: 0,
      taxAmount: 3,
      amountWithTax: 33,
    }],
  }],
  atlassianAccessExtras: {
    trello: {
      period: {
        startDate: '2018-10-02',
        endDate: '2018-11-02',
        renewalFrequency: 'MONTHLY',
      },
      unitCount: 22,
      additionalUnitCount: 2,
      amountBeforeTax: 416.00,
      amountWithTax: 457.60,
    },
    grandfathering: {
      period: {
        startDate: '2018-09-02',
        endDate: '2018-10-02',
        renewalFrequency: 'MONTHLY',
      },
      amountBeforeTax: 189.00,
      amountWithTax: 207.90,
      pricingPlanSummary: {
        description: 'A sample GF plan',
      },
    },
  },
};

module.exports = {
  beforeEach(client) {
    client.reset();
    client.maximizeWindow();
  },

  'Manage Subscriptions page test': (client) => {
    const manageSubscription = client.page.manageSubscriptions();

    manageSubscription.navigate();
    manageSubscription.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    manageSubscription.expect.element('@billEstimate').to.be.present;

    manageSubscription.expect.element('@pageTitle').text.to.equal('Manage subscriptions');
    manageSubscription.assert.count('@subscription', 8);

    const selectElement = (a, b, c) => manageSubscription.expect.element(manageSubscription.dynEl(a, b, c));

    manageSubscription.getAttributeForEach('@subscriptionRowTitle', 'title', (lines) => {
      const findIndex = name => lines.find(({ value }) => value === name).index + 1;

      const jiraCoreId = findIndex('JIRA Core');
      const strideId = findIndex('Stride');
      const jiraSoftId = findIndex('JIRA Software');
      const bitBucketId = findIndex('Bitbucket');

      STRIDE_ELEMENT_ID = strideId; // auto update stride position

      selectElement('@subscriptionRow', jiraCoreId, '.ak-product-logo').to.have.attribute('title').equals('JIRA Core');
      selectElement('@subscriptionRow', strideId, '.ak-product-logo').to.have.attribute('title').equals('Stride');

      const freeTrailLozenge = selectElement('@subscriptionRow', jiraCoreId, '.entitlement-lozenge');
      freeTrailLozenge.text.to.match(new RegExp('^FREE TRIAL$', 'i'));
      const stdLozenge = selectElement('@subscriptionRow', strideId, '.entitlement-lozenge');
      stdLozenge.text.to.match(new RegExp('^STANDARD$', 'i'));

      selectElement('@productUsage', strideId, 1).text.to.equal('500 users');
      selectElement('@productUsage', jiraSoftId, 1).text.to.equal('301 users');
      selectElement('@productUsage', bitBucketId, 1).text.to.equal('742 users');
      selectElement('@productUsage', bitBucketId, 2).text.to.equal('30 gigabytes');
      selectElement('@productUsage', bitBucketId, 3).text.to.equal('50 build minutes');

      selectElement('@billedOn', strideId, '.billingDateLabel').text.to.equal('');
      const billedOnLozenge = selectElement('@billedOn', strideId, '.payload');
      billedOnLozenge.text.to.match(new RegExp('^DOWNGRADING TO FREE ON JUN 2$', 'i'));

      selectElement('@subscriptionRow', jiraCoreId, '.price').text.to.equal('Free until Dec 17');
      selectElement('@subscriptionRow', strideId, '.price').text.to.equal('Free');
      selectElement('@subscriptionRow', jiraSoftId, '.price').text.to.equal('$750');
      selectElement('@subscriptionRow', bitBucketId, '.price').text.to.equal('$3,720');

      selectElement('@priceEstimation', bitBucketId, '').text.to.equal('Price estimate\n$3,720');
    });
  },

  'Manage Subscriptions Stride Change Edition - with existing billing details': (client) => {
    const manageSubscription = client.page.manageSubscriptions();

    manageSubscription.navigate();
    manageSubscription.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    manageSubscription.click(manageSubscription.dynEl('@actionsButton', STRIDE_ELEMENT_ID));
    manageSubscription.click('@changeEditionButton');

    manageSubscription.waitForElementVisible(manageSubscription.section.changeEdition.selector, CHUNK_AWAIT_TIMEOUT);
    manageSubscription.expect.section('@changeEdition').to.be.visible;

    const changeEdition = manageSubscription.section.changeEdition;
    changeEdition.api.useXpath();

    // go to payment details page
    changeEdition.click(changeEdition.dynEl('@editionBlockButton', 3));

    changeEdition.api.useCss();

    manageSubscription.waitForElementNotPresent(manageSubscription.section.changeEdition.selector, CHUNK_AWAIT_TIMEOUT);
    manageSubscription.section.notification.assertMessage('success');

    client.assert.title('Billing – Manage subscriptions');
  },

  'Manage Subscriptions Stride Change Edition - without existing billing details': (client) => {
    client.mockRespondWith('billing/billing-details', {
      status: 200,
      method: 'GET',
      body: Object.assign({}, billingDetailsMock, {
        creditCard: {}
      })
    });

    const manageSubscription = client.page.manageSubscriptions();

    manageSubscription.navigate();
    manageSubscription.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    manageSubscription.click(manageSubscription.dynEl('@actionsButton', STRIDE_ELEMENT_ID));
    manageSubscription.click('@changeEditionButton');

    manageSubscription.waitForElementVisible(manageSubscription.section.changeEdition.selector, CHUNK_AWAIT_TIMEOUT);
    manageSubscription.expect.section('@changeEdition').to.be.visible;

    const changeEdition = manageSubscription.section.changeEdition;
    changeEdition.api.useXpath();

    // go to payment details page
    changeEdition.click(changeEdition.dynEl('@editionBlockButton', 3));

    changeEdition.api.useCss();

    client.assert.urlContains('/paymentdetails');
  },

  'Manage Subscriptions Stride Downgrade': (client) => {
    const manageSubscription = client.page.manageSubscriptions();

    manageSubscription.navigate();
    manageSubscription.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    // click on change edition button

    manageSubscription.click(manageSubscription.dynEl('@actionsButton', STRIDE_ELEMENT_ID));
    manageSubscription.click('@changeEditionButton');

    manageSubscription.waitForElementVisible(manageSubscription.section.changeEdition.selector, CHUNK_AWAIT_TIMEOUT);
    manageSubscription.expect.section('@changeEdition').to.be.visible;

    const changeEdition = manageSubscription.section.changeEdition;
    changeEdition.api.useXpath();

    client.assert.count(changeEdition.elements.editionBlocks.selector, 3);

    // downgrade
    changeEdition.click(changeEdition.dynEl('@editionBlockButton', 1));

    changeEdition.api.useCss();

    const downgrade = changeEdition.section.downgrade;
    const survey = downgrade.section.survey;

    changeEdition.waitForElementVisible(downgrade.selector, CHUNK_AWAIT_TIMEOUT);

    survey.assert.count('@rows', 3);
    survey.expect.element('@comment').not.to.be.present;

    survey.click(survey.dynEl('@row', 3));
    survey.expect.element('@comment').to.be.present;
    survey.setFormValue('@comment', 'I\'m a potato.');

    survey.click(survey.dynEl('@row', 1));
    survey.expect.element('@comment').not.to.be.present;

    survey.click('@submit');

    changeEdition.waitForElementNotPresent(downgrade.selector, CHUNK_AWAIT_TIMEOUT);
    manageSubscription.section.notification.assertMessage('success');
    manageSubscription.expect.section('@changeEdition').not.to.be.present;
  },

  'Manage Subscriptions Stride Downgrade - fail': (client) => {
    client.mockRespondWith('billing/entitlements/b7db7827-e2ac-492e-a3bb-e1c310764043/edition/free', {
      status: 500,
      method: 'PUT'
    });

    const manageSubscription = client.page.manageSubscriptions();

    manageSubscription.navigate();
    manageSubscription.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    manageSubscription.click(manageSubscription.dynEl('@actionsButton', STRIDE_ELEMENT_ID));
    manageSubscription.click('@changeEditionButton');

    manageSubscription.waitForElementVisible(manageSubscription.section.changeEdition.selector, CHUNK_AWAIT_TIMEOUT);

    const changeEdition = manageSubscription.section.changeEdition;
    changeEdition.api.useXpath();

    // downgrade
    changeEdition.click(changeEdition.dynEl('@editionBlockButton', 1));

    changeEdition.api.useCss();

    const downgrade = changeEdition.section.downgrade;
    const survey = downgrade.section.survey;

    changeEdition.waitForElementVisible(downgrade.selector, CHUNK_AWAIT_TIMEOUT);

    survey.click('@submit');

    // still on downgrade
    changeEdition.waitForElementVisible(downgrade.selector, CHUNK_AWAIT_TIMEOUT);
    manageSubscription.section.notification.assertMessage('error');
  },

  'Manage Subscriptions Deactivate with multiple products': (client) => {
    const manageSubscription = client.page.manageSubscriptions();

    manageSubscription.navigate();
    manageSubscription.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    manageSubscription.click(manageSubscription.dynEl('@actionsButton', STRIDE_ELEMENT_ID));
    manageSubscription.click('@unsubscribeButton');

    manageSubscription.waitForElementVisible(manageSubscription.section.deactivate.selector, CHUNK_AWAIT_TIMEOUT);
    manageSubscription.expect.section('@deactivate').to.be.visible;
    client.assert.title('Billing – Delete');

    const deactivate = manageSubscription.section.deactivate;
    const survey = deactivate.section.survey;

    deactivate.expect.element('@confirmation').to.be.visible;
    deactivate.expect.section('@survey').not.to.be.present;

    deactivate.setFormValue('@confirmation', 'delete');

    deactivate.expect.section('@survey').to.be.visible;

    survey.assert.count('@rows', 4);
    survey.expect.element('@comment').not.to.be.present;

    survey.click(survey.dynEl('@row', 4));
    survey.expect.element('@comment').to.be.present;
    survey.setFormValue('@comment', 'I\'m a potato.');

    survey.click(survey.dynEl('@row', 1));
    survey.expect.element('@comment').not.to.be.present;

    survey.click('@submit');

    manageSubscription.expect.section('@deactivate').not.to.be.present;
    manageSubscription.section.notification.assertMessage('success');
    client.assert.title('Billing – Manage subscriptions');
  },

  'Manage Subscriptions Deactivate with only one product': (client) => {
    client.mockEntitlementGroup({
      status: 200,
      method: 'GET',
      body: Object.assign({}, entitlementGroupMock, {
        entitlements: [entitlementGroupMock.entitlements[0]],
      })
    });

    const manageSubscription = client.page.manageSubscriptions();

    manageSubscription.navigate();
    manageSubscription.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    manageSubscription.click(manageSubscription.dynEl('@actionsButton', 1));
    manageSubscription.click('@deleteButton');

    const redirect = manageSubscription.section.redirect;

    manageSubscription.waitForElementVisible(redirect.selector, CHUNK_AWAIT_TIMEOUT);
    client.assert.title('Billing – Manage subscriptions');

    redirect.expect.element('@continueButton').to.be.visible;

    redirect.click('@continueButton');

    manageSubscription.waitForElementNotPresent(redirect.selector, CHUNK_AWAIT_TIMEOUT);
  },

  'Manage Subscriptions Stride Comparison table': (client) => {
    const manageSubscription = client.page.manageSubscriptions();

    manageSubscription.navigate();
    manageSubscription.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    manageSubscription.click(manageSubscription.dynEl('@actionsButton', STRIDE_ELEMENT_ID));
    manageSubscription.click('@changeEditionButton');

    manageSubscription.waitForElementVisible(manageSubscription.section.changeEdition.selector, CHUNK_AWAIT_TIMEOUT);
    manageSubscription.expect.section('@changeEdition').to.be.visible;

    const changeEdition = manageSubscription.section.changeEdition;

    changeEdition.expect.section('@comparisonTable').not.to.be.present;
    changeEdition.click('@showComparisonTable');
    changeEdition.expect.section('@comparisonTable').to.be.present;
  },

  'Manage Subscriptions Stride Add billing details CTA': (client) => {
    client.mockBillingDetails({
      status: 200,
      method: 'GET',
      body: Object.assign({}, billingDetailsMock, {
        creditCard: {}
      })
    });
    client.mockEntitlementGroup({
      status: 200,
      method: 'GET',
      body: Object.assign({}, entitlementGroupMock, {
        entitlements: [
          Object.assign(
            {},
            entitlementGroupMock.entitlements.find(({ productKey }) => productKey === 'hipchat.cloud'), {
              trialEndDate: '2042-12-17',
              editionTransitions: ['DOWNGRADE']
            }
          )]
      })
    });

    const manageSubscription = client.page.manageSubscriptions();
    manageSubscription.navigate();
    manageSubscription.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);
    manageSubscription.waitForElementVisible('@addPaymentDetailsButton', CHUNK_AWAIT_TIMEOUT);

    manageSubscription.click('@addPaymentDetailsButton'); // must exists on the page

    client.page.paymentDetails().waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);
    client.assert.urlContains('/paymentdetails');
  },

  'Manage Subscriptions with inactive products': (client) => {
    client.mockEntitlementGroup({
      status: 200,
      method: 'GET',
      body: Object.assign({}, entitlementGroupMock, {
        entitlements: entitlementGroupMock.entitlements.map(item => ({ ...item, suspended: true }))
      })
    });

    const manageSubscription = client.page.manageSubscriptions();
    manageSubscription.navigate();
    manageSubscription.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);
    manageSubscription.expect.element('@reactivationLink').to.be.present.before(CHUNK_AWAIT_TIMEOUT);
  },

  'Manage Subscriptions with annual periods': (client) => {
    client.mockBillEstimate({
      status: 200,
      method: 'GET',
      body: Object.assign({}, billEstimateMock, {
        nextBillingPeriod: {
          startDate: '2018-09-21',
          endDate: '2019-09-21',
          renewalFrequency: 'ANNUAL',
        }
      })
    });

    const manageSubscription = client.page.manageSubscriptions();
    manageSubscription.navigate();

    manageSubscription.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    const selectElement = (a, b, c) => manageSubscription.expect.element(manageSubscription.dynEl(a, b, c));

    manageSubscription.getAttributeForEach('@subscriptionRowTitle', 'title', (lines) => {
      const findIndex = name => lines.find(({ value }) => value === name).index + 1;

      const jiraCoreId = findIndex('JIRA Core');
      const jiraServiceDeskId = findIndex('JIRA Service Desk');
      const strideId = findIndex('Stride');
      const jiraSoftId = findIndex('JIRA Software');

      const billedOnLozenge = selectElement('@billedOn', strideId, '.payload');
      billedOnLozenge.text.to.match(new RegExp('^DOWNGRADING TO FREE ON JUN 2, 2017$', 'i'));

      selectElement('@billedOn', jiraServiceDeskId, '.payload').text.to.equal('Dec 17, 2016');
      selectElement('@billedOn', jiraSoftId, '.payload').text.to.equal('Dec 17, 2017');

      selectElement('@subscriptionRow', jiraCoreId, '.price').text.to.equal('Free until Dec 17, 2016');
    });

    manageSubscription.click(manageSubscription.dynEl('@actionsButton', STRIDE_ELEMENT_ID));
    manageSubscription.click('@changeEditionButton');

    manageSubscription.waitForElementVisible(manageSubscription.section.changeEdition.selector, CHUNK_AWAIT_TIMEOUT);
    manageSubscription.expect.section('@changeEdition').to.be.visible;

    const changeEdition = manageSubscription.section.changeEdition;

    changeEdition.api.useXpath();
    manageSubscription.expect.element(changeEdition.dynEl('@editionBlockUsers', 2)).text.to.contain('year');
    manageSubscription.expect.element(changeEdition.dynEl('@editionBlockCost', 2)).text.to.contain('30');
    changeEdition.api.useCss();
  },

  // TODO: Port organization tests to cypress, query string does not work anymore
  // 'Manage Subscriptions with only one product for organization': (client) => {
  //   client.mockEntitlementGroup({
  //     status: 200,
  //     method: 'GET',
  //     body: Object.assign({}, entitlementGroupMock, {
  //       entitlements: [identityManagerEntitlement],
  //     })
  //   });
  //
  //   const manageSubscription = client.page.manageSubscriptions();
  //   manageSubscription.navigate({ org: '123' });
  //   manageSubscription.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);
  //
  //   manageSubscription.click(manageSubscription.dynEl('@actionsButton', 1));
  //   manageSubscription.click('@unsubscribeButton');
  //
  //   manageSubscription.waitForElementVisible(manageSubscription.section.deactivate.selector, CHUNK_AWAIT_TIMEOUT);
  //   manageSubscription.expect.section('@deactivate').to.be.visible;
  //   client.assert.title('Billing – Unsubscribe');
  //
  //   const deactivate = manageSubscription.section.deactivate;
  //   const survey = deactivate.section.survey;
  //
  //   deactivate.expect.element('@confirmation').to.be.visible;
  //   deactivate.expect.section('@survey').not.to.be.present;
  //
  //   deactivate.setFormValue('@confirmation', 'unsubscribe');
  //
  //   deactivate.expect.section('@survey').to.be.visible;
  //
  //   survey.assert.count('@rows', 4);
  //   survey.expect.element('@comment').not.to.be.present;
  //
  //   survey.click(survey.dynEl('@row', 4));
  //   survey.expect.element('@comment').to.be.present;
  //   survey.setFormValue('@comment', 'I\'m a potato.');
  //
  //   survey.click(survey.dynEl('@row', 1));
  //   survey.expect.element('@comment').not.to.be.present;
  //
  //   survey.click('@submit');
  //
  //   manageSubscription.expect.section('@deactivate').not.to.be.present;
  //   manageSubscription.section.notification.assertMessage('success');
  //   client.assert.title('Billing – Manage subscriptions');
  // },

  // TODO: Port organization tests to cypress, query string does not work anymore
  // 'Manage Subscriptions can resubscribe organization products': (client) => {
  //   client.mockEntitlementGroup({
  //     status: 200,
  //     method: 'GET',
  //     body: Object.assign({}, entitlementGroupMock, {
  //       entitlements: [{ ...identityManagerEntitlement, status: 'INACTIVE' }]
  //     })
  //   });
  //
  //   const manageSubscription = client.page.manageSubscriptions();
  //   manageSubscription.navigate({ org: '123' });
  //   manageSubscription.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);
  //
  //   client.mockEntitlementGroup({
  //     status: 200,
  //     method: 'GET',
  //     body: Object.assign({}, entitlementGroupMock, {
  //       entitlements: [identityManagerEntitlement]
  //     })
  //   });
  //
  //   client.mockBillEstimate({
  //     status: 200,
  //     method: 'GET',
  //     body: Object.assign({}, billEstimateMock, {
  //       lines: [{
  //         productKey: 'com.atlassian.identity-manager',
  //         amountBeforeTax: 1000,
  //         productName: 'Atlassian Access (Cloud)',
  //         usage: [
  //           {
  //             amountBeforeTax: 1000,
  //             taxRate: 10,
  //             description: 'Atlassian Access',
  //             units: 820,
  //             unitCode: 'users',
  //             taxAmount: 100,
  //             amountWithTax: 1100
  //           }
  //         ],
  //         taxAmount: 100,
  //         amountWithTax: 1100,
  //         entitlementId: 'a1c4dc4d-259b-493a-a179-6377cdfd414b'
  //       }]
  //     })
  //   });
  //
  //   manageSubscription.click('@resubscribeButton');
  //
  //   manageSubscription.waitForElementVisible('.section-active', CHUNK_AWAIT_TIMEOUT);
  //   manageSubscription.assert.count('@subscription', 1);
  //   manageSubscription.section.notification.assertMessage('success');
  //   manageSubscription.expect.element('.subscription-line .price').text.to.equal('$1000');
  // },

  'Manage Subscriptions cancel subscription for all products': (client) => {
    const manageSubscription = client.page.manageSubscriptions();
    manageSubscription.navigate();
    manageSubscription.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);
    manageSubscription.expect.element('@cancelSubscriptionButton').to.be.present;
    manageSubscription.click('@cancelSubscriptionButton');

    const redirect = manageSubscription.section.redirect;

    manageSubscription.waitForElementVisible(redirect.selector, CHUNK_AWAIT_TIMEOUT);
    client.assert.title('Billing – Manage subscriptions');

    redirect.expect.element('@continueButton').to.be.visible;

    redirect.click('@continueButton');

    manageSubscription.waitForElementNotPresent(redirect.selector, CHUNK_AWAIT_TIMEOUT);
  },

  'Should hide billing details for the management account': (client) => {
    client.mockBillingDetails({
      status: 200,
      method: 'GET',
      body: Object.assign({}, billingDetailsMock, {
        managedByPartner: true
      })
    });
    const manageSubscription = client.page.manageSubscriptions();

    manageSubscription.navigate();
    manageSubscription.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    manageSubscription.expect.element('@billEstimate').not.to.be.present;

    manageSubscription.expect.element(manageSubscription.dynEl('priceEstimation', 0, '')).not.to.be.present;
    manageSubscription.expect.element(manageSubscription.dynEl('priceEstimation', 1, '')).not.to.be.present;
  },

  // TODO: Port organization tests to cypress, query string does not work anymore
  // 'Manage Subscriptions with Atlassian Access and feature flag on': (client) => {
  //   client.mockEntitlementGroup({
  //     status: 200,
  //     method: 'GET',
  //     body: Object.assign({}, entitlementGroupMock, {
  //       entitlements: [identityManagerEntitlement],
  //     })
  //   });
  //
  //   client.mockBillEstimate({
  //     status: 200,
  //     method: 'GET',
  //     body: Object.assign({}, billEstimateMock, {
  //       ...atlassianAccessExtras
  //     })
  //   });
  //
  //   console.log(Object.assign({}, billEstimateMock, {
  //     ...atlassianAccessExtras
  //   }));
  //
  //   const manageSubscription = client.page.manageSubscriptions();
  //   manageSubscription.navigate({ org: '123' });
  //   manageSubscription.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);
  //
  //   manageSubscription.expect.element('@grandfatheringInfo').to.be.present;
  //   manageSubscription.expect.element('@trelloWarn').to.be.present;
  //   manageSubscription.expect.element('@usageInlineMessage').to.be.present;
  // },

  'Manage Subscriptions with Atlassian Access and feature flag off': (client) => {
    client.mockMetadata({
      status: 200,
      method: 'GET',
      body: Object.assign({}, metadataMock, {
        featureFlags: {
          'atlassian-access-warnings': false
        }
      })
    });

    client.mockEntitlementGroup({
      status: 200,
      method: 'GET',
      body: Object.assign({}, entitlementGroupMock, {
        entitlements: [identityManagerEntitlement],
      })
    });

    client.mockBillEstimate({
      status: 200,
      method: 'GET',
      body: Object.assign({}, billEstimateMock, {
        ...atlassianAccessExtras
      })
    });

    console.log(Object.assign({}, billEstimateMock, {
      ...atlassianAccessExtras
    }));

    const manageSubscription = client.page.manageSubscriptions();
    manageSubscription.navigate({ org: '123' });
    manageSubscription.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);
    manageSubscription.expect.element('@grandfatheringInfo').to.not.be.present;
    manageSubscription.expect.element('@trelloWarn').to.not.be.present;
  },
};
