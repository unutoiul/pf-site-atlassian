
const CHUNK_AWAIT_TIMEOUT = 10000;

function checkProduct({
  discoverNewProducts,
  productKey,
  name,
  tag,
  link,
  getStartedLabel,
  price,
  pricingLink,
  button
}) {
  discoverNewProducts.expect.element(discoverNewProducts.dynEl('@productRow', productKey)).be.visible;
  const expect = element =>
    discoverNewProducts.expect.element(discoverNewProducts.dynEl('@productRow', productKey, element));
  expect('div').to.have.attribute('data-logroot', productKey);
  expect('[data-test="product-title"]').text.to.contain(name);
  expect('[data-test="product-tag"]').text.to.contain(tag);
  expect('[data-test="learn-more-link"]').to.have.attribute('href', link);
  expect('[data-test="get-started-label"]').text.to.equal(getStartedLabel);
  expect('[data-test="price"]').text.to.equal(price);
  expect('[data-test="full-price-link"]').to.have.attribute('href', pricingLink);
  expect('button').text.to.equal(button);
}

module.exports = {
  beforeEach(client) {
    client.reset();
    client.maximizeWindow();
  },

  'Should render a product list': (client) => {
    const discoverNewProducts = client.page.discoverNewProducts();

    discoverNewProducts.navigate();
    discoverNewProducts.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    discoverNewProducts.expect.element('@title').text.to.equal('Atlassian products');
    discoverNewProducts.expect.element('@productList').to.be.visible;

    checkProduct({
      discoverNewProducts,
      productKey: 'jira-core.ondemand',
      name: 'Jira Core',
      tag: 'TRACK',
      link: 'https://www.atlassian.com/software/jira/core',
      getStartedLabel: 'Get started with 10 users',
      price: '$10 / month',
      pricingLink: 'https://www.atlassian.com/software/jira/core/pricing?tab=cloud',
      button: 'Free trial'
    });

    checkProduct({
      discoverNewProducts,
      productKey: 'jira-servicedesk.ondemand',
      name: 'Jira Service Desk',
      tag: 'IT & HELPDESK',
      link: 'https://www.atlassian.com/software/jira/service-desk',
      getStartedLabel: 'Get started with 3 agents',
      price: '$10 / month',
      pricingLink: 'https://www.atlassian.com/software/jira/service-desk/pricing?tab=cloud',
      button: 'Free trial'
    });

    checkProduct({
      discoverNewProducts,
      productKey: 'hipchat.cloud',
      name: 'Stride',
      tag: 'CHAT',
      link: 'https://www.stride.com',
      getStartedLabel: 'Get started now',
      price: 'Free for all teams',
      pricingLink: 'https://www.stride.com/pricing',
      button: 'Start now'
    });
  },

  'Should application list': (client) => {
    const discoverNewProducts = client.page.discoverNewProducts();

    discoverNewProducts.navigate();
    discoverNewProducts.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    discoverNewProducts.expect.element(discoverNewProducts.dynEl('@productGroup', 'confluence'))
      .to.be.visible;
    discoverNewProducts.expect
      .element(discoverNewProducts.dynEl('@productGroup', 'confluence', 'h3'))
      .text.to.equal('Recommended Confluence apps');

    checkProduct({
      discoverNewProducts,
      productKey: 'team.calendars.confluence.ondemand',
      name: 'Team Calendars for Confluence',
      tag: 'MESSAGING',
      link: 'https://www.atlassian.com/software/confluence/team-calendars',
      getStartedLabel: 'Get started with 10 users',
      price: '$10 / month',
      pricingLink: 'https://www.atlassian.com/software',
      button: 'Free trial'
    });

    checkProduct({
      discoverNewProducts,
      productKey: 'com.atlassian.confluence.plugins.confluence-questions.ondemand',
      name: 'Questions for Confluence',
      tag: 'MESSAGING',
      link: 'https://www.atlassian.com/software/confluence/questions',
      getStartedLabel: 'Get started with 10 users',
      price: '$10 / month',
      pricingLink: 'https://www.atlassian.com/software',
      button: 'Free trial'
    });

    discoverNewProducts.expect.element(discoverNewProducts.dynEl('@productGroup', 'jira'))
      .to.be.visible;
    discoverNewProducts.expect
      .element(discoverNewProducts.dynEl('@productGroup', 'jira', 'h3'))
      .text.to.equal('Recommended Jira apps');

    checkProduct({
      discoverNewProducts,
      productKey: 'bonfire.jira.ondemand',
      name: 'Capture for Jira',
      tag: 'PROJECT MANAGEMENT',
      link: 'https://www.atlassian.com/software/jira/capture',
      getStartedLabel: 'Get started with 10 users',
      price: '$10 / month',
      pricingLink: 'https://www.atlassian.com/software',
      button: 'Free trial'
    });

    checkProduct({
      discoverNewProducts,
      productKey: 'com.radiantminds.roadmaps-jira.ondemand',
      name: 'Portfolio for Jira',
      tag: 'PROJECT MANAGEMENT',
      link: 'https://marketplace.atlassian.com/plugins/com.radiantminds.roadmaps-jira',
      getStartedLabel: 'Get started with 10 users',
      price: '$10 / month',
      pricingLink: 'https://www.atlassian.com/software',
      button: 'Free trial'
    });
  },

  'Should get error on whole page': (client) => {
    const discoverNewProducts = client.page.discoverNewProducts();

    client.mockRecommendedProducts({ status: 500, method: 'GET' });
    discoverNewProducts.navigate();

    discoverNewProducts.expect.element('@productList').to.be.not.present;
    discoverNewProducts.expect.element('@globalError').to.be.visible.before(CHUNK_AWAIT_TIMEOUT);
  }
};
