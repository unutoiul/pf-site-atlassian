module.exports = {
  url() {
    return `${this.api.globals.serverUrl}?disableResponsive`;
  },
  elements: {
    //menu
    overview: '#lhstab-overview',
    paymentdetails: '#lhstab-paymentdetails',
    history: '#lhstab-history',
    applications: '#lhstab-applications',
  }
};
