const { dynamicElement, navigateWithQueryParams } = require('../util/helpers');

module.exports = {
  url() {
    return `${this.api.globals.serverUrl}/addapplication?disableResponsive`;
  },
  elements: {
    globalError: '.error.unavailable',
    page: '[data-logroot="discoverNewProducts"] .page-content',
    title: 'h3',
    productGroup: 'div[data-test="products-group:%s"]',
    productList: 'ul[data-test="product-list"]',
    productRow: 'li[data-test="product:%s"]'
  },
  commands: [{
    dynEl: dynamicElement,
    navigate: navigateWithQueryParams,
  }]
};
