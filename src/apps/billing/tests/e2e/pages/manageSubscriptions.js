const { dynamicElement, getAttributeForEach, navigateWithQueryParams } = require('../util/helpers');
const { byClassName } = require('../util/xpath');

const changeEdition = require('./manageSubscriptions/changeEdition');
const deactivate = require('./manageSubscriptions/deactivate');
const redirect = require('./manageSubscriptions/redirect');
const notification = require('./globalNotification');

const secondaryActionSelector = label => ({
  selector: `//${byClassName('secondaryActions')}//*[text()="${label}"]`,
  locateStrategy: 'xpath',
});

const primaryActionSelector = label => ({
  selector: `//${byClassName('mainActionButton')}//*[text()="${label}"]/ancestor::button`,
  locateStrategy: 'xpath',
});

module.exports = {
  url() {
    return `${this.api.globals.serverUrl}/applications?disableResponsive`;
  },
  elements: {
    page: '[data-logroot="manageSubscriptions"] .page-content',
    pageTitle: '.page-title',
    subscription: '.subscription-line',
    subscriptionRowTitle: '.subscription-line .ak-product-logo',
    subscriptionRow: '.subscription-line:nth-of-type(%s) %s',
    productUsage: '.subscription-line:nth-of-type(%s) .product-usage:nth-of-type(%s) .payload',
    billedOn: '.subscription-line:nth-of-type(%s) .billedOn %s',
    priceEstimation: '.subscription-line:nth-of-type(%s) .priceEstimation %s',

    mainActionButton: '.subscription-line:nth-of-type(%s) .mainActionButton',

    actionsButton: '.subscription-line:nth-of-type(%s) .secondaryActions',

    changeEditionButton: secondaryActionSelector('Change plan'),
    addPaymentDetailsButton: primaryActionSelector('Add billing details'),
    deleteButton: secondaryActionSelector('Delete'),
    unsubscribeButton: secondaryActionSelector('Unsubscribe'),
    resubscribeButton: '[data-logroot="Resubscribe"] button',

    reactivationLink: '[data-logroot="Resubscribe"] a',
    cancelSubscriptionButton: '.cancel-subscription-button',

    billEstimate: '.bill-estimate-summary',

    grandfatheringInfo: '[data-test="grandfathering-info"]',
    trelloWarn: '[data-test="trello-warn"]',
    usageInlineMessage: '[data-test="usage-inline-message"] button'
  },
  sections: {
    notification,
    changeEdition,
    deactivate,
    redirect,
  },
  commands: [{
    getAttributeForEach,
    dynEl: dynamicElement,
    navigate: navigateWithQueryParams,
  }]
};
