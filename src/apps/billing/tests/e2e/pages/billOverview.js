const { dynamicElement, navigateWithQueryParams } = require('../util/helpers');
const accountIsUnderManagement = require('./sections/accountUnderManagementPrompt');

module.exports = {
  url() {
    return `${this.api.globals.serverUrl}/overview?disableResponsive`;
  },
  elements: {
    globalError: '.error.unavailable',
    page: '[data-logroot="billOverview"] .page-content',
    modalMessage: '.update-contact-warning-modal',
    confirmButton: '.button-submit'
  },
  sections: {
    billEstimate: {
      selector: '.bill-estimate-widget',
      elements: {
        title: '.widget-title',
        total: '.price',
        endDate: '.date',
        detailLink: '.link',
        error: '.widget-error'
      }
    },
    paymentDetail: {
      selector: '.payment-detail-widget',
      elements: {
        title: '.widget-title',
        name: '.name',
        email: '.email',
        expiry: '.expiry',
        number: '.number',
        editLink: '.widgetEditLink',
        error: '.widget-error'
      }
    },
    billHistory: {
      selector: '.bill-history-widget',
      elements: {
        title: '.widget-title',
        invoices: '.invoice-line',
        invoiceRow: '.invoice-line:nth-child(%s) .%s',
        error: '.widget-error'
      },
      commands: [{
        dynEl: dynamicElement
      }]
    },
    invoiceContact: {
      selector: '.invoice-contact-widget',
      elements: {
        title: '.widget-title',
        email: '.email',
        myLink: '.myLink',
        updateMain: '.update-billing-contact',
        secondaryContact: '.manage-contacts',
        avatar: '.avatar',
        error: '.widget-error',
        editLink: '.widgetEditLink'
      }
    },
    paymentDetailsPrompt: {
      selector: '.paymentDetailsPrompt',
      elements: {
        button: '.prompt-actions a'
      }
    },
    accountIsUnderManagement,
  },
  commands: [{
    dynEl: dynamicElement,
    navigate: navigateWithQueryParams,
  }]
};
