const { setFormValue } = require('../../util/helpers');
const { selectOption } = require('../../util/AtlasKit');
const { assertAddressSupport, assertFieldsExist, assertFieldsDoNotExist } = require('../../assertions/addressFields');
const { xpath, nested, byClassName } = require('../../util/xpath');

module.exports = (formPrefix = '') => {
  const prefix = formPrefix ? `${formPrefix}.` : '';

  return {
    selector: '.address-fields',
    elements: xpath(nested({
      addressLineFull: `*[@name="${prefix}addressLineFull"]`,
      addressLine1: `*[@name="${prefix}addressLine1"]`,
      addressLine2: `*[@name="${prefix}addressLine2"]`,
      city: `*[@name="${prefix}city"]`,
      postcode: `*[@name="${prefix}postcode"]`,
      state: '*[@id="react-select-state-input"]',
      country: '*[@id="react-select-country-input"]',

      fullFormToggle: byClassName(`${formPrefix}OpenFullAddressForm`),
      notificationFormMessage: byClassName('form-message'),
      autocompleteResult: byClassName('autocomplete-result')
    })),
    commands: [{
      setFormValue,
      assertAddressSupport,
      assertFieldsExist,
      assertFieldsDoNotExist,
      selectOption
    }]
  };
};