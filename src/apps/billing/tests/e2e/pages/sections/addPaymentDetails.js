const {
  setFormValue, switchToIFrameBySelector, setIFrameFormValue,
  selectOption, dynamicElement
} = require('../../util/helpers');
const addressFields = require('./addressFields');
const { selectOption: selectAkOption } = require('../../util/AtlasKit');

module.exports = {
  selector: '.focusedTaskContent',
  elements: {

  },
  sections: {
    stepBillingAddress: {
      selector: 'div[data-test="stepBillingAddress"]',
      elements: {
        organisationName: 'input[name="organisationName"]',
        taxpayerId: 'input[name="taxpayerId"]',
        nextButton: 'button[type=submit]',
        backButton: 'button[type=submit]'
      },
      sections: {
        addressFields: addressFields(),
      }
    },
    stepBillingMethod: {
      selector: 'div[data-test="stepBillingMethod"]',
      elements: {
        paypalButton: '#paypal-button',
        paypalStatusEmail: 'p[data-test="paypal-status-email"]',
        nextButton: 'button[type=submit]',
        backButton: 'button.back-flow',
        creditCardNumberIFrame: '.gw-proxy-number',
        creditCardName: 'input#creditcard-name',
        expiryMonth: '//select[@id="creditcard-month"]',
        expiryYear: '//select[@id="creditcard-year"]',
        ccvIFrame: '.gw-proxy-securityCode',
        creditCardNameFieldError: '.creditcard-name-field-group > span.ak-error',
        creditCardErrorActions: '.creditCardErrorActions'
      },
      commands: [{
        setFormValue,
        switchToIFrameBySelector,
        setIFrameFormValue,
        selectOption,
        selectAkOption
      }]
    },
    stepOverview: {
      selector: 'div[data-test="stepOverview"]',
      elements: {
        address: 'p[data-test="address"]',
        taxTotal: '.tax-total',
        total: '.total-cost',
        chargeAdvise: 'p[data-test="charge-advise"]',
        nextButton: 'button[type=submit]',
        agreement: 'input[type="checkbox"]'
      },
      sections: {
        paymentMethod: {
          selector: 'div[data-test="payment-method"]',
          elements: {
            email: '.email',
            number: '.number',
            name: '.name',
            expiry: '.expiry',
          }
        },
        billEstimate: {
          selector: '.product-list',
          elements: {
            product: '.product-line',
            productRow: '.product-line:nth-child(%s) .product-%s'
          },
          commands: [{
            dynEl: dynamicElement
          }]
        }
      }
    },
  },
  commands: [{
    setFormValue,
    switchToIFrameBySelector,
    setIFrameFormValue,
    selectOption,
    selectAkOption
  }]
};
