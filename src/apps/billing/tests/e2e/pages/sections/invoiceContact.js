const {
  dynamicElement
} = require('../../util/helpers');

module.exports = {
  selector: '.invoice-contact-widget',
  elements: {
    email: '.email'
  },
  commands: [{
    dynEl: dynamicElement
  }]
};
