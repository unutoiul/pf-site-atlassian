const {
  dynamicElement
} = require('../../util/helpers');

const {
  assertSection
} = require('../../assertions/billEstimateSummary');

module.exports = {
  selector: '.bill-estimate-summary',
  elements: {
    title: '.title',
    product: '.product-line',
    productRow: '.product-line:nth-child(%s) .product-%s',
    priceRest: '.price-rest',
    tax: '.total-tax',
    total: '.total-cost',
    finePrint: '.fine-print',

    actionExpand: '.expand-bill',
    actionCollapse: '.collapse-bill'
  },
  sections: {
    finePrint: {
      selector: '.fine-print',
      elements: {
        link: {
          selector: 'a'
        }
      }
    },
  },
  commands: [{
    dynEl: dynamicElement,
    autoTest: assertSection
  }]
};
