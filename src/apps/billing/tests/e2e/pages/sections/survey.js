const { setFormValue } = require('../../util/helpers');
const {
  dynamicElement
} = require('../../util/helpers');

module.exports = {
  selector: '.survey-form',
  elements: {
    rows: '.ak-field-radio',
    row: '.ak-field-radio:nth-child(%s)',
    comment: '[name="survey.other"]',
    submit: '.button-submit',
    cancel: '.button-cancel',
  },
  commands: [{
    setFormValue,
    dynEl: dynamicElement,
  }]
};
