const {
  setFormValue, switchToIFrameBySelector, setIFrameFormValue,
  selectOption
} = require('../../util/helpers');
const { selectOption: selectAkOption } = require('../../util/AtlasKit');

module.exports = {
  selector: '.focusedTaskContent',
  elements: {
    paypalButton: '#paypal-button',
    paypalStatus: '[data-test="paypal-status-email"]',
    paypalStatusEmail: 'p[data-test="paypal-status-email"]',
    creditCardNumberIFrame: '.gw-proxy-number',
    creditCardName: 'input#creditcard-name',
    expiryMonth: '//select[@id="creditcard-month"]',
    expiryYear: '//select[@id="creditcard-year"]',
    ccvIFrame: '.gw-proxy-securityCode',
    buttonSubmit: '.button-submit',
    creditCardNameFieldError: '.creditcard-name-field-group > span.ak-error',
    creditCardErrorActions: '.creditCardErrorActions'
  },
  commands: [{
    setFormValue,
    switchToIFrameBySelector,
    setIFrameFormValue,
    selectOption,
    selectAkOption
  }]
};
