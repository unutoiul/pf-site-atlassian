const {
  setFormValue, switchToIFrameBySelector, setIFrameFormValue,
  selectOption
} = require('../util/helpers');
const { selectOption: selectAkOption } = require('../util/AtlasKit');
const { navigateWithQueryParams } = require('../util/helpers');

const billEstimateSummary = require('./sections/billEstimateSummary');
const addressFields = require('./sections/addressFields');
const accountIsUnderManagement = require('./sections/accountUnderManagementPrompt');
const editPaymentMethod = require('./sections/editPaymentMethod');
const addPaymentDetails = require('./sections/addPaymentDetails');
const notification = require('./globalNotification');

module.exports = {
  url() {
    return `${this.api.globals.serverUrl}/paymentdetails?disableResponsive`;
  },
  elements: {
    page: '[data-logroot="paymentDetails"] .page-content',
    pageTitle: '.page-title',
    creditCardNumberIFrame: '.gw-proxy-number',
    creditCardName: 'input#creditcard-name',
    expiryMonth: '//select[@id="creditcard-month"]',
    expiryYear: '//select[@id="creditcard-year"]',
    ccvIFrame: '.gw-proxy-securityCode',
    button: '.paymentDetailsSubmit',
    cc_button: '.cc-submit-button',
    editButton: '.paymentDetailsEdit',
    displayCCDetails: '.displayCreditCardDetails',
    editCCDetails: '.editCreditCardDetails',
    expiryFieldsDetails: '.expiry-fields',
    ccNumberDetails: '.ccNumberDetails > div',
    ccNameDetails: '.ccNameDetails > div',
    ccShadow: '.cc-shadow',
    organisationName: 'input[name="organisationName"]',
    taxpayerId: 'input[name="taxpayerId"]',
    invoiceContactEmail: 'input[name="invoiceContactEmail"]',
    retryButton: '.creditCardErrorActions > button',
    paymentRequired: '.payment-required',
    changeRenewalFrequency: '.change-renewal-frequency',
    changeRenewalFrequencyLink: '.change-renewal-frequency__link',

    convertToAnnualWizard: '.convert-to-annual-wizard',
    convertToAnnualNext: '.convert-to-annual-wizard [type="submit"]:not([disabled])',
    convertToAnnualStep2: '.convert-to-annual-wizard [data-step="2"]',
    convertToAnnualOptionCC: '.convert-to-annual-wizard .option-credit-card',

    editPaymentDetails: 'a.widgetEditLink',
    focusedTaskCloseButton: '.bux-focused-drawer button[aria-haspopup=true]',

    addPaymentDetailsPrompt: 'div[data-test=payment-details-prompt] a'
  },
  sections: {
    billEstimateSummary,
    addressFields: addressFields(),
    accountIsUnderManagement,
    paymentMethodPanel: {
      selector: 'div[data-test="payment-method-panel"]',
      elements: {
        title: 'h2'
      },
      sections: {
        creditCard: {
          selector: 'div[data-test="credit-card"]',
          elements: {
            name: '.name',
            number: '.number',
            expiry: '.expiry',
          }
        },
        paypal: {
          selector: 'div[data-test="paypal"]',
          elements: {
            email: '.email'
          }
        }
      }
    },
    editPaymentMethod,
    addPaymentDetails,
    notification
  },
  commands: [{
    setFormValue,
    switchToIFrameBySelector,
    setIFrameFormValue,
    selectOption,
    selectAkOption,
    navigate: navigateWithQueryParams,
  }]
};
