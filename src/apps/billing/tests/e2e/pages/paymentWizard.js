const { setFormValue } = require('../util/helpers');

const paymentDetails = require('./paymentDetails');

const billEstimateSummary = require('./sections/billEstimateSummary');
const notification = require('./globalNotification');

module.exports = {
  url() {
    return `${this.api.globals.serverUrl}/paymentwizard?disableResponsive`;
  },
  elements: {
    page: '[data-logroot="paymentWizard"] .page-content',
    pageTitle: '.page-title',
    pageSubTitle: '.page-subtitle',
    nextButton: '.wizard-next',
    backButton: '.wizard-back',
    cancelButton: '.wizard-cancel'
  },
  sections: {
    notification,
    paymentDetails: Object.assign({
      selector: '.payment-details-form'
    }, paymentDetails),
    billEstimateSummary
  },
  commands: [{
    setFormValue,
    clickNext() {
      this.section.notification.close();
      this.click('@nextButton');
    }
  }]
};
