const survey = require('../sections/survey');
const { dynamicElement } = require('../../util/helpers');

module.exports = {
  selector: '.downgradeFocusedTask',
  sections: {
    survey
  },
  commands: [{
    dynEl: dynamicElement
  }]
};