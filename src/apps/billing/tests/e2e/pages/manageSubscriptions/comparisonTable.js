const {
  dynamicElement
} = require('../../util/helpers');

module.exports = {
  selector: '.banana-comparison-table',
  elements: {
    row: '.comparison-row',
    supportedFlag: '.icon-supported',
    feature: '.comparison-row:nth-child(%s)>span:nth-child(%s)',
  },
  commands: [{
    dynEl: dynamicElement,
  }]
};