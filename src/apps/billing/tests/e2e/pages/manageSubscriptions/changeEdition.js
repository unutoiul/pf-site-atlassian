const paymentDetails = require('../paymentDetails');
const downgrade = require('./downgrade');
const comparisonTable = require('./comparisonTable');
const { dynamicElement } = require('../../util/helpers');
const { xpath, nested, byClassName } = require('../../util/xpath');

module.exports = {
  selector: '.changeEditionFocusedTask',

  elements: xpath(nested({
    editionBlocks: `${byClassName('editionBlock')}`,
    editionBlock: `${byClassName('editionBlock')}[%s]`,
    editionBlockLabel: `${byClassName('editionBlock')}[%s]/h2`,
    editionBlockUsers: `${byClassName('editionBlock')}[%s]/${byClassName('users')}`,
    editionBlockCost: `${byClassName('editionBlock')}[%s]/${byClassName('cost')}`,
    editionBlockFeatures: `${byClassName('editionBlock')}[%s]/${byClassName('features')}`,
    editionBlockButton: `${byClassName('editionBlock')}[%s]//button`,

    showComparisonTable: `${byClassName('comparisonTableLink')}`,
  })),

  sections: {
    paymentDetails: Object.assign({
      selector: '.payment-details-form'
    }, paymentDetails),
    downgrade,
    comparisonTable
  },
  commands: [{
    dynEl: dynamicElement
  }]
};