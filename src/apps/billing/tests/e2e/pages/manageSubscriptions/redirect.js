const { dynamicElement } = require('../../util/helpers');
const { setFormValue } = require('../../util/helpers');

module.exports = {
  selector: '.redirectCancelModal',
  elements: {
    continueButton: '.button-submit',
    cancelButton: '.button-cancel',
  },
  commands: [{
    dynEl: dynamicElement,
    setFormValue,
  }],
};