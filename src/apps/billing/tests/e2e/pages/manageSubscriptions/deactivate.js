const survey = require('../sections/survey');
const { dynamicElement } = require('../../util/helpers');
const { setFormValue } = require('../../util/helpers');

module.exports = {
  selector: '.deactivateFocusedTask',
  elements: {
    confirmation: '.deactivateConfirmation',
  },
  sections: {
    survey,
  },
  commands: [{
    dynEl: dynamicElement,
    setFormValue,
  }],
};