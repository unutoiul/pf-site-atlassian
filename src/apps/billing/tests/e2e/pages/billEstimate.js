const { dynamicElement, getTextForEach } = require('../util/helpers');
const accountIsUnderManagement = require('./sections/accountUnderManagementPrompt');

module.exports = {
  url() {
    return `${this.api.globals.serverUrl}/estimate?disableResponsive`;
  },
  elements: {
    page: '[data-logroot="billEstimate"] .page-content',
    pageTitle: '.page-title',
    product: '.product-line',
    productRow: '.product-line:nth-child(%s) .product-%s',
    productRowDescription: '.product-line .product-description',
    productUsage: '.product-line:nth-child(%s) .product-usage:nth-child(%s)',
    tax: '.total-tax',
    taxTotal: '.tax-total',
    total: '.total-cost',
    error: '.error.unavailable',
    paymentRequiredError: '.error.unavailable .payment-required',
    autoChargedLabel: '.auto-charged-label'
  },
  sections: {
    accountIsUnderManagement
  },
  commands: [{
    getTextForEach,
    dynEl: dynamicElement
  }]
};
