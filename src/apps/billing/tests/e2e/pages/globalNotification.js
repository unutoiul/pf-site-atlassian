const {
  assertMessage
} = require('../assertions/notification');

const closeButtonSelector = '.global-notifications button:first-of-type';

module.exports = {
  selector: '.global-notifications',
  elements: {
    notification: '[role=alert] [data-flag-e2e]',
    closeButton: closeButtonSelector
  },
  commands: [{
    close() {
      const closeCycle = () => (
        this.api.element('css selector', closeButtonSelector, (result) => {
          if (result.value && result.value.ELEMENT) {
            this.click(closeButtonSelector);
            this.api.pause(500);
            closeCycle();
          }
        })
      );
      closeCycle();
    },
    assertMessage
  }]
};