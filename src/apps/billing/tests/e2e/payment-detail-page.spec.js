const billingDetailsMock = require('../../mock-server/bux/api/billing/billing-details-get.json');
const billingEstimateMock = require('../../mock-server/bux/api/billing/bill-estimate-get.json');

const CHUNK_AWAIT_TIMEOUT = 10000;
const ELEMENT_AWAIT_TIMEOUT = 10000;

const NO_AUTOCOMPLETE_PRESENT = 0;

module.exports = {
  beforeEach(client) {
    client.reset();
    client.maximizeWindow();
  },

  'billing details page default': (client) => {
    const paymentDetails = client.page.paymentDetails();

    paymentDetails.navigate();
    paymentDetails.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    client.page.globalNotification().close();

    paymentDetails.expect.element('@pageTitle').text.to.equal('Update billing details');

    paymentDetails.expect.section('@paymentMethodPanel').to.be.visible;
    const paymentMethodPanel = paymentDetails.section.paymentMethodPanel;
    paymentMethodPanel.expect.section('@creditCard').to.be.visible;

    paymentDetails.expect.element('@changeRenewalFrequency').to.contain.text('Switch to annual');

    // Sidebar - Bill estimate
    paymentDetails.expect.section('@billEstimateSummary').to.be.visible;
    const billEstimate = paymentDetails.section.billEstimateSummary;
    billEstimate.autoTest();
  },

  'Billing details page address support': (client) => {
    const paymentDetails = client.page.paymentDetails();
    paymentDetails.navigate();
    paymentDetails.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    client.page.globalNotification().close();

    paymentDetails.expect.element('@organisationName').value.to.equal('Potato Corp.');
    paymentDetails.expect.element('@taxpayerId').value.to.equal('12345678200');
    paymentDetails.expect.element('@invoiceContactEmail').value.to.equal('bob-invoice@example.com');

    if (NO_AUTOCOMPLETE_PRESENT) {
      // nop
    } else {
      paymentDetails.click('@page'); // Without page focus address form will not show tooltips and the test fails
      paymentDetails.section.addressFields.assertAddressSupport({
        addressLine1: 'Level 6',
        addressLine2: '341 George St',
        city: 'Sydney',
        postcode: '2000',
        state: 'New South Wales',
        country: 'Australia'
      });
      paymentDetails.expect.element('payment-details-form *[aria-invalid=true]').to.not.be.present;
    }
  },

  'Billing details page validation': (client) => {
    client.mockRespondWith('billing/billing-details', {
      status: 200,
      method: 'GET',
      body: Object.assign({}, billingDetailsMock, {
        organisationName: undefined,
      })
    });
    const paymentDetails = client.page.paymentDetails();
    const globalNotification = client.page.globalNotification();

    const showFailValidation = () => {
      paymentDetails.click('@button');
      globalNotification.waitForElementVisible('@notification', ELEMENT_AWAIT_TIMEOUT);
      globalNotification.expect.element('@notification')
        .to.have.attribute('data-title').equal('Aww shoot, you\'re missing billing details');
      globalNotification.expect.element('@notification')
        .to.have.attribute('data-type').equal('error');
      globalNotification.close();
    };

    paymentDetails.navigate();
    paymentDetails.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    globalNotification.close();

    client.pause(CHUNK_AWAIT_TIMEOUT);
    showFailValidation();
    paymentDetails.setFormValue('@organisationName', 'Potato Corp.');
    client.pause(CHUNK_AWAIT_TIMEOUT);
    // showFailValidation();
    // paymentDetails.setFormValue('@creditCardName', 'Charlie');
    // client.pause(CHUNK_AWAIT_TIMEOUT);

    // should succeed
    paymentDetails.click('@button');
    globalNotification.waitForElementVisible('@notification', ELEMENT_AWAIT_TIMEOUT);
    globalNotification.expect.element('@notification')
      .to.have.attribute('data-type').equal('success');
  },

  'Billing details page validation on country change': (client) => {
    const paymentDetails = client.page.paymentDetails();

    paymentDetails.navigate();
    paymentDetails.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    client.page.globalNotification().close();

    const addressFields = paymentDetails.section.addressFields;

    addressFields.selectOption(
      client,
      addressFields.elements.country,
      'Belgium',
      true
    );
    if (!NO_AUTOCOMPLETE_PRESENT) {
      addressFields.expect.element('@addressLineFull').to.have.attribute('aria-invalid', true);
    } else {
      addressFields.expect.element('@addressLine1').to.have.attribute('aria-invalid', true);
    }
    paymentDetails.expect.element('@taxpayerId').to.have.attribute('aria-invalid', true);

    addressFields.selectOption(
      client,
      addressFields.elements.country,
      'Australia',
      true
    );

    client.pause(100);

    paymentDetails.expect.element('@taxpayerId').to.have.attribute('aria-invalid', false);
  },

  'Invoice contact submission failure': (client) => {
    client.mockRespondWith('billing/billing-details', {
      status: 200,
      method: 'GET',
      body: billingDetailsMock
    });
    client.mockRespondWith('billing/invoice-contact', {
      status: 500,
      method: 'PUT'
    });

    const paymentDetails = client.page.paymentDetails();
    paymentDetails.navigate();
    paymentDetails.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    client.page.globalNotification().close();

    paymentDetails.click('@button');
    const globalNotification = client.page.globalNotification();
    globalNotification.waitForElementVisible('@notification', ELEMENT_AWAIT_TIMEOUT);
    globalNotification.expect.element('@notification')
      .to.have.attribute('data-type').equal('error');
  },

  'Convert to annual link, should not exists for annual': (client) => {
    const paymentDetails = client.page.paymentDetails();

    client.mockRespondWith('billing/bill-estimate', {
      status: 200,
      method: 'GET',
      body: Object.assign({}, billingEstimateMock, {
        nextBillingPeriod: Object.assign(
          {},
          billingEstimateMock.nextBillingPeriod,
          { renewalFrequency: 'ANNUAL' }
        )
      })
    });

    paymentDetails.navigate();
    paymentDetails.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    client.page.globalNotification().close();

    paymentDetails.expect.element('@changeRenewalFrequency').to.not.contain.text('Switch to annual');
    paymentDetails.expect.element('@changeRenewalFrequency').to.contain.text('contact us');
  },

  'Add payment details should not exists for annual customers': (client) => {
    const paymentDetails = client.page.paymentDetails();

    client.mockRespondWith('billing/bill-estimate', {
      status: 200,
      method: 'GET',
      body: Object.assign({}, billingEstimateMock, {
        nextBillingPeriod: Object.assign(
          {},
          billingEstimateMock.nextBillingPeriod,
          { renewalFrequency: 'ANNUAL' }
        )
      })
    });

    paymentDetails.navigate();
    paymentDetails.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    client.page.globalNotification().close();

    paymentDetails.expect.element('@addPaymentDetailsPrompt').to.not.be.present;
  },

  'Should display account-is-under-management prompt when it is': (client) => {
    client.mockBillingDetails({
      status: 200,
      method: 'GET',
      body: Object.assign({}, billingDetailsMock, {
        managedByPartner: true
      })
    });
    const paymentDetails = client.page.paymentDetails();

    paymentDetails.navigate();
    paymentDetails.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    client.page.globalNotification().close();

    paymentDetails.expect.section('@accountIsUnderManagement').to.be.visible;
  },

  'Payment details focused task enabled - Credit card': (client) => {
    client.mockBillingDetails({
      status: 200,
      method: 'GET',
      body: Object.assign({}, billingDetailsMock, {
        paymentMethod: 'TOKENIZED_CREDIT_CARD',
        creditCard: {
          suffix: '1111',
          expiryMonth: 1,
          expiryYear: 2022,
          type: 'VISA',
          name: 'Superplasma Team'
        },
      })
    });

    const paymentDetails = client.page.paymentDetails();
    paymentDetails.navigate();
    paymentDetails.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    paymentDetails.expect.section('@paymentMethodPanel').to.be.present;

    const paymentMethodPanel = paymentDetails.section.paymentMethodPanel;
    paymentMethodPanel.expect.element('@title').text.to.equal('Payment method');
    paymentMethodPanel.expect.section('@creditCard').to.be.present;

    const creditCard = paymentMethodPanel.section.creditCard;
    creditCard.expect.element('@name').text.to.equal('Superplasma Team');
    creditCard.expect.element('@number').text.to.contain('1111');
    creditCard.expect.element('@expiry').text.to.contain('01/2022');
  },

  'Payment details focused task enabled - Paypal': (client) => {
    client.mockBillingDetails({
      status: 200,
      method: 'GET',
      body: Object.assign({}, billingDetailsMock, {
        paymentMethod: 'PAYPAL_ACCOUNT',
        paypalAccount: {
          email: 'user@example.com'
        },
      })
    });

    const paymentDetails = client.page.paymentDetails();
    paymentDetails.navigate();
    paymentDetails.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    paymentDetails.expect.section('@paymentMethodPanel').to.be.present;

    const paymentMethodPanel = paymentDetails.section.paymentMethodPanel;
    paymentMethodPanel.expect.element('@title').text.to.equal('Payment method');
    paymentMethodPanel.expect.section('@paypal').to.be.present;

    const paypal = paymentMethodPanel.section.paypal;
    paypal.expect.element('@email').text.to.equal('user@example.com');
  }
};
