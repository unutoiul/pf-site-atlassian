/* eslint-disable-next-line */
const fetch = require('node-fetch');

const getOverrides = (options = {}) => ({
  method: options.method || 'GET',
  status: options.status || 200,
  body: options.body || {},
  times: options.times || 1
});

function mockRespondWith(url, options, callback) {
  const overrides = getOverrides(options);

  this.perform((api, done) => {
    fetch(`${api.globals.buxMockUrl}/${url}`, {
      method: 'patch',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(overrides)
    }).then(() => {
      // eslint-disable-next-line no-console
      console.log('Patching complete, resuming tests');
      done();
    });
  }, callback);

  return this;
}

exports.createMockRespondWith = function createMockRespondWith(url) {
  return function mockRespondWithForUrl(options, callback) {
    return mockRespondWith.apply(this, [url, options, callback]);
  };
};
exports.command = mockRespondWith;
