/**
 * This is to navigate the browser off the billing pages and reset it to a blank state.
 *
 * Why do we need this?
 *
 * Turns out the .navigate() function in nightwatch page objects, do not actually navigate or refresh the page
 * if the browser is already on the same URL.
 *
 * This causes problems as we may change the mock state between each test.
 *
 * Unfortunate the provided beforeEach API is not good enough. beforeEach executes before each 'test case'.
 * A test case is the entire collection of tests in each file.
 * What we need is a beforeEach before each test, like any other sane test libraries.
 */
exports.command = function reset(callback) {
  this.perform((api, done) => {
    // eslint-disable-next-line no-console
    console.log('Navigating browser to blank page.');
    api.url('about:blank', () => {
      done();
      callback && callback();
    });
  });

  return this;
};