const { createMockRespondWith } = require('./mockRespondWith');

exports.command = createMockRespondWith('billing/bill-estimate');
