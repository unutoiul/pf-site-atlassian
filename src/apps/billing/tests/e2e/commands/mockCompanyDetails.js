const createMockRespondWith = require('./mockRespondWith').createMockRespondWith;

exports.command = createMockRespondWith('billing/customer-details');
