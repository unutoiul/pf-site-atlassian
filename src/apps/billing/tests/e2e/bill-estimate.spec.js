
const CHUNK_AWAIT_TIMEOUT = 10000;

module.exports = {
  beforeEach(client) {
    client.reset();
    client.maximizeWindow();
  },

  'Should render the page': (client) => {
    const billEstimate = client.page.billEstimate();

    billEstimate.navigate();
    billEstimate.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    billEstimate.assert.count('@product', 9);

    billEstimate.expect.element(billEstimate.dynEl('@productRow', 1, 'description'))
      .text.to.equal('Bitbucket');
    billEstimate.expect.element(billEstimate.dynEl('@productRow', 1, 'price'))
      .text.to.equal('$3,720');
    billEstimate.expect.element(billEstimate.dynEl('@productUsage', 1, 1))
      .text.to.equal('742 users');
    billEstimate.expect.element(billEstimate.dynEl('@productUsage', 1, 2))
      .text.to.equal('30 gigabytes');
    billEstimate.expect.element(billEstimate.dynEl('@productUsage', 1, 3))
      .text.to.equal('50 build minutes');

    billEstimate.expect.element(billEstimate.dynEl('@productRow', 3, 'description'))
      .text.to.equal('Identity Manager (Cloud)');
    billEstimate.expect.element(billEstimate.dynEl('@productRow', 3, 'price'))
      .text.to.equal('Inactive');
    billEstimate.expect.element(billEstimate.dynEl('@productUsage', 3, 1))
      .text.to.equal('Unlimited');

    billEstimate.expect.element(billEstimate.dynEl('@productRow', 4, 'description'))
      .text.to.equal('JIRA Core (Cloud)');
    billEstimate.expect.element(billEstimate.dynEl('@productRow', 4, 'price'))
      .text.to.equal('Free');
    billEstimate.expect.element(billEstimate.dynEl('@productUsage', 4, 1))
      .text.to.equal('1 user');

    billEstimate.expect.element('@taxTotal').text.to.match(/USD\s?1,139.00/);
    billEstimate.expect.element('@total').text.to.match(/USD\s?12,529.00/);
    billEstimate.expect.element('@autoChargedLabel')
      .text.to.equal('Based on your current configuration, you are being automatically charged $12,529.');
  }
};
