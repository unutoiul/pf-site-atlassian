const billingDetailsMock = require('../../../../mock-server/bux/api/billing/billing-details-get.json');

const CHUNK_AWAIT_TIMEOUT = 5000;
const ELEMENT_AWAIT_TIMEOUT = 2000;

module.exports = {
  beforeEach(client) {
    client.reset();
    client.maximizeWindow();
    client.mockBillingDetails({
      status: 200,
      method: 'GET',
      body: Object.assign({}, billingDetailsMock, {
        creditCard: {}
      })
    });
  },

  'Happy path': (client) => {
    const paymentDetails = client.page.paymentDetails();
    paymentDetails.navigate();
    paymentDetails.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    paymentDetails.click('@page'); // Gives page focus

    client.page.globalNotification().close();

    paymentDetails.expect.element('@addPaymentDetailsPrompt')
      .to.have.attribute('href').and.include('/paymentdetails/add');
    paymentDetails.click('@addPaymentDetailsPrompt');

    client.assert.urlContains('/paymentdetails/add');

    paymentDetails.expect.section('@addPaymentDetails').to.be.present;

    // should fill billing address
    const stepBillingAddress = paymentDetails.section.addPaymentDetails.section.stepBillingAddress;
    stepBillingAddress.section.addressFields.assertAddressSupport({
      addressLine1: 'Level 6',
      addressLine2: '341 George St',
      city: 'Sydney',
      postcode: '2000',
      state: 'NSW',
      country: 'AU'
    });
    stepBillingAddress.click('@nextButton');

    // should refill address if go back
    const stepBillingMethod = paymentDetails.section.addPaymentDetails.section.stepBillingMethod;
    stepBillingMethod.click('@backButton');
    stepBillingAddress.expect.element('@organisationName').value.to.equal('Potato Corp.');
    stepBillingAddress.expect.element('#addressLineFull').value.to.equal('Level 6, 341 George St, 2000, Sydney, NSW');
    stepBillingAddress.click('@nextButton');

    // should have paypal button
    stepBillingMethod.expect.element('@paypalButton').to.be.visible.after(CHUNK_AWAIT_TIMEOUT);

    // should fill credit card details
    stepBillingMethod.setIFrameFormValue('@creditCardNumberIFrame', 'input', '4444333322221111');
    stepBillingMethod.setFormValue('@creditCardName', 'Charlie');
    stepBillingMethod.selectAkOption(client, '@expiryMonth', 5);
    const selectedExpiryYear = new Date().getFullYear() + 1;
    stepBillingMethod.selectAkOption(client, '@expiryYear', selectedExpiryYear);
    stepBillingMethod.setIFrameFormValue('@ccvIFrame', 'input', '100');
    stepBillingMethod.click('@nextButton');

    // should have confirmation data
    const stepOverview = paymentDetails.section.addPaymentDetails.section.stepOverview;
    stepOverview.expect.element('@address')
      .text.to.equal('Potato Corp., Level 6, 341 George St, 2000, Sydney, NSW, AU');

    const paymentMethod = stepOverview.section.paymentMethod;
    paymentMethod.expect.element('@number').text.to.contain('1111');
    paymentMethod.expect.element('@name').text.to.equal('Charlie');
    paymentMethod.expect.element('@expiry').text.to.equal('Expires 05/2022');

    const billEstimate = stepOverview.section.billEstimate;
    billEstimate.assert.count('@product', 8);
    billEstimate.expect.element(billEstimate.dynEl('@productRow', 1, 'description'))
      .text.to.equal('Bitbucket');
    billEstimate.expect.element(billEstimate.dynEl('@productRow', 1, 'price'))
      .text.to.equal('$3,720');
    billEstimate.expect.element(billEstimate.dynEl('@productRow', 1, 'usage-list'))
      .text.to.equal('742 users, 30 gigabytes, 50 build minutes');

    billEstimate.expect.element(billEstimate.dynEl('@productRow', 3, 'description'))
      .text.to.equal('Identity Manager (Cloud)');
    billEstimate.expect.element(billEstimate.dynEl('@productRow', 3, 'price'))
      .text.to.equal('Inactive');
    billEstimate.expect.element(billEstimate.dynEl('@productRow', 3, 'usage-list'))
      .text.to.equal('Unlimited');

    billEstimate.expect.element(billEstimate.dynEl('@productRow', 4, 'description'))
      .text.to.equal('JIRA Core (Cloud)');
    billEstimate.expect.element(billEstimate.dynEl('@productRow', 4, 'price'))
      .text.to.equal('Free');
    billEstimate.expect.element(billEstimate.dynEl('@productRow', 4, 'usage-list'))
      .text.to.equal('1 user');

    stepOverview.expect.element('@taxTotal').text.to.match(/USD\s?1,139.00/);
    stepOverview.expect.element('@total').text.to.match(/USD\s?1,139.00/);
    stepOverview.expect.element('@chargeAdvise').text.to.contain('$12,529');
    stepOverview.expect.element('@chargeAdvise').text.to.contain('Oct 6, 2022');

    stepOverview.expect.element('@nextButton').to.have.attribute('disabled');
    stepOverview.click('@agreement');
    stepOverview.expect.element('@nextButton').to.not.have.attribute('disabled');

    stepOverview.click('@nextButton');

    const globalNotification = client.page.globalNotification();
    globalNotification.waitForElementVisible('@notification', ELEMENT_AWAIT_TIMEOUT);
    globalNotification.expect.element('@notification')
      .to.have.attribute('data-title').equal('Billing details updated');
    globalNotification.expect.element('@notification')
      .to.have.attribute('data-type').equal('success');
  },

  'Fail to get TNS session': (client) => {
    client.mockRespondWith('api/credit-card/session', {
      status: 500,
      method: 'POST',
      body: {}
    });

    const paymentDetails = client.page.paymentDetails();
    paymentDetails.navigate();
    paymentDetails.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    paymentDetails.click('@addPaymentDetailsPrompt');
    const stepBillingAddress = paymentDetails.section.addPaymentDetails.section.stepBillingAddress;
    stepBillingAddress.click('@nextButton');

    const addPaymentDetails = paymentDetails.section.addPaymentDetails;

    addPaymentDetails.expect.section('@stepBillingMethod').to.be.visible.after(CHUNK_AWAIT_TIMEOUT);
    addPaymentDetails.section.stepBillingMethod.expect.element('@creditCardErrorActions').to.be.visible;
  },

  'Fail to save credit card details': (client) => {
    client.mockRespondWith('api/credit-card/session/SESSION000000000000000000000001/save', {
      status: 500,
      method: 'POST',
      body: {}
    });

    const paymentDetails = client.page.paymentDetails();
    paymentDetails.navigate();
    paymentDetails.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    paymentDetails.click('@addPaymentDetailsPrompt');
    const stepBillingAddress = paymentDetails.section.addPaymentDetails.section.stepBillingAddress;
    stepBillingAddress.click('@nextButton');

    const addPaymentDetails = paymentDetails.section.addPaymentDetails;

    const stepBillingMethod = addPaymentDetails.section.stepBillingMethod;
    stepBillingMethod.setIFrameFormValue('@creditCardNumberIFrame', 'input', '4444333322221111');
    stepBillingMethod.setFormValue('@creditCardName', 'Charlie');
    stepBillingMethod.selectAkOption(client, '@expiryMonth', 5);
    const selectedExpiryYear = new Date().getFullYear() + 1;
    stepBillingMethod.selectAkOption(client, '@expiryYear', selectedExpiryYear);
    stepBillingMethod.setIFrameFormValue('@ccvIFrame', 'input', '100');
    stepBillingMethod.click('@nextButton');

    const stepOverview = addPaymentDetails.section.stepOverview;
    stepOverview.click('@agreement');
    stepOverview.click('@nextButton');

    paymentDetails.section.notification.assertMessage('error');
  },

  'Focused task flow': (client) => {
    const paymentDetails = client.page.paymentDetails();
    paymentDetails.navigate();
    paymentDetails.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    // should open focused task and close by button click
    paymentDetails.click('@addPaymentDetailsPrompt');
    paymentDetails.expect.section('@addPaymentDetails').to.be.visible.after(CHUNK_AWAIT_TIMEOUT);
    client.assert.urlContains('/paymentdetails/add');
    paymentDetails.click('@focusedTaskCloseButton');
    paymentDetails.assert.elementNotPresent('.focusedTaskContent');

    // should open focused task and then close by back navigation
    paymentDetails.click('@addPaymentDetailsPrompt');
    paymentDetails.expect.section('@addPaymentDetails').to.be.visible.after(CHUNK_AWAIT_TIMEOUT);
    client.assert.urlContains('/paymentdetails/add');
    client.back();
    paymentDetails.assert.elementNotPresent('.focusedTaskContent');

    // should open focused task by accessing the update url
    client.mockBillingDetails({
      status: 200,
      method: 'GET',
      body: Object.assign({}, billingDetailsMock, {
        creditCard: {}
      })
    });
    client.url(`${client.globals.serverUrl}/paymentdetails/add`);
    paymentDetails.expect.section('@addPaymentDetails').to.be.visible.after(100000000);
  }
};
