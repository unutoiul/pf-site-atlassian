const billingDetailsMock = require('../../../../mock-server/bux/api/billing/billing-details-get.json');

const CHUNK_AWAIT_TIMEOUT = 5000;

module.exports = {
  beforeEach(client) {
    client.reset();
    client.maximizeWindow();
  },

  'Replace credit card': (client) => {
    const paymentDetails = client.page.paymentDetails();
    paymentDetails.navigate();
    paymentDetails.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    paymentDetails.click('@editPaymentDetails');
    paymentDetails.expect.section('@editPaymentMethod').to.be.visible.after(CHUNK_AWAIT_TIMEOUT);
    client.assert.urlContains('/paymentdetails/update');

    const editPaymentMethod = paymentDetails.section.editPaymentMethod;

    editPaymentMethod.expect.element('@paypalButton').to.be.visible.after(CHUNK_AWAIT_TIMEOUT);

    editPaymentMethod.setIFrameFormValue('@creditCardNumberIFrame', 'input', '4444333322221111');
    editPaymentMethod.selectAkOption(client, '@expiryMonth', 4);
    const selectedExpiryYear = new Date().getFullYear() + 1;
    editPaymentMethod.selectAkOption(client, '@expiryYear', selectedExpiryYear);
    editPaymentMethod.setIFrameFormValue('@ccvIFrame', 'input', '100');

    editPaymentMethod.click('@buttonSubmit');

    editPaymentMethod.expect.element('@creditCardNameFieldError')
      .text.to.equal('Please enter a valid cardholder name');

    editPaymentMethod.setFormValue('@creditCardName', 'Charlie');

    editPaymentMethod.click('@buttonSubmit');

    paymentDetails.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);
    paymentDetails.section.notification.assertMessage('success');
  },

  'From paypal to credit card': (client) => {
    client.mockBillingDetails({
      status: 200,
      method: 'GET',
      body: Object.assign({}, billingDetailsMock, {
        paymentMethod: 'PAYPAL_ACCOUNT',
        creditCard: {},
        paypalAccount: {
          email: 'user@test.org'
        }
      })
    });

    const paymentDetails = client.page.paymentDetails();
    paymentDetails.navigate();
    paymentDetails.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    paymentDetails.click('@editPaymentDetails');
    paymentDetails.expect.section('@editPaymentMethod').to.be.visible.after(CHUNK_AWAIT_TIMEOUT);
    client.assert.urlContains('/paymentdetails/update');

    const editPaymentMethod = paymentDetails.section.editPaymentMethod;
    editPaymentMethod.expect.element('@paypalButton').not.to.be.present.before(CHUNK_AWAIT_TIMEOUT);
    editPaymentMethod.expect.element('@paypalStatus').text.to.equal('user@test.org');

    editPaymentMethod.setIFrameFormValue('@creditCardNumberIFrame', 'input', '4444333322221111');
    editPaymentMethod.selectAkOption(client, '@expiryMonth', 4);
    const selectedExpiryYear = new Date().getFullYear() + 1;
    editPaymentMethod.selectAkOption(client, '@expiryYear', selectedExpiryYear);
    editPaymentMethod.setIFrameFormValue('@ccvIFrame', 'input', '100');

    editPaymentMethod.click('@buttonSubmit');

    editPaymentMethod.expect.element('@creditCardNameFieldError')
      .text.to.equal('Please enter a valid cardholder name');

    editPaymentMethod.setFormValue('@creditCardName', 'Charlie');

    editPaymentMethod.click('@buttonSubmit');

    paymentDetails.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);
    paymentDetails.section.notification.assertMessage('success');
  },

  'Fail to get TNS session': (client) => {
    client.mockRespondWith('api/credit-card/session', {
      status: 500,
      method: 'POST',
      body: {}
    });

    const paymentDetails = client.page.paymentDetails();
    paymentDetails.navigate();
    paymentDetails.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    paymentDetails.click('@editPaymentDetails');
    paymentDetails.expect.section('@editPaymentMethod').to.be.visible.after(CHUNK_AWAIT_TIMEOUT);
    paymentDetails.section.editPaymentMethod.expect.element('@creditCardErrorActions').to.be.visible;
  },

  'Fail to save credit card details': (client) => {
    client.mockRespondWith('api/credit-card/session/SESSION000000000000000000000001/save', {
      status: 500,
      method: 'POST',
      body: {}
    });

    const paymentDetails = client.page.paymentDetails();
    paymentDetails.navigate();
    paymentDetails.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    paymentDetails.click('@editPaymentDetails');
    paymentDetails.expect.section('@editPaymentMethod').to.be.visible.after(CHUNK_AWAIT_TIMEOUT);

    const editPaymentMethod = paymentDetails.section.editPaymentMethod;

    editPaymentMethod.setIFrameFormValue('@creditCardNumberIFrame', 'input', '4444333322221111');
    editPaymentMethod.setFormValue('@creditCardName', 'Charlie');
    editPaymentMethod.selectAkOption(client, '@expiryMonth', 4);
    const selectedExpiryYear = new Date().getFullYear() + 1;
    editPaymentMethod.selectAkOption(client, '@expiryYear', selectedExpiryYear);
    editPaymentMethod.setIFrameFormValue('@ccvIFrame', 'input', '100');
    editPaymentMethod.click('@buttonSubmit');

    paymentDetails.section.notification.assertMessage('error');
  },

  'Focused task flow': (client) => {
    const paymentDetails = client.page.paymentDetails();
    paymentDetails.navigate();
    paymentDetails.waitForElementVisible('@page', CHUNK_AWAIT_TIMEOUT);

    // should open focused task and close by button click
    paymentDetails.click('@editPaymentDetails');
    paymentDetails.expect.section('@editPaymentMethod').to.be.visible.after(CHUNK_AWAIT_TIMEOUT);
    client.assert.urlContains('/paymentdetails/update');
    paymentDetails.click('@focusedTaskCloseButton');
    paymentDetails.assert.elementNotPresent('.focusedTaskContent');

    // should open focused task and then close by back navigation
    paymentDetails.click('@editPaymentDetails');
    paymentDetails.expect.section('@editPaymentMethod').to.be.visible.after(CHUNK_AWAIT_TIMEOUT);
    client.assert.urlContains('/paymentdetails/update');
    client.back();
    paymentDetails.assert.elementNotPresent('.focusedTaskContent');

    // should open focused task by accessing the update url
    client.url(`${client.globals.serverUrl}/paymentdetails/update`);
    paymentDetails.expect.section('@editPaymentMethod').to.be.visible.after(1000000000000);
  }
};
