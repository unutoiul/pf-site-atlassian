const toSelector = (object) => {
  if (typeof object === 'string') {
    return {
      selector: object
    };
  }
  return object;
};

const forObjects = (objects, callback) => {
  const result = {};
  Object.keys(objects).map(key => (result[key] = callback(toSelector(objects[key]))));
  return result;
};

/* eslint-disable-next-line */
const self = module.exports = {
  className(className) {
    return `[contains(concat(" ", normalize-space(@class), " "), " ${className} ")]`;
  },

  attr(name, value) {
    return `[@${name}="${value}"]`;
  },

  byClassName(className) {
    return `*${self.className(className)}`;
  },

  toSelector(selector) {
    return {
      selector,
      locateStrategy: 'xpath'
    };
  },

  child(selector) {
    return `.//${selector}`;
  },

  anywhere(selector) {
    return `//${selector}`;
  },

  nested(objects) {
    return forObjects(objects, object => Object.assign({}, object, {
      selector: self.child(object.selector)
    }));
  },

  xpath(objects) {
    return forObjects(objects, object => Object.assign({}, object, {
      locateStrategy: 'xpath'
    }));
  }
};
