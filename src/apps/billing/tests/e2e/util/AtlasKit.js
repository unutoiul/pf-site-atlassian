const { getElementSelector, setFormValue } = require('./helpers');

module.exports = {
  selectOption(client, selector, value, useAutocomplete) {
    this.api.useXpath();
    const inputSelector = getElementSelector(this.elements, selector);
    // lift to component root
    const topElement = `${inputSelector}/../../../../..`;

    this.click(topElement);
    this.api.pause(100);

    if (useAutocomplete) {
      setFormValue.call(this, `${topElement}//input`, value);
      this.api.pause(100);
    }
    this.api.click(`//*[@role="option" and text()[contains(.,"${value}")]]`);
    this.api.useCss();
  }
};