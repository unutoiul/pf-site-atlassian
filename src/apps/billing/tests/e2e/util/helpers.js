const util = require('util');
const qs = require('querystring');

const getElementSelector = (elements, element) => {
  if (typeof element === 'string' && element.charAt(0) === '@') {
    return elements[element.slice(1)].selector;
  } else if (typeof element === 'string') {
    return element;
  }
  return element.selector;
};

const clearValue = (client, element) => {
  // This doesn't always work
  client.clearValue(element);
  // emulate Select-All + erase. Both for Mac(local dev) and rest.
  client.setValue(element, [client.api.Keys.CONTROL, 'a'], () => {
    client.setValue(element, client.api.Keys.BACK_SPACE);
  });

  client.setValue(element, [client.api.Keys.COMMAND, 'a'], () => {
    client.setValue(element, client.api.Keys.BACK_SPACE);
  });
};

const setValue = (client, element, value) => {
  value.split('').forEach(char =>
    client.setValue(element, char));
};

module.exports = {
  getElementSelector,

  //Waiting for support by nightwatch to remove this (https://github.com/nightwatchjs/nightwatch/pull/1001)
  dynamicElement(element, ...rest) {
    const selector = getElementSelector(this.elements, element);
    return util.format(selector, ...rest);
  },
  setFormValue(element, value) {
    // By default the selenium element.setValue only appends onto
    // the current value. This function clears the existing value first.
    clearValue(this, element);
    setValue(this, element, value);
  },
  // Nightwatch currently does not support using .frame to get an iframe by class
  switchToIFrameBySelector(element, callback) {
    const frameSelector = getElementSelector(this.elements, element);
    return this.api.element('css selector', frameSelector, (frame) => {
      this.api.frame({ ELEMENT: frame.value.ELEMENT }, callback);
    });
  },
  setIFrameFormValue(iframeElement, elementSelector, value) {
    this.switchToIFrameBySelector(iframeElement, () => {
      this.waitForElementPresent(elementSelector, 100);
      this.setFormValue(elementSelector, value);
    }).frame(null);
  },
  selectOption(inputElement, value) {
    const inputSelector = getElementSelector(this.elements, inputElement);
    this.click(`${inputSelector} option[value="${value}"]`);
  },

  getTextForEach(selector, callback) {
    const list = [];
    return this.api
      .elements('css selector', getElementSelector(this.elements, selector), (elements) => {
        elements.value.forEach((element, index) => {
          this.api.elementIdText(element.ELEMENT, (result) => {
            list.push({
              index,
              value: result.value
            });
          });
        });
      })
      .perform(() => callback(list));
  },

  getAttributeForEach(selector, attributeName, callback) {
    const list = [];
    return this.api
      .elements('css selector', getElementSelector(this.elements, selector), (elements) => {
        elements.value.forEach((element, index) => {
          this.api.elementIdAttribute(element.ELEMENT, attributeName, (result) => {
            list.push({
              index,
              value: result.value
            });
          });
        });
      })
      .perform(() => callback(list));
  },

  navigateWithQueryParams(queryParams) {
    const query = queryParams && qs.stringify(queryParams);
    const baseUrl = typeof this.url === 'function' ? this.url() : this.url;
    const joinChar = baseUrl.indexOf('?') >= 0 ? '&' : '?';
    const url = query ? `${baseUrl}${joinChar}${query}` : baseUrl;
    return this.api
      .url(url)
      .waitForElementVisible('body', 10000, false);
  }
};