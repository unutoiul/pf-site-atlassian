/* eslint-disable-next-line */
require('babel-register');
/* eslint-disable-next-line */
const express = require('express');
const jsonServer = require('json-server');
const path = require('path');
const fs = require('fs');
const edgeServer = require('../../edge-server/index');

const server = express();
server.use(edgeServer());

const root = path.resolve(path.join(__dirname, '../../../../../'));
const dist = path.resolve(path.join(root, 'build/dist'));

console.log(dist);

server.use('/chunk-*', express.static(dist, {
  setHeaders: (res) => {
    res.setHeader('Content-Encoding', 'gzip');
  }
}));

server.use('/scripts', express.static(dist));

server.use(jsonServer.defaults({ static: 'dist' }));

server.get([
  '/admin',
  '/o/*',
  '/s/*',
  '/admin/s/*',
  '/admin/user*',
  '/admin/billing*',
], (req, res) => {
  const newRelic = fs.readFileSync(`${root}/bin/new-relic/stg.html`).toString();
  const index = fs.readFileSync(`${dist}/index.html`).toString();
  res.send(index
    .split('<meta new-relic-placeholder>').join(newRelic)
    .split('__ENV__').join('local')
    .split('__CDN_URL__')
    .join('/scripts'));
  res.end();
});

server.listen(4000, () => {
  console.log('E2E Server is running');
});

