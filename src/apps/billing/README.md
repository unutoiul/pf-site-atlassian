# Billing UX Frontend
Single page web app and web component for Unified Billing.

This uses React and Redux

## Setting up a development environment

1. Clone the repository
2. Install nvm https://github.com/creationix/nvm#install-script
3. `nvm install` to install proper node version for this session
4. Install Yarn https://yarnpkg.com/en/docs/install
5. Login to Atlassian's internal NPM registry using your staff ID ([read this](https://extranet.atlassian.com/display/RELENG/2017/11/26/Action+Required+-+packages.atlassian.com+is+replacing+npm-private.atlassian.io))

```
yarn login --registry=https://packages.atlassian.com/api/npm/atlassian-npm --scope=atlassian
```

6. `yarn install` to install the npm dependencies
7. `yarn build` to run a sanity check to ensure everything is working


## During development

* `yarn start` starts the app. BUX will now be available at [http://localhost:8080](http://localhost:8080)
* `yarn run lint` run eslint
* `yarn run lint:fix` automatically fix eslint issues
* `yarn storybook` to run storybook on [http://localhost:6006](http://localhost:6006)

### Unit Tests

Unit Tests are defined in the src folder next to their implementation in a 'tests' folder and should be called 'file
.spec.js'

* `yarn test` run the unit tests
* `yarn run test:watch` run unit tests and watch for changes

#### Debugging tests with WebStorm + Mocha

1. Open the 'Edit Configuration' dialog (Run > Edit Configuration)
2. Open the Defaults tree and select Mocha
3. Add the following values
  * Environmental Variables: `BABEL_ENV=test`
  * Extra Mocha options: `--require ignore-styles --require tests/common/scaffolding --compilers js:babel-core/register`
4. Go to any mocha unit test, and click the green play button next to a `describe`, that will create a Run configuration
for that file.

### E2E Tests

E2E test are using nightwatch and selenium. It starts a mock server on port 3000 which serves the static files.

* `yarn run test:e2e:bux:nightwatch` run the e2e tests

Show sources in nightwatch tests

    browser.source(function (result){
      console.log(result.value);
    });

### Cross browser testing on BrowserStack

We can run the e2e tests on various web browser, operating system combinations on BrowserStack by executing the script like this.

`./bin/bamboo/run-browserstack-tests.sh <edge|ie|chrome|firefox>`

Currently we run these tests on Microsoft Windows 8.1 or 10. Refer `nightwatch.json` for more details.

Note : You have to set following environment variables before running the tests, `BROWSERSTACK_USERNAME` and `BROWSERSTACK_ACCESS_KEY`. Find the details in `Lastpass > Supernova : BrowserStack API Keys`

### Cypress Tests

* `yarn cypress:run` release, start mock server and run the cypress e2e tests
* `yarn cypress:run:junit` release, start mock server and run the cypress e2e tests with JUNIT reports
* `yarn cypress:dev` Open cypress GUI and points test to localhost:8080 to run e2e tests against your dev env.

### Testing in Atlassian Cloud

#### Create your Atlassian Cloud

1. Sign up for an account in `https://qa-wac.internal.atlassian.com/ondemand/signup`
2. Get system administrators permission
- If vertigo, use [governator](https://governator.us-east-1.staging.atl-paas.net) to give yourself temporary
 system administrators permission. 
- If unicorn, login as sysadmin/sysadmin and give yourself `system-administrators` permission

#### Test loop

1. Run site admin in dev mode
2. Or run e2e mock to current built bundle against staging

### UI Documentation

## Deployments

BUX deployments use micros static service and only really deployed to adev. This is done automatically using DEPBAC and
can't be done manually without production AWS credentials.

Here's how to do it:
```
yarn release
yarn deploy:micros -- -e adev
yarn um:prod -- -e dev -k $KEY -s $SECRET
```

### Environments

| MICROS | UM | URL |
|---|---|---|
| adev | dev | https://billing-ux-frontend.ap-southeast-2.dev.atl-paas.net |
| stg-east | dog | https://billing-ux-frontend.us-east-1.staging.atl-paas.net |


While BUX is being deployed to staging, it isn't being used anywhere.

## Ejecting from registered components

If for some reason you need to eject BUX from the registered components, there's a handy script to do it.

`./bin/unified-billing/eject.js -e "$ENV" -k "$KEY" -s "$SECRET"`

Credentials can be found in LastPass under `GlobalAdminBilling / Billing S3 Bucket`

## Bamboo Builds

* [Branches build plan](https://sox-bamboo.internal.atlassian.com/browse/PLS-BUXUITEST)
* [Master build plan](https://deployment-bamboo.internal.atlassian.com/browse/PLS-BUXUITEST)
* [Master publish plan](https://deployment-bamboo.internal.atlassian.com/browse/PLS-BUXUIPUBLISH)

The bamboo plan templates for these builds can be found in billing-ux-frontend/bamboo-plan-template repository
If you wish to modify these builds, or add a new build for the Billing UX Frontend, please use this repository rather
than configuring the builds directly.
