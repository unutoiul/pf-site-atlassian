#!/usr/bin/env bash
REPORTER=""
CYPRESS_CMD="run"
PORT="4000"
REPORT_PATH="cypress-cloud"

if [[ $ORGANIZATION == 1 ]]; then
  REPORT_PATH="cypress-organization"
else
  ORGANIZATION=0
fi

if [[ $BAMBOO_BUILD == 1 ]]; then
  REPORTER="--reporter junit --reporter-options 'mochaFile=./build-output/tests/$REPORT_PATH/junit-output-[hash].xml'"
fi
if [[ $CYPRESS_UI == 1 ]]; then
  CYPRESS_CMD="open"
  PORT="3001"
fi

COMMAND="./node_modules/.bin/cypress $CYPRESS_CMD \
  --project src/apps/billing \
  --env BASE_URL=http://localhost:$PORT/,ORGANIZATION=$ORGANIZATION \
  $REPORTER"

echo $COMMAND
eval $COMMAND
