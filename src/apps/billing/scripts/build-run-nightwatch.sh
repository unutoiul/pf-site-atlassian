#!/usr/bin/env bash

source "./src/apps/billing/scripts/build.sh"

xvfb-run -a --server-args="-screen 0 1600x1200x24" yarn test:e2e:bux:nightwatch
