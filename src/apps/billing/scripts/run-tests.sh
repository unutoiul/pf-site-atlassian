#!/usr/bin/env bash
export BAMBOO_BUILD=1
set -xeu
[ -e /root/.npmrc ] && cp /root/.npmrc .

set +xeu
source ~/.bashrc
set -xeu

yarn install --no-emoji --no-progress --registry http://packages.atlassian.com/api/npm/npm-remote/

mkdir -p ./src/apps/billing/build-output/tests

yarn build

yarn test:e2e:bux:cypress

xvfb-run -a --server-args="-screen 0 1600x1200x24" yarn test:e2e:bux:nightwatch
