export BAMBOO_BUILD=1
set -xeu
[ -e /root/.npmrc ] && cp /root/.npmrc .

set +xeu
source ~/.bashrc
set -xeu

yarn install --no-emoji --no-progress --registry http://packages.atlassian.com/api/npm/npm-remote/

mkdir -p ./src/apps/billing/build-output/tests

NODE_OPTIONS=--max_old_space_size=4096 yarn build
