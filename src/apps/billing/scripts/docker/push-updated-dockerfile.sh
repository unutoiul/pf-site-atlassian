#!/usr/bin/env bash

cd "$(dirname "$0")"
docker build -t docker.atl-paas.net/purchasing/bux-baseagent .
docker push docker.atl-paas.net/purchasing/bux-baseagent