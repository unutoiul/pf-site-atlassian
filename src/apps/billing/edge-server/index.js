/* eslint-disable */
const express = require('express');
const fetch = require('node-fetch');
const createProxyMiddleware = require('http-proxy-middleware');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
/* eslint-enable */

module.exports = () => {
  const loadProxySettings = () => (req, res, next) => {
    req.buxProxySettings = {
      cloudId: req.cookies['bux.cloudId'] || 'cloud-id',
      serviceUrl: req.cookies['bux.service.url'],
      sessionToken: req.cookies['cloud.session.token.stg'],
    };
    next();
  };

  const app = express();
  app.use(cookieParser(), loadProxySettings());

  app.set('views', `${__dirname}/views`).set('view engine', 'ejs');
  app.route('/login')
    .get((req, res) => res.render('login', req.buxProxySettings))
    .post(bodyParser.urlencoded({ extended: true }), async (req, res) => {
      const stubAidUrl = `${req.body.serviceUrl}/stubs/aid/login`;
      try {
        const loginRes = await fetch(stubAidUrl);
        res.setHeader('set-cookie', loginRes.headers.get('set-cookie'));
      } catch (e) {
        console.warn(`Failed logging in to stub AID: ${stubAidUrl}`, e);
      }

      res.cookie('cloud.session.token.stg', req.body.sessionToken, {
        maxAge: 30 * 24 * 60 * 60 * 1000,
        httpOnly: true,
        secure: false,
      });
      res.cookie('bux.cloudId', req.body.cloudId);
      res.cookie('bux.service.url', req.body.serviceUrl);
      return res.render('login', req.buxProxySettings);
    });

  app.use('/logout', (req, res) => res.redirect('/login'));

  const proxyConf = {
    target: 'BUX Service', // Ignored, because we use router below
    router: req => req.buxProxySettings.serviceUrl || 'http://localhost:3100/',
    secure: false,
    // logLevel:'debug',
    onProxyReq(proxyReq) {
      // remove origin to pass CORS check
      proxyReq.removeHeader('Origin');
      proxyReq.removeHeader('Referer');
      proxyReq.setHeader('Referer', 'https://team-superplasma.jira-dev.com/');
    }
  };

  const billingProxy = createProxyMiddleware(proxyConf);

  const hubStarProxy = createProxyMiddleware({
    target: 'HUB Service', // Ignored, because we use router below
    router: req => req.buxProxySettings.serviceUrl || 'http://localhost:3002',
    secure: false,
    onProxyReq(proxyReq) {
      // remove origin to pass CORS check
      proxyReq.removeHeader('Origin');
      proxyReq.removeHeader('Referer');
    }
  });

  const hubProxy = createProxyMiddleware({
    target: 'http://localhost:3002',
  });

  app.use('/gateway/api/billing-ux', billingProxy);

  app.use('/gateway/api/permissions/permitted', hubProxy);

  app.use('/gateway', hubStarProxy);

  app.use('/_edge/tenant_info', (req, res) => {
    res.json({
      cloudId: req.cookies['bux.cloudId'] || '38b7be26-03f7-42a9-b18b-9cdb56d204c2'
    });
  });

  app.use('/admin/rest', hubProxy);

  return app;
};
